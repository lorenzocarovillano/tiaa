/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:14 PM
**        * FROM NATURAL LDA     : TWRL0902
************************************************************
**        * FILE NAME            : LdaTwrl0902.java
**        * CLASS NAME           : LdaTwrl0902
**        * INSTANCE NAME        : LdaTwrl0902
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl0902 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Flat_File3;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Company_Code;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Tax_Citizenship;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Contract_Nbr;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Payee_Cde;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Distribution_Cde;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Residency_Type;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Residency_Code;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Ivc_Ind;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Ivc_Protect;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Pymnt_Date;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Intfce_Dte;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Source_Code;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Payset_Type;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Gross_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Ivc_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Int_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Irr_Amt;
    private DbsField pnd_Flat_File3_Pnd_Ff3_State_Wthld;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Local_Wthld;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Plan;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Subplan;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan;
    private DbsField pnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind;

    public DbsGroup getPnd_Flat_File3() { return pnd_Flat_File3; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr() { return pnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Company_Code() { return pnd_Flat_File3_Pnd_Ff3_Company_Code; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship() { return pnd_Flat_File3_Pnd_Ff3_Tax_Citizenship; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr() { return pnd_Flat_File3_Pnd_Ff3_Contract_Nbr; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Payee_Cde() { return pnd_Flat_File3_Pnd_Ff3_Payee_Cde; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde() { return pnd_Flat_File3_Pnd_Ff3_Distribution_Cde; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Residency_Type() { return pnd_Flat_File3_Pnd_Ff3_Residency_Type; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Residency_Code() { return pnd_Flat_File3_Pnd_Ff3_Residency_Code; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind() { return pnd_Flat_File3_Pnd_Ff3_Ivc_Ind; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect() { return pnd_Flat_File3_Pnd_Ff3_Ivc_Protect; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date() { return pnd_Flat_File3_Pnd_Ff3_Pymnt_Date; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Intfce_Dte() { return pnd_Flat_File3_Pnd_Ff3_Intfce_Dte; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Source_Code() { return pnd_Flat_File3_Pnd_Ff3_Source_Code; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Payset_Type() { return pnd_Flat_File3_Pnd_Ff3_Payset_Type; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Gross_Amt() { return pnd_Flat_File3_Pnd_Ff3_Gross_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt() { return pnd_Flat_File3_Pnd_Ff3_Ivc_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Int_Amt() { return pnd_Flat_File3_Pnd_Ff3_Int_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt() { return pnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt() { return pnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt() { return pnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Irr_Amt() { return pnd_Flat_File3_Pnd_Ff3_Irr_Amt; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_State_Wthld() { return pnd_Flat_File3_Pnd_Ff3_State_Wthld; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Local_Wthld() { return pnd_Flat_File3_Pnd_Ff3_Local_Wthld; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Plan() { return pnd_Flat_File3_Pnd_Ff3_Plan; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Subplan() { return pnd_Flat_File3_Pnd_Ff3_Subplan; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct() { return pnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan() { return pnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan; }

    public DbsField getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind() { return pnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Flat_File3 = newGroupInRecord("pnd_Flat_File3", "#FLAT-FILE3");
        pnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr", "#FF3-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_Flat_File3_Pnd_Ff3_Company_Code = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Company_Code", "#FF3-COMPANY-CODE", FieldType.STRING, 
            1);
        pnd_Flat_File3_Pnd_Ff3_Tax_Citizenship = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Tax_Citizenship", "#FF3-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Flat_File3_Pnd_Ff3_Contract_Nbr = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Contract_Nbr", "#FF3-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Flat_File3_Pnd_Ff3_Payee_Cde = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Payee_Cde", "#FF3-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Flat_File3_Pnd_Ff3_Distribution_Cde = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Distribution_Cde", "#FF3-DISTRIBUTION-CDE", FieldType.STRING, 
            2);
        pnd_Flat_File3_Pnd_Ff3_Residency_Type = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Residency_Type", "#FF3-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Flat_File3_Pnd_Ff3_Residency_Code = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Residency_Code", "#FF3-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Flat_File3_Pnd_Ff3_Ivc_Ind = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Ivc_Ind", "#FF3-IVC-IND", FieldType.STRING, 1);
        pnd_Flat_File3_Pnd_Ff3_Ivc_Protect = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Ivc_Protect", "#FF3-IVC-PROTECT", FieldType.BOOLEAN);
        pnd_Flat_File3_Pnd_Ff3_Pymnt_Date = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Pymnt_Date", "#FF3-PYMNT-DATE", FieldType.STRING, 8);
        pnd_Flat_File3_Pnd_Ff3_Intfce_Dte = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Intfce_Dte", "#FF3-INTFCE-DTE", FieldType.STRING, 8);
        pnd_Flat_File3_Pnd_Ff3_Source_Code = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Source_Code", "#FF3-SOURCE-CODE", FieldType.STRING, 
            4);
        pnd_Flat_File3_Pnd_Ff3_Payset_Type = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Payset_Type", "#FF3-PAYSET-TYPE", FieldType.STRING, 
            2);
        pnd_Flat_File3_Pnd_Ff3_Gross_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Gross_Amt", "#FF3-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Flat_File3_Pnd_Ff3_Ivc_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Ivc_Amt", "#FF3-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Flat_File3_Pnd_Ff3_Int_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Int_Amt", "#FF3-INT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt", "#FF3-FED-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt", "#FF3-NRA-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt", "#FF3-CAN-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File3_Pnd_Ff3_Irr_Amt = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Irr_Amt", "#FF3-IRR-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_Flat_File3_Pnd_Ff3_State_Wthld = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_State_Wthld", "#FF3-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File3_Pnd_Ff3_Local_Wthld = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Local_Wthld", "#FF3-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File3_Pnd_Ff3_Plan = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Plan", "#FF3-PLAN", FieldType.STRING, 6);
        pnd_Flat_File3_Pnd_Ff3_Subplan = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Subplan", "#FF3-SUBPLAN", FieldType.STRING, 6);
        pnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct", "#FF3-ORGNTNG-CNTRCT", FieldType.STRING, 
            10);
        pnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan", "#FF3-ORGNTNG-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind = pnd_Flat_File3.newFieldInGroup("pnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind", "#FF3-TWRPYMNT-FATCA-IND", 
            FieldType.STRING, 1);

        this.setRecordName("LdaTwrl0902");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl0902() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
