/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:40 PM
**        * FROM NATURAL LDA     : TWRL444A
************************************************************
**        * FILE NAME            : LdaTwrl444a.java
**        * CLASS NAME           : LdaTwrl444a
**        * INSTANCE NAME        : LdaTwrl444a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl444a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Irs;
    private DbsField pnd_Irs_Pnd_T_Record_Type;
    private DbsField pnd_Irs_Pnd_T_Payment_Year;
    private DbsField pnd_Irs_Pnd_T_Prior_Yr_Data_Ind;
    private DbsField pnd_Irs_Pnd_T_Transmitters_Tin;
    private DbsField pnd_Irs_Pnd_T_Transmitter_Control_Code;
    private DbsField pnd_Irs_Pnd_T_Filler_1;
    private DbsField pnd_Irs_Pnd_T_Test_File_Indicator;
    private DbsField pnd_Irs_Pnd_T_Foreign_Entity_Ind;
    private DbsField pnd_Irs_Pnd_T_Transmitter_Name;
    private DbsField pnd_Irs_Pnd_T_Transmitter_Name_Continued;
    private DbsField pnd_Irs_Pnd_T_Company_Name;
    private DbsField pnd_Irs_Pnd_T_Company_Name_Continued;
    private DbsField pnd_Irs_Pnd_T_Company_Mailing_Address;
    private DbsField pnd_Irs_Pnd_T_Company_City;
    private DbsField pnd_Irs_Pnd_T_Company_State;
    private DbsField pnd_Irs_Pnd_T_Company_Zip_Code;
    private DbsField pnd_Irs_Pnd_T_Filler_2;
    private DbsField pnd_Irs_Pnd_T_Total_Number_Of_Payees;
    private DbsField pnd_Irs_Pnd_T_Contact_Name;
    private DbsField pnd_Irs_Pnd_T_Contact_Phone_N_Extension;
    private DbsField pnd_Irs_Pnd_T_Contact_E_Mail_Address;
    private DbsField pnd_Irs_Pnd_T_Magnetic_Tape_File_Ind;
    private DbsField pnd_Irs_Pnd_T_Media_Number;
    private DbsField pnd_Irs_Pnd_T_Filler_3;
    private DbsField pnd_Irs_Pnd_T_Sequence_Number;
    private DbsField pnd_Irs_Pnd_T_Filler_4;
    private DbsField pnd_Irs_Pnd_T_Vendor_Indicator;
    private DbsField pnd_Irs_Pnd_T_Filler_5;
    private DbsField pnd_Irs_Pnd_T_Vendor_Foreign_Entity_Ind;
    private DbsField pnd_Irs_Pnd_T_Filler_6;
    private DbsField pnd_Irs_Pnd_T_Blank_Or_Cr_Lf;
    private DbsGroup pnd_IrsRedef1;
    private DbsField pnd_Irs_Pnd_A_Record_Type;
    private DbsField pnd_Irs_Pnd_A_Payment_Year;
    private DbsField pnd_Irs_Pnd_A_Filler_1;
    private DbsField pnd_Irs_Pnd_A_Payer_Ein;
    private DbsField pnd_Irs_Pnd_A_Payer_Name_Control;
    private DbsField pnd_Irs_Pnd_A_Last_Filing_Ind;
    private DbsField pnd_Irs_Pnd_A_Combined_Filer;
    private DbsField pnd_Irs_Pnd_A_Type_Of_Return;
    private DbsField pnd_Irs_Pnd_A_Amount_Indicators;
    private DbsField pnd_Irs_Pnd_A_Filler_2;
    private DbsField pnd_Irs_Pnd_A_Foriegn_Corp_Indicator;
    private DbsField pnd_Irs_Pnd_A_Payer_Name_1;
    private DbsField pnd_Irs_Pnd_A_Payer_Name_2;
    private DbsField pnd_Irs_Pnd_A_Transfer_Agent_Indicator;
    private DbsField pnd_Irs_Pnd_A_Payer_Shipping_Address;
    private DbsField pnd_Irs_Pnd_A_Payer_City;
    private DbsField pnd_Irs_Pnd_A_Payer_State;
    private DbsField pnd_Irs_Pnd_A_Payer_Zip;
    private DbsField pnd_Irs_Pnd_A_Payer_Phone_No;
    private DbsField pnd_Irs_Pnd_A_Filler_4;
    private DbsField pnd_Irs_Pnd_A_Filler_5;
    private DbsField pnd_Irs_Pnd_A_Sequence_Number;
    private DbsField pnd_Irs_Pnd_A_Filler_6;
    private DbsGroup pnd_IrsRedef2;
    private DbsField pnd_Irs_Pnd_B_Record_Type;
    private DbsField pnd_Irs_Pnd_B_Payment_Year;
    private DbsField pnd_Irs_Pnd_B_Return_Indicator;
    private DbsField pnd_Irs_Pnd_B_Name_Control;
    private DbsField pnd_Irs_Pnd_B_Type_Of_Tin;
    private DbsField pnd_Irs_Pnd_B_Taxpayer_Id_Numb;
    private DbsField pnd_Irs_Pnd_B_Contract_Payee;
    private DbsGroup pnd_Irs_Pnd_B_Contract_PayeeRedef3;
    private DbsField pnd_Irs_Pnd_B_Contract_First_2b;
    private DbsField pnd_Irs_Pnd_B_Contract_Filler;
    private DbsField pnd_Irs_Pnd_B_Payers_Office_Code;
    private DbsField pnd_Irs_Pnd_B_Filler_1;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_1;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_2;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_3;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_4;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_5;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_6;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_7;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_8;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_9;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_A;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_B;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_C;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_D;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_E;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_F;
    private DbsField pnd_Irs_Pnd_B_Payment_Amount_G;
    private DbsField pnd_Irs_Pnd_B_Foreign_Indicator;
    private DbsField pnd_Irs_Pnd_B_Name_Line_1;
    private DbsField pnd_Irs_Pnd_B_Name_Line_2;
    private DbsField pnd_Irs_Pnd_B_Filler_3;
    private DbsField pnd_Irs_Pnd_B_Address_Line;
    private DbsField pnd_Irs_Pnd_B_Filler_4;
    private DbsField pnd_Irs_Pnd_B_City;
    private DbsField pnd_Irs_Pnd_B_State;
    private DbsField pnd_Irs_Pnd_B_Zip;
    private DbsField pnd_Irs_Pnd_B_Filler_5;
    private DbsField pnd_Irs_Pnd_B_Sequence_Number;
    private DbsField pnd_Irs_Pnd_B_Filler_6;
    private DbsField pnd_Irs_Pnd_B_Ira_Ind;
    private DbsField pnd_Irs_Pnd_B_Sep_Ind;
    private DbsField pnd_Irs_Pnd_B_Simple_Ind;
    private DbsField pnd_Irs_Pnd_B_Roth_Ind;
    private DbsField pnd_Irs_Pnd_B_Rmd_Ind;
    private DbsField pnd_Irs_Pnd_B_Yr_Pst_Cnt;
    private DbsField pnd_Irs_Pnd_B_Postpn_Code;
    private DbsField pnd_Irs_Pnd_B_Postpn_Rsn;
    private DbsField pnd_Irs_Pnd_B_Rep_Code;
    private DbsField pnd_Irs_Pnd_B_Rmd_Date;
    private DbsField pnd_Irs_Pnd_B_Codes;
    private DbsField pnd_Irs_Pnd_B_Filler_8;
    private DbsField pnd_Irs_Pnd_B_Special_Data_Entries;
    private DbsField pnd_Irs_Pnd_B_Filler_9;
    private DbsField pnd_Irs_Pnd_B_Combined_State;
    private DbsField pnd_Irs_Pnd_B_Filler_A;
    private DbsGroup pnd_IrsRedef4;
    private DbsField pnd_Irs_Pnd_C_Record_Type;
    private DbsField pnd_Irs_Pnd_C_Number_Of_Payees;
    private DbsField pnd_Irs_Pnd_C_Filler_1;
    private DbsField pnd_Irs_Pnd_C_Total_1;
    private DbsField pnd_Irs_Pnd_C_Total_2;
    private DbsField pnd_Irs_Pnd_C_Total_3;
    private DbsField pnd_Irs_Pnd_C_Total_4;
    private DbsField pnd_Irs_Pnd_C_Total_5;
    private DbsField pnd_Irs_Pnd_C_Total_6;
    private DbsField pnd_Irs_Pnd_C_Total_7;
    private DbsField pnd_Irs_Pnd_C_Total_8;
    private DbsField pnd_Irs_Pnd_C_Total_9;
    private DbsField pnd_Irs_Pnd_C_Total_A;
    private DbsField pnd_Irs_Pnd_C_Total_B;
    private DbsField pnd_Irs_Pnd_C_Total_C;
    private DbsField pnd_Irs_Pnd_C_Total_D;
    private DbsField pnd_Irs_Pnd_C_Total_E;
    private DbsField pnd_Irs_Pnd_C_Total_F;
    private DbsField pnd_Irs_Pnd_C_Total_G;
    private DbsField pnd_Irs_Pnd_C_Filler_2;
    private DbsField pnd_Irs_Pnd_C_Sequence_Number;
    private DbsField pnd_Irs_Pnd_C_Filler_4;
    private DbsField pnd_Irs_Pnd_C_Blank_Cr_Lf;
    private DbsGroup pnd_IrsRedef5;
    private DbsField pnd_Irs_Pnd_K_Record_Type;
    private DbsField pnd_Irs_Pnd_K_Number_Of_Payees;
    private DbsField pnd_Irs_Pnd_K_Filler_1;
    private DbsField pnd_Irs_Pnd_K_Total_1;
    private DbsField pnd_Irs_Pnd_K_Total_2;
    private DbsField pnd_Irs_Pnd_K_Total_3;
    private DbsField pnd_Irs_Pnd_K_Total_4;
    private DbsField pnd_Irs_Pnd_K_Total_5;
    private DbsField pnd_Irs_Pnd_K_Total_6;
    private DbsField pnd_Irs_Pnd_K_Total_7;
    private DbsField pnd_Irs_Pnd_K_Total_8;
    private DbsField pnd_Irs_Pnd_K_Total_9;
    private DbsField pnd_Irs_Pnd_K_Total_A;
    private DbsField pnd_Irs_Pnd_K_Total_B;
    private DbsField pnd_Irs_Pnd_K_Total_C;
    private DbsField pnd_Irs_Pnd_K_Total_D;
    private DbsField pnd_Irs_Pnd_K_Total_E;
    private DbsField pnd_Irs_Pnd_K_Total_F;
    private DbsField pnd_Irs_Pnd_K_Total_G;
    private DbsField pnd_Irs_Pnd_K_Filler_2;
    private DbsField pnd_Irs_Pnd_K_Sequence_Number;
    private DbsField pnd_Irs_Pnd_K_Filler_4;
    private DbsField pnd_Irs_Pnd_K_St_Tax_Withheld;
    private DbsField pnd_Irs_Pnd_K_Lc_Tax_Withheld;
    private DbsField pnd_Irs_Pnd_K_Filler_5;
    private DbsField pnd_Irs_Pnd_K_State_Code;
    private DbsField pnd_Irs_Pnd_K_Blank_Cr_Lf;
    private DbsGroup pnd_IrsRedef6;
    private DbsField pnd_Irs_Pnd_F_Record_Type;
    private DbsField pnd_Irs_Pnd_F_Number_Of_A_Records;
    private DbsField pnd_Irs_Pnd_F_Filler_1;
    private DbsField pnd_Irs_Pnd_F_Filler_2;
    private DbsField pnd_Irs_Pnd_F_Total_B_Records;
    private DbsField pnd_Irs_Pnd_F_Filler_3;
    private DbsField pnd_Irs_Pnd_F_Filler_4;
    private DbsField pnd_Irs_Pnd_F_Sequence_Number;
    private DbsField pnd_Irs_Pnd_F_Filler_5;
    private DbsField pnd_Irs_Pnd_F_Blank_Cr_Lf;
    private DbsGroup pnd_IrsRedef7;
    private DbsField pnd_Irs_Pnd_I700;
    private DbsField pnd_Irs_Pnd_I50;
    private DbsGroup pnd_Dump;
    private DbsField pnd_Dump_Pnd_D700;
    private DbsField pnd_Dump_Pnd_D50;

    public DbsGroup getPnd_Irs() { return pnd_Irs; }

    public DbsField getPnd_Irs_Pnd_T_Record_Type() { return pnd_Irs_Pnd_T_Record_Type; }

    public DbsField getPnd_Irs_Pnd_T_Payment_Year() { return pnd_Irs_Pnd_T_Payment_Year; }

    public DbsField getPnd_Irs_Pnd_T_Prior_Yr_Data_Ind() { return pnd_Irs_Pnd_T_Prior_Yr_Data_Ind; }

    public DbsField getPnd_Irs_Pnd_T_Transmitters_Tin() { return pnd_Irs_Pnd_T_Transmitters_Tin; }

    public DbsField getPnd_Irs_Pnd_T_Transmitter_Control_Code() { return pnd_Irs_Pnd_T_Transmitter_Control_Code; }

    public DbsField getPnd_Irs_Pnd_T_Filler_1() { return pnd_Irs_Pnd_T_Filler_1; }

    public DbsField getPnd_Irs_Pnd_T_Test_File_Indicator() { return pnd_Irs_Pnd_T_Test_File_Indicator; }

    public DbsField getPnd_Irs_Pnd_T_Foreign_Entity_Ind() { return pnd_Irs_Pnd_T_Foreign_Entity_Ind; }

    public DbsField getPnd_Irs_Pnd_T_Transmitter_Name() { return pnd_Irs_Pnd_T_Transmitter_Name; }

    public DbsField getPnd_Irs_Pnd_T_Transmitter_Name_Continued() { return pnd_Irs_Pnd_T_Transmitter_Name_Continued; }

    public DbsField getPnd_Irs_Pnd_T_Company_Name() { return pnd_Irs_Pnd_T_Company_Name; }

    public DbsField getPnd_Irs_Pnd_T_Company_Name_Continued() { return pnd_Irs_Pnd_T_Company_Name_Continued; }

    public DbsField getPnd_Irs_Pnd_T_Company_Mailing_Address() { return pnd_Irs_Pnd_T_Company_Mailing_Address; }

    public DbsField getPnd_Irs_Pnd_T_Company_City() { return pnd_Irs_Pnd_T_Company_City; }

    public DbsField getPnd_Irs_Pnd_T_Company_State() { return pnd_Irs_Pnd_T_Company_State; }

    public DbsField getPnd_Irs_Pnd_T_Company_Zip_Code() { return pnd_Irs_Pnd_T_Company_Zip_Code; }

    public DbsField getPnd_Irs_Pnd_T_Filler_2() { return pnd_Irs_Pnd_T_Filler_2; }

    public DbsField getPnd_Irs_Pnd_T_Total_Number_Of_Payees() { return pnd_Irs_Pnd_T_Total_Number_Of_Payees; }

    public DbsField getPnd_Irs_Pnd_T_Contact_Name() { return pnd_Irs_Pnd_T_Contact_Name; }

    public DbsField getPnd_Irs_Pnd_T_Contact_Phone_N_Extension() { return pnd_Irs_Pnd_T_Contact_Phone_N_Extension; }

    public DbsField getPnd_Irs_Pnd_T_Contact_E_Mail_Address() { return pnd_Irs_Pnd_T_Contact_E_Mail_Address; }

    public DbsField getPnd_Irs_Pnd_T_Magnetic_Tape_File_Ind() { return pnd_Irs_Pnd_T_Magnetic_Tape_File_Ind; }

    public DbsField getPnd_Irs_Pnd_T_Media_Number() { return pnd_Irs_Pnd_T_Media_Number; }

    public DbsField getPnd_Irs_Pnd_T_Filler_3() { return pnd_Irs_Pnd_T_Filler_3; }

    public DbsField getPnd_Irs_Pnd_T_Sequence_Number() { return pnd_Irs_Pnd_T_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_T_Filler_4() { return pnd_Irs_Pnd_T_Filler_4; }

    public DbsField getPnd_Irs_Pnd_T_Vendor_Indicator() { return pnd_Irs_Pnd_T_Vendor_Indicator; }

    public DbsField getPnd_Irs_Pnd_T_Filler_5() { return pnd_Irs_Pnd_T_Filler_5; }

    public DbsField getPnd_Irs_Pnd_T_Vendor_Foreign_Entity_Ind() { return pnd_Irs_Pnd_T_Vendor_Foreign_Entity_Ind; }

    public DbsField getPnd_Irs_Pnd_T_Filler_6() { return pnd_Irs_Pnd_T_Filler_6; }

    public DbsField getPnd_Irs_Pnd_T_Blank_Or_Cr_Lf() { return pnd_Irs_Pnd_T_Blank_Or_Cr_Lf; }

    public DbsGroup getPnd_IrsRedef1() { return pnd_IrsRedef1; }

    public DbsField getPnd_Irs_Pnd_A_Record_Type() { return pnd_Irs_Pnd_A_Record_Type; }

    public DbsField getPnd_Irs_Pnd_A_Payment_Year() { return pnd_Irs_Pnd_A_Payment_Year; }

    public DbsField getPnd_Irs_Pnd_A_Filler_1() { return pnd_Irs_Pnd_A_Filler_1; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Ein() { return pnd_Irs_Pnd_A_Payer_Ein; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Name_Control() { return pnd_Irs_Pnd_A_Payer_Name_Control; }

    public DbsField getPnd_Irs_Pnd_A_Last_Filing_Ind() { return pnd_Irs_Pnd_A_Last_Filing_Ind; }

    public DbsField getPnd_Irs_Pnd_A_Combined_Filer() { return pnd_Irs_Pnd_A_Combined_Filer; }

    public DbsField getPnd_Irs_Pnd_A_Type_Of_Return() { return pnd_Irs_Pnd_A_Type_Of_Return; }

    public DbsField getPnd_Irs_Pnd_A_Amount_Indicators() { return pnd_Irs_Pnd_A_Amount_Indicators; }

    public DbsField getPnd_Irs_Pnd_A_Filler_2() { return pnd_Irs_Pnd_A_Filler_2; }

    public DbsField getPnd_Irs_Pnd_A_Foriegn_Corp_Indicator() { return pnd_Irs_Pnd_A_Foriegn_Corp_Indicator; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Name_1() { return pnd_Irs_Pnd_A_Payer_Name_1; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Name_2() { return pnd_Irs_Pnd_A_Payer_Name_2; }

    public DbsField getPnd_Irs_Pnd_A_Transfer_Agent_Indicator() { return pnd_Irs_Pnd_A_Transfer_Agent_Indicator; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Shipping_Address() { return pnd_Irs_Pnd_A_Payer_Shipping_Address; }

    public DbsField getPnd_Irs_Pnd_A_Payer_City() { return pnd_Irs_Pnd_A_Payer_City; }

    public DbsField getPnd_Irs_Pnd_A_Payer_State() { return pnd_Irs_Pnd_A_Payer_State; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Zip() { return pnd_Irs_Pnd_A_Payer_Zip; }

    public DbsField getPnd_Irs_Pnd_A_Payer_Phone_No() { return pnd_Irs_Pnd_A_Payer_Phone_No; }

    public DbsField getPnd_Irs_Pnd_A_Filler_4() { return pnd_Irs_Pnd_A_Filler_4; }

    public DbsField getPnd_Irs_Pnd_A_Filler_5() { return pnd_Irs_Pnd_A_Filler_5; }

    public DbsField getPnd_Irs_Pnd_A_Sequence_Number() { return pnd_Irs_Pnd_A_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_A_Filler_6() { return pnd_Irs_Pnd_A_Filler_6; }

    public DbsGroup getPnd_IrsRedef2() { return pnd_IrsRedef2; }

    public DbsField getPnd_Irs_Pnd_B_Record_Type() { return pnd_Irs_Pnd_B_Record_Type; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Year() { return pnd_Irs_Pnd_B_Payment_Year; }

    public DbsField getPnd_Irs_Pnd_B_Return_Indicator() { return pnd_Irs_Pnd_B_Return_Indicator; }

    public DbsField getPnd_Irs_Pnd_B_Name_Control() { return pnd_Irs_Pnd_B_Name_Control; }

    public DbsField getPnd_Irs_Pnd_B_Type_Of_Tin() { return pnd_Irs_Pnd_B_Type_Of_Tin; }

    public DbsField getPnd_Irs_Pnd_B_Taxpayer_Id_Numb() { return pnd_Irs_Pnd_B_Taxpayer_Id_Numb; }

    public DbsField getPnd_Irs_Pnd_B_Contract_Payee() { return pnd_Irs_Pnd_B_Contract_Payee; }

    public DbsGroup getPnd_Irs_Pnd_B_Contract_PayeeRedef3() { return pnd_Irs_Pnd_B_Contract_PayeeRedef3; }

    public DbsField getPnd_Irs_Pnd_B_Contract_First_2b() { return pnd_Irs_Pnd_B_Contract_First_2b; }

    public DbsField getPnd_Irs_Pnd_B_Contract_Filler() { return pnd_Irs_Pnd_B_Contract_Filler; }

    public DbsField getPnd_Irs_Pnd_B_Payers_Office_Code() { return pnd_Irs_Pnd_B_Payers_Office_Code; }

    public DbsField getPnd_Irs_Pnd_B_Filler_1() { return pnd_Irs_Pnd_B_Filler_1; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_1() { return pnd_Irs_Pnd_B_Payment_Amount_1; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_2() { return pnd_Irs_Pnd_B_Payment_Amount_2; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_3() { return pnd_Irs_Pnd_B_Payment_Amount_3; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_4() { return pnd_Irs_Pnd_B_Payment_Amount_4; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_5() { return pnd_Irs_Pnd_B_Payment_Amount_5; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_6() { return pnd_Irs_Pnd_B_Payment_Amount_6; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_7() { return pnd_Irs_Pnd_B_Payment_Amount_7; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_8() { return pnd_Irs_Pnd_B_Payment_Amount_8; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_9() { return pnd_Irs_Pnd_B_Payment_Amount_9; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_A() { return pnd_Irs_Pnd_B_Payment_Amount_A; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_B() { return pnd_Irs_Pnd_B_Payment_Amount_B; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_C() { return pnd_Irs_Pnd_B_Payment_Amount_C; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_D() { return pnd_Irs_Pnd_B_Payment_Amount_D; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_E() { return pnd_Irs_Pnd_B_Payment_Amount_E; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_F() { return pnd_Irs_Pnd_B_Payment_Amount_F; }

    public DbsField getPnd_Irs_Pnd_B_Payment_Amount_G() { return pnd_Irs_Pnd_B_Payment_Amount_G; }

    public DbsField getPnd_Irs_Pnd_B_Foreign_Indicator() { return pnd_Irs_Pnd_B_Foreign_Indicator; }

    public DbsField getPnd_Irs_Pnd_B_Name_Line_1() { return pnd_Irs_Pnd_B_Name_Line_1; }

    public DbsField getPnd_Irs_Pnd_B_Name_Line_2() { return pnd_Irs_Pnd_B_Name_Line_2; }

    public DbsField getPnd_Irs_Pnd_B_Filler_3() { return pnd_Irs_Pnd_B_Filler_3; }

    public DbsField getPnd_Irs_Pnd_B_Address_Line() { return pnd_Irs_Pnd_B_Address_Line; }

    public DbsField getPnd_Irs_Pnd_B_Filler_4() { return pnd_Irs_Pnd_B_Filler_4; }

    public DbsField getPnd_Irs_Pnd_B_City() { return pnd_Irs_Pnd_B_City; }

    public DbsField getPnd_Irs_Pnd_B_State() { return pnd_Irs_Pnd_B_State; }

    public DbsField getPnd_Irs_Pnd_B_Zip() { return pnd_Irs_Pnd_B_Zip; }

    public DbsField getPnd_Irs_Pnd_B_Filler_5() { return pnd_Irs_Pnd_B_Filler_5; }

    public DbsField getPnd_Irs_Pnd_B_Sequence_Number() { return pnd_Irs_Pnd_B_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_B_Filler_6() { return pnd_Irs_Pnd_B_Filler_6; }

    public DbsField getPnd_Irs_Pnd_B_Ira_Ind() { return pnd_Irs_Pnd_B_Ira_Ind; }

    public DbsField getPnd_Irs_Pnd_B_Sep_Ind() { return pnd_Irs_Pnd_B_Sep_Ind; }

    public DbsField getPnd_Irs_Pnd_B_Simple_Ind() { return pnd_Irs_Pnd_B_Simple_Ind; }

    public DbsField getPnd_Irs_Pnd_B_Roth_Ind() { return pnd_Irs_Pnd_B_Roth_Ind; }

    public DbsField getPnd_Irs_Pnd_B_Rmd_Ind() { return pnd_Irs_Pnd_B_Rmd_Ind; }

    public DbsField getPnd_Irs_Pnd_B_Yr_Pst_Cnt() { return pnd_Irs_Pnd_B_Yr_Pst_Cnt; }

    public DbsField getPnd_Irs_Pnd_B_Postpn_Code() { return pnd_Irs_Pnd_B_Postpn_Code; }

    public DbsField getPnd_Irs_Pnd_B_Postpn_Rsn() { return pnd_Irs_Pnd_B_Postpn_Rsn; }

    public DbsField getPnd_Irs_Pnd_B_Rep_Code() { return pnd_Irs_Pnd_B_Rep_Code; }

    public DbsField getPnd_Irs_Pnd_B_Rmd_Date() { return pnd_Irs_Pnd_B_Rmd_Date; }

    public DbsField getPnd_Irs_Pnd_B_Codes() { return pnd_Irs_Pnd_B_Codes; }

    public DbsField getPnd_Irs_Pnd_B_Filler_8() { return pnd_Irs_Pnd_B_Filler_8; }

    public DbsField getPnd_Irs_Pnd_B_Special_Data_Entries() { return pnd_Irs_Pnd_B_Special_Data_Entries; }

    public DbsField getPnd_Irs_Pnd_B_Filler_9() { return pnd_Irs_Pnd_B_Filler_9; }

    public DbsField getPnd_Irs_Pnd_B_Combined_State() { return pnd_Irs_Pnd_B_Combined_State; }

    public DbsField getPnd_Irs_Pnd_B_Filler_A() { return pnd_Irs_Pnd_B_Filler_A; }

    public DbsGroup getPnd_IrsRedef4() { return pnd_IrsRedef4; }

    public DbsField getPnd_Irs_Pnd_C_Record_Type() { return pnd_Irs_Pnd_C_Record_Type; }

    public DbsField getPnd_Irs_Pnd_C_Number_Of_Payees() { return pnd_Irs_Pnd_C_Number_Of_Payees; }

    public DbsField getPnd_Irs_Pnd_C_Filler_1() { return pnd_Irs_Pnd_C_Filler_1; }

    public DbsField getPnd_Irs_Pnd_C_Total_1() { return pnd_Irs_Pnd_C_Total_1; }

    public DbsField getPnd_Irs_Pnd_C_Total_2() { return pnd_Irs_Pnd_C_Total_2; }

    public DbsField getPnd_Irs_Pnd_C_Total_3() { return pnd_Irs_Pnd_C_Total_3; }

    public DbsField getPnd_Irs_Pnd_C_Total_4() { return pnd_Irs_Pnd_C_Total_4; }

    public DbsField getPnd_Irs_Pnd_C_Total_5() { return pnd_Irs_Pnd_C_Total_5; }

    public DbsField getPnd_Irs_Pnd_C_Total_6() { return pnd_Irs_Pnd_C_Total_6; }

    public DbsField getPnd_Irs_Pnd_C_Total_7() { return pnd_Irs_Pnd_C_Total_7; }

    public DbsField getPnd_Irs_Pnd_C_Total_8() { return pnd_Irs_Pnd_C_Total_8; }

    public DbsField getPnd_Irs_Pnd_C_Total_9() { return pnd_Irs_Pnd_C_Total_9; }

    public DbsField getPnd_Irs_Pnd_C_Total_A() { return pnd_Irs_Pnd_C_Total_A; }

    public DbsField getPnd_Irs_Pnd_C_Total_B() { return pnd_Irs_Pnd_C_Total_B; }

    public DbsField getPnd_Irs_Pnd_C_Total_C() { return pnd_Irs_Pnd_C_Total_C; }

    public DbsField getPnd_Irs_Pnd_C_Total_D() { return pnd_Irs_Pnd_C_Total_D; }

    public DbsField getPnd_Irs_Pnd_C_Total_E() { return pnd_Irs_Pnd_C_Total_E; }

    public DbsField getPnd_Irs_Pnd_C_Total_F() { return pnd_Irs_Pnd_C_Total_F; }

    public DbsField getPnd_Irs_Pnd_C_Total_G() { return pnd_Irs_Pnd_C_Total_G; }

    public DbsField getPnd_Irs_Pnd_C_Filler_2() { return pnd_Irs_Pnd_C_Filler_2; }

    public DbsField getPnd_Irs_Pnd_C_Sequence_Number() { return pnd_Irs_Pnd_C_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_C_Filler_4() { return pnd_Irs_Pnd_C_Filler_4; }

    public DbsField getPnd_Irs_Pnd_C_Blank_Cr_Lf() { return pnd_Irs_Pnd_C_Blank_Cr_Lf; }

    public DbsGroup getPnd_IrsRedef5() { return pnd_IrsRedef5; }

    public DbsField getPnd_Irs_Pnd_K_Record_Type() { return pnd_Irs_Pnd_K_Record_Type; }

    public DbsField getPnd_Irs_Pnd_K_Number_Of_Payees() { return pnd_Irs_Pnd_K_Number_Of_Payees; }

    public DbsField getPnd_Irs_Pnd_K_Filler_1() { return pnd_Irs_Pnd_K_Filler_1; }

    public DbsField getPnd_Irs_Pnd_K_Total_1() { return pnd_Irs_Pnd_K_Total_1; }

    public DbsField getPnd_Irs_Pnd_K_Total_2() { return pnd_Irs_Pnd_K_Total_2; }

    public DbsField getPnd_Irs_Pnd_K_Total_3() { return pnd_Irs_Pnd_K_Total_3; }

    public DbsField getPnd_Irs_Pnd_K_Total_4() { return pnd_Irs_Pnd_K_Total_4; }

    public DbsField getPnd_Irs_Pnd_K_Total_5() { return pnd_Irs_Pnd_K_Total_5; }

    public DbsField getPnd_Irs_Pnd_K_Total_6() { return pnd_Irs_Pnd_K_Total_6; }

    public DbsField getPnd_Irs_Pnd_K_Total_7() { return pnd_Irs_Pnd_K_Total_7; }

    public DbsField getPnd_Irs_Pnd_K_Total_8() { return pnd_Irs_Pnd_K_Total_8; }

    public DbsField getPnd_Irs_Pnd_K_Total_9() { return pnd_Irs_Pnd_K_Total_9; }

    public DbsField getPnd_Irs_Pnd_K_Total_A() { return pnd_Irs_Pnd_K_Total_A; }

    public DbsField getPnd_Irs_Pnd_K_Total_B() { return pnd_Irs_Pnd_K_Total_B; }

    public DbsField getPnd_Irs_Pnd_K_Total_C() { return pnd_Irs_Pnd_K_Total_C; }

    public DbsField getPnd_Irs_Pnd_K_Total_D() { return pnd_Irs_Pnd_K_Total_D; }

    public DbsField getPnd_Irs_Pnd_K_Total_E() { return pnd_Irs_Pnd_K_Total_E; }

    public DbsField getPnd_Irs_Pnd_K_Total_F() { return pnd_Irs_Pnd_K_Total_F; }

    public DbsField getPnd_Irs_Pnd_K_Total_G() { return pnd_Irs_Pnd_K_Total_G; }

    public DbsField getPnd_Irs_Pnd_K_Filler_2() { return pnd_Irs_Pnd_K_Filler_2; }

    public DbsField getPnd_Irs_Pnd_K_Sequence_Number() { return pnd_Irs_Pnd_K_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_K_Filler_4() { return pnd_Irs_Pnd_K_Filler_4; }

    public DbsField getPnd_Irs_Pnd_K_St_Tax_Withheld() { return pnd_Irs_Pnd_K_St_Tax_Withheld; }

    public DbsField getPnd_Irs_Pnd_K_Lc_Tax_Withheld() { return pnd_Irs_Pnd_K_Lc_Tax_Withheld; }

    public DbsField getPnd_Irs_Pnd_K_Filler_5() { return pnd_Irs_Pnd_K_Filler_5; }

    public DbsField getPnd_Irs_Pnd_K_State_Code() { return pnd_Irs_Pnd_K_State_Code; }

    public DbsField getPnd_Irs_Pnd_K_Blank_Cr_Lf() { return pnd_Irs_Pnd_K_Blank_Cr_Lf; }

    public DbsGroup getPnd_IrsRedef6() { return pnd_IrsRedef6; }

    public DbsField getPnd_Irs_Pnd_F_Record_Type() { return pnd_Irs_Pnd_F_Record_Type; }

    public DbsField getPnd_Irs_Pnd_F_Number_Of_A_Records() { return pnd_Irs_Pnd_F_Number_Of_A_Records; }

    public DbsField getPnd_Irs_Pnd_F_Filler_1() { return pnd_Irs_Pnd_F_Filler_1; }

    public DbsField getPnd_Irs_Pnd_F_Filler_2() { return pnd_Irs_Pnd_F_Filler_2; }

    public DbsField getPnd_Irs_Pnd_F_Total_B_Records() { return pnd_Irs_Pnd_F_Total_B_Records; }

    public DbsField getPnd_Irs_Pnd_F_Filler_3() { return pnd_Irs_Pnd_F_Filler_3; }

    public DbsField getPnd_Irs_Pnd_F_Filler_4() { return pnd_Irs_Pnd_F_Filler_4; }

    public DbsField getPnd_Irs_Pnd_F_Sequence_Number() { return pnd_Irs_Pnd_F_Sequence_Number; }

    public DbsField getPnd_Irs_Pnd_F_Filler_5() { return pnd_Irs_Pnd_F_Filler_5; }

    public DbsField getPnd_Irs_Pnd_F_Blank_Cr_Lf() { return pnd_Irs_Pnd_F_Blank_Cr_Lf; }

    public DbsGroup getPnd_IrsRedef7() { return pnd_IrsRedef7; }

    public DbsField getPnd_Irs_Pnd_I700() { return pnd_Irs_Pnd_I700; }

    public DbsField getPnd_Irs_Pnd_I50() { return pnd_Irs_Pnd_I50; }

    public DbsGroup getPnd_Dump() { return pnd_Dump; }

    public DbsField getPnd_Dump_Pnd_D700() { return pnd_Dump_Pnd_D700; }

    public DbsField getPnd_Dump_Pnd_D50() { return pnd_Dump_Pnd_D50; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Irs = newGroupInRecord("pnd_Irs", "#IRS");
        pnd_Irs_Pnd_T_Record_Type = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Record_Type", "#T-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_T_Payment_Year = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Payment_Year", "#T-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_Irs_Pnd_T_Prior_Yr_Data_Ind = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Prior_Yr_Data_Ind", "#T-PRIOR-YR-DATA-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_T_Transmitters_Tin = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Transmitters_Tin", "#T-TRANSMITTERS-TIN", FieldType.STRING, 9);
        pnd_Irs_Pnd_T_Transmitter_Control_Code = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Transmitter_Control_Code", "#T-TRANSMITTER-CONTROL-CODE", FieldType.STRING, 
            5);
        pnd_Irs_Pnd_T_Filler_1 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_1", "#T-FILLER-1", FieldType.STRING, 7);
        pnd_Irs_Pnd_T_Test_File_Indicator = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Test_File_Indicator", "#T-TEST-FILE-INDICATOR", FieldType.STRING, 1);
        pnd_Irs_Pnd_T_Foreign_Entity_Ind = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Foreign_Entity_Ind", "#T-FOREIGN-ENTITY-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_T_Transmitter_Name = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Transmitter_Name", "#T-TRANSMITTER-NAME", FieldType.STRING, 40);
        pnd_Irs_Pnd_T_Transmitter_Name_Continued = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Transmitter_Name_Continued", "#T-TRANSMITTER-NAME-CONTINUED", 
            FieldType.STRING, 40);
        pnd_Irs_Pnd_T_Company_Name = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_Name", "#T-COMPANY-NAME", FieldType.STRING, 40);
        pnd_Irs_Pnd_T_Company_Name_Continued = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_Name_Continued", "#T-COMPANY-NAME-CONTINUED", FieldType.STRING, 
            40);
        pnd_Irs_Pnd_T_Company_Mailing_Address = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_Mailing_Address", "#T-COMPANY-MAILING-ADDRESS", FieldType.STRING, 
            40);
        pnd_Irs_Pnd_T_Company_City = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_City", "#T-COMPANY-CITY", FieldType.STRING, 40);
        pnd_Irs_Pnd_T_Company_State = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_State", "#T-COMPANY-STATE", FieldType.STRING, 2);
        pnd_Irs_Pnd_T_Company_Zip_Code = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Company_Zip_Code", "#T-COMPANY-ZIP-CODE", FieldType.STRING, 9);
        pnd_Irs_Pnd_T_Filler_2 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_2", "#T-FILLER-2", FieldType.STRING, 15);
        pnd_Irs_Pnd_T_Total_Number_Of_Payees = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Total_Number_Of_Payees", "#T-TOTAL-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        pnd_Irs_Pnd_T_Contact_Name = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Contact_Name", "#T-CONTACT-NAME", FieldType.STRING, 40);
        pnd_Irs_Pnd_T_Contact_Phone_N_Extension = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Contact_Phone_N_Extension", "#T-CONTACT-PHONE-N-EXTENSION", FieldType.NUMERIC, 
            15);
        pnd_Irs_Pnd_T_Contact_E_Mail_Address = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Contact_E_Mail_Address", "#T-CONTACT-E-MAIL-ADDRESS", FieldType.STRING, 
            50);
        pnd_Irs_Pnd_T_Magnetic_Tape_File_Ind = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Magnetic_Tape_File_Ind", "#T-MAGNETIC-TAPE-FILE-IND", FieldType.STRING, 
            2);
        pnd_Irs_Pnd_T_Media_Number = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Media_Number", "#T-MEDIA-NUMBER", FieldType.NUMERIC, 6);
        pnd_Irs_Pnd_T_Filler_3 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_3", "#T-FILLER-3", FieldType.STRING, 83);
        pnd_Irs_Pnd_T_Sequence_Number = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Sequence_Number", "#T-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_T_Filler_4 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_4", "#T-FILLER-4", FieldType.STRING, 10);
        pnd_Irs_Pnd_T_Vendor_Indicator = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Vendor_Indicator", "#T-VENDOR-INDICATOR", FieldType.STRING, 1);
        pnd_Irs_Pnd_T_Filler_5 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_5", "#T-FILLER-5", FieldType.STRING, 221);
        pnd_Irs_Pnd_T_Vendor_Foreign_Entity_Ind = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Vendor_Foreign_Entity_Ind", "#T-VENDOR-FOREIGN-ENTITY-IND", FieldType.STRING, 
            1);
        pnd_Irs_Pnd_T_Filler_6 = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Filler_6", "#T-FILLER-6", FieldType.STRING, 8);
        pnd_Irs_Pnd_T_Blank_Or_Cr_Lf = pnd_Irs.newFieldInGroup("pnd_Irs_Pnd_T_Blank_Or_Cr_Lf", "#T-BLANK-OR-CR-LF", FieldType.STRING, 2);
        pnd_IrsRedef1 = newGroupInRecord("pnd_IrsRedef1", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_A_Record_Type = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Record_Type", "#A-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_A_Payment_Year = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payment_Year", "#A-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_Irs_Pnd_A_Filler_1 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Filler_1", "#A-FILLER-1", FieldType.STRING, 6);
        pnd_Irs_Pnd_A_Payer_Ein = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Ein", "#A-PAYER-EIN", FieldType.STRING, 9);
        pnd_Irs_Pnd_A_Payer_Name_Control = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Name_Control", "#A-PAYER-NAME-CONTROL", FieldType.STRING, 
            4);
        pnd_Irs_Pnd_A_Last_Filing_Ind = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Last_Filing_Ind", "#A-LAST-FILING-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_A_Combined_Filer = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Combined_Filer", "#A-COMBINED-FILER", FieldType.STRING, 1);
        pnd_Irs_Pnd_A_Type_Of_Return = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Type_Of_Return", "#A-TYPE-OF-RETURN", FieldType.STRING, 1);
        pnd_Irs_Pnd_A_Amount_Indicators = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Amount_Indicators", "#A-AMOUNT-INDICATORS", FieldType.STRING, 14);
        pnd_Irs_Pnd_A_Filler_2 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Filler_2", "#A-FILLER-2", FieldType.STRING, 10);
        pnd_Irs_Pnd_A_Foriegn_Corp_Indicator = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Foriegn_Corp_Indicator", "#A-FORIEGN-CORP-INDICATOR", FieldType.STRING, 
            1);
        pnd_Irs_Pnd_A_Payer_Name_1 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Name_1", "#A-PAYER-NAME-1", FieldType.STRING, 40);
        pnd_Irs_Pnd_A_Payer_Name_2 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Name_2", "#A-PAYER-NAME-2", FieldType.STRING, 40);
        pnd_Irs_Pnd_A_Transfer_Agent_Indicator = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Transfer_Agent_Indicator", "#A-TRANSFER-AGENT-INDICATOR", 
            FieldType.STRING, 1);
        pnd_Irs_Pnd_A_Payer_Shipping_Address = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Shipping_Address", "#A-PAYER-SHIPPING-ADDRESS", FieldType.STRING, 
            40);
        pnd_Irs_Pnd_A_Payer_City = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_City", "#A-PAYER-CITY", FieldType.STRING, 40);
        pnd_Irs_Pnd_A_Payer_State = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_State", "#A-PAYER-STATE", FieldType.STRING, 2);
        pnd_Irs_Pnd_A_Payer_Zip = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Zip", "#A-PAYER-ZIP", FieldType.STRING, 9);
        pnd_Irs_Pnd_A_Payer_Phone_No = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Payer_Phone_No", "#A-PAYER-PHONE-NO", FieldType.STRING, 15);
        pnd_Irs_Pnd_A_Filler_4 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Filler_4", "#A-FILLER-4", FieldType.STRING, 250);
        pnd_Irs_Pnd_A_Filler_5 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Filler_5", "#A-FILLER-5", FieldType.STRING, 10);
        pnd_Irs_Pnd_A_Sequence_Number = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Sequence_Number", "#A-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_A_Filler_6 = pnd_IrsRedef1.newFieldInGroup("pnd_Irs_Pnd_A_Filler_6", "#A-FILLER-6", FieldType.STRING, 243);
        pnd_IrsRedef2 = newGroupInRecord("pnd_IrsRedef2", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_B_Record_Type = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Record_Type", "#B-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Payment_Year = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Year", "#B-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_Irs_Pnd_B_Return_Indicator = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Return_Indicator", "#B-RETURN-INDICATOR", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Name_Control = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Name_Control", "#B-NAME-CONTROL", FieldType.STRING, 4);
        pnd_Irs_Pnd_B_Type_Of_Tin = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Type_Of_Tin", "#B-TYPE-OF-TIN", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Taxpayer_Id_Numb = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Taxpayer_Id_Numb", "#B-TAXPAYER-ID-NUMB", FieldType.STRING, 9);
        pnd_Irs_Pnd_B_Contract_Payee = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Contract_Payee", "#B-CONTRACT-PAYEE", FieldType.STRING, 20);
        pnd_Irs_Pnd_B_Contract_PayeeRedef3 = pnd_IrsRedef2.newGroupInGroup("pnd_Irs_Pnd_B_Contract_PayeeRedef3", "Redefines", pnd_Irs_Pnd_B_Contract_Payee);
        pnd_Irs_Pnd_B_Contract_First_2b = pnd_Irs_Pnd_B_Contract_PayeeRedef3.newFieldInGroup("pnd_Irs_Pnd_B_Contract_First_2b", "#B-CONTRACT-FIRST-2B", 
            FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Contract_Filler = pnd_Irs_Pnd_B_Contract_PayeeRedef3.newFieldInGroup("pnd_Irs_Pnd_B_Contract_Filler", "#B-CONTRACT-FILLER", FieldType.STRING, 
            18);
        pnd_Irs_Pnd_B_Payers_Office_Code = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payers_Office_Code", "#B-PAYERS-OFFICE-CODE", FieldType.STRING, 
            4);
        pnd_Irs_Pnd_B_Filler_1 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_1", "#B-FILLER-1", FieldType.STRING, 10);
        pnd_Irs_Pnd_B_Payment_Amount_1 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_1", "#B-PAYMENT-AMOUNT-1", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_2 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_2", "#B-PAYMENT-AMOUNT-2", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_3 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_3", "#B-PAYMENT-AMOUNT-3", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_4 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_4", "#B-PAYMENT-AMOUNT-4", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_5 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_5", "#B-PAYMENT-AMOUNT-5", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_6 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_6", "#B-PAYMENT-AMOUNT-6", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_7 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_7", "#B-PAYMENT-AMOUNT-7", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_8 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_8", "#B-PAYMENT-AMOUNT-8", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_9 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_9", "#B-PAYMENT-AMOUNT-9", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_A = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_A", "#B-PAYMENT-AMOUNT-A", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_B = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_B", "#B-PAYMENT-AMOUNT-B", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_C = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_C", "#B-PAYMENT-AMOUNT-C", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_D = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_D", "#B-PAYMENT-AMOUNT-D", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_E = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_E", "#B-PAYMENT-AMOUNT-E", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_F = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_F", "#B-PAYMENT-AMOUNT-F", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Payment_Amount_G = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Payment_Amount_G", "#B-PAYMENT-AMOUNT-G", FieldType.DECIMAL, 12,
            2);
        pnd_Irs_Pnd_B_Foreign_Indicator = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Foreign_Indicator", "#B-FOREIGN-INDICATOR", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Name_Line_1 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Name_Line_1", "#B-NAME-LINE-1", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_Name_Line_2 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Name_Line_2", "#B-NAME-LINE-2", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_Filler_3 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_3", "#B-FILLER-3", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_Address_Line = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Address_Line", "#B-ADDRESS-LINE", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_Filler_4 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_4", "#B-FILLER-4", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_City = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_City", "#B-CITY", FieldType.STRING, 40);
        pnd_Irs_Pnd_B_State = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_State", "#B-STATE", FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Zip = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Zip", "#B-ZIP", FieldType.STRING, 9);
        pnd_Irs_Pnd_B_Filler_5 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_5", "#B-FILLER-5", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Sequence_Number = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Sequence_Number", "#B-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_B_Filler_6 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_6", "#B-FILLER-6", FieldType.STRING, 39);
        pnd_Irs_Pnd_B_Ira_Ind = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Ira_Ind", "#B-IRA-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Sep_Ind = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Sep_Ind", "#B-SEP-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Simple_Ind = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Simple_Ind", "#B-SIMPLE-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Roth_Ind = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Roth_Ind", "#B-ROTH-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Rmd_Ind = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Rmd_Ind", "#B-RMD-IND", FieldType.STRING, 1);
        pnd_Irs_Pnd_B_Yr_Pst_Cnt = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Yr_Pst_Cnt", "#B-YR-PST-CNT", FieldType.STRING, 4);
        pnd_Irs_Pnd_B_Postpn_Code = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Postpn_Code", "#B-POSTPN-CODE", FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Postpn_Rsn = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Postpn_Rsn", "#B-POSTPN-RSN", FieldType.STRING, 6);
        pnd_Irs_Pnd_B_Rep_Code = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Rep_Code", "#B-REP-CODE", FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Rmd_Date = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Rmd_Date", "#B-RMD-DATE", FieldType.STRING, 8);
        pnd_Irs_Pnd_B_Codes = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Codes", "#B-CODES", FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Filler_8 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_8", "#B-FILLER-8", FieldType.STRING, 87);
        pnd_Irs_Pnd_B_Special_Data_Entries = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Special_Data_Entries", "#B-SPECIAL-DATA-ENTRIES", FieldType.STRING, 
            60);
        pnd_Irs_Pnd_B_Filler_9 = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_9", "#B-FILLER-9", FieldType.STRING, 24);
        pnd_Irs_Pnd_B_Combined_State = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Combined_State", "#B-COMBINED-STATE", FieldType.STRING, 2);
        pnd_Irs_Pnd_B_Filler_A = pnd_IrsRedef2.newFieldInGroup("pnd_Irs_Pnd_B_Filler_A", "#B-FILLER-A", FieldType.STRING, 2);
        pnd_IrsRedef4 = newGroupInRecord("pnd_IrsRedef4", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_C_Record_Type = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Record_Type", "#C-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_C_Number_Of_Payees = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Number_Of_Payees", "#C-NUMBER-OF-PAYEES", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_C_Filler_1 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Filler_1", "#C-FILLER-1", FieldType.STRING, 6);
        pnd_Irs_Pnd_C_Total_1 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_1", "#C-TOTAL-1", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_2 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_2", "#C-TOTAL-2", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_3 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_3", "#C-TOTAL-3", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_4 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_4", "#C-TOTAL-4", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_5 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_5", "#C-TOTAL-5", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_6 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_6", "#C-TOTAL-6", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_7 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_7", "#C-TOTAL-7", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_8 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_8", "#C-TOTAL-8", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_9 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_9", "#C-TOTAL-9", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_A = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_A", "#C-TOTAL-A", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_B = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_B", "#C-TOTAL-B", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_C = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_C", "#C-TOTAL-C", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_D = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_D", "#C-TOTAL-D", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_E = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_E", "#C-TOTAL-E", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_F = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_F", "#C-TOTAL-F", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Total_G = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Total_G", "#C-TOTAL-G", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_C_Filler_2 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Filler_2", "#C-FILLER-2", FieldType.STRING, 196);
        pnd_Irs_Pnd_C_Sequence_Number = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Sequence_Number", "#C-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_C_Filler_4 = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Filler_4", "#C-FILLER-4", FieldType.STRING, 241);
        pnd_Irs_Pnd_C_Blank_Cr_Lf = pnd_IrsRedef4.newFieldInGroup("pnd_Irs_Pnd_C_Blank_Cr_Lf", "#C-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_IrsRedef5 = newGroupInRecord("pnd_IrsRedef5", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_K_Record_Type = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Record_Type", "#K-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_K_Number_Of_Payees = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Number_Of_Payees", "#K-NUMBER-OF-PAYEES", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_K_Filler_1 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Filler_1", "#K-FILLER-1", FieldType.STRING, 6);
        pnd_Irs_Pnd_K_Total_1 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_1", "#K-TOTAL-1", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_2 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_2", "#K-TOTAL-2", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_3 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_3", "#K-TOTAL-3", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_4 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_4", "#K-TOTAL-4", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_5 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_5", "#K-TOTAL-5", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_6 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_6", "#K-TOTAL-6", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_7 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_7", "#K-TOTAL-7", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_8 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_8", "#K-TOTAL-8", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_9 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_9", "#K-TOTAL-9", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_A = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_A", "#K-TOTAL-A", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_B = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_B", "#K-TOTAL-B", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_C = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_C", "#K-TOTAL-C", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_D = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_D", "#K-TOTAL-D", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_E = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_E", "#K-TOTAL-E", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_F = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_F", "#K-TOTAL-F", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Total_G = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Total_G", "#K-TOTAL-G", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Filler_2 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Filler_2", "#K-FILLER-2", FieldType.STRING, 196);
        pnd_Irs_Pnd_K_Sequence_Number = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Sequence_Number", "#K-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_K_Filler_4 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Filler_4", "#K-FILLER-4", FieldType.STRING, 199);
        pnd_Irs_Pnd_K_St_Tax_Withheld = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_St_Tax_Withheld", "#K-ST-TAX-WITHHELD", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Lc_Tax_Withheld = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Lc_Tax_Withheld", "#K-LC-TAX-WITHHELD", FieldType.DECIMAL, 18,2);
        pnd_Irs_Pnd_K_Filler_5 = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Filler_5", "#K-FILLER-5", FieldType.STRING, 4);
        pnd_Irs_Pnd_K_State_Code = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_State_Code", "#K-STATE-CODE", FieldType.STRING, 2);
        pnd_Irs_Pnd_K_Blank_Cr_Lf = pnd_IrsRedef5.newFieldInGroup("pnd_Irs_Pnd_K_Blank_Cr_Lf", "#K-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_IrsRedef6 = newGroupInRecord("pnd_IrsRedef6", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_F_Record_Type = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Record_Type", "#F-RECORD-TYPE", FieldType.STRING, 1);
        pnd_Irs_Pnd_F_Number_Of_A_Records = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Number_Of_A_Records", "#F-NUMBER-OF-A-RECORDS", FieldType.NUMERIC, 
            8);
        pnd_Irs_Pnd_F_Filler_1 = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Filler_1", "#F-FILLER-1", FieldType.NUMERIC, 21);
        pnd_Irs_Pnd_F_Filler_2 = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Filler_2", "#F-FILLER-2", FieldType.STRING, 19);
        pnd_Irs_Pnd_F_Total_B_Records = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Total_B_Records", "#F-TOTAL-B-RECORDS", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_F_Filler_3 = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Filler_3", "#F-FILLER-3", FieldType.STRING, 250);
        pnd_Irs_Pnd_F_Filler_4 = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Filler_4", "#F-FILLER-4", FieldType.STRING, 192);
        pnd_Irs_Pnd_F_Sequence_Number = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Sequence_Number", "#F-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_Irs_Pnd_F_Filler_5 = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Filler_5", "#F-FILLER-5", FieldType.STRING, 241);
        pnd_Irs_Pnd_F_Blank_Cr_Lf = pnd_IrsRedef6.newFieldInGroup("pnd_Irs_Pnd_F_Blank_Cr_Lf", "#F-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_IrsRedef7 = newGroupInRecord("pnd_IrsRedef7", "Redefines", pnd_Irs);
        pnd_Irs_Pnd_I700 = pnd_IrsRedef7.newFieldArrayInGroup("pnd_Irs_Pnd_I700", "#I700", FieldType.STRING, 100, new DbsArrayController(1,7));
        pnd_Irs_Pnd_I50 = pnd_IrsRedef7.newFieldInGroup("pnd_Irs_Pnd_I50", "#I50", FieldType.STRING, 50);

        pnd_Dump = newGroupInRecord("pnd_Dump", "#DUMP");
        pnd_Dump_Pnd_D700 = pnd_Dump.newFieldArrayInGroup("pnd_Dump_Pnd_D700", "#D700", FieldType.STRING, 100, new DbsArrayController(1,7));
        pnd_Dump_Pnd_D50 = pnd_Dump.newFieldInGroup("pnd_Dump_Pnd_D50", "#D50", FieldType.STRING, 50);

        this.setRecordName("LdaTwrl444a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl444a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
