/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:40 PM
**        * FROM NATURAL LDA     : TWRL443E
************************************************************
**        * FILE NAME            : LdaTwrl443e.java
**        * CLASS NAME           : LdaTwrl443e
**        * INSTANCE NAME        : LdaTwrl443e
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443e extends DbsRecord
{
    // Properties
    private DbsGroup pnd_K_Tape;
    private DbsField pnd_K_Tape_Pnd_K_Record_Type;
    private DbsField pnd_K_Tape_Pnd_K_Number_Of_Payees;
    private DbsField pnd_K_Tape_Pnd_K_Filler_1;
    private DbsField pnd_K_Tape_Pnd_K_Total_1;
    private DbsField pnd_K_Tape_Pnd_K_Total_2;
    private DbsField pnd_K_Tape_Pnd_K_Total_3;
    private DbsField pnd_K_Tape_Pnd_K_Total_4;
    private DbsField pnd_K_Tape_Pnd_K_Total_5;
    private DbsField pnd_K_Tape_Pnd_K_Total_6;
    private DbsField pnd_K_Tape_Pnd_K_Total_7;
    private DbsField pnd_K_Tape_Pnd_K_Total_8;
    private DbsField pnd_K_Tape_Pnd_K_Total_9;
    private DbsField pnd_K_Tape_Pnd_K_Total_A;
    private DbsField pnd_K_Tape_Pnd_K_Total_B;
    private DbsField pnd_K_Tape_Pnd_K_Total_C;
    private DbsField pnd_K_Tape_Pnd_K_Total_D;
    private DbsField pnd_K_Tape_Pnd_K_Total_E;
    private DbsField pnd_K_Tape_Pnd_K_Total_F;
    private DbsField pnd_K_Tape_Pnd_K_Total_G;
    private DbsField pnd_K_Tape_Pnd_K_Filler_2;
    private DbsField pnd_K_Tape_Pnd_K_Sequence_Number;
    private DbsField pnd_K_Tape_Pnd_K_Filler_4;
    private DbsField pnd_K_Tape_Pnd_K_St_Tax_Withheld;
    private DbsField pnd_K_Tape_Pnd_K_Lc_Tax_Withheld;
    private DbsField pnd_K_Tape_Pnd_K_Filler_5;
    private DbsField pnd_K_Tape_Pnd_K_State_Code;
    private DbsField pnd_K_Tape_Pnd_K_Blank_Cr_Lf;
    private DbsGroup pnd_K_TapeRedef1;
    private DbsField pnd_K_Tape_Pnd_K_Move_1;
    private DbsField pnd_K_Tape_Pnd_K_Move_2;
    private DbsField pnd_K_Tape_Pnd_K_Move_3;

    public DbsGroup getPnd_K_Tape() { return pnd_K_Tape; }

    public DbsField getPnd_K_Tape_Pnd_K_Record_Type() { return pnd_K_Tape_Pnd_K_Record_Type; }

    public DbsField getPnd_K_Tape_Pnd_K_Number_Of_Payees() { return pnd_K_Tape_Pnd_K_Number_Of_Payees; }

    public DbsField getPnd_K_Tape_Pnd_K_Filler_1() { return pnd_K_Tape_Pnd_K_Filler_1; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_1() { return pnd_K_Tape_Pnd_K_Total_1; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_2() { return pnd_K_Tape_Pnd_K_Total_2; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_3() { return pnd_K_Tape_Pnd_K_Total_3; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_4() { return pnd_K_Tape_Pnd_K_Total_4; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_5() { return pnd_K_Tape_Pnd_K_Total_5; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_6() { return pnd_K_Tape_Pnd_K_Total_6; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_7() { return pnd_K_Tape_Pnd_K_Total_7; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_8() { return pnd_K_Tape_Pnd_K_Total_8; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_9() { return pnd_K_Tape_Pnd_K_Total_9; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_A() { return pnd_K_Tape_Pnd_K_Total_A; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_B() { return pnd_K_Tape_Pnd_K_Total_B; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_C() { return pnd_K_Tape_Pnd_K_Total_C; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_D() { return pnd_K_Tape_Pnd_K_Total_D; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_E() { return pnd_K_Tape_Pnd_K_Total_E; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_F() { return pnd_K_Tape_Pnd_K_Total_F; }

    public DbsField getPnd_K_Tape_Pnd_K_Total_G() { return pnd_K_Tape_Pnd_K_Total_G; }

    public DbsField getPnd_K_Tape_Pnd_K_Filler_2() { return pnd_K_Tape_Pnd_K_Filler_2; }

    public DbsField getPnd_K_Tape_Pnd_K_Sequence_Number() { return pnd_K_Tape_Pnd_K_Sequence_Number; }

    public DbsField getPnd_K_Tape_Pnd_K_Filler_4() { return pnd_K_Tape_Pnd_K_Filler_4; }

    public DbsField getPnd_K_Tape_Pnd_K_St_Tax_Withheld() { return pnd_K_Tape_Pnd_K_St_Tax_Withheld; }

    public DbsField getPnd_K_Tape_Pnd_K_Lc_Tax_Withheld() { return pnd_K_Tape_Pnd_K_Lc_Tax_Withheld; }

    public DbsField getPnd_K_Tape_Pnd_K_Filler_5() { return pnd_K_Tape_Pnd_K_Filler_5; }

    public DbsField getPnd_K_Tape_Pnd_K_State_Code() { return pnd_K_Tape_Pnd_K_State_Code; }

    public DbsField getPnd_K_Tape_Pnd_K_Blank_Cr_Lf() { return pnd_K_Tape_Pnd_K_Blank_Cr_Lf; }

    public DbsGroup getPnd_K_TapeRedef1() { return pnd_K_TapeRedef1; }

    public DbsField getPnd_K_Tape_Pnd_K_Move_1() { return pnd_K_Tape_Pnd_K_Move_1; }

    public DbsField getPnd_K_Tape_Pnd_K_Move_2() { return pnd_K_Tape_Pnd_K_Move_2; }

    public DbsField getPnd_K_Tape_Pnd_K_Move_3() { return pnd_K_Tape_Pnd_K_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_K_Tape = newGroupInRecord("pnd_K_Tape", "#K-TAPE");
        pnd_K_Tape_Pnd_K_Record_Type = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Record_Type", "#K-RECORD-TYPE", FieldType.STRING, 1);
        pnd_K_Tape_Pnd_K_Number_Of_Payees = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Number_Of_Payees", "#K-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        pnd_K_Tape_Pnd_K_Filler_1 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Filler_1", "#K-FILLER-1", FieldType.STRING, 6);
        pnd_K_Tape_Pnd_K_Total_1 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_1", "#K-TOTAL-1", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_2 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_2", "#K-TOTAL-2", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_3 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_3", "#K-TOTAL-3", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_4 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_4", "#K-TOTAL-4", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_5 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_5", "#K-TOTAL-5", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_6 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_6", "#K-TOTAL-6", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_7 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_7", "#K-TOTAL-7", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_8 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_8", "#K-TOTAL-8", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_9 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_9", "#K-TOTAL-9", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_A = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_A", "#K-TOTAL-A", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_B = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_B", "#K-TOTAL-B", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_C = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_C", "#K-TOTAL-C", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_D = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_D", "#K-TOTAL-D", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_E = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_E", "#K-TOTAL-E", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_F = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_F", "#K-TOTAL-F", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Total_G = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Total_G", "#K-TOTAL-G", FieldType.DECIMAL, 18,2);
        pnd_K_Tape_Pnd_K_Filler_2 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Filler_2", "#K-FILLER-2", FieldType.STRING, 196);
        pnd_K_Tape_Pnd_K_Sequence_Number = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Sequence_Number", "#K-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_K_Tape_Pnd_K_Filler_4 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Filler_4", "#K-FILLER-4", FieldType.STRING, 199);
        pnd_K_Tape_Pnd_K_St_Tax_Withheld = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_St_Tax_Withheld", "#K-ST-TAX-WITHHELD", FieldType.STRING, 18);
        pnd_K_Tape_Pnd_K_Lc_Tax_Withheld = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Lc_Tax_Withheld", "#K-LC-TAX-WITHHELD", FieldType.STRING, 18);
        pnd_K_Tape_Pnd_K_Filler_5 = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Filler_5", "#K-FILLER-5", FieldType.STRING, 4);
        pnd_K_Tape_Pnd_K_State_Code = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_State_Code", "#K-STATE-CODE", FieldType.STRING, 2);
        pnd_K_Tape_Pnd_K_Blank_Cr_Lf = pnd_K_Tape.newFieldInGroup("pnd_K_Tape_Pnd_K_Blank_Cr_Lf", "#K-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_K_TapeRedef1 = newGroupInRecord("pnd_K_TapeRedef1", "Redefines", pnd_K_Tape);
        pnd_K_Tape_Pnd_K_Move_1 = pnd_K_TapeRedef1.newFieldInGroup("pnd_K_Tape_Pnd_K_Move_1", "#K-MOVE-1", FieldType.STRING, 250);
        pnd_K_Tape_Pnd_K_Move_2 = pnd_K_TapeRedef1.newFieldInGroup("pnd_K_Tape_Pnd_K_Move_2", "#K-MOVE-2", FieldType.STRING, 250);
        pnd_K_Tape_Pnd_K_Move_3 = pnd_K_TapeRedef1.newFieldInGroup("pnd_K_Tape_Pnd_K_Move_3", "#K-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443e");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_K_Tape_Pnd_K_Record_Type.setInitialValue("K");
        pnd_K_Tape_Pnd_K_Filler_1.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_Total_1.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_2.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_3.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_4.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_5.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_6.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_7.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_8.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_9.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_A.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_B.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_C.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_D.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_E.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_F.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Total_G.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Filler_2.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_Sequence_Number.setInitialValue(0);
        pnd_K_Tape_Pnd_K_Filler_4.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_St_Tax_Withheld.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_Lc_Tax_Withheld.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_Filler_5.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_State_Code.setInitialValue(" ");
        pnd_K_Tape_Pnd_K_Blank_Cr_Lf.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443e() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
