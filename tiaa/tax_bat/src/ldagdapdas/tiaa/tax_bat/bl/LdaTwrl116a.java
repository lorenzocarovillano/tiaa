/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:19 PM
**        * FROM NATURAL LDA     : TWRL116A
************************************************************
**        * FILE NAME            : LdaTwrl116a.java
**        * CLASS NAME           : LdaTwrl116a
**        * INSTANCE NAME        : LdaTwrl116a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl116a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl116a;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Surname;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_First_Name;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Initial;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Sec_Surname;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Sec_Initial;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Address_1;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Address_2;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_City;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Province;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Country;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Zip;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Recipient_Type;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Payer_Id;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Income_Code;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Currency_Code;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Exemption_Code;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Gross_Income;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Rpt_Type;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Filler;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_Rec_Type;
    private DbsGroup pnd_Twrl116aRedef1;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_1;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_2;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_3;
    private DbsField pnd_Twrl116a_Pnd_Twrl116a_4;

    public DbsGroup getPnd_Twrl116a() { return pnd_Twrl116a; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1() { return pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2() { return pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Surname() { return pnd_Twrl116a_Pnd_Twrl116a_Surname; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_First_Name() { return pnd_Twrl116a_Pnd_Twrl116a_First_Name; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Initial() { return pnd_Twrl116a_Pnd_Twrl116a_Initial; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Sec_Surname() { return pnd_Twrl116a_Pnd_Twrl116a_Sec_Surname; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name() { return pnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Sec_Initial() { return pnd_Twrl116a_Pnd_Twrl116a_Sec_Initial; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Address_1() { return pnd_Twrl116a_Pnd_Twrl116a_Address_1; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Address_2() { return pnd_Twrl116a_Pnd_Twrl116a_Address_2; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_City() { return pnd_Twrl116a_Pnd_Twrl116a_City; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Province() { return pnd_Twrl116a_Pnd_Twrl116a_Province; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Country() { return pnd_Twrl116a_Pnd_Twrl116a_Country; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Zip() { return pnd_Twrl116a_Pnd_Twrl116a_Zip; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code() { return pnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn() { return pnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct() { return pnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Recipient_Type() { return pnd_Twrl116a_Pnd_Twrl116a_Recipient_Type; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Payer_Id() { return pnd_Twrl116a_Pnd_Twrl116a_Payer_Id; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Income_Code() { return pnd_Twrl116a_Pnd_Twrl116a_Income_Code; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code() { return pnd_Twrl116a_Pnd_Twrl116a_Currency_Code; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Exemption_Code() { return pnd_Twrl116a_Pnd_Twrl116a_Exemption_Code; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24() { return pnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25() { return pnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28() { return pnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income() { return pnd_Twrl116a_Pnd_Twrl116a_Gross_Income; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held() { return pnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26() { return pnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27() { return pnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Rpt_Type() { return pnd_Twrl116a_Pnd_Twrl116a_Rpt_Type; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Filler() { return pnd_Twrl116a_Pnd_Twrl116a_Filler; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_Rec_Type() { return pnd_Twrl116a_Pnd_Twrl116a_Rec_Type; }

    public DbsGroup getPnd_Twrl116aRedef1() { return pnd_Twrl116aRedef1; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_1() { return pnd_Twrl116a_Pnd_Twrl116a_1; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_2() { return pnd_Twrl116a_Pnd_Twrl116a_2; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_3() { return pnd_Twrl116a_Pnd_Twrl116a_3; }

    public DbsField getPnd_Twrl116a_Pnd_Twrl116a_4() { return pnd_Twrl116a_Pnd_Twrl116a_4; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl116a = newGroupInRecord("pnd_Twrl116a", "#TWRL116A");
        pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1", "#TWRL116A-CORP-NAME-1", FieldType.STRING, 
            35);
        pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2", "#TWRL116A-CORP-NAME-2", FieldType.STRING, 
            35);
        pnd_Twrl116a_Pnd_Twrl116a_Surname = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Surname", "#TWRL116A-SURNAME", FieldType.STRING, 20);
        pnd_Twrl116a_Pnd_Twrl116a_First_Name = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_First_Name", "#TWRL116A-FIRST-NAME", FieldType.STRING, 
            12);
        pnd_Twrl116a_Pnd_Twrl116a_Initial = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Initial", "#TWRL116A-INITIAL", FieldType.STRING, 1);
        pnd_Twrl116a_Pnd_Twrl116a_Sec_Surname = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Sec_Surname", "#TWRL116A-SEC-SURNAME", FieldType.STRING, 
            20);
        pnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name", "#TWRL116A-SEC-FIRST-NAME", 
            FieldType.STRING, 12);
        pnd_Twrl116a_Pnd_Twrl116a_Sec_Initial = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Sec_Initial", "#TWRL116A-SEC-INITIAL", FieldType.STRING, 
            1);
        pnd_Twrl116a_Pnd_Twrl116a_Address_1 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Address_1", "#TWRL116A-ADDRESS-1", FieldType.STRING, 
            30);
        pnd_Twrl116a_Pnd_Twrl116a_Address_2 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Address_2", "#TWRL116A-ADDRESS-2", FieldType.STRING, 
            30);
        pnd_Twrl116a_Pnd_Twrl116a_City = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_City", "#TWRL116A-CITY", FieldType.STRING, 28);
        pnd_Twrl116a_Pnd_Twrl116a_Province = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Province", "#TWRL116A-PROVINCE", FieldType.STRING, 
            2);
        pnd_Twrl116a_Pnd_Twrl116a_Country = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Country", "#TWRL116A-COUNTRY", FieldType.STRING, 3);
        pnd_Twrl116a_Pnd_Twrl116a_Zip = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Zip", "#TWRL116A-ZIP", FieldType.STRING, 10);
        pnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code", "#TWRL116A-TAX-COUNTRY-CODE", 
            FieldType.STRING, 3);
        pnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn", "#TWRL116A-FOREIGN-SSN", FieldType.STRING, 
            20);
        pnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct", "#TWRL116A-NON-RESIDENT-ACCT", 
            FieldType.STRING, 9);
        pnd_Twrl116a_Pnd_Twrl116a_Recipient_Type = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Recipient_Type", "#TWRL116A-RECIPIENT-TYPE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl116a_Pnd_Twrl116a_Payer_Id = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Payer_Id", "#TWRL116A-PAYER-ID", FieldType.STRING, 
            20);
        pnd_Twrl116a_Pnd_Twrl116a_Income_Code = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Income_Code", "#TWRL116A-INCOME-CODE", FieldType.NUMERIC, 
            2);
        pnd_Twrl116a_Pnd_Twrl116a_Currency_Code = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Currency_Code", "#TWRL116A-CURRENCY-CODE", FieldType.STRING, 
            3);
        pnd_Twrl116a_Pnd_Twrl116a_Exemption_Code = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Exemption_Code", "#TWRL116A-EXEMPTION-CODE", 
            FieldType.STRING, 1);
        pnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24", "#TWRL116A-INCOME-CODE-BOX-24", 
            FieldType.NUMERIC, 2);
        pnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25", "#TWRL116A-CURRENCY-CODE-BOX-25", 
            FieldType.STRING, 3);
        pnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28", "#TWRL116A-EXEMP-CODE-BOX-28", 
            FieldType.STRING, 1);
        pnd_Twrl116a_Pnd_Twrl116a_Gross_Income = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Gross_Income", "#TWRL116A-GROSS-INCOME", FieldType.DECIMAL, 
            11,2);
        pnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held", "#TWRL116A-NON-RES-TAX-HELD", 
            FieldType.DECIMAL, 11,2);
        pnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26", "#TWRL116A-GROSS-INCOME-BOX-26", 
            FieldType.DECIMAL, 11,2);
        pnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27 = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27", "#TWRL116A-TAX-HELD-BOX-27", 
            FieldType.DECIMAL, 11,2);
        pnd_Twrl116a_Pnd_Twrl116a_Rpt_Type = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Rpt_Type", "#TWRL116A-RPT-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl116a_Pnd_Twrl116a_Filler = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Filler", "#TWRL116A-FILLER", FieldType.STRING, 2);
        pnd_Twrl116a_Pnd_Twrl116a_Rec_Type = pnd_Twrl116a.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_Rec_Type", "#TWRL116A-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl116aRedef1 = newGroupInRecord("pnd_Twrl116aRedef1", "Redefines", pnd_Twrl116a);
        pnd_Twrl116a_Pnd_Twrl116a_1 = pnd_Twrl116aRedef1.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_1", "#TWRL116A-1", FieldType.STRING, 100);
        pnd_Twrl116a_Pnd_Twrl116a_2 = pnd_Twrl116aRedef1.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_2", "#TWRL116A-2", FieldType.STRING, 100);
        pnd_Twrl116a_Pnd_Twrl116a_3 = pnd_Twrl116aRedef1.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_3", "#TWRL116A-3", FieldType.STRING, 100);
        pnd_Twrl116a_Pnd_Twrl116a_4 = pnd_Twrl116aRedef1.newFieldInGroup("pnd_Twrl116a_Pnd_Twrl116a_4", "#TWRL116A-4", FieldType.STRING, 52);

        this.setRecordName("LdaTwrl116a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl116a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
