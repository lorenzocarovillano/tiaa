/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:48 PM
**        * FROM NATURAL LDA     : TWRL462B
************************************************************
**        * FILE NAME            : LdaTwrl462b.java
**        * CLASS NAME           : LdaTwrl462b
**        * INSTANCE NAME        : LdaTwrl462b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl462b extends DbsRecord
{
    // Properties
    private DbsGroup irat_Record;
    private DbsField irat_Record_Irat_Source;
    private DbsField irat_Record_Irat_Company_Code;
    private DbsField irat_Record_Irat_Tax_Year;
    private DbsField irat_Record_Irat_Feed_Period;
    private DbsField irat_Record_Irat_Feed_Date;
    private DbsField irat_Record_Irat_Records_Tot;
    private DbsField irat_Record_Irat_Classic_Tot;
    private DbsField irat_Record_Irat_Roth_Tot;
    private DbsField irat_Record_Irat_Rollover_Tot;
    private DbsField irat_Record_Irat_Rechar_Tot;
    private DbsField irat_Record_Irat_Sep_Tot;
    private DbsField irat_Record_Irat_Fmv_Tot;
    private DbsField irat_Record_Irat_Classic_To_Roth_Tot;

    public DbsGroup getIrat_Record() { return irat_Record; }

    public DbsField getIrat_Record_Irat_Source() { return irat_Record_Irat_Source; }

    public DbsField getIrat_Record_Irat_Company_Code() { return irat_Record_Irat_Company_Code; }

    public DbsField getIrat_Record_Irat_Tax_Year() { return irat_Record_Irat_Tax_Year; }

    public DbsField getIrat_Record_Irat_Feed_Period() { return irat_Record_Irat_Feed_Period; }

    public DbsField getIrat_Record_Irat_Feed_Date() { return irat_Record_Irat_Feed_Date; }

    public DbsField getIrat_Record_Irat_Records_Tot() { return irat_Record_Irat_Records_Tot; }

    public DbsField getIrat_Record_Irat_Classic_Tot() { return irat_Record_Irat_Classic_Tot; }

    public DbsField getIrat_Record_Irat_Roth_Tot() { return irat_Record_Irat_Roth_Tot; }

    public DbsField getIrat_Record_Irat_Rollover_Tot() { return irat_Record_Irat_Rollover_Tot; }

    public DbsField getIrat_Record_Irat_Rechar_Tot() { return irat_Record_Irat_Rechar_Tot; }

    public DbsField getIrat_Record_Irat_Sep_Tot() { return irat_Record_Irat_Sep_Tot; }

    public DbsField getIrat_Record_Irat_Fmv_Tot() { return irat_Record_Irat_Fmv_Tot; }

    public DbsField getIrat_Record_Irat_Classic_To_Roth_Tot() { return irat_Record_Irat_Classic_To_Roth_Tot; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irat_Record = newGroupInRecord("irat_Record", "IRAT-RECORD");
        irat_Record_Irat_Source = irat_Record.newFieldInGroup("irat_Record_Irat_Source", "IRAT-SOURCE", FieldType.STRING, 3);
        irat_Record_Irat_Company_Code = irat_Record.newFieldInGroup("irat_Record_Irat_Company_Code", "IRAT-COMPANY-CODE", FieldType.STRING, 1);
        irat_Record_Irat_Tax_Year = irat_Record.newFieldInGroup("irat_Record_Irat_Tax_Year", "IRAT-TAX-YEAR", FieldType.STRING, 4);
        irat_Record_Irat_Feed_Period = irat_Record.newFieldInGroup("irat_Record_Irat_Feed_Period", "IRAT-FEED-PERIOD", FieldType.STRING, 3);
        irat_Record_Irat_Feed_Date = irat_Record.newFieldInGroup("irat_Record_Irat_Feed_Date", "IRAT-FEED-DATE", FieldType.STRING, 8);
        irat_Record_Irat_Records_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Records_Tot", "IRAT-RECORDS-TOT", FieldType.NUMERIC, 10);
        irat_Record_Irat_Classic_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Classic_Tot", "IRAT-CLASSIC-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Roth_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Roth_Tot", "IRAT-ROTH-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Rollover_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Rollover_Tot", "IRAT-ROLLOVER-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Rechar_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Rechar_Tot", "IRAT-RECHAR-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Sep_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Sep_Tot", "IRAT-SEP-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Fmv_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Fmv_Tot", "IRAT-FMV-TOT", FieldType.DECIMAL, 17,2);
        irat_Record_Irat_Classic_To_Roth_Tot = irat_Record.newFieldInGroup("irat_Record_Irat_Classic_To_Roth_Tot", "IRAT-CLASSIC-TO-ROTH-TOT", FieldType.DECIMAL, 
            17,2);

        this.setRecordName("LdaTwrl462b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl462b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
