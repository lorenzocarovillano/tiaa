/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:24 PM
**        * FROM NATURAL LDA     : TWRL1900
************************************************************
**        * FILE NAME            : LdaTwrl1900.java
**        * CLASS NAME           : LdaTwrl1900
**        * INSTANCE NAME        : LdaTwrl1900
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl1900 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_f96_View;
    private DbsField f96_View_Twrc_Tax_Year;
    private DbsField f96_View_Twrc_Company_Cde;
    private DbsField f96_View_Twrc_Status;
    private DbsField f96_View_Twrc_Tax_Id_Type;
    private DbsField f96_View_Twrc_Tax_Id;
    private DbsField f96_View_Twrc_Contract;
    private DbsField f96_View_Twrc_Payee;
    private DbsField f96_View_Twrc_Citizen_Code;
    private DbsField f96_View_Twrc_Residency_Code;
    private DbsField f96_View_Twrc_Ira_Contract_Type;
    private DbsGroup f96_View_Twrc_Form_Info;
    private DbsField f96_View_Twrc_Source;
    private DbsField f96_View_Twrc_Update_Source;
    private DbsField f96_View_Twrc_Additions_1;
    private DbsGroup f96_View_Twrc_Additions_1Redef1;
    private DbsField f96_View_Twrc_Error_Reason;
    private DbsField f96_View_Twrc_Orign_Area;
    private DbsGroup f96_View_Twrc_Contributions;
    private DbsField f96_View_Twrc_Classic_Amt;
    private DbsField f96_View_Twrc_Roth_Amt;
    private DbsField f96_View_Twrc_Rollover_Amt;
    private DbsField f96_View_Twrc_Rechar_Amt;
    private DbsField f96_View_Twrc_Sep_Amt;
    private DbsField f96_View_Twrc_Fair_Mkt_Val_Amt;
    private DbsField f96_View_Twrc_Roth_Conversion_Amt;
    private DbsField f96_View_Twrc_Create_User;
    private DbsField f96_View_Twrc_Postpn_Amt;
    private DbsField f96_View_Twrc_Postpn_Yr;
    private DbsField f96_View_Twrc_Postpn_Code;
    private DbsField f96_View_Twrc_Repayments_Amt;
    private DbsField f96_View_Twrc_Repayments_Code;
    private DbsField f96_View_Twrc_Create_Date;
    private DbsField f96_View_Twrc_Update_User;
    private DbsField f96_View_Twrc_Update_Date;
    private DbsField f96_View_Twrc_Update_Time;
    private DbsField f96_View_Twrc_S2_Forms;

    public DataAccessProgramView getVw_f96_View() { return vw_f96_View; }

    public DbsField getF96_View_Twrc_Tax_Year() { return f96_View_Twrc_Tax_Year; }

    public DbsField getF96_View_Twrc_Company_Cde() { return f96_View_Twrc_Company_Cde; }

    public DbsField getF96_View_Twrc_Status() { return f96_View_Twrc_Status; }

    public DbsField getF96_View_Twrc_Tax_Id_Type() { return f96_View_Twrc_Tax_Id_Type; }

    public DbsField getF96_View_Twrc_Tax_Id() { return f96_View_Twrc_Tax_Id; }

    public DbsField getF96_View_Twrc_Contract() { return f96_View_Twrc_Contract; }

    public DbsField getF96_View_Twrc_Payee() { return f96_View_Twrc_Payee; }

    public DbsField getF96_View_Twrc_Citizen_Code() { return f96_View_Twrc_Citizen_Code; }

    public DbsField getF96_View_Twrc_Residency_Code() { return f96_View_Twrc_Residency_Code; }

    public DbsField getF96_View_Twrc_Ira_Contract_Type() { return f96_View_Twrc_Ira_Contract_Type; }

    public DbsGroup getF96_View_Twrc_Form_Info() { return f96_View_Twrc_Form_Info; }

    public DbsField getF96_View_Twrc_Source() { return f96_View_Twrc_Source; }

    public DbsField getF96_View_Twrc_Update_Source() { return f96_View_Twrc_Update_Source; }

    public DbsField getF96_View_Twrc_Additions_1() { return f96_View_Twrc_Additions_1; }

    public DbsGroup getF96_View_Twrc_Additions_1Redef1() { return f96_View_Twrc_Additions_1Redef1; }

    public DbsField getF96_View_Twrc_Error_Reason() { return f96_View_Twrc_Error_Reason; }

    public DbsField getF96_View_Twrc_Orign_Area() { return f96_View_Twrc_Orign_Area; }

    public DbsGroup getF96_View_Twrc_Contributions() { return f96_View_Twrc_Contributions; }

    public DbsField getF96_View_Twrc_Classic_Amt() { return f96_View_Twrc_Classic_Amt; }

    public DbsField getF96_View_Twrc_Roth_Amt() { return f96_View_Twrc_Roth_Amt; }

    public DbsField getF96_View_Twrc_Rollover_Amt() { return f96_View_Twrc_Rollover_Amt; }

    public DbsField getF96_View_Twrc_Rechar_Amt() { return f96_View_Twrc_Rechar_Amt; }

    public DbsField getF96_View_Twrc_Sep_Amt() { return f96_View_Twrc_Sep_Amt; }

    public DbsField getF96_View_Twrc_Fair_Mkt_Val_Amt() { return f96_View_Twrc_Fair_Mkt_Val_Amt; }

    public DbsField getF96_View_Twrc_Roth_Conversion_Amt() { return f96_View_Twrc_Roth_Conversion_Amt; }

    public DbsField getF96_View_Twrc_Create_User() { return f96_View_Twrc_Create_User; }

    public DbsField getF96_View_Twrc_Postpn_Amt() { return f96_View_Twrc_Postpn_Amt; }

    public DbsField getF96_View_Twrc_Postpn_Yr() { return f96_View_Twrc_Postpn_Yr; }

    public DbsField getF96_View_Twrc_Postpn_Code() { return f96_View_Twrc_Postpn_Code; }

    public DbsField getF96_View_Twrc_Repayments_Amt() { return f96_View_Twrc_Repayments_Amt; }

    public DbsField getF96_View_Twrc_Repayments_Code() { return f96_View_Twrc_Repayments_Code; }

    public DbsField getF96_View_Twrc_Create_Date() { return f96_View_Twrc_Create_Date; }

    public DbsField getF96_View_Twrc_Update_User() { return f96_View_Twrc_Update_User; }

    public DbsField getF96_View_Twrc_Update_Date() { return f96_View_Twrc_Update_Date; }

    public DbsField getF96_View_Twrc_Update_Time() { return f96_View_Twrc_Update_Time; }

    public DbsField getF96_View_Twrc_S2_Forms() { return f96_View_Twrc_S2_Forms; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_f96_View = new DataAccessProgramView(new NameInfo("vw_f96_View", "F96-VIEW"), "TWR_IRA", "TWR_IRA");
        f96_View_Twrc_Tax_Year = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TWRC_TAX_YEAR");
        f96_View_Twrc_Company_Cde = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Company_Cde", "TWRC-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_COMPANY_CDE");
        f96_View_Twrc_Status = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_STATUS");
        f96_View_Twrc_Tax_Id_Type = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Tax_Id_Type", "TWRC-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID_TYPE");
        f96_View_Twrc_Tax_Id = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID");
        f96_View_Twrc_Contract = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT");
        f96_View_Twrc_Payee = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_PAYEE");
        f96_View_Twrc_Citizen_Code = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Citizen_Code", "TWRC-CITIZEN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_CITIZEN_CODE");
        f96_View_Twrc_Residency_Code = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Residency_Code", "TWRC-RESIDENCY-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRC_RESIDENCY_CODE");
        f96_View_Twrc_Ira_Contract_Type = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Ira_Contract_Type", "TWRC-IRA-CONTRACT-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRC_IRA_CONTRACT_TYPE");
        f96_View_Twrc_Form_Info = vw_f96_View.getRecord().newGroupInGroup("f96_View_Twrc_Form_Info", "TWRC-FORM-INFO");
        f96_View_Twrc_Source = f96_View_Twrc_Form_Info.newFieldInGroup("f96_View_Twrc_Source", "TWRC-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_SOURCE");
        f96_View_Twrc_Update_Source = f96_View_Twrc_Form_Info.newFieldInGroup("f96_View_Twrc_Update_Source", "TWRC-UPDATE-SOURCE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "TWRC_UPDATE_SOURCE");
        f96_View_Twrc_Additions_1 = f96_View_Twrc_Form_Info.newFieldInGroup("f96_View_Twrc_Additions_1", "TWRC-ADDITIONS-1", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_ADDITIONS_1");
        f96_View_Twrc_Additions_1Redef1 = vw_f96_View.getRecord().newGroupInGroup("f96_View_Twrc_Additions_1Redef1", "Redefines", f96_View_Twrc_Additions_1);
        f96_View_Twrc_Error_Reason = f96_View_Twrc_Additions_1Redef1.newFieldInGroup("f96_View_Twrc_Error_Reason", "TWRC-ERROR-REASON", FieldType.NUMERIC, 
            2);
        f96_View_Twrc_Orign_Area = f96_View_Twrc_Additions_1Redef1.newFieldInGroup("f96_View_Twrc_Orign_Area", "TWRC-ORIGN-AREA", FieldType.NUMERIC, 2);
        f96_View_Twrc_Contributions = vw_f96_View.getRecord().newGroupInGroup("f96_View_Twrc_Contributions", "TWRC-CONTRIBUTIONS");
        f96_View_Twrc_Classic_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Classic_Amt", "TWRC-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_CLASSIC_AMT");
        f96_View_Twrc_Roth_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Roth_Amt", "TWRC-ROTH-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "TWRC_ROTH_AMT");
        f96_View_Twrc_Rollover_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Rollover_Amt", "TWRC-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_ROLLOVER_AMT");
        f96_View_Twrc_Rechar_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Rechar_Amt", "TWRC-RECHAR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_RECHAR_AMT");
        f96_View_Twrc_Sep_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Sep_Amt", "TWRC-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_SEP_AMT");
        f96_View_Twrc_Fair_Mkt_Val_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Fair_Mkt_Val_Amt", "TWRC-FAIR-MKT-VAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_FAIR_MKT_VAL_AMT");
        f96_View_Twrc_Roth_Conversion_Amt = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Roth_Conversion_Amt", "TWRC-ROTH-CONVERSION-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, "TWRC_ROTH_CONVERSION_AMT");
        f96_View_Twrc_Create_User = f96_View_Twrc_Contributions.newFieldInGroup("f96_View_Twrc_Create_User", "TWRC-CREATE-USER", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TWRC_CREATE_USER");
        f96_View_Twrc_Postpn_Amt = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Postpn_Amt", "TWRC-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TWRC_POSTPN_AMT");
        f96_View_Twrc_Postpn_Yr = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Postpn_Yr", "TWRC-POSTPN-YR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TWRC_POSTPN_YR");
        f96_View_Twrc_Postpn_Code = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Postpn_Code", "TWRC-POSTPN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_POSTPN_CODE");
        f96_View_Twrc_Repayments_Amt = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Repayments_Amt", "TWRC-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_REPAYMENTS_AMT");
        f96_View_Twrc_Repayments_Code = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Repayments_Code", "TWRC-REPAYMENTS-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRC_REPAYMENTS_CODE");
        f96_View_Twrc_Create_Date = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Create_Date", "TWRC-CREATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_DATE");
        f96_View_Twrc_Update_User = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Update_User", "TWRC-UPDATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_USER");
        f96_View_Twrc_Update_Date = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Update_Date", "TWRC-UPDATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_DATE");
        f96_View_Twrc_Update_Time = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_Update_Time", "TWRC-UPDATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_TIME");
        f96_View_Twrc_S2_Forms = vw_f96_View.getRecord().newFieldInGroup("f96_View_Twrc_S2_Forms", "TWRC-S2-FORMS", FieldType.STRING, 25, RepeatingFieldStrategy.None, 
            "TWRC_S2_FORMS");

        this.setRecordName("LdaTwrl1900");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_f96_View.reset();
    }

    // Constructor
    public LdaTwrl1900() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
