/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:02 PM
**        * FROM NATURAL LDA     : TWRL465C
************************************************************
**        * FILE NAME            : LdaTwrl465c.java
**        * CLASS NAME           : LdaTwrl465c
**        * INSTANCE NAME        : LdaTwrl465c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl465c extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_newira;
    private DbsField newira_Twrc_Tax_Year;
    private DbsField newira_Twrc_Company_Cde;
    private DbsField newira_Twrc_Status;
    private DbsField newira_Twrc_Simulation_Date;
    private DbsField newira_Twrc_Citation_Ind;
    private DbsGroup newira_Twrc_Contributor;
    private DbsField newira_Twrc_Tax_Id_Type;
    private DbsField newira_Twrc_Tax_Id;
    private DbsField newira_Twrc_Contract;
    private DbsField newira_Twrc_Payee;
    private DbsField newira_Twrc_Contract_Xref;
    private DbsField newira_Twrc_Citizen_Code;
    private DbsField newira_Twrc_Residency_Code;
    private DbsField newira_Twrc_Residency_Type;
    private DbsField newira_Twrc_Locality_Cde;
    private DbsField newira_Twrc_Dob;
    private DbsField newira_Twrc_Dod;
    private DbsGroup newira_Twrc_Form_Info;
    private DbsField newira_Twrc_Pin;
    private DbsField newira_Twrc_Ira_Contract_Type;
    private DbsField newira_Twrc_Source;
    private DbsField newira_Twrc_Update_Source;
    private DbsField newira_Twrc_Form_Period;
    private DbsField newira_Twrc_Additions_1;
    private DbsGroup newira_Twrc_Additions_1Redef1;
    private DbsField newira_Twrc_Error_Reason;
    private DbsField newira_Twrc_Orign_Area;
    private DbsGroup newira_Twrc_Contributions;
    private DbsField newira_Twrc_Classic_Amt;
    private DbsField newira_Twrc_Roth_Amt;
    private DbsField newira_Twrc_Rollover_Amt;
    private DbsField newira_Twrc_Rechar_Amt;
    private DbsField newira_Twrc_Sep_Amt;
    private DbsField newira_Twrc_Fair_Mkt_Val_Amt;
    private DbsField newira_Twrc_Roth_Conversion_Amt;
    private DbsGroup newira_Twrc_Name_Addr;
    private DbsField newira_Twrc_Name;
    private DbsField newira_Twrc_Addr_1;
    private DbsField newira_Twrc_Addr_2;
    private DbsField newira_Twrc_Addr_3;
    private DbsField newira_Twrc_Addr_4;
    private DbsField newira_Twrc_Addr_5;
    private DbsField newira_Twrc_Addr_6;
    private DbsField newira_Twrc_City;
    private DbsField newira_Twrc_State;
    private DbsField newira_Twrc_Zip_Code;
    private DbsField newira_Twrc_Addr_Foreign;
    private DbsGroup newira_Twrc_Audit_Trail;
    private DbsField newira_Twrc_Comment1;
    private DbsField newira_Twrc_Comment2;
    private DbsField newira_Twrc_Comment_User;
    private DbsField newira_Twrc_Comment_Date;
    private DbsField newira_Twrc_Comment_Time;
    private DbsField newira_Twrc_Create_User;
    private DbsField newira_Twrc_Create_Date;
    private DbsField newira_Twrc_Create_Date_Inv;
    private DbsField newira_Twrc_Create_Time;
    private DbsField newira_Twrc_Create_Time_Inv;
    private DbsField newira_Twrc_Update_User;
    private DbsField newira_Twrc_Update_Date;
    private DbsField newira_Twrc_Update_Time;
    private DbsField newira_Twrc_Unique_Id;
    private DbsField newira_Twrc_Transfer_From;
    private DbsField newira_Twrc_Transfer_To;
    private DbsField newira_Twrc_Wpid;
    private DbsField newira_Twrc_Lu_Ts;
    private DbsField newira_Twrc_Rmd_Required_Flag;
    private DbsField newira_Twrc_Rmd_Decedent_Name;

    public DataAccessProgramView getVw_newira() { return vw_newira; }

    public DbsField getNewira_Twrc_Tax_Year() { return newira_Twrc_Tax_Year; }

    public DbsField getNewira_Twrc_Company_Cde() { return newira_Twrc_Company_Cde; }

    public DbsField getNewira_Twrc_Status() { return newira_Twrc_Status; }

    public DbsField getNewira_Twrc_Simulation_Date() { return newira_Twrc_Simulation_Date; }

    public DbsField getNewira_Twrc_Citation_Ind() { return newira_Twrc_Citation_Ind; }

    public DbsGroup getNewira_Twrc_Contributor() { return newira_Twrc_Contributor; }

    public DbsField getNewira_Twrc_Tax_Id_Type() { return newira_Twrc_Tax_Id_Type; }

    public DbsField getNewira_Twrc_Tax_Id() { return newira_Twrc_Tax_Id; }

    public DbsField getNewira_Twrc_Contract() { return newira_Twrc_Contract; }

    public DbsField getNewira_Twrc_Payee() { return newira_Twrc_Payee; }

    public DbsField getNewira_Twrc_Contract_Xref() { return newira_Twrc_Contract_Xref; }

    public DbsField getNewira_Twrc_Citizen_Code() { return newira_Twrc_Citizen_Code; }

    public DbsField getNewira_Twrc_Residency_Code() { return newira_Twrc_Residency_Code; }

    public DbsField getNewira_Twrc_Residency_Type() { return newira_Twrc_Residency_Type; }

    public DbsField getNewira_Twrc_Locality_Cde() { return newira_Twrc_Locality_Cde; }

    public DbsField getNewira_Twrc_Dob() { return newira_Twrc_Dob; }

    public DbsField getNewira_Twrc_Dod() { return newira_Twrc_Dod; }

    public DbsGroup getNewira_Twrc_Form_Info() { return newira_Twrc_Form_Info; }

    public DbsField getNewira_Twrc_Pin() { return newira_Twrc_Pin; }

    public DbsField getNewira_Twrc_Ira_Contract_Type() { return newira_Twrc_Ira_Contract_Type; }

    public DbsField getNewira_Twrc_Source() { return newira_Twrc_Source; }

    public DbsField getNewira_Twrc_Update_Source() { return newira_Twrc_Update_Source; }

    public DbsField getNewira_Twrc_Form_Period() { return newira_Twrc_Form_Period; }

    public DbsField getNewira_Twrc_Additions_1() { return newira_Twrc_Additions_1; }

    public DbsGroup getNewira_Twrc_Additions_1Redef1() { return newira_Twrc_Additions_1Redef1; }

    public DbsField getNewira_Twrc_Error_Reason() { return newira_Twrc_Error_Reason; }

    public DbsField getNewira_Twrc_Orign_Area() { return newira_Twrc_Orign_Area; }

    public DbsGroup getNewira_Twrc_Contributions() { return newira_Twrc_Contributions; }

    public DbsField getNewira_Twrc_Classic_Amt() { return newira_Twrc_Classic_Amt; }

    public DbsField getNewira_Twrc_Roth_Amt() { return newira_Twrc_Roth_Amt; }

    public DbsField getNewira_Twrc_Rollover_Amt() { return newira_Twrc_Rollover_Amt; }

    public DbsField getNewira_Twrc_Rechar_Amt() { return newira_Twrc_Rechar_Amt; }

    public DbsField getNewira_Twrc_Sep_Amt() { return newira_Twrc_Sep_Amt; }

    public DbsField getNewira_Twrc_Fair_Mkt_Val_Amt() { return newira_Twrc_Fair_Mkt_Val_Amt; }

    public DbsField getNewira_Twrc_Roth_Conversion_Amt() { return newira_Twrc_Roth_Conversion_Amt; }

    public DbsGroup getNewira_Twrc_Name_Addr() { return newira_Twrc_Name_Addr; }

    public DbsField getNewira_Twrc_Name() { return newira_Twrc_Name; }

    public DbsField getNewira_Twrc_Addr_1() { return newira_Twrc_Addr_1; }

    public DbsField getNewira_Twrc_Addr_2() { return newira_Twrc_Addr_2; }

    public DbsField getNewira_Twrc_Addr_3() { return newira_Twrc_Addr_3; }

    public DbsField getNewira_Twrc_Addr_4() { return newira_Twrc_Addr_4; }

    public DbsField getNewira_Twrc_Addr_5() { return newira_Twrc_Addr_5; }

    public DbsField getNewira_Twrc_Addr_6() { return newira_Twrc_Addr_6; }

    public DbsField getNewira_Twrc_City() { return newira_Twrc_City; }

    public DbsField getNewira_Twrc_State() { return newira_Twrc_State; }

    public DbsField getNewira_Twrc_Zip_Code() { return newira_Twrc_Zip_Code; }

    public DbsField getNewira_Twrc_Addr_Foreign() { return newira_Twrc_Addr_Foreign; }

    public DbsGroup getNewira_Twrc_Audit_Trail() { return newira_Twrc_Audit_Trail; }

    public DbsField getNewira_Twrc_Comment1() { return newira_Twrc_Comment1; }

    public DbsField getNewira_Twrc_Comment2() { return newira_Twrc_Comment2; }

    public DbsField getNewira_Twrc_Comment_User() { return newira_Twrc_Comment_User; }

    public DbsField getNewira_Twrc_Comment_Date() { return newira_Twrc_Comment_Date; }

    public DbsField getNewira_Twrc_Comment_Time() { return newira_Twrc_Comment_Time; }

    public DbsField getNewira_Twrc_Create_User() { return newira_Twrc_Create_User; }

    public DbsField getNewira_Twrc_Create_Date() { return newira_Twrc_Create_Date; }

    public DbsField getNewira_Twrc_Create_Date_Inv() { return newira_Twrc_Create_Date_Inv; }

    public DbsField getNewira_Twrc_Create_Time() { return newira_Twrc_Create_Time; }

    public DbsField getNewira_Twrc_Create_Time_Inv() { return newira_Twrc_Create_Time_Inv; }

    public DbsField getNewira_Twrc_Update_User() { return newira_Twrc_Update_User; }

    public DbsField getNewira_Twrc_Update_Date() { return newira_Twrc_Update_Date; }

    public DbsField getNewira_Twrc_Update_Time() { return newira_Twrc_Update_Time; }

    public DbsField getNewira_Twrc_Unique_Id() { return newira_Twrc_Unique_Id; }

    public DbsField getNewira_Twrc_Transfer_From() { return newira_Twrc_Transfer_From; }

    public DbsField getNewira_Twrc_Transfer_To() { return newira_Twrc_Transfer_To; }

    public DbsField getNewira_Twrc_Wpid() { return newira_Twrc_Wpid; }

    public DbsField getNewira_Twrc_Lu_Ts() { return newira_Twrc_Lu_Ts; }

    public DbsField getNewira_Twrc_Rmd_Required_Flag() { return newira_Twrc_Rmd_Required_Flag; }

    public DbsField getNewira_Twrc_Rmd_Decedent_Name() { return newira_Twrc_Rmd_Decedent_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_newira = new DataAccessProgramView(new NameInfo("vw_newira", "NEWIRA"), "TWR_IRA", "TWR_IRA");
        newira_Twrc_Tax_Year = vw_newira.getRecord().newFieldInGroup("newira_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TWRC_TAX_YEAR");
        newira_Twrc_Company_Cde = vw_newira.getRecord().newFieldInGroup("newira_Twrc_Company_Cde", "TWRC-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_COMPANY_CDE");
        newira_Twrc_Status = vw_newira.getRecord().newFieldInGroup("newira_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_STATUS");
        newira_Twrc_Simulation_Date = vw_newira.getRecord().newFieldInGroup("newira_Twrc_Simulation_Date", "TWRC-SIMULATION-DATE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TWRC_SIMULATION_DATE");
        newira_Twrc_Citation_Ind = vw_newira.getRecord().newFieldInGroup("newira_Twrc_Citation_Ind", "TWRC-CITATION-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_CITATION_IND");
        newira_Twrc_Contributor = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Contributor", "TWRC-CONTRIBUTOR");
        newira_Twrc_Tax_Id_Type = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Tax_Id_Type", "TWRC-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID_TYPE");
        newira_Twrc_Tax_Id = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID");
        newira_Twrc_Contract = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT");
        newira_Twrc_Payee = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_PAYEE");
        newira_Twrc_Contract_Xref = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Contract_Xref", "TWRC-CONTRACT-XREF", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT_XREF");
        newira_Twrc_Citizen_Code = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Citizen_Code", "TWRC-CITIZEN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_CITIZEN_CODE");
        newira_Twrc_Residency_Code = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Residency_Code", "TWRC-RESIDENCY-CODE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TWRC_RESIDENCY_CODE");
        newira_Twrc_Residency_Type = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Residency_Type", "TWRC-RESIDENCY-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRC_RESIDENCY_TYPE");
        newira_Twrc_Locality_Cde = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Locality_Cde", "TWRC-LOCALITY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_LOCALITY_CDE");
        newira_Twrc_Dob = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Dob", "TWRC-DOB", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRC_DOB");
        newira_Twrc_Dod = newira_Twrc_Contributor.newFieldInGroup("newira_Twrc_Dod", "TWRC-DOD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRC_DOD");
        newira_Twrc_Form_Info = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Form_Info", "TWRC-FORM-INFO");
        newira_Twrc_Pin = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Pin", "TWRC-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, "TWRC_PIN");
        newira_Twrc_Ira_Contract_Type = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Ira_Contract_Type", "TWRC-IRA-CONTRACT-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRC_IRA_CONTRACT_TYPE");
        newira_Twrc_Source = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Source", "TWRC-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_SOURCE");
        newira_Twrc_Update_Source = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Update_Source", "TWRC-UPDATE-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_SOURCE");
        newira_Twrc_Form_Period = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Form_Period", "TWRC-FORM-PERIOD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_FORM_PERIOD");
        newira_Twrc_Additions_1 = newira_Twrc_Form_Info.newFieldInGroup("newira_Twrc_Additions_1", "TWRC-ADDITIONS-1", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_ADDITIONS_1");
        newira_Twrc_Additions_1Redef1 = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Additions_1Redef1", "Redefines", newira_Twrc_Additions_1);
        newira_Twrc_Error_Reason = newira_Twrc_Additions_1Redef1.newFieldInGroup("newira_Twrc_Error_Reason", "TWRC-ERROR-REASON", FieldType.NUMERIC, 2);
        newira_Twrc_Orign_Area = newira_Twrc_Additions_1Redef1.newFieldInGroup("newira_Twrc_Orign_Area", "TWRC-ORIGN-AREA", FieldType.NUMERIC, 2);
        newira_Twrc_Contributions = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Contributions", "TWRC-CONTRIBUTIONS");
        newira_Twrc_Classic_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Classic_Amt", "TWRC-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TWRC_CLASSIC_AMT");
        newira_Twrc_Roth_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Roth_Amt", "TWRC-ROTH-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_ROTH_AMT");
        newira_Twrc_Rollover_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Rollover_Amt", "TWRC-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_ROLLOVER_AMT");
        newira_Twrc_Rechar_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Rechar_Amt", "TWRC-RECHAR-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "TWRC_RECHAR_AMT");
        newira_Twrc_Sep_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Sep_Amt", "TWRC-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_SEP_AMT");
        newira_Twrc_Fair_Mkt_Val_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Fair_Mkt_Val_Amt", "TWRC-FAIR-MKT-VAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_FAIR_MKT_VAL_AMT");
        newira_Twrc_Roth_Conversion_Amt = newira_Twrc_Contributions.newFieldInGroup("newira_Twrc_Roth_Conversion_Amt", "TWRC-ROTH-CONVERSION-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_ROTH_CONVERSION_AMT");
        newira_Twrc_Name_Addr = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Name_Addr", "TWRC-NAME-ADDR");
        newira_Twrc_Name = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Name", "TWRC-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_NAME");
        newira_Twrc_Addr_1 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_1", "TWRC-ADDR-1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_1");
        newira_Twrc_Addr_2 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_2", "TWRC-ADDR-2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_2");
        newira_Twrc_Addr_3 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_3", "TWRC-ADDR-3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_3");
        newira_Twrc_Addr_4 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_4", "TWRC-ADDR-4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_4");
        newira_Twrc_Addr_5 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_5", "TWRC-ADDR-5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_5");
        newira_Twrc_Addr_6 = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_6", "TWRC-ADDR-6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_6");
        newira_Twrc_City = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_City", "TWRC-CITY", FieldType.STRING, 30, RepeatingFieldStrategy.None, "TWRC_CITY");
        newira_Twrc_State = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_State", "TWRC-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_STATE");
        newira_Twrc_Zip_Code = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Zip_Code", "TWRC-ZIP-CODE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "TWRC_ZIP_CODE");
        newira_Twrc_Addr_Foreign = newira_Twrc_Name_Addr.newFieldInGroup("newira_Twrc_Addr_Foreign", "TWRC-ADDR-FOREIGN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_FOREIGN");
        newira_Twrc_Audit_Trail = vw_newira.getRecord().newGroupInGroup("newira_Twrc_Audit_Trail", "TWRC-AUDIT-TRAIL");
        newira_Twrc_Comment1 = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Comment1", "TWRC-COMMENT1", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT1");
        newira_Twrc_Comment2 = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Comment2", "TWRC-COMMENT2", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT2");
        newira_Twrc_Comment_User = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Comment_User", "TWRC-COMMENT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_USER");
        newira_Twrc_Comment_Date = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Comment_Date", "TWRC-COMMENT-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_DATE");
        newira_Twrc_Comment_Time = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Comment_Time", "TWRC-COMMENT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_TIME");
        newira_Twrc_Create_User = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Create_User", "TWRC-CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_USER");
        newira_Twrc_Create_Date = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Create_Date", "TWRC-CREATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_DATE");
        newira_Twrc_Create_Date_Inv = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Create_Date_Inv", "TWRC-CREATE-DATE-INV", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRC_CREATE_DATE_INV");
        newira_Twrc_Create_Time = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Create_Time", "TWRC-CREATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_TIME");
        newira_Twrc_Create_Time_Inv = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Create_Time_Inv", "TWRC-CREATE-TIME-INV", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "TWRC_CREATE_TIME_INV");
        newira_Twrc_Update_User = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Update_User", "TWRC-UPDATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_USER");
        newira_Twrc_Update_Date = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Update_Date", "TWRC-UPDATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_DATE");
        newira_Twrc_Update_Time = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Update_Time", "TWRC-UPDATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_TIME");
        newira_Twrc_Unique_Id = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Unique_Id", "TWRC-UNIQUE-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "TWRC_UNIQUE_ID");
        newira_Twrc_Transfer_From = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Transfer_From", "TWRC-TRANSFER-FROM", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TWRC_TRANSFER_FROM");
        newira_Twrc_Transfer_To = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Transfer_To", "TWRC-TRANSFER-TO", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TWRC_TRANSFER_TO");
        newira_Twrc_Wpid = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Wpid", "TWRC-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TWRC_WPID");
        newira_Twrc_Lu_Ts = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Lu_Ts", "TWRC-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TWRC_LU_TS");
        newira_Twrc_Rmd_Required_Flag = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Rmd_Required_Flag", "TWRC-RMD-REQUIRED-FLAG", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRC_RMD_REQUIRED_FLAG");
        newira_Twrc_Rmd_Decedent_Name = newira_Twrc_Audit_Trail.newFieldInGroup("newira_Twrc_Rmd_Decedent_Name", "TWRC-RMD-DECEDENT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRC_RMD_DECEDENT_NAME");

        this.setRecordName("LdaTwrl465c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_newira.reset();
    }

    // Constructor
    public LdaTwrl465c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
