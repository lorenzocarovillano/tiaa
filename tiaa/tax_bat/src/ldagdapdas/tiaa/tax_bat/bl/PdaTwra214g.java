/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:34 PM
**        * FROM NATURAL PDA     : TWRA214G
************************************************************
**        * FILE NAME            : PdaTwra214g.java
**        * CLASS NAME           : PdaTwra214g
**        * INSTANCE NAME        : PdaTwra214g
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwra214g extends PdaBase
{
    // Properties
    private DbsGroup pnd_Twra214g;
    private DbsGroup pnd_Twra214g_Pnd_Input_Parms;
    private DbsField pnd_Twra214g_Pnd_Debug_Ind;
    private DbsField pnd_Twra214g_Pnd_Abend_Ind;
    private DbsField pnd_Twra214g_Pnd_Display_Ind;
    private DbsField pnd_Twra214g_Pnd_Tax_Year;
    private DbsField pnd_Twra214g_Pnd_Pin;
    private DbsField pnd_Twra214g_Pnd_Form_Type;
    private DbsGroup pnd_Twra214g_Pnd_Output_Data;
    private DbsField pnd_Twra214g_Pnd_Ret_Code;
    private DbsField pnd_Twra214g_Pnd_Ret_Msg;

    public DbsGroup getPnd_Twra214g() { return pnd_Twra214g; }

    public DbsGroup getPnd_Twra214g_Pnd_Input_Parms() { return pnd_Twra214g_Pnd_Input_Parms; }

    public DbsField getPnd_Twra214g_Pnd_Debug_Ind() { return pnd_Twra214g_Pnd_Debug_Ind; }

    public DbsField getPnd_Twra214g_Pnd_Abend_Ind() { return pnd_Twra214g_Pnd_Abend_Ind; }

    public DbsField getPnd_Twra214g_Pnd_Display_Ind() { return pnd_Twra214g_Pnd_Display_Ind; }

    public DbsField getPnd_Twra214g_Pnd_Tax_Year() { return pnd_Twra214g_Pnd_Tax_Year; }

    public DbsField getPnd_Twra214g_Pnd_Pin() { return pnd_Twra214g_Pnd_Pin; }

    public DbsField getPnd_Twra214g_Pnd_Form_Type() { return pnd_Twra214g_Pnd_Form_Type; }

    public DbsGroup getPnd_Twra214g_Pnd_Output_Data() { return pnd_Twra214g_Pnd_Output_Data; }

    public DbsField getPnd_Twra214g_Pnd_Ret_Code() { return pnd_Twra214g_Pnd_Ret_Code; }

    public DbsField getPnd_Twra214g_Pnd_Ret_Msg() { return pnd_Twra214g_Pnd_Ret_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twra214g = dbsRecord.newGroupInRecord("pnd_Twra214g", "#TWRA214G");
        pnd_Twra214g.setParameterOption(ParameterOption.ByReference);
        pnd_Twra214g_Pnd_Input_Parms = pnd_Twra214g.newGroupInGroup("pnd_Twra214g_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Twra214g_Pnd_Debug_Ind = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Debug_Ind", "#DEBUG-IND", FieldType.BOOLEAN);
        pnd_Twra214g_Pnd_Abend_Ind = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN);
        pnd_Twra214g_Pnd_Display_Ind = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN);
        pnd_Twra214g_Pnd_Tax_Year = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Twra214g_Pnd_Pin = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Twra214g_Pnd_Form_Type = pnd_Twra214g_Pnd_Input_Parms.newFieldInGroup("pnd_Twra214g_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Twra214g_Pnd_Output_Data = pnd_Twra214g.newGroupInGroup("pnd_Twra214g_Pnd_Output_Data", "#OUTPUT-DATA");
        pnd_Twra214g_Pnd_Ret_Code = pnd_Twra214g_Pnd_Output_Data.newFieldInGroup("pnd_Twra214g_Pnd_Ret_Code", "#RET-CODE", FieldType.BOOLEAN);
        pnd_Twra214g_Pnd_Ret_Msg = pnd_Twra214g_Pnd_Output_Data.newFieldInGroup("pnd_Twra214g_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwra214g(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

