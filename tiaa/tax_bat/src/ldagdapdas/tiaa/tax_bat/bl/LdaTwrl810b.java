/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:16 PM
**        * FROM NATURAL LDA     : TWRL810B
************************************************************
**        * FILE NAME            : LdaTwrl810b.java
**        * CLASS NAME           : LdaTwrl810b
**        * INSTANCE NAME        : LdaTwrl810b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl810b extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_par;
    private DbsField par_Twrparti_Status;
    private DbsField par_Twrparti_Tax_Id_Type;
    private DbsField par_Twrparti_Tax_Id;
    private DbsField par_Twrparti_Citation_Ind;
    private DbsField par_Twrparti_Part_Eff_Start_Dte;
    private DbsField par_Twrparti_Part_Eff_End_Dte;
    private DbsField par_Twrparti_Part_Invrse_End_Dte;
    private DbsField par_Twrparti_Hold_Cde;
    private DbsField par_Twrparti_Test_Rec_Ind;
    private DbsField par_Twrparti_Citizen_Cde;
    private DbsField par_Twrparti_Residency_Over_Ind;
    private DbsField par_Twrparti_Residency_Type;
    private DbsField par_Twrparti_Residency_Code;
    private DbsField par_Twrparti_1st_Bnote_Ind;
    private DbsField par_Twrparti_2nd_Bnote_Ind;
    private DbsField par_Twrparti_1st_Sol_Ind;
    private DbsField par_Twrparti_2nd_Sol_Ind;
    private DbsField par_Twrparti_Cnote_Ind;
    private DbsField par_Twrparti_Frmw8_Ind;
    private DbsField par_Twrparti_Frmw9_Ind;
    private DbsField par_Twrparti_Frm1001_Ind;
    private DbsField par_Twrparti_Frm1078_Ind;
    private DbsField par_Twrparti_Updt_User;
    private DbsField par_Twrparti_Updt_Dte;
    private DbsField par_Twrparti_Updt_Time;
    private DbsField par_Twrparti_Lu_Ts;
    private DbsField par_Twrparti_Alt_Addr_Ind;
    private DbsField par_Twrparti_Addr_Source;
    private DbsField par_Twrparti_Origin;
    private DbsField par_Twrparti_Pin;
    private DbsField par_Twrparti_Participant_Name;
    private DbsField par_Twrparti_Addr_Ln1;
    private DbsField par_Twrparti_Addr_Ln2;
    private DbsField par_Twrparti_Addr_Ln3;
    private DbsField par_Twrparti_Addr_Ln4;
    private DbsField par_Twrparti_Addr_Ln5;
    private DbsField par_Twrparti_Addr_Ln6;
    private DbsField par_Twrparti_Participant_Dob;
    private DbsField par_Twrparti_Participant_Dod;
    private DbsField par_Twrparti_Wpid;
    private DbsField par_Twrparti_Locality_Cde;
    private DbsField par_Twrparti_1st_Bnote_Eff_Dte;
    private DbsField par_Twrparti_2nd_Bnote_Eff_Dte;
    private DbsField par_Twrparti_1st_Sol_Eff_Dte;
    private DbsField par_Twrparti_2nd_Sol_Eff_Dte;
    private DbsField par_Twrparti_Cnote_Eff_Dte;
    private DbsField par_Twrparti_Frmw8_Eff_Dte;
    private DbsField par_Twrparti_Frmw9_Eff_Dte;
    private DbsField par_Twrparti_Frm1001_Eff_Dte;
    private DbsField par_Twrparti_Frm1078_Eff_Dte;
    private DbsField par_Twrparti_Comment;
    private DbsField par_Twrparti_Country_Of_Res;
    private DbsField par_Twrparti_Country_Desc;
    private DbsField par_Twrparti_Alt_Addr_Eff_Dte;
    private DbsField par_Twrparti_Alt_Addr_Ln1;
    private DbsField par_Twrparti_Alt_Addr_Ln2;
    private DbsField par_Twrparti_Alt_Addr_Ln3;
    private DbsField par_Twrparti_Alt_Addr_Ln4;
    private DbsField par_Twrparti_Alt_Addr_Ln5;
    private DbsField par_Twrparti_Alt_Addr_Ln6;
    private DbsField par_Twrparti_Tran_From;
    private DbsGroup par_Twrparti_Tran_FromRedef1;
    private DbsField par_Twrparti_Tr_Fr_Tin_Type;
    private DbsField par_Twrparti_Tr_Fr_Tin;
    private DbsField par_Twrparti_Tran_To;
    private DbsGroup par_Twrparti_Tran_ToRedef2;
    private DbsField par_Twrparti_Tr_To_Tin_Type;
    private DbsField par_Twrparti_Tr_To_Tin;

    public DataAccessProgramView getVw_par() { return vw_par; }

    public DbsField getPar_Twrparti_Status() { return par_Twrparti_Status; }

    public DbsField getPar_Twrparti_Tax_Id_Type() { return par_Twrparti_Tax_Id_Type; }

    public DbsField getPar_Twrparti_Tax_Id() { return par_Twrparti_Tax_Id; }

    public DbsField getPar_Twrparti_Citation_Ind() { return par_Twrparti_Citation_Ind; }

    public DbsField getPar_Twrparti_Part_Eff_Start_Dte() { return par_Twrparti_Part_Eff_Start_Dte; }

    public DbsField getPar_Twrparti_Part_Eff_End_Dte() { return par_Twrparti_Part_Eff_End_Dte; }

    public DbsField getPar_Twrparti_Part_Invrse_End_Dte() { return par_Twrparti_Part_Invrse_End_Dte; }

    public DbsField getPar_Twrparti_Hold_Cde() { return par_Twrparti_Hold_Cde; }

    public DbsField getPar_Twrparti_Test_Rec_Ind() { return par_Twrparti_Test_Rec_Ind; }

    public DbsField getPar_Twrparti_Citizen_Cde() { return par_Twrparti_Citizen_Cde; }

    public DbsField getPar_Twrparti_Residency_Over_Ind() { return par_Twrparti_Residency_Over_Ind; }

    public DbsField getPar_Twrparti_Residency_Type() { return par_Twrparti_Residency_Type; }

    public DbsField getPar_Twrparti_Residency_Code() { return par_Twrparti_Residency_Code; }

    public DbsField getPar_Twrparti_1st_Bnote_Ind() { return par_Twrparti_1st_Bnote_Ind; }

    public DbsField getPar_Twrparti_2nd_Bnote_Ind() { return par_Twrparti_2nd_Bnote_Ind; }

    public DbsField getPar_Twrparti_1st_Sol_Ind() { return par_Twrparti_1st_Sol_Ind; }

    public DbsField getPar_Twrparti_2nd_Sol_Ind() { return par_Twrparti_2nd_Sol_Ind; }

    public DbsField getPar_Twrparti_Cnote_Ind() { return par_Twrparti_Cnote_Ind; }

    public DbsField getPar_Twrparti_Frmw8_Ind() { return par_Twrparti_Frmw8_Ind; }

    public DbsField getPar_Twrparti_Frmw9_Ind() { return par_Twrparti_Frmw9_Ind; }

    public DbsField getPar_Twrparti_Frm1001_Ind() { return par_Twrparti_Frm1001_Ind; }

    public DbsField getPar_Twrparti_Frm1078_Ind() { return par_Twrparti_Frm1078_Ind; }

    public DbsField getPar_Twrparti_Updt_User() { return par_Twrparti_Updt_User; }

    public DbsField getPar_Twrparti_Updt_Dte() { return par_Twrparti_Updt_Dte; }

    public DbsField getPar_Twrparti_Updt_Time() { return par_Twrparti_Updt_Time; }

    public DbsField getPar_Twrparti_Lu_Ts() { return par_Twrparti_Lu_Ts; }

    public DbsField getPar_Twrparti_Alt_Addr_Ind() { return par_Twrparti_Alt_Addr_Ind; }

    public DbsField getPar_Twrparti_Addr_Source() { return par_Twrparti_Addr_Source; }

    public DbsField getPar_Twrparti_Origin() { return par_Twrparti_Origin; }

    public DbsField getPar_Twrparti_Pin() { return par_Twrparti_Pin; }

    public DbsField getPar_Twrparti_Participant_Name() { return par_Twrparti_Participant_Name; }

    public DbsField getPar_Twrparti_Addr_Ln1() { return par_Twrparti_Addr_Ln1; }

    public DbsField getPar_Twrparti_Addr_Ln2() { return par_Twrparti_Addr_Ln2; }

    public DbsField getPar_Twrparti_Addr_Ln3() { return par_Twrparti_Addr_Ln3; }

    public DbsField getPar_Twrparti_Addr_Ln4() { return par_Twrparti_Addr_Ln4; }

    public DbsField getPar_Twrparti_Addr_Ln5() { return par_Twrparti_Addr_Ln5; }

    public DbsField getPar_Twrparti_Addr_Ln6() { return par_Twrparti_Addr_Ln6; }

    public DbsField getPar_Twrparti_Participant_Dob() { return par_Twrparti_Participant_Dob; }

    public DbsField getPar_Twrparti_Participant_Dod() { return par_Twrparti_Participant_Dod; }

    public DbsField getPar_Twrparti_Wpid() { return par_Twrparti_Wpid; }

    public DbsField getPar_Twrparti_Locality_Cde() { return par_Twrparti_Locality_Cde; }

    public DbsField getPar_Twrparti_1st_Bnote_Eff_Dte() { return par_Twrparti_1st_Bnote_Eff_Dte; }

    public DbsField getPar_Twrparti_2nd_Bnote_Eff_Dte() { return par_Twrparti_2nd_Bnote_Eff_Dte; }

    public DbsField getPar_Twrparti_1st_Sol_Eff_Dte() { return par_Twrparti_1st_Sol_Eff_Dte; }

    public DbsField getPar_Twrparti_2nd_Sol_Eff_Dte() { return par_Twrparti_2nd_Sol_Eff_Dte; }

    public DbsField getPar_Twrparti_Cnote_Eff_Dte() { return par_Twrparti_Cnote_Eff_Dte; }

    public DbsField getPar_Twrparti_Frmw8_Eff_Dte() { return par_Twrparti_Frmw8_Eff_Dte; }

    public DbsField getPar_Twrparti_Frmw9_Eff_Dte() { return par_Twrparti_Frmw9_Eff_Dte; }

    public DbsField getPar_Twrparti_Frm1001_Eff_Dte() { return par_Twrparti_Frm1001_Eff_Dte; }

    public DbsField getPar_Twrparti_Frm1078_Eff_Dte() { return par_Twrparti_Frm1078_Eff_Dte; }

    public DbsField getPar_Twrparti_Comment() { return par_Twrparti_Comment; }

    public DbsField getPar_Twrparti_Country_Of_Res() { return par_Twrparti_Country_Of_Res; }

    public DbsField getPar_Twrparti_Country_Desc() { return par_Twrparti_Country_Desc; }

    public DbsField getPar_Twrparti_Alt_Addr_Eff_Dte() { return par_Twrparti_Alt_Addr_Eff_Dte; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln1() { return par_Twrparti_Alt_Addr_Ln1; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln2() { return par_Twrparti_Alt_Addr_Ln2; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln3() { return par_Twrparti_Alt_Addr_Ln3; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln4() { return par_Twrparti_Alt_Addr_Ln4; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln5() { return par_Twrparti_Alt_Addr_Ln5; }

    public DbsField getPar_Twrparti_Alt_Addr_Ln6() { return par_Twrparti_Alt_Addr_Ln6; }

    public DbsField getPar_Twrparti_Tran_From() { return par_Twrparti_Tran_From; }

    public DbsGroup getPar_Twrparti_Tran_FromRedef1() { return par_Twrparti_Tran_FromRedef1; }

    public DbsField getPar_Twrparti_Tr_Fr_Tin_Type() { return par_Twrparti_Tr_Fr_Tin_Type; }

    public DbsField getPar_Twrparti_Tr_Fr_Tin() { return par_Twrparti_Tr_Fr_Tin; }

    public DbsField getPar_Twrparti_Tran_To() { return par_Twrparti_Tran_To; }

    public DbsGroup getPar_Twrparti_Tran_ToRedef2() { return par_Twrparti_Tran_ToRedef2; }

    public DbsField getPar_Twrparti_Tr_To_Tin_Type() { return par_Twrparti_Tr_To_Tin_Type; }

    public DbsField getPar_Twrparti_Tr_To_Tin() { return par_Twrparti_Tr_To_Tin; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_par = new DataAccessProgramView(new NameInfo("vw_par", "PAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        par_Twrparti_Status = vw_par.getRecord().newFieldInGroup("par_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        par_Twrparti_Tax_Id_Type = vw_par.getRecord().newFieldInGroup("par_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID_TYPE");
        par_Twrparti_Tax_Id = vw_par.getRecord().newFieldInGroup("par_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");
        par_Twrparti_Citation_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_CITATION_IND");
        par_Twrparti_Part_Eff_Start_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Part_Eff_Start_Dte", "TWRPARTI-PART-EFF-START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_START_DTE");
        par_Twrparti_Part_Eff_End_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        par_Twrparti_Part_Invrse_End_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Part_Invrse_End_Dte", "TWRPARTI-PART-INVRSE-END-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_INVRSE_END_DTE");
        par_Twrparti_Hold_Cde = vw_par.getRecord().newFieldInGroup("par_Twrparti_Hold_Cde", "TWRPARTI-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_HOLD_CDE");
        par_Twrparti_Test_Rec_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Test_Rec_Ind", "TWRPARTI-TEST-REC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_TEST_REC_IND");
        par_Twrparti_Citizen_Cde = vw_par.getRecord().newFieldInGroup("par_Twrparti_Citizen_Cde", "TWRPARTI-CITIZEN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPARTI_CITIZEN_CDE");
        par_Twrparti_Residency_Over_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Residency_Over_Ind", "TWRPARTI-RESIDENCY-OVER-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_OVER_IND");
        par_Twrparti_Residency_Type = vw_par.getRecord().newFieldInGroup("par_Twrparti_Residency_Type", "TWRPARTI-RESIDENCY-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_TYPE");
        par_Twrparti_Residency_Code = vw_par.getRecord().newFieldInGroup("par_Twrparti_Residency_Code", "TWRPARTI-RESIDENCY-CODE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_CODE");
        par_Twrparti_1st_Bnote_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_1st_Bnote_Ind", "TWRPARTI-1ST-BNOTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_1ST_BNOTE_IND");
        par_Twrparti_2nd_Bnote_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_2nd_Bnote_Ind", "TWRPARTI-2ND-BNOTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_2ND_BNOTE_IND");
        par_Twrparti_1st_Sol_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_1st_Sol_Ind", "TWRPARTI-1ST-SOL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_1ST_SOL_IND");
        par_Twrparti_2nd_Sol_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_2nd_Sol_Ind", "TWRPARTI-2ND-SOL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_2ND_SOL_IND");
        par_Twrparti_Cnote_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Cnote_Ind", "TWRPARTI-CNOTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_CNOTE_IND");
        par_Twrparti_Frmw8_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frmw8_Ind", "TWRPARTI-FRMW8-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW8_IND");
        par_Twrparti_Frmw9_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frmw9_Ind", "TWRPARTI-FRMW9-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW9_IND");
        par_Twrparti_Frm1001_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frm1001_Ind", "TWRPARTI-FRM1001-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRM1001_IND");
        par_Twrparti_Frm1078_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frm1078_Ind", "TWRPARTI-FRM1078-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRM1078_IND");
        par_Twrparti_Updt_User = vw_par.getRecord().newFieldInGroup("par_Twrparti_Updt_User", "TWRPARTI-UPDT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_USER");
        par_Twrparti_Updt_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Updt_Dte", "TWRPARTI-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_DTE");
        par_Twrparti_Updt_Time = vw_par.getRecord().newFieldInGroup("par_Twrparti_Updt_Time", "TWRPARTI-UPDT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_TIME");
        par_Twrparti_Lu_Ts = vw_par.getRecord().newFieldInGroup("par_Twrparti_Lu_Ts", "TWRPARTI-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TWRPARTI_LU_TS");
        par_Twrparti_Alt_Addr_Ind = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ind", "TWRPARTI-ALT-ADDR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_IND");
        par_Twrparti_Addr_Source = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Source", "TWRPARTI-ADDR-SOURCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_SOURCE");
        par_Twrparti_Origin = vw_par.getRecord().newFieldInGroup("par_Twrparti_Origin", "TWRPARTI-ORIGIN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPARTI_ORIGIN");
        par_Twrparti_Pin = vw_par.getRecord().newFieldInGroup("par_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, "TWRPARTI_PIN");
        par_Twrparti_Participant_Name = vw_par.getRecord().newFieldInGroup("par_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        par_Twrparti_Addr_Ln1 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln1", "TWRPARTI-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN1");
        par_Twrparti_Addr_Ln2 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln2", "TWRPARTI-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN2");
        par_Twrparti_Addr_Ln3 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln3", "TWRPARTI-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN3");
        par_Twrparti_Addr_Ln4 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln4", "TWRPARTI-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN4");
        par_Twrparti_Addr_Ln5 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln5", "TWRPARTI-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN5");
        par_Twrparti_Addr_Ln6 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Addr_Ln6", "TWRPARTI-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN6");
        par_Twrparti_Participant_Dob = vw_par.getRecord().newFieldInGroup("par_Twrparti_Participant_Dob", "TWRPARTI-PARTICIPANT-DOB", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOB");
        par_Twrparti_Participant_Dod = vw_par.getRecord().newFieldInGroup("par_Twrparti_Participant_Dod", "TWRPARTI-PARTICIPANT-DOD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOD");
        par_Twrparti_Wpid = vw_par.getRecord().newFieldInGroup("par_Twrparti_Wpid", "TWRPARTI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TWRPARTI_WPID");
        par_Twrparti_Locality_Cde = vw_par.getRecord().newFieldInGroup("par_Twrparti_Locality_Cde", "TWRPARTI-LOCALITY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRPARTI_LOCALITY_CDE");
        par_Twrparti_1st_Bnote_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_1st_Bnote_Eff_Dte", "TWRPARTI-1ST-BNOTE-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_1ST_BNOTE_EFF_DTE");
        par_Twrparti_2nd_Bnote_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_2nd_Bnote_Eff_Dte", "TWRPARTI-2ND-BNOTE-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_2ND_BNOTE_EFF_DTE");
        par_Twrparti_1st_Sol_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_1st_Sol_Eff_Dte", "TWRPARTI-1ST-SOL-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_1ST_SOL_EFF_DTE");
        par_Twrparti_2nd_Sol_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_2nd_Sol_Eff_Dte", "TWRPARTI-2ND-SOL-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_2ND_SOL_EFF_DTE");
        par_Twrparti_Cnote_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Cnote_Eff_Dte", "TWRPARTI-CNOTE-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_CNOTE_EFF_DTE");
        par_Twrparti_Frmw8_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frmw8_Eff_Dte", "TWRPARTI-FRMW8-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW8_EFF_DTE");
        par_Twrparti_Frmw9_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frmw9_Eff_Dte", "TWRPARTI-FRMW9-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW9_EFF_DTE");
        par_Twrparti_Frm1001_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frm1001_Eff_Dte", "TWRPARTI-FRM1001-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRM1001_EFF_DTE");
        par_Twrparti_Frm1078_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Frm1078_Eff_Dte", "TWRPARTI-FRM1078-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRM1078_EFF_DTE");
        par_Twrparti_Comment = vw_par.getRecord().newFieldInGroup("par_Twrparti_Comment", "TWRPARTI-COMMENT", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "TWRPARTI_COMMENT");
        par_Twrparti_Country_Of_Res = vw_par.getRecord().newFieldInGroup("par_Twrparti_Country_Of_Res", "TWRPARTI-COUNTRY-OF-RES", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "TWRPARTI_COUNTRY_OF_RES");
        par_Twrparti_Country_Desc = vw_par.getRecord().newFieldInGroup("par_Twrparti_Country_Desc", "TWRPARTI-COUNTRY-DESC", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_COUNTRY_DESC");
        par_Twrparti_Alt_Addr_Eff_Dte = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Eff_Dte", "TWRPARTI-ALT-ADDR-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_EFF_DTE");
        par_Twrparti_Alt_Addr_Ln1 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln1", "TWRPARTI-ALT-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN1");
        par_Twrparti_Alt_Addr_Ln2 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln2", "TWRPARTI-ALT-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN2");
        par_Twrparti_Alt_Addr_Ln3 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln3", "TWRPARTI-ALT-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN3");
        par_Twrparti_Alt_Addr_Ln4 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln4", "TWRPARTI-ALT-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN4");
        par_Twrparti_Alt_Addr_Ln5 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln5", "TWRPARTI-ALT-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN5");
        par_Twrparti_Alt_Addr_Ln6 = vw_par.getRecord().newFieldInGroup("par_Twrparti_Alt_Addr_Ln6", "TWRPARTI-ALT-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ALT_ADDR_LN6");
        par_Twrparti_Tran_From = vw_par.getRecord().newFieldInGroup("par_Twrparti_Tran_From", "TWRPARTI-TRAN-FROM", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "TWRPARTI_TRAN_FROM");
        par_Twrparti_Tran_FromRedef1 = vw_par.getRecord().newGroupInGroup("par_Twrparti_Tran_FromRedef1", "Redefines", par_Twrparti_Tran_From);
        par_Twrparti_Tr_Fr_Tin_Type = par_Twrparti_Tran_FromRedef1.newFieldInGroup("par_Twrparti_Tr_Fr_Tin_Type", "TWRPARTI-TR-FR-TIN-TYPE", FieldType.STRING, 
            1);
        par_Twrparti_Tr_Fr_Tin = par_Twrparti_Tran_FromRedef1.newFieldInGroup("par_Twrparti_Tr_Fr_Tin", "TWRPARTI-TR-FR-TIN", FieldType.STRING, 10);
        par_Twrparti_Tran_To = vw_par.getRecord().newFieldInGroup("par_Twrparti_Tran_To", "TWRPARTI-TRAN-TO", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "TWRPARTI_TRAN_TO");
        par_Twrparti_Tran_ToRedef2 = vw_par.getRecord().newGroupInGroup("par_Twrparti_Tran_ToRedef2", "Redefines", par_Twrparti_Tran_To);
        par_Twrparti_Tr_To_Tin_Type = par_Twrparti_Tran_ToRedef2.newFieldInGroup("par_Twrparti_Tr_To_Tin_Type", "TWRPARTI-TR-TO-TIN-TYPE", FieldType.STRING, 
            1);
        par_Twrparti_Tr_To_Tin = par_Twrparti_Tran_ToRedef2.newFieldInGroup("par_Twrparti_Tr_To_Tin", "TWRPARTI-TR-TO-TIN", FieldType.STRING, 10);

        this.setRecordName("LdaTwrl810b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_par.reset();
    }

    // Constructor
    public LdaTwrl810b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
