/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:23 PM
**        * FROM NATURAL LDA     : TWRL813A
************************************************************
**        * FILE NAME            : LdaTwrl813a.java
**        * CLASS NAME           : LdaTwrl813a
**        * INSTANCE NAME        : LdaTwrl813a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl813a extends DbsRecord
{
    // Properties
    private DbsGroup cited_Record;
    private DbsField cited_Record_Twrt_Company_Cde;
    private DbsField cited_Record_Twrt_Source;
    private DbsField cited_Record_Twrt_Cntrct_Nbr;
    private DbsField cited_Record_Twrt_Payee_Cde;
    private DbsField cited_Record_Twrt_Tax_Id_Type;
    private DbsField cited_Record_Twrt_Tax_Id;
    private DbsField cited_Record_Twrt_Pin_Id;
    private DbsField cited_Record_Twrt_Paymt_Dte;
    private DbsField cited_Record_Twrt_Pay_Set_Types;
    private DbsGroup cited_Record_Twrt_Pay_Set_TypesRedef1;
    private DbsField cited_Record_Twrt_Paymt_Type;
    private DbsField cited_Record_Twrt_Settl_Type;
    private DbsField cited_Record_Twrt_Currency_Cde;
    private DbsField cited_Record_Twrt_Locality_Cde;
    private DbsField cited_Record_Twrt_Rollover_Ind;
    private DbsField cited_Record_Twrt_State_Rsdncy;
    private DbsField cited_Record_Twrt_Citizenship;
    private DbsField cited_Record_Twrt_Dob_A;
    private DbsField cited_Record_Twrt_Gross_Amt;
    private DbsField cited_Record_Twrt_Fed_Whhld_Amt;
    private DbsField cited_Record_Twrt_State_Whhld_Amt;
    private DbsField cited_Record_Twrt_Interest_Amt;
    private DbsField cited_Record_Twrt_Ivc_Amt;
    private DbsField cited_Record_Twrt_Ivc_Cde;
    private DbsField cited_Record_Twrt_Na_Line;
    private DbsField cited_Record_Twrt_Tax_Elct_Cde;
    private DbsField cited_Record_Twrt_Local_Whhld_Amt;
    private DbsField cited_Record_Twrt_Tot_Ivc_9b_Amt;
    private DbsField cited_Record_Twrt_Disability_Cde;
    private DbsField cited_Record_Twrt_Lob_Cde;
    private DbsField cited_Record_Twrt_Tax_Year;
    private DbsField cited_Record_Twrt_Dod;
    private DbsField cited_Record_Twrt_Can_Whhld_Ind;
    private DbsField cited_Record_Twrt_Can_Whhld_Amt;
    private DbsField cited_Record_Twrt_Nra_Whhld_Amt;
    private DbsField cited_Record_Twrt_Payee_Tax_Res_Sts;
    private DbsField cited_Record_Twrt_Pymnt_Tax_Form_1078;
    private DbsField cited_Record_Twrt_Pymnt_Tax_Form_1001;
    private DbsField cited_Record_Twrt_Pymnt_Tax_Exempt_Ind;
    private DbsField cited_Record_Twrt_Pymnt_Settlmnt_Dte;
    private DbsField cited_Record_Twrt_Pymnt_Acctg_Dte;
    private DbsField cited_Record_Twrt_Pymnt_Intrfce_Dte;
    private DbsField cited_Record_Twrt_Ivc_Rcvry_Cde;
    private DbsField cited_Record_Twrt_Total_Distr_Amt;
    private DbsField cited_Record_Twrt_Distr_Pct;
    private DbsField cited_Record_Twrt_Addr_Chg_Ind;
    private DbsField cited_Record_Twrt_Rej_Err_Nbr;
    private DbsField cited_Record_Twrt_Type_Refer;
    private DbsField cited_Record_Twrt_Distrib_Cde;
    private DbsField cited_Record_Twrt_Citation_Flag;
    private DbsField cited_Record_Twrt_Contract_Type;
    private DbsField cited_Record_Twrt_Annt_Residency_Code;
    private DbsField cited_Record_Twrt_Annt_Locality_Code;
    private DbsField cited_Record_Twrt_Election_Code;
    private DbsField cited_Record_Twrt_Fed_Nra_Tax_Rate;
    private DbsField cited_Record_Twrt_Payment_Form1001;
    private DbsField cited_Record_Twrt_Distributing_Irc_Cde;
    private DbsField cited_Record_Twrt_Receiving_Irc_Cde;
    private DbsField cited_Record_Twrt_Pymnt_Reqst_Nbr;
    private DbsField cited_Record_Twrt_Hardship_Cde;
    private DbsField cited_Record_Twrt_Termination_Of_Empl_Cde;
    private DbsField cited_Record_Twrt_Distribution_Code;
    private DbsField cited_Record_Twrt_Nra_Exemption_Code;
    private DbsField cited_Record_Twrt_Tax_Type;
    private DbsField cited_Record_Twrt_Plan_Type;
    private DbsField cited_Record_Twrt_Fil;
    private DbsField cited_Record_Twrt_Distribution_Type;
    private DbsField cited_Record_Twrt_Check_Reason;
    private DbsField cited_Record_Twrt_Roth_Qual_Ind;
    private DbsField cited_Record_Twrt_Roth_Death_Ind;
    private DbsField cited_Record_Twrt_Filler;
    private DbsField cited_Record_Twrt_Error_Table;
    private DbsField cited_Record_Twrt_Unique_Id;

    public DbsGroup getCited_Record() { return cited_Record; }

    public DbsField getCited_Record_Twrt_Company_Cde() { return cited_Record_Twrt_Company_Cde; }

    public DbsField getCited_Record_Twrt_Source() { return cited_Record_Twrt_Source; }

    public DbsField getCited_Record_Twrt_Cntrct_Nbr() { return cited_Record_Twrt_Cntrct_Nbr; }

    public DbsField getCited_Record_Twrt_Payee_Cde() { return cited_Record_Twrt_Payee_Cde; }

    public DbsField getCited_Record_Twrt_Tax_Id_Type() { return cited_Record_Twrt_Tax_Id_Type; }

    public DbsField getCited_Record_Twrt_Tax_Id() { return cited_Record_Twrt_Tax_Id; }

    public DbsField getCited_Record_Twrt_Pin_Id() { return cited_Record_Twrt_Pin_Id; }

    public DbsField getCited_Record_Twrt_Paymt_Dte() { return cited_Record_Twrt_Paymt_Dte; }

    public DbsField getCited_Record_Twrt_Pay_Set_Types() { return cited_Record_Twrt_Pay_Set_Types; }

    public DbsGroup getCited_Record_Twrt_Pay_Set_TypesRedef1() { return cited_Record_Twrt_Pay_Set_TypesRedef1; }

    public DbsField getCited_Record_Twrt_Paymt_Type() { return cited_Record_Twrt_Paymt_Type; }

    public DbsField getCited_Record_Twrt_Settl_Type() { return cited_Record_Twrt_Settl_Type; }

    public DbsField getCited_Record_Twrt_Currency_Cde() { return cited_Record_Twrt_Currency_Cde; }

    public DbsField getCited_Record_Twrt_Locality_Cde() { return cited_Record_Twrt_Locality_Cde; }

    public DbsField getCited_Record_Twrt_Rollover_Ind() { return cited_Record_Twrt_Rollover_Ind; }

    public DbsField getCited_Record_Twrt_State_Rsdncy() { return cited_Record_Twrt_State_Rsdncy; }

    public DbsField getCited_Record_Twrt_Citizenship() { return cited_Record_Twrt_Citizenship; }

    public DbsField getCited_Record_Twrt_Dob_A() { return cited_Record_Twrt_Dob_A; }

    public DbsField getCited_Record_Twrt_Gross_Amt() { return cited_Record_Twrt_Gross_Amt; }

    public DbsField getCited_Record_Twrt_Fed_Whhld_Amt() { return cited_Record_Twrt_Fed_Whhld_Amt; }

    public DbsField getCited_Record_Twrt_State_Whhld_Amt() { return cited_Record_Twrt_State_Whhld_Amt; }

    public DbsField getCited_Record_Twrt_Interest_Amt() { return cited_Record_Twrt_Interest_Amt; }

    public DbsField getCited_Record_Twrt_Ivc_Amt() { return cited_Record_Twrt_Ivc_Amt; }

    public DbsField getCited_Record_Twrt_Ivc_Cde() { return cited_Record_Twrt_Ivc_Cde; }

    public DbsField getCited_Record_Twrt_Na_Line() { return cited_Record_Twrt_Na_Line; }

    public DbsField getCited_Record_Twrt_Tax_Elct_Cde() { return cited_Record_Twrt_Tax_Elct_Cde; }

    public DbsField getCited_Record_Twrt_Local_Whhld_Amt() { return cited_Record_Twrt_Local_Whhld_Amt; }

    public DbsField getCited_Record_Twrt_Tot_Ivc_9b_Amt() { return cited_Record_Twrt_Tot_Ivc_9b_Amt; }

    public DbsField getCited_Record_Twrt_Disability_Cde() { return cited_Record_Twrt_Disability_Cde; }

    public DbsField getCited_Record_Twrt_Lob_Cde() { return cited_Record_Twrt_Lob_Cde; }

    public DbsField getCited_Record_Twrt_Tax_Year() { return cited_Record_Twrt_Tax_Year; }

    public DbsField getCited_Record_Twrt_Dod() { return cited_Record_Twrt_Dod; }

    public DbsField getCited_Record_Twrt_Can_Whhld_Ind() { return cited_Record_Twrt_Can_Whhld_Ind; }

    public DbsField getCited_Record_Twrt_Can_Whhld_Amt() { return cited_Record_Twrt_Can_Whhld_Amt; }

    public DbsField getCited_Record_Twrt_Nra_Whhld_Amt() { return cited_Record_Twrt_Nra_Whhld_Amt; }

    public DbsField getCited_Record_Twrt_Payee_Tax_Res_Sts() { return cited_Record_Twrt_Payee_Tax_Res_Sts; }

    public DbsField getCited_Record_Twrt_Pymnt_Tax_Form_1078() { return cited_Record_Twrt_Pymnt_Tax_Form_1078; }

    public DbsField getCited_Record_Twrt_Pymnt_Tax_Form_1001() { return cited_Record_Twrt_Pymnt_Tax_Form_1001; }

    public DbsField getCited_Record_Twrt_Pymnt_Tax_Exempt_Ind() { return cited_Record_Twrt_Pymnt_Tax_Exempt_Ind; }

    public DbsField getCited_Record_Twrt_Pymnt_Settlmnt_Dte() { return cited_Record_Twrt_Pymnt_Settlmnt_Dte; }

    public DbsField getCited_Record_Twrt_Pymnt_Acctg_Dte() { return cited_Record_Twrt_Pymnt_Acctg_Dte; }

    public DbsField getCited_Record_Twrt_Pymnt_Intrfce_Dte() { return cited_Record_Twrt_Pymnt_Intrfce_Dte; }

    public DbsField getCited_Record_Twrt_Ivc_Rcvry_Cde() { return cited_Record_Twrt_Ivc_Rcvry_Cde; }

    public DbsField getCited_Record_Twrt_Total_Distr_Amt() { return cited_Record_Twrt_Total_Distr_Amt; }

    public DbsField getCited_Record_Twrt_Distr_Pct() { return cited_Record_Twrt_Distr_Pct; }

    public DbsField getCited_Record_Twrt_Addr_Chg_Ind() { return cited_Record_Twrt_Addr_Chg_Ind; }

    public DbsField getCited_Record_Twrt_Rej_Err_Nbr() { return cited_Record_Twrt_Rej_Err_Nbr; }

    public DbsField getCited_Record_Twrt_Type_Refer() { return cited_Record_Twrt_Type_Refer; }

    public DbsField getCited_Record_Twrt_Distrib_Cde() { return cited_Record_Twrt_Distrib_Cde; }

    public DbsField getCited_Record_Twrt_Citation_Flag() { return cited_Record_Twrt_Citation_Flag; }

    public DbsField getCited_Record_Twrt_Contract_Type() { return cited_Record_Twrt_Contract_Type; }

    public DbsField getCited_Record_Twrt_Annt_Residency_Code() { return cited_Record_Twrt_Annt_Residency_Code; }

    public DbsField getCited_Record_Twrt_Annt_Locality_Code() { return cited_Record_Twrt_Annt_Locality_Code; }

    public DbsField getCited_Record_Twrt_Election_Code() { return cited_Record_Twrt_Election_Code; }

    public DbsField getCited_Record_Twrt_Fed_Nra_Tax_Rate() { return cited_Record_Twrt_Fed_Nra_Tax_Rate; }

    public DbsField getCited_Record_Twrt_Payment_Form1001() { return cited_Record_Twrt_Payment_Form1001; }

    public DbsField getCited_Record_Twrt_Distributing_Irc_Cde() { return cited_Record_Twrt_Distributing_Irc_Cde; }

    public DbsField getCited_Record_Twrt_Receiving_Irc_Cde() { return cited_Record_Twrt_Receiving_Irc_Cde; }

    public DbsField getCited_Record_Twrt_Pymnt_Reqst_Nbr() { return cited_Record_Twrt_Pymnt_Reqst_Nbr; }

    public DbsField getCited_Record_Twrt_Hardship_Cde() { return cited_Record_Twrt_Hardship_Cde; }

    public DbsField getCited_Record_Twrt_Termination_Of_Empl_Cde() { return cited_Record_Twrt_Termination_Of_Empl_Cde; }

    public DbsField getCited_Record_Twrt_Distribution_Code() { return cited_Record_Twrt_Distribution_Code; }

    public DbsField getCited_Record_Twrt_Nra_Exemption_Code() { return cited_Record_Twrt_Nra_Exemption_Code; }

    public DbsField getCited_Record_Twrt_Tax_Type() { return cited_Record_Twrt_Tax_Type; }

    public DbsField getCited_Record_Twrt_Plan_Type() { return cited_Record_Twrt_Plan_Type; }

    public DbsField getCited_Record_Twrt_Fil() { return cited_Record_Twrt_Fil; }

    public DbsField getCited_Record_Twrt_Distribution_Type() { return cited_Record_Twrt_Distribution_Type; }

    public DbsField getCited_Record_Twrt_Check_Reason() { return cited_Record_Twrt_Check_Reason; }

    public DbsField getCited_Record_Twrt_Roth_Qual_Ind() { return cited_Record_Twrt_Roth_Qual_Ind; }

    public DbsField getCited_Record_Twrt_Roth_Death_Ind() { return cited_Record_Twrt_Roth_Death_Ind; }

    public DbsField getCited_Record_Twrt_Filler() { return cited_Record_Twrt_Filler; }

    public DbsField getCited_Record_Twrt_Error_Table() { return cited_Record_Twrt_Error_Table; }

    public DbsField getCited_Record_Twrt_Unique_Id() { return cited_Record_Twrt_Unique_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cited_Record = newGroupInRecord("cited_Record", "CITED-RECORD");
        cited_Record_Twrt_Company_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Company_Cde", "TWRT-COMPANY-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Source = cited_Record.newFieldInGroup("cited_Record_Twrt_Source", "TWRT-SOURCE", FieldType.STRING, 2);
        cited_Record_Twrt_Cntrct_Nbr = cited_Record.newFieldInGroup("cited_Record_Twrt_Cntrct_Nbr", "TWRT-CNTRCT-NBR", FieldType.STRING, 8);
        cited_Record_Twrt_Payee_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Payee_Cde", "TWRT-PAYEE-CDE", FieldType.STRING, 2);
        cited_Record_Twrt_Tax_Id_Type = cited_Record.newFieldInGroup("cited_Record_Twrt_Tax_Id_Type", "TWRT-TAX-ID-TYPE", FieldType.STRING, 1);
        cited_Record_Twrt_Tax_Id = cited_Record.newFieldInGroup("cited_Record_Twrt_Tax_Id", "TWRT-TAX-ID", FieldType.STRING, 10);
        cited_Record_Twrt_Pin_Id = cited_Record.newFieldInGroup("cited_Record_Twrt_Pin_Id", "TWRT-PIN-ID", FieldType.STRING, 12);
        cited_Record_Twrt_Paymt_Dte = cited_Record.newFieldInGroup("cited_Record_Twrt_Paymt_Dte", "TWRT-PAYMT-DTE", FieldType.STRING, 8);
        cited_Record_Twrt_Pay_Set_Types = cited_Record.newFieldInGroup("cited_Record_Twrt_Pay_Set_Types", "TWRT-PAY-SET-TYPES", FieldType.STRING, 2);
        cited_Record_Twrt_Pay_Set_TypesRedef1 = cited_Record.newGroupInGroup("cited_Record_Twrt_Pay_Set_TypesRedef1", "Redefines", cited_Record_Twrt_Pay_Set_Types);
        cited_Record_Twrt_Paymt_Type = cited_Record_Twrt_Pay_Set_TypesRedef1.newFieldInGroup("cited_Record_Twrt_Paymt_Type", "TWRT-PAYMT-TYPE", FieldType.STRING, 
            1);
        cited_Record_Twrt_Settl_Type = cited_Record_Twrt_Pay_Set_TypesRedef1.newFieldInGroup("cited_Record_Twrt_Settl_Type", "TWRT-SETTL-TYPE", FieldType.STRING, 
            1);
        cited_Record_Twrt_Currency_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Currency_Cde", "TWRT-CURRENCY-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Locality_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Locality_Cde", "TWRT-LOCALITY-CDE", FieldType.STRING, 3);
        cited_Record_Twrt_Rollover_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Rollover_Ind", "TWRT-ROLLOVER-IND", FieldType.STRING, 1);
        cited_Record_Twrt_State_Rsdncy = cited_Record.newFieldInGroup("cited_Record_Twrt_State_Rsdncy", "TWRT-STATE-RSDNCY", FieldType.STRING, 2);
        cited_Record_Twrt_Citizenship = cited_Record.newFieldInGroup("cited_Record_Twrt_Citizenship", "TWRT-CITIZENSHIP", FieldType.STRING, 2);
        cited_Record_Twrt_Dob_A = cited_Record.newFieldInGroup("cited_Record_Twrt_Dob_A", "TWRT-DOB-A", FieldType.STRING, 8);
        cited_Record_Twrt_Gross_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Gross_Amt", "TWRT-GROSS-AMT", FieldType.PACKED_DECIMAL, 11,2);
        cited_Record_Twrt_Fed_Whhld_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Fed_Whhld_Amt", "TWRT-FED-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_State_Whhld_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_State_Whhld_Amt", "TWRT-STATE-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Interest_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Interest_Amt", "TWRT-INTEREST-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Ivc_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Ivc_Amt", "TWRT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        cited_Record_Twrt_Ivc_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Ivc_Cde", "TWRT-IVC-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Na_Line = cited_Record.newFieldArrayInGroup("cited_Record_Twrt_Na_Line", "TWRT-NA-LINE", FieldType.STRING, 35, new DbsArrayController(1,
            8));
        cited_Record_Twrt_Tax_Elct_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Tax_Elct_Cde", "TWRT-TAX-ELCT-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Local_Whhld_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Local_Whhld_Amt", "TWRT-LOCAL-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Tot_Ivc_9b_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Tot_Ivc_9b_Amt", "TWRT-TOT-IVC-9B-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Disability_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Disability_Cde", "TWRT-DISABILITY-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Lob_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Lob_Cde", "TWRT-LOB-CDE", FieldType.STRING, 4);
        cited_Record_Twrt_Tax_Year = cited_Record.newFieldInGroup("cited_Record_Twrt_Tax_Year", "TWRT-TAX-YEAR", FieldType.NUMERIC, 4);
        cited_Record_Twrt_Dod = cited_Record.newFieldInGroup("cited_Record_Twrt_Dod", "TWRT-DOD", FieldType.STRING, 8);
        cited_Record_Twrt_Can_Whhld_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Can_Whhld_Ind", "TWRT-CAN-WHHLD-IND", FieldType.STRING, 1);
        cited_Record_Twrt_Can_Whhld_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Can_Whhld_Amt", "TWRT-CAN-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Nra_Whhld_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Nra_Whhld_Amt", "TWRT-NRA-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        cited_Record_Twrt_Payee_Tax_Res_Sts = cited_Record.newFieldInGroup("cited_Record_Twrt_Payee_Tax_Res_Sts", "TWRT-PAYEE-TAX-RES-STS", FieldType.STRING, 
            1);
        cited_Record_Twrt_Pymnt_Tax_Form_1078 = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Tax_Form_1078", "TWRT-PYMNT-TAX-FORM-1078", FieldType.STRING, 
            1);
        cited_Record_Twrt_Pymnt_Tax_Form_1001 = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Tax_Form_1001", "TWRT-PYMNT-TAX-FORM-1001", FieldType.STRING, 
            1);
        cited_Record_Twrt_Pymnt_Tax_Exempt_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Tax_Exempt_Ind", "TWRT-PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 
            1);
        cited_Record_Twrt_Pymnt_Settlmnt_Dte = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Settlmnt_Dte", "TWRT-PYMNT-SETTLMNT-DTE", FieldType.STRING, 
            8);
        cited_Record_Twrt_Pymnt_Acctg_Dte = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Acctg_Dte", "TWRT-PYMNT-ACCTG-DTE", FieldType.STRING, 
            8);
        cited_Record_Twrt_Pymnt_Intrfce_Dte = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Intrfce_Dte", "TWRT-PYMNT-INTRFCE-DTE", FieldType.STRING, 
            8);
        cited_Record_Twrt_Ivc_Rcvry_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Ivc_Rcvry_Cde", "TWRT-IVC-RCVRY-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Total_Distr_Amt = cited_Record.newFieldInGroup("cited_Record_Twrt_Total_Distr_Amt", "TWRT-TOTAL-DISTR-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        cited_Record_Twrt_Distr_Pct = cited_Record.newFieldInGroup("cited_Record_Twrt_Distr_Pct", "TWRT-DISTR-PCT", FieldType.PACKED_DECIMAL, 7,4);
        cited_Record_Twrt_Addr_Chg_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Addr_Chg_Ind", "TWRT-ADDR-CHG-IND", FieldType.STRING, 1);
        cited_Record_Twrt_Rej_Err_Nbr = cited_Record.newFieldInGroup("cited_Record_Twrt_Rej_Err_Nbr", "TWRT-REJ-ERR-NBR", FieldType.STRING, 2);
        cited_Record_Twrt_Type_Refer = cited_Record.newFieldInGroup("cited_Record_Twrt_Type_Refer", "TWRT-TYPE-REFER", FieldType.STRING, 2);
        cited_Record_Twrt_Distrib_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Distrib_Cde", "TWRT-DISTRIB-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Citation_Flag = cited_Record.newFieldInGroup("cited_Record_Twrt_Citation_Flag", "TWRT-CITATION-FLAG", FieldType.STRING, 1);
        cited_Record_Twrt_Contract_Type = cited_Record.newFieldInGroup("cited_Record_Twrt_Contract_Type", "TWRT-CONTRACT-TYPE", FieldType.STRING, 5);
        cited_Record_Twrt_Annt_Residency_Code = cited_Record.newFieldInGroup("cited_Record_Twrt_Annt_Residency_Code", "TWRT-ANNT-RESIDENCY-CODE", FieldType.STRING, 
            3);
        cited_Record_Twrt_Annt_Locality_Code = cited_Record.newFieldInGroup("cited_Record_Twrt_Annt_Locality_Code", "TWRT-ANNT-LOCALITY-CODE", FieldType.STRING, 
            3);
        cited_Record_Twrt_Election_Code = cited_Record.newFieldInGroup("cited_Record_Twrt_Election_Code", "TWRT-ELECTION-CODE", FieldType.NUMERIC, 3);
        cited_Record_Twrt_Fed_Nra_Tax_Rate = cited_Record.newFieldInGroup("cited_Record_Twrt_Fed_Nra_Tax_Rate", "TWRT-FED-NRA-TAX-RATE", FieldType.PACKED_DECIMAL, 
            5,2);
        cited_Record_Twrt_Payment_Form1001 = cited_Record.newFieldInGroup("cited_Record_Twrt_Payment_Form1001", "TWRT-PAYMENT-FORM1001", FieldType.STRING, 
            1);
        cited_Record_Twrt_Distributing_Irc_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Distributing_Irc_Cde", "TWRT-DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            20);
        cited_Record_Twrt_Receiving_Irc_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Receiving_Irc_Cde", "TWRT-RECEIVING-IRC-CDE", FieldType.STRING, 
            2);
        cited_Record_Twrt_Pymnt_Reqst_Nbr = cited_Record.newFieldInGroup("cited_Record_Twrt_Pymnt_Reqst_Nbr", "TWRT-PYMNT-REQST-NBR", FieldType.STRING, 
            35);
        cited_Record_Twrt_Hardship_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Hardship_Cde", "TWRT-HARDSHIP-CDE", FieldType.STRING, 1);
        cited_Record_Twrt_Termination_Of_Empl_Cde = cited_Record.newFieldInGroup("cited_Record_Twrt_Termination_Of_Empl_Cde", "TWRT-TERMINATION-OF-EMPL-CDE", 
            FieldType.STRING, 1);
        cited_Record_Twrt_Distribution_Code = cited_Record.newFieldInGroup("cited_Record_Twrt_Distribution_Code", "TWRT-DISTRIBUTION-CODE", FieldType.STRING, 
            2);
        cited_Record_Twrt_Nra_Exemption_Code = cited_Record.newFieldInGroup("cited_Record_Twrt_Nra_Exemption_Code", "TWRT-NRA-EXEMPTION-CODE", FieldType.STRING, 
            2);
        cited_Record_Twrt_Tax_Type = cited_Record.newFieldInGroup("cited_Record_Twrt_Tax_Type", "TWRT-TAX-TYPE", FieldType.STRING, 1);
        cited_Record_Twrt_Plan_Type = cited_Record.newFieldInGroup("cited_Record_Twrt_Plan_Type", "TWRT-PLAN-TYPE", FieldType.STRING, 1);
        cited_Record_Twrt_Fil = cited_Record.newFieldInGroup("cited_Record_Twrt_Fil", "TWRT-FIL", FieldType.STRING, 5);
        cited_Record_Twrt_Distribution_Type = cited_Record.newFieldInGroup("cited_Record_Twrt_Distribution_Type", "TWRT-DISTRIBUTION-TYPE", FieldType.STRING, 
            1);
        cited_Record_Twrt_Check_Reason = cited_Record.newFieldInGroup("cited_Record_Twrt_Check_Reason", "TWRT-CHECK-REASON", FieldType.STRING, 1);
        cited_Record_Twrt_Roth_Qual_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Roth_Qual_Ind", "TWRT-ROTH-QUAL-IND", FieldType.STRING, 1);
        cited_Record_Twrt_Roth_Death_Ind = cited_Record.newFieldInGroup("cited_Record_Twrt_Roth_Death_Ind", "TWRT-ROTH-DEATH-IND", FieldType.STRING, 1);
        cited_Record_Twrt_Filler = cited_Record.newFieldInGroup("cited_Record_Twrt_Filler", "TWRT-FILLER", FieldType.STRING, 8);
        cited_Record_Twrt_Error_Table = cited_Record.newFieldArrayInGroup("cited_Record_Twrt_Error_Table", "TWRT-ERROR-TABLE", FieldType.NUMERIC, 2, new 
            DbsArrayController(1,60));
        cited_Record_Twrt_Unique_Id = cited_Record.newFieldInGroup("cited_Record_Twrt_Unique_Id", "TWRT-UNIQUE-ID", FieldType.STRING, 15);

        this.setRecordName("LdaTwrl813a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl813a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
