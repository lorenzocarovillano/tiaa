/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:16:42 AM
**        * FROM NATURAL SUBPROGRAM : Twrn214e
************************************************************
**        * FILE NAME            : Twrn214e.java
**        * CLASS NAME           : Twrn214e
**        * INSTANCE NAME        : Twrn214e
************************************************************
************************************************************************
** PROGRAM:  TWRN214E - PARTICIPANT LEVEL REPORTING
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  SNEHA SINHA
**
** PURPOSE:  SUB PROGRAM TO CREATE 5498 FORM IN FLAT/CCP FILE FORMAT
**           FOR TAX REFACTOR PROJECT. CLONED FROM TWRN114E.
**
** HISTORY:
** -----------------------------------------------------------------
** 07-02-2018 INITIAL VERSION
** 01-29-2018 SK  - FIXING THE ADDRESS ISSUE FOR MULTIPLE 5498 FORMS
** 10-15-2020 JG  - 5498 CHANGES FOR SECURE-ACT AND BENE /* SECURE-ACT
** 11-10-2020 SK1 - TAX COMPLIANCE CHANGES FOR TAX YEAR 2020
** 01-05-2021 JG1 - TAX COMPLIANCE CHANGES FOR TAX YEAR 2020, FOR 5498
**                  TO SEGREGATE THE FORMS FOR 'Bene of' OR DECENDENTS
**                  FROM THE NORMAL.                  /* 5498 NEW FORM
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn214e extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra214e pdaTwra214e;
    private PdaPsta9610 pdaPsta9610;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaTwrl6345 ldaTwrl6345;
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaTwra0118 pdaTwra0118;
    private PdaTwratin pdaTwratin;
    private PdaPsta6346 pdaPsta6346;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaPsta9612 pdaPsta9612;
    private PdaTwra114f pdaTwra114f;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Coupon_Ind;
    private DbsField pnd_Link_Let_Type;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text;
    private DbsField pnd_Cnt;
    private DbsField pnd_Idx;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Key;

    private DbsGroup pnd_Read_Key__R_Field_1;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tax_Year;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Active_Ind;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tin;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Form_Type;

    private DataAccessProgramView vw_form_Upd;
    private DbsField form_Upd_Tirf_Part_Rpt_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Ind;
    private DbsField form_Upd_Tirf_Lu_User;
    private DbsField form_Upd_Tirf_Lu_Ts;
    private DbsField form_Upd_Tirf_Mobius_Stat;
    private DbsField form_Upd_Tirf_Mobius_Ind;
    private DbsField form_Upd_Tirf_Mobius_Stat_Date;

    private DataAccessProgramView vw_twrparti_Participant_File;
    private DbsField twrparti_Participant_File_Twrparti_Status;
    private DbsField twrparti_Participant_File_Twrparti_Tax_Id;
    private DbsField twrparti_Participant_File_Twrparti_Part_Eff_End_Dte;
    private DbsField twrparti_Participant_File_Twrparti_Test_Rec_Ind;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_2;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Byte5_Pstn6346;
    private DbsField pnd_Hold_Yyyy_A;

    private DbsGroup pnd_Hold_Yyyy_A__R_Field_3;
    private DbsField pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N;
    private DbsField pnd_Hold_Alpha_14;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Start;
    private DbsField pnd_End;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_M;
    private DbsField pnd_Coupon_Proc;
    private DbsField pnd_Coupons_Done;
    private DbsField pnd_Fatal_Err;
    private DbsField pnd_First_Write;
    private DbsField pnd_Reprint_No_Update;
    private DbsField pnd_Debug;
    private DbsField pnd_Cover_Letter_Ind;
    private DbsField pnd_New_5498_Form;
    private DbsField pnd_First_Time;
    private DbsField pnd_First_Move;

    private DbsGroup hold_Form_Data_1;

    private DbsGroup hold_Form_Data_1_Acct_Info_1;
    private DbsField hold_Form_Data_1_Hold_Decedent_Name;
    private DbsField hold_Form_Data_1_Hold_Participant_Name;
    private DbsField hold_Form_Data_1_Hold_Addr_Ln1;
    private DbsField hold_Form_Data_1_Hold_Addr_Ln2;
    private DbsField hold_Form_Data_1_Hold_Addr_Ln3;
    private DbsField hold_Form_Data_1_Hold_Addr_Ln4;
    private DbsField hold_Form_Data_1_Hold_Addr_Ln5;

    private DbsGroup hold_Form_Data;
    private DbsField hold_Form_Data_Hold_Form_Data_Array;

    private DbsGroup hold_Form_Data__R_Field_4;
    private DbsField hold_Form_Data_Form_Data_1;

    private DbsGroup hold_Form_Data__R_Field_5;

    private DbsGroup hold_Form_Data_F5498p_Form_Array_1;

    private DbsGroup hold_Form_Data_Acct_Info;
    private DbsField hold_Form_Data_Coup_Primary_Contr_Nbr;
    private DbsField hold_Form_Data_Coup_Secondary_Contr_Nbr;
    private DbsField hold_Form_Data_Coup_Ira_Type_Ind;
    private DbsField pnd_Coupon_Type_1_2;

    private DbsGroup pnd_Coupon_Type_1_2__R_Field_6;
    private DbsField pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1;
    private DbsField pnd_Coupon_Type_1_2_Pnd_Coupon_Type_2;
    private DbsField pnd_Mobindx;

    private DbsGroup pnd_Mobindx__R_Field_7;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind;
    private DbsField pnd_Mobindx_Pnd_Wk_Mobindx;
    private DbsField pnd_Tirf_Tin;

    private DbsGroup pnd_Tirf_Tin__R_Field_8;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N9;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N1;
    private DbsField pnd_Tirf_Tax_Year1;

    private DbsGroup pnd_Tirf_Tax_Year1__R_Field_9;
    private DbsField pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Cc;
    private DbsField pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Yy;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaTwrl6345 = new LdaTwrl6345();
        registerRecord(ldaTwrl6345);
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        pdaTwra0118 = new PdaTwra0118(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaPsta6346 = new PdaPsta6346(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaTwra114f = new PdaTwra114f(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaTwra214e = new PdaTwra214e(parameters);
        pdaPsta9610 = new PdaPsta9610(parameters);
        pdaTwra0214 = new PdaTwra0214(parameters);
        pnd_Test_Ind = parameters.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Test_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Coupon_Ind = parameters.newFieldInRecord("pnd_Coupon_Ind", "#COUPON-IND", FieldType.STRING, 1);
        pnd_Coupon_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Link_Let_Type = parameters.newFieldArrayInRecord("pnd_Link_Let_Type", "#LINK-LET-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Link_Let_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Mobius_Psta9960_Parm_Data = localVariables.newGroupInRecord("pnd_Mobius_Psta9960_Parm_Data", "#MOBIUS-PSTA9960-PARM-DATA");
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id", 
            "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count", "#DTLS-COUNT", 
            FieldType.PACKED_DECIMAL, 5);

        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array = pnd_Mobius_Psta9960_Parm_Data.newGroupArrayInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array", 
            "#DTLS-ARRAY", new DbsArrayController(1, 100));
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde", 
            "#DCMNT-CDE", FieldType.STRING, 8);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde", 
            "#DCMNT-VRSN-CDE", FieldType.STRING, 2);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind", 
            "#INDEX-IND", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert", 
            "#MAN-INSERT", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text", 
            "#FREE-TEXT", FieldType.STRING, 40);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Read_Key = localVariables.newFieldInRecord("pnd_Read_Key", "#READ-KEY", FieldType.STRING, 32);

        pnd_Read_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Key__R_Field_1", "REDEFINE", pnd_Read_Key);
        pnd_Read_Key_Pnd_Read_Key_Tax_Year = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tax_Year", "#READ-KEY-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Read_Key_Pnd_Read_Key_Active_Ind = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Active_Ind", "#READ-KEY-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Read_Key_Pnd_Read_Key_Tin = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tin", "#READ-KEY-TIN", FieldType.STRING, 10);
        pnd_Read_Key_Pnd_Read_Key_Form_Type = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Form_Type", "#READ-KEY-FORM-TYPE", FieldType.NUMERIC, 
            2);

        vw_form_Upd = new DataAccessProgramView(new NameInfo("vw_form_Upd", "FORM-UPD"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Upd_Tirf_Part_Rpt_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Date", "TIRF-PART-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_DATE");
        form_Upd_Tirf_Part_Rpt_Date.setDdmHeader("PART/RPT/DATE");
        form_Upd_Tirf_Part_Rpt_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Upd_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Upd_Tirf_Lu_User = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Upd_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Upd_Tirf_Lu_Ts = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_LU_TS");
        form_Upd_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Upd_Tirf_Mobius_Stat = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat", "TIRF-MOBIUS-STAT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_STAT");
        form_Upd_Tirf_Mobius_Stat.setDdmHeader("MOBIUS/STATUS");
        form_Upd_Tirf_Mobius_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Upd_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Upd_Tirf_Mobius_Stat_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat_Date", "TIRF-MOBIUS-STAT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIRF_MOBIUS_STAT_DATE");
        form_Upd_Tirf_Mobius_Stat_Date.setDdmHeader("MOBIUS/STATUS/DATE");
        registerRecord(vw_form_Upd);

        vw_twrparti_Participant_File = new DataAccessProgramView(new NameInfo("vw_twrparti_Participant_File", "TWRPARTI-PARTICIPANT-FILE"), "TWRPARTI_PARTICIPANT_FILE", 
            "TWR_PARTICIPANT");
        twrparti_Participant_File_Twrparti_Status = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Status", 
            "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Participant_File_Twrparti_Tax_Id = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Tax_Id", 
            "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Participant_File_Twrparti_Part_Eff_End_Dte = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Part_Eff_End_Dte", 
            "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        twrparti_Participant_File_Twrparti_Test_Rec_Ind = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Test_Rec_Ind", 
            "TWRPARTI-TEST-REC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_TEST_REC_IND");
        registerRecord(vw_twrparti_Participant_File);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_2", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id", 
            "#TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status", 
            "#TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte", 
            "#TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Byte5_Pstn6346 = localVariables.newFieldInRecord("pnd_Byte5_Pstn6346", "#BYTE5-PSTN6346", FieldType.STRING, 1);
        pnd_Hold_Yyyy_A = localVariables.newFieldInRecord("pnd_Hold_Yyyy_A", "#HOLD-YYYY-A", FieldType.STRING, 4);

        pnd_Hold_Yyyy_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Hold_Yyyy_A__R_Field_3", "REDEFINE", pnd_Hold_Yyyy_A);
        pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N = pnd_Hold_Yyyy_A__R_Field_3.newFieldInGroup("pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N", "#HOLD-YYYY-N", FieldType.NUMERIC, 
            4);
        pnd_Hold_Alpha_14 = localVariables.newFieldInRecord("pnd_Hold_Alpha_14", "#HOLD-ALPHA-14", FieldType.STRING, 14);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.PACKED_DECIMAL, 3);
        pnd_End = localVariables.newFieldInRecord("pnd_End", "#END", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_Coupon_Proc = localVariables.newFieldInRecord("pnd_Coupon_Proc", "#COUPON-PROC", FieldType.BOOLEAN, 1);
        pnd_Coupons_Done = localVariables.newFieldInRecord("pnd_Coupons_Done", "#COUPONS-DONE", FieldType.BOOLEAN, 1);
        pnd_Fatal_Err = localVariables.newFieldInRecord("pnd_Fatal_Err", "#FATAL-ERR", FieldType.BOOLEAN, 1);
        pnd_First_Write = localVariables.newFieldInRecord("pnd_First_Write", "#FIRST-WRITE", FieldType.BOOLEAN, 1);
        pnd_Reprint_No_Update = localVariables.newFieldInRecord("pnd_Reprint_No_Update", "#REPRINT-NO-UPDATE", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Cover_Letter_Ind = localVariables.newFieldInRecord("pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", FieldType.BOOLEAN, 1);
        pnd_New_5498_Form = localVariables.newFieldInRecord("pnd_New_5498_Form", "#NEW-5498-FORM", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_First_Move = localVariables.newFieldInRecord("pnd_First_Move", "#FIRST-MOVE", FieldType.BOOLEAN, 1);

        hold_Form_Data_1 = localVariables.newGroupInRecord("hold_Form_Data_1", "HOLD-FORM-DATA-1");

        hold_Form_Data_1_Acct_Info_1 = hold_Form_Data_1.newGroupArrayInGroup("hold_Form_Data_1_Acct_Info_1", "ACCT-INFO-1", new DbsArrayController(1, 
            8));
        hold_Form_Data_1_Hold_Decedent_Name = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Decedent_Name", "HOLD-DECEDENT-NAME", 
            FieldType.STRING, 35);
        hold_Form_Data_1_Hold_Participant_Name = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Participant_Name", "HOLD-PARTICIPANT-NAME", 
            FieldType.STRING, 35);
        hold_Form_Data_1_Hold_Addr_Ln1 = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Addr_Ln1", "HOLD-ADDR-LN1", FieldType.STRING, 
            35);
        hold_Form_Data_1_Hold_Addr_Ln2 = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Addr_Ln2", "HOLD-ADDR-LN2", FieldType.STRING, 
            35);
        hold_Form_Data_1_Hold_Addr_Ln3 = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Addr_Ln3", "HOLD-ADDR-LN3", FieldType.STRING, 
            35);
        hold_Form_Data_1_Hold_Addr_Ln4 = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Addr_Ln4", "HOLD-ADDR-LN4", FieldType.STRING, 
            35);
        hold_Form_Data_1_Hold_Addr_Ln5 = hold_Form_Data_1_Acct_Info_1.newFieldInGroup("hold_Form_Data_1_Hold_Addr_Ln5", "HOLD-ADDR-LN5", FieldType.STRING, 
            35);

        hold_Form_Data = localVariables.newGroupInRecord("hold_Form_Data", "HOLD-FORM-DATA");
        hold_Form_Data_Hold_Form_Data_Array = hold_Form_Data.newFieldArrayInGroup("hold_Form_Data_Hold_Form_Data_Array", "HOLD-FORM-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1, 496));

        hold_Form_Data__R_Field_4 = hold_Form_Data.newGroupInGroup("hold_Form_Data__R_Field_4", "REDEFINE", hold_Form_Data_Hold_Form_Data_Array);
        hold_Form_Data_Form_Data_1 = hold_Form_Data__R_Field_4.newFieldArrayInGroup("hold_Form_Data_Form_Data_1", "FORM-DATA-1", FieldType.STRING, 1, 
            new DbsArrayController(1, 496));

        hold_Form_Data__R_Field_5 = hold_Form_Data__R_Field_4.newGroupInGroup("hold_Form_Data__R_Field_5", "REDEFINE", hold_Form_Data_Form_Data_1);

        hold_Form_Data_F5498p_Form_Array_1 = hold_Form_Data__R_Field_5.newGroupArrayInGroup("hold_Form_Data_F5498p_Form_Array_1", "F5498P-FORM-ARRAY-1", 
            new DbsArrayController(1, 2));

        hold_Form_Data_Acct_Info = hold_Form_Data_F5498p_Form_Array_1.newGroupArrayInGroup("hold_Form_Data_Acct_Info", "ACCT-INFO", new DbsArrayController(1, 
            8));
        hold_Form_Data_Coup_Primary_Contr_Nbr = hold_Form_Data_Acct_Info.newFieldInGroup("hold_Form_Data_Coup_Primary_Contr_Nbr", "COUP-PRIMARY-CONTR-NBR", 
            FieldType.STRING, 15);
        hold_Form_Data_Coup_Secondary_Contr_Nbr = hold_Form_Data_Acct_Info.newFieldInGroup("hold_Form_Data_Coup_Secondary_Contr_Nbr", "COUP-SECONDARY-CONTR-NBR", 
            FieldType.STRING, 15);
        hold_Form_Data_Coup_Ira_Type_Ind = hold_Form_Data_Acct_Info.newFieldInGroup("hold_Form_Data_Coup_Ira_Type_Ind", "COUP-IRA-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Coupon_Type_1_2 = localVariables.newFieldInRecord("pnd_Coupon_Type_1_2", "#COUPON-TYPE-1-2", FieldType.STRING, 2);

        pnd_Coupon_Type_1_2__R_Field_6 = localVariables.newGroupInRecord("pnd_Coupon_Type_1_2__R_Field_6", "REDEFINE", pnd_Coupon_Type_1_2);
        pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1 = pnd_Coupon_Type_1_2__R_Field_6.newFieldInGroup("pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1", "#COUPON-TYPE-1", 
            FieldType.STRING, 1);
        pnd_Coupon_Type_1_2_Pnd_Coupon_Type_2 = pnd_Coupon_Type_1_2__R_Field_6.newFieldInGroup("pnd_Coupon_Type_1_2_Pnd_Coupon_Type_2", "#COUPON-TYPE-2", 
            FieldType.STRING, 1);
        pnd_Mobindx = localVariables.newFieldInRecord("pnd_Mobindx", "#MOBINDX", FieldType.STRING, 9);

        pnd_Mobindx__R_Field_7 = localVariables.newGroupInRecord("pnd_Mobindx__R_Field_7", "REDEFINE", pnd_Mobindx);
        pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator = pnd_Mobindx__R_Field_7.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator", "#WK-MMM-TEST-INDICATOR", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind = pnd_Mobindx__R_Field_7.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind", "#WK-MMM-5498-COUPON-IND", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mobindx = pnd_Mobindx__R_Field_7.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mobindx", "#WK-MOBINDX", FieldType.STRING, 7);
        pnd_Tirf_Tin = localVariables.newFieldInRecord("pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);

        pnd_Tirf_Tin__R_Field_8 = localVariables.newGroupInRecord("pnd_Tirf_Tin__R_Field_8", "REDEFINE", pnd_Tirf_Tin);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N9 = pnd_Tirf_Tin__R_Field_8.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N9", "#TIRF-TIN-N9", FieldType.NUMERIC, 9);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N1 = pnd_Tirf_Tin__R_Field_8.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N1", "#TIRF-TIN-N1", FieldType.NUMERIC, 1);
        pnd_Tirf_Tax_Year1 = localVariables.newFieldInRecord("pnd_Tirf_Tax_Year1", "#TIRF-TAX-YEAR1", FieldType.STRING, 4);

        pnd_Tirf_Tax_Year1__R_Field_9 = localVariables.newGroupInRecord("pnd_Tirf_Tax_Year1__R_Field_9", "REDEFINE", pnd_Tirf_Tax_Year1);
        pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Cc = pnd_Tirf_Tax_Year1__R_Field_9.newFieldInGroup("pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Cc", "#TIRF-TAX-YEAR-CC", 
            FieldType.STRING, 2);
        pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Yy = pnd_Tirf_Tax_Year1__R_Field_9.newFieldInGroup("pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Yy", "#TIRF-TAX-YEAR-YY", 
            FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_Upd.reset();
        vw_twrparti_Participant_File.reset();

        ldaTwrl6345.initializeValues();
        ldaTwrl9710.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Coupon_Proc.setInitialValue(false);
        pnd_Coupons_Done.setInitialValue(false);
        pnd_First_Write.setInitialValue(true);
        pnd_Debug.setInitialValue(false);
        pnd_Cover_Letter_Ind.setInitialValue(false);
        pnd_New_5498_Form.setInitialValue(false);
        pnd_First_Time.setInitialValue(true);
        pnd_First_Move.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn214e() throws Exception
    {
        super("Twrn214e");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *************************************
        //* ** MAIN PROGRAM LOGIC BEGINS HERE ***
        //* *************************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: FORMAT PS = 80 LS = 130;//Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"- STARTING");                                                                                                      //Natural: WRITE *PROGRAM '- STARTING'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Yyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("YYYY"));                                                                                      //Natural: MOVE EDITED *DATX ( EM = YYYY ) TO #HOLD-YYYY-A
        pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N.nsubtract(1);                                                                                                                     //Natural: ASSIGN #HOLD-YYYY-N := #HOLD-YYYY-N - 1
        //*  FO
        if (condition(pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N.less(pdaTwra214e.getPnd_Twra214e_Pnd_Tax_Year())))                                                                  //Natural: IF #HOLD-YYYY-N < #TWRA214E.#TAX-YEAR
        {
            pnd_Hold_Yyyy_A_Pnd_Hold_Yyyy_N.setValue(pdaTwra214e.getPnd_Twra214e_Pnd_Tax_Year());                                                                         //Natural: ASSIGN #HOLD-YYYY-N := #TWRA214E.#TAX-YEAR
            //*  CALL COUPON ROUTINE
            //*  ROC - DST - 11/2014
            //*  5498 NEW FORM
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra0118.getPnd_Twra0118_Pnd_Tax_Year().setValue(pnd_Hold_Yyyy_A);                                                                                             //Natural: ASSIGN #TWRA0118.#TAX-YEAR := #HOLD-YYYY-A
        pdaTwra0118.getPnd_Twra0118_Pnd_Tin().setValue(pdaTwra214e.getPnd_Twra214e_Pnd_Tin());                                                                            //Natural: ASSIGN #TWRA0118.#TIN := #TWRA214E.#TIN
        pdaTwra0118.getPnd_Twra0118_Pnd_Debug_Ind().setValue(pnd_Debug.getBoolean());                                                                                     //Natural: ASSIGN #TWRA0118.#DEBUG-IND := #DEBUG
        pnd_Coupon_Proc.setValue(false);                                                                                                                                  //Natural: ASSIGN #COUPON-PROC := FALSE
        pnd_First_Time.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-TIME := TRUE
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0118.getPnd_Twra0118_Pnd_Tax_Year());                                                                    //Natural: WRITE *PROGRAM '=' #TWRA0118.#TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_Mobius_Index().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                            //Natural: ASSIGN TAX-FORM-DATA.MOBIUS-INDEX := #TWRA0214-MOBIUS-KEY
        //*  << DUTTAD2
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF TIRF-TAX-YEAR GE '2016'
        {
            //*  MANSOOR
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                                    //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION #COVER-LETTER-IND
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
            pnd_Cover_Letter_Ind.setValue(true);
            //*  MANSOOR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SINHSN
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("M")))                                                              //Natural: IF #TWRA0214-MOBIUS-MIGR-IND = 'M'
            {
                //*    TAX-FORM-DATA.MOBINDX:= '<MOBINDX>'
                ldaTwrl6345.getTax_Form_Data_Mobindx().setValue(pnd_Mobindx);                                                                                             //Natural: ASSIGN TAX-FORM-DATA.MOBINDX := #MOBINDX
                //*  FO 2/03
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                                //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION #COVER-LETTER-IND
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
                pnd_Cover_Letter_Ind.setValue(true);
            }                                                                                                                                                             //Natural: END-IF
            //*  DUTTAD2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Read_Key_Pnd_Read_Key_Tax_Year.setValue(pdaTwra214e.getPnd_Twra214e_Pnd_Tax_Year());                                                                          //Natural: ASSIGN #READ-KEY-TAX-YEAR := #TWRA214E.#TAX-YEAR
        pnd_Read_Key_Pnd_Read_Key_Active_Ind.setValue("A");                                                                                                               //Natural: ASSIGN #READ-KEY-ACTIVE-IND := 'A'
        pnd_Read_Key_Pnd_Read_Key_Tin.setValue(pdaTwra214e.getPnd_Twra214e_Pnd_Tin());                                                                                    //Natural: ASSIGN #READ-KEY-TIN := #TWRA214E.#TIN
        pnd_Read_Key_Pnd_Read_Key_Form_Type.setValue(pdaTwra214e.getPnd_Twra214e_Pnd_Form_Type());                                                                        //Natural: ASSIGN #READ-KEY-FORM-TYPE := #TWRA214E.#FORM-TYPE
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"FORM-FILE KEY IS: ",pnd_Read_Key);                                                                                 //Natural: WRITE *PROGRAM 'FORM-FILE KEY IS: ' #READ-KEY
            if (Global.isEscape()) return;
            //*  SINHSN
        }                                                                                                                                                                 //Natural: END-IF
        //*  RD1. READ FORM-FILE BY TIRF-SUPERDE-2
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY TIRF-SUPERDE-2 STARTING FROM #READ-KEY
        (
        "RD1",
        new Wc[] { new Wc("TIRF_SUPERDE_2", ">=", pnd_Read_Key, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_2", "ASC") }
        );
        RD1:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD1")))
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Read_Key_Pnd_Read_Key_Tax_Year) || ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Read_Key_Pnd_Read_Key_Active_Ind)  //Natural: IF TIRF-TAX-YEAR NE #READ-KEY-TAX-YEAR OR TIRF-ACTIVE-IND NE #READ-KEY-ACTIVE-IND OR TIRF-TIN NE #READ-KEY-TIN OR TIRF-FORM-TYPE NE #READ-KEY-FORM-TYPE
                || ldaTwrl9710.getForm_Tirf_Tin().notEquals(pnd_Read_Key_Pnd_Read_Key_Tin) || ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Read_Key_Pnd_Read_Key_Form_Type)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *                                           DUTTAD 010515 >>
            pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                             //Natural: RESET #TWRPARTI-CURR-REC-SD #BYTE5-PSTN6346
            pnd_Byte5_Pstn6346.reset();
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                        //Natural: MOVE FORM.TIRF-TIN TO #TWRPARTI-TAX-ID
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #TWRPARTI-STATUS
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                  //Natural: MOVE '99999999' TO #TWRPARTI-PART-EFF-END-DTE
            vw_twrparti_Participant_File.startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) TWRPARTI-PARTICIPANT-FILE TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
            (
            "F1",
            new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", "=", pnd_Twrparti_Curr_Rec_Sd, WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_twrparti_Participant_File.readNextRow("F1", true)))
            {
                vw_twrparti_Participant_File.setIfNotFoundControlFlag(false);
                if (condition(vw_twrparti_Participant_File.getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO PARTICIPANT FOUND FOR TIN:",ldaTwrl9710.getForm_Tirf_Tin());                                                                //Natural: WRITE 'NO PARTICIPANT FOUND FOR TIN:' FORM.TIRF-TIN
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  DUTTAD 010515
                pnd_Test_Ind.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                                                   //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #TEST-IND
                //*   /* SNEHA CHANGES STARTED - SINHSN
                pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                          //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #WK-MMM-TEST-INDICATOR
                if (condition(pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.equals("Y")))                                                                                         //Natural: IF #WK-MMM-TEST-INDICATOR EQ 'Y'
                {
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue("Y");                                                                                                  //Natural: MOVE 'Y' TO #WK-MMM-TEST-INDICATOR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #WK-MMM-TEST-INDICATOR
                }                                                                                                                                                         //Natural: END-IF
                //*  /* SNEHA CHANGES ENDED - SINHSN
                if (condition(twrparti_Participant_File_Twrparti_Test_Rec_Ind.equals("Y")))                                                                               //Natural: IF TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND EQ 'Y'
                {
                    pnd_Byte5_Pstn6346.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #BYTE5-PSTN6346
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *                                           DUTTAD 010515 <<
            if (condition(! (pnd_Coupon_Proc.getBoolean())))                                                                                                              //Natural: IF NOT #COUPON-PROC
            {
                                                                                                                                                                          //Natural: PERFORM COUPON-PROCESSING
                sub_Coupon_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Coupon_Proc.setValue(true);                                                                                                                           //Natural: ASSIGN #COUPON-PROC := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Coupon_Type_1_2.setValue(pdaTwra0118.getPnd_Twra0118_Pnd_Coupon_Type());                                                                                  //Natural: ASSIGN #COUPON-TYPE-1-2 := #TWRA0118.#COUPON-TYPE
            pnd_Coupon_Type_1_2_Pnd_Coupon_Type_2.reset();                                                                                                                //Natural: RESET #COUPON-TYPE-2
            if (condition(((pdaTwra0118.getPnd_Twra0118_Pnd_Coupon_Type().equals("1A") || pdaTwra0118.getPnd_Twra0118_Pnd_Coupon_Type().equals("1B"))                     //Natural: IF ( #TWRA0118.#COUPON-TYPE = '1A' OR = '1B' ) AND ( TIRF-CONTRACT-NBR = #TWRA0118.#PRIMARY ( * ) OR TIRF-CONTRACT-NBR = #TWRA0118.#SECONDARY ( * ) )
                && ((((((((((((((((((((((((((((((((((((((((((((((((((ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(1)) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(2))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(3))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(4))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(5))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(6))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(7))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(8))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(9))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(10))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(11))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(12))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(13))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(14))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(15))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(16))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(17))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(18))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(19))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(20))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(21))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(22))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(23))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(24))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(25))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(26))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(27))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(28))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(29))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(30))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(31))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(32))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(33))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(34))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(35))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(36))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(37))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(38))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(39))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(40))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(41))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(42))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(43))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(44))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(45))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(46))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(47))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(48))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(49))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(50))) || (((((((((((((((((((((((((((((((((((((((((((((((((ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(1)) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(2))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(3))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(4))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(5))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(6))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(7))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(8))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(9))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(10))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(11))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(12))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(13))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(14))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(15))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(16))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(17))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(18))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(19))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(20))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(21))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(22))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(23))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(24))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(25))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(26))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(27))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(28))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(29))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(30))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(31))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(32))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(33))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(34))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(35))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(36))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(37))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(38))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(39))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(40))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(41))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(42))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(43))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(44))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(45))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(46))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(47))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(48))) || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(49))) 
                || ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(50)))))))
            {
                pnd_Coupon_Type_1_2_Pnd_Coupon_Type_2.setValue("Y");                                                                                                      //Natural: MOVE 'Y' TO #COUPON-TYPE-2
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1732 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #COUPON-TYPE-1;//Natural: VALUE '1'
            if (condition((pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.equals("1"))))
            {
                decideConditionsMet1732++;
                pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.setValue("A");                                                                                                      //Natural: MOVE 'A' TO #COUPON-TYPE-1
                //*  DUTTAD 010515
                pnd_Coupon_Ind.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #COUPON-IND
                //*  SINHSN
                pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #WK-MMM-5498-COUPON-IND
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.equals("2"))))
            {
                decideConditionsMet1732++;
                pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.setValue("B");                                                                                                      //Natural: MOVE 'B' TO #COUPON-TYPE-1
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.equals("3"))))
            {
                decideConditionsMet1732++;
                pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.setValue("C");                                                                                                      //Natural: MOVE 'C' TO #COUPON-TYPE-1
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.equals("4"))))
            {
                decideConditionsMet1732++;
                pnd_Coupon_Type_1_2_Pnd_Coupon_Type_1.setValue("D");                                                                                                      //Natural: MOVE 'D' TO #COUPON-TYPE-1
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  COMPANY LOOKUP
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD1"));                                                                                                  //Natural: ASSIGN #ISN := *ISN ( RD1. )
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM.TIRF-COMPANY-CDE
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),ldaTwrl9710.getForm_Tirf_Tax_Year());                                    //Natural: MOVE EDITED FORM.TIRF-TAX-YEAR TO #TWRACOMP.#TAX-YEAR ( EM = 9999 )
            DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp());                                                                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP
            if (condition(Global.isEscape())) return;
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ASSIGN #CNT := #CNT + 1
            pnd_Idx.nadd(1);                                                                                                                                              //Natural: ASSIGN #IDX := #IDX + 1
                                                                                                                                                                          //Natural: PERFORM FORM-5498
            sub_Form_5498();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Reprint_No_Update.reset();                                                                                                                                //Natural: RESET #REPRINT-NO-UPDATE
            //*  NOT PRINTED
            //*  MOORE
            short decideConditionsMet1763 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-PART-RPT-IND;//Natural: VALUE ' ', 'N'
            if (condition((ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals(" ") || ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals("N"))))
            {
                decideConditionsMet1763++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals("M"))))
            {
                decideConditionsMet1763++;
                if (condition(ldaTwrl9710.getForm_Tirf_Moore_Hold_Ind().equals(" ")))                                                                                     //Natural: IF FORM.TIRF-MOORE-HOLD-IND = ' '
                {
                    pnd_Reprint_No_Update.setValue(true);                                                                                                                 //Natural: ASSIGN #REPRINT-NO-UPDATE := TRUE
                    //*  OTHER PRINT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Reprint_No_Update.setValue(true);                                                                                                                     //Natural: ASSIGN #REPRINT-NO-UPDATE := TRUE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  NOT MOBIUS MASS MIGRATION
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().notEquals("M")))                                                           //Natural: IF #TWRA0214-MOBIUS-MIGR-IND NE 'M'
            {
                GT1:                                                                                                                                                      //Natural: GET FORM-UPD #ISN
                vw_form_Upd.readByID(pnd_Isn.getLong(), "GT1");
                if (condition(! (pnd_Reprint_No_Update.getBoolean())))                                                                                                    //Natural: IF NOT #REPRINT-NO-UPDATE
                {
                    form_Upd_Tirf_Part_Rpt_Date.setValue(Global.getDATX());                                                                                               //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-DATE = *DATX
                    form_Upd_Tirf_Part_Rpt_Ind.setValue("P");                                                                                                             //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-IND = 'P'
                    form_Upd_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN FORM-UPD.TIRF-LU-USER = *INIT-USER
                    form_Upd_Tirf_Lu_Ts.setValue(Global.getDATX());                                                                                                       //Natural: ASSIGN FORM-UPD.TIRF-LU-TS = *DATX
                }                                                                                                                                                         //Natural: END-IF
                if (condition((((pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("P") || pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals(" "))  //Natural: IF ( #TWRA0214-MOBIUS-MIGR-IND = 'P' OR = ' ' ) AND #TWRA0214-HARD-COPY NE 'Y' AND #PCKGE-IMMDTE-PRNT-IND NE 'Y'
                    && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().notEquals("Y")) && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind().notEquals("Y"))))
                {
                    //*      IF FORM.TIRF-PIN = 0 OR = 9999999      /* PIN-NBR EXPANSION
                    //*  PIN-NBR EXPANSION
                    if (condition(ldaTwrl9710.getForm_Tirf_Pin().equals(getZero()) || ldaTwrl9710.getForm_Tirf_Pin().equals(new DbsDecimal("999999999999"))))             //Natural: IF FORM.TIRF-PIN = 0 OR = 999999999999
                    {
                        form_Upd_Tirf_Mobius_Stat.setValue("B ");                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT = 'B '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        form_Upd_Tirf_Mobius_Stat.setValue("A2");                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT = 'A2'
                    }                                                                                                                                                     //Natural: END-IF
                    form_Upd_Tirf_Mobius_Stat_Date.setValue(Global.getDATX());                                                                                            //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT-DATE = *DATX
                    form_Upd_Tirf_Mobius_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-IND = ' '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"*** UPDATE ***");                                                                                          //Natural: WRITE *PROGRAM '*** UPDATE ***'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Stat);                                                                             //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-STAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Stat_Date);                                                                        //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-STAT-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Ind);                                                                              //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                vw_form_Upd.updateDBRow("GT1");                                                                                                                           //Natural: UPDATE ( GT1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  PERFORM CALL-POST-WRITE              /* SINHSN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM AFTER-5498-READ
        sub_After_5498_Read();
        if (condition(Global.isEscape())) {return;}
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-5498
        //*  TAX-FORM-DATA.FORM-YEAR       := '02'
        //*   TAX-FORM-DATA.COUPON-TYPE(1)  := #TWRA0118.#COUPON-TYPE
        //* *IF FORM.TIRF-DECEDENT-NAME NE ' '
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ALPHA-14
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COUPON-PROCESS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AFTER-5498-READ
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-HEADER
        //*  CALL POST OPEN
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-WRITE
        //*  CALL POST WRITE
        //*  CALL POST CLOSE
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COUPON-PROCESSING
        //* **
    }
    private void sub_Form_5498() throws Exception                                                                                                                         //Natural: FORM-5498
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM()," SUBROUTINE FORM-5498");                                                                                           //Natural: WRITE *PROGRAM ' SUBROUTINE FORM-5498'
            if (Global.isEscape()) return;
            //*  SINHSN
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tirf_Tax_Year1.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                                 //Natural: ASSIGN #TIRF-TAX-YEAR1 := TIRF-TAX-YEAR
        if (condition(pnd_Cnt.equals(1)))                                                                                                                                 //Natural: IF #CNT = 1
        {
            ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_5498_P());                                                        //Natural: ASSIGN TAX-FORM-DATA.FORM-TYPE := #FORM-5498-P
            ldaTwrl6345.getTax_Form_Data_Form_Year().setValue(pnd_Tirf_Tax_Year1_Pnd_Tirf_Tax_Year_Yy);                                                                   //Natural: ASSIGN TAX-FORM-DATA.FORM-YEAR := #TIRF-TAX-YEAR1.#TIRF-TAX-YEAR-YY
            ldaTwrl6345.getTax_Form_Data_Number_Of_Forms().setValue(1);                                                                                                   //Natural: ASSIGN TAX-FORM-DATA.NUMBER-OF-FORMS := 1
            ldaTwrl6345.getTax_Form_Data_F5498p_Tru().getValue(1,1).setValue(" ");                                                                                        //Natural: ASSIGN TAX-FORM-DATA.F5498P-TRU ( 1,1 ) := ' '
            ldaTwrl6345.getTax_Form_Data_F5498p_Truid().getValue(1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id());                                                   //Natural: ASSIGN TAX-FORM-DATA.F5498P-TRUID ( 1 ) := #TWRACOMP.#FED-ID
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
            DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin());                                                                           //Natural: CALLNAT 'TWRNTIN' USING TWRATIN
            if (condition(Global.isEscape())) return;
            ldaTwrl6345.getTax_Form_Data_F5498p_Pssn().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                           //Natural: ASSIGN TAX-FORM-DATA.F5498P-PSSN ( 1 ) := TWRATIN.#O-TIN
            //*  TAX-FORM-DATA.F5498P-PAY(1,1) := FORM.TIRF-PARTICIPANT-NAME
            //*                                                         /* SECURE-ACT
            if (condition(ldaTwrl9710.getForm_Tirf_Decedent_Name().notEquals(" ")))                                                                                       //Natural: IF FORM.TIRF-DECEDENT-NAME NE ' '
            {
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Participant_Name(),  //Natural: COMPRESS FORM.TIRF-PARTICIPANT-NAME ' as bene of ' INTO TAX-FORM-DATA.F5498P-PAY ( 1,1 ) LEAVING NO
                    " as bene of "));
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Decedent_Name());                                               //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := FORM.TIRF-DECEDENT-NAME
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := FORM.TIRF-ADDR-LN1
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := FORM.TIRF-ADDR-LN2
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := FORM.TIRF-ADDR-LN3
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Addr_Ln4(),     //Natural: COMPRESS FORM.TIRF-ADDR-LN4 FORM.TIRF-ADDR-LN5 INTO TAX-FORM-DATA.F5498P-PAY ( 1,6 ) LEAVING NO
                    ldaTwrl9710.getForm_Tirf_Addr_Ln5()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                            //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := FORM.TIRF-ADDR-LN1
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := FORM.TIRF-ADDR-LN2
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := FORM.TIRF-ADDR-LN3
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := FORM.TIRF-ADDR-LN4
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,6 ) := FORM.TIRF-ADDR-LN5
                //*  SECURE-ACT
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl6345.getTax_Form_Data_Coupon_Type().getValue(1).setValue(pnd_Coupon_Type_1_2);                                                                         //Natural: ASSIGN TAX-FORM-DATA.COUPON-TYPE ( 1 ) := #COUPON-TYPE-1-2
            ldaTwrl6345.getTax_Form_Data_Coupon_Year().getValue(1).setValue(pdaTwra0118.getPnd_Twra0118_Pnd_Tax_Year());                                                  //Natural: ASSIGN TAX-FORM-DATA.COUPON-YEAR ( 1 ) := #TWRA0118.#TAX-YEAR
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),"FORM DATA (ALL CONTRACTS): ",NEWLINE,NEWLINE,new ColumnSpacing(2),"FORM-TYPE       :",ldaTwrl6345.getTax_Form_Data_Form_Type(),NEWLINE,new  //Natural: WRITE *PROGRAM 'FORM DATA (ALL CONTRACTS): '/ / 2X 'FORM-TYPE       :' TAX-FORM-DATA.FORM-TYPE / 2X 'FORM-YEAR       :' TAX-FORM-DATA.FORM-YEAR / 2X 'NUMBER-OF-FORMS :' TAX-FORM-DATA.NUMBER-OF-FORMS / 2X 'F5498P-TRU(1,1) :' TAX-FORM-DATA.F5498P-TRU ( 1,1 ) / 2X 'F5498P-TRUID(1) :' TAX-FORM-DATA.F5498P-TRUID ( 1 ) / 2X 'F5498P-PSSN(1)  :' TAX-FORM-DATA.F5498P-PSSN ( 1 ) / 2X 'F5498P-PAY(1,1) :' TAX-FORM-DATA.F5498P-PAY ( 1,1 ) / 2X 'F5498P-PAY(1,2) :' TAX-FORM-DATA.F5498P-PAY ( 1,2 ) / 2X 'F5498P-PAY(1,3) :' TAX-FORM-DATA.F5498P-PAY ( 1,3 ) / 2X 'F5498P-PAY(1,4) :' TAX-FORM-DATA.F5498P-PAY ( 1,4 ) / 2X 'F5498P-PAY(1,5) :' TAX-FORM-DATA.F5498P-PAY ( 1,5 ) / 2X 'F5498P-PAY(1,6) :' TAX-FORM-DATA.F5498P-PAY ( 1,6 ) / 2X 'COUPON-TYPE(1)  :' TAX-FORM-DATA.COUPON-TYPE ( 1 ) / 2X 'COUPON-YEAR(1)  :' TAX-FORM-DATA.COUPON-YEAR ( 1 ) / 2X 'IRA Type Ind    :' TAX-FORM-DATA.IRA-TYPE-IND ( 1,7 ) / 2X 'SEP Contrib     :' TAX-FORM-DATA.SEP-CONTRIB ( 1,7 )
                    ColumnSpacing(2),"FORM-YEAR       :",ldaTwrl6345.getTax_Form_Data_Form_Year(),NEWLINE,new ColumnSpacing(2),"NUMBER-OF-FORMS :",ldaTwrl6345.getTax_Form_Data_Number_Of_Forms(),NEWLINE,new 
                    ColumnSpacing(2),"F5498P-TRU(1,1) :",ldaTwrl6345.getTax_Form_Data_F5498p_Tru().getValue(1,1),NEWLINE,new ColumnSpacing(2),"F5498P-TRUID(1) :",ldaTwrl6345.getTax_Form_Data_F5498p_Truid().getValue(1),NEWLINE,new 
                    ColumnSpacing(2),"F5498P-PSSN(1)  :",ldaTwrl6345.getTax_Form_Data_F5498p_Pssn().getValue(1),NEWLINE,new ColumnSpacing(2),"F5498P-PAY(1,1) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1),NEWLINE,new 
                    ColumnSpacing(2),"F5498P-PAY(1,2) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2),NEWLINE,new ColumnSpacing(2),"F5498P-PAY(1,3) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3),NEWLINE,new 
                    ColumnSpacing(2),"F5498P-PAY(1,4) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4),NEWLINE,new ColumnSpacing(2),"F5498P-PAY(1,5) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5),NEWLINE,new 
                    ColumnSpacing(2),"F5498P-PAY(1,6) :",ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6),NEWLINE,new ColumnSpacing(2),"COUPON-TYPE(1)  :",ldaTwrl6345.getTax_Form_Data_Coupon_Type().getValue(1),NEWLINE,new 
                    ColumnSpacing(2),"COUPON-YEAR(1)  :",ldaTwrl6345.getTax_Form_Data_Coupon_Year().getValue(1),NEWLINE,new ColumnSpacing(2),"IRA Type Ind    :",ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,7),NEWLINE,new 
                    ColumnSpacing(2),"SEP Contrib     :",ldaTwrl6345.getTax_Form_Data_Sep_Contrib().getValue(1,7));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M.nadd(1);                                                                                                                                                    //Natural: ASSIGN #M := #M + 1
        if (condition(pnd_M.equals(8)))                                                                                                                                   //Natural: IF #M = 8
        {
            pnd_M.setValue(1);                                                                                                                                            //Natural: ASSIGN #M := 1
        }                                                                                                                                                                 //Natural: END-IF
        hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Decedent_Name());                                                           //Natural: MOVE FORM.TIRF-DECEDENT-NAME TO HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M )
        hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                     //Natural: MOVE FORM.TIRF-PARTICIPANT-NAME TO HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M )
        hold_Form_Data_1_Hold_Addr_Ln1.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                     //Natural: MOVE FORM.TIRF-ADDR-LN1 TO HOLD-FORM-DATA-1.HOLD-ADDR-LN1 ( #M )
        hold_Form_Data_1_Hold_Addr_Ln2.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                     //Natural: MOVE FORM.TIRF-ADDR-LN2 TO HOLD-FORM-DATA-1.HOLD-ADDR-LN2 ( #M )
        hold_Form_Data_1_Hold_Addr_Ln3.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                     //Natural: MOVE FORM.TIRF-ADDR-LN3 TO HOLD-FORM-DATA-1.HOLD-ADDR-LN3 ( #M )
        hold_Form_Data_1_Hold_Addr_Ln4.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                     //Natural: MOVE FORM.TIRF-ADDR-LN4 TO HOLD-FORM-DATA-1.HOLD-ADDR-LN4 ( #M )
        hold_Form_Data_1_Hold_Addr_Ln5.getValue(pnd_M).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                     //Natural: MOVE FORM.TIRF-ADDR-LN5 TO HOLD-FORM-DATA-1.HOLD-ADDR-LN5 ( #M )
        //* *END-IF
        pnd_First_Move.reset();                                                                                                                                           //Natural: RESET #FIRST-MOVE
        if (condition(pnd_M.equals(1)))                                                                                                                                   //Natural: IF #M = 1
        {
            pnd_First_Move.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-MOVE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_M.equals(1) && hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M).notEquals(" ")))                                                             //Natural: IF #M = 1 AND HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M ) NE ' '
        {
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M ) ' as bene of ' INTO TAX-FORM-DATA.F5498P-PAY ( 1,1 ) LEAVING NO
                " as bene of "));
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M));                                        //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M )
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(hold_Form_Data_1_Hold_Addr_Ln1.getValue(pnd_M));                                             //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN1 ( #M )
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(hold_Form_Data_1_Hold_Addr_Ln2.getValue(pnd_M));                                             //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN2 ( #M )
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(hold_Form_Data_1_Hold_Addr_Ln3.getValue(pnd_M));                                             //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN3 ( #M )
            ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Addr_Ln4.getValue(pnd_M),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-ADDR-LN4 ( #M ) HOLD-FORM-DATA-1.HOLD-ADDR-LN5 ( #M ) INTO TAX-FORM-DATA.F5498P-PAY ( 1,6 ) LEAVING NO
                hold_Form_Data_1_Hold_Addr_Ln5.getValue(pnd_M)));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_M.greater(1) && hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M).notEquals(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M.getDec().subtract(1)))  //Natural: IF #M > 1 AND ( HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M ) NE HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M - 1 ) ) AND NOT #FIRST-MOVE
            && ! (pnd_First_Move.getBoolean())))
        {
            if (condition(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M.getDec().subtract(1)).notEquals(" ")))                                                       //Natural: IF HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M - 1 ) NE ' '
            {
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M.getDec().subtract(1)),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M - 1 ) ' as bene of ' INTO TAX-FORM-DATA.F5498P-PAY ( 1,1 ) LEAVING NO
                    " as bene of "));
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M.getDec().subtract(1)));               //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M - 1 )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(hold_Form_Data_1_Hold_Addr_Ln1.getValue(pnd_M.getDec().subtract(1)));                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN1 ( #M - 1 )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(hold_Form_Data_1_Hold_Addr_Ln2.getValue(pnd_M.getDec().subtract(1)));                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN2 ( #M - 1 )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(hold_Form_Data_1_Hold_Addr_Ln3.getValue(pnd_M.getDec().subtract(1)));                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN3 ( #M - 1 )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Addr_Ln4.getValue(pnd_M.getDec().subtract(1)),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-ADDR-LN4 ( #M - 1 ) HOLD-FORM-DATA-1.HOLD-ADDR-LN5 ( #M - 1 ) INTO TAX-FORM-DATA.F5498P-PAY ( 1,6 ) LEAVING NO
                    hold_Form_Data_1_Hold_Addr_Ln5.getValue(pnd_M.getDec().subtract(1))));
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
                sub_Call_Post_Write();
                if (condition(Global.isEscape())) {return;}
                pnd_Idx.setValue(1);                                                                                                                                      //Natural: ASSIGN #IDX := 1
                pnd_Cnt.reset();                                                                                                                                          //Natural: RESET #CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M));                                 //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,1 ) := HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := FORM.TIRF-ADDR-LN1
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := FORM.TIRF-ADDR-LN2
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := FORM.TIRF-ADDR-LN3
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := FORM.TIRF-ADDR-LN4
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,6 ) := FORM.TIRF-ADDR-LN5
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
                sub_Call_Post_Write();
                if (condition(Global.isEscape())) {return;}
                pnd_Idx.setValue(1);                                                                                                                                      //Natural: ASSIGN #IDX := 1
                pnd_Cnt.reset();                                                                                                                                          //Natural: RESET #CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M).equals(" ")))                                                                               //Natural: IF HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M ) = ' '
            {
                getReports().write(0, "decedent is space");                                                                                                               //Natural: WRITE 'decedent is space'
                if (Global.isEscape()) return;
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M));                                 //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,1 ) := HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := FORM.TIRF-ADDR-LN1
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := FORM.TIRF-ADDR-LN2
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := FORM.TIRF-ADDR-LN3
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := FORM.TIRF-ADDR-LN4
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,6 ) := FORM.TIRF-ADDR-LN5
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Participant_Name.getValue(pnd_M),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-PARTICIPANT-NAME ( #M ) ' as bene of ' INTO TAX-FORM-DATA.F5498P-PAY ( 1,1 ) LEAVING NO
                    " as bene of "));
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,2).setValue(hold_Form_Data_1_Hold_Decedent_Name.getValue(pnd_M));                                    //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,2 ) := HOLD-FORM-DATA-1.HOLD-DECEDENT-NAME ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,3).setValue(hold_Form_Data_1_Hold_Addr_Ln1.getValue(pnd_M));                                         //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,3 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN1 ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,4).setValue(hold_Form_Data_1_Hold_Addr_Ln2.getValue(pnd_M));                                         //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,4 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN2 ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,5).setValue(hold_Form_Data_1_Hold_Addr_Ln3.getValue(pnd_M));                                         //Natural: ASSIGN TAX-FORM-DATA.F5498P-PAY ( 1,5 ) := HOLD-FORM-DATA-1.HOLD-ADDR-LN3 ( #M )
                ldaTwrl6345.getTax_Form_Data_F5498p_Pay().getValue(1,6).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, hold_Form_Data_1_Hold_Addr_Ln4.getValue(pnd_M),  //Natural: COMPRESS HOLD-FORM-DATA-1.HOLD-ADDR-LN4 ( #M ) HOLD-FORM-DATA-1.HOLD-ADDR-LN5 ( #M ) INTO TAX-FORM-DATA.F5498P-PAY ( 1,6 ) LEAVING NO
                    hold_Form_Data_1_Hold_Addr_Ln5.getValue(pnd_M)));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Idx.equals(1)))                                                                                                                                 //Natural: IF #IDX = 1
        {
            //*  SAIK
                                                                                                                                                                          //Natural: PERFORM CALL-POST-HEADER
            sub_Call_Post_Header();
            if (condition(Global.isEscape())) {return;}
            //*  5498 NEW FORM
            pnd_Cnt.reset();                                                                                                                                              //Natural: RESET #CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Idx.equals(1) && pnd_First_Time.getBoolean()))                                                                                                  //Natural: IF #IDX = 1 AND #FIRST-TIME
        {
            //* *IF #IDX = 1
            pnd_Write_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WRITE-CNT
            if (condition(! (pnd_Coupons_Done.getBoolean())))                                                                                                             //Natural: IF NOT #COUPONS-DONE
            {
                                                                                                                                                                          //Natural: PERFORM COUPON-PROCESS
                sub_Coupon_Process();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_First_Time.reset();                                                                                                                                       //Natural: RESET #FIRST-TIME
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498 NEW FORM ENDS
        ldaTwrl6345.getTax_Form_Data_Contr_Nbr().getValue(1,pnd_Idx).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X"));             //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X ) TO TAX-FORM-DATA.CONTR-NBR ( 1,#IDX )
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ASSIGN #K := #K + 1
        if (condition(pnd_K.equals(8) || pnd_K.equals(15) || pnd_K.equals(22)))                                                                                           //Natural: IF #K = 8 OR = 15 OR = 22
        {
            pnd_K.setValue(1);                                                                                                                                            //Natural: ASSIGN #K := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(hold_Form_Data_Coup_Primary_Contr_Nbr.getValue(1,pnd_K).notEquals(" ")))                                                                            //Natural: IF HOLD-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,#K ) NE ' '
        {
            ldaTwrl6345.getTax_Form_Data_Coup_Primary_Contr_Nbr().getValue(1,pnd_Idx).setValue(hold_Form_Data_Coup_Primary_Contr_Nbr.getValue(1,pnd_K));                  //Natural: MOVE HOLD-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,#K ) TO TAX-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,#IDX )
            ldaTwrl6345.getTax_Form_Data_Coup_Secondary_Contr_Nbr().getValue(1,pnd_Idx).setValue(hold_Form_Data_Coup_Secondary_Contr_Nbr.getValue(1,pnd_K));              //Natural: MOVE HOLD-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,#K ) TO TAX-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,#IDX )
            ldaTwrl6345.getTax_Form_Data_Coup_Ira_Type_Ind().getValue(1,pnd_Idx).setValue(hold_Form_Data_Coup_Ira_Type_Ind.getValue(1,pnd_K));                            //Natural: MOVE HOLD-FORM-DATA.COUP-IRA-TYPE-IND ( 1,#K ) TO TAX-FORM-DATA.COUP-IRA-TYPE-IND ( 1,#IDX )
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498 NEW FORM ENDS
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_Corr_Ind().getValue(1,pnd_Idx).setValue("Y");                                                                                    //Natural: ASSIGN TAX-FORM-DATA.CORR-IND ( 1,#IDX ) := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                //Natural: MOVE EDITED FORM.TIRF-TRAD-IRA-CONTRIB ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Ira_Contr().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                               //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.IRA-CONTR ( 1,#IDX )
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                               //Natural: MOVE EDITED FORM.TIRF-TRAD-IRA-ROLLOVER ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Rollov_Contr().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                            //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.ROLLOV-CONTR ( 1,#IDX )
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                             //Natural: MOVE EDITED FORM.TIRF-ROTH-IRA-CONVERSION ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Roth_Conv_Amt().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                           //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.ROTH-CONV-AMT ( 1,#IDX )
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                  //Natural: MOVE EDITED FORM.TIRF-IRA-RECHAR-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Rechar_Contrib().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                          //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.RECHAR-CONTRIB ( 1,#IDX )
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Account_Fmv(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                     //Natural: MOVE EDITED FORM.TIRF-ACCOUNT-FMV ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Fair_Mkt_Val().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                            //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.FAIR-MKT-VAL ( 1,#IDX )
        //* **DASDH 5498 BOX CHANGES
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Postpn_Amt(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                      //Natural: MOVE EDITED FORM.TIRF-POSTPN-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Postpn_Amt().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                              //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.POSTPN-AMT ( 1,#IDX )
        ldaTwrl6345.getTax_Form_Data_Postpn_Yr().getValue(1,pnd_Idx).setValue(ldaTwrl9710.getForm_Tirf_Postpn_Yr());                                                      //Natural: MOVE FORM.TIRF-POSTPN-YR TO TAX-FORM-DATA.POSTPN-YR ( 1,#IDX )
        ldaTwrl6345.getTax_Form_Data_Postpn_Code().getValue(1,pnd_Idx).setValue(ldaTwrl9710.getForm_Tirf_Postpn_Code());                                                  //Natural: MOVE FORM.TIRF-POSTPN-CODE TO TAX-FORM-DATA.POSTPN-CODE ( 1,#IDX )
        //* ***DASDH
        //* **SK1   5498 BOX CHANGES
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Repayments_Amt(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                  //Natural: MOVE EDITED FORM.TIRF-REPAYMENTS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Repayments_Amt().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                          //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.REPAYMENTS-AMT ( 1,#IDX )
        ldaTwrl6345.getTax_Form_Data_Repayments_Code().getValue(1,pnd_Idx).setValue(ldaTwrl9710.getForm_Tirf_Repayments_Code());                                          //Natural: MOVE FORM.TIRF-REPAYMENTS-CODE TO TAX-FORM-DATA.REPAYMENTS-CODE ( 1,#IDX )
        //* ***SK1
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                //Natural: MOVE EDITED FORM.TIRF-ROTH-IRA-CONTRIB ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Roth_Ira_Contrib().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                        //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.ROTH-IRA-CONTRIB ( 1,#IDX )
        pnd_Hold_Alpha_14.setValueEdited(ldaTwrl9710.getForm_Tirf_Sep_Amt(),new ReportEditMask("ZZ,ZZZ,ZZ9.99"));                                                         //Natural: MOVE EDITED FORM.TIRF-SEP-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) TO #HOLD-ALPHA-14
                                                                                                                                                                          //Natural: PERFORM FORMAT-ALPHA-14
        sub_Format_Alpha_14();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl6345.getTax_Form_Data_Sep_Contrib().getValue(1,pnd_Idx).setValue(pnd_Hold_Alpha_14, MoveOption.LeftJustified);                                             //Natural: MOVE LEFT JUSTIFIED #HOLD-ALPHA-14 TO TAX-FORM-DATA.SEP-CONTRIB ( 1,#IDX )
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("01") || ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("03")))                                    //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '01' OR = '03'
        {
            ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,pnd_Idx).setValue("I");                                                                                //Natural: ASSIGN TAX-FORM-DATA.IRA-TYPE-IND ( 1,#IDX ) := 'I'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("02")))                                                                                         //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '02'
            {
                ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,pnd_Idx).setValue("R");                                                                            //Natural: ASSIGN TAX-FORM-DATA.IRA-TYPE-IND ( 1,#IDX ) := 'R'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("05")))                                                                                     //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '05'
                {
                    ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,pnd_Idx).setValue("S");                                                                        //Natural: ASSIGN TAX-FORM-DATA.IRA-TYPE-IND ( 1,#IDX ) := 'S'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET BOX 11 IF FORM YEAR GE 2003.
        if (condition(ldaTwrl6345.getTax_Form_Data_Form_Year().greaterOrEqual("03") && ldaTwrl9710.getForm_Tirf_Md_Ind().equals("Y")))                                    //Natural: IF TAX-FORM-DATA.FORM-YEAR GE '03' AND FORM.TIRF-MD-IND EQ 'Y'
        {
            ldaTwrl6345.getTax_Form_Data_Mdo_Required_Ind().getValue(1,pnd_Idx).setValue("X");                                                                            //Natural: MOVE 'X' TO TAX-FORM-DATA.MDO-REQUIRED-IND ( 1,#IDX )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            //*  DASDH
            //* SK1
            getReports().write(0, Global.getPROGRAM(),"FORM DATA (SINGLE CONTRACTS): ",NEWLINE,new ColumnSpacing(2),"CONTR-NBR       :",ldaTwrl6345.getTax_Form_Data_Contr_Nbr().getValue(1,pnd_Idx),NEWLINE,new  //Natural: WRITE *PROGRAM 'FORM DATA (SINGLE CONTRACTS): '/ 2X 'CONTR-NBR       :' TAX-FORM-DATA.CONTR-NBR ( 1,#IDX ) / 2X 'CORR-IND        :' TAX-FORM-DATA.CORR-IND ( 1,#IDX ) / 2X 'IRA-CONTR       :' TAX-FORM-DATA.IRA-CONTR ( 1,#IDX ) / 2X 'ROLLOV-CONTR    :' TAX-FORM-DATA.ROLLOV-CONTR ( 1,#IDX ) / 2X 'ROTH-CONV-AMT   :' TAX-FORM-DATA.ROTH-CONV-AMT ( 1,#IDX ) / 2X 'RECHAR-CONTRIB  :' TAX-FORM-DATA.RECHAR-CONTRIB ( 1,#IDX ) / 2X 'FAIR-MKT-VAL    :' TAX-FORM-DATA.FAIR-MKT-VAL ( 1,#IDX ) / 2X 'ROTH-IRA-CONTRIB:' TAX-FORM-DATA.ROTH-IRA-CONTRIB ( 1,#IDX ) / 2X 'SEP-CONTRIB     :' TAX-FORM-DATA.SEP-CONTRIB ( 1,#IDX ) / 2X 'IRA-TYPE-IND    :' TAX-FORM-DATA.IRA-TYPE-IND ( 1,#IDX ) / 2X 'POSTPONE-AMT    :' TAX-FORM-DATA.POSTPN-AMT ( 1,#IDX ) / 2X 'REPYMN-AMT      :' TAX-FORM-DATA.REPAYMENTS-AMT ( 1,#IDX ) /
                ColumnSpacing(2),"CORR-IND        :",ldaTwrl6345.getTax_Form_Data_Corr_Ind().getValue(1,pnd_Idx),NEWLINE,new ColumnSpacing(2),"IRA-CONTR       :",ldaTwrl6345.getTax_Form_Data_Ira_Contr().getValue(1,pnd_Idx),NEWLINE,new 
                ColumnSpacing(2),"ROLLOV-CONTR    :",ldaTwrl6345.getTax_Form_Data_Rollov_Contr().getValue(1,pnd_Idx),NEWLINE,new ColumnSpacing(2),"ROTH-CONV-AMT   :",ldaTwrl6345.getTax_Form_Data_Roth_Conv_Amt().getValue(1,pnd_Idx),NEWLINE,new 
                ColumnSpacing(2),"RECHAR-CONTRIB  :",ldaTwrl6345.getTax_Form_Data_Rechar_Contrib().getValue(1,pnd_Idx),NEWLINE,new ColumnSpacing(2),"FAIR-MKT-VAL    :",ldaTwrl6345.getTax_Form_Data_Fair_Mkt_Val().getValue(1,pnd_Idx),NEWLINE,new 
                ColumnSpacing(2),"ROTH-IRA-CONTRIB:",ldaTwrl6345.getTax_Form_Data_Roth_Ira_Contrib().getValue(1,pnd_Idx),NEWLINE,new ColumnSpacing(2),"SEP-CONTRIB     :",ldaTwrl6345.getTax_Form_Data_Sep_Contrib().getValue(1,pnd_Idx),NEWLINE,new 
                ColumnSpacing(2),"IRA-TYPE-IND    :",ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,pnd_Idx),NEWLINE,new ColumnSpacing(2),"POSTPONE-AMT    :",ldaTwrl6345.getTax_Form_Data_Postpn_Amt().getValue(1,pnd_Idx),NEWLINE,new 
                ColumnSpacing(2),"REPYMN-AMT      :",ldaTwrl6345.getTax_Form_Data_Repayments_Amt().getValue(1,pnd_Idx),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Idx.equals(7)))                                                                                                                                 //Natural: IF #IDX = 7
        {
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
            sub_Call_Post_Write();
            if (condition(Global.isEscape())) {return;}
            //*  5498 NEW FORM
            pnd_Idx.reset();                                                                                                                                              //Natural: RESET #IDX
            pnd_First_Time.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-TIME := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Alpha_14() throws Exception                                                                                                                   //Natural: FORMAT-ALPHA-14
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Hold_Alpha_14.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "$", pnd_Hold_Alpha_14));                                                              //Natural: COMPRESS '$' #HOLD-ALPHA-14 INTO #HOLD-ALPHA-14 LEAVING NO
        DbsUtil.examine(new ExamineSource(pnd_Hold_Alpha_14), new ExamineSearch(" "), new ExamineDelete());                                                               //Natural: EXAMINE #HOLD-ALPHA-14 ' ' DELETE
    }
    private void sub_Coupon_Process() throws Exception                                                                                                                    //Natural: COUPON-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM()," SUBROUTINE COUPON-PROCESS");                                                                                      //Natural: WRITE *PROGRAM ' SUBROUTINE COUPON-PROCESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        pnd_Start.compute(new ComputeParameters(false, pnd_Start), ((pnd_Write_Cnt.subtract(1)).multiply(8)).add(1));                                                     //Natural: ASSIGN #START := ( ( #WRITE-CNT - 1 ) * 8 ) + 1
        pnd_End.compute(new ComputeParameters(false, pnd_End), pnd_Start.add(7));                                                                                         //Natural: ASSIGN #END := #START + 7
        //*  5498 NEW FORM
        //*  5498 NEW FORM
        //*  5498 NEW FORM
        hold_Form_Data_Coup_Primary_Contr_Nbr.getValue(1,"*").reset();                                                                                                    //Natural: RESET HOLD-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,* ) HOLD-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,* ) HOLD-FORM-DATA.COUP-IRA-TYPE-IND ( 1,* )
        hold_Form_Data_Coup_Secondary_Contr_Nbr.getValue(1,"*").reset();
        hold_Form_Data_Coup_Ira_Type_Ind.getValue(1,"*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I FROM #START #END
        for (pnd_I.setValue(pnd_Start); condition(pnd_I.lessOrEqual(pnd_End)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.greater(50)))                                                                                                                             //Natural: IF #I > 50
            {
                pnd_Coupons_Done.setValue(true);                                                                                                                          //Natural: ASSIGN #COUPONS-DONE := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ASSIGN #J := #J + 1
            if (condition(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(pnd_I).notEquals(" ")))                                                                      //Natural: IF #TWRA0118.#PRIMARY ( #I ) NE ' '
            {
                //*  5498 NEW FORM STARTS
                //* *  MOVE EDITED #TWRA0118.#PRIMARY(#I) (EM=XXXXXXX-X)
                //* *    TO TAX-FORM-DATA.COUP-PRIMARY-CONTR-NBR  (1,#J)
                //* *  MOVE EDITED #TWRA0118.#SECONDARY(#I) (EM=XXXXXXX-X)
                //* *    TO TAX-FORM-DATA.COUP-SECONDARY-CONTR-NBR(1,#J)
                //* *  IF   TAX-FORM-DATA.FORM-YEAR  GE '03'
                //* *    MOVE #TWRA0118.#IRA-TYPE-IND (#I)
                //* *      TO TAX-FORM-DATA.COUP-IRA-TYPE-IND (1,#J)
                //* *  END-IF
                hold_Form_Data_Coup_Primary_Contr_Nbr.getValue(1,pnd_J).setValueEdited(pdaTwra0118.getPnd_Twra0118_Pnd_Primary().getValue(pnd_I),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED #TWRA0118.#PRIMARY ( #I ) ( EM = XXXXXXX-X ) TO HOLD-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,#J )
                hold_Form_Data_Coup_Secondary_Contr_Nbr.getValue(1,pnd_J).setValueEdited(pdaTwra0118.getPnd_Twra0118_Pnd_Secondary().getValue(pnd_I),new                  //Natural: MOVE EDITED #TWRA0118.#SECONDARY ( #I ) ( EM = XXXXXXX-X ) TO HOLD-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,#J )
                    ReportEditMask("XXXXXXX-X"));
                if (condition(ldaTwrl6345.getTax_Form_Data_Form_Year().greaterOrEqual("03")))                                                                             //Natural: IF TAX-FORM-DATA.FORM-YEAR GE '03'
                {
                    hold_Form_Data_Coup_Ira_Type_Ind.getValue(1,pnd_J).setValue(pdaTwra0118.getPnd_Twra0118_Pnd_Ira_Type_Ind().getValue(pnd_I));                          //Natural: MOVE #TWRA0118.#IRA-TYPE-IND ( #I ) TO HOLD-FORM-DATA.COUP-IRA-TYPE-IND ( 1,#J )
                }                                                                                                                                                         //Natural: END-IF
                //*  5498 NEW FORM ENDS
                //*  SAIK
                if (condition(pnd_J.equals(8) || pnd_J.equals(16) || pnd_J.equals(24)))                                                                                   //Natural: IF #J = 8 OR = 16 OR = 24
                {
                    //*  SAIK
                    ldaTwrl6345.getTax_Form_Data_Coup_Primary_Contr_Nbr().getValue(1,pnd_J).reset();                                                                      //Natural: RESET TAX-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,#J )
                    //*  SAIK
                    ldaTwrl6345.getTax_Form_Data_Coup_Secondary_Contr_Nbr().getValue(1,pnd_J).reset();                                                                    //Natural: RESET TAX-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,#J )
                    //*  SAIK
                    ldaTwrl6345.getTax_Form_Data_Coup_Ira_Type_Ind().getValue(1,pnd_J).reset();                                                                           //Natural: RESET TAX-FORM-DATA.COUP-IRA-TYPE-IND ( 1,#J )
                    //*  SAIK
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.equals(pdaTwra0118.getPnd_Twra0118_Pnd_Rec_Cnt())))                                                                                   //Natural: IF #I = #REC-CNT
                {
                    pnd_Coupons_Done.setValue(true);                                                                                                                      //Natural: ASSIGN #COUPONS-DONE := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Coupons_Done.setValue(true);                                                                                                                          //Natural: ASSIGN #COUPONS-DONE := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_After_5498_Read() throws Exception                                                                                                                   //Natural: AFTER-5498-READ
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM()," SUBROUTINE AFTER-5498-READ");                                                                                     //Natural: WRITE *PROGRAM ' SUBROUTINE AFTER-5498-READ'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Idx.greater(getZero())))                                                                                                                        //Natural: IF #IDX > 0
        {
            if (condition(! (pnd_Coupons_Done.getBoolean())))                                                                                                             //Natural: IF NOT #COUPONS-DONE
            {
                                                                                                                                                                          //Natural: PERFORM COUPON-PROCESS
                sub_Coupon_Process();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
            sub_Call_Post_Write();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pnd_Coupons_Done.getBoolean())))                                                                                                             //Natural: IF NOT #COUPONS-DONE
            {
                                                                                                                                                                          //Natural: PERFORM COUPON-PROCESS
                sub_Coupon_Process();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
                sub_Call_Post_Write();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Coupons_Done.getBoolean())))                                                                                                                 //Natural: IF NOT #COUPONS-DONE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Coupons_Done.getBoolean())) {break;}                                                                                                    //Natural: UNTIL #COUPONS-DONE
                pnd_Write_Cnt.nadd(1);                                                                                                                                    //Natural: ASSIGN #WRITE-CNT := #WRITE-CNT + 1
                                                                                                                                                                          //Natural: PERFORM COUPON-PROCESS
                sub_Coupon_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CALL-POST-WRITE
                sub_Call_Post_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Post_Header() throws Exception                                                                                                                  //Natural: CALL-POST-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM()," SUBROUTINE CALL-POST-WRITE");                                                                                     //Natural: WRITE *PROGRAM ' SUBROUTINE CALL-POST-WRITE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  << DUTTAD2
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF TIRF-TAX-YEAR GE '2016'
        {
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);                                                                                    //Natural: MOVE TRUE TO TAX-FORM-DATA.DONT-PULL-5498P-INSTRUCTION
            //*  >> DUTTAD2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2016")))                                                                                              //Natural: IF TIRF-TAX-YEAR LT '2016'
            {
                if (condition(pnd_First_Write.getBoolean()))                                                                                                              //Natural: IF #FIRST-WRITE
                {
                    pnd_First_Write.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #FIRST-WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);                                                                            //Natural: MOVE TRUE TO TAX-FORM-DATA.DONT-PULL-5498P-INSTRUCTION
                }                                                                                                                                                         //Natural: END-IF
                //*  DUTTAD2
            }                                                                                                                                                             //Natural: END-IF
            //*  DUTTAD2
        }                                                                                                                                                                 //Natural: END-IF
        //*  << DUTTAD2
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF TIRF-TAX-YEAR GE '2016'
        {
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                                    //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
            //*  >> DUTTAD2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2016")))                                                                                              //Natural: IF TIRF-TAX-YEAR LT '2016'
            {
                //*  <<DASRAH
                if (condition(pnd_Link_Let_Type.getValue(5).notEquals(" ") || pnd_Link_Let_Type.getValue(6).notEquals(" ")))                                              //Natural: IF #LINK-LET-TYPE ( 5 ) NE ' ' OR #LINK-LET-TYPE ( 6 ) NE ' '
                {
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                            //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
                    //*  DASRAH>>
                }                                                                                                                                                         //Natural: END-IF
                //*  DUTTAD2
            }                                                                                                                                                             //Natural: END-IF
            //*  DUTTAD2
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #TWRA0214-MOBIUS-MIGR-IND = 'M'                       /* SINHSN
        //*  TAX FORM DATA, MUST BE LOWER CASE
        pdaPsta6346.getPsta6346_Tax_Data_Array().getValue(1,":",ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Lgth()).setValue(ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Array().getValue(1, //Natural: MOVE TAX-FORM-DATA-ARRAY ( 1:TAX-FORM-DATA-LGTH ) TO PSTA6346.TAX-DATA-ARRAY ( 1:TAX-FORM-DATA-LGTH )
            ":",ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Lgth()));
        pdaPsta6346.getPsta6346_Pnd_Csf_Record_Id().setValue("b");                                                                                                        //Natural: ASSIGN PSTA6346.#CSF-RECORD-ID := 'b'
        //*  SINHASN
        //*  SINHASN
        //*  SINHASN
        pdaPsta6346.getPsta6346_Pnd_Called_From_Post().reset();                                                                                                           //Natural: RESET PSTA6346.#CALLED-FROM-POST
        ldaTwrl6345.getTax_Form_Data_S_Lob_Code_13j().getValue("*").setValue(" ");                                                                                        //Natural: ASSIGN S-LOB-CODE-13J ( * ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Payer_3_Status_Code_16d().getValue("*").setValue(" ");                                                                             //Natural: ASSIGN S-PAYER-3-STATUS-CODE-16D ( * ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Payer_4_Status_Code_16e().getValue("*").setValue(" ");                                                                             //Natural: ASSIGN S-PAYER-4-STATUS-CODE-16E ( * ) := ' '
        //*   /* SNEHA CHANGES STARTED - SINHSN - FOR HEADER
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"In Post-Open...");                                                                                                 //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Open...'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
        if (condition(ldaTwrl9710.getForm_Tirf_Pin().greater(getZero())))                                                                                                 //Natural: IF TIRF-PIN > 0
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("P");                                                                                        //Natural: ASSIGN #UNIV-ID-TYP := 'P'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Pin().equals(getZero()) || ldaTwrl9710.getForm_Tirf_Pin().equals(new DbsDecimal("999999999999"))))                     //Natural: IF TIRF-PIN = 0 OR TIRF-PIN = 999999999999
            {
                pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("N");                                                                                    //Natural: ASSIGN #UNIV-ID-TYP := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                          //Natural: IF TIRF-TAX-ID-TYPE = '4' OR = '5'
        {
            pnd_Tirf_Tin.setValue(" ");                                                                                                                                   //Natural: ASSIGN #TIRF-TIN := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tirf_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                                        //Natural: ASSIGN #TIRF-TIN := TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Ssn_Nbr().setValue(pnd_Tirf_Tin_Pnd_Tirf_Tin_N9);                                                                                         //Natural: ASSIGN SSN-NBR := #TIRF-TIN-N9
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ());                                                          //Natural: ASSIGN UNIV-ID-TYP := #UNIV-ID-TYP
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().equals("P")))                                                                               //Natural: IF #UNIV-ID-TYP = 'P'
        {
            //*   MOVE EDITED TIRF-PIN (EM=999999999999) TO  UNIV-ID
            pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                                   //Natural: MOVE TIRF-PIN TO UNIV-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN UNIV-ID := TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                                         //Natural: ASSIGN FULL-NME := TIRF-PARTICIPANT-NAME
        pdaPsta9612.getPsta9612_Last_Nme().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                                                            //Natural: ASSIGN LAST-NME := TIRF-PART-LAST-NME
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                            //Natural: ASSIGN ADDRSS-LINE-1 := TIRF-ADDR-LN1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                            //Natural: ASSIGN ADDRSS-LINE-2 := TIRF-ADDR-LN2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                            //Natural: ASSIGN ADDRSS-LINE-3 := TIRF-ADDR-LN3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                            //Natural: ASSIGN ADDRSS-LINE-4 := TIRF-ADDR-LN4
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                            //Natural: ASSIGN ADDRSS-LINE-5 := TIRF-ADDR-LN4
        pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln6());                                                                            //Natural: ASSIGN ADDRSS-LINE-6 := TIRF-ADDR-LN6
        if (condition(ldaTwrl9710.getForm_Tirf_Foreign_Addr().notEquals("Y")))                                                                                            //Natural: IF TIRF-FOREIGN-ADDR NE 'Y'
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                                       //Natural: MOVE 'U' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("F");                                                                                                       //Natural: MOVE 'F' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaPsta9612.getPsta9612_Addrss_Typ_Cde().equals("F") && ldaTwrl9710.getForm_Tirf_Res_Type().getValue("*").equals("4")))                             //Natural: IF ADDRSS-TYP-CDE EQ 'F' AND TIRF-RES-TYPE ( * ) EQ '4'
        {
            //*  IF TIRF-RES-TYPE      EQ '4'
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("C");                                                                                                       //Natural: MOVE 'C' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Code().setValue(false);                                                                                                   //Natural: ASSIGN #TWRA214E.#RET-CODE := FALSE
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Msg().setValue("POST OPEN -- Call Failed (PSTN9612)");                                                                    //Natural: ASSIGN #TWRA214E.#RET-MSG := 'POST OPEN -- Call Failed (PSTN9612)'
            pnd_Fatal_Err.setValue(true);                                                                                                                                 //Natural: ASSIGN #FATAL-ERR := TRUE
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_Mobindx().setValue(pnd_Mobindx);                                                                                                     //Natural: ASSIGN TAX-FORM-DATA.MOBINDX := #MOBINDX
    }
    private void sub_Call_Post_Write() throws Exception                                                                                                                   //Natural: CALL-POST-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"In Post-Write..");                                                                                                 //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Write..'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"Calling PSTN9500");                                                                                                //Natural: WRITE *PROGRAM 'Calling PSTN9500'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY TAX-FORM-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaTwrl6345.getSort_Key(), ldaTwrl6345.getTax_Form_Data());
        if (condition(Global.isEscape())) return;
        //*  END-IF                                 /* SINHSN
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Code().setValue(false);                                                                                                   //Natural: ASSIGN #TWRA214E.#RET-CODE := FALSE
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Msg().setValue("POST WRITE -- Call Failed (PSTN9500)");                                                                   //Natural: ASSIGN #TWRA214E.#RET-MSG := 'POST WRITE -- Call Failed (PSTN9500)'
            pnd_Fatal_Err.setValue(true);                                                                                                                                 //Natural: ASSIGN #FATAL-ERR := TRUE
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 3 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  PERFORM ERROR-CONDITION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  5498 NEW FORM
            //*  DASDH
            //*  SK1
            ldaTwrl6345.getTax_Form_Data_Contr_Nbr().getValue(1,"*").reset();                                                                                             //Natural: RESET TAX-FORM-DATA.CONTR-NBR ( 1,* ) TAX-FORM-DATA.CORR-IND ( 1,* ) TAX-FORM-DATA.IRA-CONTR ( 1,* ) TAX-FORM-DATA.ROLLOV-CONTR ( 1,* ) TAX-FORM-DATA.ROTH-CONV-AMT ( 1,* ) TAX-FORM-DATA.RECHAR-CONTRIB ( 1,* ) TAX-FORM-DATA.FAIR-MKT-VAL ( 1,* ) TAX-FORM-DATA.ROTH-IRA-CONTRIB ( 1,* ) TAX-FORM-DATA.SEP-CONTRIB ( 1,* ) TAX-FORM-DATA.IRA-TYPE-IND ( 1,* ) TAX-FORM-DATA.MDO-REQUIRED-IND ( 1,* ) TAX-FORM-DATA.POSTPN-AMT ( 1,* ) TAX-FORM-DATA.REPAYMENTS-AMT ( 1,* ) TAX-FORM-DATA.COUP-PRIMARY-CONTR-NBR ( 1,* ) TAX-FORM-DATA.COUP-SECONDARY-CONTR-NBR ( 1,* )
            ldaTwrl6345.getTax_Form_Data_Corr_Ind().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Ira_Contr().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Rollov_Contr().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Roth_Conv_Amt().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Rechar_Contrib().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Fair_Mkt_Val().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Roth_Ira_Contrib().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Sep_Contrib().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Mdo_Required_Ind().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Postpn_Amt().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Repayments_Amt().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Coup_Primary_Contr_Nbr().getValue(1,"*").reset();
            ldaTwrl6345.getTax_Form_Data_Coup_Secondary_Contr_Nbr().getValue(1,"*").reset();
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),"POST WRITE -- Call Successful");                                                                               //Natural: WRITE *PROGRAM 'POST WRITE -- Call Successful'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  POST-CLOSE
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"In Post-Close..");                                                                                                 //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Close..'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Code().setValue(false);                                                                                                   //Natural: ASSIGN #TWRA214E.#RET-CODE := FALSE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
            pdaTwra214e.getPnd_Twra214e_Pnd_Ret_Msg().setValue("POST CLOSE -- Call Failed (PSTN9685)");                                                                   //Natural: ASSIGN #TWRA214E.#RET-MSG := 'POST CLOSE -- Call Failed (PSTN9685)'
            pnd_Fatal_Err.setValue(true);                                                                                                                                 //Natural: ASSIGN #FATAL-ERR := TRUE
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  /* SNEHA CHANGES ENDED - SINHSN
    }
    private void sub_Coupon_Processing() throws Exception                                                                                                                 //Natural: COUPON-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        DbsUtil.callnat(Twrn0118.class , getCurrentProcessState(), pdaTwra0118.getPnd_Twra0118());                                                                        //Natural: CALLNAT 'TWRN0118' USING #TWRA0118
        if (condition(Global.isEscape())) return;
        pnd_Coupon_Type_1_2.reset();                                                                                                                                      //Natural: RESET #COUPON-TYPE-1-2
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0118.getPnd_Twra0118_Pnd_Coupon_Year());                                                                 //Natural: WRITE *PROGRAM '=' #TWRA0118.#COUPON-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwra0118.getPnd_Twra0118_Pnd_Ret_Cde().getBoolean()))                                                                                            //Natural: IF #TWRA0118.#RET-CDE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, NEWLINE,NEWLINE,"Error Calling TWRA0118", new FieldAttributes ("AD=I"));                                                                //Natural: WRITE // 'Error Calling TWRA0118' ( AD = I )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=80 LS=130");
    }
}
