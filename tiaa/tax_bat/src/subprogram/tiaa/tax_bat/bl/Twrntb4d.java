/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:21:46 AM
**        * FROM NATURAL SUBPROGRAM : Twrntb4d
************************************************************
**        * FILE NAME            : Twrntb4d.java
**        * CLASS NAME           : Twrntb4d
**        * INSTANCE NAME        : Twrntb4d
************************************************************
************************************************************************
* SUBPROGRAM: TWRNTB4N
* FUNCTION..: LOOKUP ON CONTROL TABLE - TABLE 4 BY ISN AND DELETE
* PROGRAMMER: MICHAEL SUPONITSKY - 01/31/2001
* HISTORY...:
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrntb4d extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4u ldaTwrltb4u;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrltb4u = new LdaTwrltb4u();
        registerRecord(ldaTwrltb4u);
        registerRecord(ldaTwrltb4u.getVw_tircntl_Rpt_U());

        // parameters
        parameters = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrltb4u.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrntb4d() throws Exception
    {
        super("Twrntb4d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        G1:                                                                                                                                                               //Natural: GET TIRCNTL-RPT-U #TWRATBL4.#ISN
        ldaTwrltb4u.getVw_tircntl_Rpt_U().readByID(pdaTwratbl4.getPnd_Twratbl4_Pnd_Isn().getLong(), "G1");
        ldaTwrltb4u.getVw_tircntl_Rpt_U().deleteDBRow("G1");                                                                                                              //Natural: DELETE ( G1. )
    }

    //
}
