/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:17:21 AM
**        * FROM NATURAL SUBPROGRAM : Twrn5006
************************************************************
**        * FILE NAME            : Twrn5006.java
**        * CLASS NAME           : Twrn5006
**        * INSTANCE NAME        : Twrn5006
************************************************************
************************************************************************
** SUBPROGRAM : TWRN5006
** SYSTEM     : TAXWARS
** AUTHOR     : MICHAEL SUPONITSKY
** FUNCTION   : CONTROL REPORT OF ROLLED UP FORMS.
** HISTORY.....:
**    12/30/2014 FE FATCA CHANGES. ADD FIELDS FOR FATCA TOTALS. FE141215
**    07/24/2012 MS SUNY CHANGES.
**    09/12/2005 MS TOPS RELEASE 6.5 - SEP IRA
**    11/29/2002 MS 1042-S BOX 8 - REFUND AMOUNT
**    11/02/2001 MS 2001 PROCESSING FOR 5498 INCLUDED.
**    11/05/2020 SK 2020 ADDED REPAYMENTS CODE FOR 5498.
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn5006 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra5000 pdaTwra5000;
    private PdaTwra5006 pdaTwra5006;
    private PdaTwrafrmn pdaTwrafrmn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaTwra5000 = new PdaTwra5000(parameters);
        pdaTwra5006 = new PdaTwra5006(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn5006() throws Exception
    {
        super("Twrn5006");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 80 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT1' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT2' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT3' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 29T 'Contribution file rollup' 68T 'Report: RPT4' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT5' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT6' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 68T 'Report: RPT7' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 68T 'Page:' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'Payment file rollup' 67T 'Report: RPT10' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Reporting System' 67T 'Page: ' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 29T 'Contribution file rollup' 67T 'Report: RPT15' / 29T 'Tax processing year' #TWRA5000.#TAX-YEAR ( SG = OFF ) // 'Company..: TIAA / CREF / LIFE / TCII / TRUST' / 'Form Type:' #TWRAFRMN.#FORM-NAME ( #I ) //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                                  //Natural: ASSIGN #TWRAFRMN.#TAX-YEAR := #TWRA5000.#TAX-YEAR
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  1099-R
        short decideConditionsMet179 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FORMS-TO-REPORT ( 1 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(1 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  1099-INT
                                                                                                                                                                          //Natural: PERFORM REPORT-1099R
            sub_Report_1099r();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 2 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(2 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  1042-S
                                                                                                                                                                          //Natural: PERFORM REPORT-1099I
            sub_Report_1099i();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 3 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(3 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  5498
                                                                                                                                                                          //Natural: PERFORM REPORT-1042S
            sub_Report_1042s();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 4 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(4 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  480.7C OR 480.6A
                                                                                                                                                                          //Natural: PERFORM REPORT-5498
            sub_Report_5498();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 5 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(5 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  480.6B
                                                                                                                                                                          //Natural: PERFORM REPORT-4807C
            sub_Report_4807c();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 6 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(6 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  NR4
                                                                                                                                                                          //Natural: PERFORM REPORT-4806B
            sub_Report_4806b();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 7 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(7 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  SUNY
                                                                                                                                                                          //Natural: PERFORM REPORT-NR4
            sub_Report_Nr4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 10 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(10 + 1).equals(true)))
        {
            decideConditionsMet179++;
            //*  INVALID FORM
                                                                                                                                                                          //Natural: PERFORM REPORT-SUNY
            sub_Report_Suny();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FORMS-TO-REPORT ( 0 )
        if (condition(pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(0 + 1).equals(true)))
        {
            decideConditionsMet179++;
                                                                                                                                                                          //Natural: PERFORM REPORT-INVALID
            sub_Report_Invalid();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet179 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* **********************
        //*  S U B R O U T I N E S
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-1099R
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-1099I
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-1042S
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-5498
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-4807C
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-4806B
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-NR4
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-SUNY
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-INVALID
    }
    private void sub_Report_1099r() throws Exception                                                                                                                      //Natural: REPORT-1099R
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().definePrinter(2, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 01 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS.#I := 1
        getReports().write(1, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 1 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Federal Gross Amount..........' #1099R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Federal Taxable Amount........' #1099R-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Federal IVC Amount............' #1099R-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Federal Tax Withheld..........' #1099R-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Residency Gross Amount........' #1099R-RES-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Residency Taxable Amount......' #1099R-RES-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Residency IVC Amount..........' #1099R-RES-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Residency Federal Tax Withheld' #1099R-RES-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'State Distribution............' #1099R-STATE-DISTR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'State Tax Withheld............' #1099R-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Local Distribution............' #1099R-LOC-DISTR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Local Tax Withheld............' #1099R-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Federal Gross Amount..........",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Federal Taxable Amount........",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Taxable_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Federal IVC Amount............",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Ivc_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Federal Tax Withheld..........",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Residency Gross Amount........",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Res_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Residency Taxable Amount......",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Res_Taxable_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Residency IVC Amount..........",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Res_Ivc_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Residency Federal Tax Withheld",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Res_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"State Distribution............",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_State_Distr(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"State Tax Withheld............",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_State_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Local Distribution............",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Loc_Distr(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Local Tax Withheld............",pdaTwra5006.getPnd_Twra5006_Pnd_1099r_Loc_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(2);                                                                                                                                     //Natural: CLOSE PRINTER ( 01 )
    }
    private void sub_Report_1099i() throws Exception                                                                                                                      //Natural: REPORT-1099I
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().definePrinter(3, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 02 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(2);                                                                                                                                         //Natural: ASSIGN #WS.#I := 2
        getReports().write(2, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 2 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Interest Amount...............' #1099I-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Federal Backup Withholding....' #1099I-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Interest Amount...............",pdaTwra5006.getPnd_Twra5006_Pnd_1099i_Int_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Federal Backup Withholding....",pdaTwra5006.getPnd_Twra5006_Pnd_1099i_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(3);                                                                                                                                     //Natural: CLOSE PRINTER ( 02 )
    }
    private void sub_Report_1042s() throws Exception                                                                                                                      //Natural: REPORT-1042S
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  FE141215 START
        //*  FE141215 END
        //*  FE141215 START
        //*  FE141215 END
        //*  FE141215 START
        //*  FE141215 END
        getReports().definePrinter(4, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 03 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(3);                                                                                                                                         //Natural: ASSIGN #WS.#I := 3
        getReports().write(3, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 3 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Gross Income..................' #1042S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   FATCA    ..................' #1042S-GROSS-INCOME-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   Non-FATCA..................' #1042S-GROSS-INCOME-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'NRA Tax withheld..............' #1042S-NRA-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   FATCA    ..................' #1042S-NRA-TAX-WTHLD-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   Non-FATCA..................' #1042S-NRA-TAX-WTHLD-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'NRA Refund Amount.............' #1042S-REFUND-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   FATCA    ..................' #1042S-REFUND-AMT-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T '   Non-FATCA..................' #1042S-REFUND-AMT-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Gross Income..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Gross_Income(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   FATCA    ..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Gross_Income_Fatca().getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   Non-FATCA..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Gross_Income_Fatca().getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"NRA Tax withheld..............",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Nra_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   FATCA    ..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Nra_Tax_Wthld_Fatca().getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   Non-FATCA..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Nra_Tax_Wthld_Fatca().getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"NRA Refund Amount.............",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Refund_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   FATCA    ..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Refund_Amt_Fatca().getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"   Non-FATCA..................",pdaTwra5006.getPnd_Twra5006_Pnd_1042s_Refund_Amt_Fatca().getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(4);                                                                                                                                     //Natural: CLOSE PRINTER ( 03 )
    }
    private void sub_Report_5498() throws Exception                                                                                                                       //Natural: REPORT-5498
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  DASDH
        //*  DASDH
        //*  SAIK
        //*  SAIK
        getReports().definePrinter(5, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 04 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(4);                                                                                                                                         //Natural: ASSIGN #WS.#I := 4
        getReports().write(4, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 4 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Traditional IRA Contributions.' #5498-TRAD-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Roth IRA Contributions........' #5498-ROTH-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'SEP  IRA Contributions........' #5498-SEP-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Recharacterization Amount.....' #5498-IRA-RECHAR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'IRA Rollovers.................' #5498-TRAD-IRA-ROLLOVER ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Roth IRA Conversions..........' #5498-ROTH-IRA-CONVERSION ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Fair Market Value.............' #5498-ACCOUNT-FMV ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Late RO Amount................' #5498-POSTPN-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Repayments Amount.............' #5498-REPAYMENTS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Traditional IRA Contributions.",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Trad_Ira_Contrib(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Roth IRA Contributions........",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Roth_Ira_Contrib(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"SEP  IRA Contributions........",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Sep_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Recharacterization Amount.....",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Ira_Rechar_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"IRA Rollovers.................",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Trad_Ira_Rollover(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Roth IRA Conversions..........",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Roth_Ira_Conversion(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Fair Market Value.............",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Account_Fmv(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Late RO Amount................",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Postpn_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Repayments Amount.............",pdaTwra5006.getPnd_Twra5006_Pnd_5498_Repayments_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(5);                                                                                                                                     //Natural: CLOSE PRINTER ( 04 )
    }
    private void sub_Report_4807c() throws Exception                                                                                                                      //Natural: REPORT-4807C
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().definePrinter(6, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 05 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(5);                                                                                                                                         //Natural: ASSIGN #WS.#I := 5
        getReports().write(5, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(5 + 1),  //Natural: WRITE ( 5 ) / 14T 'Form Count....................' 6X #FORM-CNT ( 5 ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( 5 ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Gross Amount..................' #4807C-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Interest Amount...............' #4807C-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Taxable Amount................' #4807C-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'IVC Amount....................' #4807C-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Rollover Gross Amount.........' #4807C-GROSS-AMT-ROLL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Rollover IVC Amount...........' #4807C-IVC-AMT-ROLL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(5 + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Gross Amount..................",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Interest Amount...............",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Int_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Taxable Amount................",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Taxable_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"IVC Amount....................",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Ivc_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Rollover Gross Amount.........",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Gross_Amt_Roll(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Rollover IVC Amount...........",pdaTwra5006.getPnd_Twra5006_Pnd_4807c_Ivc_Amt_Roll(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(6);                                                                                                                                     //Natural: CLOSE PRINTER ( 05 )
    }
    private void sub_Report_4806b() throws Exception                                                                                                                      //Natural: REPORT-4806B
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().definePrinter(7, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 06 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(6);                                                                                                                                         //Natural: ASSIGN #WS.#I := 6
        getReports().write(6, NEWLINE,new TabSetting(14),"Form Count.....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 6 ) / 14T 'Form Count.....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held.....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Gross Amount...................' #4806B-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Puerto Rico Tax Withheld.......' #4806B-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held.....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Gross Amount...................",pdaTwra5006.getPnd_Twra5006_Pnd_4806b_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Puerto Rico Tax Withheld.......",pdaTwra5006.getPnd_Twra5006_Pnd_4806b_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(7);                                                                                                                                     //Natural: CLOSE PRINTER ( 06 )
    }
    private void sub_Report_Nr4() throws Exception                                                                                                                        //Natural: REPORT-NR4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        getReports().definePrinter(8, "'CMPRT15'");                                                                                                                       //Natural: DEFINE PRINTER ( 07 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(7);                                                                                                                                         //Natural: ASSIGN #WS.#I := 7
        getReports().write(7, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 7 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Gross Amount..................' #NR4-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Interest Amount...............' #NR4-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Canadian Tax Withheld.........' #NR4-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Gross Amount..................",pdaTwra5006.getPnd_Twra5006_Pnd_Nr4_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Interest Amount...............",pdaTwra5006.getPnd_Twra5006_Pnd_Nr4_Int_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Canadian Tax Withheld.........",pdaTwra5006.getPnd_Twra5006_Pnd_Nr4_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(8);                                                                                                                                     //Natural: CLOSE PRINTER ( 07 )
    }
    private void sub_Report_Suny() throws Exception                                                                                                                       //Natural: REPORT-SUNY
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        getReports().definePrinter(11, "'CMPRT15'");                                                                                                                      //Natural: DEFINE PRINTER ( 10 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(10);                                                                                                                                        //Natural: ASSIGN #WS.#I := 10
        getReports().write(10, NEWLINE,new TabSetting(14),"SUNY/CUNY",NEWLINE,new TabSetting(14),"Letter (Participant) Count....",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 10 ) / 14T 'SUNY/CUNY' / 14T 'Letter (Participant) Count....' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Contract Count................' 6X #SUNY-CONTACT-CNT ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Gross Amount..................' #SUNY-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Federal Tax Withheld..........' #SUNY-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'State Tax Withheld............' #SUNY-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 14T 'Local Tax Withheld............' #SUNY-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Contract Count................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Suny_Contact_Cnt(), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Gross Amount..................",pdaTwra5006.getPnd_Twra5006_Pnd_Suny_Gross_Amt(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Federal Tax Withheld..........",pdaTwra5006.getPnd_Twra5006_Pnd_Suny_Fed_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"State Tax Withheld............",pdaTwra5006.getPnd_Twra5006_Pnd_Suny_State_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(14),"Local Tax Withheld............",pdaTwra5006.getPnd_Twra5006_Pnd_Suny_Loc_Tax_Wthld(), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().closePrinter(2);                                                                                                                                     //Natural: CLOSE PRINTER ( 01 )
    }
    private void sub_Report_Invalid() throws Exception                                                                                                                    //Natural: REPORT-INVALID
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().definePrinter(16, "'CMPRT15'");                                                                                                                      //Natural: DEFINE PRINTER ( 15 ) OUTPUT 'CMPRT15'
        pnd_Ws_Pnd_I.setValue(0);                                                                                                                                         //Natural: ASSIGN #WS.#I := 0
        getReports().write(15, NEWLINE,new TabSetting(14),"Form Count....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1),  //Natural: WRITE ( 15 ) / 14T 'Form Count....................' 6X #FORM-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 ) / 14T 'Forms Held....................' 6X #FORM-HOLD-CNT ( #I ) ( EM = -Z,ZZZ,ZZ9 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(14),"Forms Held....................",new ColumnSpacing(6),pdaTwra5006.getPnd_Twra5006_Pnd_Form_Hold_Cnt().getValue(pnd_Ws_Pnd_I.getInt() + 1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().closePrinter(16);                                                                                                                                    //Natural: CLOSE PRINTER ( 15 )
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=80 ZP=ON");
        Global.format(4, "PS=58 LS=80 ZP=ON");
        Global.format(5, "PS=58 LS=80 ZP=ON");
        Global.format(6, "PS=58 LS=80 ZP=ON");
        Global.format(7, "PS=58 LS=80 ZP=ON");
        Global.format(10, "PS=58 LS=80 ZP=ON");
        Global.format(15, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT1",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT2",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT3",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(29),"Contribution file rollup",new TabSetting(68),"Report: RPT4",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT5",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT6",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(68),"Report: RPT7",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"Payment file rollup",new TabSetting(67),"Report: RPT10",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(24),"Tax Withholding & Reporting System",new TabSetting(67),"Page: ",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(29),"Contribution file rollup",new TabSetting(67),"Report: RPT15",NEWLINE,new TabSetting(29),"Tax processing year",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(), 
            new SignPosition (false),NEWLINE,NEWLINE,"Company..: TIAA / CREF / LIFE / TCII / TRUST",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
    }
}
