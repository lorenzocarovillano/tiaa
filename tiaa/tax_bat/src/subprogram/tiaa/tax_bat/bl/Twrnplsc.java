/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:21:28 AM
**        * FROM NATURAL SUBPROGRAM : Twrnplsc
************************************************************
**        * FILE NAME            : Twrnplsc.java
**        * CLASS NAME           : Twrnplsc
**        * INSTANCE NAME        : Twrnplsc
************************************************************
************************************************************************
* SUBPROGRAM: TWRNPLSC
* FUNCTION..: LOAD SUNY/CUNY PLANS INTO ARRAY - TABLE 11
* PROGRAMMER: J.ROTHOLZ - 07/28/2012
* HISTORY...:
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrnplsc extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwraplsc pdaTwraplsc;
    private LdaTwrlplsc ldaTwrlplsc;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Plan_Super_Start;

    private DbsGroup pnd_Plan_Super_Start__R_Field_1;

    private DbsGroup pnd_Plan_Super_Start_Pnd_Plan_Super_Det;
    private DbsField pnd_Plan_Super_Start_Pnd_Act_Ind;
    private DbsField pnd_Plan_Super_Start_Pnd_Table_Id;
    private DbsField pnd_Plan_Super_End;
    private DbsField pnd_I;
    private DbsField pnd_Comm_Cnt;
    private DbsField pnd_Res_Cnt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlplsc = new LdaTwrlplsc();
        registerRecord(ldaTwrlplsc);
        registerRecord(ldaTwrlplsc.getVw_twrlplsc());

        // parameters
        parameters = new DbsRecord();
        pdaTwraplsc = new PdaTwraplsc(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Plan_Super_Start = localVariables.newFieldInRecord("pnd_Plan_Super_Start", "#PLAN-SUPER-START", FieldType.STRING, 7);

        pnd_Plan_Super_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Plan_Super_Start__R_Field_1", "REDEFINE", pnd_Plan_Super_Start);

        pnd_Plan_Super_Start_Pnd_Plan_Super_Det = pnd_Plan_Super_Start__R_Field_1.newGroupInGroup("pnd_Plan_Super_Start_Pnd_Plan_Super_Det", "#PLAN-SUPER-DET");
        pnd_Plan_Super_Start_Pnd_Act_Ind = pnd_Plan_Super_Start_Pnd_Plan_Super_Det.newFieldInGroup("pnd_Plan_Super_Start_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 
            1);
        pnd_Plan_Super_Start_Pnd_Table_Id = pnd_Plan_Super_Start_Pnd_Plan_Super_Det.newFieldInGroup("pnd_Plan_Super_Start_Pnd_Table_Id", "#TABLE-ID", 
            FieldType.STRING, 5);
        pnd_Plan_Super_End = localVariables.newFieldInRecord("pnd_Plan_Super_End", "#PLAN-SUPER-END", FieldType.STRING, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Comm_Cnt = localVariables.newFieldInRecord("pnd_Comm_Cnt", "#COMM-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Res_Cnt = localVariables.newFieldInRecord("pnd_Res_Cnt", "#RES-CNT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlplsc.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Plan_Super_Start.setInitialValue("ATX011");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrnplsc() throws Exception
    {
        super("Twrnplsc");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        pdaTwraplsc.getTwraplsc_Output_Data().reset();                                                                                                                    //Natural: RESET TWRAPLSC.OUTPUT-DATA
        pnd_Plan_Super_End.setValue(pnd_Plan_Super_Start);                                                                                                                //Natural: ASSIGN #PLAN-SUPER-END := #PLAN-SUPER-START
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Plan_Super_Start,7,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #PLAN-SUPER-START,7,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Plan_Super_End,7,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #PLAN-SUPER-END ,7,1 )
        ldaTwrlplsc.getVw_twrlplsc().startDatabaseRead                                                                                                                    //Natural: READ TWRLPLSC BY RT-SUPER1 = #PLAN-SUPER-START THRU #PLAN-SUPER-END
        (
        "RD1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Plan_Super_Start, "And", WcType.BY) ,
        new Wc("RT_SUPER1", "<=", pnd_Plan_Super_End, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        RD1:
        while (condition(ldaTwrlplsc.getVw_twrlplsc().readNextRow("RD1")))
        {
            if (condition(ldaTwrlplsc.getTwrlplsc_Rt_Pl_Comm_Ind().equals(true) && ldaTwrlplsc.getTwrlplsc_Rt_Pl_Res().equals("035")))                                    //Natural: IF TWRLPLSC.RT-PL-COMM-IND = TRUE AND TWRLPLSC.RT-PL-RES = '035'
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pdaTwraplsc.getTwraplsc_Pnd_Return_Code().setValue(true);                                                                                             //Natural: ASSIGN TWRAPLSC.#RETURN-CODE := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pdaTwraplsc.getTwraplsc_Rt_Plan().getValue(pnd_I).setValue(ldaTwrlplsc.getTwrlplsc_Rt_Plan());                                                            //Natural: MOVE TWRLPLSC.RT-PLAN TO TWRAPLSC.RT-PLAN ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pdaTwraplsc.getTwraplsc_Rt_Plan_Cnt().setValue(pnd_I);                                                                                                            //Natural: ASSIGN TWRAPLSC.RT-PLAN-CNT := #I
        if (condition(ldaTwrlplsc.getVw_twrlplsc().getAstCOUNTER().equals(getZero()) || pnd_I.equals(getZero())))                                                         //Natural: IF *COUNTER ( RD1. ) = 0 OR #I = 0
        {
            pdaTwraplsc.getTwraplsc_Pnd_Ret_Msg().setValue("Plan Array Load Failed");                                                                                     //Natural: ASSIGN TWRAPLSC.#RET-MSG := 'Plan Array Load Failed'
                                                                                                                                                                          //Natural: PERFORM ERROR-CONDITION
            sub_Error_Condition();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-CONDITION
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Condition() throws Exception                                                                                                                   //Natural: ERROR-CONDITION
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pdaTwraplsc.getTwraplsc_Pnd_Abend_Ind().getBoolean() || pdaTwraplsc.getTwraplsc_Pnd_Display_Ind().getBoolean()))                                    //Natural: IF #ABEND-IND OR #DISPLAY-IND
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Plan Array Load Failed",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 0 ) '***' 77T '***' / '***' 25T 'Plan Array Load Failed' 77T '***' / '***' 25T '   Contact Systems    ' 77T '***' / '***' 77T '***'
                TabSetting(25),"   Contact Systems    ",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwraplsc.getTwraplsc_Pnd_Abend_Ind().getBoolean()))                                                                                              //Natural: IF #ABEND-IND
        {
            DbsUtil.terminate(100);  if (true) return;                                                                                                                    //Natural: TERMINATE 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
