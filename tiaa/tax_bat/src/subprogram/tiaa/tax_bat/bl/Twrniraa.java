/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:21:02 AM
**        * FROM NATURAL SUBPROGRAM : Twrniraa
************************************************************
**        * FILE NAME            : Twrniraa.java
**        * CLASS NAME           : Twrniraa
**        * INSTANCE NAME        : Twrniraa
************************************************************
************************************************************************
* SUBPROGRAM: TWRNIRAA
* FUNCTION..: IRA ACCOUNT TYPE LOOKUP
* PROGRAMMER: MICHAEL SUPONITSKY - 05/18/2000
* HISTORY...:
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrniraa extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwrairaa pdaTwrairaa;
    private LdaTwrliraa ldaTwrliraa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Idx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrliraa = new LdaTwrliraa();
        registerRecord(ldaTwrliraa);

        // parameters
        parameters = new DbsRecord();
        pdaTwrairaa = new PdaTwrairaa(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrliraa.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrniraa() throws Exception
    {
        super("Twrniraa");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Output_Data().reset();                                                                                                            //Natural: RESET #TWRAIRAA.#OUTPUT-DATA
        if (condition(DbsUtil.maskMatches(pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type(),"99")))                                                                         //Natural: IF #TWRAIRAA.#IRA-ACCT-TYPE = MASK ( 99 )
        {
            pnd_Idx.compute(new ComputeParameters(false, pnd_Idx), pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().val());                                                //Natural: ASSIGN #IDX := VAL ( #TWRAIRAA.#IRA-ACCT-TYPE )
            if (condition(pnd_Idx.greaterOrEqual(getZero()) && pnd_Idx.lessOrEqual(ldaTwrliraa.getPnd_Twrliraa_Pnd_Max())))                                               //Natural: IF #IDX = 0 THRU #TWRLIRAA-#MAX
            {
                pdaTwrairaa.getPnd_Twrairaa_Pnd_Ret_Code().setValue(true);                                                                                                //Natural: ASSIGN #TWRAIRAA.#RET-CODE := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Idx.reset();                                                                                                                                          //Natural: RESET #IDX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Short().setValue(ldaTwrliraa.getPnd_Twrliraa_Pnd_Iraa_Short().getValue(pnd_Idx.getInt() + 1));                               //Natural: ASSIGN #TWRAIRAA.#IRAA-SHORT := #TWRLIRAA.#IRAA-SHORT ( #IDX )
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Desc().setValue(ldaTwrliraa.getPnd_Twrliraa_Pnd_Iraa_Desc().getValue(pnd_Idx.getInt() + 1));                                 //Natural: ASSIGN #TWRAIRAA.#IRAA-DESC := #TWRLIRAA.#IRAA-DESC ( #IDX )
        if (condition(pnd_Idx.equals(getZero())))                                                                                                                         //Natural: IF #IDX = 0
        {
            pdaTwrairaa.getPnd_Twrairaa_Pnd_Ret_Msg().setValue("Unknown IRA Account Type.");                                                                              //Natural: ASSIGN #TWRAIRAA.#RET-MSG := 'Unknown IRA Account Type.'
                                                                                                                                                                          //Natural: PERFORM ERROR-CONDITION
            sub_Error_Condition();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************
        //*  S U B R O U T I N E S
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-CONDITION
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Condition() throws Exception                                                                                                                   //Natural: ERROR-CONDITION
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pdaTwrairaa.getPnd_Twrairaa_Pnd_Abend_Ind().getBoolean() || pdaTwrairaa.getPnd_Twrairaa_Pnd_Display_Ind().getBoolean()))                            //Natural: IF #ABEND-IND OR #DISPLAY-IND
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),pdaTwrairaa.getPnd_Twrairaa_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new      //Natural: WRITE ( 0 ) '***' 25T #RET-MSG 77T '***' / '***' 25T 'IRA Account Type:' #TWRAIRAA.#IRA-ACCT-TYPE 77T '***'
                TabSetting(25),"IRA Account Type:",pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwrairaa.getPnd_Twrairaa_Pnd_Abend_Ind().getBoolean()))                                                                                          //Natural: IF #ABEND-IND
        {
            DbsUtil.terminate(100);  if (true) return;                                                                                                                    //Natural: TERMINATE 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
