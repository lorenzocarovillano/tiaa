/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:15:51 AM
**        * FROM NATURAL SUBPROGRAM : Twrn0904
************************************************************
**        * FILE NAME            : Twrn0904.java
**        * CLASS NAME           : Twrn0904
**        * INSTANCE NAME        : Twrn0904
************************************************************
* NAME       : TWRN0904
* FUNCTION   : CREATE TABLE WHICH WILL CONTAIN COUNTRY CODE
*                AND COUNTRY NAME WHICH WILL BE USED TO IDENTIFY THE
*                NAME OF THE COUNTRY WHERE U.S. CITIZENS OR RESIDENT
*                ALIENS ARE LIVING.
* INPUT      : FILE 98 , TABLE 3 (COUNTRY CODE TABLE)
* WRITTEN BY : EDITH  10/6/99
* UPDATES    :
*******************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn0904 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Tax_Yr;
    private DbsField pnd_Cntry_Ind;
    private DbsField pnd_Cntry_Name;

    private DataAccessProgramView vw_f98_Cntry_View;
    private DbsField f98_Cntry_View_Tircntl_Tbl_Nbr;
    private DbsField f98_Cntry_View_Tircntl_Tax_Year;
    private DbsField f98_Cntry_View_Tircntl_Cntry_Alpha_Code;
    private DbsField f98_Cntry_View_Tircntl_Cntry_Full_Name;
    private DbsField f98_Cntry_View_Tircntl_Nbr_Year_Alpha_Cde;
    private DbsField pnd_Tax_Yr_A;

    private DbsGroup pnd_Tax_Yr_A__R_Field_1;
    private DbsField pnd_Tax_Yr_A_Pnd_Tax_Yr_N;
    private DbsField pnd_Key;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Tax_Yr = parameters.newFieldInRecord("pnd_Tax_Yr", "#TAX-YR", FieldType.STRING, 4);
        pnd_Tax_Yr.setParameterOption(ParameterOption.ByReference);
        pnd_Cntry_Ind = parameters.newFieldArrayInRecord("pnd_Cntry_Ind", "#CNTRY-IND", FieldType.STRING, 2, new DbsArrayController(1, 350));
        pnd_Cntry_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Cntry_Name = parameters.newFieldArrayInRecord("pnd_Cntry_Name", "#CNTRY-NAME", FieldType.STRING, 35, new DbsArrayController(1, 350));
        pnd_Cntry_Name.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_f98_Cntry_View = new DataAccessProgramView(new NameInfo("vw_f98_Cntry_View", "F98-CNTRY-VIEW"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        f98_Cntry_View_Tircntl_Tbl_Nbr = vw_f98_Cntry_View.getRecord().newFieldInGroup("f98_Cntry_View_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_TBL_NBR");
        f98_Cntry_View_Tircntl_Tax_Year = vw_f98_Cntry_View.getRecord().newFieldInGroup("f98_Cntry_View_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_TAX_YEAR");
        f98_Cntry_View_Tircntl_Cntry_Alpha_Code = vw_f98_Cntry_View.getRecord().newFieldInGroup("f98_Cntry_View_Tircntl_Cntry_Alpha_Code", "TIRCNTL-CNTRY-ALPHA-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ALPHA_CODE");
        f98_Cntry_View_Tircntl_Cntry_Full_Name = vw_f98_Cntry_View.getRecord().newFieldInGroup("f98_Cntry_View_Tircntl_Cntry_Full_Name", "TIRCNTL-CNTRY-FULL-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FULL_NAME");
        f98_Cntry_View_Tircntl_Nbr_Year_Alpha_Cde = vw_f98_Cntry_View.getRecord().newFieldInGroup("f98_Cntry_View_Tircntl_Nbr_Year_Alpha_Cde", "TIRCNTL-NBR-YEAR-ALPHA-CDE", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "TIRCNTL_NBR_YEAR_ALPHA_CDE");
        f98_Cntry_View_Tircntl_Nbr_Year_Alpha_Cde.setSuperDescriptor(true);
        registerRecord(vw_f98_Cntry_View);

        pnd_Tax_Yr_A = localVariables.newFieldInRecord("pnd_Tax_Yr_A", "#TAX-YR-A", FieldType.STRING, 4);

        pnd_Tax_Yr_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Tax_Yr_A__R_Field_1", "REDEFINE", pnd_Tax_Yr_A);
        pnd_Tax_Yr_A_Pnd_Tax_Yr_N = pnd_Tax_Yr_A__R_Field_1.newFieldInGroup("pnd_Tax_Yr_A_Pnd_Tax_Yr_N", "#TAX-YR-N", FieldType.NUMERIC, 4);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_f98_Cntry_View.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn0904() throws Exception
    {
        super("Twrn0904");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Tax_Yr_A.setValue(pnd_Tax_Yr);                                                                                                                                //Natural: ASSIGN #TAX-YR-A := #TAX-YR
        pnd_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "3", pnd_Tax_Yr, "AA"));                                                                         //Natural: COMPRESS '3' #TAX-YR 'AA' INTO #KEY LEAVING NO SPACE
        vw_f98_Cntry_View.startDatabaseRead                                                                                                                               //Natural: READ F98-CNTRY-VIEW WITH TIRCNTL-NBR-YEAR-ALPHA-CDE = #KEY
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_ALPHA_CDE", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_ALPHA_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_f98_Cntry_View.readNextRow("READ01")))
        {
            if (condition(f98_Cntry_View_Tircntl_Tbl_Nbr.equals(3) && f98_Cntry_View_Tircntl_Tax_Year.equals(pnd_Tax_Yr_A_Pnd_Tax_Yr_N) && f98_Cntry_View_Tircntl_Cntry_Alpha_Code.greaterOrEqual("AA")  //Natural: IF TIRCNTL-TBL-NBR = 3 AND TIRCNTL-TAX-YEAR = #TAX-YR-N AND TIRCNTL-CNTRY-ALPHA-CODE = 'AA' THRU 'ZZ'
                && f98_Cntry_View_Tircntl_Cntry_Alpha_Code.lessOrEqual("ZZ")))
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Cntry_Ind.getValue(pnd_I).setValue(f98_Cntry_View_Tircntl_Cntry_Alpha_Code);                                                                          //Natural: ASSIGN #CNTRY-IND ( #I ) := TIRCNTL-CNTRY-ALPHA-CODE
                pnd_Cntry_Name.getValue(pnd_I).setValue(f98_Cntry_View_Tircntl_Cntry_Full_Name);                                                                          //Natural: ASSIGN #CNTRY-NAME ( #I ) := TIRCNTL-CNTRY-FULL-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
