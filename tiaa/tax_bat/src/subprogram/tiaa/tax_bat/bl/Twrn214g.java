/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:16:46 AM
**        * FROM NATURAL SUBPROGRAM : Twrn214g
************************************************************
**        * FILE NAME            : Twrn214g.java
**        * CLASS NAME           : Twrn214g
**        * INSTANCE NAME        : Twrn214g
************************************************************
************************************************************************
** PROGRAM:  TWRN214G - PARTICIPANT LEVEL REPORTING
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  SNEHA SINHA
** PURPOSE:  SUBPROGRAM TO SEND SUNY/CUNY LETTER DATA TO POST
**           FOR TAX REFACTOR PROJECT
**
** HISTORY:
** 10/13/2020 - RE-STOW COMPONENT FOR 5498      /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING 2020
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn214g extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra214g pdaTwra214g;
    private PdaPsta9610 pdaPsta9610;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaTwrl114g ldaTwrl114g;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaPsta9531 pdaPsta9531;
    private PdaPsta6396 pdaPsta6396;
    private PdaPsta6346 pdaPsta6346;
    private PdaPsta9612 pdaPsta9612;
    private PdaTwraplr2 pdaTwraplr2;
    private PdaTwraclr2 pdaTwraclr2;
    private PdaTwraptr2 pdaTwraptr2;
    private PdaTwratin pdaTwratin;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private LdaTwrl6395 ldaTwrl6395;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Ltr_Array;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Tin;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Cntrct;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Data;
    private DbsField pnd_Additional_Forms;
    private DbsField pnd_Test_Ind;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text;
    private DbsField pnd_Form_Sd_3_Start;

    private DbsGroup pnd_Form_Sd_3_Start__R_Field_1;

    private DbsGroup pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail;
    private DbsField pnd_Form_Sd_3_Start_Pnd_Read_Key_Tax_Year;
    private DbsField pnd_Form_Sd_3_Start_Pnd_Read_Key_Active_Ind;
    private DbsField pnd_Form_Sd_3_Start_Pnd_Read_Key_Form_Type;
    private DbsField pnd_Form_Sd_3_Start_Pnd_Read_Key_Company_Cde;
    private DbsField pnd_Form_Sd_3_Start_Pnd_Read_Key_Tin;
    private DbsField pnd_Form_Sd_3_End;

    private DataAccessProgramView vw_twrparti_Participant_File;
    private DbsField twrparti_Participant_File_Twrparti_Status;
    private DbsField twrparti_Participant_File_Twrparti_Tax_Id;
    private DbsField twrparti_Participant_File_Twrparti_Part_Eff_End_Dte;
    private DbsField twrparti_Participant_File_Twrparti_Test_Rec_Ind;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_2;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Fatal_Err;
    private DbsField pnd_First_Write;
    private DbsField pnd_Debug;
    private DbsField pnd_Cover_Letter_Ind;
    private DbsField pnd_Plan_Name_Long;
    private DbsField pnd_Ssn_Pstn6396;
    private DbsField pnd_Contract_Txt;
    private DbsField pnd_Include_Cntrct_In_Ny_Ltr;

    private DbsGroup pnd_Sunny_Aa;
    private DbsField pnd_Sunny_Aa_Pnd_Cover_Rec_Id_Aa;
    private DbsField pnd_Sunny_Aa_Pnd_Year;
    private DbsField pnd_Sunny_Aa_Pnd_Letter_Id;
    private DbsField pnd_Sunny_Aa_Pnd_Institution_Ind;
    private DbsField pnd_Sunny_Aa_Pnd_Multi_Ia_Income_Ind;
    private DbsField pnd_Sunny_Aa_Pnd_Ira_Rollover_Ind;

    private DbsGroup pnd_Sunny_Dtl;
    private DbsField pnd_Sunny_Dtl_Pnd_Form_Type;
    private DbsField pnd_Sunny_Dtl_Pnd_Form_Year;
    private DbsField pnd_Sunny_Dtl_Pnd_Number_Of_Forms;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_1099r_Instruction;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_5498f_Instruction;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_5498p_Instruction;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_Nr4i_Instruction;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_4806a_Instruction;
    private DbsField pnd_Sunny_Dtl_Pnd_Dont_Pull_1042s_Instruction;

    private DbsGroup pnd_Sunny_Dtl_Pnd_Mobius_Key;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Pin;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Year;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Form_Type;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Contract;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Payee;
    private DbsField pnd_Sunny_Dtl_Pnd_Mobius_Key_Key;
    private DbsField pnd_Sunny_Dtl_Pnd_Test_Indicator;
    private DbsField pnd_Sunny_Dtl_Pnd_5498_Coupon_Ind;
    private DbsField pnd_Sunny_Dtl_Pnd_Future_Use;
    private DbsField pnd_Mobindx;

    private DbsGroup pnd_Mobindx__R_Field_3;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind;
    private DbsField pnd_Mobindx_Pnd_Wk_Mobindx;
    private DbsField pnd_Tirf_Tin;

    private DbsGroup pnd_Tirf_Tin__R_Field_4;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N9;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N1;
    private DbsField pnd_Key_Data;

    private DbsGroup pnd_Key_Data__R_Field_5;
    private DbsField pnd_Key_Data_Pnd_Kd_Pckg_Code;
    private DbsField pnd_Key_Data_Pnd_Kd_Date_Time;
    private DbsField pnd_Key_Data_Pnd_Kd_Univ_Id_Type;
    private DbsField pnd_Key_Data_Pnd_Kd_Univ_Id;
    private DbsField pnd_Key_Data_Pnd_Kd_Recipient_Seq;
    private DbsField pnd_Record_Seq;
    private DbsField pnd_Rec_Type;
    private DbsField pnd_Detail_Id;
    private DbsField pnd_Form_Type1;
    private DbsField pnd_Sc_Cont_Tot_Income_Amt;
    private DbsField pnd_Sc_Cont_Tot_Income_Pct;
    private DbsField pnd_Sc_Plan_Income;
    private DbsField pnd_Sc_Plan_Income_Pct;
    private DbsField pnd_Detail_Cnt;
    private DbsField pnd_Ws_Tax_Yeara;

    private DbsGroup pnd_Ws_Tax_Yeara__R_Field_6;
    private DbsField pnd_Ws_Tax_Yeara_Pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Yeara__R_Field_7;
    private DbsField pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Cc;
    private DbsField pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Yy;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaTwrl114g = new LdaTwrl114g();
        registerRecord(ldaTwrl114g);
        registerRecord(ldaTwrl114g.getVw_form_File());
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        pdaPsta9531 = new PdaPsta9531(localVariables);
        pdaPsta6396 = new PdaPsta6396(localVariables);
        pdaPsta6346 = new PdaPsta6346(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaTwraplr2 = new PdaTwraplr2(localVariables);
        pdaTwraclr2 = new PdaTwraclr2(localVariables);
        pdaTwraptr2 = new PdaTwraptr2(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        ldaTwrl6395 = new LdaTwrl6395();
        registerRecord(ldaTwrl6395);

        // parameters
        parameters = new DbsRecord();
        pdaTwra214g = new PdaTwra214g(parameters);
        pdaPsta9610 = new PdaPsta9610(parameters);
        pdaTwra0214 = new PdaTwra0214(parameters);

        pnd_Ltr_Array = parameters.newGroupArrayInRecord("pnd_Ltr_Array", "#LTR-ARRAY", new DbsArrayController(1, 999));
        pnd_Ltr_Array.setParameterOption(ParameterOption.ByReference);
        pnd_Ltr_Array_Pnd_Ltr_Tin = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Tin", "#LTR-TIN", FieldType.STRING, 10);
        pnd_Ltr_Array_Pnd_Ltr_Cntrct = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Cntrct", "#LTR-CNTRCT", FieldType.STRING, 9);
        pnd_Ltr_Array_Pnd_Ltr_Data = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Data", "#LTR-DATA", FieldType.STRING, 1);
        pnd_Additional_Forms = parameters.newFieldInRecord("pnd_Additional_Forms", "#ADDITIONAL-FORMS", FieldType.PACKED_DECIMAL, 7);
        pnd_Additional_Forms.setParameterOption(ParameterOption.ByReference);
        pnd_Test_Ind = parameters.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Test_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Mobius_Psta9960_Parm_Data = localVariables.newGroupInRecord("pnd_Mobius_Psta9960_Parm_Data", "#MOBIUS-PSTA9960-PARM-DATA");
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id", 
            "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count", "#DTLS-COUNT", 
            FieldType.PACKED_DECIMAL, 5);

        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array = pnd_Mobius_Psta9960_Parm_Data.newGroupArrayInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array", 
            "#DTLS-ARRAY", new DbsArrayController(1, 999));
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde", 
            "#DCMNT-CDE", FieldType.STRING, 8);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde", 
            "#DCMNT-VRSN-CDE", FieldType.STRING, 2);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind", 
            "#INDEX-IND", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert", 
            "#MAN-INSERT", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text", 
            "#FREE-TEXT", FieldType.STRING, 40);
        pnd_Form_Sd_3_Start = localVariables.newFieldInRecord("pnd_Form_Sd_3_Start", "#FORM-SD-3-START", FieldType.STRING, 19);

        pnd_Form_Sd_3_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Form_Sd_3_Start__R_Field_1", "REDEFINE", pnd_Form_Sd_3_Start);

        pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail = pnd_Form_Sd_3_Start__R_Field_1.newGroupInGroup("pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail", "#FORM-SD-3-DETAIL");
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Tax_Year = pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail.newFieldInGroup("pnd_Form_Sd_3_Start_Pnd_Read_Key_Tax_Year", 
            "#READ-KEY-TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Active_Ind = pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail.newFieldInGroup("pnd_Form_Sd_3_Start_Pnd_Read_Key_Active_Ind", 
            "#READ-KEY-ACTIVE-IND", FieldType.STRING, 1);
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Form_Type = pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail.newFieldInGroup("pnd_Form_Sd_3_Start_Pnd_Read_Key_Form_Type", 
            "#READ-KEY-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Company_Cde = pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail.newFieldInGroup("pnd_Form_Sd_3_Start_Pnd_Read_Key_Company_Cde", 
            "#READ-KEY-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Tin = pnd_Form_Sd_3_Start_Pnd_Form_Sd_3_Detail.newFieldInGroup("pnd_Form_Sd_3_Start_Pnd_Read_Key_Tin", "#READ-KEY-TIN", 
            FieldType.STRING, 10);
        pnd_Form_Sd_3_End = localVariables.newFieldInRecord("pnd_Form_Sd_3_End", "#FORM-SD-3-END", FieldType.STRING, 19);

        vw_twrparti_Participant_File = new DataAccessProgramView(new NameInfo("vw_twrparti_Participant_File", "TWRPARTI-PARTICIPANT-FILE"), "TWRPARTI_PARTICIPANT_FILE", 
            "TWR_PARTICIPANT");
        twrparti_Participant_File_Twrparti_Status = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Status", 
            "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Participant_File_Twrparti_Tax_Id = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Tax_Id", 
            "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Participant_File_Twrparti_Part_Eff_End_Dte = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Part_Eff_End_Dte", 
            "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        twrparti_Participant_File_Twrparti_Test_Rec_Ind = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Test_Rec_Ind", 
            "TWRPARTI-TEST-REC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_TEST_REC_IND");
        registerRecord(vw_twrparti_Participant_File);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_2", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id", 
            "#TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status", 
            "#TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte", 
            "#TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Fatal_Err = localVariables.newFieldInRecord("pnd_Fatal_Err", "#FATAL-ERR", FieldType.BOOLEAN, 1);
        pnd_First_Write = localVariables.newFieldInRecord("pnd_First_Write", "#FIRST-WRITE", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Cover_Letter_Ind = localVariables.newFieldInRecord("pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", FieldType.BOOLEAN, 1);
        pnd_Plan_Name_Long = localVariables.newFieldInRecord("pnd_Plan_Name_Long", "#PLAN-NAME-LONG", FieldType.STRING, 65);
        pnd_Ssn_Pstn6396 = localVariables.newFieldInRecord("pnd_Ssn_Pstn6396", "#SSN-PSTN6396", FieldType.STRING, 11);
        pnd_Contract_Txt = localVariables.newFieldInRecord("pnd_Contract_Txt", "#CONTRACT-TXT", FieldType.STRING, 9);
        pnd_Include_Cntrct_In_Ny_Ltr = localVariables.newFieldInRecord("pnd_Include_Cntrct_In_Ny_Ltr", "#INCLUDE-CNTRCT-IN-NY-LTR", FieldType.BOOLEAN, 
            1);

        pnd_Sunny_Aa = localVariables.newGroupInRecord("pnd_Sunny_Aa", "#SUNNY-AA");
        pnd_Sunny_Aa_Pnd_Cover_Rec_Id_Aa = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Cover_Rec_Id_Aa", "#COVER-REC-ID-AA", FieldType.STRING, 2);
        pnd_Sunny_Aa_Pnd_Year = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Sunny_Aa_Pnd_Letter_Id = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Letter_Id", "#LETTER-ID", FieldType.STRING, 4);
        pnd_Sunny_Aa_Pnd_Institution_Ind = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Institution_Ind", "#INSTITUTION-IND", FieldType.STRING, 1);
        pnd_Sunny_Aa_Pnd_Multi_Ia_Income_Ind = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Multi_Ia_Income_Ind", "#MULTI-IA-INCOME-IND", FieldType.STRING, 
            1);
        pnd_Sunny_Aa_Pnd_Ira_Rollover_Ind = pnd_Sunny_Aa.newFieldInGroup("pnd_Sunny_Aa_Pnd_Ira_Rollover_Ind", "#IRA-ROLLOVER-IND", FieldType.STRING, 1);

        pnd_Sunny_Dtl = localVariables.newGroupInRecord("pnd_Sunny_Dtl", "#SUNNY-DTL");
        pnd_Sunny_Dtl_Pnd_Form_Type = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Form_Type", "#FORM-TYPE", FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Form_Year = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Form_Year", "#FORM-YEAR", FieldType.STRING, 2);
        pnd_Sunny_Dtl_Pnd_Number_Of_Forms = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Number_Of_Forms", "#NUMBER-OF-FORMS", FieldType.NUMERIC, 
            3);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_1099r_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_1099r_Instruction", "#DONT-PULL-1099R-INSTRUCTION", 
            FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_5498f_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_5498f_Instruction", "#DONT-PULL-5498F-INSTRUCTION", 
            FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_5498p_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_5498p_Instruction", "#DONT-PULL-5498P-INSTRUCTION", 
            FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_Nr4i_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_Nr4i_Instruction", "#DONT-PULL-NR4I-INSTRUCTION", 
            FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_4806a_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_4806a_Instruction", "#DONT-PULL-4806A-INSTRUCTION", 
            FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Dont_Pull_1042s_Instruction = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Dont_Pull_1042s_Instruction", "#DONT-PULL-1042S-INSTRUCTION", 
            FieldType.STRING, 1);

        pnd_Sunny_Dtl_Pnd_Mobius_Key = pnd_Sunny_Dtl.newGroupInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key", "#MOBIUS-KEY");
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Pin = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Pin", "#MOBIUS-KEY-PIN", FieldType.NUMERIC, 
            12);
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Year = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Year", "#MOBIUS-KEY-YEAR", FieldType.STRING, 
            2);
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Form_Type = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Form_Type", "#MOBIUS-KEY-FORM-TYPE", 
            FieldType.STRING, 2);
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Contract = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Contract", "#MOBIUS-KEY-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Payee = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Payee", "#MOBIUS-KEY-PAYEE", FieldType.STRING, 
            2);
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Key = pnd_Sunny_Dtl_Pnd_Mobius_Key.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Mobius_Key_Key", "#MOBIUS-KEY-KEY", FieldType.STRING, 
            9);
        pnd_Sunny_Dtl_Pnd_Test_Indicator = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Test_Indicator", "#TEST-INDICATOR", FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_5498_Coupon_Ind = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_5498_Coupon_Ind", "#5498-COUPON-IND", FieldType.STRING, 1);
        pnd_Sunny_Dtl_Pnd_Future_Use = pnd_Sunny_Dtl.newFieldInGroup("pnd_Sunny_Dtl_Pnd_Future_Use", "#FUTURE-USE", FieldType.STRING, 7);
        pnd_Mobindx = localVariables.newFieldInRecord("pnd_Mobindx", "#MOBINDX", FieldType.STRING, 9);

        pnd_Mobindx__R_Field_3 = localVariables.newGroupInRecord("pnd_Mobindx__R_Field_3", "REDEFINE", pnd_Mobindx);
        pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator = pnd_Mobindx__R_Field_3.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator", "#WK-MMM-TEST-INDICATOR", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind = pnd_Mobindx__R_Field_3.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind", "#WK-MMM-5498-COUPON-IND", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mobindx = pnd_Mobindx__R_Field_3.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mobindx", "#WK-MOBINDX", FieldType.STRING, 7);
        pnd_Tirf_Tin = localVariables.newFieldInRecord("pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);

        pnd_Tirf_Tin__R_Field_4 = localVariables.newGroupInRecord("pnd_Tirf_Tin__R_Field_4", "REDEFINE", pnd_Tirf_Tin);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N9 = pnd_Tirf_Tin__R_Field_4.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N9", "#TIRF-TIN-N9", FieldType.NUMERIC, 9);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N1 = pnd_Tirf_Tin__R_Field_4.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N1", "#TIRF-TIN-N1", FieldType.NUMERIC, 1);
        pnd_Key_Data = localVariables.newFieldInRecord("pnd_Key_Data", "#KEY-DATA", FieldType.STRING, 44);

        pnd_Key_Data__R_Field_5 = localVariables.newGroupInRecord("pnd_Key_Data__R_Field_5", "REDEFINE", pnd_Key_Data);
        pnd_Key_Data_Pnd_Kd_Pckg_Code = pnd_Key_Data__R_Field_5.newFieldInGroup("pnd_Key_Data_Pnd_Kd_Pckg_Code", "#KD-PCKG-CODE", FieldType.STRING, 8);
        pnd_Key_Data_Pnd_Kd_Date_Time = pnd_Key_Data__R_Field_5.newFieldInGroup("pnd_Key_Data_Pnd_Kd_Date_Time", "#KD-DATE-TIME", FieldType.STRING, 15);
        pnd_Key_Data_Pnd_Kd_Univ_Id_Type = pnd_Key_Data__R_Field_5.newFieldInGroup("pnd_Key_Data_Pnd_Kd_Univ_Id_Type", "#KD-UNIV-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Key_Data_Pnd_Kd_Univ_Id = pnd_Key_Data__R_Field_5.newFieldInGroup("pnd_Key_Data_Pnd_Kd_Univ_Id", "#KD-UNIV-ID", FieldType.NUMERIC, 13);
        pnd_Key_Data_Pnd_Kd_Recipient_Seq = pnd_Key_Data__R_Field_5.newFieldInGroup("pnd_Key_Data_Pnd_Kd_Recipient_Seq", "#KD-RECIPIENT-SEQ", FieldType.NUMERIC, 
            7);
        pnd_Record_Seq = localVariables.newFieldInRecord("pnd_Record_Seq", "#RECORD-SEQ", FieldType.NUMERIC, 7);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 1);
        pnd_Detail_Id = localVariables.newFieldInRecord("pnd_Detail_Id", "#DETAIL-ID", FieldType.STRING, 2);
        pnd_Form_Type1 = localVariables.newFieldInRecord("pnd_Form_Type1", "#FORM-TYPE1", FieldType.STRING, 1);
        pnd_Sc_Cont_Tot_Income_Amt = localVariables.newFieldInRecord("pnd_Sc_Cont_Tot_Income_Amt", "#SC-CONT-TOT-INCOME-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Sc_Cont_Tot_Income_Pct = localVariables.newFieldInRecord("pnd_Sc_Cont_Tot_Income_Pct", "#SC-CONT-TOT-INCOME-PCT", FieldType.PACKED_DECIMAL, 
            15, 4);
        pnd_Sc_Plan_Income = localVariables.newFieldArrayInRecord("pnd_Sc_Plan_Income", "#SC-PLAN-INCOME", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            20));
        pnd_Sc_Plan_Income_Pct = localVariables.newFieldArrayInRecord("pnd_Sc_Plan_Income_Pct", "#SC-PLAN-INCOME-PCT", FieldType.PACKED_DECIMAL, 7, 4, 
            new DbsArrayController(1, 20));
        pnd_Detail_Cnt = localVariables.newFieldInRecord("pnd_Detail_Cnt", "#DETAIL-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Tax_Yeara = localVariables.newFieldInRecord("pnd_Ws_Tax_Yeara", "#WS-TAX-YEARA", FieldType.STRING, 4);

        pnd_Ws_Tax_Yeara__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Tax_Yeara__R_Field_6", "REDEFINE", pnd_Ws_Tax_Yeara);
        pnd_Ws_Tax_Yeara_Pnd_Ws_Tax_Year = pnd_Ws_Tax_Yeara__R_Field_6.newFieldInGroup("pnd_Ws_Tax_Yeara_Pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 
            4);

        pnd_Ws_Tax_Yeara__R_Field_7 = pnd_Ws_Tax_Yeara__R_Field_6.newGroupInGroup("pnd_Ws_Tax_Yeara__R_Field_7", "REDEFINE", pnd_Ws_Tax_Yeara_Pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Cc = pnd_Ws_Tax_Yeara__R_Field_7.newFieldInGroup("pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Cc", "#WS-FORM-CC", FieldType.STRING, 
            2);
        pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Yy = pnd_Ws_Tax_Yeara__R_Field_7.newFieldInGroup("pnd_Ws_Tax_Yeara_Pnd_Ws_Form_Yy", "#WS-FORM-YY", FieldType.STRING, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_twrparti_Participant_File.reset();

        ldaTwrl114g.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl6395.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_First_Write.setInitialValue(true);
        pnd_Debug.setInitialValue(false);
        pnd_Cover_Letter_Ind.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn214g() throws Exception
    {
        super("Twrn214g");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *************************************
        //* ** MAIN PROGRAM LOGIC BEGINS HERE ***
        //* *************************************
        //*  MOVE TRUE TO #DEBUG                                                                                                                                          //Natural: FORMAT PS = 80 LS = 130
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"- STARTING");                                                                                                      //Natural: WRITE *PROGRAM '- STARTING'
            if (Global.isEscape()) return;
            //*  SINHSN
            //*  SINHSN
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Tax_Year.setValue(pdaTwra214g.getPnd_Twra214g_Pnd_Tax_Year());                                                                   //Natural: ASSIGN #READ-KEY-TAX-YEAR := #TWRA214G.#TAX-YEAR
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Active_Ind.setValue("A");                                                                                                        //Natural: ASSIGN #READ-KEY-ACTIVE-IND := 'A'
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Form_Type.setValue(pdaTwra214g.getPnd_Twra214g_Pnd_Form_Type());                                                                 //Natural: ASSIGN #READ-KEY-FORM-TYPE := #TWRA214G.#FORM-TYPE
        //*  SINHSN
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Company_Cde.reset();                                                                                                             //Natural: RESET #READ-KEY-COMPANY-CDE
        pnd_Form_Sd_3_Start_Pnd_Read_Key_Tin.setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin());                                                          //Natural: ASSIGN #READ-KEY-TIN := #TWRA0214-TIN
        pnd_Form_Sd_3_End.setValue(pnd_Form_Sd_3_Start);                                                                                                                  //Natural: ASSIGN #FORM-SD-3-END := #FORM-SD-3-START
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Form_Sd_3_Start,19,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #FORM-SD-3-START,19,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Form_Sd_3_End,19,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #FORM-SD-3-END ,19,1 )
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"FORM-FILE KEY IS: ",pnd_Form_Sd_3_Start);                                                                          //Natural: WRITE *PROGRAM 'FORM-FILE KEY IS: ' #FORM-SD-3-START
            if (Global.isEscape()) return;
            //*  SINHSN
        }                                                                                                                                                                 //Natural: END-IF
        //*  RD1. READ FORM-FILE BY TIRF-SUPERDE-3
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY TIRF-SUPERDE-3 = #FORM-SD-3-START THRU #FORM-SD-3-END
        (
        "RD1",
        new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Form_Sd_3_Start, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_3", "<=", pnd_Form_Sd_3_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
        );
        RD1:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD1")))
        {
            //*                                                 /* DUTTAD >>
            //*   /* SNEHA CHANGES STARTED - SINHSN
            pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                             //Natural: RESET #TWRPARTI-CURR-REC-SD
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                        //Natural: MOVE FORM.TIRF-TIN TO #TWRPARTI-TAX-ID
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #TWRPARTI-STATUS
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                  //Natural: MOVE '99999999' TO #TWRPARTI-PART-EFF-END-DTE
            //* * SAIK
            if (condition(ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(" ")))                                                                                           //Natural: IF TIRF-CONTRACT-NBR = ' '
            {
                vw_twrparti_Participant_File.startDatabaseFind                                                                                                            //Natural: FIND ( 1 ) TWRPARTI-PARTICIPANT-FILE TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
                (
                "F1",
                new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", "=", pnd_Twrparti_Curr_Rec_Sd, WcType.WITH) },
                1
                );
                F1:
                while (condition(vw_twrparti_Participant_File.readNextRow("F1", true)))
                {
                    vw_twrparti_Participant_File.setIfNotFoundControlFlag(false);
                    if (condition(vw_twrparti_Participant_File.getAstCOUNTER().equals(0)))                                                                                //Natural: IF NO RECORDS FOUND
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                      //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #WK-MMM-TEST-INDICATOR
                    pnd_Test_Ind.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                                               //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #TEST-IND
                    //*       WRITE 'sneha twrn214g #TEST-IND' #TEST-IND
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                      //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #WK-MMM-TEST-INDICATOR
                    if (condition(pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.equals("Y")))                                                                                     //Natural: IF #WK-MMM-TEST-INDICATOR EQ 'Y'
                    {
                        pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue("Y");                                                                                              //Natural: MOVE 'Y' TO #WK-MMM-TEST-INDICATOR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(" ");                                                                                              //Natural: MOVE ' ' TO #WK-MMM-TEST-INDICATOR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* * SAIK
            }                                                                                                                                                             //Natural: END-IF
            //*  /* SNEHA CHANGES ENDED - SINHSN
            if (condition(ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(" ")))                                                                                           //Natural: IF TIRF-CONTRACT-NBR = ' '
            {
                pdaPsta9610.getPsta9610_Write_Sqnce_Nbr().reset();                                                                                                        //Natural: RESET PSTA9610.WRITE-SQNCE-NBR
                //*    PERFORM CALL-GET-FORM-CNTRL-DTL          /* SINHSN
                //*  SAIK
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                sub_Call_Post();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SAIK
                                                                                                                                                                          //Natural: PERFORM CALL-AA-REC-DET
                sub_Call_Aa_Rec_Det();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Record_Seq.setValue(0);                                                                                                                               //Natural: ASSIGN #RECORD-SEQ := 0000000
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Txt.setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX'-'X"));                                                   //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
            DbsUtil.examine(new ExamineSource(pnd_Ltr_Array_Pnd_Ltr_Cntrct.getValue("*")), new ExamineSearch(pnd_Contract_Txt), new ExamineGivingIndex(pnd_K));           //Natural: EXAMINE #LTR-ARRAY.#LTR-CNTRCT ( * ) FOR #CONTRACT-TXT GIVING INDEX #K
            if (condition(pnd_K.greater(getZero())))                                                                                                                      //Natural: IF #K GT 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-GET-FORM-CNTRL-DTL
            sub_Call_Get_Form_Cntrl_Dtl();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CALL-POST-LETTER-DET
            sub_Call_Post_Letter_Det();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaPsta9610.getPsta9610_Write_Sqnce_Nbr().nadd(1);                                                                                                            //Natural: ADD 1 TO PSTA9610.WRITE-SQNCE-NBR
            //*  PERFORM CALL-POST-LETTER-TLR                     /* SINHSN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PERFORM CALL-POST-LETTER-TLR
        //*  CALL POST CLOSE
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
        //*  PIN-NBR             := TIRF-PIN
        //*  UNIV-ID-TYP         := #UNIV-ID-TYP
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("POST CLOSE -- Call Failed (PSTN9685)");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'POST CLOSE -- Call Failed (PSTN9685)'
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-GET-FORM-CNTRL-DTL
        //*  SC-COVER-LETTER-DATA.FORM-TYPE  := 'B'
        //*  WRITE (0) 'FORM.TIRF-PAYEE-CDE : ' FORM.TIRF-PAYEE-CDE
        //* **************************           << SAIK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST
        //*  CALL POST OPEN
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AA-REC-DET
        //* **************************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-LETTER-DET
        //*  /* SNEHA CHANGES ENDED - SINHSN
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-LETTER-TLR
        //*  TO PASS NEW #KD-RECIPIENT-SEQ BACK
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PLAN-CLIENT-LOOKUPS
        //*   PLAN TABLE LOOKUP
        //*    COMPRESS TWRAPLR2.RT-PL-LETTER-NAME-1 TWRAPLR2.RT-PL-LETTER-NAME-2
        //*      INTO #PLAN-NAME-LONG
        //*  CLIENT LOOKUP
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-POST-NUMERIC-FIELDS
        //*  '=' SORT-KEY-LGTH
        //*  SORT-KEY-LGTH (EM=HHH) /
        //*   '=' COVER-DATA-LGTH
        //*   COVER-DATA-LGTH (EM=HHHHH) /
        //*   '=' COVER-DATA-OCCURS
        //*  COVER-DATA-OCCURS (EM=HHHHH) /
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP-AA-RECORD-VALUES
        //*  '=' COVER-LETTER-DATA.COVER-REC-ID /
        //*  '=' SC-COVER-LETTER-DATA.YEAR /
        //*  '=' COVER-LETTER-DATA.LETTER-ID /
        //*  '=' SC-COVER-LETTER-DATA.MOBIUS-INDEX /
        //*  '=' SC-COVER-LETTER-DATA.MOBINDX /
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP-BB-RECORD-VALUES
        //*  '=' COVER-LETTER-DATA.COVER-REC-ID /
        //* **
    }
    private void sub_Call_Get_Form_Cntrl_Dtl() throws Exception                                                                                                           //Natural: CALL-GET-FORM-CNTRL-DTL
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Sunny_Dtl.reset();                                                                                                                                            //Natural: RESET #SUNNY-DTL
        pnd_Sunny_Dtl_Pnd_Form_Type.setValue("B");                                                                                                                        //Natural: ASSIGN #SUNNY-DTL.#FORM-TYPE := 'B'
        //*  MOVE FROM.TIRF-TAX-YEAR                     TO #WS-TAX-YEARA
        //*  MOVE #WS-FORM-YY   TO #SUNNY-DTL.#FORM-YEAR
        pnd_Sunny_Dtl_Pnd_Form_Year.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year().getSubstring(3,2));                                                                      //Natural: MOVE SUBSTR ( FORM.TIRF-TAX-YEAR,3,2 ) TO #SUNNY-DTL.#FORM-YEAR
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            pnd_Sunny_Dtl_Pnd_Dont_Pull_1099r_Instruction.setValue("X");                                                                                                  //Natural: MOVE 'X' TO #DONT-PULL-1099R-INSTRUCTION #DONT-PULL-5498F-INSTRUCTION #DONT-PULL-5498P-INSTRUCTION #DONT-PULL-NR4I-INSTRUCTION #DONT-PULL-4806A-INSTRUCTION #DONT-PULL-1042S-INSTRUCTION
            pnd_Sunny_Dtl_Pnd_Dont_Pull_5498f_Instruction.setValue("X");
            pnd_Sunny_Dtl_Pnd_Dont_Pull_5498p_Instruction.setValue("X");
            pnd_Sunny_Dtl_Pnd_Dont_Pull_Nr4i_Instruction.setValue("X");
            pnd_Sunny_Dtl_Pnd_Dont_Pull_4806a_Instruction.setValue("X");
            pnd_Sunny_Dtl_Pnd_Dont_Pull_1042s_Instruction.setValue("X");
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sunny_Dtl_Pnd_Number_Of_Forms.setValue(1);                                                                                                                    //Natural: ASSIGN #SUNNY-DTL.#NUMBER-OF-FORMS := 001
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Pin.setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                                        //Natural: ASSIGN #SUNNY-DTL.#MOBIUS-KEY-PIN := FORM.TIRF-PIN
        //*  #SUNNY-DTL.#MOBIUS-KEY-YEAR       := FORM.TIRF-TAX-YEAR
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Year.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year().getSubstring(3,2));                                                                //Natural: MOVE SUBSTR ( FORM.TIRF-TAX-YEAR,3,2 ) TO #SUNNY-DTL.#MOBIUS-KEY-YEAR
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                            //Natural: ASSIGN #SUNNY-DTL.#MOBIUS-KEY-FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Contract.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                                          //Natural: ASSIGN #SUNNY-DTL.#MOBIUS-KEY-CONTRACT := FORM.TIRF-CONTRACT-NBR
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Payee.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                                //Natural: ASSIGN #SUNNY-DTL.#MOBIUS-KEY-PAYEE := FORM.TIRF-PAYEE-CDE
        pnd_Sunny_Dtl_Pnd_Mobius_Key_Key.setValue(ldaTwrl9710.getForm_Tirf_Key());                                                                                        //Natural: ASSIGN #SUNNY-DTL.#MOBIUS-KEY-KEY := FORM.TIRF-KEY
        pnd_Sunny_Dtl_Pnd_Test_Indicator.setValue(pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator);                                                                                 //Natural: ASSIGN #SUNNY-DTL.#TEST-INDICATOR := #WK-MMM-TEST-INDICATOR
    }
    private void sub_Call_Post() throws Exception                                                                                                                         //Natural: CALL-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  =========================
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"SUBROUTINE CALL-POST");                                                                                            //Natural: WRITE *PROGRAM 'SUBROUTINE CALL-POST'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"In Post-Open...");                                                                                                 //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Open...'
            if (Global.isEscape()) return;
            //*  THIS IS FOR ONLY TEST SAIK
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9610.getPsta9610_Pst_Open_Called().setValue(false);                                                                                                        //Natural: ASSIGN PSTA9610.PST-OPEN-CALLED := FALSE
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "PST-OPEN-CALLED",pdaPsta9610.getPsta9610_Pst_Open_Called(), new ReportEditMask ("Y/N"));                                               //Natural: WRITE ( 0 ) 'PST-OPEN-CALLED' PST-OPEN-CALLED ( EM = Y/N )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
        if (condition(ldaTwrl9710.getForm_Tirf_Pin().greater(getZero())))                                                                                                 //Natural: IF FORM.TIRF-PIN > 0
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("P");                                                                                        //Natural: ASSIGN #UNIV-ID-TYP := 'P'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Pin().equals(getZero()) || ldaTwrl9710.getForm_Tirf_Pin().equals(new DbsDecimal("999999999999"))))                     //Natural: IF FORM.TIRF-PIN = 0 OR FORM.TIRF-PIN = 999999999999
            {
                pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("N");                                                                                    //Natural: ASSIGN #UNIV-ID-TYP := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                          //Natural: IF FORM.TIRF-TAX-ID-TYPE = '4' OR = '5'
        {
            pnd_Tirf_Tin.setValue(" ");                                                                                                                                   //Natural: ASSIGN #TIRF-TIN := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tirf_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                                        //Natural: ASSIGN #TIRF-TIN := FORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Ssn_Nbr().setValue(pnd_Tirf_Tin_Pnd_Tirf_Tin_N9);                                                                                         //Natural: ASSIGN SSN-NBR := #TIRF-TIN-N9
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ());                                                          //Natural: ASSIGN UNIV-ID-TYP := #UNIV-ID-TYP
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().equals("P")))                                                                               //Natural: IF #UNIV-ID-TYP = 'P'
        {
            //*  MOVE EDITED FORM.TIRF-PIN (EM=999999999999) TO  UNIV-ID
            //* SAIK
            pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                                   //Natural: MOVE FORM.TIRF-PIN TO UNIV-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN UNIV-ID := FORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                                         //Natural: ASSIGN FULL-NME := FORM.TIRF-PARTICIPANT-NAME
        pdaPsta9612.getPsta9612_Last_Nme().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                                                            //Natural: ASSIGN LAST-NME := FORM.TIRF-PART-LAST-NME
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                            //Natural: ASSIGN ADDRSS-LINE-1 := FORM.TIRF-ADDR-LN1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                            //Natural: ASSIGN ADDRSS-LINE-2 := FORM.TIRF-ADDR-LN2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                            //Natural: ASSIGN ADDRSS-LINE-3 := FORM.TIRF-ADDR-LN3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                            //Natural: ASSIGN ADDRSS-LINE-4 := FORM.TIRF-ADDR-LN4
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                            //Natural: ASSIGN ADDRSS-LINE-5 := FORM.TIRF-ADDR-LN4
        pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln6());                                                                            //Natural: ASSIGN ADDRSS-LINE-6 := FORM.TIRF-ADDR-LN6
        //*  SSN-NBR             := VAL(TIRF-TIN)
        if (condition(ldaTwrl9710.getForm_Tirf_Foreign_Addr().notEquals("Y")))                                                                                            //Natural: IF TIRF-FOREIGN-ADDR NE 'Y'
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                                       //Natural: MOVE 'U' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("F");                                                                                                       //Natural: MOVE 'F' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaPsta9612.getPsta9612_Addrss_Typ_Cde().equals("F") && ldaTwrl9710.getForm_Tirf_Res_Type().getValue("*").equals("4")))                             //Natural: IF ADDRSS-TYP-CDE EQ 'F' AND TIRF-RES-TYPE ( * ) EQ '4'
        {
            //*  IF TIRF-RES-TYPE      EQ '4'
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("C");                                                                                                       //Natural: MOVE 'C' TO ADDRSS-TYP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  CWFPDA-M
        //*  CWFPDAUS
        //*  CWFPDAUN
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                             //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("POST OPEN -- Call Failed (PSTN9612)");                                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'POST OPEN -- Call Failed (PSTN9612)'
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                                    //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * TAX-FORM-DATA.MOBINDX    := #MOBINDX  /* SAIK
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"In Post-Write..");                                                                                                 //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Write..'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Aa_Rec_Det() throws Exception                                                                                                                   //Natural: CALL-AA-REC-DET
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl6395.getSc_Cover_Letter_Data_Cover_Rec_Id_Aa().setValue("AA");                                                                                             //Natural: ASSIGN SC-COVER-LETTER-DATA.COVER-REC-ID-AA := 'AA'
        ldaTwrl6395.getSc_Cover_Letter_Data_Year().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                         //Natural: ASSIGN SC-COVER-LETTER-DATA.YEAR := FORM.TIRF-TAX-YEAR
        ldaTwrl6395.getSc_Cover_Letter_Data_Letter_Id().setValue("SUNY");                                                                                                 //Natural: ASSIGN SC-COVER-LETTER-DATA.LETTER-ID := 'SUNY'
        ldaTwrl6395.getSc_Cover_Letter_Data_Institution_Ind().setValue(ldaTwrl9710.getForm_Tirf_Inst_Typ());                                                              //Natural: ASSIGN SC-COVER-LETTER-DATA.INSTITUTION-IND := FORM.TIRF-INST-TYP
        ldaTwrl6395.getSc_Cover_Letter_Data_Multi_Ia_Income_Ind().setValueEdited(ldaTwrl9710.getForm_Tirf_Multi_Plan(),new ReportEditMask("N/Y"));                        //Natural: MOVE EDITED FORM.TIRF-MULTI-PLAN ( EM = N/Y ) TO SC-COVER-LETTER-DATA.MULTI-IA-INCOME-IND
        ldaTwrl6395.getSc_Cover_Letter_Data_Ira_Rollover_Ind().setValueEdited(ldaTwrl9710.getForm_Tirf_Rollover_Ind(),new ReportEditMask("N/Y"));                         //Natural: MOVE EDITED FORM.TIRF-ROLLOVER-IND ( EM = N/Y ) TO SC-COVER-LETTER-DATA.IRA-ROLLOVER-IND
        pnd_Sunny_Aa_Pnd_Cover_Rec_Id_Aa.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Cover_Rec_Id_Aa());                                                                 //Natural: MOVE SC-COVER-LETTER-DATA.COVER-REC-ID-AA TO #SUNNY-AA.#COVER-REC-ID-AA
        pnd_Sunny_Aa_Pnd_Year.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Year());                                                                                       //Natural: MOVE SC-COVER-LETTER-DATA.YEAR TO #SUNNY-AA.#YEAR
        pnd_Sunny_Aa_Pnd_Letter_Id.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Letter_Id());                                                                             //Natural: MOVE SC-COVER-LETTER-DATA.LETTER-ID TO #SUNNY-AA.#LETTER-ID
        pnd_Sunny_Aa_Pnd_Institution_Ind.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Institution_Ind());                                                                 //Natural: MOVE SC-COVER-LETTER-DATA.INSTITUTION-IND TO #SUNNY-AA.#INSTITUTION-IND
        pnd_Sunny_Aa_Pnd_Ira_Rollover_Ind.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Ira_Rollover_Ind());                                                               //Natural: MOVE SC-COVER-LETTER-DATA.IRA-ROLLOVER-IND TO #SUNNY-AA.#IRA-ROLLOVER-IND
        pnd_Sunny_Aa_Pnd_Multi_Ia_Income_Ind.setValue(ldaTwrl6395.getSc_Cover_Letter_Data_Multi_Ia_Income_Ind());                                                         //Natural: MOVE SC-COVER-LETTER-DATA.MULTI-IA-INCOME-IND TO #SUNNY-AA.#MULTI-IA-INCOME-IND
        //*  SINHSN
        pnd_Additional_Forms.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ADDITIONAL-FORMS
    }
    private void sub_Call_Post_Letter_Det() throws Exception                                                                                                              //Natural: CALL-POST-LETTER-DET
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  SINHSN
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cover_Data_Array().getValue("*").reset();                                                                                  //Natural: RESET SC-COVER-DATA-ARRAY ( * )
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt().reset();                                                                                             //Natural: RESET SC-CONT-TOT-INCOME-AMT SC-CONT-TOT-INCOME-PCT SC-COVER-LETTER-DATA.SC-PLAN-INCOME ( * ) SC-COVER-LETTER-DATA.SC-PLAN-INCOME-PCT ( * )
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct().reset();
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue("*").reset();
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue("*").reset();
        ldaTwrl6395.getSc_Cover_Letter_Data_Cover_Rec_Id_Bb().setValue("BB");                                                                                             //Natural: ASSIGN SC-COVER-LETTER-DATA.COVER-REC-ID-BB := 'BB'
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Type().setValue(ldaTwrl9710.getForm_Tirf_Contract_Typ().getValue(1));                                                 //Natural: ASSIGN SC-CONT-TYPE := FORM.TIRF-CONTRACT-TYP ( 1 )
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Nbr().setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                              //Natural: ASSIGN SC-CONT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Sc_Cont_Tot_Income_Amt.setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                        //Natural: ASSIGN #SC-CONT-TOT-INCOME-AMT := FORM.TIRF-GROSS-AMT
        pnd_Sc_Cont_Tot_Income_Pct.setValue(100);                                                                                                                         //Natural: ASSIGN #SC-CONT-TOT-INCOME-PCT := 100.0000
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt().setValueEdited(pnd_Sc_Cont_Tot_Income_Amt,new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED #SC-CONT-TOT-INCOME-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO SC-CONT-TOT-INCOME-AMT
        ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct().setValueEdited(pnd_Sc_Cont_Tot_Income_Pct,new ReportEditMask("ZZ9.9999"));                           //Natural: MOVE EDITED #SC-CONT-TOT-INCOME-PCT ( EM = ZZ9.9999 ) TO SC-CONT-TOT-INCOME-PCT
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_I).greater(getZero())))                                                                   //Natural: IF FORM.TIRF-LTR-GROSS-AMT ( #I ) > 0
            {
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                                                                                                                                                                          //Natural: PERFORM PLAN-CLIENT-LOOKUPS
                sub_Plan_Client_Lookups();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Inst_Type().getValue(pnd_J).setValue(pdaTwraptr2.getPnd_Twraptr2_Pnd_Plan_Type_Rpt());                             //Natural: ASSIGN SC-INST-TYPE ( #J ) := #TWRAPTR2.#PLAN-TYPE-RPT
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Orig_Cont_Type().getValue(pnd_J).setValue(ldaTwrl9710.getForm_Tirf_Orig_Contract_Typ().getValue(pnd_I));           //Natural: ASSIGN SC-ORIG-CONT-TYPE ( #J ) := FORM.TIRF-ORIG-CONTRACT-TYP ( #I )
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Orig_Cont_Nbr().getValue(pnd_J).setValue(ldaTwrl9710.getForm_Tirf_Orig_Contract_Nbr().getValue(pnd_I));            //Natural: ASSIGN SC-ORIG-CONT-NBR ( #J ) := FORM.TIRF-ORIG-CONTRACT-NBR ( #I )
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Nbr().getValue(pnd_J).setValue(ldaTwrl9710.getForm_Tirf_Plan().getValue(pnd_I));                              //Natural: ASSIGN SC-PLAN-NBR ( #J ) := FORM.TIRF-PLAN ( #I )
                pnd_Sc_Plan_Income.getValue(pnd_J).setValue(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #SC-PLAN-INCOME ( #J ) := FORM.TIRF-LTR-GROSS-AMT ( #I )
                pnd_Sc_Plan_Income_Pct.getValue(pnd_J).setValue(ldaTwrl9710.getForm_Tirf_Ltr_Per().getValue(pnd_I));                                                      //Natural: ASSIGN #SC-PLAN-INCOME-PCT ( #J ) := FORM.TIRF-LTR-PER ( #I )
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Name().getValue(pnd_J).setValue(pnd_Plan_Name_Long);                                                          //Natural: ASSIGN SC-PLAN-NAME ( #J ) := #PLAN-NAME-LONG
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(pnd_J).setValueEdited(pnd_Sc_Plan_Income.getValue(pnd_J),new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED #SC-PLAN-INCOME ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO SC-PLAN-INCOME ( #J )
                ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(pnd_J).setValueEdited(pnd_Sc_Plan_Income_Pct.getValue(pnd_J),new ReportEditMask("ZZ9.9999")); //Natural: MOVE EDITED #SC-PLAN-INCOME-PCT ( #J ) ( EM = ZZ9.9999 ) TO SC-PLAN-INCOME-PCT ( #J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SINHSN
        pdaPsta6396.getPsta6396_Pnd_Cover_Rec_Id().setValue("BB");                                                                                                        //Natural: MOVE 'BB' TO PSTA6396.#COVER-REC-ID
        pnd_Key_Data.setValue(pdaPsta9610.getPsta9610_Mq_Msg_Key_Data());                                                                                                 //Natural: ASSIGN #KEY-DATA := PSTA9610.MQ-MSG-KEY-DATA
        //*  ADD 1 TO #KEY-DATA.#KD-RECIPIENT-SEQ              /* KISHORE
        //*  #RECORD-SEQ := 0000001                            /* SINHSN
        //*  SINHSN
        pnd_Record_Seq.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #RECORD-SEQ
        pnd_Rec_Type.setValue("D");                                                                                                                                       //Natural: ASSIGN #REC-TYPE := 'D'
        getWorkFiles().write(9, false, pnd_Key_Data, pnd_Record_Seq, pnd_Rec_Type, "b ", pnd_Sunny_Dtl, pnd_Sunny_Aa, ldaTwrl6395.getSc_Cover_Letter_Data());             //Natural: WRITE WORK FILE 9 #KEY-DATA #RECORD-SEQ #REC-TYPE 'b ' #SUNNY-DTL #SUNNY-AA SC-COVER-LETTER-DATA
        //*  SINHSN
    }
    private void sub_Call_Post_Letter_Tlr() throws Exception                                                                                                              //Natural: CALL-POST-LETTER-TLR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        ldaTwrl6395.getSc_Cover_Letter_Data().reset();                                                                                                                    //Natural: RESET SC-COVER-LETTER-DATA
        pnd_Sunny_Aa.reset();                                                                                                                                             //Natural: RESET #SUNNY-AA
        //*  SINHSN
        pnd_Sunny_Dtl.reset();                                                                                                                                            //Natural: RESET #SUNNY-DTL
        pnd_Key_Data.setValue(pdaPsta9610.getPsta9610_Mq_Msg_Key_Data());                                                                                                 //Natural: ASSIGN #KEY-DATA := PSTA9610.MQ-MSG-KEY-DATA
        //*  KISHORE
        //*  KISHORE
        //*  SINHSN
        //*  SINHSN
        //*  SINHSN
        pnd_Key_Data_Pnd_Kd_Recipient_Seq.nadd(1);                                                                                                                        //Natural: ADD 1 TO #KEY-DATA.#KD-RECIPIENT-SEQ
        ldaTwrl6395.getSc_Cover_Letter_Data_Write_Seq_Number().setValue(pnd_Record_Seq);                                                                                  //Natural: ASSIGN SC-COVER-LETTER-DATA.WRITE-SEQ-NUMBER := #RECORD-SEQ
        pnd_Record_Seq.setValue(9999999);                                                                                                                                 //Natural: ASSIGN #RECORD-SEQ := 9999999
        pnd_Rec_Type.setValue("T");                                                                                                                                       //Natural: ASSIGN #REC-TYPE := 'T'
        pnd_Form_Type1.setValue(" ");                                                                                                                                     //Natural: ASSIGN #FORM-TYPE1 := ' '
        //*  COVER-LETTER-DATA.WRITE-SEQ-NUMBER := 0000001           /* SINHSN
        //*  KISHORE
        getWorkFiles().write(9, false, pnd_Key_Data, pnd_Record_Seq, pnd_Rec_Type, "  ", pnd_Sunny_Dtl, pnd_Sunny_Aa, ldaTwrl6395.getSc_Cover_Letter_Data());             //Natural: WRITE WORK FILE 9 #KEY-DATA #RECORD-SEQ #REC-TYPE '  ' #SUNNY-DTL #SUNNY-AA SC-COVER-LETTER-DATA
        pdaPsta9610.getPsta9610_Mq_Msg_Key_Data().setValue(pnd_Key_Data);                                                                                                 //Natural: ASSIGN PSTA9610.MQ-MSG-KEY-DATA := #KEY-DATA
    }
    private void sub_Plan_Client_Lookups() throws Exception                                                                                                               //Natural: PLAN-CLIENT-LOOKUPS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        short decideConditionsMet1665 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FORM.TIRF-PLAN-TYPE ( #I );//Natural: VALUE 'P'
        if (condition((ldaTwrl9710.getForm_Tirf_Plan_Type().getValue(pnd_I).equals("P"))))
        {
            decideConditionsMet1665++;
            pdaTwraplr2.getTwraplr2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRAPLR2.#FUNCTION := '1'
            pdaTwraplr2.getTwraplr2_Pnd_Act_Ind().setValue("A");                                                                                                          //Natural: ASSIGN TWRAPLR2.#ACT-IND := 'A'
            pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().setValue(ldaTwrl9710.getForm_Tirf_Plan().getValue(pnd_I));                                                             //Natural: ASSIGN TWRAPLR2.#PLAN-NUM := FORM.TIRF-PLAN ( #I )
            pdaTwraplr2.getTwraplr2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN TWRAPLR2.#ABEND-IND := FALSE
            pdaTwraplr2.getTwraplr2_Pnd_Display_Ind().setValue(false);                                                                                                    //Natural: ASSIGN TWRAPLR2.#DISPLAY-IND := FALSE
            DbsUtil.callnat(Twrnplr2.class , getCurrentProcessState(), pdaTwraplr2.getTwraplr2_Input_Parms(), new AttributeParameter("O"), pdaTwraplr2.getTwraplr2_Output_Data(),  //Natural: CALLNAT 'TWRNPLR2' USING TWRAPLR2.INPUT-PARMS ( AD = O ) TWRAPLR2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwraplr2.getTwraplr2_Pnd_Return_Code().getBoolean()))                                                                                        //Natural: IF TWRAPLR2.#RETURN-CODE
            {
                pdaTwraptr2.getPnd_Twraptr2_Pnd_Plan_Type().setValue(pdaTwraplr2.getTwraplr2_Rt_Pl_Type());                                                               //Natural: ASSIGN #TWRAPTR2.#PLAN-TYPE := TWRAPLR2.RT-PL-TYPE
                DbsUtil.callnat(Twrnptr2.class , getCurrentProcessState(), pdaTwraptr2.getPnd_Twraptr2_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNPTR2' USING #TWRAPTR2.#INPUT-PARMS ( AD = O ) #TWRAPTR2.#OUTPUT-DATA ( AD = M )
                    pdaTwraptr2.getPnd_Twraptr2_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwraptr2.getPnd_Twraptr2_Pnd_Output_Data().reset();                                                                                                    //Natural: RESET #TWRAPTR2.#OUTPUT-DATA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Plan_Name_Long.setValue(pdaTwraplr2.getTwraplr2_Rt_Pl_Letter_Name());                                                                                     //Natural: ASSIGN #PLAN-NAME-LONG := TWRAPLR2.RT-PL-LETTER-NAME
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaTwrl9710.getForm_Tirf_Plan_Type().getValue(pnd_I).equals("C"))))
        {
            decideConditionsMet1665++;
            pdaTwraptr2.getPnd_Twraptr2_Pnd_Output_Data().reset();                                                                                                        //Natural: RESET #TWRAPTR2.#OUTPUT-DATA
            pdaTwraclr2.getTwraclr2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRACLR2.#FUNCTION := '1'
            pdaTwraclr2.getTwraclr2_Pnd_Act_Ind().setValue("A");                                                                                                          //Natural: ASSIGN TWRACLR2.#ACT-IND := 'A'
            pdaTwraclr2.getTwraclr2_Pnd_Client_Id().setValue(ldaTwrl9710.getForm_Tirf_Plan().getValue(pnd_I));                                                            //Natural: ASSIGN TWRACLR2.#CLIENT-ID := FORM.TIRF-PLAN ( #I )
            pdaTwraclr2.getTwraclr2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN TWRACLR2.#ABEND-IND := FALSE
            pdaTwraclr2.getTwraclr2_Pnd_Display_Ind().setValue(false);                                                                                                    //Natural: ASSIGN TWRACLR2.#DISPLAY-IND := FALSE
            DbsUtil.callnat(Twrnclr2.class , getCurrentProcessState(), pdaTwraclr2.getTwraclr2_Input_Parms(), new AttributeParameter("O"), pdaTwraclr2.getTwraclr2_Output_Data(),  //Natural: CALLNAT 'TWRNCLR2' USING TWRACLR2.INPUT-PARMS ( AD = O ) TWRACLR2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwraclr2.getTwraclr2_Pnd_Return_Code().getBoolean()))                                                                                        //Natural: IF TWRACLR2.#RETURN-CODE
            {
                pnd_Plan_Name_Long.setValue(pdaTwraclr2.getTwraclr2_Rt_Ci_Letter_Name());                                                                                 //Natural: ASSIGN #PLAN-NAME-LONG := TWRACLR2.RT-CI-LETTER-NAME
                //*      COMPRESS TWRACLR2.RT-CI-LETTER-NAME-1 TWRACLR2.RT-CI-LETTER-NAME-2
                //*        INTO #PLAN-NAME-LONG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Display_Post_Numeric_Fields() throws Exception                                                                                                       //Natural: DISPLAY-POST-NUMERIC-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  FOR DEBUGGING PURPOSES ONLY
        //*  SINHSN
        getReports().write(0, "=",pnd_J,NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Orig_Cont_Nbr().getValue(pnd_J),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Nbr().getValue(pnd_J),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(pnd_J),new  //Natural: WRITE '=' #J / '=' SC-COVER-LETTER-DATA.SC-ORIG-CONT-NBR ( #J ) / '=' SC-COVER-LETTER-DATA.SC-PLAN-NBR ( #J ) / '=' SC-COVER-LETTER-DATA.SC-PLAN-INCOME ( #J ) 40T SC-COVER-LETTER-DATA.SC-PLAN-INCOME ( #J ) ( EM = HHHHHHH ) / '=' SC-COVER-LETTER-DATA.SC-PLAN-INCOME-PCT ( #J ) 32T SC-COVER-LETTER-DATA.SC-PLAN-INCOME-PCT ( #J ) ( EM = HHHH ) /
            TabSetting(40),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(pnd_J), new ReportEditMask ("HHHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(pnd_J),new 
            TabSetting(32),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(pnd_J), new ReportEditMask ("HHHH"),NEWLINE);
        if (Global.isEscape()) return;
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*    (P11.2)
        //*    (P3.4)
        //*    (P9.2/1:20)
        //*    (P9.2/1:20)
        //*    (P3.4/1:20)
        //*    (P3.4/1:20)
        //*    (P9.2/1:20)
        //*    (P9.2/1:20)
        //*    (P3.4/1:20)
        //*    (P3.4/1:20)
        //*    (P9.2/1:20)
        //*    (P9.2/1:20)
        //*    (P3.4/1:20)
        //*    (P3.4/1:20)
        //*    (P9.2/1:20)
        //*    (P9.2/1:20)
        //*    (P3.4/1:20)
        //*    (P3.4/1:20)
        getReports().write(0, "=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt(),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt(),              //Natural: WRITE '=' SC-CONT-TOT-INCOME-AMT SC-CONT-TOT-INCOME-AMT ( EM = HHHHHHH ) / '=' SC-CONT-TOT-INCOME-PCT SC-CONT-TOT-INCOME-PCT ( EM = HHHH ) / '=' SC-PLAN-INCOME ( 1 ) SC-PLAN-INCOME ( 1 ) ( EM = HHHHHH ) / '=' SC-PLAN-INCOME-PCT ( 1 ) SC-PLAN-INCOME-PCT ( 1 ) ( EM = HHHH ) / '=' SC-PLAN-INCOME ( 2 ) SC-PLAN-INCOME ( 2 ) ( EM = HHHHHH ) / '=' SC-PLAN-INCOME-PCT ( 2 ) SC-PLAN-INCOME-PCT ( 2 ) ( EM = HHHH ) / '=' SC-PLAN-INCOME ( 3 ) SC-PLAN-INCOME ( 3 ) ( EM = HHHHHH ) / '=' SC-PLAN-INCOME-PCT ( 3 ) SC-PLAN-INCOME-PCT ( 3 ) ( EM = HHHH ) / '=' SC-PLAN-INCOME ( 4 ) SC-PLAN-INCOME ( 4 ) ( EM = HHHHHH ) / '=' SC-PLAN-INCOME-PCT ( 4 ) SC-PLAN-INCOME-PCT ( 4 ) ( EM = HHHH ) /
            new ReportEditMask ("HHHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct(),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct(), 
            new ReportEditMask ("HHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(1),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(1), 
            new ReportEditMask ("HHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(1),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(1), 
            new ReportEditMask ("HHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(2),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(2), 
            new ReportEditMask ("HHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(2),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(2), 
            new ReportEditMask ("HHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(3),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(3), 
            new ReportEditMask ("HHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(3),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(3), 
            new ReportEditMask ("HHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(4),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(4), 
            new ReportEditMask ("HHHHHH"),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(4),ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(4), 
            new ReportEditMask ("HHHH"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Dump_Aa_Record_Values() throws Exception                                                                                                             //Natural: DUMP-AA-RECORD-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().write(0, "A",new RepeatItem(60),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Institution_Ind(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Multi_Ia_Income_Ind(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Ira_Rollover_Ind(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(2),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(3),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(4),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(5),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Name_Addr_Lines().getValue(6),NEWLINE,"=",pdaPsta6396.getPsta6396_Pnd_Cover_Letter_Date(),NEWLINE,"=",pdaPsta6396.getPsta6396_Pnd_Cover_Rec_Id(),NEWLINE,"A",new  //Natural: WRITE 'A' ( 60 ) / '=' SC-COVER-LETTER-DATA.INSTITUTION-IND / '=' SC-COVER-LETTER-DATA.MULTI-IA-INCOME-IND / '=' SC-COVER-LETTER-DATA.IRA-ROLLOVER-IND / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 1 ) / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 2 ) / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 3 ) / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 4 ) / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 5 ) / '=' SC-COVER-LETTER-DATA.NAME-ADDR-LINES ( 6 ) / '=' PSTA6396.#COVER-LETTER-DATE / '=' PSTA6396.#COVER-REC-ID / 'A' ( 60 ) //
            RepeatItem(60),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Dump_Bb_Record_Values() throws Exception                                                                                                             //Natural: DUMP-BB-RECORD-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().write(0, "B",new RepeatItem(60),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Type(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Nbr(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct(),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Inst_Type().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Orig_Cont_Type().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Orig_Cont_Nbr().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Nbr().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Income_Pct().getValue(1),NEWLINE,"=",ldaTwrl6395.getSc_Cover_Letter_Data_Sc_Plan_Name().getValue(1),NEWLINE,"=",pdaPsta6396.getPsta6396_Pnd_Cover_Rec_Id(),NEWLINE,"B",new  //Natural: WRITE 'B' ( 60 ) / '=' SC-CONT-TYPE / '=' SC-CONT-NBR / '=' SC-CONT-TOT-INCOME-AMT / '=' SC-CONT-TOT-INCOME-PCT / '=' SC-INST-TYPE ( 1 ) / '=' SC-ORIG-CONT-TYPE ( 1 ) / '=' SC-ORIG-CONT-NBR ( 1 ) / '=' SC-PLAN-NBR ( 1 ) / '=' SC-PLAN-INCOME ( 1 ) / '=' SC-PLAN-INCOME-PCT ( 1 ) / '=' SC-PLAN-NAME ( 1 ) / '=' PSTA6396.#COVER-REC-ID / 'B' ( 60 ) //
            RepeatItem(60),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=80 LS=130");
    }
}
