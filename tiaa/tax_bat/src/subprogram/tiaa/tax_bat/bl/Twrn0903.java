/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:15:49 AM
**        * FROM NATURAL SUBPROGRAM : Twrn0903
************************************************************
**        * FILE NAME            : Twrn0903.java
**        * CLASS NAME           : Twrn0903
**        * INSTANCE NAME        : Twrn0903
************************************************************
* NAME       : TWRN0903
* FUNCTION   : CREATE TABLE WHICH WILL CONTAIN RESIDENCY CODE,
*                AND STATE DESCRIPTION
* INPUT      : FILE 98 , TABLE 2 (STATE CODE TABLE)
* WRITTEN BY : EDITH  10/5/99
* UPDATES    : READS ALL NUMERIC RESIDENCY CODE
*******************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn0903 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Tax_Yr;
    private DbsField pnd_Res_Code;
    private DbsField pnd_State_Ind;
    private DbsField pnd_State_Desc;

    private DataAccessProgramView vw_f98_State_View;
    private DbsField f98_State_View_Tircntl_Tbl_Nbr;
    private DbsField f98_State_View_Tircntl_Tax_Year;
    private DbsField f98_State_View_Tircntl_State_Old_Code;

    private DbsGroup f98_State_View__R_Field_1;
    private DbsField f98_State_View_Pnd_Filler;
    private DbsField f98_State_View_Tircntl_State_Old_Code_2;
    private DbsField f98_State_View_Tircntl_State_Ind;
    private DbsField f98_State_View_Tircntl_Nbr_Year_Old_State_Cde;
    private DbsField f98_State_View_Tircntl_State_Full_Name;
    private DbsField pnd_Tax_Yr_A;

    private DbsGroup pnd_Tax_Yr_A__R_Field_2;
    private DbsField pnd_Tax_Yr_A_Pnd_Tax_Yr_N;
    private DbsField pnd_Tbl_Nbr;
    private DbsField pnd_Key;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Tax_Yr = parameters.newFieldInRecord("pnd_Tax_Yr", "#TAX-YR", FieldType.STRING, 4);
        pnd_Tax_Yr.setParameterOption(ParameterOption.ByReference);
        pnd_Res_Code = parameters.newFieldArrayInRecord("pnd_Res_Code", "#RES-CODE", FieldType.STRING, 2, new DbsArrayController(1, 100));
        pnd_Res_Code.setParameterOption(ParameterOption.ByReference);
        pnd_State_Ind = parameters.newFieldArrayInRecord("pnd_State_Ind", "#STATE-IND", FieldType.STRING, 1, new DbsArrayController(1, 100));
        pnd_State_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_State_Desc = parameters.newFieldArrayInRecord("pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 19, new DbsArrayController(1, 100));
        pnd_State_Desc.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_f98_State_View = new DataAccessProgramView(new NameInfo("vw_f98_State_View", "F98-STATE-VIEW"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        f98_State_View_Tircntl_Tbl_Nbr = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_TBL_NBR");
        f98_State_View_Tircntl_Tax_Year = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_TAX_YEAR");
        f98_State_View_Tircntl_State_Old_Code = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");

        f98_State_View__R_Field_1 = vw_f98_State_View.getRecord().newGroupInGroup("f98_State_View__R_Field_1", "REDEFINE", f98_State_View_Tircntl_State_Old_Code);
        f98_State_View_Pnd_Filler = f98_State_View__R_Field_1.newFieldInGroup("f98_State_View_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        f98_State_View_Tircntl_State_Old_Code_2 = f98_State_View__R_Field_1.newFieldInGroup("f98_State_View_Tircntl_State_Old_Code_2", "TIRCNTL-STATE-OLD-CODE-2", 
            FieldType.STRING, 2);
        f98_State_View_Tircntl_State_Ind = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_STATE_IND");
        f98_State_View_Tircntl_Nbr_Year_Old_State_Cde = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_Nbr_Year_Old_State_Cde", 
            "TIRCNTL-NBR-YEAR-OLD-STATE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TIRCNTL_NBR_YEAR_OLD_STATE_CDE");
        f98_State_View_Tircntl_Nbr_Year_Old_State_Cde.setSuperDescriptor(true);
        f98_State_View_Tircntl_State_Full_Name = vw_f98_State_View.getRecord().newFieldInGroup("f98_State_View_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", 
            FieldType.STRING, 19, RepeatingFieldStrategy.None, "TIRCNTL_STATE_FULL_NAME");
        registerRecord(vw_f98_State_View);

        pnd_Tax_Yr_A = localVariables.newFieldInRecord("pnd_Tax_Yr_A", "#TAX-YR-A", FieldType.STRING, 4);

        pnd_Tax_Yr_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Tax_Yr_A__R_Field_2", "REDEFINE", pnd_Tax_Yr_A);
        pnd_Tax_Yr_A_Pnd_Tax_Yr_N = pnd_Tax_Yr_A__R_Field_2.newFieldInGroup("pnd_Tax_Yr_A_Pnd_Tax_Yr_N", "#TAX-YR-N", FieldType.NUMERIC, 4);
        pnd_Tbl_Nbr = localVariables.newFieldInRecord("pnd_Tbl_Nbr", "#TBL-NBR", FieldType.STRING, 1);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_f98_State_View.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Tbl_Nbr.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn0903() throws Exception
    {
        super("Twrn0903");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Tax_Yr_A.setValue(pnd_Tax_Yr);                                                                                                                                //Natural: ASSIGN #TAX-YR-A := #TAX-YR
        pnd_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "2", pnd_Tax_Yr, "000"));                                                                        //Natural: COMPRESS '2' #TAX-YR '000' INTO #KEY LEAVING NO SPACE
        vw_f98_State_View.startDatabaseRead                                                                                                                               //Natural: READ F98-STATE-VIEW WITH TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #KEY
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_f98_State_View.readNextRow("READ01")))
        {
            if (condition(f98_State_View_Tircntl_Tbl_Nbr.equals(2) && f98_State_View_Tircntl_Tax_Year.equals(pnd_Tax_Yr_A_Pnd_Tax_Yr_N) && f98_State_View_Tircntl_State_Old_Code.greaterOrEqual("000")  //Natural: IF TIRCNTL-TBL-NBR = 2 AND TIRCNTL-TAX-YEAR = #TAX-YR-N AND TIRCNTL-STATE-OLD-CODE = '000' THRU '099'
                && f98_State_View_Tircntl_State_Old_Code.lessOrEqual("099")))
            {
                //*  NOT ( TIRCNTL-STATE-OLD-CODE  =  '000' THRU '057') AND
                //*     TIRCNTL-STATE-OLD-CODE  NE '097'        /* NON-US STATES
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Res_Code.getValue(pnd_I).setValue(f98_State_View_Tircntl_State_Old_Code_2);                                                                           //Natural: ASSIGN #RES-CODE ( #I ) := TIRCNTL-STATE-OLD-CODE-2
                pnd_State_Ind.getValue(pnd_I).setValue(f98_State_View_Tircntl_State_Ind);                                                                                 //Natural: ASSIGN #STATE-IND ( #I ) := TIRCNTL-STATE-IND
                pnd_State_Desc.getValue(pnd_I).setValue(f98_State_View_Tircntl_State_Full_Name);                                                                          //Natural: ASSIGN #STATE-DESC ( #I ) := TIRCNTL-STATE-FULL-NAME
                //*  ----------------------------------------------------------
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
