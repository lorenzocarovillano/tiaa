/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:15:20 AM
**        * FROM NATURAL SUBPROGRAM : Twrn0214
************************************************************
**        * FILE NAME            : Twrn0214.java
**        * CLASS NAME           : Twrn0214
**        * INSTANCE NAME        : Twrn0214
************************************************************
************************************************************************
**                                                                    **
** PROGRAM:  TWRN0214 - FORM SELECTION
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  A.WILNER
** PURPOSE:  POST WRITE DRIVER FOR TAX REFACTOR PROJECT
**           (EXECUTES POST WRITE MODULES TWRN214D,TWRN214E & TWRN214G)
**
** HISTORY:
** ------------------------------------
**
**
**
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn0214 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9610 pdaPsta9610;
    private PdaTwra214e pdaTwra214e;
    private PdaTwra114f pdaTwra114f;
    private PdaTwra214g pdaTwra214g;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Form_Page_Cnt;
    private DbsField pnd_Ws_Isn;
    private DbsField pnd_Link_Let_Type;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Coupon_Ind;

    private DbsGroup pnd_Ltr_Array;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Tin;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Cntrct;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Data;
    private DbsField pnd_Additional_Forms;

    private DataAccessProgramView vw_form_Get;
    private DbsField form_Get_Tirf_Tax_Year;
    private DbsField form_Get_Tirf_Form_Type;
    private DbsField form_Get_Tirf_Company_Cde;
    private DbsField form_Get_Tirf_Contract_Nbr;
    private DbsField form_Get_Tirf_Payee_Cde;
    private DbsField form_Get_Tirf_Key;
    private DbsField form_Get_Tirf_Pin;
    private DbsField pnd_Debug;
    private DbsField pnd_Debug2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwra214e = new PdaTwra214e(localVariables);
        pdaTwra114f = new PdaTwra114f(localVariables);
        pdaTwra214g = new PdaTwra214g(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Form_Page_Cnt = parameters.newFieldInRecord("pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Form_Page_Cnt.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Isn = parameters.newFieldInRecord("pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Isn.setParameterOption(ParameterOption.ByReference);
        pdaTwra0214 = new PdaTwra0214(parameters);
        pdaPsta9610 = new PdaPsta9610(parameters);
        pnd_Link_Let_Type = parameters.newFieldArrayInRecord("pnd_Link_Let_Type", "#LINK-LET-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Link_Let_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Test_Ind = parameters.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Test_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Coupon_Ind = parameters.newFieldInRecord("pnd_Coupon_Ind", "#COUPON-IND", FieldType.STRING, 1);
        pnd_Coupon_Ind.setParameterOption(ParameterOption.ByReference);

        pnd_Ltr_Array = parameters.newGroupArrayInRecord("pnd_Ltr_Array", "#LTR-ARRAY", new DbsArrayController(1, 999));
        pnd_Ltr_Array.setParameterOption(ParameterOption.ByReference);
        pnd_Ltr_Array_Pnd_Ltr_Tin = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Tin", "#LTR-TIN", FieldType.STRING, 10);
        pnd_Ltr_Array_Pnd_Ltr_Cntrct = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Cntrct", "#LTR-CNTRCT", FieldType.STRING, 9);
        pnd_Ltr_Array_Pnd_Ltr_Data = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Data", "#LTR-DATA", FieldType.STRING, 1);
        pnd_Additional_Forms = parameters.newFieldInRecord("pnd_Additional_Forms", "#ADDITIONAL-FORMS", FieldType.PACKED_DECIMAL, 7);
        pnd_Additional_Forms.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_form_Get = new DataAccessProgramView(new NameInfo("vw_form_Get", "FORM-GET"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Get_Tirf_Tax_Year = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Get_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Get_Tirf_Form_Type = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Get_Tirf_Form_Type.setDdmHeader("FORM");
        form_Get_Tirf_Company_Cde = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Get_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_Get_Tirf_Contract_Nbr = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Get_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_Get_Tirf_Payee_Cde = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Get_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Get_Tirf_Key = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "TIRF_KEY");
        form_Get_Tirf_Key.setDdmHeader("KEY");
        form_Get_Tirf_Pin = vw_form_Get.getRecord().newFieldInGroup("form_Get_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "TIRF_PIN");
        form_Get_Tirf_Pin.setDdmHeader("PIN");
        registerRecord(vw_form_Get);

        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Debug2 = localVariables.newFieldInRecord("pnd_Debug2", "#DEBUG2", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_Get.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Debug.setInitialValue(false);
        pnd_Debug2.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn0214() throws Exception
    {
        super("Twrn0214");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year());                                                 //Natural: WRITE *PROGRAM '=' #TWRA0214-TAX-YEAR
            if (Global.isEscape()) return;
            getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type());                                                //Natural: WRITE *PROGRAM '=' #TWRA0214-FORM-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin());                                                      //Natural: WRITE *PROGRAM '=' #TWRA0214-TIN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().equals(" ")))                                                                       //Natural: IF #TWRA0214-MOBIUS-KEY = ' '
        {
                                                                                                                                                                          //Natural: PERFORM MOBIUS-KEY
            sub_Mobius_Key();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet253 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TWRA0214-FORM-TYPE = 4 AND #TWRA0214-TAX-YEAR >= 2002
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().equals(4) && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year().greaterOrEqual(2002)))
        {
            decideConditionsMet253++;
            pdaTwra214e.getPnd_Twra214e_Pnd_Tax_Year().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year());                                           //Natural: ASSIGN #TWRA214E.#TAX-YEAR := #TWRA0214-TAX-YEAR
            pdaTwra214e.getPnd_Twra214e_Pnd_Tin().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin());                                                     //Natural: ASSIGN #TWRA214E.#TIN := #TWRA0214-TIN
            pdaTwra214e.getPnd_Twra214e_Pnd_Form_Type().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type());                                         //Natural: ASSIGN #TWRA214E.#FORM-TYPE := #TWRA0214-FORM-TYPE
            //*  DUTTAD 010515
            //*  DASRAH
            DbsUtil.callnat(Twrn214e.class , getCurrentProcessState(), pdaTwra214e.getPnd_Twra214e(), pdaPsta9610.getPsta9610(), pdaTwra0214.getPnd_Twra0214_Link_Area(), //Natural: CALLNAT 'TWRN214E' #TWRA214E PSTA9610 #TWRA0214-LINK-AREA #TEST-IND #COUPON-IND #LINK-LET-TYPE ( * )
                pnd_Test_Ind, pnd_Coupon_Ind, pnd_Link_Let_Type.getValue("*"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN #TWRA0214-FORM-TYPE = 10
        else if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().equals(10)))
        {
            decideConditionsMet253++;
            pdaTwra214g.getPnd_Twra214g_Pnd_Tax_Year().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year());                                           //Natural: ASSIGN #TWRA214G.#TAX-YEAR := #TWRA0214-TAX-YEAR
            pdaTwra214g.getPnd_Twra214g_Pnd_Pin().setValue(form_Get_Tirf_Pin);                                                                                            //Natural: ASSIGN #TWRA214G.#PIN := FORM-GET.TIRF-PIN
            pdaTwra214g.getPnd_Twra214g_Pnd_Form_Type().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type());                                         //Natural: ASSIGN #TWRA214G.#FORM-TYPE := #TWRA0214-FORM-TYPE
            //*  DUTTAD 010815
            //*  SINHSN
            //*  SINHSN
            DbsUtil.callnat(Twrn214g.class , getCurrentProcessState(), pdaTwra214g.getPnd_Twra214g(), pdaPsta9610.getPsta9610(), pdaTwra0214.getPnd_Twra0214_Link_Area(), //Natural: CALLNAT 'TWRN214G' #TWRA214G PSTA9610 #TWRA0214-LINK-AREA #LTR-ARRAY ( * ) #ADDITIONAL-FORMS #TEST-IND
                pnd_Ltr_Array.getValue("*"), pnd_Additional_Forms, pnd_Test_Ind);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  DUTTAD 010515
            DbsUtil.callnat(Twrn214d.class , getCurrentProcessState(), pnd_Form_Page_Cnt, pnd_Ws_Isn, pdaTwra0214.getPnd_Twra0214_Link_Area(), pdaPsta9610.getPsta9610(), //Natural: CALLNAT 'TWRN214D' #FORM-PAGE-CNT #WS-ISN #TWRA0214-LINK-AREA PSTA9610 #LINK-LET-TYPE ( * ) #TEST-IND
                pnd_Link_Let_Type.getValue("*"), pnd_Test_Ind);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOBIUS-KEY
    }
    private void sub_Mobius_Key() throws Exception                                                                                                                        //Natural: MOBIUS-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().greater(getZero())))                                                                       //Natural: IF #TWRA0214-ISN > 0
        {
            GET01:                                                                                                                                                        //Natural: GET FORM-GET #TWRA0214-ISN
            vw_form_Get.readByID(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getLong(), "GET01");
            //*  NOT 1042-S
            short decideConditionsMet284 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TWRA0214-FORM-TYPE NE 03 OR ( #TWRA0214-FORM-TYPE = 03 AND #TWRA0214-MOBIUS-MIGR-IND = ' ' )
            if (condition((pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().notEquals(3) || (pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().equals(3) 
                && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals(" ")))))
            {
                decideConditionsMet284++;
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Pin().setValue(form_Get_Tirf_Pin);                                                                                   //Natural: ASSIGN #TWRA114F.#TIRF-PIN := FORM-GET.TIRF-PIN
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Tax_Year().setValue(form_Get_Tirf_Tax_Year);                                                                         //Natural: ASSIGN #TWRA114F.#TIRF-TAX-YEAR := FORM-GET.TIRF-TAX-YEAR
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Form_Type().setValue(form_Get_Tirf_Form_Type);                                                                       //Natural: ASSIGN #TWRA114F.#TIRF-FORM-TYPE := FORM-GET.TIRF-FORM-TYPE
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Company_Cde().setValue(form_Get_Tirf_Company_Cde);                                                                   //Natural: ASSIGN #TWRA114F.#TIRF-COMPANY-CDE := FORM-GET.TIRF-COMPANY-CDE
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Contract_Nbr().setValue(form_Get_Tirf_Contract_Nbr);                                                                 //Natural: ASSIGN #TWRA114F.#TIRF-CONTRACT-NBR := FORM-GET.TIRF-CONTRACT-NBR
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Payee_Cde().setValue(form_Get_Tirf_Payee_Cde);                                                                       //Natural: ASSIGN #TWRA114F.#TIRF-PAYEE-CDE := FORM-GET.TIRF-PAYEE-CDE
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Key().setValue(form_Get_Tirf_Key);                                                                                   //Natural: ASSIGN #TWRA114F.#TIRF-KEY := FORM-GET.TIRF-KEY
            }                                                                                                                                                             //Natural: WHEN #TWRA0214-FORM-TYPE = 10
            else if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().equals(10)))
            {
                decideConditionsMet284++;
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Pin().setValue(form_Get_Tirf_Pin);                                                                                   //Natural: ASSIGN #TWRA114F.#TIRF-PIN := FORM-GET.TIRF-PIN
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Tax_Year().setValue(form_Get_Tirf_Tax_Year);                                                                         //Natural: ASSIGN #TWRA114F.#TIRF-TAX-YEAR := FORM-GET.TIRF-TAX-YEAR
                pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Form_Type().setValue(form_Get_Tirf_Form_Type);                                                                       //Natural: ASSIGN #TWRA114F.#TIRF-FORM-TYPE := FORM-GET.TIRF-FORM-TYPE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(1);                                                                                    //Natural: ASSIGN #TWRA0214-RET-CODE := 01
            if (condition(pnd_Debug2.getBoolean()))                                                                                                                       //Natural: IF #DEBUG2
            {
                getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn());                                                  //Natural: WRITE *PROGRAM '=' #TWRA0214-ISN
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year());                                             //Natural: WRITE *PROGRAM '=' #TWRA0214-TAX-YEAR
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type());                                            //Natural: WRITE *PROGRAM '=' #TWRA0214-FORM-TYPE
                if (Global.isEscape()) return;
                getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Pin_Nbr());                                                       //Natural: WRITE *PROGRAM '=' #PIN-NBR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra114f.getPnd_Twra114f_Pnd_Mobius_Migr_Ind().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind());                                 //Natural: ASSIGN #TWRA114F.#MOBIUS-MIGR-IND := #TWRA0214-MOBIUS-MIGR-IND
        DbsUtil.callnat(Twrn114f.class , getCurrentProcessState(), pdaTwra114f.getPnd_Twra114f());                                                                        //Natural: CALLNAT 'TWRN114F' USING #TWRA114F
        if (condition(Global.isEscape())) return;
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().setValue(pdaTwra114f.getPnd_Twra114f_Pnd_Mobius_Key());                                           //Natural: ASSIGN #TWRA0214-MOBIUS-KEY := #TWRA114F.#MOBIUS-KEY
        if (condition(pnd_Debug2.getBoolean()))                                                                                                                           //Natural: IF #DEBUG2
        {
            getReports().write(0, Global.getPROGRAM(),pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                                   //Natural: WRITE *PROGRAM #TWRA0214-MOBIUS-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
