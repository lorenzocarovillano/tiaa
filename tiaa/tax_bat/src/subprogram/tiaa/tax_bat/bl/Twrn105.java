/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:15:54 AM
**        * FROM NATURAL SUBPROGRAM : Twrn105
************************************************************
**        * FILE NAME            : Twrn105.java
**        * CLASS NAME           : Twrn105
**        * INSTANCE NAME        : Twrn105
************************************************************
************************************************************************
* PROGRAM   : TWRN105
* SYSTEM    : CPS
* TITLE     : CORE FILE ACCESS
* FUNCTION  : PASSES BACK PIN / SSN LEVEL INFO BASED ON THE GIVEN VALUE
*           : OF #P-VARS FROM A CALLING OBJECT.
*
* ----------------FOR GIVEN #P-VARS----------------  ------RETURNS------
* #CNTRCT-PPCN/PY #PIN #GET-PIN #GET-SSN-LEVEL-INFO  #PIN SSN/NM/DOB/DOD
* --------------- ---- -------- -------------------  ---- --------------
*     X                   X                            X
*     X                                X               X       X
*     X                   X            X               X       X
*     X            X                   X               X**     X**
*                  X                   X                       X***
*
* NOTE:   X**  RETURNS INFO FOR GIVEN #CNTRCT-PPCN/PY
*         X*** RETURNS INFO OF 1ST ANNUITANT POLICYHOLDER ONLY
* ----------------------------------------------------------------------
*
* HISTORY   :
* 04/09/15: MANSOOR - COR/NAAD CONVERSION
* 05/05/15  FENDAYA - COR/NAS SUNSET CONTINUATION. FE201505
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn105 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra105 pdaTwra105;
    private PdaMdma210 pdaMdma210;
    private PdaMdma100 pdaMdma100;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Core_S1;
    private DbsField pnd_Core_S1_Pnd_Cntrct_Nbr;
    private DbsField pnd_Core_S1_Pnd_Cntrct_Payee_Cd;
    private DbsField pnd_Core_S1_Pnd_Core_S1_Filler;

    private DbsGroup pnd_Core_S1__R_Field_1;
    private DbsField pnd_Core_S1_Pnd_Core_S1_Start;
    private DbsField pnd_Core_S1_End;

    private DbsGroup pnd_Core_S2_Det;
    private DbsField pnd_Core_S2_Det_Pnd_Pin;
    private DbsField pnd_Core_S2_Det_Pnd_Rcd_Type_Cde;

    private DbsGroup pnd_Core_S2_Det__R_Field_2;
    private DbsField pnd_Core_S2_Det_Pnd_Core_S2;
    private DbsField pnd_Corr_Cntrct_Ppcn_Nbr;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma210 = new PdaMdma210(localVariables);
        pdaMdma100 = new PdaMdma100(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaTwra105 = new PdaTwra105(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Core_S1 = localVariables.newGroupInRecord("pnd_Core_S1", "#CORE-S1");
        pnd_Core_S1_Pnd_Cntrct_Nbr = pnd_Core_S1.newFieldInGroup("pnd_Core_S1_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Core_S1_Pnd_Cntrct_Payee_Cd = pnd_Core_S1.newFieldInGroup("pnd_Core_S1_Pnd_Cntrct_Payee_Cd", "#CNTRCT-PAYEE-CD", FieldType.STRING, 2);
        pnd_Core_S1_Pnd_Core_S1_Filler = pnd_Core_S1.newFieldInGroup("pnd_Core_S1_Pnd_Core_S1_Filler", "#CORE-S1-FILLER", FieldType.STRING, 1);

        pnd_Core_S1__R_Field_1 = localVariables.newGroupInRecord("pnd_Core_S1__R_Field_1", "REDEFINE", pnd_Core_S1);
        pnd_Core_S1_Pnd_Core_S1_Start = pnd_Core_S1__R_Field_1.newFieldInGroup("pnd_Core_S1_Pnd_Core_S1_Start", "#CORE-S1-START", FieldType.STRING, 13);
        pnd_Core_S1_End = localVariables.newFieldInRecord("pnd_Core_S1_End", "#CORE-S1-END", FieldType.STRING, 13);

        pnd_Core_S2_Det = localVariables.newGroupInRecord("pnd_Core_S2_Det", "#CORE-S2-DET");
        pnd_Core_S2_Det_Pnd_Pin = pnd_Core_S2_Det.newFieldInGroup("pnd_Core_S2_Det_Pnd_Pin", "#PIN", FieldType.NUMERIC, 7);
        pnd_Core_S2_Det_Pnd_Rcd_Type_Cde = pnd_Core_S2_Det.newFieldInGroup("pnd_Core_S2_Det_Pnd_Rcd_Type_Cde", "#RCD-TYPE-CDE", FieldType.NUMERIC, 2);

        pnd_Core_S2_Det__R_Field_2 = localVariables.newGroupInRecord("pnd_Core_S2_Det__R_Field_2", "REDEFINE", pnd_Core_S2_Det);
        pnd_Core_S2_Det_Pnd_Core_S2 = pnd_Core_S2_Det__R_Field_2.newFieldInGroup("pnd_Core_S2_Det_Pnd_Core_S2", "#CORE-S2", FieldType.STRING, 9);
        pnd_Corr_Cntrct_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Corr_Cntrct_Ppcn_Nbr", "#CORR-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Core_S1_Pnd_Core_S1_Filler.setInitialValue("H'00'");
        pnd_Core_S2_Det_Pnd_Rcd_Type_Cde.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn105() throws Exception
    {
        super("Twrn105");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  MOVE TRUE TO +TRACE
        pnd_Corr_Cntrct_Ppcn_Nbr.setValue(" ");                                                                                                                           //Natural: ASSIGN #CORR-CNTRCT-PPCN-NBR := ' '
        if (condition(pdaTwra105.getPnd_P_Vars_Pnd_Get_Pin().equals(true) || (pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr().notEquals(" ") && pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde().notEquals(" ")))) //Natural: IF #P-VARS.#GET-PIN = TRUE OR ( #P-VARS.#CNTRCT-PPCN-NBR NE ' ' AND #P-VARS.#CNTRCT-PAYEE-CDE NE ' ' )
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CORE-PIN-RCDTYPE-WITH-CNTRCT-PAYEE
            sub_Obtain_Core_Pin_Rcdtype_With_Cntrct_Payee();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaTwra105.getPnd_P_Vars_Pnd_Pin().equals(getZero())))                                                                                          //Natural: IF #P-VARS.#PIN = 0
            {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CORE-CNTRCT#-WITH-CREF#
                sub_Obtain_Core_Cntrctpnd__With_Crefpnd();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CORE-PIN-RCDTYPE-WITH-CNTRCT-PAYEE
                sub_Obtain_Core_Pin_Rcdtype_With_Cntrct_Payee();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   PIN N0T FOUND
        if (condition(pdaTwra105.getPnd_P_Vars_Pnd_Pin().equals(getZero())))                                                                                              //Natural: IF #P-VARS.#PIN = 0
        {
            //*   RETURN CONTROL TO THE CALLING PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwra105.getPnd_P_Vars_Pnd_Get_Ssn_Level_Info().equals(true)))                                                                                    //Natural: IF #P-VARS.#GET-SSN-LEVEL-INFO = TRUE
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CORE-SSN-WITH-PIN-RCDTYPE
            sub_Obtain_Core_Ssn_With_Pin_Rcdtype();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   RETURN CONTROL TO THE CALLING PROGRAM
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-CORE-PIN-RCDTYPE-WITH-CNTRCT-PAYEE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-CORE-CNTRCT#-WITH-CREF#
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-CORE-SSN-WITH-PIN-RCDTYPE
        //* *-------------------------------------------------
        //* *------------
        //* *************************
    }
    private void sub_Obtain_Core_Pin_Rcdtype_With_Cntrct_Payee() throws Exception                                                                                         //Natural: OBTAIN-CORE-PIN-RCDTYPE-WITH-CNTRCT-PAYEE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------
        if (condition(pnd_Corr_Cntrct_Ppcn_Nbr.equals(" ")))                                                                                                              //Natural: IF #CORR-CNTRCT-PPCN-NBR = ' '
        {
            pnd_Core_S1_Pnd_Cntrct_Nbr.setValue(pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr());                                                                          //Natural: ASSIGN #CORE-S1.#CNTRCT-NBR := #P-VARS.#CNTRCT-PPCN-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Core_S1_Pnd_Cntrct_Nbr.setValue(pnd_Corr_Cntrct_Ppcn_Nbr);                                                                                                //Natural: ASSIGN #CORE-S1.#CNTRCT-NBR := #CORR-CNTRCT-PPCN-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Core_S1_Pnd_Cntrct_Payee_Cd.setValue(pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde());                                                                        //Natural: ASSIGN #CORE-S1.#CNTRCT-PAYEE-CD := #P-VARS.#CNTRCT-PAYEE-CDE
        pnd_Core_S1_End.setValue(pnd_Core_S1_Pnd_Core_S1_Start);                                                                                                          //Natural: ASSIGN #CORE-S1-END := #CORE-S1-START
        setValueToSubstring("H'FF'",pnd_Core_S1_End,13,1);                                                                                                                //Natural: MOVE H'FF' TO SUBSTR ( #CORE-S1-END,13,1 )
        //* ******************* COR-NAAD CONVERSION*************** START
        pdaMdma210.getPnd_Mdma210().reset();                                                                                                                              //Natural: RESET #MDMA210
        pdaMdma210.getPnd_Mdma210_Pnd_I_Contract_Number().setValue(pnd_Core_S1_Pnd_Cntrct_Nbr);                                                                           //Natural: ASSIGN #MDMA210.#I-CONTRACT-NUMBER := #CORE-S1.#CNTRCT-NBR
        if (condition(pnd_Core_S1_Pnd_Cntrct_Payee_Cd.equals(" ")))                                                                                                       //Natural: IF #CORE-S1.#CNTRCT-PAYEE-CD EQ ' '
        {
            pdaMdma210.getPnd_Mdma210_Pnd_I_Payee_Code_A2().setValue("01");                                                                                               //Natural: ASSIGN #MDMA210.#I-PAYEE-CODE-A2 := '01'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma210.getPnd_Mdma210_Pnd_I_Payee_Code_A2().setValue(pnd_Core_S1_Pnd_Cntrct_Payee_Cd);                                                                    //Natural: ASSIGN #MDMA210.#I-PAYEE-CODE-A2 := #CORE-S1.#CNTRCT-PAYEE-CD
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Mdmn210a.class , getCurrentProcessState(), pdaMdma210.getPnd_Mdma210());                                                                          //Natural: CALLNAT 'MDMN210A' #MDMA210
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma210.getPnd_Mdma210_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA210.#O-RETURN-CODE EQ '0000'
        {
            pdaTwra105.getPnd_P_Vars_Pnd_Pin().setValue(pdaMdma210.getPnd_Mdma210_Pnd_O_Pin());                                                                           //Natural: ASSIGN #P-VARS.#PIN := #MDMA210.#O-PIN
            pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type().setValue(2);                                                                                              //Natural: ASSIGN #P-VARS.#CNTRCT-DATA-RCD-TYPE := 02
            pdaTwra105.getPnd_P_Vars_Pnd_Ph_Data_Rcd_Type().setValue(1);                                                                                                  //Natural: ASSIGN #P-VARS.#PH-DATA-RCD-TYPE := 01
        }                                                                                                                                                                 //Natural: END-IF
        //*  HISTOGRAM (1)  COR-XREF-HIST  COR-SUPER-CNTRCT-PAYEE-PIN
        //* *          STARTING FROM   #CORE-S1-START  THRU  #CORE-S1-END
        //*   #P-VARS.#PIN                  := COR-XREF-HIST.#PH-UNIQUE-ID-NBR
        //*   #P-VARS.#CNTRCT-DATA-RCD-TYPE := COR-XREF-HIST.#PH-RCD-TYPE-CDE
        //*   #P-VARS.#PH-DATA-RCD-TYPE     := COR-XREF-HIST.#PH-RCD-TYPE-CDE  -  1
        //*  ESCAPE ROUTINE
        //*  END-HISTOGRAM
    }
    private void sub_Obtain_Core_Cntrctpnd__With_Crefpnd() throws Exception                                                                                               //Natural: OBTAIN-CORE-CNTRCT#-WITH-CREF#
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------------
        //* *READ (1)  COR-XREF  WITH  CNTRCT-CREF-NBR  =  #CORE-S1.#CNTRCT-NBR
        pdaMdma210.getPnd_Mdma210().reset();                                                                                                                              //Natural: RESET #MDMA210
        pdaMdma210.getPnd_Mdma210_Pnd_I_Contract_Number().setValue(pnd_Core_S1_Pnd_Cntrct_Nbr);                                                                           //Natural: ASSIGN #MDMA210.#I-CONTRACT-NUMBER := #CORE-S1.#CNTRCT-NBR
        if (condition(pnd_Core_S1_Pnd_Cntrct_Payee_Cd.equals(" ") || pnd_Core_S1_Pnd_Cntrct_Payee_Cd.equals("00")))                                                       //Natural: IF #CORE-S1.#CNTRCT-PAYEE-CD EQ ' ' OR EQ '00'
        {
            pdaMdma210.getPnd_Mdma210_Pnd_I_Payee_Code_A2().setValue("01");                                                                                               //Natural: ASSIGN #MDMA210.#I-PAYEE-CODE-A2 := '01'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma210.getPnd_Mdma210_Pnd_I_Payee_Code_A2().setValue(pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde());                                                    //Natural: ASSIGN #MDMA210.#I-PAYEE-CODE-A2 := #P-VARS.#CNTRCT-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Mdmn210a.class , getCurrentProcessState(), pdaMdma210.getPnd_Mdma210());                                                                          //Natural: CALLNAT 'MDMN210A' #MDMA210
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma210.getPnd_Mdma210_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA210.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pdaMdma210.getPnd_Mdma210_Pnd_O_Cref_Number().equals(pnd_Core_S1_Pnd_Cntrct_Nbr)))                                                              //Natural: IF #MDMA210.#O-CREF-NUMBER = #CORE-S1.#CNTRCT-NBR
            {
                pnd_Corr_Cntrct_Ppcn_Nbr.setValue(pdaMdma210.getPnd_Mdma210_Pnd_O_Cref_Number());                                                                         //Natural: ASSIGN #CORR-CNTRCT-PPCN-NBR := #MDMA210.#O-CREF-NUMBER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaMdma210.getPnd_Mdma210_Pnd_O_Contract_Number().equals(pnd_Core_S1_Pnd_Cntrct_Nbr)))                                                      //Natural: IF #MDMA210.#O-CONTRACT-NUMBER = #CORE-S1.#CNTRCT-NBR
                {
                    pnd_Corr_Cntrct_Ppcn_Nbr.setValue(pdaMdma210.getPnd_Mdma210_Pnd_O_Contract_Number());                                                                 //Natural: ASSIGN #CORR-CNTRCT-PPCN-NBR := #MDMA210.#O-CONTRACT-NUMBER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   IF COR-XREF.CNTRCT-CREF-NBR  =  #CORE-S1.#CNTRCT-NBR
            //*      #CORR-CNTRCT-PPCN-NBR  :=  COR-XREF.CNTRCT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-READ
    }
    private void sub_Obtain_Core_Ssn_With_Pin_Rcdtype() throws Exception                                                                                                  //Natural: OBTAIN-CORE-SSN-WITH-PIN-RCDTYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Core_S2_Det_Pnd_Pin.setValue(pdaTwra105.getPnd_P_Vars_Pnd_Pin());                                                                                             //Natural: ASSIGN #CORE-S2-DET.#PIN := #P-VARS.#PIN
        //*                              /* FE201605 START
        pdaMdma100.getPnd_Mdma100().reset();                                                                                                                              //Natural: RESET #MDMA100
        pdaMdma100.getPnd_Mdma100_Pnd_I_Pin().setValue(pnd_Core_S2_Det_Pnd_Pin);                                                                                          //Natural: ASSIGN #MDMA100.#I-PIN := #CORE-S2-DET.#PIN
        DbsUtil.callnat(Mdmn100a.class , getCurrentProcessState(), pdaMdma100.getPnd_Mdma100());                                                                          //Natural: CALLNAT 'MDMN100A' #MDMA100
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma100.getPnd_Mdma100_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA100.#O-RETURN-CODE EQ '0000'
        {
            pdaTwra105.getPnd_P_Vars_Pnd_Ssn().setValue(pdaMdma100.getPnd_Mdma100_Pnd_O_Ssn());                                                                           //Natural: ASSIGN #P-VARS.#SSN := #MDMA100.#O-SSN
            pdaTwra105.getPnd_P_Vars_Pnd_Dob_Ccyymmdd().setValue(pdaMdma100.getPnd_Mdma100_Pnd_O_Date_Of_Birth());                                                        //Natural: ASSIGN #P-VARS.#DOB-CCYYMMDD := #MDMA100.#O-DATE-OF-BIRTH
            pdaTwra105.getPnd_P_Vars_Pnd_Dod_Ccyymmdd().setValue(pdaMdma100.getPnd_Mdma100_Pnd_O_Date_Of_Death());                                                        //Natural: ASSIGN #P-VARS.#DOD-CCYYMMDD := #MDMA100.#O-DATE-OF-DEATH
            //*  FE201605 END
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #P-VARS.#PH-DATA-RCD-TYPE  =  0
        //*    #P-VARS.#PH-DATA-RCD-TYPE      :=  #CORE-S2-DET.#RCD-TYPE-CDE
        //*    #P-VARS.#CNTRCT-DATA-RCD-TYPE  :=  #P-VARS.#PH-DATA-RCD-TYPE  +  1
        //* *ELSE
        //*    #CORE-S2-DET.#RCD-TYPE-CDE  :=  #P-VARS.#PH-DATA-RCD-TYPE
        //* *END-IF
        //* *READ (1)  COR-XREF     /* FE201605 COMMENT OUT START
        //*           WITH   COR-SUPER-PIN-RCDTYPE  =  #CORE-S2  THRU  #CORE-S2
        //*   #P-VARS.#SSN           :=  COR-XREF.PH-SOCIAL-SECURITY-NO
        //*   #P-VARS.#DOB-CCYYMMDD  :=  COR-XREF.PH-DOB-DTE
        //*   #P-VARS.#DOD-CCYYMMDD  :=  COR-XREF.PH-DOD-DTE
        //*  ESCAPE ROUTINE
        //*  MOVE  BY NAME  COR-XREF.PH-NME  TO  #P-VARS
        //*  END-READ               /* FE201605 COMMENT OUT END
    }

    //
}
