/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:15:46 AM
**        * FROM NATURAL SUBPROGRAM : Twrn0901
************************************************************
**        * FILE NAME            : Twrn0901.java
**        * CLASS NAME           : Twrn0901
**        * INSTANCE NAME        : Twrn0901
************************************************************
* NAME     : TWRN0901
* FUNCTION : READ TAX CONTROL FILE 98 - DISTRIBUTION TABLE TO GET
*               THE DESCRIPTION OF TRANSACTION TYPE
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn0901 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Type_Trans;
    private DbsField pnd_Trans_Desc;

    private DataAccessProgramView vw_f98_View;
    private DbsField f98_View_Tircntl_Tbl_Nbr;

    private DbsGroup f98_View__R_Field_1;
    private DbsField f98_View_Tircntl_Tbl_Nbr_A;
    private DbsField f98_View_Tircntl_Paymt_Type;
    private DbsField f98_View_Tircntl_Settl_Type;
    private DbsField f98_View_Tircntl_Settl_Paym_Descr;
    private DbsField f98_View_Tircntl_Nbr_Year_Descr_Age;
    private DbsField pnd_Key;
    private DbsField pnd_Type;
    private DbsField pnd_B;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Tax_Year = parameters.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Tax_Year.setParameterOption(ParameterOption.ByReference);
        pnd_Type_Trans = parameters.newFieldInRecord("pnd_Type_Trans", "#TYPE-TRANS", FieldType.STRING, 2);
        pnd_Type_Trans.setParameterOption(ParameterOption.ByReference);
        pnd_Trans_Desc = parameters.newFieldInRecord("pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Trans_Desc.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_f98_View = new DataAccessProgramView(new NameInfo("vw_f98_View", "F98-VIEW"), "TIRCNTL_DIST_CODE_TBL_VIEW", "TIR_CONTROL");
        f98_View_Tircntl_Tbl_Nbr = vw_f98_View.getRecord().newFieldInGroup("f98_View_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");

        f98_View__R_Field_1 = vw_f98_View.getRecord().newGroupInGroup("f98_View__R_Field_1", "REDEFINE", f98_View_Tircntl_Tbl_Nbr);
        f98_View_Tircntl_Tbl_Nbr_A = f98_View__R_Field_1.newFieldInGroup("f98_View_Tircntl_Tbl_Nbr_A", "TIRCNTL-TBL-NBR-A", FieldType.STRING, 1);
        f98_View_Tircntl_Paymt_Type = vw_f98_View.getRecord().newFieldInGroup("f98_View_Tircntl_Paymt_Type", "TIRCNTL-PAYMT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_TYPE");
        f98_View_Tircntl_Settl_Type = vw_f98_View.getRecord().newFieldInGroup("f98_View_Tircntl_Settl_Type", "TIRCNTL-SETTL-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_SETTL_TYPE");
        f98_View_Tircntl_Settl_Paym_Descr = vw_f98_View.getRecord().newFieldInGroup("f98_View_Tircntl_Settl_Paym_Descr", "TIRCNTL-SETTL-PAYM-DESCR", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "TIRCNTL_SETTL_PAYM_DESCR");
        f98_View_Tircntl_Nbr_Year_Descr_Age = vw_f98_View.getRecord().newFieldInGroup("f98_View_Tircntl_Nbr_Year_Descr_Age", "TIRCNTL-NBR-YEAR-DESCR-AGE", 
            FieldType.STRING, 46, RepeatingFieldStrategy.None, "TIRCNTL_NBR_YEAR_DESCR_AGE");
        f98_View_Tircntl_Nbr_Year_Descr_Age.setSuperDescriptor(true);
        registerRecord(vw_f98_View);

        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 5);
        pnd_Type = localVariables.newFieldInRecord("pnd_Type", "#TYPE", FieldType.STRING, 2);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_f98_View.reset();

        parameters.reset();
        localVariables.reset();
        pnd_B.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn0901() throws Exception
    {
        super("Twrn0901");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_B.resetInitial();                                                                                                                                             //Natural: RESET INITIAL #B
        f98_View_Tircntl_Tbl_Nbr.setValue(1);                                                                                                                             //Natural: ASSIGN TIRCNTL-TBL-NBR := 1
        pnd_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, f98_View_Tircntl_Tbl_Nbr_A, pnd_Tax_Year));                                                      //Natural: COMPRESS TIRCNTL-TBL-NBR-A #TAX-YEAR TO #KEY LEAVING NO SPACE
        vw_f98_View.startDatabaseRead                                                                                                                                     //Natural: READ F98-VIEW WITH TIRCNTL-NBR-YEAR-DESCR-AGE STARTING FROM #KEY
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DESCR_AGE", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DESCR_AGE", "ASC") }
        );
        READ01:
        while (condition(vw_f98_View.readNextRow("READ01")))
        {
            pnd_Type.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, f98_View_Tircntl_Paymt_Type, f98_View_Tircntl_Settl_Type));                                 //Natural: COMPRESS TIRCNTL-PAYMT-TYPE TIRCNTL-SETTL-TYPE TO #TYPE LEAVING NO SPACE
            if (condition(pnd_Type_Trans.equals(pnd_Type)))                                                                                                               //Natural: IF #TYPE-TRANS = #TYPE
            {
                pnd_Trans_Desc.setValue(f98_View_Tircntl_Settl_Paym_Descr);                                                                                               //Natural: ASSIGN #TRANS-DESC := TIRCNTL-SETTL-PAYM-DESCR
                pnd_B.setValue(false);                                                                                                                                    //Natural: ASSIGN #B := FALSE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_B.getBoolean()))                                                                                                                                //Natural: IF #B
        {
            pnd_Trans_Desc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Type_Trans, " - ", "NO DESCRIPTION"));                                            //Natural: COMPRESS #TYPE-TRANS ' - ' 'NO DESCRIPTION' TO #TRANS-DESC LEAVING NO SPACE
            //*      #TRANS-DESC := '** NO DESCRIPTION **'
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
