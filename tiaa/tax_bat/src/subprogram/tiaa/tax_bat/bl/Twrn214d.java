/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:16:35 AM
**        * FROM NATURAL SUBPROGRAM : Twrn214d
************************************************************
**        * FILE NAME            : Twrn214d.java
**        * CLASS NAME           : Twrn214d
**        * INSTANCE NAME        : Twrn214d
************************************************************
************************************************************************
**                                                                    **
** PROGRAM:  TWRN214D - FORM SELECTION
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  A.WILNER
** PURPOSE:  SUB PROGRAM TO SEND FORMS TO POST FOR TAX REFACTOR PROJECT
**           NOTE:  THIS PROGRAM WAS COPIED FROM TWRN0114
**                  AND WILL NOW BE USED ONLY TO
**                  PRINT FORMS AT THE CONTRACT LEVEL
**
** HISTORY:
** ------------------------------------
** 10/15/2018 - VIKRAM - DOB FORMAT CHANGES FOR 1042-S - TAG: VIKRAM1
** 10/18/2018 - SAIK1  - ADD PHONE NUMBER TO PAYER's box 1099R
** 12/01/2018 - VIKRAM - EFCN DATA SIZE UPDATE FROM A7 TO A11 USED IN
**                       THE FORM 480.7C. TAG - VIKRAM2
** 01/23/2018 - SAIK2  - PLAN OR ANNUITY TYPE CODED AS QUALIFIED PRIVATE
** 01/23/2018 - SAIK3  - FIXING THE LOCAL TAX AMOUNT DISPLAY IN THE FORM
** 11/27/2020 - GHOSHJ - 480.7C DISASTER RELATED DISTRIBUTION CHANGES -
**                       /* 480.7C DISASTER CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn214d extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl6345 ldaTwrl6345;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaTwracomp pdaTwracomp;
    private PdaTwra5017 pdaTwra5017;
    private PdaTwratin pdaTwratin;
    private PdaPsta9531 pdaPsta9531;
    private PdaPsta6346 pdaPsta6346;
    private PdaTwra114f pdaTwra114f;
    private PdaTwradist pdaTwradist;
    private PdaTwracom2 pdaTwracom2;
    private PdaPsta9612 pdaPsta9612;
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Form_Page_Cnt;
    private DbsField pnd_Ws_Isn;
    private DbsField pnd_Link_Let_Type;
    private DbsField pnd_Test_Ind;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count;

    private DbsGroup pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert;
    private DbsField pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text;

    private DataAccessProgramView vw_form_Upd;
    private DbsField form_Upd_Tirf_Part_Rpt_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Ind;
    private DbsField form_Upd_Tirf_Lu_User;
    private DbsField form_Upd_Tirf_Lu_Ts;
    private DbsField form_Upd_Tirf_Mobius_Stat;
    private DbsField form_Upd_Tirf_Mobius_Ind;
    private DbsField form_Upd_Tirf_Mobius_Stat_Date;

    private DataAccessProgramView vw_twrparti_Participant_File;
    private DbsField twrparti_Participant_File_Twrparti_Status;
    private DbsField twrparti_Participant_File_Twrparti_Tax_Id;
    private DbsField twrparti_Participant_File_Twrparti_Part_Eff_End_Dte;
    private DbsField twrparti_Participant_File_Twrparti_Test_Rec_Ind;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_1;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Byte5_Pstn6346;

    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Twrpymnt_Tax_Year;
    private DbsField pymnt_Twrpymnt_Tax_Id_Nbr;
    private DbsField pymnt_Twrpymnt_Contract_Nbr;
    private DbsField pymnt_Twrpymnt_Payee_Cde;
    private DbsField pymnt_Twrpymnt_Distribution_Cde;
    private DbsField pymnt_Count_Casttwrpymnt_Payments;

    private DbsGroup pymnt_Twrpymnt_Payments;
    private DbsField pymnt_Twrpymnt_Payment_Type;
    private DbsField pymnt_Twrpymnt_Pymnt_Dte;
    private DbsField pymnt_Twrpymnt_Pymnt_Status;
    private DbsField pymnt_Twrpymnt_Settle_Type;
    private DbsField pnd_S_Twrpymnt_Form_Sd;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_2;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde;

    private DbsGroup pnd_Ws_Work_Area;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Ix;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_1042_Ix;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id;
    private DbsField pnd_Ws_Work_Area_Pnd_Ix;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara;

    private DbsGroup pnd_Ws_Work_Area__R_Field_3;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Work_Area__R_Field_4;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Form_Cc;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Form_Yy;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Tax_Rate;

    private DbsGroup pnd_Ws_Work_Area__R_Field_5;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_Tax_Ratea;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income;
    private DbsField pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld;
    private DbsField pnd_Error_Temp1;
    private DbsField pnd_Error_Temp2;
    private DbsField pnd_1099_R_Print;
    private DbsField pnd_Fld_Final_Names;
    private DbsField pnd_State_Num;
    private DbsField pnd_Reprint_No_Update;
    private DbsField pnd_Debug;
    private DbsField pnd_Cover_Letter_Ind;
    private DbsField pnd_Other;

    private DbsGroup month;
    private DbsField month_Jan;
    private DbsField month_Feb;
    private DbsField month_Mar;
    private DbsField month_Apr;
    private DbsField month_May;
    private DbsField month_Jun;
    private DbsField month_Jul;
    private DbsField month_Aug;
    private DbsField month_Sep;
    private DbsField month_Oct;
    private DbsField month_Nov;
    private DbsField month_Dec;

    private DbsGroup month__R_Field_6;
    private DbsField month_Month_Array;
    private DbsField pnd_Ws_Bday_Text;
    private DbsField pnd_Ws_Bday;

    private DbsGroup pnd_Ws_Bday__R_Field_7;
    private DbsField pnd_Ws_Bday_Pnd_Bday_Mm;
    private DbsField pnd_Ws_Bday_Pnd_Bday_Dd;
    private DbsField pnd_Ws_Bday_Pnd_Bday_Yyyy;
    private DbsField pnd_C4_Rate_P3;
    private DbsField pnd_Ws_Fin;
    private DbsField pnd_Ws_Uin;
    private DbsField pnd_Ws_Amnd_Num;

    private DbsGroup pnd_Ws_Amnd_Num__R_Field_8;
    private DbsField pnd_Ws_Amnd_Num_Pnd_Ws_Amnd_Nbr;
    private DbsField pnd_Mobindx;

    private DbsGroup pnd_Mobindx__R_Field_9;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator;
    private DbsField pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind;
    private DbsField pnd_Mobindx_Pnd_Wk_Mobindx;
    private DbsField pnd_Tirf_Tin;

    private DbsGroup pnd_Tirf_Tin__R_Field_10;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N9;
    private DbsField pnd_Tirf_Tin_Pnd_Tirf_Tin_N1;
    private DbsField pnd_P_S_Cnt;
    private DbsField pnd_Maxm_Exempt_Amt;
    private DbsField pnd_Taxa_Dis;
    private DbsField pnd_Exmpt_Dis;
    private DbsField pnd_Total_Dis;
    private DbsField pnd_Pensn;
    private DbsField pnd_Pension_Date;
    private DbsField pnd_S_Twrpymnt_Form_Sd2;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd2__R_Field_11;
    private DbsField pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year2;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd2__R_Field_12;
    private DbsField pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year_A2;
    private DbsField pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Id_Nbr2;
    private DbsField pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Contract_Nbr2;
    private DbsField pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Payee_Cde2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl6345 = new LdaTwrl6345();
        registerRecord(ldaTwrl6345);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwra5017 = new PdaTwra5017(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaPsta9531 = new PdaPsta9531(localVariables);
        pdaPsta6346 = new PdaPsta6346(localVariables);
        pdaTwra114f = new PdaTwra114f(localVariables);
        pdaTwradist = new PdaTwradist(localVariables);
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // parameters
        parameters = new DbsRecord();
        pnd_Form_Page_Cnt = parameters.newFieldInRecord("pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Form_Page_Cnt.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Isn = parameters.newFieldInRecord("pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Isn.setParameterOption(ParameterOption.ByReference);
        pdaTwra0214 = new PdaTwra0214(parameters);
        pdaPsta9610 = new PdaPsta9610(parameters);
        pnd_Link_Let_Type = parameters.newFieldArrayInRecord("pnd_Link_Let_Type", "#LINK-LET-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Link_Let_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Test_Ind = parameters.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Test_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Mobius_Psta9960_Parm_Data = localVariables.newGroupInRecord("pnd_Mobius_Psta9960_Parm_Data", "#MOBIUS-PSTA9960-PARM-DATA");
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Pst_Rqst_Id", 
            "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count = pnd_Mobius_Psta9960_Parm_Data.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Count", "#DTLS-COUNT", 
            FieldType.PACKED_DECIMAL, 5);

        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array = pnd_Mobius_Psta9960_Parm_Data.newGroupArrayInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array", 
            "#DTLS-ARRAY", new DbsArrayController(1, 100));
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Cde", 
            "#DCMNT-CDE", FieldType.STRING, 8);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Dcmnt_Vrsn_Cde", 
            "#DCMNT-VRSN-CDE", FieldType.STRING, 2);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Index_Ind", 
            "#INDEX-IND", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Man_Insert", 
            "#MAN-INSERT", FieldType.BOOLEAN, 1);
        pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text = pnd_Mobius_Psta9960_Parm_Data_Pnd_Dtls_Array.newFieldInGroup("pnd_Mobius_Psta9960_Parm_Data_Pnd_Free_Text", 
            "#FREE-TEXT", FieldType.STRING, 40);

        vw_form_Upd = new DataAccessProgramView(new NameInfo("vw_form_Upd", "FORM-UPD"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Upd_Tirf_Part_Rpt_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Date", "TIRF-PART-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_DATE");
        form_Upd_Tirf_Part_Rpt_Date.setDdmHeader("PART/RPT/DATE");
        form_Upd_Tirf_Part_Rpt_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Upd_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Upd_Tirf_Lu_User = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Upd_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Upd_Tirf_Lu_Ts = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_LU_TS");
        form_Upd_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Upd_Tirf_Mobius_Stat = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat", "TIRF-MOBIUS-STAT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_STAT");
        form_Upd_Tirf_Mobius_Stat.setDdmHeader("MOBIUS/STATUS");
        form_Upd_Tirf_Mobius_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Upd_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Upd_Tirf_Mobius_Stat_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat_Date", "TIRF-MOBIUS-STAT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIRF_MOBIUS_STAT_DATE");
        form_Upd_Tirf_Mobius_Stat_Date.setDdmHeader("MOBIUS/STATUS/DATE");
        registerRecord(vw_form_Upd);

        vw_twrparti_Participant_File = new DataAccessProgramView(new NameInfo("vw_twrparti_Participant_File", "TWRPARTI-PARTICIPANT-FILE"), "TWRPARTI_PARTICIPANT_FILE", 
            "TWR_PARTICIPANT");
        twrparti_Participant_File_Twrparti_Status = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Status", 
            "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Participant_File_Twrparti_Tax_Id = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Tax_Id", 
            "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Participant_File_Twrparti_Part_Eff_End_Dte = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Part_Eff_End_Dte", 
            "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        twrparti_Participant_File_Twrparti_Test_Rec_Ind = vw_twrparti_Participant_File.getRecord().newFieldInGroup("twrparti_Participant_File_Twrparti_Test_Rec_Ind", 
            "TWRPARTI-TEST-REC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_TEST_REC_IND");
        registerRecord(vw_twrparti_Participant_File);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_1", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id", 
            "#TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status", 
            "#TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte", 
            "#TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Byte5_Pstn6346 = localVariables.newFieldInRecord("pnd_Byte5_Pstn6346", "#BYTE5-PSTN6346", FieldType.STRING, 1);

        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT", DdmPeriodicGroups.getInstance().getGroups("TWRPYMNT_PAYMENT_FILE"));
        pymnt_Twrpymnt_Tax_Year = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_YEAR");
        pymnt_Twrpymnt_Tax_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_ID_NBR");
        pymnt_Twrpymnt_Contract_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_NBR");
        pymnt_Twrpymnt_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPYMNT_PAYEE_CDE");
        pymnt_Twrpymnt_Distribution_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_DISTRIBUTION_CDE");
        pymnt_Count_Casttwrpymnt_Payments = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Casttwrpymnt_Payments", "C*TWRPYMNT-PAYMENTS", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");

        pymnt_Twrpymnt_Payments = vw_pymnt.getRecord().newGroupInGroup("pymnt_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        pymnt_Twrpymnt_Payment_Type = pymnt_Twrpymnt_Payments.newFieldArrayInGroup("pymnt_Twrpymnt_Payment_Type", "TWRPYMNT-PAYMENT-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PAYMENT_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        pymnt_Twrpymnt_Pymnt_Dte = pymnt_Twrpymnt_Payments.newFieldArrayInGroup("pymnt_Twrpymnt_Pymnt_Dte", "TWRPYMNT-PYMNT-DTE", FieldType.STRING, 8, 
            new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        pymnt_Twrpymnt_Pymnt_Status = pymnt_Twrpymnt_Payments.newFieldArrayInGroup("pymnt_Twrpymnt_Pymnt_Status", "TWRPYMNT-PYMNT-STATUS", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_STATUS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        pymnt_Twrpymnt_Settle_Type = pymnt_Twrpymnt_Payments.newFieldArrayInGroup("pymnt_Twrpymnt_Settle_Type", "TWRPYMNT-SETTLE-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_SETTLE_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        registerRecord(vw_pymnt);

        pnd_S_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd", "#S-TWRPYMNT-FORM-SD", FieldType.STRING, 37);

        pnd_S_Twrpymnt_Form_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd__R_Field_2", "REDEFINE", pnd_S_Twrpymnt_Form_Sd);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr", 
            "#S-TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);

        pnd_Ws_Work_Area = localVariables.newGroupInRecord("pnd_Ws_Work_Area", "#WS-WORK-AREA");
        pnd_Ws_Work_Area_Pnd_Ws_Ix = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Ix", "#WS-IX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Work_Area_Pnd_Ws_1042_Ix = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_1042_Ix", "#WS-1042-IX", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1 = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1", "#WS-1042-IX1", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id", "#WS-STATE-TAX-ID", FieldType.STRING, 
            18);
        pnd_Ws_Work_Area_Pnd_Ix = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ix", "#IX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara", "#WS-TAX-YEARA", FieldType.STRING, 4);

        pnd_Ws_Work_Area__R_Field_3 = pnd_Ws_Work_Area.newGroupInGroup("pnd_Ws_Work_Area__R_Field_3", "REDEFINE", pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara);
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Year = pnd_Ws_Work_Area__R_Field_3.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 
            4);

        pnd_Ws_Work_Area__R_Field_4 = pnd_Ws_Work_Area__R_Field_3.newGroupInGroup("pnd_Ws_Work_Area__R_Field_4", "REDEFINE", pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);
        pnd_Ws_Work_Area_Pnd_Ws_Form_Cc = pnd_Ws_Work_Area__R_Field_4.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Form_Cc", "#WS-FORM-CC", FieldType.STRING, 
            2);
        pnd_Ws_Work_Area_Pnd_Ws_Form_Yy = pnd_Ws_Work_Area__R_Field_4.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Form_Yy", "#WS-FORM-YY", FieldType.STRING, 
            2);
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Rate = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Tax_Rate", "#WS-TAX-RATE", FieldType.NUMERIC, 2);

        pnd_Ws_Work_Area__R_Field_5 = pnd_Ws_Work_Area.newGroupInGroup("pnd_Ws_Work_Area__R_Field_5", "REDEFINE", pnd_Ws_Work_Area_Pnd_Ws_Tax_Rate);
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Ratea = pnd_Ws_Work_Area__R_Field_5.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_Tax_Ratea", "#WS-TAX-RATEA", FieldType.STRING, 
            2);
        pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income", "#WS-S-GROSS-INCOME", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld = pnd_Ws_Work_Area.newFieldInGroup("pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld", "#WS-S-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Error_Temp1 = localVariables.newFieldInRecord("pnd_Error_Temp1", "#ERROR-TEMP1", FieldType.STRING, 40);
        pnd_Error_Temp2 = localVariables.newFieldInRecord("pnd_Error_Temp2", "#ERROR-TEMP2", FieldType.STRING, 40);
        pnd_1099_R_Print = localVariables.newFieldInRecord("pnd_1099_R_Print", "#1099-R-PRINT", FieldType.BOOLEAN, 1);
        pnd_Fld_Final_Names = localVariables.newFieldInRecord("pnd_Fld_Final_Names", "#FLD-FINAL-NAMES", FieldType.STRING, 90);
        pnd_State_Num = localVariables.newFieldInRecord("pnd_State_Num", "#STATE-NUM", FieldType.NUMERIC, 1);
        pnd_Reprint_No_Update = localVariables.newFieldInRecord("pnd_Reprint_No_Update", "#REPRINT-NO-UPDATE", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Cover_Letter_Ind = localVariables.newFieldInRecord("pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", FieldType.BOOLEAN, 1);
        pnd_Other = localVariables.newFieldInRecord("pnd_Other", "#OTHER", FieldType.NUMERIC, 10, 2);

        month = localVariables.newGroupInRecord("month", "MONTH");
        month_Jan = month.newFieldInGroup("month_Jan", "JAN", FieldType.STRING, 3);
        month_Feb = month.newFieldInGroup("month_Feb", "FEB", FieldType.STRING, 3);
        month_Mar = month.newFieldInGroup("month_Mar", "MAR", FieldType.STRING, 3);
        month_Apr = month.newFieldInGroup("month_Apr", "APR", FieldType.STRING, 3);
        month_May = month.newFieldInGroup("month_May", "MAY", FieldType.STRING, 3);
        month_Jun = month.newFieldInGroup("month_Jun", "JUN", FieldType.STRING, 3);
        month_Jul = month.newFieldInGroup("month_Jul", "JUL", FieldType.STRING, 3);
        month_Aug = month.newFieldInGroup("month_Aug", "AUG", FieldType.STRING, 3);
        month_Sep = month.newFieldInGroup("month_Sep", "SEP", FieldType.STRING, 3);
        month_Oct = month.newFieldInGroup("month_Oct", "OCT", FieldType.STRING, 3);
        month_Nov = month.newFieldInGroup("month_Nov", "NOV", FieldType.STRING, 3);
        month_Dec = month.newFieldInGroup("month_Dec", "DEC", FieldType.STRING, 3);

        month__R_Field_6 = localVariables.newGroupInRecord("month__R_Field_6", "REDEFINE", month);
        month_Month_Array = month__R_Field_6.newFieldArrayInGroup("month_Month_Array", "MONTH-ARRAY", FieldType.STRING, 3, new DbsArrayController(1, 12));
        pnd_Ws_Bday_Text = localVariables.newFieldInRecord("pnd_Ws_Bday_Text", "#WS-BDAY-TEXT", FieldType.STRING, 3);
        pnd_Ws_Bday = localVariables.newFieldInRecord("pnd_Ws_Bday", "#WS-BDAY", FieldType.STRING, 8);

        pnd_Ws_Bday__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Bday__R_Field_7", "REDEFINE", pnd_Ws_Bday);
        pnd_Ws_Bday_Pnd_Bday_Mm = pnd_Ws_Bday__R_Field_7.newFieldInGroup("pnd_Ws_Bday_Pnd_Bday_Mm", "#BDAY-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Bday_Pnd_Bday_Dd = pnd_Ws_Bday__R_Field_7.newFieldInGroup("pnd_Ws_Bday_Pnd_Bday_Dd", "#BDAY-DD", FieldType.STRING, 2);
        pnd_Ws_Bday_Pnd_Bday_Yyyy = pnd_Ws_Bday__R_Field_7.newFieldInGroup("pnd_Ws_Bday_Pnd_Bday_Yyyy", "#BDAY-YYYY", FieldType.NUMERIC, 4);
        pnd_C4_Rate_P3 = localVariables.newFieldInRecord("pnd_C4_Rate_P3", "#C4-RATE-P3", FieldType.PACKED_DECIMAL, 5, 2);
        pnd_Ws_Fin = localVariables.newFieldInRecord("pnd_Ws_Fin", "#WS-FIN", FieldType.STRING, 22);
        pnd_Ws_Uin = localVariables.newFieldInRecord("pnd_Ws_Uin", "#WS-UIN", FieldType.STRING, 10);
        pnd_Ws_Amnd_Num = localVariables.newFieldInRecord("pnd_Ws_Amnd_Num", "#WS-AMND-NUM", FieldType.STRING, 2);

        pnd_Ws_Amnd_Num__R_Field_8 = localVariables.newGroupInRecord("pnd_Ws_Amnd_Num__R_Field_8", "REDEFINE", pnd_Ws_Amnd_Num);
        pnd_Ws_Amnd_Num_Pnd_Ws_Amnd_Nbr = pnd_Ws_Amnd_Num__R_Field_8.newFieldInGroup("pnd_Ws_Amnd_Num_Pnd_Ws_Amnd_Nbr", "#WS-AMND-NBR", FieldType.NUMERIC, 
            2);
        pnd_Mobindx = localVariables.newFieldInRecord("pnd_Mobindx", "#MOBINDX", FieldType.STRING, 9);

        pnd_Mobindx__R_Field_9 = localVariables.newGroupInRecord("pnd_Mobindx__R_Field_9", "REDEFINE", pnd_Mobindx);
        pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator = pnd_Mobindx__R_Field_9.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator", "#WK-MMM-TEST-INDICATOR", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind = pnd_Mobindx__R_Field_9.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mmm_5498_Coupon_Ind", "#WK-MMM-5498-COUPON-IND", 
            FieldType.STRING, 1);
        pnd_Mobindx_Pnd_Wk_Mobindx = pnd_Mobindx__R_Field_9.newFieldInGroup("pnd_Mobindx_Pnd_Wk_Mobindx", "#WK-MOBINDX", FieldType.STRING, 7);
        pnd_Tirf_Tin = localVariables.newFieldInRecord("pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);

        pnd_Tirf_Tin__R_Field_10 = localVariables.newGroupInRecord("pnd_Tirf_Tin__R_Field_10", "REDEFINE", pnd_Tirf_Tin);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N9 = pnd_Tirf_Tin__R_Field_10.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N9", "#TIRF-TIN-N9", FieldType.NUMERIC, 9);
        pnd_Tirf_Tin_Pnd_Tirf_Tin_N1 = pnd_Tirf_Tin__R_Field_10.newFieldInGroup("pnd_Tirf_Tin_Pnd_Tirf_Tin_N1", "#TIRF-TIN-N1", FieldType.NUMERIC, 1);
        pnd_P_S_Cnt = localVariables.newFieldInRecord("pnd_P_S_Cnt", "#P-S-CNT", FieldType.NUMERIC, 3);
        pnd_Maxm_Exempt_Amt = localVariables.newFieldInRecord("pnd_Maxm_Exempt_Amt", "#MAXM-EXEMPT-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Taxa_Dis = localVariables.newFieldInRecord("pnd_Taxa_Dis", "#TAXA-DIS", FieldType.NUMERIC, 10, 2);
        pnd_Exmpt_Dis = localVariables.newFieldInRecord("pnd_Exmpt_Dis", "#EXMPT-DIS", FieldType.NUMERIC, 10, 2);
        pnd_Total_Dis = localVariables.newFieldInRecord("pnd_Total_Dis", "#TOTAL-DIS", FieldType.NUMERIC, 10, 2);
        pnd_Pensn = localVariables.newFieldInRecord("pnd_Pensn", "#PENSN", FieldType.NUMERIC, 5);
        pnd_Pension_Date = localVariables.newFieldInRecord("pnd_Pension_Date", "#PENSION-DATE", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd2 = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd2", "#S-TWRPYMNT-FORM-SD2", FieldType.STRING, 37);

        pnd_S_Twrpymnt_Form_Sd2__R_Field_11 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd2__R_Field_11", "REDEFINE", pnd_S_Twrpymnt_Form_Sd2);
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year2 = pnd_S_Twrpymnt_Form_Sd2__R_Field_11.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year2", 
            "#S-TWRPYMNT-TAX-YEAR2", FieldType.NUMERIC, 4);

        pnd_S_Twrpymnt_Form_Sd2__R_Field_12 = pnd_S_Twrpymnt_Form_Sd2__R_Field_11.newGroupInGroup("pnd_S_Twrpymnt_Form_Sd2__R_Field_12", "REDEFINE", pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year2);
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year_A2 = pnd_S_Twrpymnt_Form_Sd2__R_Field_12.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year_A2", 
            "#S-TWRPYMNT-TAX-YEAR-A2", FieldType.STRING, 4);
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Id_Nbr2 = pnd_S_Twrpymnt_Form_Sd2__R_Field_11.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Id_Nbr2", 
            "#S-TWRPYMNT-TAX-ID-NBR2", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Contract_Nbr2 = pnd_S_Twrpymnt_Form_Sd2__R_Field_11.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Contract_Nbr2", 
            "#S-TWRPYMNT-CONTRACT-NBR2", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Payee_Cde2 = pnd_S_Twrpymnt_Form_Sd2__R_Field_11.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Payee_Cde2", 
            "#S-TWRPYMNT-PAYEE-CDE2", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_Upd.reset();
        vw_twrparti_Participant_File.reset();
        vw_pymnt.reset();

        ldaTwrl6345.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9415.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Ws_Work_Area_Pnd_Ix.setInitialValue(1);
        pnd_State_Num.setInitialValue(0);
        pnd_Reprint_No_Update.setInitialValue(false);
        pnd_Debug.setInitialValue(false);
        pnd_Cover_Letter_Ind.setInitialValue(false);
        month_Jan.setInitialValue("JAN");
        month_Feb.setInitialValue("FEB");
        month_Mar.setInitialValue("MAR");
        month_Apr.setInitialValue("APR");
        month_May.setInitialValue("MAY");
        month_Jun.setInitialValue("JUN");
        month_Jul.setInitialValue("JUL");
        month_Aug.setInitialValue("AUG");
        month_Sep.setInitialValue("SEP");
        month_Oct.setInitialValue("OCT");
        month_Nov.setInitialValue("NOV");
        month_Dec.setInitialValue("DEC");
        pnd_Maxm_Exempt_Amt.setInitialValue(10000);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn214d() throws Exception
    {
        super("Twrn214d");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  -----------------------------------------------------
        //*  MOVE TRUE TO #DEBUG
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, NEWLINE,Global.getPROGRAM(), new FieldAttributes ("AD=I"),"STARTING");                                                                  //Natural: WRITE / *PROGRAM ( AD = I ) 'STARTING'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().reset();                                                                                             //Natural: RESET #TWRA0214-RET-MSG #TWRA0214-RET-CODE
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().reset();
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().equals(getZero())))                                                                        //Natural: IF #TWRA0214-ISN EQ 0
        {
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                                   //Natural: MOVE 99 TO #TWRA0214-RET-CODE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue("ISN is Zero");                                                                         //Natural: MOVE 'ISN is Zero' TO #TWRA0214-RET-MSG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().greater(" ")))                                                                      //Natural: IF #TWRA0214-MOBIUS-KEY > ' '
        {
            ldaTwrl6345.getTax_Form_Data_Mobius_Index().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                        //Natural: ASSIGN TAX-FORM-DATA.MOBIUS-INDEX := #TWRA0214-MOBIUS-KEY
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------------------------------- MAIN PROGRAM
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) FORM WITH ISN = #TWRA0214-ISN
        (
        "READ_FORM",
        new Wc[] { new Wc("ISN", ">=", pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn(), WcType.BY) },
        new Oc[] { new Oc("ISN", "ASC") },
        1
        );
        READ_FORM:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("READ_FORM")))
        {
            if (condition(ldaTwrl9710.getVw_form().getAstISN("READ_FORM") != pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getInt()))                          //Natural: IF *ISN NE #TWRA0214-ISN
            {
                pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(98);                                                                               //Natural: MOVE 98 TO #TWRA0214-RET-CODE
                pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(DbsUtil.compress("ISN", pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn(),   //Natural: COMPRESS 'ISN' #TWRA0214-ISN 'not found on file' INTO #TWRA0214-RET-MSG
                    "not found on file"));
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Isn.equals(getZero())))                                                                                                                  //Natural: IF #WS-ISN = 0
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals("A") && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive().notEquals("Y"))) //Natural: IF TIRF-ACTIVE-IND NE 'A' AND #TWRA0214-ACCEPT-INACTIVE NE 'Y'
                {
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(97);                                                                           //Natural: MOVE 97 TO #TWRA0214-RET-CODE
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(DbsUtil.compress("Contract", ldaTwrl9710.getForm_Tirf_Contract_Nbr(),           //Natural: COMPRESS 'Contract' TIRF-CONTRACT-NBR TIRF-PAYEE-CDE 'was changed - Process Abort' INTO #TWRA0214-RET-MSG
                        ldaTwrl9710.getForm_Tirf_Payee_Cde(), "was changed - Process Abort"));
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *                                               MUKHER 12/3/14 >>
            pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                             //Natural: RESET #TWRPARTI-CURR-REC-SD #BYTE5-PSTN6346
            pnd_Byte5_Pstn6346.reset();
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                        //Natural: MOVE FORM.TIRF-TIN TO #TWRPARTI-TAX-ID
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #TWRPARTI-STATUS
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                  //Natural: MOVE '99999999' TO #TWRPARTI-PART-EFF-END-DTE
            vw_twrparti_Participant_File.startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) TWRPARTI-PARTICIPANT-FILE TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
            (
            "F1",
            new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", "=", pnd_Twrparti_Curr_Rec_Sd, WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_twrparti_Participant_File.readNextRow("F1", true)))
            {
                vw_twrparti_Participant_File.setIfNotFoundControlFlag(false);
                if (condition(vw_twrparti_Participant_File.getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORDS FOUND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  D-INDD 010515
                pnd_Test_Ind.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                                                   //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #TEST-IND
                //*   /* SNEHA CHANGES STARTED - SINHSN
                pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(twrparti_Participant_File_Twrparti_Test_Rec_Ind);                                                          //Natural: MOVE TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND TO #WK-MMM-TEST-INDICATOR
                if (condition(pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.equals("Y")))                                                                                         //Natural: IF #WK-MMM-TEST-INDICATOR EQ 'Y'
                {
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue("Y");                                                                                                  //Natural: MOVE 'Y' TO #WK-MMM-TEST-INDICATOR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Mobindx_Pnd_Wk_Mmm_Test_Indicator.setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #WK-MMM-TEST-INDICATOR
                }                                                                                                                                                         //Natural: END-IF
                //*  /* SNEHA CHANGES ENDED - SINHSN
                if (condition(twrparti_Participant_File_Twrparti_Test_Rec_Ind.equals("Y")))                                                                               //Natural: IF TWRPARTI-PARTICIPANT-FILE.TWRPARTI-TEST-REC-IND EQ 'Y'
                {
                    pnd_Byte5_Pstn6346.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #BYTE5-PSTN6346
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *                                               MUKHER 12/314 <<
            pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                              //Natural: MOVE TIRF-TAX-YEAR TO #WS-TAX-YEARA
            //*  DUTTAD
            pnd_Ws_Fin.setValue(ldaTwrl9710.getForm_Tirf_Fin());                                                                                                          //Natural: MOVE FORM.TIRF-FIN TO #WS-FIN
            //*  << SINHASN2
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2017")))                                                                                    //Natural: IF FORM.TIRF-TAX-YEAR GE '2017'
            {
                pnd_Ws_Uin.setValue(ldaTwrl9710.getForm_Tirf_1042s_Org_Cntl_Nbr());                                                                                       //Natural: MOVE FORM.TIRF-1042S-ORG-CNTL-NBR TO #WS-UIN
                pnd_Ws_Amnd_Num.setValueEdited(ldaTwrl9710.getForm_Tirf_1042s_Amd_Nbr(),new ReportEditMask("99"));                                                        //Natural: MOVE EDITED FORM.TIRF-1042S-AMD-NBR ( EM = 99 ) TO #WS-AMND-NUM
                //*  << SINHASN2
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------------
                                                                                                                                                                          //Natural: PERFORM PREPARE-REC-FOR-POST
            sub_Prepare_Rec_For_Post();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl6345.getTax_Form_Data_Form_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Form_Yy);                                                                           //Natural: MOVE #WS-FORM-YY TO TAX-FORM-DATA.FORM-YEAR
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy());                                            //Natural: WRITE *PROGRAM '=' #TWRA0214-HARD-COPY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, Global.getPROGRAM(),"=",pnd_1099_R_Print);                                                                                          //Natural: WRITE *PROGRAM '=' #1099-R-PRINT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, Global.getPROGRAM(),"=",ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind());                                                                   //Natural: WRITE *PROGRAM '=' TIRF-PART-RPT-IND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, Global.getPROGRAM(),"=",ldaTwrl9710.getForm_Tirf_Part_Rpt_Date(),NEWLINE);                                                          //Natural: WRITE *PROGRAM '=' FORM.TIRF-PART-RPT-DATE /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().equals("Y") && pnd_1099_R_Print.getBoolean()))                                   //Natural: IF #TWRA0214-HARD-COPY = 'Y' AND #1099-R-PRINT
            {
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                sub_Call_Post();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().notEquals("Y")))                                                                 //Natural: IF #TWRA0214-HARD-COPY NE 'Y'
            {
                short decideConditionsMet2018 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-FORM-TYPE = 01 AND #1099-R-PRINT
                if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1) && pnd_1099_R_Print.getBoolean()))
                {
                    decideConditionsMet2018++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN FORM.TIRF-FORM-TYPE NE 01
                else if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(1)))
                {
                    decideConditionsMet2018++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet2018 > 0))
                {
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                    sub_Call_Post();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  NOT PRINTED
            //*  MOORE
            short decideConditionsMet2032 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-PART-RPT-IND;//Natural: VALUE ' ', 'N'
            if (condition((ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals(" ") || ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals("N"))))
            {
                decideConditionsMet2032++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((ldaTwrl9710.getForm_Tirf_Part_Rpt_Ind().equals("M"))))
            {
                decideConditionsMet2032++;
                if (condition(ldaTwrl9710.getForm_Tirf_Moore_Hold_Ind().equals(" ")))                                                                                     //Natural: IF FORM.TIRF-MOORE-HOLD-IND = ' '
                {
                    pnd_Reprint_No_Update.setValue(true);                                                                                                                 //Natural: ASSIGN #REPRINT-NO-UPDATE := TRUE
                    //*  OTHER PRINT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Reprint_No_Update.setValue(true);                                                                                                                     //Natural: ASSIGN #REPRINT-NO-UPDATE := TRUE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  FO 12/01
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().greater(getZero())))                                                              //Natural: IF #TWRA0214-RET-CODE GT 0
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  -----------
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().notEquals("M")))                                                           //Natural: IF #TWRA0214-MOBIUS-MIGR-IND NE 'M'
            {
                GET1:                                                                                                                                                     //Natural: GET FORM-UPD #TWRA0214-ISN
                vw_form_Upd.readByID(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getLong(), "GET1");
                if (condition(! (pnd_Reprint_No_Update.getBoolean())))                                                                                                    //Natural: IF NOT #REPRINT-NO-UPDATE
                {
                    form_Upd_Tirf_Part_Rpt_Date.setValue(Global.getDATX());                                                                                               //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-DATE = *DATX
                    form_Upd_Tirf_Part_Rpt_Ind.setValue("P");                                                                                                             //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-IND = 'P'
                    form_Upd_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN FORM-UPD.TIRF-LU-USER = *INIT-USER
                    form_Upd_Tirf_Lu_Ts.setValue(Global.getDATX());                                                                                                       //Natural: ASSIGN FORM-UPD.TIRF-LU-TS = *DATX
                }                                                                                                                                                         //Natural: END-IF
                //*  IF #DEBUG
                //*    WRITE *PROGRAM '=' #TWRA0114-HARD-COPY
                //*    WRITE *PROGRAM '=' #PCKGE-IMMDTE-PRNT-IND
                //*      #PCKGE-IMMDTE-PRNT-IND (EM=H)
                //*  END-IF
                if (condition(((pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().notEquals("Y") && pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind().notEquals("Y"))  //Natural: IF #TWRA0214-LINK-AREA.#TWRA0214-HARD-COPY NE 'Y' AND #TWRA0214-LINK-AREA.#PCKGE-IMMDTE-PRNT-IND NE 'Y' AND ( #TWRA0214-MOBIUS-MIGR-IND = 'P' OR = ' ' )
                    && (pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("P") || pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals(" ")))))
                {
                    if (condition(pnd_Debug.getBoolean()))                                                                                                                //Natural: IF #DEBUG
                    {
                        getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy());                                    //Natural: WRITE *PROGRAM '=' #TWRA0214-HARD-COPY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, Global.getPROGRAM(),"=",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind(),pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind(),  //Natural: WRITE *PROGRAM '=' #PCKGE-IMMDTE-PRNT-IND #PCKGE-IMMDTE-PRNT-IND ( EM = H )
                            new ReportEditMask ("H"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*      IF READ-FORM.TIRF-PIN = 0 OR = 9999999      /* PIN-NBR EXPANSION
                    //*  PIN-NBR EXPANSION
                    if (condition(ldaTwrl9710.getForm_Tirf_Pin().equals(getZero()) || ldaTwrl9710.getForm_Tirf_Pin().equals(new DbsDecimal("999999999999"))))             //Natural: IF FORM.TIRF-PIN = 0 OR = 999999999999
                    {
                        form_Upd_Tirf_Mobius_Stat.setValue("B ");                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT = 'B '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        form_Upd_Tirf_Mobius_Stat.setValue("A2");                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT = 'A2'
                    }                                                                                                                                                     //Natural: END-IF
                    form_Upd_Tirf_Mobius_Stat_Date.setValue(Global.getDATX());                                                                                            //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT-DATE = *DATX
                    form_Upd_Tirf_Mobius_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-IND = ' '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"*** UPDATE ***");                                                                                          //Natural: WRITE *PROGRAM '*** UPDATE ***'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Stat);                                                                             //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-STAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Stat_Date);                                                                        //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-STAT-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, Global.getPROGRAM(),"=",form_Upd_Tirf_Mobius_Ind);                                                                              //Natural: WRITE *PROGRAM '=' FORM-UPD.TIRF-MOBIUS-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                vw_form_Upd.updateDBRow("GET1");                                                                                                                          //Natural: UPDATE ( GET1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  -----------
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREPARE-REC-FOR-POST
        //*  ======================================
        //*  ---------------
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-1099R
        //*  ----------
        //*  ------------------------------  STATE INFO
        //* *  R-Q1(1) R-Q2A(1) R-Q2B(1) R-Q2A(1) R-Q2C(1) R-Q4(1) R-Q5(1)
        //* *  R-Q7B(1) R-Q9A(1) R-Q9B(1)
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-1099I
        //*  ------------
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-5498
        //*  ----------
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-1042S
        //*        S-SAEIN (#IX)                          := TWRATIN.#O-TIN
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-480-7C
        //*  << SINHASN1
        //*  >> SINHASN1
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-480-6A
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-480-6B
        //*  ---------
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-NR4
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST
        //*  CALL POST OPEN
        //*  CALL POST WRITE
        //*  CALL POST CLOSE
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-STATE-CODE
        //*    VALUE 'T'
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOBIUS-KEY
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FATCA-1042S
        //* *********************************************************
        //* **************************************   /* 480.7C DISASTER CHANGE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PENSION-DATE-CHECK
        //* *
        //* **********************************************************
    }
    //*  COMPANY LOOKUP
    private void sub_Prepare_Rec_For_Post() throws Exception                                                                                                              //Natural: PREPARE-REC-FOR-POST
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                            //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                     //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM.TIRF-COMPANY-CDE
        //*  TIN/TIN TYPE CONVERSN
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                       //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
        pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                          //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  ... NOT FOUND
        if (condition(pdaTwratin.getTwratin_Pnd_O_Tin().equals(" ")))                                                                                                     //Natural: IF TWRATIN.#O-TIN = ' '
        {
            pdaTwratin.getTwratin_Pnd_O_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#O-TIN := FORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------
        ldaTwrl6345.getTax_Form_Data_Form_Control_Dtls().reset();                                                                                                         //Natural: RESET FORM-CONTROL-DTLS
        //*  << DUTTAD2
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            //*  MANSOOR
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                                    //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION #COVER-LETTER-IND
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
            pnd_Cover_Letter_Ind.setValue(true);
            //*  MANSOOR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("M")))                                                              //Natural: IF #TWRA0214-MOBIUS-MIGR-IND = 'M'
            {
                //*  FO 2/03
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                                //Natural: MOVE TRUE TO DONT-PULL-1099R-INSTRUCTION DONT-PULL-5498F-INSTRUCTION DONT-PULL-5498P-INSTRUCTION DONT-PULL-NR4I-INSTRUCTION DONT-PULL-4806A-INSTRUCTION DONT-PULL-1042S-INSTRUCTION #COVER-LETTER-IND
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);
                ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);
                pnd_Cover_Letter_Ind.setValue(true);
            }                                                                                                                                                             //Natural: END-IF
            //*  DUTTAD2
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_Number_Of_Forms().setValue(1);                                                                                                       //Natural: ASSIGN NUMBER-OF-FORMS := 1
        short decideConditionsMet2222 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-FORM-TYPE;//Natural: VALUE 01
        if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(1))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-1099R
            sub_Form_1099r();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(2))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-1099I
            sub_Form_1099i();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 03
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(3))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-1042S
            sub_Form_1042s();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 04
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(4))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-5498
            sub_Form_5498();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 05
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(5))))
        {
            decideConditionsMet2222++;
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2007")))                                                                                    //Natural: IF FORM.TIRF-TAX-YEAR GE '2007'
            {
                                                                                                                                                                          //Natural: PERFORM FORM-480-7C
                sub_Form_480_7c();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM FORM-480-6A
                sub_Form_480_6a();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 06
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(6))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-480-6B
            sub_Form_480_6b();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 07
        else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(7))))
        {
            decideConditionsMet2222++;
                                                                                                                                                                          //Natural: PERFORM FORM-NR4
            sub_Form_Nr4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Form_1099r() throws Exception                                                                                                                        //Natural: FORM-1099R
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==========================
        ldaTwrl6345.getTax_Form_Data_F1099r_Form_Array().getValue("*").reset();                                                                                           //Natural: RESET F1099R-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_1099_R());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-1099-R
        pnd_1099_R_Print.setValue(true);                                                                                                                                  //Natural: ASSIGN #1099-R-PRINT := TRUE
        ldaTwrl6345.getTax_Form_Data_R_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN R-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_R_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN R-CORR ( 1 ) := 'X'
            //* SAIK1
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN R-PAY ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN R-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN R-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,4).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Phone());                                                             //Natural: ASSIGN R-PAY ( 1,4 ) := #TWRACOMP.#PHONE
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN R-PAY ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_R_Pay().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN R-PAY ( 1,6 ) := ' '
        ldaTwrl6345.getTax_Form_Data_R_Payid().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO R-PAYID ( 1 )
        //*  ----------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                          //Natural: IF FORM.TIRF-TAX-ID-TYPE = '4' OR = '5'
        {
            ldaTwrl6345.getTax_Form_Data_R_Recid().getValue(1).reset();                                                                                                   //Natural: RESET R-RECID ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_R_Recid().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                               //Natural: MOVE TWRATIN.#O-TIN TO R-RECID ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                         //Natural: ASSIGN R-REC ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN R-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN R-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN R-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN R-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_R_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN R-REC ( 1,6 ) := TIRF-ADDR-LN5
        ldaTwrl6345.getTax_Form_Data_R_Anum().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X"));                        //Natural: MOVE EDITED TIRF-CONTRACT-NBR ( EM = XXXXXXX-X ) TO R-ANUM ( 1 )
        ldaTwrl6345.getTax_Form_Data_R_Anum().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_R_Anum().getValue(1),     //Natural: COMPRESS R-ANUM ( 1 ) '-' TIRF-PAYEE-CDE INTO R-ANUM ( 1 ) LEAVING NO
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        ldaTwrl6345.getTax_Form_Data_R_Q1().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                        //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q1 ( 1 )
        //*  IF TRADITIONAL OR CLASSIC IRA, MOVE TAXABLE AMOUNT TO FORM FOR 2003+.
        //*  THE FORM ROLL-UP PROCESS IDENTIFIES THESE IRAS, AND SETS
        //*  TAXABLE AMOUNT EQUAL TO GROSS AMOUNT. WE TRUST FORM ROLL-UP.
        if (condition(ldaTwrl9710.getForm_Tirf_Taxable_Not_Det().equals("Y")))                                                                                            //Natural: IF FORM.TIRF-TAXABLE-NOT-DET = 'Y'
        {
            ldaTwrl6345.getTax_Form_Data_R_Q2b().getValue(1).setValue("X", MoveOption.RightJustified);                                                                    //Natural: MOVE RIGHT 'X' TO R-Q2B ( 1 )
            if (condition(ldaTwrl9710.getForm_Tirf_Taxable_Amt().notEquals(getZero())))                                                                                   //Natural: IF FORM.TIRF-TAXABLE-AMT NE 0
            {
                //*  TAXABLE AMT SET BY ROLLUP
                ldaTwrl6345.getTax_Form_Data_R_Q2a().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Taxable_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));             //Natural: MOVE EDITED FORM.TIRF-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q2A ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TAXABLE DETERMINED
            ldaTwrl6345.getTax_Form_Data_R_Q2a().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Taxable_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                 //Natural: MOVE EDITED FORM.TIRF-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q2A ( 1 )
            //*  IVC KNOWN
            ldaTwrl6345.getTax_Form_Data_R_Q5().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Ivc_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                      //Natural: MOVE EDITED FORM.TIRF-IVC-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q5 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_R_Q2c().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Tot_Distr());                                                                  //Natural: MOVE FORM.TIRF-TOT-DISTR TO R-Q2C ( 1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tot_Distr().equals("X")))                                                                                                  //Natural: IF FORM.TIRF-TOT-DISTR = 'X'
        {
            ldaTwrl6345.getTax_Form_Data_R_Q2a().getValue(1).reset();                                                                                                     //Natural: RESET R-Q2A ( 1 ) R-Q5 ( 1 )
            ldaTwrl6345.getTax_Form_Data_R_Q5().getValue(1).reset();
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_R_Q4().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                    //Natural: MOVE EDITED FORM.TIRF-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q4 ( 1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().equals(getZero())))                                                                                        //Natural: IF FORM.TIRF-FED-TAX-WTHLD = 0
        {
            ldaTwrl6345.getTax_Form_Data_R_Q4().getValue(1).reset();                                                                                                      //Natural: RESET R-Q4 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #DEBUG
        //*    WRITE /// '=' FORM.TIRF-ROTH-YEAR //
        //*  END-IF
        ldaTwrl6345.getTax_Form_Data_R_Q16().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Roth_Year(),new ReportEditMask("ZZZZ"));                                 //Natural: MOVE EDITED FORM.TIRF-ROTH-YEAR ( EM = ZZZZ ) TO R-Q16 ( 1 )
        //* ** USE NEW ROUTINE TO CONVERT DISTRIBUTION CODE          9/24/03  RM
        pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
        pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
        pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
        pdaTwradist.getPnd_Twradist_Pnd_Tax_Year_A().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                       //Natural: ASSIGN #TWRADIST.#TAX-YEAR-A := FORM.TIRF-TAX-YEAR
        pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                                  //Natural: ASSIGN #TWRADIST.#IN-DIST := FORM.TIRF-DISTRIBUTION-CDE
        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist());                                                                        //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST
        if (condition(Global.isEscape())) return;
        ldaTwrl6345.getTax_Form_Data_R_Q7a().getValue(1).setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                                            //Natural: ASSIGN R-Q7A ( 1 ) := #TWRADIST.#OUT-DIST
        if (condition(ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("N") || ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("R")))                                //Natural: IF FORM.TIRF-DISTRIBUTION-CDE = 'N' OR = 'R'
        {
            //* *                         OR = 'H' OR = 'G' OR = 'Z'     /* JH 5/23/02
            ldaTwrl6345.getTax_Form_Data_R_Q5().getValue(1).reset();                                                                                                      //Natural: RESET R-Q5 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_R_Q9a().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Pct_Of_Tot(),new ReportEditMask("ZZZ"));                                 //Natural: MOVE EDITED FORM.TIRF-PCT-OF-TOT ( EM = ZZZ ) TO R-Q9A ( 1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tot_Contractual_Ivc().notEquals(getZero())))                                                                               //Natural: IF FORM.TIRF-TOT-CONTRACTUAL-IVC NE 0
        {
            ldaTwrl6345.getTax_Form_Data_R_Q9b().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Tot_Contractual_Ivc(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));         //Natural: MOVE EDITED FORM.TIRF-TOT-CONTRACTUAL-IVC ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q9B ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Distr().equals("Y")))                                                                                                  //Natural: IF FORM.TIRF-IRA-DISTR = 'Y'
        {
            ldaTwrl6345.getTax_Form_Data_R_Q7b().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Ira_Distr());                                                              //Natural: MOVE FORM.TIRF-IRA-DISTR TO R-Q7B ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_R_Q7b().getValue(1).setValue(" ");                                                                                               //Natural: MOVE ' ' TO R-Q7B ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        //* *
        //*  VIKRAM START
        if (condition(ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("C")))                                                                                           //Natural: IF FORM.TIRF-DISTRIBUTION-CDE EQ 'C'
        {
            pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                                               //Natural: RESET #S-TWRPYMNT-FORM-SD
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                    //Natural: MOVE #WS-TAX-YEAR TO #S-TWRPYMNT-TAX-YEAR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                    //Natural: MOVE FORM.TIRF-TIN TO #S-TWRPYMNT-TAX-ID-NBR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                         //Natural: MOVE FORM.TIRF-CONTRACT-NBR TO #S-TWRPYMNT-CONTRACT-NBR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                               //Natural: MOVE FORM.TIRF-PAYEE-CDE TO #S-TWRPYMNT-PAYEE-CDE
            vw_pymnt.startDatabaseRead                                                                                                                                    //Natural: READ PYMNT BY TWRPYMNT-FORM-SD STARTING FROM #S-TWRPYMNT-FORM-SD
            (
            "READ01",
            new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
            new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
            );
            READ01:
            while (condition(vw_pymnt.readNextRow("READ01")))
            {
                if (condition(pymnt_Twrpymnt_Tax_Year.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year) || pymnt_Twrpymnt_Tax_Id_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr)  //Natural: IF PYMNT.TWRPYMNT-TAX-YEAR NE #S-TWRPYMNT-TAX-YEAR OR PYMNT.TWRPYMNT-TAX-ID-NBR NE #S-TWRPYMNT-TAX-ID-NBR OR PYMNT.TWRPYMNT-CONTRACT-NBR NE #S-TWRPYMNT-CONTRACT-NBR OR PYMNT.TWRPYMNT-PAYEE-CDE NE #S-TWRPYMNT-PAYEE-CDE
                    || pymnt_Twrpymnt_Contract_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr) || pymnt_Twrpymnt_Payee_Cde.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pymnt_Twrpymnt_Distribution_Cde.equals("C")))                                                                                               //Natural: IF PYMNT.TWRPYMNT-DISTRIBUTION-CDE EQ 'C'
                {
                    FOR01:                                                                                                                                                //Natural: FOR #P-S-CNT = 1 TO 24
                    for (pnd_P_S_Cnt.setValue(1); condition(pnd_P_S_Cnt.lessOrEqual(24)); pnd_P_S_Cnt.nadd(1))
                    {
                        if (condition((((pymnt_Twrpymnt_Payment_Type.getValue(pnd_P_S_Cnt).equals("D") && pymnt_Twrpymnt_Settle_Type.getValue(pnd_P_S_Cnt).equals("C"))   //Natural: IF PYMNT.TWRPYMNT-PAYMENT-TYPE ( #P-S-CNT ) EQ 'D' AND PYMNT.TWRPYMNT-SETTLE-TYPE ( #P-S-CNT ) EQ 'C' AND PYMNT.TWRPYMNT-PYMNT-STATUS ( #P-S-CNT ) EQ 'C' OR EQ ' ' AND PYMNT.TWRPYMNT-PYMNT-DTE ( #P-S-CNT ) NE ' '
                            && (pymnt_Twrpymnt_Pymnt_Status.getValue(pnd_P_S_Cnt).equals("C") || pymnt_Twrpymnt_Pymnt_Status.getValue(pnd_P_S_Cnt).equals(" "))) 
                            && pymnt_Twrpymnt_Pymnt_Dte.getValue(pnd_P_S_Cnt).notEquals(" "))))
                        {
                            ldaTwrl6345.getTax_Form_Data_R_Payment_Date().getValue(1).setValue(pymnt_Twrpymnt_Pymnt_Dte.getValue(pnd_P_S_Cnt));                           //Natural: ASSIGN R-PAYMENT-DATE ( 1 ) := PYMNT.TWRPYMNT-PYMNT-DTE ( #P-S-CNT )
                            ldaTwrl6345.getTax_Form_Data_R_Payment_Date().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_R_Payment_Date().getValue(1).getSubstring(5,2),  //Natural: COMPRESS SUBSTR ( R-PAYMENT-DATE ( 1 ) ,5,2 ) '-' SUBSTR ( R-PAYMENT-DATE ( 1 ) ,7,2 ) '-' SUBSTR ( R-PAYMENT-DATE ( 1 ) ,1,4 ) INTO R-PAYMENT-DATE ( 1 ) LEAVING NO SPACE
                                "-", ldaTwrl6345.getTax_Form_Data_R_Payment_Date().getValue(1).getSubstring(7,2), "-", ldaTwrl6345.getTax_Form_Data_R_Payment_Date().getValue(1).getSubstring(1,
                                4)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            //*  VIKRAM ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        //* *
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2011")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2011'
        {
            ldaTwrl6345.getTax_Form_Data_R_Expansion4().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Irr_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));              //Natural: MOVE EDITED FORM.TIRF-IRR-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-EXPANSION4 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #WS-IX 1 C*TIRF-1099-R-STATE-GRP
        for (pnd_Ws_Work_Area_Pnd_Ws_Ix.setValue(1); condition(pnd_Ws_Work_Area_Pnd_Ws_Ix.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp())); 
            pnd_Ws_Work_Area_Pnd_Ws_Ix.nadd(1))
        {
            if (condition(pnd_Ws_Isn.greater(getZero())))                                                                                                                 //Natural: IF #WS-ISN > 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9710.getForm_Tirf_State_Rpt_Ind().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).notEquals("Y")))                                              //Natural: IF FORM.TIRF-STATE-RPT-IND ( #WS-IX ) NE 'Y'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_1099_R_Print.setValue(true);                                                                                                                              //Natural: ASSIGN #1099-R-PRINT := TRUE
            if (condition(pnd_State_Num.notEquals(1)))                                                                                                                    //Natural: IF #STATE-NUM NE 1
            {
                //*  FO 8/99
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-STATE-CODE
                sub_Translate_State_Code();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl6345.getTax_Form_Data_R_Q10().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new       //Natural: MOVE EDITED FORM.TIRF-STATE-TAX-WTHLD ( #WS-IX ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) TO R-Q10 ( 1 )
                    ReportEditMask("-ZZZ,ZZZ,ZZ9.99"));
                //*  ------
                ldaTwrl6345.getTax_Form_Data_R_Q11().getValue(1).setValue(pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id);                                                          //Natural: MOVE #WS-STATE-TAX-ID TO R-Q11 ( 1 )
                //*  ------
                ldaTwrl6345.getTax_Form_Data_R_Q12().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new           //Natural: MOVE EDITED FORM.TIRF-STATE-DISTR ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q12 ( 1 )
                    ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                ldaTwrl6345.getTax_Form_Data_R_Q14().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix));                      //Natural: MOVE FORM.TIRF-LOC-CODE ( #WS-IX ) TO R-Q14 ( 1 )
                if (condition(ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals("3E")))                                                     //Natural: IF FORM.TIRF-LOC-CODE ( #WS-IX ) = '3E'
                {
                    ldaTwrl6345.getTax_Form_Data_R_Q14().getValue(1).setValue("NYC");                                                                                     //Natural: MOVE 'NYC' TO R-Q14 ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_R_Q13().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new         //Natural: MOVE EDITED FORM.TIRF-LOC-TAX-WTHLD ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q13 ( 1 )
                    ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                //*  FO 7/00
                if (condition((ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(getZero()) && ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(" ")))) //Natural: IF ( FORM.TIRF-LOC-TAX-WTHLD ( #WS-IX ) = 0 AND FORM.TIRF-LOC-CODE ( #WS-IX ) = ' ' )
                {
                    ldaTwrl6345.getTax_Form_Data_R_Q13().getValue(1).reset();                                                                                             //Natural: RESET R-Q13 ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_R_Q15().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new             //Natural: MOVE EDITED FORM.TIRF-LOC-DISTR ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q15 ( 1 )
                    ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                //*  FO 7/00
                if (condition((ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(getZero()) && ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(" ")))) //Natural: IF ( FORM.TIRF-LOC-DISTR ( #WS-IX ) = 0 AND FORM.TIRF-LOC-CODE ( #WS-IX ) = ' ' )
                {
                    ldaTwrl6345.getTax_Form_Data_R_Q15().getValue(1).reset();                                                                                             //Natural: RESET R-Q15 ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  R-Q10    (1) := '$10,000,000.00'
            //*  R-Q11    (1) := 'R11XXXXXXXXXXX'
            //*  R-Q12    (1) := '$12,000,000.00'
            //*  R-Q13    (1) := '$13,000,000.00'
            //*  R-Q14    (1) := 'R14XXXXXXXXXXX'
            //*  R-Q15    (1) := '$15,000,000.00'
            //*  ---------------------
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().equals("Y") && ldaTwrl9710.getForm_Tirf_State_Hardcopy_Ind().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(" "))) //Natural: IF #TWRA0214-HARD-COPY = 'Y' AND FORM.TIRF-STATE-HARDCOPY-IND ( #WS-IX ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *IF  #LINK-LET-TYPE(5)  NE ' ' OR  #LINK-LET-TYPE(6)  NE ' '
            //* *    OR #TWRA0114-HARD-COPY  =  'Y'
            //* *  DONT-PULL-1099R-INSTRUCTION := TRUE
            //* *  DONT-PULL-5498F-INSTRUCTION := TRUE
            //* *  DONT-PULL-NR4I-INSTRUCTION  := TRUE
            //* *  DONT-PULL-4806A-INSTRUCTION := TRUE
            //* *END-IF
            //*  ---------------------
            if (condition(pnd_Ws_Isn.greater(getZero())))                                                                                                                 //Natural: IF #WS-ISN > 0
            {
                if (condition(pnd_Form_Page_Cnt.notEquals(pnd_Ws_Work_Area_Pnd_Ws_Ix)))                                                                                   //Natural: IF #FORM-PAGE-CNT NOT = #WS-IX
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl9710.getForm_Tirf_Res_Issue_Type().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals("3")))                                                //Natural: IF FORM.TIRF-RES-ISSUE-TYPE ( #WS-IX ) = '3'
                {
                    ldaTwrl6345.getTax_Form_Data_R_Corr().getValue(1).setValue("X");                                                                                      //Natural: ASSIGN R-CORR ( 1 ) := 'X'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greater("2000")))                                                                                       //Natural: IF FORM.TIRF-TAX-YEAR > '2000'
                {
                    pnd_State_Num.nadd(1);                                                                                                                                //Natural: ADD 1 TO #STATE-NUM
                    if (condition(pnd_State_Num.equals(2)))                                                                                                               //Natural: IF #STATE-NUM = 2
                    {
                        pnd_State_Num.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #STATE-NUM
                        //*  FO 8/99
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-STATE-CODE
                        sub_Translate_State_Code();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaTwrl6345.getTax_Form_Data_R_Q10b().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new  //Natural: MOVE EDITED FORM.TIRF-STATE-TAX-WTHLD ( #WS-IX ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) TO R-Q10B ( 1 )
                            ReportEditMask("-ZZZ,ZZZ,ZZ9.99"));
                        //*  -------
                        ldaTwrl6345.getTax_Form_Data_R_Q11b().getValue(1).setValue(pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id);                                                 //Natural: MOVE #WS-STATE-TAX-ID TO R-Q11B ( 1 )
                        //*  -------
                        ldaTwrl6345.getTax_Form_Data_R_Q12b().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new  //Natural: MOVE EDITED FORM.TIRF-STATE-DISTR ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q12B ( 1 )
                            ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                        //*  << SAIK3 - LOCAL TAX WITHHOLDING AMOUNT FIX FOR MULTIPLE STATES CODES
                        //*  SAIK3
                        if (condition(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix.getDec().subtract(1)).greater(getZero())))             //Natural: IF FORM.TIRF-LOC-TAX-WTHLD ( #WS-IX - 1 ) > 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaTwrl6345.getTax_Form_Data_R_Q14().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix));          //Natural: MOVE FORM.TIRF-LOC-CODE ( #WS-IX ) TO R-Q14 ( 1 )
                            if (condition(ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals("3E")))                                         //Natural: IF FORM.TIRF-LOC-CODE ( #WS-IX ) = '3E'
                            {
                                ldaTwrl6345.getTax_Form_Data_R_Q14().getValue(1).setValue("NYC");                                                                         //Natural: MOVE 'NYC' TO R-Q14 ( 1 )
                            }                                                                                                                                             //Natural: END-IF
                            ldaTwrl6345.getTax_Form_Data_R_Q13().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new  //Natural: MOVE EDITED FORM.TIRF-LOC-TAX-WTHLD ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q13 ( 1 )
                                ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                            //*  FO 7/00
                            if (condition((ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(getZero()) && ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(" ")))) //Natural: IF ( FORM.TIRF-LOC-TAX-WTHLD ( #WS-IX ) = 0 AND FORM.TIRF-LOC-CODE ( #WS-IX ) = ' ' )
                            {
                                ldaTwrl6345.getTax_Form_Data_R_Q13().getValue(1).reset();                                                                                 //Natural: RESET R-Q13 ( 1 )
                            }                                                                                                                                             //Natural: END-IF
                            ldaTwrl6345.getTax_Form_Data_R_Q15().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),new //Natural: MOVE EDITED FORM.TIRF-LOC-DISTR ( #WS-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO R-Q15 ( 1 )
                                ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                            //*  FO 7/00
                            if (condition((ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(getZero()) && ldaTwrl9710.getForm_Tirf_Loc_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).equals(" ")))) //Natural: IF ( FORM.TIRF-LOC-DISTR ( #WS-IX ) = 0 AND FORM.TIRF-LOC-CODE ( #WS-IX ) = ' ' )
                            {
                                ldaTwrl6345.getTax_Form_Data_R_Q15().getValue(1).reset();                                                                                 //Natural: RESET R-Q15 ( 1 )
                            }                                                                                                                                             //Natural: END-IF
                            //*  SAIK3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Work_Area_Pnd_Ws_Ix.equals(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp())))                                                      //Natural: IF #WS-IX = C*TIRF-1099-R-STATE-GRP
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST
            sub_Call_Post();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_1099_R_Print.reset();                                                                                                                                     //Natural: RESET #1099-R-PRINT R-Q7A ( 1 ) R-INDICATORS ( 1 ) R-PERCENT-AMOUNTS ( 1 ) R-DOLLAR-AMOUNTS ( 1 )
            ldaTwrl6345.getTax_Form_Data_R_Q7a().getValue(1).reset();
            ldaTwrl6345.getTax_Form_Data_R_Indicators().getValue(1).reset();
            ldaTwrl6345.getTax_Form_Data_R_Percent_Amounts().getValue(1).reset();
            ldaTwrl6345.getTax_Form_Data_R_Dollar_Amounts().getValue(1).reset();
            if (condition(pnd_Ws_Isn.greater(getZero())))                                                                                                                 //Natural: IF #WS-ISN > 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Form_1099i() throws Exception                                                                                                                        //Natural: FORM-1099I
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==========================
        ldaTwrl6345.getTax_Form_Data_F1099i_Data_Array().getValue("*").reset();                                                                                           //Natural: RESET F1099I-DATA-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_1099_Int());                                                          //Natural: ASSIGN FORM-TYPE := #FORM-1099-INT
        ldaTwrl6345.getTax_Form_Data_I_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN I-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_I_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN I-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN I-PAY ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN I-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN I-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,4).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Phone());                                                             //Natural: ASSIGN I-PAY ( 1,4 ) := #TWRACOMP.#PHONE
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN I-PAY ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_I_Pay().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN I-PAY ( 1,6 ) := ' '
        ldaTwrl6345.getTax_Form_Data_I_Payid().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO I-PAYID ( 1 )
        //*  ------------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                          //Natural: IF FORM.TIRF-TAX-ID-TYPE = '4' OR = '5'
        {
            ldaTwrl6345.getTax_Form_Data_I_Recid().getValue(1).reset();                                                                                                   //Natural: RESET I-RECID ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_I_Recid().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                               //Natural: MOVE TWRATIN.#O-TIN TO I-RECID ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                         //Natural: ASSIGN I-REC ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN I-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN I-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN I-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN I-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_I_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN I-REC ( 1,6 ) := TIRF-ADDR-LN5
        ldaTwrl6345.getTax_Form_Data_I_Anum().getValue(1).setValue("IANUMXXXXXXXXX");                                                                                     //Natural: ASSIGN I-ANUM ( 1 ) := 'IANUMXXXXXXXXX'
        ldaTwrl6345.getTax_Form_Data_I_Anum().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X"));                        //Natural: MOVE EDITED TIRF-CONTRACT-NBR ( EM = XXXXXXX-X ) TO I-ANUM ( 1 )
        ldaTwrl6345.getTax_Form_Data_I_Anum().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_I_Anum().getValue(1),     //Natural: COMPRESS I-ANUM ( 1 ) '-' TIRF-PAYEE-CDE INTO I-ANUM ( 1 ) LEAVING NO
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        ldaTwrl6345.getTax_Form_Data_I_Q1().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Int_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));    //Natural: MOVE EDITED TIRF-INT-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO I-Q1 ( #IX )
        ldaTwrl6345.getTax_Form_Data_I_Q4().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED TIRF-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO I-Q4 ( #IX )
    }
    private void sub_Form_5498() throws Exception                                                                                                                         //Natural: FORM-5498
    {
        if (BLNatReinput.isReinput()) return;

        //*  =========================
        ldaTwrl6345.getTax_Form_Data_F5498_Form_Array().getValue("*").reset();                                                                                            //Natural: RESET F5498-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_5498_F());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-5498-F
        ldaTwrl6345.getTax_Form_Data_F_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN F-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_F_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN F-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN F-TRU ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN F-TRU ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN F-TRU ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,4).setValue(" ");                                                                                                 //Natural: ASSIGN F-TRU ( 1,4 ) := ' '
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN F-TRU ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_F_Tru().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN F-TRU ( 1,6 ) := ' '
        ldaTwrl6345.getTax_Form_Data_F_Truid().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO F-TRUID ( 1 )
        //*  ----------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                          //Natural: IF FORM.TIRF-TAX-ID-TYPE = '4' OR = '5'
        {
            ldaTwrl6345.getTax_Form_Data_F_Pssn().getValue(1).reset();                                                                                                    //Natural: RESET F-PSSN ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Pssn().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                //Natural: MOVE TWRATIN.#O-TIN TO F-PSSN ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                         //Natural: ASSIGN F-PAY ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN F-PAY ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN F-PAY ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN F-PAY ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN F-PAY ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_F_Pay().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN F-PAY ( 1,6 ) := TIRF-ADDR-LN5
        ldaTwrl6345.getTax_Form_Data_F_Anum().getValue(1).setValue("FANUM-XXXXX");                                                                                        //Natural: ASSIGN F-ANUM ( 1 ) := 'FANUM-XXXXX'
        ldaTwrl6345.getTax_Form_Data_F_Anum().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X"));                        //Natural: MOVE EDITED TIRF-CONTRACT-NBR ( EM = XXXXXXX-X ) TO F-ANUM ( 1 )
        ldaTwrl6345.getTax_Form_Data_F_Anum().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_F_Anum().getValue(1),     //Natural: COMPRESS F-ANUM ( 1 ) '-' TIRF-PAYEE-CDE INTO F-ANUM ( 1 ) LEAVING NO
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        if (condition(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib().notEquals(getZero())))                                                                                  //Natural: IF FORM.TIRF-TRAD-IRA-CONTRIB NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q1().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));             //Natural: MOVE EDITED FORM.TIRF-TRAD-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q1 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q1().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q1 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover().notEquals(getZero())))                                                                                 //Natural: IF FORM.TIRF-TRAD-IRA-ROLLOVER NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q2().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));            //Natural: MOVE EDITED FORM.TIRF-TRAD-IRA-ROLLOVER ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q2 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q2().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q2 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion().notEquals(getZero())))                                                                               //Natural: IF FORM.TIRF-ROTH-IRA-CONVERSION NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q3().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));          //Natural: MOVE EDITED FORM.TIRF-ROTH-IRA-CONVERSION ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q3 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q3().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q3 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Account_Fmv().notEquals(getZero())))                                                                                       //Natural: IF FORM.TIRF-ACCOUNT-FMV NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q4().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Account_Fmv(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                  //Natural: MOVE EDITED FORM.TIRF-ACCOUNT-FMV ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q4 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q4().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q4 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Sep_Amt().notEquals(getZero())))                                                                                           //Natural: IF FORM.TIRF-SEP-AMT NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q7().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Sep_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                      //Natural: MOVE EDITED FORM.TIRF-SEP-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q7 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q7().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q7 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("01") || ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("03")))                                    //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '01' OR = '03'
        {
            ldaTwrl6345.getTax_Form_Data_F_Q6_Ind().getValue(1,1).setValue("X");                                                                                          //Natural: MOVE 'X' TO F-Q6-IND ( 1,1 )
            ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,1).setValue("X");                                                                                      //Natural: MOVE 'X' TO IRA-TYPE-IND ( 1,1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("02")))                                                                                             //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '02'
        {
            ldaTwrl6345.getTax_Form_Data_F_Q6_Ind().getValue(1,4).setValue("X");                                                                                          //Natural: MOVE 'X' TO F-Q6-IND ( 1,4 )
            ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,4).setValue("X");                                                                                      //Natural: MOVE 'X' TO IRA-TYPE-IND ( 1,4 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("05")))                                                                                             //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '05'
        {
            ldaTwrl6345.getTax_Form_Data_Ira_Type_Ind().getValue(1,7).setValue("X");                                                                                      //Natural: MOVE 'X' TO IRA-TYPE-IND ( 1,7 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_5498_Rechar_Ind().notEquals(" ")))                                                                                         //Natural: IF FORM.TIRF-5498-RECHAR-IND NE ' '
        {
            ldaTwrl6345.getTax_Form_Data_F_Q6_Ind().getValue(1,5).setValue("X");                                                                                          //Natural: MOVE 'X' TO F-Q6-IND ( 1,5 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib().notEquals(getZero())))                                                                                  //Natural: IF FORM.TIRF-ROTH-IRA-CONTRIB NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Q9().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));             //Natural: MOVE EDITED FORM.TIRF-ROTH-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-Q9 ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Q9().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                            //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-Q9 ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt().notEquals(getZero())))                                                                                    //Natural: IF FORM.TIRF-IRA-RECHAR-AMT NE 0
        {
            ldaTwrl6345.getTax_Form_Data_F_Frech().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));            //Natural: MOVE EDITED FORM.TIRF-IRA-RECHAR-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO F-FRECH ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_F_Frech().getValue(1).setValueEdited("0.00",new ReportEditMask("9.99"));                                                         //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO F-FRECH ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Form_1042s() throws Exception                                                                                                                        //Natural: FORM-1042S
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        ldaTwrl6345.getTax_Form_Data_F1042s_Form_Array().getValue("*").reset();                                                                                           //Natural: RESET F1042S-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_1042_S());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-1042-S
        pnd_C4_Rate_P3.reset();                                                                                                                                           //Natural: RESET #C4-RATE-P3
        ldaTwrl6345.getTax_Form_Data_S_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN S-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_S_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN S-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                  //Natural: ASSIGN #WS-TAX-YEARA := FORM.TIRF-TAX-YEAR
        pdaTwra5017.getPnd_Twra5017_Pnd_Tax_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                            //Natural: ASSIGN #TWRA5017.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().setValue(ldaTwrl9710.getForm_Tirf_Country_Code());                                                                 //Natural: ASSIGN #TWRA5017.#COUNTRY-CODE := FORM.TIRF-COUNTRY-CODE
        DbsUtil.callnat(Twrn5017.class , getCurrentProcessState(), pdaTwra5017.getPnd_Twra5017());                                                                        //Natural: CALLNAT 'TWRN5017' USING #TWRA5017
        if (condition(Global.isEscape())) return;
        ldaTwrl6345.getTax_Form_Data_S_Q4().getValue(1).setValue("01");                                                                                                   //Natural: MOVE '01' TO S-Q4 ( 1 )
        //*  ---------
        //*  WILL SEND ALL ZEROES IN TIN                           /* &&& ROXAN
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '5'
        {
            ldaTwrl6345.getTax_Form_Data_S_Recid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("000000000");                                                               //Natural: ASSIGN S-RECID ( #IX ) := '000000000'
            //*    S-RECID       (#IX)                         := '000-00-0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_S_Recid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                         //Natural: ASSIGN S-RECID ( #IX ) := TWRATIN.#O-TIN
            short decideConditionsMet2644 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
            if (condition((ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("1") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("3"))))
            {
                decideConditionsMet2644++;
                ldaTwrl6345.getTax_Form_Data_S_Srtin().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                     //Natural: ASSIGN S-SRTIN ( #IX ) := TWRATIN.#O-TIN
                ldaTwrl6345.getTax_Form_Data_S_Srtinx().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("X");                                                                  //Natural: ASSIGN S-SRTINX ( #IX ) := 'X'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("2"))))
            {
                decideConditionsMet2644++;
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2012")))                                                                                //Natural: IF FORM.TIRF-TAX-YEAR GE '2012'
                {
                    ldaTwrl6345.getTax_Form_Data_S_Srtin().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                 //Natural: ASSIGN S-SRTIN ( #IX ) := TWRATIN.#O-TIN
                    ldaTwrl6345.getTax_Form_Data_S_Sreinx().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("X");                                                              //Natural: ASSIGN S-SREINX ( #IX ) := 'X'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------
        ldaTwrl6345.getTax_Form_Data_S_Anum().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X"));                        //Natural: MOVE EDITED TIRF-CONTRACT-NBR ( EM = XXXXXXX-X ) TO S-ANUM ( 1 )
        ldaTwrl6345.getTax_Form_Data_S_Anum().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_S_Anum().getValue(1),     //Natural: COMPRESS S-ANUM ( 1 ) '-' TIRF-PAYEE-CDE INTO S-ANUM ( 1 ) LEAVING NO
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                         //Natural: ASSIGN S-REC ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN S-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN S-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN S-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN S-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_S_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN S-REC ( 1,6 ) := TIRF-ADDR-LN5
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().lessOrEqual("2007")))                                                                                           //Natural: IF FORM.TIRF-TAX-YEAR LE '2007'
        {
            ldaTwrl6345.getTax_Form_Data_S_Q8().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pdaTwra5017.getPnd_Twra5017_Pnd_Country_Desc());                               //Natural: ASSIGN S-Q8 ( #IX ) := #TWRA5017.#COUNTRY-DESC
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN S-WIT ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN S-WIT ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN S-WIT ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,4).setValue(" ");                                                                                                 //Natural: ASSIGN S-WIT ( 1,4 ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN S-WIT ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Wit().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN S-WIT ( 1,6 ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Witid().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO S-WITID ( 1 )
        ldaTwrl6345.getTax_Form_Data_S_Saein().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO S-SAEIN ( 1 )
        ldaTwrl6345.getTax_Form_Data_S_Saeinx().getValue(1).setValue("X");                                                                                                //Natural: MOVE 'X' TO S-SAEINX ( 1 )
        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("T");                                                                                                        //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'T'
        pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                            //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := #WS-TAX-YEAR
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(ldaTwrl9710.getForm_Tirf_Company_Cde().equals("C")))                                                                                                //Natural: IF FORM.TIRF-COMPANY-CDE = 'C'
        {
            ldaTwrl6345.getTax_Form_Data_S_Pay().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                     //Natural: MOVE #TWRACOMP.#COMP-NAME TO S-PAY ( 1,1 )
            ldaTwrl6345.getTax_Form_Data_S_Pay().getValue(1,2).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO S-PAY ( 1,2 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited("0.00",new ReportEditMask("9.99"));                                        //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO S-QB ( #IX,1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2004")))                                                                                                  //Natural: IF FORM.TIRF-TAX-YEAR LT '2004'
        {
            ldaTwrl6345.getTax_Form_Data_S_Qd().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited("0.00",new ReportEditMask("9.99"));                                    //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO S-QD ( #IX,1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited("0.00",new ReportEditMask("9.99"));                                        //Natural: MOVE EDITED 0.00 ( EM = 9.99 ) TO S-QG ( #IX,1 )
        ldaTwrl6345.getTax_Form_Data_S_Qh().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValue(ldaTwrl9710.getForm_Tirf_Irs_Country_Code());                                    //Natural: MOVE FORM.TIRF-IRS-COUNTRY-CODE TO S-QH ( #IX,1 )
        //*                                            /* &&& - FATCA - ROX
        //*  << VIKRAM1 START
        //*  MOVE EDITED TIRF-DOB(EM=MMDDYYYY)              TO #WS-BDAY
        //*  IF  #WS-BDAY >  ' '
        //*    MOVE MONTH-ARRAY(#BDAY-MM) TO #WS-BDAY-TEXT
        //*    COMPRESS #WS-BDAY-TEXT  '-' #BDAY-DD '-' #BDAY-YYYY INTO
        //*      S-BIRTHDATE(#IX) LEAVING NO
        //*  END-IF
        ldaTwrl6345.getTax_Form_Data_S_Birthdate().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Dob(),new ReportEditMask("YYYYMMDD"));       //Natural: MOVE EDITED TIRF-DOB ( EM = YYYYMMDD ) TO S-BIRTHDATE ( #IX )
        //*  >> VIKRAM1 ENDS
        //*  MOVED HERE FROM FOR LOOP
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("1")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '1'
        {
            ldaTwrl6345.getTax_Form_Data_S_Recid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Tin(),new ReportEditMask("XXX-XX-XXXX"));    //Natural: MOVE EDITED FORM.TIRF-TIN ( EM = XXX-XX-XXXX ) TO S-RECID ( #IX )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------------
        if (condition(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp().equals(getZero())))                                                                            //Natural: IF C*TIRF-1042-S-LINE-GRP = 0
        {
            //*  MASS MIGRATION
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("M")))                                                              //Natural: IF #TWRA0214-MOBIUS-MIGR-IND = 'M'
            {
                pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.setValue(1);                                                                                                              //Natural: ASSIGN #WS-1042-IX := 1
                                                                                                                                                                          //Natural: PERFORM MOBIUS-KEY
                sub_Mobius_Key();
                if (condition(Global.isEscape())) {return;}
                ldaTwrl6345.getTax_Form_Data_Mobius_Index().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                    //Natural: ASSIGN TAX-FORM-DATA.MOBIUS-INDEX := #TWRA0214-MOBIUS-KEY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DUTTAD
        ldaTwrl6345.getTax_Form_Data_S_Fin_Number().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pnd_Ws_Fin);                                                               //Natural: MOVE #WS-FIN TO TAX-FORM-DATA.S-FIN-NUMBER ( #IX )
        //*  << SINHASN2
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2017")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2017'
        {
            ldaTwrl6345.getTax_Form_Data_S_Uin_Number().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pnd_Ws_Uin);                                                           //Natural: MOVE #WS-UIN TO TAX-FORM-DATA.S-UIN-NUMBER ( #IX )
            //*  SAIK
            //*  SAIK
            if (condition(pnd_Ws_Amnd_Num_Pnd_Ws_Amnd_Nbr.equals(getZero())))                                                                                             //Natural: IF #WS-AMND-NBR EQ 0
            {
                ldaTwrl6345.getTax_Form_Data_S_Amndment_Num().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(" ");                                                            //Natural: ASSIGN TAX-FORM-DATA.S-AMNDMENT-NUM ( #IX ) := ' '
                //*  SAIK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SAIK
                //*  SAIK
                ldaTwrl6345.getTax_Form_Data_S_Amndment_Num().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(pnd_Ws_Amnd_Num_Pnd_Ws_Amnd_Nbr,new ReportEditMask("99")); //Natural: MOVE EDITED #WS-AMND-NBR ( EM = 99 ) TO TAX-FORM-DATA.S-AMNDMENT-NUM ( #IX )
                //*  SAIK
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl6345.getTax_Form_Data_S_Lob_Code_13j().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Lob());                                     //Natural: ASSIGN S-LOB-CODE-13J ( #IX ) := TIRF-LOB
            //*  << SINHASN2
            //*  SINHASN
            //*  SINHASN
            //*  SINHASN
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_S_Lob_Code_13j().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(" ");                                                                    //Natural: ASSIGN S-LOB-CODE-13J ( #IX ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Payer_3_Status_Code_16d().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(" ");                                                         //Natural: ASSIGN S-PAYER-3-STATUS-CODE-16D ( #IX ) := ' '
        ldaTwrl6345.getTax_Form_Data_S_Payer_4_Status_Code_16e().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(" ");                                                         //Natural: ASSIGN S-PAYER-4-STATUS-CODE-16E ( #IX ) := ' '
        //*  &&& - FATCA
        if (condition(ldaTwrl9710.getForm_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                              //Natural: IF SUBSTRING ( FORM.TIRF-FUTURE-USE,20,1 ) = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM FATCA-1042S
            sub_Fatca_1042s();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   S-C3-STATUS-CODE (#IX) := '19'                      /* DUTTAD
            //*  DUTTAD
            //*  DUTTAD
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2015")))                                                                                              //Natural: IF FORM.TIRF-TAX-YEAR LT '2015'
            {
                ldaTwrl6345.getTax_Form_Data_S_C3_Status_Code().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("19");                                                         //Natural: ASSIGN S-C3-STATUS-CODE ( #IX ) := '19'
                //*  DUTTAD
                //*  DUTTAD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl6345.getTax_Form_Data_S_C3_Status_Code().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("16");                                                         //Natural: ASSIGN S-C3-STATUS-CODE ( #IX ) := '16'
                //*  DUTTAD
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl6345.getTax_Form_Data_S_C4_Status_Code().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(" ");                                                              //Natural: ASSIGN S-C4-STATUS-CODE ( #IX ) := ' '
            ldaTwrl6345.getTax_Form_Data_S_C3_Box().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("X");                                                                      //Natural: ASSIGN S-C3-BOX ( #IX ) := 'X'
            FOR03:                                                                                                                                                        //Natural: FOR #WS-1042-IX = 1 TO C*TIRF-1042-S-LINE-GRP
            for (pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.setValue(1); condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())); 
                pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.nadd(1))
            {
                pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1.nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-1042-IX1
                ldaTwrl6345.getTax_Form_Data_S_Qa().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValue(ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: MOVE FORM.TIRF-1042-S-INCOME-CODE ( #WS-1042-IX ) TO S-QA ( #IX,#WS-1042-IX1 )
                ldaTwrl6345.getTax_Form_Data_S_Srepaid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED TIRF-1042-S-REFUND-AMT ( #WS-1042-IX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO TAX-FORM-DATA.S-SREPAID ( #IX )
                    ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                           //Natural: ADD FORM.TIRF-GROSS-INCOME ( #WS-1042-IX ) TO #WS-S-GROSS-INCOME
                ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-GROSS-INCOME ( #WS-1042-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QB ( #IX,#WS-1042-IX1 )
                    ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2004")))                                                                                          //Natural: IF FORM.TIRF-TAX-YEAR LT '2004'
                {
                    ldaTwrl6345.getTax_Form_Data_S_Qd().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-GROSS-INCOME ( #WS-1042-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QD ( #IX,#WS-1042-IX1 )
                        ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year.less(2001)))                                                                                               //Natural: IF #WS-TAX-YEAR < 2001
                {
                    ldaTwrl6345.getTax_Form_Data_S_Qe().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValue(ldaTwrl9710.getForm_Tirf_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: MOVE FORM.TIRF-TAX-RATE ( #WS-1042-IX ) TO S-QE ( #IX,#WS-1042-IX1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl6345.getTax_Form_Data_S_Qe().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValueEdited(ldaTwrl9710.getForm_Tirf_1042_S_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-1042-S-TAX-RATE ( #WS-1042-IX ) ( EM = 99.99 ) TO S-QE ( #IX,#WS-1042-IX1 )
                        ReportEditMask("99.99"));
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_S_Qf().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValue("0");                                     //Natural: ASSIGN S-QF ( #IX,#WS-1042-IX1 ) := '0'
                setValueToSubstring(ldaTwrl9710.getForm_Tirf_Exempt_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),ldaTwrl6345.getTax_Form_Data_S_Qf().getValue(pnd_Ws_Work_Area_Pnd_Ix, //Natural: MOVE FORM.TIRF-EXEMPT-CODE ( #WS-1042-IX ) TO SUBSTR ( S-QF ( #IX,#WS-1042-IX1 ) ,2,2 )
                    pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1),2,2);
                pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                         //Natural: ADD FORM.TIRF-NRA-TAX-WTHLD ( #WS-1042-IX ) TO #WS-S-NRA-TAX-WTHLD
                ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValueEdited(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-NRA-TAX-WTHLD ( #WS-1042-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QG ( #IX,#WS-1042-IX1 )
                    ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                ldaTwrl6345.getTax_Form_Data_S_Qh().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1).setValue(ldaTwrl9710.getForm_Tirf_Irs_Country_Code()); //Natural: MOVE FORM.TIRF-IRS-COUNTRY-CODE TO S-QH ( #IX,#WS-1042-IX1 )
                ldaTwrl6345.getTax_Form_Data_S_Q4().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("01", MoveOption.LeftJustified);                                           //Natural: MOVE LEFT '01' TO S-Q4 ( #IX )
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("1")))                                                                                        //Natural: IF FORM.TIRF-TAX-ID-TYPE = '1'
                {
                    ldaTwrl6345.getTax_Form_Data_S_Recid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Tin(),new ReportEditMask("XXX-XX-XXXX")); //Natural: MOVE EDITED FORM.TIRF-TIN ( EM = XXX-XX-XXXX ) TO S-RECID ( #IX )
                }                                                                                                                                                         //Natural: END-IF
                //*  MASS MIGRATION
                if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("M")))                                                          //Natural: IF #TWRA0214-MOBIUS-MIGR-IND = 'M'
                {
                                                                                                                                                                          //Natural: PERFORM MOBIUS-KEY
                    sub_Mobius_Key();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaTwrl6345.getTax_Form_Data_Mobius_Index().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                //Natural: ASSIGN TAX-FORM-DATA.MOBIUS-INDEX := #TWRA0214-MOBIUS-KEY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1.equals(1)))                                                                                                //Natural: IF #WS-1042-IX1 EQ 1
                {
                    ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new                     //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QB ( #IX,3 )
                        ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2004")))                                                                                      //Natural: IF FORM.TIRF-TAX-YEAR LT '2004'
                    {
                        ldaTwrl6345.getTax_Form_Data_S_Qd().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new                 //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QD ( #IX,3 )
                            ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld,new                    //Natural: MOVE EDITED #WS-S-NRA-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QG ( #IX,3 )
                        ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                    if (condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.notEquals(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())))                                       //Natural: IF #WS-1042-IX NE C*TIRF-1042-S-LINE-GRP
                    {
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                        sub_Call_Post();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income.reset();                                                                                                   //Natural: RESET #WS-S-GROSS-INCOME #WS-S-NRA-TAX-WTHLD
                        pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld.reset();
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1.setValue(0);                                                                                                         //Natural: MOVE 0 TO #WS-1042-IX1
                    pnd_Ws_Work_Area_Pnd_Ix.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #IX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------------
        ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(1,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                    //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QB ( 1,3 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2004")))                                                                                                  //Natural: IF FORM.TIRF-TAX-YEAR LT '2004'
        {
            ldaTwrl6345.getTax_Form_Data_S_Qd().getValue(1,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QD ( 1,3 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(1,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                   //Natural: MOVE EDITED #WS-S-NRA-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QG ( 1,3 )
    }
    private void sub_Form_480_7c() throws Exception                                                                                                                       //Natural: FORM-480-7C
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaTwrl6345.getTax_Form_Data_F4807c_Form_Array().getValue("*").reset();                                                                                           //Natural: RESET F4807C-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_4807_C());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-4807-C
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_P_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN P-CORR ( 1 ) := 'X'
            ldaTwrl6345.getTax_Form_Data_P_Amend_Date().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Create_Ts(),new ReportEditMask("DD/MM/YY"));                  //Natural: MOVE EDITED FORM.TIRF-CREATE-TS ( EM = DD/MM/YY ) TO P-AMEND-DATE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                  //Natural: ASSIGN #WS-TAX-YEARA := FORM.TIRF-TAX-YEAR
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                            //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                        //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(5);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 5
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        ldaTwrl6345.getTax_Form_Data_P_Payid().getValue(1).setValueEdited(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOM2.#FED-ID ( EM = XX-XXXXXXXX ) TO P-PAYID ( 1 )
        ldaTwrl6345.getTax_Form_Data_P_Pay().getValue(1,1).setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name());                                                         //Natural: ASSIGN P-PAY ( 1,1 ) := #TWRACOM2.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_P_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN P-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_P_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN P-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '5'
        {
            ldaTwrl6345.getTax_Form_Data_P_Recssn().getValue(1).reset();                                                                                                  //Natural: RESET P-RECSSN ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_P_Recssn().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                              //Natural: ASSIGN P-RECSSN ( 1 ) := TWRATIN.#O-TIN
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,1).setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO P-REC ( 1,1 )
            ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN P-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN P-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN P-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN P-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_P_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN P-REC ( 1,6 ) := TIRF-ADDR-LN5
        ldaTwrl6345.getTax_Form_Data_P_Plan_Name().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Plan_Name());                                                            //Natural: ASSIGN P-PLAN-NAME ( 1 ) := FORM.TIRF-PLAN-NAME
        ldaTwrl6345.getTax_Form_Data_P_Plan_Sponsor().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Plan_Spnsr_Name());                                                   //Natural: ASSIGN P-PLAN-SPONSOR ( 1 ) := FORM.TIRF-PLAN-SPNSR-NAME
        if (condition((ldaTwrl6345.getTax_Form_Data_P_Plan_Name().getValue(1).equals(" ") && ldaTwrl6345.getTax_Form_Data_P_Plan_Sponsor().getValue(1).equals(" "))))     //Natural: IF ( P-PLAN-NAME ( 1 ) = ' ' AND P-PLAN-SPONSOR ( 1 ) = ' ' )
        {
            ldaTwrl6345.getTax_Form_Data_P_Ein().getValue(1).setValue(" ");                                                                                               //Natural: ASSIGN P-EIN ( 1 ) := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_P_Ein().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Ein());                                                                    //Natural: ASSIGN P-EIN ( 1 ) := FORM.TIRF-EIN
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> SINHASN1
        if (condition(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll().notEquals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll().notEquals(new          //Natural: IF FORM.TIRF-PR-IVC-AMT-ROLL NE 0.00 OR FORM.TIRF-PR-GROSS-AMT-ROLL NE 0.00
            DbsDecimal("0.00"))))
        {
            ldaTwrl6345.getTax_Form_Data_P_Roll_Cont().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));       //Natural: MOVE EDITED FORM.TIRF-PR-IVC-AMT-ROLL ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-ROLL-CONT ( 1 )
            ldaTwrl6345.getTax_Form_Data_P_Roll_Dist().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));     //Natural: MOVE EDITED FORM.TIRF-PR-GROSS-AMT-ROLL ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-ROLL-DIST ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl9710.getForm_Tirf_Gross_Amt().nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO FORM.TIRF-GROSS-AMT
        ldaTwrl6345.getTax_Form_Data_P_Adist().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-ADIST ( 1 )
        ldaTwrl6345.getTax_Form_Data_P_Taxa().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Taxable_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                    //Natural: MOVE EDITED FORM.TIRF-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-TAXA ( 1 )
        ldaTwrl6345.getTax_Form_Data_P_Dfamt().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Ivc_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED FORM.TIRF-IVC-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-DFAMT ( 1 )
        //*  480.7C DISASTER   >>>>>>
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2020")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2020'
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("N")))                                                                                       //Natural: IF FORM.TIRF-DISTRIBUTION-CDE = 'N'
            {
                ldaTwrl6345.getTax_Form_Data_P_Adist().getValue("*").reset();                                                                                             //Natural: RESET P-ADIST ( * ) #TAXA-DIS P-DFAMT ( * ) #EXMPT-DIS P-TAXA ( * ) #TOTAL-DIS
                pnd_Taxa_Dis.reset();
                ldaTwrl6345.getTax_Form_Data_P_Dfamt().getValue("*").reset();
                pnd_Exmpt_Dis.reset();
                ldaTwrl6345.getTax_Form_Data_P_Taxa().getValue("*").reset();
                pnd_Total_Dis.reset();
                //*  MAXIMUM EXEMPT $10000
                if (condition(ldaTwrl9710.getForm_Tirf_Taxable_Amt().greaterOrEqual(pnd_Maxm_Exempt_Amt)))                                                                //Natural: IF FORM.TIRF-TAXABLE-AMT GE #MAXM-EXEMPT-AMT
                {
                    ldaTwrl6345.getTax_Form_Data_P_Exmpt_Dis().getValue(1).setValueEdited(pnd_Maxm_Exempt_Amt,new ReportEditMask("-,ZZ,ZZ9.99"));                         //Natural: MOVE EDITED #MAXM-EXEMPT-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-EXMPT-DIS ( 1 )
                    ldaTwrl6345.getTax_Form_Data_P_Exmpt_Dis().getValue(1).setValue("     10,000.00 ");                                                                   //Natural: MOVE '     10,000.00 ' TO P-EXMPT-DIS ( 1 )
                    pnd_Taxa_Dis.compute(new ComputeParameters(false, pnd_Taxa_Dis), ldaTwrl9710.getForm_Tirf_Taxable_Amt().subtract(pnd_Maxm_Exempt_Amt));               //Natural: ASSIGN #TAXA-DIS := FORM.TIRF-TAXABLE-AMT - #MAXM-EXEMPT-AMT
                    ldaTwrl6345.getTax_Form_Data_P_Taxa_Dis().getValue(1).setValueEdited(pnd_Taxa_Dis,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                              //Natural: MOVE EDITED #TAXA-DIS ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-TAXA-DIS ( 1 )
                    pnd_Total_Dis.compute(new ComputeParameters(false, pnd_Total_Dis), pnd_Total_Dis.add(pnd_Maxm_Exempt_Amt).add(pnd_Taxa_Dis).add(ldaTwrl9710.getForm_Tirf_Ivc_Amt())); //Natural: ADD #MAXM-EXEMPT-AMT #TAXA-DIS FORM.TIRF-IVC-AMT TO #TOTAL-DIS
                    ldaTwrl6345.getTax_Form_Data_P_Total_Dis().getValue(1).setValueEdited(pnd_Total_Dis,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                            //Natural: MOVE EDITED #TOTAL-DIS ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-TOTAL-DIS ( 1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Exmpt_Dis.setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                                                       //Natural: MOVE FORM.TIRF-TAXABLE-AMT TO #EXMPT-DIS
                    ldaTwrl6345.getTax_Form_Data_P_Exmpt_Dis().getValue(1).setValueEdited(pnd_Exmpt_Dis,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                            //Natural: MOVE EDITED #EXMPT-DIS ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-EXMPT-DIS ( 1 )
                    pnd_Taxa_Dis.setValue(0);                                                                                                                             //Natural: ASSIGN #TAXA-DIS := 0
                    ldaTwrl6345.getTax_Form_Data_P_Taxa_Dis().getValue(1).setValue("          0.00 ");                                                                    //Natural: ASSIGN P-TAXA-DIS ( 1 ) := '          0.00 '
                    pnd_Total_Dis.compute(new ComputeParameters(false, pnd_Total_Dis), pnd_Total_Dis.add(pnd_Exmpt_Dis).add(pnd_Taxa_Dis).add(ldaTwrl9710.getForm_Tirf_Ivc_Amt())); //Natural: ADD #EXMPT-DIS #TAXA-DIS FORM.TIRF-IVC-AMT TO #TOTAL-DIS
                    ldaTwrl6345.getTax_Form_Data_P_Total_Dis().getValue(1).setValueEdited(pnd_Total_Dis,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                            //Natural: MOVE EDITED #TOTAL-DIS ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-TOTAL-DIS ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_P_Aftax_Dis().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Ivc_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));           //Natural: MOVE EDITED FORM.TIRF-IVC-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-AFTAX-DIS ( 1 )
                ldaTwrl6345.getTax_Form_Data_P_Fedtx_Dis().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));     //Natural: MOVE EDITED FORM.TIRF-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-FEDTX-DIS ( 1 )
                                                                                                                                                                          //Natural: PERFORM PENSION-DATE-CHECK
                sub_Pension_Date_Check();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C DISASTER   <<
        //*  << SINHASN1
        //*  BOX 3 VARIABLE
        ldaTwrl6345.getTax_Form_Data_P_Acost().getValue(1).reset();                                                                                                       //Natural: RESET P-ACOST ( 1 )
        //*  IF (FORM.TIRF-DISTR-CATEGORY    = '1' OR = '2') /* MUKHERR
        //*  MUKHERR
        if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("1"))))                                                                                           //Natural: IF ( FORM.TIRF-DISTR-CATEGORY = '1' )
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))                                                                          //Natural: IF FORM.TIRF-IVC-AMT NE 0.00
            {
                ldaTwrl6345.getTax_Form_Data_P_Acost().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Ivc_Amt(),new ReportEditMask("-ZZZ,ZZZ,ZZ9.99"));              //Natural: MOVE EDITED FORM.TIRF-IVC-AMT ( EM = -ZZZ,ZZZ,ZZ9.99 ) TO P-ACOST ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> SINHASN1
        ldaTwrl6345.getTax_Form_Data_P_Anum().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),               //Natural: COMPRESS TIRF-CONTRACT-NBR '-' TIRF-PAYEE-CDE INTO P-ANUM ( 1 ) LEAVING NO SPACE
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        ldaTwrl6345.getTax_Form_Data_P_Dcode().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                         //Natural: ASSIGN P-DCODE ( 1 ) := FORM.TIRF-DISTRIBUTION-CDE
        //* ***************
        //*  480.7C - 2016 CHANGES
        //* **************
        //*  IF FORM.TIRF-DISTR-CATEGORY    = '1'   /* SINHASN1
        //*    P-ANNUITY-IND(1)            := 'X'   /* SINHASN1
        //*    P-PERIODIC-IND(1)             := 'X' /* SINHASN1
        //*  ELSE   /* SINHASN1
        //*    P-LUMP-IND (1)              := 'X' /* SINHASN1
        //*  END-IF                               /* SINHASN1
        //*  << SINHASN1
        pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                                                   //Natural: RESET #S-TWRPYMNT-FORM-SD
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                        //Natural: MOVE #WS-TAX-YEAR TO #S-TWRPYMNT-TAX-YEAR
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                        //Natural: MOVE FORM.TIRF-TIN TO #S-TWRPYMNT-TAX-ID-NBR
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                             //Natural: MOVE FORM.TIRF-CONTRACT-NBR TO #S-TWRPYMNT-CONTRACT-NBR
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                   //Natural: MOVE FORM.TIRF-PAYEE-CDE TO #S-TWRPYMNT-PAYEE-CDE
        vw_pymnt.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) PYMNT BY TWRPYMNT-FORM-SD STARTING FROM #S-TWRPYMNT-FORM-SD
        (
        "READ02",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") },
        1
        );
        READ02:
        while (condition(vw_pymnt.readNextRow("READ02")))
        {
            if (condition(pymnt_Twrpymnt_Tax_Year.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year) || pymnt_Twrpymnt_Tax_Id_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr)  //Natural: IF PYMNT.TWRPYMNT-TAX-YEAR NE #S-TWRPYMNT-TAX-YEAR OR PYMNT.TWRPYMNT-TAX-ID-NBR NE #S-TWRPYMNT-TAX-ID-NBR OR PYMNT.TWRPYMNT-CONTRACT-NBR NE #S-TWRPYMNT-CONTRACT-NBR OR PYMNT.TWRPYMNT-PAYEE-CDE NE #S-TWRPYMNT-PAYEE-CDE
                || pymnt_Twrpymnt_Contract_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr) || pymnt_Twrpymnt_Payee_Cde.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  ANNUITY
            if (condition(pymnt_Twrpymnt_Payment_Type.getValue(1).equals("A") || pymnt_Twrpymnt_Payment_Type.getValue(1).equals("C") || pymnt_Twrpymnt_Payment_Type.getValue(1).equals("E"))) //Natural: IF TWRPYMNT-PAYMENT-TYPE ( 1 ) = 'A' OR = 'C' OR = 'E'
            {
                ldaTwrl6345.getTax_Form_Data_P_Annty_Ind().getValue(1).setValue("X");                                                                                     //Natural: ASSIGN P-ANNTY-IND ( 1 ) := 'X'
                //*  PRIVATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl6345.getTax_Form_Data_P_Qualpvt_Ind().getValue(1).setValue("X");                                                                                   //Natural: ASSIGN P-QUALPVT-IND ( 1 ) := 'X'
            }                                                                                                                                                             //Natural: END-IF
            //* ****DASDH1 2017 TAX COMPLIANCE CHANGES******************
            //*  IF #WS-TAX-YEAR = 2017                     /*SAIK2
            //* SAIK2
            if (condition(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year.greaterOrEqual(2017)))                                                                                         //Natural: IF #WS-TAX-YEAR GE 2017
            {
                ldaTwrl6345.getTax_Form_Data_P_Qualpvt_Ind().getValue(1).setValue("X");                                                                                   //Natural: ASSIGN P-QUALPVT-IND ( 1 ) := 'X'
                ldaTwrl6345.getTax_Form_Data_P_Annty_Ind().getValue(1).setValue(" ");                                                                                     //Natural: ASSIGN P-ANNTY-IND ( 1 ) := ' '
            }                                                                                                                                                             //Natural: END-IF
            //* ****DASDH1**********************************************
            //*  PERIODIC PAYMENT
            //*  PARTIAL  PAYMENT
            //*  LUMPSUM  PAYMENT
            short decideConditionsMet2952 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-DISTR-CATEGORY;//Natural: VALUE '1'
            if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("1"))))
            {
                decideConditionsMet2952++;
                ldaTwrl6345.getTax_Form_Data_P_Periodic_Ind().getValue(1).setValue("X");                                                                                  //Natural: ASSIGN P-PERIODIC-IND ( 1 ) := 'X'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("2"))))
            {
                decideConditionsMet2952++;
                ldaTwrl6345.getTax_Form_Data_P_Partial_Ind().getValue(1).setValue("X");                                                                                   //Natural: ASSIGN P-PARTIAL-IND ( 1 ) := 'X'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("3"))))
            {
                decideConditionsMet2952++;
                ldaTwrl6345.getTax_Form_Data_P_Lump_Ind().getValue(1).setValue("X");                                                                                      //Natural: ASSIGN P-LUMP-IND ( 1 ) := 'X'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaTwrl6345.getTax_Form_Data_P_Orgcnum().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Org_Cntl_Num());                                                        //Natural: ASSIGN P-ORGCNUM ( 1 ) := FORM.TIRF-PR-ORG-CNTL-NUM
        ldaTwrl6345.getTax_Form_Data_P_Adjresn().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Adj_Reason());                                                          //Natural: ASSIGN P-ADJRESN ( 1 ) := FORM.TIRF-PR-ADJ-REASON
        ldaTwrl6345.getTax_Form_Data_P_Cnum().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Cntl_Num());                                                               //Natural: ASSIGN P-CNUM ( 1 ) := FORM.TIRF-PR-CNTL-NUM
        ldaTwrl6345.getTax_Form_Data_P_Cnum().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Cntl_Num().getSubstring(2,9));                                             //Natural: MOVE SUBSTR ( FORM.TIRF-PR-CNTL-NUM,2,9 ) TO P-CNUM ( 1 )
        //*   EFN CODE WAS LOST IN THE 1099R/INT NAD 5498 DST CHANGES
        //*   BY COMPLILANCE TEAM. RESTORED FEB 02
        //* * MOVE SUBSTRING(FORM.TIRF-FUTURE-USE,1,7) TO      /* VIKRAM2  480.7C
        //*  VIKRAM2  480.7C
        ldaTwrl6345.getTax_Form_Data_P_Efile_Confirm_Nbr().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Future_Use().getSubstring(1,11));                                //Natural: MOVE SUBSTRING ( FORM.TIRF-FUTURE-USE,1,11 ) TO P-EFILE-CONFIRM-NBR ( 1 )
        ldaTwrl6345.getTax_Form_Data_P_Total().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-TOTAL ( 1 )
        pnd_Other.compute(new ComputeParameters(false, pnd_Other), ldaTwrl9710.getForm_Tirf_Gross_Amt().subtract(ldaTwrl9710.getForm_Tirf_Ivc_Amt()));                    //Natural: COMPUTE #OTHER := FORM.TIRF-GROSS-AMT - FORM.TIRF-IVC-AMT
        ldaTwrl6345.getTax_Form_Data_P_Others().getValue(1).setValueEdited(pnd_Other,new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                                               //Natural: MOVE EDITED #OTHER ( EM = -ZZ,ZZZ,ZZ9.99 ) TO P-OTHERS ( 1 )
        //*  MOVE  RIGHT  P-TOTAL(1)  TO P-TOTAL(1)
        //*  MOVE  RIGHT  P-OTHERS(1) TO P-OTHERS(1)
        //* *
    }
    private void sub_Form_480_6a() throws Exception                                                                                                                       //Natural: FORM-480-6A
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================
        ldaTwrl6345.getTax_Form_Data_F4806_Form_Array().getValue("*").reset();                                                                                            //Natural: RESET F4806-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_4806_A());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-4806-A
        ldaTwrl6345.getTax_Form_Data_A_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN A-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_A_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN A-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                  //Natural: ASSIGN #WS-TAX-YEARA := FORM.TIRF-TAX-YEAR
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year);                                                                            //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("S");                                                                                                        //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'S'
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(5);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 5
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        ldaTwrl6345.getTax_Form_Data_A_Payid().getValue(1).setValueEdited(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOM2.#FED-ID ( EM = XX-XXXXXXXX ) TO A-PAYID ( 1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2003")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2003'
        {
            ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,1).setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Agent_A());                                                  //Natural: ASSIGN A-PAY ( 1,1 ) := #TWRACOM2.#COMP-AGENT-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,1).setValue("CREF INDIVIDUAL & INST. SERVICES INC.");                                                         //Natural: ASSIGN A-PAY ( 1,1 ) := 'CREF INDIVIDUAL & INST. SERVICES INC.'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN A-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN A-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,4).setValue(" ");                                                                                                 //Natural: ASSIGN A-PAY ( 1,4 ) := ' '
        ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN A-PAY ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_A_Pay().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN A-PAY ( 1,6 ) := ' '
        //*  ------------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '5'
        {
            ldaTwrl6345.getTax_Form_Data_A_Recssn().getValue(1).reset();                                                                                                  //Natural: RESET A-RECSSN ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_A_Recssn().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                              //Natural: MOVE TWRATIN.#O-TIN TO A-RECSSN ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------
        //*  A-REC    (1,1) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,1).setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO A-REC ( 1,1 )
            ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN A-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN A-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN A-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN A-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_A_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN A-REC ( 1,6 ) := TIRF-ADDR-LN5
        //*  MOVE EDITED 100     (EM=99,999,999.99) TO A-AMOUNTS(1,1)
        //*  MOVE EDITED 111     (EM=99,999,999.99) TO A-AMOUNTS(1,2)
        //*  MOVE EDITED 222     (EM=99,999,999.99) TO A-AMOUNTS(1,3)
        //*  MOVE EDITED 333     (EM=99,999,999.99) TO A-AMOUNTS(1,4)
        //*  MOVE EDITED 444     (EM=99,999,999.99) TO A-AMOUNTS(1,5)
        //*  MOVE EDITED 555     (EM=99,999,999.99) TO A-AMOUNTS(1,6)
        //*  MOVE EDITED 666     (EM=99,999,999.99) TO A-AMOUNTS(1,7)
        //*  MOVE EDITED 777     (EM=99,999,999.99) TO A-AMOUNTS(1,8)
        //*  MOVE EDITED 888     (EM=99,999,999.99) TO A-AMOUNTS(1,9)
        //*  A-Q10 (1) := 'A10-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*                                 TO A-Q1(A14)
        //*                                 TO A-Q2(A14)
        //*                                 TO A-Q3(A14)
        //*                                 TO A-Q4(A14)
        //*                                 TO A-Q5(A14)
        //*                                 TO A-Q6(A14)
        //*                                 TO A-Q7(A14)
        //*                                 TO A-Q8(A14)
        //*                                 TO A-Q9(A14)
        //*                                 TO A-EXPANSION1(A14/1:5)
        //*                                 TO A-AMOUNTS(A14/1:9)
        //*                                 TO A-EXPANSION(A100)
        ldaTwrl6345.getTax_Form_Data_A_Amounts().getValue(1,9).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                 //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO A-AMOUNTS ( 1,9 )
        ldaTwrl6345.getTax_Form_Data_A_Amounts().getValue(1,5).setValueEdited(ldaTwrl9710.getForm_Tirf_Int_Amt(),new ReportEditMask("-Z,ZZZ,ZZ9.99"));                    //Natural: MOVE EDITED FORM.TIRF-INT-AMT ( EM = -Z,ZZZ,ZZ9.99 ) TO A-AMOUNTS ( 1,5 )
        ldaTwrl6345.getTax_Form_Data_A_Q10().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),                //Natural: COMPRESS TIRF-CONTRACT-NBR '-' TIRF-PAYEE-CDE INTO A-Q10 ( 1 ) LEAVING NO
            "-", ldaTwrl9710.getForm_Tirf_Payee_Cde()));
    }
    private void sub_Form_480_6b() throws Exception                                                                                                                       //Natural: FORM-480-6B
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================
        ldaTwrl6345.getTax_Form_Data_F480b_Form_Array().getValue("*").reset();                                                                                            //Natural: RESET F480B-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_4806_B());                                                            //Natural: ASSIGN FORM-TYPE := #FORM-4806-B
        ldaTwrl6345.getTax_Form_Data_B_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN B-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_B_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN B-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_B_Payid().getValue(1).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Fed_Id(),new ReportEditMask("XX-XXXXXXXX"));                    //Natural: MOVE EDITED #TWRACOMP.#FED-ID ( EM = XX-XXXXXXXX ) TO B-PAYID ( 1 )
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN B-PAY ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN B-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN B-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,4).setValue(" ");                                                                                                 //Natural: ASSIGN B-PAY ( 1,4 ) := ' '
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,5).setValue(" ");                                                                                                 //Natural: ASSIGN B-PAY ( 1,5 ) := ' '
        ldaTwrl6345.getTax_Form_Data_B_Pay().getValue(1,6).setValue(" ");                                                                                                 //Natural: ASSIGN B-PAY ( 1,6 ) := ' '
        //*  ---------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '5'
        {
            ldaTwrl6345.getTax_Form_Data_B_Recssn().getValue(1).reset();                                                                                                  //Natural: RESET B-RECSSN ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_B_Recssn().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                              //Natural: MOVE TWRATIN.#O-TIN TO B-RECSSN ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,1).setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                         //Natural: ASSIGN B-REC ( 1,1 ) := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN B-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN B-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN B-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN B-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_B_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN B-REC ( 1,6 ) := TIRF-ADDR-LN5
        //*  B-BBNUM  (1)   := 'BBNUM-BBBBBBBBB'
        //*  B-BCNUM  (1)   := 'BCNUM-BBBBBBBBB'
        //*  MOVE EDITED 111     (EM=99,999,999.99) TO B-AMOUNTS(1,1)
        //*  MOVE EDITED 222     (EM=99,999,999.99) TO B-AMOUNTS(1,2)
        //*  MOVE EDITED 333     (EM=99,999,999.99) TO B-AMOUNTS(1,3)
        //*  MOVE EDITED 444     (EM=99,999,999.99) TO B-AMOUNTS(1,4)
        //*  MOVE EDITED 555     (EM=99,999,999.99) TO B-AMOUNTS(1,5)
        //*  MOVE EDITED 666     (EM=99,999,999.99) TO B-AMOUNTS(1,6)
        //*  MOVE EDITED 777     (EM=99,999,999.99) TO B-AMOUNTS(1,7)
        //*  MOVE EDITED 888     (EM=99,999,999.99) TO B-AMOUNTS(1,8)
        //*  MOVE EDITED 999     (EM=99,999,999.99) TO B-AMOUNTS(1,9)
        ldaTwrl6345.getTax_Form_Data_B_Amounts().getValue(1,1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99"));                 //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO B-AMOUNTS ( 1,1 )
        ldaTwrl6345.getTax_Form_Data_B_Amounts().getValue(1,2).setValueEdited(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(),new ReportEditMask("-Z,ZZZ,ZZ9.99"));              //Natural: MOVE EDITED FORM.TIRF-FED-TAX-WTHLD ( EM = -Z,ZZZ,ZZ9.99 ) TO B-AMOUNTS ( 1,2 )
    }
    private void sub_Form_Nr4() throws Exception                                                                                                                          //Natural: FORM-NR4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ========================
        ldaTwrl6345.getTax_Form_Data_Fnr4_N_Form_Array().getValue("*").reset();                                                                                           //Natural: RESET FNR4-N-FORM-ARRAY ( * )
        ldaTwrl6345.getTax_Form_Data_Form_Type().setValue(ldaTwrl6345.getPnd_Form_Constant_Pnd_Form_Nr4_N());                                                             //Natural: ASSIGN FORM-TYPE := #FORM-NR4-N
        ldaTwrl6345.getTax_Form_Data_N_Corr().getValue(1).setValue(" ");                                                                                                  //Natural: ASSIGN N-CORR ( 1 ) := ' '
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                            //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
        {
            ldaTwrl6345.getTax_Form_Data_N_Corr().getValue(1).setValue("X");                                                                                              //Natural: ASSIGN N-CORR ( 1 ) := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_N_Q12a().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Country_Code());                                        //Natural: MOVE FORM.TIRF-COUNTRY-CODE TO N-Q12A ( #IX )
        ldaTwrl6345.getTax_Form_Data_N_Q12b().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                        //Natural: MOVE FORM.TIRF-CONTRACT-NBR TO N-Q12B ( #IX )
        ldaTwrl6345.getTax_Form_Data_N_Q12c().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(pdaTwracomp.getPnd_Twracomp_Pnd_Nr4_Id(),new ReportEditMask("XXX-XXXXX-X")); //Natural: MOVE EDITED #TWRACOMP.#NR4-ID ( EM = XXX-XXXXX-X ) TO N-Q12C ( #IX )
        //*  ---------------
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '5'
        {
            ldaTwrl6345.getTax_Form_Data_N_Q13().getValue(pnd_Ws_Work_Area_Pnd_Ix).reset();                                                                               //Natural: RESET N-Q13 ( #IX )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_N_Q13().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                           //Natural: MOVE TWRATIN.#O-TIN TO N-Q13 ( #IX )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------
        ldaTwrl6345.getTax_Form_Data_N_Q14().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValue(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code());                                    //Natural: MOVE FORM.TIRF-NR4-INCOME-CODE TO N-Q14 ( #IX,1 )
        ldaTwrl6345.getTax_Form_Data_N_Q15().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValue("USD");                                                                         //Natural: MOVE 'USD' TO N-Q15 ( #IX,1 )
        if (condition(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("02") || ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("03") || ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("39"))) //Natural: IF FORM.TIRF-NR4-INCOME-CODE = '02' OR = '03' OR = '39'
        {
            ldaTwrl6345.getTax_Form_Data_N_Q16().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED FORM.TIRF-GROSS-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO N-Q16 ( #IX,1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl6345.getTax_Form_Data_N_Q16().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited(ldaTwrl9710.getForm_Tirf_Int_Amt(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED FORM.TIRF-INT-AMT ( EM = -ZZ,ZZZ,ZZ9.99 ) TO N-Q16 ( #IX,1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl6345.getTax_Form_Data_N_Q17().getValue(pnd_Ws_Work_Area_Pnd_Ix,1).setValueEdited(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(),new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED FORM.TIRF-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO N-Q17 ( #IX,1 )
        ldaTwrl6345.getTax_Form_Data_N_Pay().getValue(1,1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name());                                                         //Natural: ASSIGN N-PAY ( 1,1 ) := #TWRACOMP.#COMP-NAME
        ldaTwrl6345.getTax_Form_Data_N_Pay().getValue(1,2).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(1));                                               //Natural: ASSIGN N-PAY ( 1,2 ) := #TWRACOMP.#ADDRESS ( 1 )
        ldaTwrl6345.getTax_Form_Data_N_Pay().getValue(1,3).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Address().getValue(2));                                               //Natural: ASSIGN N-PAY ( 1,3 ) := #TWRACOMP.#ADDRESS ( 2 )
        //* * N-REC    (1,1) :=  FORM.TIRF-PARTICIPANT-NAME
        pnd_Fld_Final_Names.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Part_Last_Nme(), ",?", ldaTwrl9710.getForm_Tirf_Part_First_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-LAST-NME ',?' FORM.TIRF-PART-FIRST-NME '?' FORM.TIRF-PART-MDDLE-NME TO #FLD-FINAL-NAMES LEAVING NO SPACE
            "?", ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme()));
        DbsUtil.examine(new ExamineSource(pnd_Fld_Final_Names), new ExamineSearch("?"), new ExamineReplace(" "));                                                         //Natural: EXAMINE #FLD-FINAL-NAMES FOR '?' REPLACE WITH ' '
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,1).setValue(pnd_Fld_Final_Names);                                                                                 //Natural: ASSIGN N-REC ( 1,1 ) := #FLD-FINAL-NAMES
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,2).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                 //Natural: ASSIGN N-REC ( 1,2 ) := TIRF-ADDR-LN1
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,3).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                 //Natural: ASSIGN N-REC ( 1,3 ) := TIRF-ADDR-LN2
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,4).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                 //Natural: ASSIGN N-REC ( 1,4 ) := TIRF-ADDR-LN3
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,5).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                 //Natural: ASSIGN N-REC ( 1,5 ) := TIRF-ADDR-LN4
        ldaTwrl6345.getTax_Form_Data_N_Rec().getValue(1,6).setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln5());                                                                 //Natural: ASSIGN N-REC ( 1,6 ) := TIRF-ADDR-LN5
        ldaTwrl6345.getTax_Form_Data_N_Q11().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("1", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT '1' TO N-Q11 ( #IX )
        //*  MOVE FORM.TIRF-COUNTRY-CODE       TO N-Q12(#IX)
        //*  FORM-YEAR     := '99'
        //*  N-Q10 (1)     := '99'
        //*  N-Q11  (1) := 'N11'
        //*  N-Q12A (1) := '12A'
        //*  N-Q12B (1) := '12BXXX'
        //*  N-Q12C (1) := '12C-XXXXXXX'
        //*  N-Q13  (1) := 'N13-XXXXXXXXX'
        //*  N-REC   (1,1) := 'NREC1-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-REC   (1,2) := 'NREC2-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-REC   (1,3) := 'NREC3-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-REC   (1,4) := 'NREC4-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-REC   (1,5) := 'NREC5-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-REC   (1,6) := 'NREC6-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,1) := 'NPAY1-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,2) := 'NPAY2-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,3) := 'NPAY3-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,4) := 'NPAY4-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,5) := 'NPAY5-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  N-PAY   (1,6) := 'NPAY6-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        //*  COMPRESS 'N1'   'A'            INTO N-Q14(1, 1) LEAVING NO
        //*  COMPRESS 'N1'   '5'            INTO N-Q15(1, 1) LEAVING NO
        //*  COMPRESS '$1'   '6,000,000.00' INTO N-Q16(1, 1) LEAVING NO
        //*  COMPRESS '$1'   '7,000,000.00' INTO N-Q17(1, 1) LEAVING NO
        //*  COMPRESS 'N1'   '8'            INTO N-Q18(1, 1) LEAVING NO
    }
    private void sub_Call_Post() throws Exception                                                                                                                         //Natural: CALL-POST
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRN214D|sub_Call_Post");
        while(true)
        {
            try
            {
                //*  =========================
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"SUBROUTINE CALL-POST");                                                                                    //Natural: WRITE *PROGRAM 'SUBROUTINE CALL-POST'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  << DUTTAD2
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
                {
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                            //Natural: ASSIGN DONT-PULL-1099R-INSTRUCTION := TRUE
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);                                                                            //Natural: ASSIGN DONT-PULL-5498F-INSTRUCTION := TRUE
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);                                                                            //Natural: ASSIGN DONT-PULL-4806A-INSTRUCTION := TRUE
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);                                                                            //Natural: ASSIGN DONT-PULL-1042S-INSTRUCTION := TRUE
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);                                                                            //Natural: ASSIGN DONT-PULL-5498P-INSTRUCTION := TRUE
                    ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);                                                                             //Natural: ASSIGN DONT-PULL-NR4I-INSTRUCTION := TRUE
                    //*  >> DUTTAD2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2016")))                                                                                      //Natural: IF FORM.TIRF-TAX-YEAR LT '2016'
                    {
                        //*  DASRAH
                        //*  DASRAH
                        if (condition(pnd_Link_Let_Type.getValue(5).notEquals(" ") || pnd_Link_Let_Type.getValue(6).notEquals(" ") || pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy().equals("Y"))) //Natural: IF #LINK-LET-TYPE ( 5 ) NE ' ' OR #LINK-LET-TYPE ( 6 ) NE ' ' OR #TWRA0214-HARD-COPY = 'Y'
                        {
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1099r_Instruction().setValue(true);                                                                    //Natural: ASSIGN DONT-PULL-1099R-INSTRUCTION := TRUE
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498f_Instruction().setValue(true);                                                                    //Natural: ASSIGN DONT-PULL-5498F-INSTRUCTION := TRUE
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_4806a_Instruction().setValue(true);                                                                    //Natural: ASSIGN DONT-PULL-4806A-INSTRUCTION := TRUE
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_1042s_Instruction().setValue(true);                                                                    //Natural: ASSIGN DONT-PULL-1042S-INSTRUCTION := TRUE
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_5498p_Instruction().setValue(true);                                                                    //Natural: ASSIGN DONT-PULL-5498P-INSTRUCTION := TRUE
                            ldaTwrl6345.getTax_Form_Data_Dont_Pull_Nr4i_Instruction().setValue(true);                                                                     //Natural: ASSIGN DONT-PULL-NR4I-INSTRUCTION := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  DUTTAD2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DUTTAD2
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Work_Area_Pnd_Ws_Tax_Yeara.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                          //Natural: MOVE FORM.TIRF-TAX-YEAR TO #WS-TAX-YEARA
                if (condition(ldaTwrl6345.getTax_Form_Data_Form_Year().equals(" ")))                                                                                      //Natural: IF TAX-FORM-DATA.FORM-YEAR = ' '
                {
                    ldaTwrl6345.getTax_Form_Data_Form_Year().setValue(pnd_Ws_Work_Area_Pnd_Ws_Form_Yy);                                                                   //Natural: MOVE #WS-FORM-YY TO TAX-FORM-DATA.FORM-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  IF #TWRA0114-MOBIUS-MIGR-IND = 'M'                       /* SINHSN
                //*  TAX FORM DATA, MUST BE LOWER CASE
                pdaPsta6346.getPsta6346_Tax_Data_Array().getValue(1,":",ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Lgth()).setValue(ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Array().getValue(1, //Natural: MOVE TAX-FORM-DATA-ARRAY ( 1:TAX-FORM-DATA-LGTH ) TO PSTA6346.TAX-DATA-ARRAY ( 1:TAX-FORM-DATA-LGTH )
                    ":",ldaTwrl6345.getTax_Form_Data_Tax_Form_Data_Lgth()));
                pdaPsta6346.getPsta6346_Pnd_Csf_Record_Id().setValue("b");                                                                                                //Natural: ASSIGN PSTA6346.#CSF-RECORD-ID := 'b'
                //*  SINHASN
                //*  SINHASN
                //*  SINHASN
                pdaPsta6346.getPsta6346_Pnd_Called_From_Post().reset();                                                                                                   //Natural: RESET PSTA6346.#CALLED-FROM-POST
                ldaTwrl6345.getTax_Form_Data_S_Lob_Code_13j().getValue("*").setValue(" ");                                                                                //Natural: ASSIGN S-LOB-CODE-13J ( * ) := ' '
                ldaTwrl6345.getTax_Form_Data_S_Payer_3_Status_Code_16d().getValue("*").setValue(" ");                                                                     //Natural: ASSIGN S-PAYER-3-STATUS-CODE-16D ( * ) := ' '
                ldaTwrl6345.getTax_Form_Data_S_Payer_4_Status_Code_16e().getValue("*").setValue(" ");                                                                     //Natural: ASSIGN S-PAYER-4-STATUS-CODE-16E ( * ) := ' '
                //*   /* SNEHA CHANGES STARTED - SINHSN - FOR HEADER
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"In Post-Open...");                                                                                         //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Open...'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                 //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
                if (condition(ldaTwrl9710.getForm_Tirf_Pin().greater(getZero())))                                                                                         //Natural: IF TIRF-PIN > 0
                {
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("P");                                                                                //Natural: ASSIGN #UNIV-ID-TYP := 'P'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_Pin().equals(getZero()) || ldaTwrl9710.getForm_Tirf_Pin().equals(new DbsDecimal("999999999999"))))             //Natural: IF TIRF-PIN = 0 OR TIRF-PIN = 999999999999
                    {
                        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().setValue("N");                                                                            //Natural: ASSIGN #UNIV-ID-TYP := 'N'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("4") || ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("5")))                                  //Natural: IF TIRF-TAX-ID-TYPE = '4' OR = '5'
                {
                    pnd_Tirf_Tin.setValue(" ");                                                                                                                           //Natural: ASSIGN #TIRF-TIN := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tirf_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                                //Natural: ASSIGN #TIRF-TIN := TIRF-TIN
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Ssn_Nbr().setValue(pnd_Tirf_Tin_Pnd_Tirf_Tin_N9);                                                                                 //Natural: ASSIGN SSN-NBR := #TIRF-TIN-N9
                pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ());                                                  //Natural: ASSIGN UNIV-ID-TYP := #UNIV-ID-TYP
                if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ().equals("P")))                                                                       //Natural: IF #UNIV-ID-TYP = 'P'
                {
                    //*  MOVE EDITED TIRF-PIN (EM=999999999999) TO  UNIV-ID
                    //*  KISHORE
                    pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                           //Natural: MOVE TIRF-PIN TO UNIV-ID
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                           //Natural: ASSIGN UNIV-ID := TIRF-TIN
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaTwrl9710.getForm_Tirf_Participant_Name());                                                                 //Natural: ASSIGN FULL-NME := TIRF-PARTICIPANT-NAME
                pdaPsta9612.getPsta9612_Last_Nme().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                                                    //Natural: ASSIGN LAST-NME := TIRF-PART-LAST-NME
                pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln1());                                                                    //Natural: ASSIGN ADDRSS-LINE-1 := TIRF-ADDR-LN1
                pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln2());                                                                    //Natural: ASSIGN ADDRSS-LINE-2 := TIRF-ADDR-LN2
                pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln3());                                                                    //Natural: ASSIGN ADDRSS-LINE-3 := TIRF-ADDR-LN3
                pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                    //Natural: ASSIGN ADDRSS-LINE-4 := TIRF-ADDR-LN4
                pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln4());                                                                    //Natural: ASSIGN ADDRSS-LINE-5 := TIRF-ADDR-LN4
                pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(ldaTwrl9710.getForm_Tirf_Addr_Ln6());                                                                    //Natural: ASSIGN ADDRSS-LINE-6 := TIRF-ADDR-LN6
                pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                                        //Natural: ASSIGN ADDRSS-ZP9-NBR := TIRF-ZIP
                //*  SSN-NBR             := VAL(TIRF-TIN)
                if (condition(ldaTwrl9710.getForm_Tirf_Foreign_Addr().notEquals("Y")))                                                                                    //Natural: IF TIRF-FOREIGN-ADDR NE 'Y'
                {
                    pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                               //Natural: MOVE 'U' TO ADDRSS-TYP-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("F");                                                                                               //Natural: MOVE 'F' TO ADDRSS-TYP-CDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaPsta9612.getPsta9612_Addrss_Typ_Cde().equals("F") && ldaTwrl9710.getForm_Tirf_Res_Type().getValue("*").equals("4")))                     //Natural: IF ADDRSS-TYP-CDE EQ 'F' AND TIRF-RES-TYPE ( * ) EQ '4'
                {
                    //*  IF TIRF-RES-TYPE      EQ '4'
                    pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("C");                                                                                               //Natural: MOVE 'C' TO ADDRSS-TYP-CDE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),           //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
                    pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
                if (condition(Global.isEscape())) return;
                //*  CWFPDA-M
                //*  CWFPDAUS
                //*  CWFPDAUN
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                          //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
                {
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                           //Natural: MOVE 99 TO #TWRA0214-RET-CODE
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                     //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("POST OPEN -- Call Failed (PSTN9612)");                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'POST OPEN -- Call Failed (PSTN9612)'
                    if (condition(pnd_Debug.getBoolean()))                                                                                                                //Natural: IF #DEBUG
                    {
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                            //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                            //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_Mobindx().setValue(pnd_Mobindx);                                                                                             //Natural: ASSIGN TAX-FORM-DATA.MOBINDX := #MOBINDX
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"In Post-Write..");                                                                                         //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Write..'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                     //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"Calling PSTN9500");                                                                                        //Natural: WRITE *PROGRAM 'Calling PSTN9500'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                 //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
                DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),            //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY TAX-FORM-DATA
                    pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
                    ldaTwrl6345.getSort_Key(), ldaTwrl6345.getTax_Form_Data());
                if (condition(Global.isEscape())) return;
                //*  END-IF                                 /* SINHSN
                //*  -------------------------
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" "), INPUT_1))                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
                {
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                           //Natural: MOVE 99 TO #TWRA0214-RET-CODE
                    //*  FO 12-01
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                     //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
                    //*  IF ATI, STOP PROCESSING
                    if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ati().getBoolean()))                                                                 //Natural: IF #TWRA0214-ATI
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FORMAT ##MSG
                    DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                            //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
                    if (condition(Global.isEscape())) return;
                    pnd_Error_Temp1.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                  //Natural: ASSIGN #ERROR-TEMP1 := MSG-INFO-SUB.##MSG
                    pnd_Error_Temp2.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().getSubstring(41,39));                                                              //Natural: ASSIGN #ERROR-TEMP2 := SUBSTRING ( MSG-INFO-SUB.##MSG,41,39 )
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"********************************************************",  //Natural: INPUT ( AD = O ) /// 10T '********************************************************' ( CD = YE ) / 10T '* POST DATA :' ( CD = YE ) *PROGRAM ( CD = YE ) / 10T '* TIN. . . .:' FORM.TIRF-TIN / 10T '* TIN TYPE .:' FORM.TIRF-TAX-ID-TYPE / 10T '* CONTRACT .:' FORM.TIRF-CONTRACT-NBR / 10T '* PAYEE. . .:' FORM.TIRF-PAYEE-CDE / 10T '* ISN. . . .:' *ISN ( READ-FORM. ) / 10T '* ERROR FIELD:' ( CD = YE ) *OUT MSG-INFO-SUB.##ERROR-FIELD ( CD = YE ) 8X '*' ( CD = YE ) / 10T '*       MSG:' ( CD = YE ) *OUT #ERROR-TEMP1 ( CD = YE ) ' *' ( CD = YE ) / 10T '*           ' ( CD = YE ) *OUT #ERROR-TEMP2 ( CD = YE ) ' *' ( CD = YE ) / 10T '********************************************************' ( CD = YE ) // 24T '***************************' ( CD = YE ) / 24T '*                         *' ( CD = YE ) / 24T '*    THE SYSTEM CANNOT    *' ( CD = YE ) / 24T '*    CONNECT WITH POST    *' ( CD = YE ) / 24T '* PLEASE TRY AGAIN LATER  *' ( CD = YE ) / 24T '*  PRINT WAS TERMINATED   *' ( CD = YE ) / 24T '*                         *' ( CD = YE ) / 24T '***************************' ( CD = YE )
                        Color.yellow,NEWLINE,new TabSetting(10),"* POST DATA :", Color.yellow,Global.getPROGRAM(),Color.yellow,NEWLINE,new TabSetting(10),"* TIN. . . .:",ldaTwrl9710.getForm_Tirf_Tin(),NEWLINE,new 
                        TabSetting(10),"* TIN TYPE .:",ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),NEWLINE,new TabSetting(10),"* CONTRACT .:",ldaTwrl9710.getForm_Tirf_Contract_Nbr(),NEWLINE,new 
                        TabSetting(10),"* PAYEE. . .:",ldaTwrl9710.getForm_Tirf_Payee_Cde(),NEWLINE,new TabSetting(10),"* ISN. . . .:",ldaTwrl9710.getVw_form().getAstISN("READ_FORM"),NEWLINE,new 
                        TabSetting(10),"* ERROR FIELD:", Color.yellow,pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(), Color.yellow,new ColumnSpacing(8),"*", 
                        Color.yellow,NEWLINE,new TabSetting(10),"*       MSG:", Color.yellow,pnd_Error_Temp1, Color.yellow," *", Color.yellow,NEWLINE,new 
                        TabSetting(10),"*           ", Color.yellow,pnd_Error_Temp2, Color.yellow," *", Color.yellow,NEWLINE,new TabSetting(10),"********************************************************", 
                        Color.yellow,NEWLINE,NEWLINE,new TabSetting(24),"***************************", Color.yellow,NEWLINE,new TabSetting(24),"*                         *", 
                        Color.yellow,NEWLINE,new TabSetting(24),"*    THE SYSTEM CANNOT    *", Color.yellow,NEWLINE,new TabSetting(24),"*    CONNECT WITH POST    *", 
                        Color.yellow,NEWLINE,new TabSetting(24),"* PLEASE TRY AGAIN LATER  *", Color.yellow,NEWLINE,new TabSetting(24),"*  PRINT WAS TERMINATED   *", 
                        Color.yellow,NEWLINE,new TabSetting(24),"*                         *", Color.yellow,NEWLINE,new TabSetting(24),"***************************", 
                        Color.yellow);
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch().notEquals("B")))                                                      //Natural: IF #TWRA0214-ONLINE-BATCH NE 'B'
                    {
                        Global.setFetchProgram(DbsUtil.getBlType("TWRP0010"));                                                                                            //Natural: FETCH 'TWRP0010'
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9500.getPsta9500().reset();                                                                                                                        //Natural: RESET PSTA9500
                //* /* SNEHA CHANGES STARTED - SINHSN
                //*  POST-CLOSE
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, Global.getPROGRAM(),"In Post-Close..");                                                                                         //Natural: WRITE ( 0 ) *PROGRAM 'In Post-Close..'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFTAXRPM");                                                                                                 //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFTAXRPM'
                //*  PIN-NBR             := TIRF-PIN
                //*  UNIV-ID-TYP         := #UNIV-ID-TYP
                DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),           //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
                    pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
                if (condition(Global.isEscape())) return;
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                          //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
                {
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().setValue(99);                                                                           //Natural: MOVE 99 TO #TWRA0214-RET-CODE
                    pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                     //Natural: MOVE MSG-INFO-SUB.##MSG TO #TWRA0214-RET-MSG
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("POST CLOSE -- Call Failed (PSTN9685)");                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'POST CLOSE -- Call Failed (PSTN9685)'
                    if (condition(pnd_Debug.getBoolean()))                                                                                                                //Natural: IF #DEBUG
                    {
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                     //Natural: WRITE *PROGRAM MSG-INFO-SUB.##RETURN-CODE
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                             //Natural: WRITE *PROGRAM MSG-INFO-SUB.##MSG
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1));                                            //Natural: WRITE *PROGRAM ##MSG-DATA ( 1 )
                        if (Global.isEscape()) return;
                        getReports().write(0, Global.getPROGRAM(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2));                                            //Natural: WRITE *PROGRAM ##MSG-DATA ( 2 )
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  /* SNEHA CHANGES ENDED - SINHSN
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Translate_State_Code() throws Exception                                                                                                              //Natural: TRANSLATE-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================
        if (condition(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix).notEquals(pdaTwratbl2.getTwratbl2_Pnd_State_Cde().getSubstring(2,        //Natural: IF FORM.TIRF-STATE-CODE ( #WS-IX ) NE SUBSTR ( TWRATBL2.#STATE-CDE,2,2 )
            2))))
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue(1);                                                                                                           //Natural: ASSIGN TWRATBL2.#FUNCTION := 1
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), ldaTwrl9710.getForm_Tirf_Tax_Year().val()); //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue("0");                                                                                                        //Natural: ASSIGN TWRATBL2.#STATE-CDE := '0'
            setValueToSubstring(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_Ix),pdaTwratbl2.getTwratbl2_Pnd_State_Cde(),2,2);                  //Natural: MOVE FORM.TIRF-STATE-CODE ( #WS-IX ) TO SUBSTR ( TWRATBL2.#STATE-CDE,2,2 )
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            //*  -----------------------
            //*  EINCHG
            short decideConditionsMet3361 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T','A'
            if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("T") || ldaTwrl9710.getForm_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet3361++;
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                       //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TIAA TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("C"))))
            {
                decideConditionsMet3361++;
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Cref());                                                       //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-CREF TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("L"))))
            {
                decideConditionsMet3361++;
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Life());                                                       //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-LIFE TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("S"))))
            {
                decideConditionsMet3361++;
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tcii());                                                       //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TCII TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet3361++;
                //*   JB1011  2 LINES
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                      //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TRUST TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaTwrl9710.getForm_Tirf_Company_Cde().equals("F"))))
            {
                decideConditionsMet3361++;
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Fsb());                                                        //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-FSB TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet3361 > 0))
            {
                if (condition(pnd_Ws_Work_Area_Pnd_Ws_Tax_Year.less(2010)))                                                                                               //Natural: IF #WS-TAX-YEAR < 2010
                {
                    pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(DbsUtil.compress(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(), " ", pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id)); //Natural: COMPRESS TWRATBL2.TIRCNTL-STATE-ALPHA-CODE ' ' #WS-STATE-TAX-ID INTO #WS-STATE-TAX-ID
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),     //Natural: COMPRESS TWRATBL2.TIRCNTL-STATE-ALPHA-CODE #WS-STATE-TAX-ID INTO #WS-STATE-TAX-ID LEAVING NO
                        pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Work_Area_Pnd_Ws_State_Tax_Id.setValue("Unknown");                                                                                                 //Natural: MOVE 'Unknown' TO #WS-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  -----------------------
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mobius_Key() throws Exception                                                                                                                        //Natural: MOBIUS-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  NOT 1042-S
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(3)))                                                                                                 //Natural: IF FORM.TIRF-FORM-TYPE NE 03
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"SUBROUTINE MOBIUS-KEY");                                                                                           //Natural: WRITE *PROGRAM 'SUBROUTINE MOBIUS-KEY'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Pin().setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                              //Natural: ASSIGN #TWRA114F.#TIRF-PIN := FORM.TIRF-PIN
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Tax_Year().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                    //Natural: ASSIGN #TWRA114F.#TIRF-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Form_Type().setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                  //Natural: ASSIGN #TWRA114F.#TIRF-FORM-TYPE := FORM.TIRF-FORM-TYPE
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Company_Cde().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                              //Natural: ASSIGN #TWRA114F.#TIRF-COMPANY-CDE := FORM.TIRF-COMPANY-CDE
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Contract_Nbr().setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                            //Natural: ASSIGN #TWRA114F.#TIRF-CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Payee_Cde().setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                  //Natural: ASSIGN #TWRA114F.#TIRF-PAYEE-CDE := FORM.TIRF-PAYEE-CDE
        pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Key().setValue(ldaTwrl9710.getForm_Tirf_Key());                                                                              //Natural: ASSIGN #TWRA114F.#TIRF-KEY := FORM.TIRF-KEY
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(3)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE EQ 03
        {
            pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_Tax_Rate().setValue(ldaTwrl9710.getForm_Tirf_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                      //Natural: ASSIGN #TWRA114F.#TIRF-TAX-RATE := FORM.TIRF-TAX-RATE ( #WS-1042-IX )
            pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_1042_S_Tax_Rate().setValue(ldaTwrl9710.getForm_Tirf_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));               //Natural: ASSIGN #TWRA114F.#TIRF-1042-S-TAX-RATE := FORM.TIRF-TAX-RATE ( #WS-1042-IX )
            pdaTwra114f.getPnd_Twra114f_Pnd_Tirf_1042_S_Income_Code().setValue(ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));  //Natural: ASSIGN #TWRA114F.#TIRF-1042-S-INCOME-CODE := FORM.TIRF-1042-S-INCOME-CODE ( #WS-1042-IX )
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra114f.getPnd_Twra114f_Pnd_Mobius_Migr_Ind().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind());                                 //Natural: ASSIGN #TWRA114F.#MOBIUS-MIGR-IND := #TWRA0214-MOBIUS-MIGR-IND
        DbsUtil.callnat(Twrn114f.class , getCurrentProcessState(), pdaTwra114f.getPnd_Twra114f());                                                                        //Natural: CALLNAT 'TWRN114F' USING #TWRA114F
        if (condition(Global.isEscape())) return;
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().setValue(pdaTwra114f.getPnd_Twra114f_Pnd_Mobius_Key());                                           //Natural: ASSIGN #TWRA0214-MOBIUS-KEY := #TWRA114F.#MOBIUS-KEY
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                                   //Natural: WRITE *PROGRAM #TWRA0214-MOBIUS-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fatca_1042s() throws Exception                                                                                                                       //Natural: FATCA-1042S
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #WS-1042-IX = 1 TO C*TIRF-1042S-CHPTR4
        for (pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.setValue(1); condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1042s_Chptr4())); 
            pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.nadd(1))
        {
            ldaTwrl6345.getTax_Form_Data_S_C4_Box().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("X");                                                                      //Natural: ASSIGN S-C4-BOX ( #IX ) := 'X'
            ldaTwrl6345.getTax_Form_Data_S_C4_Status_Code().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Chptr4_Status_Cde().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: ASSIGN S-C4-STATUS-CODE ( #IX ) := TIRF-CHPTR4-STATUS-CDE ( #WS-1042-IX )
            ldaTwrl6345.getTax_Form_Data_S_C4_Exempt().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Chptr4_Exempt_Cde().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: ASSIGN S-C4-EXEMPT ( #IX ) := TIRF-CHPTR4-EXEMPT-CDE ( #WS-1042-IX )
            ldaTwrl6345.getTax_Form_Data_S_Qf().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix).setValue(ldaTwrl9710.getForm_Tirf_Exempt_Code().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: ASSIGN S-QF ( #IX,#WS-1042-IX ) := TIRF-EXEMPT-CODE ( #WS-1042-IX )
            ldaTwrl6345.getTax_Form_Data_S_Qa().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix).setValue(ldaTwrl9710.getForm_Tirf_Chptr4_Income_Cde().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix)); //Natural: ASSIGN S-QA ( #IX,#WS-1042-IX ) := TIRF-CHPTR4-INCOME-CDE ( #WS-1042-IX )
            ldaTwrl6345.getTax_Form_Data_S_C4_Giin().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue(ldaTwrl9710.getForm_Tirf_Giin());                                         //Natural: ASSIGN S-C4-GIIN ( #IX ) := TIRF-GIIN
            pnd_C4_Rate_P3.setValue(ldaTwrl9710.getForm_Tirf_Chptr4_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                                                //Natural: MOVE TIRF-CHPTR4-TAX-RATE ( #WS-1042-IX ) TO #C4-RATE-P3
            ldaTwrl6345.getTax_Form_Data_S_C4_Tax_Rate().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(pnd_C4_Rate_P3,new ReportEditMask("99.99"));                    //Natural: MOVE EDITED #C4-RATE-P3 ( EM = 99.99 ) TO TAX-FORM-DATA.S-C4-TAX-RATE ( #IX )
            ldaTwrl6345.getTax_Form_Data_S_Srepaid().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED TIRF-1042-S-REFUND-AMT ( #WS-1042-IX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO TAX-FORM-DATA.S-SREPAID ( #IX )
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
            pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                               //Natural: ADD FORM.TIRF-GROSS-INCOME ( #WS-1042-IX ) TO #WS-S-GROSS-INCOME
            ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-GROSS-INCOME ( #WS-1042-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QB ( #IX,#WS-1042-IX )
                ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
            pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix));                             //Natural: ADD FORM.TIRF-NRA-TAX-WTHLD ( #WS-1042-IX ) TO #WS-S-NRA-TAX-WTHLD
            ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix).setValueEdited(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix),new  //Natural: MOVE EDITED FORM.TIRF-NRA-TAX-WTHLD ( #WS-1042-IX ) ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QG ( #IX,#WS-1042-IX )
                ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
            ldaTwrl6345.getTax_Form_Data_S_Qh().getValue(pnd_Ws_Work_Area_Pnd_Ix,pnd_Ws_Work_Area_Pnd_Ws_1042_Ix).setValue(ldaTwrl9710.getForm_Tirf_Irs_Country_Code());  //Natural: MOVE FORM.TIRF-IRS-COUNTRY-CODE TO S-QH ( #IX,#WS-1042-IX )
            ldaTwrl6345.getTax_Form_Data_S_Q4().getValue(pnd_Ws_Work_Area_Pnd_Ix).setValue("01", MoveOption.LeftJustified);                                               //Natural: MOVE LEFT '01' TO S-Q4 ( #IX )
            //*  MASS MIGRATION
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().equals("M")))                                                              //Natural: IF #TWRA0214-MOBIUS-MIGR-IND = 'M'
            {
                                                                                                                                                                          //Natural: PERFORM MOBIUS-KEY
                sub_Mobius_Key();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                ldaTwrl6345.getTax_Form_Data_Mobius_Index().setValue(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key());                                    //Natural: ASSIGN TAX-FORM-DATA.MOBIUS-INDEX := #TWRA0214-MOBIUS-KEY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.equals(1)))                                                                                                     //Natural: IF #WS-1042-IX EQ 1
            {
                ldaTwrl6345.getTax_Form_Data_S_Qb().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QB ( #IX,3 )
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().less("2004")))                                                                                          //Natural: IF FORM.TIRF-TAX-YEAR LT '2004'
                {
                    ldaTwrl6345.getTax_Form_Data_S_Qd().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income,new                     //Natural: MOVE EDITED #WS-S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QD ( #IX,3 )
                        ReportEditMask("-ZZ,ZZZ,ZZ9.99"));
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl6345.getTax_Form_Data_S_Qg().getValue(pnd_Ws_Work_Area_Pnd_Ix,3).setValueEdited(pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld,new ReportEditMask("-ZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED #WS-S-NRA-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) TO S-QG ( #IX,3 )
                if (condition(pnd_Ws_Work_Area_Pnd_Ws_1042_Ix.notEquals(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())))                                           //Natural: IF #WS-1042-IX NE C*TIRF-1042-S-LINE-GRP
                {
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                    sub_Call_Post();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Ws_Work_Area_Pnd_Ws_S_Gross_Income.reset();                                                                                                       //Natural: RESET #WS-S-GROSS-INCOME #WS-S-NRA-TAX-WTHLD
                    pnd_Ws_Work_Area_Pnd_Ws_S_Nra_Tax_Wthld.reset();
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Work_Area_Pnd_Ws_1042_Ix1.setValue(0);                                                                                                             //Natural: MOVE 0 TO #WS-1042-IX1
                pnd_Ws_Work_Area_Pnd_Ix.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #IX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pension_Date_Check() throws Exception                                                                                                                //Natural: PENSION-DATE-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                                                   //Natural: RESET #S-TWRPYMNT-FORM-SD #PENSN
        pnd_Pensn.reset();
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year_A2.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                 //Natural: ASSIGN #S-TWRPYMNT-TAX-YEAR-A2 := FORM.TIRF-TAX-YEAR
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Id_Nbr2.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                      //Natural: ASSIGN #S-TWRPYMNT-TAX-ID-NBR2 := FORM.TIRF-TIN
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Contract_Nbr2.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                           //Natural: ASSIGN #S-TWRPYMNT-CONTRACT-NBR2 := FORM.TIRF-CONTRACT-NBR
        pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Payee_Cde2.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                 //Natural: ASSIGN #S-TWRPYMNT-PAYEE-CDE2 := FORM.TIRF-PAYEE-CDE
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY WITH TWRPYMNT-FORM-SD = #S-TWRPYMNT-FORM-SD2
        (
        "PV1",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd2, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        PV1:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("PV1")))
        {
            if (condition(pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Year2.notEquals(ldaTwrl9415.getPay_Twrpymnt_Tax_Year()) || pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Tax_Id_Nbr2.notEquals(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr())  //Natural: IF #S-TWRPYMNT-TAX-YEAR2 NE PAY.TWRPYMNT-TAX-YEAR OR #S-TWRPYMNT-TAX-ID-NBR2 NE PAY.TWRPYMNT-TAX-ID-NBR OR #S-TWRPYMNT-CONTRACT-NBR2 NE PAY.TWRPYMNT-CONTRACT-NBR OR #S-TWRPYMNT-PAYEE-CDE2 NE PAY.TWRPYMNT-PAYEE-CDE
                || pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Contract_Nbr2.notEquals(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr()) || pnd_S_Twrpymnt_Form_Sd2_Pnd_S_Twrpymnt_Payee_Cde2.notEquals(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde())))
            {
                if (true) break PV1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( PV1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Residency_Type().equals("3") || ldaTwrl9415.getPay_Twrpymnt_Residency_Type().equals("03")))                         //Natural: IF PAY.TWRPYMNT-RESIDENCY-TYPE EQ '3' OR EQ '03'
            {
                FOR05:                                                                                                                                                    //Natural: FOR #PENSN = 1 TO 24
                for (pnd_Pensn.setValue(1); condition(pnd_Pensn.lessOrEqual(24)); pnd_Pensn.nadd(1))
                {
                    if (condition(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Pensn).equals("A") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Pensn).equals(" "))) //Natural: IF ( PAY.TWRPYMNT-PYMNT-STATUS ( #PENSN ) EQ 'A' OR EQ ' ' )
                    {
                        if (condition(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Pensn).notEquals(" ")))                                                        //Natural: IF PAY.TWRPYMNT-PYMNT-DTE ( #PENSN ) NE ' '
                        {
                            ldaTwrl6345.getTax_Form_Data_P_Pension_Date().getValue(1).setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Pensn));              //Natural: ASSIGN P-PENSION-DATE ( 1 ) := PAY.TWRPYMNT-PYMNT-DTE ( #PENSN )
                            ldaTwrl6345.getTax_Form_Data_P_Pension_Date().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl6345.getTax_Form_Data_P_Pension_Date().getValue(1).getSubstring(5,2),  //Natural: COMPRESS SUBSTR ( P-PENSION-DATE ( 1 ) ,5,2 ) '-' SUBSTR ( P-PENSION-DATE ( 1 ) ,7,2 ) '-' SUBSTR ( P-PENSION-DATE ( 1 ) ,1,4 ) INTO P-PENSION-DATE ( 1 ) LEAVING NO SPACE
                                "-", ldaTwrl6345.getTax_Form_Data_P_Pension_Date().getValue(1).getSubstring(7,2), "-", ldaTwrl6345.getTax_Form_Data_P_Pension_Date().getValue(1).getSubstring(1,
                                4)));
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PV1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PV1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PENSION-DATE-CHECK SUBROUTINE ENDS
    }

    //
}
