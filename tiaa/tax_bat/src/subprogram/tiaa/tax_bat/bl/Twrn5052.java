/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:18:55 AM
**        * FROM NATURAL SUBPROGRAM : Twrn5052
************************************************************
**        * FILE NAME            : Twrn5052.java
**        * CLASS NAME           : Twrn5052
**        * INSTANCE NAME        : Twrn5052
************************************************************
************************************************************************
* PROGRAM  : TWRN5052
* SYSTEM   : TAXWARS
* TITLE    : NAME CONTROL ROUTINE
* FUNCTION : TAKE PARAMETER CONTAINING PARTICIPANT's Last Name and
*            POPULATE "Name Control" FIELD WITH FIRST FOUR BYTES OF
*            LAST NAME (BLANK FILLED IF LESS THAN FOUR BYTES).
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 00/00/0000 - XX      XXXXXXXXX XXXX
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrn5052 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwra5052 pdaTwra5052;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;
    private DbsField pnd_Sep_Name;
    private DbsField pnd_Sep_Num;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaTwra5052 = new PdaTwra5052(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Sep_Name = localVariables.newFieldArrayInRecord("pnd_Sep_Name", "#SEP-NAME", FieldType.STRING, 30, new DbsArrayController(1, 2));
        pnd_Sep_Num = localVariables.newFieldInRecord("pnd_Sep_Num", "#SEP-NUM", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrn5052() throws Exception
    {
        super("Twrn5052");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* **
        if (condition(pdaTwra5052.getPnd_Twra5052_Pnd_Debug_Ind().getBoolean()))                                                                                          //Natural: IF #TWRA5052.#DEBUG-IND
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, new FieldAttributes ("AD=I"),Global.getPROGRAM(),"Start");                                                                              //Natural: WRITE ( AD = I ) *PROGRAM 'Start'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra5052.getPnd_Twra5052_Pnd_Lname().separate(pnd_Sep_Name.getValue(2), null, EnumSet.of(SeparateOption.LeftJustified,SeparateOption.WithAnyDelimiters),       //Natural: SEPARATE #TWRA5052.#LNAME LEFT INTO #SEP-NAME ( 1 ) REMAINDER #SEP-NAME ( 2 ) WITH DELIMITER ' ' GIVING NUMBER IN #SEP-NUM
            " ", pnd_Sep_Name.getValue(1));
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().display(0, pnd_Sep_Name.getValue("*"));                                                                                                          //Natural: DISPLAY #SEP-NAME ( * )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra5052.getPnd_Twra5052_Pnd_Lname_Control().setValue(pnd_Sep_Name.getValue(1));                                                                               //Natural: MOVE #SEP-NAME ( 1 ) TO #LNAME-CONTROL
        pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Code().setValue(true);                                                                                                        //Natural: ASSIGN #RET-CODE := TRUE
        //* **
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, pnd_Sep_Name);
    }
}
