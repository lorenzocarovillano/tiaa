/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:20:58 AM
**        * FROM NATURAL SUBPROGRAM : Twrnfrm2
************************************************************
**        * FILE NAME            : Twrnfrm2.java
**        * CLASS NAME           : Twrnfrm2
**        * INSTANCE NAME        : Twrnfrm2
************************************************************
************************************************************************
* SUBPROGRAM: TWRNFRM2
* FUNCTION..: FORM NAME DECODE
* PROGRAMMER: MICHAEL SUPONITSKY - 01/31/2008
* HISTORY...:
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrnfrm2 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaTwrafrm2 pdaTwrafrm2;
    private LdaTwrl5000 ldaTwrl5000;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl5000 = new LdaTwrl5000();
        registerRecord(ldaTwrl5000);

        // parameters
        parameters = new DbsRecord();
        pdaTwrafrm2 = new PdaTwrafrm2(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl5000.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Twrnfrm2() throws Exception
    {
        super("Twrnfrm2");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Output_Data().reset();                                                                                                            //Natural: RESET #TWRAFRMN.#OUTPUT-DATA
        if (condition(pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Tax_Year().greaterOrEqual(2007)))                                                                                   //Natural: IF #TWRAFRMN.#TAX-YEAR GE 2007
        {
            pnd_Ws_Pnd_I.setValue(2);                                                                                                                                     //Natural: ASSIGN #WS.#I := 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_I.setValue(1);                                                                                                                                     //Natural: ASSIGN #WS.#I := 1
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Output_Data().setValuesByName(ldaTwrl5000.getPnd_Twrl5000_Pnd_Form_Table().getValue(pnd_Ws_Pnd_I));                               //Natural: MOVE BY NAME #TWRL5000.#FORM-TABLE ( #I ) TO #TWRAFRMN.#OUTPUT-DATA
        //* **********************
        //*  S U B R O U T I N E S
        //* ***************************
    }

    //
}
