/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:04:08 PM
**        * FROM NATURAL PROGRAM : Necp4014
************************************************************
**        * FILE NAME            : Necp4014.java
**        * CLASS NAME           : Necp4014
**        * INSTANCE NAME        : Necp4014
************************************************************
************************************************************************
* PROGRAM  : NECP4014 - CLONED FROM NECP4005
* SYSTEM   : NEW EXTERNALIZATION CONTROL SYSTEM
* TITLE    : INTERFACE MODULE FOR MAINTENANCE - BATCH
* CREATED  : MAR 28, 2007
* FUNCTION : RETURNS EXTERNALIZATION RECORDS BASED ON INPUT
*            REQUESTS
* MOD DATE   MOD BY        DESCRIPTION OF CHANGES
* 11/01/02   J. CAMPBELL CREATED MAINTENANCE MODULE FOR THE
*                        NEW EXTERNALIZATION TABLE. THIS MODULE
*                        CAN DO THE FOLLOWING FUNCTIONS BASED ON
*                        THE ACTION-CDE :
*                        I = INSERT NEW RECORD
*                        M = MODIFY EXISTING RECORD
*                        D = DELETE THE EXISTING RECORD
*                        S = SELECT OR RETRIEVE AN EXISTING RECORD
* 06/12/07   F. ENDAYA   INPUT FILE CHANGED TO 6219 CHARS.
* 08/21/09   F. ENDAYA   ADJUSTMENT IN FIELDS JUSTIFICATION. FE0908
* 02/03/10   F. ENDAYA   ADJUST #ACL-FUND-PERCENTAGE TO N5.2 FE0210
* 02/05/14   LEGACY      ADDED SIX NEW FIELDS, ASSIGNED VALUES FOR THE
*                        SIX NEW FIELDS TO NECA4005 DDM AND ADDED WRITE
*                        STATEMENT FOR THE SIX NEW FIELDS.
*                        EXPANDED FIELD SIZE FOR FND-ACT-UNIT-ACCT-NM.
*                        SCAN WORD LEGACY.
************************************************************************

************************************************************ */

package tiaa.ext_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Necp4014 extends BLNatBase
{
    // Data Areas
    private PdaNeca4005 pdaNeca4005;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Input_File;
    private DbsField pnd_Input_File_Pnd_Input_Rec;
    private DbsField pnd_Input_File_Pnd_Input_Fil1;
    private DbsField pnd_Input_File_Pnd_Input_Fil2;
    private DbsField pnd_Input_Count;

    private DbsGroup pnd_Nec_Input;
    private DbsField pnd_Nec_Input_Pnd_Neca4005_Rec;
    private DbsField pnd_Nec_Input_Pnd_Neca4005_Fil1;

    private DbsGroup pnd_Nec_Input__R_Field_1;
    private DbsField pnd_Nec_Input_Pnd_Action_Cde;
    private DbsField pnd_Nec_Input_Pnd_Entity_Cde;
    private DbsField pnd_Nec_Input_Pnd_Acl_Asset_Class_Cde;
    private DbsField pnd_Nec_Input_Pnd_Acl_Asset_Class_Desc;
    private DbsField pnd_Nec_Input_Pnd_Acl_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Acl_Fund_Percentage;
    private DbsField pnd_Nec_Input_Pnd_Acl_Domestic_Holdings;
    private DbsField pnd_Nec_Input_Pnd_Acl_International_Holdings;
    private DbsField pnd_Nec_Input_Pnd_Ann_Annuity_Option_Cde;
    private DbsField pnd_Nec_Input_Pnd_Ann_Annuity_Option_Desc;
    private DbsField pnd_Nec_Input_Pnd_Cal_For_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Business_Day_Ind;
    private DbsField pnd_Nec_Input_Pnd_Cal_Curr_Business_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Next_Business_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Payment_Dte_Aftr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Payment_Dte_Bfr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Interface_Dte_Aftr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Interface_Dte_Bfr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Aftr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Bfr;
    private DbsField pnd_Nec_Input_Pnd_Cal_Cut_Off_Time;
    private DbsField pnd_Nec_Input_Pnd_Cal_Future_Payment_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Future_Acctng_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Early_Closing_Time;
    private DbsField pnd_Nec_Input_Pnd_Cal_Market_Closing;
    private DbsField pnd_Nec_Input_Pnd_Cal_Payment_Dte_Ia_Eft;
    private DbsField pnd_Nec_Input_Pnd_Cal_Override_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Override_Tme;
    private DbsField pnd_Nec_Input_Pnd_Cal_Override_Ind;
    private DbsField pnd_Nec_Input_Pnd_Cal_Override_User_Id;
    private DbsField pnd_Nec_Input_Pnd_Cal_Stock_Exh_Open_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Tiaa_Cref_Open_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Bank_Open_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cal_Ia_Mail_Dte;
    private DbsField pnd_Nec_Input_Pnd_Cat_Category_Class_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cat_Category_Class_Desc;
    private DbsField pnd_Nec_Input_Pnd_Cat_Category_Rptng_Seq;
    private DbsField pnd_Nec_Input_Pnd_Cfn_Company_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cfn_Product_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cfn_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Cfn_Sub_Product_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cmp_Company_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cmp_Company_Nme;
    private DbsField pnd_Nec_Input_Pnd_Cmp_Master_Record_Keeper_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cmp_Company_Carrier_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cty_Country_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cty_Country_Unique_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cty_Iso_Country_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cty_Country_Nme;
    private DbsField pnd_Nec_Input_Pnd_Cty_Treaty;
    private DbsField pnd_Nec_Input_Pnd_Cur_Currency_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cur_Currency_Desc;
    private DbsField pnd_Nec_Input_Pnd_Cur_Country_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cur_Country_Unique_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cur_Cps_Country_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cyc_Cycle_Cde;
    private DbsField pnd_Nec_Input_Pnd_Cyc_Cycle_Desc;
    private DbsField pnd_Nec_Input_Pnd_Dat_Company_Cde;
    private DbsField pnd_Nec_Input_Pnd_Dat_Acct_From;
    private DbsField pnd_Nec_Input_Pnd_Dat_Acct_To;
    private DbsField pnd_Nec_Input_Pnd_Dat_Software_Type;
    private DbsField pnd_Nec_Input_Pnd_Dat_Acct_Type;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Company_Cde;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Trans_Type;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Trans_Cde;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Trans_System;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Software_Type;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Software_Neg_Trans_Cde;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Software_Pos_Trans_Cde;
    private DbsField pnd_Nec_Input_Pnd_Dtt_Product_Line_Cde;
    private DbsField pnd_Nec_Input_Pnd_Ded_Deduction_Cde;
    private DbsField pnd_Nec_Input_Pnd_Ded_Deduction_Nme;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Deduction_Cde;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Stmnt_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Online_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line1_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line2_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Short_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Long_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ded_Cps_Net_Change_Desc;
    private DbsField pnd_Nec_Input_Pnd_Def_Deferral_Cde;
    private DbsField pnd_Nec_Input_Pnd_Def_Deferral_Days;
    private DbsField pnd_Nec_Input_Pnd_Def_Deferral_Desc;
    private DbsField pnd_Nec_Input_Pnd_Des_Destination_Cde;
    private DbsField pnd_Nec_Input_Pnd_Des_Destination_Desc;
    private DbsField pnd_Nec_Input_Pnd_Err_Error_Cde;
    private DbsField pnd_Nec_Input_Pnd_Err_Error_Type;
    private DbsField pnd_Nec_Input_Pnd_Err_Error_Msg;
    private DbsField pnd_Nec_Input_Pnd_Fds_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Fds_Fund_Name_Ref_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fds_Fund_Nme;
    private DbsField pnd_Nec_Input_Pnd_Fds_System_Desc;
    private DbsField pnd_Nec_Input_Pnd_Ffm_Fund_Family_Cde;
    private DbsField pnd_Nec_Input_Pnd_Ffm_Fund_Family_Nme;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Family_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Style;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Size;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Nme;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Inception_Dte;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Cusip;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Category_Class_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Category_Rptng_Seq;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Category_Fund_Seq;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Num_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Reporting_Seq;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Unit_Rate_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Seq;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V1_Num_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V1_Alpha_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Abbr_Factor_Key;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Act_Unit_Acct_Nm;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V2_Alpha_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V2_Rt_1;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V2_Rt_2;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V2_Rt_3;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Ivc_Company_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_V2_Active_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Dividend_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Rate_Basis_Points;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line1;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line2;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line3;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line4;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line5;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line6;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line7;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line8;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line9;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line10;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line11;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line12;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line13;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line14;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line15;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line16;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line17;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Functional_Cmpny;
    private DbsField pnd_Nec_Input_Pnd_Fnd_External_Cusip;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Fin_Dwnld_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Ia_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Enterprise_Fund_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Proprietary_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Investment_Typ_Id;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Share_Class_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Act_Class_Cde;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Act_Invesment_Typ;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Investment_Grouping_Id;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Core_Fund_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Intl_Fund_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fnd_Pay_Ind;
    private DbsField pnd_Nec_Input_Pnd_Fsl_Fund_Style;
    private DbsField pnd_Nec_Input_Pnd_Fsl_Style_Desc;
    private DbsField pnd_Nec_Input_Pnd_Fst_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Fst_Fund_Status;
    private DbsField pnd_Nec_Input_Pnd_Fst_Effective_Dte;
    private DbsField pnd_Nec_Input_Pnd_Fst_Audit_Dte;
    private DbsField pnd_Nec_Input_Pnd_Fst_Effective_Dte_Comp;
    private DbsField pnd_Nec_Input_Pnd_Fst_Open_To_New_Invstr;
    private DbsField pnd_Nec_Input_Pnd_Fst_Open_To_Existing_Invstr;
    private DbsField pnd_Nec_Input_Pnd_Fst_Open_To_Trnsfr;
    private DbsField pnd_Nec_Input_Pnd_Fst_Open_To_Premiums;
    private DbsField pnd_Nec_Input_Pnd_Nec_Fil1a;
    private DbsField pnd_Nec_Input_Pnd_Nec_Fil1b;
    private DbsField pnd_Nec_Input_Pnd_Rte_Product_Cde;
    private DbsField pnd_Nec_Input_Pnd_Rte_Ticker_Symbol;
    private DbsField pnd_Nec_Input_Pnd_Rte_Rate_Cde;
    private DbsField pnd_Nec_Input_Pnd_Rte_Interest_Rate;
    private DbsField pnd_Nec_Input_Pnd_Rte_Effective_Dte;
    private DbsField pnd_Nec_Input_Pnd_Rte_Inception_Dte;
    private DbsField pnd_Nec_Input_Pnd_Rte_Nbr_Rates;
    private DbsField pnd_Nec_Input_Pnd_Rte_Acct_Product_Cde;
    private DbsField pnd_Nec_Input_Pnd_Nec_Fil2;
    private DbsField pnd_Nec_Input_Pnd_Nec_Fil3;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Wk_Datex;

    private DbsGroup pnd_Work_Area__R_Field_2;
    private DbsField pnd_Work_Area_Pnd_Wk_Daten;
    private DbsField pnd_Work_Area_Pnd_Wk_Dated;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4005 = new PdaNeca4005(localVariables);

        // Local Variables
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.STRING, 3);

        pnd_Input_File = localVariables.newGroupInRecord("pnd_Input_File", "#INPUT-FILE");
        pnd_Input_File_Pnd_Input_Rec = pnd_Input_File.newFieldArrayInGroup("pnd_Input_File_Pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 250, new DbsArrayController(1, 
            24));
        pnd_Input_File_Pnd_Input_Fil1 = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Input_Fil1", "#INPUT-FIL1", FieldType.STRING, 133);
        pnd_Input_File_Pnd_Input_Fil2 = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Input_Fil2", "#INPUT-FIL2", FieldType.STRING, 22);
        pnd_Input_Count = localVariables.newFieldInRecord("pnd_Input_Count", "#INPUT-COUNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Nec_Input = localVariables.newGroupInRecord("pnd_Nec_Input", "#NEC-INPUT");
        pnd_Nec_Input_Pnd_Neca4005_Rec = pnd_Nec_Input.newFieldArrayInGroup("pnd_Nec_Input_Pnd_Neca4005_Rec", "#NECA4005-REC", FieldType.STRING, 250, 
            new DbsArrayController(1, 24));
        pnd_Nec_Input_Pnd_Neca4005_Fil1 = pnd_Nec_Input.newFieldInGroup("pnd_Nec_Input_Pnd_Neca4005_Fil1", "#NECA4005-FIL1", FieldType.STRING, 133);

        pnd_Nec_Input__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Input__R_Field_1", "REDEFINE", pnd_Nec_Input);
        pnd_Nec_Input_Pnd_Action_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Action_Cde", "#ACTION-CDE", FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Entity_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Entity_Cde", "#ENTITY-CDE", FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Acl_Asset_Class_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_Asset_Class_Cde", "#ACL-ASSET-CLASS-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Acl_Asset_Class_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_Asset_Class_Desc", "#ACL-ASSET-CLASS-DESC", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Acl_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_Ticker_Symbol", "#ACL-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Acl_Fund_Percentage = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_Fund_Percentage", "#ACL-FUND-PERCENTAGE", 
            FieldType.NUMERIC, 7, 2);
        pnd_Nec_Input_Pnd_Acl_Domestic_Holdings = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_Domestic_Holdings", "#ACL-DOMESTIC-HOLDINGS", 
            FieldType.NUMERIC, 7, 2);
        pnd_Nec_Input_Pnd_Acl_International_Holdings = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Acl_International_Holdings", "#ACL-INTERNATIONAL-HOLDINGS", 
            FieldType.NUMERIC, 7, 2);
        pnd_Nec_Input_Pnd_Ann_Annuity_Option_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ann_Annuity_Option_Cde", "#ANN-ANNUITY-OPTION-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Ann_Annuity_Option_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ann_Annuity_Option_Desc", "#ANN-ANNUITY-OPTION-DESC", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Cal_For_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_For_Dte", "#CAL-FOR-DTE", FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Business_Day_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Business_Day_Ind", "#CAL-BUSINESS-DAY-IND", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Cal_Curr_Business_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Curr_Business_Dte", "#CAL-CURR-BUSINESS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Next_Business_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Next_Business_Dte", "#CAL-NEXT-BUSINESS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Payment_Dte_Aftr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Payment_Dte_Aftr", "#CAL-PAYMENT-DTE-AFTR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Payment_Dte_Bfr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Payment_Dte_Bfr", "#CAL-PAYMENT-DTE-BFR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Interface_Dte_Aftr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Interface_Dte_Aftr", "#CAL-INTERFACE-DTE-AFTR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Interface_Dte_Bfr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Interface_Dte_Bfr", "#CAL-INTERFACE-DTE-BFR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Aftr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Aftr", "#CAL-ACCTNG-DTE-AFTR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Bfr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Acctng_Dte_Bfr", "#CAL-ACCTNG-DTE-BFR", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Cut_Off_Time = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Cut_Off_Time", "#CAL-CUT-OFF-TIME", FieldType.NUMERIC, 
            6);
        pnd_Nec_Input_Pnd_Cal_Future_Payment_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Future_Payment_Dte", "#CAL-FUTURE-PAYMENT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Future_Acctng_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Future_Acctng_Dte", "#CAL-FUTURE-ACCTNG-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Early_Closing_Time = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Early_Closing_Time", "#CAL-EARLY-CLOSING-TIME", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Market_Closing = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Market_Closing", "#CAL-MARKET-CLOSING", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Payment_Dte_Ia_Eft = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Payment_Dte_Ia_Eft", "#CAL-PAYMENT-DTE-IA-EFT", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Override_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Override_Dte", "#CAL-OVERRIDE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Cal_Override_Tme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Override_Tme", "#CAL-OVERRIDE-TME", FieldType.NUMERIC, 
            7);
        pnd_Nec_Input_Pnd_Cal_Override_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Override_Ind", "#CAL-OVERRIDE-IND", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Cal_Override_User_Id = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Override_User_Id", "#CAL-OVERRIDE-USER-ID", 
            FieldType.STRING, 10);
        pnd_Nec_Input_Pnd_Cal_Stock_Exh_Open_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Stock_Exh_Open_Dte", "#CAL-STOCK-EXH-OPEN-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Tiaa_Cref_Open_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Tiaa_Cref_Open_Dte", "#CAL-TIAA-CREF-OPEN-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Cal_Bank_Open_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Bank_Open_Dte", "#CAL-BANK-OPEN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Cal_Ia_Mail_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cal_Ia_Mail_Dte", "#CAL-IA-MAIL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Cat_Category_Class_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cat_Category_Class_Cde", "#CAT-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cat_Category_Class_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cat_Category_Class_Desc", "#CAT-CATEGORY-CLASS-DESC", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Cat_Category_Rptng_Seq = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cat_Category_Rptng_Seq", "#CAT-CATEGORY-RPTNG-SEQ", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Cfn_Company_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cfn_Company_Cde", "#CFN-COMPANY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cfn_Product_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cfn_Product_Cde", "#CFN-PRODUCT-CDE", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Cfn_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cfn_Ticker_Symbol", "#CFN-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Cfn_Sub_Product_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cfn_Sub_Product_Cde", "#CFN-SUB-PRODUCT-CDE", 
            FieldType.STRING, 12);
        pnd_Nec_Input_Pnd_Cmp_Company_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cmp_Company_Cde", "#CMP-COMPANY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cmp_Company_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cmp_Company_Nme", "#CMP-COMPANY-NME", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Cmp_Master_Record_Keeper_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cmp_Master_Record_Keeper_Cde", "#CMP-MASTER-RECORD-KEEPER-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cmp_Company_Carrier_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cmp_Company_Carrier_Cde", "#CMP-COMPANY-CARRIER-CDE", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Cty_Country_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cty_Country_Cde", "#CTY-COUNTRY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cty_Country_Unique_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cty_Country_Unique_Cde", "#CTY-COUNTRY-UNIQUE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cty_Iso_Country_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cty_Iso_Country_Cde", "#CTY-ISO-COUNTRY-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cty_Country_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cty_Country_Nme", "#CTY-COUNTRY-NME", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Cty_Treaty = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cty_Treaty", "#CTY-TREATY", FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Cur_Currency_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cur_Currency_Cde", "#CUR-CURRENCY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cur_Currency_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cur_Currency_Desc", "#CUR-CURRENCY-DESC", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Cur_Country_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cur_Country_Cde", "#CUR-COUNTRY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cur_Country_Unique_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cur_Country_Unique_Cde", "#CUR-COUNTRY-UNIQUE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cur_Cps_Country_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cur_Cps_Country_Cde", "#CUR-CPS-COUNTRY-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Cyc_Cycle_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cyc_Cycle_Cde", "#CYC-CYCLE-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Cyc_Cycle_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Cyc_Cycle_Desc", "#CYC-CYCLE-DESC", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Dat_Company_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dat_Company_Cde", "#DAT-COMPANY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Dat_Acct_From = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dat_Acct_From", "#DAT-ACCT-FROM", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Dat_Acct_To = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dat_Acct_To", "#DAT-ACCT-TO", FieldType.STRING, 10);
        pnd_Nec_Input_Pnd_Dat_Software_Type = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dat_Software_Type", "#DAT-SOFTWARE-TYPE", FieldType.STRING, 
            30);
        pnd_Nec_Input_Pnd_Dat_Acct_Type = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dat_Acct_Type", "#DAT-ACCT-TYPE", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Dtt_Company_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Company_Cde", "#DTT-COMPANY-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Dtt_Trans_Type = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Trans_Type", "#DTT-TRANS-TYPE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Dtt_Trans_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Trans_Cde", "#DTT-TRANS-CDE", FieldType.STRING, 
            12);
        pnd_Nec_Input_Pnd_Dtt_Trans_System = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Trans_System", "#DTT-TRANS-SYSTEM", FieldType.STRING, 
            8);
        pnd_Nec_Input_Pnd_Dtt_Software_Type = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Software_Type", "#DTT-SOFTWARE-TYPE", FieldType.STRING, 
            30);
        pnd_Nec_Input_Pnd_Dtt_Software_Neg_Trans_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Software_Neg_Trans_Cde", "#DTT-SOFTWARE-NEG-TRANS-CDE", 
            FieldType.STRING, 30);
        pnd_Nec_Input_Pnd_Dtt_Software_Pos_Trans_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Software_Pos_Trans_Cde", "#DTT-SOFTWARE-POS-TRANS-CDE", 
            FieldType.STRING, 30);
        pnd_Nec_Input_Pnd_Dtt_Product_Line_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Dtt_Product_Line_Cde", "#DTT-PRODUCT-LINE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Ded_Deduction_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Deduction_Cde", "#DED-DEDUCTION-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Ded_Deduction_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Deduction_Nme", "#DED-DEDUCTION-NME", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Ded_Cps_Deduction_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Deduction_Cde", "#DED-CPS-DEDUCTION-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Ded_Cps_Stmnt_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Stmnt_Desc", "#DED-CPS-STMNT-DESC", 
            FieldType.STRING, 40);
        pnd_Nec_Input_Pnd_Ded_Cps_Online_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Online_Desc", "#DED-CPS-ONLINE-DESC", 
            FieldType.STRING, 30);
        pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line1_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line1_Desc", "#DED-CPS-HEADING-LINE1-DESC", 
            FieldType.STRING, 15);
        pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line2_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Heading_Line2_Desc", "#DED-CPS-HEADING-LINE2-DESC", 
            FieldType.STRING, 15);
        pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Short_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Short_Desc", "#DED-CPS-RPRT-SHORT-DESC", 
            FieldType.STRING, 20);
        pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Long_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Rprt_Long_Desc", "#DED-CPS-RPRT-LONG-DESC", 
            FieldType.STRING, 30);
        pnd_Nec_Input_Pnd_Ded_Cps_Net_Change_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ded_Cps_Net_Change_Desc", "#DED-CPS-NET-CHANGE-DESC", 
            FieldType.STRING, 40);
        pnd_Nec_Input_Pnd_Def_Deferral_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Def_Deferral_Cde", "#DEF-DEFERRAL-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Def_Deferral_Days = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Def_Deferral_Days", "#DEF-DEFERRAL-DAYS", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Def_Deferral_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Def_Deferral_Desc", "#DEF-DEFERRAL-DESC", FieldType.STRING, 
            200);
        pnd_Nec_Input_Pnd_Des_Destination_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Des_Destination_Cde", "#DES-DESTINATION-CDE", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Des_Destination_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Des_Destination_Desc", "#DES-DESTINATION-DESC", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Err_Error_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Err_Error_Cde", "#ERR-ERROR-CDE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Err_Error_Type = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Err_Error_Type", "#ERR-ERROR-TYPE", FieldType.STRING, 
            2);
        pnd_Nec_Input_Pnd_Err_Error_Msg = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Err_Error_Msg", "#ERR-ERROR-MSG", FieldType.STRING, 
            80);
        pnd_Nec_Input_Pnd_Fds_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fds_Ticker_Symbol", "#FDS-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Fds_Fund_Name_Ref_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fds_Fund_Name_Ref_Cde", "#FDS-FUND-NAME-REF-CDE", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fds_Fund_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fds_Fund_Nme", "#FDS-FUND-NME", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Fds_System_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fds_System_Desc", "#FDS-SYSTEM-DESC", FieldType.STRING, 
            20);
        pnd_Nec_Input_Pnd_Ffm_Fund_Family_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ffm_Fund_Family_Cde", "#FFM-FUND-FAMILY-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Ffm_Fund_Family_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Ffm_Fund_Family_Nme", "#FFM-FUND-FAMILY-NME", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Ticker_Symbol", "#FND-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Fnd_Fund_Family_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Family_Cde", "#FND-FUND-FAMILY-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Fnd_Fund_Style = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Style", "#FND-FUND-STYLE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Fnd_Fund_Size = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Size", "#FND-FUND-SIZE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Fnd_Fund_Nme = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Nme", "#FND-FUND-NME", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Fnd_Inception_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Inception_Dte", "#FND-INCEPTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Fnd_Cusip = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Cusip", "#FND-CUSIP", FieldType.STRING, 10);
        pnd_Nec_Input_Pnd_Fnd_Category_Class_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Category_Class_Cde", "#FND-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Input_Pnd_Fnd_Category_Rptng_Seq = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Category_Rptng_Seq", "#FND-CATEGORY-RPTNG-SEQ", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Fnd_Category_Fund_Seq = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Category_Fund_Seq", "#FND-CATEGORY-FUND-SEQ", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Cde", "#FND-ALPHA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Num_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Num_Fund_Cde", "#FND-NUM-FUND-CDE", FieldType.NUMERIC, 
            5);
        pnd_Nec_Input_Pnd_Fnd_Reporting_Seq = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Reporting_Seq", "#FND-REPORTING-SEQ", FieldType.STRING, 
            5);
        pnd_Nec_Input_Pnd_Fnd_Unit_Rate_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Unit_Rate_Ind", "#FND-UNIT-RATE-IND", FieldType.NUMERIC, 
            3);
        pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Seq = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Seq", "#FND-ALPHA-FUND-SEQ", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_V1_Num_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V1_Num_Fund_Cde", "#FND-V1-NUM-FUND-CDE", 
            FieldType.NUMERIC, 5);
        pnd_Nec_Input_Pnd_Fnd_V1_Alpha_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V1_Alpha_Fund_Cde", "#FND-V1-ALPHA-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Abbr_Factor_Key = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Abbr_Factor_Key", "#FND-ABBR-FACTOR-KEY", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Act_Unit_Acct_Nm = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Act_Unit_Acct_Nm", "#FND-ACT-UNIT-ACCT-NM", 
            FieldType.NUMERIC, 3);
        pnd_Nec_Input_Pnd_Fnd_V2_Alpha_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V2_Alpha_Cde", "#FND-V2-ALPHA-CDE", FieldType.STRING, 
            2);
        pnd_Nec_Input_Pnd_Fnd_V2_Rt_1 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V2_Rt_1", "#FND-V2-RT-1", FieldType.NUMERIC, 2);
        pnd_Nec_Input_Pnd_Fnd_V2_Rt_2 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V2_Rt_2", "#FND-V2-RT-2", FieldType.NUMERIC, 2);
        pnd_Nec_Input_Pnd_Fnd_V2_Rt_3 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V2_Rt_3", "#FND-V2-RT-3", FieldType.NUMERIC, 2);
        pnd_Nec_Input_Pnd_Fnd_Ivc_Company_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Ivc_Company_Cde", "#FND-IVC-COMPANY-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Nec_Input_Pnd_Fnd_V2_Active_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_V2_Active_Ind", "#FND-V2-ACTIVE-IND", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fnd_Dividend_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Dividend_Ind", "#FND-DIVIDEND-IND", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fnd_Rate_Basis_Points = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Rate_Basis_Points", "#FND-RATE-BASIS-POINTS", 
            FieldType.NUMERIC, 3);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line1 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line1", "#FND-FUND-SUMMARY-LINE1", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line2 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line2", "#FND-FUND-SUMMARY-LINE2", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line3 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line3", "#FND-FUND-SUMMARY-LINE3", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line4 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line4", "#FND-FUND-SUMMARY-LINE4", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line5 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line5", "#FND-FUND-SUMMARY-LINE5", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line6 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line6", "#FND-FUND-SUMMARY-LINE6", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line7 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line7", "#FND-FUND-SUMMARY-LINE7", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line8 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line8", "#FND-FUND-SUMMARY-LINE8", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line9 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line9", "#FND-FUND-SUMMARY-LINE9", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line10 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line10", "#FND-FUND-SUMMARY-LINE10", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line11 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line11", "#FND-FUND-SUMMARY-LINE11", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line12 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line12", "#FND-FUND-SUMMARY-LINE12", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line13 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line13", "#FND-FUND-SUMMARY-LINE13", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line14 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line14", "#FND-FUND-SUMMARY-LINE14", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line15 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line15", "#FND-FUND-SUMMARY-LINE15", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line16 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line16", "#FND-FUND-SUMMARY-LINE16", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line17 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line17", "#FND-FUND-SUMMARY-LINE17", 
            FieldType.STRING, 60);
        pnd_Nec_Input_Pnd_Fnd_Functional_Cmpny = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Functional_Cmpny", "#FND-FUNCTIONAL-CMPNY", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_External_Cusip = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_External_Cusip", "#FND-EXTERNAL-CUSIP", 
            FieldType.STRING, 10);
        pnd_Nec_Input_Pnd_Fnd_Fin_Dwnld_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Fin_Dwnld_Ticker_Symbol", "#FND-FIN-DWNLD-TICKER-SYMBOL", 
            FieldType.STRING, 15);
        pnd_Nec_Input_Pnd_Fnd_Ia_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Ia_Fund_Cde", "#FND-IA-FUND-CDE", FieldType.STRING, 
            5);
        pnd_Nec_Input_Pnd_Fnd_Enterprise_Fund_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Enterprise_Fund_Cde", "#FND-ENTERPRISE-FUND-CDE", 
            FieldType.STRING, 5);
        pnd_Nec_Input_Pnd_Fnd_Proprietary_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Proprietary_Ind", "#FND-PROPRIETARY-IND", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Fnd_Investment_Typ_Id = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Investment_Typ_Id", "#FND-INVESTMENT-TYP-ID", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Share_Class_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Share_Class_Ind", "#FND-SHARE-CLASS-IND", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Act_Class_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Act_Class_Cde", "#FND-ACT-CLASS-CDE", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fnd_Act_Invesment_Typ = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Act_Invesment_Typ", "#FND-ACT-INVESMENT-TYP", 
            FieldType.STRING, 2);
        pnd_Nec_Input_Pnd_Fnd_Investment_Grouping_Id = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Investment_Grouping_Id", "#FND-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4);
        pnd_Nec_Input_Pnd_Fnd_Core_Fund_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Core_Fund_Ind", "#FND-CORE-FUND-IND", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fnd_Intl_Fund_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Intl_Fund_Ind", "#FND-INTL-FUND-IND", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fnd_Pay_Ind = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fnd_Pay_Ind", "#FND-PAY-IND", FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Fsl_Fund_Style = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fsl_Fund_Style", "#FSL-FUND-STYLE", FieldType.STRING, 
            3);
        pnd_Nec_Input_Pnd_Fsl_Style_Desc = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fsl_Style_Desc", "#FSL-STYLE-DESC", FieldType.STRING, 
            60);
        pnd_Nec_Input_Pnd_Fst_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Ticker_Symbol", "#FST-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Fst_Fund_Status = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Fund_Status", "#FST-FUND-STATUS", FieldType.STRING, 
            1);
        pnd_Nec_Input_Pnd_Fst_Effective_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Effective_Dte", "#FST-EFFECTIVE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Fst_Audit_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Audit_Dte", "#FST-AUDIT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Fst_Effective_Dte_Comp = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Effective_Dte_Comp", "#FST-EFFECTIVE-DTE-COMP", 
            FieldType.NUMERIC, 8);
        pnd_Nec_Input_Pnd_Fst_Open_To_New_Invstr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Open_To_New_Invstr", "#FST-OPEN-TO-NEW-INVSTR", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Fst_Open_To_Existing_Invstr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Open_To_Existing_Invstr", "#FST-OPEN-TO-EXISTING-INVSTR", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Fst_Open_To_Trnsfr = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Open_To_Trnsfr", "#FST-OPEN-TO-TRNSFR", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Fst_Open_To_Premiums = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Fst_Open_To_Premiums", "#FST-OPEN-TO-PREMIUMS", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Nec_Fil1a = pnd_Nec_Input__R_Field_1.newFieldArrayInGroup("pnd_Nec_Input_Pnd_Nec_Fil1a", "#NEC-FIL1A", FieldType.STRING, 250, 
            new DbsArrayController(1, 3));
        pnd_Nec_Input_Pnd_Nec_Fil1b = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Nec_Fil1b", "#NEC-FIL1B", FieldType.STRING, 189);
        pnd_Nec_Input_Pnd_Rte_Product_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Product_Cde", "#RTE-PRODUCT-CDE", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Rte_Ticker_Symbol = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Ticker_Symbol", "#RTE-TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Nec_Input_Pnd_Rte_Rate_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Rate_Cde", "#RTE-RATE-CDE", FieldType.STRING, 
            8);
        pnd_Nec_Input_Pnd_Rte_Interest_Rate = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Interest_Rate", "#RTE-INTEREST-RATE", FieldType.NUMERIC, 
            5);
        pnd_Nec_Input_Pnd_Rte_Effective_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Effective_Dte", "#RTE-EFFECTIVE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Rte_Inception_Dte = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Inception_Dte", "#RTE-INCEPTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Nec_Input_Pnd_Rte_Nbr_Rates = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Nbr_Rates", "#RTE-NBR-RATES", FieldType.NUMERIC, 
            3);
        pnd_Nec_Input_Pnd_Rte_Acct_Product_Cde = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Rte_Acct_Product_Cde", "#RTE-ACCT-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Nec_Input_Pnd_Nec_Fil2 = pnd_Nec_Input__R_Field_1.newFieldArrayInGroup("pnd_Nec_Input_Pnd_Nec_Fil2", "#NEC-FIL2", FieldType.STRING, 250, new 
            DbsArrayController(1, 7));
        pnd_Nec_Input_Pnd_Nec_Fil3 = pnd_Nec_Input__R_Field_1.newFieldInGroup("pnd_Nec_Input_Pnd_Nec_Fil3", "#NEC-FIL3", FieldType.STRING, 118);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Wk_Datex = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Wk_Datex", "#WK-DATEX", FieldType.STRING, 8);

        pnd_Work_Area__R_Field_2 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_2", "REDEFINE", pnd_Work_Area_Pnd_Wk_Datex);
        pnd_Work_Area_Pnd_Wk_Daten = pnd_Work_Area__R_Field_2.newFieldInGroup("pnd_Work_Area_Pnd_Wk_Daten", "#WK-DATEN", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Wk_Dated = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Wk_Dated", "#WK-DATED", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Necp4014() throws Exception
    {
        super("Necp4014");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************                                                                                       //Natural: FORMAT SG = OFF PS = 60 LS = 132
        //*               >>>>>  MAIN PROCESS STARTS HERE     <<<<<               *
        //* ***********************************************************************
        //*  READ WORK FILE 1 #INPUT-FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-FILE
        while (condition(getWorkFiles().read(1, pnd_Input_File)))
        {
            pnd_Input_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #INPUT-COUNT
            pnd_Nec_Input_Pnd_Neca4005_Rec.getValue(1,":",24).setValue(pnd_Input_File_Pnd_Input_Rec.getValue(1,":",24));                                                  //Natural: MOVE #INPUT-REC ( 1:24 ) TO #NECA4005-REC ( 1:24 )
            pnd_Nec_Input_Pnd_Neca4005_Fil1.setValue(pnd_Input_File_Pnd_Input_Fil1);                                                                                      //Natural: MOVE #INPUT-FIL1 TO #NECA4005-FIL1
            getReports().write(0, "WILL DECIDE ","=",pnd_Nec_Input_Pnd_Action_Cde,"=",pnd_Nec_Input_Pnd_Entity_Cde);                                                      //Natural: WRITE 'WILL DECIDE ' '=' #ACTION-CDE '=' #ENTITY-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaNeca4005.getNeca4005().reset();                                                                                                                            //Natural: RESET NECA4005
            pdaNeca4005.getNeca4005_Action_Cde().setValue(pnd_Nec_Input_Pnd_Action_Cde);                                                                                  //Natural: MOVE #ACTION-CDE TO NECA4005.ACTION-CDE
            //*  JLE 04/07/2014
            pdaNeca4005.getNeca4005_Entity_Cde().setValue(pnd_Nec_Input_Pnd_Entity_Cde);                                                                                  //Natural: MOVE #ENTITY-CDE TO NECA4005.ENTITY-CDE
            //*  NECA4005.MODIFIED-BY  := 'INFORMAT'
            pdaNeca4005.getNeca4005_Modified_By().setValue("DATASTAGE");                                                                                                  //Natural: ASSIGN NECA4005.MODIFIED-BY := 'DATASTAGE'
            short decideConditionsMet888 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #ENTITY-CDE;//Natural: VALUE 'FND'
            if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("FND"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-FND
                sub_Process_Cntrl_Fnd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'ACL'
            else if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("ACL"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-ACL
                sub_Process_Cntrl_Acl();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'CFN'
            else if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("CFN"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-CFN
                sub_Process_Cntrl_Cfn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'FDS'
            else if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("FDS"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-FDS
                sub_Process_Cntrl_Fds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'FST'
            else if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("FST"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-FST
                sub_Process_Cntrl_Fst();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'RTE'
            else if (condition((pnd_Nec_Input_Pnd_Entity_Cde.equals("RTE"))))
            {
                decideConditionsMet888++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-CNTRL-RTE
                sub_Process_Cntrl_Rte();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(0, "Error ...");                                                                                                                       //Natural: WRITE 'Error ...'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            DbsUtil.callnat(Necn4005.class , getCurrentProcessState(), pdaNeca4005.getNeca4005());                                                                        //Natural: CALLNAT 'NECN4005' NECA4005
            if (condition(Global.isEscape())) return;
            getReports().write(0, pdaNeca4005.getNeca4005_Return_Cde(),NEWLINE,pdaNeca4005.getNeca4005_Return_Msg());                                                     //Natural: WRITE NECA4005.RETURN-CDE / NECA4005.RETURN-MSG
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Input_Count);                                                                                                                       //Natural: WRITE '=' #INPUT-COUNT
        if (Global.isEscape()) return;
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-FND
        //* *** LEGACY UPDATES START HERE ******************
        //* *** LEGACY UPDATES ENDS HERE *******************
        //*  NECA4005.FND-FUND-SUMMARY-LINE6 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE7 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE8 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE9 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE10 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE11 / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE12     / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE13     / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE14     / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE15     / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE16     / '='
        //*  NECA4005.FND-FUND-SUMMARY-LINE17     / '='
        //* *** LEGACY UPDATES STARTS HERE ***************
        //* *** LEGACY UPDATES ENDS HERE *****************
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-ACL
        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-ACL
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-CFN
        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-CFN.
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-FDS
        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-FDS.
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-FST
        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-FST.
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CNTRL-RTE
        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-RTE.
    }
    private void sub_Process_Cntrl_Fnd() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-FND
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  POPULATE THE REQUIRED PARAMETERS FOR TABLE NEW-EXT-CNTRL-FND
        pnd_Work_Area_Pnd_Wk_Datex.setValue(pnd_Nec_Input_Pnd_Fnd_Inception_Dte, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT JUSTIFIED #FND-INCEPTION-DTE TO #WK-DATEX
        DbsUtil.examine(new ExamineSource(pnd_Work_Area_Pnd_Wk_Datex), new ExamineSearch(" "), new ExamineReplace("0"));                                                  //Natural: EXAMINE #WK-DATEX ' ' REPLACE '0'
        if (condition(! (DbsUtil.maskMatches(pnd_Work_Area_Pnd_Wk_Datex,"MMDDYYYY"))))                                                                                    //Natural: IF #WK-DATEX NE MASK ( MMDDYYYY )
        {
            getReports().write(0, "Fund Inception Date not in MMDDYYYY format",pnd_Work_Area_Pnd_Wk_Datex);                                                               //Natural: WRITE 'Fund Inception Date not in MMDDYYYY format' #WK-DATEX
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Wk_Dated.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Work_Area_Pnd_Wk_Datex);                                                         //Natural: MOVE EDITED #WK-DATEX TO #WK-DATED ( EM = MMDDYYYY )
            pnd_Work_Area_Pnd_Wk_Datex.setValueEdited(pnd_Work_Area_Pnd_Wk_Dated,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED #WK-DATED ( EM = YYYYMMDD ) TO #WK-DATEX
            pnd_Nec_Input_Pnd_Fnd_Inception_Dte.setValue(pnd_Work_Area_Pnd_Wk_Daten);                                                                                     //Natural: MOVE #WK-DATEN TO #FND-INCEPTION-DTE
            //*  FE0908
        }                                                                                                                                                                 //Natural: END-IF
        pdaNeca4005.getNeca4005_Fnd_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Fnd_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.FND-TICKER-SYMBOL := #FND-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Fnd_Fund_Family_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Family_Cde);                                                                    //Natural: ASSIGN NECA4005.FND-FUND-FAMILY-CDE := #FND-FUND-FAMILY-CDE
        pdaNeca4005.getNeca4005_Fnd_Fund_Style().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Style);                                                                              //Natural: ASSIGN NECA4005.FND-FUND-STYLE := #FND-FUND-STYLE
        pdaNeca4005.getNeca4005_Fnd_Fund_Size().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Size);                                                                                //Natural: ASSIGN NECA4005.FND-FUND-SIZE := #FND-FUND-SIZE
        pdaNeca4005.getNeca4005_Fnd_Fund_Nme().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Nme);                                                                                  //Natural: ASSIGN NECA4005.FND-FUND-NME := #FND-FUND-NME
        pdaNeca4005.getNeca4005_Fnd_Inception_Dte().setValue(pnd_Nec_Input_Pnd_Fnd_Inception_Dte);                                                                        //Natural: ASSIGN NECA4005.FND-INCEPTION-DTE := #FND-INCEPTION-DTE
        pdaNeca4005.getNeca4005_Fnd_Cusip().setValue(pnd_Nec_Input_Pnd_Fnd_Cusip);                                                                                        //Natural: ASSIGN NECA4005.FND-CUSIP := #FND-CUSIP
        pdaNeca4005.getNeca4005_Fnd_Category_Class_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Category_Class_Cde);                                                              //Natural: ASSIGN NECA4005.FND-CATEGORY-CLASS-CDE := #FND-CATEGORY-CLASS-CDE
        pdaNeca4005.getNeca4005_Fnd_Category_Rptng_Seq().setValue(pnd_Nec_Input_Pnd_Fnd_Category_Rptng_Seq, MoveOption.RightJustified);                                   //Natural: MOVE RIGHT JUSTIFIED #FND-CATEGORY-RPTNG-SEQ TO NECA4005.FND-CATEGORY-RPTNG-SEQ
        //*  FE0908 RIGHT JUST
        //* FE0908
        pdaNeca4005.getNeca4005_Fnd_Category_Fund_Seq().setValue(pnd_Nec_Input_Pnd_Fnd_Category_Fund_Seq, MoveOption.RightJustified);                                     //Natural: MOVE RIGHT JUSTIFIED #FND-CATEGORY-FUND-SEQ TO NECA4005.FND-CATEGORY-FUND-SEQ
        pdaNeca4005.getNeca4005_Fnd_Alpha_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Cde);                                                                      //Natural: ASSIGN NECA4005.FND-ALPHA-FUND-CDE := #FND-ALPHA-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_Num_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Num_Fund_Cde);                                                                          //Natural: ASSIGN NECA4005.FND-NUM-FUND-CDE := #FND-NUM-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_Reporting_Seq().setValue(pnd_Nec_Input_Pnd_Fnd_Reporting_Seq, MoveOption.RightJustified);                                             //Natural: MOVE RIGHT JUSTIFIED #FND-REPORTING-SEQ TO NECA4005.FND-REPORTING-SEQ
        pdaNeca4005.getNeca4005_Fnd_Unit_Rate_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Unit_Rate_Ind);                                                                        //Natural: ASSIGN NECA4005.FND-UNIT-RATE-IND := #FND-UNIT-RATE-IND
        pdaNeca4005.getNeca4005_Fnd_Alpha_Fund_Seq().setValue(pnd_Nec_Input_Pnd_Fnd_Alpha_Fund_Seq);                                                                      //Natural: ASSIGN NECA4005.FND-ALPHA-FUND-SEQ := #FND-ALPHA-FUND-SEQ
        pdaNeca4005.getNeca4005_Fnd_V1_Num_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_V1_Num_Fund_Cde);                                                                    //Natural: ASSIGN NECA4005.FND-V1-NUM-FUND-CDE := #FND-V1-NUM-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_V1_Alpha_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_V1_Alpha_Fund_Cde);                                                                //Natural: ASSIGN NECA4005.FND-V1-ALPHA-FUND-CDE := #FND-V1-ALPHA-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_Abbr_Factor_Key().setValue(pnd_Nec_Input_Pnd_Fnd_Abbr_Factor_Key);                                                                    //Natural: ASSIGN NECA4005.FND-ABBR-FACTOR-KEY := #FND-ABBR-FACTOR-KEY
        pdaNeca4005.getNeca4005_Fnd_Act_Unit_Acct_Nm().setValue(pnd_Nec_Input_Pnd_Fnd_Act_Unit_Acct_Nm);                                                                  //Natural: ASSIGN NECA4005.FND-ACT-UNIT-ACCT-NM := #FND-ACT-UNIT-ACCT-NM
        pdaNeca4005.getNeca4005_Fnd_V2_Alpha_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Alpha_Cde);                                                                          //Natural: ASSIGN NECA4005.FND-V2-ALPHA-CDE := #FND-V2-ALPHA-CDE
        pdaNeca4005.getNeca4005_Fnd_V2_Alpha_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Alpha_Cde);                                                                          //Natural: ASSIGN NECA4005.FND-V2-ALPHA-CDE := #FND-V2-ALPHA-CDE
        pdaNeca4005.getNeca4005_Fnd_V2_Rt_1().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Rt_1);                                                                                    //Natural: ASSIGN NECA4005.FND-V2-RT-1 := #FND-V2-RT-1
        pdaNeca4005.getNeca4005_Fnd_V2_Rt_2().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Rt_2);                                                                                    //Natural: ASSIGN NECA4005.FND-V2-RT-2 := #FND-V2-RT-2
        pdaNeca4005.getNeca4005_Fnd_V2_Rt_3().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Rt_3);                                                                                    //Natural: ASSIGN NECA4005.FND-V2-RT-3 := #FND-V2-RT-3
        pdaNeca4005.getNeca4005_Fnd_Ivc_Company_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Ivc_Company_Cde);                                                                    //Natural: ASSIGN NECA4005.FND-IVC-COMPANY-CDE := #FND-IVC-COMPANY-CDE
        pdaNeca4005.getNeca4005_Fnd_V2_Active_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_V2_Active_Ind);                                                                        //Natural: ASSIGN NECA4005.FND-V2-ACTIVE-IND := #FND-V2-ACTIVE-IND
        pdaNeca4005.getNeca4005_Fnd_Dividend_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Dividend_Ind);                                                                          //Natural: ASSIGN NECA4005.FND-DIVIDEND-IND := #FND-DIVIDEND-IND
        pdaNeca4005.getNeca4005_Fnd_Rate_Basis_Points().setValue(pnd_Nec_Input_Pnd_Fnd_Rate_Basis_Points);                                                                //Natural: ASSIGN NECA4005.FND-RATE-BASIS-POINTS := #FND-RATE-BASIS-POINTS
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line1().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line1);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE1 := #FND-FUND-SUMMARY-LINE1
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line2().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line2);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE2 := #FND-FUND-SUMMARY-LINE2
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line3().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line3);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE3 := #FND-FUND-SUMMARY-LINE3
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line4().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line4);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE4 := #FND-FUND-SUMMARY-LINE4
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line5().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line5);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE5 := #FND-FUND-SUMMARY-LINE5
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line6().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line6);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE6 := #FND-FUND-SUMMARY-LINE6
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line7().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line7);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE7 := #FND-FUND-SUMMARY-LINE7
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line8().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line8);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE8 := #FND-FUND-SUMMARY-LINE8
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line9().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line9);                                                              //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE9 := #FND-FUND-SUMMARY-LINE9
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line10().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line10);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE10 := #FND-FUND-SUMMARY-LINE10
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line11().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line11);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE11 := #FND-FUND-SUMMARY-LINE11
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line12().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line12);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE12 := #FND-FUND-SUMMARY-LINE12
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line13().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line13);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE13 := #FND-FUND-SUMMARY-LINE13
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line14().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line14);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE14 := #FND-FUND-SUMMARY-LINE14
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line15().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line15);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE15 := #FND-FUND-SUMMARY-LINE15
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line16().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line16);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE16 := #FND-FUND-SUMMARY-LINE16
        pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line17().setValue(pnd_Nec_Input_Pnd_Fnd_Fund_Summary_Line17);                                                            //Natural: ASSIGN NECA4005.FND-FUND-SUMMARY-LINE17 := #FND-FUND-SUMMARY-LINE17
        pdaNeca4005.getNeca4005_Fnd_Functional_Cmpny().setValue(pnd_Nec_Input_Pnd_Fnd_Functional_Cmpny);                                                                  //Natural: ASSIGN NECA4005.FND-FUNCTIONAL-CMPNY := #FND-FUNCTIONAL-CMPNY
        pdaNeca4005.getNeca4005_Fnd_External_Cusip().setValue(pnd_Nec_Input_Pnd_Fnd_External_Cusip);                                                                      //Natural: ASSIGN NECA4005.FND-EXTERNAL-CUSIP := #FND-EXTERNAL-CUSIP
        pdaNeca4005.getNeca4005_Fnd_Fin_Dwnld_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Fnd_Fin_Dwnld_Ticker_Symbol);                                                    //Natural: ASSIGN NECA4005.FND-FIN-DWNLD-TICKER-SYMBOL := #FND-FIN-DWNLD-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Fnd_Ia_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Ia_Fund_Cde);                                                                            //Natural: ASSIGN NECA4005.FND-IA-FUND-CDE := #FND-IA-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_Enterprise_Fund_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Enterprise_Fund_Cde);                                                            //Natural: ASSIGN NECA4005.FND-ENTERPRISE-FUND-CDE := #FND-ENTERPRISE-FUND-CDE
        pdaNeca4005.getNeca4005_Fnd_Proprietary_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Proprietary_Ind);                                                                    //Natural: ASSIGN NECA4005.FND-PROPRIETARY-IND := #FND-PROPRIETARY-IND
        pdaNeca4005.getNeca4005_Fnd_Investment_Typ_Id().setValue(pnd_Nec_Input_Pnd_Fnd_Investment_Typ_Id);                                                                //Natural: ASSIGN NECA4005.FND-INVESTMENT-TYP-ID := #FND-INVESTMENT-TYP-ID
        pdaNeca4005.getNeca4005_Fnd_Share_Class_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Share_Class_Ind);                                                                    //Natural: ASSIGN NECA4005.FND-SHARE-CLASS-IND := #FND-SHARE-CLASS-IND
        pdaNeca4005.getNeca4005_Fnd_Act_Class_Cde().setValue(pnd_Nec_Input_Pnd_Fnd_Act_Class_Cde);                                                                        //Natural: ASSIGN NECA4005.FND-ACT-CLASS-CDE := #FND-ACT-CLASS-CDE
        pdaNeca4005.getNeca4005_Fnd_Act_Invesment_Typ().setValue(pnd_Nec_Input_Pnd_Fnd_Act_Invesment_Typ);                                                                //Natural: ASSIGN NECA4005.FND-ACT-INVESMENT-TYP := #FND-ACT-INVESMENT-TYP
        pdaNeca4005.getNeca4005_Fnd_Investment_Grouping_Id().setValue(pnd_Nec_Input_Pnd_Fnd_Investment_Grouping_Id);                                                      //Natural: ASSIGN NECA4005.FND-INVESTMENT-GROUPING-ID := #FND-INVESTMENT-GROUPING-ID
        pdaNeca4005.getNeca4005_Fnd_Core_Fund_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Core_Fund_Ind);                                                                        //Natural: ASSIGN NECA4005.FND-CORE-FUND-IND := #FND-CORE-FUND-IND
        pdaNeca4005.getNeca4005_Fnd_Intl_Fund_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Intl_Fund_Ind);                                                                        //Natural: ASSIGN NECA4005.FND-INTL-FUND-IND := #FND-INTL-FUND-IND
        pdaNeca4005.getNeca4005_Fnd_Pay_Ind().setValue(pnd_Nec_Input_Pnd_Fnd_Pay_Ind);                                                                                    //Natural: ASSIGN NECA4005.FND-PAY-IND := #FND-PAY-IND
        getReports().write(0, "=",pdaNeca4005.getNeca4005_Fnd_Ticker_Symbol(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Family_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Style(), //Natural: WRITE '=' NECA4005.FND-TICKER-SYMBOL / '=' NECA4005.FND-FUND-FAMILY-CDE / '=' NECA4005.FND-FUND-STYLE / '=' NECA4005.FND-FUND-SIZE / '=' NECA4005.FND-FUND-NME / '=' NECA4005.FND-INCEPTION-DTE / '=' NECA4005.FND-CUSIP / '=' NECA4005.FND-CATEGORY-CLASS-CDE / '=' NECA4005.FND-CATEGORY-RPTNG-SEQ / '=' NECA4005.FND-CATEGORY-FUND-SEQ / '=' NECA4005.FND-ALPHA-FUND-CDE / '=' NECA4005.FND-NUM-FUND-CDE / '=' NECA4005.FND-REPORTING-SEQ / '=' NECA4005.FND-UNIT-RATE-IND / '=' NECA4005.FND-ALPHA-FUND-SEQ / '=' NECA4005.FND-V1-NUM-FUND-CDE / '=' NECA4005.FND-V1-ALPHA-FUND-CDE / '=' NECA4005.FND-ABBR-FACTOR-KEY / '=' NECA4005.FND-ACT-UNIT-ACCT-NM / '=' NECA4005.FND-V2-ALPHA-CDE / '=' NECA4005.FND-V2-ALPHA-CDE / '=' NECA4005.FND-V2-RT-1 / '=' NECA4005.FND-V2-RT-2 / '=' NECA4005.FND-V2-RT-3 / '=' NECA4005.FND-IVC-COMPANY-CDE / '=' NECA4005.FND-V2-ACTIVE-IND / '=' NECA4005.FND-DIVIDEND-IND / '=' NECA4005.FND-RATE-BASIS-POINTS / '=' NECA4005.FND-FUND-SUMMARY-LINE1 / '=' NECA4005.FND-FUND-SUMMARY-LINE2 / '=' NECA4005.FND-FUND-SUMMARY-LINE3 / '=' NECA4005.FND-FUND-SUMMARY-LINE4 / '=' NECA4005.FND-FUND-SUMMARY-LINE5 / '=' NECA4005.FND-FUNCTIONAL-CMPNY / '=' NECA4005.FND-EXTERNAL-CUSIP / '=' NECA4005.FND-FIN-DWNLD-TICKER-SYMBOL / '=' NECA4005.FND-IA-FUND-CDE / '=' NECA4005.FND-ENTERPRISE-FUND-CDE / '=' NECA4005.FND-PROPRIETARY-IND / '=' NECA4005.FND-INVESTMENT-TYP-ID / '=' NECA4005.FND-SHARE-CLASS-IND / '=' NECA4005.FND-ACT-CLASS-CDE / '=' NECA4005.FND-ACT-INVESMENT-TYP / '=' NECA4005.FND-INVESTMENT-GROUPING-ID / '=' NECA4005.FND-CORE-FUND-IND / '=' NECA4005.FND-INTL-FUND-IND / '=' NECA4005.FND-PAY-IND
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Size(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Nme(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Inception_Dte(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Cusip(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Category_Class_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Category_Rptng_Seq(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Category_Fund_Seq(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Alpha_Fund_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Num_Fund_Cde(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Reporting_Seq(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Unit_Rate_Ind(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Alpha_Fund_Seq(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V1_Num_Fund_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V1_Alpha_Fund_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Abbr_Factor_Key(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Act_Unit_Acct_Nm(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Alpha_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Alpha_Cde(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Rt_1(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Rt_2(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Rt_3(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Ivc_Company_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_V2_Active_Ind(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Dividend_Ind(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Rate_Basis_Points(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line1(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line2(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line3(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line4(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fund_Summary_Line5(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Functional_Cmpny(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_External_Cusip(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Fin_Dwnld_Ticker_Symbol(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Ia_Fund_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Enterprise_Fund_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Proprietary_Ind(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Investment_Typ_Id(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Share_Class_Ind(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Act_Class_Cde(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Act_Invesment_Typ(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Investment_Grouping_Id(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Core_Fund_Ind(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Intl_Fund_Ind(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fnd_Pay_Ind());
        if (Global.isEscape()) return;
        //*  PROCESS-CNTRL-FND
    }
    private void sub_Process_Cntrl_Acl() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-ACL
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4005.getNeca4005_Acl_Asset_Class_Cde().setValue(pnd_Nec_Input_Pnd_Acl_Asset_Class_Cde);                                                                    //Natural: ASSIGN NECA4005.ACL-ASSET-CLASS-CDE := #ACL-ASSET-CLASS-CDE
        pdaNeca4005.getNeca4005_Acl_Asset_Class_Desc().setValue(pnd_Nec_Input_Pnd_Acl_Asset_Class_Desc);                                                                  //Natural: ASSIGN NECA4005.ACL-ASSET-CLASS-DESC := #ACL-ASSET-CLASS-DESC
        pdaNeca4005.getNeca4005_Acl_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Acl_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.ACL-TICKER-SYMBOL := #ACL-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Acl_Fund_Percentage().setValue(pnd_Nec_Input_Pnd_Acl_Fund_Percentage);                                                                    //Natural: ASSIGN NECA4005.ACL-FUND-PERCENTAGE := #ACL-FUND-PERCENTAGE
        pdaNeca4005.getNeca4005_Acl_Domestic_Holdings().setValue(pnd_Nec_Input_Pnd_Acl_Domestic_Holdings);                                                                //Natural: ASSIGN NECA4005.ACL-DOMESTIC-HOLDINGS := #ACL-DOMESTIC-HOLDINGS
        pdaNeca4005.getNeca4005_Acl_International_Holdings().setValue(pnd_Nec_Input_Pnd_Acl_International_Holdings);                                                      //Natural: ASSIGN NECA4005.ACL-INTERNATIONAL-HOLDINGS := #ACL-INTERNATIONAL-HOLDINGS
        getReports().write(0, NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_Asset_Class_Cde,NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_Asset_Class_Desc,NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_Ticker_Symbol, //Natural: WRITE / '=' #ACL-ASSET-CLASS-CDE / '=' #ACL-ASSET-CLASS-DESC / '=' #ACL-TICKER-SYMBOL / '=' #ACL-FUND-PERCENTAGE / '=' #ACL-DOMESTIC-HOLDINGS / '=' #ACL-INTERNATIONAL-HOLDINGS
            NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_Fund_Percentage,NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_Domestic_Holdings,NEWLINE,"=",pnd_Nec_Input_Pnd_Acl_International_Holdings);
        if (Global.isEscape()) return;
        //*  PROCESS-CNTRL-ACL
    }
    private void sub_Process_Cntrl_Cfn() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-CFN
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4005.getNeca4005_Cfn_Company_Cde().setValue(pnd_Nec_Input_Pnd_Cfn_Company_Cde);                                                                            //Natural: ASSIGN NECA4005.CFN-COMPANY-CDE := #CFN-COMPANY-CDE
        pdaNeca4005.getNeca4005_Cfn_Product_Cde().setValue(pnd_Nec_Input_Pnd_Cfn_Product_Cde);                                                                            //Natural: ASSIGN NECA4005.CFN-PRODUCT-CDE := #CFN-PRODUCT-CDE
        pdaNeca4005.getNeca4005_Cfn_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Cfn_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.CFN-TICKER-SYMBOL := #CFN-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Cfn_Sub_Product_Cde().setValue(pnd_Nec_Input_Pnd_Cfn_Sub_Product_Cde);                                                                    //Natural: ASSIGN NECA4005.CFN-SUB-PRODUCT-CDE := #CFN-SUB-PRODUCT-CDE
        getReports().write(0, NEWLINE,"=",pdaNeca4005.getNeca4005_Cfn_Company_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Cfn_Product_Cde(),NEWLINE,"=",                    //Natural: WRITE / '=' NECA4005.CFN-COMPANY-CDE / '=' NECA4005.CFN-PRODUCT-CDE / '=' NECA4005.CFN-TICKER-SYMBOL / '=' NECA4005.CFN-SUB-PRODUCT-CDE
            pdaNeca4005.getNeca4005_Cfn_Ticker_Symbol(),NEWLINE,"=",pdaNeca4005.getNeca4005_Cfn_Sub_Product_Cde());
        if (Global.isEscape()) return;
        //*  ------------------
        //*  PROCESS-CNTRL-CFN
    }
    private void sub_Process_Cntrl_Fds() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-FDS
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4005.getNeca4005_Fds_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Fds_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.FDS-TICKER-SYMBOL := #FDS-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Fds_Fund_Name_Ref_Cde().setValue(pnd_Nec_Input_Pnd_Fds_Fund_Name_Ref_Cde);                                                                //Natural: ASSIGN NECA4005.FDS-FUND-NAME-REF-CDE := #FDS-FUND-NAME-REF-CDE
        pdaNeca4005.getNeca4005_Fds_Fund_Nme().setValue(pnd_Nec_Input_Pnd_Fds_Fund_Nme);                                                                                  //Natural: ASSIGN NECA4005.FDS-FUND-NME := #FDS-FUND-NME
        pdaNeca4005.getNeca4005_Fds_System_Desc().setValue(pnd_Nec_Input_Pnd_Fds_System_Desc);                                                                            //Natural: ASSIGN NECA4005.FDS-SYSTEM-DESC := #FDS-SYSTEM-DESC
        getReports().write(0, NEWLINE,"=",pdaNeca4005.getNeca4005_Fds_Ticker_Symbol(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fds_Fund_Name_Ref_Cde(),NEWLINE,                //Natural: WRITE / '=' NECA4005.FDS-TICKER-SYMBOL / '=' NECA4005.FDS-FUND-NAME-REF-CDE / '=' NECA4005.FDS-FUND-NME / '=' NECA4005.FDS-SYSTEM-DESC
            "=",pdaNeca4005.getNeca4005_Fds_Fund_Nme(),NEWLINE,"=",pdaNeca4005.getNeca4005_Fds_System_Desc());
        if (Global.isEscape()) return;
        //*  PROCESS-CNTRL-FDS
    }
    private void sub_Process_Cntrl_Fst() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-FST
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4005.getNeca4005_Fst_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Fst_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.FST-TICKER-SYMBOL := #FST-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Fst_Fund_Status().setValue(pnd_Nec_Input_Pnd_Fst_Fund_Status);                                                                            //Natural: ASSIGN NECA4005.FST-FUND-STATUS := #FST-FUND-STATUS
        pdaNeca4005.getNeca4005_Fst_Effective_Dte().setValue(pnd_Nec_Input_Pnd_Fst_Effective_Dte);                                                                        //Natural: ASSIGN NECA4005.FST-EFFECTIVE-DTE := #FST-EFFECTIVE-DTE
        pdaNeca4005.getNeca4005_Fst_Audit_Dte().setValue(pnd_Nec_Input_Pnd_Fst_Audit_Dte);                                                                                //Natural: ASSIGN NECA4005.FST-AUDIT-DTE := #FST-AUDIT-DTE
        pdaNeca4005.getNeca4005_Fst_Effective_Dte_Comp().setValue(pnd_Nec_Input_Pnd_Fst_Effective_Dte_Comp);                                                              //Natural: ASSIGN NECA4005.FST-EFFECTIVE-DTE-COMP := #FST-EFFECTIVE-DTE-COMP
        pdaNeca4005.getNeca4005_Fst_Open_To_New_Invstr().setValue(pnd_Nec_Input_Pnd_Fst_Open_To_New_Invstr);                                                              //Natural: ASSIGN NECA4005.FST-OPEN-TO-NEW-INVSTR := #FST-OPEN-TO-NEW-INVSTR
        pdaNeca4005.getNeca4005_Fst_Open_To_Existing_Invstr().setValue(pnd_Nec_Input_Pnd_Fst_Open_To_Existing_Invstr);                                                    //Natural: ASSIGN NECA4005.FST-OPEN-TO-EXISTING-INVSTR := #FST-OPEN-TO-EXISTING-INVSTR
        pdaNeca4005.getNeca4005_Fst_Open_To_Trnsfr().setValue(pnd_Nec_Input_Pnd_Fst_Open_To_Trnsfr);                                                                      //Natural: ASSIGN NECA4005.FST-OPEN-TO-TRNSFR := #FST-OPEN-TO-TRNSFR
        pdaNeca4005.getNeca4005_Fst_Open_To_Premiums().setValue(pnd_Nec_Input_Pnd_Fst_Open_To_Premiums);                                                                  //Natural: ASSIGN NECA4005.FST-OPEN-TO-PREMIUMS := #FST-OPEN-TO-PREMIUMS
        getReports().write(0, NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Ticker_Symbol,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Fund_Status,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Effective_Dte, //Natural: WRITE / '=' #FST-TICKER-SYMBOL / '=' #FST-FUND-STATUS / '=' #FST-EFFECTIVE-DTE / '=' #FST-AUDIT-DTE / '=' #FST-EFFECTIVE-DTE-COMP / '=' #FST-OPEN-TO-NEW-INVSTR / '=' #FST-OPEN-TO-EXISTING-INVSTR / '=' #FST-OPEN-TO-TRNSFR / '=' #FST-OPEN-TO-PREMIUMS
            NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Audit_Dte,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Effective_Dte_Comp,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Open_To_New_Invstr,
            NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Open_To_Existing_Invstr,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Open_To_Trnsfr,NEWLINE,"=",pnd_Nec_Input_Pnd_Fst_Open_To_Premiums);
        if (Global.isEscape()) return;
        //*  PROCESS-CNTRL-FST
    }
    private void sub_Process_Cntrl_Rte() throws Exception                                                                                                                 //Natural: PROCESS-CNTRL-RTE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4005.getNeca4005_Rte_Product_Cde().setValue(pnd_Nec_Input_Pnd_Rte_Product_Cde);                                                                            //Natural: ASSIGN NECA4005.RTE-PRODUCT-CDE := #RTE-PRODUCT-CDE
        pdaNeca4005.getNeca4005_Rte_Ticker_Symbol().setValue(pnd_Nec_Input_Pnd_Rte_Ticker_Symbol);                                                                        //Natural: ASSIGN NECA4005.RTE-TICKER-SYMBOL := #RTE-TICKER-SYMBOL
        pdaNeca4005.getNeca4005_Rte_Rate_Cde().setValue(pnd_Nec_Input_Pnd_Rte_Rate_Cde);                                                                                  //Natural: ASSIGN NECA4005.RTE-RATE-CDE := #RTE-RATE-CDE
        pdaNeca4005.getNeca4005_Rte_Interest_Rate().setValue(pnd_Nec_Input_Pnd_Rte_Interest_Rate);                                                                        //Natural: ASSIGN NECA4005.RTE-INTEREST-RATE := #RTE-INTEREST-RATE
        pdaNeca4005.getNeca4005_Rte_Effective_Dte().setValue(pnd_Nec_Input_Pnd_Rte_Effective_Dte);                                                                        //Natural: ASSIGN NECA4005.RTE-EFFECTIVE-DTE := #RTE-EFFECTIVE-DTE
        pdaNeca4005.getNeca4005_Rte_Inception_Dte().setValue(pnd_Nec_Input_Pnd_Rte_Inception_Dte);                                                                        //Natural: ASSIGN NECA4005.RTE-INCEPTION-DTE := #RTE-INCEPTION-DTE
        pdaNeca4005.getNeca4005_Rte_Nbr_Rates().setValue(pnd_Nec_Input_Pnd_Rte_Nbr_Rates);                                                                                //Natural: ASSIGN NECA4005.RTE-NBR-RATES := #RTE-NBR-RATES
        pdaNeca4005.getNeca4005_Rte_Acct_Product_Cde().setValue(pnd_Nec_Input_Pnd_Rte_Acct_Product_Cde);                                                                  //Natural: ASSIGN NECA4005.RTE-ACCT-PRODUCT-CDE := #RTE-ACCT-PRODUCT-CDE
        getReports().write(0, NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Product_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Ticker_Symbol(),NEWLINE,"=",                  //Natural: WRITE / '=' NECA4005.RTE-PRODUCT-CDE / '=' NECA4005.RTE-TICKER-SYMBOL / '=' NECA4005.RTE-RATE-CDE / '=' NECA4005.RTE-INTEREST-RATE / '=' NECA4005.RTE-EFFECTIVE-DTE / '=' NECA4005.RTE-INCEPTION-DTE / '=' NECA4005.RTE-NBR-RATES / '=' NECA4005.RTE-ACCT-PRODUCT-CDE
            pdaNeca4005.getNeca4005_Rte_Rate_Cde(),NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Interest_Rate(),NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Effective_Dte(),
            NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Inception_Dte(),NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Nbr_Rates(),NEWLINE,"=",pdaNeca4005.getNeca4005_Rte_Acct_Product_Cde());
        if (Global.isEscape()) return;
        //*  PROCESS-CNTRL-RTE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "SG=OFF PS=60 LS=132");
    }
}
