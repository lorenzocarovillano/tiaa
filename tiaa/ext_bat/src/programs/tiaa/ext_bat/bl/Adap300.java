/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:26 PM
**        * FROM NATURAL PROGRAM : Adap300
************************************************************
**        * FILE NAME            : Adap300.java
**        * CLASS NAME           : Adap300
**        * INSTANCE NAME        : Adap300
************************************************************
**********************************************************************
* SUB-SYSTEM  :  OIA                                                 *
*                                                                    *
* PROGRAM     :  ADAP300                                             *
*                                                                    *
* DESCRIPTION :  EXTRACT AVAILABLE TICKER SYMBOLS FROM               *
*                EXTERNALIZATION FILE                                *
*                                                                    *
* FILE ACCESS :  READ ONLY = NEW-EXT-CNTRL-FST & NEW-EXT-CNTRL-FND   *
*                                                                    *
* PROGRAMMER  :  EDNA PANGANIBAN                                     *
* DATE        :  01/25/2002                                          *
*                                                                    *
* MAINTENANCE :                                                      *
* PROGRAMMER          DATE         DESCRIPTION                       *
*
* KCD (C.DIMITRIU): 08/15/2009     FINRA - EXPENSE FOR ILLUSTRATIONS *
* KCD1(C.DIMITRIU): 08/2013        CREF MULTICLASS PROJECT
**********************************************************************

************************************************************ */

package tiaa.ext_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adap300 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_nec_Fst_View1;
    private DbsField nec_Fst_View1_Nec_Table_Cde;
    private DbsField nec_Fst_View1_Nec_Ticker_Symbol;
    private DbsField nec_Fst_View1_Nec_Fund_Status;
    private DbsField nec_Fst_View1_Nec_Fst_Super;

    private DataAccessProgramView vw_nec_Fnd_View1;
    private DbsField nec_Fnd_View1_Nec_Table_Cde;
    private DbsField nec_Fnd_View1_Nec_Ticker_Symbol;
    private DbsField nec_Fnd_View1_Nec_Fund_Family_Cde;
    private DbsField nec_Fnd_View1_Nec_Fund_Nme;
    private DbsField nec_Fnd_View1_Nec_Fund_Inception_Dte;
    private DbsField nec_Fnd_View1_Nec_Fund_Cusip;
    private DbsField nec_Fnd_View1_Nec_Category_Class_Cde;
    private DbsField nec_Fnd_View1_Nec_Category_Rptng_Seq;
    private DbsField nec_Fnd_View1_Nec_Category_Fund_Seq;
    private DbsField nec_Fnd_View1_Nec_Alpha_Fund_Cde;
    private DbsField nec_Fnd_View1_Nec_Num_Fund_Cde;
    private DbsField nec_Fnd_View1_Nec_Reporting_Seq;
    private DbsField nec_Fnd_View1_Nec_Alpha_Fund_Seq;
    private DbsField nec_Fnd_View1_Nec_Abbr_Factor_Key;
    private DbsField nec_Fnd_View1_Nec_Act_Unit_Acct_Nm;
    private DbsField nec_Fnd_View1_Nec_Functional_Cmpny;
    private DbsField nec_Fnd_View1_Nec_Fnd_Super1;
    private DbsField nec_Fnd_View1_Nec_Act_Class_Cde;
    private DbsField nec_Fnd_View1_Nec_Act_Invesment_Typ;
    private DbsField nec_Fnd_View1_Nec_Investment_Grouping_Id;

    private DataAccessProgramView vw_nec_Exp_View1;
    private DbsField nec_Exp_View1_Nec_Table_Cde;
    private DbsField nec_Exp_View1_Nec_Ticker_Symbol;
    private DbsField nec_Exp_View1_Nec_Modified_By;
    private DbsField nec_Exp_View1_Nec_Modified_Dte;
    private DbsField nec_Exp_View1_Nec_Invstmnt_Advsry_Exp;
    private DbsField nec_Exp_View1_Nec_Administrative_Exp;
    private DbsField nec_Exp_View1_Nec_Distribution_Exp_12b1;
    private DbsField nec_Exp_View1_Nec_Mrtlty_Exp_Risk_Chrgs;
    private DbsField nec_Exp_View1_Nec_Liquidity_Guarantee;
    private DbsField nec_Exp_View1_Nec_Actual_Expense_Chrg;
    private DbsField nec_Exp_View1_Nec_Cref_Avg_Exp_Chrg;
    private DbsField nec_Exp_View1_Nec_Publ_Max_Exp_Chrg;
    private DbsField nec_Exp_View1_Nec_Max_Undrlyng_Mf_Exp;
    private DbsField nec_Exp_View1_Nec_Exp_Reimbrsmnt_Wvrs;
    private DbsField nec_Exp_View1_Nec_Access_Exp_Chrg;
    private DbsField nec_Exp_View1_Nec_Finra_Illstrtn_Exp_Chrg;

    private DataAccessProgramView vw_nec_Rte_View1;
    private DbsField nec_Rte_View1_Nec_Table_Cde;
    private DbsField nec_Rte_View1_Nec_Product_Cde;
    private DbsField nec_Rte_View1_Nec_Ticker_Symbol;
    private DbsField nec_Rte_View1_Nec_Rate_Cde;
    private DbsField nec_Rte_View1_Nec_Rte_Super1;
    private DbsField pnd_Nec_Fst_Super1;

    private DbsGroup pnd_Nec_Fst_Super1__R_Field_1;
    private DbsField pnd_Nec_Fst_Super1_Pnd_Nec_Fst_Table_Cde_Super1;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_2;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1;
    private DbsField pnd_Nec_Rte_Super1;

    private DbsGroup pnd_Nec_Rte_Super1__R_Field_3;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Ticker_Symbol_Super1;

    private DbsGroup pnd_Workfile;
    private DbsField pnd_Workfile_Pnd_W_Ticker_Symbol;
    private DbsField pnd_Workfile_Pnd_W_Fund_Family_Cde;
    private DbsField pnd_Workfile_Pnd_W_Inception_Dte;
    private DbsField pnd_Workfile_Pnd_W_Fund_Cusip;
    private DbsField pnd_Workfile_Pnd_W_Category_Class_Cde;
    private DbsField pnd_Workfile_Pnd_W_Category_Rptng_Seq;
    private DbsField pnd_Workfile_Pnd_W_Category_Fund_Seq;
    private DbsField pnd_Workfile_Pnd_W_Alpha_Fund_Cde;
    private DbsField pnd_Workfile_Pnd_W_Num_Fund_Cde;
    private DbsField pnd_Workfile_Pnd_W_Reporting_Seq;
    private DbsField pnd_Workfile_Pnd_W_Alpha_Fund_Seq;
    private DbsField pnd_Workfile_Pnd_W_Abbr_Factor_Key;
    private DbsField pnd_Workfile_Pnd_W_Gra_Rb;
    private DbsField pnd_Workfile_Pnd_W_Gsra_Rb;
    private DbsField pnd_Workfile_Pnd_W_Act_Unit_Acct_Nm;
    private DbsField pnd_Workfile_Pnd_W_Fund_Nme;
    private DbsField pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp;
    private DbsField pnd_Workfile_Pnd_Filler;
    private DbsField pnd_Workfile_Pnd_W_Nec_Act_Class_Cde;
    private DbsField pnd_Workfile_Pnd_W_Nec_Act_Investment_Typ;
    private DbsField pnd_Workfile_Pnd_W_Nec_Investment_Grouping_Id;
    private DbsField pnd_Workfile_Pnd_Filler1;

    private DbsGroup pnd_Workfile3;
    private DbsField pnd_Workfile3_Nec_Ticker_Symbol;
    private DbsField pnd_Workfile3_Nec_Invstmnt_Advsry_Exp;
    private DbsField pnd_Workfile3_Nec_Administrative_Exp;
    private DbsField pnd_Workfile3_Nec_Distribution_Exp_12b1;
    private DbsField pnd_Workfile3_Nec_Mrtlty_Exp_Risk_Chrgs;
    private DbsField pnd_Workfile3_Nec_Liquidity_Guarantee;
    private DbsField pnd_Workfile3_Nec_Actual_Expense_Chrg;
    private DbsField pnd_Workfile3_Nec_Cref_Avg_Exp_Chrg;
    private DbsField pnd_Workfile3_Nec_Publ_Max_Exp_Chrg;
    private DbsField pnd_Workfile3_Nec_Max_Undrlyng_Mf_Exp;
    private DbsField pnd_Workfile3_Nec_Exp_Reimbrsmnt_Wvrs;
    private DbsField pnd_Workfile3_Nec_Access_Exp_Chrg;
    private DbsField pnd_Workfile3_Nec_Finra_Illstrtn_Exp_Chrg;
    private DbsField pnd_Workfile3_Nec_Functional_Cmpny;
    private DbsField pnd_Workfile3_Filler;
    private DbsField pnd_Count_Funds;
    private DbsField pnd_Count_Not_Active;
    private DbsField pnd_Rb_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_nec_Fst_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Fst_View1", "NEC-FST-VIEW1"), "NEW_EXT_CNTRL_FST", "NEW_EXT_CNTRL");
        nec_Fst_View1_Nec_Table_Cde = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Fst_View1_Nec_Ticker_Symbol = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Fst_View1_Nec_Fund_Status = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Fund_Status", "NEC-FUND-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_FUND_STATUS");
        nec_Fst_View1_Nec_Fst_Super = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Fst_Super", "NEC-FST-SUPER", FieldType.STRING, 22, 
            RepeatingFieldStrategy.None, "NEC_FST_SUPER");
        nec_Fst_View1_Nec_Fst_Super.setSuperDescriptor(true);
        registerRecord(vw_nec_Fst_View1);

        vw_nec_Fnd_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Fnd_View1", "NEC-FND-VIEW1"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        nec_Fnd_View1_Nec_Table_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Fnd_View1_Nec_Ticker_Symbol = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Fnd_View1_Nec_Fund_Family_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Family_Cde", "NEC-FUND-FAMILY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NEC_FUND_FAMILY_CDE");
        nec_Fnd_View1_Nec_Fund_Nme = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Nme", "NEC-FUND-NME", FieldType.STRING, 60, 
            RepeatingFieldStrategy.None, "NEC_FUND_NME");
        nec_Fnd_View1_Nec_Fund_Inception_Dte = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Inception_Dte", "NEC-FUND-INCEPTION-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_FUND_INCEPTION_DTE");
        nec_Fnd_View1_Nec_Fund_Cusip = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Cusip", "NEC-FUND-CUSIP", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_FUND_CUSIP");
        nec_Fnd_View1_Nec_Category_Class_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Class_Cde", "NEC-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        nec_Fnd_View1_Nec_Category_Rptng_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Rptng_Seq", "NEC-CATEGORY-RPTNG-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        nec_Fnd_View1_Nec_Category_Fund_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Fund_Seq", "NEC-CATEGORY-FUND-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_FUND_SEQ");
        nec_Fnd_View1_Nec_Alpha_Fund_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Alpha_Fund_Cde", "NEC-ALPHA-FUND-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_CDE");
        nec_Fnd_View1_Nec_Num_Fund_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Num_Fund_Cde", "NEC-NUM-FUND-CDE", FieldType.NUMERIC, 
            5, RepeatingFieldStrategy.None, "NEC_NUM_FUND_CDE");
        nec_Fnd_View1_Nec_Reporting_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Reporting_Seq", "NEC-REPORTING-SEQ", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "NEC_REPORTING_SEQ");
        nec_Fnd_View1_Nec_Alpha_Fund_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Alpha_Fund_Seq", "NEC-ALPHA-FUND-SEQ", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_SEQ");
        nec_Fnd_View1_Nec_Abbr_Factor_Key = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Abbr_Factor_Key", "NEC-ABBR-FACTOR-KEY", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ABBR_FACTOR_KEY");
        nec_Fnd_View1_Nec_Act_Unit_Acct_Nm = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Unit_Acct_Nm", "NEC-ACT-UNIT-ACCT-NM", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "NEC_ACT_UNIT_ACCT_NM");
        nec_Fnd_View1_Nec_Functional_Cmpny = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Functional_Cmpny", "NEC-FUNCTIONAL-CMPNY", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_FUNCTIONAL_CMPNY");
        nec_Fnd_View1_Nec_Fnd_Super1 = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fnd_Super1", "NEC-FND-SUPER1", FieldType.STRING, 
            13, RepeatingFieldStrategy.None, "NEC_FND_SUPER1");
        nec_Fnd_View1_Nec_Fnd_Super1.setSuperDescriptor(true);
        nec_Fnd_View1_Nec_Act_Class_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Class_Cde", "NEC-ACT-CLASS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_ACT_CLASS_CDE");
        nec_Fnd_View1_Nec_Act_Invesment_Typ = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Invesment_Typ", "NEC-ACT-INVESMENT-TYP", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ACT_INVESMENT_TYP");
        nec_Fnd_View1_Nec_Investment_Grouping_Id = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_nec_Fnd_View1);

        vw_nec_Exp_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Exp_View1", "NEC-EXP-VIEW1"), "NEW_EXT_CNTRL_EXP", "NEW_EXT_CNTRL");
        nec_Exp_View1_Nec_Table_Cde = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Exp_View1_Nec_Table_Cde.setDdmHeader("TABLE CODE");
        nec_Exp_View1_Nec_Ticker_Symbol = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Exp_View1_Nec_Modified_By = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Modified_By", "NEC-MODIFIED-BY", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_MODIFIED_BY");
        nec_Exp_View1_Nec_Modified_Dte = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Modified_Dte", "NEC-MODIFIED-DTE", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "NEC_MODIFIED_DTE");
        nec_Exp_View1_Nec_Invstmnt_Advsry_Exp = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Invstmnt_Advsry_Exp", "NEC-INVSTMNT-ADVSRY-EXP", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_INVSTMNT_ADVSRY_EXP");
        nec_Exp_View1_Nec_Administrative_Exp = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Administrative_Exp", "NEC-ADMINISTRATIVE-EXP", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_ADMINISTRATIVE_EXP");
        nec_Exp_View1_Nec_Distribution_Exp_12b1 = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Distribution_Exp_12b1", "NEC-DISTRIBUTION-EXP-12B1", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_DISTRIBUTION_EXP_12B1");
        nec_Exp_View1_Nec_Mrtlty_Exp_Risk_Chrgs = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Mrtlty_Exp_Risk_Chrgs", "NEC-MRTLTY-EXP-RISK-CHRGS", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_MRTLTY_EXP_RISK_CHRGS");
        nec_Exp_View1_Nec_Liquidity_Guarantee = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Liquidity_Guarantee", "NEC-LIQUIDITY-GUARANTEE", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_LIQUIDITY_GUARANTEE");
        nec_Exp_View1_Nec_Actual_Expense_Chrg = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Actual_Expense_Chrg", "NEC-ACTUAL-EXPENSE-CHRG", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_ACTUAL_EXPENSE_CHRG");
        nec_Exp_View1_Nec_Cref_Avg_Exp_Chrg = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Cref_Avg_Exp_Chrg", "NEC-CREF-AVG-EXP-CHRG", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_CREF_AVG_EXP_CHRG");
        nec_Exp_View1_Nec_Publ_Max_Exp_Chrg = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Publ_Max_Exp_Chrg", "NEC-PUBL-MAX-EXP-CHRG", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_PUBL_MAX_EXP_CHRG");
        nec_Exp_View1_Nec_Max_Undrlyng_Mf_Exp = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Max_Undrlyng_Mf_Exp", "NEC-MAX-UNDRLYNG-MF-EXP", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_MAX_UNDRLYNG_MF_EXP");
        nec_Exp_View1_Nec_Exp_Reimbrsmnt_Wvrs = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Exp_Reimbrsmnt_Wvrs", "NEC-EXP-REIMBRSMNT-WVRS", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_EXP_REIMBRSMNT_WVRS");
        nec_Exp_View1_Nec_Access_Exp_Chrg = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Access_Exp_Chrg", "NEC-ACCESS-EXP-CHRG", FieldType.NUMERIC, 
            4, 3, RepeatingFieldStrategy.None, "NEC_ACCESS_EXP_CHRG");
        nec_Exp_View1_Nec_Finra_Illstrtn_Exp_Chrg = vw_nec_Exp_View1.getRecord().newFieldInGroup("nec_Exp_View1_Nec_Finra_Illstrtn_Exp_Chrg", "NEC-FINRA-ILLSTRTN-EXP-CHRG", 
            FieldType.NUMERIC, 4, 3, RepeatingFieldStrategy.None, "NEC_FINRA_ILLSTRTN_EXP_CHRG");
        registerRecord(vw_nec_Exp_View1);

        vw_nec_Rte_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Rte_View1", "NEC-RTE-VIEW1"), "NEW_EXT_CNTRL_RTE", "NEW_EXT_CNTRL");
        nec_Rte_View1_Nec_Table_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Rte_View1_Nec_Product_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Product_Cde", "NEC-PRODUCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_PRODUCT_CDE");
        nec_Rte_View1_Nec_Product_Cde.setDdmHeader("PRODUCT CODE");
        nec_Rte_View1_Nec_Ticker_Symbol = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Rte_View1_Nec_Rate_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Rate_Cde", "NEC-RATE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NEC_RATE_CDE");
        nec_Rte_View1_Nec_Rte_Super1 = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Rte_Super1", "NEC-RTE-SUPER1", FieldType.STRING, 
            31, RepeatingFieldStrategy.None, "NEC_RTE_SUPER1");
        nec_Rte_View1_Nec_Rte_Super1.setSuperDescriptor(true);
        registerRecord(vw_nec_Rte_View1);

        pnd_Nec_Fst_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fst_Super1", "#NEC-FST-SUPER1", FieldType.STRING, 22);

        pnd_Nec_Fst_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Fst_Super1__R_Field_1", "REDEFINE", pnd_Nec_Fst_Super1);
        pnd_Nec_Fst_Super1_Pnd_Nec_Fst_Table_Cde_Super1 = pnd_Nec_Fst_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Fst_Super1_Pnd_Nec_Fst_Table_Cde_Super1", 
            "#NEC-FST-TABLE-CDE-SUPER1", FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_2 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_2", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1 = pnd_Nec_Fnd_Super1__R_Field_2.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1", 
            "#NEC-FND-TABLE-CDE-SUPER1", FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1 = pnd_Nec_Fnd_Super1__R_Field_2.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1", 
            "#NEC-FND-TICKER-SYMBOL-SUPER1", FieldType.STRING, 10);
        pnd_Nec_Rte_Super1 = localVariables.newFieldInRecord("pnd_Nec_Rte_Super1", "#NEC-RTE-SUPER1", FieldType.STRING, 31);

        pnd_Nec_Rte_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Nec_Rte_Super1__R_Field_3", "REDEFINE", pnd_Nec_Rte_Super1);
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1 = pnd_Nec_Rte_Super1__R_Field_3.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1", 
            "#NEC-RTE-TABLE-CDE-SUPER1", FieldType.STRING, 3);
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1 = pnd_Nec_Rte_Super1__R_Field_3.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1", 
            "#NEC-RTE-PRODUCT-CDE-SUPER1", FieldType.STRING, 10);
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Ticker_Symbol_Super1 = pnd_Nec_Rte_Super1__R_Field_3.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Ticker_Symbol_Super1", 
            "#NEC-RTE-TICKER-SYMBOL-SUPER1", FieldType.STRING, 10);

        pnd_Workfile = localVariables.newGroupInRecord("pnd_Workfile", "#WORKFILE");
        pnd_Workfile_Pnd_W_Ticker_Symbol = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Ticker_Symbol", "#W-TICKER-SYMBOL", FieldType.STRING, 10);
        pnd_Workfile_Pnd_W_Fund_Family_Cde = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Fund_Family_Cde", "#W-FUND-FAMILY-CDE", FieldType.STRING, 
            3);
        pnd_Workfile_Pnd_W_Inception_Dte = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Inception_Dte", "#W-INCEPTION-DTE", FieldType.NUMERIC, 8);
        pnd_Workfile_Pnd_W_Fund_Cusip = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Fund_Cusip", "#W-FUND-CUSIP", FieldType.STRING, 10);
        pnd_Workfile_Pnd_W_Category_Class_Cde = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Category_Class_Cde", "#W-CATEGORY-CLASS-CDE", FieldType.STRING, 
            3);
        pnd_Workfile_Pnd_W_Category_Rptng_Seq = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Category_Rptng_Seq", "#W-CATEGORY-RPTNG-SEQ", FieldType.STRING, 
            5);
        pnd_Workfile_Pnd_W_Category_Fund_Seq = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Category_Fund_Seq", "#W-CATEGORY-FUND-SEQ", FieldType.STRING, 
            5);
        pnd_Workfile_Pnd_W_Alpha_Fund_Cde = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Alpha_Fund_Cde", "#W-ALPHA-FUND-CDE", FieldType.STRING, 2);
        pnd_Workfile_Pnd_W_Num_Fund_Cde = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Num_Fund_Cde", "#W-NUM-FUND-CDE", FieldType.NUMERIC, 5);
        pnd_Workfile_Pnd_W_Reporting_Seq = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Reporting_Seq", "#W-REPORTING-SEQ", FieldType.STRING, 5);
        pnd_Workfile_Pnd_W_Alpha_Fund_Seq = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Alpha_Fund_Seq", "#W-ALPHA-FUND-SEQ", FieldType.STRING, 2);
        pnd_Workfile_Pnd_W_Abbr_Factor_Key = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Abbr_Factor_Key", "#W-ABBR-FACTOR-KEY", FieldType.STRING, 
            2);
        pnd_Workfile_Pnd_W_Gra_Rb = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Gra_Rb", "#W-GRA-RB", FieldType.STRING, 8);
        pnd_Workfile_Pnd_W_Gsra_Rb = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Gsra_Rb", "#W-GSRA-RB", FieldType.STRING, 8);
        pnd_Workfile_Pnd_W_Act_Unit_Acct_Nm = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Act_Unit_Acct_Nm", "#W-ACT-UNIT-ACCT-NM", FieldType.NUMERIC, 
            3);
        pnd_Workfile_Pnd_W_Fund_Nme = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Fund_Nme", "#W-FUND-NME", FieldType.STRING, 60);
        pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp", "#W-MAX-UNDRLYNG-MF-EXP", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile_Pnd_Filler = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler", "#FILLER", FieldType.STRING, 8);
        pnd_Workfile_Pnd_W_Nec_Act_Class_Cde = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Nec_Act_Class_Cde", "#W-NEC-ACT-CLASS-CDE", FieldType.STRING, 
            1);
        pnd_Workfile_Pnd_W_Nec_Act_Investment_Typ = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Nec_Act_Investment_Typ", "#W-NEC-ACT-INVESTMENT-TYP", 
            FieldType.STRING, 2);
        pnd_Workfile_Pnd_W_Nec_Investment_Grouping_Id = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_W_Nec_Investment_Grouping_Id", "#W-NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4);
        pnd_Workfile_Pnd_Filler1 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler1", "#FILLER1", FieldType.STRING, 22);

        pnd_Workfile3 = localVariables.newGroupInRecord("pnd_Workfile3", "#WORKFILE3");
        pnd_Workfile3_Nec_Ticker_Symbol = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 10);
        pnd_Workfile3_Nec_Invstmnt_Advsry_Exp = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Invstmnt_Advsry_Exp", "NEC-INVSTMNT-ADVSRY-EXP", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Administrative_Exp = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Administrative_Exp", "NEC-ADMINISTRATIVE-EXP", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Distribution_Exp_12b1 = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Distribution_Exp_12b1", "NEC-DISTRIBUTION-EXP-12B1", 
            FieldType.NUMERIC, 4, 3);
        pnd_Workfile3_Nec_Mrtlty_Exp_Risk_Chrgs = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Mrtlty_Exp_Risk_Chrgs", "NEC-MRTLTY-EXP-RISK-CHRGS", 
            FieldType.NUMERIC, 4, 3);
        pnd_Workfile3_Nec_Liquidity_Guarantee = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Liquidity_Guarantee", "NEC-LIQUIDITY-GUARANTEE", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Actual_Expense_Chrg = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Actual_Expense_Chrg", "NEC-ACTUAL-EXPENSE-CHRG", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Cref_Avg_Exp_Chrg = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Cref_Avg_Exp_Chrg", "NEC-CREF-AVG-EXP-CHRG", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Publ_Max_Exp_Chrg = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Publ_Max_Exp_Chrg", "NEC-PUBL-MAX-EXP-CHRG", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Max_Undrlyng_Mf_Exp = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Max_Undrlyng_Mf_Exp", "NEC-MAX-UNDRLYNG-MF-EXP", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Exp_Reimbrsmnt_Wvrs = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Exp_Reimbrsmnt_Wvrs", "NEC-EXP-REIMBRSMNT-WVRS", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Access_Exp_Chrg = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Access_Exp_Chrg", "NEC-ACCESS-EXP-CHRG", FieldType.NUMERIC, 
            4, 3);
        pnd_Workfile3_Nec_Finra_Illstrtn_Exp_Chrg = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Finra_Illstrtn_Exp_Chrg", "NEC-FINRA-ILLSTRTN-EXP-CHRG", 
            FieldType.NUMERIC, 4, 3);
        pnd_Workfile3_Nec_Functional_Cmpny = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Nec_Functional_Cmpny", "NEC-FUNCTIONAL-CMPNY", FieldType.STRING, 
            2);
        pnd_Workfile3_Filler = pnd_Workfile3.newFieldInGroup("pnd_Workfile3_Filler", "FILLER", FieldType.STRING, 140);
        pnd_Count_Funds = localVariables.newFieldInRecord("pnd_Count_Funds", "#COUNT-FUNDS", FieldType.NUMERIC, 4);
        pnd_Count_Not_Active = localVariables.newFieldInRecord("pnd_Count_Not_Active", "#COUNT-NOT-ACTIVE", FieldType.NUMERIC, 4);
        pnd_Rb_Found = localVariables.newFieldInRecord("pnd_Rb_Found", "#RB-FOUND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_nec_Fst_View1.reset();
        vw_nec_Fnd_View1.reset();
        vw_nec_Exp_View1.reset();
        vw_nec_Rte_View1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adap300() throws Exception
    {
        super("Adap300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Nec_Fst_Super1_Pnd_Nec_Fst_Table_Cde_Super1.setValue("FST");                                                                                                  //Natural: MOVE 'FST' TO #NEC-FST-TABLE-CDE-SUPER1
        vw_nec_Fst_View1.startDatabaseRead                                                                                                                                //Natural: READ NEC-FST-VIEW1 BY NEC-FST-VIEW1.NEC-FST-SUPER STARTING FROM #NEC-FST-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("NEC_FST_SUPER", ">=", pnd_Nec_Fst_Super1, WcType.BY) },
        new Oc[] { new Oc("NEC_FST_SUPER", "ASC") }
        );
        READ01:
        while (condition(vw_nec_Fst_View1.readNextRow("READ01")))
        {
            if (condition(nec_Fst_View1_Nec_Table_Cde.notEquals(pnd_Nec_Fst_Super1_Pnd_Nec_Fst_Table_Cde_Super1)))                                                        //Natural: IF NEC-FST-VIEW1.NEC-TABLE-CDE NE #NEC-FST-TABLE-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(nec_Fst_View1_Nec_Fund_Status.notEquals("A")))                                                                                                  //Natural: IF NEC-FST-VIEW1.NEC-FUND-STATUS NE 'A'
            {
                getReports().write(0, "=",nec_Fst_View1_Nec_Ticker_Symbol,"  NOT ACTIVE");                                                                                //Natural: WRITE '=' NEC-FST-VIEW1.NEC-TICKER-SYMBOL '  NOT ACTIVE'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Count_Not_Active.nadd(1);                                                                                                                             //Natural: ADD 1 TO #COUNT-NOT-ACTIVE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1.setValue("FND");                                                                                              //Natural: MOVE 'FND' TO #NEC-FND-TABLE-CDE-SUPER1
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                //Natural: MOVE NEC-FST-VIEW1.NEC-TICKER-SYMBOL TO #NEC-FND-TICKER-SYMBOL-SUPER1
            vw_nec_Fnd_View1.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) NEC-FND-VIEW1 WITH NEC-FND-VIEW1.NEC-FND-SUPER1 = #NEC-FND-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_nec_Fnd_View1.readNextRow("FIND01", true)))
            {
                vw_nec_Fnd_View1.setIfNotFoundControlFlag(false);
                if (condition(vw_nec_Fnd_View1.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORD FOUND
                {
                    pnd_Workfile_Pnd_W_Fund_Family_Cde.setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #W-FUND-FAMILY-CDE #W-FUND-CUSIP #W-CATEGORY-CLASS-CDE #W-CATEGORY-RPTNG-SEQ #W-CATEGORY-FUND-SEQ #W-REPORTING-SEQ #W-ALPHA-FUND-SEQ #W-ABBR-FACTOR-KEY #W-FUND-NME
                    pnd_Workfile_Pnd_W_Fund_Cusip.setValue(" ");
                    pnd_Workfile_Pnd_W_Category_Class_Cde.setValue(" ");
                    pnd_Workfile_Pnd_W_Category_Rptng_Seq.setValue(" ");
                    pnd_Workfile_Pnd_W_Category_Fund_Seq.setValue(" ");
                    pnd_Workfile_Pnd_W_Reporting_Seq.setValue(" ");
                    pnd_Workfile_Pnd_W_Alpha_Fund_Seq.setValue(" ");
                    pnd_Workfile_Pnd_W_Abbr_Factor_Key.setValue(" ");
                    pnd_Workfile_Pnd_W_Fund_Nme.setValue(" ");
                    pnd_Workfile_Pnd_W_Inception_Dte.setValue(0);                                                                                                         //Natural: MOVE 0 TO #W-INCEPTION-DTE #W-NUM-FUND-CDE #W-ALPHA-FUND-CDE #W-ACT-UNIT-ACCT-NM
                    pnd_Workfile_Pnd_W_Num_Fund_Cde.setValue(0);
                    pnd_Workfile_Pnd_W_Alpha_Fund_Cde.setValue(0);
                    pnd_Workfile_Pnd_W_Act_Unit_Acct_Nm.setValue(0);
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Workfile_Pnd_W_Fund_Family_Cde.setValue(nec_Fnd_View1_Nec_Fund_Family_Cde);                                                                           //Natural: MOVE NEC-FND-VIEW1.NEC-FUND-FAMILY-CDE TO #W-FUND-FAMILY-CDE
                pnd_Workfile_Pnd_W_Inception_Dte.setValue(nec_Fnd_View1_Nec_Fund_Inception_Dte);                                                                          //Natural: MOVE NEC-FND-VIEW1.NEC-FUND-INCEPTION-DTE TO #W-INCEPTION-DTE
                pnd_Workfile_Pnd_W_Fund_Cusip.setValue(nec_Fnd_View1_Nec_Fund_Cusip);                                                                                     //Natural: MOVE NEC-FND-VIEW1.NEC-FUND-CUSIP TO #W-FUND-CUSIP
                pnd_Workfile_Pnd_W_Category_Class_Cde.setValue(nec_Fnd_View1_Nec_Category_Class_Cde);                                                                     //Natural: MOVE NEC-FND-VIEW1.NEC-CATEGORY-CLASS-CDE TO #W-CATEGORY-CLASS-CDE
                pnd_Workfile_Pnd_W_Category_Rptng_Seq.setValue(nec_Fnd_View1_Nec_Category_Rptng_Seq);                                                                     //Natural: MOVE NEC-FND-VIEW1.NEC-CATEGORY-RPTNG-SEQ TO #W-CATEGORY-RPTNG-SEQ
                pnd_Workfile_Pnd_W_Category_Fund_Seq.setValue(nec_Fnd_View1_Nec_Category_Fund_Seq);                                                                       //Natural: MOVE NEC-FND-VIEW1.NEC-CATEGORY-FUND-SEQ TO #W-CATEGORY-FUND-SEQ
                pnd_Workfile_Pnd_W_Alpha_Fund_Cde.setValue(nec_Fnd_View1_Nec_Alpha_Fund_Cde);                                                                             //Natural: MOVE NEC-FND-VIEW1.NEC-ALPHA-FUND-CDE TO #W-ALPHA-FUND-CDE
                pnd_Workfile_Pnd_W_Num_Fund_Cde.setValue(nec_Fnd_View1_Nec_Num_Fund_Cde);                                                                                 //Natural: MOVE NEC-FND-VIEW1.NEC-NUM-FUND-CDE TO #W-NUM-FUND-CDE
                pnd_Workfile_Pnd_W_Reporting_Seq.setValue(nec_Fnd_View1_Nec_Reporting_Seq);                                                                               //Natural: MOVE NEC-FND-VIEW1.NEC-REPORTING-SEQ TO #W-REPORTING-SEQ
                pnd_Workfile_Pnd_W_Alpha_Fund_Seq.setValue(nec_Fnd_View1_Nec_Alpha_Fund_Seq);                                                                             //Natural: MOVE NEC-FND-VIEW1.NEC-ALPHA-FUND-SEQ TO #W-ALPHA-FUND-SEQ
                pnd_Workfile_Pnd_W_Abbr_Factor_Key.setValue(nec_Fnd_View1_Nec_Abbr_Factor_Key);                                                                           //Natural: MOVE NEC-FND-VIEW1.NEC-ABBR-FACTOR-KEY TO #W-ABBR-FACTOR-KEY
                pnd_Workfile_Pnd_W_Act_Unit_Acct_Nm.setValue(nec_Fnd_View1_Nec_Act_Unit_Acct_Nm);                                                                         //Natural: MOVE NEC-FND-VIEW1.NEC-ACT-UNIT-ACCT-NM TO #W-ACT-UNIT-ACCT-NM
                pnd_Workfile_Pnd_W_Fund_Nme.setValue(nec_Fnd_View1_Nec_Fund_Nme);                                                                                         //Natural: MOVE NEC-FND-VIEW1.NEC-FUND-NME TO #W-FUND-NME
                //*  KCD1 MULTI CLASS, 3 NEW FIELDS
                //* **     WRITE 'KEN MOVING - ' NEC-FST-VIEW1.NEC-TICKER-SYMBOL
                //* **                           NEC-FND-VIEW1.NEC-INVESTMENT-GROUPING-ID
                pnd_Workfile_Pnd_W_Nec_Act_Class_Cde.setValue(nec_Fnd_View1_Nec_Act_Class_Cde);                                                                           //Natural: MOVE NEC-FND-VIEW1.NEC-ACT-CLASS-CDE TO #W-NEC-ACT-CLASS-CDE
                pnd_Workfile_Pnd_W_Nec_Act_Investment_Typ.setValue(nec_Fnd_View1_Nec_Act_Invesment_Typ);                                                                  //Natural: MOVE NEC-FND-VIEW1.NEC-ACT-INVESMENT-TYP TO #W-NEC-ACT-INVESTMENT-TYP
                pnd_Workfile_Pnd_W_Nec_Investment_Grouping_Id.setValue(nec_Fnd_View1_Nec_Investment_Grouping_Id);                                                         //Natural: MOVE NEC-FND-VIEW1.NEC-INVESTMENT-GROUPING-ID TO #W-NEC-INVESTMENT-GROUPING-ID
                //* *
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1.setValue("EXP");                                                                                              //Natural: MOVE 'EXP' TO #NEC-FND-TABLE-CDE-SUPER1
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                //Natural: MOVE NEC-FST-VIEW1.NEC-TICKER-SYMBOL TO #NEC-FND-TICKER-SYMBOL-SUPER1
            vw_nec_Exp_View1.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) NEC-EXP-VIEW1 WITH NEC-EXP-VIEW1.NEC-FND-SUPER1 = #NEC-FND-SUPER1
            (
            "FIND02",
            new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(vw_nec_Exp_View1.readNextRow("FIND02", true)))
            {
                vw_nec_Exp_View1.setIfNotFoundControlFlag(false);
                //* *        IF NO RECORD FOUND
                //* *           MOVE ' '   TO #W-FUND-FAMILY-CDE
                //* *                         #W-FUND-CUSIP         #W-CATEGORY-CLASS-CDE
                //* *                         #W-CATEGORY-RPTNG-SEQ #W-CATEGORY-FUND-SEQ
                //* *                         #W-REPORTING-SEQ      #W-ALPHA-FUND-SEQ
                //* *                         #W-ABBR-FACTOR-KEY
                //* *                         #W-FUND-NME
                //* *           MOVE 0     TO #W-INCEPTION-DTE      #W-NUM-FUND-CDE
                //* *                         #W-ALPHA-FUND-CDE     #W-ACT-UNIT-ACCT-NM
                if (condition(vw_nec_Exp_View1.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORD FOUND
                {
                    pnd_Workfile3_Nec_Ticker_Symbol.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                            //Natural: MOVE NEC-FST-VIEW1.NEC-TICKER-SYMBOL TO #WORKFILE3.NEC-TICKER-SYMBOL
                    getWorkFiles().write(3, false, pnd_Workfile3);                                                                                                        //Natural: WRITE WORK FILE 3 #WORKFILE3
                    pnd_Workfile3.reset();                                                                                                                                //Natural: RESET #WORKFILE3
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                //* **         WRITE 'RECORD FOUND' NEC-EXP-VIEW1.NEC-TICKER-SYMBOL
                //* **                              NEC-EXP-VIEW1.NEC-INVSTMNT-ADVSRY-EXP
                pnd_Workfile3.setValuesByName(vw_nec_Exp_View1);                                                                                                          //Natural: MOVE BY NAME NEC-EXP-VIEW1 TO #WORKFILE3
                getWorkFiles().write(3, false, pnd_Workfile3);                                                                                                            //Natural: WRITE WORK FILE 3 #WORKFILE3
                pnd_Workfile3.reset();                                                                                                                                    //Natural: RESET #WORKFILE3
                //*           /*  ADD EXPENSE TO THE EXISTING FNDINFO FILES  /* KCD FINRA
                //* *            MOVE NEC-EXP-VIEW1.NEC-MAX-UNDRLYNG-MF-EXP TO
                //* *                               #W-MAX-UNDRLYNG-MF-EXP
                //*  DECIDE ON FIRST VALUE OF NEC-EXP-VIEW1.NEC-TICKER-SYMBOL
                //* *VALUE 'W151#' , 'W251#' , 'W351#' , 'W451#'
                //* *          MOVE NEC-EXP-VIEW1.NEC-MAX-UNDRLYNG-MF-EXP TO
                //* *                              #W-MAX-UNDRLYNG-MF-EXP
                //* *VALUE 'CSTK#','CGLB#','CGRW#','CEQX#','CBND#','CILB#','CSCL#','CMMA#'
                //* *          MOVE NEC-EXP-VIEW1.NEC-CREF-AVG-EXP-CHRG   TO
                //* *                              #W-MAX-UNDRLYNG-MF-EXP
                //* *VALUE 'TREA#'
                //* ***        MOVE NEC-EXP-VIEW1.NEC-ACTUAL-EXPENSE-CHRG TO
                //* *          MOVE NEC-EXP-VIEW1.NEC-PUBL-MAX-EXP-CHRG   TO
                //* *                              #W-MAX-UNDRLYNG-MF-EXP
                //*  NONE VALUE
                //* *       IGNORE
                //* * END-DECIDE
                if (condition(nec_Fnd_View1_Nec_Ticker_Symbol.equals("W151#") || nec_Fnd_View1_Nec_Ticker_Symbol.equals("W251#") || nec_Fnd_View1_Nec_Ticker_Symbol.equals("W351#")  //Natural: IF NEC-FND-VIEW1.NEC-TICKER-SYMBOL = 'W151#' OR = 'W251#' OR = 'W351#' OR = 'W451#'
                    || nec_Fnd_View1_Nec_Ticker_Symbol.equals("W451#")))
                {
                    pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp.setValue(nec_Exp_View1_Nec_Max_Undrlyng_Mf_Exp);                                                               //Natural: MOVE NEC-EXP-VIEW1.NEC-MAX-UNDRLYNG-MF-EXP TO #W-MAX-UNDRLYNG-MF-EXP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(nec_Fnd_View1_Nec_Functional_Cmpny.equals("C")))                                                                                        //Natural: IF NEC-FND-VIEW1.NEC-FUNCTIONAL-CMPNY = 'C'
                    {
                        pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp.setValue(nec_Exp_View1_Nec_Cref_Avg_Exp_Chrg);                                                             //Natural: MOVE NEC-EXP-VIEW1.NEC-CREF-AVG-EXP-CHRG TO #W-MAX-UNDRLYNG-MF-EXP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(nec_Fnd_View1_Nec_Functional_Cmpny.equals("R")))                                                                                    //Natural: IF NEC-FND-VIEW1.NEC-FUNCTIONAL-CMPNY = 'R'
                        {
                            pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp.setValue(nec_Exp_View1_Nec_Publ_Max_Exp_Chrg);                                                         //Natural: MOVE NEC-EXP-VIEW1.NEC-PUBL-MAX-EXP-CHRG TO #W-MAX-UNDRLYNG-MF-EXP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ****************************
            pnd_Count_Funds.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #COUNT-FUNDS
            pnd_Workfile_Pnd_W_Ticker_Symbol.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                                   //Natural: MOVE NEC-FST-VIEW1.NEC-TICKER-SYMBOL TO #W-TICKER-SYMBOL
            if (condition(nec_Fst_View1_Nec_Ticker_Symbol.equals("TIAA#")))                                                                                               //Natural: IF NEC-FST-VIEW1.NEC-TICKER-SYMBOL EQ 'TIAA#'
            {
                pnd_Workfile_Pnd_W_Gra_Rb.setValue("00000000");                                                                                                           //Natural: MOVE '00000000' TO #W-GRA-RB
                pnd_Workfile_Pnd_W_Gsra_Rb.setValue("00000000");                                                                                                          //Natural: MOVE '00000000' TO #W-GSRA-RB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM FIND-GRA
                sub_Find_Gra();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM FIND-GSRA
                sub_Find_Gsra();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Workfile_Pnd_W_Gra_Rb.equals(" ")))                                                                                                         //Natural: IF #W-GRA-RB EQ ' '
            {
                pnd_Workfile_Pnd_W_Gra_Rb.setValue("00000000");                                                                                                           //Natural: MOVE '00000000' TO #W-GRA-RB
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Workfile_Pnd_W_Gsra_Rb.equals(" ")))                                                                                                        //Natural: IF #W-GSRA-RB EQ ' '
            {
                pnd_Workfile_Pnd_W_Gsra_Rb.setValue("00000000");                                                                                                          //Natural: MOVE '00000000' TO #W-GSRA-RB
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Workfile);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORKFILE
            //*  KCD FINRA
            pnd_Workfile_Pnd_W_Max_Undrlyng_Mf_Exp.reset();                                                                                                               //Natural: RESET #W-MAX-UNDRLYNG-MF-EXP
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-GRA
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-GSRA
        getReports().write(0, "=",pnd_Count_Funds);                                                                                                                       //Natural: WRITE '=' #COUNT-FUNDS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Count_Not_Active);                                                                                                                  //Natural: WRITE '=' #COUNT-NOT-ACTIVE
        if (Global.isEscape()) return;
    }
    private void sub_Find_Gra() throws Exception                                                                                                                          //Natural: FIND-GRA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1.setValue("RTE");                                                                                                  //Natural: MOVE 'RTE' TO #NEC-RTE-TABLE-CDE-SUPER1
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1.setValue("GRA");                                                                                                //Natural: MOVE 'GRA' TO #NEC-RTE-PRODUCT-CDE-SUPER1
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Ticker_Symbol_Super1.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                    //Natural: MOVE NEC-FST-VIEW1.NEC-TICKER-SYMBOL TO #NEC-RTE-TICKER-SYMBOL-SUPER1
        vw_nec_Rte_View1.startDatabaseRead                                                                                                                                //Natural: READ NEC-RTE-VIEW1 BY NEC-RTE-VIEW1.NEC-RTE-SUPER1 STARTING FROM #NEC-RTE-SUPER1
        (
        "READ02",
        new Wc[] { new Wc("NEC_RTE_SUPER1", ">=", pnd_Nec_Rte_Super1, WcType.BY) },
        new Oc[] { new Oc("NEC_RTE_SUPER1", "ASC") }
        );
        READ02:
        while (condition(vw_nec_Rte_View1.readNextRow("READ02")))
        {
            if (condition(nec_Rte_View1_Nec_Table_Cde.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1)))                                                        //Natural: IF NEC-RTE-VIEW1.NEC-TABLE-CDE NE #NEC-RTE-TABLE-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(nec_Rte_View1_Nec_Product_Cde.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1)))                                                    //Natural: IF NEC-RTE-VIEW1.NEC-PRODUCT-CDE NE #NEC-RTE-PRODUCT-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(nec_Rte_View1_Nec_Ticker_Symbol.notEquals(nec_Fst_View1_Nec_Ticker_Symbol)))                                                                    //Natural: IF NEC-RTE-VIEW1.NEC-TICKER-SYMBOL NE NEC-FST-VIEW1.NEC-TICKER-SYMBOL
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Workfile_Pnd_W_Gra_Rb.setValue(nec_Rte_View1_Nec_Rate_Cde);                                                                                               //Natural: MOVE NEC-RTE-VIEW1.NEC-RATE-CDE TO #W-GRA-RB
            pnd_Rb_Found.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #RB-FOUND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Rb_Found.equals(" ")))                                                                                                                          //Natural: IF #RB-FOUND EQ ' '
        {
            pnd_Workfile_Pnd_W_Gra_Rb.setValue("00000000");                                                                                                               //Natural: MOVE '00000000' TO #W-GRA-RB
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rb_Found.reset();                                                                                                                                             //Natural: RESET #RB-FOUND #NEC-RTE-PRODUCT-CDE-SUPER1
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1.reset();
    }
    private void sub_Find_Gsra() throws Exception                                                                                                                         //Natural: FIND-GSRA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1.setValue("GSRA");                                                                                               //Natural: MOVE 'GSRA' TO #NEC-RTE-PRODUCT-CDE-SUPER1
        vw_nec_Rte_View1.startDatabaseRead                                                                                                                                //Natural: READ NEC-RTE-VIEW1 BY NEC-RTE-VIEW1.NEC-RTE-SUPER1 STARTING FROM #NEC-RTE-SUPER1
        (
        "READ03",
        new Wc[] { new Wc("NEC_RTE_SUPER1", ">=", pnd_Nec_Rte_Super1, WcType.BY) },
        new Oc[] { new Oc("NEC_RTE_SUPER1", "ASC") }
        );
        READ03:
        while (condition(vw_nec_Rte_View1.readNextRow("READ03")))
        {
            if (condition(nec_Rte_View1_Nec_Table_Cde.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1)))                                                        //Natural: IF NEC-RTE-VIEW1.NEC-TABLE-CDE NE #NEC-RTE-TABLE-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(nec_Rte_View1_Nec_Product_Cde.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1)))                                                    //Natural: IF NEC-RTE-VIEW1.NEC-PRODUCT-CDE NE #NEC-RTE-PRODUCT-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(nec_Rte_View1_Nec_Ticker_Symbol.notEquals(nec_Fst_View1_Nec_Ticker_Symbol)))                                                                    //Natural: IF NEC-RTE-VIEW1.NEC-TICKER-SYMBOL NE NEC-FST-VIEW1.NEC-TICKER-SYMBOL
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Workfile_Pnd_W_Gsra_Rb.setValue(nec_Rte_View1_Nec_Rate_Cde);                                                                                              //Natural: MOVE NEC-RTE-VIEW1.NEC-RATE-CDE TO #W-GSRA-RB
            pnd_Rb_Found.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #RB-FOUND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Rb_Found.equals(" ")))                                                                                                                          //Natural: IF #RB-FOUND EQ ' '
        {
            pnd_Workfile_Pnd_W_Gsra_Rb.setValue("00000000");                                                                                                              //Natural: MOVE '00000000' TO #W-GSRA-RB
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rb_Found.reset();                                                                                                                                             //Natural: RESET #RB-FOUND #NEC-RTE-PRODUCT-CDE-SUPER1
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Product_Cde_Super1.reset();
    }

    //
}
