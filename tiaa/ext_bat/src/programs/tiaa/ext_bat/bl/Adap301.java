/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:29 PM
**        * FROM NATURAL PROGRAM : Adap301
************************************************************
**        * FILE NAME            : Adap301.java
**        * CLASS NAME           : Adap301
**        * INSTANCE NAME        : Adap301
************************************************************
* PROGRAM     :  ADAP301                                             *
*                                                                    *
* DESCRIPTION :  EXTRACT RATES FROM  NEW-EXT-CNTRL-RTE FILE          *
*                                                                    *
* FILE ACCESS :  READ ONLY = NEW-EXT-CNTRL-FST & NEW-EXT-CNTRL-RTE
*                                                                    *
* MAINTENANCE :                                                      *
* KCD (C.DIMITRIU): 08/15/2009     FINRA - EXPENSE FOR ILLUSTRATIONS *
* KCD1(C.DIMITRIU): 12/2013        CREF MULTICLASS PROJECT
*                   INTRODUCED "FST" FILE,  NEW-EXT-CNTRL-FST,
*                  (TO ELIMINATE RECS WITH NEC-FUND-STATUS NE 'A',
*                                           CSTK#,CBND#,CMMA#....)
*                   THIS KEEPS THE CSTK#,CBND#,CMMA#... REC'S ON ADABAS
*                   FILE, BUT THEY ARE ELMINATED FROM PCS0300D - EXTRACT
*                                      (.....EXTERNAL.RATES.INFO)

************************************************************ */

package tiaa.ext_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adap301 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_nec_Rte_View1;
    private DbsField nec_Rte_View1_Nec_Table_Cde;
    private DbsField nec_Rte_View1_Nec_Product_Cde;
    private DbsField nec_Rte_View1_Nec_Ticker_Symbol;
    private DbsField nec_Rte_View1_Nec_Rate_Cde;
    private DbsField nec_Rte_View1_Nec_Interest_Rate;
    private DbsField nec_Rte_View1_Nec_Effective_Dte;
    private DbsField nec_Rte_View1_Nec_Inception_Dte;
    private DbsField nec_Rte_View1_Nec_Nbr_Rates;
    private DbsField nec_Rte_View1_Nec_Acct_Product_Cde;
    private DbsField pnd_Nec_Rte_Super1;

    private DbsGroup pnd_Nec_Rte_Super1__R_Field_1;
    private DbsField pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1;

    private DataAccessProgramView vw_nec_Fst_View1;
    private DbsField nec_Fst_View1_Nec_Table_Cde;
    private DbsField nec_Fst_View1_Nec_Ticker_Symbol;
    private DbsField nec_Fst_View1_Nec_Fund_Status;
    private DbsField pnd_Nec_Fst_Super2;

    private DbsGroup pnd_Nec_Fst_Super2__R_Field_2;
    private DbsField pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Table;
    private DbsField pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Ticker_Symbol;

    private DbsGroup pnd_Workfile_1;
    private DbsField pnd_Workfile_1_Pnd_W_Product_Cde_1;
    private DbsField pnd_Workfile_1_Pnd_W_Ticker_Symbol_1;
    private DbsField pnd_Workfile_1_Pnd_W_Rate_Cde_1;
    private DbsField pnd_Workfile_1_Pnd_W_Inception_Dte_1;
    private DbsField pnd_Count_Not_Active;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_nec_Rte_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Rte_View1", "NEC-RTE-VIEW1"), "NEW_EXT_CNTRL_RTE", "NEW_EXT_CNTRL");
        nec_Rte_View1_Nec_Table_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Rte_View1_Nec_Product_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Product_Cde", "NEC-PRODUCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_PRODUCT_CDE");
        nec_Rte_View1_Nec_Product_Cde.setDdmHeader("PRODUCT CODE");
        nec_Rte_View1_Nec_Ticker_Symbol = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Rte_View1_Nec_Rate_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Rate_Cde", "NEC-RATE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NEC_RATE_CDE");
        nec_Rte_View1_Nec_Interest_Rate = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Interest_Rate", "NEC-INTEREST-RATE", FieldType.NUMERIC, 
            7, 2, RepeatingFieldStrategy.None, "NEC_INTEREST_RATE");
        nec_Rte_View1_Nec_Effective_Dte = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Effective_Dte", "NEC-EFFECTIVE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "NEC_EFFECTIVE_DTE");
        nec_Rte_View1_Nec_Inception_Dte = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Inception_Dte", "NEC-INCEPTION-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "NEC_INCEPTION_DTE");
        nec_Rte_View1_Nec_Nbr_Rates = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Nbr_Rates", "NEC-NBR-RATES", FieldType.PACKED_DECIMAL, 
            3, RepeatingFieldStrategy.None, "NEC_NBR_RATES");
        nec_Rte_View1_Nec_Acct_Product_Cde = vw_nec_Rte_View1.getRecord().newFieldInGroup("nec_Rte_View1_Nec_Acct_Product_Cde", "NEC-ACCT-PRODUCT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_ACCT_PRODUCT_CDE");
        registerRecord(vw_nec_Rte_View1);

        pnd_Nec_Rte_Super1 = localVariables.newFieldInRecord("pnd_Nec_Rte_Super1", "#NEC-RTE-SUPER1", FieldType.STRING, 31);

        pnd_Nec_Rte_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Rte_Super1__R_Field_1", "REDEFINE", pnd_Nec_Rte_Super1);
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1 = pnd_Nec_Rte_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1", 
            "#NEC-RTE-TABLE-CDE-SUPER1", FieldType.STRING, 3);

        vw_nec_Fst_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Fst_View1", "NEC-FST-VIEW1"), "NEW_EXT_CNTRL_FST", "NEW_EXT_CNTRL");
        nec_Fst_View1_Nec_Table_Cde = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Fst_View1_Nec_Ticker_Symbol = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Fst_View1_Nec_Fund_Status = vw_nec_Fst_View1.getRecord().newFieldInGroup("nec_Fst_View1_Nec_Fund_Status", "NEC-FUND-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_FUND_STATUS");
        registerRecord(vw_nec_Fst_View1);

        pnd_Nec_Fst_Super2 = localVariables.newFieldInRecord("pnd_Nec_Fst_Super2", "#NEC-FST-SUPER2", FieldType.STRING, 13);

        pnd_Nec_Fst_Super2__R_Field_2 = localVariables.newGroupInRecord("pnd_Nec_Fst_Super2__R_Field_2", "REDEFINE", pnd_Nec_Fst_Super2);
        pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Table = pnd_Nec_Fst_Super2__R_Field_2.newFieldInGroup("pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Table", "#NEC-FST-TABLE", 
            FieldType.STRING, 3);
        pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Ticker_Symbol = pnd_Nec_Fst_Super2__R_Field_2.newFieldInGroup("pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Ticker_Symbol", "#NEC-FST-TICKER-SYMBOL", 
            FieldType.STRING, 10);

        pnd_Workfile_1 = localVariables.newGroupInRecord("pnd_Workfile_1", "#WORKFILE-1");
        pnd_Workfile_1_Pnd_W_Product_Cde_1 = pnd_Workfile_1.newFieldInGroup("pnd_Workfile_1_Pnd_W_Product_Cde_1", "#W-PRODUCT-CDE-1", FieldType.STRING, 
            10);
        pnd_Workfile_1_Pnd_W_Ticker_Symbol_1 = pnd_Workfile_1.newFieldInGroup("pnd_Workfile_1_Pnd_W_Ticker_Symbol_1", "#W-TICKER-SYMBOL-1", FieldType.STRING, 
            10);
        pnd_Workfile_1_Pnd_W_Rate_Cde_1 = pnd_Workfile_1.newFieldInGroup("pnd_Workfile_1_Pnd_W_Rate_Cde_1", "#W-RATE-CDE-1", FieldType.STRING, 8);
        pnd_Workfile_1_Pnd_W_Inception_Dte_1 = pnd_Workfile_1.newFieldInGroup("pnd_Workfile_1_Pnd_W_Inception_Dte_1", "#W-INCEPTION-DTE-1", FieldType.NUMERIC, 
            8);
        pnd_Count_Not_Active = localVariables.newFieldInRecord("pnd_Count_Not_Active", "#COUNT-NOT-ACTIVE", FieldType.PACKED_DECIMAL, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_nec_Rte_View1.reset();
        vw_nec_Fst_View1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adap301() throws Exception
    {
        super("Adap301");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1.setValue("RTE");                                                                                                  //Natural: MOVE 'RTE' TO #NEC-RTE-TABLE-CDE-SUPER1
        vw_nec_Rte_View1.startDatabaseRead                                                                                                                                //Natural: READ NEC-RTE-VIEW1 BY NEC-RTE-SUPER1 STARTING FROM #NEC-RTE-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("NEC_RTE_SUPER1", ">=", pnd_Nec_Rte_Super1, WcType.BY) },
        new Oc[] { new Oc("NEC_RTE_SUPER1", "ASC") }
        );
        READ01:
        while (condition(vw_nec_Rte_View1.readNextRow("READ01")))
        {
            if (condition(nec_Rte_View1_Nec_Table_Cde.notEquals(pnd_Nec_Rte_Super1_Pnd_Nec_Rte_Table_Cde_Super1)))                                                        //Natural: IF NEC-TABLE-CDE NE #NEC-RTE-TABLE-CDE-SUPER1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  KCD1
            pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Table.setValue("FST");                                                                                                         //Natural: MOVE 'FST' TO #NEC-FST-TABLE
            pnd_Nec_Fst_Super2_Pnd_Nec_Fst_Ticker_Symbol.setValue(nec_Rte_View1_Nec_Ticker_Symbol);                                                                       //Natural: MOVE NEC-RTE-VIEW1.NEC-TICKER-SYMBOL TO #NEC-FST-TICKER-SYMBOL
            vw_nec_Fst_View1.startDatabaseFind                                                                                                                            //Natural: FIND NEC-FST-VIEW1 WITH NEC-FST-SUPER2 = #NEC-FST-SUPER2
            (
            "FIND01",
            new Wc[] { new Wc("NEC_FST_SUPER2", "=", pnd_Nec_Fst_Super2, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_nec_Fst_View1.readNextRow("FIND01")))
            {
                vw_nec_Fst_View1.setIfNotFoundControlFlag(false);
                if (condition(nec_Fst_View1_Nec_Fund_Status.notEquals("A")))                                                                                              //Natural: IF NEC-FST-VIEW1.NEC-FUND-STATUS NE 'A'
                {
                    getReports().write(0, "=",nec_Fst_View1_Nec_Ticker_Symbol,"  NOT ACTIVE");                                                                            //Natural: WRITE '=' NEC-FST-VIEW1.NEC-TICKER-SYMBOL '  NOT ACTIVE'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Count_Not_Active.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COUNT-NOT-ACTIVE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Workfile_1_Pnd_W_Product_Cde_1.setValue(nec_Rte_View1_Nec_Product_Cde);                                                                           //Natural: MOVE NEC-PRODUCT-CDE TO #W-PRODUCT-CDE-1
                    pnd_Workfile_1_Pnd_W_Ticker_Symbol_1.setValue(nec_Fst_View1_Nec_Ticker_Symbol);                                                                       //Natural: MOVE NEC-TICKER-SYMBOL TO #W-TICKER-SYMBOL-1
                    pnd_Workfile_1_Pnd_W_Rate_Cde_1.setValue(nec_Rte_View1_Nec_Rate_Cde);                                                                                 //Natural: MOVE NEC-RATE-CDE TO #W-RATE-CDE-1
                    pnd_Workfile_1_Pnd_W_Inception_Dte_1.setValue(nec_Rte_View1_Nec_Inception_Dte);                                                                       //Natural: MOVE NEC-INCEPTION-DTE TO #W-INCEPTION-DTE-1
                    getWorkFiles().write(1, false, pnd_Workfile_1);                                                                                                       //Natural: WRITE WORK FILE 1 #WORKFILE-1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Count_Not_Active);                                                                                                                  //Natural: WRITE '=' #COUNT-NOT-ACTIVE
        if (Global.isEscape()) return;
    }

    //
}
