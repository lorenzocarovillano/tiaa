/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:37 PM
**        *   FROM NATURAL MAP   :  Iaam382a
************************************************************
**        * FILE NAME               : Iaam382a.java
**        * CLASS NAME              : Iaam382a
**        * INSTANCE NAME           : Iaam382a
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     IAA-DEDUCTION-VIEW.DDCTN-CDE IAA-DEDUCTION-VIEW.DDCTN-FINAL-DTE                                                          *     IAA-DEDUCTION-VIEW.DDCTN-ID-NBR 
    *     IAA-DEDUCTION-VIEW.DDCTN-INTENT-CDE                                                                                      *     IAA-DEDUCTION-VIEW.DDCTN-PAYEE 
    IAA-DEDUCTION-VIEW.DDCTN-PAYEE-CDE                                                        *     IAA-DEDUCTION-VIEW.DDCTN-PD-TO-DTE                  
    *     IAA-DEDUCTION-VIEW.DDCTN-PER-AMT                                                                                         *     IAA-DEDUCTION-VIEW.DDCTN-PPCN-NBR 
    *     IAA-DEDUCTION-VIEW.DDCTN-SEQ-NBR IAA-DEDUCTION-VIEW.DDCTN-STP-DTE                                                        *     IAA-DEDUCTION-VIEW.DDCTN-STRT-DTE 
    *     IAA-DEDUCTION-VIEW.DDCTN-TOT-AMT IAA-DEDUCTION-VIEW.DDCTN-YTD-AMT                                                        *     IAA-DEDUCTION-VIEW.LST-TRANS-DTE
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam382a extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField iaa_Deduction_View_Ddctn_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Final_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Payee;
    private DbsField iaa_Deduction_View_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_View_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_View_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_View_Lst_Trans_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        iaa_Deduction_View_Ddctn_Cde = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Cde", "IAA-DEDUCTION-VIEW.DDCTN-CDE", FieldType.STRING, 3);
        iaa_Deduction_View_Ddctn_Final_Dte = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Final_Dte", "IAA-DEDUCTION-VIEW.DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8);
        iaa_Deduction_View_Ddctn_Id_Nbr = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Id_Nbr", "IAA-DEDUCTION-VIEW.DDCTN-ID-NBR", FieldType.NUMERIC, 
            12);
        iaa_Deduction_View_Ddctn_Intent_Cde = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Intent_Cde", "IAA-DEDUCTION-VIEW.DDCTN-INTENT-CDE", 
            FieldType.NUMERIC, 1);
        iaa_Deduction_View_Ddctn_Payee = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Payee", "IAA-DEDUCTION-VIEW.DDCTN-PAYEE", FieldType.STRING, 
            5);
        iaa_Deduction_View_Ddctn_Payee_Cde = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Payee_Cde", "IAA-DEDUCTION-VIEW.DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        iaa_Deduction_View_Ddctn_Pd_To_Dte = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Pd_To_Dte", "IAA-DEDUCTION-VIEW.DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            10, 2);
        iaa_Deduction_View_Ddctn_Per_Amt = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Per_Amt", "IAA-DEDUCTION-VIEW.DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        iaa_Deduction_View_Ddctn_Ppcn_Nbr = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Ppcn_Nbr", "IAA-DEDUCTION-VIEW.DDCTN-PPCN-NBR", FieldType.STRING, 
            10);
        iaa_Deduction_View_Ddctn_Seq_Nbr = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Seq_Nbr", "IAA-DEDUCTION-VIEW.DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Deduction_View_Ddctn_Stp_Dte = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Stp_Dte", "IAA-DEDUCTION-VIEW.DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        iaa_Deduction_View_Ddctn_Strt_Dte = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Strt_Dte", "IAA-DEDUCTION-VIEW.DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        iaa_Deduction_View_Ddctn_Tot_Amt = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Tot_Amt", "IAA-DEDUCTION-VIEW.DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        iaa_Deduction_View_Ddctn_Ytd_Amt = parameters.newFieldInRecord("iaa_Deduction_View_Ddctn_Ytd_Amt", "IAA-DEDUCTION-VIEW.DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        iaa_Deduction_View_Lst_Trans_Dte = parameters.newFieldInRecord("iaa_Deduction_View_Lst_Trans_Dte", "IAA-DEDUCTION-VIEW.LST-TRANS-DTE", FieldType.TIME);
        parameters.reset();
    }

    public Iaam382a() throws Exception
    {
        super("Iaam382a");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam382a", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam382a"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("iaa_Deduction_View_Lst_Trans_Dte", iaa_Deduction_View_Lst_Trans_Dte, true, 1, 2, 0, "WHITE", true, true, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Ppcn_Nbr", iaa_Deduction_View_Ddctn_Ppcn_Nbr, true, 1, 12, 10, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Payee_Cde", iaa_Deduction_View_Ddctn_Payee_Cde, true, 1, 24, 2, "WHITE", "Z9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Cde", iaa_Deduction_View_Ddctn_Cde, true, 1, 37, 3, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Seq_Nbr", iaa_Deduction_View_Ddctn_Seq_Nbr, true, 1, 42, 3, "WHITE", "ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Payee", iaa_Deduction_View_Ddctn_Payee, true, 1, 47, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Per_Amt", iaa_Deduction_View_Ddctn_Per_Amt, true, 1, 54, 8, "WHITE", "ZZZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Ytd_Amt", iaa_Deduction_View_Ddctn_Ytd_Amt, true, 1, 64, 10, "WHITE", "ZZZZZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Pd_To_Dte", iaa_Deduction_View_Ddctn_Pd_To_Dte, true, 1, 76, 10, "WHITE", "ZZZZZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Tot_Amt", iaa_Deduction_View_Ddctn_Tot_Amt, true, 1, 88, 10, "WHITE", "ZZZZZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Intent_Cde", iaa_Deduction_View_Ddctn_Intent_Cde, true, 1, 100, 1, "WHITE", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Strt_Dte", iaa_Deduction_View_Ddctn_Strt_Dte, true, 1, 103, 8, "WHITE", "99999999", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Stp_Dte", iaa_Deduction_View_Ddctn_Stp_Dte, true, 1, 113, 8, "WHITE", "99999999", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Deduction_View_Ddctn_Final_Dte", iaa_Deduction_View_Ddctn_Final_Dte, true, 1, 123, 8, "WHITE", "99999999", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
