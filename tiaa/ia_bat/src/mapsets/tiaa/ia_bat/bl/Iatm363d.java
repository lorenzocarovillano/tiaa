/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:20 PM
**        *   FROM NATURAL MAP   :  Iatm363d
************************************************************
**        * FILE NAME               : Iatm363d.java
**        * CLASS NAME              : Iatm363d
**        * INSTANCE NAME           : Iatm363d
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M361D-EFF-DTE-OT #M361D-FRM-CNTRCT #M361D-FRM-FND                                                                       *     #M361D-FRM-QUNTY 
    #M361D-FRM-TYP #M361D-PRTCPNT-NME                                                                       *     #M361D-RCVD-DTE-OT #M361D-STATUS #M361D-TO-CNTRCT 
    #M361D-TO-FND                                                          *     #M361D-TO-QUNTY #M361D-TO-TYP #M361D-UNIQUE-ID #M361D-UNT-TYP          
    *     #M361D-XFR-TYPE
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm363d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M361d_Eff_Dte_Ot;
    private DbsField pnd_M361d_Frm_Cntrct;
    private DbsField pnd_M361d_Frm_Fnd;
    private DbsField pnd_M361d_Frm_Qunty;
    private DbsField pnd_M361d_Frm_Typ;
    private DbsField pnd_M361d_Prtcpnt_Nme;
    private DbsField pnd_M361d_Rcvd_Dte_Ot;
    private DbsField pnd_M361d_Status;
    private DbsField pnd_M361d_To_Cntrct;
    private DbsField pnd_M361d_To_Fnd;
    private DbsField pnd_M361d_To_Qunty;
    private DbsField pnd_M361d_To_Typ;
    private DbsField pnd_M361d_Unique_Id;
    private DbsField pnd_M361d_Unt_Typ;
    private DbsField pnd_M361d_Xfr_Type;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M361d_Eff_Dte_Ot = parameters.newFieldInRecord("pnd_M361d_Eff_Dte_Ot", "#M361D-EFF-DTE-OT", FieldType.STRING, 10);
        pnd_M361d_Frm_Cntrct = parameters.newFieldInRecord("pnd_M361d_Frm_Cntrct", "#M361D-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_M361d_Frm_Fnd = parameters.newFieldInRecord("pnd_M361d_Frm_Fnd", "#M361D-FRM-FND", FieldType.STRING, 1);
        pnd_M361d_Frm_Qunty = parameters.newFieldInRecord("pnd_M361d_Frm_Qunty", "#M361D-FRM-QUNTY", FieldType.NUMERIC, 8, 2);
        pnd_M361d_Frm_Typ = parameters.newFieldInRecord("pnd_M361d_Frm_Typ", "#M361D-FRM-TYP", FieldType.STRING, 1);
        pnd_M361d_Prtcpnt_Nme = parameters.newFieldInRecord("pnd_M361d_Prtcpnt_Nme", "#M361D-PRTCPNT-NME", FieldType.STRING, 30);
        pnd_M361d_Rcvd_Dte_Ot = parameters.newFieldInRecord("pnd_M361d_Rcvd_Dte_Ot", "#M361D-RCVD-DTE-OT", FieldType.STRING, 10);
        pnd_M361d_Status = parameters.newFieldInRecord("pnd_M361d_Status", "#M361D-STATUS", FieldType.STRING, 4);
        pnd_M361d_To_Cntrct = parameters.newFieldInRecord("pnd_M361d_To_Cntrct", "#M361D-TO-CNTRCT", FieldType.STRING, 10);
        pnd_M361d_To_Fnd = parameters.newFieldInRecord("pnd_M361d_To_Fnd", "#M361D-TO-FND", FieldType.STRING, 1);
        pnd_M361d_To_Qunty = parameters.newFieldInRecord("pnd_M361d_To_Qunty", "#M361D-TO-QUNTY", FieldType.NUMERIC, 8, 2);
        pnd_M361d_To_Typ = parameters.newFieldInRecord("pnd_M361d_To_Typ", "#M361D-TO-TYP", FieldType.STRING, 1);
        pnd_M361d_Unique_Id = parameters.newFieldInRecord("pnd_M361d_Unique_Id", "#M361D-UNIQUE-ID", FieldType.STRING, 12);
        pnd_M361d_Unt_Typ = parameters.newFieldInRecord("pnd_M361d_Unt_Typ", "#M361D-UNT-TYP", FieldType.STRING, 1);
        pnd_M361d_Xfr_Type = parameters.newFieldInRecord("pnd_M361d_Xfr_Type", "#M361D-XFR-TYPE", FieldType.STRING, 1);
        parameters.reset();
    }

    public Iatm363d() throws Exception
    {
        super("Iatm363d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=022 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm363d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm363d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_M361d_Unique_Id", pnd_M361d_Unique_Id, true, 1, 1, 12, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Frm_Cntrct", pnd_M361d_Frm_Cntrct, true, 1, 14, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_To_Cntrct", pnd_M361d_To_Cntrct, true, 1, 25, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Rcvd_Dte_Ot", pnd_M361d_Rcvd_Dte_Ot, true, 1, 36, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M361d_Eff_Dte_Ot", pnd_M361d_Eff_Dte_Ot, true, 1, 47, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Xfr_Type", pnd_M361d_Xfr_Type, true, 1, 58, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Frm_Fnd", pnd_M361d_Frm_Fnd, true, 1, 61, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_To_Fnd", pnd_M361d_To_Fnd, true, 1, 65, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Frm_Qunty", pnd_M361d_Frm_Qunty, true, 1, 68, 9, "BLUE", "ZZ,ZZZ.99", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Frm_Typ", pnd_M361d_Frm_Typ, true, 1, 79, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_To_Qunty", pnd_M361d_To_Qunty, true, 1, 82, 9, "BLUE", "ZZ,ZZZ.99", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M361d_Unt_Typ", pnd_M361d_Unt_Typ, true, 1, 93, 1, "BLUE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_To_Typ", pnd_M361d_To_Typ, true, 1, 97, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M361d_Prtcpnt_Nme", pnd_M361d_Prtcpnt_Nme, true, 1, 99, 29, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M361d_Status", pnd_M361d_Status, true, 1, 129, 4, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
