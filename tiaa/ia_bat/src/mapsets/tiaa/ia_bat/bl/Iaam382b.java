/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:38 PM
**        *   FROM NATURAL MAP   :  Iaam382b
************************************************************
**        * FILE NAME               : Iaam382b.java
**        * CLASS NAME              : Iaam382b
**        * INSTANCE NAME           : Iaam382b
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    * MAP2: MAP PROFILES *****************************        200***********                                                       * .TTO      OOO1II RE 
    NE6IBL ?_      +()                    *                                                       * 060132        N1NYUCN____        X         01 SYSDBA 
    YL             *                                                       ************************************************************************     
    *   *.01S010 D    .                                                                     *.01S008 T    .                                             
    *.01S005 P05.0.                                                                      * MAP2: VALIDATION ***************************************************** 
    * MAP2: END OF MAP *****************************************************
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam382b extends BLNatBase
{
    // from LocalVariables/Parameters

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				
    }

    public Iaam382b() throws Exception
    {
        super("Iaam382b");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam382b", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam382b"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PROGRAM: IAAP382", "BLUE", 1, 1, 16);
            uiForm.setUiLabel("label_2", "IAA SYSTEM", "BLUE", 1, 60, 10);
            uiForm.setUiLabel("label_3", "DATE:", "BLUE", 1, 114, 5);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 120, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "DEDUCTION RECORDS RESET REPORT", "BLUE", 2, 49, 30);
            uiForm.setUiLabel("label_5", "TIME:", "BLUE", 2, 114, 5);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 2, 120, 8, "WHITE", "HH.MM.SS", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "PAGE:", "BLUE", 3, 114, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 3, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
