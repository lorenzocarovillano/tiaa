/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:39 PM
**        *   FROM NATURAL MAP   :  Iaam384
************************************************************
**        * FILE NAME               : Iaam384.java
**        * CLASS NAME              : Iaam384
**        * INSTANCE NAME           : Iaam384
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TOTAL-ACTIVE #TOTAL-ACTIVE-DIV-AMT #TOTAL-ACTIVE-PER-AMT                                                                *     #TOTAL-BYPASSED-ACTIVE 
    #TOTAL-BYPASSED-ACTIVE-DIV-AMT                                                                    *     #TOTAL-BYPASSED-ACTIVE-PER-AMT #TOTAL-READ #TOTAL-WRITTEN 
    *     #TOTAL-WRITTEN-CREF
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam384 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Total_Active;
    private DbsField pnd_Total_Active_Div_Amt;
    private DbsField pnd_Total_Active_Per_Amt;
    private DbsField pnd_Total_Bypassed_Active;
    private DbsField pnd_Total_Bypassed_Active_Div_Amt;
    private DbsField pnd_Total_Bypassed_Active_Per_Amt;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Written;
    private DbsField pnd_Total_Written_Cref;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Total_Active = parameters.newFieldInRecord("pnd_Total_Active", "#TOTAL-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total_Active_Div_Amt = parameters.newFieldInRecord("pnd_Total_Active_Div_Amt", "#TOTAL-ACTIVE-DIV-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Total_Active_Per_Amt = parameters.newFieldInRecord("pnd_Total_Active_Per_Amt", "#TOTAL-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Total_Bypassed_Active = parameters.newFieldInRecord("pnd_Total_Bypassed_Active", "#TOTAL-BYPASSED-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total_Bypassed_Active_Div_Amt = parameters.newFieldInRecord("pnd_Total_Bypassed_Active_Div_Amt", "#TOTAL-BYPASSED-ACTIVE-DIV-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Total_Bypassed_Active_Per_Amt = parameters.newFieldInRecord("pnd_Total_Bypassed_Active_Per_Amt", "#TOTAL-BYPASSED-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Total_Read = parameters.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.INTEGER, 4);
        pnd_Total_Written = parameters.newFieldInRecord("pnd_Total_Written", "#TOTAL-WRITTEN", FieldType.INTEGER, 4);
        pnd_Total_Written_Cref = parameters.newFieldInRecord("pnd_Total_Written_Cref", "#TOTAL-WRITTEN-CREF", FieldType.INTEGER, 4);
        parameters.reset();
    }

    public Iaam384() throws Exception
    {
        super("Iaam384");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam384", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam384"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PROGRAM: IAAP384", "BLUE", 1, 1, 16);
            uiForm.setUiLabel("label_2", "IAA SYSTEM", "BLUE", 1, 60, 10);
            uiForm.setUiLabel("label_3", "DATE:", "BLUE", 1, 114, 5);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 120, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "GRAND TOTAL REPORT", "BLUE", 2, 57, 18);
            uiForm.setUiLabel("label_5", "TIME:", "BLUE", 2, 114, 5);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 2, 120, 8, "WHITE", "HH.MM.SS", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "PAGE:", "BLUE", 3, 114, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 3, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "TOTAL READ RECORDS", "BLUE", 8, 1, 18);
            uiForm.setUiLabel("label_8", ":", "BLUE", 8, 33, 1);
            uiForm.setUiControl("pnd_Total_Read", pnd_Total_Read, true, 8, 38, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "TOTAL BYPASSED ACTIVE RECORDS", "BLUE", 10, 1, 29);
            uiForm.setUiLabel("label_10", ":", "BLUE", 10, 33, 1);
            uiForm.setUiControl("pnd_Total_Bypassed_Active", pnd_Total_Bypassed_Active, true, 10, 38, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "TOTAL BYPASSED ACTIVE PER AMOUNT:", "BLUE", 12, 1, 33);
            uiForm.setUiControl("pnd_Total_Bypassed_Active_Per_Amt", pnd_Total_Bypassed_Active_Per_Amt, true, 12, 36, 19, "WHITE", "' '$ZZ,ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "TOTAL BYPASSED ACTIVE DIV AMOUNT:", "BLUE", 14, 1, 33);
            uiForm.setUiControl("pnd_Total_Bypassed_Active_Div_Amt", pnd_Total_Bypassed_Active_Div_Amt, true, 14, 36, 19, "WHITE", "' '$ZZ,ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "TOTAL ACTIVE RECORDS", "BLUE", 16, 1, 20);
            uiForm.setUiLabel("label_14", ":", "BLUE", 16, 33, 1);
            uiForm.setUiControl("pnd_Total_Active", pnd_Total_Active, true, 16, 38, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "TOTAL ACTIVE PER AMOUNT", "BLUE", 18, 1, 23);
            uiForm.setUiLabel("label_16", ":", "BLUE", 18, 33, 1);
            uiForm.setUiControl("pnd_Total_Active_Per_Amt", pnd_Total_Active_Per_Amt, true, 18, 36, 19, "WHITE", "' '$ZZ,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "TOTAL ACTIVE DIV AMOUNT", "BLUE", 20, 1, 23);
            uiForm.setUiLabel("label_18", ":", "BLUE", 20, 33, 1);
            uiForm.setUiControl("pnd_Total_Active_Div_Amt", pnd_Total_Active_Div_Amt, true, 20, 36, 19, "WHITE", "' '$ZZ,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "TOTAL WRITTEN RECORDS:", "BLUE", 22, 1, 22);
            uiForm.setUiLabel("label_20", ":", "BLUE", 22, 33, 1);
            uiForm.setUiControl("pnd_Total_Written", pnd_Total_Written, true, 22, 38, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "TOTAL WRITTEN CREF RECORDS", "BLUE", 24, 1, 26);
            uiForm.setUiLabel("label_22", ":", "BLUE", 24, 33, 1);
            uiForm.setUiControl("pnd_Total_Written_Cref", pnd_Total_Written_Cref, true, 24, 38, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
