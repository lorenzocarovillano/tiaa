/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:21 PM
**        *   FROM NATURAL MAP   :  Iatm363t
************************************************************
**        * FILE NAME               : Iatm363t.java
**        * CLASS NAME              : Iatm363t
**        * INSTANCE NAME           : Iatm363t
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #M362T-ANUL-MTHD-AMT #M362T-CNTCT-AT-AMT #M362T-CNTCT-CS-AMT                                                             *     #M362T-CNTCT-FX-AMT 
    #M362T-CNTCT-IG-AMT #M362T-CNTCT-IN-AMT                                                              *     #M362T-CNTCT-ML-AMT #M362T-CNTCT-VR-AMT #M362T-CNTCT-VS-AMT 
    *     #M362T-M-T-M-ACCT-AMT #M362T-M-T-O-ACCT-AMT #M362T-MNTH-MTHD-AMT                                                         *     #M362T-O-T-M-ACCT-AMT 
    #M362T-O-T-O-ACCT-AMT #M362T-REPT-RQSTS                                                            *     #M362T-SINGLE-RQSTS #M362T-STAT-AF-AMT #M362T-STAT-DA-AMT 
    *     #M362T-STAT-PA-AMT #M362T-STAT-RJ-AMT #M362T-STAT-SO-AMT                                                                 *     #M362T-TOTL-RQST-AMT 
    #M362T-TTL-PRTCPNTS-AMT                                                                             *     #M362T-TTL-RQST-PCNT-AMT #M362T-TTL-RQSTS-DLRS-AMT 
    *     #M362T-TTL-RQSTS-UNTS-AMT
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm363t extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M362t_Anul_Mthd_Amt;
    private DbsField pnd_M362t_Cntct_At_Amt;
    private DbsField pnd_M362t_Cntct_Cs_Amt;
    private DbsField pnd_M362t_Cntct_Fx_Amt;
    private DbsField pnd_M362t_Cntct_Ig_Amt;
    private DbsField pnd_M362t_Cntct_In_Amt;
    private DbsField pnd_M362t_Cntct_Ml_Amt;
    private DbsField pnd_M362t_Cntct_Vr_Amt;
    private DbsField pnd_M362t_Cntct_Vs_Amt;
    private DbsField pnd_M362t_M_T_M_Acct_Amt;
    private DbsField pnd_M362t_M_T_O_Acct_Amt;
    private DbsField pnd_M362t_Mnth_Mthd_Amt;
    private DbsField pnd_M362t_O_T_M_Acct_Amt;
    private DbsField pnd_M362t_O_T_O_Acct_Amt;
    private DbsField pnd_M362t_Rept_Rqsts;
    private DbsField pnd_M362t_Single_Rqsts;
    private DbsField pnd_M362t_Stat_Af_Amt;
    private DbsField pnd_M362t_Stat_Da_Amt;
    private DbsField pnd_M362t_Stat_Pa_Amt;
    private DbsField pnd_M362t_Stat_Rj_Amt;
    private DbsField pnd_M362t_Stat_So_Amt;
    private DbsField pnd_M362t_Totl_Rqst_Amt;
    private DbsField pnd_M362t_Ttl_Prtcpnts_Amt;
    private DbsField pnd_M362t_Ttl_Rqst_Pcnt_Amt;
    private DbsField pnd_M362t_Ttl_Rqsts_Dlrs_Amt;
    private DbsField pnd_M362t_Ttl_Rqsts_Unts_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M362t_Anul_Mthd_Amt = parameters.newFieldInRecord("pnd_M362t_Anul_Mthd_Amt", "#M362T-ANUL-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_At_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_At_Amt", "#M362T-CNTCT-AT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Cs_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Cs_Amt", "#M362T-CNTCT-CS-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Fx_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Fx_Amt", "#M362T-CNTCT-FX-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Ig_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Ig_Amt", "#M362T-CNTCT-IG-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_In_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_In_Amt", "#M362T-CNTCT-IN-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Ml_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Ml_Amt", "#M362T-CNTCT-ML-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Vr_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Vr_Amt", "#M362T-CNTCT-VR-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Cntct_Vs_Amt = parameters.newFieldInRecord("pnd_M362t_Cntct_Vs_Amt", "#M362T-CNTCT-VS-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_M_T_M_Acct_Amt = parameters.newFieldInRecord("pnd_M362t_M_T_M_Acct_Amt", "#M362T-M-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_M_T_O_Acct_Amt = parameters.newFieldInRecord("pnd_M362t_M_T_O_Acct_Amt", "#M362T-M-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Mnth_Mthd_Amt = parameters.newFieldInRecord("pnd_M362t_Mnth_Mthd_Amt", "#M362T-MNTH-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_O_T_M_Acct_Amt = parameters.newFieldInRecord("pnd_M362t_O_T_M_Acct_Amt", "#M362T-O-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_O_T_O_Acct_Amt = parameters.newFieldInRecord("pnd_M362t_O_T_O_Acct_Amt", "#M362T-O-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Rept_Rqsts = parameters.newFieldInRecord("pnd_M362t_Rept_Rqsts", "#M362T-REPT-RQSTS", FieldType.NUMERIC, 9);
        pnd_M362t_Single_Rqsts = parameters.newFieldInRecord("pnd_M362t_Single_Rqsts", "#M362T-SINGLE-RQSTS", FieldType.NUMERIC, 9);
        pnd_M362t_Stat_Af_Amt = parameters.newFieldInRecord("pnd_M362t_Stat_Af_Amt", "#M362T-STAT-AF-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Stat_Da_Amt = parameters.newFieldInRecord("pnd_M362t_Stat_Da_Amt", "#M362T-STAT-DA-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Stat_Pa_Amt = parameters.newFieldInRecord("pnd_M362t_Stat_Pa_Amt", "#M362T-STAT-PA-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Stat_Rj_Amt = parameters.newFieldInRecord("pnd_M362t_Stat_Rj_Amt", "#M362T-STAT-RJ-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Stat_So_Amt = parameters.newFieldInRecord("pnd_M362t_Stat_So_Amt", "#M362T-STAT-SO-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Totl_Rqst_Amt = parameters.newFieldInRecord("pnd_M362t_Totl_Rqst_Amt", "#M362T-TOTL-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Ttl_Prtcpnts_Amt = parameters.newFieldInRecord("pnd_M362t_Ttl_Prtcpnts_Amt", "#M362T-TTL-PRTCPNTS-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Ttl_Rqst_Pcnt_Amt = parameters.newFieldInRecord("pnd_M362t_Ttl_Rqst_Pcnt_Amt", "#M362T-TTL-RQST-PCNT-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Ttl_Rqsts_Dlrs_Amt = parameters.newFieldInRecord("pnd_M362t_Ttl_Rqsts_Dlrs_Amt", "#M362T-TTL-RQSTS-DLRS-AMT", FieldType.NUMERIC, 9);
        pnd_M362t_Ttl_Rqsts_Unts_Amt = parameters.newFieldInRecord("pnd_M362t_Ttl_Rqsts_Unts_Amt", "#M362T-TTL-RQSTS-UNTS-AMT", FieldType.NUMERIC, 9);
        parameters.reset();
    }

    public Iatm363t() throws Exception
    {
        super("Iatm363t");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm363t", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm363t"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Transfer Requests Summary Statistics", "WHITE", 5, 38, 36);
            uiForm.setUiLabel("label_2", "Total Requests", "WHITE", 8, 30, 14);
            uiForm.setUiLabel("label_3", "......................", "", 8, 46, 22);
            uiForm.setUiControl("pnd_M362t_Totl_Rqst_Amt", pnd_M362t_Totl_Rqst_Amt, true, 8, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Total Repetative Requests", "WHITE", 9, 30, 25);
            uiForm.setUiLabel("label_5", "...........", "WHITE", 9, 57, 11);
            uiForm.setUiControl("pnd_M362t_Rept_Rqsts", pnd_M362t_Rept_Rqsts, true, 9, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Total Single Requets", "WHITE", 10, 30, 20);
            uiForm.setUiLabel("label_7", "................", "", 10, 52, 16);
            uiForm.setUiControl("pnd_M362t_Single_Rqsts", pnd_M362t_Single_Rqsts, true, 10, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Total Annual Revalued Requets", "WHITE", 11, 30, 29);
            uiForm.setUiLabel("label_9", ".......", "WHITE", 11, 61, 7);
            uiForm.setUiControl("pnd_M362t_Anul_Mthd_Amt", pnd_M362t_Anul_Mthd_Amt, true, 11, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Total Monthly Revalued Request", "WHITE", 12, 30, 30);
            uiForm.setUiLabel("label_11", "......", "WHITE", 12, 62, 6);
            uiForm.setUiControl("pnd_M362t_Mnth_Mthd_Amt", pnd_M362t_Mnth_Mthd_Amt, true, 12, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Total Participants", "WHITE", 13, 30, 18);
            uiForm.setUiLabel("label_13", "..................", "", 13, 50, 18);
            uiForm.setUiControl("pnd_M362t_Ttl_Prtcpnts_Amt", pnd_M362t_Ttl_Prtcpnts_Amt, true, 13, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Total Requests by:", "WHITE", 15, 30, 18);
            uiForm.setUiLabel("label_15", "Units", "WHITE", 15, 50, 5);
            uiForm.setUiLabel("label_16", "...........", "WHITE", 15, 57, 11);
            uiForm.setUiControl("pnd_M362t_Ttl_Rqsts_Unts_Amt", pnd_M362t_Ttl_Rqsts_Unts_Amt, true, 15, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Dollars", "WHITE", 16, 50, 7);
            uiForm.setUiLabel("label_18", ".........", "WHITE", 16, 59, 9);
            uiForm.setUiControl("pnd_M362t_Ttl_Rqsts_Dlrs_Amt", pnd_M362t_Ttl_Rqsts_Dlrs_Amt, true, 16, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Percent", "WHITE", 17, 50, 7);
            uiForm.setUiLabel("label_20", ".........", "WHITE", 17, 59, 9);
            uiForm.setUiControl("pnd_M362t_Ttl_Rqst_Pcnt_Amt", pnd_M362t_Ttl_Rqst_Pcnt_Amt, true, 17, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Total Requests of:", "WHITE", 19, 30, 18);
            uiForm.setUiLabel("label_22", "One to One", "WHITE", 19, 50, 10);
            uiForm.setUiLabel("label_23", "......", "WHITE", 19, 62, 6);
            uiForm.setUiControl("pnd_M362t_O_T_O_Acct_Amt", pnd_M362t_O_T_O_Acct_Amt, true, 19, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "One to Many", "WHITE", 20, 50, 11);
            uiForm.setUiLabel("label_25", ".....", "WHITE", 20, 63, 5);
            uiForm.setUiControl("pnd_M362t_O_T_M_Acct_Amt", pnd_M362t_O_T_M_Acct_Amt, true, 20, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "Many to One", "WHITE", 21, 50, 11);
            uiForm.setUiLabel("label_27", ".....", "WHITE", 21, 63, 5);
            uiForm.setUiControl("pnd_M362t_M_T_O_Acct_Amt", pnd_M362t_M_T_O_Acct_Amt, true, 21, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "Many to Many", "WHITE", 22, 50, 12);
            uiForm.setUiLabel("label_29", "....", "WHITE", 22, 64, 4);
            uiForm.setUiControl("pnd_M362t_M_T_M_Acct_Amt", pnd_M362t_M_T_M_Acct_Amt, true, 22, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "Total Contacts by:", "WHITE", 24, 30, 18);
            uiForm.setUiLabel("label_31", "Call Sheet", "WHITE", 24, 50, 10);
            uiForm.setUiLabel("label_32", "......", "WHITE", 24, 62, 6);
            uiForm.setUiControl("pnd_M362t_Cntct_Cs_Amt", pnd_M362t_Cntct_Cs_Amt, true, 24, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "Mail", "WHITE", 25, 50, 4);
            uiForm.setUiLabel("label_34", "............", "WHITE", 25, 56, 12);
            uiForm.setUiControl("pnd_M362t_Cntct_Ml_Amt", pnd_M362t_Cntct_Ml_Amt, true, 25, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "Visit", "WHITE", 26, 50, 5);
            uiForm.setUiLabel("label_36", "...........", "WHITE", 26, 57, 11);
            uiForm.setUiControl("pnd_M362t_Cntct_Vs_Amt", pnd_M362t_Cntct_Vs_Amt, true, 26, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "Fax", "WHITE", 27, 50, 3);
            uiForm.setUiLabel("label_38", ".............", "WHITE", 27, 55, 13);
            uiForm.setUiControl("pnd_M362t_Cntct_Fx_Amt", pnd_M362t_Cntct_Fx_Amt, true, 27, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "Internally Gened .", "WHITE", 28, 50, 18);
            uiForm.setUiControl("pnd_M362t_Cntct_Ig_Amt", pnd_M362t_Cntct_Ig_Amt, true, 28, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "Internet", "WHITE", 29, 50, 8);
            uiForm.setUiLabel("label_41", "........", "WHITE", 29, 60, 8);
            uiForm.setUiControl("pnd_M362t_Cntct_In_Amt", pnd_M362t_Cntct_In_Amt, true, 29, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "ATS", "WHITE", 30, 50, 3);
            uiForm.setUiLabel("label_43", ".............", "WHITE", 30, 55, 13);
            uiForm.setUiControl("pnd_M362t_Cntct_At_Amt", pnd_M362t_Cntct_At_Amt, true, 30, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "Voice Recording", "WHITE", 31, 50, 15);
            uiForm.setUiLabel("label_45", ".", "WHITE", 31, 67, 1);
            uiForm.setUiControl("pnd_M362t_Cntct_Vr_Amt", pnd_M362t_Cntct_Vr_Amt, true, 31, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "Totals by Status:", "WHITE", 35, 30, 17);
            uiForm.setUiLabel("label_47", "Awaiting Factor....", "WHITE", 35, 49, 19);
            uiForm.setUiControl("pnd_M362t_Stat_Af_Amt", pnd_M362t_Stat_Af_Amt, true, 35, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "Pnding Application.", "WHITE", 36, 49, 19);
            uiForm.setUiControl("pnd_M362t_Stat_Pa_Amt", pnd_M362t_Stat_Pa_Amt, true, 36, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "Suprvsr Ovrd Rqrd .", "WHITE", 37, 49, 19);
            uiForm.setUiControl("pnd_M362t_Stat_So_Amt", pnd_M362t_Stat_So_Amt, true, 37, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "Delayed............", "WHITE", 38, 49, 19);
            uiForm.setUiControl("pnd_M362t_Stat_Da_Amt", pnd_M362t_Stat_Da_Amt, true, 38, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "Rejected...........", "WHITE", 39, 49, 19);
            uiForm.setUiControl("pnd_M362t_Stat_Rj_Amt", pnd_M362t_Stat_Rj_Amt, true, 39, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "****", "WHITE", 41, 51, 4);
            uiForm.setUiLabel("label_53", "END OF REPORT", "WHITE", 41, 57, 13);
            uiForm.setUiLabel("label_54", "****", "WHITE", 41, 72, 4);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
