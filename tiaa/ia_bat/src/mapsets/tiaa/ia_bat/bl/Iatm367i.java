/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:23 PM
**        *   FROM NATURAL MAP   :  Iatm367i
************************************************************
**        * FILE NAME               : Iatm367i.java
**        * CLASS NAME              : Iatm367i
**        * INSTANCE NAME           : Iatm367i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M367I-CNTCT-MTHD
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm367i extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M367i_Cntct_Mthd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M367i_Cntct_Mthd = parameters.newFieldInRecord("pnd_M367i_Cntct_Mthd", "#M367I-CNTCT-MTHD", FieldType.STRING, 20);
        parameters.reset();
    }

    public Iatm367i() throws Exception
    {
        super("Iatm367i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm367i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm367i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Requests Received by:", "WHITE", 2, 1, 21);
            uiForm.setUiControl("pnd_M367i_Cntct_Mthd", pnd_M367i_Cntct_Mthd, true, 2, 23, 20, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "State", "WHITE", 4, 48, 5);
            uiForm.setUiLabel("label_3", "PIN", "WHITE", 5, 1, 3);
            uiForm.setUiLabel("label_4", "Request", "WHITE", 5, 25, 7);
            uiForm.setUiLabel("label_5", "Request", "WHITE", 5, 36, 7);
            uiForm.setUiLabel("label_6", "of", "WHITE", 5, 48, 2);
            uiForm.setUiLabel("label_7", "of", "WHITE", 5, 52, 2);
            uiForm.setUiLabel("label_8", "From", "WHITE", 5, 59, 4);
            uiForm.setUiLabel("label_9", "Frm", "WHITE", 5, 66, 3);
            uiForm.setUiLabel("label_10", "From", "WHITE", 5, 79, 4);
            uiForm.setUiLabel("label_11", "Number", "WHITE", 6, 1, 6);
            uiForm.setUiLabel("label_12", "Contract", "WHITE", 6, 14, 8);
            uiForm.setUiLabel("label_13", "Rcvd Date", "WHITE", 6, 25, 9);
            uiForm.setUiLabel("label_14", "Entry Date", "WHITE", 6, 36, 10);
            uiForm.setUiLabel("label_15", "Iss Res", "WHITE", 6, 48, 7);
            uiForm.setUiLabel("label_16", "Fnd Mthd Typ", "WHITE", 6, 57, 12);
            uiForm.setUiLabel("label_17", "Quantity", "WHITE", 6, 75, 8);
            uiForm.setUiLabel("label_18", "Participants Name", "WHITE", 6, 85, 17);
            uiForm.setUiLabel("label_19", "Status", "WHITE", 6, 116, 6);
            uiForm.setUiLabel("label_20", "------------ ---------", "WHITE", 7, 1, 22);
            uiForm.setUiLabel("label_21", "---------- ----------", "WHITE", 7, 25, 21);
            uiForm.setUiLabel("label_22", "--- ---", "WHITE", 7, 48, 7);
            uiForm.setUiLabel("label_23", "--- ---- --- -------------", "WHITE", 7, 57, 26);
            uiForm.setUiLabel("label_24", "------------------------------", "", 7, 85, 30);
            uiForm.setUiLabel("label_25", "-----------------", "", 7, 116, 17);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
