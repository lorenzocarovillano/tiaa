/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:22 PM
**        *   FROM NATURAL MAP   :  Iatm367d
************************************************************
**        * FILE NAME               : Iatm367d.java
**        * CLASS NAME              : Iatm367d
**        * INSTANCE NAME           : Iatm367d
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M367D-FRM-CNTRCT #M367D-FRM-FND #M367D-FRM-MTHD #M367D-FRM-QUNTY                                                        *     #M367D-FRM-TYP 
    #M367D-ISSUE-STATE #M367D-PRTCPNT-NME                                                                     *     #M367D-RCVD-DTE-OT #M367D-RQST-ENTRY-DTE-OT 
    #M367D-RSDNCE-STATE                                                          *     #M367D-STATUS #M367D-UNIQUE-ID
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm367d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M367d_Frm_Cntrct;
    private DbsField pnd_M367d_Frm_Fnd;
    private DbsField pnd_M367d_Frm_Mthd;
    private DbsField pnd_M367d_Frm_Qunty;
    private DbsField pnd_M367d_Frm_Typ;
    private DbsField pnd_M367d_Issue_State;
    private DbsField pnd_M367d_Prtcpnt_Nme;
    private DbsField pnd_M367d_Rcvd_Dte_Ot;
    private DbsField pnd_M367d_Rqst_Entry_Dte_Ot;
    private DbsField pnd_M367d_Rsdnce_State;
    private DbsField pnd_M367d_Status;
    private DbsField pnd_M367d_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M367d_Frm_Cntrct = parameters.newFieldInRecord("pnd_M367d_Frm_Cntrct", "#M367D-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_M367d_Frm_Fnd = parameters.newFieldInRecord("pnd_M367d_Frm_Fnd", "#M367D-FRM-FND", FieldType.STRING, 1);
        pnd_M367d_Frm_Mthd = parameters.newFieldInRecord("pnd_M367d_Frm_Mthd", "#M367D-FRM-MTHD", FieldType.STRING, 1);
        pnd_M367d_Frm_Qunty = parameters.newFieldInRecord("pnd_M367d_Frm_Qunty", "#M367D-FRM-QUNTY", FieldType.NUMERIC, 12, 2);
        pnd_M367d_Frm_Typ = parameters.newFieldInRecord("pnd_M367d_Frm_Typ", "#M367D-FRM-TYP", FieldType.STRING, 1);
        pnd_M367d_Issue_State = parameters.newFieldInRecord("pnd_M367d_Issue_State", "#M367D-ISSUE-STATE", FieldType.STRING, 2);
        pnd_M367d_Prtcpnt_Nme = parameters.newFieldInRecord("pnd_M367d_Prtcpnt_Nme", "#M367D-PRTCPNT-NME", FieldType.STRING, 30);
        pnd_M367d_Rcvd_Dte_Ot = parameters.newFieldInRecord("pnd_M367d_Rcvd_Dte_Ot", "#M367D-RCVD-DTE-OT", FieldType.STRING, 10);
        pnd_M367d_Rqst_Entry_Dte_Ot = parameters.newFieldInRecord("pnd_M367d_Rqst_Entry_Dte_Ot", "#M367D-RQST-ENTRY-DTE-OT", FieldType.STRING, 10);
        pnd_M367d_Rsdnce_State = parameters.newFieldInRecord("pnd_M367d_Rsdnce_State", "#M367D-RSDNCE-STATE", FieldType.STRING, 2);
        pnd_M367d_Status = parameters.newFieldInRecord("pnd_M367d_Status", "#M367D-STATUS", FieldType.STRING, 17);
        pnd_M367d_Unique_Id = parameters.newFieldInRecord("pnd_M367d_Unique_Id", "#M367D-UNIQUE-ID", FieldType.STRING, 8);
        parameters.reset();
    }

    public Iatm367d() throws Exception
    {
        super("Iatm367d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=022 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm367d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm367d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_M367d_Unique_Id", pnd_M367d_Unique_Id, true, 1, 1, 8, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Frm_Cntrct", pnd_M367d_Frm_Cntrct, true, 1, 10, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Rcvd_Dte_Ot", pnd_M367d_Rcvd_Dte_Ot, true, 1, 21, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M367d_Rqst_Entry_Dte_Ot", pnd_M367d_Rqst_Entry_Dte_Ot, true, 1, 33, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M367d_Issue_State", pnd_M367d_Issue_State, true, 1, 46, 2, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Rsdnce_State", pnd_M367d_Rsdnce_State, true, 1, 52, 2, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M367d_Frm_Fnd", pnd_M367d_Frm_Fnd, true, 1, 58, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Frm_Mthd", pnd_M367d_Frm_Mthd, true, 1, 62, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Frm_Typ", pnd_M367d_Frm_Typ, true, 1, 67, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Frm_Qunty", pnd_M367d_Frm_Qunty, true, 1, 70, 13, "BLUE", "ZZZZZZ,ZZZ.99", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M367d_Prtcpnt_Nme", pnd_M367d_Prtcpnt_Nme, true, 1, 85, 30, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M367d_Status", pnd_M367d_Status, true, 1, 116, 17, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
