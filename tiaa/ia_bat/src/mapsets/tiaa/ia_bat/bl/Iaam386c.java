/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:40 PM
**        *   FROM NATURAL MAP   :  Iaam386c
************************************************************
**        * FILE NAME               : Iaam386c.java
**        * CLASS NAME              : Iaam386c
**        * INSTANCE NAME           : Iaam386c
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE                                                                               *     IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE 
    *     IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR                                                                              *     IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE 
    *     IAA-TIAA-CREF-OLD-RATES-REC.FUND-LST-PD-DTE
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam386c extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde = parameters.newFieldInRecord("iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde", "IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE", 
            FieldType.STRING, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde = parameters.newFieldInRecord("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde", "IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr", "IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte = parameters.newFieldInRecord("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte", "IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte = parameters.newFieldInRecord("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte", "IAA-TIAA-CREF-OLD-RATES-REC.FUND-LST-PD-DTE", 
            FieldType.DATE);
        parameters.reset();
    }

    public Iaam386c() throws Exception
    {
        super("Iaam386c");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam386c", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam386c"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr", iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr, true, 1, 2, 10, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde", iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde, true, 1, 15, 2, "WHITE", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde", iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde, true, 1, 22, 3, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte", iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte, true, 1, 30, 0, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte", iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte, true, 1, 41, 
                8, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
