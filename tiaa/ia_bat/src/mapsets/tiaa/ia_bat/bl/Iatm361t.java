/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:19 PM
**        *   FROM NATURAL MAP   :  Iatm361t
************************************************************
**        * FILE NAME               : Iatm361t.java
**        * CLASS NAME              : Iatm361t
**        * INSTANCE NAME           : Iatm361t
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #M361T-ANUL-MTHD-AMT #M361T-CNTCT-AT-AMT #M361T-CNTCT-CS-AMT                                                             *     #M361T-CNTCT-FX-AMT 
    #M361T-CNTCT-IG-AMT #M361T-CNTCT-IN-AMT                                                              *     #M361T-CNTCT-ML-AMT #M361T-CNTCT-VR-AMT #M361T-CNTCT-VS-AMT 
    *     #M361T-GRADED #M361T-M-T-M-ACCT-AMT #M361T-M-T-O-ACCT-AMT                                                                *     #M361T-MNTH-MTHD-AMT 
    #M361T-O-T-M-ACCT-AMT #M361T-O-T-O-ACCT-AMT                                                         *     #M361T-PRIOR-DEL #M361T-STAT-AF-AMT #M361T-STAT-CM-AMT 
    *     #M361T-STAT-DE-AMT #M361T-STAT-DY-AMT #M361T-STAT-PA-AMT                                                                 *     #M361T-STAT-RJ-AMT 
    #M361T-STAT-SO-AMT #M361T-TOTL-RQST-AMT                                                               *     #M361T-TTL-PRTCPNTS-AMT #M361T-TTL-RQST-PCNT-AMT 
    *     #M361T-TTL-RQSTS-DLRS-AMT #M361T-TTL-RQSTS-UNTS-AMT
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm361t extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M361t_Anul_Mthd_Amt;
    private DbsField pnd_M361t_Cntct_At_Amt;
    private DbsField pnd_M361t_Cntct_Cs_Amt;
    private DbsField pnd_M361t_Cntct_Fx_Amt;
    private DbsField pnd_M361t_Cntct_Ig_Amt;
    private DbsField pnd_M361t_Cntct_In_Amt;
    private DbsField pnd_M361t_Cntct_Ml_Amt;
    private DbsField pnd_M361t_Cntct_Vr_Amt;
    private DbsField pnd_M361t_Cntct_Vs_Amt;
    private DbsField pnd_M361t_Graded;
    private DbsField pnd_M361t_M_T_M_Acct_Amt;
    private DbsField pnd_M361t_M_T_O_Acct_Amt;
    private DbsField pnd_M361t_Mnth_Mthd_Amt;
    private DbsField pnd_M361t_O_T_M_Acct_Amt;
    private DbsField pnd_M361t_O_T_O_Acct_Amt;
    private DbsField pnd_M361t_Prior_Del;
    private DbsField pnd_M361t_Stat_Af_Amt;
    private DbsField pnd_M361t_Stat_Cm_Amt;
    private DbsField pnd_M361t_Stat_De_Amt;
    private DbsField pnd_M361t_Stat_Dy_Amt;
    private DbsField pnd_M361t_Stat_Pa_Amt;
    private DbsField pnd_M361t_Stat_Rj_Amt;
    private DbsField pnd_M361t_Stat_So_Amt;
    private DbsField pnd_M361t_Totl_Rqst_Amt;
    private DbsField pnd_M361t_Ttl_Prtcpnts_Amt;
    private DbsField pnd_M361t_Ttl_Rqst_Pcnt_Amt;
    private DbsField pnd_M361t_Ttl_Rqsts_Dlrs_Amt;
    private DbsField pnd_M361t_Ttl_Rqsts_Unts_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M361t_Anul_Mthd_Amt = parameters.newFieldInRecord("pnd_M361t_Anul_Mthd_Amt", "#M361T-ANUL-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_At_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_At_Amt", "#M361T-CNTCT-AT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Cs_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Cs_Amt", "#M361T-CNTCT-CS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Fx_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Fx_Amt", "#M361T-CNTCT-FX-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Ig_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Ig_Amt", "#M361T-CNTCT-IG-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_In_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_In_Amt", "#M361T-CNTCT-IN-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Ml_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Ml_Amt", "#M361T-CNTCT-ML-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Vr_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Vr_Amt", "#M361T-CNTCT-VR-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Vs_Amt = parameters.newFieldInRecord("pnd_M361t_Cntct_Vs_Amt", "#M361T-CNTCT-VS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Graded = parameters.newFieldInRecord("pnd_M361t_Graded", "#M361T-GRADED", FieldType.NUMERIC, 9);
        pnd_M361t_M_T_M_Acct_Amt = parameters.newFieldInRecord("pnd_M361t_M_T_M_Acct_Amt", "#M361T-M-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_M_T_O_Acct_Amt = parameters.newFieldInRecord("pnd_M361t_M_T_O_Acct_Amt", "#M361T-M-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Mnth_Mthd_Amt = parameters.newFieldInRecord("pnd_M361t_Mnth_Mthd_Amt", "#M361T-MNTH-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_O_T_M_Acct_Amt = parameters.newFieldInRecord("pnd_M361t_O_T_M_Acct_Amt", "#M361T-O-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_O_T_O_Acct_Amt = parameters.newFieldInRecord("pnd_M361t_O_T_O_Acct_Amt", "#M361T-O-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Prior_Del = parameters.newFieldInRecord("pnd_M361t_Prior_Del", "#M361T-PRIOR-DEL", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Af_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_Af_Amt", "#M361T-STAT-AF-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Cm_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_Cm_Amt", "#M361T-STAT-CM-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_De_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_De_Amt", "#M361T-STAT-DE-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Dy_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_Dy_Amt", "#M361T-STAT-DY-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Pa_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_Pa_Amt", "#M361T-STAT-PA-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Rj_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_Rj_Amt", "#M361T-STAT-RJ-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_So_Amt = parameters.newFieldInRecord("pnd_M361t_Stat_So_Amt", "#M361T-STAT-SO-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Totl_Rqst_Amt = parameters.newFieldInRecord("pnd_M361t_Totl_Rqst_Amt", "#M361T-TOTL-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Prtcpnts_Amt = parameters.newFieldInRecord("pnd_M361t_Ttl_Prtcpnts_Amt", "#M361T-TTL-PRTCPNTS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Rqst_Pcnt_Amt = parameters.newFieldInRecord("pnd_M361t_Ttl_Rqst_Pcnt_Amt", "#M361T-TTL-RQST-PCNT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Rqsts_Dlrs_Amt = parameters.newFieldInRecord("pnd_M361t_Ttl_Rqsts_Dlrs_Amt", "#M361T-TTL-RQSTS-DLRS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Rqsts_Unts_Amt = parameters.newFieldInRecord("pnd_M361t_Ttl_Rqsts_Unts_Amt", "#M361T-TTL-RQSTS-UNTS-AMT", FieldType.NUMERIC, 9);
        parameters.reset();
    }

    public Iatm361t() throws Exception
    {
        super("Iatm361t");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm361t", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm361t"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Transfer Requests Summary Statistics", "WHITE", 5, 38, 36);
            uiForm.setUiLabel("label_2", "Total Requests", "WHITE", 8, 30, 14);
            uiForm.setUiLabel("label_3", "......................", "", 8, 46, 22);
            uiForm.setUiControl("pnd_M361t_Totl_Rqst_Amt", pnd_M361t_Totl_Rqst_Amt, true, 8, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Total Annual Revalued Requests", "WHITE", 9, 30, 30);
            uiForm.setUiLabel("label_5", "......", "WHITE", 9, 62, 6);
            uiForm.setUiControl("pnd_M361t_Anul_Mthd_Amt", pnd_M361t_Anul_Mthd_Amt, true, 9, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Total Monthly Revalued Requests", "WHITE", 10, 30, 31);
            uiForm.setUiLabel("label_7", ".....", "WHITE", 10, 63, 5);
            uiForm.setUiControl("pnd_M361t_Mnth_Mthd_Amt", pnd_M361t_Mnth_Mthd_Amt, true, 10, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Total Graded to Standard Requests", "WHITE", 11, 30, 33);
            uiForm.setUiLabel("label_9", "...", "WHITE", 11, 65, 3);
            uiForm.setUiControl("pnd_M361t_Graded", pnd_M361t_Graded, true, 11, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_10", "Total Participants", "WHITE", 12, 30, 18);
            uiForm.setUiLabel("label_11", "..................", "", 12, 50, 18);
            uiForm.setUiControl("pnd_M361t_Ttl_Prtcpnts_Amt", pnd_M361t_Ttl_Prtcpnts_Amt, true, 12, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Total Requests by:", "WHITE", 14, 30, 18);
            uiForm.setUiLabel("label_13", "Units", "WHITE", 14, 50, 5);
            uiForm.setUiLabel("label_14", "...........", "WHITE", 14, 57, 11);
            uiForm.setUiControl("pnd_M361t_Ttl_Rqsts_Unts_Amt", pnd_M361t_Ttl_Rqsts_Unts_Amt, true, 14, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Dollars", "WHITE", 15, 50, 7);
            uiForm.setUiLabel("label_16", ".........", "WHITE", 15, 59, 9);
            uiForm.setUiControl("pnd_M361t_Ttl_Rqsts_Dlrs_Amt", pnd_M361t_Ttl_Rqsts_Dlrs_Amt, true, 15, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Percent", "WHITE", 16, 50, 7);
            uiForm.setUiLabel("label_18", ".........", "WHITE", 16, 59, 9);
            uiForm.setUiControl("pnd_M361t_Ttl_Rqst_Pcnt_Amt", pnd_M361t_Ttl_Rqst_Pcnt_Amt, true, 16, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Total Requests of:", "WHITE", 18, 30, 18);
            uiForm.setUiLabel("label_20", "One to One", "WHITE", 18, 50, 10);
            uiForm.setUiLabel("label_21", "......", "WHITE", 18, 62, 6);
            uiForm.setUiControl("pnd_M361t_O_T_O_Acct_Amt", pnd_M361t_O_T_O_Acct_Amt, true, 18, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "One to Many", "WHITE", 19, 50, 11);
            uiForm.setUiLabel("label_23", ".....", "WHITE", 19, 63, 5);
            uiForm.setUiControl("pnd_M361t_O_T_M_Acct_Amt", pnd_M361t_O_T_M_Acct_Amt, true, 19, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "Many to One", "WHITE", 20, 50, 11);
            uiForm.setUiLabel("label_25", ".....", "WHITE", 20, 63, 5);
            uiForm.setUiControl("pnd_M361t_M_T_O_Acct_Amt", pnd_M361t_M_T_O_Acct_Amt, true, 20, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "Many to Many", "WHITE", 21, 50, 12);
            uiForm.setUiLabel("label_27", "....", "WHITE", 21, 64, 4);
            uiForm.setUiControl("pnd_M361t_M_T_M_Acct_Amt", pnd_M361t_M_T_M_Acct_Amt, true, 21, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "Total Contacts by:", "WHITE", 23, 30, 18);
            uiForm.setUiLabel("label_29", "Call Sheet", "WHITE", 23, 50, 10);
            uiForm.setUiLabel("label_30", "......", "WHITE", 23, 62, 6);
            uiForm.setUiControl("pnd_M361t_Cntct_Cs_Amt", pnd_M361t_Cntct_Cs_Amt, true, 23, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "Mail", "WHITE", 24, 50, 4);
            uiForm.setUiLabel("label_32", "............", "WHITE", 24, 56, 12);
            uiForm.setUiControl("pnd_M361t_Cntct_Ml_Amt", pnd_M361t_Cntct_Ml_Amt, true, 24, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "Visit", "WHITE", 25, 50, 5);
            uiForm.setUiLabel("label_34", "...........", "WHITE", 25, 57, 11);
            uiForm.setUiControl("pnd_M361t_Cntct_Vs_Amt", pnd_M361t_Cntct_Vs_Amt, true, 25, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "Fax", "WHITE", 26, 50, 3);
            uiForm.setUiLabel("label_36", ".............", "WHITE", 26, 55, 13);
            uiForm.setUiControl("pnd_M361t_Cntct_Fx_Amt", pnd_M361t_Cntct_Fx_Amt, true, 26, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "Internally Gened .", "WHITE", 27, 50, 18);
            uiForm.setUiControl("pnd_M361t_Cntct_Ig_Amt", pnd_M361t_Cntct_Ig_Amt, true, 27, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "Internet", "WHITE", 28, 50, 8);
            uiForm.setUiLabel("label_39", "........", "WHITE", 28, 60, 8);
            uiForm.setUiControl("pnd_M361t_Cntct_In_Amt", pnd_M361t_Cntct_In_Amt, true, 28, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "ATS", "WHITE", 29, 50, 3);
            uiForm.setUiLabel("label_41", ".............", "WHITE", 29, 55, 13);
            uiForm.setUiControl("pnd_M361t_Cntct_At_Amt", pnd_M361t_Cntct_At_Amt, true, 29, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "Voice Recording", "WHITE", 30, 50, 15);
            uiForm.setUiLabel("label_43", ".", "WHITE", 30, 67, 1);
            uiForm.setUiControl("pnd_M361t_Cntct_Vr_Amt", pnd_M361t_Cntct_Vr_Amt, true, 30, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "Totals by Status:", "WHITE", 33, 30, 17);
            uiForm.setUiLabel("label_45", "Awaiting Factor", "WHITE", 33, 49, 15);
            uiForm.setUiLabel("label_46", "..", "WHITE", 33, 66, 2);
            uiForm.setUiControl("pnd_M361t_Stat_Af_Amt", pnd_M361t_Stat_Af_Amt, true, 33, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "Pnding Application.", "WHITE", 34, 49, 19);
            uiForm.setUiControl("pnd_M361t_Stat_Pa_Amt", pnd_M361t_Stat_Pa_Amt, true, 34, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "Suprvsr Ovrd Rqrd .", "WHITE", 35, 49, 19);
            uiForm.setUiControl("pnd_M361t_Stat_So_Amt", pnd_M361t_Stat_So_Amt, true, 35, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "Deleted", "WHITE", 36, 49, 7);
            uiForm.setUiLabel("label_50", "..........", "WHITE", 36, 58, 10);
            uiForm.setUiControl("pnd_M361t_Stat_De_Amt", pnd_M361t_Stat_De_Amt, true, 36, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "Delayed", "WHITE", 37, 49, 7);
            uiForm.setUiLabel("label_52", "..........", "WHITE", 37, 58, 10);
            uiForm.setUiControl("pnd_M361t_Stat_Dy_Amt", pnd_M361t_Stat_Dy_Amt, true, 37, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_53", "Rejected...........", "WHITE", 38, 49, 19);
            uiForm.setUiControl("pnd_M361t_Stat_Rj_Amt", pnd_M361t_Stat_Rj_Amt, true, 38, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_54", "Complete", "WHITE", 39, 49, 8);
            uiForm.setUiLabel("label_55", ".........", "WHITE", 39, 59, 9);
            uiForm.setUiControl("pnd_M361t_Stat_Cm_Amt", pnd_M361t_Stat_Cm_Amt, true, 39, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_56", "Deletion of prior Transfer Requests...", "WHITE", 42, 30, 38);
            uiForm.setUiControl("pnd_M361t_Prior_Del", pnd_M361t_Prior_Del, true, 42, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_57", "****", "WHITE", 45, 54, 4);
            uiForm.setUiLabel("label_58", "END OF REPORT", "WHITE", 45, 60, 13);
            uiForm.setUiLabel("label_59", "****", "WHITE", 45, 75, 4);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
