/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:22 PM
**        *   FROM NATURAL MAP   :  Iatm365i
************************************************************
**        * FILE NAME               : Iatm365i.java
**        * CLASS NAME              : Iatm365i
**        * INSTANCE NAME           : Iatm365i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    * MAP2: MAP PROFILES *****************************        410***********                                                       * .TT     OO   YE7I  
    BL6I       ?     +(                       *                                                       * 060133ZDLAYOM N1NNUCN____        X         01 NCST 
    YL           1 *                                                       ************************************************************************     
    *   * MAP2: VALIDATION *****************************************************                                                       * MAP2: END OF MAP 
    *****************************************************
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm365i extends BLNatBase
{
    // from LocalVariables/Parameters

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				
    }

    public Iatm365i() throws Exception
    {
        super("Iatm365i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm365i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm365i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PIN", "WHITE", 2, 1, 3);
            uiForm.setUiLabel("label_2", "Request", "WHITE", 2, 25, 7);
            uiForm.setUiLabel("label_3", "Request", "WHITE", 2, 37, 7);
            uiForm.setUiLabel("label_4", "From", "WHITE", 2, 50, 4);
            uiForm.setUiLabel("label_5", "Frm", "WHITE", 2, 57, 3);
            uiForm.setUiLabel("label_6", "From", "WHITE", 2, 71, 4);
            uiForm.setUiLabel("label_7", "Number", "WHITE", 3, 1, 6);
            uiForm.setUiLabel("label_8", "Contract", "WHITE", 3, 14, 8);
            uiForm.setUiLabel("label_9", "Rcvd Date", "WHITE", 3, 25, 9);
            uiForm.setUiLabel("label_10", "Entry Date Fnd Mthd Typ", "WHITE", 3, 37, 23);
            uiForm.setUiLabel("label_11", "Quantity", "WHITE", 3, 67, 8);
            uiForm.setUiLabel("label_12", "Participants Name", "WHITE", 3, 77, 17);
            uiForm.setUiLabel("label_13", "------------ ---------", "WHITE", 4, 1, 22);
            uiForm.setUiLabel("label_14", "----------", "WHITE", 4, 25, 10);
            uiForm.setUiLabel("label_15", "---------- --- ---", "WHITE", 4, 37, 18);
            uiForm.setUiLabel("label_16", "---", "WHITE", 4, 57, 3);
            uiForm.setUiLabel("label_17", "-------------", "WHITE", 4, 62, 13);
            uiForm.setUiLabel("label_18", "-----------------------------", "", 4, 77, 29);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
