/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:39 PM
**        *   FROM NATURAL MAP   :  Iaam386
************************************************************
**        * FILE NAME               : Iaam386.java
**        * CLASS NAME              : Iaam386
**        * INSTANCE NAME           : Iaam386
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TOTAL-DUPLICATE #TOTAL-READ #TOTAL-WRITTEN
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam386 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Total_Duplicate;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Written;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Total_Duplicate = parameters.newFieldInRecord("pnd_Total_Duplicate", "#TOTAL-DUPLICATE", FieldType.INTEGER, 4);
        pnd_Total_Read = parameters.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.INTEGER, 4);
        pnd_Total_Written = parameters.newFieldInRecord("pnd_Total_Written", "#TOTAL-WRITTEN", FieldType.INTEGER, 4);
        parameters.reset();
    }

    public Iaam386() throws Exception
    {
        super("Iaam386");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam386", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam386"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PROGRAM: IAAP386", "BLUE", 1, 1, 16);
            uiForm.setUiLabel("label_2", "IAA SYSTEM", "BLUE", 1, 60, 10);
            uiForm.setUiLabel("label_3", "DATE:", "BLUE", 1, 114, 5);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 120, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "GRAND TOTAL REPORT", "BLUE", 2, 57, 18);
            uiForm.setUiLabel("label_5", "TIME:", "BLUE", 2, 114, 5);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 2, 120, 8, "WHITE", "HH.MM.SS", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "PAGE:", "BLUE", 3, 114, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 3, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "TOTAL READ RECORDS", "BLUE", 8, 1, 18);
            uiForm.setUiLabel("label_8", ":", "BLUE", 8, 22, 1);
            uiForm.setUiControl("pnd_Total_Read", pnd_Total_Read, true, 8, 25, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "TOTAL DUPL RECORDS", "BLUE", 10, 1, 18);
            uiForm.setUiLabel("label_10", ":", "BLUE", 10, 22, 1);
            uiForm.setUiControl("pnd_Total_Duplicate", pnd_Total_Duplicate, true, 10, 25, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "TOTAL WRITTEN RECORDS:", "BLUE", 12, 1, 22);
            uiForm.setUiControl("pnd_Total_Written", pnd_Total_Written, true, 12, 25, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
