/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:23 PM
**        *   FROM NATURAL MAP   :  Iatm369i
************************************************************
**        * FILE NAME               : Iatm369i.java
**        * CLASS NAME              : Iatm369i
**        * INSTANCE NAME           : Iatm369i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M361I-STAT-1 #M361I-STAT-2
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm369i extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M361i_Stat_1;
    private DbsField pnd_M361i_Stat_2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M361i_Stat_1 = parameters.newFieldInRecord("pnd_M361i_Stat_1", "#M361I-STAT-1", FieldType.STRING, 4);
        pnd_M361i_Stat_2 = parameters.newFieldInRecord("pnd_M361i_Stat_2", "#M361I-STAT-2", FieldType.STRING, 4);
        parameters.reset();
    }

    public Iatm369i() throws Exception
    {
        super("Iatm369i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm369i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm369i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PIN", "WHITE", 2, 1, 3);
            uiForm.setUiLabel("label_2", "From", "WHITE", 2, 15, 4);
            uiForm.setUiLabel("label_3", "To", "WHITE", 2, 25, 2);
            uiForm.setUiLabel("label_4", "X", "WHITE", 2, 35, 1);
            uiForm.setUiLabel("label_5", "Request", "WHITE", 2, 38, 7);
            uiForm.setUiLabel("label_6", "Request", "WHITE", 2, 49, 7);
            uiForm.setUiLabel("label_7", "Frm To", "WHITE", 2, 61, 6);
            uiForm.setUiLabel("label_8", "From", "WHITE", 2, 72, 4);
            uiForm.setUiLabel("label_9", "Frm", "WHITE", 2, 82, 3);
            uiForm.setUiLabel("label_10", "To", "WHITE", 2, 88, 2);
            uiForm.setUiLabel("label_11", "To", "WHITE", 2, 100, 2);
            uiForm.setUiLabel("label_12", "Number", "WHITE", 3, 1, 6);
            uiForm.setUiLabel("label_13", "Contract", "WHITE", 3, 15, 8);
            uiForm.setUiLabel("label_14", "Contract", "WHITE", 3, 25, 8);
            uiForm.setUiLabel("label_15", "O", "WHITE", 3, 35, 1);
            uiForm.setUiLabel("label_16", "Rcvd Date", "WHITE", 3, 38, 9);
            uiForm.setUiLabel("label_17", "Effv Date", "WHITE", 3, 49, 9);
            uiForm.setUiLabel("label_18", "Fnd Fnd", "WHITE", 3, 61, 7);
            uiForm.setUiLabel("label_19", "Quantit", "WHITE", 3, 72, 7);
            uiForm.setUiLabel("label_20", "Typ", "WHITE", 3, 82, 3);
            uiForm.setUiLabel("label_21", "Quantity", "WHITE", 3, 88, 8);
            uiForm.setUiLabel("label_22", "Typ Participants Name", "WHITE", 3, 100, 21);
            uiForm.setUiControl("pnd_M361i_Stat_1", pnd_M361i_Stat_1, true, 3, 129, 4, "", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "------------", "WHITE", 4, 1, 12);
            uiForm.setUiLabel("label_24", "--------", "WHITE", 4, 15, 8);
            uiForm.setUiLabel("label_25", "--------", "WHITE", 4, 25, 8);
            uiForm.setUiLabel("label_26", "-", "WHITE", 4, 35, 1);
            uiForm.setUiLabel("label_27", "---------- ----------", "WHITE", 4, 38, 21);
            uiForm.setUiLabel("label_28", "--- -- ------------- -- -------------", "WHITE", 4, 61, 37);
            uiForm.setUiLabel("label_29", "--", "WHITE", 4, 100, 2);
            uiForm.setUiLabel("label_30", "-------------------------", "", 4, 103, 25);
            uiForm.setUiControl("pnd_M361i_Stat_2", pnd_M361i_Stat_2, true, 4, 129, 4, "", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
