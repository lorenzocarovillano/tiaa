/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:40 PM
**        *   FROM NATURAL MAP   :  Iaam386b
************************************************************
**        * FILE NAME               : Iaam386b.java
**        * CLASS NAME              : Iaam386b
**        * INSTANCE NAME           : Iaam386b
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    * MAP2: MAP PROFILES *****************************        200***********                                                       * .TTO      OOO1II RE 
    NE6IBL ?_      +()                    *                                                       * 060132        N1NYUCN____        X         01 SYSDBA 
    YL             *                                                       ************************************************************************     
    *   *.01S005 P05.0.                                                                      * MAP2: VALIDATION ***************************************************** 
    * MAP2: END OF MAP *****************************************************
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam386b extends BLNatBase
{
    // from LocalVariables/Parameters

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				
    }

    public Iaam386b() throws Exception
    {
        super("Iaam386b");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam386b", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam386b"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 114, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "___________________________________________________________________________________________________________________________________", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_3", "PPCN NBR", "BLUE", 3, 2, 8);
            uiForm.setUiLabel("label_4", "PAYEE", "BLUE", 3, 13, 5);
            uiForm.setUiLabel("label_5", "CMPNY", "BLUE", 3, 21, 5);
            uiForm.setUiLabel("label_6", "FUND LST", "BLUE", 3, 29, 8);
            uiForm.setUiLabel("label_7", "FUND INVRSE LST", "BLUE", 3, 40, 15);
            uiForm.setUiLabel("label_8", "CDE", "BLUE", 4, 14, 3);
            uiForm.setUiLabel("label_9", "CDE", "BLUE", 4, 22, 3);
            uiForm.setUiLabel("label_10", "PD DTE", "BLUE", 4, 30, 6);
            uiForm.setUiLabel("label_11", "PD DTE", "BLUE", 4, 42, 6);
            uiForm.setUiLabel("label_12", "___________________________________________________________________________________________________________________________________", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
