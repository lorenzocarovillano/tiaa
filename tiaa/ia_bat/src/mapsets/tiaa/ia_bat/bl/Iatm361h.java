/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:18 PM
**        *   FROM NATURAL MAP   :  Iatm361h
************************************************************
**        * FILE NAME               : Iatm361h.java
**        * CLASS NAME              : Iatm361h
**        * INSTANCE NAME           : Iatm361h
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #HEADER1 #HEADER2 #M361H-BUSNS-DTE #M361H-PAGE #PROGRAM
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm361h extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M361h_Busns_Dte;
    private DbsField pnd_M361h_Page;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Header1 = parameters.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = parameters.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M361h_Busns_Dte = parameters.newFieldInRecord("pnd_M361h_Busns_Dte", "#M361H-BUSNS-DTE", FieldType.NUMERIC, 8);
        pnd_M361h_Page = parameters.newFieldInRecord("pnd_M361h_Page", "#M361H-PAGE", FieldType.NUMERIC, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Iatm361h() throws Exception
    {
        super("Iatm361h");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm361h", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm361h"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "BLUE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Header1", pnd_Header1, true, 1, 40, 50, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "Date:", "WHITE", 1, 99, 5);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 105, 10, "BLUE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Time:", "WHITE", 1, 117, 5);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 1, 123, 10, "BLUE", "HH:II:SS:T", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Header2", pnd_Header2, true, 2, 43, 45, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Page", "WHITE", 2, 117, 4);
            uiForm.setUiControl("pnd_M361h_Page", pnd_M361h_Page, true, 2, 123, 6, "BLUE", "ZZ,ZZ9", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "Entries Made:", "WHITE", 3, 54, 13);
            uiForm.setUiControl("pnd_M361h_Busns_Dte", pnd_M361h_Busns_Dte, true, 3, 68, 10, "BLUE", "99/99/9999", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
