/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:38 PM
**        *   FROM NATURAL MAP   :  Iaam382d
************************************************************
**        * FILE NAME               : Iaam382d.java
**        * CLASS NAME              : Iaam382d
**        * INSTANCE NAME           : Iaam382d
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TOTAL #TOTAL-ACTIVE #TOTAL-ACTIVE-PER-AMT #TOTAL-DELETED                                                                *     #TOTAL-RESET
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam382d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Total;
    private DbsField pnd_Total_Active;
    private DbsField pnd_Total_Active_Per_Amt;
    private DbsField pnd_Total_Deleted;
    private DbsField pnd_Total_Reset;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Total = parameters.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.INTEGER, 4);
        pnd_Total_Active = parameters.newFieldInRecord("pnd_Total_Active", "#TOTAL-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total_Active_Per_Amt = parameters.newFieldInRecord("pnd_Total_Active_Per_Amt", "#TOTAL-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Total_Deleted = parameters.newFieldInRecord("pnd_Total_Deleted", "#TOTAL-DELETED", FieldType.INTEGER, 4);
        pnd_Total_Reset = parameters.newFieldInRecord("pnd_Total_Reset", "#TOTAL-RESET", FieldType.INTEGER, 4);
        parameters.reset();
    }

    public Iaam382d() throws Exception
    {
        super("Iaam382d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam382d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam382d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PROGRAM: IAAP382", "BLUE", 1, 1, 16);
            uiForm.setUiLabel("label_2", "IAA SYSTEM", "BLUE", 1, 60, 10);
            uiForm.setUiLabel("label_3", "DATE:", "BLUE", 1, 114, 5);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 120, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "DEDUCTION RECORDS", "BLUE", 2, 57, 17);
            uiForm.setUiLabel("label_5", "TIME:", "BLUE", 2, 114, 5);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 2, 120, 8, "WHITE", "HH.MM.SS", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "GRAND TOTAL REPORT", "BLUE", 3, 57, 18);
            uiForm.setUiLabel("label_7", "PAGE:", "BLUE", 3, 114, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 3, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_8", "TOTAL DELETED RECORDS", "BLUE", 8, 1, 21);
            uiForm.setUiLabel("label_9", ":", "BLUE", 8, 30, 1);
            uiForm.setUiControl("pnd_Total_Deleted", pnd_Total_Deleted, true, 8, 35, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "TOTAL RESET RECORDS", "BLUE", 10, 1, 19);
            uiForm.setUiLabel("label_11", ":", "BLUE", 10, 30, 1);
            uiForm.setUiControl("pnd_Total_Reset", pnd_Total_Reset, true, 10, 35, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "TOTAL ACTIVE RECORDS", "BLUE", 12, 1, 20);
            uiForm.setUiLabel("label_13", ":", "BLUE", 12, 30, 1);
            uiForm.setUiControl("pnd_Total_Active", pnd_Total_Active, true, 12, 35, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "TOTAL ACTIVE PERIODIC AMOUNTS:", "BLUE", 14, 1, 30);
            uiForm.setUiControl("pnd_Total_Active_Per_Amt", pnd_Total_Active_Per_Amt, true, 14, 33, 19, "WHITE", "' '$ZZ,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "TOTAL NUMBER OF RECORDS", "BLUE", 16, 1, 23);
            uiForm.setUiLabel("label_16", ":", "BLUE", 16, 30, 1);
            uiForm.setUiControl("pnd_Total", pnd_Total, true, 16, 35, 13, "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
