/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:22 PM
**        *   FROM NATURAL MAP   :  Iatm365t
************************************************************
**        * FILE NAME               : Iatm365t.java
**        * CLASS NAME              : Iatm365t
**        * INSTANCE NAME           : Iatm365t
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #M365T-ANUL-TO-MNTH-AMT #M365T-CNTCT-AT-AMT #M365T-CNTCT-CS-AMT                                                          *     #M365T-CNTCT-FX-AMT 
    #M365T-CNTCT-IG-AMT #M365T-CNTCT-IN-AMT                                                              *     #M365T-CNTCT-ML-AMT #M365T-CNTCT-VR-AMT #M365T-CNTCT-VS-AMT 
    *     #M365T-MNTH-TO-ANUL-AMT #M365T-STAT-AF-AMT #M365T-STAT-DE-AMT                                                            *     #M365T-SWITCH-AMT 
    #M365T-TOTL-RQST-AMT #M365T-TTL-PRTCPNTS-AMT                                                           *     #M365T-TTL-RQST-PCNT-AMT #M365T-TTL-RQSTS-DLRS-AMT 
    *     #M365T-TTL-RQSTS-UNTS-AMT
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm365t extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M365t_Anul_To_Mnth_Amt;
    private DbsField pnd_M365t_Cntct_At_Amt;
    private DbsField pnd_M365t_Cntct_Cs_Amt;
    private DbsField pnd_M365t_Cntct_Fx_Amt;
    private DbsField pnd_M365t_Cntct_Ig_Amt;
    private DbsField pnd_M365t_Cntct_In_Amt;
    private DbsField pnd_M365t_Cntct_Ml_Amt;
    private DbsField pnd_M365t_Cntct_Vr_Amt;
    private DbsField pnd_M365t_Cntct_Vs_Amt;
    private DbsField pnd_M365t_Mnth_To_Anul_Amt;
    private DbsField pnd_M365t_Stat_Af_Amt;
    private DbsField pnd_M365t_Stat_De_Amt;
    private DbsField pnd_M365t_Switch_Amt;
    private DbsField pnd_M365t_Totl_Rqst_Amt;
    private DbsField pnd_M365t_Ttl_Prtcpnts_Amt;
    private DbsField pnd_M365t_Ttl_Rqst_Pcnt_Amt;
    private DbsField pnd_M365t_Ttl_Rqsts_Dlrs_Amt;
    private DbsField pnd_M365t_Ttl_Rqsts_Unts_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M365t_Anul_To_Mnth_Amt = parameters.newFieldInRecord("pnd_M365t_Anul_To_Mnth_Amt", "#M365T-ANUL-TO-MNTH-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_At_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_At_Amt", "#M365T-CNTCT-AT-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Cs_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Cs_Amt", "#M365T-CNTCT-CS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Fx_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Fx_Amt", "#M365T-CNTCT-FX-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Ig_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Ig_Amt", "#M365T-CNTCT-IG-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_In_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_In_Amt", "#M365T-CNTCT-IN-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Ml_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Ml_Amt", "#M365T-CNTCT-ML-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Vr_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Vr_Amt", "#M365T-CNTCT-VR-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Vs_Amt = parameters.newFieldInRecord("pnd_M365t_Cntct_Vs_Amt", "#M365T-CNTCT-VS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Mnth_To_Anul_Amt = parameters.newFieldInRecord("pnd_M365t_Mnth_To_Anul_Amt", "#M365T-MNTH-TO-ANUL-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Stat_Af_Amt = parameters.newFieldInRecord("pnd_M365t_Stat_Af_Amt", "#M365T-STAT-AF-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Stat_De_Amt = parameters.newFieldInRecord("pnd_M365t_Stat_De_Amt", "#M365T-STAT-DE-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Switch_Amt = parameters.newFieldInRecord("pnd_M365t_Switch_Amt", "#M365T-SWITCH-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Totl_Rqst_Amt = parameters.newFieldInRecord("pnd_M365t_Totl_Rqst_Amt", "#M365T-TOTL-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Prtcpnts_Amt = parameters.newFieldInRecord("pnd_M365t_Ttl_Prtcpnts_Amt", "#M365T-TTL-PRTCPNTS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Rqst_Pcnt_Amt = parameters.newFieldInRecord("pnd_M365t_Ttl_Rqst_Pcnt_Amt", "#M365T-TTL-RQST-PCNT-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Rqsts_Dlrs_Amt = parameters.newFieldInRecord("pnd_M365t_Ttl_Rqsts_Dlrs_Amt", "#M365T-TTL-RQSTS-DLRS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Rqsts_Unts_Amt = parameters.newFieldInRecord("pnd_M365t_Ttl_Rqsts_Unts_Amt", "#M365T-TTL-RQSTS-UNTS-AMT", FieldType.NUMERIC, 9);
        parameters.reset();
    }

    public Iatm365t() throws Exception
    {
        super("Iatm365t");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm365t", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm365t"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Switch Requests Summary Statistics", "WHITE", 5, 40, 34);
            uiForm.setUiLabel("label_2", "Total Requests", "WHITE", 8, 30, 14);
            uiForm.setUiLabel("label_3", ".......................", "", 8, 45, 23);
            uiForm.setUiControl("pnd_M365t_Totl_Rqst_Amt", pnd_M365t_Totl_Rqst_Amt, true, 8, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Total Switch Requests", "WHITE", 9, 30, 21);
            uiForm.setUiLabel("label_5", "...............", "WHITE", 9, 53, 15);
            uiForm.setUiControl("pnd_M365t_Switch_Amt", pnd_M365t_Switch_Amt, true, 9, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Total Monthly to Annual Switches", "WHITE", 10, 30, 32);
            uiForm.setUiLabel("label_7", "....", "WHITE", 10, 64, 4);
            uiForm.setUiControl("pnd_M365t_Mnth_To_Anul_Amt", pnd_M365t_Mnth_To_Anul_Amt, true, 10, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Total Annual to Monthly Switches", "WHITE", 11, 30, 32);
            uiForm.setUiLabel("label_9", "....", "WHITE", 11, 64, 4);
            uiForm.setUiControl("pnd_M365t_Anul_To_Mnth_Amt", pnd_M365t_Anul_To_Mnth_Amt, true, 11, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Total Participants", "WHITE", 12, 30, 18);
            uiForm.setUiLabel("label_11", "..................", "", 12, 50, 18);
            uiForm.setUiControl("pnd_M365t_Ttl_Prtcpnts_Amt", pnd_M365t_Ttl_Prtcpnts_Amt, true, 12, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Total Requests by:", "WHITE", 14, 30, 18);
            uiForm.setUiLabel("label_13", "Units", "WHITE", 14, 50, 5);
            uiForm.setUiLabel("label_14", "...........", "WHITE", 14, 57, 11);
            uiForm.setUiControl("pnd_M365t_Ttl_Rqsts_Unts_Amt", pnd_M365t_Ttl_Rqsts_Unts_Amt, true, 14, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Dollars", "WHITE", 15, 50, 7);
            uiForm.setUiLabel("label_16", ".........", "WHITE", 15, 59, 9);
            uiForm.setUiControl("pnd_M365t_Ttl_Rqsts_Dlrs_Amt", pnd_M365t_Ttl_Rqsts_Dlrs_Amt, true, 15, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Percent", "WHITE", 16, 50, 7);
            uiForm.setUiLabel("label_18", ".........", "WHITE", 16, 59, 9);
            uiForm.setUiControl("pnd_M365t_Ttl_Rqst_Pcnt_Amt", pnd_M365t_Ttl_Rqst_Pcnt_Amt, true, 16, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Total Contacts by:", "WHITE", 18, 30, 18);
            uiForm.setUiLabel("label_20", "Call Sheet", "WHITE", 18, 50, 10);
            uiForm.setUiLabel("label_21", "......", "WHITE", 18, 62, 6);
            uiForm.setUiControl("pnd_M365t_Cntct_Cs_Amt", pnd_M365t_Cntct_Cs_Amt, true, 18, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "Mail", "WHITE", 19, 50, 4);
            uiForm.setUiLabel("label_23", "............", "WHITE", 19, 56, 12);
            uiForm.setUiControl("pnd_M365t_Cntct_Ml_Amt", pnd_M365t_Cntct_Ml_Amt, true, 19, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "Visit", "WHITE", 20, 50, 5);
            uiForm.setUiLabel("label_25", "...........", "WHITE", 20, 57, 11);
            uiForm.setUiControl("pnd_M365t_Cntct_Vs_Amt", pnd_M365t_Cntct_Vs_Amt, true, 20, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "Fax", "WHITE", 21, 50, 3);
            uiForm.setUiLabel("label_27", ".............", "WHITE", 21, 55, 13);
            uiForm.setUiControl("pnd_M365t_Cntct_Fx_Amt", pnd_M365t_Cntct_Fx_Amt, true, 21, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "Internally Gened .", "WHITE", 22, 50, 18);
            uiForm.setUiControl("pnd_M365t_Cntct_Ig_Amt", pnd_M365t_Cntct_Ig_Amt, true, 22, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "Internet", "WHITE", 23, 50, 8);
            uiForm.setUiLabel("label_30", "........", "WHITE", 23, 60, 8);
            uiForm.setUiControl("pnd_M365t_Cntct_In_Amt", pnd_M365t_Cntct_In_Amt, true, 23, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "ATS", "WHITE", 24, 50, 3);
            uiForm.setUiLabel("label_32", ".............", "WHITE", 24, 55, 13);
            uiForm.setUiControl("pnd_M365t_Cntct_At_Amt", pnd_M365t_Cntct_At_Amt, true, 24, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "Voice Recording", "WHITE", 25, 50, 15);
            uiForm.setUiLabel("label_34", ".", "WHITE", 25, 67, 1);
            uiForm.setUiControl("pnd_M365t_Cntct_Vr_Amt", pnd_M365t_Cntct_Vr_Amt, true, 25, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "Totals by Status:", "WHITE", 28, 30, 17);
            uiForm.setUiLabel("label_36", "Awaiting Factor", "WHITE", 28, 49, 15);
            uiForm.setUiLabel("label_37", "..", "WHITE", 28, 66, 2);
            uiForm.setUiControl("pnd_M365t_Stat_Af_Amt", pnd_M365t_Stat_Af_Amt, true, 28, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "Deleted ...........", "WHITE", 29, 49, 19);
            uiForm.setUiControl("pnd_M365t_Stat_De_Amt", pnd_M365t_Stat_De_Amt, true, 29, 70, 11, "BLUE", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
