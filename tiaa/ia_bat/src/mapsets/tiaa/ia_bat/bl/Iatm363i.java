/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:21 PM
**        *   FROM NATURAL MAP   :  Iatm363i
************************************************************
**        * FILE NAME               : Iatm363i.java
**        * CLASS NAME              : Iatm363i
**        * INSTANCE NAME           : Iatm363i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M361I-STAT-1 #M361I-STAT-2
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm363i extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M361i_Stat_1;
    private DbsField pnd_M361i_Stat_2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M361i_Stat_1 = parameters.newFieldInRecord("pnd_M361i_Stat_1", "#M361I-STAT-1", FieldType.STRING, 4);
        pnd_M361i_Stat_2 = parameters.newFieldInRecord("pnd_M361i_Stat_2", "#M361I-STAT-2", FieldType.STRING, 4);
        parameters.reset();
    }

    public Iatm363i() throws Exception
    {
        super("Iatm363i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm363i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm363i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PIN", "WHITE", 2, 1, 3);
            uiForm.setUiLabel("label_2", "From", "WHITE", 2, 14, 4);
            uiForm.setUiLabel("label_3", "To", "WHITE", 2, 25, 2);
            uiForm.setUiLabel("label_4", "Request", "WHITE", 2, 36, 7);
            uiForm.setUiLabel("label_5", "Request", "WHITE", 2, 47, 7);
            uiForm.setUiLabel("label_6", "X Frm To", "WHITE", 2, 58, 8);
            uiForm.setUiLabel("label_7", "From", "WHITE", 2, 69, 4);
            uiForm.setUiLabel("label_8", "Frm To", "WHITE", 2, 78, 6);
            uiForm.setUiLabel("label_9", "Unt To", "WHITE", 2, 92, 6);
            uiForm.setUiLabel("label_10", "Number", "WHITE", 3, 1, 6);
            uiForm.setUiLabel("label_11", "Contract", "WHITE", 3, 14, 8);
            uiForm.setUiLabel("label_12", "Contract", "WHITE", 3, 25, 8);
            uiForm.setUiLabel("label_13", "Rcvd Date", "WHITE", 3, 36, 9);
            uiForm.setUiLabel("label_14", "Effv Date", "WHITE", 3, 47, 9);
            uiForm.setUiLabel("label_15", "O Fnd Fnd", "WHITE", 3, 58, 9);
            uiForm.setUiLabel("label_16", "Quantity Typ Quanitity Typ Typ Participants Name", "WHITE", 3, 69, 48);
            uiForm.setUiControl("pnd_M361i_Stat_1", pnd_M361i_Stat_1, true, 3, 129, 4, "", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "------------ ---------- ---------- ---------- ---------- - --- --- ---------", "WHITE", 4, 1, 76);
            uiForm.setUiLabel("label_18", "--- --------- --- --", "WHITE", 4, 78, 20);
            uiForm.setUiLabel("label_19", "-----------------------------", "", 4, 99, 29);
            uiForm.setUiControl("pnd_M361i_Stat_2", pnd_M361i_Stat_2, true, 4, 129, 4, "", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
