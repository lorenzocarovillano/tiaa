/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:37 PM
**        *   FROM NATURAL MAP   :  Iaam382
************************************************************
**        * FILE NAME               : Iaam382.java
**        * CLASS NAME              : Iaam382
**        * INSTANCE NAME           : Iaam382
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    * MAP2: MAP PROFILES *****************************        200***********                                                       * .TTO      OOO1II RE 
    NE6IBL ?_      +()                    *                                                       * 060132        N1NYUCN____        X         01 SYSDBA 
    YL             *                                                       ************************************************************************     
    *   * MAP2: VALIDATION *****************************************************                                                       * MAP2: END OF MAP 
    *****************************************************
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaam382 extends BLNatBase
{
    // from LocalVariables/Parameters

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				
    }

    public Iaam382() throws Exception
    {
        super("Iaam382");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iaam382", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iaam382"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "___________________________________________________________________________________________________________________________________", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_2", "TRN DTE", "BLUE", 3, 1, 7);
            uiForm.setUiLabel("label_3", "PPCN NBR", "BLUE", 3, 12, 8);
            uiForm.setUiLabel("label_4", "PAYEE", "BLUE", 3, 23, 5);
            uiForm.setUiLabel("label_5", "ID", "BLUE", 3, 31, 2);
            uiForm.setUiLabel("label_6", "DDCTN SEQ PAYEE", "BLUE", 3, 37, 15);
            uiForm.setUiLabel("label_7", "PER AMT", "BLUE", 3, 56, 7);
            uiForm.setUiLabel("label_8", "YTD AMT", "BLUE", 3, 67, 7);
            uiForm.setUiLabel("label_9", "PD TO", "BLUE", 3, 79, 5);
            uiForm.setUiLabel("label_10", "TOT AMT", "BLUE", 3, 89, 7);
            uiForm.setUiLabel("label_11", "INT", "BLUE", 3, 99, 3);
            uiForm.setUiLabel("label_12", "STRT", "BLUE", 3, 105, 4);
            uiForm.setUiLabel("label_13", "STP DTE", "BLUE", 3, 114, 7);
            uiForm.setUiLabel("label_14", "FINAL", "BLUE", 3, 125, 5);
            uiForm.setUiLabel("label_15", "CDE", "BLUE", 4, 24, 3);
            uiForm.setUiLabel("label_16", "NBR", "BLUE", 4, 31, 3);
            uiForm.setUiLabel("label_17", "CD", "BLUE", 4, 38, 2);
            uiForm.setUiLabel("label_18", "DTE", "BLUE", 4, 80, 3);
            uiForm.setUiLabel("label_19", "CDE", "BLUE", 4, 99, 3);
            uiForm.setUiLabel("label_20", "DTE", "BLUE", 4, 105, 3);
            uiForm.setUiLabel("label_21", "DTE", "BLUE", 4, 125, 3);
            uiForm.setUiLabel("label_22", "___________________________________________________________________________________________________________________________________", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
