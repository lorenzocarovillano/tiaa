/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:51:21 PM
**        *   FROM NATURAL MAP   :  Iatm365d
************************************************************
**        * FILE NAME               : Iatm365d.java
**        * CLASS NAME              : Iatm365d
**        * INSTANCE NAME           : Iatm365d
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #M365D-FRM-CNTRCT #M365D-FRM-FND #M365D-FRM-MTHD #M365D-FRM-QUNTY                                                        *     #M365D-FRM-TYP 
    #M365D-PRTCPNT-NME #M365D-RCVD-DTE-OT                                                                     *     #M365D-RQST-ENTRY-DTE-OT #M365D-STATUS 
    #M365D-UNIQUE-ID
************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatm365d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M365d_Frm_Cntrct;
    private DbsField pnd_M365d_Frm_Fnd;
    private DbsField pnd_M365d_Frm_Mthd;
    private DbsField pnd_M365d_Frm_Qunty;
    private DbsField pnd_M365d_Frm_Typ;
    private DbsField pnd_M365d_Prtcpnt_Nme;
    private DbsField pnd_M365d_Rcvd_Dte_Ot;
    private DbsField pnd_M365d_Rqst_Entry_Dte_Ot;
    private DbsField pnd_M365d_Status;
    private DbsField pnd_M365d_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M365d_Frm_Cntrct = parameters.newFieldInRecord("pnd_M365d_Frm_Cntrct", "#M365D-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_M365d_Frm_Fnd = parameters.newFieldInRecord("pnd_M365d_Frm_Fnd", "#M365D-FRM-FND", FieldType.STRING, 1);
        pnd_M365d_Frm_Mthd = parameters.newFieldInRecord("pnd_M365d_Frm_Mthd", "#M365D-FRM-MTHD", FieldType.STRING, 1);
        pnd_M365d_Frm_Qunty = parameters.newFieldInRecord("pnd_M365d_Frm_Qunty", "#M365D-FRM-QUNTY", FieldType.NUMERIC, 12, 2);
        pnd_M365d_Frm_Typ = parameters.newFieldInRecord("pnd_M365d_Frm_Typ", "#M365D-FRM-TYP", FieldType.STRING, 1);
        pnd_M365d_Prtcpnt_Nme = parameters.newFieldInRecord("pnd_M365d_Prtcpnt_Nme", "#M365D-PRTCPNT-NME", FieldType.STRING, 30);
        pnd_M365d_Rcvd_Dte_Ot = parameters.newFieldInRecord("pnd_M365d_Rcvd_Dte_Ot", "#M365D-RCVD-DTE-OT", FieldType.STRING, 10);
        pnd_M365d_Rqst_Entry_Dte_Ot = parameters.newFieldInRecord("pnd_M365d_Rqst_Entry_Dte_Ot", "#M365D-RQST-ENTRY-DTE-OT", FieldType.STRING, 10);
        pnd_M365d_Status = parameters.newFieldInRecord("pnd_M365d_Status", "#M365D-STATUS", FieldType.STRING, 20);
        pnd_M365d_Unique_Id = parameters.newFieldInRecord("pnd_M365d_Unique_Id", "#M365D-UNIQUE-ID", FieldType.STRING, 12);
        parameters.reset();
    }

    public Iatm365d() throws Exception
    {
        super("Iatm365d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=022 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Iatm365d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Iatm365d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_M365d_Unique_Id", pnd_M365d_Unique_Id, true, 1, 1, 12, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Frm_Cntrct", pnd_M365d_Frm_Cntrct, true, 1, 14, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Rcvd_Dte_Ot", pnd_M365d_Rcvd_Dte_Ot, true, 1, 25, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M365d_Rqst_Entry_Dte_Ot", pnd_M365d_Rqst_Entry_Dte_Ot, true, 1, 37, 10, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M365d_Frm_Fnd", pnd_M365d_Frm_Fnd, true, 1, 49, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Frm_Mthd", pnd_M365d_Frm_Mthd, true, 1, 53, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Frm_Typ", pnd_M365d_Frm_Typ, true, 1, 58, 1, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Frm_Qunty", pnd_M365d_Frm_Qunty, true, 1, 62, 13, "BLUE", "ZZZZZZ,ZZZ.99", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M365d_Prtcpnt_Nme", pnd_M365d_Prtcpnt_Nme, true, 1, 77, 30, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_M365d_Status", pnd_M365d_Status, true, 1, 108, 20, "BLUE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
