/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:41 PM
**        * FROM NATURAL PROGRAM : Iaap701k
************************************************************
**        * FILE NAME            : Iaap701k.java
**        * CLASS NAME           : Iaap701k
**        * INSTANCE NAME        : Iaap701k
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP701K                                          *
*                  PENDED REPORT - PEND K ONLY
*                  CONTRACTS THAT HAVE BEEN PLACED ON PEND K OR
*                  RELEASED FROM PEND K.
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap701k extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Dte;

    private DbsGroup pnd_Parm_Dte__R_Field_1;
    private DbsField pnd_Parm_Dte_Pnd_P_Mm;
    private DbsField pnd_Parm_Dte__Filler1;
    private DbsField pnd_Parm_Dte_Pnd_P_Dd;
    private DbsField pnd_Parm_Dte__Filler2;
    private DbsField pnd_Parm_Dte_Pnd_P_Yyyy;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Pmt_Amt;
    private DbsField pnd_Pend_Dte;
    private DbsField pnd_Cnt;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Lst_Trans_Dte;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Actvty_Cde;
    private DbsField trans_Trans_Check_Dte;
    private DbsField trans_Trans_Todays_Dte;
    private DbsField trans_Trans_Effective_Dte;
    private DbsField trans_Trans_User_Area;
    private DbsField trans_Trans_User_Id;
    private DbsField trans_Trans_Verify_Cde;
    private DbsField trans_Trans_Verify_Id;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_2;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_3;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_C_Yyyy;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_C_Mm;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_C_Dd;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte;

    private DataAccessProgramView vw_cpr_Trans_A;
    private DbsField cpr_Trans_A_Trans_Dte;
    private DbsField cpr_Trans_A_Invrse_Trans_Dte;
    private DbsField cpr_Trans_A_Lst_Trans_Dte;
    private DbsField cpr_Trans_A_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Trans_A_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Trans_A_Cntrct_Pend_Cde;
    private DbsField cpr_Trans_A_Trans_Check_Dte;
    private DbsField cpr_Trans_A_Bfre_Imge_Id;
    private DbsField cpr_Trans_A_Aftr_Imge_Id;

    private DataAccessProgramView vw_cpr_Trans_B;
    private DbsField cpr_Trans_B_Trans_Dte;
    private DbsField cpr_Trans_B_Invrse_Trans_Dte;
    private DbsField cpr_Trans_B_Lst_Trans_Dte;
    private DbsField cpr_Trans_B_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Trans_B_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Trans_B_Cntrct_Pend_Cde;
    private DbsField cpr_Trans_B_Trans_Check_Dte;
    private DbsField cpr_Trans_B_Bfre_Imge_Id;
    private DbsField cpr_Trans_B_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_4;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_B_Cntrct;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_B_Payee;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_B_Trans_Dte;
    private DbsField pnd_Cpr_Aftr_Key;

    private DbsGroup pnd_Cpr_Aftr_Key__R_Field_5;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Cntrct;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Payee;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte;

    private DataAccessProgramView vw_fund;
    private DbsField fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup fund__R_Field_6;
    private DbsField fund_Cmpny;
    private DbsField fund_Fund_Cde;
    private DbsField fund_Tiaa_Tot_Per_Amt;
    private DbsField fund_Tiaa_Tot_Div_Amt;

    private DbsGroup fund_Tiaa_Rate_Data_Grp;
    private DbsField fund_Tiaa_Units_Cnt;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_7;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund_Cde;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_Input;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_8;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_9;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_10;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Output;
    private DbsField aian026_Pnd_Rtrn_Cd_Pgm;

    private DbsGroup aian026__R_Field_11;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_12;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Days_In_Request_Month;
    private DbsField aian026_Pnd_Days_In_Particip_Month;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Tot_Pmt;
    private DbsField pnd_W_Amt;
    private DbsField pnd_Cpr_A_Found;
    private DbsField pnd_Cpr_B_Found;
    private DbsField pnd_A_Pend;
    private DbsField pnd_B_Pend;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm_Dte = localVariables.newFieldInRecord("pnd_Parm_Dte", "#PARM-DTE", FieldType.STRING, 10);

        pnd_Parm_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Dte__R_Field_1", "REDEFINE", pnd_Parm_Dte);
        pnd_Parm_Dte_Pnd_P_Mm = pnd_Parm_Dte__R_Field_1.newFieldInGroup("pnd_Parm_Dte_Pnd_P_Mm", "#P-MM", FieldType.STRING, 2);
        pnd_Parm_Dte__Filler1 = pnd_Parm_Dte__R_Field_1.newFieldInGroup("pnd_Parm_Dte__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Parm_Dte_Pnd_P_Dd = pnd_Parm_Dte__R_Field_1.newFieldInGroup("pnd_Parm_Dte_Pnd_P_Dd", "#P-DD", FieldType.STRING, 2);
        pnd_Parm_Dte__Filler2 = pnd_Parm_Dte__R_Field_1.newFieldInGroup("pnd_Parm_Dte__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Parm_Dte_Pnd_P_Yyyy = pnd_Parm_Dte__R_Field_1.newFieldInGroup("pnd_Parm_Dte_Pnd_P_Yyyy", "#P-YYYY", FieldType.STRING, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Pmt_Amt = localVariables.newFieldInRecord("pnd_Pmt_Amt", "#PMT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pend_Dte = localVariables.newFieldInRecord("pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 8);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Lst_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Actvty_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_ACTVTY_CDE");
        trans_Trans_Check_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        trans_Trans_Todays_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_TODAYS_DTE");
        trans_Trans_Effective_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_EFFECTIVE_DTE");
        trans_Trans_User_Area = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TRANS_USER_AREA");
        trans_Trans_User_Id = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TRANS_USER_ID");
        trans_Trans_Verify_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_CDE");
        trans_Trans_Verify_Id = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_ID");
        registerRecord(vw_trans);

        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_2", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Trans_Chck_Dte_Key__R_Field_3 = pnd_Trans_Chck_Dte_Key__R_Field_2.newGroupInGroup("pnd_Trans_Chck_Dte_Key__R_Field_3", "REDEFINE", pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte);
        pnd_Trans_Chck_Dte_Key_Pnd_C_Yyyy = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_C_Yyyy", "#C-YYYY", FieldType.STRING, 
            4);
        pnd_Trans_Chck_Dte_Key_Pnd_C_Mm = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_C_Mm", "#C-MM", FieldType.STRING, 
            2);
        pnd_Trans_Chck_Dte_Key_Pnd_C_Dd = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_C_Dd", "#C-DD", FieldType.STRING, 
            2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde", "#TRANS-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte", 
            "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);

        vw_cpr_Trans_A = new DataAccessProgramView(new NameInfo("vw_cpr_Trans_A", "CPR-TRANS-A"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        cpr_Trans_A_Trans_Dte = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cpr_Trans_A_Invrse_Trans_Dte = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cpr_Trans_A_Lst_Trans_Dte = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        cpr_Trans_A_Cntrct_Part_Ppcn_Nbr = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Trans_A_Cntrct_Part_Payee_Cde = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Trans_A_Cntrct_Pend_Cde = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        cpr_Trans_A_Trans_Check_Dte = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        cpr_Trans_A_Bfre_Imge_Id = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cpr_Trans_A_Aftr_Imge_Id = vw_cpr_Trans_A.getRecord().newFieldInGroup("cpr_Trans_A_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        registerRecord(vw_cpr_Trans_A);

        vw_cpr_Trans_B = new DataAccessProgramView(new NameInfo("vw_cpr_Trans_B", "CPR-TRANS-B"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        cpr_Trans_B_Trans_Dte = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cpr_Trans_B_Invrse_Trans_Dte = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cpr_Trans_B_Lst_Trans_Dte = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        cpr_Trans_B_Cntrct_Part_Ppcn_Nbr = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Trans_B_Cntrct_Part_Payee_Cde = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Trans_B_Cntrct_Pend_Cde = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        cpr_Trans_B_Trans_Check_Dte = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        cpr_Trans_B_Bfre_Imge_Id = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cpr_Trans_B_Aftr_Imge_Id = vw_cpr_Trans_B.getRecord().newFieldInGroup("cpr_Trans_B_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        registerRecord(vw_cpr_Trans_B);

        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_4", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cpr_Bfre_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Bfre_Key_Pnd_B_Cntrct = pnd_Cpr_Bfre_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_B_Cntrct", "#B-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_B_Payee = pnd_Cpr_Bfre_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_B_Payee", "#B-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_B_Trans_Dte = pnd_Cpr_Bfre_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_B_Trans_Dte", "#B-TRANS-DTE", FieldType.TIME);
        pnd_Cpr_Aftr_Key = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key__R_Field_5", "REDEFINE", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cpr_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Aftr_Key_Pnd_A_Cntrct = pnd_Cpr_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Cntrct", "#A-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Aftr_Key_Pnd_A_Payee = pnd_Cpr_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Payee", "#A-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte = pnd_Cpr_Aftr_Key__R_Field_5.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte", "#A-INVRSE-DTE", FieldType.NUMERIC, 
            12);

        vw_fund = new DataAccessProgramView(new NameInfo("vw_fund", "FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        fund_Tiaa_Cntrct_Ppcn_Nbr = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_PPCN_NBR");
        fund_Tiaa_Cntrct_Payee_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        fund_Tiaa_Cmpny_Fund_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIAA_CMPNY_FUND_CDE");

        fund__R_Field_6 = vw_fund.getRecord().newGroupInGroup("fund__R_Field_6", "REDEFINE", fund_Tiaa_Cmpny_Fund_Cde);
        fund_Cmpny = fund__R_Field_6.newFieldInGroup("fund_Cmpny", "CMPNY", FieldType.STRING, 1);
        fund_Fund_Cde = fund__R_Field_6.newFieldInGroup("fund_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        fund_Tiaa_Tot_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_TOT_PER_AMT");
        fund_Tiaa_Tot_Div_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_TOT_DIV_AMT");

        fund_Tiaa_Rate_Data_Grp = vw_fund.getRecord().newGroupInGroup("fund_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Units_Cnt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund);

        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_7", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct", "#FUND-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee", "#FUND-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_7.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund_Cde", 
            "#CMPNY-FUND-CDE", FieldType.STRING, 3);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_Input = aian026.newGroupInGroup("aian026_Pnd_Input", "#INPUT");
        aian026_Pnd_Call_Type = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_8 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_8", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_9 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_9", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_10 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_10", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_10.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_10.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Output = aian026.newGroupInGroup("aian026_Pnd_Output", "#OUTPUT");
        aian026_Pnd_Rtrn_Cd_Pgm = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Rtrn_Cd_Pgm", "#RTRN-CD-PGM", FieldType.STRING, 11);

        aian026__R_Field_11 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_11", "REDEFINE", aian026_Pnd_Rtrn_Cd_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_11.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_11.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_12 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_12", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_12.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Days_In_Request_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", FieldType.NUMERIC, 
            2);
        aian026_Pnd_Days_In_Particip_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Tot_Pmt = localVariables.newFieldInRecord("pnd_Tot_Pmt", "#TOT-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cpr_A_Found = localVariables.newFieldInRecord("pnd_Cpr_A_Found", "#CPR-A-FOUND", FieldType.BOOLEAN, 1);
        pnd_Cpr_B_Found = localVariables.newFieldInRecord("pnd_Cpr_B_Found", "#CPR-B-FOUND", FieldType.BOOLEAN, 1);
        pnd_A_Pend = localVariables.newFieldInRecord("pnd_A_Pend", "#A-PEND", FieldType.STRING, 1);
        pnd_B_Pend = localVariables.newFieldInRecord("pnd_B_Pend", "#B-PEND", FieldType.STRING, 1);
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trans.reset();
        vw_cpr_Trans_A.reset();
        vw_cpr_Trans_B.reset();
        vw_fund.reset();

        localVariables.reset();
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Cpr_Aftr_Key.setInitialValue("2");
        pnd_Fund_2.setInitialValue("09");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap701k() throws Exception
    {
        super("Iaap701k");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 150 PS = 0
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getWorkFiles().read(1, pnd_Parm_Dte);                                                                                                                             //Natural: READ WORK 1 ONCE #PARM-DTE
        pnd_Trans_Chck_Dte_Key_Pnd_C_Yyyy.setValue(pnd_Parm_Dte_Pnd_P_Yyyy);                                                                                              //Natural: ASSIGN #C-YYYY := #P-YYYY
        pnd_Trans_Chck_Dte_Key_Pnd_C_Mm.setValue(pnd_Parm_Dte_Pnd_P_Mm);                                                                                                  //Natural: ASSIGN #C-MM := #P-MM
        pnd_Trans_Chck_Dte_Key_Pnd_C_Dd.setValue(pnd_Parm_Dte_Pnd_P_Dd);                                                                                                  //Natural: ASSIGN #C-DD := #P-DD
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtrn_Cde);                                                                 //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTRN-CDE
        if (condition(Global.isEscape())) return;
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde.reset();                                                                                                                     //Natural: RESET #TRANS-CDE #INVRSE-TRANS-DTE
        pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte.reset();
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_trans.readNextRow("READ01")))
        {
            if (condition(trans_Trans_Check_Dte.notEquals(pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte)))                                                                   //Natural: IF TRANS-CHECK-DTE NE #TRANS-CHECK-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(trans_Trans_Cde.equals(102) && trans_Trans_Verify_Cde.equals(" "))))                                                                          //Natural: ACCEPT IF TRANS-CDE = 102 AND TRANS-VERIFY-CDE = ' '
            {
                continue;
            }
            pnd_B_Pend.reset();                                                                                                                                           //Natural: RESET #B-PEND #A-PEND
            pnd_A_Pend.reset();
                                                                                                                                                                          //Natural: PERFORM GET-BEFORE-IMAGE
            sub_Get_Before_Image();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM GET-AFTER-IMAGE
            sub_Get_After_Image();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_A_Pend.equals("K") || pnd_B_Pend.equals("K")))                                                                                              //Natural: IF #A-PEND = 'K' OR #B-PEND = 'K'
            {
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT
                sub_Get_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(1, "/Contract",                                                                                                                      //Natural: DISPLAY ( 1 ) '/Contract' TRANS-PPCN-NBR ( AL = 8 ) '/Py' TRANS-PAYEE-CDE ( EM = 99 ) 'Previous/Pend' #B-PEND 'Current/Pend' #A-PEND '/Trans Dte' TRANS.TRANS-DTE ( EM = YYYYMMDD ) '/User' TRANS-USER-ID '/Verifier' TRANS-VERIFY-ID '/Total Pymnt' #TOT-PMT ( EM = Z,ZZZ,ZZ9.99 )
                		trans_Trans_Ppcn_Nbr, new AlphanumericLength (8),"/Py",
                		trans_Trans_Payee_Cde, new ReportEditMask ("99"),"Previous/Pend",
                		pnd_B_Pend,"Current/Pend",
                		pnd_A_Pend,"/Trans Dte",
                		trans_Trans_Dte, new ReportEditMask ("YYYYMMDD"),"/User",
                		trans_Trans_User_Id,"/Verifier",
                		trans_Trans_Verify_Id,"/Total Pymnt",
                		pnd_Tot_Pmt, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.equals(getZero())))                                                                                                                         //Natural: IF #CNT = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"*******************************************************************",     //Natural: WRITE ( 1 ) ///// '*******************************************************************' '*                                                                 *' '*                                                                 *' '*             NO SUSPEND K TRANSACTIONS IN THIS RUN               *' '*                                                                 *' '*                                                                 *' '*******************************************************************'
                "*                                                                 *","*                                                                 *",
                "*             NO SUSPEND K TRANSACTIONS IN THIS RUN               *","*                                                                 *",
                "*                                                                 *","*******************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BEFORE-IMAGE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AFTER-IMAGE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PAYMENT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* ***********************************************************************
    }
    private void sub_Get_Before_Image() throws Exception                                                                                                                  //Natural: GET-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Bfre_Key_Pnd_B_Cntrct.setValue(trans_Trans_Ppcn_Nbr);                                                                                                     //Natural: ASSIGN #B-CNTRCT := TRANS-PPCN-NBR
        pnd_Cpr_Bfre_Key_Pnd_B_Payee.setValue(trans_Trans_Payee_Cde);                                                                                                     //Natural: ASSIGN #B-PAYEE := TRANS-PAYEE-CDE
        pnd_Cpr_Bfre_Key_Pnd_B_Trans_Dte.setValue(trans_Trans_Dte);                                                                                                       //Natural: ASSIGN #B-TRANS-DTE := TRANS.TRANS-DTE
        pnd_Cpr_B_Found.setValue(true);                                                                                                                                   //Natural: ASSIGN #CPR-B-FOUND := TRUE
        vw_cpr_Trans_B.startDatabaseFind                                                                                                                                  //Natural: FIND CPR-TRANS-B WITH CPR-BFRE-KEY = #CPR-BFRE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CPR_BFRE_KEY", "=", pnd_Cpr_Bfre_Key.getBinary(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cpr_Trans_B.readNextRow("FIND01", true)))
        {
            vw_cpr_Trans_B.setIfNotFoundControlFlag(false);
            if (condition(vw_cpr_Trans_B.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO
            {
                pnd_Cpr_B_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #CPR-B-FOUND := FALSE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_B_Pend.setValue(cpr_Trans_B_Cntrct_Pend_Cde);                                                                                                             //Natural: ASSIGN #B-PEND := CPR-TRANS-B.CNTRCT-PEND-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_After_Image() throws Exception                                                                                                                   //Natural: GET-AFTER-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Aftr_Key_Pnd_A_Cntrct.setValue(trans_Trans_Ppcn_Nbr);                                                                                                     //Natural: ASSIGN #A-CNTRCT := TRANS-PPCN-NBR
        pnd_Cpr_Aftr_Key_Pnd_A_Payee.setValue(trans_Trans_Payee_Cde);                                                                                                     //Natural: ASSIGN #A-PAYEE := TRANS-PAYEE-CDE
        pnd_Cpr_Aftr_Key_Pnd_A_Invrse_Dte.setValue(trans_Invrse_Trans_Dte);                                                                                               //Natural: ASSIGN #A-INVRSE-DTE := TRANS.INVRSE-TRANS-DTE
        pnd_Cpr_A_Found.setValue(true);                                                                                                                                   //Natural: ASSIGN #CPR-A-FOUND := TRUE
        vw_cpr_Trans_A.startDatabaseFind                                                                                                                                  //Natural: FIND CPR-TRANS-A WITH CPR-AFTR-KEY = #CPR-AFTR-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CPR_AFTR_KEY", "=", pnd_Cpr_Aftr_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_cpr_Trans_A.readNextRow("FIND02", true)))
        {
            vw_cpr_Trans_A.setIfNotFoundControlFlag(false);
            if (condition(vw_cpr_Trans_A.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO
            {
                pnd_Cpr_A_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #CPR-A-FOUND := FALSE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_A_Pend.setValue(cpr_Trans_A_Cntrct_Pend_Cde);                                                                                                             //Natural: ASSIGN #A-PEND := CPR-TRANS-A.CNTRCT-PEND-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Payment() throws Exception                                                                                                                       //Natural: GET-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct.setValue(trans_Trans_Ppcn_Nbr);                                                                                          //Natural: ASSIGN #FUND-CNTRCT := TRANS-PPCN-NBR
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee.setValue(trans_Trans_Payee_Cde);                                                                                          //Natural: ASSIGN #FUND-PAYEE := TRANS-PAYEE-CDE
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund_Cde.reset();                                                                                                              //Natural: RESET #CMPNY-FUND-CDE #TOT-PMT
        pnd_Tot_Pmt.reset();
        vw_fund.startDatabaseRead                                                                                                                                         //Natural: READ FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #TIAA-CNTRCT-FUND-KEY
        (
        "READ02",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Tiaa_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_fund.readNextRow("READ02")))
        {
            if (condition(fund_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct) || fund_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #FUND-CNTRCT OR TIAA-CNTRCT-PAYEE-CDE NE #FUND-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(fund_Tiaa_Cmpny_Fund_Cde,"'U'") || DbsUtil.maskMatches(fund_Tiaa_Cmpny_Fund_Cde,"'2'")))                                    //Natural: IF TIAA-CMPNY-FUND-CDE = MASK ( 'U' ) OR = MASK ( '2' )
            {
                fund_Tiaa_Tot_Div_Amt.reset();                                                                                                                            //Natural: RESET TIAA-TOT-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(fund_Tiaa_Cmpny_Fund_Cde,"'W'") || DbsUtil.maskMatches(fund_Tiaa_Cmpny_Fund_Cde,"'4'")))                                    //Natural: IF TIAA-CMPNY-FUND-CDE = MASK ( 'W' ) OR = MASK ( '4' )
            {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                sub_Get_Auv();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Pmt.compute(new ComputeParameters(false, pnd_Tot_Pmt), pnd_Tot_Pmt.add(fund_Tiaa_Tot_Per_Amt).add(fund_Tiaa_Tot_Div_Amt));                            //Natural: COMPUTE #TOT-PMT = #TOT-PMT + TIAA-TOT-PER-AMT + TIAA-TOT-DIV-AMT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(fund_Fund_Cde)))                                                                                        //Natural: IF +FUND-NUM-CDE ( #I ) = FUND-CDE
            {
                aian026_Pnd_Ia_Fund_Code.setValue(pls_Fund_Alpha_Cde.getValue(pnd_I));                                                                                    //Natural: ASSIGN #IA-FUND-CODE := +FUND-ALPHA-CDE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(aian026_Pnd_Ia_Fund_Code.equals(" ")))                                                                                                              //Natural: IF #IA-FUND-CODE = ' '
        {
            getReports().write(0, "Error: Invalid fund code for:",fund_Tiaa_Cntrct_Ppcn_Nbr,fund_Tiaa_Cntrct_Payee_Cde,fund_Tiaa_Cmpny_Fund_Cde);                         //Natural: WRITE 'Error: Invalid fund code for:' FUND.TIAA-CNTRCT-PPCN-NBR FUND.TIAA-CNTRCT-PAYEE-CDE FUND.TIAA-CMPNY-FUND-CDE
            if (Global.isEscape()) return;
            DbsUtil.terminate(77);  if (true) return;                                                                                                                     //Natural: TERMINATE 77
        }                                                                                                                                                                 //Natural: END-IF
        aian026_Pnd_Call_Type.setValue("P");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'P'
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        aian026_Pnd_Prtcptn_Dte.setValue(pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte);                                                                                     //Natural: ASSIGN #PRTCPTN-DTE := #TRANS-CHECK-DTE
        aian026_Pnd_Uv_Req_Dte.setValue(pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte);                                                                                      //Natural: ASSIGN #UV-REQ-DTE := #TRANS-CHECK-DTE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.equals(getZero())))                                                                                                             //Natural: IF #RTRN-CD = 0
        {
            fund_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(false, fund_Tiaa_Tot_Per_Amt), aian026_Pnd_Iuv.multiply(fund_Tiaa_Units_Cnt.getValue(1)));                //Natural: COMPUTE FUND.TIAA-TOT-PER-AMT = #IUV * FUND.TIAA-UNITS-CNT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Error in cntrct:",fund_Tiaa_Cntrct_Ppcn_Nbr,"Payee",fund_Tiaa_Cntrct_Payee_Cde,NEWLINE,"No unit value for fund:",aian026_Pnd_Ia_Fund_Code); //Natural: WRITE 'Error in cntrct:' FUND.TIAA-CNTRCT-PPCN-NBR 'Payee' FUND.TIAA-CNTRCT-PAYEE-CDE / 'No unit value for fund:' #IA-FUND-CODE
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"                   Suspend Code K Historical Report",NEWLINE,"                      for Check Cycle", //Natural: WRITE ( 1 ) NOTITLE NOHDR '                   Suspend Code K Historical Report' / '                      for Check Cycle' #PARM-DTE
                        pnd_Parm_Dte);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=150 PS=0");

        getReports().setDisplayColumns(1, "/Contract",
        		trans_Trans_Ppcn_Nbr, new AlphanumericLength (8),"/Py",
        		trans_Trans_Payee_Cde, new ReportEditMask ("99"),"Previous/Pend",
        		pnd_B_Pend,"Current/Pend",
        		pnd_A_Pend,"/Trans Dte",
        		trans_Trans_Dte, new ReportEditMask ("YYYYMMDD"),"/User",
        		trans_Trans_User_Id,"/Verifier",
        		trans_Trans_Verify_Id,"/Total Pymnt",
        		pnd_Tot_Pmt, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
    }
}
