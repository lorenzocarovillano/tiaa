/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:38 PM
**        * FROM NATURAL PROGRAM : Iaap701
************************************************************
**        * FILE NAME            : Iaap701.java
**        * CLASS NAME           : Iaap701
**        * INSTANCE NAME        : Iaap701
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP701                                           *
*                                                                    *
*   HISTORY :                                                        *
*         5/03    - CHANGES FOR EGTRRA PROCESS ADDED FIELDS IN       *
*                   GTN-PDA-I FOR LOB OF BUSINESS                    *
*                   DO SCAN ON 05/03 FOR CHANGES                     *
*                                                                    *
*           : 03/25/03    ADDED NEW FCPA140E ADDED NEW FIELDS TO     *
*                         GTN-PDA-E-ARRAY & GTN-PDA-I FOR SELF       *
*                         REMITTER & EGTTRA CHANGES                  *
*                         DO SCAN ON 3/03 FOR CHANGES                *
*           : 09/04/01    ADDED CNTRCT-AC-LT-10YRS (L) IN REC LAYOUT *
*                         DO SCAN ON 9/01 FOR CHANGES                *
*           : 08/07/98 KN NEW FCPA140E AND ARRAY                     *
*           : 04/03/98 RM LOGIC TO HANDLE INDX PROBLEM GT 6          *
*                         FOR--> GTN-PDA-E-ARRAY(/6)
*                         DO SCAN ON 4/98                            *
*           : 08/09/96 KN CHANGE HOLD CODE PROCESSING TO ONLY MODIFY *
*                         CHECKS,EFT,OR GLOBAL PAYMENTS TO CHECKS    *
*           : 08/20/96 KN RESET #INVALID-COMBINE                     *
*                         SET ERROR-CODE-1 ACROSS ALL COMBINED       *
*                         SET ERROR-CODE-2 FOR PENDS                 *
*           : 04/11/08 JT ADD ROTH FIELDS                            *
*           : 11/25/09 JT ADD CANADIAN PROVINCE CHECK TO PREVENT     *
*                         HOLD                                       *
*           : 01/08/10 JT ADD CODE FOR CANADIAN CURRENCY PER TOM MCGEE
*           : 01/12/12 JT ADD NEW FIELDS FOR SUNY/CUNY/DATA WAREHOUSE
*           : 08/2017  RC PIN EXPANSION
*           : 07/2020  RC REMOVE PYMNT-PAY-TYPE-REQ-IND(#INDX1) :=1
*           :             HANDLED BY OMNIPAY NOW. 20200724
**************************************1  *****************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap701 extends BLNatBase
{
    // Data Areas
    private PdaFcpa140e pdaFcpa140e;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup gtn_Pda_I;
    private DbsField gtn_Pda_I_Pymnt_Reqst_Log_Dte_Time;
    private DbsField gtn_Pda_I_Cntrct_Unq_Id_Nbr;
    private DbsField gtn_Pda_I_Cntrct_Ppcn_Nbr;
    private DbsField gtn_Pda_I_Cntrct_Cmbn_Nbr;
    private DbsField gtn_Pda_I_Annt_Soc_Sec_Ind;
    private DbsField gtn_Pda_I_Annt_Soc_Sec_Nbr;
    private DbsField gtn_Pda_I_Annt_Ctznshp_Cde;
    private DbsField gtn_Pda_I_Annt_Rsdncy_Cde;
    private DbsField gtn_Pda_I_Annt_Locality_Cde;

    private DbsGroup gtn_Pda_I_Ph_Name;
    private DbsField gtn_Pda_I_Ph_Last_Name;
    private DbsField gtn_Pda_I_Ph_Middle_Name;
    private DbsField gtn_Pda_I_Ph_First_Name;
    private DbsField gtn_Pda_I_Pymnt_Dob;

    private DbsGroup gtn_Pda_I_Pymnt_Nme_And_Addr_Grp;
    private DbsField gtn_Pda_I_Pymnt_Nme;
    private DbsField gtn_Pda_I_Pymnt_Addr_Lines;

    private DbsGroup gtn_Pda_I__R_Field_1;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line_Txt;

    private DbsGroup gtn_Pda_I__R_Field_2;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line1_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line2_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line3_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line4_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line5_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Line6_Txt;
    private DbsField gtn_Pda_I_Pymnt_Addr_Zip_Cde;
    private DbsField gtn_Pda_I_Pymnt_Postl_Data;
    private DbsField gtn_Pda_I_Pymnt_Addr_Type_Ind;
    private DbsField gtn_Pda_I_Pymnt_Foreign_Cde;
    private DbsField gtn_Pda_I_Pymnt_Addr_Last_Chg_Dte;
    private DbsField gtn_Pda_I_Pymnt_Addr_Last_Chg_Tme;
    private DbsField gtn_Pda_I_Pymnt_Addr_Chg_Ind;
    private DbsField gtn_Pda_I_Pymnt_Settl_Ivc_Ind;

    private DbsGroup gtn_Pda_I_Pnd_Pnd_Fund_Payee;
    private DbsField gtn_Pda_I_Inv_Acct_Cde;
    private DbsField gtn_Pda_I_Inv_Acct_Settl_Amt;
    private DbsField gtn_Pda_I_Inv_Acct_Cntrct_Amt;
    private DbsField gtn_Pda_I_Inv_Acct_Dvdnd_Amt;
    private DbsField gtn_Pda_I_Inv_Acct_Unit_Value;
    private DbsField gtn_Pda_I_Inv_Acct_Unit_Qty;
    private DbsField gtn_Pda_I_Inv_Acct_Ivc_Amt;
    private DbsField gtn_Pda_I_Inv_Acct_Ivc_Ind;
    private DbsField gtn_Pda_I_Inv_Acct_Valuat_Period;

    private DbsGroup gtn_Pda_I_Pnd_Pnd_Deductions;
    private DbsField gtn_Pda_I_Pymnt_Ded_Cde;
    private DbsField gtn_Pda_I_Pymnt_Ded_Amt;
    private DbsField gtn_Pda_I_Pymnt_Ded_Payee_Cde;

    private DbsGroup gtn_Pda_I__R_Field_3;
    private DbsField gtn_Pda_I_Pymnt_Ded_Payee_Cde_5;
    private DbsField gtn_Pda_I_Pymnt_Ded_Payee_Cde_3;
    private DbsField gtn_Pda_I_Pymnt_Payee_Tax_Ind;
    private DbsField gtn_Pda_I_Pymnt_Settlmnt_Dte;
    private DbsField gtn_Pda_I_Pymnt_Check_Dte;
    private DbsField gtn_Pda_I_Pymnt_Cycle_Dte;
    private DbsField gtn_Pda_I_Pymnt_Acctg_Dte;
    private DbsField gtn_Pda_I_Pymnt_Ia_Issue_Dte;
    private DbsField gtn_Pda_I_Pymnt_Intrfce_Dte;
    private DbsField gtn_Pda_I_Cntrct_Check_Crrncy_Cde;
    private DbsField gtn_Pda_I_Cntrct_Orgn_Cde;
    private DbsField gtn_Pda_I_Cntrct_Type_Cde;
    private DbsField gtn_Pda_I_Cntrct_Payee_Cde;
    private DbsField gtn_Pda_I_Cntrct_Option_Cde;
    private DbsField gtn_Pda_I_Cntrct_Ac_Lt_10yrs;
    private DbsField gtn_Pda_I_Cntrct_Mode_Cde;
    private DbsField gtn_Pda_I_Pymnt_Pay_Type_Req_Ind;
    private DbsField gtn_Pda_I_Pymnt_Spouse_Pay_Stats;
    private DbsField gtn_Pda_I_Cntrct_Pymnt_Type_Ind;
    private DbsField gtn_Pda_I_Cntrct_Sttlmnt_Type_Ind;
    private DbsField gtn_Pda_I_Cntrct_Pymnt_Dest_Cde;
    private DbsField gtn_Pda_I_Cntrct_Roll_Dest_Cde;
    private DbsField gtn_Pda_I_Cntrct_Dvdnd_Payee_Cde;
    private DbsField gtn_Pda_I_Pymnt_Eft_Acct_Nbr;
    private DbsField gtn_Pda_I_Pymnt_Eft_Transit_Id_A;

    private DbsGroup gtn_Pda_I__R_Field_4;
    private DbsField gtn_Pda_I_Pymnt_Eft_Transit_Id;
    private DbsField gtn_Pda_I_Pymnt_Chk_Sav_Ind;
    private DbsField gtn_Pda_I_Cntrct_Hold_Cde;
    private DbsField gtn_Pda_I_Cntrct_Hold_Ind;
    private DbsField gtn_Pda_I_Cntrct_Hold_Grp;
    private DbsField gtn_Pda_I_Cntrct_Hold_User_Id;
    private DbsField gtn_Pda_I_Pymnt_Tax_Exempt_Ind;
    private DbsField gtn_Pda_I_Cntrct_Qlfied_Cde;
    private DbsField gtn_Pda_I_Cntrct_Lob_Cde;
    private DbsField gtn_Pda_I_Cntrct_Sub_Lob_Cde;
    private DbsField gtn_Pda_I_Cntrct_Ia_Lob_Cde;
    private DbsField gtn_Pda_I_Cntrct_Annty_Ins_Type;
    private DbsField gtn_Pda_I_Cntrct_Annty_Type_Cde;
    private DbsField gtn_Pda_I_Cntrct_Insurance_Option;
    private DbsField gtn_Pda_I_Cntrct_Life_Contingency;
    private DbsField gtn_Pda_I_Pymnt_Suspend_Cde;
    private DbsField gtn_Pda_I_Pymnt_Suspend_Dte;
    private DbsField gtn_Pda_I_Pnd_Pnd_This_Pymnt;
    private DbsField gtn_Pda_I_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField gtn_Pda_I_Gtn_Ret_Code;

    private DbsGroup gtn_Pda_I__R_Field_5;
    private DbsField gtn_Pda_I_Global_Country;
    private DbsField gtn_Pda_I_Temp_Pend_Cde;
    private DbsField gtn_Pda_I_Error_Code_1;
    private DbsField gtn_Pda_I_Error_Code_2;
    private DbsField gtn_Pda_I_Current_Mode;
    private DbsField gtn_Pda_I_Egtrra_Eligibility_Ind;
    private DbsField gtn_Pda_I_Originating_Irs_Cde;

    private DbsGroup gtn_Pda_I__R_Field_6;
    private DbsField gtn_Pda_I_Distributing_Irc_Cde;
    private DbsField gtn_Pda_I_Receiving_Irc_Cde;
    private DbsField gtn_Pda_I_Cntrct_Money_Source;
    private DbsField gtn_Pda_I_Roth_Dob;
    private DbsField gtn_Pda_I_Roth_First_Contrib_Dte;
    private DbsField gtn_Pda_I_Roth_Death_Dte;
    private DbsField gtn_Pda_I_Roth_Disability_Dte;
    private DbsField gtn_Pda_I_Roth_Money_Source;
    private DbsField gtn_Pda_I_Roth_Qual_Non_Qual_Distrib;
    private DbsField gtn_Pda_I_Ia_Orgn_Cde;
    private DbsField gtn_Pda_I_Plan_Number;
    private DbsField gtn_Pda_I_Sub_Plan;
    private DbsField gtn_Pda_I_Orig_Cntrct_Nbr;
    private DbsField gtn_Pda_I_Orig_Sub_Plan;

    private DbsGroup gtn_Pda_E_Array;
    private DbsField gtn_Pda_E_Array_Pymnt_Corp_Wpid;
    private DbsField gtn_Pda_E_Array_Pymnt_Reqst_Log_Dte_Time;
    private DbsField gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr;
    private DbsField gtn_Pda_E_Array_Cntrct_Ppcn_Nbr;
    private DbsField gtn_Pda_E_Array_Cntrct_Cmbn_Nbr;
    private DbsField gtn_Pda_E_Array_Cntrct_Cref_Nbr;
    private DbsField gtn_Pda_E_Array_Annt_Soc_Sec_Ind;
    private DbsField gtn_Pda_E_Array_Annt_Soc_Sec_Nbr;
    private DbsField gtn_Pda_E_Array_Annt_Ctznshp_Cde;
    private DbsField gtn_Pda_E_Array_Annt_Rsdncy_Cde;
    private DbsField gtn_Pda_E_Array_Annt_Locality_Cde;

    private DbsGroup gtn_Pda_E_Array_Ph_Name;
    private DbsField gtn_Pda_E_Array_Ph_Last_Name;
    private DbsField gtn_Pda_E_Array_Ph_Middle_Name;
    private DbsField gtn_Pda_E_Array_Ph_First_Name;
    private DbsField gtn_Pda_E_Array_Pymnt_Dob;

    private DbsGroup gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp;
    private DbsField gtn_Pda_E_Array_Pymnt_Nme;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Lines;

    private DbsGroup gtn_Pda_E_Array__R_Field_7;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line_Txt;

    private DbsGroup gtn_Pda_E_Array__R_Field_8;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line1_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line2_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line3_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line4_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line5_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Line6_Txt;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Zip_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Postl_Data;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Type_Ind;
    private DbsField gtn_Pda_E_Array_Pymnt_Foreign_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte_X;

    private DbsGroup gtn_Pda_E_Array__R_Field_9;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme_X;

    private DbsGroup gtn_Pda_E_Array__R_Field_10;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme;
    private DbsField gtn_Pda_E_Array_Pymnt_Addr_Chg_Ind;
    private DbsField gtn_Pda_E_Array_Pymnt_Settl_Ivc_Ind;

    private DbsGroup gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Cde;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Amt;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Dpi_Amt;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Fund_Dpi_Amt;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Total_Check_Amt;

    private DbsGroup gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Cde;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Settl_Amt;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Cntrct_Amt;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Dvdnd_Amt;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Unit_Value;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Unit_Qty;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Ivc_Amt;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Ivc_Ind;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Adj_Ivc_Amt;
    private DbsField gtn_Pda_E_Array_Inv_Acct_Valuat_Period;

    private DbsGroup gtn_Pda_E_Array_Pnd_Pnd_Deductions;
    private DbsField gtn_Pda_E_Array_Pymnt_Ded_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Ded_Amt;
    private DbsField gtn_Pda_E_Array_Pymnt_Ded_Payee_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Payee_Tax_Ind;
    private DbsField gtn_Pda_E_Array_Pymnt_Settlmnt_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Check_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Cycle_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Acctg_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Ia_Issue_Dte;
    private DbsField gtn_Pda_E_Array_Pymnt_Intrfce_Dte;
    private DbsField gtn_Pda_E_Array_Cntrct_Check_Crrncy_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Orgn_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Type_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Payee_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Option_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Ac_Lt_10yrs;
    private DbsField gtn_Pda_E_Array_Cntrct_Mode_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Method_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind;
    private DbsField gtn_Pda_E_Array_Pymnt_Spouse_Pay_Stats;
    private DbsField gtn_Pda_E_Array_Cntrct_Pymnt_Type_Ind;
    private DbsField gtn_Pda_E_Array_Cntrct_Sttlmnt_Type_Ind;
    private DbsField gtn_Pda_E_Array_Cntrct_Pymnt_Dest_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Roll_Dest_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Dvdnd_Payee_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Eft_Acct_Nbr;
    private DbsField gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X;

    private DbsGroup gtn_Pda_E_Array__R_Field_11;
    private DbsField gtn_Pda_E_Array_Pymnt_Eft_Transit_Id;
    private DbsField gtn_Pda_E_Array_Pymnt_Chk_Sav_Ind;
    private DbsField gtn_Pda_E_Array_Cntrct_Hold_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Hold_Ind;
    private DbsField gtn_Pda_E_Array_Cntrct_Hold_Grp;
    private DbsField gtn_Pda_E_Array_Cntrct_Hold_User_Id;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Tx_Withholding_Tax_Types;

    private DbsGroup gtn_Pda_E_Array__R_Field_12;
    private DbsField gtn_Pda_E_Array_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_State_Tax_Code;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Local_Tax_Code;
    private DbsField gtn_Pda_E_Array_Pymnt_Tax_Exempt_Ind;
    private DbsField gtn_Pda_E_Array_Pymnt_Tax_Form;
    private DbsField gtn_Pda_E_Array_Pymnt_Tax_Calc_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Qlfied_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Lob_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Sub_Lob_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Ia_Lob_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Annty_Ins_Type;
    private DbsField gtn_Pda_E_Array_Cntrct_Annty_Type_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Insurance_Option;
    private DbsField gtn_Pda_E_Array_Cntrct_Life_Contingency;
    private DbsField gtn_Pda_E_Array_Pymnt_Suspend_Cde;
    private DbsField gtn_Pda_E_Array_Pymnt_Suspend_Dte;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Pda_Count;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt;
    private DbsField gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField gtn_Pda_E_Array_Gtn_Ret_Code;

    private DbsGroup gtn_Pda_E_Array__R_Field_13;
    private DbsField gtn_Pda_E_Array_Global_Country;
    private DbsField gtn_Pda_E_Array_Gtn_Ret_Msg;
    private DbsField gtn_Pda_E_Array_Temp_Pend_Cde;
    private DbsField gtn_Pda_E_Array_Error_Code_1;
    private DbsField gtn_Pda_E_Array_Error_Code_2;
    private DbsField gtn_Pda_E_Array_Current_Mode;
    private DbsField gtn_Pda_E_Array_Egtrra_Eligibility_Ind;
    private DbsField gtn_Pda_E_Array_Originating_Irs_Cde;

    private DbsGroup gtn_Pda_E_Array__R_Field_14;
    private DbsField gtn_Pda_E_Array_Distributing_Irc_Cde;
    private DbsField gtn_Pda_E_Array_Receiving_Irc_Cde;
    private DbsField gtn_Pda_E_Array_Cntrct_Money_Source;
    private DbsField gtn_Pda_E_Array_Roth_Dob;
    private DbsField gtn_Pda_E_Array_Roth_First_Contrib_Dte;
    private DbsField gtn_Pda_E_Array_Roth_Death_Dte;
    private DbsField gtn_Pda_E_Array_Roth_Disability_Dte;
    private DbsField gtn_Pda_E_Array_Roth_Money_Source;
    private DbsField gtn_Pda_E_Array_Roth_Qual_Non_Qual_Distrib;
    private DbsField gtn_Pda_E_Array_Ia_Orgn_Cde;
    private DbsField gtn_Pda_E_Array_Plan_Number;
    private DbsField gtn_Pda_E_Array_Sub_Plan;
    private DbsField gtn_Pda_E_Array_Orig_Cntrct_Nbr;
    private DbsField gtn_Pda_E_Array_Orig_Sub_Plan;
    private DbsField pnd_Cntrct_Cmbn_Nbr;

    private DbsGroup pnd_Cntrct_Cmbn_Nbr__R_Field_15;
    private DbsField pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Ctr;
    private DbsField pnd_Ctr_In;
    private DbsField pnd_Ctr_Out;
    private DbsField pnd_Indx;
    private DbsField pnd_Indx1;
    private DbsField pnd_Indx2;
    private DbsField pnd_Nbr_Of_Pymnts0;
    private DbsField pnd_Nbr_Of_Pymnts1;
    private DbsField pnd_This_Pymnt0;
    private DbsField pnd_This_Pymnt1;
    private DbsField pnd_Payment_Type;
    private DbsField pnd_First;
    private DbsField pnd_Combine;
    private DbsField pnd_Pend;
    private DbsField pnd_Active;
    private DbsField pnd_All_Terminated;
    private DbsField pnd_Save_Hold_Cde;
    private DbsField pnd_Save_Hold_Grp;
    private DbsField pnd_Save_Hold_User_Id;
    private DbsField pnd_Eft_Transit_Id;

    private DbsGroup pnd_Eft_Transit_Id__R_Field_16;
    private DbsField pnd_Eft_Transit_Id_Pnd_Eft_Id;
    private DbsField pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt;

    private DbsGroup pnd_Eft_Transit_Id__R_Field_17;
    private DbsField pnd_Eft_Transit_Id_Pnd_Eft_Id_A;
    private DbsField pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt_A;
    private DbsField pnd_Weight_Factor;

    private DbsGroup pnd_Weight_Factor__R_Field_18;
    private DbsField pnd_Weight_Factor_Pnd_Wt_Factor;
    private DbsField pnd_Weighted_Trans_Id;
    private DbsField pnd_Weighted_Sum;

    private DbsGroup pnd_Weighted_Sum__R_Field_19;
    private DbsField pnd_Weighted_Sum__Filler1;
    private DbsField pnd_Weighted_Sum_Pnd_Weighted_Digit;
    private DbsField pnd_W_Cmb_1_Ctr;
    private DbsField pnd_W_Inv_Cmbne_Ctr;
    private DbsField pnd_W_Uncmbne_Ctr;
    private DbsField pnd_Comments;
    private DbsField pnd_Invalid_Combine;
    private DbsField pnd_Count;
    private DbsField pnd_Rec_Cnt_In;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_Annt_Rsdncy_Cde;

    private DbsGroup pnd_Annt_Rsdncy_Cde__R_Field_20;
    private DbsField pnd_Annt_Rsdncy_Cde__Filler2;
    private DbsField pnd_Annt_Rsdncy_Cde_Pnd_Rsdncy_Cde;
    private DbsField pnd_Canada;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa140e = new PdaFcpa140e(localVariables);

        // Local Variables

        gtn_Pda_I = localVariables.newGroupInRecord("gtn_Pda_I", "GTN-PDA-I");
        gtn_Pda_I_Pymnt_Reqst_Log_Dte_Time = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        gtn_Pda_I_Cntrct_Unq_Id_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        gtn_Pda_I_Cntrct_Ppcn_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        gtn_Pda_I_Cntrct_Cmbn_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        gtn_Pda_I_Annt_Soc_Sec_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        gtn_Pda_I_Annt_Soc_Sec_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        gtn_Pda_I_Annt_Ctznshp_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_I_Annt_Rsdncy_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Annt_Locality_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);

        gtn_Pda_I_Ph_Name = gtn_Pda_I.newGroupInGroup("gtn_Pda_I_Ph_Name", "PH-NAME");
        gtn_Pda_I_Ph_Last_Name = gtn_Pda_I_Ph_Name.newFieldInGroup("gtn_Pda_I_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        gtn_Pda_I_Ph_Middle_Name = gtn_Pda_I_Ph_Name.newFieldInGroup("gtn_Pda_I_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        gtn_Pda_I_Ph_First_Name = gtn_Pda_I_Ph_Name.newFieldInGroup("gtn_Pda_I_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        gtn_Pda_I_Pymnt_Dob = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);

        gtn_Pda_I_Pymnt_Nme_And_Addr_Grp = gtn_Pda_I.newGroupArrayInGroup("gtn_Pda_I_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1, 
            2));
        gtn_Pda_I_Pymnt_Nme = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        gtn_Pda_I_Pymnt_Addr_Lines = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", FieldType.STRING, 
            210);

        gtn_Pda_I__R_Field_1 = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_I__R_Field_1", "REDEFINE", gtn_Pda_I_Pymnt_Addr_Lines);
        gtn_Pda_I_Pymnt_Addr_Line_Txt = gtn_Pda_I__R_Field_1.newFieldArrayInGroup("gtn_Pda_I_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));

        gtn_Pda_I__R_Field_2 = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_I__R_Field_2", "REDEFINE", gtn_Pda_I_Pymnt_Addr_Lines);
        gtn_Pda_I_Pymnt_Addr_Line1_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Line2_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Line3_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Line4_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Line5_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Line6_Txt = gtn_Pda_I__R_Field_2.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        gtn_Pda_I_Pymnt_Addr_Zip_Cde = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 
            9);
        gtn_Pda_I_Pymnt_Postl_Data = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        gtn_Pda_I_Pymnt_Addr_Type_Ind = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_I_Pymnt_Foreign_Cde = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", FieldType.STRING, 
            1);
        gtn_Pda_I_Pymnt_Addr_Last_Chg_Dte = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", 
            FieldType.NUMERIC, 8);
        gtn_Pda_I_Pymnt_Addr_Last_Chg_Tme = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", 
            FieldType.NUMERIC, 7);
        gtn_Pda_I_Pymnt_Addr_Chg_Ind = gtn_Pda_I_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_I_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 
            1);
        gtn_Pda_I_Pymnt_Settl_Ivc_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);

        gtn_Pda_I_Pnd_Pnd_Fund_Payee = gtn_Pda_I.newGroupArrayInGroup("gtn_Pda_I_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1, 40));
        gtn_Pda_I_Inv_Acct_Cde = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Inv_Acct_Settl_Amt = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_I_Inv_Acct_Cntrct_Amt = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_I_Inv_Acct_Dvdnd_Amt = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_I_Inv_Acct_Unit_Value = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4);
        gtn_Pda_I_Inv_Acct_Unit_Qty = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        gtn_Pda_I_Inv_Acct_Ivc_Amt = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_I_Inv_Acct_Ivc_Ind = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        gtn_Pda_I_Inv_Acct_Valuat_Period = gtn_Pda_I_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_I_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);

        gtn_Pda_I_Pnd_Pnd_Deductions = gtn_Pda_I.newGroupArrayInGroup("gtn_Pda_I_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1, 10));
        gtn_Pda_I_Pymnt_Ded_Cde = gtn_Pda_I_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_I_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_I_Pymnt_Ded_Amt = gtn_Pda_I_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_I_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        gtn_Pda_I_Pymnt_Ded_Payee_Cde = gtn_Pda_I_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_I_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8);

        gtn_Pda_I__R_Field_3 = gtn_Pda_I_Pnd_Pnd_Deductions.newGroupInGroup("gtn_Pda_I__R_Field_3", "REDEFINE", gtn_Pda_I_Pymnt_Ded_Payee_Cde);
        gtn_Pda_I_Pymnt_Ded_Payee_Cde_5 = gtn_Pda_I__R_Field_3.newFieldInGroup("gtn_Pda_I_Pymnt_Ded_Payee_Cde_5", "PYMNT-DED-PAYEE-CDE-5", FieldType.STRING, 
            5);
        gtn_Pda_I_Pymnt_Ded_Payee_Cde_3 = gtn_Pda_I__R_Field_3.newFieldInGroup("gtn_Pda_I_Pymnt_Ded_Payee_Cde_3", "PYMNT-DED-PAYEE-CDE-3", FieldType.STRING, 
            3);
        gtn_Pda_I_Pymnt_Payee_Tax_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Payee_Tax_Ind", "PYMNT-PAYEE-TAX-IND", FieldType.STRING, 1);
        gtn_Pda_I_Pymnt_Settlmnt_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        gtn_Pda_I_Pymnt_Check_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        gtn_Pda_I_Pymnt_Cycle_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        gtn_Pda_I_Pymnt_Acctg_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        gtn_Pda_I_Pymnt_Ia_Issue_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        gtn_Pda_I_Pymnt_Intrfce_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        gtn_Pda_I_Cntrct_Check_Crrncy_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);
        gtn_Pda_I_Cntrct_Orgn_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Cntrct_Type_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Cntrct_Payee_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Option_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_I_Cntrct_Ac_Lt_10yrs = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 1);
        gtn_Pda_I_Cntrct_Mode_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_I_Pymnt_Pay_Type_Req_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_I_Pymnt_Spouse_Pay_Stats = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Pymnt_Type_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Sttlmnt_Type_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_I_Cntrct_Pymnt_Dest_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Roll_Dest_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Dvdnd_Payee_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        gtn_Pda_I_Pymnt_Eft_Acct_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        gtn_Pda_I_Pymnt_Eft_Transit_Id_A = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Eft_Transit_Id_A", "PYMNT-EFT-TRANSIT-ID-A", FieldType.STRING, 9);

        gtn_Pda_I__R_Field_4 = gtn_Pda_I.newGroupInGroup("gtn_Pda_I__R_Field_4", "REDEFINE", gtn_Pda_I_Pymnt_Eft_Transit_Id_A);
        gtn_Pda_I_Pymnt_Eft_Transit_Id = gtn_Pda_I__R_Field_4.newFieldInGroup("gtn_Pda_I_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9);
        gtn_Pda_I_Pymnt_Chk_Sav_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Hold_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Hold_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Hold_Grp = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        gtn_Pda_I_Cntrct_Hold_User_Id = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 3);
        gtn_Pda_I_Pymnt_Tax_Exempt_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Qlfied_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Lob_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Sub_Lob_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_I_Cntrct_Ia_Lob_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Cntrct_Annty_Ins_Type = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Annty_Type_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        gtn_Pda_I_Cntrct_Insurance_Option = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        gtn_Pda_I_Cntrct_Life_Contingency = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        gtn_Pda_I_Pymnt_Suspend_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 1);
        gtn_Pda_I_Pymnt_Suspend_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", FieldType.DATE);
        gtn_Pda_I_Pnd_Pnd_This_Pymnt = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 2);
        gtn_Pda_I_Pnd_Pnd_Nbr_Of_Pymnts = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 2);
        gtn_Pda_I_Gtn_Ret_Code = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);

        gtn_Pda_I__R_Field_5 = gtn_Pda_I.newGroupInGroup("gtn_Pda_I__R_Field_5", "REDEFINE", gtn_Pda_I_Gtn_Ret_Code);
        gtn_Pda_I_Global_Country = gtn_Pda_I__R_Field_5.newFieldInGroup("gtn_Pda_I_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        gtn_Pda_I_Temp_Pend_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Temp_Pend_Cde", "TEMP-PEND-CDE", FieldType.STRING, 1);
        gtn_Pda_I_Error_Code_1 = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        gtn_Pda_I_Error_Code_2 = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        gtn_Pda_I_Current_Mode = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Current_Mode", "CURRENT-MODE", FieldType.STRING, 1);
        gtn_Pda_I_Egtrra_Eligibility_Ind = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        gtn_Pda_I_Originating_Irs_Cde = gtn_Pda_I.newFieldArrayInGroup("gtn_Pda_I_Originating_Irs_Cde", "ORIGINATING-IRS-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1, 8));

        gtn_Pda_I__R_Field_6 = gtn_Pda_I.newGroupInGroup("gtn_Pda_I__R_Field_6", "REDEFINE", gtn_Pda_I_Originating_Irs_Cde);
        gtn_Pda_I_Distributing_Irc_Cde = gtn_Pda_I__R_Field_6.newFieldInGroup("gtn_Pda_I_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        gtn_Pda_I_Receiving_Irc_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Cntrct_Money_Source = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_I_Roth_Dob = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        gtn_Pda_I_Roth_First_Contrib_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_I_Roth_Death_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_I_Roth_Disability_Dte = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_I_Roth_Money_Source = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_I_Roth_Qual_Non_Qual_Distrib = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", FieldType.STRING, 
            1);
        gtn_Pda_I_Ia_Orgn_Cde = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_I_Plan_Number = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Plan_Number", "PLAN-NUMBER", FieldType.STRING, 6);
        gtn_Pda_I_Sub_Plan = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Sub_Plan", "SUB-PLAN", FieldType.STRING, 6);
        gtn_Pda_I_Orig_Cntrct_Nbr = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Orig_Cntrct_Nbr", "ORIG-CNTRCT-NBR", FieldType.STRING, 10);
        gtn_Pda_I_Orig_Sub_Plan = gtn_Pda_I.newFieldInGroup("gtn_Pda_I_Orig_Sub_Plan", "ORIG-SUB-PLAN", FieldType.STRING, 6);

        gtn_Pda_E_Array = localVariables.newGroupArrayInRecord("gtn_Pda_E_Array", "GTN-PDA-E-ARRAY", new DbsArrayController(1, 6));
        gtn_Pda_E_Array_Pymnt_Corp_Wpid = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        gtn_Pda_E_Array_Pymnt_Reqst_Log_Dte_Time = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        gtn_Pda_E_Array_Cntrct_Ppcn_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Array_Cntrct_Cmbn_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        gtn_Pda_E_Array_Cntrct_Cref_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Array_Annt_Soc_Sec_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_E_Array_Annt_Soc_Sec_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        gtn_Pda_E_Array_Annt_Ctznshp_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        gtn_Pda_E_Array_Annt_Rsdncy_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Array_Annt_Locality_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 
            2);

        gtn_Pda_E_Array_Ph_Name = gtn_Pda_E_Array.newGroupInGroup("gtn_Pda_E_Array_Ph_Name", "PH-NAME");
        gtn_Pda_E_Array_Ph_Last_Name = gtn_Pda_E_Array_Ph_Name.newFieldInGroup("gtn_Pda_E_Array_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        gtn_Pda_E_Array_Ph_Middle_Name = gtn_Pda_E_Array_Ph_Name.newFieldInGroup("gtn_Pda_E_Array_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            12);
        gtn_Pda_E_Array_Ph_First_Name = gtn_Pda_E_Array_Ph_Name.newFieldInGroup("gtn_Pda_E_Array_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        gtn_Pda_E_Array_Pymnt_Dob = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);

        gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp = gtn_Pda_E_Array.newGroupArrayInGroup("gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", 
            new DbsArrayController(1, 2));
        gtn_Pda_E_Array_Pymnt_Nme = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        gtn_Pda_E_Array_Pymnt_Addr_Lines = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", 
            FieldType.STRING, 210);

        gtn_Pda_E_Array__R_Field_7 = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Array__R_Field_7", "REDEFINE", gtn_Pda_E_Array_Pymnt_Addr_Lines);
        gtn_Pda_E_Array_Pymnt_Addr_Line_Txt = gtn_Pda_E_Array__R_Field_7.newFieldArrayInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 6));

        gtn_Pda_E_Array__R_Field_8 = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Array__R_Field_8", "REDEFINE", gtn_Pda_E_Array_Pymnt_Addr_Lines);
        gtn_Pda_E_Array_Pymnt_Addr_Line1_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Line2_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Line3_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Line4_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Line5_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Line6_Txt = gtn_Pda_E_Array__R_Field_8.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        gtn_Pda_E_Array_Pymnt_Addr_Zip_Cde = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", 
            FieldType.STRING, 9);
        gtn_Pda_E_Array_Pymnt_Postl_Data = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32);
        gtn_Pda_E_Array_Pymnt_Addr_Type_Ind = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Foreign_Cde = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte_X = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte_X", 
            "PYMNT-ADDR-LAST-CHG-DTE-X", FieldType.STRING, 8);

        gtn_Pda_E_Array__R_Field_9 = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Array__R_Field_9", "REDEFINE", gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte_X);
        gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte = gtn_Pda_E_Array__R_Field_9.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", 
            FieldType.NUMERIC, 8);
        gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme_X = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme_X", 
            "PYMNT-ADDR-LAST-CHG-TME-X", FieldType.STRING, 7);

        gtn_Pda_E_Array__R_Field_10 = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E_Array__R_Field_10", "REDEFINE", gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme_X);
        gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme = gtn_Pda_E_Array__R_Field_10.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", 
            FieldType.NUMERIC, 7);
        gtn_Pda_E_Array_Pymnt_Addr_Chg_Ind = gtn_Pda_E_Array_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Settl_Ivc_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 
            1);

        gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt = gtn_Pda_E_Array.newGroupArrayInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt", "##FUND-SETTLMNT", new DbsArrayController(1, 
            40));
        gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Cde = gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Cde", "##FUND-SETTL-CDE", 
            FieldType.STRING, 2);
        gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Amt", "##FUND-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Dpi_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Settl_Dpi_Amt", 
            "##FUND-SETTL-DPI-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Array_Pnd_Pnd_Fund_Dpi_Amt = gtn_Pda_E_Array.newFieldArrayInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Dpi_Amt", "##FUND-DPI-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 40));
        gtn_Pda_E_Array_Pnd_Pnd_Total_Check_Amt = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Total_Check_Amt", "##TOTAL-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee = gtn_Pda_E_Array.newGroupArrayInGroup("gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1, 
            40));
        gtn_Pda_E_Array_Inv_Acct_Cde = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Array_Inv_Acct_Settl_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Array_Inv_Acct_Cntrct_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Array_Inv_Acct_Dvdnd_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Array_Inv_Acct_Unit_Value = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 4);
        gtn_Pda_E_Array_Inv_Acct_Unit_Qty = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", 
            FieldType.PACKED_DECIMAL, 9, 3);
        gtn_Pda_E_Array_Inv_Acct_Ivc_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        gtn_Pda_E_Array_Inv_Acct_Ivc_Ind = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Inv_Acct_Adj_Ivc_Amt = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        gtn_Pda_E_Array_Inv_Acct_Valuat_Period = gtn_Pda_E_Array_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Array_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);

        gtn_Pda_E_Array_Pnd_Pnd_Deductions = gtn_Pda_E_Array.newGroupArrayInGroup("gtn_Pda_E_Array_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1, 
            10));
        gtn_Pda_E_Array_Pymnt_Ded_Cde = gtn_Pda_E_Array_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3);
        gtn_Pda_E_Array_Pymnt_Ded_Amt = gtn_Pda_E_Array_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_E_Array_Pymnt_Ded_Payee_Cde = gtn_Pda_E_Array_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8);
        gtn_Pda_E_Array_Pymnt_Payee_Tax_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Payee_Tax_Ind", "PYMNT-PAYEE-TAX-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Pymnt_Settlmnt_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pymnt_Check_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pymnt_Cycle_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pymnt_Acctg_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pymnt_Ia_Issue_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pymnt_Intrfce_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Cntrct_Check_Crrncy_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Cntrct_Orgn_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Array_Cntrct_Type_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Array_Cntrct_Payee_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Array_Cntrct_Option_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        gtn_Pda_E_Array_Cntrct_Ac_Lt_10yrs = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 
            1);
        gtn_Pda_E_Array_Cntrct_Mode_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Array_Pymnt_Method_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_E_Array_Pymnt_Spouse_Pay_Stats = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Pymnt_Type_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Sttlmnt_Type_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Cntrct_Pymnt_Dest_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 
            4);
        gtn_Pda_E_Array_Cntrct_Roll_Dest_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 
            4);
        gtn_Pda_E_Array_Cntrct_Dvdnd_Payee_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 
            5);
        gtn_Pda_E_Array_Pymnt_Eft_Acct_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 
            21);
        gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X", "PYMNT-EFT-TRANSIT-ID-X", FieldType.STRING, 
            9);

        gtn_Pda_E_Array__R_Field_11 = gtn_Pda_E_Array.newGroupInGroup("gtn_Pda_E_Array__R_Field_11", "REDEFINE", gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X);
        gtn_Pda_E_Array_Pymnt_Eft_Transit_Id = gtn_Pda_E_Array__R_Field_11.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        gtn_Pda_E_Array_Pymnt_Chk_Sav_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Hold_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Array_Cntrct_Hold_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        gtn_Pda_E_Array_Cntrct_Hold_Grp = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        gtn_Pda_E_Array_Cntrct_Hold_User_Id = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 
            3);
        gtn_Pda_E_Array_Pnd_Pnd_Tx_Withholding_Tax_Types = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Tx_Withholding_Tax_Types", "##TX-WITHHOLDING-TAX-TYPES", 
            FieldType.STRING, 3);

        gtn_Pda_E_Array__R_Field_12 = gtn_Pda_E_Array.newGroupInGroup("gtn_Pda_E_Array__R_Field_12", "REDEFINE", gtn_Pda_E_Array_Pnd_Pnd_Tx_Withholding_Tax_Types);
        gtn_Pda_E_Array_Pymnt_Payee_Tx_Elct_Trggr = gtn_Pda_E_Array__R_Field_12.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pnd_Pnd_State_Tax_Code = gtn_Pda_E_Array__R_Field_12.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_State_Tax_Code", "##STATE-TAX-CODE", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pnd_Pnd_Local_Tax_Code = gtn_Pda_E_Array__R_Field_12.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Local_Tax_Code", "##LOCAL-TAX-CODE", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Tax_Exempt_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Pymnt_Tax_Form = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        gtn_Pda_E_Array_Pymnt_Tax_Calc_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Array_Cntrct_Qlfied_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Lob_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Array_Cntrct_Sub_Lob_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 
            4);
        gtn_Pda_E_Array_Cntrct_Ia_Lob_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Array_Cntrct_Annty_Ins_Type = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Annty_Type_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Cntrct_Insurance_Option = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Cntrct_Life_Contingency = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Pymnt_Suspend_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Pymnt_Suspend_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", FieldType.DATE);
        gtn_Pda_E_Array_Pnd_Pnd_Pda_Count = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Pda_Count", "##PDA-COUNT", FieldType.PACKED_DECIMAL, 
            2);
        gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 
            2);
        gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 
            2);
        gtn_Pda_E_Array_Gtn_Ret_Code = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);

        gtn_Pda_E_Array__R_Field_13 = gtn_Pda_E_Array.newGroupInGroup("gtn_Pda_E_Array__R_Field_13", "REDEFINE", gtn_Pda_E_Array_Gtn_Ret_Code);
        gtn_Pda_E_Array_Global_Country = gtn_Pda_E_Array__R_Field_13.newFieldInGroup("gtn_Pda_E_Array_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 
            3);
        gtn_Pda_E_Array_Gtn_Ret_Msg = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Gtn_Ret_Msg", "GTN-RET-MSG", FieldType.STRING, 60);
        gtn_Pda_E_Array_Temp_Pend_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Temp_Pend_Cde", "TEMP-PEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Array_Error_Code_1 = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        gtn_Pda_E_Array_Error_Code_2 = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        gtn_Pda_E_Array_Current_Mode = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Current_Mode", "CURRENT-MODE", FieldType.STRING, 1);
        gtn_Pda_E_Array_Egtrra_Eligibility_Ind = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Array_Originating_Irs_Cde = gtn_Pda_E_Array.newFieldArrayInGroup("gtn_Pda_E_Array_Originating_Irs_Cde", "ORIGINATING-IRS-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 8));

        gtn_Pda_E_Array__R_Field_14 = gtn_Pda_E_Array.newGroupInGroup("gtn_Pda_E_Array__R_Field_14", "REDEFINE", gtn_Pda_E_Array_Originating_Irs_Cde);
        gtn_Pda_E_Array_Distributing_Irc_Cde = gtn_Pda_E_Array__R_Field_14.newFieldInGroup("gtn_Pda_E_Array_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", 
            FieldType.STRING, 16);
        gtn_Pda_E_Array_Receiving_Irc_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Array_Cntrct_Money_Source = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 
            5);
        gtn_Pda_E_Array_Roth_Dob = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        gtn_Pda_E_Array_Roth_First_Contrib_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Array_Roth_Death_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Array_Roth_Disability_Dte = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Array_Roth_Money_Source = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 
            5);
        gtn_Pda_E_Array_Roth_Qual_Non_Qual_Distrib = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", 
            FieldType.STRING, 1);
        gtn_Pda_E_Array_Ia_Orgn_Cde = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Array_Plan_Number = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Plan_Number", "PLAN-NUMBER", FieldType.STRING, 6);
        gtn_Pda_E_Array_Sub_Plan = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Sub_Plan", "SUB-PLAN", FieldType.STRING, 6);
        gtn_Pda_E_Array_Orig_Cntrct_Nbr = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Orig_Cntrct_Nbr", "ORIG-CNTRCT-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Array_Orig_Sub_Plan = gtn_Pda_E_Array.newFieldInGroup("gtn_Pda_E_Array_Orig_Sub_Plan", "ORIG-SUB-PLAN", FieldType.STRING, 6);
        pnd_Cntrct_Cmbn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Cmbn_Nbr", "#CNTRCT-CMBN-NBR", FieldType.STRING, 14);

        pnd_Cntrct_Cmbn_Nbr__R_Field_15 = localVariables.newGroupInRecord("pnd_Cntrct_Cmbn_Nbr__R_Field_15", "REDEFINE", pnd_Cntrct_Cmbn_Nbr);
        pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Cmbn_Nbr__R_Field_15.newFieldInGroup("pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Cmbn_Nbr__R_Field_15.newFieldInGroup("pnd_Cntrct_Cmbn_Nbr_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_In = localVariables.newFieldInRecord("pnd_Ctr_In", "#CTR-IN", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Out = localVariables.newFieldInRecord("pnd_Ctr_Out", "#CTR-OUT", FieldType.PACKED_DECIMAL, 9);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 2);
        pnd_Indx1 = localVariables.newFieldInRecord("pnd_Indx1", "#INDX1", FieldType.NUMERIC, 2);
        pnd_Indx2 = localVariables.newFieldInRecord("pnd_Indx2", "#INDX2", FieldType.NUMERIC, 2);
        pnd_Nbr_Of_Pymnts0 = localVariables.newFieldInRecord("pnd_Nbr_Of_Pymnts0", "#NBR-OF-PYMNTS0", FieldType.NUMERIC, 2);
        pnd_Nbr_Of_Pymnts1 = localVariables.newFieldInRecord("pnd_Nbr_Of_Pymnts1", "#NBR-OF-PYMNTS1", FieldType.NUMERIC, 2);
        pnd_This_Pymnt0 = localVariables.newFieldInRecord("pnd_This_Pymnt0", "#THIS-PYMNT0", FieldType.NUMERIC, 2);
        pnd_This_Pymnt1 = localVariables.newFieldInRecord("pnd_This_Pymnt1", "#THIS-PYMNT1", FieldType.NUMERIC, 2);
        pnd_Payment_Type = localVariables.newFieldInRecord("pnd_Payment_Type", "#PAYMENT-TYPE", FieldType.STRING, 15);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Combine = localVariables.newFieldInRecord("pnd_Combine", "#COMBINE", FieldType.BOOLEAN, 1);
        pnd_Pend = localVariables.newFieldInRecord("pnd_Pend", "#PEND", FieldType.BOOLEAN, 1);
        pnd_Active = localVariables.newFieldInRecord("pnd_Active", "#ACTIVE", FieldType.BOOLEAN, 1);
        pnd_All_Terminated = localVariables.newFieldInRecord("pnd_All_Terminated", "#ALL-TERMINATED", FieldType.BOOLEAN, 1);
        pnd_Save_Hold_Cde = localVariables.newFieldInRecord("pnd_Save_Hold_Cde", "#SAVE-HOLD-CDE", FieldType.STRING, 4);
        pnd_Save_Hold_Grp = localVariables.newFieldInRecord("pnd_Save_Hold_Grp", "#SAVE-HOLD-GRP", FieldType.STRING, 3);
        pnd_Save_Hold_User_Id = localVariables.newFieldInRecord("pnd_Save_Hold_User_Id", "#SAVE-HOLD-USER-ID", FieldType.STRING, 3);
        pnd_Eft_Transit_Id = localVariables.newFieldInRecord("pnd_Eft_Transit_Id", "#EFT-TRANSIT-ID", FieldType.STRING, 9);

        pnd_Eft_Transit_Id__R_Field_16 = localVariables.newGroupInRecord("pnd_Eft_Transit_Id__R_Field_16", "REDEFINE", pnd_Eft_Transit_Id);
        pnd_Eft_Transit_Id_Pnd_Eft_Id = pnd_Eft_Transit_Id__R_Field_16.newFieldArrayInGroup("pnd_Eft_Transit_Id_Pnd_Eft_Id", "#EFT-ID", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 8));
        pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt = pnd_Eft_Transit_Id__R_Field_16.newFieldInGroup("pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt", "#EFT-CHK-DGT", FieldType.NUMERIC, 
            1);

        pnd_Eft_Transit_Id__R_Field_17 = localVariables.newGroupInRecord("pnd_Eft_Transit_Id__R_Field_17", "REDEFINE", pnd_Eft_Transit_Id);
        pnd_Eft_Transit_Id_Pnd_Eft_Id_A = pnd_Eft_Transit_Id__R_Field_17.newFieldArrayInGroup("pnd_Eft_Transit_Id_Pnd_Eft_Id_A", "#EFT-ID-A", FieldType.STRING, 
            1, new DbsArrayController(1, 8));
        pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt_A = pnd_Eft_Transit_Id__R_Field_17.newFieldInGroup("pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt_A", "#EFT-CHK-DGT-A", 
            FieldType.STRING, 1);
        pnd_Weight_Factor = localVariables.newFieldInRecord("pnd_Weight_Factor", "#WEIGHT-FACTOR", FieldType.NUMERIC, 8);

        pnd_Weight_Factor__R_Field_18 = localVariables.newGroupInRecord("pnd_Weight_Factor__R_Field_18", "REDEFINE", pnd_Weight_Factor);
        pnd_Weight_Factor_Pnd_Wt_Factor = pnd_Weight_Factor__R_Field_18.newFieldArrayInGroup("pnd_Weight_Factor_Pnd_Wt_Factor", "#WT-FACTOR", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 8));
        pnd_Weighted_Trans_Id = localVariables.newFieldArrayInRecord("pnd_Weighted_Trans_Id", "#WEIGHTED-TRANS-ID", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            8));
        pnd_Weighted_Sum = localVariables.newFieldInRecord("pnd_Weighted_Sum", "#WEIGHTED-SUM", FieldType.NUMERIC, 3);

        pnd_Weighted_Sum__R_Field_19 = localVariables.newGroupInRecord("pnd_Weighted_Sum__R_Field_19", "REDEFINE", pnd_Weighted_Sum);
        pnd_Weighted_Sum__Filler1 = pnd_Weighted_Sum__R_Field_19.newFieldInGroup("pnd_Weighted_Sum__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_Weighted_Sum_Pnd_Weighted_Digit = pnd_Weighted_Sum__R_Field_19.newFieldInGroup("pnd_Weighted_Sum_Pnd_Weighted_Digit", "#WEIGHTED-DIGIT", FieldType.NUMERIC, 
            1);
        pnd_W_Cmb_1_Ctr = localVariables.newFieldInRecord("pnd_W_Cmb_1_Ctr", "#W-CMB-1-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_W_Inv_Cmbne_Ctr = localVariables.newFieldInRecord("pnd_W_Inv_Cmbne_Ctr", "#W-INV-CMBNE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_W_Uncmbne_Ctr = localVariables.newFieldInRecord("pnd_W_Uncmbne_Ctr", "#W-UNCMBNE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Comments = localVariables.newFieldInRecord("pnd_Comments", "#COMMENTS", FieldType.STRING, 24);
        pnd_Invalid_Combine = localVariables.newFieldInRecord("pnd_Invalid_Combine", "#INVALID-COMBINE", FieldType.BOOLEAN, 1);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 5);
        pnd_Rec_Cnt_In = localVariables.newFieldInRecord("pnd_Rec_Cnt_In", "#REC-CNT-IN", FieldType.NUMERIC, 9);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_Annt_Rsdncy_Cde = localVariables.newFieldInRecord("pnd_Annt_Rsdncy_Cde", "#ANNT-RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Annt_Rsdncy_Cde__R_Field_20 = localVariables.newGroupInRecord("pnd_Annt_Rsdncy_Cde__R_Field_20", "REDEFINE", pnd_Annt_Rsdncy_Cde);
        pnd_Annt_Rsdncy_Cde__Filler2 = pnd_Annt_Rsdncy_Cde__R_Field_20.newFieldInGroup("pnd_Annt_Rsdncy_Cde__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Annt_Rsdncy_Cde_Pnd_Rsdncy_Cde = pnd_Annt_Rsdncy_Cde__R_Field_20.newFieldInGroup("pnd_Annt_Rsdncy_Cde_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Canada = localVariables.newFieldArrayInRecord("pnd_Canada", "#CANADA", FieldType.STRING, 2, new DbsArrayController(1, 14));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Weight_Factor.setInitialValue(37137137);
        pnd_Annt_Rsdncy_Cde.setInitialValue("000");
        pnd_Canada.getValue(1).setInitialValue("74");
        pnd_Canada.getValue(2).setInitialValue("75");
        pnd_Canada.getValue(3).setInitialValue("76");
        pnd_Canada.getValue(4).setInitialValue("77");
        pnd_Canada.getValue(5).setInitialValue("78");
        pnd_Canada.getValue(6).setInitialValue("79");
        pnd_Canada.getValue(7).setInitialValue("80");
        pnd_Canada.getValue(8).setInitialValue("81");
        pnd_Canada.getValue(9).setInitialValue("82");
        pnd_Canada.getValue(10).setInitialValue("83");
        pnd_Canada.getValue(11).setInitialValue("84");
        pnd_Canada.getValue(12).setInitialValue("85");
        pnd_Canada.getValue(13).setInitialValue("86");
        pnd_Canada.getValue(14).setInitialValue("96");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap701() throws Exception
    {
        super("Iaap701");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP701", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 GTN-PDA-I
        while (condition(getWorkFiles().read(1, gtn_Pda_I)))
        {
            pdaFcpa140e.getGtn_Pda_E().setValuesByName(gtn_Pda_I);                                                                                                        //Natural: MOVE BY NAME GTN-PDA-I TO GTN-PDA-E
            //*  ADDED 4/98
            pnd_Rec_Cnt_In.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-CNT-IN
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                gtn_Pda_E_Array.getValue(1).setValuesByName(pdaFcpa140e.getGtn_Pda_E());                                                                                  //Natural: MOVE BY NAME GTN-PDA-E TO GTN-PDA-E-ARRAY ( 1 )
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Indx.setValue(1);                                                                                                                                     //Natural: ASSIGN #INDX := 1
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  IK 3/3/2000
            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr().getSubstring(1,10).equals(gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(1).getSubstring(1,                //Natural: IF SUBSTR ( GTN-PDA-E.CNTRCT-CMBN-NBR,1,10 ) = SUBSTR ( GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( 1 ) ,1,10 )
                10))))
            {
                if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Unq_Id_Nbr().equals(getZero())))                                                                            //Natural: IF GTN-PDA-E.CNTRCT-UNQ-ID-NBR = 0
                {
                    pdaFcpa140e.getGtn_Pda_E_Cntrct_Unq_Id_Nbr().setValue(gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr.getValue(1));                                                 //Natural: MOVE GTN-PDA-E-ARRAY.CNTRCT-UNQ-ID-NBR ( 1 ) TO GTN-PDA-E.CNTRCT-UNQ-ID-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr.getValue(1).equals(getZero())))                                                                           //Natural: IF GTN-PDA-E-ARRAY.CNTRCT-UNQ-ID-NBR ( 1 ) = 0
                {
                    gtn_Pda_E_Array_Cntrct_Unq_Id_Nbr.getValue(1).setValue(pdaFcpa140e.getGtn_Pda_E_Cntrct_Unq_Id_Nbr());                                                 //Natural: MOVE GTN-PDA-E.CNTRCT-UNQ-ID-NBR TO GTN-PDA-E-ARRAY.CNTRCT-UNQ-ID-NBR ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 4/98 TO HANDLE INDX GT 6
            //*  ADDED 4/98 TO HANDLE INDX GT 6
            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr().equals(gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(1)) && pnd_Indx.less(6)))                            //Natural: IF GTN-PDA-E.CNTRCT-CMBN-NBR = GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( 1 ) AND #INDX LT 6
            {
                pnd_Indx.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #INDX
                gtn_Pda_E_Array.getValue(pnd_Indx).setValuesByName(pdaFcpa140e.getGtn_Pda_E());                                                                           //Natural: MOVE BY NAME GTN-PDA-E TO GTN-PDA-E-ARRAY ( #INDX )
                pnd_Combine.setValue(true);                                                                                                                               //Natural: ASSIGN #COMBINE := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ADDED FOLLOWING TO HANDLE INDX GT 6   4/98
                //*  ADDED 4/98 TO HANDLE INDX GT 6
                if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr().equals(gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(1)) && pnd_Indx.greater(5)))                     //Natural: IF GTN-PDA-E.CNTRCT-CMBN-NBR = GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( 1 ) AND #INDX GT 5
                {
                    getReports().write(0, "INDX COMBN GT 6  ",Global.getPROGRAM()," FOR CONTRACT #: ",pdaFcpa140e.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde()); //Natural: WRITE 'INDX COMBN GT 6  ' *PROGRAM ' FOR CONTRACT #: ' GTN-PDA-E.CNTRCT-PPCN-NBR GTN-PDA-E.CNTRCT-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Indx);                                                                                                                  //Natural: WRITE '=' #INDX
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ADDED 4/98
                    getReports().write(0, "=",pnd_Rec_Cnt_In, new ReportEditMask ("ZZZ,ZZZ,ZZZ"));                                                                        //Natural: WRITE '=' #REC-CNT-IN ( EM = ZZZ,ZZZ,ZZZ )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "GTN-PDA-E.CNTRCT-CMBN-NBR =",pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr());                                                      //Natural: WRITE 'GTN-PDA-E.CNTRCT-CMBN-NBR =' GTN-PDA-E.CNTRCT-CMBN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR(1)=",gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(1));                                             //Natural: WRITE 'GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR(1)=' GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*   END OF ADD  4/98
                                                                                                                                                                          //Natural: PERFORM VALIDATE-PAYMENTS
                sub_Validate_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                gtn_Pda_E_Array.getValue("*").reset();                                                                                                                    //Natural: RESET GTN-PDA-E-ARRAY ( * )
                gtn_Pda_E_Array.getValue(1).setValuesByName(pdaFcpa140e.getGtn_Pda_E());                                                                                  //Natural: MOVE BY NAME GTN-PDA-E TO GTN-PDA-E-ARRAY ( 1 )
                pnd_Indx.setValue(1);                                                                                                                                     //Natural: ASSIGN #INDX := 1
                pnd_Combine.setValue(false);                                                                                                                              //Natural: ASSIGN #COMBINE := FALSE
                pnd_Pend.setValue(false);                                                                                                                                 //Natural: ASSIGN #PEND := FALSE
                pnd_Active.setValue(false);                                                                                                                               //Natural: ASSIGN #ACTIVE := FALSE
                pnd_This_Pymnt0.setValue(0);                                                                                                                              //Natural: ASSIGN #THIS-PYMNT0 := 0
                pnd_This_Pymnt1.setValue(0);                                                                                                                              //Natural: ASSIGN #THIS-PYMNT1 := 0
                pnd_Nbr_Of_Pymnts0.setValue(0);                                                                                                                           //Natural: ASSIGN #NBR-OF-PYMNTS0 := 0
                pnd_Nbr_Of_Pymnts1.setValue(0);                                                                                                                           //Natural: ASSIGN #NBR-OF-PYMNTS1 := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM VALIDATE-PAYMENTS
        sub_Validate_Payments();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-PAYMENTS
        //*  KN 08/09/96
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-VALID-COMBINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-TERM
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PYMNT-TYPE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-EFT
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-GLOBAL
        //* **********************************************************************
        //*  1/19/1999 - JFT - CHECK FOR EM CURRENCY
        //*  1/19/1999 - JFT - END
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-HOLD-CODE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PYMNT-MODE
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NAME-ADDRESS
        //* **********************************************************************
        //*  SET ERROR-CODE 1 ACROSS ALL COMBINED CONTRACTS
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMBINES
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ALL-TERMINATED
        //* ********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ANY-PEND
        //* **********************************************************************
        //* **********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
        //* **********************************************************************
    }
    private void sub_Validate_Payments() throws Exception                                                                                                                 //Natural: VALIDATE-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ALL-TERMINATED
        sub_Determine_All_Terminated();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_All_Terminated.getBoolean()))                                                                                                                   //Natural: IF #ALL-TERMINATED
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ANY-PEND
        sub_Determine_Any_Pend();
        if (condition(Global.isEscape())) {return;}
        pnd_Invalid_Combine.setValue(false);                                                                                                                              //Natural: ASSIGN #INVALID-COMBINE := FALSE
        if (condition(pnd_Indx.equals(1)))                                                                                                                                //Natural: IF #INDX = 1
        {
            gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt.getValue(1).setValue(1);                                                                                                   //Natural: ASSIGN GTN-PDA-E-ARRAY.##THIS-PYMNT ( 1 ) := 1
            gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts.getValue(1).setValue(1);                                                                                                //Natural: ASSIGN GTN-PDA-E-ARRAY.##NBR-OF-PYMNTS ( 1 ) := 1
                                                                                                                                                                          //Natural: PERFORM CHECK-EFT
            sub_Check_Eft();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                              //Natural: IF #INVALID-COMBINE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-GLOBAL
                sub_Check_Global();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                          //Natural: IF #INVALID-COMBINE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-HOLD-CODE
                    sub_Check_Hold_Code();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-VALID-COMBINE
            sub_Check_For_Valid_Combine();
            if (condition(Global.isEscape())) {return;}
            FOR01:                                                                                                                                                        //Natural: FOR #INDX1 1 #INDX
            for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-PYMNT-MODE
                sub_Check_Pymnt_Mode();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM UPDATE-NAME-ADDRESS
                sub_Update_Name_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMBINES
            sub_Process_Combines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(((((gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).equals("B0  ") || gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).equals("C0  "))   //Natural: IF ( GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) = 'B0  ' OR = 'C0  ' OR = '00  ' ) AND ( GTN-PDA-E-ARRAY.PYMNT-SUSPEND-CDE ( #INDX1 ) EQ ' ' OR = '0' ) AND ( GTN-PDA-E-ARRAY.ERROR-CODE-1 ( #INDX1 ) EQ ' ' OR EQ '0' )
                || gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).equals("00  ")) && (gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).equals(" ") 
                || gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).equals("0"))) && (gtn_Pda_E_Array_Error_Code_1.getValue(pnd_Indx1).equals(" ") 
                || gtn_Pda_E_Array_Error_Code_1.getValue(pnd_Indx1).equals("0")))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gtn_Pda_E_Array_Error_Code_1.getValue(pnd_Indx1).equals("1")))                                                                              //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-1 ( #INDX1 ) = '1'
                {
                    pnd_Comments.setValue("MISSING NAME AND ADDRESS");                                                                                                    //Natural: ASSIGN #COMMENTS := 'MISSING NAME AND ADDRESS'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).equals("1")))                                                                          //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) = '1'
                    {
                        pnd_Comments.setValue("INVALID COMBINE/PEND  ");                                                                                                  //Natural: ASSIGN #COMMENTS := 'INVALID COMBINE/PEND  '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Comments.setValue(" ");                                                                                                                       //Natural: ASSIGN #COMMENTS := ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet828 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 );//Natural: VALUE 1
                if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(1))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" CHECK");                                                                                                                  //Natural: ASSIGN #PAYMENT-TYPE := ' CHECK'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(2))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" EFT");                                                                                                                    //Natural: ASSIGN #PAYMENT-TYPE := ' EFT'
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(3))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" GLOBAL");                                                                                                                 //Natural: ASSIGN #PAYMENT-TYPE := ' GLOBAL'
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(4))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" WIRE");                                                                                                                   //Natural: ASSIGN #PAYMENT-TYPE := ' WIRE'
                }                                                                                                                                                         //Natural: VALUE 5
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(5))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" CHECK/FEDEX");                                                                                                            //Natural: ASSIGN #PAYMENT-TYPE := ' CHECK/FEDEX'
                }                                                                                                                                                         //Natural: VALUE 6
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(6))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" SUSPENSE");                                                                                                               //Natural: ASSIGN #PAYMENT-TYPE := ' SUSPENSE'
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(7))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" NON PAYMENT");                                                                                                            //Natural: ASSIGN #PAYMENT-TYPE := ' NON PAYMENT'
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(8))))
                {
                    decideConditionsMet828++;
                    pnd_Payment_Type.setValue(" ROLLOVER");                                                                                                               //Natural: ASSIGN #PAYMENT-TYPE := ' ROLLOVER'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().write(2, gtn_Pda_E_Array_Cntrct_Ppcn_Nbr.getValue(pnd_Indx1),new TabSetting(12),gtn_Pda_E_Array_Cntrct_Payee_Cde.getValue(pnd_Indx1),new     //Natural: WRITE ( 2 ) GTN-PDA-E-ARRAY.CNTRCT-PPCN-NBR ( #INDX1 ) 12T GTN-PDA-E-ARRAY.CNTRCT-PAYEE-CDE ( #INDX1 ) 20T GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( #INDX1 ) 37T #PAYMENT-TYPE 53T GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) 64T GTN-PDA-E-ARRAY.CNTRCT-HOLD-IND ( #INDX1 ) 75T GTN-PDA-E-ARRAY.TEMP-PEND-CDE ( #INDX1 ) 84T GTN-PDA-E-ARRAY.PYMNT-SUSPEND-CDE ( #INDX1 ) 91T GTN-PDA-E-ARRAY.ERROR-CODE-1 ( #INDX1 ) 101T GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) 108T #COMMENTS
                    TabSetting(20),gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(pnd_Indx1),new TabSetting(37),pnd_Payment_Type,new TabSetting(53),gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1),new 
                    TabSetting(64),gtn_Pda_E_Array_Cntrct_Hold_Ind.getValue(pnd_Indx1),new TabSetting(75),gtn_Pda_E_Array_Temp_Pend_Cde.getValue(pnd_Indx1),new 
                    TabSetting(84),gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1),new TabSetting(91),gtn_Pda_E_Array_Error_Code_1.getValue(pnd_Indx1),new 
                    TabSetting(101),gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1),new TabSetting(108),pnd_Comments);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_For_Valid_Combine() throws Exception                                                                                                           //Natural: CHECK-FOR-VALID-COMBINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM CHECK-TERM
        sub_Check_Term();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                                  //Natural: IF #INVALID-COMBINE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-PYMNT-TYPE
        sub_Check_Pymnt_Type();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                                  //Natural: IF #INVALID-COMBINE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-EFT
        sub_Check_Eft();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                                  //Natural: IF #INVALID-COMBINE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-GLOBAL
        sub_Check_Global();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Invalid_Combine.getBoolean()))                                                                                                                  //Natural: IF #INVALID-COMBINE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-HOLD-CODE
        sub_Check_Hold_Code();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Check_Term() throws Exception                                                                                                                        //Natural: CHECK-TERM
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            //*  TERMINATED
            if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).equals("2")))                                                                                  //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) = '2'
            {
                FOR04:                                                                                                                                                    //Natural: FOR #INDX1 1 TO #INDX
                for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
                {
                    gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue("AMCM");                                                                                 //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := 'AMCM'
                    //*      GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND(#INDX1) := 1 /* 20200724
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Invalid_Combine.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMBINE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Pymnt_Type() throws Exception                                                                                                                  //Natural: CHECK-PYMNT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(1).notEquals(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1))))                      //Natural: IF GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( 1 ) NE GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 )
            {
                FOR06:                                                                                                                                                    //Natural: FOR #INDX1 1 TO #INDX
                for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
                {
                    gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue("AMER");                                                                                 //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := 'AMER'
                    //*      GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND(#INDX1) := 1  /* 20200724
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Invalid_Combine.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMBINE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Eft() throws Exception                                                                                                                         //Natural: CHECK-EFT
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).notEquals(2)))                                                                       //Natural: IF GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) NE 2
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (DbsUtil.maskMatches(gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X.getValue(1),"NNNNNNNNN"))))                                                           //Natural: IF GTN-PDA-E-ARRAY.PYMNT-EFT-TRANSIT-ID-X ( 1 ) NE MASK ( NNNNNNNNN )
        {
            FOR08:                                                                                                                                                        //Natural: FOR #INDX1 1 TO #INDX
            for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
            {
                gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue("AMTR");                                                                                     //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := 'AMTR'
                gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).setValue(1);                                                                                   //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) := 1
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Invalid_Combine.setValue(true);                                                                                                                           //Natural: ASSIGN #INVALID-COMBINE := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Eft_Transit_Id.setValue(gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X.getValue(1));                                                                              //Natural: ASSIGN #EFT-TRANSIT-ID := GTN-PDA-E-ARRAY.PYMNT-EFT-TRANSIT-ID-X ( 1 )
            FOR09:                                                                                                                                                        //Natural: FOR #INDX1 1 TO 8
            for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(8)); pnd_Indx1.nadd(1))
            {
                pnd_Weighted_Trans_Id.getValue(pnd_Indx1).compute(new ComputeParameters(false, pnd_Weighted_Trans_Id.getValue(pnd_Indx1)), pnd_Eft_Transit_Id_Pnd_Eft_Id.getValue(pnd_Indx1).multiply(pnd_Weight_Factor_Pnd_Wt_Factor.getValue(pnd_Indx1))); //Natural: ASSIGN #WEIGHTED-TRANS-ID ( #INDX1 ) := #EFT-ID ( #INDX1 ) * #WT-FACTOR ( #INDX1 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Weighted_Sum.compute(new ComputeParameters(false, pnd_Weighted_Sum), pnd_Weighted_Trans_Id.getValue("*").add(getZero()));                                 //Natural: ASSIGN #WEIGHTED-SUM := #WEIGHTED-TRANS-ID ( * ) + 0
            if (condition(pnd_Weighted_Sum_Pnd_Weighted_Digit.greaterOrEqual(1) && pnd_Weighted_Sum_Pnd_Weighted_Digit.lessOrEqual(9)))                                   //Natural: IF #WEIGHTED-DIGIT = 1 THRU 9
            {
                pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt.compute(new ComputeParameters(false, pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt), DbsField.subtract(10,pnd_Weighted_Sum_Pnd_Weighted_Digit)); //Natural: ASSIGN #EFT-CHK-DGT := 10 - #WEIGHTED-DIGIT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt.setValue(0);                                                                                                           //Natural: MOVE 0 TO #EFT-CHK-DGT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!gtn_Pda_E_Array_Pymnt_Eft_Transit_Id_X.getValue(1).getSubstring(9,1).equals(pnd_Eft_Transit_Id_Pnd_Eft_Chk_Dgt_A)))                            //Natural: IF SUBSTR ( GTN-PDA-E-ARRAY.PYMNT-EFT-TRANSIT-ID-X ( 1 ) ,9,1 ) NE #EFT-CHK-DGT-A
            {
                FOR10:                                                                                                                                                    //Natural: FOR #INDX1 1 TO #INDX
                for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
                {
                    gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue("AMTR");                                                                                 //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := 'AMTR'
                    gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).setValue(1);                                                                               //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) := 1
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Invalid_Combine.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMBINE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Global() throws Exception                                                                                                                      //Natural: CHECK-GLOBAL
    {
        if (BLNatReinput.isReinput()) return;

        FOR11:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(3) || gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(4)    //Natural: IF GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) = 3 OR GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) = 4 OR GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) = 9
                || gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(9)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Annt_Rsdncy_Cde_Pnd_Rsdncy_Cde.setValue(gtn_Pda_E_Array_Annt_Rsdncy_Cde.getValue(1));                                                                         //Natural: ASSIGN #RSDNCY-CDE := GTN-PDA-E-ARRAY.ANNT-RSDNCY-CDE ( 1 )
        DbsUtil.callnat(Iagn010.class , getCurrentProcessState(), pnd_Annt_Rsdncy_Cde, pnd_Rtrn_Cde);                                                                     //Natural: CALLNAT 'IAGN010' #ANNT-RSDNCY-CDE #RTRN-CDE
        if (condition(Global.isEscape())) return;
        FOR12:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(!gtn_Pda_E_Array_Global_Country.getValue(pnd_Indx1).getSubstring(2,2).equals(gtn_Pda_E_Array_Annt_Rsdncy_Cde.getValue(1))))                     //Natural: IF SUBSTR ( GTN-PDA-E-ARRAY.GLOBAL-COUNTRY ( #INDX1 ) ,2,2 ) NE GTN-PDA-E-ARRAY.ANNT-RSDNCY-CDE ( 1 )
            {
                //*  1/19/1999 - JFT - CHECK FOR EM CURRENCY
                if (condition(pnd_Rtrn_Cde.equals(getZero())))                                                                                                            //Natural: IF #RTRN-CDE = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  1/19/1999 - JFT - END
                //*  11/09 FOR CANADIAN PROVINCES
                if (condition(pnd_Annt_Rsdncy_Cde_Pnd_Rsdncy_Cde.equals(pnd_Canada.getValue("*")) && gtn_Pda_E_Array_Global_Country.getValue(pnd_Indx1).getSubstring(2,   //Natural: IF #RSDNCY-CDE = #CANADA ( * ) AND SUBSTR ( GTN-PDA-E-ARRAY.GLOBAL-COUNTRY ( #INDX1 ) ,2,2 ) = 'CA'
                    2).equals("CA")))
                {
                    //*  11/09 SO IT WON't HOLD
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  11/09
                }                                                                                                                                                         //Natural: END-IF
                FOR13:                                                                                                                                                    //Natural: FOR #INDX1 1 TO #INDX
                for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
                {
                    gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue("AMGO");                                                                                 //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := 'AMGO'
                    gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).setValue(1);                                                                               //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) := 1
                    gtn_Pda_E_Array_Global_Country.getValue(pnd_Indx1).setValue(" ");                                                                                     //Natural: ASSIGN GTN-PDA-E-ARRAY.GLOBAL-COUNTRY ( #INDX1 ) := ' '
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Invalid_Combine.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMBINE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Hold_Code() throws Exception                                                                                                                   //Natural: CHECK-HOLD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        FOR14:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition((gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).notEquals("B0  ") && gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).notEquals("C0  ") //Natural: IF ( GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) NE 'B0  ' AND GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) NE 'C0  ' AND GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) NE '00  ' )
                && gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).notEquals("00  "))))
            {
                pnd_Save_Hold_Cde.setValue(gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1));                                                                          //Natural: ASSIGN #SAVE-HOLD-CDE := GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 )
                pnd_Save_Hold_Grp.setValue(gtn_Pda_E_Array_Cntrct_Hold_Grp.getValue(pnd_Indx1));                                                                          //Natural: ASSIGN #SAVE-HOLD-GRP := GTN-PDA-E-ARRAY.CNTRCT-HOLD-GRP ( #INDX1 )
                pnd_Save_Hold_User_Id.setValue(gtn_Pda_E_Array_Cntrct_Hold_User_Id.getValue(pnd_Indx1));                                                                  //Natural: ASSIGN #SAVE-HOLD-USER-ID := GTN-PDA-E-ARRAY.CNTRCT-HOLD-USER-ID ( #INDX1 )
                FOR15:                                                                                                                                                    //Natural: FOR #INDX1 1 TO #INDX
                for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
                {
                    //*   KN 08/09/96
                    if (condition(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(1) || gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(2)  //Natural: IF GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) = 1 OR = 2 OR = 3
                        || gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(3)))
                    {
                        gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).setValue(1);                                                                           //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) := 1
                    }                                                                                                                                                     //Natural: END-IF
                    gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue(pnd_Save_Hold_Cde);                                                                      //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := #SAVE-HOLD-CDE
                    gtn_Pda_E_Array_Cntrct_Hold_Grp.getValue(pnd_Indx1).setValue(pnd_Save_Hold_Grp);                                                                      //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-GRP ( #INDX1 ) := #SAVE-HOLD-GRP
                    gtn_Pda_E_Array_Cntrct_Hold_User_Id.getValue(pnd_Indx1).setValue(pnd_Save_Hold_User_Id);                                                              //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-USER-ID ( #INDX1 ) := #SAVE-HOLD-USER-ID
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Invalid_Combine.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMBINE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(1).equals("B0  ") || gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(1).equals("C0  ")))                          //Natural: IF GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( 1 ) EQ 'B0  ' OR = 'C0  '
        {
            pnd_Save_Hold_Cde.setValue(gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(1));                                                                                      //Natural: ASSIGN #SAVE-HOLD-CDE := GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( 1 )
            FOR16:                                                                                                                                                        //Natural: FOR #INDX1 1 TO #INDX
            for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
            {
                gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue(pnd_Save_Hold_Cde);                                                                          //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := #SAVE-HOLD-CDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Pymnt_Mode() throws Exception                                                                                                                  //Natural: CHECK-PYMNT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).equals("2")))                                                                                      //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) = '2'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gtn_Pda_E_Array_Current_Mode.getValue(pnd_Indx1).equals("0")))                                                                                      //Natural: IF GTN-PDA-E-ARRAY.CURRENT-MODE ( #INDX1 ) = '0'
        {
            pnd_Nbr_Of_Pymnts0.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NBR-OF-PYMNTS0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nbr_Of_Pymnts1.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NBR-OF-PYMNTS1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Name_Address() throws Exception                                                                                                               //Natural: UPDATE-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        gtn_Pda_E_Array_Ph_Last_Name.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Ph_Last_Name.getValue(1));                                                              //Natural: ASSIGN GTN-PDA-E-ARRAY.PH-LAST-NAME ( #INDX1 ) := GTN-PDA-E-ARRAY.PH-LAST-NAME ( 1 )
        gtn_Pda_E_Array_Ph_Middle_Name.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Ph_Middle_Name.getValue(1));                                                          //Natural: ASSIGN GTN-PDA-E-ARRAY.PH-MIDDLE-NAME ( #INDX1 ) := GTN-PDA-E-ARRAY.PH-MIDDLE-NAME ( 1 )
        gtn_Pda_E_Array_Ph_First_Name.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Ph_First_Name.getValue(1));                                                            //Natural: ASSIGN GTN-PDA-E-ARRAY.PH-FIRST-NAME ( #INDX1 ) := GTN-PDA-E-ARRAY.PH-FIRST-NAME ( 1 )
        FOR17:                                                                                                                                                            //Natural: FOR #INDX2 1 TO 2
        for (pnd_Indx2.setValue(1); condition(pnd_Indx2.lessOrEqual(2)); pnd_Indx2.nadd(1))
        {
            gtn_Pda_E_Array_Pymnt_Addr_Line1_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line1_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE1-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE1-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Line2_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line2_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE2-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE2-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Line3_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line3_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE3-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE3-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Line4_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line4_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE4-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE4-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Line5_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line5_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE5-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE5-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Line6_Txt.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Line6_Txt.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE6-TXT ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LINE6-TXT ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Zip_Cde.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Zip_Cde.getValue(1,pnd_Indx2));                          //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-ZIP-CDE ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-ZIP-CDE ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Postl_Data.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Postl_Data.getValue(1,pnd_Indx2));                              //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-POSTL-DATA ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-POSTL-DATA ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Type_Ind.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Type_Ind.getValue(1,pnd_Indx2));                        //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-TYPE-IND ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-TYPE-IND ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Foreign_Cde.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte.getValue(1,pnd_Indx2));                      //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-FOREIGN-CDE ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-FOREIGN-CDE ( 1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LAST-CHG-DTE ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Foreign_Cde.getValue(1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte.getValue(1,pnd_Indx2));
            gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Dte.getValue(1,pnd_Indx2));
            gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Last_Chg_Tme.getValue(1,pnd_Indx2));                //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-LAST-CHG-TME ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-LAST-CHG-TME ( 1,#INDX2 )
            gtn_Pda_E_Array_Pymnt_Addr_Chg_Ind.getValue(pnd_Indx1,pnd_Indx2).setValue(gtn_Pda_E_Array_Pymnt_Addr_Chg_Ind.getValue(1,pnd_Indx2));                          //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-ADDR-CHG-IND ( #INDX1,#INDX2 ) := GTN-PDA-E-ARRAY.PYMNT-ADDR-CHG-IND ( 1,#INDX2 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gtn_Pda_E_Array_Pymnt_Eft_Acct_Nbr.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Pymnt_Eft_Acct_Nbr.getValue(1));                                                  //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-EFT-ACCT-NBR ( #INDX1 ) := GTN-PDA-E-ARRAY.PYMNT-EFT-ACCT-NBR ( 1 )
        gtn_Pda_E_Array_Pymnt_Eft_Transit_Id.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Pymnt_Eft_Transit_Id.getValue(1));                                              //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-EFT-TRANSIT-ID ( #INDX1 ) := GTN-PDA-E-ARRAY.PYMNT-EFT-TRANSIT-ID ( 1 )
        gtn_Pda_E_Array_Pymnt_Chk_Sav_Ind.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Pymnt_Chk_Sav_Ind.getValue(1));                                                    //Natural: ASSIGN GTN-PDA-E-ARRAY.PYMNT-CHK-SAV-IND ( #INDX1 ) := GTN-PDA-E-ARRAY.PYMNT-CHK-SAV-IND ( 1 )
        gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Cntrct_Hold_Cde.getValue(1));                                                        //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( #INDX1 ) := GTN-PDA-E-ARRAY.CNTRCT-HOLD-CDE ( 1 )
        gtn_Pda_E_Array_Cntrct_Hold_Grp.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Cntrct_Hold_Grp.getValue(1));                                                        //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-GRP ( #INDX1 ) := GTN-PDA-E-ARRAY.CNTRCT-HOLD-GRP ( 1 )
        gtn_Pda_E_Array_Cntrct_Hold_User_Id.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Cntrct_Hold_User_Id.getValue(1));                                                //Natural: ASSIGN GTN-PDA-E-ARRAY.CNTRCT-HOLD-USER-ID ( #INDX1 ) := GTN-PDA-E-ARRAY.CNTRCT-HOLD-USER-ID ( 1 )
        gtn_Pda_E_Array_Error_Code_1.getValue(pnd_Indx1).setValue(gtn_Pda_E_Array_Error_Code_1.getValue(1));                                                              //Natural: ASSIGN GTN-PDA-E-ARRAY.ERROR-CODE-1 ( #INDX1 ) := GTN-PDA-E-ARRAY.ERROR-CODE-1 ( 1 )
    }
    private void sub_Process_Combines() throws Exception                                                                                                                  //Natural: PROCESS-COMBINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).equals("2")))                                                                                      //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) = '2'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pend.getBoolean() && pnd_Active.getBoolean()))                                                                                                  //Natural: IF #PEND AND #ACTIVE
        {
            gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).setValue("1");                                                                                               //Natural: ASSIGN GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) := '1'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Indx.greater(1)))                                                                                                                               //Natural: IF #INDX GT 1
        {
            if (condition(gtn_Pda_E_Array_Current_Mode.getValue(pnd_Indx1).equals("0")))                                                                                  //Natural: IF GTN-PDA-E-ARRAY.CURRENT-MODE ( #INDX1 ) = '0'
            {
                pnd_This_Pymnt0.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #THIS-PYMNT0
                gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt.getValue(pnd_Indx1).setValue(pnd_This_Pymnt0);                                                                         //Natural: ASSIGN GTN-PDA-E-ARRAY.##THIS-PYMNT ( #INDX1 ) := #THIS-PYMNT0
                gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts.getValue(pnd_Indx1).setValue(pnd_Nbr_Of_Pymnts0);                                                                   //Natural: ASSIGN GTN-PDA-E-ARRAY.##NBR-OF-PYMNTS ( #INDX1 ) := #NBR-OF-PYMNTS0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_This_Pymnt1.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #THIS-PYMNT1
                gtn_Pda_E_Array_Pnd_Pnd_This_Pymnt.getValue(pnd_Indx1).setValue(pnd_This_Pymnt1);                                                                         //Natural: ASSIGN GTN-PDA-E-ARRAY.##THIS-PYMNT ( #INDX1 ) := #THIS-PYMNT1
                gtn_Pda_E_Array_Pnd_Pnd_Nbr_Of_Pymnts.getValue(pnd_Indx1).setValue(pnd_Nbr_Of_Pymnts1);                                                                   //Natural: ASSIGN GTN-PDA-E-ARRAY.##NBR-OF-PYMNTS ( #INDX1 ) := #NBR-OF-PYMNTS1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED CODE FOR CANADIAN PROVINCE EFT -- 1/2010 PER TOM MCGEE
        //*  IF RESIDENCY CODE IS CANADIAN PROVINCE, MOVE IT TO GLOBAL COUNTRY
        if (condition(gtn_Pda_E_Array_Pymnt_Pay_Type_Req_Ind.getValue(pnd_Indx1).equals(3)))                                                                              //Natural: IF GTN-PDA-E-ARRAY.PYMNT-PAY-TYPE-REQ-IND ( #INDX1 ) = 3
        {
            if (condition(gtn_Pda_E_Array_Annt_Rsdncy_Cde.getValue(pnd_Indx1).equals(pnd_Canada.getValue("*"))))                                                          //Natural: IF GTN-PDA-E-ARRAY.ANNT-RSDNCY-CDE ( #INDX1 ) = #CANADA ( * )
            {
                setValueToSubstring(gtn_Pda_E_Array_Annt_Rsdncy_Cde.getValue(pnd_Indx1),gtn_Pda_E_Array_Global_Country.getValue(pnd_Indx1),2,2);                          //Natural: MOVE GTN-PDA-E-ARRAY.ANNT-RSDNCY-CDE ( #INDX1 ) TO SUBSTR ( GTN-PDA-E-ARRAY.GLOBAL-COUNTRY ( #INDX1 ) ,2,2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADDED CODE == 1/2010
        getWorkFiles().write(2, false, gtn_Pda_E_Array.getValue(pnd_Indx1));                                                                                              //Natural: WRITE WORK FILE 2 GTN-PDA-E-ARRAY ( #INDX1 )
        pnd_Ctr_Out.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR-OUT
    }
    private void sub_Determine_All_Terminated() throws Exception                                                                                                          //Natural: DETERMINE-ALL-TERMINATED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Terminated.setValue(true);                                                                                                                                //Natural: ASSIGN #ALL-TERMINATED := TRUE
        FOR18:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).notEquals("2")))                                                                               //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) NE '2'
            {
                pnd_All_Terminated.reset();                                                                                                                               //Natural: RESET #ALL-TERMINATED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Any_Pend() throws Exception                                                                                                                //Natural: DETERMINE-ANY-PEND
    {
        if (BLNatReinput.isReinput()) return;

        FOR19:                                                                                                                                                            //Natural: FOR #INDX1 1 TO #INDX
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(pnd_Indx)); pnd_Indx1.nadd(1))
        {
            if (condition(gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).notEquals("2") && gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).notEquals(" ")         //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) NE '2' AND GTN-PDA-E-ARRAY.PYMNT-SUSPEND-CDE ( #INDX1 ) NE ' ' AND GTN-PDA-E-ARRAY.PYMNT-SUSPEND-CDE ( #INDX1 ) NE '0'
                && gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).notEquals("0")))
            {
                pnd_Pend.setValue(true);                                                                                                                                  //Natural: ASSIGN #PEND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition((gtn_Pda_E_Array_Error_Code_2.getValue(pnd_Indx1).notEquals("2") && (gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).equals(" ")          //Natural: IF GTN-PDA-E-ARRAY.ERROR-CODE-2 ( #INDX1 ) NE '2' AND ( GTN-PDA-E-ARRAY.PYMNT-SUSPEND-CDE ( #INDX1 ) EQ ' ' OR = '0' )
                || gtn_Pda_E_Array_Pymnt_Suspend_Cde.getValue(pnd_Indx1).equals("0")))))
            {
                pnd_Active.setValue(true);                                                                                                                                //Natural: ASSIGN #ACTIVE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, Global.getPROGRAM(),new TabSetting(46),"IA ADMINISTRATION PAYMENT EXTRACT ERROR REPORT");                                       //Natural: WRITE ( 2 ) *PROGRAM 46T 'IA ADMINISTRATION PAYMENT EXTRACT ERROR REPORT'
                    getReports().write(2, new TabSetting(36),"PAYMENT",new TabSetting(53),"HOLD",new TabSetting(59),"PERMANENT",new TabSetting(70),"TEMPORARY",new        //Natural: WRITE ( 2 ) 36T 'PAYMENT' 53T 'HOLD' 59T 'PERMANENT' 70T 'TEMPORARY' 82T 'PEND' 88T 'ERROR' 98T 'ERROR'
                        TabSetting(82),"PEND",new TabSetting(88),"ERROR",new TabSetting(98),"ERROR");
                    getReports().write(2, "CONTRACT  PAYEE",new TabSetting(20),"COMBINE NUMBER",new TabSetting(38),"TYPE",new TabSetting(52),"GROUP",new                  //Natural: WRITE ( 2 ) 'CONTRACT  PAYEE' 20T 'COMBINE NUMBER' 38T 'TYPE' 52T 'GROUP' 62T 'HOLD' 73T 'HOLD' 82T 'CODE' 88T 'CODE 1' 98T 'CODE 2' 108T 'COMMENTS'
                        TabSetting(62),"HOLD",new TabSetting(73),"HOLD",new TabSetting(82),"CODE",new TabSetting(88),"CODE 1",new TabSetting(98),"CODE 2",new 
                        TabSetting(108),"COMMENTS");
                    //*    103T 'SET'
                    getReports().write(2, "-",new RepeatItem(9),new TabSetting(11),"-",new RepeatItem(5),new TabSetting(20),"-",new RepeatItem(14),new                    //Natural: WRITE ( 2 ) '-' ( 9 ) 11T '-' ( 5 ) 20T '-' ( 14 ) 36T '-' ( 7 ) 52T '-' ( 5 ) 59T '-' ( 9 ) 70T '-' ( 9 ) 82T '-' ( 4 ) 88T '-' ( 6 ) 98T '-' ( 6 ) 108T '-' ( 24 )
                        TabSetting(36),"-",new RepeatItem(7),new TabSetting(52),"-",new RepeatItem(5),new TabSetting(59),"-",new RepeatItem(9),new TabSetting(70),"-",new 
                        RepeatItem(9),new TabSetting(82),"-",new RepeatItem(4),new TabSetting(88),"-",new RepeatItem(6),new TabSetting(98),"-",new RepeatItem(6),new 
                        TabSetting(108),"-",new RepeatItem(24));
                    //*    103T '-'(3)
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pdaFcpa140e.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde()); //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' GTN-PDA-E.CNTRCT-PPCN-NBR GTN-PDA-E.CNTRCT-PAYEE-CDE
        getReports().write(0, "=",pnd_Ctr_Out,"=",pnd_Ctr_In);                                                                                                            //Natural: WRITE '=' #CTR-OUT '=' #CTR-IN
        getReports().write(0, "=",pnd_Indx);                                                                                                                              //Natural: WRITE '=' #INDX
        //*  ADDED 4/98
        getReports().write(0, "=",pnd_Rec_Cnt_In, new ReportEditMask ("ZZZ,ZZZ,ZZZ"));                                                                                    //Natural: WRITE '=' #REC-CNT-IN ( EM = ZZZ,ZZZ,ZZZ )
        getReports().write(0, "GTN-PDA-E.CNTRCT-CMBN-NBR =",pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr());                                                                  //Natural: WRITE 'GTN-PDA-E.CNTRCT-CMBN-NBR =' GTN-PDA-E.CNTRCT-CMBN-NBR
        getReports().write(0, "GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR(1)=",gtn_Pda_E_Array_Cntrct_Cmbn_Nbr.getValue(1));                                                         //Natural: WRITE 'GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR(1)=' GTN-PDA-E-ARRAY.CNTRCT-CMBN-NBR ( 1 )
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
