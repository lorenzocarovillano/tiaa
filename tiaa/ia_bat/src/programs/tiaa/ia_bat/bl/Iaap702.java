/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:49 PM
**        * FROM NATURAL PROGRAM : Iaap702
************************************************************
**        * FILE NAME            : Iaap702.java
**        * CLASS NAME           : Iaap702
**        * INSTANCE NAME        : Iaap702
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP702                                           *
*                                                                    *
*   HISTORY :                                                        *
*           : KN : 08/07/98 NEW FCPA140E                             *
*           : KN : 12/02/97 MONTHLY UNITS - ARRAY OCCURS 40 TIME     *
*           : KN : 08/26/96 INVALID COMBINE LOGIC                    *
*                  BASED ON ERROR-CODE-2                             *
*                  04/30/02  'IAAN0510' WAS REPLACED WITH 'IAAN051Z' *
*                                                                    *
*                  08/22/02  ADDED LOGIC TO ACCUM 01IV CHECKS        *
*                            DO SCAN ON 8/02                         *
*                  03/25/03  ADDED NEW FCPA140E NEW FIELDS FOR       *
*                            SELF-REMITTER & EGTTRA                  *
*                  01/20/08  ADDED INTERFACE TO OMNI TRADE FOR TIAA
*                            ACCESS
*                  01/06/11  REMOVED INTERFACE TO OMNI TRADE
*                  04/2017   RE-STOWED ONLY FOR PIN EXPANSION.
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap702 extends BLNatBase
{
    // Data Areas
    private PdaFcpa140e pdaFcpa140e;
    private LdaFcpl800 ldaFcpl800;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Control_Rec_Array;
    private DbsField pnd_Control_Rec_Array_Pnd_Control_Rec_Id;

    private DbsGroup pnd_Control_Rec_Array_Pnd_Control_Counts;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Rec_Count;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Cref_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Gross_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Ded_Amt;
    private DbsField pnd_S_Rec_Count;
    private DbsField pnd_S_Cntrct_Amt;
    private DbsField pnd_S_Gross_Amt;
    private DbsField pnd_S_Dvdnd_Amt;
    private DbsField pnd_S_Cref_Amt;
    private DbsField pnd_S_Ded_Amt;
    private DbsField pnd_T_Rec_Count;
    private DbsField pnd_T_Cntrct_Amt;
    private DbsField pnd_T_Dvdnd_Amt;
    private DbsField pnd_T_Cref_Amt;
    private DbsField pnd_T_Gross_Amt;
    private DbsField pnd_T_Ded_Amt;
    private DbsField pnd_Summ_Per_Pymnt;
    private DbsField pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Ddctn_Per_Amt;
    private DbsField pnd_Amount;
    private DbsField pnd_First;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Cntrct_Payee;

    private DbsGroup pnd_Cntrct_Payee__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Pnd_Cntrct;
    private DbsField pnd_Cntrct_Payee_Pnd_Payee;
    private DbsField pnd_Prev_Cntrct_Payee;
    private DbsField pnd_Save_Check_Dte_A;
    private DbsField pnd_C_Indx;
    private DbsField pnd_D_Indx;
    private DbsField pnd_F_Indx;
    private DbsField pnd_Indx1;
    private DbsField pnd_Ctr;
    private DbsField pnd_Cref_Ctr;
    private DbsField pnd_Proc_Ctr;
    private DbsField pnd_Tiaafslash_Cref;
    private DbsField pnd_I;
    private DbsField pnd_Date_Out;
    private DbsField pnd_Cref_Amount;
    private DbsField pnd_Ivc_01iv_Checks;
    private DbsField pnd_Ivc_01iv_Pmt_Checks;
    private DbsField pnd_Ivc_01iv_Irax_Checks;
    private DbsField pnd_Ivc_01iv_Irax_Pmt_Checks;
    private DbsField pnd_Ivc_01iv_Pend_Checks;
    private DbsField pnd_Ivc_01iv_Pend_Pmt_Checks;
    private DbsField pnd_Ivc_01iv_Miss_Na_Checks;
    private DbsField pnd_Ivc_01iv_Miss_Na_Pmt_Checks;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa140e = new PdaFcpa140e(localVariables);
        ldaFcpl800 = new LdaFcpl800();
        registerRecord(ldaFcpl800);
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables

        pnd_Control_Rec_Array = localVariables.newGroupInRecord("pnd_Control_Rec_Array", "#CONTROL-REC-ARRAY");
        pnd_Control_Rec_Array_Pnd_Control_Rec_Id = pnd_Control_Rec_Array.newFieldInGroup("pnd_Control_Rec_Array_Pnd_Control_Rec_Id", "#CONTROL-REC-ID", 
            FieldType.STRING, 6);

        pnd_Control_Rec_Array_Pnd_Control_Counts = pnd_Control_Rec_Array.newGroupArrayInGroup("pnd_Control_Rec_Array_Pnd_Control_Counts", "#CONTROL-COUNTS", 
            new DbsArrayController(1, 23));
        pnd_Control_Rec_Array_Pnd_C_Rec_Count = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Rec_Count", "#C-REC-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt", "#C-CNTRCT-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt", "#C-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Cref_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Cref_Amt", "#C-CREF-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Gross_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Gross_Amt", "#C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Ded_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Ded_Amt", "#C-DED-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_S_Rec_Count = localVariables.newFieldArrayInRecord("pnd_S_Rec_Count", "#S-REC-COUNT", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 
            2));
        pnd_S_Cntrct_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cntrct_Amt", "#S-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Gross_Amt = localVariables.newFieldArrayInRecord("pnd_S_Gross_Amt", "#S-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Dvdnd_Amt = localVariables.newFieldArrayInRecord("pnd_S_Dvdnd_Amt", "#S-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Cref_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cref_Amt", "#S-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Ded_Amt = localVariables.newFieldArrayInRecord("pnd_S_Ded_Amt", "#S-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_T_Rec_Count = localVariables.newFieldInRecord("pnd_T_Rec_Count", "#T-REC-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_T_Cntrct_Amt = localVariables.newFieldInRecord("pnd_T_Cntrct_Amt", "#T-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_T_Dvdnd_Amt", "#T-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Cref_Amt = localVariables.newFieldInRecord("pnd_T_Cref_Amt", "#T-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Gross_Amt = localVariables.newFieldInRecord("pnd_T_Gross_Amt", "#T-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Ded_Amt = localVariables.newFieldInRecord("pnd_T_Ded_Amt", "#T-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Summ_Per_Pymnt = localVariables.newFieldInRecord("pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Summ_Per_Dvdnd = localVariables.newFieldInRecord("pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ddctn_Per_Amt = localVariables.newFieldInRecord("pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Amount = localVariables.newFieldInRecord("pnd_Amount", "#AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 14);

        pnd_Cntrct_Payee__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee__R_Field_1", "REDEFINE", pnd_Cntrct_Payee);
        pnd_Cntrct_Payee_Pnd_Cntrct = pnd_Cntrct_Payee__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Pnd_Payee = pnd_Cntrct_Payee__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Pnd_Payee", "#PAYEE", FieldType.STRING, 4);
        pnd_Prev_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Prev_Cntrct_Payee", "#PREV-CNTRCT-PAYEE", FieldType.STRING, 14);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);
        pnd_C_Indx = localVariables.newFieldInRecord("pnd_C_Indx", "#C-INDX", FieldType.NUMERIC, 2);
        pnd_D_Indx = localVariables.newFieldInRecord("pnd_D_Indx", "#D-INDX", FieldType.NUMERIC, 2);
        pnd_F_Indx = localVariables.newFieldInRecord("pnd_F_Indx", "#F-INDX", FieldType.NUMERIC, 2);
        pnd_Indx1 = localVariables.newFieldInRecord("pnd_Indx1", "#INDX1", FieldType.NUMERIC, 3);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Cref_Ctr = localVariables.newFieldInRecord("pnd_Cref_Ctr", "#CREF-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Proc_Ctr = localVariables.newFieldInRecord("pnd_Proc_Ctr", "#PROC-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Tiaafslash_Cref = localVariables.newFieldInRecord("pnd_Tiaafslash_Cref", "#TIAA/CREF", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Date_Out = localVariables.newFieldInRecord("pnd_Date_Out", "#DATE-OUT", FieldType.STRING, 8);
        pnd_Cref_Amount = localVariables.newFieldInRecord("pnd_Cref_Amount", "#CREF-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ivc_01iv_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Checks", "#IVC-01IV-CHECKS", FieldType.NUMERIC, 7);
        pnd_Ivc_01iv_Pmt_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Pmt_Checks", "#IVC-01IV-PMT-CHECKS", FieldType.NUMERIC, 7);
        pnd_Ivc_01iv_Irax_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Irax_Checks", "#IVC-01IV-IRAX-CHECKS", FieldType.NUMERIC, 7);
        pnd_Ivc_01iv_Irax_Pmt_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Irax_Pmt_Checks", "#IVC-01IV-IRAX-PMT-CHECKS", FieldType.NUMERIC, 
            7);
        pnd_Ivc_01iv_Pend_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Pend_Checks", "#IVC-01IV-PEND-CHECKS", FieldType.NUMERIC, 7);
        pnd_Ivc_01iv_Pend_Pmt_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Pend_Pmt_Checks", "#IVC-01IV-PEND-PMT-CHECKS", FieldType.NUMERIC, 
            7);
        pnd_Ivc_01iv_Miss_Na_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Miss_Na_Checks", "#IVC-01IV-MISS-NA-CHECKS", FieldType.NUMERIC, 7);
        pnd_Ivc_01iv_Miss_Na_Pmt_Checks = localVariables.newFieldInRecord("pnd_Ivc_01iv_Miss_Na_Pmt_Checks", "#IVC-01IV-MISS-NA-PMT-CHECKS", FieldType.NUMERIC, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl800.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap702() throws Exception
    {
        super("Iaap702");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP702", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(1, pdaFcpa140e.getGtn_Pda_E());                                                                                                           //Natural: READ WORK 1 ONCE GTN-PDA-E
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
                                                                                                                                                                          //Natural: PERFORM PROCESS-GTN-PDA
            sub_Process_Gtn_Pda();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getWorkFiles().read(3, ldaFcpl800.getIa_Cntl_Rec());                                                                                                              //Natural: READ WORK 3 ONCE IA-CNTL-REC
        FOR01:                                                                                                                                                            //Natural: FOR #INDX1 = 1 TO 22
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(22)); pnd_Indx1.nadd(1))
        {
            //*  CPS CONTROL RECORD NEEDS TO HAVE ALL ROLLOVER COMBINED TO THE 4TH
            //*  OCCURANCE
            if (condition(pnd_Indx1.equals(20) || pnd_Indx1.equals(21) || pnd_Indx1.equals(22)))                                                                          //Natural: IF #INDX1 = 20 OR = 21 OR = 22
            {
                ldaFcpl800.getIa_Cntl_Rec_C_Rec_Count().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1));                                      //Natural: ADD #C-REC-COUNT ( #INDX1 ) TO IA-CNTL-REC.C-REC-COUNT ( 4 )
                ldaFcpl800.getIa_Cntl_Rec_C_Cntrct_Amt().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1));                                    //Natural: ADD #C-CNTRCT-AMT ( #INDX1 ) TO IA-CNTL-REC.C-CNTRCT-AMT ( 4 )
                ldaFcpl800.getIa_Cntl_Rec_C_Dvdnd_Amt().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1));                                      //Natural: ADD #C-DVDND-AMT ( #INDX1 ) TO IA-CNTL-REC.C-DVDND-AMT ( 4 )
                ldaFcpl800.getIa_Cntl_Rec_C_Cref_Amt().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1));                                        //Natural: ADD #C-CREF-AMT ( #INDX1 ) TO IA-CNTL-REC.C-CREF-AMT ( 4 )
                ldaFcpl800.getIa_Cntl_Rec_C_Gross_Amt().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1));                                      //Natural: ADD #C-GROSS-AMT ( #INDX1 ) TO IA-CNTL-REC.C-GROSS-AMT ( 4 )
                ldaFcpl800.getIa_Cntl_Rec_C_Ded_Amt().getValue(4).nadd(pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1));                                          //Natural: ADD #C-DED-AMT ( #INDX1 ) TO IA-CNTL-REC.C-DED-AMT ( 4 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl800.getIa_Cntl_Rec_C_Rec_Count().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1));                          //Natural: ASSIGN IA-CNTL-REC.C-REC-COUNT ( #INDX1 ) := #C-REC-COUNT ( #INDX1 )
                ldaFcpl800.getIa_Cntl_Rec_C_Cntrct_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1));                        //Natural: ASSIGN IA-CNTL-REC.C-CNTRCT-AMT ( #INDX1 ) := #C-CNTRCT-AMT ( #INDX1 )
                ldaFcpl800.getIa_Cntl_Rec_C_Dvdnd_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1));                          //Natural: ASSIGN IA-CNTL-REC.C-DVDND-AMT ( #INDX1 ) := #C-DVDND-AMT ( #INDX1 )
                ldaFcpl800.getIa_Cntl_Rec_C_Cref_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1));                            //Natural: ASSIGN IA-CNTL-REC.C-CREF-AMT ( #INDX1 ) := #C-CREF-AMT ( #INDX1 )
                ldaFcpl800.getIa_Cntl_Rec_C_Gross_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1));                          //Natural: ASSIGN IA-CNTL-REC.C-GROSS-AMT ( #INDX1 ) := #C-GROSS-AMT ( #INDX1 )
                ldaFcpl800.getIa_Cntl_Rec_C_Ded_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1));                              //Natural: ASSIGN IA-CNTL-REC.C-DED-AMT ( #INDX1 ) := #C-DED-AMT ( #INDX1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CNTRL-SUMMARY
        sub_Write_Cntrl_Summary();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl800.getIa_Cntl_Rec_Cntl_Rec_Id().setValue("999999");                                                                                                       //Natural: ASSIGN CNTL-REC-ID := '999999'
        getWorkFiles().write(2, false, ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte(), ldaFcpl800.getIa_Cntl_Rec_Cntl_Rec_Id(), ldaFcpl800.getIa_Cntl_Rec_Cntl_Counts().getValue("*")); //Natural: WRITE WORK FILE 2 IA-CNTL-REC.CNTL-CHECK-DTE IA-CNTL-REC.CNTL-REC-ID IA-CNTL-REC.CNTL-COUNTS ( * )
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",pnd_Ctr);                                                                                        //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",pnd_Proc_Ctr);                                                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #PROC-CTR
        if (Global.isEscape()) return;
        //*  REMOVE OMNI TRADE FEED  -- FIF   01/11
        //* *IF #OMNI-CNT = 0           /* 01/09 CREATE AN EMPTY OMNI TRADE RECORD
        //* *  WRITE WORK 4 #OMNI-TRADE /* IF THERE's no TIAA Access payment
        //* *  WRITE(2) //// 'No Monthly OMNI Trade Transactions in this Run'
        //* *ELSE
        //* *  WRITE(2) 'Total' 29X #OMNI-CNT 5X #TOT-OMNI-AMT
        //* *END-IF
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-GTN-PDA
        //*  END OF CHANGE 8/02
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-RECORD
        //*   KN 08/26/96
        //*  VALUE '2'
        //*    #C-INDX := 8
        //*    PERFORM ACCUMULATE-CONTROL-RECORD
        //*    IF #PAYMENT-DUE
        //*      #C-INDX := 17
        //*      PERFORM ACCUMULATE-CONTROL-RECORD
        //*    END-IF
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUMULATE-CONTROL-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CNTRL-SUMMARY
        //*                                  NEW FUNDS ADDED 1/2001
        //*  WRITE (1) / 'TPA IVC MISS N/A  '
        //*                         #IVC-01IV-MISS-NA-CHECKS  (EM=Z,ZZZ,ZZ9)
        //*  WRITE (1) / 'TPA ROLL PEND IVC ' #IVC-01IV-PEND-CHECKS (EM=Z,ZZZ,ZZ9)
        //*  WRITE (1) / 'TOT TPA IRAX IVC  '  #IVC-01IV-IRAX-CHECKS (EM=Z,ZZZ,ZZ9)
        //*  END OF ADDED 8/02
        //* **********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL
        //* *********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-COMPANY
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OMNI-RECORD
    }
    private void sub_Reset() throws Exception                                                                                                                             //Natural: RESET
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Indx1.setValue(0);                                                                                                                                            //Natural: ASSIGN #INDX1 := 0
        pnd_Summ_Per_Pymnt.setValue(0);                                                                                                                                   //Natural: ASSIGN #SUMM-PER-PYMNT := 0
        pnd_Summ_Per_Dvdnd.setValue(0);                                                                                                                                   //Natural: ASSIGN #SUMM-PER-DVDND := 0
        pnd_Ddctn_Per_Amt.setValue(0);                                                                                                                                    //Natural: ASSIGN #DDCTN-PER-AMT := 0
        pnd_Cref_Amount.setValue(0);                                                                                                                                      //Natural: ASSIGN #CREF-AMOUNT := 0
    }
    private void sub_Process_Gtn_Pda() throws Exception                                                                                                                   //Natural: PROCESS-GTN-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Proc_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
        if (condition(pdaFcpa140e.getGtn_Pda_E_Current_Mode().equals("1")))                                                                                               //Natural: IF GTN-PDA-E.CURRENT-MODE = '1'
        {
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Payment_Due.setValue(false);                                                                                                                              //Natural: ASSIGN #PAYMENT-DUE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Rec_Count.getValue(1).nadd(1);                                                                                                                              //Natural: ADD 1 TO #S-REC-COUNT ( 1 )
        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                      //Natural: IF #PAYMENT-DUE
        {
            pnd_S_Rec_Count.getValue(2).nadd(1);                                                                                                                          //Natural: ADD 1 TO #S-REC-COUNT ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED 8/02
        if (condition(DbsUtil.maskMatches(pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde(),"'01IV'")))                                                                         //Natural: IF CNTRCT-PAYEE-CDE = MASK ( '01IV' )
        {
            //*  ADDED 8/02
            pnd_Ivc_01iv_Checks.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #IVC-01IV-CHECKS
            //*  ADDED 8/02
            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAX")))                                                                                //Natural: IF CNTRCT-ROLL-DEST-CDE = 'IRAX'
            {
                //*  ADDED 8/02
                pnd_Ivc_01iv_Irax_Checks.nadd(1);                                                                                                                         //Natural: ADD 1 TO #IVC-01IV-IRAX-CHECKS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ADDED 8/0
                //*  ADDED 6/03
                //*  ADDED 6/03
                //*  ADDED 6/03
                if (condition(((((((((pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC"))   //Natural: IF CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC' AND ERROR-CODE-1 = '1'
                    || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC")) 
                    || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")) 
                    || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC")) 
                    && pdaFcpa140e.getGtn_Pda_E_Error_Code_1().equals("1"))))
                {
                    //*  ADDED 8/0
                    pnd_Ivc_01iv_Miss_Na_Checks.nadd(1);                                                                                                                  //Natural: ADD 1 TO #IVC-01IV-MISS-NA-CHECKS
                    //*  ADDED 8/02
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED 8/02
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 8/02
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING 4/02
        //*  ADDED 4/02
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().notEquals("0")))                                                                                       //Natural: IF PYMNT-SUSPEND-CDE NE '0'
        {
            //*  ADDED 8/02
            if (condition(DbsUtil.maskMatches(pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde(),"'01IV'")))                                                                     //Natural: IF CNTRCT-PAYEE-CDE = MASK ( '01IV' )
            {
                //*  ADDED 8/02
                if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().notEquals("IRAX")))                                                                         //Natural: IF CNTRCT-ROLL-DEST-CDE NE 'IRAX'
                {
                    //*  ADDED 8/02
                    pnd_Ivc_01iv_Pend_Checks.nadd(1);                                                                                                                     //Natural: ADD 1 TO #IVC-01IV-PEND-CHECKS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                      //Natural: IF #PAYMENT-DUE
        {
            //*  ADDED 8/02
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().notEquals("0")))                                                                                   //Natural: IF PYMNT-SUSPEND-CDE NE '0'
            {
                //*  ADDED 8/02
                if (condition(DbsUtil.maskMatches(pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde(),"'01IV'")))                                                                 //Natural: IF CNTRCT-PAYEE-CDE = MASK ( '01IV' )
                {
                    //*  ADDED 8/02
                    if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().notEquals("IRAX")))                                                                     //Natural: IF CNTRCT-ROLL-DEST-CDE NE 'IRAX'
                    {
                        //*  ADDED 8/02
                        pnd_Ivc_01iv_Pend_Pmt_Checks.nadd(1);                                                                                                             //Natural: ADD 1 TO #IVC-01IV-PEND-PMT-CHECKS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADDED 8/02
        //*  ADDED FOLLOWING  FOLLOWING 8/02 TO ACCUM TPA IVC CHECKS SEPARATELY
        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                      //Natural: IF #PAYMENT-DUE
        {
            //*  ADDED 8/02
            if (condition(DbsUtil.maskMatches(pdaFcpa140e.getGtn_Pda_E_Cntrct_Payee_Cde(),"'01IV'")))                                                                     //Natural: IF CNTRCT-PAYEE-CDE = MASK ( '01IV' )
            {
                //*  ADDED 8/02
                pnd_Ivc_01iv_Pmt_Checks.nadd(1);                                                                                                                          //Natural: ADD 1 TO #IVC-01IV-PMT-CHECKS
                //*  ADDED 8/02
                if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAX")))                                                                            //Natural: IF CNTRCT-ROLL-DEST-CDE = 'IRAX'
                {
                    //*  ADDED 8/02
                    pnd_Ivc_01iv_Irax_Pmt_Checks.nadd(1);                                                                                                                 //Natural: ADD 1 TO #IVC-01IV-IRAX-PMT-CHECKS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ADDED 8/02
                    //*  ADDED 6/03
                    //*  ADDED 6/03
                    //*  ADDED 6/03
                    if (condition(((((((((pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC"))  //Natural: IF CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC' AND ERROR-CODE-1 = '1'
                        || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC")) 
                        || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")) 
                        || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT")) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC")) 
                        && pdaFcpa140e.getGtn_Pda_E_Error_Code_1().equals("1"))))
                    {
                        //*  ADDED 8/02
                        pnd_Ivc_01iv_Miss_Na_Pmt_Checks.nadd(1);                                                                                                          //Natural: ADD 1 TO #IVC-01IV-MISS-NA-PMT-CHECKS
                        //*  ADDED 8/02
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADDED 8/02
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #F-INDX 1 TO 80
        for (pnd_F_Indx.setValue(1); condition(pnd_F_Indx.lessOrEqual(80)); pnd_F_Indx.nadd(1))
        {
            if (condition(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).equals(" ")))                                                                      //Natural: IF GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaafslash_Cref.reset();                                                                                                                              //Natural: RESET #TIAA/CREF
                                                                                                                                                                          //Natural: PERFORM GET-FUND-COMPANY
                sub_Get_Fund_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                short decideConditionsMet626 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA/CREF = 'T'
                if (condition(pnd_Tiaafslash_Cref.equals("T")))
                {
                    decideConditionsMet626++;
                    pnd_Summ_Per_Pymnt.nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx));                                                         //Natural: ADD GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX ) TO #SUMM-PER-PYMNT
                    pnd_Summ_Per_Dvdnd.nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx));                                                          //Natural: ADD GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX ) TO #SUMM-PER-DVDND
                    pnd_S_Cntrct_Amt.getValue(1).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx));                                               //Natural: ADD GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX ) TO #S-CNTRCT-AMT ( 1 )
                    pnd_S_Dvdnd_Amt.getValue(1).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx));                                                 //Natural: ADD GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX ) TO #S-DVDND-AMT ( 1 )
                    pnd_S_Gross_Amt.getValue(1).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx));                                                //Natural: ADD GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX ) TO #S-GROSS-AMT ( 1 )
                    pnd_S_Gross_Amt.getValue(1).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx));                                                 //Natural: ADD GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX ) TO #S-GROSS-AMT ( 1 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_S_Cntrct_Amt.getValue(2).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx));                                           //Natural: ADD GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX ) TO #S-CNTRCT-AMT ( 2 )
                        pnd_S_Gross_Amt.getValue(2).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx));                                            //Natural: ADD GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX ) TO #S-GROSS-AMT ( 2 )
                        pnd_S_Dvdnd_Amt.getValue(2).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx));                                             //Natural: ADD GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX ) TO #S-DVDND-AMT ( 2 )
                        pnd_S_Gross_Amt.getValue(2).nadd(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx));                                             //Natural: ADD GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX ) TO #S-GROSS-AMT ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).equals("09") || pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).equals("11"))) //Natural: IF GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) = '09' OR = '11'
                    {
                        pnd_Amount.setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(pnd_F_Indx));                                                          //Natural: ASSIGN #AMOUNT := GTN-PDA-E.INV-ACCT-SETTL-AMT ( #F-INDX )
                        pnd_Cref_Amount.nadd(pnd_Amount);                                                                                                                 //Natural: ADD #AMOUNT TO #CREF-AMOUNT
                        pnd_S_Cref_Amt.getValue(1).nadd(pnd_Amount);                                                                                                      //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 1 )
                        pnd_S_Gross_Amt.getValue(1).nadd(pnd_Amount);                                                                                                     //Natural: ADD #AMOUNT TO #S-GROSS-AMT ( 1 )
                        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                      //Natural: IF #PAYMENT-DUE
                        {
                            pnd_S_Cref_Amt.getValue(2).nadd(pnd_Amount);                                                                                                  //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 2 )
                            pnd_S_Gross_Amt.getValue(2).nadd(pnd_Amount);                                                                                                 //Natural: ADD #AMOUNT TO #S-GROSS-AMT ( 2 )
                            //*  01/09 OMNI TRADE
                            if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0") && pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).equals("11"))) //Natural: IF PYMNT-SUSPEND-CDE = '0' AND GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) = '11'
                            {
                                                                                                                                                                          //Natural: PERFORM WRITE-OMNI-RECORD
                                sub_Write_Omni_Record();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  01/09 END
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #TIAA/CREF = 'C'
                else if (condition(pnd_Tiaafslash_Cref.equals("C")))
                {
                    decideConditionsMet626++;
                    pnd_Amount.setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(pnd_F_Indx));                                                              //Natural: ASSIGN #AMOUNT := GTN-PDA-E.INV-ACCT-SETTL-AMT ( #F-INDX )
                    pnd_Cref_Amount.nadd(pnd_Amount);                                                                                                                     //Natural: ADD #AMOUNT TO #CREF-AMOUNT
                    pnd_S_Cref_Amt.getValue(1).nadd(pnd_Amount);                                                                                                          //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 1 )
                    pnd_S_Gross_Amt.getValue(1).nadd(pnd_Amount);                                                                                                         //Natural: ADD #AMOUNT TO #S-GROSS-AMT ( 1 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_S_Cref_Amt.getValue(2).nadd(pnd_Amount);                                                                                                      //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 2 )
                        pnd_S_Gross_Amt.getValue(2).nadd(pnd_Amount);                                                                                                     //Natural: ADD #AMOUNT TO #S-GROSS-AMT ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #D-INDX 1 TO 10
        for (pnd_D_Indx.setValue(1); condition(pnd_D_Indx.lessOrEqual(10)); pnd_D_Indx.nadd(1))
        {
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Ded_Amt().getValue(pnd_D_Indx).equals(getZero())))                                                               //Natural: IF GTN-PDA-E.PYMNT-DED-AMT ( #D-INDX ) = 0
            {
                ignore();
                //*    ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ddctn_Per_Amt.nadd(pdaFcpa140e.getGtn_Pda_E_Pymnt_Ded_Amt().getValue(pnd_D_Indx));                                                                    //Natural: ADD GTN-PDA-E.PYMNT-DED-AMT ( #D-INDX ) TO #DDCTN-PER-AMT
                pnd_S_Ded_Amt.getValue(1).nadd(pdaFcpa140e.getGtn_Pda_E_Pymnt_Ded_Amt().getValue(pnd_D_Indx));                                                            //Natural: ADD GTN-PDA-E.PYMNT-DED-AMT ( #D-INDX ) TO #S-DED-AMT ( 1 )
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_S_Ded_Amt.getValue(2).nadd(pdaFcpa140e.getGtn_Pda_E_Pymnt_Ded_Amt().getValue(pnd_D_Indx));                                                        //Natural: ADD GTN-PDA-E.PYMNT-DED-AMT ( #D-INDX ) TO #S-DED-AMT ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CONTROL-RECORD
        sub_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET
        sub_Reset();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Control_Record() throws Exception                                                                                                                    //Natural: CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals(" ")))                                                                                          //Natural: IF PYMNT-SUSPEND-CDE = ' '
        {
            pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().setValue("0");                                                                                                   //Natural: ASSIGN PYMNT-SUSPEND-CDE := '0'
        }                                                                                                                                                                 //Natural: END-IF
        //*  KN 08/26/96
        //*  INVALID COMBINE
        if (condition(pdaFcpa140e.getGtn_Pda_E_Error_Code_2().equals("1")))                                                                                               //Natural: IF ERROR-CODE-2 = '1'
        {
            pnd_C_Indx.setValue(8);                                                                                                                                       //Natural: ASSIGN #C-INDX := 8
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(17);                                                                                                                                  //Natural: ASSIGN #C-INDX := 17
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MISSING N/A
        if (condition(pdaFcpa140e.getGtn_Pda_E_Error_Code_1().equals("1") && pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0")))                                   //Natural: IF ERROR-CODE-1 = '1' AND PYMNT-SUSPEND-CDE = '0'
        {
            pnd_C_Indx.setValue(7);                                                                                                                                       //Natural: ASSIGN #C-INDX := 7
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(16);                                                                                                                                  //Natural: ASSIGN #C-INDX := 16
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0")))                                                                                          //Natural: IF PYMNT-SUSPEND-CDE = '0'
        {
            //*  ADDED 6/03
            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC")                //Natural: IF GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT") 
                || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT") || pdaFcpa140e.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")))
            {
                //*  AC ROLLOVER INT
                if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(21)))                                                                                   //Natural: IF GTN-PDA-E.CNTRCT-OPTION-CDE = 21
                {
                    pnd_C_Indx.setValue(4);                                                                                                                               //Natural: ASSIGN #C-INDX := 4
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  P&I ROLLOVER
                    if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(22)))                                                                               //Natural: IF GTN-PDA-E.CNTRCT-OPTION-CDE = 22
                    {
                        pnd_C_Indx.setValue(20);                                                                                                                          //Natural: ASSIGN #C-INDX := 20
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                        sub_Accumulate_Control_Record();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  TPA ROLLOV
                        if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(28) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(30)))                //Natural: IF GTN-PDA-E.CNTRCT-OPTION-CDE = 28 OR = 30
                        {
                            pnd_C_Indx.setValue(21);                                                                                                                      //Natural: ASSIGN #C-INDX := 21
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                            sub_Accumulate_Control_Record();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  IPRO ROLL
                            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(25) || pdaFcpa140e.getGtn_Pda_E_Cntrct_Option_Cde().equals(27)))            //Natural: IF GTN-PDA-E.CNTRCT-OPTION-CDE = 25 OR = 27
                            {
                                pnd_C_Indx.setValue(22);                                                                                                                  //Natural: ASSIGN #C-INDX := 22
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                                sub_Accumulate_Control_Record();
                                if (condition(Global.isEscape())) {return;}
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ROLLOVER (INT)
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(13);                                                                                                                              //Natural: ASSIGN #C-INDX := 13
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet753 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF PYMNT-SUSPEND-CDE;//Natural: VALUE '0'
        if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0"))))
        {
            decideConditionsMet753++;
            //*  CHECK
            short decideConditionsMet756 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet756++;
                pnd_C_Indx.setValue(1);                                                                                                                                   //Natural: ASSIGN #C-INDX := 1
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(10);                                                                                                                              //Natural: ASSIGN #C-INDX := 10
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  EFT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet756++;
                pnd_C_Indx.setValue(2);                                                                                                                                   //Natural: ASSIGN #C-INDX := 2
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(11);                                                                                                                              //Natural: ASSIGN #C-INDX := 11
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  GLOBAL PAY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet756++;
                pnd_C_Indx.setValue(3);                                                                                                                                   //Natural: ASSIGN #C-INDX := 3
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(12);                                                                                                                              //Natural: ASSIGN #C-INDX := 12
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  MISCELLANEOUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                pnd_C_Indx.setValue(9);                                                                                                                                   //Natural: ASSIGN #C-INDX := 9
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(18);                                                                                                                              //Natural: ASSIGN #C-INDX := 18
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  BLOCKED ACCOUNTS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("I"))))
        {
            decideConditionsMet753++;
            pnd_C_Indx.setValue(5);                                                                                                                                       //Natural: ASSIGN #C-INDX := 5
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(14);                                                                                                                                  //Natural: ASSIGN #C-INDX := 14
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                //*  UNCLAIMED SUMS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'K'
        else if (condition((pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("K"))))
        {
            decideConditionsMet753++;
            pnd_C_Indx.setValue(6);                                                                                                                                       //Natural: ASSIGN #C-INDX := 6
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(15);                                                                                                                                  //Natural: ASSIGN #C-INDX := 15
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                //*  MISCELLANEOUS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            pnd_C_Indx.setValue(9);                                                                                                                                       //Natural: ASSIGN #C-INDX := 9
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(18);                                                                                                                                  //Natural: ASSIGN #C-INDX := 18
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Accumulate_Control_Record() throws Exception                                                                                                         //Natural: ACCUMULATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_C_Indx).nadd(1);                                                                                               //Natural: ADD 1 TO #C-REC-COUNT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_C_Indx).nadd(pnd_Summ_Per_Pymnt);                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #C-CNTRCT-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(pnd_Summ_Per_Pymnt);                                                                              //Natural: ADD #SUMM-PER-PYMNT TO #C-GROSS-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_C_Indx).nadd(pnd_Summ_Per_Dvdnd);                                                                              //Natural: ADD #SUMM-PER-DVDND TO #C-DVDND-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(pnd_Summ_Per_Dvdnd);                                                                              //Natural: ADD #SUMM-PER-DVDND TO #C-GROSS-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_C_Indx).nadd(pnd_Cref_Amount);                                                                                  //Natural: ADD #CREF-AMOUNT TO #C-CREF-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(pnd_Cref_Amount);                                                                                 //Natural: ADD #CREF-AMOUNT TO #C-GROSS-AMT ( #C-INDX )
        pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_C_Indx).nadd(pnd_Ddctn_Per_Amt);                                                                                 //Natural: ADD #DDCTN-PER-AMT TO #C-DED-AMT ( #C-INDX )
        //*  IF #C-INDX = 7
        //*  WRITE ' ACCUMULATE-CONTROL-RECORD'
        //*  '=' #SUMM-PER-PYMNT
        //*  '=' #SUMM-PER-DVDND '=' #C-GROSS-AMT(#C-INDX)
        //*  END-IF
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        getReports().write(0, Global.getPROGRAM(),"STARTED AT: ",Global.getTIMN());                                                                                       //Natural: WRITE *PROGRAM 'STARTED AT: ' *TIMN
        if (Global.isEscape()) return;
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        //* *RESET #OMNI-TRADE #ACTVTY-AS-OF-PRICE/* 01/09 RESET OMNI TRADE FIELDS
        //* *  #ACTVTY-PROC-PRICE #RQSTD-SHRS #RQSTD-CASH #SYSTM-PIN
    }
    private void sub_Write_Cntrl_Summary() throws Exception                                                                                                               //Natural: WRITE-CNTRL-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Date_Out.setValueEdited(ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                                           //Natural: MOVE EDITED CNTL-CHECK-DTE ( EM = MM/DD/YY ) TO #DATE-OUT
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM ",Global.getPROGRAM(),new TabSetting(35),"IA ADMINISTRATION CONTROL SUMMARY FOR PAYMENTS DUE ", //Natural: WRITE ( 1 ) NOTITLE 1T 'PROGRAM ' *PROGRAM 35T 'IA ADMINISTRATION CONTROL SUMMARY FOR PAYMENTS DUE ' #DATE-OUT
            pnd_Date_Out);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(4),"DATE ",Global.getDATU(),NEWLINE);                                                                   //Natural: WRITE ( 1 ) 4T 'DATE ' *DATU /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(38),new TabSetting(40),"IA FILE CONTROLS","-",new RepeatItem(54));                                  //Natural: WRITE ( 1 ) '-' ( 38 ) 40T 'IA FILE CONTROLS' '-' ( 54 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(24),"RECORD",new TabSetting(34),"CONTRACTUAL");                                                         //Natural: WRITE ( 1 ) 24T 'RECORD' 34T 'CONTRACTUAL'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"COUNT",new TabSetting(36),"AMOUNT",new TabSetting(52),"DIVIDEND",new TabSetting(65),"TIAA/CREF AMT",new  //Natural: WRITE ( 1 ) 25T 'COUNT' 36T 'AMOUNT' 52T 'DIVIDEND' 65T 'TIAA/CREF AMT' 82T 'GROSS AMOUNT' 99T 'DEDUCTIONS'
            TabSetting(82),"GROSS AMOUNT",new TabSetting(99),"DEDUCTIONS");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(13),new TabSetting(24),"-",new RepeatItem(7),new TabSetting(33),"-",new RepeatItem(14),new          //Natural: WRITE ( 1 ) '-' ( 13 ) 24T '-' ( 7 ) 33T '-' ( 14 ) 49T '-' ( 14 ) 65T '-' ( 14 ) 81T '-' ( 14 ) 97T '-' ( 14 )
            TabSetting(49),"-",new RepeatItem(14),new TabSetting(65),"-",new RepeatItem(14),new TabSetting(81),"-",new RepeatItem(14),new TabSetting(97),"-",new 
            RepeatItem(14));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(1);                                                                                                                                            //Natural: ASSIGN #INDX1 := 1
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"CHECKS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1), new                     //Natural: WRITE ( 1 ) / 'CHECKS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(2);                                                                                                                                            //Natural: ASSIGN #INDX1 := 2
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"EFTS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1), new ReportEditMask        //Natural: WRITE ( 1 ) / 'EFTS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(3);                                                                                                                                            //Natural: ASSIGN #INDX1 := 3
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"GLOBAL PAY",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                     //Natural: WRITE ( 1 ) / 'GLOBAL PAY' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(4);                                                                                                                                            //Natural: ASSIGN #INDX1 := 4
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"AC ROLLOVER (INT)",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),              //Natural: WRITE ( 1 ) / 'AC ROLLOVER (INT)' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(20);                                                                                                                                           //Natural: ASSIGN #INDX1 := 20
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"P/I ROLLOVER (INT)",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),             //Natural: WRITE ( 1 ) / 'P/I ROLLOVER (INT)' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(21);                                                                                                                                           //Natural: ASSIGN #INDX1 := 21
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TPA ROLLOVER (INT)",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),             //Natural: WRITE ( 1 ) / 'TPA ROLLOVER (INT)' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(22);                                                                                                                                           //Natural: ASSIGN #INDX1 := 22
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"IPRO ROLLOVER (INT)",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),            //Natural: WRITE ( 1 ) / 'IPRO ROLLOVER (INT)' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE (1) / 'TPA REINVSTMNT(INT)'
        //*    24T #C-REC-COUNT(23)  (EM=ZZZ,ZZ9)
        //*    33T #C-CNTRCT-AMT(23) (EM=ZZZ,ZZZ,ZZZ.99)
        //*    49T #C-DVDND-AMT(23)  (EM=ZZZ,ZZZ,ZZZ.99)
        //*    65T #C-CREF-AMT(23)   (EM=ZZZ,ZZZ,ZZZ.99)
        //*    81T #C-GROSS-AMT(23)  (EM=ZZZ,ZZZ,ZZZ.99)
        //*    97T #C-DED-AMT(23)    (EM=ZZZ,ZZZ,ZZZ.99)
        //*  #INDX1 := 23
        //*  PERFORM TOTAL
        //*  ==
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL PAY",new TabSetting(24),pnd_T_Rec_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_T_Cntrct_Amt,  //Natural: WRITE ( 1 ) / 'TOTAL PAY' 24T #T-REC-COUNT ( EM = ZZZ,ZZ9 ) 33T #T-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #T-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #T-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #T-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #T-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_T_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_T_Cref_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_T_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_T_Ded_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_T_Rec_Count.reset();                                                                                                                                          //Natural: RESET #T-REC-COUNT #T-CNTRCT-AMT #T-DVDND-AMT #T-CREF-AMT #T-GROSS-AMT #T-DED-AMT
        pnd_T_Cntrct_Amt.reset();
        pnd_T_Dvdnd_Amt.reset();
        pnd_T_Cref_Amt.reset();
        pnd_T_Gross_Amt.reset();
        pnd_T_Ded_Amt.reset();
        getReports().write(1, ReportOption.NOTITLE,"LEDGERIZED PENDS");                                                                                                   //Natural: WRITE ( 1 ) 'LEDGERIZED PENDS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"----------------");                                                                                                   //Natural: WRITE ( 1 ) '----------------'
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(5);                                                                                                                                            //Natural: ASSIGN #INDX1 := 5
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BLOCKED ACCOUNTS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),               //Natural: WRITE ( 1 ) / 'BLOCKED ACCOUNTS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(6);                                                                                                                                            //Natural: ASSIGN #INDX1 := 6
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"UNCLAIMED SUMS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                 //Natural: WRITE ( 1 ) / 'UNCLAIMED SUMS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"NON-LEDGERIZED PENDS");                                                                                               //Natural: WRITE ( 1 ) 'NON-LEDGERIZED PENDS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"--------------------");                                                                                               //Natural: WRITE ( 1 ) '--------------------'
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(7);                                                                                                                                            //Natural: ASSIGN #INDX1 := 7
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"N/A MISSING",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                    //Natural: WRITE ( 1 ) / 'N/A MISSING' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(8);                                                                                                                                            //Natural: ASSIGN #INDX1 := 8
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INVALID COMBINE",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                //Natural: WRITE ( 1 ) / 'INVALID COMBINE' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(9);                                                                                                                                            //Natural: ASSIGN #INDX1 := 9
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"MISCELLANEOUS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                  //Natural: WRITE ( 1 ) / 'MISCELLANEOUS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL PENDS",new TabSetting(24),pnd_T_Rec_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_T_Cntrct_Amt,  //Natural: WRITE ( 1 ) / 'TOTAL PENDS' 24T #T-REC-COUNT ( EM = ZZZ,ZZ9 ) 33T #T-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #T-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #T-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #T-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #T-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_T_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_T_Cref_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_T_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_T_Ded_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_T_Rec_Count.reset();                                                                                                                                          //Natural: RESET #T-REC-COUNT #T-CNTRCT-AMT #T-DVDND-AMT #T-CREF-AMT #T-GROSS-AMT #T-DED-AMT
        pnd_T_Cntrct_Amt.reset();
        pnd_T_Dvdnd_Amt.reset();
        pnd_T_Cref_Amt.reset();
        pnd_T_Gross_Amt.reset();
        pnd_T_Ded_Amt.reset();
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL",new TabSetting(24),pnd_S_Rec_Count.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new                     //Natural: WRITE ( 1 ) / 'TOTAL' 24T #S-REC-COUNT ( 1 ) ( EM = ZZZ,ZZ9 ) 33T #S-CNTRCT-AMT ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #S-DVDND-AMT ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #S-CREF-AMT ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #S-GROSS-AMT ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #S-DED-AMT ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            TabSetting(33),pnd_S_Cntrct_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_S_Dvdnd_Amt.getValue(1), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_S_Cref_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_S_Gross_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_S_Ded_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING 8/02
        //*  WRITE PAYMENT TOTALS FOR PMTS TO BE MADE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TPA ROLL IVC COUNT  ",pnd_Ivc_01iv_Checks, new ReportEditMask ("Z,ZZZ,ZZ9"),"Record Count Total Minus Total TPA roll ivc count should match P2270IAM total"); //Natural: WRITE ( 1 ) / 'TPA ROLL IVC COUNT  ' #IVC-01IV-CHECKS ( EM = Z,ZZZ,ZZ9 ) 'Record Count Total Minus Total TPA roll ivc count should match P2270IAM total'
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(40),new TabSetting(42),"PAYMENTS DUE","-",new RepeatItem(56));                                      //Natural: WRITE ( 1 ) '-' ( 40 ) 42T 'PAYMENTS DUE' '-' ( 56 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(24),"RECORD",new TabSetting(34),"CONTRACTUAL");                                                         //Natural: WRITE ( 1 ) 24T 'RECORD' 34T 'CONTRACTUAL'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"COUNT",new TabSetting(36),"AMOUNT",new TabSetting(52),"DIVIDEND",new TabSetting(65),"TIAA/CREF AMT",new  //Natural: WRITE ( 1 ) 25T 'COUNT' 36T 'AMOUNT' 52T 'DIVIDEND' 65T 'TIAA/CREF AMT' 82T 'GROSS AMOUNT' 99T 'DEDUCTIONS'
            TabSetting(82),"GROSS AMOUNT",new TabSetting(99),"DEDUCTIONS");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(13),new TabSetting(24),"-",new RepeatItem(7),new TabSetting(33),"-",new RepeatItem(14),new          //Natural: WRITE ( 1 ) '-' ( 13 ) 24T '-' ( 7 ) 33T '-' ( 14 ) 49T '-' ( 14 ) 65T '-' ( 14 ) 81T '-' ( 14 ) 97T '-' ( 14 )
            TabSetting(49),"-",new RepeatItem(14),new TabSetting(65),"-",new RepeatItem(14),new TabSetting(81),"-",new RepeatItem(14),new TabSetting(97),"-",new 
            RepeatItem(14));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(10);                                                                                                                                           //Natural: ASSIGN #INDX1 := 10
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"CHECKS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1), new                     //Natural: WRITE ( 1 ) / 'CHECKS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(11);                                                                                                                                           //Natural: ASSIGN #INDX1 := 11
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"EFTS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1), new ReportEditMask        //Natural: WRITE ( 1 ) / 'EFTS' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(12);                                                                                                                                           //Natural: ASSIGN #INDX1 := 12
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"GLOBAL PAY",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                     //Natural: WRITE ( 1 ) / 'GLOBAL PAY' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(13);                                                                                                                                           //Natural: ASSIGN #INDX1 := 13
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"ROLLOVER (INT)",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1),                 //Natural: WRITE ( 1 ) / 'ROLLOVER (INT)' 24T #C-REC-COUNT ( #INDX1 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( #INDX1 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL PAY",new TabSetting(24),pnd_T_Rec_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_T_Cntrct_Amt,  //Natural: WRITE ( 1 ) / 'TOTAL PAY' 24T #T-REC-COUNT ( EM = ZZZ,ZZ9 ) 33T #T-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #T-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #T-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #T-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #T-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_T_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_T_Cref_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_T_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_T_Ded_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_T_Rec_Count.reset();                                                                                                                                          //Natural: RESET #T-REC-COUNT #T-CNTRCT-AMT #T-DVDND-AMT #T-CREF-AMT #T-GROSS-AMT #T-DED-AMT
        pnd_T_Cntrct_Amt.reset();
        pnd_T_Dvdnd_Amt.reset();
        pnd_T_Cref_Amt.reset();
        pnd_T_Gross_Amt.reset();
        pnd_T_Ded_Amt.reset();
        getReports().write(1, ReportOption.NOTITLE,"LEDGERIZED PENDS");                                                                                                   //Natural: WRITE ( 1 ) 'LEDGERIZED PENDS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"----------------");                                                                                                   //Natural: WRITE ( 1 ) '----------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BLOCKED ACCOUNTS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(14), new                  //Natural: WRITE ( 1 ) / 'BLOCKED ACCOUNTS' 24T #C-REC-COUNT ( 14 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( 14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( 14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( 14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( 14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( 14 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(14), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(14), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(14), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(14), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(14), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(14);                                                                                                                                           //Natural: ASSIGN #INDX1 := 14
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"UNCLAIMED SUMS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(15), new                    //Natural: WRITE ( 1 ) / 'UNCLAIMED SUMS' 24T #C-REC-COUNT ( 15 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( 15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( 15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( 15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( 15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( 15 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(15), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(15), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(15), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(15), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(15), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(15);                                                                                                                                           //Natural: ASSIGN #INDX1 := 15
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"NON-LEDGERIZED PENDS");                                                                                               //Natural: WRITE ( 1 ) 'NON-LEDGERIZED PENDS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"--------------------");                                                                                               //Natural: WRITE ( 1 ) '--------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"N/A MISSING",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(16), new ReportEditMask        //Natural: WRITE ( 1 ) / 'N/A MISSING' 24T #C-REC-COUNT ( 16 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( 16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( 16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( 16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( 16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( 16 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(16), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(16), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(16), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(16), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(16), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(16);                                                                                                                                           //Natural: ASSIGN #INDX1 := 16
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INVALID COMBINE",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(17), new                   //Natural: WRITE ( 1 ) / 'INVALID COMBINE' 24T #C-REC-COUNT ( 17 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( 17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( 17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( 17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( 17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( 17 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(17), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(17), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(17), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(17), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(17), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(17);                                                                                                                                           //Natural: ASSIGN #INDX1 := 17
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"MISCELLANEOUS",new TabSetting(24),pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(18), new                     //Natural: WRITE ( 1 ) / 'MISCELLANEOUS' 24T #C-REC-COUNT ( 18 ) ( EM = ZZZ,ZZ9 ) 33T #C-CNTRCT-AMT ( 18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #C-DVDND-AMT ( 18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #C-CREF-AMT ( 18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #C-GROSS-AMT ( 18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #C-DED-AMT ( 18 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(18), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(49),pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(18), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(18), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(18), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(97),pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(18), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Indx1.setValue(18);                                                                                                                                           //Natural: ASSIGN #INDX1 := 18
                                                                                                                                                                          //Natural: PERFORM TOTAL
        sub_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL PENDS",new TabSetting(24),pnd_T_Rec_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(33),pnd_T_Cntrct_Amt,  //Natural: WRITE ( 1 ) / 'TOTAL PENDS' 24T #T-REC-COUNT ( EM = ZZZ,ZZ9 ) 33T #T-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #T-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #T-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #T-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #T-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_T_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_T_Cref_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_T_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_T_Ded_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL",new TabSetting(24),pnd_S_Rec_Count.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new                     //Natural: WRITE ( 1 ) / 'TOTAL' 24T #S-REC-COUNT ( 2 ) ( EM = ZZZ,ZZ9 ) 33T #S-CNTRCT-AMT ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 49T #S-DVDND-AMT ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 65T #S-CREF-AMT ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #S-GROSS-AMT ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 97T #S-DED-AMT ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            TabSetting(33),pnd_S_Cntrct_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(49),pnd_S_Dvdnd_Amt.getValue(2), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(65),pnd_S_Cref_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),pnd_S_Gross_Amt.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(97),pnd_S_Ded_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING 8/02
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TPA ROLL IVC COUNT  ",pnd_Ivc_01iv_Pmt_Checks, new ReportEditMask ("Z,ZZZ,ZZ9"),"Record Count Total Minus Total TPA ivc count should match P2270IAM total"); //Natural: WRITE ( 1 ) / 'TPA ROLL IVC COUNT  ' #IVC-01IV-PMT-CHECKS ( EM = Z,ZZZ,ZZ9 ) 'Record Count Total Minus Total TPA ivc count should match P2270IAM total'
        if (Global.isEscape()) return;
        //*  WRITE (1) / 'TOTAL TPA PEND IVC  '
        //*   #IVC-01IV-PEND-PMT-CHECKS (EM=Z,ZZZ,ZZ9)
        //*  WRITE (1) / 'TOTAL TPA IRAX IVC  '
        //*   #IVC-01IV-IRAX-PMT-CHECKS (EM=Z,ZZZ,ZZ9)
        //*  END OF ADDED 8/02
    }
    private void sub_Total() throws Exception                                                                                                                             //Natural: TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_T_Rec_Count.nadd(pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1));                                                                                  //Natural: ADD #C-REC-COUNT ( #INDX1 ) TO #T-REC-COUNT
        pnd_T_Cntrct_Amt.nadd(pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1));                                                                                //Natural: ADD #C-CNTRCT-AMT ( #INDX1 ) TO #T-CNTRCT-AMT
        pnd_T_Dvdnd_Amt.nadd(pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1));                                                                                  //Natural: ADD #C-DVDND-AMT ( #INDX1 ) TO #T-DVDND-AMT
        pnd_T_Cref_Amt.nadd(pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1));                                                                                    //Natural: ADD #C-CREF-AMT ( #INDX1 ) TO #T-CREF-AMT
        pnd_T_Gross_Amt.nadd(pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1));                                                                                  //Natural: ADD #C-GROSS-AMT ( #INDX1 ) TO #T-GROSS-AMT
        pnd_T_Ded_Amt.nadd(pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1));                                                                                      //Natural: ADD #C-DED-AMT ( #INDX1 ) TO #T-DED-AMT
    }
    private void sub_Get_Fund_Company() throws Exception                                                                                                                  //Natural: GET-FUND-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx))))               //Natural: IF IAAA051Z.#IA-STD-NM-CD ( #I ) = GTN-PDA-E.INV-ACCT-CDE ( #F-INDX )
            {
                if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Cmpny_Cd().getValue(pnd_I).equals(1)))                                                                   //Natural: IF IAAA051Z.#IA-STD-CMPNY-CD ( #I ) = 1
                {
                    pnd_Tiaafslash_Cref.setValue("T");                                                                                                                    //Natural: ASSIGN #TIAA/CREF := 'T'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Cmpny_Cd().getValue(pnd_I).equals(2)))                                                               //Natural: IF IAAA051Z.#IA-STD-CMPNY-CD ( #I ) = 2
                    {
                        pnd_Tiaafslash_Cref.setValue("C");                                                                                                                //Natural: ASSIGN #TIAA/CREF := 'C'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Omni_Record() throws Exception                                                                                                                 //Natural: WRITE-OMNI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  REMOVE INTERFACE TO OMNI TRADE   01/11
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //* *IF GTN-PDA-E.ERROR-CODE-1 = '1' OR  /* 01/09 REJECT IF NAAD IS MISSING
        //* *    GTN-PDA-E.ERROR-CODE-2 = '1'    /* OR INVALID COMBINE
        //* *  ESCAPE ROUTINE
        //* *END-IF
        //* *IF #OMNI-CNT = 0
        //* *  COMPUTE #WRK-DTE = GTN-PDA-E.PYMNT-CHECK-DTE -1
        //* *END-IF
        //* *#REC-TYPE := 'AC'
        //* *#REC-VRSN := '00'
        //* *#PLAN-NMBR := 'IAP000'
        //* *#SCRTY-ID := 'ANW99A051'
        //* *#ACTION := 'S'
        //* *IF GTN-PDA-E.INV-ACCT-VALUAT-PERIOD(#F-INDX) = 'M'
        //* *  #ACTVTY-TYPE := 'MP'
        //* *ELSE
        //* *  #ACTVTY-TYPE := 'AP'
        //* *END-IF
        //* *#PEND-STTS := ' '
        //* *#REQ-TYPE := '1'
        //* *#CERTAINTY := 'C'
        //* *#BYPASS-FLAG := 'N'
        //* *#TRADE-FLAG := 'Y'
        //* *#REVERSAL-FLAG := 'N'
        //* *MOVE EDITED #WRK-DTE(EM=YYYYMMDD) TO #ACTVTY-AS-OF-DTE
        //* *#RQSTD-SHRS := 0
        //* *#RQSTD-CASH := GTN-PDA-E.INV-ACCT-SETTL-AMT(#F-INDX)
        //* *#GAIN-LOSS-CASH := 0
        //* *#GAIN-LOSS-SIGN := ' '
        //* *#SYSTM-ID := 'IA'
        //* *#TEXT-FILE-NAME := ' '
        //* *#SYSTM-PIN := GTN-PDA-E.CNTRCT-UNQ-ID-NBR
        //* *#SYSTM-CNTRCT := GTN-PDA-E.CNTRCT-PPCN-NBR
        //* *#INTRNL-SPCFC-AREA := ' '
        //* *#PRTCPNT-NMBR := ' '
        //* *WRITE WORK 4 #OMNI-TRADE
        //* *ADD 1 TO #OMNI-CNT
        //* *ADD #RQSTD-CASH TO #TOT-OMNI-AMT
        //* *DISPLAY(2) NOTITLE
        //*   'PIN'       #SYSTM-PIN
        //*   'Cntrct'    #SYSTM-CNTRCT
        //*   'Py'        GTN-PDA-E.CNTRCT-PAYEE-CDE(AL=2)
        //*   'Actn'      #ACTION
        //*   'Type'      #ACTVTY-TYPE
        //*   'As of Dte' #ACTVTY-AS-OF-DTE
        //*   'Trade Amt' #RQSTD-CASH
        //*   'CUSIP'     #SCRTY-ID
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(10),"IA Monthly OMNI Trade Transactions Report",new                  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 10X 'IA Monthly OMNI Trade Transactions Report' 5X *DATX ( EM = MM/DD/YYYY ) / *TIMX 18X 'For Check Date:' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) 16X *PAGE-NUMBER ( 2 ) //
                        ColumnSpacing(5),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,Global.getTIMX(),new ColumnSpacing(18),"For Check Date:",pdaFcpa140e.getGtn_Pda_E_Pymnt_Check_Dte(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(16),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pnd_Cntrct_Payee);                                                              //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #CNTRCT-PAYEE
                                                                                                                                                                          //Natural: PERFORM WRITE-CNTRL-SUMMARY
        sub_Write_Cntrl_Summary();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
