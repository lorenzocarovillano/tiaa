/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:59 PM
**        * FROM NATURAL PROGRAM : Iaap780
************************************************************
**        * FILE NAME            : Iaap780.java
**        * CLASS NAME           : Iaap780
**        * INSTANCE NAME        : Iaap780
************************************************************
**********************************************************************
* PROGRAM:  IATP780 - NEW IA SYSTEM
*                     ACTUARAL REPORTSEDGER REPORT DRIVER
*                     REPORT FILE GENERATOR
*
* THIS PROGRAMS READS THE THE IA ADMINISTRATIVE MASTER FILE RECORDS
* AND WRITES A WORK FILE FOR INPUT TO FOUR REPORT PROGRAMS.
*
* READ THE PARM CARD FOR THE CHECK DATE TO BE PROCESSED.
*  IF NO PARM CARD ENTRY EXISTS, USE LATEST CONTROL REC CHECK DATE.
* READ THE IAA-CNTRL-RCRD USING THE CHECK DATE FROM THE PARM CARD.
* READ THE IAA-CNTRL-RCRD USING THE CHECK DATE PLUS ONE MONTH.
* READ ALL IAA-TRANS-RCRD RECORDS FOR THE CHECK DATE.
*   FOR EACH TRANSACTION RECORD, DO THE FOLLOWING:
*     READ IAA-CNTRCT-TRANS      RECORD TO COLLECT DATA.
*     READ IAA-CPR-TRANS         RECORD TO COLLECT DATA.
*     READ IAA-CREF-FUND-TRANS   RECORD TO COLLECT DATA.
*     WRITE REPORT FILE TO WORK FILE 1.
*
*
*
* THE WORK FILE IS SORTED AND REPORTED ON BY THE FOLLOWING PROGRAMS.
*
*  IATP782 -
*  IATP784 -
*  IATP786 -
*  IATP788 -
*
* -----------------------------------------------------MAINTENENCE LOG
*   DATE   PROGRAMMER     DESCRIPTION OF MODIFICATION
* -------------------------------------------------------------------
* 03/27/96 M.OLIVA        CHANGED ACCESS OF AFTER IMAGE FUND RECORDS
*                         TO NOT ACCESS THEM WHEN THERE IS(ARE) NO
*                         BEFORE IMAGE FUND RECORDS.
*
* 03/28/96 M.OLIVA        ADDED ADDITIONAL TRANSACTION SELECTION
*                         CRITERIA TO BYPASS CERTAIN TRANSACTION
*                         SUB CODES AND ACTIVITY CODES.
*
* 11/06/96 A GROSSMAN     CHANGED THE LOGIC FOR ACCESSING BEFORE AND
*                         AFTER IMAGES FOR TRANSACTION VIEW.
*
*  5/18/00                NEW PA SELECT FUNDS & ARIGIN TO FILE
*                         DO SCAN ON 5/00 FOR CHANGES
*  4/2002                 CHANGED IAAN0510 TO IAAN051Z FOR NEW OIA
*  6/2011                 ADD NEW FIELDS TO PASS TO PRINT PROGRAM
*                         IAAP784 - MARKED JB01
*  3/2012                 RATE BASE EXPANSION  DO SCAN ON 3/12
*  10/2014                COR/NAAD DECOMMISSION SCAN ON 10/2014
*  08/2017   RC           PIN EXPANSION
**********************************************************************
**********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap780 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;
    private LdaIaalpsgm ldaIaalpsgm;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField parm_Card;

    private DbsGroup parm_Card__R_Field_1;
    private DbsField parm_Card_Pnd_Check_Date;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees;

    private DataAccessProgramView vw_iaa_Trans_Rcrd_View;
    private DbsField iaa_Trans_Rcrd_View_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_View_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_View_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_View_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_View_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_View_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_View_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_View_Trans_Cwf_Id_Nbr;

    private DataAccessProgramView vw_iaa_Cntrct_Trans_View;
    private DbsField iaa_Cntrct_Trans_View_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_View_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_View_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_View_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_View_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_View_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_View_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Cntrct_View;
    private DbsField iaa_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_View_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_View_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_View_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_View_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_View_Lst_Trans_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role_View;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Rllvr_Cntrct_Nbr;

    private DataAccessProgramView vw_iaa_Cpr_Trans_View;
    private DbsField iaa_Cpr_Trans_View_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_View_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_View_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_View_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_View_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_View_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_View_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_View_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_View_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_View_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_View_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_View_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_View_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_Trans_View_Rllvr_Cntrct_Nbr;

    private DataAccessProgramView vw_iaa_Cref_Fund_Trans_1_View;
    private DbsField iaa_Cref_Fund_Trans_1_View_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Trans_1_View__R_Field_2;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_View_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Trans_1_View_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_1_View_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_1_View_Aftr_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_View_Cref_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd_1_View;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Rcrd_1_View__R_Field_3;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte;
    private DbsField pnd_I_Pin;
    private DbsField pnd_Tiaa_Fund;
    private DbsField pnd_Report_File;

    private DbsGroup pnd_Report_File__R_Field_4;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Ppcn;
    private DbsField pnd_Report_File_Pnd_Rep_Payee;
    private DbsField pnd_Report_File_Pnd_Rep_First_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Second_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Option_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Mode_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_First_Pay_Due_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Before;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_After;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Diff;
    private DbsField pnd_Report_File_Pnd_Rep_Orgn_Cde;
    private DbsField pnd_Report_File_Pnd_Rep_Last_Name;
    private DbsField pnd_Report_File_Pnd_Rep_First_Name;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Pay_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Div_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Report_File_Pnd_Rep_Inst_Iss_Cde;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key;
    private DbsField pnd_Tiaa_Fund_Key;

    private DbsGroup pnd_Tiaa_Fund_Key__R_Field_5;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Contract;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Payee;
    private DbsField pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Code;
    private DbsField pnd_Inverse_Check_Date;
    private DbsField pnd_Store_Trans_Date;
    private DbsField pnd_Store_Inverse_Trans_Date;
    private DbsField pnd_Current_Check_Date;

    private DbsGroup pnd_Current_Check_Date__R_Field_6;
    private DbsField pnd_Current_Check_Date_Pnd_Current_Check_Date_A;
    private DbsField pnd_Current_First_Trans_Date;
    private DbsField pnd_Fund_Found;
    private DbsField pnd_Display_Date;
    private DbsField pnd_Bef_Image_Fund_Found;
    private DbsField pnd_Aft_Image_Fund_Found;
    private DbsField pnd_Report_Recs_Written;
    private DbsField pnd_Bef_Contract_Key;

    private DbsGroup pnd_Bef_Contract_Key__R_Field_7;
    private DbsField pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_1;
    private DbsField pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_2;
    private DbsField pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_3;
    private DbsField pnd_Aft_Contract_Key;

    private DbsGroup pnd_Aft_Contract_Key__R_Field_8;
    private DbsField pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_1;
    private DbsField pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_2;
    private DbsField pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_3;
    private DbsField pnd_Control_Key;

    private DbsGroup pnd_Control_Key__R_Field_9;
    private DbsField pnd_Control_Key_Pnd_Control_Key_1;
    private DbsField pnd_Control_Key_Pnd_Control_Key_2;
    private DbsField pnd_Bef_Cpr_Key;

    private DbsGroup pnd_Bef_Cpr_Key__R_Field_10;
    private DbsField pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_1;
    private DbsField pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_2;
    private DbsField pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_3;
    private DbsField pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_4;
    private DbsField pnd_Aft_Cpr_Key;

    private DbsGroup pnd_Aft_Cpr_Key__R_Field_11;
    private DbsField pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_1;
    private DbsField pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_2;
    private DbsField pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_3;
    private DbsField pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_4;
    private DbsField pnd_Cpr_Master_Key;

    private DbsGroup pnd_Cpr_Master_Key__R_Field_12;
    private DbsField pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_1;
    private DbsField pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_2;
    private DbsField pnd_Bef_Fund_Key;

    private DbsGroup pnd_Bef_Fund_Key__R_Field_13;
    private DbsField pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_1;
    private DbsField pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_2;
    private DbsField pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_3;
    private DbsField pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_4;
    private DbsField pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_5;
    private DbsField pnd_Aft_Fund_Key;

    private DbsGroup pnd_Aft_Fund_Key__R_Field_14;
    private DbsField pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_1;
    private DbsField pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_2;
    private DbsField pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_3;
    private DbsField pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_4;
    private DbsField pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_5;
    private DbsField pnd_Mst_Fund_Key;

    private DbsGroup pnd_Mst_Fund_Key__R_Field_15;
    private DbsField pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_1;
    private DbsField pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_2;
    private DbsField pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_3;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;

    private DbsGroup pnd_Fund_Totals;
    private DbsField pnd_Fund_Totals_Pnd_Fund_Code;
    private DbsField pnd_Fund_Totals_Pnd_Fund_Update_Sw;
    private DbsField pnd_Fund_Totals_Pnd_Bef_Units;
    private DbsField pnd_Fund_Totals_Pnd_Aft_Units;

    private DbsGroup pnd_Fund_Id_Table;
    private DbsField pnd_Fund_Id_Table_Pnd_Tiaa_Id_1;
    private DbsField pnd_Fund_Id_Table_Pnd_Tiaa_Id_2;
    private DbsField pnd_Fund_Id_Table_Pnd_Tiaa_Id_3;
    private DbsField pnd_Fund_Id_Table_Pnd_Stck_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Mma_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Sca_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Bond_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Global_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Equity_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Growth_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Rea_Id;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund10;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund11;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund12;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund13;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund14;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund15;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund16;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund17;
    private DbsField pnd_Fund_Id_Table_Pnd_Fund18;
    private DbsField pnd_Fund_Id;
    private DbsField pnd_Hold_Date;

    private DbsGroup pnd_Hold_Date__R_Field_16;
    private DbsField pnd_Hold_Date_Pnd_Hold_Yyyy;
    private DbsField pnd_Hold_Date_Pnd_Hold_Mm;
    private DbsField pnd_Hold_Date_Pnd_Hold_Dd;
    private DbsField pnd_Format_Date;

    private DbsGroup pnd_Format_Date__R_Field_17;
    private DbsField pnd_Format_Date_Pnd_Format_Mm;
    private DbsField pnd_Format_Date_Pnd_Format_Dd;
    private DbsField pnd_Format_Date_Pnd_Format_Yyyy;
    private DbsField pnd_Check_Date_Found_In_Parm;
    private DbsField pnd_Trans_Error_Sw;
    private DbsField pnd_Rep_Trans_Date_A15;

    private DbsGroup pnd_Rep_Trans_Date_A15__R_Field_18;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time;
    private DbsField pnd_Format_Datea;

    private DbsGroup pnd_Format_Datea__R_Field_19;
    private DbsField pnd_Format_Datea_Pnd_Format_Date_Mm;
    private DbsField pnd_Format_Datea_Pnd_Format_Date_Dd;
    private DbsField pnd_Format_Datea_Pnd_Format_Date_Yyyy;
    private int psgm001ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);
        ldaIaalpsgm = new LdaIaalpsgm();
        registerRecord(ldaIaalpsgm);

        // Local Variables
        parm_Card = localVariables.newFieldInRecord("parm_Card", "PARM-CARD", FieldType.NUMERIC, 8);

        parm_Card__R_Field_1 = localVariables.newGroupInRecord("parm_Card__R_Field_1", "REDEFINE", parm_Card);
        parm_Card_Pnd_Check_Date = parm_Card__R_Field_1.newFieldInGroup("parm_Card_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte", 
            "CNTRL-FRST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_VIEW_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt", 
            "CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt", 
            "CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees", 
            "CNTRL-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", 
            "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units", "CNTRL-UNITS", 
            FieldType.PACKED_DECIMAL, 13, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Amt = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys", 
            "CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys", 
            "CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees", 
            "CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        vw_iaa_Trans_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd_View", "IAA-TRANS-RCRD-VIEW"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_View_Trans_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Trans_Rcrd_View_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_View_Lst_Trans_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_View_Trans_Payee_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Payee_Cde", "TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_View_Trans_Sub_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_View_Trans_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TRANS_CDE");
        iaa_Trans_Rcrd_View_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_View_Trans_Check_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_View_Trans_Todays_Dte = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Todays_Dte", "TRANS-TODAYS-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_View_Trans_User_Area = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_View_Trans_User_Id = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_View_Trans_Verify_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Verify_Cde", "TRANS-VERIFY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_View_Trans_Verify_Id = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_View_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", 
            FieldType.STRING, 12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_View_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cwf_Wpid", "TRANS-CWF-WPID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_View_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd_View.getRecord().newFieldInGroup("iaa_Trans_Rcrd_View_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd_View);

        vw_iaa_Cntrct_Trans_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans_View", "IAA-CNTRCT-TRANS-VIEW"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_View_Trans_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cntrct_Trans_View_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_View_Lst_Trans_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_View_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Due_Dte", 
            "CNTRCT-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Pd_Dte", 
            "CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_View_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Pnsn_Pln_Cde", 
            "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_View_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Orig_Da_Cntrct_Nbr", 
            "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_View_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Rsdncy_At_Issue_Cde", 
            "CNTRCT-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Xref_Ind", 
            "CNTRCT-FIRST-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dob_Dte", 
            "CNTRCT-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Sex_Cde", 
            "CNTRCT-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Lfe_Cnt", 
            "CNTRCT-FIRST-ANNT-LFE-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_First_Annt_Dod_Dte", 
            "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Xref_Ind", 
            "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dob_Dte", 
            "CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Sex_Cde", 
            "CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Dod_Dte", 
            "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Life_Cnt", 
            "CNTRCT-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Ssn", 
            "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_View_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Div_Payee_Cde", 
            "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Div_Coll_Cde", 
            "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_View_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Cntrct_Inst_Iss_Cde", 
            "CNTRCT-INST-ISS-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_View_Trans_Check_Dte = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_View_Bfre_Imge_Id = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Bfre_Imge_Id", "BFRE-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_View_Aftr_Imge_Id = vw_iaa_Cntrct_Trans_View.getRecord().newFieldInGroup("iaa_Cntrct_Trans_View_Aftr_Imge_Id", "AFTR-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cntrct_Trans_View);

        vw_iaa_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_View", "IAA-CNTRCT-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_View_Cntrct_Optn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_View_Cntrct_Orgn_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_View_Cntrct_Acctng_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_View_Cntrct_Issue_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_View_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_View_Cntrct_Type_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_View_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_View_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_View_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_View_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_View_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_View_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_View_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_View_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_View_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_View_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_View_Lst_Trans_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrct_View);

        vw_iaa_Cntrct_Prtcpnt_Role_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role_View", "IAA-CNTRCT-PRTCPNT-ROLE-VIEW"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cpr_Id_Nbr", 
            "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Lst_Trans_Dte", 
            "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cash_Cde", 
            "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind", 
            "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Xref_Ind", 
            "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Bnfcry_Dod_Dte", 
            "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Cde", 
            "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Hold_Cde", 
            "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Pend_Dte", 
            "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cpr_Lgl_Res_Cde", 
            "CPR-LGL-RES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cpr_Xfr_Iss_Dte", 
            "CPR-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_View_Rllvr_Cntrct_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role_View);

        vw_iaa_Cpr_Trans_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans_View", "IAA-CPR-TRANS-VIEW"), "IAA_CPR_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_View_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cpr_Trans_View_Invrse_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_View_Lst_Trans_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_View_Cpr_Id_Nbr = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cpr_Trans_View_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_View_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_View_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_View_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_View_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_View_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_View_Cntrct_Company_Data = vw_iaa_Cpr_Trans_View.getRecord().newGroupInGroup("iaa_Cpr_Trans_View_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Company_Cd = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Ivc_Amt = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Rtb_Amt = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Rtb_Percent = iaa_Cpr_Trans_View_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_View_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_View_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_View_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_View_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_View_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_View_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_View_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_View_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_View_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_View_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_View_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", 
            FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_View_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_View_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_View_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_View_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_View_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_View_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_View_Cntrct_State_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_State_Cde", "CNTRCT-STATE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_View_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_View_Cntrct_Local_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_View_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_View_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_View_Trans_Check_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_View_Bfre_Imge_Id = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cpr_Trans_View_Aftr_Imge_Id = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cpr_Trans_View_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_View_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_View_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cpr_Trans_View_Rllvr_Cntrct_Nbr = vw_iaa_Cpr_Trans_View.getRecord().newFieldInGroup("iaa_Cpr_Trans_View_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        registerRecord(vw_iaa_Cpr_Trans_View);

        vw_iaa_Cref_Fund_Trans_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans_1_View", "IAA-CREF-FUND-TRANS-1-VIEW"), "IAA_CREF_FUND_TRANS_1", 
            "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_1_View_Trans_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Trans_Dte", "TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte", 
            "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_1_View_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Lst_Trans_Dte", 
            "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_CMPNY_FUND_CDE", 
            "CREF-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Trans_1_View__R_Field_2 = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_1_View__R_Field_2", "REDEFINE", 
            iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans_1_View__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Cmpny_Cde", 
            "CREF-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Cref_Fund_Trans_1_View_Cref_Fund_Cde = iaa_Cref_Fund_Trans_1_View__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Trans_1_View_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Trans_1_View_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newGroupInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_RATE_DATA_GRP", 
            "CREF-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_View_Cref_Rate_Cde = iaa_Cref_Fund_Trans_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_RATE_CDE", 
            "CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_View_Cref_Rate_Dte = iaa_Cref_Fund_Trans_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Rate_Dte", 
            "CREF-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_View_Cref_Units_Cnt = iaa_Cref_Fund_Trans_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_UNITS_CNT", 
            "CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_View_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Trans_Check_Dte", 
            "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_1_View_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Bfre_Imge_Id", 
            "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_1_View_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Aftr_Imge_Id", 
            "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cref_Fund_Trans_1_View_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_XFR_ISS_DTE", 
            "CREF-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Trans_1_View_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_1_VIEW_CREF_LST_XFR_IN_DTE", 
            "CREF-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Trans_1_View_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Trans_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_View_Cref_Lst_Xfr_Out_Dte", 
            "CREF-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Cref_Fund_Trans_1_View);

        vw_iaa_Cref_Fund_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd_1_View", "IAA-CREF-FUND-RCRD-1-VIEW"), "IAA_CREF_FUND_RCRD_1", 
            "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_CNTRCT_PAYEE_CDE", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_CMPNY_FUND_CDE", 
            "CREF-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Rcrd_1_View__R_Field_3 = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_View__R_Field_3", "REDEFINE", 
            iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_1_View__R_Field_3.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_1_View__R_Field_3.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newGroupInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_RATE_DATA_GRP", 
            "CREF-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_RATE_CDE", 
            "CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_RATE_DTE", 
            "CREF-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_1_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_VIEW_CREF_UNITS_CNT", 
            "CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_View_Lst_Trans_Dte", 
            "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        registerRecord(vw_iaa_Cref_Fund_Rcrd_1_View);

        pnd_I_Pin = localVariables.newFieldInRecord("pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 12);
        pnd_Tiaa_Fund = localVariables.newFieldInRecord("pnd_Tiaa_Fund", "#TIAA-FUND", FieldType.STRING, 1);
        pnd_Report_File = localVariables.newFieldInRecord("pnd_Report_File", "#REPORT-FILE", FieldType.STRING, 175);

        pnd_Report_File__R_Field_4 = localVariables.newGroupInRecord("pnd_Report_File__R_Field_4", "REDEFINE", pnd_Report_File);
        pnd_Report_File_Pnd_Rep_Check_Date = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Date", "#REP-CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Report_File_Pnd_Rep_Ppcn = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Ppcn", "#REP-PPCN", FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Payee = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Payee", "#REP-PAYEE", FieldType.NUMERIC, 2);
        pnd_Report_File_Pnd_Rep_First_Annt_Ind = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Annt_Ind", "#REP-FIRST-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Second_Annt_Ind = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Second_Annt_Ind", "#REP-SECOND-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Option_Code = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Option_Code", "#REP-OPTION-CODE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Mode_Ind = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Mode_Ind", "#REP-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_First_Pay_Due_Date = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Pay_Due_Date", "#REP-FIRST-PAY-DUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Report_File_Pnd_Rep_Trans_Code = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Code", "#REP-TRANS-CODE", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_Trans_Date = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Date", "#REP-TRANS-DATE", FieldType.TIME);
        pnd_Report_File_Pnd_Rep_Fund_Code = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Code", "#REP-FUND-CODE", FieldType.STRING, 
            2);
        pnd_Report_File_Pnd_Rep_Fund_Units_Before = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Before", "#REP-FUND-UNITS-BEFORE", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_After = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_After", "#REP-FUND-UNITS-AFTER", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_Diff = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Diff", "#REP-FUND-UNITS-DIFF", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Orgn_Cde = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Orgn_Cde", "#REP-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Last_Name = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Last_Name", "#REP-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_First_Name = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Name", "#REP-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_Per_Pay_Amt = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Pay_Amt", "#REP-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Per_Div_Amt = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Div_Amt", "#REP-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr", "#REP-RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Inst_Iss_Cde = pnd_Report_File__R_Field_4.newFieldInGroup("pnd_Report_File_Pnd_Rep_Inst_Iss_Cde", "#REP-INST-ISS-CDE", 
            FieldType.STRING, 5);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_FUND_KEY", "TIAA-CNTRCT-FUND-KEY", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CREF_CNTRCT_FUND_KEY");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Fund_Key.setSuperDescriptor(true);
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        pnd_Tiaa_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Key", "#TIAA-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Key__R_Field_5", "REDEFINE", pnd_Tiaa_Fund_Key);
        pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Contract = pnd_Tiaa_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Contract", "#FUND-KEY-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Payee = pnd_Tiaa_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Payee", "#FUND-KEY-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Code = pnd_Tiaa_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Code", "#FUND-KEY-CODE", FieldType.STRING, 
            3);
        pnd_Inverse_Check_Date = localVariables.newFieldInRecord("pnd_Inverse_Check_Date", "#INVERSE-CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Store_Trans_Date = localVariables.newFieldInRecord("pnd_Store_Trans_Date", "#STORE-TRANS-DATE", FieldType.TIME);
        pnd_Store_Inverse_Trans_Date = localVariables.newFieldInRecord("pnd_Store_Inverse_Trans_Date", "#STORE-INVERSE-TRANS-DATE", FieldType.NUMERIC, 
            12);
        pnd_Current_Check_Date = localVariables.newFieldInRecord("pnd_Current_Check_Date", "#CURRENT-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Check_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Current_Check_Date__R_Field_6", "REDEFINE", pnd_Current_Check_Date);
        pnd_Current_Check_Date_Pnd_Current_Check_Date_A = pnd_Current_Check_Date__R_Field_6.newFieldInGroup("pnd_Current_Check_Date_Pnd_Current_Check_Date_A", 
            "#CURRENT-CHECK-DATE-A", FieldType.STRING, 8);
        pnd_Current_First_Trans_Date = localVariables.newFieldInRecord("pnd_Current_First_Trans_Date", "#CURRENT-FIRST-TRANS-DATE", FieldType.TIME);
        pnd_Fund_Found = localVariables.newFieldInRecord("pnd_Fund_Found", "#FUND-FOUND", FieldType.STRING, 1);
        pnd_Display_Date = localVariables.newFieldInRecord("pnd_Display_Date", "#DISPLAY-DATE", FieldType.STRING, 8);
        pnd_Bef_Image_Fund_Found = localVariables.newFieldInRecord("pnd_Bef_Image_Fund_Found", "#BEF-IMAGE-FUND-FOUND", FieldType.STRING, 1);
        pnd_Aft_Image_Fund_Found = localVariables.newFieldInRecord("pnd_Aft_Image_Fund_Found", "#AFT-IMAGE-FUND-FOUND", FieldType.STRING, 1);
        pnd_Report_Recs_Written = localVariables.newFieldInRecord("pnd_Report_Recs_Written", "#REPORT-RECS-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Bef_Contract_Key = localVariables.newFieldInRecord("pnd_Bef_Contract_Key", "#BEF-CONTRACT-KEY", FieldType.STRING, 18);

        pnd_Bef_Contract_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Bef_Contract_Key__R_Field_7", "REDEFINE", pnd_Bef_Contract_Key);
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_1 = pnd_Bef_Contract_Key__R_Field_7.newFieldInGroup("pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_1", "#BEF-CONTRACT-KEY-1", 
            FieldType.STRING, 1);
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_2 = pnd_Bef_Contract_Key__R_Field_7.newFieldInGroup("pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_2", "#BEF-CONTRACT-KEY-2", 
            FieldType.STRING, 10);
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_3 = pnd_Bef_Contract_Key__R_Field_7.newFieldInGroup("pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_3", "#BEF-CONTRACT-KEY-3", 
            FieldType.TIME);
        pnd_Aft_Contract_Key = localVariables.newFieldInRecord("pnd_Aft_Contract_Key", "#AFT-CONTRACT-KEY", FieldType.STRING, 23);

        pnd_Aft_Contract_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Aft_Contract_Key__R_Field_8", "REDEFINE", pnd_Aft_Contract_Key);
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_1 = pnd_Aft_Contract_Key__R_Field_8.newFieldInGroup("pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_1", "#AFT-CONTRACT-KEY-1", 
            FieldType.STRING, 1);
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_2 = pnd_Aft_Contract_Key__R_Field_8.newFieldInGroup("pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_2", "#AFT-CONTRACT-KEY-2", 
            FieldType.STRING, 10);
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_3 = pnd_Aft_Contract_Key__R_Field_8.newFieldInGroup("pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_3", "#AFT-CONTRACT-KEY-3", 
            FieldType.NUMERIC, 12);
        pnd_Control_Key = localVariables.newFieldInRecord("pnd_Control_Key", "#CONTROL-KEY", FieldType.STRING, 10);

        pnd_Control_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Control_Key__R_Field_9", "REDEFINE", pnd_Control_Key);
        pnd_Control_Key_Pnd_Control_Key_1 = pnd_Control_Key__R_Field_9.newFieldInGroup("pnd_Control_Key_Pnd_Control_Key_1", "#CONTROL-KEY-1", FieldType.STRING, 
            2);
        pnd_Control_Key_Pnd_Control_Key_2 = pnd_Control_Key__R_Field_9.newFieldInGroup("pnd_Control_Key_Pnd_Control_Key_2", "#CONTROL-KEY-2", FieldType.NUMERIC, 
            8);
        pnd_Bef_Cpr_Key = localVariables.newFieldInRecord("pnd_Bef_Cpr_Key", "#BEF-CPR-KEY", FieldType.STRING, 20);

        pnd_Bef_Cpr_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Bef_Cpr_Key__R_Field_10", "REDEFINE", pnd_Bef_Cpr_Key);
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_1 = pnd_Bef_Cpr_Key__R_Field_10.newFieldInGroup("pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_1", "#BEF-CPR-KEY-1", FieldType.STRING, 
            1);
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_2 = pnd_Bef_Cpr_Key__R_Field_10.newFieldInGroup("pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_2", "#BEF-CPR-KEY-2", FieldType.STRING, 
            10);
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_3 = pnd_Bef_Cpr_Key__R_Field_10.newFieldInGroup("pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_3", "#BEF-CPR-KEY-3", FieldType.STRING, 
            2);
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_4 = pnd_Bef_Cpr_Key__R_Field_10.newFieldInGroup("pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_4", "#BEF-CPR-KEY-4", FieldType.TIME);
        pnd_Aft_Cpr_Key = localVariables.newFieldInRecord("pnd_Aft_Cpr_Key", "#AFT-CPR-KEY", FieldType.STRING, 25);

        pnd_Aft_Cpr_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Aft_Cpr_Key__R_Field_11", "REDEFINE", pnd_Aft_Cpr_Key);
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_1 = pnd_Aft_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_1", "#AFT-CPR-KEY-1", FieldType.STRING, 
            1);
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_2 = pnd_Aft_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_2", "#AFT-CPR-KEY-2", FieldType.STRING, 
            10);
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_3 = pnd_Aft_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_3", "#AFT-CPR-KEY-3", FieldType.STRING, 
            2);
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_4 = pnd_Aft_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_4", "#AFT-CPR-KEY-4", FieldType.NUMERIC, 
            12);
        pnd_Cpr_Master_Key = localVariables.newFieldInRecord("pnd_Cpr_Master_Key", "#CPR-MASTER-KEY", FieldType.STRING, 14);

        pnd_Cpr_Master_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Cpr_Master_Key__R_Field_12", "REDEFINE", pnd_Cpr_Master_Key);
        pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_1 = pnd_Cpr_Master_Key__R_Field_12.newFieldInGroup("pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_1", "#CPR-MASTER-KEY-1", 
            FieldType.STRING, 10);
        pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_2 = pnd_Cpr_Master_Key__R_Field_12.newFieldInGroup("pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_2", "#CPR-MASTER-KEY-2", 
            FieldType.STRING, 2);
        pnd_Bef_Fund_Key = localVariables.newFieldInRecord("pnd_Bef_Fund_Key", "#BEF-FUND-KEY", FieldType.STRING, 23);

        pnd_Bef_Fund_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Bef_Fund_Key__R_Field_13", "REDEFINE", pnd_Bef_Fund_Key);
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_1 = pnd_Bef_Fund_Key__R_Field_13.newFieldInGroup("pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_1", "#BEF-FUND-KEY-1", FieldType.STRING, 
            1);
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_2 = pnd_Bef_Fund_Key__R_Field_13.newFieldInGroup("pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_2", "#BEF-FUND-KEY-2", FieldType.STRING, 
            10);
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_3 = pnd_Bef_Fund_Key__R_Field_13.newFieldInGroup("pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_3", "#BEF-FUND-KEY-3", FieldType.STRING, 
            2);
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_4 = pnd_Bef_Fund_Key__R_Field_13.newFieldInGroup("pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_4", "#BEF-FUND-KEY-4", FieldType.TIME);
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_5 = pnd_Bef_Fund_Key__R_Field_13.newFieldInGroup("pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_5", "#BEF-FUND-KEY-5", FieldType.STRING, 
            3);
        pnd_Aft_Fund_Key = localVariables.newFieldInRecord("pnd_Aft_Fund_Key", "#AFT-FUND-KEY", FieldType.STRING, 28);

        pnd_Aft_Fund_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Aft_Fund_Key__R_Field_14", "REDEFINE", pnd_Aft_Fund_Key);
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_1 = pnd_Aft_Fund_Key__R_Field_14.newFieldInGroup("pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_1", "#AFT-FUND-KEY-1", FieldType.STRING, 
            1);
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_2 = pnd_Aft_Fund_Key__R_Field_14.newFieldInGroup("pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_2", "#AFT-FUND-KEY-2", FieldType.STRING, 
            10);
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_3 = pnd_Aft_Fund_Key__R_Field_14.newFieldInGroup("pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_3", "#AFT-FUND-KEY-3", FieldType.STRING, 
            2);
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_4 = pnd_Aft_Fund_Key__R_Field_14.newFieldInGroup("pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_4", "#AFT-FUND-KEY-4", FieldType.NUMERIC, 
            12);
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_5 = pnd_Aft_Fund_Key__R_Field_14.newFieldInGroup("pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_5", "#AFT-FUND-KEY-5", FieldType.STRING, 
            3);
        pnd_Mst_Fund_Key = localVariables.newFieldInRecord("pnd_Mst_Fund_Key", "#MST-FUND-KEY", FieldType.STRING, 15);

        pnd_Mst_Fund_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Mst_Fund_Key__R_Field_15", "REDEFINE", pnd_Mst_Fund_Key);
        pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_1 = pnd_Mst_Fund_Key__R_Field_15.newFieldInGroup("pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_1", "#MST-FUND-KEY-1", FieldType.STRING, 
            10);
        pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_2 = pnd_Mst_Fund_Key__R_Field_15.newFieldInGroup("pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_2", "#MST-FUND-KEY-2", FieldType.STRING, 
            2);
        pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_3 = pnd_Mst_Fund_Key__R_Field_15.newFieldInGroup("pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_3", "#MST-FUND-KEY-3", FieldType.STRING, 
            3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);

        pnd_Fund_Totals = localVariables.newGroupInRecord("pnd_Fund_Totals", "#FUND-TOTALS");
        pnd_Fund_Totals_Pnd_Fund_Code = pnd_Fund_Totals.newFieldArrayInGroup("pnd_Fund_Totals_Pnd_Fund_Code", "#FUND-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        pnd_Fund_Totals_Pnd_Fund_Update_Sw = pnd_Fund_Totals.newFieldArrayInGroup("pnd_Fund_Totals_Pnd_Fund_Update_Sw", "#FUND-UPDATE-SW", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        pnd_Fund_Totals_Pnd_Bef_Units = pnd_Fund_Totals.newFieldArrayInGroup("pnd_Fund_Totals_Pnd_Bef_Units", "#BEF-UNITS", FieldType.PACKED_DECIMAL, 9, 
            3, new DbsArrayController(1, 40));
        pnd_Fund_Totals_Pnd_Aft_Units = pnd_Fund_Totals.newFieldArrayInGroup("pnd_Fund_Totals_Pnd_Aft_Units", "#AFT-UNITS", FieldType.PACKED_DECIMAL, 9, 
            3, new DbsArrayController(1, 40));

        pnd_Fund_Id_Table = localVariables.newGroupInRecord("pnd_Fund_Id_Table", "#FUND-ID-TABLE");
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_1 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Tiaa_Id_1", "#TIAA-ID-1", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_2 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Tiaa_Id_2", "#TIAA-ID-2", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_3 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Tiaa_Id_3", "#TIAA-ID-3", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Stck_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Stck_Id", "#STCK-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Mma_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Mma_Id", "#MMA-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Sca_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Sca_Id", "#SCA-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Bond_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Bond_Id", "#BOND-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Global_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Global_Id", "#GLOBAL-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Equity_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Equity_Id", "#EQUITY-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Growth_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Growth_Id", "#GROWTH-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Rea_Id = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Rea_Id", "#REA-ID", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund10 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund10", "#FUND10", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund11 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund11", "#FUND11", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund12 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund12", "#FUND12", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund13 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund13", "#FUND13", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund14 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund14", "#FUND14", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund15 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund15", "#FUND15", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund16 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund16", "#FUND16", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund17 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund17", "#FUND17", FieldType.STRING, 10);
        pnd_Fund_Id_Table_Pnd_Fund18 = pnd_Fund_Id_Table.newFieldInGroup("pnd_Fund_Id_Table_Pnd_Fund18", "#FUND18", FieldType.STRING, 10);
        pnd_Fund_Id = localVariables.newFieldArrayInRecord("pnd_Fund_Id", "#FUND-ID", FieldType.STRING, 2, new DbsArrayController(1, 40));
        pnd_Hold_Date = localVariables.newFieldInRecord("pnd_Hold_Date", "#HOLD-DATE", FieldType.NUMERIC, 8);

        pnd_Hold_Date__R_Field_16 = localVariables.newGroupInRecord("pnd_Hold_Date__R_Field_16", "REDEFINE", pnd_Hold_Date);
        pnd_Hold_Date_Pnd_Hold_Yyyy = pnd_Hold_Date__R_Field_16.newFieldInGroup("pnd_Hold_Date_Pnd_Hold_Yyyy", "#HOLD-YYYY", FieldType.NUMERIC, 4);
        pnd_Hold_Date_Pnd_Hold_Mm = pnd_Hold_Date__R_Field_16.newFieldInGroup("pnd_Hold_Date_Pnd_Hold_Mm", "#HOLD-MM", FieldType.NUMERIC, 2);
        pnd_Hold_Date_Pnd_Hold_Dd = pnd_Hold_Date__R_Field_16.newFieldInGroup("pnd_Hold_Date_Pnd_Hold_Dd", "#HOLD-DD", FieldType.NUMERIC, 2);
        pnd_Format_Date = localVariables.newFieldInRecord("pnd_Format_Date", "#FORMAT-DATE", FieldType.NUMERIC, 8);

        pnd_Format_Date__R_Field_17 = localVariables.newGroupInRecord("pnd_Format_Date__R_Field_17", "REDEFINE", pnd_Format_Date);
        pnd_Format_Date_Pnd_Format_Mm = pnd_Format_Date__R_Field_17.newFieldInGroup("pnd_Format_Date_Pnd_Format_Mm", "#FORMAT-MM", FieldType.NUMERIC, 
            2);
        pnd_Format_Date_Pnd_Format_Dd = pnd_Format_Date__R_Field_17.newFieldInGroup("pnd_Format_Date_Pnd_Format_Dd", "#FORMAT-DD", FieldType.NUMERIC, 
            2);
        pnd_Format_Date_Pnd_Format_Yyyy = pnd_Format_Date__R_Field_17.newFieldInGroup("pnd_Format_Date_Pnd_Format_Yyyy", "#FORMAT-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Check_Date_Found_In_Parm = localVariables.newFieldInRecord("pnd_Check_Date_Found_In_Parm", "#CHECK-DATE-FOUND-IN-PARM", FieldType.STRING, 
            1);
        pnd_Trans_Error_Sw = localVariables.newFieldInRecord("pnd_Trans_Error_Sw", "#TRANS-ERROR-SW", FieldType.STRING, 1);
        pnd_Rep_Trans_Date_A15 = localVariables.newFieldInRecord("pnd_Rep_Trans_Date_A15", "#REP-TRANS-DATE-A15", FieldType.STRING, 15);

        pnd_Rep_Trans_Date_A15__R_Field_18 = localVariables.newGroupInRecord("pnd_Rep_Trans_Date_A15__R_Field_18", "REDEFINE", pnd_Rep_Trans_Date_A15);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy = pnd_Rep_Trans_Date_A15__R_Field_18.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy", "#REP-TRANS-YYYY", 
            FieldType.STRING, 4);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm = pnd_Rep_Trans_Date_A15__R_Field_18.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm", "#REP-TRANS-MM", 
            FieldType.STRING, 2);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd = pnd_Rep_Trans_Date_A15__R_Field_18.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd", "#REP-TRANS-DD", 
            FieldType.STRING, 2);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time = pnd_Rep_Trans_Date_A15__R_Field_18.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time", "#REP-TRANS-TIME", 
            FieldType.STRING, 7);
        pnd_Format_Datea = localVariables.newFieldInRecord("pnd_Format_Datea", "#FORMAT-DATEA", FieldType.STRING, 8);

        pnd_Format_Datea__R_Field_19 = localVariables.newGroupInRecord("pnd_Format_Datea__R_Field_19", "REDEFINE", pnd_Format_Datea);
        pnd_Format_Datea_Pnd_Format_Date_Mm = pnd_Format_Datea__R_Field_19.newFieldInGroup("pnd_Format_Datea_Pnd_Format_Date_Mm", "#FORMAT-DATE-MM", FieldType.STRING, 
            2);
        pnd_Format_Datea_Pnd_Format_Date_Dd = pnd_Format_Datea__R_Field_19.newFieldInGroup("pnd_Format_Datea_Pnd_Format_Date_Dd", "#FORMAT-DATE-DD", FieldType.STRING, 
            2);
        pnd_Format_Datea_Pnd_Format_Date_Yyyy = pnd_Format_Datea__R_Field_19.newFieldInGroup("pnd_Format_Datea_Pnd_Format_Date_Yyyy", "#FORMAT-DATE-YYYY", 
            FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1_View.reset();
        vw_iaa_Trans_Rcrd_View.reset();
        vw_iaa_Cntrct_Trans_View.reset();
        vw_iaa_Cntrct_View.reset();
        vw_iaa_Cntrct_Prtcpnt_Role_View.reset();
        vw_iaa_Cpr_Trans_View.reset();
        vw_iaa_Cref_Fund_Trans_1_View.reset();
        vw_iaa_Cref_Fund_Rcrd_1_View.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();

        ldaIaalpsgm.initializeValues();

        localVariables.reset();
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_1.setInitialValue("1 TIAA-GRP");
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_2.setInitialValue("1STIAA-STD");
        pnd_Fund_Id_Table_Pnd_Tiaa_Id_3.setInitialValue("1GTIAA-GRD");
        pnd_Fund_Id_Table_Pnd_Stck_Id.setInitialValue("02STOCK   ");
        pnd_Fund_Id_Table_Pnd_Mma_Id.setInitialValue("03MMA     ");
        pnd_Fund_Id_Table_Pnd_Sca_Id.setInitialValue("04SOCIAL  ");
        pnd_Fund_Id_Table_Pnd_Bond_Id.setInitialValue("05BOND    ");
        pnd_Fund_Id_Table_Pnd_Global_Id.setInitialValue("06GLOBAL  ");
        pnd_Fund_Id_Table_Pnd_Equity_Id.setInitialValue("07EQUITY  ");
        pnd_Fund_Id_Table_Pnd_Growth_Id.setInitialValue("08GROWTH  ");
        pnd_Fund_Id_Table_Pnd_Rea_Id.setInitialValue("09REA     ");
        pnd_Fund_Id_Table_Pnd_Fund10.setInitialValue("10fund10  ");
        pnd_Fund_Id_Table_Pnd_Fund11.setInitialValue("11fund11  ");
        pnd_Fund_Id_Table_Pnd_Fund12.setInitialValue("12fund12  ");
        pnd_Fund_Id_Table_Pnd_Fund13.setInitialValue("13fund13  ");
        pnd_Fund_Id_Table_Pnd_Fund14.setInitialValue("14fund14  ");
        pnd_Fund_Id_Table_Pnd_Fund15.setInitialValue("15fund15  ");
        pnd_Fund_Id_Table_Pnd_Fund16.setInitialValue("16fund16  ");
        pnd_Fund_Id_Table_Pnd_Fund17.setInitialValue("17fund17  ");
        pnd_Fund_Id_Table_Pnd_Fund18.setInitialValue("18fund18  ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap780() throws Exception
    {
        super("Iaap780");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  -----------------------------------------------------------------                                                                                            //Natural: FORMAT LS = 132 PS = 60
        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 PARM-CARD
        while (condition(getWorkFiles().read(1, parm_Card)))
        {
            if (condition(! (DbsUtil.maskMatches(parm_Card_Pnd_Check_Date,"CCYYMMDD"))))                                                                                  //Natural: IF #CHECK-DATE NE MASK ( CCYYMMDD )
            {
                getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                             //Natural: WRITE '**********************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"*                                            *");                                                             //Natural: WRITE '*                                            *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"* No Check Date in PARM-CARD --- will use    *");                                                             //Natural: WRITE '* No Check Date in PARM-CARD --- will use    *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"* Check Date from latest Control Record      *");                                                             //Natural: WRITE '* Check Date from latest Control Record      *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"*                                            *");                                                             //Natural: WRITE '*                                            *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                             //Natural: WRITE '**********************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Check_Date_Found_In_Parm.setValue("N");                                                                                                               //Natural: MOVE 'N' TO #CHECK-DATE-FOUND-IN-PARM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, ReportOption.NOTITLE,"CHECK-DATE =",parm_Card_Pnd_Check_Date);                                                                      //Natural: WRITE 'CHECK-DATE =' #CHECK-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Date.setValue(parm_Card_Pnd_Check_Date);                                                                                                         //Natural: MOVE #CHECK-DATE TO #HOLD-DATE
                pnd_Format_Date_Pnd_Format_Yyyy.setValue(pnd_Hold_Date_Pnd_Hold_Yyyy);                                                                                    //Natural: MOVE #HOLD-YYYY TO #FORMAT-YYYY
                pnd_Format_Date_Pnd_Format_Mm.setValue(pnd_Hold_Date_Pnd_Hold_Mm);                                                                                        //Natural: MOVE #HOLD-MM TO #FORMAT-MM
                pnd_Format_Date_Pnd_Format_Dd.setValue(pnd_Hold_Date_Pnd_Hold_Dd);                                                                                        //Natural: MOVE #HOLD-DD TO #FORMAT-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        //*  -----------------------------------------------------------------
        if (condition(pnd_Check_Date_Found_In_Parm.equals("N")))                                                                                                          //Natural: IF #CHECK-DATE-FOUND-IN-PARM = 'N'
        {
            getReports().write(0, ReportOption.NOTITLE,"Will now try to access the latest Control Record on file");                                                       //Natural: WRITE 'Will now try to access the latest Control Record on file'
            if (Global.isEscape()) return;
            pnd_Control_Key_Pnd_Control_Key_1.setValue("AA");                                                                                                             //Natural: MOVE 'AA' TO #CONTROL-KEY-1
            vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM #CONTROL-KEY
            (
            "READ01",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Control_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ01")))
            {
                if (condition(iaa_Cntrl_Rcrd_1_View_Cntrl_Cde.notEquals("AA")))                                                                                           //Natural: IF CNTRL-CDE NOT = 'AA'
                {
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                         //Natural: WRITE '**********************************************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*                                            *");                                                         //Natural: WRITE '*                                            *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"* No Control Record on File !!!              *");                                                         //Natural: WRITE '* No Control Record on File !!!              *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*         (Terminating IATP700)              *");                                                         //Natural: WRITE '*         (Terminating IATP700)              *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*                                            *");                                                         //Natural: WRITE '*                                            *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                         //Natural: WRITE '**********************************************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Current_Check_Date_Pnd_Current_Check_Date_A.setValueEdited(iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CURRENT-CHECK-DATE-A
                pnd_Current_First_Trans_Date.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte);                                                                        //Natural: MOVE CNTRL-FRST-TRANS-DTE TO #CURRENT-FIRST-TRANS-DATE
                getReports().write(0, ReportOption.NOTITLE,"CNTRL-CHECK-DTE",iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte, new ReportEditMask ("YYYYMMDD"));                     //Natural: WRITE 'CNTRL-CHECK-DTE' CNTRL-CHECK-DTE ( EM = YYYYMMDD )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"CNTRL-FRST-TRANS-DTE",iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte, new ReportEditMask ("YYYYMMDDHHIISST"));    //Natural: WRITE 'CNTRL-FRST-TRANS-DTE' CNTRL-FRST-TRANS-DTE ( EM = YYYYMMDDHHIISST )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"#CURRENT-FIRST-TRANS-DATE",pnd_Current_First_Trans_Date, new ReportEditMask ("YYYYMMDDHHIISST"));             //Natural: WRITE '#CURRENT-FIRST-TRANS-DATE' #CURRENT-FIRST-TRANS-DATE ( EM = YYYYMMDDHHIISST )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"#CURRENT-CHECK-DATE",pnd_Current_Check_Date);                                                                 //Natural: WRITE '#CURRENT-CHECK-DATE' #CURRENT-CHECK-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            //*  (CHECK DATE IS ON THE PARM)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Inverse_Check_Date.compute(new ComputeParameters(false, pnd_Inverse_Check_Date), DbsField.subtract(100000000,parm_Card_Pnd_Check_Date));                  //Natural: SUBTRACT #CHECK-DATE FROM 100000000 GIVING #INVERSE-CHECK-DATE
            getReports().write(0, ReportOption.NOTITLE,"Invert the check-date",parm_Card_Pnd_Check_Date,pnd_Inverse_Check_Date);                                          //Natural: WRITE 'Invert the check-date' #CHECK-DATE #INVERSE-CHECK-DATE
            if (Global.isEscape()) return;
            pnd_Control_Key_Pnd_Control_Key_1.setValue("AA");                                                                                                             //Natural: MOVE 'AA' TO #CONTROL-KEY-1
            pnd_Control_Key_Pnd_Control_Key_2.setValue(pnd_Inverse_Check_Date);                                                                                           //Natural: MOVE #INVERSE-CHECK-DATE TO #CONTROL-KEY-2
            vw_iaa_Cntrl_Rcrd_1_View.startDatabaseFind                                                                                                                    //Natural: FIND IAA-CNTRL-RCRD-1-VIEW WITH CNTRL-RCRD-KEY = #CONTROL-KEY
            (
            "FIND01",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", "=", pnd_Control_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("FIND01", true)))
            {
                vw_iaa_Cntrl_Rcrd_1_View.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrl_Rcrd_1_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                         //Natural: WRITE '**********************************************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*                                            *");                                                         //Natural: WRITE '*                                            *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"* No Control Record found for CHECK-DATE !!! *");                                                         //Natural: WRITE '* No Control Record found for CHECK-DATE !!! *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*         (Terminating IATP700)              *");                                                         //Natural: WRITE '*         (Terminating IATP700)              *'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                         //Natural: WRITE '**********************************************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"CHECK-DATE =",parm_Card_Pnd_Check_Date);                                                                  //Natural: WRITE 'CHECK-DATE =' #CHECK-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                         //Natural: WRITE '**********************************************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Current_Check_Date_Pnd_Current_Check_Date_A.setValueEdited(iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CURRENT-CHECK-DATE-A
                pnd_Current_First_Trans_Date.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte);                                                                        //Natural: MOVE CNTRL-FRST-TRANS-DTE TO #CURRENT-FIRST-TRANS-DATE
                getReports().write(0, ReportOption.NOTITLE,"CNTRL-CHECK-DTE",iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte, new ReportEditMask ("YYYYMMDD"));                     //Natural: WRITE 'CNTRL-CHECK-DTE' CNTRL-CHECK-DTE ( EM = YYYYMMDD )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"CNTRL-FRST-TRANS-DTE",iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte, new ReportEditMask ("YYYYMMDDHHIISST"));    //Natural: WRITE 'CNTRL-FRST-TRANS-DTE' CNTRL-FRST-TRANS-DTE ( EM = YYYYMMDDHHIISST )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"#CURRENT-FIRST-TRANS-DATE",pnd_Current_First_Trans_Date, new ReportEditMask ("YYYYMMDDHHIISST"));             //Natural: WRITE '#CURRENT-FIRST-TRANS-DATE' #CURRENT-FIRST-TRANS-DATE ( EM = YYYYMMDDHHIISST )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"#CURRENT-CHECK-DATE",pnd_Current_Check_Date);                                                                 //Natural: WRITE '#CURRENT-CHECK-DATE' #CURRENT-CHECK-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"------------------------------------------------------");                                                         //Natural: WRITE '------------------------------------------------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        //*  CALLNAT 'IAAN0500' IAAA0500
        //*  #FUND-ID(1:20) := IAAA0500.#IA-STD-NM-CD(1:20)
        //*  ADDED 5/00
        //*  ADDED 5/00
        //*  CHANGED TO 40 FROM 20  5/00
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Fund_Id.getValue(1,":",40).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(1,":",40));                                                           //Natural: ASSIGN #FUND-ID ( 1:40 ) := IAAA051Z.#IA-STD-NM-CD ( 1:40 )
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            pnd_Fund_Totals_Pnd_Fund_Code.getValue(pnd_I).setValue(pnd_Fund_Id.getValue(pnd_I));                                                                          //Natural: MOVE #FUND-ID ( #I ) TO #FUND-CODE ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  -----------------------------------------------------------------
        pnd_Report_File.reset();                                                                                                                                          //Natural: RESET #REPORT-FILE
        pnd_Hold_Date.setValue(pnd_Current_Check_Date);                                                                                                                   //Natural: MOVE #CURRENT-CHECK-DATE TO #HOLD-DATE
        pnd_Format_Date_Pnd_Format_Yyyy.setValue(pnd_Hold_Date_Pnd_Hold_Yyyy);                                                                                            //Natural: MOVE #HOLD-YYYY TO #FORMAT-YYYY
        pnd_Format_Date_Pnd_Format_Mm.setValue(pnd_Hold_Date_Pnd_Hold_Mm);                                                                                                //Natural: MOVE #HOLD-MM TO #FORMAT-MM
        pnd_Format_Date_Pnd_Format_Dd.setValue(pnd_Hold_Date_Pnd_Hold_Dd);                                                                                                //Natural: MOVE #HOLD-DD TO #FORMAT-DD
        getReports().write(0, ReportOption.NOTITLE,"BEFORE ACCESS TO TRANSACTION RECORD LOOP");                                                                           //Natural: WRITE 'BEFORE ACCESS TO TRANSACTION RECORD LOOP'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"#CURRENT-CHECK-DATE =",pnd_Current_Check_Date);                                                                       //Natural: WRITE '#CURRENT-CHECK-DATE =' #CURRENT-CHECK-DATE
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"---");                                                                                                                //Natural: WRITE '---'
        if (Global.isEscape()) return;
        vw_iaa_Trans_Rcrd_View.startDatabaseRead                                                                                                                          //Natural: READ IAA-TRANS-RCRD-VIEW WITH TRANS-CHCK-DTE-KEY = #CURRENT-CHECK-DATE
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Current_Check_Date, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd_View.readNextRow("READ02")))
        {
            //*  JB01
            pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr.reset();                                                                                                             //Natural: RESET #REP-RLLVR-CNTRCT-NBR
            //*     WRITE '======================================================='
            //*     WRITE '=                NEW TRANSACTION RECORD               '
            //*     WRITE '======================================================='
            //*     WRITE 'WITHIN THE READ TRANS, BEFORE THE ACCEPT STATEMENT'
            //*  WRITE 'TRANS/SUB Cd:' TRANS-CDE '/' TRANS-SUB-CDE
            //*    'PPCN:' TRANS-PPCN-NBR
            //*    'Paye:' TRANS-PAYEE-CDE
            //*           'VFY CD:' TRANS-VERIFY-CDE
            //*           'ACTVCD:' TRANS-ACTVTY-CDE
            //*     WRITE 'TRANS CHECK DATE:' TRANS-CHECK-DTE
            //*           'TRANS EFFCT DATE:' TRANS-EFFCTVE-DTE
            //*     --------------------------------------------------------------
            if (condition(!((((((((((((iaa_Trans_Rcrd_View_Trans_Cde.equals(6) || iaa_Trans_Rcrd_View_Trans_Cde.equals(20)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(30))  //Natural: ACCEPT IF ( TRANS-CDE = 006 OR = 020 OR = 030 OR = 031 OR = 033 OR = 035 OR = 037 OR = 040 OR = 050 OR = 060 OR = 066 ) AND ( TRANS-VERIFY-CDE NOT = 'V' )
                || iaa_Trans_Rcrd_View_Trans_Cde.equals(31)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(33)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(35)) || 
                iaa_Trans_Rcrd_View_Trans_Cde.equals(37)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(40)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(50)) || iaa_Trans_Rcrd_View_Trans_Cde.equals(60)) 
                || iaa_Trans_Rcrd_View_Trans_Cde.equals(66)) && iaa_Trans_Rcrd_View_Trans_Verify_Cde.notEquals("V")))))
            {
                continue;
            }
            pnd_Rep_Trans_Date_A15.setValueEdited(iaa_Trans_Rcrd_View_Trans_Dte,new ReportEditMask("YYYYMMDDHHIISST"));                                                   //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDDHHIISST ) TO #REP-TRANS-DATE-A15
            pnd_Format_Datea_Pnd_Format_Date_Mm.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm);                                                                        //Natural: MOVE #REP-TRANS-MM TO #FORMAT-DATE-MM
            pnd_Format_Datea_Pnd_Format_Date_Dd.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd);                                                                        //Natural: MOVE #REP-TRANS-DD TO #FORMAT-DATE-DD
            pnd_Format_Datea_Pnd_Format_Date_Yyyy.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy);                                                                    //Natural: MOVE #REP-TRANS-YYYY TO #FORMAT-DATE-YYYY
            //*  IF #FORMAT-DATE-YYYY < '1997' OR
            //*     (#FORMAT-DATE-YYYY = '1997' AND
            //*      #FORMAT-DATE-MM   = '01' AND
            //*      #FORMAT-DATE-DD   = '01')
            //*     IGNORE
            //*  ELSE
            //*     ESCAPE TOP
            //*  END-IF
            //*  KB ADDED
            if (condition(iaa_Trans_Rcrd_View_Trans_Check_Dte.greater(pnd_Current_Check_Date)))                                                                           //Natural: IF TRANS-CHECK-DTE > #CURRENT-CHECK-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*     (ADDITONAL BYPASS LOGIC AS PER IA TEAM)
            if (condition((iaa_Trans_Rcrd_View_Trans_Cde.equals(37)) && (iaa_Trans_Rcrd_View_Trans_Sub_Cde.equals("37B"))))                                               //Natural: IF ( TRANS-CDE = 037 ) AND ( TRANS-SUB-CDE = '37B' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((iaa_Trans_Rcrd_View_Trans_Cde.equals(37) && iaa_Trans_Rcrd_View_Trans_Sub_Cde.equals("066")) && (iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(2)  //Natural: IF ( TRANS-CDE = 037 ) AND ( TRANS-SUB-CDE = '066' ) AND ( TRANS-PAYEE-CDE = 02 OR = 03 )
                || iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(3)))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition((iaa_Trans_Rcrd_View_Trans_Cde.equals(33)) && (iaa_Trans_Rcrd_View_Trans_Actvty_Cde.equals("R"))))                                              //Natural: IF ( TRANS-CDE = 033 ) AND ( TRANS-ACTVTY-CDE = 'R' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*     --------------------------------------------------------------
            //*  FIELDS FROM THE TRANS-RECORD TO THE REPORT FILE RECORD
            pnd_Report_File_Pnd_Rep_Check_Date.setValue(pnd_Current_Check_Date);                                                                                          //Natural: MOVE #CURRENT-CHECK-DATE TO #REP-CHECK-DATE
            pnd_Report_File_Pnd_Rep_Ppcn.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                                    //Natural: MOVE TRANS-PPCN-NBR TO #REP-PPCN
            pnd_Report_File_Pnd_Rep_Payee.setValue(iaa_Trans_Rcrd_View_Trans_Payee_Cde);                                                                                  //Natural: MOVE TRANS-PAYEE-CDE TO #REP-PAYEE
            pnd_Report_File_Pnd_Rep_Trans_Code.setValue(iaa_Trans_Rcrd_View_Trans_Cde);                                                                                   //Natural: MOVE TRANS-CDE TO #REP-TRANS-CODE
            pnd_Report_File_Pnd_Rep_Trans_Date.setValue(iaa_Trans_Rcrd_View_Trans_Dte);                                                                                   //Natural: MOVE IAA-TRANS-RCRD-VIEW.TRANS-DTE TO #REP-TRANS-DATE
            //*  WRITE 'rep-payee:' #REP-PAYEE (EM=99)
            //*     --------------------------------------------------------------
            pnd_Trans_Error_Sw.setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #TRANS-ERROR-SW
            //*     --------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-INFO
            sub_Get_Contract_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Trans_Error_Sw.equals("Y")))                                                                                                                //Natural: IF #TRANS-ERROR-SW = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*     --------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM GET-CPR-INFO
            sub_Get_Cpr_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Trans_Error_Sw.equals("Y")))                                                                                                                //Natural: IF #TRANS-ERROR-SW = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*     --------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM GET-FUND-INFO-AND-LOAD-TABLE
            sub_Get_Fund_Info_And_Load_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     --------------------------------------------------------------
            //*  WRITE '------------------------------------------------'
            //*  WRITE '  Fund Table values to be written to workfile   '
            //*  WRITE '------------------------------------------------'
            //*  FOR #I 1 TO 40 /* CHANGED FROM 20 TO 40  5/00
            //*    IF #FUND-UPDATE-SW(#I) = 'Y'
            //*      WRITE '(' #I ') ' #FUND-CODE(#I) #FUND-UPDATE-SW(#I)
            //*        #BEF-UNITS(#I) #AFT-UNITS(#I)
            //*    END-IF
            //*  END-FOR
            //*     --------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC-FROM-TABLE
            sub_Write_Report_Rec_From_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     --------------------------------------------------------------
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM EOJ-RTN
        sub_Eoj_Rtn();
        if (condition(Global.isEscape())) {return;}
        //*  (END OF MAINLINE ROUTINE --- ALL SUBROUTINES FOLLOW)
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT-INFO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-INFO
        //*    #COR-KEY1-PIN      :=IAA-CPR-TRANS-VIEW.CPR-ID-NBR
        //*      #COR-KEY1-PIN := IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CPR-ID-NBR
        //*  #COR-KEY1-PIN      :=IAA-CPR-TRANS-VIEW.CPR-ID-NBR
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-INFO-AND-LOAD-TABLE
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ALL-AFT-FUND-RECS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC-FROM-TABLE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR
        //* *********************************************************************
        //* *#COR-KEY1-RT       := 1
        //* *READ (1) COR-PH BY COR-SUPER-PIN-RCDTYPE      FROM
        //*     #COR-KEY1
        //*   IF PH-UNIQUE-ID-NBR  = #COR-KEY1-PIN       AND
        //*       PH-RCD-TYPE-CDE   = #COR-KEY1-RT
        //*     #REP-LAST-NAME :=  PH-LAST-NME
        //*     #REP-FIRST-NAME := PH-FIRST-NME
        //*   ELSE
        //*     WRITE   'COR XREF FILE REC NOT FOUND' #COR-KEY1
        //*   END-IF
        //* *END-READ
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EOJ-RTN
    }
    private void sub_Get_Contract_Info() throws Exception                                                                                                                 //Natural: GET-CONTRACT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  -----------------------------------------------------
        //*  CHECK FOR BEFORE IMAGE OF CONTRACT TRANSACTION RECORD
        //*  -----------------------------------------------------
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_1.setValue("1");                                                                                                        //Natural: MOVE '1' TO #BEF-CONTRACT-KEY-1
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                         //Natural: MOVE TRANS-PPCN-NBR TO #BEF-CONTRACT-KEY-2
        pnd_Bef_Contract_Key_Pnd_Bef_Contract_Key_3.setValue(iaa_Trans_Rcrd_View_Trans_Dte);                                                                              //Natural: MOVE IAA-TRANS-RCRD-VIEW.TRANS-DTE TO #BEF-CONTRACT-KEY-3
        //* *WRITE 'IAA-TRANS-RCRD-VIEW.TRANS-DTE'
        //* *  IAA-TRANS-RCRD-VIEW.TRANS-DTE (EM=YYYYMMDDHHIISST)
        vw_iaa_Cntrct_Trans_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRCT-TRANS-VIEW BY CNTRCT-BFRE-KEY STARTING FROM #BEF-CONTRACT-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Bef_Contract_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Cntrct_Trans_View.readNextRow("READ03")))
        {
            //*  WRITE 'IAA-CNTRCT-TRANS-VIEW.Trans-Dte ='
            //*    IAA-CNTRCT-TRANS-VIEW.TRANS-DTE (EM=YYYYMMDDHHIISST)
            if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cntrct_Trans_View_Cntrct_Ppcn_Nbr)))                                                              //Natural: IF TRANS-PPCN-NBR = IAA-CNTRCT-TRANS-VIEW.CNTRCT-PPCN-NBR
            {
                pnd_Report_File_Pnd_Rep_First_Annt_Ind.setValue(iaa_Cntrct_Trans_View_Cntrct_First_Annt_Xref_Ind);                                                        //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-FIRST-ANNT-XREF-IND TO #REP-FIRST-ANNT-IND
                pnd_Report_File_Pnd_Rep_Second_Annt_Ind.setValue(iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Xref_Ind);                                                        //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-SCND-ANNT-XREF-IND TO #REP-SECOND-ANNT-IND
                //*  ADDED 5/00
                pnd_Report_File_Pnd_Rep_Option_Code.setValue(iaa_Cntrct_Trans_View_Cntrct_Optn_Cde);                                                                      //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-OPTN-CDE TO #REP-OPTION-CODE
                pnd_Report_File_Pnd_Rep_Orgn_Cde.setValue(iaa_Cntrct_Trans_View_Cntrct_Orgn_Cde);                                                                         //Natural: ASSIGN #REP-ORGN-CDE := IAA-CNTRCT-TRANS-VIEW.CNTRCT-ORGN-CDE
                pnd_Report_File_Pnd_Rep_First_Pay_Due_Date.setValue(iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Due_Dte);                                                    //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE TO #REP-FIRST-PAY-DUE-DATE
                //*  JB01
                pnd_Report_File_Pnd_Rep_Inst_Iss_Cde.setValue(iaa_Cntrct_Trans_View_Cntrct_Inst_Iss_Cde);                                                                 //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-INST-ISS-CDE TO #REP-INST-ISS-CDE
                //*    WRITE 'Found Before Image for Contract Transaction Record'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*       --------------------------------
                //*       CHECK FOR CONTRACT MASTER RECORD
                //*       --------------------------------
                vw_iaa_Cntrct_View.startDatabaseFind                                                                                                                      //Natural: FIND IAA-CNTRCT-VIEW WITH IAA-CNTRCT-VIEW.CNTRCT-PPCN-NBR = TRANS-PPCN-NBR
                (
                "FIND02",
                new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_iaa_Cntrct_View.readNextRow("FIND02", true)))
                {
                    vw_iaa_Cntrct_View.setIfNotFoundControlFlag(false);
                    if (condition(vw_iaa_Cntrct_View.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"* NO MASTER Contract record found for PPCN   *");                                                     //Natural: WRITE '* NO MASTER Contract record found for PPCN   *'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"PPCN =",iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                          //Natural: WRITE 'PPCN =' TRANS-PPCN-NBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* ************* TERMINATE
                        pnd_Trans_Error_Sw.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #TRANS-ERROR-SW
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Report_File_Pnd_Rep_First_Annt_Ind.setValue(iaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind);                                                          //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-FIRST-ANNT-XREF-IND TO #REP-FIRST-ANNT-IND
                    pnd_Report_File_Pnd_Rep_Second_Annt_Ind.setValue(iaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind);                                                          //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-SCND-ANNT-XREF-IND TO #REP-SECOND-ANNT-IND
                    //*  ADDED 5/00
                    pnd_Report_File_Pnd_Rep_Option_Code.setValue(iaa_Cntrct_View_Cntrct_Optn_Cde);                                                                        //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-OPTN-CDE TO #REP-OPTION-CODE
                    pnd_Report_File_Pnd_Rep_Orgn_Cde.setValue(iaa_Cntrct_View_Cntrct_Orgn_Cde);                                                                           //Natural: ASSIGN #REP-ORGN-CDE := IAA-CNTRCT-VIEW.CNTRCT-ORGN-CDE
                    pnd_Report_File_Pnd_Rep_First_Pay_Due_Date.setValue(iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte);                                                      //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE TO #REP-FIRST-PAY-DUE-DATE
                    //*  JB01
                    pnd_Report_File_Pnd_Rep_Inst_Iss_Cde.setValue(iaa_Cntrct_View_Cntrct_Inst_Iss_Cde);                                                                   //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-INST-ISS-CDE TO #REP-INST-ISS-CDE
                    //*      WRITE 'Using Master Contract Record - no before image'
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ----------------------------------------------------
        //*  CHECK FOR AFTER IMAGE OF CONTRACT TRANSACTION RECORD
        //*  ----------------------------------------------------
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_1.setValue("2");                                                                                                        //Natural: MOVE '2' TO #AFT-CONTRACT-KEY-1
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                         //Natural: MOVE TRANS-PPCN-NBR TO #AFT-CONTRACT-KEY-2
        pnd_Aft_Contract_Key_Pnd_Aft_Contract_Key_3.setValue(iaa_Trans_Rcrd_View_Invrse_Trans_Dte);                                                                       //Natural: MOVE IAA-TRANS-RCRD-VIEW.INVRSE-TRANS-DTE TO #AFT-CONTRACT-KEY-3
        vw_iaa_Cntrct_Trans_View.startDatabaseFind                                                                                                                        //Natural: FIND IAA-CNTRCT-TRANS-VIEW WITH CNTRCT-AFTR-KEY = #AFT-CONTRACT-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", "=", pnd_Aft_Contract_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_iaa_Cntrct_Trans_View.readNextRow("FIND03", true)))
        {
            vw_iaa_Cntrct_Trans_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct_Trans_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORDS FOUND
            {
                //*    WRITE / '*** No AFT Image CONTRACT TRANS - using BEF or MST' /
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Report_File_Pnd_Rep_First_Annt_Ind.setValue(iaa_Cntrct_Trans_View_Cntrct_First_Annt_Xref_Ind);                                                            //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-FIRST-ANNT-XREF-IND TO #REP-FIRST-ANNT-IND
            pnd_Report_File_Pnd_Rep_Second_Annt_Ind.setValue(iaa_Cntrct_Trans_View_Cntrct_Scnd_Annt_Xref_Ind);                                                            //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-SCND-ANNT-XREF-IND TO #REP-SECOND-ANNT-IND
            //*  ADDED 5/00
            pnd_Report_File_Pnd_Rep_Option_Code.setValue(iaa_Cntrct_Trans_View_Cntrct_Optn_Cde);                                                                          //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-OPTN-CDE TO #REP-OPTION-CODE
            pnd_Report_File_Pnd_Rep_Orgn_Cde.setValue(iaa_Cntrct_Trans_View_Cntrct_Orgn_Cde);                                                                             //Natural: ASSIGN #REP-ORGN-CDE := IAA-CNTRCT-TRANS-VIEW.CNTRCT-ORGN-CDE
            pnd_Report_File_Pnd_Rep_First_Pay_Due_Date.setValue(iaa_Cntrct_Trans_View_Cntrct_First_Pymnt_Due_Dte);                                                        //Natural: MOVE IAA-CNTRCT-TRANS-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE TO #REP-FIRST-PAY-DUE-DATE
            //*  WRITE 'Found AFTER Image for Contract Transaction Record'
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr_Info() throws Exception                                                                                                                      //Natural: GET-CPR-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  ------------------------------------------------
        //*  CHECK FOR BEFORE IMAGE OF CPR TRANSACTION RECORD
        //*  ------------------------------------------------
        pnd_Tiaa_Fund.reset();                                                                                                                                            //Natural: RESET #TIAA-FUND
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_1.setValue("1");                                                                                                                  //Natural: MOVE '1' TO #BEF-CPR-KEY-1
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                                   //Natural: MOVE TRANS-PPCN-NBR TO #BEF-CPR-KEY-2
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_3.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                                   //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #BEF-CPR-KEY-3
        pnd_Bef_Cpr_Key_Pnd_Bef_Cpr_Key_4.setValue(iaa_Trans_Rcrd_View_Trans_Dte);                                                                                        //Natural: MOVE IAA-TRANS-RCRD-VIEW.TRANS-DTE TO #BEF-CPR-KEY-4
        //* *WRITE 'cpr-before-key =' #BEF-CPR-KEY
        //* *  'cpr-bef-key-4  =' #BEF-CPR-KEY-4 (EM=YYYYMMDDHHIISST)
        vw_iaa_Cpr_Trans_View.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) IAA-CPR-TRANS-VIEW BY CPR-BFRE-KEY STARTING FROM #BEF-CPR-KEY
        (
        "READ04",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Bef_Cpr_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_iaa_Cpr_Trans_View.readNextRow("READ04")))
        {
            //*  WRITE '...within the read (1) of the before image of cpr trans'
            //*  WRITE 'Trans-PPCN-nbr  =' TRANS-PPCN-NBR
            //*    ' Trans-Payee-cd =' TRANS-PAYEE-CDE
            //*  WRITE 'CPR PPCN        =' IAA-CPR-TRANS-VIEW.CNTRCT-PART-PPCN-NBR
            //*    ' CPR PAYEE      =' IAA-CPR-TRANS-VIEW.CNTRCT-PART-PAYEE-CDE
            //*    ' CPR trns-dt =' IAA-CPR-TRANS-VIEW.TRANS-DTE
            //*    (EM=YYYYMMDDHHIISST)
            if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cpr_Trans_View_Cntrct_Part_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cpr_Trans_View_Cntrct_Part_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR = IAA-CPR-TRANS-VIEW.CNTRCT-PART-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CPR-TRANS-VIEW.CNTRCT-PART-PAYEE-CDE
            {
                //*    WRITE 'Found Before Image of CPR Transaction Record'
                pnd_Report_File_Pnd_Rep_Mode_Ind.setValue(iaa_Cpr_Trans_View_Cntrct_Mode_Ind);                                                                            //Natural: MOVE IAA-CPR-TRANS-VIEW.CNTRCT-MODE-IND TO #REP-MODE-IND
                if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.greaterOrEqual("W0541000") && iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.lessOrEqual("W0799999")))               //Natural: IF TRANS-PPCN-NBR = 'W0541000' THRU 'W0799999'
                {
                    pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr.setValue(iaa_Cpr_Trans_View_Rllvr_Cntrct_Nbr);                                                               //Natural: MOVE IAA-CPR-TRANS-VIEW.RLLVR-CNTRCT-NBR TO #REP-RLLVR-CNTRCT-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Cpr_Trans_View_Cntrct_Company_Cd.getValue("*").equals("T")))                                                                            //Natural: IF IAA-CPR-TRANS-VIEW.CNTRCT-COMPANY-CD ( * ) = 'T'
                {
                    pnd_Tiaa_Fund.setValue("Y");                                                                                                                          //Natural: ASSIGN #TIAA-FUND := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                pnd_I_Pin.setValue(iaa_Cpr_Trans_View_Cpr_Id_Nbr);                                                                                                        //Natural: ASSIGN #I-PIN := IAA-CPR-TRANS-VIEW.CPR-ID-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*       ---------------------------
                //*       CHECK FOR CPR MASTER RECORD
                //*       ---------------------------
                pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_1.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                     //Natural: MOVE TRANS-PPCN-NBR TO #CPR-MASTER-KEY-1
                pnd_Cpr_Master_Key_Pnd_Cpr_Master_Key_2.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                     //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #CPR-MASTER-KEY-2
                vw_iaa_Cntrct_Prtcpnt_Role_View.startDatabaseFind                                                                                                         //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE-VIEW WITH IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-PAYEE-KEY = #CPR-MASTER-KEY
                (
                "FIND04",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Master_Key, WcType.WITH) }
                );
                FIND04:
                while (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.readNextRow("FIND04", true)))
                {
                    vw_iaa_Cntrct_Prtcpnt_Role_View.setIfNotFoundControlFlag(false);
                    if (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"* NO MASTER CPR record found for PPCN/PAYEE  *");                                                     //Natural: WRITE '* NO MASTER CPR record found for PPCN/PAYEE  *'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"PPCN =",iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr," PAYEE =",iaa_Trans_Rcrd_View_Trans_Payee_Cde);           //Natural: WRITE 'PPCN =' TRANS-PPCN-NBR ' PAYEE =' TRANS-PAYEE-CDE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"**********************************************");                                                     //Natural: WRITE '**********************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* ************* TERMINATE
                        pnd_Trans_Error_Sw.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #TRANS-ERROR-SW
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Report_File_Pnd_Rep_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind);                                                              //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-MODE-IND TO #REP-MODE-IND
                    if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.greaterOrEqual("W0541000") && iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.lessOrEqual("W0799999")))           //Natural: IF TRANS-PPCN-NBR = 'W0541000' THRU 'W0799999'
                    {
                        //*  JB01
                        pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_View_Rllvr_Cntrct_Nbr);                                                 //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE-VIEW.RLLVR-CNTRCT-NBR TO #REP-RLLVR-CNTRCT-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Company_Cd.getValue("*").equals("T")))                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-COMPANY-CD ( * ) = 'T'
                    {
                        pnd_Tiaa_Fund.setValue("Y");                                                                                                                      //Natural: ASSIGN #TIAA-FUND := 'Y'
                        //*  10/2014
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_I_Pin.setValue(iaa_Cntrct_Prtcpnt_Role_View_Cpr_Id_Nbr);                                                                                          //Natural: ASSIGN #I-PIN := IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CPR-ID-NBR
                    //*      WRITE 'Accessed Master CPR record - no before image found'
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  -----------------------------------------------
        //*  CHECK FOR AFTER IMAGE OF CPR TRANSACTION RECORD
        //*  -----------------------------------------------
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_1.setValue("2");                                                                                                                  //Natural: MOVE '2' TO #AFT-CPR-KEY-1
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                                   //Natural: MOVE TRANS-PPCN-NBR TO #AFT-CPR-KEY-2
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_3.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                                   //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #AFT-CPR-KEY-3
        pnd_Aft_Cpr_Key_Pnd_Aft_Cpr_Key_4.setValue(iaa_Trans_Rcrd_View_Invrse_Trans_Dte);                                                                                 //Natural: MOVE IAA-TRANS-RCRD-VIEW.INVRSE-TRANS-DTE TO #AFT-CPR-KEY-4
        vw_iaa_Cpr_Trans_View.startDatabaseFind                                                                                                                           //Natural: FIND IAA-CPR-TRANS-VIEW WITH CPR-AFTR-KEY = #AFT-CPR-KEY
        (
        "FIND05",
        new Wc[] { new Wc("CPR_AFTR_KEY", "=", pnd_Aft_Cpr_Key, WcType.WITH) }
        );
        FIND05:
        while (condition(vw_iaa_Cpr_Trans_View.readNextRow("FIND05", true)))
        {
            vw_iaa_Cpr_Trans_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cpr_Trans_View.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
            {
                //*    WRITE / '*** No AFT Image CPR TRANS - using BEF or MST' /
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Report_File_Pnd_Rep_Mode_Ind.setValue(iaa_Cpr_Trans_View_Cntrct_Mode_Ind);                                                                                //Natural: MOVE IAA-CPR-TRANS-VIEW.CNTRCT-MODE-IND TO #REP-MODE-IND
            if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.greaterOrEqual("W0541000") && iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.lessOrEqual("W0799999")))                   //Natural: IF TRANS-PPCN-NBR = 'W0541000' THRU 'W0799999'
            {
                pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr.setValue(iaa_Cpr_Trans_View_Rllvr_Cntrct_Nbr);                                                                   //Natural: MOVE IAA-CPR-TRANS-VIEW.RLLVR-CNTRCT-NBR TO #REP-RLLVR-CNTRCT-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cpr_Trans_View_Cntrct_Company_Cd.getValue("*").equals("T")))                                                                                //Natural: IF IAA-CPR-TRANS-VIEW.CNTRCT-COMPANY-CD ( * ) = 'T'
            {
                pnd_Tiaa_Fund.setValue("Y");                                                                                                                              //Natural: ASSIGN #TIAA-FUND := 'Y'
                //*  10/2014
            }                                                                                                                                                             //Natural: END-IF
            pnd_I_Pin.setValue(iaa_Cpr_Trans_View_Cpr_Id_Nbr);                                                                                                            //Natural: ASSIGN #I-PIN := IAA-CPR-TRANS-VIEW.CPR-ID-NBR
            //*  WRITE 'Found After Image of CPR Transaction Rec - Using it !'
            //*  WRITE 'CPR after-image trans-dte ='
            //*    IAA-CPR-TRANS-VIEW.TRANS-DTE (EM=YYYYMMDDHHIISST)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Contract.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                             //Natural: MOVE TRANS-PPCN-NBR TO #FUND-KEY-CONTRACT
        pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Payee.setValue(iaa_Trans_Rcrd_View_Trans_Payee_Cde);                                                                               //Natural: MOVE TRANS-PAYEE-CDE TO #FUND-KEY-PAYEE
        pnd_Report_File_Pnd_Rep_Per_Pay_Amt.reset();                                                                                                                      //Natural: RESET #REP-PER-PAY-AMT #REP-PER-DIV-AMT
        pnd_Report_File_Pnd_Rep_Per_Div_Amt.reset();
        if (condition(pnd_Tiaa_Fund.equals("Y")))                                                                                                                         //Natural: IF #TIAA-FUND = 'Y'
        {
            //*  WRITE '=' #TIAA-FUND-KEY
            vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY FROM #TIAA-FUND-KEY
            (
            "READ05",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Tiaa_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
            1
            );
            READ05:
            while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ05")))
            {
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.equals(pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Contract) && iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.equals(pnd_Tiaa_Fund_Key_Pnd_Fund_Key_Payee))) //Natural: IF TIAA-CNTRCT-PPCN-NBR = #FUND-KEY-CONTRACT AND TIAA-CNTRCT-PAYEE-CDE = #FUND-KEY-PAYEE
                {
                    pnd_Report_File_Pnd_Rep_Per_Pay_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                    //Natural: ASSIGN #REP-PER-PAY-AMT := TIAA-TOT-PER-AMT
                    pnd_Report_File_Pnd_Rep_Per_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                    //Natural: ASSIGN #REP-PER-DIV-AMT := TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().display(0, "TIAA FUND RECORD NOT FOUND",                                                                                                 //Natural: DISPLAY 'TIAA FUND RECORD NOT FOUND' #TIAA-FUND-KEY
                    		pnd_Tiaa_Fund_Key);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  JB01
                                                                                                                                                                          //Natural: PERFORM READ-COR
        sub_Read_Cor();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Get_Fund_Info_And_Load_Table() throws Exception                                                                                                      //Natural: GET-FUND-INFO-AND-LOAD-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Fund_Totals_Pnd_Bef_Units.getValue("*").reset();                                                                                                              //Natural: RESET #BEF-UNITS ( * ) #AFT-UNITS ( * ) #FUND-UPDATE-SW ( * )
        pnd_Fund_Totals_Pnd_Aft_Units.getValue("*").reset();
        pnd_Fund_Totals_Pnd_Fund_Update_Sw.getValue("*").reset();
        //*  --------------------------------------------------------
        //*  GET THE MOST AVAILABLE TRANSACTION DATE FOR FUND RECORDS
        //*  --------------------------------------------------------
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_1.setValue("1");                                                                                                                //Natural: MOVE '1' TO #BEF-FUND-KEY-1
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                                 //Natural: MOVE TRANS-PPCN-NBR TO #BEF-FUND-KEY-2
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_3.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                                 //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #BEF-FUND-KEY-3
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_4.setValue(iaa_Trans_Rcrd_View_Trans_Dte);                                                                                      //Natural: MOVE IAA-TRANS-RCRD-VIEW.TRANS-DTE TO #BEF-FUND-KEY-4
        pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_5.setValue("   ");                                                                                                              //Natural: MOVE '   ' TO #BEF-FUND-KEY-5
        pnd_Bef_Image_Fund_Found.setValue("N");                                                                                                                           //Natural: MOVE 'N' TO #BEF-IMAGE-FUND-FOUND
        vw_iaa_Cref_Fund_Trans_1_View.startDatabaseRead                                                                                                                   //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS-1-VIEW BY CREF-FUND-BFRE-KEY-2 STARTING FROM #BEF-FUND-KEY
        (
        "READ06",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Bef_Fund_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") },
        1
        );
        READ06:
        while (condition(vw_iaa_Cref_Fund_Trans_1_View.readNextRow("READ06")))
        {
            if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde)  //Natural: IF TRANS-PPCN-NBR = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE AND IAA-TRANS-RCRD-VIEW.TRANS-DTE = IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE
                && iaa_Trans_Rcrd_View_Trans_Dte.equals(iaa_Cref_Fund_Trans_1_View_Trans_Dte)))
            {
                //*  FOR PPCN AND PAYEE
                pnd_Bef_Image_Fund_Found.setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #BEF-IMAGE-FUND-FOUND
                //*  FOR SUBSEQUENT ACCESS TO BEFORE-IMAGE
                pnd_Store_Trans_Date.setValue(iaa_Cref_Fund_Trans_1_View_Trans_Dte);                                                                                      //Natural: MOVE IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE TO #STORE-TRANS-DATE
                //*    WRITE 'Found Before Image Fund record on READ(1)'
                //*    WRITE 'Bef Fund Trans-Dt =' IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE
                //*      (EM=YYYYMMDDHHIISST)
                //*    WRITE 'This date will now be used to access all bef fund recs'
                //*    WRITE '------------------------------------------------------'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *WRITE '#bef-image-fund-found =' #BEF-IMAGE-FUND-FOUND
        if (condition(pnd_Bef_Image_Fund_Found.equals("Y")))                                                                                                              //Natural: IF #BEF-IMAGE-FUND-FOUND = 'Y'
        {
            pnd_Bef_Fund_Key_Pnd_Bef_Fund_Key_4.setValue(pnd_Store_Trans_Date);                                                                                           //Natural: MOVE #STORE-TRANS-DATE TO #BEF-FUND-KEY-4
            vw_iaa_Cref_Fund_Trans_1_View.startDatabaseRead                                                                                                               //Natural: READ IAA-CREF-FUND-TRANS-1-VIEW BY CREF-FUND-BFRE-KEY-2 STARTING FROM #BEF-FUND-KEY
            (
            "READ07",
            new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Bef_Fund_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
            );
            READ07:
            while (condition(vw_iaa_Cref_Fund_Trans_1_View.readNextRow("READ07")))
            {
                if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde)  //Natural: IF TRANS-PPCN-NBR = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE AND #STORE-TRANS-DATE = IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE
                    && pnd_Store_Trans_Date.equals(iaa_Cref_Fund_Trans_1_View_Trans_Dte)))
                {
                    //*      WRITE 'Within the BEF IMAGE FUND loop for TRANS-DT ='
                    //*        #BEF-FUND-KEY-4 (EM=YYYYMMDDHHIISST)
                    //*      WRITE 'Trying to match transaction fields to fund fields'
                    //*      WRITE '-------------------------------------------------'
                    //*      WRITE 'trans-ppcn-nbr =' TRANS-PPCN-NBR
                    //*        'fund-ppcn ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR
                    //*      WRITE 'trans-payee-cd =' TRANS-PAYEE-CDE
                    //*        'fund-paye ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE
                    //*      WRITE 'Fund Trans Date ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE (EM=YYYYMMDDHHIISST)
                    //*      WRITE 'FUND CODE ===>'
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Before Examine statement of fund-bef-image-rtn'
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Fund code to be examined:'
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '... and units to be moved to the #bef-units ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-UNITS-CNT (1)
                    DbsUtil.examine(new ExamineSource(pnd_Fund_Totals_Pnd_Fund_Code.getValue("*")), new ExamineSearch(iaa_Cref_Fund_Trans_1_View_Cref_Fund_Cde),          //Natural: EXAMINE #FUND-CODE ( * ) FOR IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE GIVING INDEX #X
                        new ExamineGivingIndex(pnd_X));
                    if (condition(pnd_X.equals(getZero())))                                                                                                               //Natural: IF #X = 0
                    {
                        //*  CHANGED FROM 20 TO 40  5/00
                        pnd_X.setValue(40);                                                                                                                               //Natural: MOVE 40 TO #X
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Fund_Totals_Pnd_Bef_Units.getValue(pnd_X).setValue(iaa_Cref_Fund_Trans_1_View_Cref_Units_Cnt.getValue(1));                                        //Natural: MOVE IAA-CREF-FUND-TRANS-1-VIEW.CREF-UNITS-CNT ( 1 ) TO #BEF-UNITS ( #X )
                    pnd_Fund_Totals_Pnd_Fund_Update_Sw.getValue(pnd_X).setValue("Y");                                                                                     //Natural: MOVE 'Y' TO #FUND-UPDATE-SW ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM READ-ALL-AFT-FUND-RECS
            sub_Read_All_Aft_Fund_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*    ----------------------------
            //*    CHECK FOR FUND MASTER RECORD  /* (WHEN THERE IS NO BEFORE IMAGE)
            //*    ----------------------------
            pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_1.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                             //Natural: MOVE TRANS-PPCN-NBR TO #MST-FUND-KEY-1
            pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_2.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                             //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #MST-FUND-KEY-2
            pnd_Mst_Fund_Key_Pnd_Mst_Fund_Key_3.setValue("  ");                                                                                                           //Natural: MOVE '  ' TO #MST-FUND-KEY-3
            vw_iaa_Cref_Fund_Rcrd_1_View.startDatabaseRead                                                                                                                //Natural: READ IAA-CREF-FUND-RCRD-1-VIEW BY IAA-CREF-FUND-RCRD-1-VIEW.CREF-CNTRCT-FUND-KEY STARTING FROM #MST-FUND-KEY
            (
            "READ08",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Mst_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
            );
            READ08:
            while (condition(vw_iaa_Cref_Fund_Rcrd_1_View.readNextRow("READ08")))
            {
                if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cref_Fund_Rcrd_1_View_Cref_Cntrct_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR = IAA-CREF-FUND-RCRD-1-VIEW.CREF-CNTRCT-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CREF-FUND-RCRD-1-VIEW.CREF-CNTRCT-PAYEE-CDE
                {
                    //*      WRITE 'Found Master Fund record - no before image found'
                    //*      WRITE 'FUND CODE from MASTER record ===>'
                    //*        IAA-CREF-FUND-RCRD-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Before Examine statement of fund-master-rtn'
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Fund code to be examined:'
                    //*        IAA-CREF-FUND-RCRD-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '... and units to be moved to the #bef-units ='
                    //*        IAA-CREF-FUND-RCRD-1-VIEW.CREF-UNITS-CNT (1)
                    DbsUtil.examine(new ExamineSource(pnd_Fund_Totals_Pnd_Fund_Code.getValue("*")), new ExamineSearch(iaa_Cref_Fund_Rcrd_1_View_Cref_Fund_Cde),           //Natural: EXAMINE #FUND-CODE ( * ) FOR IAA-CREF-FUND-RCRD-1-VIEW.CREF-FUND-CDE GIVING INDEX #X
                        new ExamineGivingIndex(pnd_X));
                    if (condition(pnd_X.equals(getZero())))                                                                                                               //Natural: IF #X = 0
                    {
                        //*  CHANGED FROM 20 TO 40  5/00
                        pnd_X.setValue(40);                                                                                                                               //Natural: MOVE 40 TO #X
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Fund_Totals_Pnd_Bef_Units.getValue(pnd_X).setValue(iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt.getValue(1));                                         //Natural: MOVE IAA-CREF-FUND-RCRD-1-VIEW.CREF-UNITS-CNT ( 1 ) TO #BEF-UNITS ( #X )
                    pnd_Fund_Totals_Pnd_Aft_Units.getValue(pnd_X).setValue(iaa_Cref_Fund_Rcrd_1_View_Cref_Units_Cnt.getValue(1));                                         //Natural: MOVE IAA-CREF-FUND-RCRD-1-VIEW.CREF-UNITS-CNT ( 1 ) TO #AFT-UNITS ( #X )
                    pnd_Fund_Totals_Pnd_Fund_Update_Sw.getValue(pnd_X).setValue("Y");                                                                                     //Natural: MOVE 'Y' TO #FUND-UPDATE-SW ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            //* ** PERFORM READ-ALL-AFT-FUND-RECS   /* DO NOT ACCESS AFTER IMAGE RECS
            //*                                        WHEN USING THE MASTER RECORD
            //*                                        AS THE BEFORE IMAGE.
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_All_Aft_Fund_Recs() throws Exception                                                                                                            //Natural: READ-ALL-AFT-FUND-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_1.setValue("2");                                                                                                                //Natural: MOVE '2' TO #AFT-FUND-KEY-1
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_2.setValue(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr);                                                                                 //Natural: MOVE TRANS-PPCN-NBR TO #AFT-FUND-KEY-2
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_3.setValueEdited(iaa_Trans_Rcrd_View_Trans_Payee_Cde,new ReportEditMask("99"));                                                 //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #AFT-FUND-KEY-3
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_4.setValue(iaa_Trans_Rcrd_View_Invrse_Trans_Dte);                                                                               //Natural: MOVE IAA-TRANS-RCRD-VIEW.INVRSE-TRANS-DTE TO #AFT-FUND-KEY-4
        pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_5.setValue("   ");                                                                                                              //Natural: MOVE '   ' TO #AFT-FUND-KEY-5
        pnd_Aft_Image_Fund_Found.setValue("N");                                                                                                                           //Natural: MOVE 'N' TO #AFT-IMAGE-FUND-FOUND
        vw_iaa_Cref_Fund_Trans_1_View.startDatabaseRead                                                                                                                   //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS-1-VIEW BY CREF-FUND-AFTR-KEY-2 STARTING FROM #AFT-FUND-KEY
        (
        "READ09",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Aft_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") },
        1
        );
        READ09:
        while (condition(vw_iaa_Cref_Fund_Trans_1_View.readNextRow("READ09")))
        {
            if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde)  //Natural: IF TRANS-PPCN-NBR = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE AND IAA-TRANS-RCRD-VIEW.INVRSE-TRANS-DTE = IAA-CREF-FUND-TRANS-1-VIEW.INVRSE-TRANS-DTE
                && iaa_Trans_Rcrd_View_Invrse_Trans_Dte.equals(iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte)))
            {
                //*  FOR PPCN AND PAYEE
                pnd_Aft_Image_Fund_Found.setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #AFT-IMAGE-FUND-FOUND
                //*  FOR SUBSQNT ACCESS TO AFT-IMAGE
                pnd_Store_Inverse_Trans_Date.setValue(iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte);                                                                       //Natural: MOVE IAA-CREF-FUND-TRANS-1-VIEW.INVRSE-TRANS-DTE TO #STORE-INVERSE-TRANS-DATE
                //*    WRITE 'Found After Image Fund record on READ(1)'
                //*    WRITE 'AFT Fund Trans-Dt =' IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE
                //*      (EM=YYYYMMDDHHIISST)
                //*    WRITE 'This date will now be used to access all AFT fund recs'
                //*    WRITE '------------------------------------------------------'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Aft_Image_Fund_Found.equals("Y")))                                                                                                              //Natural: IF #AFT-IMAGE-FUND-FOUND = 'Y'
        {
            pnd_Aft_Fund_Key_Pnd_Aft_Fund_Key_4.setValue(pnd_Store_Inverse_Trans_Date);                                                                                   //Natural: MOVE #STORE-INVERSE-TRANS-DATE TO #AFT-FUND-KEY-4
            vw_iaa_Cref_Fund_Trans_1_View.startDatabaseRead                                                                                                               //Natural: READ IAA-CREF-FUND-TRANS-1-VIEW BY CREF-FUND-AFTR-KEY-2 STARTING FROM #AFT-FUND-KEY
            (
            "READ10",
            new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Aft_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
            );
            READ10:
            while (condition(vw_iaa_Cref_Fund_Trans_1_View.readNextRow("READ10")))
            {
                if (condition(iaa_Trans_Rcrd_View_Trans_Ppcn_Nbr.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Ppcn_Nbr) && iaa_Trans_Rcrd_View_Trans_Payee_Cde.equals(iaa_Cref_Fund_Trans_1_View_Cref_Cntrct_Payee_Cde)  //Natural: IF TRANS-PPCN-NBR = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR AND TRANS-PAYEE-CDE = IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE AND #STORE-INVERSE-TRANS-DATE = IAA-CREF-FUND-TRANS-1-VIEW.INVRSE-TRANS-DTE
                    && pnd_Store_Inverse_Trans_Date.equals(iaa_Cref_Fund_Trans_1_View_Invrse_Trans_Dte)))
                {
                    //*      WRITE 'Within the AFT image fund loop for inv-trans-dt key ='
                    //*        #AFT-FUND-KEY-4 (EM=999999999999)
                    //*      WRITE 'Inv-trans-dt found on AFT image fund record         ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.INVRSE-TRANS-DTE
                    //*        (EM=999999999999)
                    //*      WRITE '-------------------------------------------------'
                    //*      WRITE 'Trying to match transaction fields to fund fields'
                    //*      WRITE '-------------------------------------------------'
                    //*      WRITE 'trans-ppcn-nbr =' TRANS-PPCN-NBR
                    //*        'fund-ppcn ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PPCN-NBR
                    //*      WRITE 'trans-payee-cd =' TRANS-PAYEE-CDE
                    //*        'fund-paye ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-CNTRCT-PAYEE-CDE
                    //*      WRITE 'Fund Trans Date ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.TRANS-DTE (EM=YYYYMMDDHHIISST)
                    //*      WRITE 'FUND CODE ===>'
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Before Examine statement of fund-AFT-image-rtn'
                    //*      WRITE '-----------------------------------------------'
                    //*      WRITE 'Fund code to be examined:'
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE
                    //*      WRITE '... and units to be moved to the #AFT-units ='
                    //*        IAA-CREF-FUND-TRANS-1-VIEW.CREF-UNITS-CNT (1)
                    DbsUtil.examine(new ExamineSource(pnd_Fund_Totals_Pnd_Fund_Code.getValue("*")), new ExamineSearch(iaa_Cref_Fund_Trans_1_View_Cref_Fund_Cde),          //Natural: EXAMINE #FUND-CODE ( * ) FOR IAA-CREF-FUND-TRANS-1-VIEW.CREF-FUND-CDE GIVING INDEX #X
                        new ExamineGivingIndex(pnd_X));
                    if (condition(pnd_X.equals(getZero())))                                                                                                               //Natural: IF #X = 0
                    {
                        //*  CHANGED FROM 20 TO 40  5/00
                        pnd_X.setValue(40);                                                                                                                               //Natural: MOVE 40 TO #X
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Fund_Totals_Pnd_Aft_Units.getValue(pnd_X).setValue(iaa_Cref_Fund_Trans_1_View_Cref_Units_Cnt.getValue(1));                                        //Natural: MOVE IAA-CREF-FUND-TRANS-1-VIEW.CREF-UNITS-CNT ( 1 ) TO #AFT-UNITS ( #X )
                    pnd_Fund_Totals_Pnd_Fund_Update_Sw.getValue(pnd_X).setValue("Y");                                                                                     //Natural: MOVE 'Y' TO #FUND-UPDATE-SW ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  CHANGED FROM 20 TO 40  5/00
    private void sub_Write_Report_Rec_From_Table() throws Exception                                                                                                       //Natural: WRITE-REPORT-REC-FROM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            //*   IF (#BEF-UNITS (#I) > 0  OR  #AFT-UNITS (#I) > 0)
            if (condition((pnd_Fund_Totals_Pnd_Fund_Update_Sw.getValue(pnd_I).equals("Y"))))                                                                              //Natural: IF ( #FUND-UPDATE-SW ( #I ) = 'Y' )
            {
                pnd_Report_File_Pnd_Rep_Fund_Code.setValue(pnd_Fund_Totals_Pnd_Fund_Code.getValue(pnd_I));                                                                //Natural: MOVE #FUND-CODE ( #I ) TO #REP-FUND-CODE
                pnd_Report_File_Pnd_Rep_Fund_Units_Before.setValue(pnd_Fund_Totals_Pnd_Bef_Units.getValue(pnd_I));                                                        //Natural: MOVE #BEF-UNITS ( #I ) TO #REP-FUND-UNITS-BEFORE
                pnd_Report_File_Pnd_Rep_Fund_Units_After.setValue(pnd_Fund_Totals_Pnd_Aft_Units.getValue(pnd_I));                                                         //Natural: MOVE #AFT-UNITS ( #I ) TO #REP-FUND-UNITS-AFTER
                pnd_Report_File_Pnd_Rep_Fund_Units_Diff.compute(new ComputeParameters(false, pnd_Report_File_Pnd_Rep_Fund_Units_Diff), pnd_Fund_Totals_Pnd_Bef_Units.getValue(pnd_I).subtract(pnd_Fund_Totals_Pnd_Aft_Units.getValue(pnd_I))); //Natural: COMPUTE #REP-FUND-UNITS-DIFF = #BEF-UNITS ( #I ) - #AFT-UNITS ( #I )
                getWorkFiles().write(2, false, pnd_Report_File);                                                                                                          //Natural: WRITE WORK FILE 02 #REPORT-FILE
                pnd_Report_Recs_Written.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REPORT-RECS-WRITTEN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cor() throws Exception                                                                                                                          //Natural: READ-COR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function().setValue("PR");                                                                                          //Natural: ASSIGN #IN-FUNCTION := 'PR'
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                      //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type().setValue("PIN");                                                                                             //Natural: ASSIGN #IN-TYPE := 'PIN'
        //* *VE EDITED #I-PIN(EM=999999999999) TO #IN-PIN-SSN   /* PIN EXPANSION
        //*  PIN EXPANSION
        ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn().setValue(pnd_I_Pin);                                                                                      //Natural: MOVE #I-PIN TO #IN-PIN-SSN
        psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ(), //Natural: CALL 'PSGM001' #PSGM001-COMMON-AREA
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Input_String(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Pref(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_1(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_2(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_3(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_4(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_5(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_City(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Country(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_State_Nm(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd(),
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status());
        //* *WRITE '=' #RT-RET-CODE /
        //*   '=' #RT-LAST-NAME /
        //*   '=' #RT-FIRST-NAME
        if (condition(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code().notEquals("00")))                                                                          //Natural: IF #RT-RET-CODE NE '00'
        {
            getReports().write(0, ReportOption.NOTITLE,"COR XREF FILE REC NOT FOUND",pnd_I_Pin);                                                                          //Natural: WRITE 'COR XREF FILE REC NOT FOUND' #I-PIN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_File_Pnd_Rep_Last_Name.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name());                                                        //Natural: ASSIGN #REP-LAST-NAME := #RT-LAST-NAME
            pnd_Report_File_Pnd_Rep_First_Name.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name());                                                      //Natural: ASSIGN #REP-FIRST-NAME := #RT-FIRST-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*                          /* 10/2014 - END
    }
    private void sub_Eoj_Rtn() throws Exception                                                                                                                           //Natural: EOJ-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM:  ",Global.getPROGRAM(),NEWLINE,NEWLINE,"RUN DATE: ",Global.getDATU(),new          //Natural: WRITE NOTITLE NOHDR / 'PROGRAM:  ' *PROGRAM // 'RUN DATE: ' *DATU 38X 'IA ADMINIST.SYSTEM' 41X 'PAGE: ' *PAGE-NUMBER / 'RUN TIME: ' *TIMX 33X 'Report File Generator Program' / 60X 'Control Totals' / 53X 'For Check Date ' #FORMAT-DATE ( EM = 99/99/9999 ) //
            ColumnSpacing(38),"IA ADMINIST.SYSTEM",new ColumnSpacing(41),"PAGE: ",getReports().getPageNumberDbs(0),NEWLINE,"RUN TIME: ",Global.getTIMX(),new 
            ColumnSpacing(33),"Report File Generator Program",NEWLINE,new ColumnSpacing(60),"Control Totals",NEWLINE,new ColumnSpacing(53),"For Check Date ",pnd_Format_Date, 
            new ReportEditMask ("99/99/9999"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total Report File Records Written:",pnd_Report_Recs_Written,NEWLINE);                         //Natural: WRITE /// 'Total Report File Records Written:' #REPORT-RECS-WRITTEN /
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");

        getReports().setDisplayColumns(0, "TIAA FUND RECORD NOT FOUND",
        		pnd_Tiaa_Fund_Key);
    }
}
