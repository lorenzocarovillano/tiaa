/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:21 PM
**        * FROM NATURAL PROGRAM : Iatp407
************************************************************
**        * FILE NAME            : Iatp407.java
**        * CLASS NAME           : Iatp407
**        * INSTANCE NAME        : Iatp407
************************************************************
************************************************************************
* PROGRAM: IATP407
* DATE   : 09/18/98
* AUTHOR : LEN B/ARI G
* MODE   : BATCH
* DESC   : THIS PROGRAM SENDS A REPORT TO THE CONSOLIDATED ISSUE
*        : SYSTEM (CIS) FOR NEW CONTRACTS CREATED BY A FUND TRANSFER.
*        : THIS PROGRAM READ THE IAA-XFR-AUDIT FILE TO FIND ALL NEW
*        : ISSUES. THIS PROGRAM ONLY PROCESSES TRANSFER FROM VARIABLE
*        : FUNDS TO TIAA AND OTHER VARIABLE FUNDS. TIAA TO CREF
*        : TRANSFERS ARE PROCESSED IN IATP408.
* HISTORY: KN 7/99 ADDED CHECK TO BYPASS TIAA TO CREF XFRS
* 01/20/09 OS TIAA ACCESS CHANGES. SC 012009.
*             TRANSFERS FROM REA/ACCESS WILL NOT UPDATE THE
*             CIS STATUS CODE, I.E. IT WILL REMAIN A (ASSIGNED).
*             CREF PACKAGES CANNOT BE GENERATED AS OF THIS TIME.
* 05/22/09 OS PROD FIX. FUND CODE FOR STANDARD SHOULD BE '1S'
*             INSTEAD OF '1T'. SC 052209.
* 05/27/09 OS CALL AIAN063 TO FILL THE CIS-COMUT-* FIELDS. SC 052709.
* 03/30/10 OS PCPOP CHANGES. SC 033010.
* 11/23/10 OS PROD FIX. ADDED NAZL510 WHEN CALLING AIAN063. SC 112310.
* 05/09/12 JT RBE PROJECT. CHANGED NAZL510 TO AIAA510 AND NAZA6007 TO
*          AIAA6007 FOR RATE EXPANSION. SC 050912.
* 02/27/14 OS CREF REA CHANGES MARKED REA0614.
* 10/07/14 JT COR/NAAD DECOMMISSION CHANGES MARKED 10/2014
* 04/2017  OS PIN EXPANSION - SC 082017 FOR CHANGES.
* 07/2018  JT RESET #I-TIAA-CREF-IND BEFORE CALLING MDM - SC 07/2018.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp407 extends BLNatBase
{
    // Data Areas
    private PdaIaaa299a pdaIaaa299a;
    private PdaIaaa202h pdaIaaa202h;
    private PdaIaapda_M pdaIaapda_M;
    private LdaIatl406 ldaIatl406;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaAial0131 ldaAial0131;
    private LdaAial0132 ldaAial0132;
    private LdaNazl6004 ldaNazl6004;
    private PdaAiaa6007 pdaAiaa6007;
    private PdaAiaa510 pdaAiaa510;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_Rqst_Id;

    private DbsGroup iaa_Xfr_Audit__R_Field_1;
    private DbsField iaa_Xfr_Audit_Pnd_W_Pin;
    private DbsField iaa_Xfr_Audit_Pnd_W_Inverse;
    private DbsField iaa_Xfr_Audit_Pnd_W_Log_Date;

    private DbsGroup iaa_Xfr_Audit__R_Field_2;
    private DbsField iaa_Xfr_Audit_Pnd_W_Log_Date_Ccyy;
    private DbsField iaa_Xfr_Audit_Pnd_W_Log_Date_Mm;
    private DbsField iaa_Xfr_Audit_Pnd_W_Log_Date_Dd;
    private DbsField iaa_Xfr_Audit_Pnd_W_Log_Time;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Wpid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;

    private DbsGroup iaa_Xfr_Audit__R_Field_3;
    private DbsField iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A1;

    private DbsGroup iaa_Xfr_Audit__R_Field_4;
    private DbsField iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_In_Progress_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Retry_Cnt;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Rate_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Acctng_Dte;
    private DbsField iaa_Xfr_Audit_Lst_Chnge_Dte;
    private DbsField iaa_Xfr_Audit_Rqst_Xfr_Type;
    private DbsField iaa_Xfr_Audit_Rqst_Unit_Cde;
    private DbsField iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull;

    private DataAccessProgramView vw_cis_Updt;
    private DbsField cis_Updt_Cis_Rqst_Id_Key;

    private DbsGroup cis_Updt_Cis_Sg_Fund_Identifier;
    private DbsField cis_Updt_Cis_Sg_Fund_Ticker;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_New_Issue_Tiaa;
    private DbsField pnd_L_Pnd_Vacation_Address;
    private DbsField pnd_L_Pnd_Error;
    private DbsField pnd_L_Pnd_Issue_Error;
    private DbsField pnd_L_Pnd_Rec_Found;
    private DbsField pnd_L_Pnd_First_Call;

    private DbsGroup pnd_Count;
    private DbsField pnd_Count_Pnd_Err_Cnt;
    private DbsField pnd_Count_Pnd_Comp_Cnt;
    private DbsField pnd_Count_Pnd_Rec_Cnt;
    private DbsField pnd_Count_Pnd_Counter;
    private DbsField pnd_Count_Pnd_Sub1;
    private DbsField pnd_Count_Pnd_Sub2;
    private DbsField pnd_Count_Pnd_Acct_Ctr;
    private DbsField pnd_Count_Pnd_Total_Amt;

    private DbsGroup pnd_W_Xfr_Units;
    private DbsField pnd_W_Xfr_Units_Pnd_W_To_Fund;
    private DbsField pnd_W_Xfr_Units_Pnd_W_To_Units;
    private DbsField pnd_Ratio;
    private DbsField pnd_W_Cref_Units;
    private DbsField pnd_Total_Units;
    private DbsField pnd_Diff;
    private DbsField pnd_Var_Type;
    private DbsField pnd_Reason1;
    private DbsField pnd_Reason2;
    private DbsField pnd_Found;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_C;
    private DbsField pnd_T;
    private DbsField pnd_Date_D;
    private DbsField pnd_Audit_Cntrct;
    private DbsField pnd_Audit_Payee;
    private DbsField pnd_Annty_Strt_Dte;

    private DbsGroup pnd_Annty_Strt_Dte__R_Field_6;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm;
    private DbsField pnd_Annty_Strt_Dte_Fillerx;
    private DbsField pnd_Date_A8;

    private DbsGroup pnd_Date_A8__R_Field_7;
    private DbsField pnd_Date_A8_Pnd_Date_N8;

    private DbsGroup pnd_Date_A8__R_Field_8;
    private DbsField pnd_Date_A8_Pnd_Date_Yyyymm;
    private DbsField pnd_Date_A8_Pnd_Date_Dd;

    private DbsGroup pnd_Date_A8__R_Field_9;
    private DbsField pnd_Date_A8_Pnd_Date_Yyyy;
    private DbsField pnd_Date_A8_Pnd_Date_Mm;

    private DbsGroup pnd_Date_A8__R_Field_10;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Ccyy;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Mm;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Dd;
    private DbsField pnd_Orig_Issue;
    private DbsField pnd_Orig_Issue_Re;
    private DbsField pnd_Issue_Date_D;
    private DbsField pnd_Issue_Date_A8;

    private DbsGroup pnd_Issue_Date_A8__R_Field_11;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_N8;

    private DbsGroup pnd_Issue_Date_A8__R_Field_12;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A8__R_Field_13;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Mm;
    private DbsField pnd_Cor_Unique_Id;
    private DbsField pnd_Numeric_Ste;
    private DbsField pnd_Alpha_Ste;
    private DbsField pnd_Ii;
    private DbsField pnd_Todays_Date;
    private DbsField pnd_Address_Array;
    private DbsField pnd_Err_Reason;
    private DbsField pnd_Comp_Reason;
    private DbsField pnd_T_Company;
    private DbsField pnd_F_Company;
    private DbsField pnd_From_Cref;
    private DbsField pnd_From_Real;
    private DbsField pnd_Process_Date_A;

    private DbsGroup pnd_Process_Date_A__R_Field_14;
    private DbsField pnd_Process_Date_A_Pnd_Process_Date_N;
    private DbsField pnd_From_Cntrct_Payee;
    private DbsField pnd_Fr_Cntrct_Payee;

    private DbsGroup pnd_Fr_Cntrct_Payee__R_Field_15;
    private DbsField pnd_Fr_Cntrct_Payee_Pnd_Fr_Cntrct;
    private DbsField pnd_Fr_Cntrct_Payee_Pnd_Fr_Payee;
    private DbsField pnd_Nbr_Acct;
    private DbsField pnd_Acct_Std_Alpha_Cde;
    private DbsField pnd_Acct_Nme_5;
    private DbsField pnd_Acct_Tckr;
    private DbsField pnd_Acct_Effctve_Dte;
    private DbsField pnd_Acct_Unit_Rte_Ind;
    private DbsField pnd_Acct_Std_Nm_Cde;
    private DbsField pnd_Acct_Rpt_Seq;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw;
    private DbsField iaa_Trnsfr_Sw_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Ia_To_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Type;
    private DbsField pnd_Cis_Found;
    private DbsField pnd_Isn;
    private DbsField pnd_A;
    private DbsField pnd_Total_Sw;
    private DbsField cntrct_Issue_Dte_D;

    private DbsGroup cntrct_Issue_Dte_D__R_Field_16;
    private DbsField cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date;
    private DbsField cntrct_Issue_Dte_Dd_1;
    private DbsField cntrct_Dte_D;

    private DbsGroup cntrct_Dte_D__R_Field_17;
    private DbsField cntrct_Dte_D_Cntrct_Dte_Date;
    private DbsField pnd_Curr_Proc_Dte;

    private DbsGroup pnd_Curr_Proc_Dte__R_Field_18;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Yyyy;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Mm;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Dd;
    private DbsField pnd_Dod_1_Sw;
    private DbsField pnd_Dod_2_Sw;
    private DbsField pnd_Count_Pe;
    private DbsField pnd_Cis_Unit_A;
    private DbsField pnd_Cis_Unit_M;
    private DbsField pnd_Iaxfr_Units_A;
    private DbsField pnd_Iaxfr_Units_M;
    private DbsField pnd_Tiaa_Type;

    private DbsGroup print_Line;
    private DbsField print_Line_Ppcn;
    private DbsField print_Line_Filler;
    private DbsField print_Line_Payee_Cd;
    private DbsField print_Line_Fillr;
    private DbsField print_Line_Mode_Code;
    private DbsField print_Line_Filler1;
    private DbsField print_Line_Fund;
    private DbsField print_Line_Filler2;
    private DbsField print_Line_Ia_Units;
    private DbsField print_Line_Filler3;
    private DbsField print_Line_Cis_Units;
    private DbsField print_Line_Filler4;
    private DbsField print_Line_Cis_Option;
    private DbsField print_Line_Filler5;
    private DbsField print_Line_Cis_Gr_Years;
    private DbsField print_Line_Filler6;
    private DbsField print_Line_Cis_Gr_Days;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(localVariables);
        pdaIaaa202h = new PdaIaaa202h(localVariables);
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        ldaIatl406 = new LdaIatl406();
        registerRecord(ldaIatl406);
        registerRecord(ldaIatl406.getVw_cis_Prtcpnt_File());
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        ldaAial0132 = new LdaAial0132();
        registerRecord(ldaAial0132);
        ldaNazl6004 = new LdaNazl6004();
        registerRecord(ldaNazl6004);
        pdaAiaa6007 = new PdaAiaa6007(localVariables);
        pdaAiaa510 = new PdaAiaa510(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_Rcrd_Type_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");

        iaa_Xfr_Audit__R_Field_1 = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit__R_Field_1", "REDEFINE", iaa_Xfr_Audit_Rqst_Id);
        iaa_Xfr_Audit_Pnd_W_Pin = iaa_Xfr_Audit__R_Field_1.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        iaa_Xfr_Audit_Pnd_W_Inverse = iaa_Xfr_Audit__R_Field_1.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Inverse", "#W-INVERSE", FieldType.NUMERIC, 8);
        iaa_Xfr_Audit_Pnd_W_Log_Date = iaa_Xfr_Audit__R_Field_1.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Log_Date", "#W-LOG-DATE", FieldType.NUMERIC, 8);

        iaa_Xfr_Audit__R_Field_2 = iaa_Xfr_Audit__R_Field_1.newGroupInGroup("iaa_Xfr_Audit__R_Field_2", "REDEFINE", iaa_Xfr_Audit_Pnd_W_Log_Date);
        iaa_Xfr_Audit_Pnd_W_Log_Date_Ccyy = iaa_Xfr_Audit__R_Field_2.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Log_Date_Ccyy", "#W-LOG-DATE-CCYY", FieldType.STRING, 
            4);
        iaa_Xfr_Audit_Pnd_W_Log_Date_Mm = iaa_Xfr_Audit__R_Field_2.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Log_Date_Mm", "#W-LOG-DATE-MM", FieldType.STRING, 
            2);
        iaa_Xfr_Audit_Pnd_W_Log_Date_Dd = iaa_Xfr_Audit__R_Field_2.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Log_Date_Dd", "#W-LOG-DATE-DD", FieldType.STRING, 
            2);
        iaa_Xfr_Audit_Pnd_W_Log_Time = iaa_Xfr_Audit__R_Field_1.newFieldInGroup("iaa_Xfr_Audit_Pnd_W_Log_Time", "#W-LOG-TIME", FieldType.NUMERIC, 6);
        iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte", "IAXFR-INVRSE-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_INVRSE_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14, RepeatingFieldStrategy.None, "IAXFR_INVRSE_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_Iaxfr_Entry_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_ENTRY_TME");
        iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr", "IAXFR-CALC-STTMNT-INDCTR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_STTMNT_INDCTR");
        iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        iaa_Xfr_Audit_Iaxfr_Cwf_Wpid = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IAXFR_CWF_WPID");
        iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "IAXFR_CWF_LOG_DTE_TIME");
        iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");

        iaa_Xfr_Audit__R_Field_3 = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit__R_Field_3", "REDEFINE", iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);
        iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A1 = iaa_Xfr_Audit__R_Field_3.newFieldInGroup("iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A1", "#IAXFR-FROM-PPCN-NBR-A1", 
            FieldType.STRING, 1);

        iaa_Xfr_Audit__R_Field_4 = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit__R_Field_4", "REDEFINE", iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);
        iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3 = iaa_Xfr_Audit__R_Field_4.newFieldInGroup("iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3", "#IAXFR-FROM-PPCN-NBR-A3", 
            FieldType.STRING, 3);
        iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_REJECT_CDE");
        iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_OPN_CLSD_IND");
        iaa_Xfr_Audit_Iaxfr_In_Progress_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "IAXFR_IN_PROGRESS_IND");
        iaa_Xfr_Audit_Iaxfr_Retry_Cnt = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "IAXFR_RETRY_CNT");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data", 
            "C*IAXFR-CALC-FROM-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", "IAXFR-FROM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", "IAXFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Qty = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_ASSET_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data", 
            "C*IAXFR-CALC-TO-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", "IAXFR-TO-NEW-FUND-REC", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Qty = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", "IAXFR-TO-BFR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", "IAXFR-TO-BFR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", "IAXFR-TO-BFR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", "IAXFR-TO-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", "IAXFR-TO-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Rate_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Rate_Cde", "IAXFR-TO-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_RATE_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", "IAXFR-TO-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", "IAXFR-TO-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", "IAXFR-TO-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", "IAXFR-TO-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_Iaxfr_Acctng_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ACCTNG_DTE");
        iaa_Xfr_Audit_Lst_Chnge_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        iaa_Xfr_Audit_Rqst_Xfr_Type = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Xfr_Audit_Rqst_Unit_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Xfr_Audit);

        vw_cis_Updt = new DataAccessProgramView(new NameInfo("vw_cis_Updt", "CIS-UPDT"), "CIS_PRTCPNT_FILE", "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE"));
        cis_Updt_Cis_Rqst_Id_Key = vw_cis_Updt.getRecord().newFieldInGroup("cis_Updt_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "CIS_RQST_ID_KEY");

        cis_Updt_Cis_Sg_Fund_Identifier = vw_cis_Updt.getRecord().newGroupInGroup("cis_Updt_Cis_Sg_Fund_Identifier", "CIS-SG-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Updt_Cis_Sg_Fund_Ticker = cis_Updt_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Updt_Cis_Sg_Fund_Ticker", "CIS-SG-FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_TICKER", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        registerRecord(vw_cis_Updt);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_New_Issue_Tiaa = pnd_L.newFieldInGroup("pnd_L_Pnd_New_Issue_Tiaa", "#NEW-ISSUE-TIAA", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Vacation_Address = pnd_L.newFieldInGroup("pnd_L_Pnd_Vacation_Address", "#VACATION-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Error = pnd_L.newFieldInGroup("pnd_L_Pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Issue_Error = pnd_L.newFieldInGroup("pnd_L_Pnd_Issue_Error", "#ISSUE-ERROR", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Rec_Found = pnd_L.newFieldInGroup("pnd_L_Pnd_Rec_Found", "#REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_First_Call = pnd_L.newFieldInGroup("pnd_L_Pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);

        pnd_Count = localVariables.newGroupInRecord("pnd_Count", "#COUNT");
        pnd_Count_Pnd_Err_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Comp_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Comp_Cnt", "#COMP-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Rec_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Counter = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 3);
        pnd_Count_Pnd_Sub1 = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Sub1", "#SUB1", FieldType.PACKED_DECIMAL, 5);
        pnd_Count_Pnd_Sub2 = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Sub2", "#SUB2", FieldType.PACKED_DECIMAL, 5);
        pnd_Count_Pnd_Acct_Ctr = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Acct_Ctr", "#ACCT-CTR", FieldType.PACKED_DECIMAL, 5);
        pnd_Count_Pnd_Total_Amt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Total_Amt", "#TOTAL-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_W_Xfr_Units = localVariables.newGroupArrayInRecord("pnd_W_Xfr_Units", "#W-XFR-UNITS", new DbsArrayController(1, 2));
        pnd_W_Xfr_Units_Pnd_W_To_Fund = pnd_W_Xfr_Units.newFieldInGroup("pnd_W_Xfr_Units_Pnd_W_To_Fund", "#W-TO-FUND", FieldType.STRING, 1);
        pnd_W_Xfr_Units_Pnd_W_To_Units = pnd_W_Xfr_Units.newFieldInGroup("pnd_W_Xfr_Units_Pnd_W_To_Units", "#W-TO-UNITS", FieldType.PACKED_DECIMAL, 8, 
            3);
        pnd_Ratio = localVariables.newFieldInRecord("pnd_Ratio", "#RATIO", FieldType.PACKED_DECIMAL, 8, 5);
        pnd_W_Cref_Units = localVariables.newFieldInRecord("pnd_W_Cref_Units", "#W-CREF-UNITS", FieldType.PACKED_DECIMAL, 8, 3);
        pnd_Total_Units = localVariables.newFieldInRecord("pnd_Total_Units", "#TOTAL-UNITS", FieldType.PACKED_DECIMAL, 8, 3);
        pnd_Diff = localVariables.newFieldInRecord("pnd_Diff", "#DIFF", FieldType.PACKED_DECIMAL, 8, 3);
        pnd_Var_Type = localVariables.newFieldInRecord("pnd_Var_Type", "#VAR-TYPE", FieldType.BOOLEAN, 1);
        pnd_Reason1 = localVariables.newFieldInRecord("pnd_Reason1", "#REASON1", FieldType.STRING, 18);
        pnd_Reason2 = localVariables.newFieldInRecord("pnd_Reason2", "#REASON2", FieldType.STRING, 18);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 2);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Audit_Cntrct = localVariables.newFieldInRecord("pnd_Audit_Cntrct", "#AUDIT-CNTRCT", FieldType.STRING, 10);
        pnd_Audit_Payee = localVariables.newFieldInRecord("pnd_Audit_Payee", "#AUDIT-PAYEE", FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte = localVariables.newFieldInRecord("pnd_Annty_Strt_Dte", "#ANNTY-STRT-DTE", FieldType.STRING, 8);

        pnd_Annty_Strt_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Annty_Strt_Dte__R_Field_6", "REDEFINE", pnd_Annty_Strt_Dte);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc = pnd_Annty_Strt_Dte__R_Field_6.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc", "#ANNTY-STRT-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy = pnd_Annty_Strt_Dte__R_Field_6.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy", "#ANNTY-STRT-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm = pnd_Annty_Strt_Dte__R_Field_6.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm", "#ANNTY-STRT-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Fillerx = pnd_Annty_Strt_Dte__R_Field_6.newFieldInGroup("pnd_Annty_Strt_Dte_Fillerx", "FILLERX", FieldType.NUMERIC, 2);
        pnd_Date_A8 = localVariables.newFieldInRecord("pnd_Date_A8", "#DATE-A8", FieldType.STRING, 8);

        pnd_Date_A8__R_Field_7 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_7", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_N8 = pnd_Date_A8__R_Field_7.newFieldInGroup("pnd_Date_A8_Pnd_Date_N8", "#DATE-N8", FieldType.NUMERIC, 8);

        pnd_Date_A8__R_Field_8 = pnd_Date_A8__R_Field_7.newGroupInGroup("pnd_Date_A8__R_Field_8", "REDEFINE", pnd_Date_A8_Pnd_Date_N8);
        pnd_Date_A8_Pnd_Date_Yyyymm = pnd_Date_A8__R_Field_8.newFieldInGroup("pnd_Date_A8_Pnd_Date_Yyyymm", "#DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Date_A8_Pnd_Date_Dd = pnd_Date_A8__R_Field_8.newFieldInGroup("pnd_Date_A8_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        pnd_Date_A8__R_Field_9 = pnd_Date_A8__R_Field_7.newGroupInGroup("pnd_Date_A8__R_Field_9", "REDEFINE", pnd_Date_A8_Pnd_Date_N8);
        pnd_Date_A8_Pnd_Date_Yyyy = pnd_Date_A8__R_Field_9.newFieldInGroup("pnd_Date_A8_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Date_A8_Pnd_Date_Mm = pnd_Date_A8__R_Field_9.newFieldInGroup("pnd_Date_A8_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);

        pnd_Date_A8__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_10", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_A8_Ccyy = pnd_Date_A8__R_Field_10.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Ccyy", "#DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Date_A8_Pnd_Date_A8_Mm = pnd_Date_A8__R_Field_10.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Mm", "#DATE-A8-MM", FieldType.STRING, 2);
        pnd_Date_A8_Pnd_Date_A8_Dd = pnd_Date_A8__R_Field_10.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Dd", "#DATE-A8-DD", FieldType.NUMERIC, 2);
        pnd_Orig_Issue = localVariables.newFieldInRecord("pnd_Orig_Issue", "#ORIG-ISSUE", FieldType.STRING, 3);
        pnd_Orig_Issue_Re = localVariables.newFieldInRecord("pnd_Orig_Issue_Re", "#ORIG-ISSUE-RE", FieldType.STRING, 3);
        pnd_Issue_Date_D = localVariables.newFieldInRecord("pnd_Issue_Date_D", "#ISSUE-DATE-D", FieldType.DATE);
        pnd_Issue_Date_A8 = localVariables.newFieldInRecord("pnd_Issue_Date_A8", "#ISSUE-DATE-A8", FieldType.STRING, 8);

        pnd_Issue_Date_A8__R_Field_11 = localVariables.newGroupInRecord("pnd_Issue_Date_A8__R_Field_11", "REDEFINE", pnd_Issue_Date_A8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_N8 = pnd_Issue_Date_A8__R_Field_11.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_N8", "#ISSUE-DATE-N8", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A8__R_Field_12 = pnd_Issue_Date_A8__R_Field_11.newGroupInGroup("pnd_Issue_Date_A8__R_Field_12", "REDEFINE", pnd_Issue_Date_A8_Pnd_Issue_Date_N8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A8__R_Field_12.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Dd = pnd_Issue_Date_A8__R_Field_12.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A8__R_Field_13 = pnd_Issue_Date_A8__R_Field_11.newGroupInGroup("pnd_Issue_Date_A8__R_Field_13", "REDEFINE", pnd_Issue_Date_A8_Pnd_Issue_Date_N8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A8__R_Field_13.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Mm = pnd_Issue_Date_A8__R_Field_13.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Cor_Unique_Id = localVariables.newFieldInRecord("pnd_Cor_Unique_Id", "#COR-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Numeric_Ste = localVariables.newFieldInRecord("pnd_Numeric_Ste", "#NUMERIC-STE", FieldType.STRING, 2);
        pnd_Alpha_Ste = localVariables.newFieldInRecord("pnd_Alpha_Ste", "#ALPHA-STE", FieldType.STRING, 2);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.NUMERIC, 2);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.DATE);
        pnd_Address_Array = localVariables.newFieldInRecord("pnd_Address_Array", "#ADDRESS-ARRAY", FieldType.NUMERIC, 1);
        pnd_Err_Reason = localVariables.newFieldInRecord("pnd_Err_Reason", "#ERR-REASON", FieldType.STRING, 60);
        pnd_Comp_Reason = localVariables.newFieldInRecord("pnd_Comp_Reason", "#COMP-REASON", FieldType.STRING, 60);
        pnd_T_Company = localVariables.newFieldInRecord("pnd_T_Company", "#T-COMPANY", FieldType.STRING, 4);
        pnd_F_Company = localVariables.newFieldInRecord("pnd_F_Company", "#F-COMPANY", FieldType.STRING, 4);
        pnd_From_Cref = localVariables.newFieldInRecord("pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_From_Real = localVariables.newFieldInRecord("pnd_From_Real", "#FROM-REAL", FieldType.STRING, 1);
        pnd_Process_Date_A = localVariables.newFieldInRecord("pnd_Process_Date_A", "#PROCESS-DATE-A", FieldType.STRING, 8);

        pnd_Process_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Process_Date_A__R_Field_14", "REDEFINE", pnd_Process_Date_A);
        pnd_Process_Date_A_Pnd_Process_Date_N = pnd_Process_Date_A__R_Field_14.newFieldInGroup("pnd_Process_Date_A_Pnd_Process_Date_N", "#PROCESS-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_From_Cntrct_Payee = localVariables.newFieldInRecord("pnd_From_Cntrct_Payee", "#FROM-CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Fr_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Fr_Cntrct_Payee", "#FR-CNTRCT-PAYEE", FieldType.STRING, 10);

        pnd_Fr_Cntrct_Payee__R_Field_15 = localVariables.newGroupInRecord("pnd_Fr_Cntrct_Payee__R_Field_15", "REDEFINE", pnd_Fr_Cntrct_Payee);
        pnd_Fr_Cntrct_Payee_Pnd_Fr_Cntrct = pnd_Fr_Cntrct_Payee__R_Field_15.newFieldInGroup("pnd_Fr_Cntrct_Payee_Pnd_Fr_Cntrct", "#FR-CNTRCT", FieldType.STRING, 
            8);
        pnd_Fr_Cntrct_Payee_Pnd_Fr_Payee = pnd_Fr_Cntrct_Payee__R_Field_15.newFieldInGroup("pnd_Fr_Cntrct_Payee_Pnd_Fr_Payee", "#FR-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Nbr_Acct = localVariables.newFieldInRecord("pnd_Nbr_Acct", "#NBR-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Acct_Std_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Alpha_Cde", "#ACCT-STD-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Acct_Nme_5 = localVariables.newFieldArrayInRecord("pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 6, new DbsArrayController(1, 20));
        pnd_Acct_Tckr = localVariables.newFieldArrayInRecord("pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Acct_Effctve_Dte = localVariables.newFieldArrayInRecord("pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            20));
        pnd_Acct_Unit_Rte_Ind = localVariables.newFieldArrayInRecord("pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 
            20));
        pnd_Acct_Std_Nm_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Nm_Cde", "#ACCT-STD-NM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pnd_Acct_Rpt_Seq = localVariables.newFieldArrayInRecord("pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 20));

        vw_iaa_Trnsfr_Sw = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw", "IAA-TRNSFR-SW"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        iaa_Trnsfr_Sw_Rqst_Id = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Ia_Unique_Id = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Ia_To_Payee = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_TO_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        registerRecord(vw_iaa_Trnsfr_Sw);

        pnd_Cis_Found = localVariables.newFieldInRecord("pnd_Cis_Found", "#CIS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Sw = localVariables.newFieldInRecord("pnd_Total_Sw", "#TOTAL-SW", FieldType.STRING, 1);
        cntrct_Issue_Dte_D = localVariables.newFieldInRecord("cntrct_Issue_Dte_D", "CNTRCT-ISSUE-DTE-D", FieldType.STRING, 8);

        cntrct_Issue_Dte_D__R_Field_16 = localVariables.newGroupInRecord("cntrct_Issue_Dte_D__R_Field_16", "REDEFINE", cntrct_Issue_Dte_D);
        cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date = cntrct_Issue_Dte_D__R_Field_16.newFieldInGroup("cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date", "CNTRCT-ISSUE-DTE-DATE", 
            FieldType.NUMERIC, 8);
        cntrct_Issue_Dte_Dd_1 = localVariables.newFieldInRecord("cntrct_Issue_Dte_Dd_1", "CNTRCT-ISSUE-DTE-DD-1", FieldType.STRING, 2);
        cntrct_Dte_D = localVariables.newFieldInRecord("cntrct_Dte_D", "CNTRCT-DTE-D", FieldType.STRING, 8);

        cntrct_Dte_D__R_Field_17 = localVariables.newGroupInRecord("cntrct_Dte_D__R_Field_17", "REDEFINE", cntrct_Dte_D);
        cntrct_Dte_D_Cntrct_Dte_Date = cntrct_Dte_D__R_Field_17.newFieldInGroup("cntrct_Dte_D_Cntrct_Dte_Date", "CNTRCT-DTE-DATE", FieldType.NUMERIC, 
            6);
        pnd_Curr_Proc_Dte = localVariables.newFieldInRecord("pnd_Curr_Proc_Dte", "#CURR-PROC-DTE", FieldType.STRING, 8);

        pnd_Curr_Proc_Dte__R_Field_18 = localVariables.newGroupInRecord("pnd_Curr_Proc_Dte__R_Field_18", "REDEFINE", pnd_Curr_Proc_Dte);
        pnd_Curr_Proc_Dte_Pnd_Yyyy = pnd_Curr_Proc_Dte__R_Field_18.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Curr_Proc_Dte_Pnd_Mm = pnd_Curr_Proc_Dte__R_Field_18.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Curr_Proc_Dte_Pnd_Dd = pnd_Curr_Proc_Dte__R_Field_18.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Dod_1_Sw = localVariables.newFieldInRecord("pnd_Dod_1_Sw", "#DOD-1-SW", FieldType.STRING, 1);
        pnd_Dod_2_Sw = localVariables.newFieldInRecord("pnd_Dod_2_Sw", "#DOD-2-SW", FieldType.STRING, 1);
        pnd_Count_Pe = localVariables.newFieldInRecord("pnd_Count_Pe", "#COUNT-PE", FieldType.NUMERIC, 3);
        pnd_Cis_Unit_A = localVariables.newFieldArrayInRecord("pnd_Cis_Unit_A", "#CIS-UNIT-A", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20));
        pnd_Cis_Unit_M = localVariables.newFieldArrayInRecord("pnd_Cis_Unit_M", "#CIS-UNIT-M", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20));
        pnd_Iaxfr_Units_A = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Units_A", "#IAXFR-UNITS-A", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_Units_M = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Units_M", "#IAXFR-UNITS-M", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            20));
        pnd_Tiaa_Type = localVariables.newFieldArrayInRecord("pnd_Tiaa_Type", "#TIAA-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 4));

        print_Line = localVariables.newGroupInRecord("print_Line", "PRINT-LINE");
        print_Line_Ppcn = print_Line.newFieldInGroup("print_Line_Ppcn", "PPCN", FieldType.STRING, 8);
        print_Line_Filler = print_Line.newFieldInGroup("print_Line_Filler", "FILLER", FieldType.STRING, 5);
        print_Line_Payee_Cd = print_Line.newFieldInGroup("print_Line_Payee_Cd", "PAYEE-CD", FieldType.STRING, 2);
        print_Line_Fillr = print_Line.newFieldInGroup("print_Line_Fillr", "FILLR", FieldType.STRING, 4);
        print_Line_Mode_Code = print_Line.newFieldInGroup("print_Line_Mode_Code", "MODE-CODE", FieldType.STRING, 4);
        print_Line_Filler1 = print_Line.newFieldInGroup("print_Line_Filler1", "FILLER1", FieldType.STRING, 5);
        print_Line_Fund = print_Line.newFieldInGroup("print_Line_Fund", "FUND", FieldType.STRING, 1);
        print_Line_Filler2 = print_Line.newFieldInGroup("print_Line_Filler2", "FILLER2", FieldType.STRING, 1);
        print_Line_Ia_Units = print_Line.newFieldInGroup("print_Line_Ia_Units", "IA-UNITS", FieldType.NUMERIC, 11, 3);
        print_Line_Filler3 = print_Line.newFieldInGroup("print_Line_Filler3", "FILLER3", FieldType.STRING, 3);
        print_Line_Cis_Units = print_Line.newFieldInGroup("print_Line_Cis_Units", "CIS-UNITS", FieldType.NUMERIC, 11, 3);
        print_Line_Filler4 = print_Line.newFieldInGroup("print_Line_Filler4", "FILLER4", FieldType.STRING, 5);
        print_Line_Cis_Option = print_Line.newFieldInGroup("print_Line_Cis_Option", "CIS-OPTION", FieldType.STRING, 4);
        print_Line_Filler5 = print_Line.newFieldInGroup("print_Line_Filler5", "FILLER5", FieldType.STRING, 10);
        print_Line_Cis_Gr_Years = print_Line.newFieldInGroup("print_Line_Cis_Gr_Years", "CIS-GR-YEARS", FieldType.STRING, 4);
        print_Line_Filler6 = print_Line.newFieldInGroup("print_Line_Filler6", "FILLER6", FieldType.STRING, 14);
        print_Line_Cis_Gr_Days = print_Line.newFieldInGroup("print_Line_Cis_Gr_Days", "CIS-GR-DAYS", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Audit.reset();
        vw_cis_Updt.reset();
        vw_iaa_Trnsfr_Sw.reset();

        ldaIatl406.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaAial0131.initializeValues();
        ldaAial0132.initializeValues();
        ldaNazl6004.initializeValues();

        localVariables.reset();
        pnd_L_Pnd_First_Call.setInitialValue(true);
        pnd_Total_Sw.setInitialValue("Y");
        pnd_Dod_1_Sw.setInitialValue("N");
        pnd_Dod_2_Sw.setInitialValue("N");
        pnd_Tiaa_Type.getValue(1).setInitialValue("T");
        pnd_Tiaa_Type.getValue(2).setInitialValue("G");
        pnd_Tiaa_Type.getValue(3).setInitialValue("R");
        pnd_Tiaa_Type.getValue(4).setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp407() throws Exception
    {
        super("Iatp407");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        //* *======================= AT TOP OF PAGE ===============================
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* *======================================================================
        //*                          START OF PROGRAM
        //* *======================================================================
        //*  10/2014 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATES
        sub_Get_Control_Dates();
        if (condition(Global.isEscape())) {return;}
        //*  012009
                                                                                                                                                                          //Natural: PERFORM GET-FUND-INFO
        sub_Get_Fund_Info();
        if (condition(Global.isEscape())) {return;}
        //*  CHANGED THE DRIVING RECORD TO IAA-TRNSFR-SW SIMILAR TO IATP408
        //*  012009 START
        //* *READ IAA-XFR-AUDIT BY IAA-XFR-AUDIT.RQST-ID
        vw_iaa_Trnsfr_Sw.startDatabaseRead                                                                                                                                //Natural: READ IAA-TRNSFR-SW BY IA-SUPER-DE-02 STARTING FROM '1M1 '
        (
        "RD1",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", "1M1 ", WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        RD1:
        while (condition(vw_iaa_Trnsfr_Sw.readNextRow("RD1")))
        {
            if (condition(iaa_Trnsfr_Sw_Xfr_Stts_Cde.notEquals("M1") || iaa_Trnsfr_Sw_Rcrd_Type_Cde.notEquals("1")))                                                      //Natural: IF IAA-TRNSFR-SW.XFR-STTS-CDE NE 'M1' OR IAA-TRNSFR-SW.RCRD-TYPE-CDE NE '1'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  ONLY FROM VARIABLE
            if (condition(!(iaa_Trnsfr_Sw_Rqst_Xfr_Type.equals(" "))))                                                                                                    //Natural: ACCEPT IF IAA-TRNSFR-SW.RQST-XFR-TYPE = ' '
            {
                continue;
            }
            //*    DISPLAY 'PIN' IAA-TRNSFR-SW.IA-UNIQUE-ID
            //*      'EFF DTE' IAA-TRNSFR-SW.RQST-EFFCTV-DTE
            //*      'FROM' IAA-TRNSFR-SW.IA-FRM-CNTRCT
            //*      'FPYEE' IAA-TRNSFR-SW.IA-FRM-PAYEE
            //*      'TO' IAA-TRNSFR-SW.IA-TO-CNTRCT
            //*      'TPYEE' IAA-TRNSFR-SW.IA-TO-PAYEE
            //*      'STTS' IAA-TRNSFR-SW.XFR-STTS-CDE
            //*      'TYPE' IAA-TRNSFR-SW.RQST-XFR-TYPE
            //*   WRITE IAXFR-CYCLE-DTE #TODAYS-DATE
            //*  ACCEPT IF (IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND EQ 'B' OR = 'C'
            //*    OR EQ 'T' OR = 'R')
            //*    AND (#TODAYS-DATE = IAXFR-CYCLE-DTE)
            //*  KN 7/99
            //*    AND (RQST-XFR-TYPE = ' ')
            pnd_L_Pnd_Rec_Found.reset();                                                                                                                                  //Natural: RESET #REC-FOUND
            vw_iaa_Xfr_Audit.startDatabaseFind                                                                                                                            //Natural: FIND IAA-XFR-AUDIT WITH IAA-XFR-AUDIT.RQST-ID = IAA-TRNSFR-SW.RQST-ID
            (
            "FD1",
            new Wc[] { new Wc("RQST_ID", "=", iaa_Trnsfr_Sw_Rqst_Id, WcType.WITH) }
            );
            FD1:
            while (condition(vw_iaa_Xfr_Audit.readNextRow("FD1", true)))
            {
                vw_iaa_Xfr_Audit.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Xfr_Audit.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORD FOUND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(!((((((iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind.equals("B") || iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind.equals("C"))            //Natural: ACCEPT IF ( IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND EQ 'B' OR = 'C' OR EQ 'T' OR = 'R' OR = 'D' ) AND #TODAYS-DATE = IAXFR-CYCLE-DTE
                    || iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind.equals("T")) || iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind.equals("R")) || iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind.equals("D")) 
                    && pnd_Todays_Date.equals(iaa_Xfr_Audit_Iaxfr_Cycle_Dte)))))
                {
                    continue;
                }
                pnd_Process_Date_A.setValueEdited(iaa_Xfr_Audit_Iaxfr_Cycle_Dte,new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED IAXFR-CYCLE-DTE ( EM = YYYYMMDD ) TO #PROCESS-DATE-A
                //*  WRITE '=' IAXFR-FROM-PPCN-NBR '=' IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND
                pnd_Count_Pe.setValue(iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data);                                                                                   //Natural: ASSIGN #COUNT-PE := C*IAXFR-CALC-TO-ACCT-DATA
                pnd_L_Pnd_Rec_Found.setValue(true);                                                                                                                       //Natural: ASSIGN #REC-FOUND := TRUE
                //*  FD1.
                //*  FIND IAA-TRNSFR-SW WITH IAA-TRNSFR-SW.RQST-ID =
                //*      IAA-XFR-AUDIT.RQST-ID
                //*    IF NO RECORD FOUND
                //*      RESET #FOUND
                //*      ESCAPE BOTTOM
                //*    END-NOREC
                //*    ACCEPT IF XFR-STTS-CDE EQ 'M1' AND RCRD-TYPE-CDE EQ '1'
                //*    #FOUND := 'Y'
                //*  END-FIND
                //*  IF #FOUND = ' '
                //*    COMPRESS  'Could not find transfer rec for'
                //*      IAA-XFR-AUDIT.RQST-ID INTO #ERR-REASON
                //*    PERFORM #WRITE-ERROR-REPORT
                //*    ESCAPE TOP
                //*  END-IF
                //*    RPT1.
                //*    REPEAT /* USED TO READ THE NEXT RECORD. (PROCESS-ERROR)
                //*  012009 END
                //*  012009
                ldaIatl406.getVw_cis_Prtcpnt_File().reset();                                                                                                              //Natural: RESET CIS-PRTCPNT-FILE #ERROR
                pnd_L_Pnd_Error.reset();
                                                                                                                                                                          //Natural: PERFORM GET-CIS-REC
                sub_Get_Cis_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pnd_Cis_Found.getBoolean())))                                                                                                            //Natural: IF NOT #CIS-FOUND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  052709
                    //*  012009
                    //*  012009
                }                                                                                                                                                         //Natural: END-IF
                pnd_L_Pnd_First_Call.setValue(true);                                                                                                                      //Natural: ASSIGN #FIRST-CALL := TRUE
                G1:                                                                                                                                                       //Natural: GET CIS-PRTCPNT-FILE #ISN
                ldaIatl406.getVw_cis_Prtcpnt_File().readByID(pnd_Isn.getLong(), "G1");
                G2:                                                                                                                                                       //Natural: GET IAA-TRNSFR-SW *ISN ( RD1. )
                vw_iaa_Trnsfr_Sw.readByID(vw_iaa_Trnsfr_Sw.getAstISN("FD1"), "G2");
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte()),          //Natural: ASSIGN CIS-ANNTY-START-DTE := IAXFR-EFFCTVE-DTE + 1
                    iaa_Xfr_Audit_Iaxfr_Effctve_Dte.add(1));
                                                                                                                                                                          //Natural: PERFORM NEW-CONTRACT-TIAA-OR-CREF
                sub_New_Contract_Tiaa_Or_Cref();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-COR-PAYEE-01
                sub_Process_Cor_Payee_01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012009 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-IAA-CNTRCT
                sub_Process_Iaa_Cntrct();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012009 END
                }                                                                                                                                                         //Natural: END-IF
                if (condition((((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) ||                     //Natural: IF CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR CNTRCT-OPTN-CDE = 10 THRU 17 OR CNTRCT-OPTN-CDE = 54 THRU 57
                    ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) 
                    && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17))) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) 
                    && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))))
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COR-PAYEE-02
                    sub_Process_Cor_Payee_02();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  012009 START
                    //*  012009 END
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();                                                                                      //Natural: RESET CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB CIS-SCND-ANNT-SSN
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();
                }                                                                                                                                                         //Natural: END-IF
                //*  012009 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-CIS-PRTCPNT-FILE
                sub_Fill_Cis_Prtcpnt_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012009 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-PRTCPNT-ROLE
                sub_Process_Prtcpnt_Role();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-GRD-STD-GRNTED-AMT
                sub_Fill_Grd_Std_Grnted_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COMPUTE-ANNTY-START-DATE
                sub_Compute_Annty_Start_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Curr_Proc_Dte);                                    //Natural: MOVE EDITED #CURR-PROC-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
                                                                                                                                                                          //Natural: PERFORM FILL-ADDRESS-INFO
                sub_Fill_Address_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  MOVE THE UPDATE TO CIS/XFR BEFORE CALLING THE CIS ROUTINE
                ldaIatl406.getVw_cis_Prtcpnt_File().updateDBRow("G1");                                                                                                    //Natural: UPDATE ( G1. )
                iaa_Trnsfr_Sw_Xfr_Stts_Cde.setValue("M2");                                                                                                                //Natural: ASSIGN XFR-STTS-CDE := 'M2'
                vw_iaa_Trnsfr_Sw.updateDBRow("G2");                                                                                                                       //Natural: UPDATE ( G2. )
                                                                                                                                                                          //Natural: PERFORM UPDATE-CIS-PRTCPNT-FILE
                sub_Update_Cis_Prtcpnt_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_L_Pnd_Issue_Error.getBoolean()))                                                                                                        //Natural: IF #ISSUE-ERROR
                {
                    pnd_Err_Reason.setValue("CREF CONTRACT MUST BE ISSUED MANUALLY");                                                                                     //Natural: ASSIGN #ERR-REASON := 'CREF CONTRACT MUST BE ISSUED MANUALLY'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                    sub_Pnd_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_L_Pnd_Issue_Error.reset();                                                                                                                        //Natural: RESET #ISSUE-ERROR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-COMPLETE-REPORT
                    sub_Write_Complete_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*    PERFORM WRITE-COMPLETE-REPORT                     /* 012009 END
                //* *  ESCAPE BOTTOM (RPT1.)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_L_Pnd_Rec_Found.getBoolean())))                                                                                                          //Natural: IF NOT #REC-FOUND
            {
                pnd_Err_Reason.setValue("COULD NOT FIND AUDIT REC");                                                                                                      //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND AUDIT REC'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *  /*==============================                  /* 012009 START
            //* *  DEFINE SUBROUTINE PROCESS-ERROR
            //* *  /*==============================
            //* *  BACKOUT TRANSACTION
            //* *  ESCAPE BOTTOM (RPT1.)
            //* *  END-SUBROUTINE                                    /* 012009 END
            //*    END-REPEAT /* RPT1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  10/2014 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-TOTALS
        sub_Write_Report_Totals();
        if (condition(Global.isEscape())) {return;}
        //* *======================================================================
        //*                          START OF SUBROUTINES
        //* *======================================================================
        //* ==============================                  /* 012009 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //* ********************************************************012009*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-ANNTY-START-DATE
        //* ***************************************************012009**************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-CIS-PRTCPNT-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATES
        //* ***********************************************************************
        //*  GET TODAY's date
        //* ***************************************************012009**************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-INFO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CONTROL-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-CONTRACT-TIAA-OR-CREF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-GRD-STD-GRNTED-AMT
        //* ********************************************************052709*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN063
        //* ********************************************************012009 START***
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-100
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-601-603
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-701-706
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-801-812
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COR-PAYEE-01
        //* ***********************************************************************
        //*  GET THE COR UNIQUE ID FOR PAYEE 01
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-PAYEE-CDE := '01'
        //*  10/2014 - START
        //*  #COR-SUPER-PIN-RCDTYPE.#PH-UNIQUE-ID-NBR  := #COR-UNIQUE-ID
        //*  #I-PIN := #COR-UNIQUE-ID
        //*  10/2014 - START
        //*    CIS-FRST-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
        //*    CIS-FRST-ANNT-PRFX     := PH-PRFX-NME
        //*    CIS-FRST-ANNT-FRST-NME := PH-FIRST-NME
        //*    CIS-FRST-ANNT-MID-NME  := PH-MDDLE-NME
        //*    CIS-FRST-ANNT-LST-NME  := PH-LAST-NME
        //*    CIS-FRST-ANNT-SFFX     := PH-SFFX-NME
        //*    CIS-FRST-ANNT-SSN      := #MDMA100.#O-SSN
        //*    #DATE-N8 := PH-DOB-DTE
        //*   MOVE EDITED #DATE-A8 TO #CIS-FRST-ANNT-DOB (EM=YYYYMMDD)
        //*    CIS-FRST-ANNT-SEX-CDE  := PH-SEX-CDE
        //* ********************************************************012009*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CIS-PRTCPNT-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-COMPLETE-REPORT
        //* ********************************************************012009*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *DE-CODE :=  CIS-PYMNT-MODE
        //*  CIS-PRTCPNT-FILE-VIEW
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-ERROR-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COR-PAYEE-02
        //* ***********************************************************************
        //*  GET THE COR UNIQUE ID FOR PAYEE 02
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-PAYEE-CDE := '02'
        //*  10/2014 - START
        //*  #COR-SUPER-PIN-RCDTYPE.#PH-UNIQUE-ID-NBR  := #COR-UNIQUE-ID
        //*  #I-PIN := #COR-UNIQUE-ID
        //*    #CIS-SCND-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
        //*    #CIS-SCND-ANNT-PRFX     := PH-PRFX-NME
        //*    #CIS-SCND-ANNT-FRST-NME := PH-FIRST-NME
        //*    #CIS-SCND-ANNT-MID-NME  := PH-MDDLE-NME
        //*    #CIS-SCND-ANNT-LST-NME  := PH-LAST-NME
        //*    #CIS-SCND-ANNT-SFFX     := PH-SFFX-NME
        //*  10/2014 - START
        //*    CIS-SCND-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
        //*    CIS-SCND-ANNT-PRFX     := PH-PRFX-NME
        //*    CIS-SCND-ANNT-FRST-NME := PH-FIRST-NME
        //*    CIS-SCND-ANNT-MID-NME  := PH-MDDLE-NME
        //*    CIS-SCND-ANNT-LST-NME  := PH-LAST-NME
        //*    CIS-SCND-ANNT-SFFX     := PH-SFFX-NME
        //*    CIS-SCND-ANNT-SSN      := #MDMA100.#O-SSN
        //*    #DATE-N8 := PH-DOB-DTE
        //*    CIS-SCND-ANNT-SEX-CDE  := PH-SEX-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        //*  10/2014 - START
        //* *#COR-UNIQUE-ID := #MDMA210.#O-PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COR-XREF-PH
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IAA-CNTRCT
        //*      #CIS-ANNTY-OPTION := 'OL'
        //*      #CIS-ANNTY-OPTION := 'LSF'
        //*      #CIS-ANNTY-OPTION := 'JS'
        //*      #CIS-ANNTY-OPTION := 'LS'
        //*      #CIS-ANNTY-OPTION := 'AC'
        //*      #CIS-ANNTY-OPTION := 'LST'
        //*    VALUE 05, 08, 16, 17, 54
        //*      #CIS-GUAR := '- 10 YR'
        //*    VALUE 09, 13, 14, 15, 55
        //*      #CIS-GUAR := '- 15 YR'
        //*    VALUE 06, 10, 11, 12, 56
        //*      #CIS-GUAR := '- 20 YR'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PRTCPNT-ROLE
        //* *#CNTRCT-PART-PPCN-NBR  := IAXFR-FROM-PPCN-NBR
        //* *#CNTRCT-PART-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
        //* *FIND(1) IAA-CNTRCT-PRTCPNT-ROLE WITH
        //* *    CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        //* *  IF NO RECORD FOUND
        //* *    #ERR-REASON := 'Could not find IAA-CNTRCT-PRTCPNT-ROLE'
        //* *    PERFORM #WRITE-ERROR-REPORT
        //* *    PERFORM PROCESS-ERROR
        //* *  END-NOREC
        //* *  IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RWRTTN-IND = 1
        //* *    MOVE 'Y' TO #CIS-REWRITE
        //* *  ELSE
        //* *    MOVE 'N' TO #CIS-REWRITE
        //* *  END-IF
        //* *END-FIND
        //* *RESET #AUDIT-CNTRCT #AUDIT-PAYEE
        //* *IF IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR = ' '
        //* *  #AUDIT-CNTRCT := IAA-XFR-AUDIT.IAXFR-FROM-PPCN-NBR
        //* *  #AUDIT-PAYEE  := IAA-XFR-AUDIT.IAXFR-FROM-PAYEE-CDE
        //* *ELSE
        //* *  #AUDIT-CNTRCT := IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR
        //* *  #AUDIT-PAYEE  := IAA-XFR-AUDIT.IAXFR-CALC-TO-PAYEE-CDE
        //* *END-IF
        //* *#CNTRCT-PART-PPCN-NBR  := #AUDIT-CNTRCT
        //* *#CNTRCT-PART-PAYEE-CDE := #AUDIT-PAYEE
        //*      #CIS-PYMNT-MODE := 'M'
        //*      #CIS-PYMNT-MODE := 'S'
        //*      #CIS-PYMNT-MODE := 'Q'
        //*      #CIS-PYMNT-MODE := 'A'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-ADDRESS-INFO
        //* ***********************************************************************
        //*  ==============================================
        //*  FILL PARTICIPANT CORRESPONDENCE ADDRESS POS(1)
        //*  ==============================================
        //*  10/2014 - START
        //* *#I-PIN := #MDMA210.#O-PIN
        //*  =============================================
        //*  FILL PARTICIPANT CHECK MAILING ADDRESS POS(2)
        //*  =============================================
        //* ********************************************************012009*********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CIS-REC
        //* ***********************************************************************
        //*  10/2014 - START
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *#CAD-CNTRCT-TYPE-CDE           := 'I'
        //* *#CAD-CORRESPONDENCE-ADDRSS-IND := 'O'
        //*  #CAD-CNTRCT-NMBR               := IAXFR-CALC-TO-PPCN-NBR
        //*  #CAD-CNTRCT-PAYEE-CDE          := IAXFR-CALC-TO-PAYEE-CDE
        //* *#CAD-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#CAD-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-NAME-ADDRESS WITH
        //*     CNTRCT-TYPE-CORR-CNTRCT-KEY = #CNTRCT-TYPE-CORR-CNTRCT-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for correspondence'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *#MAD-CNTRCT-TYPE-CDE           := 'I'
        //* *#MAD-CHECK-MAILING-ADDRSS-IND  := 'K'
        //*  #MAD-CNTRCT-NMBR               := IAXFR-CALC-TO-PPCN-NBR
        //*  #MAD-CNTRCT-PAYEE-CDE          := IAXFR-CALC-TO-PAYEE-CDE
        //* *#MAD-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#MAD-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-NAME-ADDRESS WITH
        //*     CNTRCT-TYPE-CHECK-CNTRCT-KEY = #CNTRCT-TYPE-CHECK-CNTRCT-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for mailing check'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *#CAD-V-CORRESPONDENCE-ADDRSS-IND :=  'O'
        //*  #CAD-V-CNTRCT-NMBR               := IAXFR-CALC-TO-PPCN-NBR
        //*  #CAD-V-CNTRCT-PAYEE-CDE          := IAXFR-CALC-TO-PAYEE-CDE
        //* *#CAD-V-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#CAD-V-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-VACATION-ADDRESS WITH
        //*     V-CORR-CNTRCT-PAYEE-KEY = #V-CORR-CNTRCT-PAYEE-KEY
        //*   IF NO RECORDS FOUND
        //*     MOVE  'Could not find NAS-VACATION-ADDRESS for correspondence.'
        //*       TO #ERR-REASON
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *#MAD-V-CHECK-MAILING-ADDRSS-IND := 'K'
        //*  #MAD-V-CNTRCT-NMBR              := IAXFR-CALC-TO-PPCN-NBR
        //*  #MAD-V-CNTRCT-PAYEE-CDE         := IAXFR-CALC-TO-PAYEE-CDE
        //* *#MAD-V-CNTRCT-NMBR              := IAXFR-FROM-PPCN-NBR
        //* *#MAD-V-CNTRCT-PAYEE-CDE         := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-VACATION-ADDRESS WITH
        //*     V-CHECK-CNTRCT-PAYEE-KEY = #V-CHECK-CNTRCT-PAYEE-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-VACATION-ADDRESS to mail check.'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //*  10/2014 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        //*   CIS-ADDRESS-DEST-NAME(#I) :=
        //*     NAS-VACATION-ADDRESS.V-CNTRCT-NAME-FREE
        //*     NAS-VACATION-ADDRESS.V-ADDRSS-LNE-2 #O-CO-ADDRESS-LINE-4
        //*   CIS-ADDRESS-TXT(#I,3) := #O-CO-ADDRESS-CITY
        //*     NAS-VACATION-ADDRESS.V-ADDRSS-LNE-3
        //*   CIS-ADDRESS-TXT(#I,4) :=
        //*     NAS-VACATION-ADDRESS.V-ADDRSS-LNE-4
        //*   CIS-ADDRESS-TXT(#I,5) :=
        //*     NAS-VACATION-ADDRESS.V-ADDRSS-LNE-5
        //*     NAS-VACATION-ADDRESS.V-ADDRSS-ZIP-PLUS-4
        //*   CIS-BANK-PYMNT-ACCT-NMBR(#I) :=
        //*     NAS-VACATION-ADDRESS.V-PH-BANK-PYMNT-ACCT-NMBR
        //*   CIS-BANK-ABA-ACCT-NMBR(#I) :=
        //*     NAS-VACATION-ADDRESS.V-BANK-ABA-ACCT-NMBR
        //*   CIS-CHECKING-SAVING-CD(#I) :=
        //*     NAS-VACATION-ADDRESS.V-CHECKING-SAVING-CDE
        //*  CIS-ADDRESS-DEST-NAME(#I) :=
        //*    NAS-NAME-ADDRESS.CNTRCT-NAME-FREE
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-1
        //*  CIS-ADDRESS-TXT(#I,2) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-2
        //*  CIS-ADDRESS-TXT(#I,3) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-3
        //*  CIS-ADDRESS-TXT(#I,4) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-4
        //*  CIS-ADDRESS-TXT(#I,5) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-5
        //*    NAS-NAME-ADDRESS.ADDRSS-ZIP-PLUS-4
        //*    NAS-NAME-ADDRESS.PH-BANK-PYMNT-ACCT-NMBR
        //*    NAS-NAME-ADDRESS.BANK-ABA-ACCT-NMBR
        //*    NAS-NAME-ADDRESS.CHECKING-SAVING-CDE
        //*  07/2018 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PARM-VALUES
        //*  07/2018 - END
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_L_Pnd_Error.setValue(true);                                                                                                                                   //Natural: ASSIGN #ERROR := TRUE
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        //*  012009 END
    }
    private void sub_Compute_Annty_Start_Date() throws Exception                                                                                                          //Natural: COMPUTE-ANNTY-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Curr_Proc_Dte.setValueEdited(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CURR-PROC-DTE
        pnd_Curr_Proc_Dte_Pnd_Dd.setValue(1);                                                                                                                             //Natural: ASSIGN #DD := 01
        if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100)))                                                                              //Natural: IF CNTRCT-MODE-IND = 100
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet2172 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #MM;//Natural: VALUE 01
            if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(1))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(701)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 701 OR = 801
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(801)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 02
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(2))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(702)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 702 OR = 802
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(802)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 03
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(3))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(703)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 703 OR = 803
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(803)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 04
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(4))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(704)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 704 OR = 804
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(804)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 05
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(5))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(705)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 705 OR = 805
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(805)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(6))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(706)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 706 OR = 806
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(806)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 07
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(7))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(701)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 701 OR = 807
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(807)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 08
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(8))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(702)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 702 OR = 808
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(808)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(9))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(703)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 703 OR = 809
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(809)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(10))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(704)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 704 OR = 810
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(810)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(11))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(705)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 705 OR = 811
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(811)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(12))))
            {
                decideConditionsMet2172++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(706)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 706 OR = 812
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(812)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Curr_Proc_Dte_Pnd_Mm.nadd(1);                                                                                                                             //Natural: ADD 1 TO #MM
            if (condition(pnd_Curr_Proc_Dte_Pnd_Mm.greater(12)))                                                                                                          //Natural: IF #MM GT 12
            {
                pnd_Curr_Proc_Dte_Pnd_Mm.setValue(1);                                                                                                                     //Natural: ASSIGN #MM := 01
                pnd_Curr_Proc_Dte_Pnd_Yyyy.nadd(1);                                                                                                                       //Natural: ADD 1 TO #YYYY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Fill_Cis_Prtcpnt_File() throws Exception                                                                                                             //Natural: FILL-CIS-PRTCPNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte().setValue(pnd_Todays_Date);                                                                                  //Natural: ASSIGN CIS-CNTRCT-PRINT-DTE := #TODAYS-DATE
        if (condition(iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull.equals("Y")))                                                                                                     //Natural: IF IA-NEW-ISS-PRT-PULL = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("SPEC");                                                                                              //Natural: ASSIGN CIS-PULL-CODE := 'SPEC'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("    ");                                                                                              //Natural: ASSIGN CIS-PULL-CODE := '    '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().reset();                                                                                                        //Natural: RESET CIS-OWNERSHIP-CD
        if (condition(((((((((((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR = 10 OR = 11 OR = 12 OR = 13 OR = 14 OR = 15 OR = 16 OR = 17 OR CNTRCT-OPTN-CDE = 54 THRU 57
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11)) 
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14)) 
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17)) 
            || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))))
        {
            if (condition(pnd_Dod_1_Sw.equals("Y")))                                                                                                                      //Natural: IF #DOD-1-SW = 'Y'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().setValue(2);                                                                                            //Natural: ASSIGN CIS-OWNERSHIP-CD := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Dod_2_Sw.equals("Y")))                                                                                                                  //Natural: IF #DOD-2-SW = 'Y'
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().setValue(1);                                                                                        //Natural: ASSIGN CIS-OWNERSHIP-CD := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  052709
        //*  033010 START
        pnd_Dod_1_Sw.reset();                                                                                                                                             //Natural: RESET #DOD-1-SW #DOD-2-SW CIS-TRNSF-FROM-COMPANY CIS-TIAA-DOI CIS-CREF-DOI CIS-CNTRCT-TYPE CIS-DA-CERT-NBR ( * ) CIS-DA-CREF-PROCEEDS-AMT ( * ) CIS-DA-TIAA-NBR ( * ) CIS-DA-TIAA-PROCEEDS-AMT ( * ) CIS-DA-REA-PROCEEDS-AMT ( * ) CIS-TRNSF-UNITS CIS-CREF-CNTRCT-TYPE CIS-TIAA-CNTRCT-TYPE CIS-TIAA-COMMUTED-INFO ( * ) CIS-TRNSF-TICKER ( * ) CIS-TRNSF-REVAL-TYPE ( * ) CIS-TRNSF-CREF-UNITS ( * ) CIS-TRNSF-RECE-COMPANY ( * )
        pnd_Dod_2_Sw.reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cert_Nbr().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Tiaa_Nbr().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Units().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Commuted_Info().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Ticker().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Reval_Type().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Cref_Units().getValue("*").reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Rece_Company().getValue("*").reset();
        //*                                               /* 033010 END
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().setValue("CREF");                                                                                     //Natural: ASSIGN CIS-TRNSF-FROM-COMPANY := 'CREF'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().setValue("TIAA");                                                                                     //Natural: ASSIGN CIS-TRNSF-FROM-COMPANY := 'TIAA'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi()), iaa_Xfr_Audit_Iaxfr_Effctve_Dte.add(1)); //Natural: ASSIGN CIS-TIAA-DOI := IAXFR-EFFCTVE-DTE + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi()), iaa_Xfr_Audit_Iaxfr_Effctve_Dte.add(1)); //Natural: ASSIGN CIS-CREF-DOI := IAXFR-EFFCTVE-DTE + 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Pin_Nbr().setValue(iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id);                                                                        //Natural: ASSIGN CIS-PIN-NBR := IAXFR-CALC-UNIQUE-ID
        ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id().setValue("IA");                                                                                                      //Natural: ASSIGN CIS-RQST-ID := 'IA'
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Type().setValue("T");                                                                                               //Natural: ASSIGN CIS-CNTRCT-TYPE := 'T'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Type().setValue("C");                                                                                               //Natural: ASSIGN CIS-CNTRCT-TYPE := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind().setValue("O");                                                                                                  //Natural: ASSIGN CIS-OPN-CLSD-IND := 'O'
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Status_Cd().setValue("I");                                                                                                 //Natural: ASSIGN CIS-STATUS-CD := 'I'
            pnd_L_Pnd_Issue_Error.setValue(false);                                                                                                                        //Natural: ASSIGN #ISSUE-ERROR := FALSE
            //*  FROM REA/ACCESS SHOULD REMAIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Status_Cd().setValue("A");                                                                                                 //Natural: ASSIGN CIS-STATUS-CD := 'A'
            pnd_L_Pnd_Issue_Error.setValue(true);                                                                                                                         //Natural: ASSIGN #ISSUE-ERROR := TRUE
            //*  ASSIGNED
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cert_Nbr().getValue(1).setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                         //Natural: ASSIGN CIS-DA-CERT-NBR ( 1 ) := IAXFR-FROM-PPCN-NBR
        //*  ?? NOT SURE
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Units().nadd(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue("*"));                                                    //Natural: ADD IAXFR-FROM-RQSTD-XFR-UNITS ( * ) TO CIS-TRNSF-UNITS
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                  //Natural: ASSIGN CIS-TRNSF-CERT-NBR := IAXFR-FROM-PPCN-NBR
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Acct_Cde().setValue(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(1));                                                        //Natural: ASSIGN CIS-TRNSF-ACCT-CDE := IAXFR-FRM-ACCT-CD ( 1 )
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Pymnt_Mode().setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(1));                                                     //Natural: ASSIGN CIS-TRNSF-PYMNT-MODE := IAXFR-FRM-UNIT-TYP ( 1 )
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Flag().setValue("Y");                                                                                                    //Natural: ASSIGN CIS-TRNSF-FLAG := 'Y'
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().equals("TIAA")))                                                                            //Natural: IF CIS-TRNSF-FROM-COMPANY EQ 'TIAA'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Cntrct_Type().setValue("T");                                                                                          //Natural: ASSIGN CIS-CREF-CNTRCT-TYPE := 'T'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type().setValue("C");                                                                                          //Natural: ASSIGN CIS-TIAA-CNTRCT-TYPE := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN CIS-CORP-SYNC-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().setValue("Y");                                                                                                  //Natural: ASSIGN CIS-ADDR-SYN-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Process_Env().setValue("B");                                                                                              //Natural: ASSIGN CIS-ADDR-PROCESS-ENV := 'B'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().setValue("Y");                                                                                                  //Natural: ASSIGN CIS-COR-SYNC-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Process_Env().setValue("B");                                                                                               //Natural: ASSIGN CIS-COR-PROCESS-ENV := 'B'
        pnd_Count_Pnd_Acct_Ctr.setValue(1);                                                                                                                               //Natural: ASSIGN #ACCT-CTR := 1
        pnd_Count_Pnd_Total_Amt.reset();                                                                                                                                  //Natural: RESET #TOTAL-AMT
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Typ.getValue("*").equals("D")))                                                                                          //Natural: IF IAXFR-TO-TYP ( * ) = 'D'
            {
                pnd_Count_Pnd_Total_Amt.nadd(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue("*"));                                                                                   //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + IAXFR-TO-QTY ( * )
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #SUB1 1 C*IAXFR-CALC-FROM-ACCT-DATA
            for (pnd_Count_Pnd_Sub1.setValue(1); condition(pnd_Count_Pnd_Sub1.lessOrEqual(iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data)); pnd_Count_Pnd_Sub1.nadd(1))
            {
                DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_Count_Pnd_Sub1)), //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR IAXFR-FRM-ACCT-CD ( #SUB1 ) GIVING INDEX #A
                    new ExamineGivingIndex(pnd_A));
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Ticker().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(pnd_Acct_Tckr.getValue(pnd_A));                               //Natural: ASSIGN CIS-TRNSF-TICKER ( #ACCT-CTR ) := #ACCT-TCKR ( #A )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Reval_Type().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_Count_Pnd_Sub1)); //Natural: ASSIGN CIS-TRNSF-REVAL-TYPE ( #ACCT-CTR ) := IAXFR-FRM-UNIT-TYP ( #SUB1 )
                pnd_W_Xfr_Units.getValue("*").reset();                                                                                                                    //Natural: RESET #W-XFR-UNITS ( * ) #VAR-TYPE
                pnd_Var_Type.reset();
                FOR03:                                                                                                                                                    //Natural: FOR #SUB2 1 C*IAXFR-CALC-TO-ACCT-DATA
                for (pnd_Count_Pnd_Sub2.setValue(1); condition(pnd_Count_Pnd_Sub2.lessOrEqual(iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data)); pnd_Count_Pnd_Sub2.nadd(1))
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_Count_Pnd_Sub2).equals(pnd_Tiaa_Type.getValue("*"))))                                       //Natural: IF IAXFR-TO-ACCT-CD ( #SUB2 ) = #TIAA-TYPE ( * )
                    {
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(pnd_Count_Pnd_Sub2).equals("P")))                                                               //Natural: IF IAXFR-TO-TYP ( #SUB2 ) = 'P'
                        {
                            pnd_W_Cref_Units.compute(new ComputeParameters(false, pnd_W_Cref_Units), (iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_Count_Pnd_Sub2).divide(100)).multiply(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_Count_Pnd_Sub1))); //Natural: COMPUTE #W-CREF-UNITS := ( IAXFR-TO-QTY ( #SUB2 ) /100 ) * IAXFR-FROM-RQSTD-XFR-UNITS ( #SUB1 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ratio.compute(new ComputeParameters(true, pnd_Ratio), iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_Count_Pnd_Sub2).divide(pnd_Count_Pnd_Total_Amt)); //Natural: COMPUTE ROUNDED #RATIO = IAXFR-TO-QTY ( #SUB2 ) / #TOTAL-AMT
                            pnd_W_Cref_Units.compute(new ComputeParameters(false, pnd_W_Cref_Units), pnd_Ratio.multiply(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_Count_Pnd_Sub1))); //Natural: COMPUTE #W-CREF-UNITS := #RATIO * IAXFR-FROM-RQSTD-XFR-UNITS ( #SUB1 )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_Count_Pnd_Sub2).equals(pnd_Tiaa_Type.getValue(1)) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_Count_Pnd_Sub2).equals(pnd_Tiaa_Type.getValue(2)))) //Natural: IF IAXFR-TO-ACCT-CD ( #SUB2 ) = #TIAA-TYPE ( 1 ) OR = #TIAA-TYPE ( 2 )
                        {
                            pnd_W_Xfr_Units_Pnd_W_To_Fund.getValue(1).setValue("T");                                                                                      //Natural: ASSIGN #W-TO-FUND ( 1 ) := 'T'
                            pnd_W_Xfr_Units_Pnd_W_To_Units.getValue(1).nadd(pnd_W_Cref_Units);                                                                            //Natural: ASSIGN #W-TO-UNITS ( 1 ) := #W-TO-UNITS ( 1 ) + #W-CREF-UNITS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_W_Xfr_Units_Pnd_W_To_Fund.getValue(2).setValue("S");                                                                                      //Natural: ASSIGN #W-TO-FUND ( 2 ) := 'S'
                            pnd_W_Xfr_Units_Pnd_W_To_Units.getValue(2).nadd(pnd_W_Cref_Units);                                                                            //Natural: ASSIGN #W-TO-UNITS ( 2 ) := #W-TO-UNITS ( 2 ) + #W-CREF-UNITS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Var_Type.setValue(true);                                                                                                                      //Natural: ASSIGN #VAR-TYPE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_W_Xfr_Units_Pnd_W_To_Fund.getValue(1).equals("T")))                                                                                     //Natural: IF #W-TO-FUND ( 1 ) = 'T'
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Cref_Units().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(pnd_W_Xfr_Units_Pnd_W_To_Units.getValue(1));          //Natural: ASSIGN CIS-TRNSF-CREF-UNITS ( #ACCT-CTR ) := #W-TO-UNITS ( 1 )
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Rece_Company().getValue(pnd_Count_Pnd_Acct_Ctr).setValue("TIAA");                                            //Natural: ASSIGN CIS-TRNSF-RECE-COMPANY ( #ACCT-CTR ) := 'TIAA'
                    pnd_Count_Pnd_Acct_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ACCT-CTR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Xfr_Units_Pnd_W_To_Fund.getValue(2).equals("S")))                                                                                     //Natural: IF #W-TO-FUND ( 2 ) = 'S'
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Cref_Units().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(pnd_W_Xfr_Units_Pnd_W_To_Units.getValue(2));          //Natural: ASSIGN CIS-TRNSF-CREF-UNITS ( #ACCT-CTR ) := #W-TO-UNITS ( 2 )
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Rece_Company().getValue(pnd_Count_Pnd_Acct_Ctr).setValue("SEPA");                                            //Natural: ASSIGN CIS-TRNSF-RECE-COMPANY ( #ACCT-CTR ) := 'SEPA'
                    if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Ticker().getValue(pnd_Count_Pnd_Acct_Ctr).equals(" ")))                                        //Natural: IF CIS-TRNSF-TICKER ( #ACCT-CTR ) = ' '
                    {
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Ticker().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Ticker().getValue(pnd_Count_Pnd_Acct_Ctr.getDec().subtract(1))); //Natural: ASSIGN CIS-TRNSF-TICKER ( #ACCT-CTR ) := CIS-TRNSF-TICKER ( #ACCT-CTR - 1 )
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Reval_Type().getValue(pnd_Count_Pnd_Acct_Ctr).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Reval_Type().getValue(pnd_Count_Pnd_Acct_Ctr.getDec().subtract(1))); //Natural: ASSIGN CIS-TRNSF-REVAL-TYPE ( #ACCT-CTR ) := CIS-TRNSF-REVAL-TYPE ( #ACCT-CTR - 1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Count_Pnd_Acct_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ACCT-CTR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Var_Type.getBoolean())))                                                                                                             //Natural: IF NOT #VAR-TYPE
                {
                    pnd_Total_Units.reset();                                                                                                                              //Natural: RESET #TOTAL-UNITS
                    pnd_Total_Units.nadd(pnd_W_Xfr_Units_Pnd_W_To_Units.getValue("*"));                                                                                   //Natural: ASSIGN #TOTAL-UNITS := #TOTAL-UNITS + #W-TO-UNITS ( * )
                    pnd_Diff.compute(new ComputeParameters(false, pnd_Diff), pnd_Total_Units.subtract(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_Count_Pnd_Sub1))); //Natural: ASSIGN #DIFF := #TOTAL-UNITS - IAXFR-FROM-RQSTD-XFR-UNITS ( #SUB1 )
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Cref_Units().getValue(pnd_Count_Pnd_Acct_Ctr.getDec().subtract(1)).nadd(pnd_Diff);                           //Natural: ADD #DIFF TO CIS-TRNSF-CREF-UNITS ( #ACCT-CTR - 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                /* 033010 END
    }
    private void sub_Get_Control_Dates() throws Exception                                                                                                                 //Natural: GET-CONTROL-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde().setValue("DC");                                                                                                        //Natural: ASSIGN #IAAN299A-IN.CNTRL-CDE := 'DC'
                                                                                                                                                                          //Natural: PERFORM #GET-CONTROL-REC
        sub_Pnd_Get_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte().equals(getZero()) || pdaIaaa299a.getPnd_Iaan299a_Out_Pnd_Return_Code().equals("E")))         //Natural: IF #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE = 0 OR #IAAN299A-OUT.#RETURN-CODE = 'E'
        {
            getReports().write(1, ReportOption.NOTITLE,"Control Record 'DC' returned an invalid date");                                                                   //Natural: WRITE ( 1 ) 'Control Record "DC" returned an invalid date'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Todays date   = ",pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                        //Natural: WRITE ( 1 ) 'Todays date   = ' #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Fatal error Program Terminated");                                                                                 //Natural: WRITE ( 1 ) 'Fatal error Program Terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Todays_Date.setValue(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                                                             //Natural: ASSIGN #TODAYS-DATE := #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTROL-DATES
    }
    private void sub_Get_Fund_Info() throws Exception                                                                                                                     //Natural: GET-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), pnd_Nbr_Acct, pnd_Acct_Std_Alpha_Cde.getValue("*"), pnd_Acct_Nme_5.getValue("*"), pnd_Acct_Tckr.getValue("*"),  //Natural: CALLNAT 'IATN216' #NBR-ACCT #ACCT-STD-ALPHA-CDE ( * ) #ACCT-NME-5 ( * ) #ACCT-TCKR ( * ) #ACCT-EFFCTVE-DTE ( * ) #ACCT-UNIT-RTE-IND ( * ) #ACCT-STD-NM-CDE ( * ) #ACCT-RPT-SEQ ( * )
            pnd_Acct_Effctve_Dte.getValue("*"), pnd_Acct_Unit_Rte_Ind.getValue("*"), pnd_Acct_Std_Nm_Cde.getValue("*"), pnd_Acct_Rpt_Seq.getValue("*"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Get_Control_Rec() throws Exception                                                                                                               //Natural: #GET-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FETCH LATEST CONTROL DATE
        DbsUtil.callnat(Iaan299a.class , getCurrentProcessState(), pdaIaaa299a.getPnd_Iaan299a_In(), pdaIaaa299a.getPnd_Iaan299a_Out(), pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1()); //Natural: CALLNAT 'IAAN299A' #IAAN299A-IN #IAAN299A-OUT #IAA-CNTRL-RCRD-1
        if (condition(Global.isEscape())) return;
        //*  #GET-CONTROL-REC
    }
    private void sub_New_Contract_Tiaa_Or_Cref() throws Exception                                                                                                         //Natural: NEW-CONTRACT-TIAA-OR-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_From_Cref.reset();                                                                                                                                            //Natural: RESET #FROM-CREF #FROM-REAL
        pnd_From_Real.reset();
        //*  RE
        if (condition(((DbsUtil.maskMatches(iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A1,"N") || iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A1.equals("Z")) &&                     //Natural: IF ( #IAXFR-FROM-PPCN-NBR-A1 = MASK ( N ) OR = 'Z' ) AND NOT ( #IAXFR-FROM-PPCN-NBR-A3 = '6L7' OR = '6M7' OR = '6N7' )
            ! (((iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3.equals("6L7") || iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3.equals("6M7")) || iaa_Xfr_Audit_Pnd_Iaxfr_From_Ppcn_Nbr_A3.equals("6N7"))))))
        {
            pnd_From_Cref.setValue("Y");                                                                                                                                  //Natural: ASSIGN #FROM-CREF := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_From_Real.setValue("Y");                                                                                                                                  //Natural: ASSIGN #FROM-REAL := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fill_Grd_Std_Grnted_Amt() throws Exception                                                                                                           //Natural: FILL-GRD-STD-GRNTED-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  052709
        pdaAiaa6007.getPrm_Naza6007().reset();                                                                                                                            //Natural: RESET PRM-NAZA6007 #COUNTER
        pnd_Count_Pnd_Counter.reset();
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  052209
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals("  ")))                                                                                  //Natural: IF IAXFR-TO-FUND-CDE ( #I ) = '  '
            {
                //*  052209
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  052209
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals("1G")))                                                                                  //Natural: IF IAXFR-TO-FUND-CDE ( #I ) = '1G'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Grd_Amt().setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                                       //Natural: ASSIGN CIS-GRNTED-GRD-AMT := IAXFR-TO-AFTR-XFR-GUAR ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  IF IAXFR-TO-FUND-CDE(#I) = '1T'                     /* 052209
            //*  052209
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue(pnd_I).equals("1S")))                                                                                  //Natural: IF IAXFR-TO-FUND-CDE ( #I ) = '1S'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Std_Amt().setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                                       //Natural: ASSIGN CIS-GRNTED-STD-AMT := IAXFR-TO-AFTR-XFR-GUAR ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  THE FOLLOWING LOGIC TAKEN FROM ADSP6002            /* 052709 START
        //*  GRADED IS FILLED-UP FIRST AND THEN STANDARD
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Grd_Amt().greater(getZero())))                                                                            //Natural: IF CIS-GRNTED-GRD-AMT > 0
        {
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue("*")), new ExamineSearch("1G"), new ExamineGivingIndex(pnd_I));                    //Natural: EXAMINE IAXFR-TO-FUND-CDE ( * ) FOR '1G' GIVING INDEX #I
            pdaAiaa6007.getPrm_Naza6007_Prm_Std_Grd_Ind().setValue("G");                                                                                                  //Natural: ASSIGN PRM-STD-GRD-IND := 'G'
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Rate_Code().getValue(1).setValue(iaa_Xfr_Audit_Iaxfr_To_Rate_Cde.getValue(pnd_I));                                       //Natural: ASSIGN PRM-RCGP-RATE-CODE ( 1 ) := IAXFR-TO-RATE-CDE ( #I )
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Guar_Paymt().getValue(1).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Grd_Amt());                                  //Natural: ASSIGN PRM-RCGP-GUAR-PAYMT ( 1 ) := CIS-GRNTED-GRD-AMT
            //*  112310 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area().reset();                                                                                                              //Natural: RESET NAZ-ACT-CALC-INPUT-AREA
            if (condition(DbsUtil.maskMatches(pnd_Date_A8,"YYYYMMDD")))                                                                                                   //Natural: IF #DATE-A8 = MASK ( YYYYMMDD )
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                //Natural: MOVE EDITED #DATE-A8 TO NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
                //*  112310 END
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde());                                             //Natural: ASSIGN NAZ-ACT-OPTION-CDE := IAA-CNTRCT.CNTRCT-OPTN-CDE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN063
            sub_Call_Aian063();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaAiaa6007.getPrm_Naza6007_Prm_Return_Code().notEquals(getZero())))                                                                            //Natural: IF PRM-RETURN-CODE NE 0
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Pnd_Counter.setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Occurs_Ctr());                                                                             //Natural: MOVE PRM-PMB-OCCURS-CTR TO #COUNTER
            FOR05:                                                                                                                                                        //Natural: FOR #I 1 TO PRM-PMB-OCCURS-CTR
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Occurs_Ctr())); pnd_I.nadd(1))
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2().getValue(pnd_I).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Paymt_Amount().getValue(pnd_I));     //Natural: MOVE PRM-PMB-PAYMT-AMOUNT ( #I ) TO CIS-COMUT-GRNTED-AMT-2 ( #I )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Pymt_Method_2().getValue(pnd_I).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Paymt_Method().getValue(pnd_I));    //Natural: MOVE PRM-PMB-PAYMT-METHOD ( #I ) TO CIS-COMUT-PYMT-METHOD-2 ( #I )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Int_Rate_2().getValue(pnd_I).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Eff_Anl_Int().getValue(pnd_I));        //Natural: MOVE PRM-PMB-EFF-ANL-INT ( #I ) TO CIS-COMUT-INT-RATE-2 ( #I )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Mortality_Basis().getValue(pnd_I).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Mort_Basis().getValue(pnd_I));    //Natural: MOVE PRM-PMB-MORT-BASIS ( #I ) TO CIS-COMUT-MORTALITY-BASIS ( #I )
                //*    WRITE
                //*      '=' CIS-COMUT-GRNTED-AMT-2 (#I) /
                //*      '=' CIS-COMUT-PYMT-METHOD-2 (#I) /
                //*      '=' CIS-COMUT-INT-RATE-2 (#I) /
                //*      '=' CIS-COMUT-MORTALITY-BASIS (#I)
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Std_Amt().greater(getZero())))                                                                            //Natural: IF CIS-GRNTED-STD-AMT > 0
        {
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Rate_Code().getValue("*").reset();                                                                                       //Natural: RESET PRM-RCGP-RATE-CODE ( * ) PRM-RCGP-GUAR-PAYMT ( * )
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Guar_Paymt().getValue("*").reset();
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Audit_Iaxfr_To_Fund_Cde.getValue("*")), new ExamineSearch("1S"), new ExamineGivingIndex(pnd_I));                    //Natural: EXAMINE IAXFR-TO-FUND-CDE ( * ) FOR '1S' GIVING INDEX #I
            pdaAiaa6007.getPrm_Naza6007_Prm_Std_Grd_Ind().setValue("S");                                                                                                  //Natural: ASSIGN PRM-STD-GRD-IND := 'S'
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Rate_Code().getValue(1).setValue(iaa_Xfr_Audit_Iaxfr_To_Rate_Cde.getValue(pnd_I));                                       //Natural: ASSIGN PRM-RCGP-RATE-CODE ( 1 ) := IAXFR-TO-RATE-CDE ( #I )
            pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Guar_Paymt().getValue(1).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Std_Amt());                                  //Natural: ASSIGN PRM-RCGP-GUAR-PAYMT ( 1 ) := CIS-GRNTED-STD-AMT
            //*  112310 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area().reset();                                                                                                              //Natural: RESET NAZ-ACT-CALC-INPUT-AREA
            if (condition(DbsUtil.maskMatches(pnd_Date_A8,"YYYYMMDD")))                                                                                                   //Natural: IF #DATE-A8 = MASK ( YYYYMMDD )
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                //Natural: MOVE EDITED #DATE-A8 TO NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
                //*  112310 END
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde());                                             //Natural: ASSIGN NAZ-ACT-OPTION-CDE := IAA-CNTRCT.CNTRCT-OPTN-CDE
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN063
            sub_Call_Aian063();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaAiaa6007.getPrm_Naza6007_Prm_Return_Code().notEquals(getZero())))                                                                            //Natural: IF PRM-RETURN-CODE NE 0
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            FOR06:                                                                                                                                                        //Natural: FOR #I 1 TO PRM-PMB-OCCURS-CTR
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Occurs_Ctr())); pnd_I.nadd(1))
            {
                pnd_Count_Pnd_Counter.nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNTER
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2().getValue(pnd_Count_Pnd_Counter).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Paymt_Amount().getValue(pnd_I)); //Natural: MOVE PRM-PMB-PAYMT-AMOUNT ( #I ) TO CIS-COMUT-GRNTED-AMT-2 ( #COUNTER )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Pymt_Method_2().getValue(pnd_Count_Pnd_Counter).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Paymt_Method().getValue(pnd_I)); //Natural: MOVE PRM-PMB-PAYMT-METHOD ( #I ) TO CIS-COMUT-PYMT-METHOD-2 ( #COUNTER )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Int_Rate_2().getValue(pnd_Count_Pnd_Counter).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Eff_Anl_Int().getValue(pnd_I)); //Natural: MOVE PRM-PMB-EFF-ANL-INT ( #I ) TO CIS-COMUT-INT-RATE-2 ( #COUNTER )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Comut_Mortality_Basis().getValue(pnd_Count_Pnd_Counter).setValue(pdaAiaa6007.getPrm_Naza6007_Prm_Pmb_Mort_Basis().getValue(pnd_I)); //Natural: MOVE PRM-PMB-MORT-BASIS ( #I ) TO CIS-COMUT-MORTALITY-BASIS ( #COUNTER )
                //*    WRITE
                //*      '=' CIS-COMUT-GRNTED-AMT-2 (#COUNTER) /
                //*      '=' CIS-COMUT-PYMT-METHOD-2 (#COUNTER) /
                //*      '=' CIS-COMUT-INT-RATE-2 (#COUNTER) /
                //*      '=' CIS-COMUT-MORTALITY-BASIS (#COUNTER)
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  012009 START (TAKEN FROM IATP408)
        cntrct_Dte_D.setValueEdited(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMM"));                                                 //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMM ) TO CNTRCT-DTE-D
        if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().equals(getZero()) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().lessOrEqual(cntrct_Dte_D_Cntrct_Dte_Date))) //Natural: IF CNTRCT-FINAL-PER-PAY-DTE = 0 OR CNTRCT-FINAL-PER-PAY-DTE LE CNTRCT-DTE-DATE
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_End_Dte().reset();                                                                                                   //Natural: RESET CIS-ANNTY-END-DTE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A8_Pnd_Date_Dd.setValue(1);                                                                                                                              //Natural: ASSIGN #DATE-DD := 01
        short decideConditionsMet2488 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100))))
        {
            decideConditionsMet2488++;
                                                                                                                                                                          //Natural: PERFORM MODE-100
            sub_Mode_100();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(601) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(603)))))
        {
            decideConditionsMet2488++;
                                                                                                                                                                          //Natural: PERFORM MODE-601-603
            sub_Mode_601_603();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(701) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(706)))))
        {
            decideConditionsMet2488++;
                                                                                                                                                                          //Natural: PERFORM MODE-701-706
            sub_Mode_701_706();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(801) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(812)))))
        {
            decideConditionsMet2488++;
                                                                                                                                                                          //Natural: PERFORM MODE-801-812
            sub_Mode_801_812();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            pnd_Issue_Date_A8.setValueEdited(ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #ISSUE-DATE-A8
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Issue_Date_A8.setValueEdited(ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED CIS-CREF-DOI ( EM = YYYYMMDD ) TO #ISSUE-DATE-A8
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISSUE DTE < PERIOD END DTE           **
        if (condition(pnd_Issue_Date_A8_Pnd_Issue_Date_N8.less(pnd_Date_A8_Pnd_Date_N8)))                                                                                 //Natural: IF #ISSUE-DATE-N8 LT #DATE-N8
        {
            if (condition(pnd_Date_A8_Pnd_Date_Mm.less(pnd_Issue_Date_A8_Pnd_Issue_Date_Mm)))                                                                             //Natural: IF #DATE-MM LT #ISSUE-DATE-MM
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),      //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = ( #DATE-YYYY - 1 ) - #ISSUE-DATE-YYYY
                    (pnd_Date_A8_Pnd_Date_Yyyy.subtract(1)).subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Date_A8_Pnd_Date_Mm.equals(pnd_Issue_Date_A8_Pnd_Issue_Date_Mm) && pnd_Date_A8_Pnd_Date_A8_Dd.less(pnd_Issue_Date_A8_Pnd_Issue_Date_Dd))) //Natural: IF #DATE-MM = #ISSUE-DATE-MM AND #DATE-A8-DD LT #ISSUE-DATE-DD
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = ( #DATE-YYYY - 1 ) - #ISSUE-DATE-YYYY
                        (pnd_Date_A8_Pnd_Date_Yyyy.subtract(1)).subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = #DATE-YYYY - #ISSUE-DATE-YYYY
                        pnd_Date_A8_Pnd_Date_Yyyy.subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy.nadd(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs());                                                               //Natural: ADD CIS-GRNTED-PERIOD-YRS TO #ISSUE-DATE-YYYY
        if (condition(! (DbsUtil.maskMatches(pnd_Issue_Date_A8,"YYYYMMDD"))))                                                                                             //Natural: IF #ISSUE-DATE-A8 NE MASK ( YYYYMMDD )
        {
            pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(28);                                                                                                             //Natural: ASSIGN #ISSUE-DATE-DD := 28
            if (condition(pnd_From_Cref.equals("Y")))                                                                                                                     //Natural: IF #FROM-CREF = 'Y'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi().setValue(iaa_Xfr_Audit_Iaxfr_Effctve_Dte);                                                                  //Natural: ASSIGN CIS-TIAA-DOI := IAXFR-EFFCTVE-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().setValue(iaa_Xfr_Audit_Iaxfr_Effctve_Dte);                                                                  //Natural: ASSIGN CIS-CREF-DOI := IAXFR-EFFCTVE-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISS DTE
        pnd_Issue_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Issue_Date_A8);                                                                                //Natural: MOVE EDITED #ISSUE-DATE-A8 TO #ISSUE-DATE-D ( EM = YYYYMMDD )
        //*  GRNTD END DTE
        pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                                                                            //Natural: MOVE EDITED #DATE-A8 TO #DATE-D ( EM = YYYYMMDD )
        //*  CALC THE NUMBER OF DAYS
        ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys()),              //Natural: COMPUTE CIS-GRNTED-PERIOD-DYS = #DATE-D - #ISSUE-DATE-D
            pnd_Date_D.subtract(pnd_Issue_Date_D));
        //*  FILL-GRD-STD-GRNTED-AMT
    }
    private void sub_Call_Aian063() throws Exception                                                                                                                      //Natural: CALL-AIAN063
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_L_Pnd_First_Call.getBoolean()))                                                                                                                 //Natural: IF #FIRST-CALL
        {
            pdaAiaa6007.getPrm_Naza6007_Prm_Cn_Pfx().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Ppcn_Nbr().getSubstring(1,2));                                             //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,2 ) TO PRM-CN-PFX
            pdaAiaa6007.getPrm_Naza6007_Prm_Cn_Nmbr().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Ppcn_Nbr().getSubstring(3,6));                                            //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,3,6 ) TO PRM-CN-NMBR
            pdaAiaa6007.getPrm_Naza6007_Prm_Origin().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                               //Natural: MOVE CNTRCT-ORGN-CDE TO PRM-ORIGIN
            pnd_Annty_Strt_Dte.setValueEdited(ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte(),new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #ANNTY-STRT-DTE
            //*  REA0614
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Annty_Strt_Dte);                                   //Natural: MOVE EDITED #ANNTY-STRT-DTE TO NAZ-ACT-ASD-DATE ( EM = YYYYMMDD )
            pdaAiaa6007.getPrm_Naza6007_Prm_Id_Century().setValue(pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc);                                                              //Natural: MOVE #ANNTY-STRT-DTE-CC TO PRM-ID-CENTURY
            pdaAiaa6007.getPrm_Naza6007_Prm_Id_Year().setValue(pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy);                                                                 //Natural: MOVE #ANNTY-STRT-DTE-YY TO PRM-ID-YEAR
            pdaAiaa6007.getPrm_Naza6007_Prm_Id_Month().setValue(pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm);                                                                //Natural: MOVE #ANNTY-STRT-DTE-MM TO PRM-ID-MONTH
            pnd_L_Pnd_First_Call.reset();                                                                                                                                 //Natural: RESET #FIRST-CALL
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa6007.getPrm_Naza6007_Prm_Rcgp_Occurs_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN PRM-RCGP-OCCURS-CTR := 1
        //*  112310
        DbsUtil.callnat(Aian063.class , getCurrentProcessState(), pdaAiaa6007.getPrm_Naza6007(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                                //Natural: CALLNAT 'AIAN063' PRM-NAZA6007 NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        if (condition(pdaAiaa6007.getPrm_Naza6007_Prm_Return_Code().notEquals(getZero())))                                                                                //Natural: IF PRM-RETURN-CODE NE 0
        {
            pnd_Err_Reason.setValue(DbsUtil.compress("AIAN063 error with RC=", pdaAiaa6007.getPrm_Naza6007_Prm_Return_Code(), "for call ind", pdaAiaa6007.getPrm_Naza6007_Prm_Std_Grd_Ind())); //Natural: COMPRESS 'AIAN063 error with RC=' PRM-RETURN-CODE 'for call ind' PRM-STD-GRD-IND INTO #ERR-REASON
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_100() throws Exception                                                                                                                          //Natural: MODE-100
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Date_A8_Pnd_Date_Mm.equals(12)))                                                                                                                //Natural: IF #DATE-MM = 12
        {
            pnd_Date_A8_Pnd_Date_Mm.setValue(1);                                                                                                                          //Natural: ASSIGN #DATE-MM := 1
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Date_A8_Pnd_Date_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_601_603() throws Exception                                                                                                                      //Natural: MODE-601-603
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Mm.nadd(3);                                                                                                                                  //Natural: ADD 3 TO #DATE-MM
        if (condition(pnd_Date_A8_Pnd_Date_Mm.greater(12)))                                                                                                               //Natural: IF #DATE-MM > 12
        {
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
            pnd_Date_A8_Pnd_Date_Mm.nsubtract(12);                                                                                                                        //Natural: SUBTRACT 12 FROM #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_701_706() throws Exception                                                                                                                      //Natural: MODE-701-706
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Mm.nadd(6);                                                                                                                                  //Natural: ADD 6 TO #DATE-MM
        if (condition(pnd_Date_A8_Pnd_Date_Mm.greater(12)))                                                                                                               //Natural: IF #DATE-MM > 12
        {
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
            pnd_Date_A8_Pnd_Date_Mm.nsubtract(12);                                                                                                                        //Natural: SUBTRACT 12 FROM #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_801_812() throws Exception                                                                                                                      //Natural: MODE-801-812
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                                //Natural: ADD 1 TO #DATE-YYYY
        //*  012009 END
    }
    //*  10/2014
    private void sub_Process_Cor_Payee_01() throws Exception                                                                                                              //Natural: PROCESS-COR-PAYEE-01
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(1);                                                                                                         //Natural: ASSIGN #I-PAYEE-CODE := 01
                                                                                                                                                                          //Natural: PERFORM READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        sub_Read_Cor_Xref_Cntrct_For_Unique_Id();
        if (condition(Global.isEscape())) {return;}
        pnd_Reason1.reset();                                                                                                                                              //Natural: RESET #REASON1
        if (condition(pnd_Cor_Unique_Id.equals(getZero())))                                                                                                               //Natural: IF #COR-UNIQUE-ID EQ 0
        {
            //*  RESET #CIS-FRST-ANNT-SSN                            /* 012009 START
            //*    #CIS-FRST-ANNT-PRFX
            //*    #CIS-FRST-ANNT-FRST-NME
            //*    #CIS-FRST-ANNT-MID-NME
            //*    #CIS-FRST-ANNT-LST-NME
            //*    #CIS-FRST-ANNT-SFFX
            //*    #CIS-FRST-ANNT-DOB
            //*  012009 END
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().reset();                                                                                                   //Natural: RESET CIS-FRST-ANNT-SSN CIS-FRST-ANNT-PRFX CIS-FRST-ANNT-FRST-NME CIS-FRST-ANNT-MID-NME CIS-FRST-ANNT-LST-NME CIS-FRST-ANNT-SFFX CIS-FRST-ANNT-DOB
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().reset();
            pnd_Reason1.setValue("DATA NOT AVAILABLE");                                                                                                                   //Natural: ASSIGN #REASON1 := 'DATA NOT AVAILABLE'
            pnd_Err_Reason.setValue("COULD NOT FIND COR UNIQUE ID. (PAYEE 01)");                                                                                          //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND COR UNIQUE ID. (PAYEE 01)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            //*  PERFORM PROCESS-ERROR
            //*  082017
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Cor_Unique_Id);                                                                                        //Natural: ASSIGN #I-PIN-N12 := #COR-UNIQUE-ID
            //*  IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 2
            //*    #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 1
            //*  ELSE IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 4
            //*      #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 3
            //*    END-IF
            //*  END-IF
            //*  10/2014 - END
            //*  #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE   := 01
                                                                                                                                                                          //Natural: PERFORM GET-COR-XREF-PH
            sub_Get_Cor_Xref_Ph();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Found.notEquals("Y")))                                                                                                                      //Natural: IF #FOUND NOT = 'Y'
            {
                //*    RESET #CIS-FRST-ANNT-SSN                          /* 012009 START
                //*      #CIS-FRST-ANNT-PRFX
                //*      #CIS-FRST-ANNT-FRST-NME
                //*      #CIS-FRST-ANNT-MID-NME
                //*      #CIS-FRST-ANNT-LST-NME
                //*      #CIS-FRST-ANNT-SFFX
                //*      #CIS-FRST-ANNT-DOB
                //*  ELSE
                //*    #CIS-FRST-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
                //*    #CIS-FRST-ANNT-PRFX     := PH-PRFX-NME
                //*    #CIS-FRST-ANNT-FRST-NME := PH-FIRST-NME
                //*    #CIS-FRST-ANNT-MID-NME  := PH-MDDLE-NME
                //*    #CIS-FRST-ANNT-LST-NME  := PH-LAST-NME
                //*    #CIS-FRST-ANNT-SFFX     := PH-SFFX-NME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().reset();                                                                                               //Natural: RESET CIS-FRST-ANNT-SSN CIS-FRST-ANNT-PRFX CIS-FRST-ANNT-FRST-NME CIS-FRST-ANNT-MID-NME CIS-FRST-ANNT-LST-NME CIS-FRST-ANNT-SFFX CIS-FRST-ANNT-DOB
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().reset();
                //*  082017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Ssn());                                                       //Natural: ASSIGN CIS-FRST-ANNT-SSN := #MDMA101.#O-SSN
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix());                                                   //Natural: ASSIGN CIS-FRST-ANNT-PRFX := #O-PREFIX
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                           //Natural: ASSIGN CIS-FRST-ANNT-FRST-NME := #O-FIRST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                           //Natural: ASSIGN CIS-FRST-ANNT-MID-NME := #O-MIDDLE-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                             //Natural: ASSIGN CIS-FRST-ANNT-LST-NME := #O-LAST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix());                                                   //Natural: ASSIGN CIS-FRST-ANNT-SFFX := #O-SUFFIX
                //*    IF   PH-DOD-DTE > 0                               /* 012009
                //*  012009
                //*  012009
                //*  012009
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero()) && iaa_Xfr_Audit_Iaxfr_From_Payee_Cde.greater(1)))                       //Natural: IF #O-DATE-OF-DEATH > 0 AND IAXFR-FROM-PAYEE-CDE GT 01
                {
                    pnd_Dod_1_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #DOD-1-SW := 'Y'
                    //*  012009
                    //*  012009
                }                                                                                                                                                         //Natural: END-IF
                pnd_Date_A8_Pnd_Date_N8.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                        //Natural: ASSIGN #DATE-N8 := #O-DATE-OF-BIRTH
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Gender_Code());                                           //Natural: ASSIGN CIS-FRST-ANNT-SEX-CDE := #O-GENDER-CODE
                //*  10/2014 - END
                //*  012009
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-FRST-ANNT-DOB ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-COR-PAYEE-01
    }
    private void sub_Update_Cis_Prtcpnt_File() throws Exception                                                                                                           //Natural: UPDATE-CIS-PRTCPNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key().setValue(iaa_Xfr_Audit_Rqst_Id);                                                                                 //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY := IAA-XFR-AUDIT.RQST-ID
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key());                                             //Natural: MOVE CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY TO #CIS-ENTRY-DATE-TIME-KEY
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Requestor().setValue("IA");                                                                                                   //Natural: MOVE 'IA' TO #CIS-REQUESTOR
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Pin_Number().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Pin_Nbr());                                                          //Natural: MOVE CIS-PIN-NBR TO #CIS-PIN-NUMBER
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Ssn().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn());                                                           //Natural: MOVE CIS-FRST-ANNT-SSN TO #CIS-SSN
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Dob().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob());                                                           //Natural: MOVE CIS-FRST-ANNT-DOB TO #CIS-DOB
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Tiaa_No().getValue(1).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Nbr());                                                //Natural: MOVE CIS-TIAA-NBR TO #CIS-TIAA-NO ( 1 )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Cref_No().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Cert_Nbr());                                                            //Natural: MOVE CIS-CERT-NBR TO #CIS-CREF-NO
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme());                                   //Natural: MOVE CIS-RQST-SYS-MIT-LOG-DTE-TME TO #CIS-RQST-LOG-DTE-TME
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("I");                                                                                  //Natural: MOVE 'I' TO #CIS-ASSIGN-ISSUE-CNTRCT-IND
        }                                                                                                                                                                 //Natural: END-IF
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Product_Cde().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #CIS-PRODUCT-CDE
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Cntrct_Type().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #CIS-CNTRCT-TYPE
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Msg_Text().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #CIS-MSG-TEXT
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Msg_Number().getValue("*").setValue(" ");                                                                                     //Natural: MOVE ' ' TO #CIS-MSG-NUMBER ( * )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Functions().getValue(4).setValue("YB");                                                                                       //Natural: MOVE 'YB' TO #CIS-FUNCTIONS ( 4 )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Mit_Function_Code().setValue("MO");                                                                                           //Natural: MOVE 'MO' TO #CIS-MIT-FUNCTION-CODE
        DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), ldaNazl6004.getPnd_Nazl6004());                                                                        //Natural: CALLNAT 'CISN1000' USING #NAZL6004
        if (condition(Global.isEscape())) return;
        //*  REA0614 START
        vw_cis_Updt.startDatabaseFind                                                                                                                                     //Natural: FIND CIS-UPDT WITH CIS-UPDT.CIS-RQST-ID-KEY = CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cis_Updt.readNextRow("FIND01")))
        {
            vw_cis_Updt.setIfNotFoundControlFlag(false);
            FOR07:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(" ")))                                                                                //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I)),               //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR IAXFR-TO-ACCT-CD ( #I ) GIVING INDEX #A
                    new ExamineGivingIndex(pnd_A));
                //*  PER KRIS, SHOULD NOT BE LOADED FOR TIAA TYPE FUNDS
                if (condition(pnd_A.greater(getZero()) && ! (iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Tiaa_Type.getValue("*")))))                        //Natural: IF #A GT 0 AND NOT ( IAXFR-TO-ACCT-CD ( #I ) = #TIAA-TYPE ( * ) )
                {
                    cis_Updt_Cis_Sg_Fund_Ticker.getValue(pnd_I).setValue(pnd_Acct_Tckr.getValue(pnd_A));                                                                  //Natural: ASSIGN CIS-UPDT.CIS-SG-FUND-TICKER ( #I ) := #ACCT-TCKR ( #A )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_cis_Updt.updateDBRow("FIND01");                                                                                                                            //Natural: UPDATE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  REA0614 END
        pnd_Count_Pnd_Rec_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-CNT
    }
    private void sub_Write_Complete_Report() throws Exception                                                                                                             //Natural: WRITE-COMPLETE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Count_Pnd_Comp_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #COMP-CNT
        //*  012009 START
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* *WRITE(1) IAXFR-CALC-UNIQUE-ID
        //* *  IAXFR-FROM-PPCN-NBR
        //* *  IAXFR-FROM-PAYEE-CDE
        //* *  IAXFR-CALC-TO-PPCN-NBR
        //* *  IAXFR-CALC-TO-PAYEE-CDE
        //* *  #COMP-REASON
        //* *RESET  #COMP-REASON                                 /* 012009 END
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_From_Cref.equals("Y")))                                                                                                                         //Natural: IF #FROM-CREF = 'Y'
        {
            print_Line_Ppcn.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Nbr());                                                                                      //Natural: ASSIGN PPCN := CIS-TIAA-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            print_Line_Ppcn.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Cert_Nbr());                                                                                      //Natural: ASSIGN PPCN := CIS-CERT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        print_Line_Payee_Cd.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                                                 //Natural: ASSIGN PAYEE-CD := IAXFR-FROM-PAYEE-CDE
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().greater(getZero())))                                                                         //Natural: IF CIS-GRNTED-PERIOD-YRS GT 0
        {
            print_Line_Cis_Gr_Years.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs());                                                                     //Natural: ASSIGN CIS-GR-YEARS := CIS-GRNTED-PERIOD-YRS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys().greater(getZero())))                                                                         //Natural: IF CIS-GRNTED-PERIOD-DYS GT 0
        {
            print_Line_Cis_Gr_Days.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys());                                                                      //Natural: ASSIGN CIS-GR-DAYS := CIS-GRNTED-PERIOD-DYS
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  052209
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(" ")))                                                                                    //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = ' '
            {
                //*  052209
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  052209
            }                                                                                                                                                             //Natural: END-IF
            print_Line_Cis_Option.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option());                                                                            //Natural: ASSIGN CIS-OPTION := CIS-ANNTY-OPTION
            if (condition(! (iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("T") || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("G"))))                  //Natural: IF NOT IAXFR-TO-ACCT-CD ( #I ) = 'T' OR = 'G'
            {
                if (condition(pnd_Iaxfr_Units_A.getValue(pnd_I).greater(getZero())))                                                                                      //Natural: IF #IAXFR-UNITS-A ( #I ) GT 0
                {
                    print_Line_Ia_Units.setValue(pnd_Iaxfr_Units_A.getValue(pnd_I));                                                                                      //Natural: ASSIGN IA-UNITS := #IAXFR-UNITS-A ( #I )
                    print_Line_Fund.setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I));                                                                             //Natural: ASSIGN FUND := IAXFR-TO-ACCT-CD ( #I )
                    print_Line_Mode_Code.setValue("A");                                                                                                                   //Natural: ASSIGN MODE-CODE := 'A'
                    print_Line_Cis_Units.setValue(pnd_Cis_Unit_A.getValue(pnd_I));                                                                                        //Natural: ASSIGN CIS-UNITS := #CIS-UNIT-A ( #I )
                    getReports().write(1, ReportOption.NOTITLE,print_Line_Ppcn,print_Line_Filler,print_Line_Payee_Cd,print_Line_Fillr,print_Line_Mode_Code,               //Natural: WRITE ( 1 ) PRINT-LINE
                        print_Line_Filler1,print_Line_Fund,print_Line_Filler2,print_Line_Ia_Units,print_Line_Filler3,print_Line_Cis_Units,print_Line_Filler4,
                        print_Line_Cis_Option,print_Line_Filler5,print_Line_Cis_Gr_Years,print_Line_Filler6,print_Line_Cis_Gr_Days);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    print_Line_Ia_Units.reset();                                                                                                                          //Natural: RESET IA-UNITS FUND MODE-CODE CIS-UNITS
                    print_Line_Fund.reset();
                    print_Line_Mode_Code.reset();
                    print_Line_Cis_Units.reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Iaxfr_Units_M.getValue(pnd_I).greater(getZero())))                                                                                      //Natural: IF #IAXFR-UNITS-M ( #I ) GT 0
                {
                    print_Line_Fund.setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I));                                                                             //Natural: ASSIGN FUND := IAXFR-TO-ACCT-CD ( #I )
                    print_Line_Ia_Units.setValue(pnd_Iaxfr_Units_M.getValue(pnd_I));                                                                                      //Natural: ASSIGN IA-UNITS := #IAXFR-UNITS-M ( #I )
                    print_Line_Mode_Code.setValue("M");                                                                                                                   //Natural: ASSIGN MODE-CODE := 'M'
                    print_Line_Cis_Units.setValue(pnd_Cis_Unit_M.getValue(pnd_I));                                                                                        //Natural: ASSIGN CIS-UNITS := #CIS-UNIT-M ( #I )
                    getReports().write(1, ReportOption.NOTITLE,print_Line_Ppcn,print_Line_Filler,print_Line_Payee_Cd,print_Line_Fillr,print_Line_Mode_Code,               //Natural: WRITE ( 1 ) PRINT-LINE
                        print_Line_Filler1,print_Line_Fund,print_Line_Filler2,print_Line_Ia_Units,print_Line_Filler3,print_Line_Cis_Units,print_Line_Filler4,
                        print_Line_Cis_Option,print_Line_Filler5,print_Line_Cis_Gr_Years,print_Line_Filler6,print_Line_Cis_Gr_Days);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    print_Line_Ia_Units.reset();                                                                                                                          //Natural: RESET IA-UNITS FUND MODE-CODE CIS-UNITS
                    print_Line_Fund.reset();
                    print_Line_Mode_Code.reset();
                    print_Line_Cis_Units.reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                print_Line_Fund.setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I));                                                                                 //Natural: ASSIGN FUND := IAXFR-TO-ACCT-CD ( #I )
                getReports().write(1, ReportOption.NOTITLE,print_Line_Ppcn,print_Line_Filler,print_Line_Payee_Cd,print_Line_Fillr,print_Line_Mode_Code,                   //Natural: WRITE ( 1 ) PRINT-LINE
                    print_Line_Filler1,print_Line_Fund,print_Line_Filler2,print_Line_Ia_Units,print_Line_Filler3,print_Line_Cis_Units,print_Line_Filler4,
                    print_Line_Cis_Option,print_Line_Filler5,print_Line_Cis_Gr_Years,print_Line_Filler6,print_Line_Cis_Gr_Days);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                print_Line_Ia_Units.reset();                                                                                                                              //Natural: RESET IA-UNITS FUND MODE-CODE CIS-UNITS
                print_Line_Fund.reset();
                print_Line_Mode_Code.reset();
                print_Line_Cis_Units.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        print_Line.reset();                                                                                                                                               //Natural: RESET PRINT-LINE #IAXFR-UNITS-A ( * ) #IAXFR-UNITS-M ( * ) #CIS-UNIT-A ( * ) #CIS-UNIT-M ( * )
        pnd_Iaxfr_Units_A.getValue("*").reset();
        pnd_Iaxfr_Units_M.getValue("*").reset();
        pnd_Cis_Unit_A.getValue("*").reset();
        pnd_Cis_Unit_M.getValue("*").reset();
        //*  WRITE-REPORT
    }
    private void sub_Pnd_Write_Error_Report() throws Exception                                                                                                            //Natural: #WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Count_Pnd_Err_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERR-CNT
        //* *WRITE(2) IAXFR-CALC-UNIQUE-ID                  /* 012009 START
        //* *  IAXFR-FROM-PPCN-NBR
        //* *  IAXFR-FROM-PAYEE-CDE
        //* *  IAXFR-CALC-TO-PPCN-NBR
        //* *  IAXFR-CALC-TO-PAYEE-CDE
        //* *  #ERR-REASON
        //*  012009 END
        getReports().write(2, ReportOption.NOTITLE,iaa_Trnsfr_Sw_Ia_Unique_Id,iaa_Trnsfr_Sw_Ia_Frm_Cntrct,iaa_Trnsfr_Sw_Ia_Frm_Payee," ",iaa_Trnsfr_Sw_Ia_To_Cntrct,      //Natural: WRITE ( 2 ) IAA-TRNSFR-SW.IA-UNIQUE-ID IAA-TRNSFR-SW.IA-FRM-CNTRCT IAA-TRNSFR-SW.IA-FRM-PAYEE ' ' IAA-TRNSFR-SW.IA-TO-CNTRCT IAA-TRNSFR-SW.IA-TO-PAYEE ' ' #ERR-REASON
            iaa_Trnsfr_Sw_Ia_To_Payee," ",pnd_Err_Reason);
        if (Global.isEscape()) return;
        pnd_Err_Reason.reset();                                                                                                                                           //Natural: RESET #ERR-REASON
        //*  #WRITE-ERROR-REPORT
    }
    private void sub_Write_Report_Totals() throws Exception                                                                                                               //Natural: WRITE-REPORT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, ReportOption.NOTITLE,"Total number of records complete ...",pnd_Count_Pnd_Comp_Cnt);                                                        //Natural: WRITE ( 2 ) 'Total number of records complete ...' #COMP-CNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total number of errors .............",pnd_Count_Pnd_Err_Cnt);                                                         //Natural: WRITE ( 2 ) 'Total number of errors .............' #ERR-CNT
        if (Global.isEscape()) return;
        //*  SKIP (1) 2
        //*  WRITE (1) 'TOTAL NUMBER OF RECORDS COMPLETE....' #COMP-CNT
        //*  WRITE (1) 'TOTAL NUMBER OF ERRORS .............' #ERR-CNT
        //*  WRITE-ERROR-TOTALS
    }
    //*  10/2014
    private void sub_Process_Cor_Payee_02() throws Exception                                                                                                              //Natural: PROCESS-COR-PAYEE-02
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(2);                                                                                                         //Natural: ASSIGN #I-PAYEE-CODE := 02
                                                                                                                                                                          //Natural: PERFORM READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        sub_Read_Cor_Xref_Cntrct_For_Unique_Id();
        if (condition(Global.isEscape())) {return;}
        pnd_Reason2.reset();                                                                                                                                              //Natural: RESET #REASON2
        if (condition(pnd_Cor_Unique_Id.equals(getZero())))                                                                                                               //Natural: IF #COR-UNIQUE-ID EQ 0
        {
            //*  RESET #CIS-SCND-ANNT-SSN                            /* 012009 START
            //*    #CIS-SCND-ANNT-PRFX
            //*    #CIS-SCND-ANNT-FRST-NME
            //*    #CIS-SCND-ANNT-MID-NME
            //*    #CIS-SCND-ANNT-LST-NME
            //*    #CIS-SCND-ANNT-SFFX
            //*    #CIS-SCND-ANNT-DOB
            //*  012009 END
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();                                                                                                   //Natural: RESET CIS-SCND-ANNT-SSN CIS-SCND-ANNT-PRFX CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
            pnd_Reason2.setValue("DATA NOT AVAILABLE");                                                                                                                   //Natural: ASSIGN #REASON2 := 'DATA NOT AVAILABLE'
            pnd_Err_Reason.setValue("COULD NOT FIND COR UNIQUE ID. (PAYEE 02)");                                                                                          //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND COR UNIQUE ID. (PAYEE 02)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            //*  PERFORM PROCESS-ERROR
            //*  082017
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Cor_Unique_Id);                                                                                        //Natural: ASSIGN #I-PIN-N12 := #COR-UNIQUE-ID
            //*  IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 2
            //*    #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 1
            //*  ELSE IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 4
            //*      #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 3
            //*    END-IF
            //*  END-IF
            //*  10/2014 - END
            //*  #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE   := 03 /* 2ND ANNUITANT
                                                                                                                                                                          //Natural: PERFORM GET-COR-XREF-PH
            sub_Get_Cor_Xref_Ph();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Found.notEquals("Y")))                                                                                                                      //Natural: IF #FOUND NOT = 'Y'
            {
                //*    RESET #CIS-SCND-ANNT-SSN                          /* 012009 START
                //*      #CIS-SCND-ANNT-PRFX
                //*      #CIS-SCND-ANNT-FRST-NME
                //*      #CIS-SCND-ANNT-MID-NME
                //*      #CIS-SCND-ANNT-LST-NME
                //*      #CIS-SCND-ANNT-SFFX
                //*      #CIS-SCND-ANNT-DOB
                //*  012009 END
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();                                                                                               //Natural: RESET CIS-SCND-ANNT-SSN CIS-SCND-ANNT-PRFX CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
                //*  082017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Ssn());                                                       //Natural: ASSIGN CIS-SCND-ANNT-SSN := #MDMA101.#O-SSN
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix());                                                   //Natural: ASSIGN CIS-SCND-ANNT-PRFX := #O-PREFIX
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                           //Natural: ASSIGN CIS-SCND-ANNT-FRST-NME := #O-FIRST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                           //Natural: ASSIGN CIS-SCND-ANNT-MID-NME := #O-MIDDLE-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                             //Natural: ASSIGN CIS-SCND-ANNT-LST-NME := #O-LAST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix());                                                   //Natural: ASSIGN CIS-SCND-ANNT-SFFX := #O-SUFFIX
                //*    IF   PH-DOD-DTE > 0                               /* 012009
                //*  012009
                //*  012009
                //*  012009
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero()) && iaa_Xfr_Audit_Iaxfr_From_Payee_Cde.greater(1)))                       //Natural: IF #O-DATE-OF-DEATH > 0 AND IAXFR-FROM-PAYEE-CDE GT 01
                {
                    pnd_Dod_2_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #DOD-2-SW := 'Y'
                    //*  012009
                }                                                                                                                                                         //Natural: END-IF
                pnd_Date_A8_Pnd_Date_N8.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                        //Natural: ASSIGN #DATE-N8 := #O-DATE-OF-BIRTH
                //*  012009
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-SCND-ANNT-DOB ( EM = YYYYMMDD )
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Gender_Code());                                           //Natural: ASSIGN CIS-SCND-ANNT-SEX-CDE := #O-GENDER-CODE
                //*  10/2014 - END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-COR-PAYEE-02
    }
    private void sub_Read_Cor_Xref_Cntrct_For_Unique_Id() throws Exception                                                                                                //Natural: READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  07/2018
        //*  10/2014
        pnd_Cor_Unique_Id.reset();                                                                                                                                        //Natural: RESET #COR-UNIQUE-ID
        pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                    //Natural: ASSIGN #I-TIAA-CREF-IND := 'T'
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                    //Natural: ASSIGN #I-CONTRACT-NUMBER := IAXFR-FROM-PPCN-NBR
        //* *LLNAT 'MDMN210A' #MDMA210                      /* 082017 START
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //* * #MDMA210.#O-RETURN-CODE EQ '0000'
        //*  082017 END
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
        {
            pnd_Cor_Unique_Id.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12());                                                                                        //Natural: ASSIGN #COR-UNIQUE-ID := #MDMA211.#O-PIN-N12
            //*  07/2018
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  07/2018
                                                                                                                                                                          //Natural: PERFORM WRITE-PARM-VALUES
            sub_Write_Parm_Values();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-NBR       := IAXFR-FROM-PPCN-NBR
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#PH-UNIQUE-ID-NBR := 0
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#PH-RCD-TYPE-CDE  := 0
        //* *READ(1) COR-XREF-CNTRCT BY COR-SUPER-CNTRCT-PAYEE-PIN
        //*     STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
        //*  #COR-UNIQUE-ID := COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*   IF CNTRCT-PAYEE-CDE EQ #CNTRCT-PAYEE-CDE AND
        //*       CNTRCT-NBR EQ #CNTRCT-NBR
        //*     #COR-UNIQUE-ID := COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*   ELSE
        //*     #COR-UNIQUE-ID := 0
        //*   END-IF
        //* *END-READ
        //*  10/2014 - END
        //*  READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
    }
    private void sub_Get_Cor_Xref_Ph() throws Exception                                                                                                                   //Natural: GET-COR-XREF-PH
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Found.setValue("Y");                                                                                                                                          //Natural: ASSIGN #FOUND := 'Y'
        //*  10/2014 - START
        //* *FIND(1) COR-XREF-PH
        //*     WITH  COR-SUPER-PIN-RCDTYPE = #COR-SUPER-PIN-RCDTYPE
        //*   IF NO RECORD FOUND
        //*     #ERR-REASON := 'Could not find COR unique id. (COR-XREF-PH)'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     RESET #FOUND
        //*    PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *LLNAT 'MDMN100A' #MDMA100                      /* 082017 START
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //* * #MDMA100.#O-RETURN-CODE EQ '0000'
        //*  082017 END
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Err_Reason.setValue("Could not find COR unique id. (COR-XREF-PH)");                                                                                       //Natural: ASSIGN #ERR-REASON := 'Could not find COR unique id. (COR-XREF-PH)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Found.reset();                                                                                                                                            //Natural: RESET #FOUND
        }                                                                                                                                                                 //Natural: END-IF
        //*  10/2014 - END
        //*  GET-COR-XREF-PH
    }
    private void sub_Process_Iaa_Cntrct() throws Exception                                                                                                                //Natural: PROCESS-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr.equals(" ")))                                                                                                  //Natural: IF IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR = ' '
        {
            pnd_Audit_Cntrct.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #AUDIT-CNTRCT := IAA-XFR-AUDIT.IAXFR-FROM-PPCN-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Audit_Cntrct.setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                                                              //Natural: ASSIGN #AUDIT-CNTRCT := IAA-XFR-AUDIT.IAXFR-CALC-TO-PPCN-NBR
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #AUDIT-CNTRCT
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Audit_Cntrct, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND02", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("Could not find IAA-CNTRCT.");                                                                                                    //Natural: ASSIGN #ERR-REASON := 'Could not find IAA-CNTRCT.'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(DbsUtil.maskMatches(ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde(),"NNN") && ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().greater("000")  //Natural: IF CNTRCT-RSDNCY-AT-ISSUE-CDE = MASK ( NNN ) AND ( CNTRCT-RSDNCY-AT-ISSUE-CDE GT '000' AND CNTRCT-RSDNCY-AT-ISSUE-CDE LT '070' )
                && ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().less("070")))
            {
                pnd_Numeric_Ste.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().getSubstring(2,2));                                                       //Natural: ASSIGN #NUMERIC-STE := SUBSTRING ( CNTRCT-RSDNCY-AT-ISSUE-CDE,2,2 )
                pnd_Alpha_Ste.reset();                                                                                                                                    //Natural: RESET #ALPHA-STE
                //*  CONVERT NUMERIC RSDNCY CDE TO ALPHA RSDNCY CDE
                DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Alpha_Ste, pnd_Numeric_Ste);                                 //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
                if (condition(Global.isEscape())) return;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Orig_Issue_State().setValue(pnd_Alpha_Ste);                                                                            //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := #ALPHA-STE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Orig_Issue_State().setValue("NY");                                                                                     //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := 'NY'
            }                                                                                                                                                             //Natural: END-IF
            //*  FILL THE CIS-ANNTY-OPTION CODE
            //*  012009 END
            short decideConditionsMet2975 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE OF IAA-CNTRCT.CNTRCT-OPTN-CDE;//Natural: VALUE 01, 05, 06, 09
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9))))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("OL");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'OL'
            }                                                                                                                                                             //Natural: VALUE 04, 11, 14, 16
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16))))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LSF");                                                                                        //Natural: ASSIGN CIS-ANNTY-OPTION := 'LSF'
            }                                                                                                                                                             //Natural: VALUE 07, 08, 10, 13
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13))))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("JS");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'JS'
            }                                                                                                                                                             //Natural: VALUE 03, 12, 15, 17
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17))))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LS");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'LS'
            }                                                                                                                                                             //Natural: VALUE 21
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("AC");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'AC'
            }                                                                                                                                                             //Natural: VALUE 54:57
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))
            {
                decideConditionsMet2975++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LST");                                                                                        //Natural: ASSIGN CIS-ANNTY-OPTION := 'LST'
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet2975 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* * THE FOLLOWING TAKEN FROM IATP408                  /* 012009 START
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF CNTRCT-ISSUE-DTE-DD = 0
            {
                cntrct_Issue_Dte_Dd_1.setValue("01");                                                                                                                     //Natural: ASSIGN CNTRCT-ISSUE-DTE-DD-1 := '01'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cntrct_Issue_Dte_Dd_1.setValueEdited(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd(),new ReportEditMask("99"));                                           //Natural: MOVE EDITED CNTRCT-ISSUE-DTE-DD ( EM = 99 ) TO CNTRCT-ISSUE-DTE-DD-1
            }                                                                                                                                                             //Natural: END-IF
            cntrct_Issue_Dte_D.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte(), cntrct_Issue_Dte_Dd_1));            //Natural: COMPRESS CNTRCT-ISSUE-DTE CNTRCT-ISSUE-DTE-DD-1 INTO CNTRCT-ISSUE-DTE-D LEAVING NO
            short decideConditionsMet2999 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-OPTN-CDE = 01 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 01 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 02
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(2)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("IR00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'IR00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 05 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 06 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 09 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 05 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 06 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 09 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 03 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 04 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 07 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 03 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 04 AND CNTRCT-ISSUE-DTE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4) && ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte().less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 07 AND CNTRCT-ISSUE-DTE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) && ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte().less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 08 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 10 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 13 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 08 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 10 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 13 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 11 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 14 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 16 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 11 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 14 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 16 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 12 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 15 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 17 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 12 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 15 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 17 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 54
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(54)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 55
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(55)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 56
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(56)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 57
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(57)))
            {
                decideConditionsMet2999++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT00'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
                //*  012009 END
                //*  ORIGINAL ISSUE
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                               //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(1);                                                                                                          //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                            //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_A8.setValue(pnd_Issue_Date_A8);                                                                                                                      //Natural: ASSIGN #DATE-A8 := #ISSUE-DATE-A8
            short decideConditionsMet3087 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF IAA-CNTRCT.CNTRCT-OPTN-CDE;//Natural: VALUE 05, 08, 16, 17
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17))))
            {
                decideConditionsMet3087++;
                pnd_Date_A8_Pnd_Date_Yyyy.nadd(10);                                                                                                                       //Natural: ADD 10 TO #DATE-YYYY
            }                                                                                                                                                             //Natural: VALUE 09, 13, 14, 15
            else if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15))))
            {
                decideConditionsMet3087++;
                pnd_Date_A8_Pnd_Date_Yyyy.nadd(15);                                                                                                                       //Natural: ADD 15 TO #DATE-YYYY
            }                                                                                                                                                             //Natural: VALUE 06, 10, 11, 12
            else if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12))))
            {
                decideConditionsMet3087++;
                pnd_Date_A8_Pnd_Date_Yyyy.nadd(20);                                                                                                                       //Natural: ADD 20 TO #DATE-YYYY
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  USE THE NEW TRANSFER ISSUE DATE (EFFECTIVE DATE + 1 )
            //*  WRITE '='   CIS-ANNTY-START-DTE
            pnd_Issue_Date_A8.setValueEdited(ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #ISSUE-DATE-A8
            //*  ISSUE DTE < PERIOD END DTE
            if (condition(pnd_Issue_Date_A8_Pnd_Issue_Date_N8.less(pnd_Date_A8_Pnd_Date_N8)))                                                                             //Natural: IF #ISSUE-DATE-N8 LT #DATE-N8
            {
                //*  CALC THE NUMBER OF YEARS
                //*      WRITE '=' #DATE-MM  '='  #ISSUE-DATE-MM
                //*            '=' #DATE-YYYY '=' #ISSUE-DATE-YYYY
                if (condition(pnd_Date_A8_Pnd_Date_Mm.less(pnd_Issue_Date_A8_Pnd_Issue_Date_Mm)))                                                                         //Natural: IF #DATE-MM LT #ISSUE-DATE-MM
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = ( #DATE-YYYY - 1 ) - #ISSUE-DATE-YYYY
                        (pnd_Date_A8_Pnd_Date_Yyyy.subtract(1)).subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = #DATE-YYYY - #ISSUE-DATE-YYYY
                        pnd_Date_A8_Pnd_Date_Yyyy.subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy.nadd(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs());                                                       //Natural: ADD CIS-GRNTED-PERIOD-YRS TO #ISSUE-DATE-YYYY
                //*  ISS DTE
                pnd_Issue_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Issue_Date_A8);                                                                        //Natural: MOVE EDITED #ISSUE-DATE-A8 TO #ISSUE-DATE-D ( EM = YYYYMMDD )
                //*  GRNTD END DTE
                pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                                                                    //Natural: MOVE EDITED #DATE-A8 TO #DATE-D ( EM = YYYYMMDD )
                //*  CALC THE NUMBER OF DAYS
                ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys()),      //Natural: COMPUTE CIS-GRNTED-PERIOD-DYS = #DATE-D - #ISSUE-DATE-D
                    pnd_Date_D.subtract(pnd_Issue_Date_D));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = IAXFR-FROM-PPCN-NBR
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND03", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("Could not find IAA-CNTRCT.");                                                                                                    //Natural: ASSIGN #ERR-REASON := 'Could not find IAA-CNTRCT.'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  ORIGINAL ISSUE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                               //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(1);                                                                                                          //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                            //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            //*  CALCULATE THE GUARANTEED PERIOD END (ORIG ISSUE DTE + GRNTD YEARS)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-IAA-CNTRCT
    }
    private void sub_Process_Prtcpnt_Role() throws Exception                                                                                                              //Natural: PROCESS-PRTCPNT-ROLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  012009 START
        //*  012009 END
        pnd_Iaxfr_Units_A.getValue("*").reset();                                                                                                                          //Natural: RESET #IAXFR-UNITS-A ( * ) #IAXFR-UNITS-M ( * ) #CIS-UNIT-A ( * ) #CIS-UNIT-M ( * )
        pnd_Iaxfr_Units_M.getValue("*").reset();
        pnd_Cis_Unit_A.getValue("*").reset();
        pnd_Cis_Unit_M.getValue("*").reset();
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                                     //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-CALC-TO-PPCN-NBR
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde);                                                                   //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-CALC-TO-PAYEE-CDE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND04",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND04:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND04", true)))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("Could not find IAA-CNTRCT-PRTCPNT-ROLE");                                                                                        //Natural: ASSIGN #ERR-REASON := 'Could not find IAA-CNTRCT-PRTCPNT-ROLE'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012009
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            //*  012009 START
            if (condition((DbsUtil.maskMatches(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde(),"NNN") && (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().greater("000")  //Natural: IF PRTCPNT-RSDNCY-CDE = MASK ( NNN ) AND ( PRTCPNT-RSDNCY-CDE GT '000' OR PRTCPNT-RSDNCY-CDE LT '070' )
                || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().less("070")))))
            {
                pnd_Numeric_Ste.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().getSubstring(2,2));                                                  //Natural: ASSIGN #NUMERIC-STE := SUBSTRING ( PRTCPNT-RSDNCY-CDE,2,2 )
                pnd_Alpha_Ste.reset();                                                                                                                                    //Natural: RESET #ALPHA-STE
                //*  CONVERT NUMERIC RSDNCY CDE TO ALPHA RSDNCY CDE
                DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Alpha_Ste, pnd_Numeric_Ste);                                 //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
                if (condition(Global.isEscape())) return;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Alpha_Ste);                                                                         //Natural: ASSIGN CIS-FRST-ANNT-RSDNC-CDE := #ALPHA-STE
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue(pnd_Alpha_Ste);                                                                              //Natural: ASSIGN CIS-ISSUE-STATE-CD := #ALPHA-STE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue("NY");                                                                                  //Natural: ASSIGN CIS-FRST-ANNT-RSDNC-CDE := 'NY'
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue("NY");                                                                                       //Natural: ASSIGN CIS-ISSUE-STATE-CD := 'NY'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().greater("58")))                                                                     //Natural: IF PRTCPNT-RSDNCY-CDE GT '58'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue("NY");                                                                                       //Natural: ASSIGN CIS-ISSUE-STATE-CD := 'NY'
                //*  012009 END
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet3191 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PRTCPNT-CTZNSHP-CDE;//Natural: VALUE 1
            if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().equals(1))))
            {
                decideConditionsMet3191++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue("US");                                                                                 //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := 'US'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().equals(2))))
            {
                decideConditionsMet3191++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue("CA");                                                                                 //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := 'CA'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde());                             //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := CIS-FRST-ANNT-RSDNC-CDE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  FILL MODE
            //*  012009
            //*  012009
            //*  012009
            //*  012009
            short decideConditionsMet3206 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-MODE-IND = 100
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100)))
            {
                decideConditionsMet3206++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("M");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'M'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 701 AND CNTRCT-MODE-IND LE 706
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(701) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(706)))
            {
                decideConditionsMet3206++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("S");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'S'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 601 AND CNTRCT-MODE-IND LE 603
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(601) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(603)))
            {
                decideConditionsMet3206++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("Q");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'Q'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 801 AND CNTRCT-MODE-IND LE 812
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(801) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(812)))
            {
                decideConditionsMet3206++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("A");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'A'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  012009 START
            //*  THE FOLLOWING TAKEN FROM IATP408. I'm not sure about the
            //*  ACCURACY.
            short decideConditionsMet3221 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT.CNTRCT-OPTN-CDE = 03 OR = 12 OR = 15 OR = 17 AND CNTRCT-FIRST-ANNT-DOD-DTE > 0
            if (condition(((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17)) && ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()))))
            {
                decideConditionsMet3221++;
                FOR09:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                           //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(2)); //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 2
                        pnd_Iaxfr_Units_A.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(2)); //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 2
                        pnd_Iaxfr_Units_M.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 07 OR = 08 OR = 10 OR = 13 ) AND ( CNTRCT-FIRST-ANNT-DOD-DTE > 0 OR CNTRCT-SCND-ANNT-DOD-DTE > 0 )
            else if (condition(((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || 
                ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13)) && (ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greater(getZero())))))
            {
                decideConditionsMet3221++;
                FOR10:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                           //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.5
                            DbsDecimal("1.5")));
                        pnd_Iaxfr_Units_A.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.5
                            DbsDecimal("1.5")));
                        pnd_Iaxfr_Units_M.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN IAA-CNTRCT.CNTRCT-OPTN-CDE = 54 THRU 57 AND CNTRCT-FIRST-ANNT-DOD-DTE > 0
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57) 
                && ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero())))
            {
                decideConditionsMet3221++;
                FOR11:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                           //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.333333
                            DbsDecimal("1.333333")));
                        pnd_Iaxfr_Units_A.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.333333
                            DbsDecimal("1.333333")));
                        pnd_Iaxfr_Units_M.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                FOR12:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                           //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                   //Natural: ASSIGN #CIS-UNIT-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                        pnd_Iaxfr_Units_A.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                   //Natural: ASSIGN #CIS-UNIT-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                        pnd_Iaxfr_Units_M.getValue(pnd_I).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                //Natural: ASSIGN #IAXFR-UNITS-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ************
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue("*").reset();                                                                             //Natural: RESET CIS-CREF-ANNUAL-NBR-UNITS ( * ) CIS-CREF-MNTHLY-NBR-UNITS ( * ) CIS-REA-ANNUAL-NBR-UNITS CIS-REA-MNTHLY-NBR-UNITS CIS-TACC-ANNUAL-NBR-UNITS ( * ) CIS-TACC-MNTHLY-NBR-UNITS ( * ) CIS-CREF-ACCT-CDE ( * ) #K CIS-CREF-ANNTY-AMT CIS-REA-ANNTY-AMT CIS-TACC-ANNTY-AMT
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units().getValue("*").reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units().getValue("*").reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units().getValue("*").reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue("*").reset();
            pnd_K.reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annty_Amt().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Annty_Amt().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Annty_Amt().reset();
            FOR13:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(" ")))                                                                                //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Tiaa_Type.getValue("*")))))                                                    //Natural: IF NOT IAXFR-TO-ACCT-CD ( #I ) = #TIAA-TYPE ( * )
                {
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I));                          //Natural: ASSIGN CIS-CREF-ACCT-CDE ( #K ) := IAXFR-TO-ACCT-CD ( #I )
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                           //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-CREF-ANNUAL-NBR-UNITS ( #K )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-CREF-MNTHLY-NBR-UNITS ( #K )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annty_Amt().nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                                           //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-CREF-ANNTY-AMT
                    //*  ACCORDING TO KRIS, THE ACCUM AMOUNT SHOULD BE STORED IN THE
                    //*  PROCEEDS AMOUNT
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_K).nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                     //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-DA-CREF-PROCEEDS-AMT ( #K )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    short decideConditionsMet3282 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAXFR-TO-ACCT-CD ( #I ) = 'R'
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("R")))
                    {
                        decideConditionsMet3282++;
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                       //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Annual_Nbr_Units().nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                               //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-REA-ANNUAL-NBR-UNITS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Mnthly_Nbr_Units().nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                               //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-REA-MNTHLY-NBR-UNITS
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Annty_Amt().nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                                        //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-REA-ANNTY-AMT
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt().getValue(1).nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                      //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-DA-REA-PROCEEDS-AMT ( 1 )
                    }                                                                                                                                                     //Natural: WHEN IAXFR-TO-ACCT-CD ( #I ) = 'D'
                    else if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("D")))
                    {
                        decideConditionsMet3282++;
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Ind().getValue(1).setValue("A");                                                                          //Natural: ASSIGN CIS-TACC-IND ( 1 ) := 'A'
                        DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I)),       //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR IAXFR-TO-ACCT-CD ( #I ) GIVING INDEX #A
                            new ExamineGivingIndex(pnd_A));
                        if (condition(pnd_A.greater(getZero())))                                                                                                          //Natural: IF #A GT 0
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Account().getValue(1).setValue(pnd_Acct_Tckr.getValue(pnd_A));                                        //Natural: ASSIGN CIS-TACC-ACCOUNT ( 1 ) := #ACCT-TCKR ( #A )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Account().getValue(1).setValue("WA51#");                                                              //Natural: ASSIGN CIS-TACC-ACCOUNT ( 1 ) := 'WA51#'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                       //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Annual_Nbr_Units().getValue(1).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-TACC-ANNUAL-NBR-UNITS ( 1 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Mnthly_Nbr_Units().getValue(1).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-TACC-MNTHLY-NBR-UNITS ( 1 )
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Tacc_Annty_Amt().nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                                       //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-TACC-ANNTY-AMT
                        //*  ACCORDING TO KRIS, THE ACCUM AMOUNT FOR ACCESS SHOULD BE STORED
                        //*  IN THE REA PROCEEDS AMT
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Rea_Proceeds_Amt().getValue(1).nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                      //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-DA-REA-PROCEEDS-AMT ( 1 )
                    }                                                                                                                                                     //Natural: WHEN IAXFR-TO-ACCT-CD ( #I ) = 'T' OR = 'G'
                    else if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("T") || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("G")))
                    {
                        decideConditionsMet3282++;
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt().getValue(1).nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                     //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO CIS-DA-TIAA-PROCEEDS-AMT ( 1 )
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  012009 END
            //*  FILL END DATE
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().notEquals(getZero())))                                                        //Natural: IF CNTRCT-FINAL-PER-PAY-DTE NE 0
            {
                pnd_Date_A8_Pnd_Date_Yyyymm.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                                                  //Natural: ASSIGN #DATE-YYYYMM := CNTRCT-FINAL-PER-PAY-DTE
                pnd_Date_A8_Pnd_Date_Dd.setValue(1);                                                                                                                      //Natural: ASSIGN #DATE-DD := 01
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_End_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-ANNTY-END-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-PRTCPNT-ROLE
    }
    private void sub_Fill_Address_Info() throws Exception                                                                                                                 //Natural: FILL-ADDRESS-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                    //Natural: ASSIGN #I-CONTRACT-NUMBER := IAXFR-FROM-PPCN-NBR
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                        //Natural: ASSIGN #I-PAYEE-CODE := IAXFR-FROM-PAYEE-CDE
        //* *LLNAT 'MDMN210A' #MDMA210                      /* 082017 START
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //* * #MDMA210.#O-RETURN-CODE EQ '0000'
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
        {
            //* *RESET #MDMA100
            pdaMdma101.getPnd_Mdma101().reset();                                                                                                                          //Natural: RESET #MDMA101
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12());                                                                //Natural: ASSIGN #I-PIN-N12 := #MDMA211.#O-PIN-N12
            //* *CALLNAT 'MDMN100A' #MDMA100
            //*  082017 END
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Err_Reason.setValue("Could not find NAS-NAME-ADDRESS for correspondence");                                                                                //Natural: ASSIGN #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for correspondence'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Address_Array.setValue(1);                                                                                                                                    //Natural: ASSIGN #ADDRESS-ARRAY := 1
        //* *PERFORM GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* *IF NAS-NAME-ADDRESS.PERMANENT-ADDRSS-IND = 'P'  /* CORRESPOND PERM
        //*   RESET #VACATION-ADDRESS
        //* *ELSE
        //*   PERFORM GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //*   #VACATION-ADDRESS  := TRUE
        //* *END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        sub_Fill_One_Occur_Of_Address_Array();
        if (condition(Global.isEscape())) {return;}
        pnd_Address_Array.setValue(2);                                                                                                                                    //Natural: ASSIGN #ADDRESS-ARRAY := 2
        //* *PERFORM GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* *IF NAS-NAME-ADDRESS.PERMANENT-ADDRSS-IND = 'P'  /* CHECK MAIL PERM
        //*   RESET #VACATION-ADDRESS
        //* *ELSE
        //*   PERFORM GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //*   #VACATION-ADDRESS  := TRUE
        //* *END-IF
        //*  10/2014 - END
                                                                                                                                                                          //Natural: PERFORM FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        sub_Fill_One_Occur_Of_Address_Array();
        if (condition(Global.isEscape())) {return;}
        //*  FILL-ADDRESS-INFO
    }
    private void sub_Get_Cis_Rec() throws Exception                                                                                                                       //Natural: GET-CIS-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cis_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #CIS-FOUND := FALSE
        ldaIatl406.getVw_cis_Prtcpnt_File().startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) CIS-PRTCPNT-FILE WITH CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY = IAA-XFR-AUDIT.RQST-ID
        (
        "CIS",
        new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", iaa_Xfr_Audit_Rqst_Id, WcType.WITH) },
        1
        );
        CIS:
        while (condition(ldaIatl406.getVw_cis_Prtcpnt_File().readNextRow("CIS", true)))
        {
            ldaIatl406.getVw_cis_Prtcpnt_File().setIfNotFoundControlFlag(false);
            if (condition(ldaIatl406.getVw_cis_Prtcpnt_File().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("COULD NOT FIND CIS REC");                                                                                                        //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND CIS REC'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Cis_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #CIS-FOUND := TRUE
            pnd_Isn.setValue(ldaIatl406.getVw_cis_Prtcpnt_File().getAstISN("CIS"));                                                                                       //Natural: ASSIGN #ISN := *ISN ( CIS. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Fill_One_Occur_Of_Address_Array() throws Exception                                                                                                   //Natural: FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Address_Array.less(1) || pnd_Address_Array.greater(3)))                                                                                         //Natural: IF #ADDRESS-ARRAY LT 1 OR #ADDRESS-ARRAY GT 3
        {
            pnd_Err_Reason.setValue(DbsUtil.compress(" Address array = ", pnd_Address_Array, "it mus be 1,2 or 3."));                                                     //Natural: COMPRESS ' Address array = ' #ADDRESS-ARRAY 'it mus be 1,2 or 3.' INTO #ERR-REASON
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            //*  012009
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.setValue(pnd_Address_Array);                                                                                                                                //Natural: ASSIGN #I := #ADDRESS-ARRAY
        //*  10/2014 - START
        //* *IF  #VACATION-ADDRESS
        //*  CORRESPONDENCE
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),                 //Natural: COMPRESS #O-LAST-NAME #O-PREFIX #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO CIS-ADDRESS-DEST-NAME ( #I )
                pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), 
                pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,1).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_1());                             //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,1 ) := #O-CO-ADDRESS-LINE-1
            pnd_K.setValue(2);                                                                                                                                            //Natural: ASSIGN #K := 2
            //*     NAS-VACATION-ADDRESS.V-ADDRSS-LNE-1
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_2().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-2 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_2());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,2 ) := #O-CO-ADDRESS-LINE-2
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_3().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-3 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_3());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,3 ) := #O-CO-ADDRESS-LINE-3
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_4().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-4 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_4());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,4 ) := #O-CO-ADDRESS-LINE-4
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(DbsUtil.compress(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_City(),           //Natural: COMPRESS #O-CO-ADDRESS-CITY #O-CO-ADDRESS-ST-PROV TO CIS-ADDRESS-TXT ( #I,#K )
                pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_St_Prov()));
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Type_Code().equals("C") || pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Type_Code().equals("F")))      //Natural: IF #O-CO-ADDRESS-TYPE-CODE = 'C' OR = 'F'
            {
                if (condition(pnd_K.less(5)))                                                                                                                             //Natural: IF #K LT 5
                {
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Country());                //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#K ) := #O-CO-ADDRESS-COUNTRY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(DbsUtil.compress(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K),  //Natural: COMPRESS CIS-ADDRESS-TXT ( #I,#K ) #O-CO-ADDRESS-COUNTRY INTO CIS-ADDRESS-TXT ( #I,#K )
                        pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Country()));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Zip_Code());                                //Natural: ASSIGN CIS-ZIP-CODE ( #I ) := #O-CO-ADDRESS-ZIP-CODE
            ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).setValue(pnd_I);                                                                         //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #I ) := #I
            //*  CHECK MAILING
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),                 //Natural: COMPRESS #O-LAST-NAME #O-PREFIX #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO CIS-ADDRESS-DEST-NAME ( #I )
                pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), 
                pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,1).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_1());                             //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,1 ) := #O-CM-ADDRESS-LINE-1
            pnd_K.setValue(2);                                                                                                                                            //Natural: ASSIGN #K := 2
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_2().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-2 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_2());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,2 ) := #O-CM-ADDRESS-LINE-2
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_3().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-3 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_3());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,3 ) := #O-CM-ADDRESS-LINE-3
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_4().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-4 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_4());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,4 ) := #O-CM-ADDRESS-LINE-4
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(DbsUtil.compress(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_City(),           //Natural: COMPRESS #O-CM-ADDRESS-CITY #O-CM-ADDRESS-ST-PROV TO CIS-ADDRESS-TXT ( #I,#K )
                pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_St_Prov()));
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Type_Code().equals("C") || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Type_Code().equals("F")))      //Natural: IF #O-CM-ADDRESS-TYPE-CODE = 'C' OR = 'F'
            {
                if (condition(pnd_K.less(5)))                                                                                                                             //Natural: IF #K LT 5
                {
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Country());                //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#K ) := #O-CM-ADDRESS-COUNTRY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K).setValue(DbsUtil.compress(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_K),  //Natural: COMPRESS CIS-ADDRESS-TXT ( #I,#K ) #O-CM-ADDRESS-COUNTRY INTO CIS-ADDRESS-TXT ( #I,#K )
                        pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Country()));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Zip_Code());                                //Natural: ASSIGN CIS-ZIP-CODE ( #I ) := #O-CM-ADDRESS-ZIP-CODE
            ldaIatl406.getCis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Account_Number());                 //Natural: ASSIGN CIS-BANK-PYMNT-ACCT-NMBR ( #I ) := #O-CM-BANK-ACCOUNT-NUMBER
            ldaIatl406.getCis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Aba_Eft_Number());                   //Natural: ASSIGN CIS-BANK-ABA-ACCT-NMBR ( #I ) := #O-CM-BANK-ABA-EFT-NUMBER
            ldaIatl406.getCis_Prtcpnt_File_Cis_Checking_Saving_Cd().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Check_Saving_Ind());                      //Natural: ASSIGN CIS-CHECKING-SAVING-CD ( #I ) := #O-CM-CHECK-SAVING-IND
            ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).setValue(pnd_I);                                                                         //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #I ) := #I
        }                                                                                                                                                                 //Natural: END-IF
        //*  10/2014 - END
        //*  FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
    }
    private void sub_Write_Parm_Values() throws Exception                                                                                                                 //Natural: WRITE-PARM-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "CALLING MDMN211A",NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number(), //Natural: WRITE 'CALLING MDMN211A' / '=' #I-TIAA-CREF-IND / '=' #I-CONTRACT-NUMBER / '=' #I-PAYEE-CODE / '=' #MDMA211.#O-RETURN-CODE / '=' #MDMA211.#O-RETURN-TEXT
            NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATU(),new ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATU 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM' 12X 'PAGE       :  ' *PAGE-NUMBER ( 1 ) / 01X 'RUN TIME : ' *TIMX 33X 'CIS NEW ISSUE REPORT FOR' 29X 'PROGRAM ID : ' *PROGRAM / 47X 'TRANSFER REQUESTS PROCESSED ON' #TODAYS-DATE ( EM = MM/DD/YYYY ) ///
                        ColumnSpacing(12),"PAGE       :  ",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new 
                        ColumnSpacing(33),"CIS NEW ISSUE REPORT FOR",new ColumnSpacing(29),"PROGRAM ID : ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(47),"TRANSFER REQUESTS PROCESSED ON",pnd_Todays_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE);
                    if (condition(pnd_Total_Sw.equals("Y")))                                                                                                              //Natural: IF #TOTAL-SW = 'Y'
                    {
                        getReports().write(1, ReportOption.NOTITLE,"PPCN    ",new ColumnSpacing(3),"PAYEE CD ",new ColumnSpacing(1),"UNIT TYPE ",new ColumnSpacing(1)," FUND",new  //Natural: WRITE ( 1 ) 'PPCN    ' 3X 'PAYEE CD ' 1X 'UNIT TYPE ' 1X ' FUND' 4X '  IA UNITS ' 4X '    CIS UNITS  ' 1X 'INCOME OPTION ' 1X ' GUARANTEED YEARS  ' 1X ' GUARANTEED DAYS'
                            ColumnSpacing(4),"  IA UNITS ",new ColumnSpacing(4),"    CIS UNITS  ",new ColumnSpacing(1),"INCOME OPTION ",new ColumnSpacing(1)," GUARANTEED YEARS  ",new 
                            ColumnSpacing(1)," GUARANTEED DAYS");
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ERROR REPORT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATX(), new ReportEditMask                 //Natural: WRITE ( 2 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATX ( EM = MM/DD/YYYY ) 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM' 12X 'PAGE       :  ' *PAGE-NUMBER ( 2 ) / 01X 'RUN TIME : ' *TIMX 33X 'CIS NEW ISSUE ERROR REPORT FOR' 27X 'PROGRAM ID : ' *PROGRAM / 47X 'TRANSFER REQUESTS PROCESSED ON' #TODAYS-DATE ( EM = MM/DD/YYYY ) /// /01X 'PIN     From     From   To       To                   Reject' /01X 'Number  Contract Payee  Contract Payee                Reason' /01X '------ --------- -----  -------- -----' '-' ( 60 )
                        ("MM/DD/YYYY"),new ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM",new ColumnSpacing(12),"PAGE       :  ",getReports().getPageNumberDbs(2),NEWLINE,new 
                        ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(33),"CIS NEW ISSUE ERROR REPORT FOR",new ColumnSpacing(27),"PROGRAM ID : ",Global.getPROGRAM(),NEWLINE,new 
                        ColumnSpacing(47),"TRANSFER REQUESTS PROCESSED ON",pnd_Todays_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
                        ColumnSpacing(1),"PIN     From     From   To       To                   Reject",NEWLINE,new ColumnSpacing(1),"Number  Contract Payee  Contract Payee                Reason",NEWLINE,new 
                        ColumnSpacing(1),"------ --------- -----  -------- -----","-",new RepeatItem(60));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
