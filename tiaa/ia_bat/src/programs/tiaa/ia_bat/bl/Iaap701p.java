/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:44 PM
**        * FROM NATURAL PROGRAM : Iaap701p
************************************************************
**        * FILE NAME            : Iaap701p.java
**        * CLASS NAME           : Iaap701p
**        * INSTANCE NAME        : Iaap701p
************************************************************
************************************************************************
*                                                                      *
* PROGRAM: IAAP701P                                                    *
* PURPOSE: PENDED REPORTS FOR ALL PENDED PAYMENTS AND PENDED PAYMENTS  *
*          DUE FOR THE CHECK CYCLE.                                    *
* INPUT:   CMWKF01  DSN=PPDT.PNA.IA.CPS.FILE(0),DISP=SHR               *
*          FORM P1930IAM.                                              *
* HISTORY                                                              *
*                                                                      *
* 06/2013  ADD A REPORT OF ALL PAYMENTS DUE WITH B0 HOLD CODE FOR      *
* JFT      PARTICIPANTS IN FOREIGN COUNTRIES. SCAN ON 6/13 FOR CHANGES *
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.                  *
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap701p extends BLNatBase
{
    // Data Areas
    private PdaNazpda_M pdaNazpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup gtn_Pda_E;
    private DbsField gtn_Pda_E_Pymnt_Corp_Wpid;
    private DbsField gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time;
    private DbsField gtn_Pda_E_Cntrct_Unq_Id_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Ppcn_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Cmbn_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Cref_Nbr;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Ind;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Nbr;
    private DbsField gtn_Pda_E_Annt_Ctznshp_Cde;
    private DbsField gtn_Pda_E_Annt_Rsdncy_Cde;
    private DbsField gtn_Pda_E_Annt_Locality_Cde;

    private DbsGroup gtn_Pda_E_Ph_Name;
    private DbsField gtn_Pda_E_Ph_Last_Name;
    private DbsField gtn_Pda_E_Ph_Middle_Name;
    private DbsField gtn_Pda_E_Ph_First_Name;
    private DbsField gtn_Pda_E_Pymnt_Dob;

    private DbsGroup gtn_Pda_E_Pymnt_Nme_And_Addr_Grp;
    private DbsField gtn_Pda_E_Pymnt_Nme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Lines;

    private DbsGroup gtn_Pda_E__R_Field_1;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line_Txt;

    private DbsGroup gtn_Pda_E__R_Field_2;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line1_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line2_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line3_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line4_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line5_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line6_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Zip_Cde;
    private DbsField gtn_Pda_E_Pymnt_Postl_Data;
    private DbsField gtn_Pda_E_Pymnt_Addr_Type_Ind;
    private DbsField gtn_Pda_E_Pymnt_Foreign_Cde;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X;

    private DbsGroup gtn_Pda_E__R_Field_3;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X;

    private DbsGroup gtn_Pda_E__R_Field_4;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Chg_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settl_Ivc_Ind;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Total_Check_Amt;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Fund_Payee;
    private DbsField gtn_Pda_E_Inv_Acct_Cde;
    private DbsField gtn_Pda_E_Inv_Acct_Settl_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Cntrct_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Dvdnd_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Value;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Qty;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Ind;
    private DbsField gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Valuat_Period;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Deductions;
    private DbsField gtn_Pda_E_Pymnt_Ded_Cde;
    private DbsField gtn_Pda_E_Pymnt_Ded_Amt;
    private DbsField gtn_Pda_E_Pymnt_Ded_Payee_Cde;
    private DbsField gtn_Pda_E_Pymnt_Payee_Tax_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settlmnt_Dte;
    private DbsField gtn_Pda_E_Pymnt_Check_Dte;
    private DbsField gtn_Pda_E_Pymnt_Cycle_Dte;
    private DbsField gtn_Pda_E_Pymnt_Acctg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Ia_Issue_Dte;
    private DbsField gtn_Pda_E_Pymnt_Intrfce_Dte;
    private DbsField gtn_Pda_E_Cntrct_Check_Crrncy_Cde;
    private DbsField gtn_Pda_E_Cntrct_Orgn_Cde;
    private DbsField gtn_Pda_E_Cntrct_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Payee_Cde;
    private DbsField gtn_Pda_E_Cntrct_Option_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ac_Lt_10yrs;
    private DbsField gtn_Pda_E_Cntrct_Mode_Cde;
    private DbsField gtn_Pda_E_Pymnt_Method_Cde;
    private DbsField gtn_Pda_E_Pymnt_Pay_Type_Req_Ind;
    private DbsField gtn_Pda_E_Pymnt_Spouse_Pay_Stats;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Roll_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde;
    private DbsField gtn_Pda_E_Pymnt_Eft_Acct_Nbr;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id_X;

    private DbsGroup gtn_Pda_E__R_Field_5;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id;
    private DbsField gtn_Pda_E_Pymnt_Chk_Sav_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Cde;
    private DbsField gtn_Pda_E_Cntrct_Hold_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Grp;
    private DbsField gtn_Pda_E_Cntrct_Hold_User_Id;
    private DbsField gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types;

    private DbsGroup gtn_Pda_E__R_Field_6;
    private DbsField gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField gtn_Pda_E_Pnd_Pnd_State_Tax_Code;
    private DbsField gtn_Pda_E_Pnd_Pnd_Local_Tax_Code;
    private DbsField gtn_Pda_E_Pymnt_Tax_Exempt_Ind;
    private DbsField gtn_Pda_E_Pymnt_Tax_Form;
    private DbsField gtn_Pda_E_Pymnt_Tax_Calc_Cde;
    private DbsField gtn_Pda_E_Cntrct_Qlfied_Cde;
    private DbsField gtn_Pda_E_Cntrct_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Sub_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ia_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Annty_Ins_Type;
    private DbsField gtn_Pda_E_Cntrct_Annty_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Insurance_Option;
    private DbsField gtn_Pda_E_Cntrct_Life_Contingency;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Cde;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Dte;
    private DbsField gtn_Pda_E_Pnd_Pnd_Pda_Count;
    private DbsField gtn_Pda_E_Pnd_Pnd_This_Pymnt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField gtn_Pda_E_Gtn_Ret_Code;

    private DbsGroup gtn_Pda_E__R_Field_7;
    private DbsField gtn_Pda_E_Global_Country;
    private DbsField gtn_Pda_E_Gtn_Ret_Msg;
    private DbsField gtn_Pda_E_Temp_Pend_Cde;
    private DbsField gtn_Pda_E_Error_Code_1;
    private DbsField gtn_Pda_E_Error_Code_2;
    private DbsField gtn_Pda_E_Current_Mode;
    private DbsField gtn_Pda_E_Egtrra_Eligibility_Ind;
    private DbsField gtn_Pda_E_Originating_Irs_Cde;

    private DbsGroup gtn_Pda_E__R_Field_8;
    private DbsField gtn_Pda_E_Distributing_Irc_Cde;
    private DbsField gtn_Pda_E_Receiving_Irc_Cde;
    private DbsField gtn_Pda_E_Cntrct_Money_Source;
    private DbsField gtn_Pda_E_Roth_Dob;
    private DbsField gtn_Pda_E_Roth_First_Contrib_Dte;
    private DbsField gtn_Pda_E_Roth_Death_Dte;
    private DbsField gtn_Pda_E_Roth_Disability_Dte;
    private DbsField gtn_Pda_E_Roth_Money_Source;
    private DbsField gtn_Pda_E_Roth_Qual_Non_Qual_Distrib;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Pend_Dte;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;

    private DataAccessProgramView vw_cntrct;
    private DbsField cntrct_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Cntrct_Issue_Dte;
    private DbsField cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Orgn_Cde;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_9;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Payee;
    private DbsField pnd_Iss_Dte;

    private DbsGroup pnd_Iss_Dte__R_Field_10;
    private DbsField pnd_Iss_Dte_Pnd_Iss_Yyyymm;
    private DbsField pnd_Iss_Dte_Pnd_Iss_Dd;
    private DbsField pnd_Dod;
    private DbsField pnd_C_Cnt;
    private DbsField pnd_D_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_State;
    private DbsField pnd_Gtn_Payee;

    private DbsGroup pnd_Gtn_Payee__R_Field_11;
    private DbsField pnd_Gtn_Payee_Pnd_P_Cde;
    private DbsField pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Pdte;
    private DbsField pnd_Fnd_N;
    private DbsField pnd_Fnd_A;
    private DbsField pnd_Fnd_1;
    private DbsField pnd_Fund;
    private DbsField pnd_Fn;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Trade_Dte;
    private DbsField pnd_Trde_D;
    private DbsField pnd_Cnt;
    private DbsField pnd_Amt;
    private DbsField pnd_Div;
    private DbsField pnd_Pmt;
    private DbsField pnd_Name;
    private DbsField pnd_Payee_Addr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNazpda_M = new PdaNazpda_M(localVariables);

        // Local Variables

        gtn_Pda_E = localVariables.newGroupInRecord("gtn_Pda_E", "GTN-PDA-E");
        gtn_Pda_E_Pymnt_Corp_Wpid = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        gtn_Pda_E_Cntrct_Unq_Id_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        gtn_Pda_E_Cntrct_Ppcn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Cntrct_Cmbn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        gtn_Pda_E_Cntrct_Cref_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Annt_Soc_Sec_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        gtn_Pda_E_Annt_Soc_Sec_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        gtn_Pda_E_Annt_Ctznshp_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Annt_Rsdncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Annt_Locality_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);

        gtn_Pda_E_Ph_Name = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Ph_Name", "PH-NAME");
        gtn_Pda_E_Ph_Last_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        gtn_Pda_E_Ph_Middle_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        gtn_Pda_E_Ph_First_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        gtn_Pda_E_Pymnt_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);

        gtn_Pda_E_Pymnt_Nme_And_Addr_Grp = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1, 
            2));
        gtn_Pda_E_Pymnt_Nme = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        gtn_Pda_E_Pymnt_Addr_Lines = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", FieldType.STRING, 
            210);

        gtn_Pda_E__R_Field_1 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_1", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line_Txt = gtn_Pda_E__R_Field_1.newFieldArrayInGroup("gtn_Pda_E_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));

        gtn_Pda_E__R_Field_2 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_2", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line1_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line2_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line3_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line4_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line5_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line6_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Zip_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 
            9);
        gtn_Pda_E_Pymnt_Postl_Data = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        gtn_Pda_E_Pymnt_Addr_Type_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Foreign_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X", "PYMNT-ADDR-LAST-CHG-DTE-X", 
            FieldType.STRING, 8);

        gtn_Pda_E__R_Field_3 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_3", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte = gtn_Pda_E__R_Field_3.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X", "PYMNT-ADDR-LAST-CHG-TME-X", 
            FieldType.STRING, 7);

        gtn_Pda_E__R_Field_4 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_4", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme = gtn_Pda_E__R_Field_4.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 
            7);
        gtn_Pda_E_Pymnt_Addr_Chg_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Settl_Ivc_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);

        gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt", "##FUND-SETTLMNT", new DbsArrayController(1, 
            40));
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde", "##FUND-SETTL-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt", "##FUND-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt", "##FUND-SETTL-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt = gtn_Pda_E.newFieldArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt", "##FUND-DPI-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 40));
        gtn_Pda_E_Pnd_Pnd_Total_Check_Amt = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Total_Check_Amt", "##TOTAL-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        gtn_Pda_E_Pnd_Pnd_Fund_Payee = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1, 40));
        gtn_Pda_E_Inv_Acct_Cde = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Inv_Acct_Settl_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Cntrct_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Dvdnd_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Unit_Value = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4);
        gtn_Pda_E_Inv_Acct_Unit_Qty = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        gtn_Pda_E_Inv_Acct_Ivc_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_E_Inv_Acct_Ivc_Ind = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_E_Inv_Acct_Valuat_Period = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);

        gtn_Pda_E_Pnd_Pnd_Deductions = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1, 10));
        gtn_Pda_E_Pymnt_Ded_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Ded_Amt = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        gtn_Pda_E_Pymnt_Ded_Payee_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8);
        gtn_Pda_E_Pymnt_Payee_Tax_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Payee_Tax_Ind", "PYMNT-PAYEE-TAX-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Settlmnt_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Check_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Cycle_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Acctg_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Ia_Issue_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Intrfce_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        gtn_Pda_E_Cntrct_Check_Crrncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Orgn_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Option_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Cntrct_Ac_Lt_10yrs = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 1);
        gtn_Pda_E_Cntrct_Mode_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Method_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Pay_Type_Req_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_E_Pymnt_Spouse_Pay_Stats = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Pymnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Pymnt_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Roll_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        gtn_Pda_E_Pymnt_Eft_Acct_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        gtn_Pda_E_Pymnt_Eft_Transit_Id_X = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id_X", "PYMNT-EFT-TRANSIT-ID-X", FieldType.STRING, 9);

        gtn_Pda_E__R_Field_5 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_5", "REDEFINE", gtn_Pda_E_Pymnt_Eft_Transit_Id_X);
        gtn_Pda_E_Pymnt_Eft_Transit_Id = gtn_Pda_E__R_Field_5.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9);
        gtn_Pda_E_Pymnt_Chk_Sav_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Hold_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Grp = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        gtn_Pda_E_Cntrct_Hold_User_Id = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 3);
        gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types", "##TX-WITHHOLDING-TAX-TYPES", 
            FieldType.STRING, 3);

        gtn_Pda_E__R_Field_6 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_6", "REDEFINE", gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types);
        gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr = gtn_Pda_E__R_Field_6.newFieldInGroup("gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        gtn_Pda_E_Pnd_Pnd_State_Tax_Code = gtn_Pda_E__R_Field_6.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_State_Tax_Code", "##STATE-TAX-CODE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pnd_Pnd_Local_Tax_Code = gtn_Pda_E__R_Field_6.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Local_Tax_Code", "##LOCAL-TAX-CODE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Tax_Exempt_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Tax_Form = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        gtn_Pda_E_Pymnt_Tax_Calc_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Qlfied_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Sub_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Ia_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Annty_Ins_Type = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Annty_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Insurance_Option = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Life_Contingency = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Suspend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Suspend_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", FieldType.DATE);
        gtn_Pda_E_Pnd_Pnd_Pda_Count = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Pda_Count", "##PDA-COUNT", FieldType.PACKED_DECIMAL, 2);
        gtn_Pda_E_Pnd_Pnd_This_Pymnt = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 2);
        gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 2);
        gtn_Pda_E_Gtn_Ret_Code = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);

        gtn_Pda_E__R_Field_7 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_7", "REDEFINE", gtn_Pda_E_Gtn_Ret_Code);
        gtn_Pda_E_Global_Country = gtn_Pda_E__R_Field_7.newFieldInGroup("gtn_Pda_E_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        gtn_Pda_E_Gtn_Ret_Msg = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Gtn_Ret_Msg", "GTN-RET-MSG", FieldType.STRING, 60);
        gtn_Pda_E_Temp_Pend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Temp_Pend_Cde", "TEMP-PEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_1 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_2 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        gtn_Pda_E_Current_Mode = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Current_Mode", "CURRENT-MODE", FieldType.STRING, 1);
        gtn_Pda_E_Egtrra_Eligibility_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        gtn_Pda_E_Originating_Irs_Cde = gtn_Pda_E.newFieldArrayInGroup("gtn_Pda_E_Originating_Irs_Cde", "ORIGINATING-IRS-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1, 8));

        gtn_Pda_E__R_Field_8 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_8", "REDEFINE", gtn_Pda_E_Originating_Irs_Cde);
        gtn_Pda_E_Distributing_Irc_Cde = gtn_Pda_E__R_Field_8.newFieldInGroup("gtn_Pda_E_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        gtn_Pda_E_Receiving_Irc_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_First_Contrib_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Roth_Death_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Disability_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Qual_Non_Qual_Distrib = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", FieldType.STRING, 
            1);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Pend_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        registerRecord(vw_cpr);

        vw_cntrct = new DataAccessProgramView(new NameInfo("vw_cntrct", "CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        cntrct_Cntrct_Ppcn_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Cntrct_Issue_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        cntrct_Cntrct_Issue_Dte_Dd = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        cntrct_Cntrct_First_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        cntrct_Cntrct_Orgn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        registerRecord(vw_cntrct);

        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_9", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cpr_Cntrct = pnd_Cpr_Key__R_Field_9.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Cntrct", "#CPR-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cpr_Payee = pnd_Cpr_Key__R_Field_9.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Payee", "#CPR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Iss_Dte = localVariables.newFieldInRecord("pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 8);

        pnd_Iss_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Iss_Dte__R_Field_10", "REDEFINE", pnd_Iss_Dte);
        pnd_Iss_Dte_Pnd_Iss_Yyyymm = pnd_Iss_Dte__R_Field_10.newFieldInGroup("pnd_Iss_Dte_Pnd_Iss_Yyyymm", "#ISS-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Iss_Dte_Pnd_Iss_Dd = pnd_Iss_Dte__R_Field_10.newFieldInGroup("pnd_Iss_Dte_Pnd_Iss_Dd", "#ISS-DD", FieldType.NUMERIC, 2);
        pnd_Dod = localVariables.newFieldInRecord("pnd_Dod", "#DOD", FieldType.NUMERIC, 6);
        pnd_C_Cnt = localVariables.newFieldInRecord("pnd_C_Cnt", "#C-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_D_Cnt = localVariables.newFieldInRecord("pnd_D_Cnt", "#D-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Gtn_Payee = localVariables.newFieldInRecord("pnd_Gtn_Payee", "#GTN-PAYEE", FieldType.STRING, 4);

        pnd_Gtn_Payee__R_Field_11 = localVariables.newGroupInRecord("pnd_Gtn_Payee__R_Field_11", "REDEFINE", pnd_Gtn_Payee);
        pnd_Gtn_Payee_Pnd_P_Cde = pnd_Gtn_Payee__R_Field_11.newFieldInGroup("pnd_Gtn_Payee_Pnd_P_Cde", "#P-CDE", FieldType.NUMERIC, 2);
        pnd_Inv_Acct_Settl_Amt = localVariables.newFieldInRecord("pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.STRING, 14);
        pnd_Inv_Acct_Cntrct_Amt = localVariables.newFieldInRecord("pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.STRING, 14);
        pnd_Inv_Acct_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.STRING, 14);
        pnd_Inv_Acct_Unit_Value = localVariables.newFieldInRecord("pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.STRING, 11);
        pnd_Inv_Acct_Unit_Qty = localVariables.newFieldInRecord("pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.STRING, 11);
        pnd_Pdte = localVariables.newFieldInRecord("pnd_Pdte", "#PDTE", FieldType.STRING, 8);
        pnd_Fnd_N = localVariables.newFieldArrayInRecord("pnd_Fnd_N", "#FND-N", FieldType.STRING, 2, new DbsArrayController(1, 21));
        pnd_Fnd_A = localVariables.newFieldArrayInRecord("pnd_Fnd_A", "#FND-A", FieldType.STRING, 3, new DbsArrayController(1, 21));
        pnd_Fnd_1 = localVariables.newFieldArrayInRecord("pnd_Fnd_1", "#FND-1", FieldType.STRING, 1, new DbsArrayController(1, 21));
        pnd_Fund = localVariables.newFieldInRecord("pnd_Fund", "#FUND", FieldType.STRING, 3);
        pnd_Fn = localVariables.newFieldInRecord("pnd_Fn", "#FN", FieldType.STRING, 1);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 10);
        pnd_Trade_Dte = localVariables.newFieldInRecord("pnd_Trade_Dte", "#TRADE-DTE", FieldType.STRING, 10);
        pnd_Trde_D = localVariables.newFieldInRecord("pnd_Trde_D", "#TRDE-D", FieldType.DATE);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Amt = localVariables.newFieldInRecord("pnd_Amt", "#AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Div = localVariables.newFieldInRecord("pnd_Div", "#DIV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pmt = localVariables.newFieldInRecord("pnd_Pmt", "#PMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_Payee_Addr = localVariables.newFieldArrayInRecord("pnd_Payee_Addr", "#PAYEE-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 7));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cpr.reset();
        vw_cntrct.reset();

        localVariables.reset();
        pnd_Fnd_N.getValue(1).setInitialValue("1 ");
        pnd_Fnd_N.getValue(2).setInitialValue("1G");
        pnd_Fnd_N.getValue(3).setInitialValue("1S");
        pnd_Fnd_N.getValue(4).setInitialValue("05");
        pnd_Fnd_N.getValue(5).setInitialValue("07");
        pnd_Fnd_N.getValue(6).setInitialValue("06");
        pnd_Fnd_N.getValue(7).setInitialValue("08");
        pnd_Fnd_N.getValue(8).setInitialValue("10");
        pnd_Fnd_N.getValue(9).setInitialValue("03");
        pnd_Fnd_N.getValue(10).setInitialValue("04");
        pnd_Fnd_N.getValue(11).setInitialValue("02");
        pnd_Fnd_N.getValue(12).setInitialValue("09");
        pnd_Fnd_N.getValue(13).setInitialValue("11");
        pnd_Fnd_N.getValue(14).setInitialValue("42");
        pnd_Fnd_N.getValue(15).setInitialValue("43");
        pnd_Fnd_N.getValue(16).setInitialValue("44");
        pnd_Fnd_N.getValue(17).setInitialValue("45");
        pnd_Fnd_N.getValue(18).setInitialValue("46");
        pnd_Fnd_N.getValue(19).setInitialValue("47");
        pnd_Fnd_N.getValue(20).setInitialValue("48");
        pnd_Fnd_N.getValue(21).setInitialValue("49");
        pnd_Fnd_A.getValue(1).setInitialValue("STD");
        pnd_Fnd_A.getValue(2).setInitialValue("GRD");
        pnd_Fnd_A.getValue(3).setInitialValue("STD");
        pnd_Fnd_A.getValue(4).setInitialValue("BND");
        pnd_Fnd_A.getValue(5).setInitialValue("GRW");
        pnd_Fnd_A.getValue(6).setInitialValue("GLB");
        pnd_Fnd_A.getValue(7).setInitialValue("EQX");
        pnd_Fnd_A.getValue(8).setInitialValue("ILB");
        pnd_Fnd_A.getValue(9).setInitialValue("MMA");
        pnd_Fnd_A.getValue(10).setInitialValue("SCL");
        pnd_Fnd_A.getValue(11).setInitialValue("STK");
        pnd_Fnd_A.getValue(12).setInitialValue("REA");
        pnd_Fnd_A.getValue(13).setInitialValue("LRI");
        pnd_Fnd_A.getValue(14).setInitialValue("PSI");
        pnd_Fnd_A.getValue(15).setInitialValue("PGE");
        pnd_Fnd_A.getValue(16).setInitialValue("PGI");
        pnd_Fnd_A.getValue(17).setInitialValue("PIE");
        pnd_Fnd_A.getValue(18).setInitialValue("PSE");
        pnd_Fnd_A.getValue(19).setInitialValue("PLC");
        pnd_Fnd_A.getValue(20).setInitialValue("PSC");
        pnd_Fnd_A.getValue(21).setInitialValue("PRE");
        pnd_Fnd_1.getValue(1).setInitialValue("T");
        pnd_Fnd_1.getValue(2).setInitialValue("G");
        pnd_Fnd_1.getValue(3).setInitialValue("T");
        pnd_Fnd_1.getValue(4).setInitialValue("B");
        pnd_Fnd_1.getValue(5).setInitialValue("L");
        pnd_Fnd_1.getValue(6).setInitialValue("W");
        pnd_Fnd_1.getValue(7).setInitialValue("E");
        pnd_Fnd_1.getValue(8).setInitialValue("I");
        pnd_Fnd_1.getValue(9).setInitialValue("M");
        pnd_Fnd_1.getValue(10).setInitialValue("S");
        pnd_Fnd_1.getValue(11).setInitialValue("C");
        pnd_Fnd_1.getValue(12).setInitialValue("R");
        pnd_Fnd_1.getValue(13).setInitialValue("D");
        pnd_Fnd_1.getValue(14).setInitialValue("V");
        pnd_Fnd_1.getValue(15).setInitialValue("N");
        pnd_Fnd_1.getValue(16).setInitialValue("O");
        pnd_Fnd_1.getValue(17).setInitialValue("P");
        pnd_Fnd_1.getValue(18).setInitialValue("Q");
        pnd_Fnd_1.getValue(19).setInitialValue("J");
        pnd_Fnd_1.getValue(20).setInitialValue("K");
        pnd_Fnd_1.getValue(21).setInitialValue("X");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap701p() throws Exception
    {
        super("Iaap701p");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 250 PS = 0;//Natural: FORMAT ( 2 ) LS = 250 PS = 0;//Natural: FORMAT ( 3 ) LS = 150 PS = 0
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  6/13 - START
        //*  6/13 - END                                                                                                                                                   //Natural: AT TOP OF PAGE ( 3 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 GTN-PDA-E
        while (condition(getWorkFiles().read(1, gtn_Pda_E)))
        {
            //*  6/13 START
            if (condition(pnd_Check_Dte.equals(" ")))                                                                                                                     //Natural: IF #CHECK-DTE = ' '
            {
                pnd_Check_Dte.setValueEdited(gtn_Pda_E_Pymnt_Check_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                 //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #CHECK-DTE
                getWorkFiles().write(2, false, pnd_Check_Dte);                                                                                                            //Natural: WRITE WORK FILE 2 #CHECK-DTE
                //*  6/13 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gtn_Pda_E_Pymnt_Suspend_Cde.equals("0") || gtn_Pda_E_Pymnt_Suspend_Cde.equals(" ")))                                                            //Natural: IF PYMNT-SUSPEND-CDE = '0' OR = ' '
            {
                //*  6/13 - START
                if (condition(gtn_Pda_E_Current_Mode.equals("1") && DbsUtil.maskMatches(gtn_Pda_E_Cntrct_Hold_Cde,"'B0'")))                                               //Natural: IF CURRENT-MODE = '1' AND CNTRCT-HOLD-CDE = MASK ( 'B0' )
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-B0-REPORT
                    sub_Write_B0_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  6/13 - END
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Trade_Dte.equals(" ")))                                                                                                                     //Natural: IF #TRADE-DTE = ' '
            {
                pnd_Trde_D.compute(new ComputeParameters(false, pnd_Trde_D), gtn_Pda_E_Pymnt_Check_Dte.subtract(1));                                                      //Natural: COMPUTE #TRDE-D = PYMNT-CHECK-DTE - 1
                pnd_Trade_Dte.setValueEdited(pnd_Trde_D,new ReportEditMask("MM/DD/YYYY"));                                                                                //Natural: MOVE EDITED #TRDE-D ( EM = MM/DD/YYYY ) TO #TRADE-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Iss_Dte.setValueEdited(gtn_Pda_E_Pymnt_Ia_Issue_Dte,new ReportEditMask("YYYYMM"));                                                                        //Natural: MOVE EDITED PYMNT-IA-ISSUE-DTE ( EM = YYYYMM ) TO #ISS-DTE
            vw_cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) CNTRCT BY CNTRCT-PPCN-NBR = GTN-PDA-E.CNTRCT-PPCN-NBR
            (
            "READ02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", gtn_Pda_E_Cntrct_Ppcn_Nbr, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cntrct.readNextRow("READ02")))
            {
                pnd_Iss_Dte_Pnd_Iss_Yyyymm.setValue(cntrct_Cntrct_Issue_Dte);                                                                                             //Natural: ASSIGN #ISS-YYYYMM := CNTRCT-ISSUE-DTE
                if (condition(cntrct_Cntrct_Issue_Dte_Dd.equals(getZero())))                                                                                              //Natural: IF CNTRCT-ISSUE-DTE-DD = 0
                {
                    cntrct_Cntrct_Issue_Dte_Dd.setValue(1);                                                                                                               //Natural: ASSIGN CNTRCT-ISSUE-DTE-DD := 01
                }                                                                                                                                                         //Natural: END-IF
                pnd_Iss_Dte_Pnd_Iss_Dd.setValue(cntrct_Cntrct_Issue_Dte_Dd);                                                                                              //Natural: ASSIGN #ISS-DD := CNTRCT-ISSUE-DTE-DD
                pnd_Dod.reset();                                                                                                                                          //Natural: RESET #DOD
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero()) || cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                 //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0 OR CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.greater(cntrct_Cntrct_Scnd_Annt_Dod_Dte)))                                                             //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT CNTRCT-SCND-ANNT-DOD-DTE
                    {
                        pnd_Dod.setValue(cntrct_Cntrct_First_Annt_Dod_Dte);                                                                                               //Natural: ASSIGN #DOD := CNTRCT-FIRST-ANNT-DOD-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Dod.setValue(cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                                                                //Natural: ASSIGN #DOD := CNTRCT-SCND-ANNT-DOD-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cpr_Key_Pnd_Cpr_Cntrct.setValue(gtn_Pda_E_Cntrct_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #CPR-CNTRCT := GTN-PDA-E.CNTRCT-PPCN-NBR
            pnd_Gtn_Payee.setValue(gtn_Pda_E_Cntrct_Payee_Cde);                                                                                                           //Natural: ASSIGN #GTN-PAYEE := GTN-PDA-E.CNTRCT-PAYEE-CDE
            pnd_Cpr_Key_Pnd_Cpr_Payee.setValue(pnd_Gtn_Payee_Pnd_P_Cde);                                                                                                  //Natural: ASSIGN #CPR-PAYEE := #P-CDE
            vw_cpr.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CPR-KEY
            (
            "READ03",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
            1
            );
            READ03:
            while (condition(vw_cpr.readNextRow("READ03")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_State.reset();                                                                                                                                            //Natural: RESET #STATE #PDTE
            pnd_Pdte.reset();
            DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaNazpda_M.getMsg_Info_Sub(), pnd_State, gtn_Pda_E_Annt_Rsdncy_Cde);                               //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #STATE GTN-PDA-E.ANNT-RSDNCY-CDE
            if (condition(Global.isEscape())) return;
            if (condition(gtn_Pda_E_Pymnt_Suspend_Dte.greater(getZero())))                                                                                                //Natural: IF GTN-PDA-E.PYMNT-SUSPEND-DTE GT 0
            {
                pnd_Pdte.setValueEdited(gtn_Pda_E_Pymnt_Suspend_Dte,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED GTN-PDA-E.PYMNT-SUSPEND-DTE ( EM = YYYYMMDD ) TO #PDTE
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 40
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
            {
                if (condition(gtn_Pda_E_Inv_Acct_Cde.getValue(pnd_I).equals(" ")))                                                                                        //Natural: IF INV-ACCT-CDE ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Inv_Acct_Settl_Amt.reset();                                                                                                                           //Natural: RESET #INV-ACCT-SETTL-AMT #INV-ACCT-CNTRCT-AMT #FUND #FN #INV-ACCT-DVDND-AMT #INV-ACCT-UNIT-VALUE #INV-ACCT-UNIT-QTY
                pnd_Inv_Acct_Cntrct_Amt.reset();
                pnd_Fund.reset();
                pnd_Fn.reset();
                pnd_Inv_Acct_Dvdnd_Amt.reset();
                pnd_Inv_Acct_Unit_Value.reset();
                pnd_Inv_Acct_Unit_Qty.reset();
                if (condition(gtn_Pda_E_Inv_Acct_Settl_Amt.getValue(pnd_I).greater(getZero())))                                                                           //Natural: IF INV-ACCT-SETTL-AMT ( #I ) GT 0
                {
                    pnd_Inv_Acct_Settl_Amt.setValueEdited(gtn_Pda_E_Inv_Acct_Settl_Amt.getValue(pnd_I),new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));                             //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #INV-ACCT-SETTL-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gtn_Pda_E_Inv_Acct_Cntrct_Amt.getValue(pnd_I).greater(getZero())))                                                                          //Natural: IF INV-ACCT-CNTRCT-AMT ( #I ) GT 0
                {
                    pnd_Inv_Acct_Cntrct_Amt.setValueEdited(gtn_Pda_E_Inv_Acct_Cntrct_Amt.getValue(pnd_I),new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED INV-ACCT-CNTRCT-AMT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #INV-ACCT-CNTRCT-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gtn_Pda_E_Inv_Acct_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                                           //Natural: IF INV-ACCT-DVDND-AMT ( #I ) GT 0
                {
                    pnd_Inv_Acct_Dvdnd_Amt.setValueEdited(gtn_Pda_E_Inv_Acct_Dvdnd_Amt.getValue(pnd_I),new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));                             //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #INV-ACCT-DVDND-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gtn_Pda_E_Inv_Acct_Unit_Value.getValue(pnd_I).greater(getZero())))                                                                          //Natural: IF INV-ACCT-UNIT-VALUE ( #I ) GT 0
                {
                    pnd_Inv_Acct_Unit_Value.setValueEdited(gtn_Pda_E_Inv_Acct_Unit_Value.getValue(pnd_I),new ReportEditMask("ZZ,ZZ9.9999"));                              //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #I ) ( EM = ZZ,ZZ9.9999 ) TO #INV-ACCT-UNIT-VALUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gtn_Pda_E_Inv_Acct_Unit_Qty.getValue(pnd_I).greater(getZero())))                                                                            //Natural: IF INV-ACCT-UNIT-QTY ( #I ) GT 0
                {
                    pnd_Inv_Acct_Unit_Qty.setValueEdited(gtn_Pda_E_Inv_Acct_Unit_Qty.getValue(pnd_I),new ReportEditMask("ZZZ,ZZ9.999"));                                  //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #I ) ( EM = ZZZ,ZZ9.999 ) TO #INV-ACCT-UNIT-QTY
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Fnd_N.getValue("*"),true), new ExamineSearch(gtn_Pda_E_Inv_Acct_Cde.getValue(pnd_I), true), new                     //Natural: EXAMINE FULL #FND-N ( * ) FOR FULL INV-ACCT-CDE ( #I ) GIVING INDEX #J
                    ExamineGivingIndex(pnd_J));
                if (condition(pnd_J.greater(getZero())))                                                                                                                  //Natural: IF #J GT 0
                {
                    pnd_Fund.setValue(pnd_Fnd_A.getValue(pnd_J));                                                                                                         //Natural: ASSIGN #FUND := #FND-A ( #J )
                    pnd_Fn.setValue(pnd_Fnd_1.getValue(pnd_J));                                                                                                           //Natural: ASSIGN #FN := #FND-1 ( #J )
                }                                                                                                                                                         //Natural: END-IF
                //*  PAYMENT DUE
                if (condition(gtn_Pda_E_Current_Mode.equals("1")))                                                                                                        //Natural: IF GTN-PDA-E.CURRENT-MODE = '1'
                {
                    getReports().display(1, "Check Dte",                                                                                                                  //Natural: DISPLAY ( 1 ) 'Check Dte' #CHECK-DTE 'Part Dte' #TRADE-DTE 'Contract' GTN-PDA-E.CNTRCT-PPCN-NBR 'Py' GTN-PDA-E.CNTRCT-PAYEE-CDE ( AL = 2 ) 'Issue Dte' #ISS-DTE 'Optn' GTN-PDA-E.CNTRCT-OPTION-CDE ( EM = 99 ) 'Orgn' CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) 'Mode' GTN-PDA-E.CNTRCT-MODE-CDE 'Res State' #STATE 'Date of Death' #DOD 'Pend Cde' GTN-PDA-E.PYMNT-SUSPEND-CDE 'Pend Dte' #PDTE 'Finl Per Pay Dte' CNTRCT-FINAL-PER-PAY-DTE 'Fnd Cde' #FN 'Fnd Nme' #FUND 'Reval Mthd' INV-ACCT-VALUAT-PERIOD ( #I ) 'Guar Amt' #INV-ACCT-CNTRCT-AMT 'Div Amt' #INV-ACCT-DVDND-AMT 'Units' #INV-ACCT-UNIT-QTY 'AUV' #INV-ACCT-UNIT-VALUE 'Total Pmt' #INV-ACCT-SETTL-AMT
                    		pnd_Check_Dte,"Part Dte",
                    		pnd_Trade_Dte,"Contract",
                    		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Py",
                    		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Issue Dte",
                    		pnd_Iss_Dte,"Optn",
                    		gtn_Pda_E_Cntrct_Option_Cde, new ReportEditMask ("99"),"Orgn",
                    		cntrct_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"Mode",
                    		gtn_Pda_E_Cntrct_Mode_Cde,"Res State",
                    		pnd_State,"Date of Death",
                    		pnd_Dod,"Pend Cde",
                    		gtn_Pda_E_Pymnt_Suspend_Cde,"Pend Dte",
                    		pnd_Pdte,"Finl Per Pay Dte",
                    		cpr_Cntrct_Final_Per_Pay_Dte,"Fnd Cde",
                    		pnd_Fn,"Fnd Nme",
                    		pnd_Fund,"Reval Mthd",
                    		gtn_Pda_E_Inv_Acct_Valuat_Period.getValue(pnd_I),"Guar Amt",
                    		pnd_Inv_Acct_Cntrct_Amt,"Div Amt",
                    		pnd_Inv_Acct_Dvdnd_Amt,"Units",
                    		pnd_Inv_Acct_Unit_Qty,"AUV",
                    		pnd_Inv_Acct_Unit_Value,"Total Pmt",
                    		pnd_Inv_Acct_Settl_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_D_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #D-CNT
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(2, "Check Dte",                                                                                                                      //Natural: DISPLAY ( 2 ) 'Check Dte' #CHECK-DTE 'Part Dte' #TRADE-DTE 'Contract' GTN-PDA-E.CNTRCT-PPCN-NBR 'Py' GTN-PDA-E.CNTRCT-PAYEE-CDE ( AL = 2 ) 'Issue Dte' #ISS-DTE 'Optn' GTN-PDA-E.CNTRCT-OPTION-CDE ( EM = 99 ) 'Orgn' CNTRCT.CNTRCT-ORGN-CDE ( EM = 99 ) 'Mode' GTN-PDA-E.CNTRCT-MODE-CDE 'Res State' #STATE 'Date of Death' #DOD 'Pend Cde' GTN-PDA-E.PYMNT-SUSPEND-CDE 'Pend Dte' #PDTE 'Finl Per Pay Dte' CNTRCT-FINAL-PER-PAY-DTE 'Fnd Cde' #FN 'Fnd Nme' #FUND 'Reval Mthd' INV-ACCT-VALUAT-PERIOD ( #I ) 'Guar Amt' #INV-ACCT-CNTRCT-AMT 'Div Amt' #INV-ACCT-DVDND-AMT 'Units' #INV-ACCT-UNIT-QTY 'AUV' #INV-ACCT-UNIT-VALUE 'Total Pmt' #INV-ACCT-SETTL-AMT
                		pnd_Check_Dte,"Part Dte",
                		pnd_Trade_Dte,"Contract",
                		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Py",
                		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Issue Dte",
                		pnd_Iss_Dte,"Optn",
                		gtn_Pda_E_Cntrct_Option_Cde, new ReportEditMask ("99"),"Orgn",
                		cntrct_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"Mode",
                		gtn_Pda_E_Cntrct_Mode_Cde,"Res State",
                		pnd_State,"Date of Death",
                		pnd_Dod,"Pend Cde",
                		gtn_Pda_E_Pymnt_Suspend_Cde,"Pend Dte",
                		pnd_Pdte,"Finl Per Pay Dte",
                		cpr_Cntrct_Final_Per_Pay_Dte,"Fnd Cde",
                		pnd_Fn,"Fnd Nme",
                		pnd_Fund,"Reval Mthd",
                		gtn_Pda_E_Inv_Acct_Valuat_Period.getValue(pnd_I),"Guar Amt",
                		pnd_Inv_Acct_Cntrct_Amt,"Div Amt",
                		pnd_Inv_Acct_Dvdnd_Amt,"Units",
                		pnd_Inv_Acct_Unit_Qty,"AUV",
                		pnd_Inv_Acct_Unit_Value,"Total Pmt",
                		pnd_Inv_Acct_Settl_Amt);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_C_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-CNT
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Pended Payments Due Cnt:",pnd_D_Cnt);                                                                           //Natural: WRITE ( 1 ) 'Total Pended Payments Due Cnt:' #D-CNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Pended Payments Cnt:",pnd_C_Cnt);                                                                               //Natural: WRITE ( 2 ) 'Total Pended Payments Cnt:' #C-CNT
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total B0 Records Count:",pnd_Cnt, new ReportEditMask ("ZZZ,ZZ9"));                                                    //Natural: WRITE ( 3 ) 'Total B0 Records Count:' #CNT ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-B0-REPORT
        //*  'PIN' CNTRCT-UNQ-ID-NBR
    }
    private void sub_Write_B0_Report() throws Exception                                                                                                                   //Natural: WRITE-B0-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Name.setValue(DbsUtil.compress(gtn_Pda_E_Ph_First_Name, gtn_Pda_E_Ph_Middle_Name, gtn_Pda_E_Ph_Last_Name));                                                   //Natural: COMPRESS GTN-PDA-E.PH-FIRST-NAME GTN-PDA-E.PH-MIDDLE-NAME GTN-PDA-E.PH-LAST-NAME INTO #NAME
        pnd_Amt.compute(new ComputeParameters(false, pnd_Amt), DbsField.add(getZero(),gtn_Pda_E_Inv_Acct_Cntrct_Amt.getValue("*")));                                      //Natural: COMPUTE #AMT = 0 + INV-ACCT-CNTRCT-AMT ( * )
        //*  082017 11 CHARS IS MAX
        pnd_Div.compute(new ComputeParameters(false, pnd_Div), DbsField.add(getZero(),gtn_Pda_E_Inv_Acct_Dvdnd_Amt.getValue("*")));                                       //Natural: COMPUTE #DIV = 0 + INV-ACCT-DVDND-AMT ( * )
        pnd_Pmt.compute(new ComputeParameters(false, pnd_Pmt), DbsField.add(getZero(),gtn_Pda_E_Inv_Acct_Settl_Amt.getValue("*")));                                       //Natural: ASSIGN #PMT := 0 + INV-ACCT-SETTL-AMT ( * )
        pnd_Payee_Addr.getValue(1).setValue(gtn_Pda_E_Pymnt_Nme.getValue(1));                                                                                             //Natural: ASSIGN #PAYEE-ADDR ( 1 ) := PYMNT-NME ( 1 )
        pnd_Payee_Addr.getValue(2,":",7).setValue(gtn_Pda_E_Pymnt_Addr_Line_Txt.getValue(1,1,":",6));                                                                     //Natural: ASSIGN #PAYEE-ADDR ( 2:7 ) := PYMNT-ADDR-LINE-TXT ( 1,1:6 )
        getReports().display(3, "PIN",                                                                                                                                    //Natural: DISPLAY ( 3 ) 'PIN' CNTRCT-UNQ-ID-NBR ( NL = 11 ) 'Combine Nbr' CNTRCT-CMBN-NBR 'Contract' GTN-PDA-E.CNTRCT-PPCN-NBR 'Payee' GTN-PDA-E.CNTRCT-PAYEE-CDE ( AL = 2 ) 'Participant Name' #NAME 'Payee Name and Address' #PAYEE-ADDR ( * ) 'Guar Amt' #AMT ( EM = ZZZ,ZZ9.99 ) 'Div Amt' #DIV ( EM = ZZZ,ZZ9.99 ) 'Settl Amt' #PMT ( EM = ZZZ,ZZ9.99 )
        		gtn_Pda_E_Cntrct_Unq_Id_Nbr, new NumericLength (11),"Combine Nbr",
        		gtn_Pda_E_Cntrct_Cmbn_Nbr,"Contract",
        		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Payee",
        		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Participant Name",
        		pnd_Name,"Payee Name and Address",
        		pnd_Payee_Addr.getValue("*"),"Guar Amt",
        		pnd_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"Div Amt",
        		pnd_Div, new ReportEditMask ("ZZZ,ZZ9.99"),"Settl Amt",
        		pnd_Pmt, new ReportEditMask ("ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Cnt.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #CNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(86),"Pended Payments Due Report",new                 //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 86T 'Pended Payments Due Report' 208T *DATX ( EM = MM/DD/YYYY ) / 85T 'For Check Cycle: ' #CHECK-DTE
                        TabSetting(208),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(85),"For Check Cycle: ",pnd_Check_Dte);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(88),"Pended Payments Report",new                     //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM 88T 'Pended Payments Report' 208T *DATX ( EM = MM/DD/YYYY ) / 85T 'For Check Cycle: ' #CHECK-DTE
                        TabSetting(208),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(85),"For Check Cycle: ",pnd_Check_Dte);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(35),"Contracts with Hold Code B0 for Payments Due: ",pnd_Check_Dte,new  //Natural: WRITE ( 3 ) NOTITLE NOHDR *PROGRAM 35X 'Contracts with Hold Code B0 for Payments Due: ' #CHECK-DTE 35X *DATX ( EM = MM/DD/YYYY )
                        ColumnSpacing(35),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) ' '
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=250 PS=0");
        Global.format(2, "LS=250 PS=0");
        Global.format(3, "LS=150 PS=0");

        getReports().setDisplayColumns(1, "Check Dte",
        		pnd_Check_Dte,"Part Dte",
        		pnd_Trade_Dte,"Contract",
        		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Py",
        		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Issue Dte",
        		pnd_Iss_Dte,"Optn",
        		gtn_Pda_E_Cntrct_Option_Cde, new ReportEditMask ("99"),"Orgn",
        		cntrct_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"Mode",
        		gtn_Pda_E_Cntrct_Mode_Cde,"Res State",
        		pnd_State,"Date of Death",
        		pnd_Dod,"Pend Cde",
        		gtn_Pda_E_Pymnt_Suspend_Cde,"Pend Dte",
        		pnd_Pdte,"Finl Per Pay Dte",
        		cpr_Cntrct_Final_Per_Pay_Dte,"Fnd Cde",
        		pnd_Fn,"Fnd Nme",
        		pnd_Fund,"Reval Mthd",
        		gtn_Pda_E_Inv_Acct_Valuat_Period,"Guar Amt",
        		pnd_Inv_Acct_Cntrct_Amt,"Div Amt",
        		pnd_Inv_Acct_Dvdnd_Amt,"Units",
        		pnd_Inv_Acct_Unit_Qty,"AUV",
        		pnd_Inv_Acct_Unit_Value,"Total Pmt",
        		pnd_Inv_Acct_Settl_Amt);
        getReports().setDisplayColumns(2, "Check Dte",
        		pnd_Check_Dte,"Part Dte",
        		pnd_Trade_Dte,"Contract",
        		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Py",
        		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Issue Dte",
        		pnd_Iss_Dte,"Optn",
        		gtn_Pda_E_Cntrct_Option_Cde, new ReportEditMask ("99"),"Orgn",
        		cntrct_Cntrct_Orgn_Cde, new ReportEditMask ("99"),"Mode",
        		gtn_Pda_E_Cntrct_Mode_Cde,"Res State",
        		pnd_State,"Date of Death",
        		pnd_Dod,"Pend Cde",
        		gtn_Pda_E_Pymnt_Suspend_Cde,"Pend Dte",
        		pnd_Pdte,"Finl Per Pay Dte",
        		cpr_Cntrct_Final_Per_Pay_Dte,"Fnd Cde",
        		pnd_Fn,"Fnd Nme",
        		pnd_Fund,"Reval Mthd",
        		gtn_Pda_E_Inv_Acct_Valuat_Period,"Guar Amt",
        		pnd_Inv_Acct_Cntrct_Amt,"Div Amt",
        		pnd_Inv_Acct_Dvdnd_Amt,"Units",
        		pnd_Inv_Acct_Unit_Qty,"AUV",
        		pnd_Inv_Acct_Unit_Value,"Total Pmt",
        		pnd_Inv_Acct_Settl_Amt);
        getReports().setDisplayColumns(3, "PIN",
        		gtn_Pda_E_Cntrct_Unq_Id_Nbr, new NumericLength (11),"Combine Nbr",
        		gtn_Pda_E_Cntrct_Cmbn_Nbr,"Contract",
        		gtn_Pda_E_Cntrct_Ppcn_Nbr,"Payee",
        		gtn_Pda_E_Cntrct_Payee_Cde, new AlphanumericLength (2),"Participant Name",
        		pnd_Name,"Payee Name and Address",
        		pnd_Payee_Addr,"Guar Amt",
        		pnd_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"Div Amt",
        		pnd_Div, new ReportEditMask ("ZZZ,ZZ9.99"),"Settl Amt",
        		pnd_Pmt, new ReportEditMask ("ZZZ,ZZ9.99"));
    }
}
