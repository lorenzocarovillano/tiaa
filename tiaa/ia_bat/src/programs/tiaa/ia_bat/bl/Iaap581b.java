/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:49 PM
**        * FROM NATURAL PROGRAM : Iaap581b
************************************************************
**        * FILE NAME            : Iaap581b.java
**        * CLASS NAME           : Iaap581b
**        * INSTANCE NAME        : Iaap581b
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP581B   READ WORK FILE (CREATED IN IAAP581S)   *
*      DATE     -  02/95      AND WRITE OUT INITIAL DEATH TRANS. FOR *
*    AUTHOR     -  ARI G.     THE SPECIFIC CHECK CYCLE. THIS REPORT  *
*                             IS FOR BPS - PAYMENT CONTROL (GAPS).   *
*                             IT CONTAINS CONTRACT SETTLEMENTS THAT  *
*                             HAVE FURTHUR PAYMENTS.                 *
*   HISTORY :                                                        *
*                                                                    *
*             10/10/2000  CHANGE TITLE OF REPORT - MAKE ONLY FOR     *
*                         SURVIVORS                                  *
*   04/2017 OS RE-STOWED FOR IAAL581A PIN EXPANSION.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap581b extends BLNatBase
{
    // Data Areas
    private LdaIaal581a ldaIaal581a;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal581a = new LdaIaal581a();
        registerRecord(ldaIaal581a);
        registerRecord(ldaIaal581a.getVw_iaa_Cntrl_Rcrd());
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal581a.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap581b() throws Exception
    {
        super("Iaap581b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "*******************************************",NEWLINE,"  START OF IAAP581B SURVIVOR REPORT PROGRAM",NEWLINE,"*******************************************"); //Natural: WRITE '*******************************************' / '  START OF IAAP581B SURVIVOR REPORT PROGRAM' / '*******************************************'
        if (Global.isEscape()) return;
        ldaIaal581a.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal581a.getVw_iaa_Cntrl_Rcrd().readNextRow("READ01")))
        {
            //*   MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE (EM=YYYYMMDD) TO
            //*               #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal581a.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal581a.getPnd_Work_Record())))
        {
            ldaIaal581a.getPnd_Work_Records_Read().nadd(1);                                                                                                               //Natural: ADD 1 TO #WORK-RECORDS-READ
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE1
            sub_Pnd_Setup_Line1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE2
            sub_Pnd_Setup_Line2();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-OTHER-LINES
            sub_Pnd_Setup_Other_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal581a.getPnd_Lines_Left().lessOrEqual(ldaIaal581a.getPnd_Lines_To_Print())))                                                              //Natural: IF #LINES-LEFT NOT > #LINES-TO-PRINT
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaIaal581a.getPnd_Line_1());                                                                              //Natural: WRITE ( 1 ) / #LINE-1
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal581a.getPnd_Line_2().notEquals(" ")))                                                                                                    //Natural: IF #LINE-2 NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal581a.getPnd_Line_2());                                                                                  //Natural: WRITE ( 1 ) #LINE-2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            F5:                                                                                                                                                           //Natural: FOR #I = 1 TO #OTHER-LINES
            for (ldaIaal581a.getPnd_I().setValue(1); condition(ldaIaal581a.getPnd_I().lessOrEqual(ldaIaal581a.getPnd_Other_Lines())); ldaIaal581a.getPnd_I().nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(ldaIaal581a.getPnd_I()));                      //Natural: WRITE ( 1 ) #TABLE-GROUP ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal581a.getPnd_Lines_Left().nsubtract(ldaIaal581a.getPnd_Lines_To_Print());                                                                               //Natural: SUBTRACT #LINES-TO-PRINT FROM #LINES-LEFT
                                                                                                                                                                          //Natural: PERFORM #RESET-FIELDS
            sub_Pnd_Reset_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #PRINT-FINAL-LINE
        sub_Pnd_Print_Final_Line();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "WORK FILE READS =====> ",ldaIaal581a.getPnd_Work_Records_Read());                                                                          //Natural: WRITE 'WORK FILE READS =====> ' #WORK-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "*****************************************",NEWLINE,"  END OF IAAP581B SURVIVOR REPORT PROGRAM",NEWLINE,"*****************************************"); //Natural: WRITE '*****************************************' / '  END OF IAAP581B SURVIVOR REPORT PROGRAM'/ '*****************************************'
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE1
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE2
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-OTHER-LINES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-FINAL-LINE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FIELDS
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Setup_Line1() throws Exception                                                                                                                   //Natural: #SETUP-LINE1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Name().setValue(DbsUtil.compress(ldaIaal581a.getPnd_Work_Record_Pnd_W_Last_Name(), ldaIaal581a.getPnd_Work_Record_Pnd_W_First_Name(),  //Natural: COMPRESS #W-LAST-NAME #W-FIRST-NAME #W-MIDDLE-NAME INTO #LN1-NAME
            ldaIaal581a.getPnd_Work_Record_Pnd_W_Middle_Name()));
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Cont_Stat().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Cont_Stat());                                                         //Natural: MOVE #W-CONT-STAT TO #LN1-CONT-STAT
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Annu_Type().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Annu_Type());                                                         //Natural: MOVE #W-ANNU-TYPE TO #LN1-ANNU-TYPE
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Cont_Mode().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Contract_Mode());                                                     //Natural: MOVE #W-CONTRACT-MODE TO #LN1-CONT-MODE
        if (condition(ldaIaal581a.getPnd_Work_Record_Pnd_W_Dod_Dte().notEquals(" ")))                                                                                     //Natural: IF #W-DOD-DTE NOT = ' '
        {
            ldaIaal581a.getPnd_Date_Ccyymmdd().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Dod_Dte());                                                                  //Natural: MOVE #W-DOD-DTE TO #DATE-CCYYMMDD
            ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Dod().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581a.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),              //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-YY INTO #LN1-DOD LEAVING NO
                "/", ldaIaal581a.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", ldaIaal581a.getPnd_Date_Ccyymmdd_Pnd_Date_Yy()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Dod().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-DOD
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_Notify_Name().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Name());                                                     //Natural: MOVE #W-NOTIFY-NAME TO #LN1-NOTIFY-NAME
        ldaIaal581a.getPnd_Line_1_Pnd_Ln1_User_Id().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_User_Id());                                                             //Natural: MOVE #W-USER-ID TO #LN1-USER-ID
        ldaIaal581a.getPnd_Lines_To_Print().nadd(2);                                                                                                                      //Natural: ADD 2 TO #LINES-TO-PRINT
    }
    private void sub_Pnd_Setup_Line2() throws Exception                                                                                                                   //Natural: #SETUP-LINE2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal581a.getPnd_Line_2_Pnd_Ln2_User_Area().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_User_Area());                                                         //Natural: MOVE #W-USER-AREA TO #LN2-USER-AREA
        ldaIaal581a.getPnd_Line_2_Pnd_Ln2_Pin().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Pin());                                                                     //Natural: MOVE #W-PIN TO #LN2-PIN
        ldaIaal581a.getPnd_Line_2_Pnd_Ln2_Notify_Addr1().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr1());                                                   //Natural: MOVE #W-NOTIFY-ADDR1 TO #LN2-NOTIFY-ADDR1
        if (condition(ldaIaal581a.getPnd_Line_2().notEquals(" ")))                                                                                                        //Natural: IF #LINE-2 NE ' '
        {
            ldaIaal581a.getPnd_Lines_To_Print().nadd(1);                                                                                                                  //Natural: ADD 1 TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Setup_Other_Lines() throws Exception                                                                                                             //Natural: #SETUP-OTHER-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal581a.getPnd_Work_Record_Pnd_W_Ssn().notEquals(" ")))                                                                                         //Natural: IF #W-SSN NE ' '
        {
            ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Field().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581a.getPnd_Work_Record_Pnd_Ssn_1_3(),  //Natural: COMPRESS #SSN-1-3 '-' #SSN-4-5 '-' #SSN-6-9 INTO #TAB-FIELD ( 1 ) LEAVING NO
                "-", ldaIaal581a.getPnd_Work_Record_Pnd_Ssn_4_5(), "-", ldaIaal581a.getPnd_Work_Record_Pnd_Ssn_6_9()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Field().getValue(1).setValue(" ");                                                                               //Natural: MOVE ' ' TO #TAB-FIELD ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet249 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #W-NOTIFY-ADDR2 NOT = ' '
        if (condition(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr2().notEquals(" ")))
        {
            decideConditionsMet249++;
            ldaIaal581a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Notify_Address().getValue(ldaIaal581a.getPnd_Corr_Count()).setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr2()); //Natural: MOVE #W-NOTIFY-ADDR2 TO #TAB-NOTIFY-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #W-NOTIFY-ADDR3 NOT = ' '
        if (condition(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr3().notEquals(" ")))
        {
            decideConditionsMet249++;
            ldaIaal581a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Notify_Address().getValue(ldaIaal581a.getPnd_Corr_Count()).setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr3()); //Natural: MOVE #W-NOTIFY-ADDR3 TO #TAB-NOTIFY-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #W-NOTIFY-ADDR4 NOT = ' '
        if (condition(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr4().notEquals(" ")))
        {
            decideConditionsMet249++;
            ldaIaal581a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Notify_Address().getValue(ldaIaal581a.getPnd_Corr_Count()).setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Addr4()); //Natural: MOVE #W-NOTIFY-ADDR4 TO #TAB-NOTIFY-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet249 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaal581a.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_City(), ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_State())); //Natural: COMPRESS #W-NOTIFY-CITY #W-NOTIFY-STATE INTO #CITY-STATE
        ldaIaal581a.getPnd_Zip_Breakdown().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Zip());                                                                   //Natural: MOVE #W-NOTIFY-ZIP TO #ZIP-BREAKDOWN
        if (condition(ldaIaal581a.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal581a.getPnd_Total_Zip().setValue(ldaIaal581a.getPnd_Work_Record_Pnd_W_Notify_Zip());                                                                   //Natural: MOVE #W-NOTIFY-ZIP TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581a.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581a.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal581a.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581a.getPnd_Corr_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #CORR-COUNT
        ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Tab_Notify_Address().getValue(ldaIaal581a.getPnd_Corr_Count()).setValue(DbsUtil.compress(ldaIaal581a.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-NOTIFY-ADDRESS ( #CORR-COUNT )
            ldaIaal581a.getPnd_Total_Zip()));
        ldaIaal581a.getPnd_Other_Lines().setValue(ldaIaal581a.getPnd_Corr_Count());                                                                                       //Natural: MOVE #CORR-COUNT TO #OTHER-LINES
        ldaIaal581a.getPnd_Lines_To_Print().nadd(ldaIaal581a.getPnd_Corr_Count());                                                                                        //Natural: ADD #CORR-COUNT TO #LINES-TO-PRINT
    }
    private void sub_Pnd_Print_Final_Line() throws Exception                                                                                                              //Natural: #PRINT-FINAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal581a.getPnd_Lines_Left().lessOrEqual(2)))                                                                                                    //Natural: IF #LINES-LEFT NOT > 2
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Fields() throws Exception                                                                                                                  //Natural: #RESET-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal581a.getPnd_Line_1().reset();                                                                                                                              //Natural: RESET #LINE-1 #LINE-2 #CORR-COUNT #LINES-TO-PRINT #OTHER-LINES #TABLE-GROUP ( 1:4 )
        ldaIaal581a.getPnd_Line_2().reset();
        ldaIaal581a.getPnd_Corr_Count().reset();
        ldaIaal581a.getPnd_Lines_To_Print().reset();
        ldaIaal581a.getPnd_Other_Lines().reset();
        ldaIaal581a.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(1,":",4).reset();
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal581a.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal581a.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal581a.getPnd_Page_Ctr().nadd(1);                                                                                                                //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(47),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 47T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR
                        TabSetting(119),"PAGE ",ldaIaal581a.getPnd_Page_Ctr());
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(37)," SURVIVOR FFP--INITIAL DEATH TRANSACTION FOR",             //Natural: WRITE ( 1 ) '   DATE ' *DATU 37T ' SURVIVOR FFP--INITIAL DEATH TRANSACTION FOR' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 'CHECK CYCLE'
                        ldaIaal581a.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),"CHECK CYCLE");
                    ldaIaal581a.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581a.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal581a.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", ldaIaal581a.getIaa_Parm_Card_Pnd_Parm_Date_Yy()));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(58)," FOR",new TabSetting(63),ldaIaal581a.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 58T ' FOR' 63T #W-PARM-DATE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NAME                                    ",new TabSetting(42)," CONT/STAT ",new          //Natural: WRITE ( 1 ) 001T 'NAME                                    ' 042T ' CONT/STAT ' 055T 'PY' 059T 'MODE' 064T ' DATE  ' 073T '            CONTACT                ' 110T 'OPERATOR' 120T '  COMMENTS'
                        TabSetting(55),"PY",new TabSetting(59),"MODE",new TabSetting(64)," DATE  ",new TabSetting(73),"            CONTACT                ",new 
                        TabSetting(110),"OPERATOR",new TabSetting(120),"  COMMENTS");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PIN",new TabSetting(65)," OF",new TabSetting(110),"ID/UNIT");                           //Natural: WRITE ( 1 ) 001T 'PIN' 065T ' OF' 110T 'ID/UNIT'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"SSN/EIN",new TabSetting(65),"DEATH");                                                   //Natural: WRITE ( 1 ) 001T 'SSN/EIN' 065T 'DEATH'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"----------------------------------------",new TabSetting(42),"-----------",new          //Natural: WRITE ( 1 ) 001T '----------------------------------------' 042T '-----------' 055T '--' 059T '----' 064T '--------' 073T '-----------------------------------' 110T '--------' 120T '-------------'
                        TabSetting(55),"--",new TabSetting(59),"----",new TabSetting(64),"--------",new TabSetting(73),"-----------------------------------",new 
                        TabSetting(110),"--------",new TabSetting(120),"-------------");
                    ldaIaal581a.getPnd_Lines_Left().setValue(48);                                                                                                         //Natural: ASSIGN #LINES-LEFT := 48
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
