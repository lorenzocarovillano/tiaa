/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:33 PM
**        * FROM NATURAL PROGRAM : Iaap391
************************************************************
**        * FILE NAME            : Iaap391.java
**        * CLASS NAME           : Iaap391
**        * INSTANCE NAME        : Iaap391
************************************************************
************************************************************************
* PROGRAM  : IAAP391
* DATE     : DECEMBER 16, 1997
* FUNCTION : THIS PROGRAM WILL PERFORM THE ANNUAL REVALUATION OF CREF
*            CONTRACTS.  IT WILL ALSO UPDATE THE CONTROL RECORD.
*
* MAINTENANCE LOG:
*
* PROGRAMMER        DATE        DESCRIPTION
*
* 1. JUN F. TINIO   12/16/1997  ORIGINAL CODE
*
* 2. JUN F. TINIO   09/21/1999  USE #IA-STD-NUM-CD WHEN IDENTIFYING THE
*                               OCCURRENCE OF FUND CODE IN THE TABLE
* 3.                04/13/2000  PA SELECT FUNDS CHANGED 'IAAN050A' TO
*                               'IAAN051A', 'IAAN0500' TO 'IAAN0510',
*                               'IAAA0500' TO 'IAAA0510'
*                                CHANGED ALL 40 OCCURS TO 80 OCCURS
* 4. T. DIAZ        04/29/02    RESTOWED FOR OIA CHANGES MADE IN
*                               IAAN0510
*
*                   04/2001    BYPASS SPIA FUND (U) ACTIVE YET 4/01
*                              NO UNIT VALUE ON PROD FILE AS PER
*                              ELLIOT REMOVE THIS BYPASS AFTER
*                              REVALUATION COMPLET FOR 2001
*                              DO SCAN ON 4/01
*                   04/2002    OIA CHANGED IAAN0510 IAAN051Z
*                              OIA CHANGED IAAA0510 IAAA051Z
*                   03/2012    RATE BASE EXPANSION - USE FULL BYTES IN
*                              THE REPORT TO PREVENT TRUNCATION.
*                              DO SCAN ON 3/12
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap391 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_Lst_Trans_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Next_Bus_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Trans_Cnt;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Iuv_Table;
    private DbsField pnd_Rep_Iuv;
    private DbsField pnd_Chng_Amt;
    private DbsField pnd_Tot_Old_Amt;
    private DbsField pnd_Tot_New_Amt;
    private DbsField pnd_Tot_Units;
    private DbsField pnd_Old_Amt;
    private DbsField pnd_Change;
    private DbsField pnd_Grand_Old;
    private DbsField pnd_Grand_New;
    private DbsField pnd_Grand_Chng;
    private DbsField pnd_Grand_Units;
    private DbsField pnd_Isn;
    private DbsField pnd_Call_Type;
    private DbsField pnd_Uv_Req_Dte_A;

    private DbsGroup pnd_Uv_Req_Dte_A__R_Field_2;
    private DbsField pnd_Uv_Req_Dte_A_Pnd_Uv_Req_Dte_N;
    private DbsField pnd_Issue_Dte_A;

    private DbsGroup pnd_Issue_Dte_A__R_Field_3;
    private DbsField pnd_Issue_Dte_A__Filler1;
    private DbsField pnd_Issue_Dte_A_Pnd_Issue_Mm;

    private DbsGroup pnd_Issue_Dte_A__R_Field_4;
    private DbsField pnd_Issue_Dte_A_Pnd_Issue_Dte_N;
    private DbsField pnd_Reval_Meth;
    private DbsField pnd_Desc;
    private DbsField pnd_Cmpny;
    private DbsField pnd_Len;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Per_10000;
    private DbsField pnd_Save_Cpr_Key;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;

    private DbsGroup pnd_Cntrct_Payee_Key_Pnd_Cp_Structure;
    private DbsField pnd_Cntrct_Payee_Key_Cref_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Cref_Cntrct_Payee_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables

        vw_iaa_Cref_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd", "IAA-CREF-FUND-RCRD"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Rcrd__R_Field_1 = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Cref_Fund_Rcrd_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_UNIT_VAL", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_OLD_PER_AMT", "CREF-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_DTE", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        registerRecord(vw_iaa_Cref_Fund_Rcrd);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Cntrl_Units = iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Cntrl_Amt = iaa_Cntrl_Rcrd_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Trans_Cnt = localVariables.newFieldInRecord("pnd_Trans_Cnt", "#TRANS-CNT", FieldType.INTEGER, 2);
        pnd_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_Iuv_Table = localVariables.newFieldArrayInRecord("pnd_Iuv_Table", "#IUV-TABLE", FieldType.PACKED_DECIMAL, 8, 4, new DbsArrayController(1, 80));
        pnd_Rep_Iuv = localVariables.newFieldArrayInRecord("pnd_Rep_Iuv", "#REP-IUV", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 80));
        pnd_Chng_Amt = localVariables.newFieldArrayInRecord("pnd_Chng_Amt", "#CHNG-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 80));
        pnd_Tot_Old_Amt = localVariables.newFieldArrayInRecord("pnd_Tot_Old_Amt", "#TOT-OLD-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            80));
        pnd_Tot_New_Amt = localVariables.newFieldArrayInRecord("pnd_Tot_New_Amt", "#TOT-NEW-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            80));
        pnd_Tot_Units = localVariables.newFieldArrayInRecord("pnd_Tot_Units", "#TOT-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 
            80));
        pnd_Old_Amt = localVariables.newFieldInRecord("pnd_Old_Amt", "#OLD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Change = localVariables.newFieldInRecord("pnd_Change", "#CHANGE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grand_Old = localVariables.newFieldInRecord("pnd_Grand_Old", "#GRAND-OLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Grand_New = localVariables.newFieldInRecord("pnd_Grand_New", "#GRAND-NEW", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Grand_Chng = localVariables.newFieldInRecord("pnd_Grand_Chng", "#GRAND-CHNG", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Grand_Units = localVariables.newFieldInRecord("pnd_Grand_Units", "#GRAND-UNITS", FieldType.PACKED_DECIMAL, 12, 3);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Call_Type = localVariables.newFieldInRecord("pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        pnd_Uv_Req_Dte_A = localVariables.newFieldInRecord("pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        pnd_Uv_Req_Dte_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Uv_Req_Dte_A__R_Field_2", "REDEFINE", pnd_Uv_Req_Dte_A);
        pnd_Uv_Req_Dte_A_Pnd_Uv_Req_Dte_N = pnd_Uv_Req_Dte_A__R_Field_2.newFieldInGroup("pnd_Uv_Req_Dte_A_Pnd_Uv_Req_Dte_N", "#UV-REQ-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Issue_Dte_A = localVariables.newFieldInRecord("pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 8);

        pnd_Issue_Dte_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Issue_Dte_A__R_Field_3", "REDEFINE", pnd_Issue_Dte_A);
        pnd_Issue_Dte_A__Filler1 = pnd_Issue_Dte_A__R_Field_3.newFieldInGroup("pnd_Issue_Dte_A__Filler1", "_FILLER1", FieldType.STRING, 4);
        pnd_Issue_Dte_A_Pnd_Issue_Mm = pnd_Issue_Dte_A__R_Field_3.newFieldInGroup("pnd_Issue_Dte_A_Pnd_Issue_Mm", "#ISSUE-MM", FieldType.NUMERIC, 2);

        pnd_Issue_Dte_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Issue_Dte_A__R_Field_4", "REDEFINE", pnd_Issue_Dte_A);
        pnd_Issue_Dte_A_Pnd_Issue_Dte_N = pnd_Issue_Dte_A__R_Field_4.newFieldInGroup("pnd_Issue_Dte_A_Pnd_Issue_Dte_N", "#ISSUE-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Reval_Meth = localVariables.newFieldInRecord("pnd_Reval_Meth", "#REVAL-METH", FieldType.STRING, 1);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 35);
        pnd_Cmpny = localVariables.newFieldInRecord("pnd_Cmpny", "#CMPNY", FieldType.STRING, 4);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Per_10000 = localVariables.newFieldInRecord("pnd_Per_10000", "#PER-10000", FieldType.PACKED_DECIMAL, 7);
        pnd_Save_Cpr_Key = localVariables.newFieldInRecord("pnd_Save_Cpr_Key", "#SAVE-CPR-KEY", FieldType.STRING, 12);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);

        pnd_Cntrct_Payee_Key_Pnd_Cp_Structure = pnd_Cntrct_Payee_Key__R_Field_5.newGroupInGroup("pnd_Cntrct_Payee_Key_Pnd_Cp_Structure", "#CP-STRUCTURE");
        pnd_Cntrct_Payee_Key_Cref_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key_Pnd_Cp_Structure.newFieldInGroup("pnd_Cntrct_Payee_Key_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Cref_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key_Pnd_Cp_Structure.newFieldInGroup("pnd_Cntrct_Payee_Key_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cref_Fund_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        localVariables.reset();
        pnd_Call_Type.setInitialValue("P");
        pnd_Reval_Meth.setInitialValue("A");
        pnd_Len.setInitialValue(6);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap391() throws Exception
    {
        super("Iaap391");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP391", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "PRT01");                                                                                                                           //Natural: DEFINE PRINTER ( PRT01 = 01 ) OUTPUT 'CMPRT01'
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: ON ERROR
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR-FOR-CREF-FUNDS
        sub_Get_Factor_For_Cref_Funds();
        if (condition(Global.isEscape())) {return;}
        vw_iaa_Cref_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY
        (
        "RF",
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RF:
        while (condition(vw_iaa_Cref_Fund_Rcrd.readNextRow("RF")))
        {
            pnd_Trans_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TRANS-CNT
            pnd_Per_10000.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PER-10000
            //*  WRITE CONTRACT NBR AND PAYEE CODE
            if (condition(pnd_Per_10000.greaterOrEqual(10000)))                                                                                                           //Natural: IF #PER-10000 GE 10000
            {
                pnd_Per_10000.reset();                                                                                                                                    //Natural: RESET #PER-10000
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Cntrct",iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"Payee",iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde,      //Natural: WRITE 'Cntrct' IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR ( AL = 8 ) 'Payee' IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE ( EM = 99 )
                    new ReportEditMask ("99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Trans_Cnt.greaterOrEqual(100)))                                                                                                             //Natural: IF #TRANS-CNT GE 100
            {
                pnd_Trans_Cnt.reset();                                                                                                                                    //Natural: RESET #TRANS-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde.equals("2") || iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde.equals("U"))))                                             //Natural: ACCEPT IF IAA-CREF-FUND-RCRD.CREF-CMPNY-CDE = '2' OR = 'U'
            {
                continue;
            }
            pnd_Cntrct_Payee_Key_Pnd_Cp_Structure.setValuesByName(vw_iaa_Cref_Fund_Rcrd);                                                                                 //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO #CP-STRUCTURE
            if (condition(pnd_Cntrct_Payee_Key.notEquals(pnd_Save_Cpr_Key)))                                                                                              //Natural: IF #CNTRCT-PAYEE-KEY NE #SAVE-CPR-KEY
            {
                pnd_Save_Cpr_Key.setValue(pnd_Cntrct_Payee_Key);                                                                                                          //Natural: ASSIGN #SAVE-CPR-KEY := #CNTRCT-PAYEE-KEY
                                                                                                                                                                          //Natural: PERFORM GET-CPR-STATUS
                sub_Get_Cpr_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  09/21/1999
            DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(iaa_Cref_Fund_Rcrd_Cref_Fund_Cde,         //Natural: EXAMINE FULL #IA-STD-NM-CD ( * ) FOR FULL IAA-CREF-FUND-RCRD.CREF-FUND-CDE GIVING INDEX #I
                true), new ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                G1:                                                                                                                                                       //Natural: GET IAA-CREF-FUND-RCRD *ISN ( RF. )
                vw_iaa_Cref_Fund_Rcrd.readByID(vw_iaa_Cref_Fund_Rcrd.getAstISN("RF"), "G1");
                iaa_Cref_Fund_Rcrd_Cref_Unit_Val.setValue(pnd_Iuv_Table.getValue(pnd_I));                                                                                 //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-UNIT-VAL := #IUV-TABLE ( #I )
                pnd_Old_Amt.setValue(iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt);                                                                                                //Natural: ASSIGN #OLD-AMT := IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT
                iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt.compute(new ComputeParameters(true, iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt), iaa_Cref_Fund_Rcrd_Cref_Units_Cnt.getValue(1).multiply(pnd_Iuv_Table.getValue(pnd_I))); //Natural: COMPUTE ROUNDED IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT = IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) * #IUV-TABLE ( #I )
                pnd_Change.compute(new ComputeParameters(false, pnd_Change), iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt.subtract(pnd_Old_Amt));                                  //Natural: ASSIGN #CHANGE := IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT - #OLD-AMT
                vw_iaa_Cref_Fund_Rcrd.updateDBRow("G1");                                                                                                                  //Natural: UPDATE ( G1. )
                pnd_Rec_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-CNT
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                       //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.examine(new ExamineSource(iaa_Cntrl_Rcrd_Cntrl_Fund_Cde.getValue("*"),true), new ExamineSearch(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde,        //Natural: EXAMINE FULL IAA-CNTRL-RCRD.CNTRL-FUND-CDE ( * ) FOR FULL IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE GIVING INDEX #J
                        true), new ExamineGivingIndex(pnd_J));
                    if (condition(pnd_J.greater(getZero())))                                                                                                              //Natural: IF #J GT 0
                    {
                        pnd_Fund_Cde.getValue(pnd_J).setValue(iaa_Cntrl_Rcrd_Cntrl_Fund_Cde.getValue(pnd_J).getSubstring(2,2));                                           //Natural: ASSIGN #FUND-CDE ( #J ) := SUBSTR ( IAA-CNTRL-RCRD.CNTRL-FUND-CDE ( #J ) ,2,2 )
                        pnd_Rep_Iuv.getValue(pnd_J).setValue(pnd_Iuv_Table.getValue(pnd_I));                                                                              //Natural: ASSIGN #REP-IUV ( #J ) := #IUV-TABLE ( #I )
                        pnd_Chng_Amt.getValue(pnd_J).nadd(pnd_Change);                                                                                                    //Natural: ADD #CHANGE TO #CHNG-AMT ( #J )
                        pnd_Tot_Old_Amt.getValue(pnd_J).nadd(pnd_Old_Amt);                                                                                                //Natural: ADD #OLD-AMT TO #TOT-OLD-AMT ( #J )
                        pnd_Tot_New_Amt.getValue(pnd_J).nadd(iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt);                                                                        //Natural: ADD IAA-CREF-FUND-RCRD.CREF-TOT-PER-AMT TO #TOT-NEW-AMT ( #J )
                        pnd_Tot_Units.getValue(pnd_J).nadd(iaa_Cref_Fund_Rcrd_Cref_Units_Cnt.getValue("*"));                                                              //Natural: ADD IAA-CREF-FUND-RCRD.CREF-UNITS-CNT ( * ) TO #TOT-UNITS ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        G2:                                                                                                                                                               //Natural: GET IAA-CNTRL-RCRD #ISN
        vw_iaa_Cntrl_Rcrd.readByID(pnd_Isn.getLong(), "G2");
        iaa_Cntrl_Rcrd_Cntrl_Amt.getValue("*").nadd(pnd_Chng_Amt.getValue("*"));                                                                                          //Natural: ADD #CHNG-AMT ( * ) TO IAA-CNTRL-RCRD.CNTRL-AMT ( * )
        vw_iaa_Cntrl_Rcrd.updateDBRow("G2");                                                                                                                              //Natural: UPDATE ( G2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FACTOR-FOR-CREF-FUNDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-STATUS
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 80
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            if (condition(pnd_Fund_Cde.getValue(pnd_I).greater(" ")))                                                                                                     //Natural: IF #FUND-CDE ( #I ) GT ' '
            {
                //*  3/12
                //*  3/12
                //*  3/12
                //*  3/12
                DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Cde.getValue(pnd_I), pnd_Desc, pnd_Cmpny, pnd_Len);                                   //Natural: CALLNAT 'IAAN051A' #FUND-CDE ( #I ) #DESC #CMPNY #LEN
                if (condition(Global.isEscape())) return;
                getReports().display(1, "Fund",                                                                                                                           //Natural: DISPLAY ( 1 ) 'Fund' #DESC ( AL = 6 ) 'Before' #TOT-OLD-AMT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 2X 'After' #TOT-NEW-AMT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 2X 'Change' #CHNG-AMT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99- ) 3X 'Units' #TOT-UNITS ( #I ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 'Unit Value' #REP-IUV ( #I ) ( EM = ZZ,ZZ9.9999 )
                		pnd_Desc, new AlphanumericLength (6),"Before",
                		pnd_Tot_Old_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"After",
                		pnd_Tot_New_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Change",
                		pnd_Chng_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(3),"Units",
                		pnd_Tot_Units.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),"Unit Value",
                		pnd_Rep_Iuv.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.9999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  3/12
            //*  3/12
            //*  3/12
            //*  3/12
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Grand_New.compute(new ComputeParameters(false, pnd_Grand_New), DbsField.add(getZero(),pnd_Tot_New_Amt.getValue("*")));                                        //Natural: ASSIGN #GRAND-NEW := 0 + #TOT-NEW-AMT ( * )
        pnd_Grand_Old.compute(new ComputeParameters(false, pnd_Grand_Old), DbsField.add(getZero(),pnd_Tot_Old_Amt.getValue("*")));                                        //Natural: ASSIGN #GRAND-OLD := 0 + #TOT-OLD-AMT ( * )
        pnd_Grand_Chng.compute(new ComputeParameters(false, pnd_Grand_Chng), DbsField.add(getZero(),pnd_Chng_Amt.getValue("*")));                                         //Natural: ASSIGN #GRAND-CHNG := 0 + #CHNG-AMT ( * )
        pnd_Grand_Units.compute(new ComputeParameters(false, pnd_Grand_Units), DbsField.add(getZero(),pnd_Tot_Units.getValue("*")));                                      //Natural: ASSIGN #GRAND-UNITS := 0 + #TOT-UNITS ( * )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(80),NEWLINE,"Total",new TabSetting(8),pnd_Grand_Old, new ReportEditMask                     //Natural: WRITE ( 1 ) / '-' ( 80 ) / 'Total' 8T #GRAND-OLD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 28T #GRAND-NEW ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 46T #GRAND-CHNG ( EM = ZZ,ZZZ,ZZZ,ZZ9.99- ) 65T #GRAND-UNITS ( EM = ZZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(28),pnd_Grand_New, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(46),pnd_Grand_Chng, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(65),pnd_Grand_Units, new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(0, "Total number of records processed:",pnd_Rec_Cnt);                                                                                          //Natural: WRITE 'Total number of records processed:' #REC-CNT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "R1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("R1")))
        {
            pnd_Uv_Req_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #UV-REQ-DTE-A
            pnd_Issue_Dte_A.setValue(pnd_Uv_Req_Dte_A);                                                                                                                   //Natural: ASSIGN #ISSUE-DTE-A := #UV-REQ-DTE-A
            if (condition(DbsUtil.maskMatches(pnd_Uv_Req_Dte_A,"....'05'")))                                                                                              //Natural: IF #UV-REQ-DTE-A = MASK ( ....'05' )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*********************************************************",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ", //Natural: WRITE ( 1 ) / '*********************************************************' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '                                                         ' / '                                                         ' / '  CONTROL DATE IS: ' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE / '                                                         ' / '  CONTROL DATE NOT A REVALUATION PERIOD FOR CREF FUNDS   ' / '                                                         ' / '                                                         ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '*********************************************************'
                    NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"                                                         ",
                    NEWLINE,"                                                         ",NEWLINE,"  CONTROL DATE IS: ",iaa_Cntrl_Rcrd_Cntrl_Check_Dte,NEWLINE,
                    "                                                         ",NEWLINE,"  CONTROL DATE NOT A REVALUATION PERIOD FOR CREF FUNDS   ",NEWLINE,
                    "                                                         ",NEWLINE,"                                                         ",NEWLINE,
                    "  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,
                    "*********************************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(66);  if (true) return;                                                                                                                 //Natural: TERMINATE 66
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "R2",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R2:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("R2")))
        {
            if (condition(iaa_Cntrl_Rcrd_Cntrl_Cde.equals("DC")))                                                                                                         //Natural: IF IAA-CNTRL-RCRD.CNTRL-CDE = 'DC'
            {
                pnd_Isn.setValue(vw_iaa_Cntrl_Rcrd.getAstISN("R2"));                                                                                                      //Natural: ASSIGN #ISN := *ISN ( R2. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*********************************************************",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ", //Natural: WRITE ( 1 ) / '*********************************************************' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '                                                         ' / '                                                         ' / '  NO DC CONTROL RECORD ON FILE' / '                                                         ' / '                                                         ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '*********************************************************'
                    NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"                                                         ",
                    NEWLINE,"                                                         ",NEWLINE,"  NO DC CONTROL RECORD ON FILE",NEWLINE,"                                                         ",
                    NEWLINE,"                                                         ",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",
                    NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"*********************************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(66);  if (true) return;                                                                                                                 //Natural: TERMINATE 66
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Factor_For_Cref_Funds() throws Exception                                                                                                         //Natural: GET-FACTOR-FOR-CREF-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CREF FUNDS START FROM OCCURRENCE 4
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Issue_Dte_A_Pnd_Issue_Mm.setValue(4);                                                                                                                         //Natural: ASSIGN #ISSUE-MM := 04
        FOR02:                                                                                                                                                            //Natural: FOR #I 4 TO 40
        for (pnd_I.setValue(4); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            //*  FOLLOWING TO BYPASS SPIA FUND NOT ACTIVE YET 4/01 REMVE AFTR RUN
            //*  RAY TOLD ME TO COMMENT IT OUT ON 5/01/2002
            //*   IF IAAA051Z.#IA-STD-ALPHA-CD(#I) = 'U'
            //*     WRITE    ' FUND CODE --> '#IA-STD-ALPHA-CD(#I)
            //*     ' <-- BYPASSED NO FACTOR AVAILABLE' /
            //*      ' NO FACTOR AVAILABLE FOR -->'  PEC-ACCT-NAME-6(#I)
            //*     ESCAPE TOP
            //*   END-IF
            //*  END OF BYPASS  4/01
            if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I).equals(" ")))                                                                     //Natural: IF #IA-STD-ALPHA-CD ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Iaan390.class , getCurrentProcessState(), pnd_Call_Type, pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I), pnd_Reval_Meth,       //Natural: CALLNAT 'IAAN390' #CALL-TYPE #IA-STD-ALPHA-CD ( #I ) #REVAL-METH #UV-REQ-DTE-N #ISSUE-DTE-N #IUV-TABLE ( #I )
                pnd_Uv_Req_Dte_A_Pnd_Uv_Req_Dte_N, pnd_Issue_Dte_A_Pnd_Issue_Dte_N, pnd_Iuv_Table.getValue(pnd_I));
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Iuv_Table.getValue(pnd_I).equals(getZero())))                                                                                               //Natural: IF #IUV-TABLE ( #I ) = 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*********************************************************",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ", //Natural: WRITE ( 1 ) / '*********************************************************' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '                                                         ' / '                                                         ' / '  NO FACTOR AVAILABLE FOR FUND:' #IA-STD-ALPHA-CD ( #I ) / '  FOR FACTOR DATE:' #UV-REQ-DTE-N / '                                                         ' / '  REVALUATION CANNOT BE PERFORMED.  NO FACTOR AVAILABLE  ' / '                                                         ' / '                                                         ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ' / '*********************************************************'
                    NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"                                                         ",
                    NEWLINE,"                                                         ",NEWLINE,"  NO FACTOR AVAILABLE FOR FUND:",pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I),
                    NEWLINE,"  FOR FACTOR DATE:",pnd_Uv_Req_Dte_A_Pnd_Uv_Req_Dte_N,NEWLINE,"                                                         ",NEWLINE,
                    "  REVALUATION CANNOT BE PERFORMED.  NO FACTOR AVAILABLE  ",NEWLINE,"                                                         ",NEWLINE,
                    "                                                         ",NEWLINE,"  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,
                    "  ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR  ",NEWLINE,"*********************************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(66);  if (true) return;                                                                                                                 //Natural: TERMINATE 66
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr_Status() throws Exception                                                                                                                    //Natural: GET-CPR-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("F1")))
        {
            vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(20),"Transaction Report",new ColumnSpacing(20),"Date:",Global.getDATX(),NEWLINE,Global.getTIMX(),new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 20X 'Transaction Report' 20X 'Date:' *DATX / *TIMX 17X 'CREF Revaluation - Annual' 16X 'Page:' *PAGE-NUMBER ( 1 ) /
                        ColumnSpacing(17),"CREF Revaluation - Annual",new ColumnSpacing(16),"Page:",getReports().getPageNumberDbs(1),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "************************ E R R O R ***********************",NEWLINE,NEWLINE,"Error Number:",Global.getERROR_NR(),"Error Line  :",          //Natural: WRITE '************************ E R R O R ***********************' // 'Error Number:' *ERROR-NR 'Error Line  :' *ERROR-LINE 'Program     :' *PROGRAM // '************************ E R R O R ***********************'
            Global.getERROR_LINE(),"Program     :",Global.getPROGRAM(),NEWLINE,NEWLINE,"************************ E R R O R ***********************");
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        DbsUtil.terminate(66);  if (true) return;                                                                                                                         //Natural: TERMINATE 66
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "Fund",
        		pnd_Desc, new AlphanumericLength (6),"Before",
        		pnd_Tot_Old_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"After",
        		pnd_Tot_New_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Change",
        		pnd_Chng_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(3),"Units",
        		pnd_Tot_Units, new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),"Unit Value",
        		pnd_Rep_Iuv, new ReportEditMask ("ZZ,ZZ9.9999"));
    }
}
