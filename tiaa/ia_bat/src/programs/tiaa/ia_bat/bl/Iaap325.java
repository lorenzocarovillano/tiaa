/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:45 PM
**        * FROM NATURAL PROGRAM : Iaap325
************************************************************
**        * FILE NAME            : Iaap325.java
**        * CLASS NAME           : Iaap325
**        * INSTANCE NAME        : Iaap325
************************************************************
************************************************************************
*
* PROGRAM: IAAP325
* DESC   : WRITES TRANSACTION RECORDS TO WORK FILES USED FOR
*            REPORT. (IAAP795
*
*
* HISTORY:
* --------
*  6/02     MADE CHANGE IN ROUTINE #WRITE-RECORD
*           TO CORRECT FINAL-PMMT & DIV-AMTS FOR 50 TRANS MODE CHANGE
*           DO SCAN ON 6/02
*  1/02     MADE CHANGE IN ROUTINE IAA-FUND-TRANS-BEFORE-AFTER
*           TO CORRECT FINAL-PMMT & DIV-AMTS FOR 60 TRANS
*           DO SCAN ON 1/02
*  6/99     ADDED TIAA TO CREF TRANS (512-518)
*  9/98     MADE CHANGE IN ROUTINE IAA-FUND-TRANS-BEFORE-AFTER
*           DO SCAN ON 9/98
*  3/03     INCREASE TIAA RATE FROM 1:40 TO 1:60
*           DO SCAN ON 3/03 FOR CHANGES
*  4/08     INCREASE TIAA RATE FROM 1:90 TO 1:99 AND ADDED ORIGIN CODE
*           DO SCAN ON 4/08 FOR CHANGES
*  3/12     INCREASE TIAA RATE FROM 99 TO 250
*           DO SCAN ON 3/12 FOR CHANGES
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap325 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Inactive_Cpr;
    private DbsField pnd_Bi_Not_On_File;
    private DbsField pnd_Cnt_Read;
    private DbsField pnd_Cnt_Accept;
    private DbsField pnd_Cnt_60;
    private DbsField pnd_Cnt_500;
    private DbsField pnd_Cnt_06;
    private DbsField pnd_Cnt_40;
    private DbsField pnd_Cnt_30;
    private DbsField pnd_Cnt_31;
    private DbsField pnd_Cnt_33;
    private DbsField pnd_Cnt_50;
    private DbsField pnd_Cnt_31r;
    private DbsField pnd_Cnt_33r;
    private DbsField pnd_Cnt_30r;
    private DbsField pnd_Cnt_66r;
    private DbsField pnd_Cnt_102r;
    private DbsField pnd_Cnt_Write;
    private DbsField pnd_Special_Table_Mode;
    private DbsField pnd_U;
    private DbsField pnd_Hold_Trans_Dte;
    private DbsField pnd_Special_Records_Read;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;

    private DbsGroup iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Product_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Trans__R_Field_2;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;

    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans1;
    private DbsField iaa_Tiaa_Fund_Trans1_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans1_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans1_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Trans1__R_Field_3;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Product_Cde;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Div_Amt;

    private DbsGroup iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_I;
    private DbsField pnd_Check_Date_Ccyymmdd_A;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_4;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_5;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd;
    private DbsField pnd_W_Trans_Dte;
    private DbsField pnd_W_Trans_Dte_Next;
    private DbsField pnd_W_Trans_Input_Dte;
    private DbsField pnd_W_Date_4;

    private DbsGroup pnd_W_Date_4__R_Field_6;
    private DbsField pnd_W_Date_4_Pnd_W_Date_3;
    private DbsField pnd_W_Date_4_Pnd_W_Date_1;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Check_Dte;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Dte;
    private DbsField pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Sub_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Actvty_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Sub_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Pnd_O_Trans_Payee_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Product_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Work_Record_Pnd_O_Cref_Units_Cnt;
    private DbsField pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt;
    private DbsField pnd_Work_Record_Pnd_O_Critical_Ind;
    private DbsField pnd_Work_Record_Pnd_O_Company_Cde;
    private DbsField pnd_Work_Record_Pnd_O_Final_Payment;
    private DbsField pnd_Work_Record_Pnd_O_Final_Dividend;
    private DbsField pnd_Work_Record_Pnd_O_B_Final_Payment;
    private DbsField pnd_Work_Record_Pnd_O_B_Final_Dividend;
    private DbsField pnd_Ccyymmdd;
    private DbsField pnd_W_Cntrct_Key_A;

    private DbsGroup pnd_W_Cntrct_Key_A__R_Field_7;
    private DbsField pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Imge_Id_A;
    private DbsField pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_A;
    private DbsField pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Invrse_Trans_Dte_A;
    private DbsField pnd_W_Cntrct_Key_B;

    private DbsGroup pnd_W_Cntrct_Key_B__R_Field_8;
    private DbsField pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Imge_Id_B;
    private DbsField pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_B;
    private DbsField pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Trans_Dte_B;
    private DbsField pnd_W_Cpr_Key;

    private DbsGroup pnd_W_Cpr_Key__R_Field_9;
    private DbsField pnd_W_Cpr_Key_Pnd_W_Cpr_Imge_Id;
    private DbsField pnd_W_Cpr_Key_Pnd_W_Cpr_Cntrct_Ppcn_Nbr;
    private DbsField pnd_W_Cpr_Key_Pnd_W_Cpr_Part_Payee_Cde;
    private DbsField pnd_W_Cpr_Key_Pnd_W_Cpr_Trans_Dte;
    private DbsField pnd_W_Cpr_Key_A;

    private DbsGroup pnd_W_Cpr_Key_A__R_Field_10;
    private DbsField pnd_W_Cpr_Key_A_Pnd_W_Cpr_Imge_Id_A;
    private DbsField pnd_W_Cpr_Key_A_Pnd_W_Cpr_Cntrct_Ppcn_Nbr_A;
    private DbsField pnd_W_Cpr_Key_A_Pnd_W_Cpr_Part_Payee_Cde_A;
    private DbsField pnd_W_Cpr_Key_A_Pnd_W_Cpr_Inv_Trans_Dte;
    private DbsField pnd_W_Prtcpnt_Key;

    private DbsGroup pnd_W_Prtcpnt_Key__R_Field_11;
    private DbsField pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Cntrct_Ppcn_Nbr;
    private DbsField pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Part_Payee_Cde;
    private DbsField pnd_W_B_Fund_Key;

    private DbsGroup pnd_W_B_Fund_Key__R_Field_12;
    private DbsField pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id;
    private DbsField pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr;
    private DbsField pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde;
    private DbsField pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte;
    private DbsField pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde;
    private DbsField pnd_W_A_Fund_Key;

    private DbsGroup pnd_W_A_Fund_Key__R_Field_13;
    private DbsField pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id;
    private DbsField pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr;
    private DbsField pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde;
    private DbsField pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte;
    private DbsField pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde;
    private DbsField pnd_W_Fund_Rcrd_Key;

    private DbsGroup pnd_W_Fund_Rcrd_Key__R_Field_14;
    private DbsField pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Part_Payee_Cde;
    private DbsField pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cmpny_Cde;
    private DbsField pnd_Fund_Fnd_Sw;
    private DbsField pnd_Fund_Aftr_Fnd_Sw;
    private DbsField pnd_Fund_Bfr_Fnd_Sw;
    private DbsField pnd_W_Count;
    private DbsField pnd_W_Bypass_Count;
    private DbsField pnd_Count;
    private DbsField pnd_W_Record_Count;
    private DbsField pnd_W_Record_Count_Spec;
    private DbsField pnd_W_Cntrl_Trans_Dte;
    private DbsField pnd_W_Special_Sw;
    private DbsField pnd_W_Print;

    private DbsGroup pnd_Save_Keys;
    private DbsField pnd_Save_Keys_Sve_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Save_Keys_Sve_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup pnd_Save_Keys__R_Field_15;
    private DbsField pnd_Save_Keys_Sve_Tiaa_Fund_Cde;
    private DbsField pnd_Save_Keys_Sve_Tiaa_Product_Cde;
    private DbsField pnd_Save_Keys_Sve_Invrse_Trans_Dte;
    private DbsField pnd_I2;
    private DbsField pnd_Tcnt;
    private DbsField pnd_W_First_Loop;
    private DbsField pnd_W_Hold_Invrse_Date;
    private DbsField pnd_W_Hold_Invrse_Trans_Dte;
    private DbsField pnd_W_Hold_Trans_Sub_Cde;
    private DbsField pnd_H_Tiaa_Tot_Fin_Pmt;
    private DbsField pnd_H_Tiaa_Tot_Fin_Div;
    private DbsField pnd_H_Tiaa_Tot_Per_Amt;
    private DbsField pnd_H_Tiaa_Tot_Div_Amt;
    private DbsField pnd_H_Cref_Units_Cnt;
    private DbsField pnd_W_Cntrct_Mode_Ind;
    private DbsField pnd_H_Cntrct_Mode_Ind;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_16;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_17;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_18;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_19;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_20;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_21;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Inactive_Cpr = localVariables.newFieldInRecord("pnd_Inactive_Cpr", "#INACTIVE-CPR", FieldType.STRING, 1);
        pnd_Bi_Not_On_File = localVariables.newFieldInRecord("pnd_Bi_Not_On_File", "#BI-NOT-ON-FILE", FieldType.STRING, 1);
        pnd_Cnt_Read = localVariables.newFieldInRecord("pnd_Cnt_Read", "#CNT-READ", FieldType.NUMERIC, 6);
        pnd_Cnt_Accept = localVariables.newFieldInRecord("pnd_Cnt_Accept", "#CNT-ACCEPT", FieldType.NUMERIC, 6);
        pnd_Cnt_60 = localVariables.newFieldInRecord("pnd_Cnt_60", "#CNT-60", FieldType.NUMERIC, 6);
        pnd_Cnt_500 = localVariables.newFieldInRecord("pnd_Cnt_500", "#CNT-500", FieldType.NUMERIC, 6);
        pnd_Cnt_06 = localVariables.newFieldInRecord("pnd_Cnt_06", "#CNT-06", FieldType.NUMERIC, 6);
        pnd_Cnt_40 = localVariables.newFieldInRecord("pnd_Cnt_40", "#CNT-40", FieldType.NUMERIC, 6);
        pnd_Cnt_30 = localVariables.newFieldInRecord("pnd_Cnt_30", "#CNT-30", FieldType.NUMERIC, 6);
        pnd_Cnt_31 = localVariables.newFieldInRecord("pnd_Cnt_31", "#CNT-31", FieldType.NUMERIC, 6);
        pnd_Cnt_33 = localVariables.newFieldInRecord("pnd_Cnt_33", "#CNT-33", FieldType.NUMERIC, 6);
        pnd_Cnt_50 = localVariables.newFieldInRecord("pnd_Cnt_50", "#CNT-50", FieldType.NUMERIC, 6);
        pnd_Cnt_31r = localVariables.newFieldInRecord("pnd_Cnt_31r", "#CNT-31R", FieldType.NUMERIC, 6);
        pnd_Cnt_33r = localVariables.newFieldInRecord("pnd_Cnt_33r", "#CNT-33R", FieldType.NUMERIC, 6);
        pnd_Cnt_30r = localVariables.newFieldInRecord("pnd_Cnt_30r", "#CNT-30R", FieldType.NUMERIC, 6);
        pnd_Cnt_66r = localVariables.newFieldInRecord("pnd_Cnt_66r", "#CNT-66R", FieldType.NUMERIC, 6);
        pnd_Cnt_102r = localVariables.newFieldInRecord("pnd_Cnt_102r", "#CNT-102R", FieldType.NUMERIC, 6);
        pnd_Cnt_Write = localVariables.newFieldInRecord("pnd_Cnt_Write", "#CNT-WRITE", FieldType.NUMERIC, 6);
        pnd_Special_Table_Mode = localVariables.newFieldArrayInRecord("pnd_Special_Table_Mode", "#SPECIAL-TABLE-MODE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            22));
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.NUMERIC, 2);
        pnd_Hold_Trans_Dte = localVariables.newFieldInRecord("pnd_Hold_Trans_Dte", "#HOLD-TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Special_Records_Read = localVariables.newFieldInRecord("pnd_Special_Records_Read", "#SPECIAL-RECORDS-READ", FieldType.NUMERIC, 2);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        registerRecord(vw_iaa_Cntrct);

        iaa_Trans_Rcrd = localVariables.newGroupInRecord("iaa_Trans_Rcrd", "IAA-TRANS-RCRD");
        iaa_Trans_Rcrd_Trans_Dte = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME);
        iaa_Trans_Rcrd_Invrse_Trans_Dte = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        iaa_Trans_Rcrd_Lst_Trans_Dte = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10);
        iaa_Trans_Rcrd_Trans_Payee_Cde = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        iaa_Trans_Rcrd_Trans_Sub_Cde = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3);
        iaa_Trans_Rcrd_Trans_Actvty_Cde = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 1);
        iaa_Trans_Rcrd_Trans_Cde = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3);
        iaa_Trans_Rcrd_Trans_Check_Dte = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        iaa_Trans_Rcrd_Trans_Todays_Dte = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8);
        iaa_Trans_Rcrd_Trans_Verify_Cde = iaa_Trans_Rcrd.newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 1);

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        registerRecord(vw_iaa_Cntrct_Trans);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_iaa_Cpr_Trans);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Product_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Product_Cde", "TIAA-PRODUCT-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Trans__R_Field_2 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans__R_Field_2", "REDEFINE", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde = iaa_Tiaa_Fund_Trans__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde", "TIAA-PRODUCT-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");

        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Trans);

        vw_iaa_Tiaa_Fund_Trans1 = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans1", "IAA-TIAA-FUND-TRANS1"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans1_Trans_Dte = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans1_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS1_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans1_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans1_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans1_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans1_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS1_TIAA_CNTRCT_PAYEE_CDE", 
            "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans1_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Trans1__R_Field_3 = vw_iaa_Tiaa_Fund_Trans1.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans1__R_Field_3", "REDEFINE", iaa_Tiaa_Fund_Trans1_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans1__R_Field_3.newFieldInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Trans1_Tiaa_Product_Cde = iaa_Tiaa_Fund_Trans1__R_Field_3.newFieldInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Product_Cde", "TIAA-PRODUCT-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS1_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans1.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS1_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");

        iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans1.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Trans1);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_Check_Date_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd_A", "#CHECK-DATE-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_4", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd = pnd_Check_Date_Ccyymmdd_A__R_Field_4.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd", 
            "#CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_5", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm = pnd_Check_Date_Ccyymmdd_A__R_Field_5.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm", 
            "#CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd = pnd_Check_Date_Ccyymmdd_A__R_Field_5.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd", 
            "#CHECK-DATE-DD", FieldType.NUMERIC, 2);
        pnd_W_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Dte", "#W-TRANS-DTE", FieldType.STRING, 8);
        pnd_W_Trans_Dte_Next = localVariables.newFieldInRecord("pnd_W_Trans_Dte_Next", "#W-TRANS-DTE-NEXT", FieldType.STRING, 8);
        pnd_W_Trans_Input_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Input_Dte", "#W-TRANS-INPUT-DTE", FieldType.STRING, 8);
        pnd_W_Date_4 = localVariables.newFieldInRecord("pnd_W_Date_4", "#W-DATE-4", FieldType.STRING, 4);

        pnd_W_Date_4__R_Field_6 = localVariables.newGroupInRecord("pnd_W_Date_4__R_Field_6", "REDEFINE", pnd_W_Date_4);
        pnd_W_Date_4_Pnd_W_Date_3 = pnd_W_Date_4__R_Field_6.newFieldInGroup("pnd_W_Date_4_Pnd_W_Date_3", "#W-DATE-3", FieldType.STRING, 3);
        pnd_W_Date_4_Pnd_W_Date_1 = pnd_W_Date_4__R_Field_6.newFieldInGroup("pnd_W_Date_4_Pnd_W_Date_1", "#W-DATE-1", FieldType.STRING, 1);

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Pnd_O_Trans_Check_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Check_Dte", "#O-TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_Pnd_O_Trans_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Dte", "#O-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind", "#O-CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_Pnd_O_Trans_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Cde", "#O-TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Work_Record_Pnd_O_Sub_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Sub_Cde", "#O-SUB-CDE", FieldType.STRING, 3);
        pnd_Work_Record_Pnd_O_Trans_Actvty_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Actvty_Cde", "#O-TRANS-ACTVTY-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde", "#O-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte", "#O-CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_Pnd_O_Trans_Sub_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Sub_Cde", "#O-TRANS-SUB-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr", "#O-TRANS-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_Pnd_O_Trans_Payee_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Trans_Payee_Cde", "#O-TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_O_Product_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Product_Cde", "#O-PRODUCT-CDE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt", "#O-TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt", "#O-TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_Pnd_O_Cref_Units_Cnt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Cref_Units_Cnt", "#O-CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt", "#O-B-TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt", "#O-B-TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt", "#O-B-CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Work_Record_Pnd_O_Critical_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Critical_Ind", "#O-CRITICAL-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_O_Company_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Company_Cde", "#O-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_O_Final_Payment = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Final_Payment", "#O-FINAL-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_Pnd_O_Final_Dividend = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_Final_Dividend", "#O-FINAL-DIVIDEND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_Pnd_O_B_Final_Payment = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_B_Final_Payment", "#O-B-FINAL-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_Pnd_O_B_Final_Dividend = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_O_B_Final_Dividend", "#O-B-FINAL-DIVIDEND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ccyymmdd = localVariables.newFieldInRecord("pnd_Ccyymmdd", "#CCYYMMDD", FieldType.STRING, 8);
        pnd_W_Cntrct_Key_A = localVariables.newFieldInRecord("pnd_W_Cntrct_Key_A", "#W-CNTRCT-KEY-A", FieldType.STRING, 23);

        pnd_W_Cntrct_Key_A__R_Field_7 = localVariables.newGroupInRecord("pnd_W_Cntrct_Key_A__R_Field_7", "REDEFINE", pnd_W_Cntrct_Key_A);
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Imge_Id_A = pnd_W_Cntrct_Key_A__R_Field_7.newFieldInGroup("pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Imge_Id_A", "#W-CNTRCT-IMGE-ID-A", 
            FieldType.STRING, 1);
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_A = pnd_W_Cntrct_Key_A__R_Field_7.newFieldInGroup("pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_A", 
            "#W-CNTRCT-CNTRCT-PPCN-NBR-A", FieldType.STRING, 10);
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Invrse_Trans_Dte_A = pnd_W_Cntrct_Key_A__R_Field_7.newFieldInGroup("pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Invrse_Trans_Dte_A", 
            "#W-CNTRCT-INVRSE-TRANS-DTE-A", FieldType.NUMERIC, 12);
        pnd_W_Cntrct_Key_B = localVariables.newFieldInRecord("pnd_W_Cntrct_Key_B", "#W-CNTRCT-KEY-B", FieldType.STRING, 18);

        pnd_W_Cntrct_Key_B__R_Field_8 = localVariables.newGroupInRecord("pnd_W_Cntrct_Key_B__R_Field_8", "REDEFINE", pnd_W_Cntrct_Key_B);
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Imge_Id_B = pnd_W_Cntrct_Key_B__R_Field_8.newFieldInGroup("pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Imge_Id_B", "#W-CNTRCT-IMGE-ID-B", 
            FieldType.STRING, 1);
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_B = pnd_W_Cntrct_Key_B__R_Field_8.newFieldInGroup("pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_B", 
            "#W-CNTRCT-CNTRCT-PPCN-NBR-B", FieldType.STRING, 10);
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Trans_Dte_B = pnd_W_Cntrct_Key_B__R_Field_8.newFieldInGroup("pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Trans_Dte_B", "#W-CNTRCT-TRANS-DTE-B", 
            FieldType.TIME);
        pnd_W_Cpr_Key = localVariables.newFieldInRecord("pnd_W_Cpr_Key", "#W-CPR-KEY", FieldType.STRING, 20);

        pnd_W_Cpr_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_W_Cpr_Key__R_Field_9", "REDEFINE", pnd_W_Cpr_Key);
        pnd_W_Cpr_Key_Pnd_W_Cpr_Imge_Id = pnd_W_Cpr_Key__R_Field_9.newFieldInGroup("pnd_W_Cpr_Key_Pnd_W_Cpr_Imge_Id", "#W-CPR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_W_Cpr_Key_Pnd_W_Cpr_Cntrct_Ppcn_Nbr = pnd_W_Cpr_Key__R_Field_9.newFieldInGroup("pnd_W_Cpr_Key_Pnd_W_Cpr_Cntrct_Ppcn_Nbr", "#W-CPR-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_W_Cpr_Key_Pnd_W_Cpr_Part_Payee_Cde = pnd_W_Cpr_Key__R_Field_9.newFieldInGroup("pnd_W_Cpr_Key_Pnd_W_Cpr_Part_Payee_Cde", "#W-CPR-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_W_Cpr_Key_Pnd_W_Cpr_Trans_Dte = pnd_W_Cpr_Key__R_Field_9.newFieldInGroup("pnd_W_Cpr_Key_Pnd_W_Cpr_Trans_Dte", "#W-CPR-TRANS-DTE", FieldType.TIME);
        pnd_W_Cpr_Key_A = localVariables.newFieldInRecord("pnd_W_Cpr_Key_A", "#W-CPR-KEY-A", FieldType.STRING, 25);

        pnd_W_Cpr_Key_A__R_Field_10 = localVariables.newGroupInRecord("pnd_W_Cpr_Key_A__R_Field_10", "REDEFINE", pnd_W_Cpr_Key_A);
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Imge_Id_A = pnd_W_Cpr_Key_A__R_Field_10.newFieldInGroup("pnd_W_Cpr_Key_A_Pnd_W_Cpr_Imge_Id_A", "#W-CPR-IMGE-ID-A", FieldType.STRING, 
            1);
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Cntrct_Ppcn_Nbr_A = pnd_W_Cpr_Key_A__R_Field_10.newFieldInGroup("pnd_W_Cpr_Key_A_Pnd_W_Cpr_Cntrct_Ppcn_Nbr_A", "#W-CPR-CNTRCT-PPCN-NBR-A", 
            FieldType.STRING, 10);
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Part_Payee_Cde_A = pnd_W_Cpr_Key_A__R_Field_10.newFieldInGroup("pnd_W_Cpr_Key_A_Pnd_W_Cpr_Part_Payee_Cde_A", "#W-CPR-PART-PAYEE-CDE-A", 
            FieldType.NUMERIC, 2);
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Inv_Trans_Dte = pnd_W_Cpr_Key_A__R_Field_10.newFieldInGroup("pnd_W_Cpr_Key_A_Pnd_W_Cpr_Inv_Trans_Dte", "#W-CPR-INV-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_W_Prtcpnt_Key = localVariables.newFieldInRecord("pnd_W_Prtcpnt_Key", "#W-PRTCPNT-KEY", FieldType.STRING, 12);

        pnd_W_Prtcpnt_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_W_Prtcpnt_Key__R_Field_11", "REDEFINE", pnd_W_Prtcpnt_Key);
        pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Cntrct_Ppcn_Nbr = pnd_W_Prtcpnt_Key__R_Field_11.newFieldInGroup("pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Cntrct_Ppcn_Nbr", 
            "#W-PRTCPNT-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Part_Payee_Cde = pnd_W_Prtcpnt_Key__R_Field_11.newFieldInGroup("pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Part_Payee_Cde", 
            "#W-PRTCPNT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_W_B_Fund_Key = localVariables.newFieldInRecord("pnd_W_B_Fund_Key", "#W-B-FUND-KEY", FieldType.STRING, 23);

        pnd_W_B_Fund_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_W_B_Fund_Key__R_Field_12", "REDEFINE", pnd_W_B_Fund_Key);
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id = pnd_W_B_Fund_Key__R_Field_12.newFieldInGroup("pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id", "#W-B-FUND-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr = pnd_W_B_Fund_Key__R_Field_12.newFieldInGroup("pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr", 
            "#W-B-FUND-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde = pnd_W_B_Fund_Key__R_Field_12.newFieldInGroup("pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde", "#W-B-FUND-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte = pnd_W_B_Fund_Key__R_Field_12.newFieldInGroup("pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte", "#W-B-FUND-TRANS-DTE", 
            FieldType.TIME);
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde = pnd_W_B_Fund_Key__R_Field_12.newFieldInGroup("pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde", "#W-B-FUND-CMPNY-CDE", 
            FieldType.STRING, 3);
        pnd_W_A_Fund_Key = localVariables.newFieldInRecord("pnd_W_A_Fund_Key", "#W-A-FUND-KEY", FieldType.STRING, 28);

        pnd_W_A_Fund_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_W_A_Fund_Key__R_Field_13", "REDEFINE", pnd_W_A_Fund_Key);
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id = pnd_W_A_Fund_Key__R_Field_13.newFieldInGroup("pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id", "#W-A-FUND-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr = pnd_W_A_Fund_Key__R_Field_13.newFieldInGroup("pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr", 
            "#W-A-FUND-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde = pnd_W_A_Fund_Key__R_Field_13.newFieldInGroup("pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde", "#W-A-FUND-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte = pnd_W_A_Fund_Key__R_Field_13.newFieldInGroup("pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte", 
            "#W-A-FUND-INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde = pnd_W_A_Fund_Key__R_Field_13.newFieldInGroup("pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde", "#W-A-FUND-CMPNY-CDE", 
            FieldType.STRING, 3);
        pnd_W_Fund_Rcrd_Key = localVariables.newFieldInRecord("pnd_W_Fund_Rcrd_Key", "#W-FUND-RCRD-KEY", FieldType.STRING, 15);

        pnd_W_Fund_Rcrd_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_W_Fund_Rcrd_Key__R_Field_14", "REDEFINE", pnd_W_Fund_Rcrd_Key);
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cntrct_Ppcn_Nbr = pnd_W_Fund_Rcrd_Key__R_Field_14.newFieldInGroup("pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cntrct_Ppcn_Nbr", 
            "#W-FUND-RCRD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Part_Payee_Cde = pnd_W_Fund_Rcrd_Key__R_Field_14.newFieldInGroup("pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Part_Payee_Cde", 
            "#W-FUND-RCRD-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cmpny_Cde = pnd_W_Fund_Rcrd_Key__R_Field_14.newFieldInGroup("pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cmpny_Cde", 
            "#W-FUND-RCRD-CMPNY-CDE", FieldType.STRING, 3);
        pnd_Fund_Fnd_Sw = localVariables.newFieldInRecord("pnd_Fund_Fnd_Sw", "#FUND-FND-SW", FieldType.STRING, 1);
        pnd_Fund_Aftr_Fnd_Sw = localVariables.newFieldInRecord("pnd_Fund_Aftr_Fnd_Sw", "#FUND-AFTR-FND-SW", FieldType.STRING, 1);
        pnd_Fund_Bfr_Fnd_Sw = localVariables.newFieldInRecord("pnd_Fund_Bfr_Fnd_Sw", "#FUND-BFR-FND-SW", FieldType.STRING, 1);
        pnd_W_Count = localVariables.newFieldInRecord("pnd_W_Count", "#W-COUNT", FieldType.NUMERIC, 9);
        pnd_W_Bypass_Count = localVariables.newFieldInRecord("pnd_W_Bypass_Count", "#W-BYPASS-COUNT", FieldType.NUMERIC, 9);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 9);
        pnd_W_Record_Count = localVariables.newFieldInRecord("pnd_W_Record_Count", "#W-RECORD-COUNT", FieldType.NUMERIC, 3);
        pnd_W_Record_Count_Spec = localVariables.newFieldInRecord("pnd_W_Record_Count_Spec", "#W-RECORD-COUNT-SPEC", FieldType.NUMERIC, 3);
        pnd_W_Cntrl_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Cntrl_Trans_Dte", "#W-CNTRL-TRANS-DTE", FieldType.STRING, 8);
        pnd_W_Special_Sw = localVariables.newFieldInRecord("pnd_W_Special_Sw", "#W-SPECIAL-SW", FieldType.STRING, 1);
        pnd_W_Print = localVariables.newFieldInRecord("pnd_W_Print", "#W-PRINT", FieldType.STRING, 1);

        pnd_Save_Keys = localVariables.newGroupInRecord("pnd_Save_Keys", "#SAVE-KEYS");
        pnd_Save_Keys_Sve_Tiaa_Cntrct_Ppcn_Nbr = pnd_Save_Keys.newFieldArrayInGroup("pnd_Save_Keys_Sve_Tiaa_Cntrct_Ppcn_Nbr", "SVE-TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Save_Keys_Sve_Tiaa_Cntrct_Payee_Cde = pnd_Save_Keys.newFieldArrayInGroup("pnd_Save_Keys_Sve_Tiaa_Cntrct_Payee_Cde", "SVE-TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1, 20));
        pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde = pnd_Save_Keys.newFieldArrayInGroup("pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde", "SVE-TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1, 20));

        pnd_Save_Keys__R_Field_15 = pnd_Save_Keys.newGroupInGroup("pnd_Save_Keys__R_Field_15", "REDEFINE", pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde);
        pnd_Save_Keys_Sve_Tiaa_Fund_Cde = pnd_Save_Keys__R_Field_15.newFieldArrayInGroup("pnd_Save_Keys_Sve_Tiaa_Fund_Cde", "SVE-TIAA-FUND-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20));
        pnd_Save_Keys_Sve_Tiaa_Product_Cde = pnd_Save_Keys__R_Field_15.newFieldArrayInGroup("pnd_Save_Keys_Sve_Tiaa_Product_Cde", "SVE-TIAA-PRODUCT-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Save_Keys_Sve_Invrse_Trans_Dte = pnd_Save_Keys.newFieldArrayInGroup("pnd_Save_Keys_Sve_Invrse_Trans_Dte", "SVE-INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, new DbsArrayController(1, 20));
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 4);
        pnd_Tcnt = localVariables.newFieldInRecord("pnd_Tcnt", "#TCNT", FieldType.INTEGER, 4);
        pnd_W_First_Loop = localVariables.newFieldInRecord("pnd_W_First_Loop", "#W-FIRST-LOOP", FieldType.STRING, 1);
        pnd_W_Hold_Invrse_Date = localVariables.newFieldInRecord("pnd_W_Hold_Invrse_Date", "#W-HOLD-INVRSE-DATE", FieldType.NUMERIC, 12);
        pnd_W_Hold_Invrse_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Hold_Invrse_Trans_Dte", "#W-HOLD-INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        pnd_W_Hold_Trans_Sub_Cde = localVariables.newFieldInRecord("pnd_W_Hold_Trans_Sub_Cde", "#W-HOLD-TRANS-SUB-CDE", FieldType.STRING, 3);
        pnd_H_Tiaa_Tot_Fin_Pmt = localVariables.newFieldInRecord("pnd_H_Tiaa_Tot_Fin_Pmt", "#H-TIAA-TOT-FIN-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_H_Tiaa_Tot_Fin_Div = localVariables.newFieldInRecord("pnd_H_Tiaa_Tot_Fin_Div", "#H-TIAA-TOT-FIN-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_H_Tiaa_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_H_Tiaa_Tot_Per_Amt", "#H-TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_H_Tiaa_Tot_Div_Amt = localVariables.newFieldInRecord("pnd_H_Tiaa_Tot_Div_Amt", "#H-TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_H_Cref_Units_Cnt = localVariables.newFieldInRecord("pnd_H_Cref_Units_Cnt", "#H-CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_W_Cntrct_Mode_Ind = localVariables.newFieldInRecord("pnd_W_Cntrct_Mode_Ind", "#W-CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        pnd_H_Cntrct_Mode_Ind = localVariables.newFieldInRecord("pnd_H_Cntrct_Mode_Ind", "#H-CNTRCT-MODE-IND", FieldType.NUMERIC, 3);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_16", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_16.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_16.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_17", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_18 = pnd_Save_Check_Dte_A__R_Field_17.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_18", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_19 = pnd_Save_Check_Dte_A__R_Field_18.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_19", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_19.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_19.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_20 = pnd_Save_Check_Dte_A__R_Field_18.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_20", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_20.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_21 = pnd_Save_Check_Dte_A__R_Field_17.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_21", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_21.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Tiaa_Fund_Trans1.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrl_Rcrd.reset();

        localVariables.reset();
        pnd_W_Print.setInitialValue("Y");
        pnd_W_First_Loop.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap325() throws Exception
    {
        super("Iaap325");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP325", onError);
        //*  ======================================================================
        //*                          START OF PROGRAM
        //*  ======================================================================
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Save_Check_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ==================
        //*   READ WORK FILE 3
        //*  ==================
        pnd_Ccyymmdd.setValue(Global.getDATN());                                                                                                                          //Natural: MOVE *DATN TO #CCYYMMDD
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 03 IAA-TRANS-RCRD
        while (condition(getWorkFiles().read(3, iaa_Trans_Rcrd)))
        {
            pnd_Cnt_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-READ
            pnd_W_Trans_Input_Dte.setValueEdited(iaa_Trans_Rcrd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-DTE ( EM = YYYYMMDD ) TO #W-TRANS-INPUT-DTE
            if (condition(iaa_Trans_Rcrd_Trans_Verify_Cde.equals("V") || ! (iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("A") || iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")    //Natural: IF TRANS-VERIFY-CDE = 'V' OR NOT ( TRANS-ACTVTY-CDE = 'A' OR = 'R' OR = ' ' )
                || iaa_Trans_Rcrd_Trans_Actvty_Cde.equals(" "))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(31) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                            //Natural: IF TRANS-CDE = 031 AND TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cnt_31r.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-31R
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(33) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                            //Natural: IF TRANS-CDE = 033 AND TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cnt_33r.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-33R
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(30) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                            //Natural: IF TRANS-CDE = 030 AND TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cnt_30r.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-30R
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(66) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                            //Natural: IF TRANS-CDE = 066 AND TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cnt_66r.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-66R
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(102) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                           //Natural: IF TRANS-CDE = 102 AND TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cnt_102r.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CNT-102R
            }                                                                                                                                                             //Natural: END-IF
            //*  MAIN SELECTION OF TRANSACTIONS TO SELECT FOR PROCESSING
            if (condition((((((((((((((((((((((((((((iaa_Trans_Rcrd_Trans_Cde.equals(20) || iaa_Trans_Rcrd_Trans_Cde.equals(31)) || iaa_Trans_Rcrd_Trans_Cde.equals(33))  //Natural: IF ( ( TRANS-CDE = 020 OR = 031 OR = 033 OR = 035 OR = 037 OR = 040 OR = 050 OR = 066 OR = 006 OR = 500 OR = 502 OR = 504 OR = 506 OR = 512 OR = 514 OR = 516 OR = 518 OR = 508 OR = 051 OR = 052 OR = 053 OR = 036 OR = 030 OR = 060 OR = 061 OR = 062 OR = 520 ) OR ( TRANS-CDE = 102 AND TRANS-ACTVTY-CDE = 'R' ) )
                || iaa_Trans_Rcrd_Trans_Cde.equals(35)) || iaa_Trans_Rcrd_Trans_Cde.equals(37)) || iaa_Trans_Rcrd_Trans_Cde.equals(40)) || iaa_Trans_Rcrd_Trans_Cde.equals(50)) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(66)) || iaa_Trans_Rcrd_Trans_Cde.equals(6)) || iaa_Trans_Rcrd_Trans_Cde.equals(500)) || iaa_Trans_Rcrd_Trans_Cde.equals(502)) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(504)) || iaa_Trans_Rcrd_Trans_Cde.equals(506)) || iaa_Trans_Rcrd_Trans_Cde.equals(512)) || iaa_Trans_Rcrd_Trans_Cde.equals(514)) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(516)) || iaa_Trans_Rcrd_Trans_Cde.equals(518)) || iaa_Trans_Rcrd_Trans_Cde.equals(508)) || iaa_Trans_Rcrd_Trans_Cde.equals(51)) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(52)) || iaa_Trans_Rcrd_Trans_Cde.equals(53)) || iaa_Trans_Rcrd_Trans_Cde.equals(36)) || iaa_Trans_Rcrd_Trans_Cde.equals(30)) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(60)) || iaa_Trans_Rcrd_Trans_Cde.equals(61)) || iaa_Trans_Rcrd_Trans_Cde.equals(62)) || iaa_Trans_Rcrd_Trans_Cde.equals(520)) 
                || (iaa_Trans_Rcrd_Trans_Cde.equals(102) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))))
            {
                //*  6/99 KN
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt_Accept.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-ACCEPT
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD
            pnd_W_Cntrct_Mode_Ind.reset();                                                                                                                                //Natural: RESET #W-CNTRCT-MODE-IND
            pnd_Work_Record_Pnd_O_Trans_Check_Dte.setValue(iaa_Trans_Rcrd_Trans_Check_Dte);                                                                               //Natural: MOVE TRANS-CHECK-DTE TO #O-TRANS-CHECK-DTE #HOLD-TRANS-DTE
            pnd_Hold_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Check_Dte);
            pnd_Work_Record_Pnd_O_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                           //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #O-TRANS-DTE
            pnd_Work_Record_Pnd_O_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                           //Natural: MOVE TRANS-CDE TO #O-TRANS-CDE
            pnd_Work_Record_Pnd_O_Sub_Cde.setValue(iaa_Trans_Rcrd_Trans_Sub_Cde);                                                                                         //Natural: MOVE TRANS-SUB-CDE TO #O-SUB-CDE
            pnd_Work_Record_Pnd_O_Trans_Sub_Cde.setValue(iaa_Trans_Rcrd_Trans_Sub_Cde);                                                                                   //Natural: MOVE TRANS-SUB-CDE TO #O-TRANS-SUB-CDE
            pnd_Work_Record_Pnd_O_Trans_Actvty_Cde.setValue(iaa_Trans_Rcrd_Trans_Actvty_Cde);                                                                             //Natural: MOVE TRANS-ACTVTY-CDE TO #O-TRANS-ACTVTY-CDE
            pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                 //Natural: MOVE TRANS-PPCN-NBR TO #O-TRANS-PPCN-NBR
            pnd_Work_Record_Pnd_O_Trans_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                               //Natural: MOVE TRANS-PAYEE-CDE TO #O-TRANS-PAYEE-CDE
            pnd_Work_Record_Pnd_O_Critical_Ind.setValue("2");                                                                                                             //Natural: MOVE '2' TO #O-CRITICAL-IND
            short decideConditionsMet376 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE TRANS-CDE;//Natural: VALUE 030
            if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(30))))
            {
                decideConditionsMet376++;
                pnd_Cnt_30.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-30
            }                                                                                                                                                             //Natural: VALUE 031
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(31))))
            {
                decideConditionsMet376++;
                pnd_Cnt_31.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-31
            }                                                                                                                                                             //Natural: VALUE 033
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(33))))
            {
                decideConditionsMet376++;
                pnd_Cnt_33.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-33
            }                                                                                                                                                             //Natural: VALUE 050
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(50))))
            {
                decideConditionsMet376++;
                pnd_Cnt_50.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-50
            }                                                                                                                                                             //Natural: VALUE 006
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(6))))
            {
                decideConditionsMet376++;
                pnd_Cnt_06.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-06
            }                                                                                                                                                             //Natural: VALUE 060
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(60))))
            {
                decideConditionsMet376++;
                pnd_Cnt_60.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-60
            }                                                                                                                                                             //Natural: VALUE 500 : 520
            else if (condition(((iaa_Trans_Rcrd_Trans_Cde.greaterOrEqual(500) && iaa_Trans_Rcrd_Trans_Cde.lessOrEqual(520)))))
            {
                decideConditionsMet376++;
                pnd_Cnt_500.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-500
            }                                                                                                                                                             //Natural: VALUE 040
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(40))))
            {
                decideConditionsMet376++;
                pnd_Cnt_40.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-40
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  MAIN PROCESSING LOGIC FOR CREATING TRANSACTION RECORDS
            short decideConditionsMet399 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE TRANS-CDE;//Natural: VALUES 030, 031, 033, 035, 036, 053
            if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(30) || iaa_Trans_Rcrd_Trans_Cde.equals(31) || iaa_Trans_Rcrd_Trans_Cde.equals(33) || iaa_Trans_Rcrd_Trans_Cde.equals(35) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(36) || iaa_Trans_Rcrd_Trans_Cde.equals(53))))
            {
                decideConditionsMet399++;
                if (condition((((iaa_Trans_Rcrd_Trans_Cde.equals(30) || iaa_Trans_Rcrd_Trans_Cde.equals(31)) || iaa_Trans_Rcrd_Trans_Cde.equals(33)) &&                   //Natural: IF TRANS-CDE = 030 OR = 031 OR = 033 AND ( TRANS-ACTVTY-CDE = 'R' )
                    iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R"))))
                {
                    pnd_W_First_Loop.setValue("Y");                                                                                                                       //Natural: ASSIGN #W-FIRST-LOOP := 'Y'
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE
                    sub_Iaa_Cpr_Trans_Before();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  CPR BEFORE NOT FOUND
                    if (condition(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.equals(getZero())))                                                                               //Natural: IF #O-CNTRCT-MODE-IND = 0
                    {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                        sub_Iaa_Cpr_Trans_After();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                    sub_Iaa_Cntrct_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-AFTER-31R
                    sub_Iaa_Fund_Trans_After_31r();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                    sub_Iaa_Cpr_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                    sub_Iaa_Cntrct_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-AFTER
                    sub_Iaa_Fund_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 037
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(37))))
            {
                decideConditionsMet399++;
                pnd_W_Record_Count.reset();                                                                                                                               //Natural: RESET #W-RECORD-COUNT
                pnd_Fund_Fnd_Sw.reset();                                                                                                                                  //Natural: RESET #FUND-FND-SW
                if (condition(((iaa_Trans_Rcrd_Trans_Cde.equals(37)) && iaa_Trans_Rcrd_Trans_Sub_Cde.equals("   "))))                                                     //Natural: IF ( ( TRANS-CDE = 037 ) AND TRANS-SUB-CDE = '   ' )
                {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                    sub_Iaa_Cpr_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                    sub_Iaa_Cntrct_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-BEFORE
                    sub_Iaa_Fund_Trans_Before();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Fund_Fnd_Sw.notEquals("Y")))                                                                                                        //Natural: IF #FUND-FND-SW NE 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM READ-IAA-TIAA-FUND-RCRD
                        sub_Read_Iaa_Tiaa_Fund_Rcrd();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                        sub_Pnd_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  OUT CYLCLE
                if (condition(((iaa_Trans_Rcrd_Trans_Cde.equals(37)) && iaa_Trans_Rcrd_Trans_Sub_Cde.equals("37B"))))                                                     //Natural: IF ( ( TRANS-CDE = 037 ) AND TRANS-SUB-CDE = '37B' )
                {
                    //*  REVERSAL
                    pnd_W_Record_Count.reset();                                                                                                                           //Natural: RESET #W-RECORD-COUNT
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE
                    sub_Iaa_Cpr_Trans_Before();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  CPR BEFORE NOT FOUND
                    if (condition(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.equals(getZero())))                                                                               //Natural: IF #O-CNTRCT-MODE-IND = 0
                    {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                        sub_Iaa_Cpr_Trans_After();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-BEFORE
                    sub_Iaa_Cntrct_Trans_Before();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WHERE CPR
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-BEFORE-37B
                    sub_Iaa_Fund_Trans_Before_37b();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IS DROPPED
                    if (condition(pnd_W_Record_Count.equals(getZero())))                                                                                                  //Natural: IF #W-RECORD-COUNT = 0
                    {
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-AFTER-37B
                        sub_Iaa_Fund_Trans_After_37b();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                    sub_Pnd_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES 060,061,062
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(60) || iaa_Trans_Rcrd_Trans_Cde.equals(61) || iaa_Trans_Rcrd_Trans_Cde.equals(62))))
            {
                decideConditionsMet399++;
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE
                sub_Iaa_Cpr_Trans_Before();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                sub_Iaa_Cpr_Trans_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                sub_Iaa_Cntrct_Trans_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-BEFORE-AFTER
                sub_Iaa_Fund_Trans_Before_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(iaa_Trans_Rcrd_Trans_Cde.notEquals(60)))                                                                                                    //Natural: IF TRANS-CDE NE 060
                {
                    if (condition(pnd_Fund_Fnd_Sw.notEquals("Y") || pnd_Fund_Bfr_Fnd_Sw.equals("Y")))                                                                     //Natural: IF #FUND-FND-SW NE 'Y' OR #FUND-BFR-FND-SW = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM READ-IAA-TIAA-FUND-RCRD
                        sub_Read_Iaa_Tiaa_Fund_Rcrd();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADDED 102 4/98
                }                                                                                                                                                         //Natural: END-IF
                //*  ==========>
            }                                                                                                                                                             //Natural: VALUES 051, 050, 102, 500:520
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(51) || iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(102) || 
                (iaa_Trans_Rcrd_Trans_Cde.greaterOrEqual(500) && iaa_Trans_Rcrd_Trans_Cde.lessOrEqual(520)))))
            {
                decideConditionsMet399++;
                //*  ==========>
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE
                sub_Iaa_Cpr_Trans_Before();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                sub_Iaa_Cpr_Trans_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                sub_Iaa_Cntrct_Trans_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-FUND-TRANS-BEFORE-2
                sub_Read_Fund_Trans_Before_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #READ-AFTER-FND-TRAN
                sub_Pnd_Read_After_Fnd_Tran();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fund_Aftr_Fnd_Sw.notEquals("Y") && pnd_Fund_Bfr_Fnd_Sw.notEquals("Y")))                                                                 //Natural: IF #FUND-AFTR-FND-SW NE 'Y' AND #FUND-BFR-FND-SW NE 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM READ-IAA-TIAA-FUND-RCRD
                    sub_Read_Iaa_Tiaa_Fund_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  REMOVED 102 4/98
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES 066,052,020
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(66) || iaa_Trans_Rcrd_Trans_Cde.equals(52) || iaa_Trans_Rcrd_Trans_Cde.equals(20))))
            {
                decideConditionsMet399++;
                //*  LEFT
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE-A
                sub_Iaa_Cpr_Trans_Before_A();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CPR BEFORE NOT FOUND
                if (condition(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.equals(getZero())))                                                                                   //Natural: IF #O-CNTRCT-MODE-IND = 0
                {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-AFTER
                    sub_Iaa_Cpr_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-BEFORE
                sub_Iaa_Cntrct_Trans_Before();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-BEFORE-A
                sub_Iaa_Fund_Trans_Before_A();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fund_Fnd_Sw.notEquals("Y")))                                                                                                            //Natural: IF #FUND-FND-SW NE 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-AFTER
                    sub_Iaa_Fund_Trans_After();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Fnd_Sw.notEquals("Y")))                                                                                                            //Natural: IF #FUND-FND-SW NE 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM READ-IAA-TIAA-FUND-RCRD
                    sub_Read_Iaa_Tiaa_Fund_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES 006,040
            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(6) || iaa_Trans_Rcrd_Trans_Cde.equals(40))))
            {
                decideConditionsMet399++;
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-BEFORE-A
                sub_Iaa_Cpr_Trans_Before_A();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Inactive_Cpr.notEquals("Y")))                                                                                                           //Natural: IF #INACTIVE-CPR NE 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-BEFORE
                    sub_Iaa_Cntrct_Trans_Before();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM IAA-FUND-TRANS-BEFORE-A
                    sub_Iaa_Fund_Trans_Before_A();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  --------------------------------------------------------------------
        getReports().write(0, "WORK RECORDS READ =========> ",pnd_Cnt_Read);                                                                                              //Natural: WRITE 'WORK RECORDS READ =========> ' #CNT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "WORK RECORDS ACCEPTED =====> ",pnd_Cnt_Accept);                                                                                            //Natural: WRITE 'WORK RECORDS ACCEPTED =====> ' #CNT-ACCEPT
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 60 ===========> ",pnd_Cnt_60);                                                                                                //Natural: WRITE 'RECORD TRANS 60 ===========> ' #CNT-60
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 500 to 520 ===> ",pnd_Cnt_500);                                                                                               //Natural: WRITE 'RECORD TRANS 500 to 520 ===> ' #CNT-500
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 06 ===========> ",pnd_Cnt_06);                                                                                                //Natural: WRITE 'RECORD TRANS 06 ===========> ' #CNT-06
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 40 ===========> ",pnd_Cnt_40);                                                                                                //Natural: WRITE 'RECORD TRANS 40 ===========> ' #CNT-40
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 30 ===========> ",pnd_Cnt_30);                                                                                                //Natural: WRITE 'RECORD TRANS 30 ===========> ' #CNT-30
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 30R===========> ",pnd_Cnt_30r);                                                                                               //Natural: WRITE 'RECORD TRANS 30R===========> ' #CNT-30R
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 31 ===========> ",pnd_Cnt_31);                                                                                                //Natural: WRITE 'RECORD TRANS 31 ===========> ' #CNT-31
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 31R ==========> ",pnd_Cnt_31r);                                                                                               //Natural: WRITE 'RECORD TRANS 31R ==========> ' #CNT-31R
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 31 ===========> ",pnd_Cnt_33);                                                                                                //Natural: WRITE 'RECORD TRANS 31 ===========> ' #CNT-33
        if (Global.isEscape()) return;
        getReports().write(0, "RECORD TRANS 31R ==========> ",pnd_Cnt_33r);                                                                                               //Natural: WRITE 'RECORD TRANS 31R ==========> ' #CNT-33R
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS WRITTEN ===========> ",pnd_Cnt_Write);                                                                                             //Natural: WRITE 'RECORDS WRITTEN ===========> ' #CNT-WRITE
        if (Global.isEscape()) return;
        //*  FORCING THESE 22 RECORD TO MAKE SURE THAT AT LEAST ONE MODE RECORD
        //*  IS PRINTED FOR REPORTS IAAP796A,IAAP796B,IAAP796C AND IAAP796D
        pnd_Special_Table_Mode.getValue(1).setValue(100);                                                                                                                 //Natural: MOVE 100 TO #SPECIAL-TABLE-MODE ( 1 )
        pnd_Special_Table_Mode.getValue(2).setValue(601);                                                                                                                 //Natural: MOVE 601 TO #SPECIAL-TABLE-MODE ( 2 )
        pnd_Special_Table_Mode.getValue(3).setValue(602);                                                                                                                 //Natural: MOVE 602 TO #SPECIAL-TABLE-MODE ( 3 )
        pnd_Special_Table_Mode.getValue(4).setValue(603);                                                                                                                 //Natural: MOVE 603 TO #SPECIAL-TABLE-MODE ( 4 )
        pnd_Special_Table_Mode.getValue(5).setValue(701);                                                                                                                 //Natural: MOVE 701 TO #SPECIAL-TABLE-MODE ( 5 )
        pnd_Special_Table_Mode.getValue(6).setValue(702);                                                                                                                 //Natural: MOVE 702 TO #SPECIAL-TABLE-MODE ( 6 )
        pnd_Special_Table_Mode.getValue(7).setValue(703);                                                                                                                 //Natural: MOVE 703 TO #SPECIAL-TABLE-MODE ( 7 )
        pnd_Special_Table_Mode.getValue(8).setValue(704);                                                                                                                 //Natural: MOVE 704 TO #SPECIAL-TABLE-MODE ( 8 )
        pnd_Special_Table_Mode.getValue(9).setValue(705);                                                                                                                 //Natural: MOVE 705 TO #SPECIAL-TABLE-MODE ( 9 )
        pnd_Special_Table_Mode.getValue(10).setValue(706);                                                                                                                //Natural: MOVE 706 TO #SPECIAL-TABLE-MODE ( 10 )
        pnd_Special_Table_Mode.getValue(11).setValue(801);                                                                                                                //Natural: MOVE 801 TO #SPECIAL-TABLE-MODE ( 11 )
        pnd_Special_Table_Mode.getValue(12).setValue(802);                                                                                                                //Natural: MOVE 802 TO #SPECIAL-TABLE-MODE ( 12 )
        pnd_Special_Table_Mode.getValue(13).setValue(803);                                                                                                                //Natural: MOVE 803 TO #SPECIAL-TABLE-MODE ( 13 )
        pnd_Special_Table_Mode.getValue(14).setValue(804);                                                                                                                //Natural: MOVE 804 TO #SPECIAL-TABLE-MODE ( 14 )
        pnd_Special_Table_Mode.getValue(15).setValue(805);                                                                                                                //Natural: MOVE 805 TO #SPECIAL-TABLE-MODE ( 15 )
        pnd_Special_Table_Mode.getValue(16).setValue(806);                                                                                                                //Natural: MOVE 806 TO #SPECIAL-TABLE-MODE ( 16 )
        pnd_Special_Table_Mode.getValue(17).setValue(807);                                                                                                                //Natural: MOVE 807 TO #SPECIAL-TABLE-MODE ( 17 )
        pnd_Special_Table_Mode.getValue(18).setValue(808);                                                                                                                //Natural: MOVE 808 TO #SPECIAL-TABLE-MODE ( 18 )
        pnd_Special_Table_Mode.getValue(19).setValue(809);                                                                                                                //Natural: MOVE 809 TO #SPECIAL-TABLE-MODE ( 19 )
        pnd_Special_Table_Mode.getValue(20).setValue(810);                                                                                                                //Natural: MOVE 810 TO #SPECIAL-TABLE-MODE ( 20 )
        pnd_Special_Table_Mode.getValue(21).setValue(811);                                                                                                                //Natural: MOVE 811 TO #SPECIAL-TABLE-MODE ( 21 )
        pnd_Special_Table_Mode.getValue(22).setValue(812);                                                                                                                //Natural: MOVE 812 TO #SPECIAL-TABLE-MODE ( 22 )
        FR:                                                                                                                                                               //Natural: FOR #U = 1 TO 22
        for (pnd_U.setValue(1); condition(pnd_U.lessOrEqual(22)); pnd_U.nadd(1))
        {
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD
            pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr.setValue("9999999999");                                                                                                  //Natural: MOVE '9999999999' TO #O-TRANS-PPCN-NBR
            pnd_Work_Record_Pnd_O_Trans_Payee_Cde.setValue(99);                                                                                                           //Natural: MOVE 99 TO #O-TRANS-PAYEE-CDE
            pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(pnd_Special_Table_Mode.getValue(pnd_U));                                                                       //Natural: MOVE #SPECIAL-TABLE-MODE ( #U ) TO #O-CNTRCT-MODE-IND
            pnd_Work_Record_Pnd_O_Trans_Check_Dte.setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                                      //Natural: MOVE #SAVE-CHECK-DTE TO #O-TRANS-CHECK-DTE
            pnd_Special_Records_Read.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SPECIAL-RECORDS-READ
            getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                              //Natural: WRITE WORK FILE 01 #WORK-RECORD
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CPR-TRANS-BEFORE-A
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CPR-TRANS-BEFORE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CPR-TRANS-AFTER
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CPR-TRANS-NOT-FOUND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CNTRCT-TRANS-BEFORE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CNTRCT-TRANS-AFTER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-BEFORE
        //*  ADDED FOLLOWING ROUTINE 2/98
        //*  LEFT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-BEFORE-AFTER
        //*        IAA-TIAA-FUND-TRANS1.TIAA-PRODUCT-CDE =
        //*          IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE
        //*  CHANGED ABOVE COMMENTED TO FOLLOWING TO INCLUDE COMPANY-CODE   9/98
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-FUND-TRANS-BEFORE-2
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-AFTER-FUND-TRAN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-AFTER-FND-TRAN
        //*  END OF ADD ROUTINE 2/98
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-BEFORE-A
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #NOW-CHECK-AI
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-AFTER
        //* ***********************************************************************
        //*    NEW ISSUE WITH REVERSAL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-AFTER-31R
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-BEFORE-37B
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-FUND-TRANS-AFTER-37B
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-RECORD
        //*  *************************************************************
        //*  *************************************************************
        //*  FOLLOWING ROUTINES DONE IF NO TRANSACTION RECORD FOUND
        //*  WHEN REQUIRED READ THE CURRENT FILES FOR INFORMATION NEEDED
        //*  *************************************************************
        //*    CNTRCT TRANS RECORD NOT FOUND
        //*  *************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IAA-CNTRCT-TRANS-NOT-FOUND
        //*  ADDED FOLLOWING  2/98
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-TIAA-FUND-RCRD
        //*  6/99 KN
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Iaa_Cpr_Trans_Before_A() throws Exception                                                                                                            //Natural: IAA-CPR-TRANS-BEFORE-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Inactive_Cpr.reset();                                                                                                                                         //Natural: RESET #INACTIVE-CPR
        pnd_W_Cpr_Key_Pnd_W_Cpr_Imge_Id.setValue("1");                                                                                                                    //Natural: MOVE '1' TO #W-CPR-IMGE-ID
        pnd_W_Cpr_Key_Pnd_W_Cpr_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                  //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-CPR-CNTRCT-PPCN-NBR
        pnd_W_Cpr_Key_Pnd_W_Cpr_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                  //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-CPR-PART-PAYEE-CDE
        pnd_W_Cpr_Key_Pnd_W_Cpr_Trans_Dte.setValue(iaa_Trans_Rcrd_Lst_Trans_Dte);                                                                                         //Natural: MOVE IAA-TRANS-RCRD.LST-TRANS-DTE TO #W-CPR-TRANS-DTE
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #W-CPR-KEY
        (
        "RW",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_W_Cpr_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        RW:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("RW")))
        {
            if (condition(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Cpr_Trans_Cntrct_Part_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-CPR-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE
                && iaa_Cpr_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte)))
            {
                if (condition(iaa_Cpr_Trans_Cntrct_Actvty_Cde.equals(9)))                                                                                                 //Natural: IF IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE = 9
                {
                    pnd_Inactive_Cpr.setValue("Y");                                                                                                                       //Natural: MOVE 'Y' TO #INACTIVE-CPR
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(iaa_Cpr_Trans_Cntrct_Mode_Ind);                                                                            //Natural: MOVE IAA-CPR-TRANS.CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte.setValue(iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte);                                                          //Natural: MOVE IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE TO #O-CNTRCT-FINAL-PER-PAY-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Cpr_Trans_Before() throws Exception                                                                                                              //Natural: IAA-CPR-TRANS-BEFORE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Cntrct_Mode_Ind.reset();                                                                                                                                    //Natural: RESET #W-CNTRCT-MODE-IND
        pnd_W_Cpr_Key_Pnd_W_Cpr_Imge_Id.setValue("1");                                                                                                                    //Natural: MOVE '1' TO #W-CPR-IMGE-ID
        pnd_W_Cpr_Key_Pnd_W_Cpr_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                  //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-CPR-CNTRCT-PPCN-NBR
        pnd_W_Cpr_Key_Pnd_W_Cpr_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                  //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-CPR-PART-PAYEE-CDE
        pnd_W_Cpr_Key_Pnd_W_Cpr_Trans_Dte.setValue(iaa_Trans_Rcrd_Lst_Trans_Dte);                                                                                         //Natural: MOVE IAA-TRANS-RCRD.LST-TRANS-DTE TO #W-CPR-TRANS-DTE
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #W-CPR-KEY
        (
        "R1",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_W_Cpr_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("R1")))
        {
            if (condition(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Cpr_Trans_Cntrct_Part_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde))) //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = TRANS-PAYEE-CDE
            {
                if (condition(iaa_Cpr_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte)))                                                                              //Natural: IF IAA-CPR-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE
                {
                    if (condition(iaa_Cpr_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte) || (pnd_W_Date_4_Pnd_W_Date_3.equals("000") && (iaa_Cpr_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Trans_Dte))))) //Natural: IF IAA-CPR-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE OR ( #W-DATE-3 = '000' AND ( IAA-CPR-TRANS.TRANS-DTE = IAA-TRANS-RCRD.TRANS-DTE ) )
                    {
                        pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(iaa_Cpr_Trans_Cntrct_Mode_Ind);                                                                    //Natural: MOVE IAA-CPR-TRANS.CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                        pnd_W_Cntrct_Mode_Ind.setValue(iaa_Cpr_Trans_Cntrct_Mode_Ind);                                                                                    //Natural: MOVE IAA-CPR-TRANS.CNTRCT-MODE-IND TO #W-CNTRCT-MODE-IND
                        pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte.setValue(iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte);                                                  //Natural: MOVE IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE TO #O-CNTRCT-FINAL-PER-PAY-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Cpr_Trans_After() throws Exception                                                                                                               //Natural: IAA-CPR-TRANS-AFTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Imge_Id_A.setValue("2");                                                                                                                //Natural: MOVE '2' TO #W-CPR-IMGE-ID-A
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Cntrct_Ppcn_Nbr_A.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                              //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-CPR-CNTRCT-PPCN-NBR-A
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Part_Payee_Cde_A.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                              //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-CPR-PART-PAYEE-CDE-A
        pnd_W_Cpr_Key_A_Pnd_W_Cpr_Inv_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                                //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-CPR-INV-TRANS-DTE
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #W-CPR-KEY-A
        (
        "R2",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_W_Cpr_Key_A, WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
        1
        );
        R2:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("R2")))
        {
            if (condition(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Cpr_Trans_Cntrct_Part_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR = IAA-TRANS-RCRD.TRANS-PPCN-NBR AND IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE = IAA-TRANS-RCRD.TRANS-PAYEE-CDE AND IAA-CPR-TRANS.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                && iaa_Cpr_Trans_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte)))
            {
                pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(iaa_Cpr_Trans_Cntrct_Mode_Ind);                                                                            //Natural: MOVE IAA-CPR-TRANS.CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte.setValue(iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte);                                                          //Natural: MOVE IAA-CPR-TRANS.CNTRCT-FINAL-PER-PAY-DTE TO #O-CNTRCT-FINAL-PER-PAY-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  CPR TRANS AFTER NOT FOUND
        if (condition(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.equals(getZero())))                                                                                           //Natural: IF #O-CNTRCT-MODE-IND = 0
        {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-NOT-FOUND
            sub_Iaa_Cpr_Trans_Not_Found();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Iaa_Cpr_Trans_Not_Found() throws Exception                                                                                                           //Natural: IAA-CPR-TRANS-NOT-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                          //Natural: MOVE TRANS-PPCN-NBR TO #W-PRTCPNT-CNTRCT-PPCN-NBR
        pnd_W_Prtcpnt_Key_Pnd_W_Prtcpnt_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                          //Natural: MOVE TRANS-PAYEE-CDE TO #W-PRTCPNT-PART-PAYEE-CDE
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #W-PRTCPNT-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_W_Prtcpnt_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ03")))
        {
            if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr))))                                                          //Natural: IF ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR = IAA-TRANS-RCRD.TRANS-PPCN-NBR )
            {
                if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde))))                                                    //Natural: IF ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE = IAA-TRANS-RCRD.TRANS-PAYEE-CDE )
                {
                    pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                              //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                    pnd_W_Cntrct_Mode_Ind.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                                              //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND TO #W-CNTRCT-MODE-IND
                    pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte);                                            //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE TO #O-CNTRCT-FINAL-PER-PAY-DTE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(100);                                                                                                      //Natural: MOVE 100 TO #O-CNTRCT-MODE-IND
                getReports().write(0, "NO IAA-CNTRCT-PRTCPNT-ROLE RECORD: ",pnd_W_Prtcpnt_Key);                                                                           //Natural: WRITE 'NO IAA-CNTRCT-PRTCPNT-ROLE RECORD: ' #W-PRTCPNT-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Cntrct_Trans_Before() throws Exception                                                                                                           //Natural: IAA-CNTRCT-TRANS-BEFORE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Imge_Id_B.setValue("1");                                                                                                          //Natural: MOVE '1' TO #W-CNTRCT-IMGE-ID-B
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_B.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-CNTRCT-CNTRCT-PPCN-NBR-B
        pnd_W_Cntrct_Key_B_Pnd_W_Cntrct_Trans_Dte_B.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                   //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #W-CNTRCT-TRANS-DTE-B
        vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #W-CNTRCT-KEY-B
        (
        "R4A",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_W_Cntrct_Key_B.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
        1
        );
        R4A:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("R4A")))
        {
            if (condition(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Cntrct_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Trans_Dte)))         //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-CNTRCT-TRANS.TRANS-DTE = IAA-TRANS-RCRD.TRANS-DTE
            {
                pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde.setValue(iaa_Cntrct_Trans_Cntrct_Optn_Cde);                                                                         //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-OPTN-CDE TO #O-CNTRCT-OPTN-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-AFTER
                sub_Iaa_Cntrct_Trans_After();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R4A"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R4A"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Cntrct_Trans_After() throws Exception                                                                                                            //Natural: IAA-CNTRCT-TRANS-AFTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Imge_Id_A.setValue("2");                                                                                                          //Natural: MOVE '2' TO #W-CNTRCT-IMGE-ID-A
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Cntrct_Ppcn_Nbr_A.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-CNTRCT-CNTRCT-PPCN-NBR-A
        pnd_W_Cntrct_Key_A_Pnd_W_Cntrct_Invrse_Trans_Dte_A.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                     //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-CNTRCT-INVRSE-TRANS-DTE-A
        vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #W-CNTRCT-KEY-A
        (
        "R4",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_W_Cntrct_Key_A, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
        1
        );
        R4:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("R4")))
        {
            if (condition(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Cntrct_Trans_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte))) //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE
            {
                pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde.setValue(iaa_Cntrct_Trans_Cntrct_Optn_Cde);                                                                         //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-OPTN-CDE TO #O-CNTRCT-OPTN-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM IAA-CNTRCT-TRANS-NOT-FOUND
                sub_Iaa_Cntrct_Trans_Not_Found();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R4"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_Before() throws Exception                                                                                                             //Natural: IAA-FUND-TRANS-BEFORE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_Fund_Bfr_Fnd_Sw.reset();                                                                                                                                      //Natural: RESET #FUND-BFR-FND-SW
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id.setValue("1");                                                                                                              //Natural: MOVE '1' TO #W-B-FUND-IMGE-ID
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-B-FUND-CNTRCT-PPCN-NBR
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-B-FUND-PART-PAYEE-CDE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                       //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #W-B-FUND-TRANS-DTE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-B-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #W-B-FUND-KEY
        (
        "R5",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_W_B_Fund_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
        );
        R5:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("R5")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Trans_Dte)))
            {
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT
                    pnd_Work_Record_Pnd_O_B_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-B-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                               //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                      //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-B-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*   NOW GET AFTER IMAGE FOR FUND INFORMATION
                pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                      //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
                pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
                pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
                pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                 //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
                pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #W-A-FUND-CMPNY-CDE
                vw_iaa_Tiaa_Fund_Trans1.startDatabaseFind                                                                                                                 //Natural: FIND ( 1 ) IAA-TIAA-FUND-TRANS1 WITH TIAA-FUND-AFTR-KEY-2 = #W-A-FUND-KEY
                (
                "F8",
                new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", "=", pnd_W_A_Fund_Key, WcType.WITH) },
                1
                );
                F8:
                while (condition(vw_iaa_Tiaa_Fund_Trans1.readNextRow("F8", true)))
                {
                    vw_iaa_Tiaa_Fund_Trans1.setIfNotFoundControlFlag(false);
                    if (condition(vw_iaa_Tiaa_Fund_Trans1.getAstCOUNTER().equals(0)))                                                                                     //Natural: IF NO RECORDS FOUND
                    {
                        pnd_Fund_Bfr_Fnd_Sw.setValue("N");                                                                                                                //Natural: MOVE 'N' TO #FUND-BFR-FND-SW
                        if (true) break F8;                                                                                                                               //Natural: ESCAPE BOTTOM ( F8. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    if (condition(iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde.equals("T")))                                                                                        //Natural: IF IAA-TIAA-FUND-TRANS1.TIAA-FUND-CDE = 'T'
                    {
                        pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                           //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                        pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Div_Amt);                                                           //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                        pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                        //*  4/08
                        //*  REPLACED REPEAT            /* 4/08
                        pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                             //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                        //*  4/08
                        pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                            //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                           //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                        pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                               //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                        //*  4/08 REPLACED ADD WITH MOVE
                        pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt.getValue(1));                                                   //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fund_Bfr_Fnd_Sw.equals("N")))                                                                                                           //Natural: IF #FUND-BFR-FND-SW = 'N'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*      PERFORM READ-IAA-TIAA-FUND-RCRD
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Bfr_Fnd_Sw.notEquals("N")))                                                                                                        //Natural: IF #FUND-BFR-FND-SW NE 'N'
                {
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                    sub_Pnd_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R5"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R5"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R5;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R5. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_Before_After() throws Exception                                                                                                       //Natural: IAA-FUND-TRANS-BEFORE-AFTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "RAFTER",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        RAFTER:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("RAFTER")))
        {
            pnd_Fund_Bfr_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-BFR-FND-SW := 'Y'
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte)))
            {
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    //*  4/08
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                    //*  4/08
                    //*  4/08
                    pnd_Work_Record_Pnd_O_B_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-B-FINAL-PAYMENT
                    //*  4/08
                    //*  4/08
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                               //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(pnd_Work_Record_Pnd_O_Cref_Units_Cnt);                                                                //Natural: MOVE #O-CREF-UNITS-CNT TO #O-B-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*   NOW GET BEFORE IMAGE FOR FUND INFORMATION
                pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id.setValue("1");                                                                                                      //Natural: MOVE '1' TO #W-B-FUND-IMGE-ID
                pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-B-FUND-CNTRCT-PPCN-NBR
                pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-B-FUND-PART-PAYEE-CDE
                pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                               //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #W-B-FUND-TRANS-DTE
                pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #W-B-FUND-CMPNY-CDE
                //*   READ BEFORE IMAGE
                vw_iaa_Tiaa_Fund_Trans1.startDatabaseRead                                                                                                                 //Natural: READ IAA-TIAA-FUND-TRANS1 BY TIAA-FUND-BFRE-KEY STARTING FROM #W-B-FUND-KEY
                (
                "RBFR60",
                new Wc[] { new Wc("CREF_FUND_BFRE_KEY", ">=", pnd_W_B_Fund_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("CREF_FUND_BFRE_KEY", "ASC") }
                );
                RBFR60:
                while (condition(vw_iaa_Tiaa_Fund_Trans1.readNextRow("RBFR60")))
                {
                    if (condition(iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Ppcn_Nbr.notEquals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) || iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Payee_Cde.notEquals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS1.TIAA-CNTRCT-PPCN-NBR NE TRANS-PPCN-NBR OR IAA-TIAA-FUND-TRANS1.TIAA-CNTRCT-PAYEE-CDE NE TRANS-PAYEE-CDE OR #O-TRANS-CHECK-DTE NE #SAVE-CHECK-DTE
                        || pnd_Work_Record_Pnd_O_Trans_Check_Dte.notEquals(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte)))
                    {
                        pnd_Fund_Bfr_Fnd_Sw.setValue("Y");                                                                                                                //Natural: ASSIGN #FUND-BFR-FND-SW := 'Y'
                        if (true) break RBFR60;                                                                                                                           //Natural: ESCAPE BOTTOM ( RBFR60. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans1_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS1.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS1.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS1.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE AND IAA-TIAA-FUND-TRANS1.TIAA-CMPNY-FUND-CDE = IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
                        && iaa_Tiaa_Fund_Trans1_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte) && iaa_Tiaa_Fund_Trans1_Tiaa_Cmpny_Fund_Cde.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde)))
                    {
                        pnd_Fund_Bfr_Fnd_Sw.setValue(" ");                                                                                                                //Natural: ASSIGN #FUND-BFR-FND-SW := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_Bfr_Fnd_Sw.setValue("Y");                                                                                                                //Natural: ASSIGN #FUND-BFR-FND-SW := 'Y'
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde.equals("T")))                                                                                        //Natural: IF IAA-TIAA-FUND-TRANS1.TIAA-FUND-CDE = 'T'
                    {
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Div_Amt);                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                        pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                               //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                        //*  ADDED 1/02 TO CLEAR OUT
                        //*  AMTS FROM PREVIOUS READ
                        pnd_Work_Record_Pnd_O_B_Final_Payment.reset();                                                                                                    //Natural: RESET #O-B-FINAL-PAYMENT #O-B-FINAL-DIVIDEND
                        pnd_Work_Record_Pnd_O_B_Final_Dividend.reset();
                        //*  4/08
                        //*  REPLACED REPEAT           /* 4/08
                        pnd_Work_Record_Pnd_O_B_Final_Payment.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                           //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-B-FINAL-PAYMENT
                        //*  4/08
                        pnd_Work_Record_Pnd_O_B_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                          //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-B-FINAL-DIVIDEND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                             //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                        //*  4/08 REPLACED ADD WITH MOVE                  /* 4/08
                        //*  4/08
                        //*  4/08
                        pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt.getValue(1));                                                 //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-UNITS-CNT ( 1 ) TO #O-B-CREF-UNITS-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    if (true) break RBFR60;                                                                                                                               //Natural: ESCAPE BOTTOM ( RBFR60. )
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RAFTER"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RAFTER"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fund_Bfr_Fnd_Sw.equals("Y")))                                                                                                           //Natural: IF #FUND-BFR-FND-SW = 'Y'
                {
                    if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(60)))                                                                                                   //Natural: IF TRANS-CDE = 060
                    {
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                             //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                             //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                        pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                               //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                        pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                        pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                               //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Fnd_Sw.equals("Y")))                                                                                                               //Natural: IF #FUND-FND-SW = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                    sub_Pnd_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RAFTER"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RAFTER"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Fund_Bfr_Fnd_Sw.setValue(" ");                                                                                                                    //Natural: ASSIGN #FUND-BFR-FND-SW := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RAFTER;                                                                                                                                   //Natural: ESCAPE BOTTOM ( RAFTER. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Fund_Trans_Before_2() throws Exception                                                                                                          //Natural: READ-FUND-TRANS-BEFORE-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_Bfr_Fnd_Sw.reset();                                                                                                                                      //Natural: RESET #FUND-BFR-FND-SW
        pnd_Save_Keys_Sve_Tiaa_Cntrct_Ppcn_Nbr.getValue("*").reset();                                                                                                     //Natural: RESET SVE-TIAA-CNTRCT-PPCN-NBR ( * ) SVE-TIAA-CNTRCT-PAYEE-CDE ( * ) SVE-TIAA-CMPNY-FUND-CDE ( * ) SVE-INVRSE-TRANS-DTE ( * ) #TCNT
        pnd_Save_Keys_Sve_Tiaa_Cntrct_Payee_Cde.getValue("*").reset();
        pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde.getValue("*").reset();
        pnd_Save_Keys_Sve_Invrse_Trans_Dte.getValue("*").reset();
        pnd_Tcnt.reset();
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id.setValue("1");                                                                                                              //Natural: MOVE '1' TO #W-B-FUND-IMGE-ID
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-B-FUND-CNTRCT-PPCN-NBR
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-B-FUND-PART-PAYEE-CDE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                       //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #W-B-FUND-TRANS-DTE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-B-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #W-B-FUND-KEY
        (
        "RD_BFR2",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_W_B_Fund_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
        );
        RD_BFR2:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("RD_BFR2")))
        {
            pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
            pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
            pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
            pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-TIAA-TOT-PER-AMT
            pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
            pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                             //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
            pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                            //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
            pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.notEquals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) || iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.notEquals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR NE TRANS-PPCN-NBR OR IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE NE TRANS-PAYEE-CDE OR #O-TRANS-CHECK-DTE NE #SAVE-CHECK-DTE
                || pnd_Work_Record_Pnd_O_Trans_Check_Dte.notEquals(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte)))
            {
                if (true) break RD_BFR2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD-BFR2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Trans_Dte)))
            {
                pnd_Fund_Bfr_Fnd_Sw.setValue("Y");                                                                                                                        //Natural: ASSIGN #FUND-BFR-FND-SW := 'Y'
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Tcnt.nadd(1);                                                                                                                                         //Natural: ASSIGN #TCNT := #TCNT + 1
                pnd_Save_Keys_Sve_Tiaa_Cntrct_Ppcn_Nbr.getValue(pnd_Tcnt).setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                        //Natural: ASSIGN SVE-TIAA-CNTRCT-PPCN-NBR ( #TCNT ) := TRANS-PPCN-NBR
                pnd_Save_Keys_Sve_Tiaa_Cntrct_Payee_Cde.getValue(pnd_Tcnt).setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                      //Natural: ASSIGN SVE-TIAA-CNTRCT-PAYEE-CDE ( #TCNT ) := TRANS-PAYEE-CDE
                pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde.getValue(pnd_Tcnt).setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                               //Natural: ASSIGN SVE-TIAA-CMPNY-FUND-CDE ( #TCNT ) := IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
                pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde.getValue(pnd_Tcnt).setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                               //Natural: ASSIGN SVE-TIAA-CMPNY-FUND-CDE ( #TCNT ) := IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
                pnd_Save_Keys_Sve_Invrse_Trans_Dte.getValue(pnd_Tcnt).setValue(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte);                                                     //Natural: ASSIGN SVE-INVRSE-TRANS-DTE ( #TCNT ) := IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT          /* 4/08
                    pnd_Work_Record_Pnd_O_B_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-B-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                               //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                              //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.reset();                                                                                                         //Natural: RESET #O-CREF-UNITS-CNT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                      //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-B-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED IF 4/98 BYPASS AFTER TRANS FOR 102 R
                if (condition(iaa_Trans_Rcrd_Trans_Cde.notEquals(102)))                                                                                                   //Natural: IF TRANS-CDE NE 102
                {
                    //*  TRANS FOR DTH CLAIM
                                                                                                                                                                          //Natural: PERFORM FIND-AFTER-FUND-TRAN
                    sub_Find_After_Fund_Tran();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_BFR2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_BFR2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_BFR2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_BFR2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Bfr_Fnd_Sw.reset();                                                                                                                              //Natural: RESET #FUND-BFR-FND-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RD_BFR2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD-BFR2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Find_After_Fund_Tran() throws Exception                                                                                                              //Natural: FIND-AFTER-FUND-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Aftr_Fnd_Sw.reset();                                                                                                                                     //Natural: RESET #FUND-AFTR-FND-SW
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans1.startDatabaseFind                                                                                                                         //Natural: FIND ( 1 ) IAA-TIAA-FUND-TRANS1 WITH TIAA-FUND-AFTR-KEY-2 = #W-A-FUND-KEY
        (
        "F2",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", "=", pnd_W_A_Fund_Key, WcType.WITH) },
        1
        );
        F2:
        while (condition(vw_iaa_Tiaa_Fund_Trans1.readNextRow("F2", true)))
        {
            vw_iaa_Tiaa_Fund_Trans1.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Tiaa_Fund_Trans1.getAstCOUNTER().equals(0)))                                                                                             //Natural: IF NO RECORDS FOUND
            {
                if (true) break F2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F2. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Fund_Aftr_Fnd_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #FUND-AFTR-FND-SW := 'Y'
            if (condition(iaa_Tiaa_Fund_Trans1_Tiaa_Fund_Cde.equals("T")))                                                                                                //Natural: IF IAA-TIAA-FUND-TRANS1.TIAA-FUND-CDE = 'T'
            {
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                                   //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Div_Amt);                                                                   //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                //*  4/08
                //*  REPLACED REPEAT             /* 4/08
                pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                     //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                //*  4/08
                pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans1_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                    //Natural: ADD IAA-TIAA-FUND-TRANS1.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Tot_Per_Amt);                                                                   //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                //*  4/08 REPLACED ADD WITH MOVE
                pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans1_Tiaa_Units_Cnt.getValue(1));                                                           //Natural: MOVE IAA-TIAA-FUND-TRANS1.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_After_Fnd_Tran() throws Exception                                                                                                           //Natural: #READ-AFTER-FND-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RESET   #FUND-AFTR-FND-SW  /* COMMENTED  4/98
        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                             //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
        pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                             //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
        pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                                //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
        pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                               //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
        pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                               //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "RAFTR",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        RAFTR:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("RAFTR")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte)))
            {
                pnd_Fund_Aftr_Fnd_Sw.setValue("Y");                                                                                                                       //Natural: ASSIGN #FUND-AFTR-FND-SW := 'Y'
                FOR01:                                                                                                                                                    //Natural: FOR #I2 = 1 TO #TCNT
                for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(pnd_Tcnt)); pnd_I2.nadd(1))
                {
                    if (condition(pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde.getValue(pnd_I2).equals(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde) && pnd_Save_Keys_Sve_Invrse_Trans_Dte.getValue(pnd_Tcnt).equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte))) //Natural: IF SVE-TIAA-CMPNY-FUND-CDE ( #I2 ) = IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE AND SVE-INVRSE-TRANS-DTE ( #TCNT ) = IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RAFTR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RAFTR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Save_Keys_Sve_Tiaa_Cmpny_Fund_Cde.getValue(pnd_I2).equals(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde) && pnd_Save_Keys_Sve_Invrse_Trans_Dte.getValue(pnd_Tcnt).equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte))) //Natural: IF SVE-TIAA-CMPNY-FUND-CDE ( #I2 ) = IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE AND SVE-INVRSE-TRANS-DTE ( #TCNT ) = IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RAFTR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RAFTR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RAFTR;                                                                                                                                    //Natural: ESCAPE BOTTOM ( RAFTR. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_Before_A() throws Exception                                                                                                           //Natural: IAA-FUND-TRANS-BEFORE-A
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id.setValue("1");                                                                                                              //Natural: MOVE '1' TO #W-B-FUND-IMGE-ID
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-B-FUND-CNTRCT-PPCN-NBR
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-B-FUND-PART-PAYEE-CDE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                       //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #W-B-FUND-TRANS-DTE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-B-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #W-B-FUND-KEY
        (
        "RZ",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_W_B_Fund_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
        );
        RZ:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("RZ")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Trans_Dte)))
            {
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RZ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RZ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Fund_Fnd_Sw.notEquals("Y")))                                                                                                                    //Natural: IF #FUND-FND-SW NE 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #NOW-CHECK-AI
            sub_Pnd_Now_Check_Ai();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Now_Check_Ai() throws Exception                                                                                                                  //Natural: #NOW-CHECK-AI
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "R6",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        R6:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("R6")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte)))
            {
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                        //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R6"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R6"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R6;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R6. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_After() throws Exception                                                                                                              //Natural: IAA-FUND-TRANS-AFTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "R7",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        R7:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("R7")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                && iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.equals(iaa_Trans_Rcrd_Invrse_Trans_Dte)))
            {
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                         //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                        //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R7"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R7"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R7;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R7. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_After_31r() throws Exception                                                                                                          //Natural: IAA-FUND-TRANS-AFTER-31R
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                         //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "READ04",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ04")))
        {
            if (condition(pnd_W_First_Loop.equals("Y")))                                                                                                                  //Natural: IF #W-FIRST-LOOP = 'Y'
            {
                pnd_W_First_Loop.setValue(" ");                                                                                                                           //Natural: MOVE ' ' TO #W-FIRST-LOOP
                pnd_W_Hold_Invrse_Date.setValue(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte);                                                                                    //Natural: MOVE IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE TO #W-HOLD-INVRSE-DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE AND IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE = #W-HOLD-INVRSE-DATE
                && iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.equals(pnd_W_Hold_Invrse_Date)))
            {
                pnd_W_Record_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-RECORD-COUNT
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                    sub_Pnd_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                    //*  WRITE CREF RECORDS
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                    sub_Pnd_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_Before_37b() throws Exception                                                                                                         //Natural: IAA-FUND-TRANS-BEFORE-37B
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Imge_Id.setValue("1");                                                                                                              //Natural: MOVE '1' TO #W-B-FUND-IMGE-ID
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-B-FUND-CNTRCT-PPCN-NBR
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-B-FUND-PART-PAYEE-CDE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Trans_Dte.setValue(0);                                                                                                              //Natural: MOVE 0 TO #W-B-FUND-TRANS-DTE
        pnd_W_B_Fund_Key_Pnd_W_B_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-B-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #W-B-FUND-KEY
        (
        "READ05",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_W_B_Fund_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
        );
        READ05:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ05")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE
            {
                pnd_W_Record_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-RECORD-COUNT
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*  KB IF DATE/TIME EQUAL LEAVE RTNE, OTHERWISE GET LASTEST RECORD
                if (condition(iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte)))                                                                        //Natural: IF IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Iaa_Fund_Trans_After_37b() throws Exception                                                                                                          //Natural: IAA-FUND-TRANS-AFTER-37B
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Imge_Id.setValue("2");                                                                                                              //Natural: MOVE '2' TO #W-A-FUND-IMGE-ID
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-A-FUND-CNTRCT-PPCN-NBR
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-A-FUND-PART-PAYEE-CDE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Invrse_Trans_Dte.setValue(0);                                                                                                       //Natural: MOVE 0 TO #W-A-FUND-INVRSE-TRANS-DTE
        pnd_W_A_Fund_Key_Pnd_W_A_Fund_Cmpny_Cde.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-A-FUND-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #W-A-FUND-KEY
        (
        "READ06",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_W_A_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ06:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ06")))
        {
            if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE
            {
                pnd_W_Record_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-RECORD-COUNT
                if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde.equals("T")))                                                                                             //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    //*  4/08
                    //*  REPLACED REPEAT            /* 4/08
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    //*  4/08
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-TRANS.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Product_Cde);                                                                     //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                    pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE TO #O-COMPANY-CDE
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    //*  4/08 REPLACED ADD WITH MOVE
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1));                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*  KB IF DATE/TIME EQUAL LEAVE RTNE, OTHERWISE GET LASTEST RECORD
                if (condition(iaa_Tiaa_Fund_Trans_Trans_Dte.equals(iaa_Trans_Rcrd_Lst_Trans_Dte)))                                                                        //Natural: IF IAA-TIAA-FUND-TRANS.TRANS-DTE = IAA-TRANS-RCRD.LST-TRANS-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Record() throws Exception                                                                                                                  //Natural: #WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  COMMENTED FOLLOWING 10/01 TO PRINT TPA
        //*  IF #O-CNTRCT-OPTN-CDE = 22 OR = 23 OR = 25 OR = 27 OR = 28 OR = 30
        //*   ADD 1 TO #W-BYPASS-COUNT
        //*  MOVE ' ' TO #SW-AFTER-SW
        //*   ESCAPE ROUTINE
        //*  ELSE
        //*  END OF COMMENTED FOLLOWING 10/01 TO PRINT TPA
        if (condition(((pnd_Work_Record_Pnd_O_Trans_Cde.equals(506) || pnd_Work_Record_Pnd_O_Trans_Cde.equals(508)) && pnd_Work_Record_Pnd_O_Sub_Cde.equals("033"))))     //Natural: IF ( #O-TRANS-CDE = 506 OR = 508 ) AND #O-SUB-CDE = '033'
        {
            pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
            pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
            pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
            pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                            //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
            pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                           //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-IF
        pnd_Work_Record_Pnd_O_Critical_Ind.setValue("2");                                                                                                                 //Natural: MOVE '2' TO #O-CRITICAL-IND
        //*  CHECK FOR
        //*  MODE CHANGE
        if (condition((((((iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(51)) || iaa_Trans_Rcrd_Trans_Cde.equals(60)) || iaa_Trans_Rcrd_Trans_Cde.equals(61))  //Natural: IF ( TRANS-CDE = 050 OR = 051 OR = 060 OR = 061 OR = 62 ) AND #W-CNTRCT-MODE-IND = 0
            || iaa_Trans_Rcrd_Trans_Cde.equals(62)) && pnd_W_Cntrct_Mode_Ind.equals(getZero()))))
        {
                                                                                                                                                                          //Natural: PERFORM IAA-CPR-TRANS-NOT-FOUND
            sub_Iaa_Cpr_Trans_Not_Found();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK FOR
        if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(51) || iaa_Trans_Rcrd_Trans_Cde.equals(60) || iaa_Trans_Rcrd_Trans_Cde.equals(61)  //Natural: IF ( TRANS-CDE = 050 OR = 051 OR = 060 OR = 061 OR = 62 )
            || iaa_Trans_Rcrd_Trans_Cde.equals(62)))
        {
            if (condition(pnd_W_Cntrct_Mode_Ind.notEquals(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind)))                                                                        //Natural: IF #W-CNTRCT-MODE-IND NE #O-CNTRCT-MODE-IND
            {
                pnd_H_Cntrct_Mode_Ind.setValue(pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind);                                                                                    //Natural: MOVE #O-CNTRCT-MODE-IND TO #H-CNTRCT-MODE-IND
                pnd_H_Tiaa_Tot_Per_Amt.setValue(pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt);                                                                                  //Natural: MOVE #O-TIAA-TOT-PER-AMT TO #H-TIAA-TOT-PER-AMT
                pnd_H_Tiaa_Tot_Div_Amt.setValue(pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt);                                                                                  //Natural: MOVE #O-TIAA-TOT-DIV-AMT TO #H-TIAA-TOT-DIV-AMT
                pnd_H_Cref_Units_Cnt.setValue(pnd_Work_Record_Pnd_O_Cref_Units_Cnt);                                                                                      //Natural: MOVE #O-CREF-UNITS-CNT TO #H-CREF-UNITS-CNT
                //* * ADDED FOLLOWING 6/02
                pnd_H_Tiaa_Tot_Fin_Pmt.setValue(pnd_Work_Record_Pnd_O_Final_Payment);                                                                                     //Natural: MOVE #O-FINAL-PAYMENT TO #H-TIAA-TOT-FIN-PMT
                pnd_H_Tiaa_Tot_Fin_Div.setValue(pnd_Work_Record_Pnd_O_Final_Dividend);                                                                                    //Natural: MOVE #O-FINAL-DIVIDEND TO #H-TIAA-TOT-FIN-DIV
                //* * END ADD 6/02
                pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(pnd_W_Cntrct_Mode_Ind);                                                                                    //Natural: MOVE #W-CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                pnd_Work_Record_Pnd_O_Final_Payment.setValue(0);                                                                                                          //Natural: MOVE 0 TO #O-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_Final_Dividend.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-FINAL-DIVIDEND
                getReports().write(0, "OUTPUTA: ",pnd_Work_Record_Pnd_O_Trans_Check_Dte,pnd_Work_Record_Pnd_O_Trans_Dte,pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind,            //Natural: WRITE 'OUTPUTA: ' #WORK-RECORD
                    pnd_Work_Record_Pnd_O_Trans_Cde,pnd_Work_Record_Pnd_O_Sub_Cde,pnd_Work_Record_Pnd_O_Trans_Actvty_Cde,pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde,
                    pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte,pnd_Work_Record_Pnd_O_Trans_Sub_Cde,pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr,pnd_Work_Record_Pnd_O_Trans_Payee_Cde,
                    pnd_Work_Record_Pnd_O_Product_Cde,pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt,pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt,pnd_Work_Record_Pnd_O_Cref_Units_Cnt,
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt,pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt,pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt,pnd_Work_Record_Pnd_O_Critical_Ind,
                    pnd_Work_Record_Pnd_O_Company_Cde,pnd_Work_Record_Pnd_O_Final_Payment,pnd_Work_Record_Pnd_O_Final_Dividend,pnd_Work_Record_Pnd_O_B_Final_Payment,
                    pnd_Work_Record_Pnd_O_B_Final_Dividend);
                if (Global.isEscape()) return;
                getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                          //Natural: WRITE WORK FILE 01 #WORK-RECORD
                pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind.setValue(pnd_H_Cntrct_Mode_Ind);                                                                                    //Natural: MOVE #H-CNTRCT-MODE-IND TO #O-CNTRCT-MODE-IND
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(pnd_H_Tiaa_Tot_Per_Amt);                                                                                  //Natural: MOVE #H-TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(pnd_H_Tiaa_Tot_Div_Amt);                                                                                  //Natural: MOVE #H-TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(pnd_H_Cref_Units_Cnt);                                                                                      //Natural: MOVE #H-CREF-UNITS-CNT TO #O-CREF-UNITS-CNT
                //* * ADDED FOLLOWING 6/02
                pnd_Work_Record_Pnd_O_Final_Payment.setValue(pnd_H_Tiaa_Tot_Fin_Pmt);                                                                                     //Natural: MOVE #H-TIAA-TOT-FIN-PMT TO #O-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_Final_Dividend.setValue(pnd_H_Tiaa_Tot_Fin_Div);                                                                                    //Natural: MOVE #H-TIAA-TOT-FIN-DIV TO #O-FINAL-DIVIDEND
                //* * END ADD 6/02
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                        //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING  5/98 HANDLE REINSTATING BENEY PAST FINAL-PER-DTE
        if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(37) && pnd_Work_Record_Pnd_O_Sub_Cde.equals("37B"))))                                                              //Natural: IF ( TRANS-CDE = 037 AND #O-SUB-CDE = '37B' )
        {
            if (condition(pnd_Work_Record_Pnd_O_Trans_Payee_Cde.greater(2) && pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm.greater(pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte))) //Natural: IF #O-TRANS-PAYEE-CDE GT 02 AND #SAVE-CHECK-DTE-YYYYMM GT #O-CNTRCT-FINAL-PER-PAY-DTE
            {
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt);                                                                //Natural: MOVE #O-TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt);                                                                //Natural: MOVE #O-TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(pnd_Work_Record_Pnd_O_Cref_Units_Cnt);                                                                    //Natural: MOVE #O-CREF-UNITS-CNT TO #O-B-CREF-UNITS-CNT
                pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(pnd_Work_Record_Pnd_O_Final_Payment);                                                                      //Natural: MOVE #O-FINAL-PAYMENT TO #O-B-FINAL-PAYMENT
                pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(pnd_Work_Record_Pnd_O_Final_Dividend);                                                                    //Natural: MOVE #O-FINAL-DIVIDEND TO #O-B-FINAL-DIVIDEND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 5/98
        getReports().write(0, "OUTPUT WRITE RECORD: ",pnd_Work_Record_Pnd_O_Trans_Check_Dte,pnd_Work_Record_Pnd_O_Trans_Dte,pnd_Work_Record_Pnd_O_Cntrct_Mode_Ind,        //Natural: WRITE 'OUTPUT WRITE RECORD: ' #WORK-RECORD
            pnd_Work_Record_Pnd_O_Trans_Cde,pnd_Work_Record_Pnd_O_Sub_Cde,pnd_Work_Record_Pnd_O_Trans_Actvty_Cde,pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde,pnd_Work_Record_Pnd_O_Cntrct_Final_Per_Pay_Dte,
            pnd_Work_Record_Pnd_O_Trans_Sub_Cde,pnd_Work_Record_Pnd_O_Trans_Ppcn_Nbr,pnd_Work_Record_Pnd_O_Trans_Payee_Cde,pnd_Work_Record_Pnd_O_Product_Cde,
            pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt,pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt,pnd_Work_Record_Pnd_O_Cref_Units_Cnt,pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt,
            pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt,pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt,pnd_Work_Record_Pnd_O_Critical_Ind,pnd_Work_Record_Pnd_O_Company_Cde,
            pnd_Work_Record_Pnd_O_Final_Payment,pnd_Work_Record_Pnd_O_Final_Dividend,pnd_Work_Record_Pnd_O_B_Final_Payment,pnd_Work_Record_Pnd_O_B_Final_Dividend);
        if (Global.isEscape()) return;
        getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                                  //Natural: WRITE WORK FILE 01 #WORK-RECORD
        pnd_Cnt_Write.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CNT-WRITE
    }
    private void sub_Iaa_Cntrct_Trans_Not_Found() throws Exception                                                                                                        //Natural: IAA-CNTRCT-TRANS-NOT-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Trans_Rcrd_Trans_Ppcn_Nbr, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA-CNTRCT RECORD: ",iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: WRITE 'NO IAA-CNTRCT RECORD: ' TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Work_Record_Pnd_O_Cntrct_Optn_Cde.setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                                   //Natural: MOVE IAA-CNTRCT.CNTRCT-OPTN-CDE TO #O-CNTRCT-OPTN-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Tiaa_Fund_Rcrd() throws Exception                                                                                                           //Natural: READ-IAA-TIAA-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fund_Fnd_Sw.reset();                                                                                                                                          //Natural: RESET #FUND-FND-SW
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                      //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #W-FUND-RCRD-CNTRCT-PPCN-NBR
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                      //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #W-FUND-RCRD-PART-PAYEE-CDE
        pnd_W_Fund_Rcrd_Key_Pnd_W_Fund_Rcrd_Cmpny_Cde.setValue(" ");                                                                                                      //Natural: MOVE ' ' TO #W-FUND-RCRD-CMPNY-CDE
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #W-FUND-RCRD-KEY
        (
        "R8",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_W_Fund_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R8:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("R8")))
        {
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = TRANS-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = TRANS-PAYEE-CDE
            {
                pnd_Fund_Fnd_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FUND-FND-SW := 'Y'
                pnd_Work_Record_Pnd_O_Product_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Product_Cde);                                                                          //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-PRODUCT-CDE TO #O-PRODUCT-CDE
                pnd_Work_Record_Pnd_O_Company_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde);                                                                             //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE TO #O-COMPANY-CDE
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                //*  3/01
                pnd_Work_Record_Pnd_O_Final_Payment.setValue(0);                                                                                                          //Natural: MOVE 0 TO #O-FINAL-PAYMENT
                //*  3/01
                pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                        //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                //*  3/01
                pnd_Work_Record_Pnd_O_Final_Dividend.setValue(0);                                                                                                         //Natural: MOVE 0 TO #O-FINAL-DIVIDEND
                //*  3/01
                pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                       //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("T")))                                                                                              //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE = 'T'
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                               //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                               //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(0);                                                                                                     //Natural: MOVE 0 TO #O-CREF-UNITS-CNT
                    pnd_Work_Record_Pnd_O_Final_Payment.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                   //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-FINAL-PAYMENT
                    pnd_Work_Record_Pnd_O_Final_Dividend.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                  //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-FINAL-DIVIDEND
                    pnd_Work_Record_Pnd_O_B_Final_Payment.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                 //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #O-B-FINAL-PAYMENT
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( * ) TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #O-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                               //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_Cref_Units_Cnt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                                         //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 ) TO #O-CREF-UNITS-CNT
                    //*  ADDED 2/98
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(pnd_Work_Record_Pnd_O_Cref_Units_Cnt);                                                                //Natural: MOVE #O-CREF-UNITS-CNT TO #O-B-CREF-UNITS-CNT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(500) || iaa_Trans_Rcrd_Trans_Cde.equals(502) ||                   //Natural: IF NOT ( TRANS-CDE = 050 OR = 500 OR = 502 OR = 504 OR = 060 OR = 512 OR = 514 OR = 516 OR = 518 OR = 061 OR = 062 OR = 506 OR = 508 OR = 051 OR = 520 )
                    iaa_Trans_Rcrd_Trans_Cde.equals(504) || iaa_Trans_Rcrd_Trans_Cde.equals(60) || iaa_Trans_Rcrd_Trans_Cde.equals(512) || iaa_Trans_Rcrd_Trans_Cde.equals(514) 
                    || iaa_Trans_Rcrd_Trans_Cde.equals(516) || iaa_Trans_Rcrd_Trans_Cde.equals(518) || iaa_Trans_Rcrd_Trans_Cde.equals(61) || iaa_Trans_Rcrd_Trans_Cde.equals(62) 
                    || iaa_Trans_Rcrd_Trans_Cde.equals(506) || iaa_Trans_Rcrd_Trans_Cde.equals(508) || iaa_Trans_Rcrd_Trans_Cde.equals(51) || iaa_Trans_Rcrd_Trans_Cde.equals(520))))
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                    //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: END-IF
                //*  6/99 KN
                if (condition(((((pnd_Work_Record_Pnd_O_Trans_Cde.equals(506) || pnd_Work_Record_Pnd_O_Trans_Cde.equals(508)) || pnd_Work_Record_Pnd_O_Trans_Cde.equals(516))  //Natural: IF ( #O-TRANS-CDE = 506 OR = 508 OR = 516 OR = 518 ) AND #O-SUB-CDE = '033'
                    || pnd_Work_Record_Pnd_O_Trans_Cde.equals(518)) && pnd_Work_Record_Pnd_O_Sub_Cde.equals("033"))))
                {
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Per_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-PER-AMT
                    pnd_Work_Record_Pnd_O_B_Tiaa_Tot_Div_Amt.setValue(0);                                                                                                 //Natural: MOVE 0 TO #O-B-TIAA-TOT-DIV-AMT
                    pnd_Work_Record_Pnd_O_B_Cref_Units_Cnt.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-CREF-UNITS-CNT
                    pnd_Work_Record_Pnd_O_B_Final_Payment.setValue(0);                                                                                                    //Natural: MOVE 0 TO #O-B-FINAL-PAYMENT
                    pnd_Work_Record_Pnd_O_B_Final_Dividend.setValue(0);                                                                                                   //Natural: MOVE 0 TO #O-B-FINAL-DIVIDEND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-RECORD
                sub_Pnd_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R8"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R8"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R8;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R8. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, "=",iaa_Trans_Rcrd_Trans_Dte,NEWLINE,"=",iaa_Trans_Rcrd_Invrse_Trans_Dte,NEWLINE,"=",iaa_Trans_Rcrd_Lst_Trans_Dte,NEWLINE,                  //Natural: WRITE '=' IAA-TRANS-RCRD.TRANS-DTE / '=' IAA-TRANS-RCRD.INVRSE-TRANS-DTE / '=' IAA-TRANS-RCRD.LST-TRANS-DTE / '=' IAA-TRANS-RCRD.TRANS-PPCN-NBR / '=' IAA-TRANS-RCRD.TRANS-PAYEE-CDE / '=' IAA-TRANS-RCRD.TRANS-SUB-CDE / '=' IAA-TRANS-RCRD.TRANS-CDE / '=' IAA-TRANS-RCRD.TRANS-ACTVTY-CDE / '=' IAA-TRANS-RCRD.TRANS-CHECK-DTE / '=' IAA-TRANS-RCRD.TRANS-VERIFY-CDE / '=' IAA-TRANS-RCRD.TRANS-TODAYS-DTE /
            "=",iaa_Trans_Rcrd_Trans_Ppcn_Nbr,NEWLINE,"=",iaa_Trans_Rcrd_Trans_Payee_Cde,NEWLINE,"=",iaa_Trans_Rcrd_Trans_Sub_Cde,NEWLINE,"=",iaa_Trans_Rcrd_Trans_Cde,
            NEWLINE,"=",iaa_Trans_Rcrd_Trans_Actvty_Cde,NEWLINE,"=",iaa_Trans_Rcrd_Trans_Check_Dte,NEWLINE,"=",iaa_Trans_Rcrd_Trans_Verify_Cde,NEWLINE,"=",
            iaa_Trans_Rcrd_Trans_Todays_Dte,NEWLINE);
    };                                                                                                                                                                    //Natural: END-ERROR
}
