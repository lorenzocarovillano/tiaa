/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:47 PM
**        * FROM NATURAL PROGRAM : Iatp189
************************************************************
**        * FILE NAME            : Iatp189.java
**        * CLASS NAME           : Iatp189
**        * INSTANCE NAME        : Iatp189
************************************************************
************************************************************************
*         ASSET VALUE CONTROL REPORT FOR TRANSFERS AND SWITCHES
*         -----------------------------------------------------
*
*  PROGRAM: IATP189
*  DATE   : 12/19/97
*  BY     : LEN BERNSTEIN
*  DESC   : THIS REPORT PRODUCES THE FOLLOWING PAGES:-
*           1. TRANSFER TOTALS FOR ALL ANNUAL ACCOUNTS
*           2. TRANSFER TOTALS FOR ALL MONTHLY ACCOUNTS
*           3. SWITCH  TOTALS FROM ANNUAL  TO  MONTHLY ACCOUNTS
*           4. SWITCH  TOTALS FROM MONTHLY TO  ANNUAL ACCOUNTS
*           A GRAND TOTAL FOR TRANSFERS AND SWITCHES WILL BE PRINTED
*           ON THE LAST PAGE.
*  HISTORY:
*  -------
*
*  05/02  TD  : CHANGE CALL TO IAAN050A TO IAAN051A
*  04/98 LEN B: ADDED "ASSET VALUE NET" COLUMN TO THIS REPORT
*  03/09  OS  TIAA ACCESS CHANGES. SC 030209.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp189 extends BLNatBase
{
    // Data Areas
    private PdaIatl400p pdaIatl400p;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Cntrl;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Lst_Chnge_Dte;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1;

    private DbsGroup pnd_Iaxfr_Ctl_Super_De_1__R_Field_1;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte;

    private DbsGroup pnd_Iaan050a_Pda;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Fund_2;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Desc;
    private DbsField pnd_Iaan050a_Pda_Pnd_Cmpny_Desc;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Len;

    private DbsGroup pnd_Headings;
    private DbsField pnd_Headings_Pnd_Transfer_Annual;
    private DbsField pnd_Headings_Pnd_Transfer_Monthly;
    private DbsField pnd_Headings_Pnd_Switch_From_Annual;
    private DbsField pnd_Headings_Pnd_Switch_From_Monthly;

    private DbsGroup pnd_Index;
    private DbsField pnd_Index_Pnd_Index_1g;
    private DbsField pnd_Index_Pnd_Index_1s;
    private DbsField pnd_Index_Pnd_Index_09;
    private DbsField pnd_Index_Pnd_Index_11;
    private DbsField pnd_Index_Pnd_Write_Index;

    private DbsGroup pnd_Write_Line;
    private DbsField pnd_Write_Line_Pnd_Desc;
    private DbsField pnd_Write_Line_Pnd_From;
    private DbsField pnd_Write_Line_Pnd_To;
    private DbsField pnd_Write_Line_Pnd_Net;

    private DbsGroup pnd_Grand;
    private DbsField pnd_Grand_Pnd_Trnsfr_Sw_From;
    private DbsField pnd_Grand_Pnd_Trnsfr_Sw_To;
    private DbsField pnd_Grand_Pnd_Net;

    private DbsGroup pnd_Temp_Tot;
    private DbsField pnd_Temp_Tot_Pnd_Trnsfr_From;
    private DbsField pnd_Temp_Tot_Pnd_Trnsfr_To;
    private DbsField pnd_Temp_Tot_Pnd_Switch_From;
    private DbsField pnd_Temp_Tot_Pnd_Switch_To;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_Transfer;
    private DbsField pnd_L_Pnd_Annual;
    private DbsField pnd_L_Pnd_From_Annual;
    private DbsField pnd_L_Pnd_No_Rec_Found;
    private DbsField pnd_L_Pnd_Inside_Read;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_I;
    private DbsField pnd_Fund_Desc;

    private DbsRecord internalLoopRecord;
    private DbsField rD1Global_astIsnOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);

        // Local Variables

        vw_iaa_Xfr_Cntrl = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Cntrl", "IAA-XFR-CNTRL"), "IAA_XFR_CNTRL", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_CNTRL"));
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type", "IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_CTL_RCRD_TYPE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte", "IAXFR-CTL-EFFCTVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CTL_EFFCTVE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte", "IAXFR-CTL-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CTL_CYCLE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt", "IAXFR-CTL-TOTAL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_FRM_ASSET_AMT");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt", "IAXFR-CTL-TOTAL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_TO_ASSET_AMT");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data", "C*IAXFR-CTL-FRM-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data", "IAXFR-CTL-FRM-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde", "IAXFR-CTL-FRM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt", "IAXFR-CTL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data", "C*IAXFR-CTL-TO-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_TO_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data", "IAXFR-CTL-TO-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde", "IAXFR-CTL-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt", "IAXFR-CTL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Lst_Chnge_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        registerRecord(vw_iaa_Xfr_Cntrl);

        pnd_Iaxfr_Ctl_Super_De_1 = localVariables.newFieldInRecord("pnd_Iaxfr_Ctl_Super_De_1", "#IAXFR-CTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Iaxfr_Ctl_Super_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaxfr_Ctl_Super_De_1__R_Field_1", "REDEFINE", pnd_Iaxfr_Ctl_Super_De_1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type", 
            "#IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte", 
            "#IAXFR-CTL-CYCLE-DTE", FieldType.DATE);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte", 
            "#IAXFR-CTL-EFFCTVE-DTE", FieldType.DATE);

        pnd_Iaan050a_Pda = localVariables.newGroupInRecord("pnd_Iaan050a_Pda", "#IAAN050A-PDA");
        pnd_Iaan050a_Pda_Pnd_Parm_Fund_2 = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Iaan050a_Pda_Pnd_Parm_Desc = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);
        pnd_Iaan050a_Pda_Pnd_Cmpny_Desc = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Iaan050a_Pda_Pnd_Parm_Len = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);

        pnd_Headings = localVariables.newGroupInRecord("pnd_Headings", "#HEADINGS");
        pnd_Headings_Pnd_Transfer_Annual = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Transfer_Annual", "#TRANSFER-ANNUAL", FieldType.STRING, 50);
        pnd_Headings_Pnd_Transfer_Monthly = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Transfer_Monthly", "#TRANSFER-MONTHLY", FieldType.STRING, 50);
        pnd_Headings_Pnd_Switch_From_Annual = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Switch_From_Annual", "#SWITCH-FROM-ANNUAL", FieldType.STRING, 
            50);
        pnd_Headings_Pnd_Switch_From_Monthly = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Switch_From_Monthly", "#SWITCH-FROM-MONTHLY", FieldType.STRING, 
            50);

        pnd_Index = localVariables.newGroupInRecord("pnd_Index", "#INDEX");
        pnd_Index_Pnd_Index_1g = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_1g", "#INDEX-1G", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_1s = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_1s", "#INDEX-1S", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_09 = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_09", "#INDEX-09", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_11 = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_11", "#INDEX-11", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Write_Index = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Write_Index", "#WRITE-INDEX", FieldType.PACKED_DECIMAL, 2);

        pnd_Write_Line = localVariables.newGroupInRecord("pnd_Write_Line", "#WRITE-LINE");
        pnd_Write_Line_Pnd_Desc = pnd_Write_Line.newFieldInGroup("pnd_Write_Line_Pnd_Desc", "#DESC", FieldType.STRING, 43);
        pnd_Write_Line_Pnd_From = pnd_Write_Line.newFieldInGroup("pnd_Write_Line_Pnd_From", "#FROM", FieldType.NUMERIC, 11, 2);
        pnd_Write_Line_Pnd_To = pnd_Write_Line.newFieldInGroup("pnd_Write_Line_Pnd_To", "#TO", FieldType.NUMERIC, 11, 2);
        pnd_Write_Line_Pnd_Net = pnd_Write_Line.newFieldInGroup("pnd_Write_Line_Pnd_Net", "#NET", FieldType.NUMERIC, 11, 2);

        pnd_Grand = localVariables.newGroupInRecord("pnd_Grand", "#GRAND");
        pnd_Grand_Pnd_Trnsfr_Sw_From = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Trnsfr_Sw_From", "#TRNSFR-SW-FROM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Grand_Pnd_Trnsfr_Sw_To = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Trnsfr_Sw_To", "#TRNSFR-SW-TO", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Grand_Pnd_Net = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Net", "#NET", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Temp_Tot = localVariables.newGroupInRecord("pnd_Temp_Tot", "#TEMP-TOT");
        pnd_Temp_Tot_Pnd_Trnsfr_From = pnd_Temp_Tot.newFieldInGroup("pnd_Temp_Tot_Pnd_Trnsfr_From", "#TRNSFR-FROM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Temp_Tot_Pnd_Trnsfr_To = pnd_Temp_Tot.newFieldInGroup("pnd_Temp_Tot_Pnd_Trnsfr_To", "#TRNSFR-TO", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Temp_Tot_Pnd_Switch_From = pnd_Temp_Tot.newFieldInGroup("pnd_Temp_Tot_Pnd_Switch_From", "#SWITCH-FROM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Temp_Tot_Pnd_Switch_To = pnd_Temp_Tot.newFieldInGroup("pnd_Temp_Tot_Pnd_Switch_To", "#SWITCH-TO", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_Transfer = pnd_L.newFieldInGroup("pnd_L_Pnd_Transfer", "#TRANSFER", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Annual = pnd_L.newFieldInGroup("pnd_L_Pnd_Annual", "#ANNUAL", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_From_Annual = pnd_L.newFieldInGroup("pnd_L_Pnd_From_Annual", "#FROM-ANNUAL", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_No_Rec_Found = pnd_L.newFieldInGroup("pnd_L_Pnd_No_Rec_Found", "#NO-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Inside_Read = pnd_L.newFieldInGroup("pnd_L_Pnd_Inside_Read", "#INSIDE-READ", FieldType.BOOLEAN, 1);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35, new DbsArrayController(1, 40));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rD1Global_astIsnOld = internalLoopRecord.newFieldInRecord("RD1_Global_astIsn_OLD", "Global_astIsn_OLD", FieldType.PACKED_DECIMAL, 10);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Cntrl.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Headings_Pnd_Transfer_Annual.setInitialValue("TRANSFER RQSTS FROM ANNUAL ACCOUNTS PROCESSED ON ");
        pnd_Headings_Pnd_Transfer_Monthly.setInitialValue("TRANSFER RQSTS FROM MONTHLY ACCOUNTS PROCESSED ON ");
        pnd_Headings_Pnd_Switch_From_Annual.setInitialValue("ANNUAL SWITCH RQSTS ACCOUNTS PROCESSED ON ");
        pnd_Headings_Pnd_Switch_From_Monthly.setInitialValue("MONTHLY SWITCH RQSTS ACCOUNTS PROCESSED ON ");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp189() throws Exception
    {
        super("Iatp189");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ======================================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ======================================================================
        //*                           START OF PROGRAM
        //* ======================================================================
        pnd_Index_Pnd_Index_1g.reset();                                                                                                                                   //Natural: RESET #INDEX-1G #INDEX-1S #INDEX-09 #WRITE-INDEX
        pnd_Index_Pnd_Index_1s.reset();
        pnd_Index_Pnd_Index_09.reset();
        pnd_Index_Pnd_Write_Index.reset();
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DATE
        sub_Get_Business_Date();
        if (condition(Global.isEscape())) {return;}
        //*  ============================
        //*   PORCESS THE TRANSFER PAGES
        //*  ============================
        pnd_L_Pnd_Transfer.setValue(true);                                                                                                                                //Natural: ASSIGN #TRANSFER := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TRNSFR-SW-REPORT-PAGES
        sub_Write_Trnsfr_Sw_Report_Pages();
        if (condition(Global.isEscape())) {return;}
        //*  ==========================
        //*   PORCESS THE SWITCH PAGES
        //*  ==========================
        pnd_L_Pnd_Transfer.reset();                                                                                                                                       //Natural: RESET #TRANSFER #INSIDE-READ
        pnd_L_Pnd_Inside_Read.reset();
                                                                                                                                                                          //Natural: PERFORM WRITE-TRNSFR-SW-REPORT-PAGES
        sub_Write_Trnsfr_Sw_Report_Pages();
        if (condition(Global.isEscape())) {return;}
        //*  =======================
        //*   WRITE THE GRAND TOTAL
        //*  =======================
        if (condition(pnd_L_Pnd_Inside_Read.getBoolean()))                                                                                                                //Natural: IF #INSIDE-READ
        {
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            pnd_Grand_Pnd_Net.compute(new ComputeParameters(false, pnd_Grand_Pnd_Net), pnd_Grand_Pnd_Trnsfr_Sw_To.subtract(pnd_Grand_Pnd_Trnsfr_Sw_From));                //Natural: COMPUTE #GRAND.#NET = #GRAND.#TRNSFR-SW-TO - #GRAND.#TRNSFR-SW-FROM
            getReports().write(1, ReportOption.NOTITLE,"Transfer/Switch Grand Total ",new ColumnSpacing(20),pnd_Grand_Pnd_Trnsfr_Sw_From, new ReportEditMask              //Natural: WRITE ( 1 ) 'Transfer/Switch Grand Total ' 20X #GRAND.#TRNSFR-SW-FROM ( EM = ZZZ,ZZZ,ZZZ.99 ) 22X #GRAND.#TRNSFR-SW-TO ( EM = ZZZ,ZZZ,ZZZ.99 ) 14X #GRAND.#NET ( EM = ZZZ,ZZZ,ZZZ.99- )
                ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(22),pnd_Grand_Pnd_Trnsfr_Sw_To, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(14),pnd_Grand_Pnd_Net, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ======================================================================
        //*                           START OF SUBROUTINES
        //* ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRNSFR-SW-REPORT-PAGES
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-1-LINE
        //*  WRITE THE LINE
    }
    private void sub_Get_Business_Date() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'DC'
        //*  FETCH THE LATEST CONTROL RECORD FOR CNTRL CD
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        if (condition(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte().equals(getZero())))                                                                               //Natural: IF #IATN400-OUT.CNTRL-TODAYS-DTE EQ 0
        {
            getReports().write(0, "Control Record 'DC' does not have a business date.");                                                                                  //Natural: WRITE 'Control Record "DC" does not have a business date.'
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-BUSINESS-DATE
    }
    private void sub_Write_Trnsfr_Sw_Report_Pages() throws Exception                                                                                                      //Natural: WRITE-TRNSFR-SW-REPORT-PAGES
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte.reset();                                                                                                       //Natural: RESET #IAXFR-CTL-EFFCTVE-DTE
        //*  SET SEARCH KEY
        if (condition(pnd_L_Pnd_Transfer.getBoolean()))                                                                                                                   //Natural: IF #TRANSFER
        {
            pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.setValue(pnd_Const_Pnd_Rcrd_Type_Trnsfr);                                                                    //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE := #RCRD-TYPE-TRNSFR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.setValue(pnd_Const_Pnd_Rcrd_Type_Switch);                                                                    //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE := #RCRD-TYPE-SWITCH
            //*  BUSINESS DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte.setValue(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte());                                                     //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE := #IATN400-OUT.CNTRL-TODAYS-DTE
        //*  READ THE CONTROL FILE
        vw_iaa_Xfr_Cntrl.startDatabaseRead                                                                                                                                //Natural: READ IAA-XFR-CNTRL BY IAXFR-CTL-SUPER-DE-1 STARTING FROM #IAXFR-CTL-SUPER-DE-1
        (
        "RD1",
        new Wc[] { new Wc("IAXFR_CTL_SUPER_DE_1", ">=", pnd_Iaxfr_Ctl_Super_De_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IAXFR_CTL_SUPER_DE_1", "ASC") }
        );
        boolean endOfDataRd1 = true;
        boolean firstRd1 = true;
        RD1:
        while (condition(vw_iaa_Xfr_Cntrl.readNextRow("RD1")))
        {
            if (condition(vw_iaa_Xfr_Cntrl.getAstCOUNTER().greater(0)))
            {
                atBreakEventRd1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRd1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF IAXFR-CTL-EFFCTVE-DTE
            if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type.notEquals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type) || iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte.notEquals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte))) //Natural: IF IAXFR-CTL-RCRD-TYPE NE #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE OR IAXFR-CTL-CYCLE-DTE NE #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK THE POSITIONS OF TIAA STANDARD, TIAA GRADED AND TIAA REAL EST
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(1,":",40)), new ExamineSearch("1G"), new ExamineGivingIndex(pnd_Index_Pnd_Index_1g)); //Natural: EXAMINE IAXFR-CTL-FRM-FUND-CDE ( 1:40 ) FOR '1G' GIVING INDEX #INDEX-1G
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(1,":",40)), new ExamineSearch("1S"), new ExamineGivingIndex(pnd_Index_Pnd_Index_1s)); //Natural: EXAMINE IAXFR-CTL-FRM-FUND-CDE ( 1:40 ) FOR '1S' GIVING INDEX #INDEX-1S
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(1,":",20)), new ExamineSearch("09"), new ExamineGivingIndex(pnd_Index_Pnd_Index_09)); //Natural: EXAMINE IAXFR-CTL-FRM-FUND-CDE ( 1:20 ) FOR '09' GIVING INDEX #INDEX-09
            //*  030209
            DbsUtil.examine(new ExamineSource(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(1,":",20)), new ExamineSearch("11"), new ExamineGivingIndex(pnd_Index_Pnd_Index_11)); //Natural: EXAMINE IAXFR-CTL-FRM-FUND-CDE ( 1:20 ) FOR '11' GIVING INDEX #INDEX-11
            //*  030209
            if (condition(pnd_Index_Pnd_Index_1g.notEquals(1) || pnd_Index_Pnd_Index_1s.notEquals(21) || pnd_Index_Pnd_Index_09.notEquals(9) || pnd_Index_Pnd_Index_11.notEquals(11))) //Natural: IF #INDEX-1G NE 1 OR #INDEX-1S NE 21 OR #INDEX-09 NE 9 OR #INDEX-11 NE 11
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*                  Warning                       *");                                                                              //Natural: WRITE '*                  Warning                       *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*                  =======                       *");                                                                              //Natural: WRITE '*                  =======                       *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* The positions of certain funds are not         *");                                                                              //Natural: WRITE '* The positions of certain funds are not         *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* correct. This may cause an error in the Totals *");                                                                              //Natural: WRITE '* correct. This may cause an error in the Totals *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Position of TIAA Graded should be '1'          *");                                                                              //Natural: WRITE '* Position of TIAA Graded should be "1"          *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Position of TIAA Standard Should be '21'       *");                                                                              //Natural: WRITE '* Position of TIAA Standard Should be "21"       *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Position of Annual Real Estate should be '09'  *");                                                                              //Natural: WRITE '* Position of Annual Real Estate should be "09"  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030209
                getReports().write(0, "* Position of Annual Access should be '11'       *");                                                                              //Natural: WRITE '* Position of Annual Access should be "11"       *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*  TIAA Graded pos        = ",pnd_Index_Pnd_Index_1g);                                                                             //Natural: WRITE '*  TIAA Graded pos        = ' #INDEX-1G
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*  TIAA Standard pos      = ",pnd_Index_Pnd_Index_1s);                                                                             //Natural: WRITE '*  TIAA Standard pos      = ' #INDEX-1S
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*  Annual Real Estate pos = ",pnd_Index_Pnd_Index_09);                                                                             //Natural: WRITE '*  Annual Real Estate pos = ' #INDEX-09
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030209
                getReports().write(0, "*  Annual Access pos      = ",pnd_Index_Pnd_Index_11);                                                                             //Natural: WRITE '*  Annual Access pos      = ' #INDEX-11
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*  Program                =  ",Global.getPROGRAM());                                                                               //Natural: WRITE '*  Program                =  ' *PROGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ====================
            //*   WRITE ANNUAL FUNDS
            //*  ====================
            pnd_Temp_Tot.reset();                                                                                                                                         //Natural: RESET #TEMP-TOT
            pnd_L_Pnd_Annual.setValue(true);                                                                                                                              //Natural: ASSIGN #ANNUAL := TRUE
            pnd_L_Pnd_Inside_Read.setValue(true);                                                                                                                         //Natural: ASSIGN #INSIDE-READ := TRUE
            //*  WRITE TIAA STANDARD ANNUAL
            if (condition(pnd_Index_Pnd_Index_1s.notEquals(getZero()) && pnd_L_Pnd_Transfer.getBoolean()))                                                                //Natural: IF #INDEX-1S NE 0 AND #TRANSFER
            {
                pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_1s);                                                                                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-1S
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE TIAA GRADED ANNUAL
            if (condition(pnd_Index_Pnd_Index_1g.notEquals(getZero()) && pnd_L_Pnd_Transfer.getBoolean()))                                                                //Natural: IF #INDEX-1G NE 0 AND #TRANSFER
            {
                pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_1g);                                                                                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-1G
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE TIAA REAL ESTATE ANNUAL
            if (condition(pnd_Index_Pnd_Index_09.notEquals(getZero())))                                                                                                   //Natural: IF #INDEX-09 NE 0
            {
                pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_09);                                                                                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-09
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE TIAA ACCESS ANNUAL                    /* 030209 START
            if (condition(pnd_Index_Pnd_Index_11.notEquals(getZero())))                                                                                                   //Natural: IF #INDEX-11 NE 0
            {
                pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_11);                                                                                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-11
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030209 END
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                //*  SKIP TIAA GRAD AND REA
                //*  030209
                if (condition(pnd_I.equals(pnd_Index_Pnd_Index_1g) || pnd_I.equals(pnd_Index_Pnd_Index_09) || pnd_I.equals(pnd_Index_Pnd_Index_11)))                      //Natural: IF #I = #INDEX-1G OR = #INDEX-09 OR = #INDEX-11
                {
                    //*  FOR
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  CHECK THAT A FUND EXISTS ON THE FROM OR TO SIDE AND IF IT DOES
                //*  CHECK THAT THERE IS AN ASSEST AMOUNT > 0
                if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF IAXFR-CTL-FRM-FUND-CDE ( #I ) NE ' '
                {
                    pnd_Index_Pnd_Write_Index.setValue(pnd_I);                                                                                                            //Natural: ASSIGN #WRITE-INDEX := #I
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                    sub_Write_1_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ======================
            //*   WRITE MONTHLY FUNDS
            //*  ======================
            pnd_L_Pnd_Annual.reset();                                                                                                                                     //Natural: RESET #ANNUAL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PROCESS TIAA REAL ESTATE MONTHLY
            if (condition(pnd_Index_Pnd_Index_09.notEquals(getZero())))                                                                                                   //Natural: IF #INDEX-09 NE 0
            {
                pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_Index_Pnd_Index_09.add(20));                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-09 + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  PROCESS TIAA ACCESS MONTHLY                 /* 030209 START
            if (condition(pnd_Index_Pnd_Index_11.notEquals(getZero())))                                                                                                   //Natural: IF #INDEX-11 NE 0
            {
                pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_Index_Pnd_Index_11.add(20));                               //Natural: ASSIGN #WRITE-INDEX := #INDEX-11 + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030209 END
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                //*  SKIP TIAA GRAD AND REA
                //*  030209
                if (condition(pnd_I.equals(pnd_Index_Pnd_Index_1g) || pnd_I.equals(pnd_Index_Pnd_Index_09) || pnd_I.equals(pnd_Index_Pnd_Index_11)))                      //Natural: IF #I = #INDEX-1G OR = #INDEX-09 OR = #INDEX-11
                {
                    //*  FOR
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  CHECK THAT A FUND EXISTS
                if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I.getDec().add(20)).notEquals(" ")))                                                      //Natural: IF IAXFR-CTL-FRM-FUND-CDE ( #I + 20 ) NE ' '
                {
                    pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_I.add(20));                                            //Natural: ASSIGN #WRITE-INDEX := #I + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                    sub_Write_1_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            if (condition(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.equals(pnd_Const_Pnd_Rcrd_Type_Trnsfr)))                                                       //Natural: IF #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE = #RCRD-TYPE-TRNSFR
            {
                pnd_Write_Line_Pnd_Desc.setValue("Transfer Totals:");                                                                                                     //Natural: ASSIGN #WRITE-LINE.#DESC := 'Transfer Totals:'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Write_Line_Pnd_Desc.setValue("Switch Totals:");                                                                                                       //Natural: ASSIGN #WRITE-LINE.#DESC := 'Switch Totals:'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Write_Line_Pnd_From.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt);                                                                                //Natural: ASSIGN #WRITE-LINE.#FROM := IAXFR-CTL-TOTAL-FRM-ASSET-AMT
            pnd_Write_Line_Pnd_To.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt);                                                                                   //Natural: ASSIGN #WRITE-LINE.#TO := IAXFR-CTL-TOTAL-TO-ASSET-AMT
            pnd_Write_Line_Pnd_Net.compute(new ComputeParameters(false, pnd_Write_Line_Pnd_Net), pnd_Write_Line_Pnd_To.subtract(pnd_Write_Line_Pnd_From));                //Natural: COMPUTE #WRITE-LINE.#NET = #WRITE-LINE.#TO - #WRITE-LINE.#FROM
            getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new ColumnSpacing(5),pnd_Write_Line_Pnd_From, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new    //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 05X #WRITE-LINE.#FROM ( EM = ZZZ,ZZZ,ZZZ.99 ) 22X #WRITE-LINE.#TO ( EM = ZZZ,ZZZ,ZZZ.99 ) 14X #WRITE-LINE.#NET ( EM = ZZZ,ZZZ,ZZZ.99- )
                ColumnSpacing(22),pnd_Write_Line_Pnd_To, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(14),pnd_Write_Line_Pnd_Net, new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Temp_Tot_Pnd_Trnsfr_From.notEquals(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt) || pnd_Temp_Tot_Pnd_Trnsfr_To.notEquals(iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt))) //Natural: IF #TEMP-TOT.#TRNSFR-FROM NE IAXFR-CTL-TOTAL-FRM-ASSET-AMT OR #TEMP-TOT.#TRNSFR-TO NE IAXFR-CTL-TOTAL-TO-ASSET-AMT
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(25),"********************************************************");                             //Natural: WRITE ( 1 ) 25X '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(25),"***     WARNING :- Totals do not Balance.         ******");                             //Natural: WRITE ( 1 ) 25X '***     WARNING :- Totals do not Balance.         ******'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(25),"***     From Asset value Total =",pnd_Temp_Tot_Pnd_Trnsfr_From);                        //Natural: WRITE ( 1 ) 25X '***     From Asset value Total =' #TEMP-TOT.#TRNSFR-FROM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(25),"***     To Asset value Total   =",pnd_Temp_Tot_Pnd_Trnsfr_To);                          //Natural: WRITE ( 1 ) 25X '***     To Asset value Total   =' #TEMP-TOT.#TRNSFR-TO
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(25),"********************************************************");                             //Natural: WRITE ( 1 ) 25X '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  RD1.
            rD1Global_astIsnOld.setValue(vw_iaa_Xfr_Cntrl.getAstISN("RD1"));                                                                                              //Natural: END-READ
        }
        if (condition(vw_iaa_Xfr_Cntrl.getAstCOUNTER().greater(0)))
        {
            atBreakEventRd1(endOfDataRd1);
        }
        if (Global.isEscape()) return;
        //*  IF NO DATA WAS FOUND
        if (condition(! (pnd_L_Pnd_Inside_Read.getBoolean())))                                                                                                            //Natural: IF NOT #INSIDE-READ
        {
            pnd_L_Pnd_No_Rec_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #NO-REC-FOUND := TRUE
            pnd_L_Pnd_Annual.setValue(true);                                                                                                                              //Natural: ASSIGN #ANNUAL := TRUE
            FOR03:                                                                                                                                                        //Natural: FOR #I = 1 TO 2
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
            {
                if (condition(pnd_I.equals(2)))                                                                                                                           //Natural: IF #I = 2
                {
                    pnd_L_Pnd_Annual.reset();                                                                                                                             //Natural: RESET #ANNUAL
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().skip(1, 10);                                                                                                                                 //Natural: SKIP ( 1 ) 10
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"******************************************************");                               //Natural: WRITE ( 1 ) 20X '******************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"***                                             ******");                               //Natural: WRITE ( 1 ) 20X '***                                             ******'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.equals(pnd_Const_Pnd_Rcrd_Type_Trnsfr)))                                                   //Natural: IF #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE = #RCRD-TYPE-TRNSFR
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"***     No Transfer Data found for this Page    ******");                           //Natural: WRITE ( 1 ) 20X '***     No Transfer Data found for this Page    ******'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"***     No Switch Data found for this Page      ******");                           //Natural: WRITE ( 1 ) 20X '***     No Switch Data found for this Page      ******'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"***                                             ******");                               //Natural: WRITE ( 1 ) 20X '***                                             ******'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"******************************************************");                               //Natural: WRITE ( 1 ) 20X '******************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_L_Pnd_No_Rec_Found.reset();                                                                                                                               //Natural: RESET #NO-REC-FOUND
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE-TRNSFR-SW-REPORT-PAGES
    }
    private void sub_Write_1_Line() throws Exception                                                                                                                      //Natural: WRITE-1-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  THIS CODE ASSUMES THAT FOR ALL RECORDS ON THE FILE THE POSITIONS OF
        //*  A FUND REMAINS THE SAME.
        //*  IE:- I ONLY FETCH A FUND DESCRIPTION ONCE AND PLACE IT IN THE CORRECT
        //*  POSITION.
        if (condition(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).equals(" ") && pnd_L_Pnd_Annual.getBoolean()))                                                    //Natural: IF #FUND-DESC ( #WRITE-INDEX ) = ' ' AND #ANNUAL
        {
            pnd_Iaan050a_Pda_Pnd_Parm_Desc.reset();                                                                                                                       //Natural: RESET #PARM-DESC
            pnd_Iaan050a_Pda_Pnd_Parm_Fund_2.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Index_Pnd_Write_Index));                                          //Natural: ASSIGN #PARM-FUND-2 := IAXFR-CTL-FRM-FUND-CDE ( #WRITE-INDEX )
            pnd_Iaan050a_Pda_Pnd_Parm_Len.setValue(25);                                                                                                                   //Natural: ASSIGN #PARM-LEN := 25
            //*  CALLNAT 'IAAN050A'
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Iaan050a_Pda);                                                                                 //Natural: CALLNAT 'IAAN051A' #IAAN050A-PDA
            if (condition(Global.isEscape())) return;
            //*  TIAA FUNDS
            if (condition(pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1g) || pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1s)))                          //Natural: IF #WRITE-INDEX = #INDEX-1G OR = #INDEX-1S
            {
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                                               //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX ) := #PARM-DESC
                //*  ANNUAL
                //*  MONTHLY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                                               //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX ) := #PARM-DESC
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index.getDec().add(20)).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                              //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX + 20 ) := #PARM-DESC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Write_Line_Pnd_Desc.setValue(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index));                                                                              //Natural: ASSIGN #WRITE-LINE.#DESC := #FUND-DESC ( #WRITE-INDEX )
        pnd_Write_Line_Pnd_From.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt.getValue(pnd_Index_Pnd_Write_Index));                                                      //Natural: ASSIGN #WRITE-LINE.#FROM := IAXFR-CTL-FRM-ASSET-AMT ( #WRITE-INDEX )
        pnd_Write_Line_Pnd_To.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt.getValue(pnd_Index_Pnd_Write_Index));                                                         //Natural: ASSIGN #WRITE-LINE.#TO := IAXFR-CTL-TO-ASSET-AMT ( #WRITE-INDEX )
        pnd_Write_Line_Pnd_Net.compute(new ComputeParameters(false, pnd_Write_Line_Pnd_Net), pnd_Write_Line_Pnd_To.subtract(pnd_Write_Line_Pnd_From));                    //Natural: COMPUTE #WRITE-LINE.#NET = #WRITE-LINE.#TO - #WRITE-LINE.#FROM
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new ColumnSpacing(5),pnd_Write_Line_Pnd_From, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new        //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 05X #WRITE-LINE.#FROM ( EM = ZZZ,ZZZ,ZZZ.99 ) 22X #WRITE-LINE.#TO ( EM = ZZZ,ZZZ,ZZZ.99 ) 14X #WRITE-LINE.#NET ( EM = ZZZ,ZZZ,ZZZ.99- )
            ColumnSpacing(22),pnd_Write_Line_Pnd_To, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(14),pnd_Write_Line_Pnd_Net, new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ADD TOTALS
        //*  FOR BALANCING
        pnd_Temp_Tot_Pnd_Trnsfr_From.nadd(pnd_Write_Line_Pnd_From);                                                                                                       //Natural: ADD #WRITE-LINE.#FROM TO #TEMP-TOT.#TRNSFR-FROM
        pnd_Grand_Pnd_Trnsfr_Sw_From.nadd(pnd_Write_Line_Pnd_From);                                                                                                       //Natural: ADD #WRITE-LINE.#FROM TO #GRAND.#TRNSFR-SW-FROM
        //*  FOR BALANCING
        pnd_Temp_Tot_Pnd_Trnsfr_To.nadd(pnd_Write_Line_Pnd_To);                                                                                                           //Natural: ADD #WRITE-LINE.#TO TO #TEMP-TOT.#TRNSFR-TO
        pnd_Grand_Pnd_Trnsfr_Sw_To.nadd(pnd_Write_Line_Pnd_To);                                                                                                           //Natural: ADD #WRITE-LINE.#TO TO #GRAND.#TRNSFR-SW-TO
        //*  WRITE-1-LINE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN DATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new                 //Natural: WRITE ( 1 ) NOTITLE 01X 'RUN DATE : ' *DATX ( EM = MM/DD/YYYY ) 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBITY SYSTEM ' 13X 'PROGRAM ID : ' *PROGRAM
                        ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBITY SYSTEM ",new ColumnSpacing(13),"PROGRAM ID : ",Global.getPROGRAM());
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(24),"ASSET TRANSFER/SWITCH VALUE CONTROL REPORT FOR",new  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 24X 'ASSET TRANSFER/SWITCH VALUE CONTROL REPORT FOR' 17X 'PAGE       :  ' *PAGE-NUMBER ( 1 )
                        ColumnSpacing(17),"PAGE       :  ",getReports().getPageNumberDbs(1));
                    //*  SET SUB HEADINGS
                    if (condition(pnd_L_Pnd_Transfer.getBoolean()))                                                                                                       //Natural: IF #TRANSFER
                    {
                        if (condition(pnd_L_Pnd_Annual.getBoolean()))                                                                                                     //Natural: IF #ANNUAL
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(45),pnd_Headings_Pnd_Transfer_Annual,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),  //Natural: WRITE ( 1 ) 45X #TRANSFER-ANNUAL #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = MM'/'DD'/'YYYY )
                                new ReportEditMask ("MM'/'DD'/'YYYY"));
                            //*  MONTHLY
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(45),pnd_Headings_Pnd_Transfer_Monthly,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),  //Natural: WRITE ( 1 ) 45X #TRANSFER-MONTHLY #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = MM'/'DD'/'YYYY )
                                new ReportEditMask ("MM'/'DD'/'YYYY"));
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SWITCHES
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_L_Pnd_Annual.getBoolean()))                                                                                                     //Natural: IF #ANNUAL
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(45),pnd_Headings_Pnd_Switch_From_Annual,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),  //Natural: WRITE ( 1 ) 45X #SWITCH-FROM-ANNUAL #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = MM'/'DD'/'YYYY )
                                new ReportEditMask ("MM'/'DD'/'YYYY"));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(45),pnd_Headings_Pnd_Switch_From_Monthly,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),  //Natural: WRITE ( 1 ) 45X #SWITCH-FROM-MONTHLY #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = MM'/'DD'/'YYYY )
                                new ReportEditMask ("MM'/'DD'/'YYYY"));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(! (pnd_L_Pnd_No_Rec_Found.getBoolean())))                                                                                               //Natural: IF NOT #NO-REC-FOUND
                    {
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"EFFECTIVE DATE : ",iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte, new ReportEditMask       //Natural: WRITE ( 1 ) 01X 'EFFECTIVE DATE : ' IAXFR-CTL-EFFCTVE-DTE ( EM = MM'/'DD'/'YYYY )
                            ("MM'/'DD'/'YYYY"));
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(5),"FUND                         ASSET VALUE TRANSFERRED FROM(-)",new                //Natural: WRITE ( 1 ) 05X 'FUND                         ASSET VALUE TRANSFERRED FROM(-)' 7X 'ASSET VALUE TRANSFERRED TO(+)' 10X 'ASSET VALUE NET(=)'
                            ColumnSpacing(7),"ASSET VALUE TRANSFERRED TO(+)",new ColumnSpacing(10),"ASSET VALUE NET(=)");
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (1)
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRd1() throws Exception {atBreakEventRd1(false);}
    private void atBreakEventRd1(boolean endOfData) throws Exception
    {
        boolean iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_DteIsBreak = iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte.isBreak(endOfData);
        if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_DteIsBreak))
        {
            //*  THIS IS TO HANDLE THE LAST RECORD ON THE FILE WHICH WILL BE READ
            //*  TWICE.
            if (condition(rD1Global_astIsnOld.equals(vw_iaa_Xfr_Cntrl.getAstISN("RD1"))))                                                                                 //Natural: IF OLD ( *ISN ) = *ISN
            {
                if (!endOfData)                                                                                                                                           //Natural: ESCAPE BOTTOM ( RD1. )
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "RD1");
                }
                if (condition(true)) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type.equals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type) && iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte.equals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte))) //Natural: IF IAXFR-CTL-RCRD-TYPE EQ #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE AND IAXFR-CTL-CYCLE-DTE EQ #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
