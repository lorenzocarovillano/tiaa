/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:51 PM
**        * FROM NATURAL PROGRAM : Iaap804a
************************************************************
**        * FILE NAME            : Iaap804a.java
**        * CLASS NAME           : Iaap804a
**        * INSTANCE NAME        : Iaap804a
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAP804A : PIN EXPANSION                           *
*      DATE     -  06/15                                              *
*                                                                     *
*  HISTORY:                                                           *
*  04/2017 OS PIN EXPANSION CHANGES MARKED 082017.                    *
*                                                                     *
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap804a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Input1;

    private DbsGroup pnd_Work_Input1__R_Field_1;
    private DbsField pnd_Work_Input1_Pnd_Wk_Pin;

    private DbsGroup pnd_Work_Input1__R_Field_2;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Pin;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Pin_5;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Other;

    private DbsGroup pnd_Work_Input1__R_Field_3;
    private DbsField pnd_Work_Input1_Cntrct_Nmbr;
    private DbsField pnd_Work_Input1_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Input1_Cntrct_Name_Free;
    private DbsField pnd_Work_Input1_Addrss_Lne_1;
    private DbsField pnd_Work_Input1_Addrss_Lne_2;
    private DbsField pnd_Work_Input1_Addrss_Lne_3;
    private DbsField pnd_Work_Input1_Addrss_Lne_4;
    private DbsField pnd_Work_Input1_Addrss_Lne_5;
    private DbsField pnd_Work_Input1_Addrss_Lne_6;
    private DbsField pnd_Work_Input1_Addrss_Postal_Data;

    private DbsGroup pnd_Work_Input1__R_Field_4;
    private DbsField pnd_Work_Input1_Zip_5;
    private DbsField pnd_Work_Input1_Addrss_Type_Cde;
    private DbsField pnd_Work_Out2;

    private DbsGroup pnd_Work_Out2__R_Field_5;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Pin_Out;

    private DbsGroup pnd_Work_Out2__R_Field_6;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Pin_Out_N;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Other_Out;
    private DbsField pnd_Work_Input3;

    private DbsGroup pnd_Work_Input3__R_Field_7;
    private DbsField pnd_Work_Input3_Pnd_Wk3_Pin_A;

    private DbsGroup pnd_Work_Input3__R_Field_8;
    private DbsField pnd_Work_Input3_Pnd_Wk3_Pin;
    private DbsField pnd_Work_Input3_Pnd_Wk3_Pin_5;
    private DbsField pnd_Work_Input3_Pnd_Wk3_Other;

    private DbsGroup pnd_Work_Input3__R_Field_9;
    private DbsField pnd_Work_Input3_Pnd_Cntrct_Nmbr;
    private DbsField pnd_Work_Input3_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Input3_Pnd_Cntrct_Name_Free;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_1;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_2;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_3;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_4;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_5;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Lne_6;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Postal_Data;

    private DbsGroup pnd_Work_Input3__R_Field_10;
    private DbsField pnd_Work_Input3_Pnd_Zip_5;
    private DbsField pnd_Work_Input3_Pnd_Addrss_Type_Cde;
    private DbsField pnd_Work_Out4;

    private DbsGroup pnd_Work_Out4__R_Field_11;
    private DbsField pnd_Work_Out4_Pnd_Wk4_Pin_Out;

    private DbsGroup pnd_Work_Out4__R_Field_12;
    private DbsField pnd_Work_Out4_Pnd_Wk4_Pin_Out_N;
    private DbsField pnd_Work_Out4_Pnd_Wk4_Other_Out;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_13;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_I;
    private DbsField pnd_Ovrrde_Payees;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Input1 = localVariables.newFieldInRecord("pnd_Work_Input1", "#WORK-INPUT1", FieldType.STRING, 377);

        pnd_Work_Input1__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Input1__R_Field_1", "REDEFINE", pnd_Work_Input1);
        pnd_Work_Input1_Pnd_Wk_Pin = pnd_Work_Input1__R_Field_1.newFieldInGroup("pnd_Work_Input1_Pnd_Wk_Pin", "#WK-PIN", FieldType.STRING, 12);

        pnd_Work_Input1__R_Field_2 = pnd_Work_Input1__R_Field_1.newGroupInGroup("pnd_Work_Input1__R_Field_2", "REDEFINE", pnd_Work_Input1_Pnd_Wk_Pin);
        pnd_Work_Input1_Pnd_Wk1_Pin = pnd_Work_Input1__R_Field_2.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Pin", "#WK1-PIN", FieldType.STRING, 7);
        pnd_Work_Input1_Pnd_Wk1_Pin_5 = pnd_Work_Input1__R_Field_2.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Pin_5", "#WK1-PIN-5", FieldType.STRING, 5);
        pnd_Work_Input1_Pnd_Wk1_Other = pnd_Work_Input1__R_Field_1.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Other", "#WK1-OTHER", FieldType.STRING, 365);

        pnd_Work_Input1__R_Field_3 = pnd_Work_Input1__R_Field_1.newGroupInGroup("pnd_Work_Input1__R_Field_3", "REDEFINE", pnd_Work_Input1_Pnd_Wk1_Other);
        pnd_Work_Input1_Cntrct_Nmbr = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Work_Input1_Cntrct_Payee_Cde = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Input1_Cntrct_Name_Free = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Work_Input1_Addrss_Lne_1 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Lne_2 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Lne_3 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Lne_4 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Lne_5 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Lne_6 = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Work_Input1_Addrss_Postal_Data = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Work_Input1__R_Field_4 = pnd_Work_Input1__R_Field_3.newGroupInGroup("pnd_Work_Input1__R_Field_4", "REDEFINE", pnd_Work_Input1_Addrss_Postal_Data);
        pnd_Work_Input1_Zip_5 = pnd_Work_Input1__R_Field_4.newFieldInGroup("pnd_Work_Input1_Zip_5", "ZIP-5", FieldType.STRING, 5);
        pnd_Work_Input1_Addrss_Type_Cde = pnd_Work_Input1__R_Field_3.newFieldInGroup("pnd_Work_Input1_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Work_Out2 = localVariables.newFieldInRecord("pnd_Work_Out2", "#WORK-OUT2", FieldType.STRING, 377);

        pnd_Work_Out2__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Out2__R_Field_5", "REDEFINE", pnd_Work_Out2);
        pnd_Work_Out2_Pnd_Wk2_Pin_Out = pnd_Work_Out2__R_Field_5.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Pin_Out", "#WK2-PIN-OUT", FieldType.STRING, 12);

        pnd_Work_Out2__R_Field_6 = pnd_Work_Out2__R_Field_5.newGroupInGroup("pnd_Work_Out2__R_Field_6", "REDEFINE", pnd_Work_Out2_Pnd_Wk2_Pin_Out);
        pnd_Work_Out2_Pnd_Wk2_Pin_Out_N = pnd_Work_Out2__R_Field_6.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Pin_Out_N", "#WK2-PIN-OUT-N", FieldType.NUMERIC, 
            12);
        pnd_Work_Out2_Pnd_Wk2_Other_Out = pnd_Work_Out2__R_Field_5.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Other_Out", "#WK2-OTHER-OUT", FieldType.STRING, 
            365);
        pnd_Work_Input3 = localVariables.newFieldInRecord("pnd_Work_Input3", "#WORK-INPUT3", FieldType.STRING, 449);

        pnd_Work_Input3__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Input3__R_Field_7", "REDEFINE", pnd_Work_Input3);
        pnd_Work_Input3_Pnd_Wk3_Pin_A = pnd_Work_Input3__R_Field_7.newFieldInGroup("pnd_Work_Input3_Pnd_Wk3_Pin_A", "#WK3-PIN-A", FieldType.STRING, 12);

        pnd_Work_Input3__R_Field_8 = pnd_Work_Input3__R_Field_7.newGroupInGroup("pnd_Work_Input3__R_Field_8", "REDEFINE", pnd_Work_Input3_Pnd_Wk3_Pin_A);
        pnd_Work_Input3_Pnd_Wk3_Pin = pnd_Work_Input3__R_Field_8.newFieldInGroup("pnd_Work_Input3_Pnd_Wk3_Pin", "#WK3-PIN", FieldType.STRING, 7);
        pnd_Work_Input3_Pnd_Wk3_Pin_5 = pnd_Work_Input3__R_Field_8.newFieldInGroup("pnd_Work_Input3_Pnd_Wk3_Pin_5", "#WK3-PIN-5", FieldType.STRING, 5);
        pnd_Work_Input3_Pnd_Wk3_Other = pnd_Work_Input3__R_Field_7.newFieldInGroup("pnd_Work_Input3_Pnd_Wk3_Other", "#WK3-OTHER", FieldType.STRING, 437);

        pnd_Work_Input3__R_Field_9 = pnd_Work_Input3__R_Field_7.newGroupInGroup("pnd_Work_Input3__R_Field_9", "REDEFINE", pnd_Work_Input3_Pnd_Wk3_Other);
        pnd_Work_Input3_Pnd_Cntrct_Nmbr = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Cntrct_Nmbr", "#CNTRCT-NMBR", FieldType.STRING, 
            10);
        pnd_Work_Input3_Pnd_Cntrct_Payee_Cde = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Input3_Pnd_Cntrct_Name_Free = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Cntrct_Name_Free", "#CNTRCT-NAME-FREE", 
            FieldType.STRING, 35);
        pnd_Work_Input3_Pnd_Addrss_Lne_1 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_1", "#ADDRSS-LNE-1", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Lne_2 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_2", "#ADDRSS-LNE-2", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Lne_3 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_3", "#ADDRSS-LNE-3", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Lne_4 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_4", "#ADDRSS-LNE-4", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Lne_5 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_5", "#ADDRSS-LNE-5", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Lne_6 = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Lne_6", "#ADDRSS-LNE-6", FieldType.STRING, 
            35);
        pnd_Work_Input3_Pnd_Addrss_Postal_Data = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Postal_Data", "#ADDRSS-POSTAL-DATA", 
            FieldType.STRING, 32);

        pnd_Work_Input3__R_Field_10 = pnd_Work_Input3__R_Field_9.newGroupInGroup("pnd_Work_Input3__R_Field_10", "REDEFINE", pnd_Work_Input3_Pnd_Addrss_Postal_Data);
        pnd_Work_Input3_Pnd_Zip_5 = pnd_Work_Input3__R_Field_10.newFieldInGroup("pnd_Work_Input3_Pnd_Zip_5", "#ZIP-5", FieldType.STRING, 5);
        pnd_Work_Input3_Pnd_Addrss_Type_Cde = pnd_Work_Input3__R_Field_9.newFieldInGroup("pnd_Work_Input3_Pnd_Addrss_Type_Cde", "#ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Work_Out4 = localVariables.newFieldInRecord("pnd_Work_Out4", "#WORK-OUT4", FieldType.STRING, 449);

        pnd_Work_Out4__R_Field_11 = localVariables.newGroupInRecord("pnd_Work_Out4__R_Field_11", "REDEFINE", pnd_Work_Out4);
        pnd_Work_Out4_Pnd_Wk4_Pin_Out = pnd_Work_Out4__R_Field_11.newFieldInGroup("pnd_Work_Out4_Pnd_Wk4_Pin_Out", "#WK4-PIN-OUT", FieldType.STRING, 12);

        pnd_Work_Out4__R_Field_12 = pnd_Work_Out4__R_Field_11.newGroupInGroup("pnd_Work_Out4__R_Field_12", "REDEFINE", pnd_Work_Out4_Pnd_Wk4_Pin_Out);
        pnd_Work_Out4_Pnd_Wk4_Pin_Out_N = pnd_Work_Out4__R_Field_12.newFieldInGroup("pnd_Work_Out4_Pnd_Wk4_Pin_Out_N", "#WK4-PIN-OUT-N", FieldType.NUMERIC, 
            12);
        pnd_Work_Out4_Pnd_Wk4_Other_Out = pnd_Work_Out4__R_Field_11.newFieldInGroup("pnd_Work_Out4_Pnd_Wk4_Other_Out", "#WK4-OTHER-OUT", FieldType.STRING, 
            437);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_13", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_13.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_13.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_13.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_13.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Ovrrde_Payees = localVariables.newFieldArrayInRecord("pnd_Ovrrde_Payees", "#OVRRDE-PAYEES", FieldType.STRING, 35, new DbsArrayController(1, 
            10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap804a() throws Exception
    {
        super("Iaap804a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM GET-OVERRIDE
        sub_Get_Override();
        if (condition(Global.isEscape())) {return;}
        //*  CORR ADDRESS
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-INPUT1
        while (condition(getWorkFiles().read(1, pnd_Work_Input1)))
        {
            if (condition(pnd_Work_Input1_Pnd_Wk1_Pin.equals(" ")))                                                                                                       //Natural: IF #WK1-PIN = ' '
            {
                pnd_Work_Input1_Pnd_Wk1_Pin.setValue("0000000");                                                                                                          //Natural: MOVE '0000000' TO #WK1-PIN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Input1_Addrss_Type_Cde.equals("C")))                                                                                                   //Natural: IF ADDRSS-TYPE-CDE = 'C'
            {
                                                                                                                                                                          //Natural: PERFORM ADD-CANADA-TO-CORR-ADDRESS-LINES
                sub_Add_Canada_To_Corr_Address_Lines();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Input1_Addrss_Type_Cde.equals("U")))                                                                                                   //Natural: IF ADDRSS-TYPE-CDE = 'U'
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-ZIP-CORR-FORMAT
                sub_Check_Zip_Corr_Format();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet121 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK-PIN IS ( N12 )
            if (condition(DbsUtil.is(pnd_Work_Input1_Pnd_Wk_Pin.getText(),"N12")))
            {
                decideConditionsMet121++;
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out2_Pnd_Wk2_Pin_Out_N), pnd_Work_Input1_Pnd_Wk_Pin.val());                 //Natural: ASSIGN #WK2-PIN-OUT-N := VAL ( #WK-PIN )
            }                                                                                                                                                             //Natural: WHEN #WK1-PIN IS ( N7 )
            else if (condition(DbsUtil.is(pnd_Work_Input1_Pnd_Wk1_Pin.getText(),"N7")))
            {
                decideConditionsMet121++;
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out2_Pnd_Wk2_Pin_Out_N), pnd_Work_Input1_Pnd_Wk1_Pin.val());                //Natural: ASSIGN #WK2-PIN-OUT-N := VAL ( #WK1-PIN )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.setValue(0);                                                                                                              //Natural: ASSIGN #WK2-PIN-OUT-N := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  MOVE #WK1-PIN    TO #WK2-PIN-OUT               /* 082017 END
            pnd_Work_Out2_Pnd_Wk2_Other_Out.setValue(pnd_Work_Input1_Pnd_Wk1_Other);                                                                                      //Natural: MOVE #WK1-OTHER TO #WK2-OTHER-OUT
            getWorkFiles().write(2, false, pnd_Work_Out2);                                                                                                                //Natural: WRITE WORK FILE 2 #WORK-OUT2
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CHECK MAIL - OK
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #WORK-INPUT3
        while (condition(getWorkFiles().read(3, pnd_Work_Input3)))
        {
            if (condition(pnd_Work_Input3_Pnd_Wk3_Pin.equals(" ")))                                                                                                       //Natural: IF #WK3-PIN = ' '
            {
                pnd_Work_Input3_Pnd_Wk3_Pin.setValue("0000000");                                                                                                          //Natural: MOVE '0000000' TO #WK3-PIN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Input3_Pnd_Addrss_Type_Cde.equals("C")))                                                                                               //Natural: IF #ADDRSS-TYPE-CDE = 'C'
            {
                                                                                                                                                                          //Natural: PERFORM ADD-CANADA-TO-CHCK-ADDRESS-LINES
                sub_Add_Canada_To_Chck_Address_Lines();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Input3_Pnd_Addrss_Type_Cde.equals("U")))                                                                                               //Natural: IF #ADDRSS-TYPE-CDE = 'U'
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-ZIP-CHCK-FORMAT
                sub_Check_Zip_Chck_Format();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE #WK3-PIN    TO #WK4-PIN-OUT               /* 082017 START
            short decideConditionsMet145 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK3-PIN-A IS ( N12 )
            if (condition(DbsUtil.is(pnd_Work_Input3_Pnd_Wk3_Pin_A.getText(),"N12")))
            {
                decideConditionsMet145++;
                pnd_Work_Out4_Pnd_Wk4_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out4_Pnd_Wk4_Pin_Out_N), pnd_Work_Input3_Pnd_Wk3_Pin_A.val());              //Natural: ASSIGN #WK4-PIN-OUT-N := VAL ( #WK3-PIN-A )
            }                                                                                                                                                             //Natural: WHEN #WK3-PIN IS ( N7 )
            else if (condition(DbsUtil.is(pnd_Work_Input3_Pnd_Wk3_Pin.getText(),"N7")))
            {
                decideConditionsMet145++;
                pnd_Work_Out4_Pnd_Wk4_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out4_Pnd_Wk4_Pin_Out_N), pnd_Work_Input3_Pnd_Wk3_Pin.val());                //Natural: ASSIGN #WK4-PIN-OUT-N := VAL ( #WK3-PIN )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Out4_Pnd_Wk4_Pin_Out_N.setValue(0);                                                                                                              //Natural: ASSIGN #WK4-PIN-OUT-N := 0
                //*  082017 END
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Ovrrde_Payees.getValue("*").notEquals(" ")))                                                                                                //Natural: IF #OVRRDE-PAYEES ( * ) NE ' '
            {
                if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_1.notEquals(" ") && pnd_Work_Input3_Pnd_Addrss_Lne_1.equals(pnd_Ovrrde_Payees.getValue("*"))))               //Natural: IF #ADDRSS-LNE-1 NE ' ' AND #ADDRSS-LNE-1 = #OVRRDE-PAYEES ( * )
                {
                    getReports().write(0, "BEFORE");                                                                                                                      //Natural: WRITE 'BEFORE'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Work_Input3_Pnd_Cntrct_Nmbr,"=",pnd_Work_Input3_Pnd_Cntrct_Payee_Cde);                                                  //Natural: WRITE '=' #CNTRCT-NMBR '=' #CNTRCT-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Work_Input3_Pnd_Cntrct_Name_Free,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_1,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_2, //Natural: WRITE '=' #CNTRCT-NAME-FREE / '=' #ADDRSS-LNE-1 / '=' #ADDRSS-LNE-2 / '=' #ADDRSS-LNE-3 / '=' #ADDRSS-LNE-4 / '=' #ADDRSS-LNE-5 / '=' #ADDRSS-LNE-6
                        NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_3,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_4,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_5,
                        NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_6);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Work_Input3_Pnd_Cntrct_Name_Free.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_1);                                                                      //Natural: ASSIGN #CNTRCT-NAME-FREE := #ADDRSS-LNE-1
                    pnd_Work_Input3_Pnd_Addrss_Lne_1.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_2);                                                                          //Natural: ASSIGN #ADDRSS-LNE-1 := #ADDRSS-LNE-2
                    pnd_Work_Input3_Pnd_Addrss_Lne_2.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_3);                                                                          //Natural: ASSIGN #ADDRSS-LNE-2 := #ADDRSS-LNE-3
                    pnd_Work_Input3_Pnd_Addrss_Lne_3.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_4);                                                                          //Natural: ASSIGN #ADDRSS-LNE-3 := #ADDRSS-LNE-4
                    pnd_Work_Input3_Pnd_Addrss_Lne_4.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_5);                                                                          //Natural: ASSIGN #ADDRSS-LNE-4 := #ADDRSS-LNE-5
                    pnd_Work_Input3_Pnd_Addrss_Lne_5.setValue(pnd_Work_Input3_Pnd_Addrss_Lne_6);                                                                          //Natural: ASSIGN #ADDRSS-LNE-5 := #ADDRSS-LNE-6
                    pnd_Work_Input3_Pnd_Addrss_Lne_6.setValue(" ");                                                                                                       //Natural: ASSIGN #ADDRSS-LNE-6 := ' '
                    getReports().write(0, "AFTER");                                                                                                                       //Natural: WRITE 'AFTER'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Work_Input3_Pnd_Cntrct_Nmbr,"=",pnd_Work_Input3_Pnd_Cntrct_Payee_Cde);                                                  //Natural: WRITE '=' #CNTRCT-NMBR '=' #CNTRCT-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Work_Input3_Pnd_Cntrct_Name_Free,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_1,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_2, //Natural: WRITE '=' #CNTRCT-NAME-FREE / '=' #ADDRSS-LNE-1 / '=' #ADDRSS-LNE-2 / '=' #ADDRSS-LNE-3 / '=' #ADDRSS-LNE-4 / '=' #ADDRSS-LNE-5 / '=' #ADDRSS-LNE-6
                        NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_3,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_4,NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_5,
                        NEWLINE,"=",pnd_Work_Input3_Pnd_Addrss_Lne_6);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Out4_Pnd_Wk4_Other_Out.setValue(pnd_Work_Input3_Pnd_Wk3_Other);                                                                                      //Natural: MOVE #WK3-OTHER TO #WK4-OTHER-OUT
            getWorkFiles().write(4, false, pnd_Work_Out4);                                                                                                                //Natural: WRITE WORK FILE 4 #WORK-OUT4
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OVERRIDE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CANADA-TO-CORR-ADDRESS-LINES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CANADA-TO-CHCK-ADDRESS-LINES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ZIP-CORR-FORMAT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ZIP-CHCK-FORMAT
    }
    private void sub_Get_Override() throws Exception                                                                                                                      //Natural: GET-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ076");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ076'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("OVR");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'OVR'
        //* *#NAZ-TABLE-LVL3-ID := 'PAYEE'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ03",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ03:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ03")))
        {
            //*  OR ALLOW MULTIPLE
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)))
            {
                //*      NAZ-TBL-RCRD-LVL3-ID NE #NAZ-TABLE-LVL3-ID /     PAYEES
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_Ovrrde_Payees.getValue(pnd_I).setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                           //Natural: ASSIGN #OVRRDE-PAYEES ( #I ) := NAZ-TBL-RCRD-DSCRPTN-TXT
            getReports().write(0, "=",pnd_Ovrrde_Payees.getValue(pnd_I));                                                                                                 //Natural: WRITE '=' #OVRRDE-PAYEES ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Add_Canada_To_Corr_Address_Lines() throws Exception                                                                                                  //Natural: ADD-CANADA-TO-CORR-ADDRESS-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet222 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADDRSS-LNE-1 NE ' ' AND ADDRSS-LNE-1 NE 'CANADA' AND ADDRSS-LNE-2 = ' '
        if (condition(pnd_Work_Input1_Addrss_Lne_1.notEquals(" ") && pnd_Work_Input1_Addrss_Lne_1.notEquals("CANADA") && pnd_Work_Input1_Addrss_Lne_2.equals(" ")))
        {
            decideConditionsMet222++;
            pnd_Work_Input1_Addrss_Lne_2.setValue("CANADA");                                                                                                              //Natural: ASSIGN ADDRSS-LNE-2 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN ADDRSS-LNE-2 NE ' ' AND ADDRSS-LNE-2 NE 'CANADA' AND ADDRSS-LNE-3 = ' '
        else if (condition(pnd_Work_Input1_Addrss_Lne_2.notEquals(" ") && pnd_Work_Input1_Addrss_Lne_2.notEquals("CANADA") && pnd_Work_Input1_Addrss_Lne_3.equals(" ")))
        {
            decideConditionsMet222++;
            pnd_Work_Input1_Addrss_Lne_3.setValue("CANADA");                                                                                                              //Natural: ASSIGN ADDRSS-LNE-3 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN ADDRSS-LNE-3 NE ' ' AND ADDRSS-LNE-3 NE 'CANADA' AND ADDRSS-LNE-4 = ' '
        else if (condition(pnd_Work_Input1_Addrss_Lne_3.notEquals(" ") && pnd_Work_Input1_Addrss_Lne_3.notEquals("CANADA") && pnd_Work_Input1_Addrss_Lne_4.equals(" ")))
        {
            decideConditionsMet222++;
            pnd_Work_Input1_Addrss_Lne_4.setValue("CANADA");                                                                                                              //Natural: ASSIGN ADDRSS-LNE-4 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN ADDRSS-LNE-4 NE ' ' AND ADDRSS-LNE-4 NE 'CANADA' AND ADDRSS-LNE-5 = ' '
        else if (condition(pnd_Work_Input1_Addrss_Lne_4.notEquals(" ") && pnd_Work_Input1_Addrss_Lne_4.notEquals("CANADA") && pnd_Work_Input1_Addrss_Lne_5.equals(" ")))
        {
            decideConditionsMet222++;
            pnd_Work_Input1_Addrss_Lne_5.setValue("CANADA");                                                                                                              //Natural: ASSIGN ADDRSS-LNE-5 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Add_Canada_To_Chck_Address_Lines() throws Exception                                                                                                  //Natural: ADD-CANADA-TO-CHCK-ADDRESS-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet239 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ADDRSS-LNE-1 NE ' ' AND #ADDRSS-LNE-1 NE 'CANADA' AND #ADDRSS-LNE-2 = ' '
        if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_1.notEquals(" ") && pnd_Work_Input3_Pnd_Addrss_Lne_1.notEquals("CANADA") && pnd_Work_Input3_Pnd_Addrss_Lne_2.equals(" ")))
        {
            decideConditionsMet239++;
            pnd_Work_Input3_Pnd_Addrss_Lne_2.setValue("CANADA");                                                                                                          //Natural: ASSIGN #ADDRSS-LNE-2 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #ADDRSS-LNE-2 NE ' ' AND #ADDRSS-LNE-2 NE 'CANADA' AND #ADDRSS-LNE-3 = ' '
        else if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_2.notEquals(" ") && pnd_Work_Input3_Pnd_Addrss_Lne_2.notEquals("CANADA") && pnd_Work_Input3_Pnd_Addrss_Lne_3.equals(" ")))
        {
            decideConditionsMet239++;
            pnd_Work_Input3_Pnd_Addrss_Lne_3.setValue("CANADA");                                                                                                          //Natural: ASSIGN #ADDRSS-LNE-3 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #ADDRSS-LNE-3 NE ' ' AND #ADDRSS-LNE-3 NE 'CANADA' AND #ADDRSS-LNE-4 = ' '
        else if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_3.notEquals(" ") && pnd_Work_Input3_Pnd_Addrss_Lne_3.notEquals("CANADA") && pnd_Work_Input3_Pnd_Addrss_Lne_4.equals(" ")))
        {
            decideConditionsMet239++;
            pnd_Work_Input3_Pnd_Addrss_Lne_4.setValue("CANADA");                                                                                                          //Natural: ASSIGN #ADDRSS-LNE-4 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #ADDRSS-LNE-4 NE ' ' AND #ADDRSS-LNE-4 NE 'CANADA' AND #ADDRSS-LNE-5 = ' '
        else if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_4.notEquals(" ") && pnd_Work_Input3_Pnd_Addrss_Lne_4.notEquals("CANADA") && pnd_Work_Input3_Pnd_Addrss_Lne_5.equals(" ")))
        {
            decideConditionsMet239++;
            pnd_Work_Input3_Pnd_Addrss_Lne_5.setValue("CANADA");                                                                                                          //Natural: ASSIGN #ADDRSS-LNE-5 := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Check_Zip_Corr_Format() throws Exception                                                                                                             //Natural: CHECK-ZIP-CORR-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        if (condition(DbsUtil.maskMatches(pnd_Work_Input1_Zip_5,"99999")))                                                                                                //Natural: IF ZIP-5 = MASK ( 99999 )
        {
            short decideConditionsMet258 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADDRSS-LNE-5 = ZIP-5
            if (condition(pnd_Work_Input1_Addrss_Lne_5.equals(pnd_Work_Input1_Zip_5)))
            {
                decideConditionsMet258++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input1_Addrss_Lne_4), new ExamineSearch(pnd_Work_Input1_Zip_5), new ExamineGivingNumber(pnd_I));               //Natural: EXAMINE ADDRSS-LNE-4 FOR ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input1_Addrss_Lne_4.setValue(DbsUtil.compress(pnd_Work_Input1_Addrss_Lne_4, pnd_Work_Input1_Addrss_Lne_5));                                  //Natural: COMPRESS ADDRSS-LNE-4 ADDRSS-LNE-5 INTO ADDRSS-LNE-4
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input1_Addrss_Lne_5.reset();                                                                                                                     //Natural: RESET ADDRSS-LNE-5
            }                                                                                                                                                             //Natural: WHEN ADDRSS-LNE-4 = ZIP-5
            else if (condition(pnd_Work_Input1_Addrss_Lne_4.equals(pnd_Work_Input1_Zip_5)))
            {
                decideConditionsMet258++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input1_Addrss_Lne_3), new ExamineSearch(pnd_Work_Input1_Zip_5), new ExamineGivingNumber(pnd_I));               //Natural: EXAMINE ADDRSS-LNE-3 FOR ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input1_Addrss_Lne_3.setValue(DbsUtil.compress(pnd_Work_Input1_Addrss_Lne_3, pnd_Work_Input1_Addrss_Lne_4));                                  //Natural: COMPRESS ADDRSS-LNE-3 ADDRSS-LNE-4 INTO ADDRSS-LNE-3
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input1_Addrss_Lne_4.reset();                                                                                                                     //Natural: RESET ADDRSS-LNE-4 ADDRSS-LNE-5
                pnd_Work_Input1_Addrss_Lne_5.reset();
            }                                                                                                                                                             //Natural: WHEN ADDRSS-LNE-3 = ZIP-5
            else if (condition(pnd_Work_Input1_Addrss_Lne_3.equals(pnd_Work_Input1_Zip_5)))
            {
                decideConditionsMet258++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input1_Addrss_Lne_2), new ExamineSearch(pnd_Work_Input1_Zip_5), new ExamineGivingNumber(pnd_I));               //Natural: EXAMINE ADDRSS-LNE-2 FOR ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input1_Addrss_Lne_2.setValue(DbsUtil.compress(pnd_Work_Input1_Addrss_Lne_2, pnd_Work_Input1_Addrss_Lne_3));                                  //Natural: COMPRESS ADDRSS-LNE-2 ADDRSS-LNE-3 INTO ADDRSS-LNE-2
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input1_Addrss_Lne_3.reset();                                                                                                                     //Natural: RESET ADDRSS-LNE-3 ADDRSS-LNE-4 ADDRSS-LNE-5
                pnd_Work_Input1_Addrss_Lne_4.reset();
                pnd_Work_Input1_Addrss_Lne_5.reset();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Zip_Chck_Format() throws Exception                                                                                                             //Natural: CHECK-ZIP-CHCK-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        if (condition(DbsUtil.maskMatches(pnd_Work_Input3_Pnd_Zip_5,"99999")))                                                                                            //Natural: IF #ZIP-5 = MASK ( 99999 )
        {
            short decideConditionsMet294 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ADDRSS-LNE-5 = #ZIP-5
            if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_5.equals(pnd_Work_Input3_Pnd_Zip_5)))
            {
                decideConditionsMet294++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input3_Pnd_Addrss_Lne_4), new ExamineSearch(pnd_Work_Input3_Pnd_Zip_5), new ExamineGivingNumber(pnd_I));       //Natural: EXAMINE #ADDRSS-LNE-4 FOR #ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input3_Pnd_Addrss_Lne_4.setValue(DbsUtil.compress(pnd_Work_Input3_Pnd_Addrss_Lne_4, pnd_Work_Input3_Pnd_Addrss_Lne_5));                      //Natural: COMPRESS #ADDRSS-LNE-4 #ADDRSS-LNE-5 INTO #ADDRSS-LNE-4
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input3_Pnd_Addrss_Lne_5.reset();                                                                                                                 //Natural: RESET #ADDRSS-LNE-5
            }                                                                                                                                                             //Natural: WHEN #ADDRSS-LNE-4 = #ZIP-5
            else if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_4.equals(pnd_Work_Input3_Pnd_Zip_5)))
            {
                decideConditionsMet294++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input3_Pnd_Addrss_Lne_3), new ExamineSearch(pnd_Work_Input3_Pnd_Zip_5), new ExamineGivingNumber(pnd_I));       //Natural: EXAMINE #ADDRSS-LNE-3 FOR #ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input3_Pnd_Addrss_Lne_3.setValue(DbsUtil.compress(pnd_Work_Input3_Pnd_Addrss_Lne_3, pnd_Work_Input3_Pnd_Addrss_Lne_4));                      //Natural: COMPRESS #ADDRSS-LNE-3 #ADDRSS-LNE-4 INTO #ADDRSS-LNE-3
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input3_Pnd_Addrss_Lne_4.reset();                                                                                                                 //Natural: RESET #ADDRSS-LNE-4 #ADDRSS-LNE-5
                pnd_Work_Input3_Pnd_Addrss_Lne_5.reset();
            }                                                                                                                                                             //Natural: WHEN #ADDRSS-LNE-3 = #ZIP-5
            else if (condition(pnd_Work_Input3_Pnd_Addrss_Lne_3.equals(pnd_Work_Input3_Pnd_Zip_5)))
            {
                decideConditionsMet294++;
                DbsUtil.examine(new ExamineSource(pnd_Work_Input3_Pnd_Addrss_Lne_2), new ExamineSearch(pnd_Work_Input3_Pnd_Zip_5), new ExamineGivingNumber(pnd_I));       //Natural: EXAMINE #ADDRSS-LNE-2 FOR #ZIP-5 GIVING NUMBER #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Input3_Pnd_Addrss_Lne_2.setValue(DbsUtil.compress(pnd_Work_Input3_Pnd_Addrss_Lne_2, pnd_Work_Input3_Pnd_Addrss_Lne_3));                      //Natural: COMPRESS #ADDRSS-LNE-2 #ADDRSS-LNE-3 INTO #ADDRSS-LNE-2
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Input3_Pnd_Addrss_Lne_3.reset();                                                                                                                 //Natural: RESET #ADDRSS-LNE-3 #ADDRSS-LNE-4 #ADDRSS-LNE-5
                pnd_Work_Input3_Pnd_Addrss_Lne_4.reset();
                pnd_Work_Input3_Pnd_Addrss_Lne_5.reset();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
