/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:21 PM
**        * FROM NATURAL PROGRAM : Adsp723
************************************************************
**        * FILE NAME            : Adsp723.java
**        * CLASS NAME           : Adsp723
**        * INSTANCE NAME        : Adsp723
************************************************************
************************************************************************
* PROGRAM    : ADSP723
* SYSTEM     : ANNUITIZATION SUNGARD
* TITLE      : REPORT FOR ACTUARIAL
* WRITTEN    : FEB, 2006
* WRITTEN BY : MARINA NACHBER
* FUNCTION :
* HISTORY
* 05/17/2006 O SOTTO ADDED ACCESS OF IA CONTROL RECORD TO GET THE
*                    DATE PERIOD FOR SELECTION AND WRITE TO A
*                    SEQUENTIAL FILE.
* 02/22/2008 O SOTTO TIAA RATE EXPANSION TO 99 OCCURS. SC 022208.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A.
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 03/05/12  E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                     UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030512.
* 01/18/13  E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                     AREAS.
* 10/15/13  E. MELNIK RECOMPILED FOR NEW ADSL401A.  CREF/REA REDESIGN
*                     CHANGES.
* 02/27/2017 R.CARREON RESTOWED FOR PIN EXPANSION 02272017
*********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp723 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Todays_Dte;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Contr_Rec_Read;
    private DbsField pnd_Counters_Pnd_Part_Rec_Read;
    private DbsField pnd_Frm_Dte;

    private DbsGroup pnd_Frm_Dte__R_Field_1;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Yyyy;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Mm;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Dd;
    private DbsField pnd_Thru_Dte;

    private DbsGroup pnd_Thru_Dte__R_Field_2;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Yyyy;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Mm;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Dd;
    private DbsField pnd_Date;
    private DbsField pnd_Datea;
    private DbsField pnd_First;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_3;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;
    private DbsField pnd_A;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Ext;
    private DbsField pnd_Max_Rates;

    private DbsGroup pnd_Extract;
    private DbsField pnd_Extract_Pnd_E_Date;
    private DbsField pnd_Extract_Pnd_E_Orig_Cntrct_Cnt;
    private DbsField pnd_Extract_Pnd_E_Orig_Cntrct;
    private DbsField pnd_Extract_Pnd_E_Cntrct;
    private DbsField pnd_Extract_Pnd_E_Cert;
    private DbsField pnd_Extract_Pnd_Fund;

    private DbsGroup pnd_Extract_Pnd_Rte_Info;
    private DbsField pnd_Extract_Pnd_Rte_Cd;
    private DbsField pnd_Extract_Pnd_Rte_Actl_Amt;
    private DbsField pnd_Extract_Pnd_Rte_Grd_Mnth_Amt;
    private DbsField pnd_Extract_Pnd_Rte_Std_Annl_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Contr_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Contr_Rec_Read", "#CONTR-REC-READ", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Part_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Part_Rec_Read", "#PART-REC-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Frm_Dte = localVariables.newFieldInRecord("pnd_Frm_Dte", "#FRM-DTE", FieldType.STRING, 8);

        pnd_Frm_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Frm_Dte__R_Field_1", "REDEFINE", pnd_Frm_Dte);
        pnd_Frm_Dte_Pnd_Frm_Yyyy = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Yyyy", "#FRM-YYYY", FieldType.NUMERIC, 4);
        pnd_Frm_Dte_Pnd_Frm_Mm = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Mm", "#FRM-MM", FieldType.NUMERIC, 2);
        pnd_Frm_Dte_Pnd_Frm_Dd = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Dd", "#FRM-DD", FieldType.NUMERIC, 2);
        pnd_Thru_Dte = localVariables.newFieldInRecord("pnd_Thru_Dte", "#THRU-DTE", FieldType.STRING, 8);

        pnd_Thru_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Thru_Dte__R_Field_2", "REDEFINE", pnd_Thru_Dte);
        pnd_Thru_Dte_Pnd_Thru_Yyyy = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Yyyy", "#THRU-YYYY", FieldType.NUMERIC, 4);
        pnd_Thru_Dte_Pnd_Thru_Mm = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Mm", "#THRU-MM", FieldType.NUMERIC, 2);
        pnd_Thru_Dte_Pnd_Thru_Dd = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Dd", "#THRU-DD", FieldType.NUMERIC, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Datea = localVariables.newFieldInRecord("pnd_Datea", "#DATEA", FieldType.STRING, 8);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_3", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_3.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 5);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 5);
        pnd_Ext = localVariables.newFieldInRecord("pnd_Ext", "#EXT", FieldType.PACKED_DECIMAL, 7);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 3);

        pnd_Extract = localVariables.newGroupInRecord("pnd_Extract", "#EXTRACT");
        pnd_Extract_Pnd_E_Date = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Date", "#E-DATE", FieldType.STRING, 8);
        pnd_Extract_Pnd_E_Orig_Cntrct_Cnt = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Orig_Cntrct_Cnt", "#E-ORIG-CNTRCT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Extract_Pnd_E_Orig_Cntrct = pnd_Extract.newFieldArrayInGroup("pnd_Extract_Pnd_E_Orig_Cntrct", "#E-ORIG-CNTRCT", FieldType.STRING, 10, new 
            DbsArrayController(1, 12));
        pnd_Extract_Pnd_E_Cntrct = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Cntrct", "#E-CNTRCT", FieldType.STRING, 10);
        pnd_Extract_Pnd_E_Cert = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Cert", "#E-CERT", FieldType.STRING, 10);
        pnd_Extract_Pnd_Fund = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_Fund", "#FUND", FieldType.STRING, 1);

        pnd_Extract_Pnd_Rte_Info = pnd_Extract.newGroupArrayInGroup("pnd_Extract_Pnd_Rte_Info", "#RTE-INFO", new DbsArrayController(1, 250));
        pnd_Extract_Pnd_Rte_Cd = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Cd", "#RTE-CD", FieldType.STRING, 2);
        pnd_Extract_Pnd_Rte_Actl_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Actl_Amt", "#RTE-ACTL-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Extract_Pnd_Rte_Grd_Mnth_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Grd_Mnth_Amt", "#RTE-GRD-MNTH-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Extract_Pnd_Rte_Std_Annl_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Std_Annl_Amt", "#RTE-STD-ANNL-AMT", FieldType.NUMERIC, 
            11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp723() throws Exception
    {
        super("Adsp723");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 133 PS = 55
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //*  INPUT +DEBUG-ON
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            if (condition(cntrl_Cntrl_Cde.notEquals("DC")))                                                                                                               //Natural: IF CNTRL-CDE NE 'DC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Thru_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #THRU-DTE
                if (condition(pnd_Thru_Dte_Pnd_Thru_Dd.lessOrEqual(20)))                                                                                                  //Natural: IF #THRU-DD LE 20
                {
                    DbsUtil.terminate(0);  if (true) return;                                                                                                              //Natural: TERMINATE 00
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Frm_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #FRM-DTE
            if (condition(pnd_Frm_Dte_Pnd_Frm_Mm.notEquals(pnd_Thru_Dte_Pnd_Thru_Mm)))                                                                                    //Natural: IF #FRM-MM NE #THRU-MM
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "FROM DATE:",pnd_Frm_Dte," TO DATE:",pnd_Thru_Dte);                                                                                         //Natural: WRITE 'FROM DATE:' #FRM-DTE ' TO DATE:' #THRU-DTE
        if (Global.isEscape()) return;
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Thru_Dte);                                                                                             //Natural: MOVE EDITED #THRU-DTE TO #DATE ( EM = YYYYMMDD )
        pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Frm_Dte);                                                                                //Natural: MOVE EDITED #FRM-DTE TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
        //*  IF +DEBUG-ON
        //*  WRITE 'Reading ADS-PRTCPNT....by key ' #KEY-CNTL-BSNSS-DTE
        //*  END-IF
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
        (
        "READ02",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        READ02:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(pnd_Date)))                                                                         //Natural: IF ADP-LST-ACTVTY-DTE GT #DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Part_Rec_Read.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PART-REC-READ
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T"))))                                                                //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T'
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CONTRACT-FILE
            sub_Read_Contract_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"  ");                                                                                                                 //Natural: WRITE ( 01 ) '  '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"ADS-PRTCPNT RECORDS READ     ",pnd_Counters_Pnd_Part_Rec_Read);                                                       //Natural: WRITE ( 01 ) 'ADS-PRTCPNT RECORDS READ     ' #PART-REC-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"  ");                                                                                                                 //Natural: WRITE ( 01 ) '  '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"ADS-CNTRCT  RECORDS READ     ",pnd_Counters_Pnd_Contr_Rec_Read);                                                      //Natural: WRITE ( 01 ) 'ADS-CNTRCT  RECORDS READ     ' #CONTR-REC-READ
        if (Global.isEscape()) return;
        //* *
        getReports().write(0, "Total ADS records written    ",pnd_Ext);                                                                                                   //Natural: WRITE 'Total ADS records written    ' #EXT
        if (Global.isEscape()) return;
        //* *****************************************************************
        //*      S U B R O U T I N E S
        //* *****************************************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTRACT-FILE
        //*    'Req ID'    ADS-PRTCPNT-VIEW.RQST-ID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-REA-INFO
        //* ***********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-INFO
        //* **********************************************************************
        //* *R #Y 1 80
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT
        //* ***********************************************************************
    }
    private void sub_Read_Contract_File() throws Exception                                                                                                                //Natural: READ-CONTRACT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Extract.reset();                                                                                                                                              //Natural: RESET #EXTRACT
        //*  IF +DEBUG-ON
        //*  WRITE 'Reading ADS-CNTRCT  ....by 'ADS-PRTCPNT-VIEW.RQST-ID
        //*  END-IF
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseRead                                                                                                             //Natural: READ ADS-CNTRCT-VIEW BY ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "READ03",
        new Wc[] { new Wc("RQST_ID", ">=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.BY) },
        new Oc[] { new Oc("RQST_ID", "ASC") }
        );
        READ03:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("READ03")))
        {
            pnd_Counters_Pnd_Contr_Rec_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CONTR-REC-READ
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Rqst_Id().notEquals(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id())))                                                  //Natural: IF ADS-CNTRCT-VIEW.RQST-ID NE ADS-PRTCPNT-VIEW.RQST-ID
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "Last Activity/    Date",                                                                                                             //Natural: DISPLAY ( 01 ) 'Last Activity/    Date' ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) 'IA TIAA/NUMBER ' ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR 'IA CREF/ Number ' ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR /
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(), new ReportEditMask ("YYYYMMDD"),"IA TIAA/NUMBER ",
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr(),"IA CREF/ Number ",
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr(),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #X 1 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_X).equals(" ")))                                                                 //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( #X ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,"    ACCT CDE",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_X),"    Acct Actual Amt",            //Natural: WRITE ( 01 ) '    ACCT CDE' ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( #X ) '    Acct Actual Amt' ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #X ) '    Grd/Mmthly Amt' ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-ACTL-AMT ( #X ) '    Std/Annl Amt' ADS-CNTRCT-VIEW.ADC-STNDRD-ANNL-ACTL-AMT ( #X ) //
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_X),"    Grd/Mmthly Amt",ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_X),
                    "    Std/Annl Amt",ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_X),NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Extract_Pnd_Fund.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_X));                                                             //Natural: ASSIGN #FUND := ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( #X )
                pnd_Extract_Pnd_E_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #E-DATE
                pnd_Extract_Pnd_E_Cntrct.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                                      //Natural: ASSIGN #E-CNTRCT := ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR
                pnd_Extract_Pnd_E_Cert.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                                        //Natural: ASSIGN #E-CERT := ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_X).equals("T")))                                                                 //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( #X ) = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                    sub_Write_Tiaa_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-REA-INFO
                    sub_Write_Cref_Rea_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Datea.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DATEA
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-CONTRACT-FILE
    }
    private void sub_Write_Cref_Rea_Info() throws Exception                                                                                                               //Natural: WRITE-CREF-REA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Extract_Pnd_Rte_Cd.getValue(1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cref_Rate_Cde().getValue(pnd_X));                                             //Natural: ASSIGN #RTE-CD ( 1 ) := ADC-ACCT-CREF-RATE-CDE ( #X )
        pnd_Extract_Pnd_Rte_Actl_Amt.getValue(1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_X));                                            //Natural: ASSIGN #RTE-ACTL-AMT ( 1 ) := ADC-ACCT-ACTL-AMT ( #X )
        pnd_Extract_Pnd_Rte_Grd_Mnth_Amt.getValue(1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_X));                                  //Natural: ASSIGN #RTE-GRD-MNTH-AMT ( 1 ) := ADC-GRD-MNTHLY-ACTL-AMT ( #X )
        pnd_Extract_Pnd_Rte_Std_Annl_Amt.getValue(1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_X));                                 //Natural: ASSIGN #RTE-STD-ANNL-AMT ( 1 ) := ADC-STNDRD-ANNL-ACTL-AMT ( #X )
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT
        sub_Write_Extract();
        if (condition(Global.isEscape())) {return;}
    }
    //*  022208
    private void sub_Write_Tiaa_Info() throws Exception                                                                                                                   //Natural: WRITE-TIAA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #Y 1 #MAX-RATES
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_Max_Rates)); pnd_Y.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Y).equals(" ")))                                                            //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Extract_Pnd_Rte_Cd.getValue(pnd_Y).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Y));                                      //Natural: ASSIGN #RTE-CD ( #Y ) := ADC-DTL-TIAA-RATE-CDE ( #Y )
            pnd_Extract_Pnd_Rte_Actl_Amt.getValue(pnd_Y).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_Y));                                //Natural: ASSIGN #RTE-ACTL-AMT ( #Y ) := ADC-DTL-TIAA-ACTL-AMT ( #Y )
            pnd_Extract_Pnd_Rte_Grd_Mnth_Amt.getValue(pnd_Y).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Y));                        //Natural: ASSIGN #RTE-GRD-MNTH-AMT ( #Y ) := ADC-DTL-TIAA-GRD-ACTL-AMT ( #Y )
            pnd_Extract_Pnd_Rte_Std_Annl_Amt.getValue(pnd_Y).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Y));                     //Natural: ASSIGN #RTE-STD-ANNL-AMT ( #Y ) := ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #Y )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT
        sub_Write_Extract();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Extract() throws Exception                                                                                                                     //Natural: WRITE-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #A 1 12
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue(pnd_A).equals(" ")))                                                              //Natural: IF ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Extract_Pnd_E_Orig_Cntrct_Cnt.setValue(pnd_A);                                                                                                            //Natural: ASSIGN #E-ORIG-CNTRCT-CNT := #A
            pnd_Extract_Pnd_E_Orig_Cntrct.getValue(pnd_A).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue(pnd_A));                                 //Natural: ASSIGN #E-ORIG-CNTRCT ( #A ) := ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getWorkFiles().write(1, false, pnd_Extract);                                                                                                                      //Natural: WRITE WORK FILE 1 #EXTRACT
        pnd_Extract_Pnd_Rte_Info.getValue("*").reset();                                                                                                                   //Natural: RESET #RTE-INFO ( * )
        pnd_Ext.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #EXT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(52),"TIAA/CREF",NEWLINE,new ColumnSpacing(1),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 52X 'TIAA/CREF' / 1X 'RUN DATE  : ' *DATU 21X 'ADAS Report for Actuarial' 15X 'PROGRAM ID: ' *PROGRAM / 1X 'RUN TIME  : ' *TIMX 61X 'PAGE : ' *PAGE-NUMBER ( 01 ) ///
                        ColumnSpacing(21),"ADAS Report for Actuarial",new ColumnSpacing(15),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(1),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(61),"PAGE : ",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55");

        getReports().setDisplayColumns(1, "Last Activity/    Date",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(), new ReportEditMask ("YYYYMMDD"),"IA TIAA/NUMBER ",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr(),"IA CREF/ Number ",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr(),NEWLINE);
    }
}
