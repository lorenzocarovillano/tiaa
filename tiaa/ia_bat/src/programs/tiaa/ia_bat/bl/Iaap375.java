/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:01 PM
**        * FROM NATURAL PROGRAM : Iaap375
************************************************************
**        * FILE NAME            : Iaap375.java
**        * CLASS NAME           : Iaap375
**        * INSTANCE NAME        : Iaap375
************************************************************
************************************************************************
* PROGRAM  : IAAP375
* SYSTEM   : IAD
* TITLE    : UPDATE IA CPR
* CREATED  : APRIL 1996
* FUNCTION : APPLY CHANGE RESIDENCY CODE (106) TRANSACTIONS
* TITLE    : UPDATE CPR VIEW & CREATE B4 & AFTER TRANS
*
*
*
* HISTORY
* 02/25/98 LEN B : TODAYS DATE IS RETRIEVED FROM THE IA CONTROL REC
*                : AND PLACED ON THE IAA-TRANS-RCRD FILE.
*
* 4/08           : ADDED ROTH FIELDS
* 5/16           : BYPASSED RESIDENCY CODE OF '000'. SCAN ON 5/16.
* 8/2017   RC    : RESTOWED FOR PIN EXPANSION
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap375 extends BLNatBase
{
    // Data Areas
    private PdaIatl400p pdaIatl400p;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_2;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key__Filler1;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Cref_Cntrct_Fund_Key;

    private DbsGroup pnd_Cref_Cntrct_Fund_Key__R_Field_3;
    private DbsField pnd_Cref_Cntrct_Fund_Key__Filler2;
    private DbsField pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Iaa_Ddctn_Key;

    private DbsGroup pnd_Iaa_Ddctn_Key__R_Field_4;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_Trans_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;

    private DbsGroup pnd_Tran_Rec_In;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee;

    private DbsGroup pnd_Tran_Rec_In__R_Field_5;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Filler8;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Filler12;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Batch_Nbr;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Trans_Date;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Xref;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Filler61;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Res_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Filler32;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte;
    private DbsField pnd_Check_D;
    private DbsField pnd_Check_A;

    private DbsGroup pnd_Check_A__R_Field_6;
    private DbsField pnd_Check_A_Pnd_Check_N;
    private DbsField pnd_Wrk_Timx;
    private DbsField pnd_Trans_Dte;

    private DbsGroup pnd_Prog_Misc_Area;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Ppcn_Nbr_Payee;
    private DbsField pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr_Payee;

    private DbsGroup pnd_Prog_Misc_Area__R_Field_7;
    private DbsField pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr;
    private DbsField pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee;

    private DbsGroup pnd_Prog_Misc_Area__R_Field_8;
    private DbsField pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee_Alpha;
    private DbsField pnd_Prog_Misc_Area_Pnd_Total_Trans;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_106_Trans;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_107_Trans;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_Accpt;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_Rejct;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_Inact;
    private DbsField pnd_Prog_Misc_Area_Pnd_Tot_Ntfnd;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Last_Updte_Dte;
    private DbsField pnd_Next_Updte_Dte;
    private DbsField pnd_Datd;
    private DbsField pnd_Date8;

    private DbsGroup pnd_Date8__R_Field_9;
    private DbsField pnd_Date8_Pnd_Next_Updt_Ccyymm;
    private DbsField pnd_Date8_Pnd_Next_Updt_Dd;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_10;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_W_Eff_Date8;

    private DbsGroup pnd_W_Eff_Date8__R_Field_11;
    private DbsField pnd_W_Eff_Date8_Pnd_W_Eff_Date_Ccyymm;
    private DbsField pnd_W_Eff_Date8_Pnd_W_Eff_Date_Dd;

    private DbsGroup pnd_W_Eff_Date8__R_Field_12;
    private DbsField pnd_W_Eff_Date8_Pnd_W_Eff_Datea8;

    private DbsGroup pnd_Gen;
    private DbsField pnd_Gen_Pnd_Msg;
    private DbsField pnd_Todays_Date_A;

    private DbsGroup pnd_Todays_Date_A__R_Field_13;
    private DbsField pnd_Todays_Date_A_Pnd_Todays_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_2", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key__Filler1 = pnd_Tiaa_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key__Filler1", "_FILLER1", FieldType.STRING, 
            12);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", 
            "#TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Cref_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cref_Cntrct_Fund_Key", "#CREF-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cref_Cntrct_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cref_Cntrct_Fund_Key__R_Field_3", "REDEFINE", pnd_Cref_Cntrct_Fund_Key);
        pnd_Cref_Cntrct_Fund_Key__Filler2 = pnd_Cref_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cref_Cntrct_Fund_Key__Filler2", "_FILLER2", FieldType.STRING, 
            12);
        pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde = pnd_Cref_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde", 
            "#CREF-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Iaa_Ddctn_Key = localVariables.newFieldInRecord("pnd_Iaa_Ddctn_Key", "#IAA-DDCTN-KEY", FieldType.STRING, 18);

        pnd_Iaa_Ddctn_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Iaa_Ddctn_Key__R_Field_4", "REDEFINE", pnd_Iaa_Ddctn_Key);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Iaa_Ddctn_Key__R_Field_4.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde = pnd_Iaa_Ddctn_Key__R_Field_4.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr = pnd_Iaa_Ddctn_Key__R_Field_4.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde = pnd_Iaa_Ddctn_Key__R_Field_4.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cpr_Trans_Roth_Dsblty_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        registerRecord(vw_iaa_Cpr_Trans);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Tran_Rec_In = localVariables.newGroupInRecord("pnd_Tran_Rec_In", "#TRAN-REC-IN");
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee", "#TR-PPCN-NBR-PAYEE", FieldType.STRING, 
            10);

        pnd_Tran_Rec_In__R_Field_5 = pnd_Tran_Rec_In.newGroupInGroup("pnd_Tran_Rec_In__R_Field_5", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr = pnd_Tran_Rec_In__R_Field_5.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr", "#TR-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee = pnd_Tran_Rec_In__R_Field_5.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee", "#TR-PPCN-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde", "#TR-TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Tran_Rec_In_Pnd_Tr_Filler8 = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Filler8", "#TR-FILLER8", FieldType.STRING, 8);
        pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due", "#TR-CHK-DTE-DUE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Tran_Rec_In_Pnd_Tr_Filler12 = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Filler12", "#TR-FILLER12", FieldType.STRING, 12);
        pnd_Tran_Rec_In_Pnd_Tr_Batch_Nbr = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Batch_Nbr", "#TR-BATCH-NBR", FieldType.NUMERIC, 4);
        pnd_Tran_Rec_In_Pnd_Tr_Trans_Date = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Trans_Date", "#TR-TRANS-DATE", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Tran_Rec_In_Pnd_Tr_Xref = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Xref", "#TR-XREF", FieldType.STRING, 9);
        pnd_Tran_Rec_In_Pnd_Tr_Filler61 = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Filler61", "#TR-FILLER61", FieldType.STRING, 61);
        pnd_Tran_Rec_In_Pnd_Tr_Res_Cde = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Res_Cde", "#TR-RES-CDE", FieldType.STRING, 3);
        pnd_Tran_Rec_In_Pnd_Tr_Filler32 = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Filler32", "#TR-FILLER32", FieldType.STRING, 32);
        pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte", "#TR-EFF-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Check_D = localVariables.newFieldInRecord("pnd_Check_D", "#CHECK-D", FieldType.DATE);
        pnd_Check_A = localVariables.newFieldInRecord("pnd_Check_A", "#CHECK-A", FieldType.STRING, 6);

        pnd_Check_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Check_A__R_Field_6", "REDEFINE", pnd_Check_A);
        pnd_Check_A_Pnd_Check_N = pnd_Check_A__R_Field_6.newFieldInGroup("pnd_Check_A_Pnd_Check_N", "#CHECK-N", FieldType.NUMERIC, 6);
        pnd_Wrk_Timx = localVariables.newFieldInRecord("pnd_Wrk_Timx", "#WRK-TIMX", FieldType.TIME);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        pnd_Prog_Misc_Area = localVariables.newGroupInRecord("pnd_Prog_Misc_Area", "#PROG-MISC-AREA");
        pnd_Prog_Misc_Area_Pnd_Sve_Ppcn_Nbr_Payee = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Ppcn_Nbr_Payee", "#SVE-PPCN-NBR-PAYEE", 
            FieldType.STRING, 10);
        pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr_Payee = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr_Payee", "#WRK-PPCN-NBR-PAYEE", 
            FieldType.STRING, 12);

        pnd_Prog_Misc_Area__R_Field_7 = pnd_Prog_Misc_Area.newGroupInGroup("pnd_Prog_Misc_Area__R_Field_7", "REDEFINE", pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr_Payee);
        pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr = pnd_Prog_Misc_Area__R_Field_7.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Nbr", "#WRK-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee = pnd_Prog_Misc_Area__R_Field_7.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee", "#WRK-PPCN-PAYEE", 
            FieldType.NUMERIC, 2);

        pnd_Prog_Misc_Area__R_Field_8 = pnd_Prog_Misc_Area__R_Field_7.newGroupInGroup("pnd_Prog_Misc_Area__R_Field_8", "REDEFINE", pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee);
        pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee_Alpha = pnd_Prog_Misc_Area__R_Field_8.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Wrk_Ppcn_Payee_Alpha", "#WRK-PPCN-PAYEE-ALPHA", 
            FieldType.STRING, 2);
        pnd_Prog_Misc_Area_Pnd_Total_Trans = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Total_Trans", "#TOTAL-TRANS", FieldType.NUMERIC, 
            5);
        pnd_Prog_Misc_Area_Pnd_Tot_106_Trans = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_106_Trans", "#TOT-106-TRANS", FieldType.NUMERIC, 
            5);
        pnd_Prog_Misc_Area_Pnd_Tot_107_Trans = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_107_Trans", "#TOT-107-TRANS", FieldType.NUMERIC, 
            5);
        pnd_Prog_Misc_Area_Pnd_Tot_Accpt = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_Accpt", "#TOT-ACCPT", FieldType.NUMERIC, 5);
        pnd_Prog_Misc_Area_Pnd_Tot_Rejct = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_Rejct", "#TOT-REJCT", FieldType.NUMERIC, 5);
        pnd_Prog_Misc_Area_Pnd_Tot_Inact = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_Inact", "#TOT-INACT", FieldType.NUMERIC, 5);
        pnd_Prog_Misc_Area_Pnd_Tot_Ntfnd = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Tot_Ntfnd", "#TOT-NTFND", FieldType.NUMERIC, 5);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 3);
        pnd_Last_Updte_Dte = localVariables.newFieldInRecord("pnd_Last_Updte_Dte", "#LAST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Next_Updte_Dte = localVariables.newFieldInRecord("pnd_Next_Updte_Dte", "#NEXT-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);

        pnd_Date8__R_Field_9 = localVariables.newGroupInRecord("pnd_Date8__R_Field_9", "REDEFINE", pnd_Date8);
        pnd_Date8_Pnd_Next_Updt_Ccyymm = pnd_Date8__R_Field_9.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Ccyymm", "#NEXT-UPDT-CCYYMM", FieldType.NUMERIC, 
            6);
        pnd_Date8_Pnd_Next_Updt_Dd = pnd_Date8__R_Field_9.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Dd", "#NEXT-UPDT-DD", FieldType.STRING, 2);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_10 = localVariables.newGroupInRecord("pnd_Year__R_Field_10", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_10.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_W_Eff_Date8 = localVariables.newFieldInRecord("pnd_W_Eff_Date8", "#W-EFF-DATE8", FieldType.NUMERIC, 8);

        pnd_W_Eff_Date8__R_Field_11 = localVariables.newGroupInRecord("pnd_W_Eff_Date8__R_Field_11", "REDEFINE", pnd_W_Eff_Date8);
        pnd_W_Eff_Date8_Pnd_W_Eff_Date_Ccyymm = pnd_W_Eff_Date8__R_Field_11.newFieldInGroup("pnd_W_Eff_Date8_Pnd_W_Eff_Date_Ccyymm", "#W-EFF-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        pnd_W_Eff_Date8_Pnd_W_Eff_Date_Dd = pnd_W_Eff_Date8__R_Field_11.newFieldInGroup("pnd_W_Eff_Date8_Pnd_W_Eff_Date_Dd", "#W-EFF-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Eff_Date8__R_Field_12 = localVariables.newGroupInRecord("pnd_W_Eff_Date8__R_Field_12", "REDEFINE", pnd_W_Eff_Date8);
        pnd_W_Eff_Date8_Pnd_W_Eff_Datea8 = pnd_W_Eff_Date8__R_Field_12.newFieldInGroup("pnd_W_Eff_Date8_Pnd_W_Eff_Datea8", "#W-EFF-DATEA8", FieldType.STRING, 
            8);

        pnd_Gen = localVariables.newGroupInRecord("pnd_Gen", "#GEN");
        pnd_Gen_Pnd_Msg = pnd_Gen.newFieldInGroup("pnd_Gen_Pnd_Msg", "#MSG", FieldType.STRING, 60);
        pnd_Todays_Date_A = localVariables.newFieldInRecord("pnd_Todays_Date_A", "#TODAYS-DATE-A", FieldType.STRING, 8);

        pnd_Todays_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Todays_Date_A__R_Field_13", "REDEFINE", pnd_Todays_Date_A);
        pnd_Todays_Date_A_Pnd_Todays_Date_N = pnd_Todays_Date_A__R_Field_13.newFieldInGroup("pnd_Todays_Date_A_Pnd_Todays_Date_N", "#TODAYS-DATE-N", FieldType.NUMERIC, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap375() throws Exception
    {
        super("Iaap375");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Updte_Dte, pnd_Next_Updte_Dte);                                                               //Natural: CALLNAT 'IAAN0020' #LAST-UPDTE-DTE #NEXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"MM'/'DD'/'YYYY")))                                                                                          //Natural: IF #LAST-UPDTE-DTE = MASK ( MM'/'DD'/'YYYY )
        {
            pnd_Datd.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Next_Updte_Dte);                                                                                 //Natural: MOVE EDITED #NEXT-UPDTE-DTE TO #DATD ( EM = MM/DD/YYYY )
            pnd_Date8.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE8
            pnd_Year.setValueEdited(pnd_Datd,new ReportEditMask("YYYY"));                                                                                                 //Natural: MOVE EDITED #DATD ( EM = YYYY ) TO #YEAR
            if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"'01'")))                                                                                                //Natural: IF #LAST-UPDTE-DTE = MASK ( '01' )
            {
                pnd_Year_Pnd_Year_N.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #YEAR-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR   ERROR   ERROR  ERROR",NEWLINE,"NO CONTROL RECORD ON FILE",NEWLINE,"PROCESSING WILL NOT CONTINUE");                             //Natural: WRITE 'ERROR   ERROR   ERROR  ERROR' / 'NO CONTROL RECORD ON FILE' / 'PROCESSING WILL NOT CONTINUE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET TODAYS DATE FROM THE IA CONTROL FILE.
        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'DC'
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        pnd_Todays_Date_A.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DATE-A
        //* *=================================================================
        //*                      MAIN PROCESSING LOOP
        //* *=================================================================
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #TRAN-REC-IN
        while (condition(getWorkFiles().read(1, pnd_Tran_Rec_In)))
        {
            pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-TRANS
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde.equals(106)))                                                                                                  //Natural: IF #TR-TRANS-CDE = 106
            {
                pnd_Prog_Misc_Area_Pnd_Tot_106_Trans.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-106-TRANS
                //*  5/16 - START
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Res_Cde.equals("000")))                                                                                              //Natural: IF #TR-RES-CDE = '000'
                {
                    pnd_Prog_Misc_Area_Pnd_Tot_Rejct.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-REJCT
                    pnd_Gen_Pnd_Msg.setValue(" CONTRACT PAYEE RESIDENCY CODE IS 000 - REJECTED");                                                                         //Natural: ASSIGN #GEN.#MSG := ' CONTRACT PAYEE RESIDENCY CODE IS 000 - REJECTED'
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
                    sub_Print_Detail_Line1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  5/16 - END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prog_Misc_Area_Pnd_Tot_107_Trans.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-107-TRANS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Wrk_Timx.setValue(Global.getTIMX());                                                                                                                      //Natural: ASSIGN #WRK-TIMX := *TIMX
            //*     TO PREVENT DUPLICATE DESCRIPTOR KEY ON STORE CPR-TRANS
            //*     WHEN HAVE 2 TRANS FOR SAME CONTRACT & PAYEE
            //*     VARIABLE *TIMX IS USED AS PART OF KEY FOR UNIQUENESS
            if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Ppcn_Nbr_Payee.equals(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee)))                                                       //Natural: IF #SVE-PPCN-NBR-PAYEE = #TR-PPCN-NBR-PAYEE
            {
                pnd_Wrk_Timx.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WRK-TIMX
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prog_Misc_Area_Pnd_Sve_Ppcn_Nbr_Payee.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Payee);                                                                    //Natural: ASSIGN #SVE-PPCN-NBR-PAYEE := #TR-PPCN-NBR-PAYEE
            //*  CREATE A TRANSACTION TYPE RECORD
            iaa_Trans_Rcrd_Lst_Trans_Dte.setValue(pnd_Wrk_Timx);                                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE := #WRK-TIMX
            iaa_Trans_Rcrd_Trans_Dte.setValue(pnd_Wrk_Timx);
            pnd_Trans_Dte.setValue(pnd_Wrk_Timx);
            //*  #TRANS-DTE := *TIMX
            //*  CREATE A CONTRACT-PARTICIPANT-ROLE HISTORY RECORD
            //*  BEFORE IMAGE
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR := #TR-PPCN-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee);                                                                        //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #TR-PPCN-PAYEE
            vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
            (
            "F1",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("F1", true)))
            {
                vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrct_Prtcpnt_Role.getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORDS FOUND
                {
                    pnd_Gen_Pnd_Msg.setValue(" CONTRACT PAYEE NOT FOUND ON FILE");                                                                                        //Natural: ASSIGN #GEN.#MSG := ' CONTRACT PAYEE NOT FOUND ON FILE'
                    pnd_Prog_Misc_Area_Pnd_Tot_Ntfnd.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-NTFND
                    pnd_Prog_Misc_Area_Pnd_Tot_Rejct.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-REJCT
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
                    sub_Print_Detail_Line1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                       //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
                {
                    pnd_Gen_Pnd_Msg.setValue(" CONTRACT PAYEE INACTIVE         ");                                                                                        //Natural: ASSIGN #GEN.#MSG := ' CONTRACT PAYEE INACTIVE         '
                    pnd_Prog_Misc_Area_Pnd_Tot_Inact.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-INACT
                    pnd_Prog_Misc_Area_Pnd_Tot_Rejct.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-REJCT
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
                    sub_Print_Detail_Line1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(iaa_Trans_Rcrd_Lst_Trans_Dte));             //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - IAA-TRANS-RCRD.LST-TRANS-DTE
                iaa_Trans_Rcrd_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
                iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #TR-PPCN-NBR
                iaa_Trans_Rcrd_Trans_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee);                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #TR-PPCN-PAYEE
                iaa_Trans_Rcrd_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Trans_Check_Dte), pnd_Date8.val());                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := VAL ( #DATE8 )
                iaa_Trans_Rcrd_Trans_User_Area.setValue("BATCH");                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
                iaa_Trans_Rcrd_Trans_User_Id.setValue("IAAP375");                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP375'
                iaa_Trans_Rcrd_Trans_Actvty_Cde.setValue("A");                                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
                iaa_Trans_Rcrd_Trans_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde);                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := #TR-TRANS-CDE
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due.less(pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte)))                                                                   //Natural: IF #TR-CHK-DTE-DUE LT #TR-EFF-DTE
                {
                    pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte.setValue(pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due);                                                                          //Natural: ASSIGN #TR-EFF-DTE := #TR-CHK-DTE-DUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Eff_Date8_Pnd_W_Eff_Date_Ccyymm.setValue(pnd_Tran_Rec_In_Pnd_Tr_Eff_Dte);                                                                           //Natural: ASSIGN #W-EFF-DATE-CCYYMM := #TR-EFF-DTE
                pnd_W_Eff_Date8_Pnd_W_Eff_Date_Dd.setValue(1);                                                                                                            //Natural: ASSIGN #W-EFF-DATE-DD := 01
                //*  LB 02/98
                iaa_Trans_Rcrd_Trans_Effective_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Eff_Date8_Pnd_W_Eff_Datea8);                                       //Natural: MOVE EDITED #W-EFF-DATEA8 TO IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD )
                iaa_Trans_Rcrd_Trans_Todays_Dte.setValue(pnd_Todays_Date_A_Pnd_Todays_Date_N);                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DATE-N
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
                vw_iaa_Cpr_Trans.setValuesByName(vw_iaa_Cntrct_Prtcpnt_Role);                                                                                             //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
                iaa_Cpr_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                  //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
                iaa_Cpr_Trans_Bfre_Imge_Id.setValue("1");                                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := '1'
                iaa_Cpr_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := ' '
                iaa_Cpr_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cpr_Trans_Trans_Check_Dte), pnd_Date8.val());                                      //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := VAL ( #DATE8 )
                iaa_Cpr_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                          //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := #TRANS-DTE
                vw_iaa_Cpr_Trans.insertDBRow();                                                                                                                           //Natural: STORE IAA-CPR-TRANS
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde.equals(106)))                                                                                              //Natural: IF #TR-TRANS-CDE = 106
                {
                    iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Res_Cde);                                                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE := #TR-RES-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Res_Cde);                                                                    //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-LOCAL-CDE := #TR-RES-CDE
                }                                                                                                                                                         //Natural: END-IF
                iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE := #TRANS-DTE
                //*     UPDATE CPR MASTER RECORD
                vw_iaa_Cntrct_Prtcpnt_Role.updateDBRow("F1");                                                                                                             //Natural: UPDATE ( F1. )
                //*  AFTER  IMAGE  CPR TRANS
                vw_iaa_Cpr_Trans.setValuesByName(vw_iaa_Cntrct_Prtcpnt_Role);                                                                                             //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
                iaa_Cpr_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                  //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
                iaa_Cpr_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cpr_Trans_Trans_Check_Dte), pnd_Date8.val());                                      //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := VAL ( #DATE8 )
                iaa_Cpr_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                          //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := #TRANS-DTE
                iaa_Cpr_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := ' '
                iaa_Cpr_Trans_Aftr_Imge_Id.setValue("2");                                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := '2'
                vw_iaa_Cpr_Trans.insertDBRow();                                                                                                                           //Natural: STORE IAA-CPR-TRANS
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde.equals(106)))                                                                                              //Natural: IF #TR-TRANS-CDE = 106
                {
                    pnd_Gen_Pnd_Msg.setValue(" CONTRACT PAYEE RESIDENCY CODE CHANGED  ");                                                                                 //Natural: ASSIGN #GEN.#MSG := ' CONTRACT PAYEE RESIDENCY CODE CHANGED  '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Gen_Pnd_Msg.setValue(" CONTRACT PAYEE LOCALITY CODE CHANGED   ");                                                                                 //Natural: ASSIGN #GEN.#MSG := ' CONTRACT PAYEE LOCALITY CODE CHANGED   '
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prog_Misc_Area_Pnd_Tot_Accpt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOT-ACCPT
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
                sub_Print_Detail_Line1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL ACCEPTED TRANSACTIONS ..............",pnd_Prog_Misc_Area_Pnd_Tot_Accpt, new ReportEditMask              //Natural: WRITE ( 1 ) / 'TOTAL ACCEPTED TRANSACTIONS ..............' #TOT-ACCPT ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL REJECTED TRANSACTIONS ..............",pnd_Prog_Misc_Area_Pnd_Tot_Rejct, new ReportEditMask              //Natural: WRITE ( 1 ) / 'TOTAL REJECTED TRANSACTIONS ..............' #TOT-REJCT ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL 106 RESIDENCY CODE CHANGE TRANS IN..",pnd_Prog_Misc_Area_Pnd_Tot_106_Trans, new ReportEditMask          //Natural: WRITE ( 1 ) / 'TOTAL 106 RESIDENCY CODE CHANGE TRANS IN..' #TOT-106-TRANS ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL 107 LOCALITY CODE CHANGE TRANS IN...",pnd_Prog_Misc_Area_Pnd_Tot_107_Trans, new ReportEditMask          //Natural: WRITE ( 1 ) / 'TOTAL 107 LOCALITY CODE CHANGE TRANS IN...' #TOT-107-TRANS ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TRANSACTIONS PROCESSED..............",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask            //Natural: WRITE ( 1 ) / 'TOTAL TRANSACTIONS PROCESSED..............' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL CONTRACT PAYEES NOT FOUND ON FILE...",pnd_Prog_Misc_Area_Pnd_Tot_Ntfnd, new ReportEditMask              //Natural: WRITE ( 1 ) / 'TOTAL CONTRACT PAYEES NOT FOUND ON FILE...' #TOT-NTFND ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL INACTIVE CONTRACT PAYEES............",pnd_Prog_Misc_Area_Pnd_Tot_Inact, new ReportEditMask              //Natural: WRITE ( 1 ) / 'TOTAL INACTIVE CONTRACT PAYEES............' #TOT-INACT ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  -------------------------------------------
        //*  PRINT CONTROL CONTRACT NUMBER FOR COMBINES
        //*  -------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINE1
        //*  --------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-PROCESSING
    }
    private void sub_Print_Detail_Line1() throws Exception                                                                                                                //Natural: PRINT-DETAIL-LINE1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Payee,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR ( EM = XXXXXXX-X ) 3X#TR-PPCN-PAYEE ( EM = 99 ) 3X #TR-RES-CDE 2X#TR-TRANS-CDE ( EM = 999 ) 2X #GEN.#MSG
            new ReportEditMask ("99"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Res_Cde,new ColumnSpacing(2),pnd_Tran_Rec_In_Pnd_Tr_Trans_Cde, new ReportEditMask 
            ("999"),new ColumnSpacing(2),pnd_Gen_Pnd_Msg);
        if (Global.isEscape()) return;
    }
    private void sub_Error_Processing() throws Exception                                                                                                                  //Natural: ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"**** ERROR PROCESSING AUTO COMBINE CHECKS TRANS****");                                                                //Natural: WRITE ( 1 ) '**** ERROR PROCESSING AUTO COMBINE CHECKS TRANS****'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"**** JOB ABORTED FIX ERROR AND RERUN JOB       ****");                                                                //Natural: WRITE ( 1 ) '**** JOB ABORTED FIX ERROR AND RERUN JOB       ****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** ERROR PROCESSING AUTO COMBINE CHECKS TRANS****");                                                                                     //Natural: WRITE '**** ERROR PROCESSING AUTO COMBINE CHECKS TRANS****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** JOB ABORTED FIX ERROR AND RERUN JOB       ****");                                                                                     //Natural: WRITE '**** JOB ABORTED FIX ERROR AND RERUN JOB       ****'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN RESIDENCY CODE CHANGE TRANS APPLIED FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Date8_Pnd_Next_Updt_Ccyymm,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X'IA ADMIN RESIDENCY CODE CHANGE TRANS APPLIED FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' #W-PAGE-CTR ( EM = ZZ9 )
                        new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE,NEWLINE);                                                                //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT //
                    getReports().write(1, ReportOption.NOTITLE," CONTRACT PAYEE  RSD TRAN                                      ");                                        //Natural: WRITE ( 1 ) ' CONTRACT PAYEE  RSD TRAN                                      '
                    getReports().write(1, ReportOption.NOTITLE,"  NUMBER   CODE  CDE CODE   MESSAGE                            ");                                        //Natural: WRITE ( 1 ) '  NUMBER   CODE  CDE CODE   MESSAGE                            '
                    getReports().write(1, ReportOption.NOTITLE,"--------- ------ --- -----  -----------------------------------");                                        //Natural: WRITE ( 1 ) '--------- ------ --- -----  -----------------------------------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
