/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:08 PM
**        * FROM NATURAL PROGRAM : Iaap600
************************************************************
**        * FILE NAME            : Iaap600.java
**        * CLASS NAME           : Iaap600
**        * INSTANCE NAME        : Iaap600
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP600    THIS PROGRAM WILL EXTRACT IA PROSPECTUS  *
*      DATE   -  07/11      DATA FROM THE IA MASTER FILE USING THE   *
*    AUTHOR   -  TED P.     LATEST CHECK DATE                        *
*    HISTORY  -
*    2/2012   - JUN T.      FIXED REVERSAL OF NEW ISSUE WHERE AN ACTUAL
*                           PURGE OF THE MASTER RECORDS HAS OCCURRED TO
*                           READ THE BEFORE IMAGES INSTEAD OF THE MASTER
*                           RECORDS.
*    2/2013   - JUN T.      SPLIT NON-PENSION (ORIGIN 37,38,40,43) FROM
*                           THE PENSION DATA. TWO FILES WILL NOW BE
*                           CREATED. SCAN ON 2/2013 FOR CHANGES
*
* 11/05/2013 J TINIO TIAA ACCESS CHANGE - CALL NECN4000 FOR CREF/REA
*                    REDESIGN PROJECT.
*
* 05/04/2015 J TINIO MAKE SECOND CALL TO NECN4000 TO GET THE REST OF
*                    CUSIP. SCAN ON 05/15 FOR CHANGES
* 04/2017    O SOTTO PIN EXPANSION CHANGE MARKED 082017.
**********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap600 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_cntrct_Tr;
    private DbsField cntrct_Tr_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Tr_Cntrct_Optn_Cde;
    private DbsField cntrct_Tr_Cntrct_Orgn_Cde;
    private DbsField cntrct_Tr_Plan_Nmbr;
    private DbsField cntrct_Tr_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Tr_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField cntrct_Tr_Trans_Dte;
    private DbsField cntrct_Tr_Bfre_Imge_Id;
    private DbsField cntrct_Tr_Aftr_Imge_Id;
    private DbsField cntrct_Tr_Invrse_Trans_Dte;
    private DbsField cntrct_Tr_Lst_Trans_Dte;
    private DbsField pnd_Cntrct_Bfre_Key;

    private DbsGroup pnd_Cntrct_Bfre_Key__R_Field_1;
    private DbsField pnd_Cntrct_Bfre_Key__Filler1;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_C_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_C_T_Dte;
    private DbsField pnd_Cntrct_Aftr_Key;

    private DbsGroup pnd_Cntrct_Aftr_Key__R_Field_2;
    private DbsField pnd_Cntrct_Aftr_Key__Filler2;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_C_A_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_C_A_Inv;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Pend_Cde;

    private DataAccessProgramView vw_cpr_Tr;
    private DbsField cpr_Tr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Tr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Tr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Tr_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Tr_Cntrct_Actvty_Cde;
    private DbsField cpr_Tr_Cntrct_Pend_Cde;
    private DbsField cpr_Tr_Trans_Dte;
    private DbsField cpr_Tr_Bfre_Imge_Id;
    private DbsField cpr_Tr_Aftr_Imge_Id;
    private DbsField cpr_Tr_Invrse_Trans_Dte;
    private DbsField cpr_Tr_Lst_Trans_Dte;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_3;
    private DbsField pnd_Cpr_Bfre_Key__Filler3;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cp_Cntrct;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cp_Payee;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cp_T_Dte;
    private DbsField pnd_Cpr_Aftr_Key;

    private DbsGroup pnd_Cpr_Aftr_Key__R_Field_4;
    private DbsField pnd_Cpr_Aftr_Key__Filler4;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cp_A_Cntrct;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cp_A_Payee;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cp_A_Inv;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_5;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler;
    private DbsField iaa_Tiaa_Fund_Rcrd_Company;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;

    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Rcrd__R_Field_6;
    private DbsField iaa_Cref_Fund_Rcrd_Filler;
    private DbsField iaa_Cref_Fund_Rcrd_Company;
    private DbsField iaa_Cref_Fund_Rcrd_Filler1;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;

    private DbsGroup pnd_Contract_Record_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Event_Ind_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Plan_Num_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ssn_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Fund_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Eff_Date_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Inverse_Date_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Contract_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Payee_Cde_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Cusip_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Plan_Type_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Source_App_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trans_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Act_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trn_Act_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Pend_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trans_Dte_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Fund_Id_Out_Work;
    private DbsField pnd_Hold_Cmpny_Fund_Cde;
    private DbsField pnd_Notfnd;
    private DbsField pnd_Fnd_Found;
    private DbsField pnd_Hold_Key;

    private DbsGroup pnd_Hold_Key__R_Field_7;
    private DbsField pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr;
    private DbsField pnd_Hold_Key_Pnd_Hold_Payee_Cde;
    private DbsField pnd_Max_Nbr_Of_Ext_Fnds;

    private DbsGroup pnd_Fnd_Table;
    private DbsField pnd_Fnd_Table_Fnd_Ticker_Symbol;
    private DbsField pnd_Fnd_Table_Fnd_Cusip;
    private DbsField pnd_Fnd_Table_Fnd_Num_Fund_Cde;
    private DbsField pnd_Fnd_Table_Fnd_Alpha_Fund_Cde;
    private DbsField pnd_Fnd_Table_Fnd_Unit_Rate_Ind;

    private DbsGroup pnd_Fnd_Tab_Rec;
    private DbsField pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip;
    private DbsField pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde;
    private DbsField ticker_Symbol;
    private DbsField hold_Fund;
    private DbsField pnd_Work_Fund_Id;

    private DbsGroup pnd_Work_Fund_Id__R_Field_8;
    private DbsField pnd_Work_Fund_Id_Pnd_Work_Fund_Alpha;
    private DbsField pnd_Work_Fund_Id_Pnd_Work_Fund_Num;
    private DbsField pnd_Hold_Ticker_Symbol;

    private DbsGroup pnd_Hold_Ticker_Symbol__R_Field_9;
    private DbsField pnd_Hold_Ticker_Symbol_Pnd_Beg_Ticker_Symbol;
    private DbsField pnd_Hold_Ticker_Symbol_Pnd_End_Ticker_Symbol;
    private DbsField pnd_Work_Cusip;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_I;
    private DbsField cntr;
    private DbsField pnd_Fnd_Ix;
    private DbsField pnd_Ix1;
    private DbsField pnd_Ix2;
    private DbsField x;
    private DbsField pnd_Ddate;
    private DbsField pnd_Ddate2;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Nbr_Of_Fnds;
    private DbsField pnd_Nbr_Of_Ext_Fnds;
    private DbsField pnd_Ext_Rtrn_Cde;
    private DbsField pnd_Cmpy_Cde;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Fnd_Alpha;
    private DbsField pnd_Work_Num;
    private DbsField pnd_Work_Alpha;
    private DbsField pnd_Ssn_Work_Num;

    private DbsGroup pnd_Ssn_Work_Num__R_Field_10;
    private DbsField pnd_Ssn_Work_Num_Pnd_Ssn_Work_Alpha;

    private DbsGroup pnd_Tot_Report_Rec;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Diff;
    private DbsField pnd_Lastrec_Key;

    private DbsGroup pnd_Lastrec_Key__R_Field_11;
    private DbsField pnd_Lastrec_Key_Pnd_Lastrec_Ppcn_Nbr;
    private DbsField pnd_Lastrec_Key_Pnd_Lastrec_Payee_Cde;

    private DbsGroup work_Fields_In;
    private DbsField work_Fields_In_Pnd_Work_Contract_In;
    private DbsField work_Fields_In_Pnd_Work_Payee_Cde_In;
    private DbsField work_Fields_In_Pnd_Work_Trans_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_cntrct_Tr = new DataAccessProgramView(new NameInfo("vw_cntrct_Tr", "CNTRCT-TR"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        cntrct_Tr_Cntrct_Ppcn_Nbr = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Tr_Cntrct_Optn_Cde = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        cntrct_Tr_Cntrct_Orgn_Cde = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        cntrct_Tr_Plan_Nmbr = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        cntrct_Tr_Cntrct_First_Annt_Dod_Dte = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Tr_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        cntrct_Tr_Trans_Dte = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cntrct_Tr_Bfre_Imge_Id = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cntrct_Tr_Aftr_Imge_Id = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        cntrct_Tr_Invrse_Trans_Dte = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cntrct_Tr_Lst_Trans_Dte = vw_cntrct_Tr.getRecord().newFieldInGroup("cntrct_Tr_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        registerRecord(vw_cntrct_Tr);

        pnd_Cntrct_Bfre_Key = localVariables.newFieldInRecord("pnd_Cntrct_Bfre_Key", "#CNTRCT-BFRE-KEY", FieldType.STRING, 18);

        pnd_Cntrct_Bfre_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Bfre_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Bfre_Key);
        pnd_Cntrct_Bfre_Key__Filler1 = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Cntrct_Bfre_Key_Pnd_C_Ppcn_Nbr = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_C_Ppcn_Nbr", "#C-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Bfre_Key_Pnd_C_T_Dte = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_C_T_Dte", "#C-T-DTE", FieldType.TIME);
        pnd_Cntrct_Aftr_Key = localVariables.newFieldInRecord("pnd_Cntrct_Aftr_Key", "#CNTRCT-AFTR-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Aftr_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Aftr_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Aftr_Key);
        pnd_Cntrct_Aftr_Key__Filler2 = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Cntrct_Aftr_Key_Pnd_C_A_Ppcn_Nbr = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_C_A_Ppcn_Nbr", "#C-A-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Pnd_C_A_Inv = pnd_Cntrct_Aftr_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_C_A_Inv", "#C-A-INV", FieldType.NUMERIC, 
            12);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Typ = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_TYP");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        registerRecord(vw_cpr);

        vw_cpr_Tr = new DataAccessProgramView(new NameInfo("vw_cpr_Tr", "CPR-TR"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        cpr_Tr_Cntrct_Part_Ppcn_Nbr = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Tr_Cntrct_Part_Payee_Cde = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Tr_Prtcpnt_Tax_Id_Nbr = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Tr_Prtcpnt_Tax_Id_Typ = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_TYP");
        cpr_Tr_Cntrct_Actvty_Cde = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Tr_Cntrct_Pend_Cde = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Tr_Trans_Dte = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        cpr_Tr_Bfre_Imge_Id = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cpr_Tr_Aftr_Imge_Id = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        cpr_Tr_Invrse_Trans_Dte = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        cpr_Tr_Lst_Trans_Dte = vw_cpr_Tr.getRecord().newFieldInGroup("cpr_Tr_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        registerRecord(vw_cpr_Tr);

        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_3", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key__Filler3 = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Cpr_Bfre_Key_Pnd_Cp_Cntrct = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cp_Cntrct", "#CP-CNTRCT", FieldType.STRING, 
            10);
        pnd_Cpr_Bfre_Key_Pnd_Cp_Payee = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cp_Payee", "#CP-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Cp_T_Dte = pnd_Cpr_Bfre_Key__R_Field_3.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cp_T_Dte", "#CP-T-DTE", FieldType.TIME);
        pnd_Cpr_Aftr_Key = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key__R_Field_4", "REDEFINE", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key__Filler4 = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Cpr_Aftr_Key_Pnd_Cp_A_Cntrct = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cp_A_Cntrct", "#CP-A-CNTRCT", FieldType.STRING, 
            10);
        pnd_Cpr_Aftr_Key_Pnd_Cp_A_Payee = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cp_A_Payee", "#CP-A-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Cpr_Aftr_Key_Pnd_Cp_A_Inv = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cp_A_Inv", "#CP-A-INV", FieldType.NUMERIC, 12);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_5 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_5", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Filler = iaa_Tiaa_Fund_Rcrd__R_Field_5.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler", "FILLER", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Company = iaa_Tiaa_Fund_Rcrd__R_Field_5.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Company", "COMPANY", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Filler1 = iaa_Tiaa_Fund_Rcrd__R_Field_5.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler1", "FILLER1", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cref_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd", "IAA-CREF-FUND-RCRD"), "IAA_CREF_FUND_RCRD", "IA_MULTI_FUNDS");
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_CNTRCT_PPCN_NBR", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AT");
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Rcrd__R_Field_6 = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd__R_Field_6", "REDEFINE", iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_Filler = iaa_Cref_Fund_Rcrd__R_Field_6.newFieldInGroup("iaa_Cref_Fund_Rcrd_Filler", "FILLER", FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_Company = iaa_Cref_Fund_Rcrd__R_Field_6.newFieldInGroup("iaa_Cref_Fund_Rcrd_Company", "COMPANY", FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_Filler1 = iaa_Cref_Fund_Rcrd__R_Field_6.newFieldInGroup("iaa_Cref_Fund_Rcrd_Filler1", "FILLER1", FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_TOT_PER_AMT", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AY");
        registerRecord(vw_iaa_Cref_Fund_Rcrd);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Contract_Record_Out = localVariables.newGroupInRecord("pnd_Contract_Record_Out", "#CONTRACT-RECORD-OUT");
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Event_Ind_Out", "#EVENT-IND-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Plan_Num_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Plan_Num_Out", "#PLAN-NUM-OUT", 
            FieldType.STRING, 6);
        pnd_Contract_Record_Out_Pnd_Ssn_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ssn_Out", "#SSN-OUT", FieldType.NUMERIC, 
            9);
        pnd_Contract_Record_Out_Pnd_Fund_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Fund_Id_Out", "#FUND-ID-OUT", FieldType.STRING, 
            2);
        pnd_Contract_Record_Out_Pnd_Eff_Date_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Eff_Date_Out", "#EFF-DATE-OUT", 
            FieldType.STRING, 8);
        pnd_Contract_Record_Out_Pnd_Inverse_Date_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Inverse_Date_Out", "#INVERSE-DATE-OUT", 
            FieldType.NUMERIC, 12);
        pnd_Contract_Record_Out_Pnd_Contract_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Contract_Out", "#CONTRACT-OUT", 
            FieldType.STRING, 10);
        pnd_Contract_Record_Out_Pnd_Payee_Cde_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Payee_Cde_Out", "#PAYEE-CDE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Cusip_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Cusip_Out", "#CUSIP-OUT", FieldType.STRING, 
            9);
        pnd_Contract_Record_Out_Pnd_Plan_Type_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Plan_Type_Out", "#PLAN-TYPE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Source_App_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Source_App_Id_Out", "#SOURCE-APP-ID-OUT", 
            FieldType.STRING, 4);
        pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out", "#IA-ORIG-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out", "#IA-OPT-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out", 
            "#IA-SETTLEMENT-ID-OUT", FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Trans_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trans_Code_Out", "#TRANS-CODE-OUT", 
            FieldType.NUMERIC, 3);
        pnd_Contract_Record_Out_Pnd_Act_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Act_Code_Out", "#ACT-CODE-OUT", 
            FieldType.NUMERIC, 1);
        pnd_Contract_Record_Out_Pnd_Trn_Act_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trn_Act_Code_Out", "#TRN-ACT-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Pend_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Pend_Code_Out", "#PEND-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out", "#FIRST-ANN-DOD-OUT", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out", "#SECOND-ANN-DOD-OUT", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out", "#TRANS-SUB-CODE-OUT", 
            FieldType.STRING, 3);
        pnd_Contract_Record_Out_Pnd_Trans_Dte_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trans_Dte_Out", "#TRANS-DTE-OUT", 
            FieldType.TIME);
        pnd_Contract_Record_Out_Pnd_Fund_Id_Out_Work = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Fund_Id_Out_Work", "#FUND-ID-OUT-WORK", 
            FieldType.STRING, 3);
        pnd_Hold_Cmpny_Fund_Cde = localVariables.newFieldInRecord("pnd_Hold_Cmpny_Fund_Cde", "#HOLD-CMPNY-FUND-CDE", FieldType.STRING, 5);
        pnd_Notfnd = localVariables.newFieldInRecord("pnd_Notfnd", "#NOTFND", FieldType.BOOLEAN, 1);
        pnd_Fnd_Found = localVariables.newFieldInRecord("pnd_Fnd_Found", "#FND-FOUND", FieldType.BOOLEAN, 1);
        pnd_Hold_Key = localVariables.newFieldInRecord("pnd_Hold_Key", "#HOLD-KEY", FieldType.STRING, 12);

        pnd_Hold_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Hold_Key__R_Field_7", "REDEFINE", pnd_Hold_Key);
        pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr = pnd_Hold_Key__R_Field_7.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr", "#HOLD-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Hold_Key_Pnd_Hold_Payee_Cde = pnd_Hold_Key__R_Field_7.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Payee_Cde", "#HOLD-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Max_Nbr_Of_Ext_Fnds = localVariables.newFieldInRecord("pnd_Max_Nbr_Of_Ext_Fnds", "#MAX-NBR-OF-EXT-FNDS", FieldType.PACKED_DECIMAL, 3);

        pnd_Fnd_Table = localVariables.newGroupArrayInRecord("pnd_Fnd_Table", "#FND-TABLE", new DbsArrayController(1, 150));
        pnd_Fnd_Table_Fnd_Ticker_Symbol = pnd_Fnd_Table.newFieldInGroup("pnd_Fnd_Table_Fnd_Ticker_Symbol", "FND-TICKER-SYMBOL", FieldType.STRING, 10);
        pnd_Fnd_Table_Fnd_Cusip = pnd_Fnd_Table.newFieldInGroup("pnd_Fnd_Table_Fnd_Cusip", "FND-CUSIP", FieldType.STRING, 10);
        pnd_Fnd_Table_Fnd_Num_Fund_Cde = pnd_Fnd_Table.newFieldInGroup("pnd_Fnd_Table_Fnd_Num_Fund_Cde", "FND-NUM-FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Fnd_Table_Fnd_Alpha_Fund_Cde = pnd_Fnd_Table.newFieldInGroup("pnd_Fnd_Table_Fnd_Alpha_Fund_Cde", "FND-ALPHA-FUND-CDE", FieldType.STRING, 2);
        pnd_Fnd_Table_Fnd_Unit_Rate_Ind = pnd_Fnd_Table.newFieldInGroup("pnd_Fnd_Table_Fnd_Unit_Rate_Ind", "FND-UNIT-RATE-IND", FieldType.PACKED_DECIMAL, 
            3);

        pnd_Fnd_Tab_Rec = localVariables.newGroupInRecord("pnd_Fnd_Tab_Rec", "#FND-TAB-REC");
        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip = pnd_Fnd_Tab_Rec.newFieldInGroup("pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip", "#FND-TAB-CUSIP", FieldType.STRING, 10);
        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde = pnd_Fnd_Tab_Rec.newFieldInGroup("pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde", "#FND-TAB-NUM-FUND-CDE", 
            FieldType.NUMERIC, 2);
        ticker_Symbol = localVariables.newFieldInRecord("ticker_Symbol", "TICKER-SYMBOL", FieldType.STRING, 10);
        hold_Fund = localVariables.newFieldInRecord("hold_Fund", "HOLD-FUND", FieldType.STRING, 3);
        pnd_Work_Fund_Id = localVariables.newFieldInRecord("pnd_Work_Fund_Id", "#WORK-FUND-ID", FieldType.STRING, 3);

        pnd_Work_Fund_Id__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Fund_Id__R_Field_8", "REDEFINE", pnd_Work_Fund_Id);
        pnd_Work_Fund_Id_Pnd_Work_Fund_Alpha = pnd_Work_Fund_Id__R_Field_8.newFieldInGroup("pnd_Work_Fund_Id_Pnd_Work_Fund_Alpha", "#WORK-FUND-ALPHA", 
            FieldType.STRING, 1);
        pnd_Work_Fund_Id_Pnd_Work_Fund_Num = pnd_Work_Fund_Id__R_Field_8.newFieldInGroup("pnd_Work_Fund_Id_Pnd_Work_Fund_Num", "#WORK-FUND-NUM", FieldType.STRING, 
            2);
        pnd_Hold_Ticker_Symbol = localVariables.newFieldInRecord("pnd_Hold_Ticker_Symbol", "#HOLD-TICKER-SYMBOL", FieldType.STRING, 10);

        pnd_Hold_Ticker_Symbol__R_Field_9 = localVariables.newGroupInRecord("pnd_Hold_Ticker_Symbol__R_Field_9", "REDEFINE", pnd_Hold_Ticker_Symbol);
        pnd_Hold_Ticker_Symbol_Pnd_Beg_Ticker_Symbol = pnd_Hold_Ticker_Symbol__R_Field_9.newFieldInGroup("pnd_Hold_Ticker_Symbol_Pnd_Beg_Ticker_Symbol", 
            "#BEG-TICKER-SYMBOL", FieldType.STRING, 2);
        pnd_Hold_Ticker_Symbol_Pnd_End_Ticker_Symbol = pnd_Hold_Ticker_Symbol__R_Field_9.newFieldInGroup("pnd_Hold_Ticker_Symbol_Pnd_End_Ticker_Symbol", 
            "#END-TICKER-SYMBOL", FieldType.STRING, 8);
        pnd_Work_Cusip = localVariables.newFieldInRecord("pnd_Work_Cusip", "#WORK-CUSIP", FieldType.STRING, 9);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 5);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 5);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        cntr = localVariables.newFieldInRecord("cntr", "CNTR", FieldType.PACKED_DECIMAL, 5);
        pnd_Fnd_Ix = localVariables.newFieldInRecord("pnd_Fnd_Ix", "#FND-IX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ix1 = localVariables.newFieldInRecord("pnd_Ix1", "#IX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ix2 = localVariables.newFieldInRecord("pnd_Ix2", "#IX2", FieldType.PACKED_DECIMAL, 3);
        x = localVariables.newFieldInRecord("x", "X", FieldType.PACKED_DECIMAL, 7);
        pnd_Ddate = localVariables.newFieldInRecord("pnd_Ddate", "#DDATE", FieldType.STRING, 8);
        pnd_Ddate2 = localVariables.newFieldInRecord("pnd_Ddate2", "#DDATE2", FieldType.STRING, 8);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Nbr_Of_Fnds = localVariables.newFieldInRecord("pnd_Nbr_Of_Fnds", "#NBR-OF-FNDS", FieldType.PACKED_DECIMAL, 3);
        pnd_Nbr_Of_Ext_Fnds = localVariables.newFieldInRecord("pnd_Nbr_Of_Ext_Fnds", "#NBR-OF-EXT-FNDS", FieldType.PACKED_DECIMAL, 3);
        pnd_Ext_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Ext_Rtrn_Cde", "#EXT-RTRN-CDE", FieldType.STRING, 1);
        pnd_Cmpy_Cde = localVariables.newFieldArrayInRecord("pnd_Cmpy_Cde", "#CMPY-CDE", FieldType.STRING, 3, new DbsArrayController(1, 2));
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Fnd_Alpha = localVariables.newFieldInRecord("pnd_Fnd_Alpha", "#FND-ALPHA", FieldType.STRING, 2);
        pnd_Work_Num = localVariables.newFieldInRecord("pnd_Work_Num", "#WORK-NUM", FieldType.NUMERIC, 2);
        pnd_Work_Alpha = localVariables.newFieldInRecord("pnd_Work_Alpha", "#WORK-ALPHA", FieldType.STRING, 2);
        pnd_Ssn_Work_Num = localVariables.newFieldInRecord("pnd_Ssn_Work_Num", "#SSN-WORK-NUM", FieldType.NUMERIC, 9);

        pnd_Ssn_Work_Num__R_Field_10 = localVariables.newGroupInRecord("pnd_Ssn_Work_Num__R_Field_10", "REDEFINE", pnd_Ssn_Work_Num);
        pnd_Ssn_Work_Num_Pnd_Ssn_Work_Alpha = pnd_Ssn_Work_Num__R_Field_10.newFieldInGroup("pnd_Ssn_Work_Num_Pnd_Ssn_Work_Alpha", "#SSN-WORK-ALPHA", FieldType.STRING, 
            9);

        pnd_Tot_Report_Rec = localVariables.newGroupInRecord("pnd_Tot_Report_Rec", "#TOT-REPORT-REC");
        pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar", "#TOT-FST-DOLLAR", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar", "#TOT-LST-DOLLAR", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events", "#TOT-FSTLST-EVENTS", 
            FieldType.NUMERIC, 5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset", "#TOT-FST-OFFSET", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset", "#TOT-LST-OFFSET", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written", "#TOT-FSTLST-OFFSET-WRITTEN", 
            FieldType.NUMERIC, 5);
        pnd_Tot_Report_Rec_Pnd_Tot_Diff = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Diff", "#TOT-DIFF", FieldType.NUMERIC, 5);
        pnd_Lastrec_Key = localVariables.newFieldInRecord("pnd_Lastrec_Key", "#LASTREC-KEY", FieldType.STRING, 12);

        pnd_Lastrec_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Lastrec_Key__R_Field_11", "REDEFINE", pnd_Lastrec_Key);
        pnd_Lastrec_Key_Pnd_Lastrec_Ppcn_Nbr = pnd_Lastrec_Key__R_Field_11.newFieldInGroup("pnd_Lastrec_Key_Pnd_Lastrec_Ppcn_Nbr", "#LASTREC-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Lastrec_Key_Pnd_Lastrec_Payee_Cde = pnd_Lastrec_Key__R_Field_11.newFieldInGroup("pnd_Lastrec_Key_Pnd_Lastrec_Payee_Cde", "#LASTREC-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        work_Fields_In = localVariables.newGroupInRecord("work_Fields_In", "WORK-FIELDS-IN");
        work_Fields_In_Pnd_Work_Contract_In = work_Fields_In.newFieldInGroup("work_Fields_In_Pnd_Work_Contract_In", "#WORK-CONTRACT-IN", FieldType.STRING, 
            10);
        work_Fields_In_Pnd_Work_Payee_Cde_In = work_Fields_In.newFieldInGroup("work_Fields_In_Pnd_Work_Payee_Cde_In", "#WORK-PAYEE-CDE-IN", FieldType.NUMERIC, 
            2);
        work_Fields_In_Pnd_Work_Trans_Dte = work_Fields_In.newFieldInGroup("work_Fields_In_Pnd_Work_Trans_Dte", "#WORK-TRANS-DTE", FieldType.TIME);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrct.reset();
        vw_cntrct_Tr.reset();
        vw_cpr.reset();
        vw_cpr_Tr.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cref_Fund_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
        pnd_Cntrct_Bfre_Key.setInitialValue("1");
        pnd_Cntrct_Aftr_Key.setInitialValue("2");
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Cpr_Aftr_Key.setInitialValue("2");
        pnd_Max_Nbr_Of_Ext_Fnds.setInitialValue(150);
        pnd_Cmpy_Cde.getValue(1).setInitialValue("001");
        pnd_Cmpy_Cde.getValue(2).setInitialValue("002");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap600() throws Exception
    {
        super("Iaap600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP600 ***");                                                                                                        //Natural: WRITE '*** START OF PROGRAM IAAP600 ***'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM LOAD-EXTERNALIZATION-FUNDS-DATA
        sub_Load_Externalization_Funds_Data();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CNTRL-RCRD
        sub_Read_Iaa_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-IAA-TRANSACTION
        sub_Process_Iaa_Transaction();
        if (condition(Global.isEscape())) {return;}
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CNTRL-RCRD
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IAA-TRANSACTION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CNTRCT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IAA-CNTRCT-PRTCPNT-ROLE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-EXTERNALIZATION-FUNDS-DATA
        //* ***    #FND-TABLE.FND-NUM-FUND-CDE(#K) := FND-IA-FUND-CDE (#I)
        //* ***    #FND-TABLE.FND-NUM-FUND-CDE(#K) := FND-IA-FUND-CDE (#I)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEARCH-FUND-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-FUNDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CREF-FUNDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
    }
    private void sub_Read_Iaa_Cntrl_Rcrd() throws Exception                                                                                                               //Natural: READ-IAA-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Ddate.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DDATE
            pnd_Check_Dte.compute(new ComputeParameters(false, pnd_Check_Dte), pnd_Ddate.val());                                                                          //Natural: ASSIGN #CHECK-DTE := VAL ( #DDATE )
            pnd_Ddate2.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DDATE2
            pnd_Todays_Dte.compute(new ComputeParameters(false, pnd_Todays_Dte), pnd_Ddate2.val());                                                                       //Natural: ASSIGN #TODAYS-DTE := VAL ( #DDATE2 )
            getReports().write(0, "CNTRL #CHECK-DTE  ",pnd_Check_Dte);                                                                                                    //Natural: WRITE 'CNTRL #CHECK-DTE  ' #CHECK-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "CNTRL #TODAYS-DTE ",pnd_Todays_Dte);                                                                                                   //Natural: WRITE 'CNTRL #TODAYS-DTE ' #TODAYS-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "CNTRL #INVERSE-DT ",iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte);                                                                                //Natural: WRITE 'CNTRL #INVERSE-DT ' CNTRL-INVRSE-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   READ-IAA-CNTRL-RCRD
    }
    private void sub_Process_Iaa_Transaction() throws Exception                                                                                                           //Natural: PROCESS-IAA-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #CHECK-DTE
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Check_Dte, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            if (condition(!(iaa_Trans_Rcrd_Trans_Cde.equals(6) || iaa_Trans_Rcrd_Trans_Cde.equals(20) || iaa_Trans_Rcrd_Trans_Cde.equals(30) || iaa_Trans_Rcrd_Trans_Cde.equals(31)  //Natural: ACCEPT IF TRANS-CDE = 6 OR TRANS-CDE = 20 OR TRANS-CDE = 30 OR TRANS-CDE = 31 OR TRANS-CDE = 33 OR TRANS-CDE = 35 OR TRANS-CDE = 36 OR TRANS-CDE = 40 OR TRANS-CDE = 50 OR TRANS-CDE = 51 OR TRANS-CDE = 52 OR TRANS-CDE = 53 OR TRANS-CDE = 37 OR TRANS-CDE = 66 OR TRANS-CDE = 102 OR TRANS-CDE = 500 OR TRANS-CDE = 502 OR TRANS-CDE = 504 OR TRANS-CDE = 506 OR TRANS-CDE = 508 OR TRANS-CDE = 512 OR TRANS-CDE = 514 OR TRANS-CDE = 516 OR TRANS-CDE = 518
                || iaa_Trans_Rcrd_Trans_Cde.equals(33) || iaa_Trans_Rcrd_Trans_Cde.equals(35) || iaa_Trans_Rcrd_Trans_Cde.equals(36) || iaa_Trans_Rcrd_Trans_Cde.equals(40) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(51) || iaa_Trans_Rcrd_Trans_Cde.equals(52) || iaa_Trans_Rcrd_Trans_Cde.equals(53) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(37) || iaa_Trans_Rcrd_Trans_Cde.equals(66) || iaa_Trans_Rcrd_Trans_Cde.equals(102) || iaa_Trans_Rcrd_Trans_Cde.equals(500) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(502) || iaa_Trans_Rcrd_Trans_Cde.equals(504) || iaa_Trans_Rcrd_Trans_Cde.equals(506) || iaa_Trans_Rcrd_Trans_Cde.equals(508) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(512) || iaa_Trans_Rcrd_Trans_Cde.equals(514) || iaa_Trans_Rcrd_Trans_Cde.equals(516) || iaa_Trans_Rcrd_Trans_Cde.equals(518))))
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.equals(pnd_Check_Dte)))                                                                                          //Natural: IF TRANS-CHECK-DTE = #CHECK-DTE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(0, "TRANS CHECK DATE NOT FOUND");                                                                                                    //Natural: DISPLAY 'TRANS CHECK DATE NOT FOUND'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(50) && (iaa_Trans_Rcrd_Trans_User_Area.equals("BPS") || iaa_Trans_Rcrd_Trans_User_Area.equals("BPSTFR")         //Natural: IF TRANS-CDE = 50 AND ( TRANS-USER-AREA = 'BPS' OR TRANS-USER-AREA = 'BPSTFR' OR TRANS-USER-AREA = 'GROUP' OR TRANS-USER-AREA = 'TAX' OR TRANS-USER-AREA = 'GAPS' OR TRANS-USER-AREA = 'COUNS' OR TRANS-USER-AREA = 'COR' OR TRANS-USER-ID = 'IAAP999' )
                || iaa_Trans_Rcrd_Trans_User_Area.equals("GROUP") || iaa_Trans_Rcrd_Trans_User_Area.equals("TAX") || iaa_Trans_Rcrd_Trans_User_Area.equals("GAPS") 
                || iaa_Trans_Rcrd_Trans_User_Area.equals("COUNS") || iaa_Trans_Rcrd_Trans_User_Area.equals("COR") || iaa_Trans_Rcrd_Trans_User_Id.equals("IAAP999"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.equals(pnd_Check_Dte) && iaa_Trans_Rcrd_Trans_Todays_Dte.equals(pnd_Todays_Dte) && iaa_Trans_Rcrd_Trans_Verify_Cde.equals(" "))) //Natural: IF TRANS-CHECK-DTE = #CHECK-DTE AND TRANS-TODAYS-DTE = #TODAYS-DTE AND TRANS-VERIFY-CDE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Notfnd.reset();                                                                                                                                           //Natural: RESET #NOTFND
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CNTRCT
            sub_Read_Iaa_Cntrct();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Notfnd.getBoolean()))                                                                                                                       //Natural: IF #NOTFND
            {
                getReports().write(0, "CONTRACT NOT FOUND ON IAA-CNTRCT FILE ",iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                            //Natural: WRITE 'CONTRACT NOT FOUND ON IAA-CNTRCT FILE ' TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                   //Natural: MOVE TRANS-PPCN-NBR TO #HOLD-PPCN-NBR
                pnd_Hold_Key_Pnd_Hold_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                 //Natural: MOVE TRANS-PAYEE-CDE TO #HOLD-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM FIND-IAA-CNTRCT-PRTCPNT-ROLE
                sub_Find_Iaa_Cntrct_Prtcpnt_Role();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Notfnd.getBoolean()))                                                                                                                   //Natural: IF #NOTFND
                {
                    getReports().write(0, "CONTRACT NOT FOUND ON PART ROLE FILE ",iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                         //Natural: WRITE 'CONTRACT NOT FOUND ON PART ROLE FILE ' TRANS-PPCN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  IF (CNTRCT-ACTVTY-CDE = 1 AND CNTRCT-PEND-CDE = '0') OR
            //*    (CNTRCT-ACTVTY-CDE = 9 AND TRANS-CDE > 500)       OR
            //*    ((TRANS-ACTVTY-CDE  = 'R') AND (TRANS-CDE = 30 OR TRANS-CDE = 31 OR
            //*    TRANS-CDE = 33 OR TRANS-CDE = 66 ))
            //*    IGNORE
            //*  ELSE
            //*    ESCAPE TOP
            //*  END-IF
            //*  PERFORM PROCESS-TIAA-FUNDS
            //*  PERFORM PROCESS-CREF-FUNDS
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
            sub_Write_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PROCESS-IAA-TRANSACTION
    }
    private void sub_Read_Iaa_Cntrct() throws Exception                                                                                                                   //Natural: READ-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = TRANS-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Trans_Rcrd_Trans_Ppcn_Nbr, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORD
            {
                pnd_Notfnd.setValue(true);                                                                                                                                //Natural: ASSIGN #NOTFND := TRUE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  02/2012 BEGIN
        if (condition((pnd_Notfnd.getBoolean() && (iaa_Trans_Rcrd_Trans_Cde.equals(31) || iaa_Trans_Rcrd_Trans_Cde.equals(33)))))                                         //Natural: IF #NOTFND AND ( TRANS-CDE = 31 OR = 33 )
        {
            if (condition(iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cntrct_Bfre_Key_Pnd_C_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                               //Natural: ASSIGN #C-PPCN-NBR := TRANS-PPCN-NBR
                pnd_Cntrct_Bfre_Key_Pnd_C_T_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                       //Natural: ASSIGN #C-T-DTE := IAA-TRANS-RCRD.TRANS-DTE
                vw_cntrct_Tr.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) CNTRCT-TR BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Cntrct_Bfre_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
                1
                );
                READ03:
                while (condition(vw_cntrct_Tr.readNextRow("READ03")))
                {
                    if (condition(cntrct_Tr_Cntrct_Ppcn_Nbr.equals(pnd_Cntrct_Bfre_Key_Pnd_C_Ppcn_Nbr) && cntrct_Tr_Trans_Dte.equals(pnd_Cntrct_Bfre_Key_Pnd_C_T_Dte)))   //Natural: IF CNTRCT-TR.CNTRCT-PPCN-NBR = #C-PPCN-NBR AND CNTRCT-TR.TRANS-DTE = #C-T-DTE
                    {
                        pnd_Notfnd.setValue(false);                                                                                                                       //Natural: ASSIGN #NOTFND := FALSE
                        vw_iaa_Cntrct.setValuesByName(vw_cntrct_Tr);                                                                                                      //Natural: MOVE BY NAME CNTRCT-TR TO IAA-CNTRCT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cntrct_Aftr_Key_Pnd_C_A_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                             //Natural: ASSIGN #C-A-PPCN-NBR := TRANS-PPCN-NBR
                pnd_Cntrct_Aftr_Key_Pnd_C_A_Inv.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                                //Natural: ASSIGN #C-A-INV := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                vw_cntrct_Tr.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) CNTRCT-TR BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
                (
                "READ04",
                new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_Cntrct_Aftr_Key, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
                1
                );
                READ04:
                while (condition(vw_cntrct_Tr.readNextRow("READ04")))
                {
                    if (condition(cntrct_Tr_Cntrct_Ppcn_Nbr.equals(pnd_Cntrct_Aftr_Key_Pnd_C_A_Ppcn_Nbr) && cntrct_Tr_Invrse_Trans_Dte.equals(pnd_Cntrct_Aftr_Key_Pnd_C_A_Inv))) //Natural: IF CNTRCT-TR.CNTRCT-PPCN-NBR = #C-A-PPCN-NBR AND CNTRCT-TR.INVRSE-TRANS-DTE = #C-A-INV
                    {
                        pnd_Notfnd.setValue(false);                                                                                                                       //Natural: ASSIGN #NOTFND := FALSE
                        vw_iaa_Cntrct.setValuesByName(vw_cntrct_Tr);                                                                                                      //Natural: MOVE BY NAME CNTRCT-TR TO IAA-CNTRCT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  02/2012 END
        //*   READ-IAA-CNTRCT
    }
    private void sub_Find_Iaa_Cntrct_Prtcpnt_Role() throws Exception                                                                                                      //Natural: FIND-IAA-CNTRCT-PRTCPNT-ROLE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #HOLD-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Hold_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_cpr.readNextRow("FIND02", true)))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
            if (condition(vw_cpr.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORD
            {
                pnd_Notfnd.setValue(true);                                                                                                                                //Natural: ASSIGN #NOTFND := TRUE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  02/2012 BEGIN
        if (condition((pnd_Notfnd.getBoolean() && (iaa_Trans_Rcrd_Trans_Cde.equals(31) || iaa_Trans_Rcrd_Trans_Cde.equals(33)))))                                         //Natural: IF #NOTFND AND ( TRANS-CDE = 31 OR = 33 )
        {
            if (condition(iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Cpr_Bfre_Key_Pnd_Cp_Cntrct.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                   //Natural: ASSIGN #CP-CNTRCT := TRANS-PPCN-NBR
                pnd_Cpr_Bfre_Key_Pnd_Cp_Payee.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                   //Natural: ASSIGN #CP-PAYEE := TRANS-PAYEE-CDE
                pnd_Cpr_Bfre_Key_Pnd_Cp_T_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                         //Natural: ASSIGN #CP-T-DTE := IAA-TRANS-RCRD.TRANS-DTE
                vw_cpr_Tr.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CPR-TR BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
                (
                "READ05",
                new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Cpr_Bfre_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
                1
                );
                READ05:
                while (condition(vw_cpr_Tr.readNextRow("READ05")))
                {
                    if (condition(cpr_Tr_Cntrct_Part_Ppcn_Nbr.equals(pnd_Cpr_Bfre_Key_Pnd_Cp_Cntrct) && cpr_Tr_Cntrct_Part_Payee_Cde.equals(pnd_Cpr_Bfre_Key_Pnd_Cp_Payee)  //Natural: IF CPR-TR.CNTRCT-PART-PPCN-NBR = #CP-CNTRCT AND CPR-TR.CNTRCT-PART-PAYEE-CDE = #CP-PAYEE AND CPR-TR.TRANS-DTE = #CP-T-DTE
                        && cpr_Tr_Trans_Dte.equals(pnd_Cpr_Bfre_Key_Pnd_Cp_T_Dte)))
                    {
                        pnd_Notfnd.reset();                                                                                                                               //Natural: RESET #NOTFND
                        vw_cpr.setValuesByName(vw_cpr_Tr);                                                                                                                //Natural: MOVE BY NAME CPR-TR TO CPR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cpr_Aftr_Key_Pnd_Cp_A_Cntrct.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                 //Natural: ASSIGN #CP-A-CNTRCT := TRANS-PPCN-NBR
                pnd_Cpr_Aftr_Key_Pnd_Cp_A_Payee.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                 //Natural: ASSIGN #CP-A-PAYEE := TRANS-PAYEE-CDE
                pnd_Cpr_Aftr_Key_Pnd_Cp_A_Inv.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                                  //Natural: ASSIGN #CP-A-INV := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                vw_cpr_Tr.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CPR-TR BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
                (
                "READ06",
                new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Cpr_Aftr_Key, WcType.BY) },
                new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
                1
                );
                READ06:
                while (condition(vw_cpr_Tr.readNextRow("READ06")))
                {
                    if (condition(cpr_Tr_Cntrct_Part_Ppcn_Nbr.equals(pnd_Cpr_Aftr_Key_Pnd_Cp_A_Cntrct) && cpr_Tr_Cntrct_Part_Payee_Cde.equals(pnd_Cpr_Aftr_Key_Pnd_Cp_A_Payee)  //Natural: IF CPR-TR.CNTRCT-PART-PPCN-NBR = #CP-A-CNTRCT AND CPR-TR.CNTRCT-PART-PAYEE-CDE = #CP-A-PAYEE AND CPR-TR.INVRSE-TRANS-DTE = #CP-A-INV
                        && cpr_Tr_Invrse_Trans_Dte.equals(pnd_Cpr_Aftr_Key_Pnd_Cp_A_Inv)))
                    {
                        pnd_Notfnd.reset();                                                                                                                               //Natural: RESET #NOTFND
                        vw_cpr.setValuesByName(vw_cpr_Tr);                                                                                                                //Natural: MOVE BY NAME CPR-TR TO CPR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  02/2012 END
        //*   FIND-IAA-CNTRCT-PRTCPNT-ROLE
    }
    private void sub_Load_Externalization_Funds_Data() throws Exception                                                                                                   //Natural: LOAD-EXTERNALIZATION-FUNDS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  RESET #J #K
        //*  REPEAT
        //*   ADD 1 TO #J
        //*   IF #J GT 2
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*  05/15
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Request_Ind().setValue("*");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := '*'
        pdaNeca4000.getNeca4000_Function_Cde().setValue("FND");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'FND'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("03");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '03'
        //*  NECA4000.FUNCTION-CDE := 'CFN'
        //*  NECA4000.INPT-KEY-OPTION-CDE := '01'          /* BY COMPANY FUND
        //*  NECA4000.CFN-FND-COMPANY-CDE := #CMPY-CDE(#J) /* 1 = TIAA, 2 = PA
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        //*  IF NECA4000.RETURN-CDE = '00' OR = '  '  /* 05/15
        //*  05/15 - FIRST LOOP
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("05")))                                                                                                 //Natural: IF NECA4000.RETURN-CDE = '05'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO OUTPUT-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaNeca4000.getNeca4000_Output_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I).equals(" ")))                                                                   //Natural: IF NECA4000.FND-TICKER-SYMBOL ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Hold_Ticker_Symbol.setValue(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I));                                                             //Natural: MOVE NECA4000.FND-TICKER-SYMBOL ( #I ) TO #HOLD-TICKER-SYMBOL
                pnd_Fnd_Alpha.setValue(pdaNeca4000.getNeca4000_Fnd_Ia_Fund_Cde().getValue(pnd_I), MoveOption.RightJustified);                                             //Natural: MOVE RIGHT FND-IA-FUND-CDE ( #I ) TO #FND-ALPHA
                if (condition(DbsUtil.is(pnd_Fnd_Alpha.getText(),"N2")))                                                                                                  //Natural: IF #FND-ALPHA IS ( N2 )
                {
                    if (condition(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I).notEquals("TIAASNDX")))                                                     //Natural: IF NECA4000.FND-TICKER-SYMBOL ( #I ) NOT = 'TIAASNDX'
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        pnd_Fnd_Table_Fnd_Ticker_Symbol.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I));                            //Natural: ASSIGN #FND-TABLE.FND-TICKER-SYMBOL ( #K ) := NECA4000.FND-TICKER-SYMBOL ( #I )
                        pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K).compute(new ComputeParameters(false, pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K)),              //Natural: ASSIGN #FND-TABLE.FND-NUM-FUND-CDE ( #K ) := VAL ( #FND-ALPHA )
                            pnd_Fnd_Alpha.val());
                        pnd_Fnd_Table_Fnd_Alpha_Fund_Cde.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Alpha_Fund_Cde().getValue(pnd_I));                          //Natural: ASSIGN #FND-TABLE.FND-ALPHA-FUND-CDE ( #K ) := NECA4000.FND-ALPHA-FUND-CDE ( #I )
                        if (condition(pnd_Hold_Ticker_Symbol_Pnd_Beg_Ticker_Symbol.equals("TL")))                                                                         //Natural: IF #BEG-TICKER-SYMBOL = 'TL'
                        {
                            pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_External_Cusip().getValue(pnd_I));                               //Natural: ASSIGN #FND-TABLE.FND-CUSIP ( #K ) := FND-EXTERNAL-CUSIP ( #I )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Cusip().getValue(pnd_I));                                        //Natural: ASSIGN #FND-TABLE.FND-CUSIP ( #K ) := NECA4000.FND-CUSIP ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip.setValue(pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K));                                                              //Natural: MOVE #FND-TABLE.FND-CUSIP ( #K ) TO #FND-TAB-CUSIP
                        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde.setValue(pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K));                                                //Natural: MOVE #FND-TABLE.FND-NUM-FUND-CDE ( #K ) TO #FND-TAB-NUM-FUND-CDE
                        getWorkFiles().write(2, false, pnd_Fnd_Tab_Rec);                                                                                                  //Natural: WRITE WORK FILE 2 #FND-TAB-REC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  05/15 - START
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        //*  NEXT LOOP
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals("  ")))                                            //Natural: IF NECA4000.RETURN-CDE = '00' OR = '  '
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO OUTPUT-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaNeca4000.getNeca4000_Output_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I).equals(" ")))                                                                   //Natural: IF NECA4000.FND-TICKER-SYMBOL ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Hold_Ticker_Symbol.setValue(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I));                                                             //Natural: MOVE NECA4000.FND-TICKER-SYMBOL ( #I ) TO #HOLD-TICKER-SYMBOL
                pnd_Fnd_Alpha.setValue(pdaNeca4000.getNeca4000_Fnd_Ia_Fund_Cde().getValue(pnd_I), MoveOption.RightJustified);                                             //Natural: MOVE RIGHT FND-IA-FUND-CDE ( #I ) TO #FND-ALPHA
                if (condition(DbsUtil.is(pnd_Fnd_Alpha.getText(),"N2")))                                                                                                  //Natural: IF #FND-ALPHA IS ( N2 )
                {
                    if (condition(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I).notEquals("TIAASNDX")))                                                     //Natural: IF NECA4000.FND-TICKER-SYMBOL ( #I ) NOT = 'TIAASNDX'
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        pnd_Fnd_Table_Fnd_Ticker_Symbol.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Ticker_Symbol().getValue(pnd_I));                            //Natural: ASSIGN #FND-TABLE.FND-TICKER-SYMBOL ( #K ) := NECA4000.FND-TICKER-SYMBOL ( #I )
                        pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K).compute(new ComputeParameters(false, pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K)),              //Natural: ASSIGN #FND-TABLE.FND-NUM-FUND-CDE ( #K ) := VAL ( #FND-ALPHA )
                            pnd_Fnd_Alpha.val());
                        pnd_Fnd_Table_Fnd_Alpha_Fund_Cde.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Alpha_Fund_Cde().getValue(pnd_I));                          //Natural: ASSIGN #FND-TABLE.FND-ALPHA-FUND-CDE ( #K ) := NECA4000.FND-ALPHA-FUND-CDE ( #I )
                        if (condition(pnd_Hold_Ticker_Symbol_Pnd_Beg_Ticker_Symbol.equals("TL")))                                                                         //Natural: IF #BEG-TICKER-SYMBOL = 'TL'
                        {
                            pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_External_Cusip().getValue(pnd_I));                               //Natural: ASSIGN #FND-TABLE.FND-CUSIP ( #K ) := FND-EXTERNAL-CUSIP ( #I )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K).setValue(pdaNeca4000.getNeca4000_Fnd_Cusip().getValue(pnd_I));                                        //Natural: ASSIGN #FND-TABLE.FND-CUSIP ( #K ) := NECA4000.FND-CUSIP ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip.setValue(pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_K));                                                              //Natural: MOVE #FND-TABLE.FND-CUSIP ( #K ) TO #FND-TAB-CUSIP
                        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde.setValue(pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_K));                                                //Natural: MOVE #FND-TABLE.FND-NUM-FUND-CDE ( #K ) TO #FND-TAB-NUM-FUND-CDE
                        getWorkFiles().write(2, false, pnd_Fnd_Tab_Rec);                                                                                                  //Natural: WRITE WORK FILE 2 #FND-TAB-REC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  05/15 - END
        //*  END-REPEAT
        //*  LOAD-EXTERNALIZATION-FUNDS-DATA
    }
    private void sub_Search_Fund_Table() throws Exception                                                                                                                 //Natural: SEARCH-FUND-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DISPLAY 'IN SEARCH-FUND-TABLE'
        pnd_Fnd_Found.reset();                                                                                                                                            //Natural: RESET #FND-FOUND
        FOR03:                                                                                                                                                            //Natural: FOR #FND-IX = 1 TO #K
        for (pnd_Fnd_Ix.setValue(1); condition(pnd_Fnd_Ix.lessOrEqual(pnd_K)); pnd_Fnd_Ix.nadd(1))
        {
            if (condition(pnd_Fnd_Table_Fnd_Num_Fund_Cde.getValue(pnd_Fnd_Ix).equals(pnd_Work_Num)))                                                                      //Natural: IF #FND-TABLE.FND-NUM-FUND-CDE ( #FND-IX ) = #WORK-NUM
            {
                pnd_Fnd_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #FND-FOUND := TRUE
                pnd_Work_Cusip.setValue(pnd_Fnd_Table_Fnd_Cusip.getValue(pnd_Fnd_Ix));                                                                                    //Natural: MOVE #FND-TABLE.FND-CUSIP ( #FND-IX ) TO #WORK-CUSIP
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SEARCH-FUND-TABLE
    }
    private void sub_Process_Tiaa_Funds() throws Exception                                                                                                                //Natural: PROCESS-TIAA-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #HOLD-KEY
        (
        "READ07",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Hold_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ07:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ07")))
        {
            if (condition((iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr)) || (iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Hold_Key_Pnd_Hold_Payee_Cde)))) //Natural: IF ( TIAA-CNTRCT-PPCN-NBR NE #HOLD-PPCN-NBR ) OR ( TIAA-CNTRCT-PAYEE-CDE NE #HOLD-PAYEE-CDE )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("U41") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("W41")))                                  //Natural: IF TIAA-CMPNY-FUND-CDE = 'U41' OR TIAA-CMPNY-FUND-CDE = 'W41'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1G") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1S") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1 "))) //Natural: IF TIAA-CMPNY-FUND-CDE = 'T1G' OR TIAA-CMPNY-FUND-CDE = 'T1S' OR TIAA-CMPNY-FUND-CDE = 'T1 '
            {
                iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.setValue("T1");                                                                                                    //Natural: MOVE 'T1' TO TIAA-CMPNY-FUND-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Fund_Id.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);                                                                                            //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #WORK-FUND-ID
            pnd_Work_Alpha.setValue(pnd_Work_Fund_Id_Pnd_Work_Fund_Num, MoveOption.RightJustified);                                                                       //Natural: MOVE RIGHT #WORK-FUND-NUM TO #WORK-ALPHA
            pnd_Work_Num.compute(new ComputeParameters(false, pnd_Work_Num), pnd_Work_Alpha.val());                                                                       //Natural: ASSIGN #WORK-NUM := VAL ( #WORK-ALPHA )
                                                                                                                                                                          //Natural: PERFORM SEARCH-FUND-TABLE
            sub_Search_Fund_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Fnd_Found.getBoolean()))                                                                                                                    //Natural: IF #FND-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "TIAA FUND NOT FOUND ON EXT FILE ",iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);                                                         //Natural: WRITE 'TIAA FUND NOT FOUND ON EXT FILE ' TIAA-CMPNY-FUND-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PROCESS-TIAA-FUNDS
    }
    private void sub_Process_Cref_Funds() throws Exception                                                                                                                //Natural: PROCESS-CREF-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cref_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #HOLD-KEY
        (
        "READ08",
        new Wc[] { new Wc("B8", ">=", pnd_Hold_Key, WcType.BY) },
        new Oc[] { new Oc("B8", "ASC") }
        );
        READ08:
        while (condition(vw_iaa_Cref_Fund_Rcrd.readNextRow("READ08")))
        {
            if (condition((iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr.notEquals(pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr)) || (iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde.notEquals(pnd_Hold_Key_Pnd_Hold_Payee_Cde)))) //Natural: IF ( CREF-CNTRCT-PPCN-NBR NE #HOLD-PPCN-NBR ) OR ( CREF-CNTRCT-PAYEE-CDE NE #HOLD-PAYEE-CDE )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde.equals("U41") || iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde.equals("W41")))                                  //Natural: IF CREF-CMPNY-FUND-CDE = 'U41' OR CREF-CMPNY-FUND-CDE = 'W41'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Fund_Id.setValue(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);                                                                                            //Natural: MOVE CREF-CMPNY-FUND-CDE TO #WORK-FUND-ID
            pnd_Work_Alpha.setValue(pnd_Work_Fund_Id_Pnd_Work_Fund_Num, MoveOption.RightJustified);                                                                       //Natural: MOVE RIGHT #WORK-FUND-NUM TO #WORK-ALPHA
            pnd_Work_Num.compute(new ComputeParameters(false, pnd_Work_Num), pnd_Work_Alpha.val());                                                                       //Natural: ASSIGN #WORK-NUM := VAL ( #WORK-ALPHA )
                                                                                                                                                                          //Natural: PERFORM SEARCH-FUND-TABLE
            sub_Search_Fund_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Fnd_Found.getBoolean()))                                                                                                                    //Natural: IF #FND-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "CREF FUND NOT FOUND ON EXT FILE ",iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);                                                         //Natural: WRITE 'CREF FUND NOT FOUND ON EXT FILE ' CREF-CMPNY-FUND-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  PROCESS-CREF-FUNDS
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  VE PLAN-NMBR                    TO #PLAN-NUM-OUT
        pnd_Contract_Record_Out_Pnd_Plan_Num_Out.setValue("      ");                                                                                                      //Natural: MOVE '      ' TO #PLAN-NUM-OUT
        //*  IF PRTCPNT-TAX-ID-NBR = 0 OR PRTCPNT-TAX-ID-NBR = 999999999
        //*    MOVE ' '                       TO #SSN-WORK-ALPHA
        //*  ELSE
        pnd_Ssn_Work_Num.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                                                //Natural: MOVE CPR.PRTCPNT-TAX-ID-NBR TO #SSN-WORK-NUM
        //*   TRANS-CDE = 051 OR 5**
        if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(51) || iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.greater(499)))                               //Natural: IF TRANS-CDE = 051 OR TRANS-CDE = 50 OR TRANS-CDE > 499
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.equals(work_Fields_In_Pnd_Work_Contract_In) && pnd_Hold_Key_Pnd_Hold_Payee_Cde.equals(work_Fields_In_Pnd_Work_Payee_Cde_In)  //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = #WORK-CONTRACT-IN AND #HOLD-PAYEE-CDE = #WORK-PAYEE-CDE-IN AND IAA-TRANS-RCRD.TRANS-DTE = #WORK-TRANS-DTE
                && iaa_Trans_Rcrd_Trans_Dte.equals(work_Fields_In_Pnd_Work_Trans_Dte)))
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                          //Natural: ESCAPE BOTTOM
                if (true) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                work_Fields_In_Pnd_Work_Contract_In.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                 //Natural: MOVE IAA-CNTRCT.CNTRCT-PPCN-NBR TO #WORK-CONTRACT-IN
                work_Fields_In_Pnd_Work_Payee_Cde_In.setValue(pnd_Hold_Key_Pnd_Hold_Payee_Cde);                                                                           //Natural: MOVE #HOLD-PAYEE-CDE TO #WORK-PAYEE-CDE-IN
                work_Fields_In_Pnd_Work_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                     //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #WORK-TRANS-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract_Record_Out_Pnd_Ssn_Out.setValue(pnd_Ssn_Work_Num);                                                                                                   //Natural: MOVE #SSN-WORK-NUM TO #SSN-OUT
        //*  MOVE #WORK-FUND-NUM               TO #FUND-ID-OUT
        //*  MOVE #WORK-FUND-ID                TO #FUND-ID-OUT-WORK
        pnd_Contract_Record_Out_Pnd_Trans_Dte_Out.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                     //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #TRANS-DTE-OUT
        pnd_Contract_Record_Out_Pnd_Eff_Date_Out.setValue(iaa_Trans_Rcrd_Trans_Todays_Dte);                                                                               //Natural: MOVE TRANS-TODAYS-DTE TO #EFF-DATE-OUT
        pnd_Contract_Record_Out_Pnd_Inverse_Date_Out.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                           //Natural: MOVE IAA-TRANS-RCRD.INVRSE-TRANS-DTE TO #INVERSE-DATE-OUT
        pnd_Contract_Record_Out_Pnd_Contract_Out.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                    //Natural: MOVE IAA-CNTRCT.CNTRCT-PPCN-NBR TO #CONTRACT-OUT
        pnd_Contract_Record_Out_Pnd_Payee_Cde_Out.setValue(pnd_Hold_Key_Pnd_Hold_Payee_Cde);                                                                              //Natural: MOVE #HOLD-PAYEE-CDE TO #PAYEE-CDE-OUT
        pnd_Contract_Record_Out_Pnd_Cusip_Out.setValue(pnd_Work_Cusip);                                                                                                   //Natural: MOVE #WORK-CUSIP TO #CUSIP-OUT
        pnd_Contract_Record_Out_Pnd_Plan_Type_Out.setValue("9");                                                                                                          //Natural: MOVE '9' TO #PLAN-TYPE-OUT
        pnd_Contract_Record_Out_Pnd_Source_App_Id_Out.setValue("IA");                                                                                                     //Natural: MOVE 'IA' TO #SOURCE-APP-ID-OUT
        pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out.setValue(iaa_Cntrct_Cntrct_Orgn_Cde);                                                                                //Natural: MOVE IAA-CNTRCT.CNTRCT-ORGN-CDE TO #IA-ORIG-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out.setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                                 //Natural: MOVE IAA-CNTRCT.CNTRCT-OPTN-CDE TO #IA-OPT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Trans_Code_Out.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                    //Natural: MOVE TRANS-CDE TO #TRANS-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Act_Code_Out.setValue(cpr_Cntrct_Actvty_Cde);                                                                                         //Natural: MOVE CPR.CNTRCT-ACTVTY-CDE TO #ACT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Trn_Act_Code_Out.setValue(iaa_Trans_Rcrd_Trans_Actvty_Cde);                                                                           //Natural: MOVE TRANS-ACTVTY-CDE TO #TRN-ACT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Pend_Code_Out.setValue(cpr_Cntrct_Pend_Cde);                                                                                          //Natural: MOVE CPR.CNTRCT-PEND-CDE TO #PEND-CODE-OUT
        pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out.setValue(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);                                                                     //Natural: MOVE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE TO #FIRST-ANN-DOD-OUT
        pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                                     //Natural: MOVE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE TO #SECOND-ANN-DOD-OUT
        pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out.setValue(iaa_Trans_Rcrd_Trans_Sub_Cde);                                                                            //Natural: MOVE TRANS-SUB-CDE TO #TRANS-SUB-CODE-OUT
        //*  2/2013 CHANGES - START
        if (condition(iaa_Cntrct_Cntrct_Orgn_Cde.equals(37) || iaa_Cntrct_Cntrct_Orgn_Cde.equals(38) || iaa_Cntrct_Cntrct_Orgn_Cde.equals(40) || iaa_Cntrct_Cntrct_Orgn_Cde.equals(43))) //Natural: IF IAA-CNTRCT.CNTRCT-ORGN-CDE = 37 OR = 38 OR = 40 OR = 43
        {
            getWorkFiles().write(3, false, pnd_Contract_Record_Out);                                                                                                      //Natural: WRITE WORK FILE 3 #CONTRACT-RECORD-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(1, false, pnd_Contract_Record_Out);                                                                                                      //Natural: WRITE WORK FILE 1 #CONTRACT-RECORD-OUT
        }                                                                                                                                                                 //Natural: END-IF
        //*  2/2013 CHANGES - END
        //*   WRITE-RECORD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "TRANS CHECK DATE NOT FOUND");
    }
}
