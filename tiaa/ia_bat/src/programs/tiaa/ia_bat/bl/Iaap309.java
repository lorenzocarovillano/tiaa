/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:18 PM
**        * FROM NATURAL PROGRAM : Iaap309
************************************************************
**        * FILE NAME            : Iaap309.java
**        * CLASS NAME           : Iaap309
**        * INSTANCE NAME        : Iaap309
************************************************************
**********************************************************************
*                                                                    *
*                                                                    *
*                                                                    *
*                                                                    *
*   PROGRAM    -   IAAP309                                           *
*   (OLD NAME  -   IAARPT90)      PROGRAM CALCULATES AND PRINTS      *
*                                 IA BIRTHDAY LIST REPORT FOR        *
*                                 CHECK DATE DUE                     *
*      DATE    -   12/95                                             *
*                                                                    *
*   HISTORY                                                          *
*    6/00         LOGIC TO HANDLE SELECTING CORRECT ANNUITANT FOR
*                 BIRTHDAY LIST                                      *
*    9/12         EXPANDED UNITS-CNT TO 6.3                          *
*   05/17  OS     PIN EXPANSION CHANGES MARKED 082017.               *
*   10/20  JT     REMOVED DUPLICATES. SC 202010                      *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap309 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_250;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Check_Dte;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Check_Dte1;
    private DbsField pnd_Input_Pnd_Check_Dte2;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_Cntrct_Inst_Iss_Cde;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_Cpr_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Cpr_Status_Cde;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_51;

    private DbsGroup pnd_Name;
    private DbsField pnd_Name_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee;

    private DbsGroup pnd_Name__R_Field_8;
    private DbsField pnd_Name_Cntrct_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name__R_Field_9;
    private DbsField pnd_Name_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Addrss_Lne_1_K;
    private DbsField pnd_Name_Addrss_Lne_2_K;
    private DbsField pnd_Name_Addrss_Lne_3_K;
    private DbsField pnd_Name_Addrss_Lne_4_K;
    private DbsField pnd_Name_Addrss_Lne_5_K;
    private DbsField pnd_Name_Addrss_Lne_6_K;
    private DbsField pnd_Name_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name__R_Field_10;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_K;

    private DbsGroup pnd_Name__R_Field_11;
    private DbsField pnd_Name_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_Eft_Status_Ind_K;
    private DbsField pnd_Name_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Intl_Bank_Pymnt_Acct_Mnbr;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name__R_Field_12;
    private DbsField pnd_Name_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Addrss_Lne_1_R;
    private DbsField pnd_Name_Addrss_Lne_2_R;
    private DbsField pnd_Name_Addrss_Lne_3_R;
    private DbsField pnd_Name_Addrss_Lne_4_R;
    private DbsField pnd_Name_Addrss_Lne_5_R;
    private DbsField pnd_Name_Addrss_Lne_6_R;
    private DbsField pnd_Name_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name__R_Field_13;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_R;

    private DbsGroup pnd_Name__R_Field_14;
    private DbsField pnd_Name_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_Eft_Status_Ind_R;
    private DbsField pnd_Name_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name__R_Field_15;
    private DbsField pnd_Name_V_Cntrct_Name_Free_K;
    private DbsField pnd_Name_V_Addrss_Lne_1_K;
    private DbsField pnd_Name_V_Addrss_Lne_2_K;
    private DbsField pnd_Name_V_Addrss_Lne_3_K;
    private DbsField pnd_Name_V_Addrss_Lne_4_K;
    private DbsField pnd_Name_V_Addrss_Lne_5_K;
    private DbsField pnd_Name_V_Addrss_Lne_6_K;
    private DbsField pnd_Name_V_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name__R_Field_16;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_K;

    private DbsGroup pnd_Name__R_Field_17;
    private DbsField pnd_Name_V_Addrss_Type_Cde_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_V_Eft_Status_Ind_K;
    private DbsField pnd_Name_V_Checking_Saving_Cde_K;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_V_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_V_Intl_Bank_Pymnt_Acct_Mnbr;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name__R_Field_18;
    private DbsField pnd_Name_V_Cntrct_Name_Free_R;
    private DbsField pnd_Name_V_Addrss_Lne_1_R;
    private DbsField pnd_Name_V_Addrss_Lne_2_R;
    private DbsField pnd_Name_V_Addrss_Lne_3_R;
    private DbsField pnd_Name_V_Addrss_Lne_4_R;
    private DbsField pnd_Name_V_Addrss_Lne_5_R;
    private DbsField pnd_Name_V_Addrss_Lne_6_R;
    private DbsField pnd_Name_V_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name__R_Field_19;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_R;

    private DbsGroup pnd_Name__R_Field_20;
    private DbsField pnd_Name_V_Addrss_Type_Cde_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_V_Eft_Status_Ind_R;
    private DbsField pnd_Name_V_Checking_Saving_Cde_R;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Name_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Global_Indicator;
    private DbsField pnd_Name_Global_Country;
    private DbsField pnd_Name_Legal_State;

    private DbsGroup pnd_Extr_Rec_Out_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Ss_Nbr_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Name_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F2;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Dob_Dte;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F3;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Age;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F3a;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Prt_Sex_Cde_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F4;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr1_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F5;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr2_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F6;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr3_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F7;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr4_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F8;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr5_1;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_F9;
    private DbsField pnd_Extr_Rec_Out_1_Pnd_Extr_Adr6_1;

    private DbsGroup pnd_Extr_Rec_Out;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cont_No;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_21;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_22;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Name;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr1;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr2;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr3;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr4;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr5;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr6;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Zip;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_23;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Bypass;
    private DbsField pnd_Name_Eof;
    private DbsField pnd_New_Payee;
    private DbsField pnd_Log_Dte;
    private DbsField pnd_Save_Clg_Cde;
    private DbsField pnd_Tiaa_Cref_Fld;
    private DbsField pnd_Prt_Sex_Cde;
    private DbsField pnd_Save_Status_Cde;
    private DbsField pnd_Save_Cntrct_Payee;

    private DbsGroup pnd_Save_Cntrct_Payee__R_Field_24;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr;

    private DbsGroup pnd_Save_Cntrct_Payee__R_Field_25;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr1;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr2;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Flr1;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde;
    private DbsField pnd_Save_Cntrct_Record_Code;
    private DbsField pnd_Save_Cntrct_Optn_Cde;
    private DbsField pnd_Save_Cntrct_Orgn_Cde;
    private DbsField pnd_Save_Cntrct_Crrncy_Cde;
    private DbsField pnd_Save_Cntrct_First_Annt_Dob_Dte;

    private DbsGroup pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26;
    private DbsField pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy;
    private DbsField pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm;
    private DbsField pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Dd;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Dob_Dte;

    private DbsGroup pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Dd;
    private DbsField pnd_Prt_Cntrct_Annt_Dob_Dte;

    private DbsGroup pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28;
    private DbsField pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Ccyy;
    private DbsField pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Mm;
    private DbsField pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Dd;
    private DbsField pnd_Save_Cntrct_First_Annt_Sex_Cde;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Save_Cntrct_First_Annt_Dod_Dte;
    private DbsField pnd_Save_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Save_Age_Calc;
    private DbsField pnd_Save_Extr_Name;
    private DbsField pnd_Save_Extr_Adr1;
    private DbsField pnd_Save_Extr_Adr2;
    private DbsField pnd_Save_Extr_Adr3;
    private DbsField pnd_Save_Extr_Adr4;
    private DbsField pnd_Save_Extr_Adr5;
    private DbsField pnd_Save_Extr_Adr6;
    private DbsField pnd_Save_Extr_Zip;
    private DbsField pnd_Save_Tax_Id_Nbr;

    private DbsGroup pnd_Save_Tax_Id_Nbr__R_Field_29;
    private DbsField pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N1;
    private DbsField pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N2;
    private DbsField pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N3;
    private DbsField pnd_Parm_Check_Dte_A;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_30;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_31;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Yyyy;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_32;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Cc;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Yy;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Mm;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Dd;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_33;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm;
    private DbsField pnd_Save_Dte_A;

    private DbsGroup pnd_Save_Dte_A__R_Field_34;
    private DbsField pnd_Save_Dte_A_Pnd_Save_Dte;

    private DbsGroup pnd_Save_Dte_A__R_Field_35;
    private DbsField pnd_Save_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Dte_A__R_Field_36;
    private DbsField pnd_Save_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Dte_A_Pnd_Mm;
    private DbsField pnd_Save_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Dte_A__R_Field_37;
    private DbsField pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm;
    private DbsField pnd_Date90;

    private DbsGroup pnd_Date90__R_Field_38;
    private DbsField pnd_Date90_Pnd_Date90_Ccyy;

    private DbsGroup pnd_Date90__R_Field_39;
    private DbsField pnd_Date90_Pnd_Date90_Cc;
    private DbsField pnd_Date90_Pnd_Date90_Yy;
    private DbsField pnd_Date90_Pnd_Date90_Mm;
    private DbsField pnd_Date95;

    private DbsGroup pnd_Date95__R_Field_40;
    private DbsField pnd_Date95_Pnd_Date95_Ccyy;

    private DbsGroup pnd_Date95__R_Field_41;
    private DbsField pnd_Date95_Pnd_Date95_Cc;
    private DbsField pnd_Date95_Pnd_Date95_Yy;
    private DbsField pnd_Date95_Pnd_Date95_Mm;
    private DbsField pnd_Date100;

    private DbsGroup pnd_Date100__R_Field_42;
    private DbsField pnd_Date100_Pnd_Date100_Ccyy;

    private DbsGroup pnd_Date100__R_Field_43;
    private DbsField pnd_Date100_Pnd_Date100_Cc;
    private DbsField pnd_Date100_Pnd_Date100_Yy;
    private DbsField pnd_Date100_Pnd_Date100_Mm;
    private DbsField pnd_Print_Dte;

    private DbsGroup pnd_Print_Dte__R_Field_44;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Mm;
    private DbsField pnd_Print_Dte_Pnd_Flr1;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Dd;
    private DbsField pnd_Print_Dte_Pnd_Flr2;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Yy;
    private DbsField pnd_Print_Tax_Id_Nbr;

    private DbsGroup pnd_Print_Tax_Id_Nbr__R_Field_45;
    private DbsField pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_1;
    private DbsField pnd_Print_Tax_Id_Nbr_Pnd_Flr3;
    private DbsField pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_2;
    private DbsField pnd_Print_Tax_Id_Nbr_Pnd_Flr4;
    private DbsField pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_3;
    private DbsField pnd_Print_Ppcn_Nbr;

    private DbsGroup pnd_Print_Ppcn_Nbr__R_Field_46;
    private DbsField pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_1;
    private DbsField pnd_Print_Ppcn_Nbr_Pnd_Flr5;
    private DbsField pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_2;
    private DbsField pnd_Ctr_Clg;
    private DbsField pnd_Ctr_Sort;
    private DbsField pnd_Ctr_Read;
    private DbsField pnd_Ctr_Extr;
    private DbsField pnd_Name_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Bypass_Status;
    private DbsField pnd_Bypass_Payee;
    private DbsField pnd_Ctr_Coll_Cde;
    private DbsField pnd_Sel_Sw;
    private DbsField pnd_Sel_Move;
    private DbsField pnd_Sel_Age;
    private DbsField pnd_Prt_Ssns;
    private DbsField pnd_Prt_Cnt;
    private DbsField pnd_Extr_Dob_Dte_1;

    private DbsGroup pnd_Extr_Dob_Dte_1__R_Field_47;
    private DbsField pnd_Extr_Dob_Dte_1_Pnd_Dob_Yyyy;
    private DbsField pnd_Extr_Dob_Dte_1_Pnd_Dob_Mm;
    private DbsField pnd_Extr_Dob_Dte_1_Pnd_Dob_Dd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_250 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_250", "#REST-OF-RECORD-250", FieldType.STRING, 250);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Check_Dte = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_2.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Check_Dte);
        pnd_Input_Pnd_Check_Dte1 = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Check_Dte1", "#CHECK-DTE1", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Check_Dte2 = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Check_Dte2", "#CHECK-DTE2", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Cntrct_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input__Filler1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 22);
        pnd_Input_Pnd_Cntrct_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input__Filler2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 24);
        pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte", "#CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input__Filler3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 4);
        pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde", "#CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte", "#CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 9);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte", "#CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input__Filler6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 4);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde", "#CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte", "#CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 16);
        pnd_Input_Pnd_Cntrct_Inst_Iss_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Inst_Iss_Cde", "#CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input__Filler8 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 26);
        pnd_Input_Pnd_Cpr_Tax_Id_Nbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Tax_Id_Nbr", "#CPR-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler9 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 1);
        pnd_Input_Pnd_Cpr_Status_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Status_Cde", "#CPR-STATUS-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_6 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input__Filler10 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 23);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 5);
        pnd_Input__Filler11 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 16);
        pnd_Input_Pnd_Ddctn_Strt_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_7 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input__Filler12 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 10);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler13 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Rest_Of_Record_51 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_51", "#REST-OF-RECORD-51", FieldType.STRING, 51);

        pnd_Name = localVariables.newGroupInRecord("pnd_Name", "#NAME");
        pnd_Name_Ph_Unque_Id_Nmbr = pnd_Name.newFieldInGroup("pnd_Name_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Cntrct_Payee = pnd_Name.newFieldInGroup("pnd_Name_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name__R_Field_8 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_8", "REDEFINE", pnd_Name_Cntrct_Payee);
        pnd_Name_Cntrct_Nmbr = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Cntrct_Payee_Cde = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Name_Pnd_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);

        pnd_Name__R_Field_9 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_9", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Cntrct_Name_Free_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);

        pnd_Name__R_Field_10 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_10", "REDEFINE", pnd_Name_Addrss_Postal_Data_K);
        pnd_Name_Addrss_Zip_Plus_4_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);

        pnd_Name__R_Field_11 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_11", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_K);
        pnd_Name_Addrss_Type_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Mailing_Addrss_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 
            2);
        pnd_Name_Intl_Eft_Pay_Type_Cde = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Name_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Iintl_Bank_Pymnt_Eft_Nmbr", "IINTL-BANK-PYMNT-EFT-NMBR", FieldType.STRING, 
            35);
        pnd_Name_Intl_Bank_Pymnt_Acct_Mnbr = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Intl_Bank_Pymnt_Acct_Mnbr", "INTL-BANK-PYMNT-ACCT-MNBR", FieldType.STRING, 
            35);
        pnd_Name_Pnd_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);

        pnd_Name__R_Field_12 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_12", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Cntrct_Name_Free_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);

        pnd_Name__R_Field_13 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_13", "REDEFINE", pnd_Name_Addrss_Postal_Data_R);
        pnd_Name_Addrss_Zip_Plus_4_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name__R_Field_14 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_14", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_R);
        pnd_Name_Addrss_Type_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Correspondence_Addrss_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 
            2);
        pnd_Name_Pnd_V_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 245);

        pnd_Name__R_Field_15 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_15", "REDEFINE", pnd_Name_Pnd_V_Cntrct_Name_Add_K);
        pnd_Name_V_Cntrct_Name_Free_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_1_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_2_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_3_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_4_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_5_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_6_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);

        pnd_Name__R_Field_16 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_16", "REDEFINE", pnd_Name_V_Addrss_Postal_Data_K);
        pnd_Name_V_Addrss_Zip_Plus_4_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Name_V_Addrss_Carrier_Rte_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Walk_Rte_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", FieldType.STRING, 4);
        pnd_Name_V_Addrss_Usps_Future_Use_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 148);

        pnd_Name__R_Field_17 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_17", "REDEFINE", pnd_Name_Pnd_V_Rest_Of_Record_K);
        pnd_Name_V_Addrss_Type_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", FieldType.STRING, 1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 
            8);
        pnd_Name_V_Addrss_Last_Chnge_Time_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_V_Eft_Status_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", FieldType.STRING, 1);
        pnd_Name_V_Checking_Saving_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_V_Pending_Addrss_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Addrss_Chnge_Dte_K", "V-PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Addrss_Restore_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Addrss_Restore_Dte_K", "V-PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K", "V-PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Check_Mailing_Addrss_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Check_Mailing_Addrss_Ind_K", "V-CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 
            2);
        pnd_Name_V_Intl_Eft_Pay_Type_Cde = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Iintl_Bank_Pymnt_Eft_Nmbr", "V-IINTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_V_Intl_Bank_Pymnt_Acct_Mnbr = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Intl_Bank_Pymnt_Acct_Mnbr", "V-INTL-BANK-PYMNT-ACCT-MNBR", 
            FieldType.STRING, 35);
        pnd_Name_Pnd_V_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 245);

        pnd_Name__R_Field_18 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_18", "REDEFINE", pnd_Name_Pnd_V_Cntrct_Name_Add_R);
        pnd_Name_V_Cntrct_Name_Free_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_1_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_2_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_3_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_4_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_5_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_6_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);

        pnd_Name__R_Field_19 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_19", "REDEFINE", pnd_Name_V_Addrss_Postal_Data_R);
        pnd_Name_V_Addrss_Zip_Plus_4_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Name_V_Addrss_Carrier_Rte_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Walk_Rte_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", FieldType.STRING, 4);
        pnd_Name_V_Addrss_Usps_Future_Use_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name__R_Field_20 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_20", "REDEFINE", pnd_Name_Pnd_V_Rest_Of_Record_R);
        pnd_Name_V_Addrss_Type_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", FieldType.STRING, 1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 
            8);
        pnd_Name_V_Addrss_Last_Chnge_Time_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_V_Eft_Status_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", FieldType.STRING, 1);
        pnd_Name_V_Checking_Saving_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_V_Pending_Addrss_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Addrss_Chnge_Dte_R", "V-PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Addrss_Restore_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Addrss_Restore_Dte_R", "V-PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R", "V-PENDING-PERM-ADD-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Correspondence_Addrss_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Correspondence_Addrss_Ind_R", "V-CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 
            2);
        pnd_Name_Global_Indicator = pnd_Name.newFieldInGroup("pnd_Name_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Name_Global_Country = pnd_Name.newFieldInGroup("pnd_Name_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Name_Legal_State = pnd_Name.newFieldInGroup("pnd_Name_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        pnd_Extr_Rec_Out_1 = localVariables.newGroupInRecord("pnd_Extr_Rec_Out_1", "#EXTR-REC-OUT-1");
        pnd_Extr_Rec_Out_1_Pnd_Extr_Ss_Nbr_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Ss_Nbr_1", "#EXTR-SS-NBR-1", FieldType.NUMERIC, 
            9);
        pnd_Extr_Rec_Out_1_Pnd_F1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Name_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Name_1", "#EXTR-NAME-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F2 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Dob_Dte = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Dob_Dte", "#DOB-DTE", FieldType.STRING, 10);
        pnd_Extr_Rec_Out_1_Pnd_F3 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Age = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Age", "#EXTR-AGE", FieldType.NUMERIC, 4);
        pnd_Extr_Rec_Out_1_Pnd_F3a = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F3a", "#F3A", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Prt_Sex_Cde_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Prt_Sex_Cde_1", "#PRT-SEX-CDE-1", FieldType.STRING, 
            1);
        pnd_Extr_Rec_Out_1_Pnd_F4 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr1_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr1_1", "#EXTR-ADR1-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F5 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr2_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr2_1", "#EXTR-ADR2-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F6 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr3_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr3_1", "#EXTR-ADR3-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F7 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F7", "#F7", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr4_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr4_1", "#EXTR-ADR4-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F8 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F8", "#F8", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr5_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr5_1", "#EXTR-ADR5-1", FieldType.STRING, 
            35);
        pnd_Extr_Rec_Out_1_Pnd_F9 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_F9", "#F9", FieldType.STRING, 1);
        pnd_Extr_Rec_Out_1_Pnd_Extr_Adr6_1 = pnd_Extr_Rec_Out_1.newFieldInGroup("pnd_Extr_Rec_Out_1_Pnd_Extr_Adr6_1", "#EXTR-ADR6-1", FieldType.STRING, 
            35);

        pnd_Extr_Rec_Out = localVariables.newGroupInRecord("pnd_Extr_Rec_Out", "#EXTR-REC-OUT");
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cont_No", "#EXTR-CONT-NO", FieldType.STRING, 12);

        pnd_Extr_Rec_Out__R_Field_21 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_21", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Cont_No);
        pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr = pnd_Extr_Rec_Out__R_Field_21.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr", "#EXTR-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde = pnd_Extr_Rec_Out__R_Field_21.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde", "#EXTR-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Extr_Rec_Out__R_Field_22 = pnd_Extr_Rec_Out__R_Field_21.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_22", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde);
        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A = pnd_Extr_Rec_Out__R_Field_22.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A", "#EXTR-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte", "#EXTR-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc", "#EXTR-AGE-CALC", FieldType.NUMERIC, 
            4);
        pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde", "#EXTR-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde", "#EXTR-CLG-CDE", FieldType.STRING, 5);
        pnd_Extr_Rec_Out_Pnd_Extr_Name = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Name", "#EXTR-NAME", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr1 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr1", "#EXTR-ADR1", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr2 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr2", "#EXTR-ADR2", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr3 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr3", "#EXTR-ADR3", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr4 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr4", "#EXTR-ADR4", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr5 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr5", "#EXTR-ADR5", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr6 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr6", "#EXTR-ADR6", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Zip = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Zip", "#EXTR-ZIP", FieldType.STRING, 9);
        pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr", "#EXTR-SS-NBR", FieldType.NUMERIC, 9);
        pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind", "#EXTR-INST-IND", FieldType.STRING, 
            1);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_23", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_23.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_23.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Name_Eof = localVariables.newFieldInRecord("pnd_Name_Eof", "#NAME-EOF", FieldType.BOOLEAN, 1);
        pnd_New_Payee = localVariables.newFieldInRecord("pnd_New_Payee", "#NEW-PAYEE", FieldType.BOOLEAN, 1);
        pnd_Log_Dte = localVariables.newFieldInRecord("pnd_Log_Dte", "#LOG-DTE", FieldType.STRING, 15);
        pnd_Save_Clg_Cde = localVariables.newFieldInRecord("pnd_Save_Clg_Cde", "#SAVE-CLG-CDE", FieldType.STRING, 5);
        pnd_Tiaa_Cref_Fld = localVariables.newFieldInRecord("pnd_Tiaa_Cref_Fld", "#TIAA-CREF-FLD", FieldType.STRING, 13);
        pnd_Prt_Sex_Cde = localVariables.newFieldInRecord("pnd_Prt_Sex_Cde", "#PRT-SEX-CDE", FieldType.STRING, 1);
        pnd_Save_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Status_Cde", "#SAVE-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Save_Cntrct_Payee", "#SAVE-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Save_Cntrct_Payee__R_Field_24 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Payee__R_Field_24", "REDEFINE", pnd_Save_Cntrct_Payee);
        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr = pnd_Save_Cntrct_Payee__R_Field_24.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", 
            FieldType.STRING, 10);

        pnd_Save_Cntrct_Payee__R_Field_25 = pnd_Save_Cntrct_Payee__R_Field_24.newGroupInGroup("pnd_Save_Cntrct_Payee__R_Field_25", "REDEFINE", pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr);
        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr1 = pnd_Save_Cntrct_Payee__R_Field_25.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr1", "#SAVE-PPCN-NBR1", 
            FieldType.NUMERIC, 7);
        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr2 = pnd_Save_Cntrct_Payee__R_Field_25.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr2", "#SAVE-PPCN-NBR2", 
            FieldType.NUMERIC, 1);
        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Flr1 = pnd_Save_Cntrct_Payee__R_Field_25.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Flr1", "#SAVE-PPCN-FLR1", 
            FieldType.STRING, 2);
        pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde = pnd_Save_Cntrct_Payee__R_Field_24.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Record_Code = localVariables.newFieldInRecord("pnd_Save_Cntrct_Record_Code", "#SAVE-CNTRCT-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Optn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Optn_Cde", "#SAVE-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Orgn_Cde", "#SAVE-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Save_Cntrct_Crrncy_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Crrncy_Cde", "#SAVE-CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Cntrct_First_Annt_Dob_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_First_Annt_Dob_Dte", "#SAVE-CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26 = localVariables.newGroupInRecord("pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26", "REDEFINE", 
            pnd_Save_Cntrct_First_Annt_Dob_Dte);
        pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy = pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26.newFieldInGroup("pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy", 
            "#F-CCYY", FieldType.NUMERIC, 4);
        pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm = pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26.newFieldInGroup("pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm", 
            "#F-MM", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Dd = pnd_Save_Cntrct_First_Annt_Dob_Dte__R_Field_26.newFieldInGroup("pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Dd", 
            "#F-DD", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_Scnd_Annt_Dob_Dte", "#SAVE-CNTRCT-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27", "REDEFINE", pnd_Save_Cntrct_Scnd_Annt_Dob_Dte);
        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy = pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27.newFieldInGroup("pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy", 
            "#S-CCYY", FieldType.NUMERIC, 4);
        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm = pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27.newFieldInGroup("pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm", 
            "#S-MM", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Dd = pnd_Save_Cntrct_Scnd_Annt_Dob_Dte__R_Field_27.newFieldInGroup("pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Dd", 
            "#S-DD", FieldType.NUMERIC, 2);
        pnd_Prt_Cntrct_Annt_Dob_Dte = localVariables.newFieldInRecord("pnd_Prt_Cntrct_Annt_Dob_Dte", "#PRT-CNTRCT-ANNT-DOB-DTE", FieldType.NUMERIC, 8);

        pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28 = localVariables.newGroupInRecord("pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28", "REDEFINE", pnd_Prt_Cntrct_Annt_Dob_Dte);
        pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Ccyy = pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28.newFieldInGroup("pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Ccyy", 
            "#PRT-CCYY", FieldType.NUMERIC, 4);
        pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Mm = pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28.newFieldInGroup("pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Mm", "#PRT-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Dd = pnd_Prt_Cntrct_Annt_Dob_Dte__R_Field_28.newFieldInGroup("pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Dd", "#PRT-DD", 
            FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_First_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_First_Annt_Sex_Cde", "#SAVE-CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Save_Cntrct_Scnd_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Scnd_Annt_Sex_Cde", "#SAVE-CNTRCT-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Save_Cntrct_First_Annt_Dod_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_First_Annt_Dod_Dte", "#SAVE-CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Cntrct_Scnd_Annt_Dod_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_Scnd_Annt_Dod_Dte", "#SAVE-CNTRCT-SCND-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Save_Age_Calc = localVariables.newFieldInRecord("pnd_Save_Age_Calc", "#SAVE-AGE-CALC", FieldType.NUMERIC, 4);
        pnd_Save_Extr_Name = localVariables.newFieldInRecord("pnd_Save_Extr_Name", "#SAVE-EXTR-NAME", FieldType.STRING, 35);
        pnd_Save_Extr_Adr1 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr1", "#SAVE-EXTR-ADR1", FieldType.STRING, 35);
        pnd_Save_Extr_Adr2 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr2", "#SAVE-EXTR-ADR2", FieldType.STRING, 35);
        pnd_Save_Extr_Adr3 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr3", "#SAVE-EXTR-ADR3", FieldType.STRING, 35);
        pnd_Save_Extr_Adr4 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr4", "#SAVE-EXTR-ADR4", FieldType.STRING, 35);
        pnd_Save_Extr_Adr5 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr5", "#SAVE-EXTR-ADR5", FieldType.STRING, 35);
        pnd_Save_Extr_Adr6 = localVariables.newFieldInRecord("pnd_Save_Extr_Adr6", "#SAVE-EXTR-ADR6", FieldType.STRING, 35);
        pnd_Save_Extr_Zip = localVariables.newFieldInRecord("pnd_Save_Extr_Zip", "#SAVE-EXTR-ZIP", FieldType.STRING, 9);
        pnd_Save_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_Save_Tax_Id_Nbr", "#SAVE-TAX-ID-NBR", FieldType.NUMERIC, 9);

        pnd_Save_Tax_Id_Nbr__R_Field_29 = localVariables.newGroupInRecord("pnd_Save_Tax_Id_Nbr__R_Field_29", "REDEFINE", pnd_Save_Tax_Id_Nbr);
        pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N1 = pnd_Save_Tax_Id_Nbr__R_Field_29.newFieldInGroup("pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N1", "#SAVE-TAX-N1", FieldType.NUMERIC, 
            3);
        pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N2 = pnd_Save_Tax_Id_Nbr__R_Field_29.newFieldInGroup("pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N2", "#SAVE-TAX-N2", FieldType.NUMERIC, 
            2);
        pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N3 = pnd_Save_Tax_Id_Nbr__R_Field_29.newFieldInGroup("pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N3", "#SAVE-TAX-N3", FieldType.NUMERIC, 
            4);
        pnd_Parm_Check_Dte_A = localVariables.newFieldInRecord("pnd_Parm_Check_Dte_A", "#PARM-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Parm_Check_Dte_A__R_Field_30 = localVariables.newGroupInRecord("pnd_Parm_Check_Dte_A__R_Field_30", "REDEFINE", pnd_Parm_Check_Dte_A);
        pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte = pnd_Parm_Check_Dte_A__R_Field_30.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Parm_Check_Dte_A__R_Field_31 = pnd_Parm_Check_Dte_A__R_Field_30.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_31", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte);
        pnd_Parm_Check_Dte_A_Pnd_P_Yyyy = pnd_Parm_Check_Dte_A__R_Field_31.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Yyyy", "#P-YYYY", FieldType.NUMERIC, 
            4);

        pnd_Parm_Check_Dte_A__R_Field_32 = pnd_Parm_Check_Dte_A__R_Field_31.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_32", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_P_Yyyy);
        pnd_Parm_Check_Dte_A_Pnd_P_Cc = pnd_Parm_Check_Dte_A__R_Field_32.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Cc", "#P-CC", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Yy = pnd_Parm_Check_Dte_A__R_Field_32.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Yy", "#P-YY", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Mm = pnd_Parm_Check_Dte_A__R_Field_31.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Mm", "#P-MM", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Dd = pnd_Parm_Check_Dte_A__R_Field_31.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Dd", "#P-DD", FieldType.NUMERIC, 
            2);

        pnd_Parm_Check_Dte_A__R_Field_33 = pnd_Parm_Check_Dte_A__R_Field_30.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_33", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte);
        pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm = pnd_Parm_Check_Dte_A__R_Field_33.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm", 
            "#PARM-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Save_Dte_A = localVariables.newFieldInRecord("pnd_Save_Dte_A", "#SAVE-DTE-A", FieldType.STRING, 8);

        pnd_Save_Dte_A__R_Field_34 = localVariables.newGroupInRecord("pnd_Save_Dte_A__R_Field_34", "REDEFINE", pnd_Save_Dte_A);
        pnd_Save_Dte_A_Pnd_Save_Dte = pnd_Save_Dte_A__R_Field_34.newFieldInGroup("pnd_Save_Dte_A_Pnd_Save_Dte", "#SAVE-DTE", FieldType.NUMERIC, 8);

        pnd_Save_Dte_A__R_Field_35 = pnd_Save_Dte_A__R_Field_34.newGroupInGroup("pnd_Save_Dte_A__R_Field_35", "REDEFINE", pnd_Save_Dte_A_Pnd_Save_Dte);
        pnd_Save_Dte_A_Pnd_Yyyy = pnd_Save_Dte_A__R_Field_35.newFieldInGroup("pnd_Save_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Save_Dte_A__R_Field_36 = pnd_Save_Dte_A__R_Field_35.newGroupInGroup("pnd_Save_Dte_A__R_Field_36", "REDEFINE", pnd_Save_Dte_A_Pnd_Yyyy);
        pnd_Save_Dte_A_Pnd_Cc = pnd_Save_Dte_A__R_Field_36.newFieldInGroup("pnd_Save_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Dte_A_Pnd_Yy = pnd_Save_Dte_A__R_Field_36.newFieldInGroup("pnd_Save_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Dte_A_Pnd_Mm = pnd_Save_Dte_A__R_Field_35.newFieldInGroup("pnd_Save_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Save_Dte_A_Pnd_Dd = pnd_Save_Dte_A__R_Field_35.newFieldInGroup("pnd_Save_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Dte_A__R_Field_37 = pnd_Save_Dte_A__R_Field_34.newGroupInGroup("pnd_Save_Dte_A__R_Field_37", "REDEFINE", pnd_Save_Dte_A_Pnd_Save_Dte);
        pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm = pnd_Save_Dte_A__R_Field_37.newFieldInGroup("pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm", "#SAVE-DTE-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Date90 = localVariables.newFieldInRecord("pnd_Date90", "#DATE90", FieldType.NUMERIC, 6);

        pnd_Date90__R_Field_38 = localVariables.newGroupInRecord("pnd_Date90__R_Field_38", "REDEFINE", pnd_Date90);
        pnd_Date90_Pnd_Date90_Ccyy = pnd_Date90__R_Field_38.newFieldInGroup("pnd_Date90_Pnd_Date90_Ccyy", "#DATE90-CCYY", FieldType.NUMERIC, 4);

        pnd_Date90__R_Field_39 = localVariables.newGroupInRecord("pnd_Date90__R_Field_39", "REDEFINE", pnd_Date90);
        pnd_Date90_Pnd_Date90_Cc = pnd_Date90__R_Field_39.newFieldInGroup("pnd_Date90_Pnd_Date90_Cc", "#DATE90-CC", FieldType.NUMERIC, 2);
        pnd_Date90_Pnd_Date90_Yy = pnd_Date90__R_Field_39.newFieldInGroup("pnd_Date90_Pnd_Date90_Yy", "#DATE90-YY", FieldType.NUMERIC, 2);
        pnd_Date90_Pnd_Date90_Mm = pnd_Date90__R_Field_39.newFieldInGroup("pnd_Date90_Pnd_Date90_Mm", "#DATE90-MM", FieldType.NUMERIC, 2);
        pnd_Date95 = localVariables.newFieldInRecord("pnd_Date95", "#DATE95", FieldType.NUMERIC, 6);

        pnd_Date95__R_Field_40 = localVariables.newGroupInRecord("pnd_Date95__R_Field_40", "REDEFINE", pnd_Date95);
        pnd_Date95_Pnd_Date95_Ccyy = pnd_Date95__R_Field_40.newFieldInGroup("pnd_Date95_Pnd_Date95_Ccyy", "#DATE95-CCYY", FieldType.NUMERIC, 4);

        pnd_Date95__R_Field_41 = localVariables.newGroupInRecord("pnd_Date95__R_Field_41", "REDEFINE", pnd_Date95);
        pnd_Date95_Pnd_Date95_Cc = pnd_Date95__R_Field_41.newFieldInGroup("pnd_Date95_Pnd_Date95_Cc", "#DATE95-CC", FieldType.NUMERIC, 2);
        pnd_Date95_Pnd_Date95_Yy = pnd_Date95__R_Field_41.newFieldInGroup("pnd_Date95_Pnd_Date95_Yy", "#DATE95-YY", FieldType.NUMERIC, 2);
        pnd_Date95_Pnd_Date95_Mm = pnd_Date95__R_Field_41.newFieldInGroup("pnd_Date95_Pnd_Date95_Mm", "#DATE95-MM", FieldType.NUMERIC, 2);
        pnd_Date100 = localVariables.newFieldInRecord("pnd_Date100", "#DATE100", FieldType.NUMERIC, 6);

        pnd_Date100__R_Field_42 = localVariables.newGroupInRecord("pnd_Date100__R_Field_42", "REDEFINE", pnd_Date100);
        pnd_Date100_Pnd_Date100_Ccyy = pnd_Date100__R_Field_42.newFieldInGroup("pnd_Date100_Pnd_Date100_Ccyy", "#DATE100-CCYY", FieldType.NUMERIC, 4);

        pnd_Date100__R_Field_43 = localVariables.newGroupInRecord("pnd_Date100__R_Field_43", "REDEFINE", pnd_Date100);
        pnd_Date100_Pnd_Date100_Cc = pnd_Date100__R_Field_43.newFieldInGroup("pnd_Date100_Pnd_Date100_Cc", "#DATE100-CC", FieldType.NUMERIC, 2);
        pnd_Date100_Pnd_Date100_Yy = pnd_Date100__R_Field_43.newFieldInGroup("pnd_Date100_Pnd_Date100_Yy", "#DATE100-YY", FieldType.NUMERIC, 2);
        pnd_Date100_Pnd_Date100_Mm = pnd_Date100__R_Field_43.newFieldInGroup("pnd_Date100_Pnd_Date100_Mm", "#DATE100-MM", FieldType.NUMERIC, 2);
        pnd_Print_Dte = localVariables.newFieldInRecord("pnd_Print_Dte", "#PRINT-DTE", FieldType.STRING, 8);

        pnd_Print_Dte__R_Field_44 = localVariables.newGroupInRecord("pnd_Print_Dte__R_Field_44", "REDEFINE", pnd_Print_Dte);
        pnd_Print_Dte_Pnd_Prt_Dte_Mm = pnd_Print_Dte__R_Field_44.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Mm", "#PRT-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Print_Dte_Pnd_Flr1 = pnd_Print_Dte__R_Field_44.newFieldInGroup("pnd_Print_Dte_Pnd_Flr1", "#FLR1", FieldType.STRING, 1);
        pnd_Print_Dte_Pnd_Prt_Dte_Dd = pnd_Print_Dte__R_Field_44.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Dd", "#PRT-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Print_Dte_Pnd_Flr2 = pnd_Print_Dte__R_Field_44.newFieldInGroup("pnd_Print_Dte_Pnd_Flr2", "#FLR2", FieldType.STRING, 1);
        pnd_Print_Dte_Pnd_Prt_Dte_Yy = pnd_Print_Dte__R_Field_44.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Yy", "#PRT-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Print_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_Print_Tax_Id_Nbr", "#PRINT-TAX-ID-NBR", FieldType.STRING, 11);

        pnd_Print_Tax_Id_Nbr__R_Field_45 = localVariables.newGroupInRecord("pnd_Print_Tax_Id_Nbr__R_Field_45", "REDEFINE", pnd_Print_Tax_Id_Nbr);
        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_1 = pnd_Print_Tax_Id_Nbr__R_Field_45.newFieldInGroup("pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_1", "#PRT-TAX-1", FieldType.NUMERIC, 
            3);
        pnd_Print_Tax_Id_Nbr_Pnd_Flr3 = pnd_Print_Tax_Id_Nbr__R_Field_45.newFieldInGroup("pnd_Print_Tax_Id_Nbr_Pnd_Flr3", "#FLR3", FieldType.STRING, 1);
        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_2 = pnd_Print_Tax_Id_Nbr__R_Field_45.newFieldInGroup("pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_2", "#PRT-TAX-2", FieldType.NUMERIC, 
            2);
        pnd_Print_Tax_Id_Nbr_Pnd_Flr4 = pnd_Print_Tax_Id_Nbr__R_Field_45.newFieldInGroup("pnd_Print_Tax_Id_Nbr_Pnd_Flr4", "#FLR4", FieldType.STRING, 1);
        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_3 = pnd_Print_Tax_Id_Nbr__R_Field_45.newFieldInGroup("pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_3", "#PRT-TAX-3", FieldType.NUMERIC, 
            4);
        pnd_Print_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Print_Ppcn_Nbr", "#PRINT-PPCN-NBR", FieldType.STRING, 9);

        pnd_Print_Ppcn_Nbr__R_Field_46 = localVariables.newGroupInRecord("pnd_Print_Ppcn_Nbr__R_Field_46", "REDEFINE", pnd_Print_Ppcn_Nbr);
        pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_1 = pnd_Print_Ppcn_Nbr__R_Field_46.newFieldInGroup("pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_1", "#PRT-PPCN-1", FieldType.NUMERIC, 
            7);
        pnd_Print_Ppcn_Nbr_Pnd_Flr5 = pnd_Print_Ppcn_Nbr__R_Field_46.newFieldInGroup("pnd_Print_Ppcn_Nbr_Pnd_Flr5", "#FLR5", FieldType.STRING, 1);
        pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_2 = pnd_Print_Ppcn_Nbr__R_Field_46.newFieldInGroup("pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_2", "#PRT-PPCN-2", FieldType.NUMERIC, 
            1);
        pnd_Ctr_Clg = localVariables.newFieldInRecord("pnd_Ctr_Clg", "#CTR-CLG", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Sort = localVariables.newFieldInRecord("pnd_Ctr_Sort", "#CTR-SORT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Read = localVariables.newFieldInRecord("pnd_Ctr_Read", "#CTR-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Extr = localVariables.newFieldInRecord("pnd_Ctr_Extr", "#CTR-EXTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Name_Ctr = localVariables.newFieldInRecord("pnd_Name_Ctr", "#NAME-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Status = localVariables.newFieldInRecord("pnd_Bypass_Status", "#BYPASS-STATUS", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Payee = localVariables.newFieldInRecord("pnd_Bypass_Payee", "#BYPASS-PAYEE", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Coll_Cde = localVariables.newFieldInRecord("pnd_Ctr_Coll_Cde", "#CTR-COLL-CDE", FieldType.PACKED_DECIMAL, 9);
        pnd_Sel_Sw = localVariables.newFieldInRecord("pnd_Sel_Sw", "#SEL-SW", FieldType.NUMERIC, 1);
        pnd_Sel_Move = localVariables.newFieldInRecord("pnd_Sel_Move", "#SEL-MOVE", FieldType.PACKED_DECIMAL, 9);
        pnd_Sel_Age = localVariables.newFieldInRecord("pnd_Sel_Age", "#SEL-AGE", FieldType.PACKED_DECIMAL, 9);
        pnd_Prt_Ssns = localVariables.newFieldArrayInRecord("pnd_Prt_Ssns", "#PRT-SSNS", FieldType.NUMERIC, 9, new DbsArrayController(1, 50000));
        pnd_Prt_Cnt = localVariables.newFieldInRecord("pnd_Prt_Cnt", "#PRT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Extr_Dob_Dte_1 = localVariables.newFieldInRecord("pnd_Extr_Dob_Dte_1", "#EXTR-DOB-DTE-1", FieldType.NUMERIC, 8);

        pnd_Extr_Dob_Dte_1__R_Field_47 = localVariables.newGroupInRecord("pnd_Extr_Dob_Dte_1__R_Field_47", "REDEFINE", pnd_Extr_Dob_Dte_1);
        pnd_Extr_Dob_Dte_1_Pnd_Dob_Yyyy = pnd_Extr_Dob_Dte_1__R_Field_47.newFieldInGroup("pnd_Extr_Dob_Dte_1_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.STRING, 
            4);
        pnd_Extr_Dob_Dte_1_Pnd_Dob_Mm = pnd_Extr_Dob_Dte_1__R_Field_47.newFieldInGroup("pnd_Extr_Dob_Dte_1_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_Extr_Dob_Dte_1_Pnd_Dob_Dd = pnd_Extr_Dob_Dte_1__R_Field_47.newFieldInGroup("pnd_Extr_Dob_Dte_1_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Extr_Rec_Out_1_Pnd_F1.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F2.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F3.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F3a.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F4.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F5.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F6.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F7.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F8.setInitialValue(";");
        pnd_Extr_Rec_Out_1_Pnd_F9.setInitialValue(";");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap309() throws Exception
    {
        super("Iaap309");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap309|Main");
        OnErrorManager.pushEvent("IAAP309", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  202010
                //*  202010
                //* *FORMAT (2) LS=133 PS=56                                                                                                                              //Natural: FORMAT ( 1 ) LS = 133 PS = 0 ZP = OFF SG = OFF
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Parm_Check_Dte_A);                                                                                 //Natural: INPUT #PARM-CHECK-DTE-A
                //*  202010 - START
                getWorkFiles().write(4, true, "SSN; Name; DOB; Age; Sex; Address 1; Address 2; Address 3;", "Address 4; Address 5; Address 6");                           //Natural: WRITE WORK FILE 4 VARIABLE 'SSN; Name; DOB; Age; Sex; Address 1; Address 2; Address 3;' 'Address 4; Address 5; Address 6'
                //*  202010 - END
                READWORK01:                                                                                                                                               //Natural: READ WORK 1 #INPUT
                while (condition(getWorkFiles().read(1, pnd_Input)))
                {
                    pnd_Ctr_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTR-READ
                    if (condition(pnd_Input_Pnd_Cntrct_Inst_Iss_Cde.notEquals("     ") && pnd_Input_Pnd_Cntrct_Inst_Iss_Cde.notEquals("00000")))                          //Natural: IF #INPUT.#CNTRCT-INST-ISS-CDE NOT = '     ' AND #INPUT.#CNTRCT-INST-ISS-CDE NOT = '00000'
                    {
                        pnd_Ctr_Coll_Cde.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-COLL-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADDED 6/00
                    if (condition(pnd_Input_Pnd_Record_Code.equals(0)))                                                                                                   //Natural: IF #RECORD-CODE = 00
                    {
                        if (condition(pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm.equals(getZero())))                                                                  //Natural: IF #PARM-CHECK-DTE-YYYYMM = 0
                        {
                            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                   //Natural: IF #PPCN-NBR = '   CHEADER'
                            {
                                pnd_Save_Dte_A_Pnd_Save_Dte.setValue(pnd_Input_Pnd_Check_Dte);                                                                            //Natural: ASSIGN #SAVE-DTE = #CHECK-DTE
                                pnd_Date90.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                  //Natural: ASSIGN #DATE90 = #SAVE-DTE-YYYYMM
                                pnd_Date95.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                  //Natural: ASSIGN #DATE95 = #SAVE-DTE-YYYYMM
                                pnd_Date100.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                 //Natural: ASSIGN #DATE100 = #SAVE-DTE-YYYYMM
                                pnd_Date90_Pnd_Date90_Ccyy.compute(new ComputeParameters(false, pnd_Date90_Pnd_Date90_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(90));       //Natural: COMPUTE #DATE90-CCYY = #YYYY - 90
                                pnd_Date95_Pnd_Date95_Ccyy.compute(new ComputeParameters(false, pnd_Date95_Pnd_Date95_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(95));       //Natural: COMPUTE #DATE95-CCYY = #YYYY - 95
                                pnd_Date100_Pnd_Date100_Ccyy.compute(new ComputeParameters(false, pnd_Date100_Pnd_Date100_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(100));  //Natural: COMPUTE #DATE100-CCYY = #YYYY - 100
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER")  //Natural: IF #PPCN-NBR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                                    || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
                                {
                                    if (condition(true)) continue;                                                                                                        //Natural: ESCAPE TOP
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Save_Dte_A_Pnd_Save_Dte.setValue(pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte);                                                                //Natural: ASSIGN #SAVE-DTE = #PARM-CHECK-DTE
                            pnd_Date90.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                      //Natural: ASSIGN #DATE90 = #SAVE-DTE-YYYYMM
                            pnd_Date95.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                      //Natural: ASSIGN #DATE95 = #SAVE-DTE-YYYYMM
                            pnd_Date100.setValue(pnd_Save_Dte_A_Pnd_Save_Dte_Yyyymm);                                                                                     //Natural: ASSIGN #DATE100 = #SAVE-DTE-YYYYMM
                            pnd_Date90_Pnd_Date90_Ccyy.compute(new ComputeParameters(false, pnd_Date90_Pnd_Date90_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(90));           //Natural: COMPUTE #DATE90-CCYY = #YYYY - 90
                            pnd_Date95_Pnd_Date95_Ccyy.compute(new ComputeParameters(false, pnd_Date95_Pnd_Date95_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(95));           //Natural: COMPUTE #DATE95-CCYY = #YYYY - 95
                            pnd_Date100_Pnd_Date100_Ccyy.compute(new ComputeParameters(false, pnd_Date100_Pnd_Date100_Ccyy), pnd_Save_Dte_A_Pnd_Yyyy.subtract(100));      //Natural: COMPUTE #DATE100-CCYY = #YYYY - 100
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ADDED 6/00
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet518 = 0;                                                                                                                     //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
                    if (condition((pnd_Input_Pnd_Record_Code.equals(10))))
                    {
                        decideConditionsMet518++;
                        pnd_Bypass.reset();                                                                                                                               //Natural: RESET #BYPASS
                        if (condition(pnd_Input_Pnd_Cntrct_Crrncy_Cde.equals(2) || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(22) || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(23)   //Natural: IF #CNTRCT-CRRNCY-CDE = 2 OR #CNTRCT-OPTN-CDE = 22 OR #CNTRCT-OPTN-CDE = 23 OR #CNTRCT-OPTN-CDE = 25 OR #CNTRCT-OPTN-CDE = 27 OR #CNTRCT-OPTN-CDE = 28 OR #CNTRCT-OPTN-CDE = 30
                            || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(25) || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(27) || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(28) 
                            || pnd_Input_Pnd_Cntrct_Optn_Cde.equals(30)))
                        {
                            pnd_Bypass.setValue(true);                                                                                                                    //Natural: ASSIGN #BYPASS := TRUE
                            pnd_Bypass_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #BYPASS-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Save_Cntrct_Optn_Cde.setValue(pnd_Input_Pnd_Cntrct_Optn_Cde);                                                                                 //Natural: ASSIGN #SAVE-CNTRCT-OPTN-CDE := #INPUT.#CNTRCT-OPTN-CDE
                        pnd_Save_Cntrct_Crrncy_Cde.setValue(pnd_Input_Pnd_Cntrct_Crrncy_Cde);                                                                             //Natural: ASSIGN #SAVE-CNTRCT-CRRNCY-CDE := #INPUT.#CNTRCT-CRRNCY-CDE
                        pnd_Save_Cntrct_First_Annt_Dob_Dte.setValue(pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte);                                                             //Natural: ASSIGN #SAVE-CNTRCT-FIRST-ANNT-DOB-DTE := #INPUT.#CNTRCT-FIRST-ANNT-DOB-DTE
                        pnd_Save_Cntrct_First_Annt_Sex_Cde.setValue(pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde);                                                             //Natural: ASSIGN #SAVE-CNTRCT-FIRST-ANNT-SEX-CDE := #INPUT.#CNTRCT-FIRST-ANNT-SEX-CDE
                        pnd_Save_Cntrct_First_Annt_Dod_Dte.setValue(pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte);                                                             //Natural: ASSIGN #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE := #INPUT.#CNTRCT-FIRST-ANNT-DOD-DTE
                        pnd_Save_Cntrct_Scnd_Annt_Dob_Dte.setValue(pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte);                                                               //Natural: ASSIGN #SAVE-CNTRCT-SCND-ANNT-DOB-DTE := #INPUT.#CNTRCT-SCND-ANNT-DOB-DTE
                        pnd_Save_Cntrct_Scnd_Annt_Sex_Cde.setValue(pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde);                                                               //Natural: ASSIGN #SAVE-CNTRCT-SCND-ANNT-SEX-CDE := #INPUT.#CNTRCT-SCND-ANNT-SEX-CDE
                        pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.setValue(pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte);                                                               //Natural: ASSIGN #SAVE-CNTRCT-SCND-ANNT-DOD-DTE := #INPUT.#CNTRCT-SCND-ANNT-DOD-DTE
                        pnd_Save_Clg_Cde.setValue(pnd_Input_Pnd_Cntrct_Inst_Iss_Cde);                                                                                     //Natural: ASSIGN #SAVE-CLG-CDE := #INPUT.#CNTRCT-INST-ISS-CDE
                                                                                                                                                                          //Natural: PERFORM #SELECT-DOB-SEX
                        sub_Pnd_Select_Dob_Sex();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 20
                    else if (condition((pnd_Input_Pnd_Record_Code.equals(20))))
                    {
                        decideConditionsMet518++;
                        if (condition(pnd_Bypass.getBoolean()))                                                                                                           //Natural: IF #BYPASS
                        {
                            pnd_Bypass_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #BYPASS-CTR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Pnd_Cpr_Status_Cde.equals(9)))                                                                                        //Natural: IF #CPR-STATUS-CDE = 9
                            {
                                //*          ADD 1 TO #BYPASS-CTR
                                pnd_Bypass_Status.nadd(1);                                                                                                                //Natural: ADD 1 TO #BYPASS-STATUS
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Input_Pnd_Payee_Cde.notEquals(1) && pnd_Input_Pnd_Payee_Cde.notEquals(2)))                                              //Natural: IF #PAYEE-CDE NOT = 01 AND #PAYEE-CDE NOT = 02
                                {
                                    //*            ADD 1 TO #BYPASS-CTR
                                    pnd_Bypass_Payee.nadd(1);                                                                                                             //Natural: ADD 1 TO #BYPASS-PAYEE
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Save_Cntrct_Payee.setValue(pnd_Input_Pnd_Cntrct_Payee);                                                                           //Natural: ASSIGN #SAVE-CNTRCT-PAYEE := #INPUT.#CNTRCT-PAYEE
                                    pnd_Save_Tax_Id_Nbr.setValue(pnd_Input_Pnd_Cpr_Tax_Id_Nbr);                                                                           //Natural: ASSIGN #SAVE-TAX-ID-NBR := #INPUT.#CPR-TAX-ID-NBR
                                    pnd_Save_Status_Cde.setValue(pnd_Input_Pnd_Cpr_Status_Cde);                                                                           //Natural: ASSIGN #SAVE-STATUS-CDE := #INPUT.#CPR-STATUS-CDE
                                                                                                                                                                          //Natural: PERFORM #SELECT-AGE-RTN
                                    sub_Pnd_Select_Age_Rtn();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(Map.getDoInput())) {return;}
                                    pnd_Sel_Age.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SEL-AGE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                READWORK02:                                                                                                                                               //Natural: READ WORK 3 #EXTR-REC-OUT
                while (condition(getWorkFiles().read(3, pnd_Extr_Rec_Out)))
                {
                    pnd_Ctr_Sort.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTR-SORT
                    getSort().writeSortInData(pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind, pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc, pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte,                  //Natural: END-ALL
                        pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Name, pnd_Extr_Rec_Out_Pnd_Extr_Adr1, pnd_Extr_Rec_Out_Pnd_Extr_Adr2, pnd_Extr_Rec_Out_Pnd_Extr_Adr3, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4, pnd_Extr_Rec_Out_Pnd_Extr_Adr5, pnd_Extr_Rec_Out_Pnd_Extr_Adr6, pnd_Extr_Rec_Out_Pnd_Extr_Zip, pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind, "DESCENDING", pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc, "DESCENDING", pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte, //Natural: SORT BY #EXTR-INST-IND DESCENDING #EXTR-AGE-CALC DESCENDING #EXTR-DOB-DTE ASCENDING #EXTR-PPCN-NBR ASCENDING USING #EXTR-PAYEE-CDE #EXTR-CLG-CDE #EXTR-SEX-CDE #EXTR-NAME #EXTR-ADR1 #EXTR-ADR2 #EXTR-ADR3 #EXTR-ADR4 #EXTR-ADR5 #EXTR-ADR6 #EXTR-ZIP #EXTR-SS-NBR
                    pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr);
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind, pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc, pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Name, pnd_Extr_Rec_Out_Pnd_Extr_Adr1, pnd_Extr_Rec_Out_Pnd_Extr_Adr2, pnd_Extr_Rec_Out_Pnd_Extr_Adr3, pnd_Extr_Rec_Out_Pnd_Extr_Adr4, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Adr5, pnd_Extr_Rec_Out_Pnd_Extr_Adr6, pnd_Extr_Rec_Out_Pnd_Extr_Zip, pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr)))
                {
                    pnd_Print_Dte_Pnd_Prt_Dte_Mm.setValue(pnd_Save_Dte_A_Pnd_Mm);                                                                                         //Natural: MOVE #MM TO #PRT-DTE-MM
                    pnd_Print_Dte_Pnd_Prt_Dte_Dd.setValue(pnd_Save_Dte_A_Pnd_Dd);                                                                                         //Natural: MOVE #DD TO #PRT-DTE-DD
                    pnd_Print_Dte_Pnd_Prt_Dte_Yy.setValue(pnd_Save_Dte_A_Pnd_Yy);                                                                                         //Natural: MOVE #YY TO #PRT-DTE-YY
                    pnd_Print_Dte_Pnd_Flr1.setValue("/");                                                                                                                 //Natural: MOVE '/' TO #FLR1
                    pnd_Print_Dte_Pnd_Flr2.setValue("/");                                                                                                                 //Natural: MOVE '/' TO #FLR2
                    pnd_Save_Tax_Id_Nbr.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr);                                                                                       //Natural: MOVE #EXTR-SS-NBR TO #SAVE-TAX-ID-NBR
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc.equals(90) || pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc.equals(95) || pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc.greaterOrEqual(100))) //Natural: IF #EXTR-AGE-CALC = 90 OR #EXTR-AGE-CALC = 95 OR #EXTR-AGE-CALC GE 100
                    {
                        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr);                                                             //Natural: MOVE #EXTR-PPCN-NBR TO #SAVE-PPCN-NBR
                        pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_1.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr1);                                                             //Natural: MOVE #SAVE-PPCN-NBR1 TO #PRT-PPCN-1
                        pnd_Print_Ppcn_Nbr_Pnd_Flr5.setValue("-");                                                                                                        //Natural: MOVE '-' TO #FLR5
                        pnd_Print_Ppcn_Nbr_Pnd_Prt_Ppcn_2.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr2);                                                             //Natural: MOVE #SAVE-PPCN-NBR2 TO #PRT-PPCN-2
                        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_1.setValue(pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N1);                                                                 //Natural: MOVE #SAVE-TAX-N1 TO #PRT-TAX-1
                        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_2.setValue(pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N2);                                                                 //Natural: MOVE #SAVE-TAX-N2 TO #PRT-TAX-2
                        pnd_Print_Tax_Id_Nbr_Pnd_Prt_Tax_3.setValue(pnd_Save_Tax_Id_Nbr_Pnd_Save_Tax_N3);                                                                 //Natural: MOVE #SAVE-TAX-N3 TO #PRT-TAX-3
                        pnd_Print_Tax_Id_Nbr_Pnd_Flr3.setValue("-");                                                                                                      //Natural: MOVE '-' TO #FLR3
                        pnd_Print_Tax_Id_Nbr_Pnd_Flr4.setValue("-");                                                                                                      //Natural: MOVE '-' TO #FLR4
                        pnd_Prt_Cntrct_Annt_Dob_Dte.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte);                                                                          //Natural: MOVE #EXTR-DOB-DTE TO #PRT-CNTRCT-ANNT-DOB-DTE
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind.equals("2")))                                                                                    //Natural: IF #EXTR-INST-IND = '2'
                        {
                            //*      WRITE ' INST-IND = PRINT  '  #EXTR-INST-IND
                            pnd_Tiaa_Cref_Fld.setValue("**TIAA-CREF**");                                                                                                  //Natural: MOVE '**TIAA-CREF**' TO #TIAA-CREF-FLD
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tiaa_Cref_Fld.reset();                                                                                                                    //Natural: RESET #TIAA-CREF-FLD
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.equals(1)))                                                                                       //Natural: IF #EXTR-SEX-CDE = 1
                        {
                            pnd_Prt_Sex_Cde.setValue("M");                                                                                                                //Natural: MOVE 'M' TO #PRT-SEX-CDE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Prt_Sex_Cde.setValue("F");                                                                                                                //Natural: MOVE 'F' TO #PRT-SEX-CDE
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, new ReportEmptyLineSuppression(true),ReportOption.NOTITLE,new TabSetting(2),pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Mm,         //Natural: WRITE ( 1 ) ( ES = ON ) 2T #PRT-MM ( EM = 99 ) 2X #PRT-DD ( EM = 99 ) 2X #PRT-CCYY 17T #EXTR-AGE-CALC 26T #PRT-SEX-CDE 32T #EXTR-NAME 4X #PRINT-TAX-ID-NBR
                            new ReportEditMask ("99"),new ColumnSpacing(2),pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Dd, new ReportEditMask ("99"),new ColumnSpacing(2),pnd_Prt_Cntrct_Annt_Dob_Dte_Pnd_Prt_Ccyy,new 
                            TabSetting(17),pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc,new TabSetting(26),pnd_Prt_Sex_Cde,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Name,new 
                            ColumnSpacing(4),pnd_Print_Tax_Id_Nbr);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr1.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR1 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr1,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR1 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr2.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR2 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr2,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR2 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr3.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR3 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr3,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR3 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr4.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR4 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr4,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR4 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr5.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR5 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr5,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR5 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr6.greater(" ")))                                                                                       //Natural: IF #EXTR-ADR6 GT ' '
                        {
                            getReports().write(1, ReportOption.NOTITLE,new TabSetting(32),pnd_Extr_Rec_Out_Pnd_Extr_Adr6,NEWLINE);                                        //Natural: WRITE ( 1 ) 32T #EXTR-ADR6 /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*    IF #EXTR-ZIP   GT ' '
                        //*      WRITE (1) 51T #EXTR-ZIP  /
                        //*    END-IF
                        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ( 1 ) ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                              //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - IA FILE      : ",pnd_Ctr_Read);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - IA FILE      : ' #CTR-READ
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - NAME FILE    : ",pnd_Name_Ctr);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - NAME FILE    : ' #NAME-CTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - SORTED       : ",pnd_Ctr_Sort);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - SORTED       : ' #CTR-SORT
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF COLLEGE-CODE                : ",pnd_Ctr_Coll_Cde);                                                                       //Natural: WRITE 'NUMBER OF COLLEGE-CODE                : ' #CTR-COLL-CDE
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF COLLEGE-CODE - 01940        : ",pnd_Ctr_Clg);                                                                            //Natural: WRITE 'NUMBER OF COLLEGE-CODE - 01940        : ' #CTR-CLG
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS WRITTEN             : ",pnd_Ctr_Extr);                                                                           //Natural: WRITE 'NUMBER OF RECORDS WRITTEN             : ' #CTR-EXTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Bypass_Ctr);                                                                         //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #BYPASS-CTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED-STATUS     : ",pnd_Bypass_Status);                                                                      //Natural: WRITE 'NUMBER OF RECORDS BYPASSED-STATUS     : ' #BYPASS-STATUS
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED-PAYEE      : ",pnd_Bypass_Payee);                                                                       //Natural: WRITE 'NUMBER OF RECORDS BYPASSED-PAYEE      : ' #BYPASS-PAYEE
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS WITH SEL-AGE        : ",pnd_Sel_Age);                                                                            //Natural: WRITE 'NUMBER OF RECORDS WITH SEL-AGE        : ' #SEL-AGE
                if (Global.isEscape()) return;
                //* *****************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SELECT-AGE-RTN
                //* *****************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SELECT-DOB-SEX
                //* *****************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-NAME-ADDRESS
                //*  WRITE ' EXTR-NAME FROM NM-FR = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FR = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FR = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FR = ' #SAVE-EXTR-ADR3
                //*  WRITE ' EXTR-NAME FROM NM-FK = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FK = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FK = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FK = ' #SAVE-EXTR-ADR3
                //*  WRITE ' EXTR-NAME FROM NM-FRV = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FRV = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FRV = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FRV = ' #SAVE-EXTR-ADR3
                //*  WRITE ' EXTR-NAME FROM NM-FKV = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FKV = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FKV = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FKV = ' #SAVE-EXTR-ADR3
                //*  WRITE ' EXTR-NAME FROM NM-FRV1 = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FRV1 = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FRV1 = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FRV1 = ' #SAVE-EXTR-ADR3
                //*  WRITE ' EXTR-NAME FROM NM-FKV1 = ' #SAVE-EXTR-NAME
                //*        ' EXTR-ADR1 FROM NM-FKV1 = ' #SAVE-EXTR-ADR1
                //*        ' EXTR-ADR2 FROM NM-FKV1 = ' #SAVE-EXTR-ADR2
                //*        ' EXTR-ADR3 FROM NM-FKV1 = ' #SAVE-EXTR-ADR3
                //*                                                                                                                                                       //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Pnd_Select_Age_Rtn() throws Exception                                                                                                                //Natural: #SELECT-AGE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Sel_Sw.reset();                                                                                                                                               //Natural: RESET #SEL-SW
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy.equals(pnd_Date90_Pnd_Date90_Ccyy)           //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND #F-CCYY = #DATE90-CCYY AND #F-MM = #DATE90-MM )
            && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm.equals(pnd_Date90_Pnd_Date90_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.greater(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy.equals(pnd_Date90_Pnd_Date90_Ccyy)  //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND #SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0 AND #S-CCYY = #DATE90-CCYY AND #S-MM = #DATE90-MM )
            && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm.equals(pnd_Date90_Pnd_Date90_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF (#SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0
        //*     AND
        //*   #F-CCYY = #DATE95-CCYY
        //*     AND
        //*   #F-MM = #DATE95-MM)
        //*     OR
        //*     (#SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0
        //*     AND
        //*   #S-CCYY =#DATE95-CCYY
        //*     AND
        //*   #S-MM = #DATE95-MM)
        //*   MOVE 2 TO #SEL-SW
        //*   END-IF
        //*  CHANGED ABOVE COMMENTED TO FOLLOWING   6/00
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy.equals(pnd_Date95_Pnd_Date95_Ccyy)           //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND #F-CCYY = #DATE95-CCYY AND #F-MM = #DATE95-MM )
            && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm.equals(pnd_Date95_Pnd_Date95_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.greater(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy.equals(pnd_Date95_Pnd_Date95_Ccyy)  //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND #SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0 AND #S-CCYY = #DATE95-CCYY AND #S-MM = #DATE95-MM )
            && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm.equals(pnd_Date95_Pnd_Date95_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF (((#SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0)
        //*    AND
        //*    (#F-CCYY = #DATE100-CCYY OR
        //*    #F-CCYY < #DATE100-CCYY)
        //*    AND
        //*    (#F-MM = #DATE100-MM))
        //*    OR
        //*    ((#SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0)
        //*    AND
        //*    (#S-CCYY = #DATE100-CCYY OR
        //*    #S-CCYY < #DATE100-CCYY)
        //*    AND
        //*    (#S-MM = #DATE100-MM)))
        //*  MOVE 3 TO #SEL-SW
        //*  END-IF
        //*  CHANGED ABOVE COMMENTED TO FOLLOWING   6/00
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy.lessOrEqual(pnd_Date100_Pnd_Date100_Ccyy)    //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND #F-CCYY LE #DATE100-CCYY AND #F-MM = #DATE100-MM )
            && pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm.equals(pnd_Date100_Pnd_Date100_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Save_Cntrct_First_Annt_Dod_Dte.greater(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy.lessOrEqual(pnd_Date100_Pnd_Date100_Ccyy)  //Natural: IF ( #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND #SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0 AND #S-CCYY LE #DATE100-CCYY AND #S-MM = #DATE100-MM )
            && pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm.equals(pnd_Date100_Pnd_Date100_Mm))))
        {
            pnd_Sel_Sw.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #SEL-SW
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sel_Sw.equals(getZero())))                                                                                                                      //Natural: IF #SEL-SW = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                                             //Natural: IF #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE > 0
        {
            pnd_Save_Age_Calc.compute(new ComputeParameters(false, pnd_Save_Age_Calc), pnd_Save_Dte_A_Pnd_Yyyy.subtract(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Ccyy));   //Natural: SUBTRACT #S-CCYY FROM #YYYY GIVING #SAVE-AGE-CALC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Save_Age_Calc.compute(new ComputeParameters(false, pnd_Save_Age_Calc), pnd_Save_Dte_A_Pnd_Yyyy.subtract(pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Ccyy));  //Natural: SUBTRACT #F-CCYY FROM #YYYY GIVING #SAVE-AGE-CALC
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Age_Calc.equals(90) || pnd_Save_Age_Calc.equals(95) || pnd_Save_Age_Calc.greaterOrEqual(100)))                                             //Natural: IF #SAVE-AGE-CALC = 90 OR #SAVE-AGE-CALC = 95 OR #SAVE-AGE-CALC GE 100
        {
            //*  WRITE 'SAVE-AGE-CALC = '  #SAVE-AGE-CALC
            //*        'SAVE-PPCN-NBR = '  #SAVE-PPCN-NBR
            pnd_Extr_Rec_Out_Pnd_Extr_Age_Calc.setValue(pnd_Save_Age_Calc);                                                                                               //Natural: MOVE #SAVE-AGE-CALC TO #EXTR-AGE-CALC
            pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr);                                                                         //Natural: MOVE #SAVE-PPCN-NBR TO #EXTR-PPCN-NBR
            pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde);                                                                       //Natural: MOVE #SAVE-PAYEE-CDE TO #EXTR-PAYEE-CDE
            pnd_Extr_Rec_Out_Pnd_Extr_Clg_Cde.setValue(pnd_Save_Clg_Cde);                                                                                                 //Natural: MOVE #SAVE-CLG-CDE TO #EXTR-CLG-CDE
            //*  IF #EXTR-CLG-CDE = '01940'
            //*    WRITE ' COLLEGE-CDE (01940) WRITE EXTR-REC ' #SAVE-CLG-CDE
            //*  END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-NAME-ADDRESS
            sub_Pnd_Get_Name_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Save_Extr_Name);                                                                                                  //Natural: MOVE #SAVE-EXTR-NAME TO #EXTR-NAME
            pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Save_Extr_Adr1);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR1 TO #EXTR-ADR1
            pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Save_Extr_Adr2);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR2 TO #EXTR-ADR2
            pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Save_Extr_Adr3);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR3 TO #EXTR-ADR3
            pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Save_Extr_Adr4);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR4 TO #EXTR-ADR4
            pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Save_Extr_Adr5);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR5 TO #EXTR-ADR5
            pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Save_Extr_Adr6);                                                                                                  //Natural: MOVE #SAVE-EXTR-ADR6 TO #EXTR-ADR6
            pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Save_Extr_Zip);                                                                                                    //Natural: MOVE #SAVE-EXTR-ZIP TO #EXTR-ZIP
            pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr.setValue(pnd_Save_Tax_Id_Nbr);                                                                                               //Natural: MOVE #SAVE-TAX-ID-NBR TO #EXTR-SS-NBR
            //*  202010 - START
            if (condition(pnd_Prt_Ssns.getValue("*").equals(pnd_Save_Tax_Id_Nbr) && pnd_Save_Tax_Id_Nbr.greater(getZero())))                                              //Natural: IF #PRT-SSNS ( * ) = #SAVE-TAX-ID-NBR AND #SAVE-TAX-ID-NBR GT 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PRT-CNT
                pnd_Prt_Ssns.getValue(pnd_Prt_Cnt).setValue(pnd_Save_Tax_Id_Nbr);                                                                                         //Natural: ASSIGN #PRT-SSNS ( #PRT-CNT ) := #SAVE-TAX-ID-NBR
                getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                         //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
                pnd_Ctr_Extr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR-EXTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  202010 - END
        //* ************************2/12/202
        //*  202010
        //*  202010
        if (condition(pnd_Save_Age_Calc.equals(90) || pnd_Save_Age_Calc.equals(95) || pnd_Save_Age_Calc.greaterOrEqual(100)))                                             //Natural: IF #SAVE-AGE-CALC = 90 OR #SAVE-AGE-CALC = 95 OR #SAVE-AGE-CALC GE 100
        {
            pnd_Extr_Rec_Out_1_Pnd_Extr_Name_1.setValue(pnd_Save_Extr_Name);                                                                                              //Natural: MOVE #SAVE-EXTR-NAME TO #EXTR-NAME-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr1_1.setValue(pnd_Save_Extr_Adr1);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR1 TO #EXTR-ADR1-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr2_1.setValue(pnd_Save_Extr_Adr2);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR2 TO #EXTR-ADR2-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr3_1.setValue(pnd_Save_Extr_Adr3);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR3 TO #EXTR-ADR3-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr4_1.setValue(pnd_Save_Extr_Adr4);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR4 TO #EXTR-ADR4-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr5_1.setValue(pnd_Save_Extr_Adr5);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR5 TO #EXTR-ADR5-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Adr6_1.setValue(pnd_Save_Extr_Adr6);                                                                                              //Natural: MOVE #SAVE-EXTR-ADR6 TO #EXTR-ADR6-1
            if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.equals(1)))                                                                                                   //Natural: IF #EXTR-SEX-CDE = 1
            {
                pnd_Extr_Rec_Out_1_Pnd_Prt_Sex_Cde_1.setValue("M");                                                                                                       //Natural: MOVE 'M' TO #PRT-SEX-CDE-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Extr_Rec_Out_1_Pnd_Prt_Sex_Cde_1.setValue("F");                                                                                                       //Natural: MOVE 'F' TO #PRT-SEX-CDE-1
            }                                                                                                                                                             //Natural: END-IF
            //*  202010
            pnd_Extr_Rec_Out_1_Pnd_Extr_Ss_Nbr_1.setValue(pnd_Save_Tax_Id_Nbr);                                                                                           //Natural: MOVE #SAVE-TAX-ID-NBR TO #EXTR-SS-NBR-1
            pnd_Extr_Rec_Out_1_Pnd_Extr_Age.setValue(pnd_Save_Age_Calc);                                                                                                  //Natural: ASSIGN #EXTR-AGE := #SAVE-AGE-CALC
            pnd_Extr_Rec_Out_1_Pnd_Dob_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Extr_Dob_Dte_1_Pnd_Dob_Mm, "/", pnd_Extr_Dob_Dte_1_Pnd_Dob_Dd,    //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-YYYY INTO #DOB-DTE LEAVE NO SPACE
                "/", pnd_Extr_Dob_Dte_1_Pnd_Dob_Yyyy));
            getWorkFiles().write(4, true, pnd_Extr_Rec_Out_1);                                                                                                            //Natural: WRITE WORK FILE 4 VARIABLE #EXTR-REC-OUT-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Select_Dob_Sex() throws Exception                                                                                                                //Natural: #SELECT-DOB-SEX
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  IF (#SAVE-CLG-CDE = '01940' AND (#SAVE-AGE-CALC = 90 OR
        //*     #EXTR-AGE-CALC = 95 OR
        //*        #EXTR-AGE-CALC GE 100))
        if (condition(pnd_Save_Clg_Cde.equals("01940")))                                                                                                                  //Natural: IF #SAVE-CLG-CDE = '01940'
        {
            //*  WRITE ' COLLEGE-CDE (01940) = D0B-SEX-RTN ' #CNTRCT-INST-ISS-CDE
            //*  WRITE ' EXTR-IND = DOB-SEX-RTN '  #EXTR-INST-IND
            pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind.setValue("2");                                                                                                             //Natural: MOVE '2' TO #EXTR-INST-IND
            pnd_Ctr_Clg.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CTR-CLG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind.setValue("1");                                                                                                             //Natural: MOVE '1' TO #EXTR-INST-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero())))                                       //Natural: IF #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND #SAVE-CNTRCT-SCND-ANNT-DOD-DTE = 0
        {
            if (condition(pnd_Save_Dte_A_Pnd_Mm.equals(pnd_Save_Cntrct_First_Annt_Dob_Dte_Pnd_F_Mm)))                                                                     //Natural: IF #MM = #F-MM
            {
                pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte.setValue(pnd_Save_Cntrct_First_Annt_Dob_Dte);                                                                           //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-DOB-DTE TO #EXTR-DOB-DTE
                pnd_Extr_Dob_Dte_1.setValue(pnd_Save_Cntrct_First_Annt_Dob_Dte);                                                                                          //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-DOB-DTE TO #EXTR-DOB-DTE-1
                pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.setValue(pnd_Save_Cntrct_First_Annt_Sex_Cde);                                                                           //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-SEX-CDE TO #EXTR-SEX-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Save_Dte_A_Pnd_Mm.equals(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte_Pnd_S_Mm)))                                                                  //Natural: IF #MM = #S-MM
                {
                    pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte.setValue(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte);                                                                        //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-DOB-DTE TO #EXTR-DOB-DTE
                    pnd_Extr_Dob_Dte_1.setValue(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte);                                                                                       //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-DOB-DTE TO #EXTR-DOB-DTE-1
                    pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.setValue(pnd_Save_Cntrct_Scnd_Annt_Sex_Cde);                                                                        //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-SEX-CDE TO #EXTR-SEX-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                                             //Natural: IF #SAVE-CNTRCT-FIRST-ANNT-DOD-DTE > 0
        {
            pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte.setValue(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte);                                                                                //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-DOB-DTE TO #EXTR-DOB-DTE
            pnd_Extr_Dob_Dte_1.setValue(pnd_Save_Cntrct_Scnd_Annt_Dob_Dte);                                                                                               //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-DOB-DTE TO #EXTR-DOB-DTE-1
            pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.setValue(pnd_Save_Cntrct_Scnd_Annt_Sex_Cde);                                                                                //Natural: MOVE #SAVE-CNTRCT-SCND-ANNT-SEX-CDE TO #EXTR-SEX-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Save_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                          //Natural: IF #SAVE-CNTRCT-SCND-ANNT-DOD-DTE > 0
            {
                pnd_Extr_Rec_Out_Pnd_Extr_Dob_Dte.setValue(pnd_Save_Cntrct_First_Annt_Dob_Dte);                                                                           //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-DOB-DTE TO #EXTR-DOB-DTE
                pnd_Extr_Dob_Dte_1.setValue(pnd_Save_Cntrct_First_Annt_Dob_Dte);                                                                                          //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-DOB-DTE TO #EXTR-DOB-DTE-1
                pnd_Extr_Rec_Out_Pnd_Extr_Sex_Cde.setValue(pnd_Save_Cntrct_First_Annt_Sex_Cde);                                                                           //Natural: MOVE #SAVE-CNTRCT-FIRST-ANNT-SEX-CDE TO #EXTR-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Get_Name_Address() throws Exception                                                                                                              //Natural: #GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'SUBROUTINE #GET-NAME-ADDRESS'
        if (condition(pnd_Name_Eof.getBoolean()))                                                                                                                         //Natural: IF #NAME-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  WRITE ' NAME FROM NM-F = ' #NAME.CNTRCT-PAYEE
            //*        ' NAME FROM CPRF = ' #INPUT.#CNTRCT-PAYEE
            pnd_Save_Extr_Name.setValue("NO NAME & ADDRESS FOUND");                                                                                                       //Natural: MOVE 'NO NAME & ADDRESS FOUND' TO #SAVE-EXTR-NAME
            pnd_Save_Extr_Adr1.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #SAVE-EXTR-ADR1 #SAVE-EXTR-ADR2 #SAVE-EXTR-ADR3 #SAVE-EXTR-ADR4 #SAVE-EXTR-ADR5 #SAVE-EXTR-ADR6 #SAVE-EXTR-ZIP
            pnd_Save_Extr_Adr2.setValue(" ");
            pnd_Save_Extr_Adr3.setValue(" ");
            pnd_Save_Extr_Adr4.setValue(" ");
            pnd_Save_Extr_Adr5.setValue(" ");
            pnd_Save_Extr_Adr6.setValue(" ");
            pnd_Save_Extr_Zip.setValue(" ");
            if (condition(pnd_Name_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                     //Natural: IF #NAME.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                      //Natural: IF #NAME.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
            {
                short decideConditionsMet891 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R = 0 AND #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-R = 0
                if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.equals(getZero()) && pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R.equals(getZero())))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_Cntrct_Name_Free_R.equals(" ")))                                                                                               //Natural: IF CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_K);                                                                                         //Natural: ASSIGN #SAVE-EXTR-NAME := CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR1 := ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR2 := ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR3 := ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR4 := ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR5 := ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR6 := ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_K.getSubstring(1,9));                                                                      //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_R);                                                                                         //Natural: ASSIGN #SAVE-EXTR-NAME := CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR1 := ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR2 := ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR3 := ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR4 := ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR5 := ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR6 := ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_R.getSubstring(1,9));                                                                      //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-K = 0
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.equals(getZero()) && 
                    pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K.equals(getZero())))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_Cntrct_Name_Free_K.equals(" ")))                                                                                               //Natural: IF CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_R);                                                                                         //Natural: ASSIGN #SAVE-EXTR-NAME := CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR1 := ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR2 := ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR3 := ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR4 := ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR5 := ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_R);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR6 := ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_R.getSubstring(1,9));                                                                      //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_K);                                                                                         //Natural: ASSIGN #SAVE-EXTR-NAME := CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR1 := ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR2 := ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR3 := ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR4 := ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR5 := ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_K);                                                                                             //Natural: ASSIGN #SAVE-EXTR-ADR6 := ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_K.getSubstring(1,9));                                                                      //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R = 0 AND #SAVE-DTE GE #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-R
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.equals(getZero()) && 
                    pnd_Save_Dte_A_Pnd_Save_Dte.greaterOrEqual(pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R)))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_R.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #SAVE-DTE GE #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-K
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.equals(getZero()) && 
                    pnd_Save_Dte_A_Pnd_Save_Dte.greaterOrEqual(pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K)))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_K.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R NE 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R NE 0 AND ( #SAVE-DTE GE #NAME.PENDING-ADDRSS-CHNGE-DTE-R AND #SAVE-DTE LE #NAME.PENDING-ADDRSS-RESTORE-DTE-R )
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.notEquals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.notEquals(getZero()) 
                    && pnd_Save_Dte_A_Pnd_Save_Dte.greaterOrEqual(pnd_Name_Pending_Addrss_Chnge_Dte_R) && pnd_Save_Dte_A_Pnd_Save_Dte.lessOrEqual(pnd_Name_Pending_Addrss_Restore_Dte_R)))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_R.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K NE 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K NE 0 AND ( #SAVE-DTE GE #NAME.PENDING-ADDRSS-CHNGE-DTE-K AND #SAVE-DTE LE #NAME.PENDING-ADDRSS-RESTORE-DTE-K )
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.notEquals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.notEquals(getZero()) 
                    && pnd_Save_Dte_A_Pnd_Save_Dte.greaterOrEqual(pnd_Name_Pending_Addrss_Chnge_Dte_K) && pnd_Save_Dte_A_Pnd_Save_Dte.lessOrEqual(pnd_Name_Pending_Addrss_Restore_Dte_K)))
                {
                    decideConditionsMet891++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_K.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                                       //Natural: ASSIGN #SAVE-EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Save_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Save_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Save_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Save_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Save_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Save_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                                           //Natural: ASSIGN #SAVE-EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Save_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                                    //Natural: ASSIGN #SAVE-EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Name);                                                                                                                             //Natural: READ WORK 2 ONCE #NAME
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Name_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NAME-EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-ENDFILE
            pnd_Name_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NAME-CTR
            //*  IF #NAME-CTR > 600000
            //*    ESCAPE BOTTOM
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"PROGRAM: ",Global.getPROGRAM(),new TabSetting(31),"IA ADMINISTRATION BITHDAY MAILING LIST FOR PAYMENTS DUE ",pnd_Print_Dte,new  //Natural: WRITE ( 1 ) NOTITLE 2T 'PROGRAM: ' *PROGRAM 31T 'IA ADMINISTRATION BITHDAY MAILING LIST FOR PAYMENTS DUE ' #PRINT-DTE 111T 'PAGE' *PAGE-NUMBER ( 1 ) / 5T 'DATE: ' *DATU /
                        TabSetting(111),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(5),"DATE: ",Global.getDATU(),NEWLINE);
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"DATE OF BIRTH",new TabSetting(18),"AGE",new TabSetting(25),"SEX",new                    //Natural: WRITE ( 1 ) 2T 'DATE OF BIRTH' 18T 'AGE' 25T 'SEX' 32T 'NAME AND ADDRESS' 26X 'SSN' / 2T '-' ( 13 ) 18T '-' ( 03 ) 25T '-' ( 03 ) 32T '-' ( 35 ) 4X '-' ( 11 ) /
                        TabSetting(32),"NAME AND ADDRESS",new ColumnSpacing(26),"SSN",NEWLINE,new TabSetting(2),"-",new RepeatItem(13),new TabSetting(18),"-",new 
                        RepeatItem(3),new TabSetting(25),"-",new RepeatItem(3),new TabSetting(32),"-",new RepeatItem(35),new ColumnSpacing(4),"-",new RepeatItem(11),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde);                                //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #PPCN-NBR #PAYEE-CDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=0 ZP=OFF SG=OFF");
    }
}
