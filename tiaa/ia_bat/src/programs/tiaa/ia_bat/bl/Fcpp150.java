/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:21 PM
**        * FROM NATURAL PROGRAM : Fcpp150
************************************************************
**        * FILE NAME            : Fcpp150.java
**        * CLASS NAME           : Fcpp150
**        * INSTANCE NAME        : Fcpp150
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: CONTRACT-PAYEE EXTRACT
**SAG SYSTEM: CPS
**SAG REPORT-HEADING(1): BATCH UPDATE REPORT
**SAG PRINT-FILE(1): *
**SAG REPORT-HEADING(2): FCP-CONS-HOLD
**SAG PRINT-FILE(2): *
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM WILL EXTRACT CONTRACT-PAYEE INFO FROM
**SAG DESCS(2): FCP-CONS-HOLD WITH ACTIVE INDICATOR = 'Y', UPDATE THE
**SAG DESCS(3): ACTIVE INDICATOR TO 'N' AND GENERATE A REPORT
**SAG PRIMARY-FILE: FCP-CONS-HOLD
**SAG PRIMARY-KEY: CNTRCT-ACT-RQST-ID
**SAG MODEL-1: X
**SAG MODEL-3: X
**SAG USER-FIELD-DEFINITION(1): #INPUT-DATE               A060TF
************************************************************************
* PROGRAM  : FCPP150
* SYSTEM   : CPS
* TITLE    : CONTRACT-PAYEE EXTRACT
* GENERATED: SEP 08,93 AT 11:43 AM
* FUNCTION : THIS PROGRAM WILL EXTRACT CONTRACT-PAYEE INFO FROM
*            FCP-CONS-HOLD WITH ACTIVE INDICATOR = 'Y', UPDATE THE
*            ACTIVE INDICATOR TO 'N' AND GENERATE A REPORT
*
* HISTORY
* 5/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp150 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Structure;
    private DbsField pnd_Key_Cntrct_Rcrd_Typ;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Cntrct_Rqst_Settl_Id;
    private DbsField pnd_Key_Cntrct_Act_Ind;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_2;
    private DbsField pnd_End_Key_Cntrct_Rcrd_Typ;
    private DbsField pnd_End_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_End_Key_Cntrct_Rqst_Settl_Id;
    private DbsField pnd_End_Key_Cntrct_Act_Ind;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Cntrct_Rcrd_Typ;
    private DbsField pnd_Input_Cntrct_Orgn_Cde;
    private DbsField pnd_Input_Cntrct_Rqst_Settl_Id;
    private DbsField pnd_Input_Cntrct_Act_Ind;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Cntrct_Act_Rqst_Id;
    private DbsField pnd_Input_Date;

    private DbsGroup pnd_Input_Date__R_Field_4;
    private DbsField pnd_Input_Date_Pnd_Cc;
    private DbsField pnd_Input_Date_Pnd_Yy;
    private DbsField pnd_Input_Date_Pnd_Mm;

    private DataAccessProgramView vw_fcp_Cons_Hold;
    private DbsField fcp_Cons_Hold_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Hold_Cntrct_Act_Ind;
    private DbsField fcp_Cons_Hold_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Hold_Cntrct_Hold_Grp;
    private DbsField fcp_Cons_Hold_Cntrct_Hold_User_Id;
    private DbsField fcp_Cons_Hold_Cntrct_Rqst_Settl_Id;

    private DbsGroup fcp_Cons_Hold__R_Field_5;
    private DbsField fcp_Cons_Hold_Pnd_Pymnt_Dte;
    private DbsField fcp_Cons_Hold_Pnd_Orgn_Cde;
    private DbsField fcp_Cons_Hold_Cntrct_Ppcn_Nbr;

    private DbsGroup fcp_Cons_Hold__R_Field_6;
    private DbsField fcp_Cons_Hold_Pnd_Contract_Nbr;
    private DbsField fcp_Cons_Hold_Cntrct_Payee_Cde;

    private DbsGroup fcp_Cons_Hold__R_Field_7;
    private DbsField fcp_Cons_Hold_Pnd_Payee_Cde;
    private DbsField pnd_In_Date;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Filler;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Cntrct_Ppcn_NbrCount169;
    private DbsField sort01Cntrct_Ppcn_NbrCount;
    private DbsField sort01Cntrct_Hold_GrpOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 24);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Structure = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Structure", "#KEY-STRUCTURE");
        pnd_Key_Cntrct_Rcrd_Typ = pnd_Key_Pnd_Key_Structure.newFieldInGroup("pnd_Key_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Structure.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Cntrct_Rqst_Settl_Id = pnd_Key_Pnd_Key_Structure.newFieldInGroup("pnd_Key_Cntrct_Rqst_Settl_Id", "CNTRCT-RQST-SETTL-ID", FieldType.STRING, 
            20);
        pnd_Key_Cntrct_Act_Ind = pnd_Key_Pnd_Key_Structure.newFieldInGroup("pnd_Key_Cntrct_Act_Ind", "CNTRCT-ACT-IND", FieldType.STRING, 1);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 24);

        pnd_End_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_2", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Cntrct_Rcrd_Typ = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_End_Key_Cntrct_Orgn_Cde = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_End_Key_Cntrct_Rqst_Settl_Id = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Cntrct_Rqst_Settl_Id", "CNTRCT-RQST-SETTL-ID", FieldType.STRING, 
            20);
        pnd_End_Key_Cntrct_Act_Ind = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Cntrct_Act_Ind", "CNTRCT-ACT-IND", FieldType.STRING, 1);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Cntrct_Rcrd_Typ = pnd_Input.newFieldInGroup("pnd_Input_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Input_Cntrct_Orgn_Cde = pnd_Input.newFieldInGroup("pnd_Input_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Input_Cntrct_Rqst_Settl_Id = pnd_Input.newFieldInGroup("pnd_Input_Cntrct_Rqst_Settl_Id", "CNTRCT-RQST-SETTL-ID", FieldType.STRING, 20);
        pnd_Input_Cntrct_Act_Ind = pnd_Input.newFieldInGroup("pnd_Input_Cntrct_Act_Ind", "CNTRCT-ACT-IND", FieldType.STRING, 1);

        pnd_Input__R_Field_3 = localVariables.newGroupInRecord("pnd_Input__R_Field_3", "REDEFINE", pnd_Input);
        pnd_Input_Cntrct_Act_Rqst_Id = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Cntrct_Act_Rqst_Id", "CNTRCT-ACT-RQST-ID", FieldType.STRING, 24);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 6);

        pnd_Input_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Date__R_Field_4", "REDEFINE", pnd_Input_Date);
        pnd_Input_Date_Pnd_Cc = pnd_Input_Date__R_Field_4.newFieldInGroup("pnd_Input_Date_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Input_Date_Pnd_Yy = pnd_Input_Date__R_Field_4.newFieldInGroup("pnd_Input_Date_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Input_Date_Pnd_Mm = pnd_Input_Date__R_Field_4.newFieldInGroup("pnd_Input_Date_Pnd_Mm", "#MM", FieldType.STRING, 2);

        vw_fcp_Cons_Hold = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Hold", "FCP-CONS-HOLD"), "FCP_CONS_HOLD", "FCP_EFT_GLBL");
        fcp_Cons_Hold_Cntrct_Rcrd_Typ = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Hold_Cntrct_Act_Ind = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Act_Ind", "CNTRCT-ACT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACT_IND");
        fcp_Cons_Hold_Cntrct_Orgn_Cde = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Hold_Cntrct_Hold_Grp = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_HOLD_GRP");
        fcp_Cons_Hold_Cntrct_Hold_User_Id = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_HOLD_USER_ID");
        fcp_Cons_Hold_Cntrct_Rqst_Settl_Id = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Rqst_Settl_Id", "CNTRCT-RQST-SETTL-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "CNTRCT_RQST_SETTL_ID");

        fcp_Cons_Hold__R_Field_5 = vw_fcp_Cons_Hold.getRecord().newGroupInGroup("fcp_Cons_Hold__R_Field_5", "REDEFINE", fcp_Cons_Hold_Cntrct_Rqst_Settl_Id);
        fcp_Cons_Hold_Pnd_Pymnt_Dte = fcp_Cons_Hold__R_Field_5.newFieldInGroup("fcp_Cons_Hold_Pnd_Pymnt_Dte", "#PYMNT-DTE", FieldType.STRING, 6);
        fcp_Cons_Hold_Pnd_Orgn_Cde = fcp_Cons_Hold__R_Field_5.newFieldInGroup("fcp_Cons_Hold_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        fcp_Cons_Hold_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");

        fcp_Cons_Hold__R_Field_6 = vw_fcp_Cons_Hold.getRecord().newGroupInGroup("fcp_Cons_Hold__R_Field_6", "REDEFINE", fcp_Cons_Hold_Cntrct_Ppcn_Nbr);
        fcp_Cons_Hold_Pnd_Contract_Nbr = fcp_Cons_Hold__R_Field_6.newFieldInGroup("fcp_Cons_Hold_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 
            8);
        fcp_Cons_Hold_Cntrct_Payee_Cde = vw_fcp_Cons_Hold.getRecord().newFieldInGroup("fcp_Cons_Hold_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");

        fcp_Cons_Hold__R_Field_7 = vw_fcp_Cons_Hold.getRecord().newGroupInGroup("fcp_Cons_Hold__R_Field_7", "REDEFINE", fcp_Cons_Hold_Cntrct_Payee_Cde);
        fcp_Cons_Hold_Pnd_Payee_Cde = fcp_Cons_Hold__R_Field_7.newFieldInGroup("fcp_Cons_Hold_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        registerRecord(vw_fcp_Cons_Hold);

        pnd_In_Date = localVariables.newFieldInRecord("pnd_In_Date", "#IN-DATE", FieldType.STRING, 6);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Filler = localVariables.newFieldInRecord("pnd_Filler", "#FILLER", FieldType.STRING, 64);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Cntrct_Ppcn_NbrCount169 = internalLoopRecord.newFieldInRecord("Sort01_Cntrct_Ppcn_Nbr_COUNT_169", "Cntrct_Ppcn_Nbr_COUNT_169", FieldType.NUMERIC, 
            9);
        sort01Cntrct_Ppcn_NbrCount = internalLoopRecord.newFieldInRecord("Sort01_Cntrct_Ppcn_Nbr_COUNT", "Cntrct_Ppcn_Nbr_COUNT", FieldType.NUMERIC, 9);
        sort01Cntrct_Hold_GrpOld = internalLoopRecord.newFieldInRecord("Sort01_Cntrct_Hold_Grp_OLD", "Cntrct_Hold_Grp_OLD", FieldType.STRING, 3);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Hold.reset();
        internalLoopRecord.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Input_Cntrct_Rcrd_Typ.setInitialValue("3");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp150() throws Exception
    {
        super("Fcpp150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 55;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #INPUT-DATE
        while (condition(getWorkFiles().read(2, pnd_Input_Date)))
        {
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT AFTER-RANGE-INPUT
        pnd_In_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Date_Pnd_Yy, pnd_Input_Date_Pnd_Mm, "01"));                                        //Natural: COMPRESS #YY #MM '01' INTO #IN-DATE LEAVE NO SPACE
        pnd_End_Key.moveAll("H'FF'");                                                                                                                                     //Natural: MOVE ALL H'FF' TO #END-KEY
        pnd_End_Key_Cntrct_Rcrd_Typ.setValue("3");                                                                                                                        //Natural: ASSIGN #END-KEY.CNTRCT-RCRD-TYP = '3'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  PRIMARY FILE
        vw_fcp_Cons_Hold.startDatabaseRead                                                                                                                                //Natural: READ FCP-CONS-HOLD BY CNTRCT-ACT-RQST-ID STARTING FROM #INPUT.CNTRCT-ACT-RQST-ID
        (
        "READ_PRIME",
        new Wc[] { new Wc("CNTRCT_ACT_RQST_ID", ">=", pnd_Input_Cntrct_Act_Rqst_Id, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_ACT_RQST_ID", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_fcp_Cons_Hold.readNextRow("READ_PRIME")))
        {
            pnd_Key_Pnd_Key_Structure.setValuesByName(vw_fcp_Cons_Hold);                                                                                                  //Natural: MOVE BY NAME FCP-CONS-HOLD TO #KEY-STRUCTURE
            //*  IF THE MAXIMUM VALUE OF KEY IS REACHED, DO END OF DATA PROCESSING
            if (condition(pnd_Key.greater(pnd_End_Key)))                                                                                                                  //Natural: IF #KEY > #END-KEY THEN
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(((fcp_Cons_Hold_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Hold_Cntrct_Orgn_Cde.equals("AP")) && fcp_Cons_Hold_Pnd_Pymnt_Dte.equals(pnd_In_Date))))) //Natural: ACCEPT IF ( FCP-CONS-HOLD.CNTRCT-ORGN-CDE = 'IA' OR = 'AP' ) AND FCP-CONS-HOLD.#PYMNT-DTE = #IN-DATE
            {
                continue;
            }
            if (condition(fcp_Cons_Hold_Cntrct_Act_Ind.equals("Y")))                                                                                                      //Natural: IF FCP-CONS-HOLD.CNTRCT-ACT-IND = 'Y'
            {
                G1:                                                                                                                                                       //Natural: GET FCP-CONS-HOLD *ISN ( READ-PRIME. )
                vw_fcp_Cons_Hold.readByID(vw_fcp_Cons_Hold.getAstISN("READ_PRIME"), "G1");
                fcp_Cons_Hold_Cntrct_Act_Ind.setValue("N");                                                                                                               //Natural: ASSIGN FCP-CONS-HOLD.CNTRCT-ACT-IND = 'N'
                vw_fcp_Cons_Hold.updateDBRow("G1");                                                                                                                       //Natural: UPDATE ( G1. )
                pnd_Et_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ET-COUNT
                if (condition(pnd_Et_Count.greaterOrEqual(50)))                                                                                                           //Natural: IF #ET-COUNT GE 50
                {
                    pnd_Et_Count.reset();                                                                                                                                 //Natural: RESET #ET-COUNT
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(fcp_Cons_Hold_Cntrct_Hold_Grp, fcp_Cons_Hold_Cntrct_Ppcn_Nbr, fcp_Cons_Hold_Cntrct_Payee_Cde, fcp_Cons_Hold_Cntrct_Hold_User_Id);   //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(fcp_Cons_Hold_Cntrct_Hold_Grp, fcp_Cons_Hold_Cntrct_Ppcn_Nbr);                                                                                 //Natural: SORT BY FCP-CONS-HOLD.CNTRCT-HOLD-GRP FCP-CONS-HOLD.CNTRCT-PPCN-NBR USING FCP-CONS-HOLD.CNTRCT-PAYEE-CDE FCP-CONS-HOLD.CNTRCT-HOLD-USER-ID GIVE COUNT FCP-CONS-HOLD.CNTRCT-PPCN-NBR
        sort01Cntrct_Ppcn_NbrCount169.setDec(new DbsDecimal(0));
        sort01Cntrct_Ppcn_NbrCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(fcp_Cons_Hold_Cntrct_Hold_Grp, fcp_Cons_Hold_Cntrct_Ppcn_Nbr, fcp_Cons_Hold_Cntrct_Payee_Cde, fcp_Cons_Hold_Cntrct_Hold_User_Id)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Cntrct_Ppcn_NbrCount169.setInt(sort01Cntrct_Ppcn_NbrCount169.getInt() + 1);
            sort01Cntrct_Ppcn_NbrCount.setInt(sort01Cntrct_Ppcn_NbrCount.getInt() + 1);
            getWorkFiles().write(1, false, fcp_Cons_Hold_Pnd_Contract_Nbr, fcp_Cons_Hold_Pnd_Payee_Cde, fcp_Cons_Hold_Cntrct_Hold_Grp, fcp_Cons_Hold_Cntrct_Hold_User_Id, //Natural: WRITE WORK FILE 1 FCP-CONS-HOLD.#CONTRACT-NBR FCP-CONS-HOLD.#PAYEE-CDE FCP-CONS-HOLD.CNTRCT-HOLD-GRP FCP-CONS-HOLD.CNTRCT-HOLD-USER-ID #FILLER
                pnd_Filler);
            //*                                                                                                                                                           //Natural: AT BREAK OF FCP-CONS-HOLD.CNTRCT-HOLD-GRP
            getReports().display(1, "User Group",                                                                                                                         //Natural: DISPLAY ( 1 ) 'User Group' FCP-CONS-HOLD.CNTRCT-HOLD-GRP 'Contract#' FCP-CONS-HOLD.CNTRCT-PPCN-NBR ( AL = 8 ) 'Payee/Code' FCP-CONS-HOLD.CNTRCT-PAYEE-CDE ( AL = 2 ) 'User' FCP-CONS-HOLD.CNTRCT-HOLD-USER-ID
            		fcp_Cons_Hold_Cntrct_Hold_Grp,"Contract#",
            		fcp_Cons_Hold_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"Payee/Code",
            		fcp_Cons_Hold_Cntrct_Payee_Cde, new AlphanumericLength (2),"User",
            		fcp_Cons_Hold_Cntrct_Hold_User_Id);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            sort01Cntrct_Hold_GrpOld.setValue(fcp_Cons_Hold_Cntrct_Hold_Grp);                                                                                             //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(15),"                Batch Report                       ",new           //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 15T '                Batch Report                       ' 71T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YY ) 15T '            FCP-CONS-HOLD Extract                   ' 71T *TIMX ( EM = HH':'II' 'AP )
                        TabSetting(71),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(0), new NumericLength (4), 
                        new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YY"),new TabSetting(15),"            FCP-CONS-HOLD Extract                   ",new 
                        TabSetting(71),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean fcp_Cons_Hold_Cntrct_Hold_GrpIsBreak = fcp_Cons_Hold_Cntrct_Hold_Grp.isBreak(endOfData);
        if (condition(fcp_Cons_Hold_Cntrct_Hold_GrpIsBreak))
        {
            getReports().write(0, "Total number of records for",sort01Cntrct_Hold_GrpOld,":",sort01Cntrct_Ppcn_NbrCount169);                                              //Natural: WRITE 'Total number of records for' OLD ( FCP-CONS-HOLD.CNTRCT-HOLD-GRP ) ':' COUNT ( FCP-CONS-HOLD.CNTRCT-PPCN-NBR )
            if (condition(Global.isEscape())) return;
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP 1
            sort01Cntrct_Ppcn_NbrCount169.setDec(new DbsDecimal(0));                                                                                                      //Natural: END-BREAK
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55");

        getReports().setDisplayColumns(1, "User Group",
        		fcp_Cons_Hold_Cntrct_Hold_Grp,"Contract#",
        		fcp_Cons_Hold_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"Payee/Code",
        		fcp_Cons_Hold_Cntrct_Payee_Cde, new AlphanumericLength (2),"User",
        		fcp_Cons_Hold_Cntrct_Hold_User_Id);
    }
}
