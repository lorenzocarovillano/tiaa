/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:01 PM
**        * FROM NATURAL PROGRAM : Iaap900a
************************************************************
**        * FILE NAME            : Iaap900a.java
**        * CLASS NAME           : Iaap900a
**        * INSTANCE NAME        : Iaap900a
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: IA ADMIN CORE FILE
**SAG SYSTEM: PAS
**SAG GDA: IADG000
**SAG DESCS(1): THIS PROGRAM ...
**SAG PRIMARY-FILE: COR-XREF-FILE
**SAG PRIMARY-KEY: COR-SUPER-LOB-CNTRCT-PAYEE
************************************************************************
* PROGRAM  : IAAP900A
* SYSTEM   : PAS
* TITLE    : IA ADMIN CORE FILE
* GENERATED: OCT 25,95
* FUNCTION : THIS PROGRAM FORMATS THE COR ETL FILE
* HISTORY
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap900a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Input1;

    private DbsGroup pnd_Work_Input1__R_Field_1;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Ppcn;
    private DbsField pnd_Work_Input1_Pnd_Wk_Pin;

    private DbsGroup pnd_Work_Input1__R_Field_2;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Pin;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Pin_5;
    private DbsField pnd_Work_Input1_Pnd_Wk1_Other;
    private DbsField pnd_Work_Out2;

    private DbsGroup pnd_Work_Out2__R_Field_3;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Ppcn;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Pin_Out;

    private DbsGroup pnd_Work_Out2__R_Field_4;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Pin_Out_N;
    private DbsField pnd_Work_Out2_Pnd_Wk2_Other_Out;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Input1 = localVariables.newFieldInRecord("pnd_Work_Input1", "#WORK-INPUT1", FieldType.STRING, 143);

        pnd_Work_Input1__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Input1__R_Field_1", "REDEFINE", pnd_Work_Input1);
        pnd_Work_Input1_Pnd_Wk1_Ppcn = pnd_Work_Input1__R_Field_1.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Ppcn", "#WK1-PPCN", FieldType.STRING, 12);
        pnd_Work_Input1_Pnd_Wk_Pin = pnd_Work_Input1__R_Field_1.newFieldInGroup("pnd_Work_Input1_Pnd_Wk_Pin", "#WK-PIN", FieldType.STRING, 12);

        pnd_Work_Input1__R_Field_2 = pnd_Work_Input1__R_Field_1.newGroupInGroup("pnd_Work_Input1__R_Field_2", "REDEFINE", pnd_Work_Input1_Pnd_Wk_Pin);
        pnd_Work_Input1_Pnd_Wk1_Pin = pnd_Work_Input1__R_Field_2.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Pin", "#WK1-PIN", FieldType.STRING, 7);
        pnd_Work_Input1_Pnd_Wk1_Pin_5 = pnd_Work_Input1__R_Field_2.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Pin_5", "#WK1-PIN-5", FieldType.STRING, 5);
        pnd_Work_Input1_Pnd_Wk1_Other = pnd_Work_Input1__R_Field_1.newFieldInGroup("pnd_Work_Input1_Pnd_Wk1_Other", "#WK1-OTHER", FieldType.STRING, 119);
        pnd_Work_Out2 = localVariables.newFieldInRecord("pnd_Work_Out2", "#WORK-OUT2", FieldType.STRING, 143);

        pnd_Work_Out2__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Out2__R_Field_3", "REDEFINE", pnd_Work_Out2);
        pnd_Work_Out2_Pnd_Wk2_Ppcn = pnd_Work_Out2__R_Field_3.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Ppcn", "#WK2-PPCN", FieldType.STRING, 12);
        pnd_Work_Out2_Pnd_Wk2_Pin_Out = pnd_Work_Out2__R_Field_3.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Pin_Out", "#WK2-PIN-OUT", FieldType.STRING, 12);

        pnd_Work_Out2__R_Field_4 = pnd_Work_Out2__R_Field_3.newGroupInGroup("pnd_Work_Out2__R_Field_4", "REDEFINE", pnd_Work_Out2_Pnd_Wk2_Pin_Out);
        pnd_Work_Out2_Pnd_Wk2_Pin_Out_N = pnd_Work_Out2__R_Field_4.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Pin_Out_N", "#WK2-PIN-OUT-N", FieldType.NUMERIC, 
            12);
        pnd_Work_Out2_Pnd_Wk2_Other_Out = pnd_Work_Out2__R_Field_3.newFieldInGroup("pnd_Work_Out2_Pnd_Wk2_Other_Out", "#WK2-OTHER-OUT", FieldType.STRING, 
            119);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap900a() throws Exception
    {
        super("Iaap900a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  COR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-INPUT1
        while (condition(getWorkFiles().read(1, pnd_Work_Input1)))
        {
            pnd_Work_Out2_Pnd_Wk2_Ppcn.setValue(pnd_Work_Input1_Pnd_Wk1_Ppcn);                                                                                            //Natural: MOVE #WK1-PPCN TO #WK2-PPCN
            //*  IF #WK1-PIN = ' '                              /* 082017 START
            //*    MOVE '0000000' TO #WK1-PIN
            //*  END-IF
            //*  MOVE #WK1-PIN    TO #WK2-PIN-OUT
            short decideConditionsMet51 = 0;                                                                                                                              //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK1-PIN = ' '
            if (condition(pnd_Work_Input1_Pnd_Wk1_Pin.equals(" ")))
            {
                decideConditionsMet51++;
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.setValue(0);                                                                                                              //Natural: ASSIGN #WK2-PIN-OUT-N := 0
            }                                                                                                                                                             //Natural: WHEN #WK1-PIN-5 = ' ' AND #WK1-PIN IS ( N7 )
            else if (condition(pnd_Work_Input1_Pnd_Wk1_Pin_5.equals(" ") && DbsUtil.is(pnd_Work_Input1_Pnd_Wk1_Pin.getText(),"N7")))
            {
                decideConditionsMet51++;
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out2_Pnd_Wk2_Pin_Out_N), pnd_Work_Input1_Pnd_Wk1_Pin.val());                //Natural: ASSIGN #WK2-PIN-OUT-N := VAL ( #WK1-PIN )
            }                                                                                                                                                             //Natural: WHEN #WK-PIN IS ( N12 )
            else if (condition(DbsUtil.is(pnd_Work_Input1_Pnd_Wk_Pin.getText(),"N12")))
            {
                decideConditionsMet51++;
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.compute(new ComputeParameters(false, pnd_Work_Out2_Pnd_Wk2_Pin_Out_N), pnd_Work_Input1_Pnd_Wk_Pin.val());                 //Natural: ASSIGN #WK2-PIN-OUT-N := VAL ( #WK-PIN )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Out2_Pnd_Wk2_Pin_Out_N.reset();                                                                                                                  //Natural: RESET #WK2-PIN-OUT-N
                //*  082017 END
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Work_Out2_Pnd_Wk2_Other_Out.setValue(pnd_Work_Input1_Pnd_Wk1_Other);                                                                                      //Natural: MOVE #WK1-OTHER TO #WK2-OTHER-OUT
            getWorkFiles().write(2, false, pnd_Work_Out2);                                                                                                                //Natural: WRITE WORK FILE 2 #WORK-OUT2
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
