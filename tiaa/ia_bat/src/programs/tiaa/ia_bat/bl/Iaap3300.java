/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:59 PM
**        * FROM NATURAL PROGRAM : Iaap3300
************************************************************
**        * FILE NAME            : Iaap3300.java
**        * CLASS NAME           : Iaap3300
**        * INSTANCE NAME        : Iaap3300
************************************************************
************************************************************************
* PROGRAM  : IAAP3300
* SYSTEM   : IA
* GENERATED: SEP 15,10
* FUNCTION : SPLIT THE IA MASTER FILES INTO PENSION, NON-PENSION AND
*            TC LIFE CONTRACTS.
* INPUT    : IA MASTER MONTHLY EXTRACT
*
* HISTORY
* 04/05/12 O. SOTTO - RATE BASE CHANGES. SC 040512.
* 04/2017  O. SOTTO - PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3300 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_T_6;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_Rsdncy_At_Issue;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Input_Pnd_W1_Cntrct_At_Iss_Re;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input__Filler14;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input__Filler15;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input__Filler16;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler17;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input__Filler18;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input__Filler19;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input__Filler20;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input__Filler21;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input__Filler22;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input__Filler23;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input__Filler24;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input__Filler25;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input__Filler26;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;

    private DbsGroup pnd_W_Rec10;
    private DbsField pnd_W_Rec10_Pnd_Cntrct_Payee;

    private DbsGroup pnd_W_Rec10__R_Field_16;
    private DbsField pnd_W_Rec10_Pnd_Ppcn_Nbr;

    private DbsGroup pnd_W_Rec10__R_Field_17;
    private DbsField pnd_W_Rec10__Filler27;
    private DbsField pnd_W_Rec10_Pnd_T_6;
    private DbsField pnd_W_Rec10_Pnd_Payee_Cde;

    private DbsGroup pnd_W_Rec10__R_Field_18;
    private DbsField pnd_W_Rec10_Pnd_Payee_Cde_A;
    private DbsField pnd_W_Rec10_Pnd_Record_Code;
    private DbsField pnd_W_Rec10_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_W_Rec10__R_Field_19;
    private DbsField pnd_W_Rec10_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_W_Rec10__R_Field_20;
    private DbsField pnd_W_Rec10_Pnd_Optn_Cde;
    private DbsField pnd_W_Rec10_Pnd_Orgn_Cde;
    private DbsField pnd_W_Rec10__Filler28;
    private DbsField pnd_W_Rec10_Pnd_Issue_Dte;

    private DbsGroup pnd_W_Rec10__R_Field_21;
    private DbsField pnd_W_Rec10_Pnd_Issue_Dte_A;
    private DbsField pnd_W_Rec10_Pnd_1st_Due_Dte;
    private DbsField pnd_W_Rec10_Pnd_1st_Pd_Dte;
    private DbsField pnd_W_Rec10_Pnd_Crrncy_Cde;

    private DbsGroup pnd_W_Rec10__R_Field_22;
    private DbsField pnd_W_Rec10_Pnd_Crrncy_Cde_A;
    private DbsField pnd_W_Rec10_Pnd_Type_Cde;
    private DbsField pnd_W_Rec10__Filler29;
    private DbsField pnd_W_Rec10_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_W_Rec10__Filler30;
    private DbsField pnd_W_Rec10_Pnd_Rsdncy_At_Issue;
    private DbsField pnd_W_Rec10__Filler31;
    private DbsField pnd_W_Rec10_Pnd_First_Ann_Dob;
    private DbsField pnd_W_Rec10__Filler32;
    private DbsField pnd_W_Rec10_Pnd_First_Ann_Dod;
    private DbsField pnd_W_Rec10__Filler33;
    private DbsField pnd_W_Rec10_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_W_Rec10__Filler34;
    private DbsField pnd_W_Rec10_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_W_Rec10__Filler35;
    private DbsField pnd_W_Rec10_Pnd_Div_Coll_Cde;
    private DbsField pnd_W_Rec10__Filler36;
    private DbsField pnd_W_Rec10_Pnd_Lst_Trans_Dte;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_Type;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_At_Iss_Re;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_W_Rec10_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_Strt_Dte;
    private DbsField pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd;

    private DbsGroup pnd_W_Rec10__R_Field_23;
    private DbsField pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd_A;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_W_Rec10_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_W_Rec10_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Non_Pension_Origin;
    private DbsField pnd_Non_Pension;
    private DbsField pnd_Tclife;
    private DbsField pnd_A;
    private DbsField pnd_Bypass;
    private DbsField pnd_Non_Psn;
    private DbsField pnd_Psn;
    private DbsField pnd_Tc;
    private DbsField pnd_Rec10_Written;
    private DbsField pnd_Tot_Rec;
    private DbsField pnd_Tot_Rec10;
    private DbsField pnd_Rec10;
    private DbsField pnd_Tot_Rec20;
    private DbsField pnd_Rec20;
    private DbsField pnd_Tot_Rec30;
    private DbsField pnd_Rec30;
    private DbsField pnd_In_Cnt;
    private DbsField pnd_Bypass_Cnt;
    private DbsField pnd_Out_Np_Cnt;
    private DbsField pnd_Out_P_Cnt;
    private DbsField pnd_Out_Tc_Cnt;
    private DbsField pnd_W_Iss_Dte;

    private DbsGroup pnd_W_Iss_Dte__R_Field_24;
    private DbsField pnd_W_Iss_Dte_Pnd_W_Iss_Dte_N;
    private DbsField pnd_W_Cntrct_Pyee;

    private DbsGroup pnd_W_Cntrct_Pyee__R_Field_25;
    private DbsField pnd_W_Cntrct_Pyee_Pnd_W_Cntrct;
    private DbsField pnd_W_Cntrct_Pyee_Pnd_W_Payee;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Ppcn_Nbr);
        pnd_Input__Filler1 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_T_6 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_T_6", "#T-6", FieldType.STRING, 6);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input__Filler2 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input__Filler3 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 9);
        pnd_Input_Pnd_Rsdncy_At_Issue = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Rsdncy_At_Issue", "#RSDNCY-AT-ISSUE", FieldType.STRING, 3);
        pnd_Input__Filler5 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler6 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler7 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler8 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler9 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input__Filler10 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_W1_Cntrct_Type = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_W1_Cntrct_At_Iss_Re = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_At_Iss_Re", "#W1-CNTRCT-AT-ISS-RE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_5.newFieldArrayInGroup("pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte", "#W1-CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_W1_Cntrct_Strt_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Strt_Dte", "#W1-CNTRCT-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Cntrct_Issue_Dte_Dd);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A", "#CNTRCT-ISSUE-DTE-DD-A", FieldType.STRING, 
            2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_9 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input__Filler11 = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_9.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_9.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input__Filler12 = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_12 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler13 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_12.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input__Filler14 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input__Filler14", "_FILLER14", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_14 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input__Filler15 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler15", "_FILLER15", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input__Filler16 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler16", "_FILLER16", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler17 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler17", "_FILLER17", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input__Filler18 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler18", "_FILLER18", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input__Filler19 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler19", "_FILLER19", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input__Filler20 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler20", "_FILLER20", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input__Filler21 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler21", "_FILLER21", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input__Filler22 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler22", "_FILLER22", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_14.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input__Filler23 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler23", "_FILLER23", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input__Filler24 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler24", "_FILLER24", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input__Filler25 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler25", "_FILLER25", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input__Filler26 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler26", "_FILLER26", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        pnd_W_Rec10 = localVariables.newGroupInRecord("pnd_W_Rec10", "#W-REC10");
        pnd_W_Rec10_Pnd_Cntrct_Payee = pnd_W_Rec10.newFieldInGroup("pnd_W_Rec10_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_W_Rec10__R_Field_16 = pnd_W_Rec10.newGroupInGroup("pnd_W_Rec10__R_Field_16", "REDEFINE", pnd_W_Rec10_Pnd_Cntrct_Payee);
        pnd_W_Rec10_Pnd_Ppcn_Nbr = pnd_W_Rec10__R_Field_16.newFieldInGroup("pnd_W_Rec10_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);

        pnd_W_Rec10__R_Field_17 = pnd_W_Rec10__R_Field_16.newGroupInGroup("pnd_W_Rec10__R_Field_17", "REDEFINE", pnd_W_Rec10_Pnd_Ppcn_Nbr);
        pnd_W_Rec10__Filler27 = pnd_W_Rec10__R_Field_17.newFieldInGroup("pnd_W_Rec10__Filler27", "_FILLER27", FieldType.STRING, 1);
        pnd_W_Rec10_Pnd_T_6 = pnd_W_Rec10__R_Field_17.newFieldInGroup("pnd_W_Rec10_Pnd_T_6", "#T-6", FieldType.STRING, 6);
        pnd_W_Rec10_Pnd_Payee_Cde = pnd_W_Rec10__R_Field_16.newFieldInGroup("pnd_W_Rec10_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_W_Rec10__R_Field_18 = pnd_W_Rec10__R_Field_16.newGroupInGroup("pnd_W_Rec10__R_Field_18", "REDEFINE", pnd_W_Rec10_Pnd_Payee_Cde);
        pnd_W_Rec10_Pnd_Payee_Cde_A = pnd_W_Rec10__R_Field_18.newFieldInGroup("pnd_W_Rec10_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_W_Rec10_Pnd_Record_Code = pnd_W_Rec10.newFieldInGroup("pnd_W_Rec10_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_W_Rec10_Pnd_Rest_Of_Record_353 = pnd_W_Rec10.newFieldArrayInGroup("pnd_W_Rec10_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_W_Rec10__R_Field_19 = pnd_W_Rec10.newGroupInGroup("pnd_W_Rec10__R_Field_19", "REDEFINE", pnd_W_Rec10_Pnd_Rest_Of_Record_353);
        pnd_W_Rec10_Pnd_Header_Chk_Dte = pnd_W_Rec10__R_Field_19.newFieldInGroup("pnd_W_Rec10_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 
            8);

        pnd_W_Rec10__R_Field_20 = pnd_W_Rec10.newGroupInGroup("pnd_W_Rec10__R_Field_20", "REDEFINE", pnd_W_Rec10_Pnd_Rest_Of_Record_353);
        pnd_W_Rec10_Pnd_Optn_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_W_Rec10_Pnd_Orgn_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_W_Rec10__Filler28 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler28", "_FILLER28", FieldType.STRING, 2);
        pnd_W_Rec10_Pnd_Issue_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_W_Rec10__R_Field_21 = pnd_W_Rec10__R_Field_20.newGroupInGroup("pnd_W_Rec10__R_Field_21", "REDEFINE", pnd_W_Rec10_Pnd_Issue_Dte);
        pnd_W_Rec10_Pnd_Issue_Dte_A = pnd_W_Rec10__R_Field_21.newFieldInGroup("pnd_W_Rec10_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_W_Rec10_Pnd_1st_Due_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_W_Rec10_Pnd_1st_Pd_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_W_Rec10_Pnd_Crrncy_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_W_Rec10__R_Field_22 = pnd_W_Rec10__R_Field_20.newGroupInGroup("pnd_W_Rec10__R_Field_22", "REDEFINE", pnd_W_Rec10_Pnd_Crrncy_Cde);
        pnd_W_Rec10_Pnd_Crrncy_Cde_A = pnd_W_Rec10__R_Field_22.newFieldInGroup("pnd_W_Rec10_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_W_Rec10_Pnd_Type_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_W_Rec10__Filler29 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler29", "_FILLER29", FieldType.STRING, 1);
        pnd_W_Rec10_Pnd_Pnsn_Pln_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_W_Rec10__Filler30 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler30", "_FILLER30", FieldType.STRING, 9);
        pnd_W_Rec10_Pnd_Rsdncy_At_Issue = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Rsdncy_At_Issue", "#RSDNCY-AT-ISSUE", FieldType.STRING, 
            3);
        pnd_W_Rec10__Filler31 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler31", "_FILLER31", FieldType.STRING, 9);
        pnd_W_Rec10_Pnd_First_Ann_Dob = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 
            8);
        pnd_W_Rec10__Filler32 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler32", "_FILLER32", FieldType.STRING, 6);
        pnd_W_Rec10_Pnd_First_Ann_Dod = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_W_Rec10__Filler33 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler33", "_FILLER33", FieldType.STRING, 9);
        pnd_W_Rec10_Pnd_Scnd_Ann_Dob = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_W_Rec10__Filler34 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler34", "_FILLER34", FieldType.STRING, 5);
        pnd_W_Rec10_Pnd_Scnd_Ann_Dod = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_W_Rec10__Filler35 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler35", "_FILLER35", FieldType.STRING, 11);
        pnd_W_Rec10_Pnd_Div_Coll_Cde = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_W_Rec10__Filler36 = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10__Filler36", "_FILLER36", FieldType.STRING, 5);
        pnd_W_Rec10_Pnd_Lst_Trans_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_W_Rec10_Pnd_W1_Cntrct_Type = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_W_Rec10_Pnd_W1_Cntrct_At_Iss_Re = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_At_Iss_Re", "#W1-CNTRCT-AT-ISS-RE", FieldType.STRING, 
            3);
        pnd_W_Rec10_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_W_Rec10__R_Field_20.newFieldArrayInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_Fnl_Prm_Dte", "#W1-CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5));
        pnd_W_Rec10_Pnd_Cntrct_Mtch_Ppcn = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_W_Rec10_Pnd_W1_Cntrct_Strt_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_Strt_Dte", "#W1-CNTRCT-STRT-DTE", FieldType.DATE);
        pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Rec10__R_Field_23 = pnd_W_Rec10__R_Field_20.newGroupInGroup("pnd_W_Rec10__R_Field_23", "REDEFINE", pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd);
        pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd_A = pnd_W_Rec10__R_Field_23.newFieldInGroup("pnd_W_Rec10_Pnd_Cntrct_Issue_Dte_Dd_A", "#CNTRCT-ISSUE-DTE-DD-A", 
            FieldType.STRING, 2);
        pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_W_Rec10_Pnd_Roth_Frst_Cntrb_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_W_Rec10_Pnd_Roth_Ssnng_Dte = pnd_W_Rec10__R_Field_20.newFieldInGroup("pnd_W_Rec10_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Non_Pension_Origin = localVariables.newFieldArrayInRecord("pnd_Non_Pension_Origin", "#NON-PENSION-ORIGIN", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            12));
        pnd_Non_Pension = localVariables.newFieldArrayInRecord("pnd_Non_Pension", "#NON-PENSION", FieldType.NUMERIC, 2, new DbsArrayController(1, 3));
        pnd_Tclife = localVariables.newFieldArrayInRecord("pnd_Tclife", "#TCLIFE", FieldType.NUMERIC, 2, new DbsArrayController(1, 9));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 5);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Non_Psn = localVariables.newFieldInRecord("pnd_Non_Psn", "#NON-PSN", FieldType.BOOLEAN, 1);
        pnd_Psn = localVariables.newFieldInRecord("pnd_Psn", "#PSN", FieldType.BOOLEAN, 1);
        pnd_Tc = localVariables.newFieldInRecord("pnd_Tc", "#TC", FieldType.BOOLEAN, 1);
        pnd_Rec10_Written = localVariables.newFieldInRecord("pnd_Rec10_Written", "#REC10-WRITTEN", FieldType.BOOLEAN, 1);
        pnd_Tot_Rec = localVariables.newFieldInRecord("pnd_Tot_Rec", "#TOT-REC", FieldType.PACKED_DECIMAL, 11);
        pnd_Tot_Rec10 = localVariables.newFieldInRecord("pnd_Tot_Rec10", "#TOT-REC10", FieldType.PACKED_DECIMAL, 11);
        pnd_Rec10 = localVariables.newFieldInRecord("pnd_Rec10", "#REC10", FieldType.PACKED_DECIMAL, 11);
        pnd_Tot_Rec20 = localVariables.newFieldInRecord("pnd_Tot_Rec20", "#TOT-REC20", FieldType.PACKED_DECIMAL, 11);
        pnd_Rec20 = localVariables.newFieldInRecord("pnd_Rec20", "#REC20", FieldType.PACKED_DECIMAL, 11);
        pnd_Tot_Rec30 = localVariables.newFieldInRecord("pnd_Tot_Rec30", "#TOT-REC30", FieldType.PACKED_DECIMAL, 11);
        pnd_Rec30 = localVariables.newFieldInRecord("pnd_Rec30", "#REC30", FieldType.PACKED_DECIMAL, 11);
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Bypass_Cnt = localVariables.newFieldInRecord("pnd_Bypass_Cnt", "#BYPASS-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Out_Np_Cnt = localVariables.newFieldInRecord("pnd_Out_Np_Cnt", "#OUT-NP-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Out_P_Cnt = localVariables.newFieldInRecord("pnd_Out_P_Cnt", "#OUT-P-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Out_Tc_Cnt = localVariables.newFieldInRecord("pnd_Out_Tc_Cnt", "#OUT-TC-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_W_Iss_Dte = localVariables.newFieldInRecord("pnd_W_Iss_Dte", "#W-ISS-DTE", FieldType.STRING, 8);

        pnd_W_Iss_Dte__R_Field_24 = localVariables.newGroupInRecord("pnd_W_Iss_Dte__R_Field_24", "REDEFINE", pnd_W_Iss_Dte);
        pnd_W_Iss_Dte_Pnd_W_Iss_Dte_N = pnd_W_Iss_Dte__R_Field_24.newFieldInGroup("pnd_W_Iss_Dte_Pnd_W_Iss_Dte_N", "#W-ISS-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_W_Cntrct_Pyee = localVariables.newFieldInRecord("pnd_W_Cntrct_Pyee", "#W-CNTRCT-PYEE", FieldType.STRING, 12);

        pnd_W_Cntrct_Pyee__R_Field_25 = localVariables.newGroupInRecord("pnd_W_Cntrct_Pyee__R_Field_25", "REDEFINE", pnd_W_Cntrct_Pyee);
        pnd_W_Cntrct_Pyee_Pnd_W_Cntrct = pnd_W_Cntrct_Pyee__R_Field_25.newFieldInGroup("pnd_W_Cntrct_Pyee_Pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 
            10);
        pnd_W_Cntrct_Pyee_Pnd_W_Payee = pnd_W_Cntrct_Pyee__R_Field_25.newFieldInGroup("pnd_W_Cntrct_Pyee_Pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Non_Pension_Origin.getValue(1).setInitialValue(3);
        pnd_Non_Pension_Origin.getValue(2).setInitialValue(17);
        pnd_Non_Pension_Origin.getValue(3).setInitialValue(18);
        pnd_Non_Pension_Origin.getValue(4).setInitialValue(35);
        pnd_Non_Pension_Origin.getValue(5).setInitialValue(36);
        pnd_Non_Pension_Origin.getValue(6).setInitialValue(37);
        pnd_Non_Pension_Origin.getValue(7).setInitialValue(38);
        pnd_Non_Pension_Origin.getValue(8).setInitialValue(40);
        pnd_Non_Pension_Origin.getValue(9).setInitialValue(43);
        pnd_Non_Pension_Origin.getValue(10).setInitialValue(44);
        pnd_Non_Pension_Origin.getValue(11).setInitialValue(45);
        pnd_Non_Pension_Origin.getValue(12).setInitialValue(46);
        pnd_Non_Pension.getValue(1).setInitialValue(3);
        pnd_Non_Pension.getValue(2).setInitialValue(17);
        pnd_Non_Pension.getValue(3).setInitialValue(18);
        pnd_Tclife.getValue(1).setInitialValue(35);
        pnd_Tclife.getValue(2).setInitialValue(36);
        pnd_Tclife.getValue(3).setInitialValue(37);
        pnd_Tclife.getValue(4).setInitialValue(38);
        pnd_Tclife.getValue(5).setInitialValue(40);
        pnd_Tclife.getValue(6).setInitialValue(43);
        pnd_Tclife.getValue(7).setInitialValue(44);
        pnd_Tclife.getValue(8).setInitialValue(45);
        pnd_Tclife.getValue(9).setInitialValue(46);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3300() throws Exception
    {
        super("Iaap3300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            pnd_Tot_Rec.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOT-REC
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #INPUT.#PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                               //Natural: IF #INPUT.#PPCN-NBR = '   CHEADER'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-PENSION-EXTRACT
                    sub_Write_Pension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-TCLIFE-EXTRACT
                    sub_Write_Tclife_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-NONPENSION-EXTRACT
                    sub_Write_Nonpension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Out_P_Cnt.reset();                                                                                                                                //Natural: RESET #OUT-P-CNT #OUT-TC-CNT #OUT-NP-CNT
                    pnd_Out_Tc_Cnt.reset();
                    pnd_Out_Np_Cnt.reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20) || pnd_Input_Pnd_Record_Code.equals(30))))                       //Natural: ACCEPT IF #INPUT.#RECORD-CODE = 10 OR = 20 OR = 30
            {
                continue;
            }
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
            short decideConditionsMet292 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INPUT.#RECORD-CODE = 10
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))
            {
                decideConditionsMet292++;
                pnd_Rec10_Written.reset();                                                                                                                                //Natural: RESET #REC10-WRITTEN
                pnd_Tot_Rec10.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOT-REC10
                pnd_W_Cntrct_Pyee.setValue(pnd_Input_Pnd_Cntrct_Payee);                                                                                                   //Natural: ASSIGN #W-CNTRCT-PYEE := #INPUT.#CNTRCT-PAYEE
                pnd_W_Rec10.setValuesByName(pnd_Input);                                                                                                                   //Natural: MOVE BY NAME #INPUT TO #W-REC10
                pnd_Non_Psn.reset();                                                                                                                                      //Natural: RESET #NON-PSN #PSN #TC
                pnd_Psn.reset();
                pnd_Tc.reset();
                if (condition(pnd_Input_Pnd_Cntrct_Issue_Dte_Dd.equals(getZero())))                                                                                       //Natural: IF #INPUT.#CNTRCT-ISSUE-DTE-DD = 0
                {
                    pnd_Input_Pnd_Cntrct_Issue_Dte_Dd.setValue(1);                                                                                                        //Natural: ASSIGN #INPUT.#CNTRCT-ISSUE-DTE-DD := 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Iss_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Pnd_Issue_Dte_A, pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A));                  //Natural: COMPRESS #INPUT.#ISSUE-DTE-A #INPUT.#CNTRCT-ISSUE-DTE-DD-A INTO #W-ISS-DTE LEAVING NO
                if (condition(pnd_Input_Pnd_Orgn_Cde.equals(pnd_Non_Pension_Origin.getValue("*"))))                                                                       //Natural: IF #INPUT.#ORGN-CDE = #NON-PENSION-ORIGIN ( * )
                {
                    if (condition(pnd_Input_Pnd_Orgn_Cde.equals(3) && pnd_W_Iss_Dte_Pnd_W_Iss_Dte_N.lessOrEqual(19861231)))                                               //Natural: IF #INPUT.#ORGN-CDE = 03 AND #W-ISS-DTE-N LE 19861231
                    {
                        pnd_Psn.setValue(true);                                                                                                                           //Natural: ASSIGN #PSN := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Input_Pnd_Orgn_Cde.equals(pnd_Non_Pension.getValue("*"))))                                                                      //Natural: IF #INPUT.#ORGN-CDE = #NON-PENSION ( * )
                        {
                            pnd_Non_Psn.setValue(true);                                                                                                                   //Natural: ASSIGN #NON-PSN := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tc.setValue(true);                                                                                                                        //Natural: ASSIGN #TC := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Psn.setValue(true);                                                                                                                               //Natural: ASSIGN #PSN := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #INPUT.#RECORD-CODE = 20
            else if (condition(pnd_Input_Pnd_Record_Code.equals(20)))
            {
                decideConditionsMet292++;
                pnd_Tot_Rec20.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOT-REC20
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                if (condition(pnd_Input_Pnd_Pend_Cde.equals("0") || pnd_Input_Pnd_Pend_Cde.equals(" ") || pnd_Input_Pnd_Pend_Cde.equals("I") || pnd_Input_Pnd_Pend_Cde.equals("K")  //Natural: IF ( #PEND-CDE EQ '0' OR = ' ' OR = 'I' OR = 'K' OR = 'M' ) OR #STATUS-CDE = 9
                    || pnd_Input_Pnd_Pend_Cde.equals("M") || pnd_Input_Pnd_Status_Cde.equals(9)))
                {
                    //*          OR #CPR-ID-NBR = 0 OR = 9999999
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                    //*  THIS REC 20  BYPASSED
                    pnd_Bypass_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CNT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rec20.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC20
                short decideConditionsMet327 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PSN
                if (condition(pnd_Psn.getBoolean()))
                {
                    decideConditionsMet327++;
                                                                                                                                                                          //Natural: PERFORM WRITE-REC10
                    sub_Write_Rec10();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-PENSION-EXTRACT
                    sub_Write_Pension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #TC
                else if (condition(pnd_Tc.getBoolean()))
                {
                    decideConditionsMet327++;
                                                                                                                                                                          //Natural: PERFORM WRITE-REC10
                    sub_Write_Rec10();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-TCLIFE-EXTRACT
                    sub_Write_Tclife_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #NON-PSN
                else if (condition(pnd_Non_Psn.getBoolean()))
                {
                    decideConditionsMet327++;
                                                                                                                                                                          //Natural: PERFORM WRITE-REC10
                    sub_Write_Rec10();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-NONPENSION-EXTRACT
                    sub_Write_Nonpension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    getReports().write(0, "Logic error.");                                                                                                                //Natural: WRITE 'Logic error.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                    //*  RECORD 30
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Tot_Rec30.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOT-REC30
                if (condition(pnd_Bypass.getBoolean()))                                                                                                                   //Natural: IF #BYPASS
                {
                    pnd_Bypass_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CNT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rec30.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC30
                short decideConditionsMet349 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PSN
                if (condition(pnd_Psn.getBoolean()))
                {
                    decideConditionsMet349++;
                                                                                                                                                                          //Natural: PERFORM WRITE-PENSION-EXTRACT
                    sub_Write_Pension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #TC
                else if (condition(pnd_Tc.getBoolean()))
                {
                    decideConditionsMet349++;
                                                                                                                                                                          //Natural: PERFORM WRITE-TCLIFE-EXTRACT
                    sub_Write_Tclife_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #NON-PSN
                else if (condition(pnd_Non_Psn.getBoolean()))
                {
                    decideConditionsMet349++;
                                                                                                                                                                          //Natural: PERFORM WRITE-NONPENSION-EXTRACT
                    sub_Write_Nonpension_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    getReports().write(0, "Logic error.");                                                                                                                //Natural: WRITE 'Logic error.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Bypass_Cnt.compute(new ComputeParameters(false, pnd_Bypass_Cnt), pnd_Bypass_Cnt.add((pnd_Tot_Rec10.subtract(pnd_Rec10))));                                    //Natural: ASSIGN #BYPASS-CNT := #BYPASS-CNT + ( #TOT-REC10 - #REC10 )
        getReports().write(0, NEWLINE,NEWLINE,"Total records ========================>",pnd_Tot_Rec,"Total record 10, 20 ,30 ==============>",pnd_In_Cnt,                 //Natural: WRITE // 'Total records ========================>' #TOT-REC 'Total record 10, 20 ,30 ==============>' #IN-CNT 'Non-pension records ==================>' #OUT-NP-CNT 'Pension records ======================>' #OUT-P-CNT 'TC Life records ======================>' #OUT-TC-CNT 'Total Rec 10 =========================>' #TOT-REC10 'Selected Rec 10 ======================>' #REC10 'Total Rec 20 =========================>' #TOT-REC20 'Selected Rec 20 ======================>' #REC20 'Total Rec 30 =========================>' #TOT-REC30 'Selected Rec 30 ======================>' #REC30 'Bypassed records =====================>' #BYPASS-CNT
            "Non-pension records ==================>",pnd_Out_Np_Cnt,"Pension records ======================>",pnd_Out_P_Cnt,"TC Life records ======================>",
            pnd_Out_Tc_Cnt,"Total Rec 10 =========================>",pnd_Tot_Rec10,"Selected Rec 10 ======================>",pnd_Rec10,"Total Rec 20 =========================>",
            pnd_Tot_Rec20,"Selected Rec 20 ======================>",pnd_Rec20,"Total Rec 30 =========================>",pnd_Tot_Rec30,"Selected Rec 30 ======================>",
            pnd_Rec30,"Bypassed records =====================>",pnd_Bypass_Cnt);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PENSION-EXTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NONPENSION-EXTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TCLIFE-EXTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REC10
    }
    private void sub_Write_Pension_Extract() throws Exception                                                                                                             //Natural: WRITE-PENSION-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(2, false, pnd_Input);                                                                                                                        //Natural: WRITE WORK FILE 2 #INPUT
        pnd_Out_P_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #OUT-P-CNT
    }
    private void sub_Write_Nonpension_Extract() throws Exception                                                                                                          //Natural: WRITE-NONPENSION-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(3, false, pnd_Input);                                                                                                                        //Natural: WRITE WORK FILE 3 #INPUT
        pnd_Out_Np_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #OUT-NP-CNT
    }
    private void sub_Write_Tclife_Extract() throws Exception                                                                                                              //Natural: WRITE-TCLIFE-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(4, false, pnd_Input);                                                                                                                        //Natural: WRITE WORK FILE 4 #INPUT
        pnd_Out_Tc_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #OUT-TC-CNT
    }
    private void sub_Write_Rec10() throws Exception                                                                                                                       //Natural: WRITE-REC10
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Rec10_Written.getBoolean())))                                                                                                                //Natural: IF NOT #REC10-WRITTEN
        {
            pnd_Rec10_Written.setValue(true);                                                                                                                             //Natural: ASSIGN #REC10-WRITTEN := TRUE
            pnd_Rec10.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #REC10
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet400 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PSN
        if (condition(pnd_Psn.getBoolean()))
        {
            decideConditionsMet400++;
            getWorkFiles().write(2, false, pnd_W_Rec10);                                                                                                                  //Natural: WRITE WORK FILE 2 #W-REC10
            pnd_Out_P_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #OUT-P-CNT
        }                                                                                                                                                                 //Natural: WHEN #TC
        else if (condition(pnd_Tc.getBoolean()))
        {
            decideConditionsMet400++;
            getWorkFiles().write(4, false, pnd_W_Rec10);                                                                                                                  //Natural: WRITE WORK FILE 4 #W-REC10
            pnd_Out_Tc_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #OUT-TC-CNT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getWorkFiles().write(3, false, pnd_W_Rec10);                                                                                                                  //Natural: WRITE WORK FILE 3 #W-REC10
            pnd_Out_Np_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #OUT-NP-CNT
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
