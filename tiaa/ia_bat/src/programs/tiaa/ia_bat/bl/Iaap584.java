/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:59 PM
**        * FROM NATURAL PROGRAM : Iaap584
************************************************************
**        * FILE NAME            : Iaap584.java
**        * CLASS NAME           : Iaap584
**        * INSTANCE NAME        : Iaap584
************************************************************
*****************************************************************
* YR2000 COMPLIANT FIX APPLIED  ---> A MURTHY 08/06/99          *
*                      LAST PCY STOW       ON 01/04/99 14:33:53 *
*                      REVIEWED BY   ED    ON 01/26/00          *
*****************************************************************
**Y2CHAM CP IMNDLP
*
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP584       PRODUCES DAILY GROSS OVERPAYMENT      *
*      DATE   -  03/95         REPORTS FOR EACH TYPE OF ACCOUNT      *
*    AUTHOR   -  ARI G.        EX. TIAA, CREF STOCK, CREF GLOBAL ETC.*
*
*   HISTORY   -  07/20/95      ADDED REAL ESTATE ACCOUNT TO REPORT.  *
*             -  06/20/96      ADDED BOND ACCOUNT AND CHANGED REPORTS*
*                              TO BE BASED ON FUND CODE NOT CNT #.   *
*             -  08/20/96      REWROTE PROGRAM TO MAKE REPORT FUND   *
*                              BASED. REMOVED PARTS OF PROGRAM THAT  *
*                              ARE HARD CODED (WITH THE EXCEPTION OF *
*                              OF TIAA TRADITIONAL)                  *
*             -  05/13/02      CHANGE IAAN050A TO IAAN051A
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap584 extends BLNatBase
{
    // Data Areas
    private LdaIaal584w ldaIaal584w;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cref_Fund;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_1;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_W_Fund_Code_Tot;

    private DbsGroup pnd_W_Fund_Code_Tot__R_Field_2;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2;

    private DbsGroup pnd_W2_File;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code;

    private DbsGroup pnd_W2_File__R_Field_3;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_1;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_2_3;
    private DbsField pnd_W2_File_Ovrpymnt_Ppcn_Nbr;
    private DbsField pnd_W2_File_Pnd_W2_Payee_Cde_Alpha;
    private DbsField pnd_W2_File_Ovrpymnt_Status_Timestamp;
    private DbsField pnd_W2_File_Ovrpymnt_Begin_Fiscal_Mo;
    private DbsField pnd_W2_File_Ovrpymnt_Begin_Fiscal_Yr;
    private DbsField pnd_W2_File_Ovrpymnt_End_Fiscal_Mo;
    private DbsField pnd_W2_File_Ovrpymnt_End_Fiscal_Yr;
    private DbsField pnd_W2_File_Ovrpymnt_Nbr;
    private DbsField pnd_W2_File_Ovrpymnt_Dcdnt_Per_Amt;
    private DbsField pnd_W2_File_Ovrpymnt_Ovr_Per_Amt;
    private DbsField pnd_W2_File_Ovrpymnt_Optn_Cde;
    private DbsField pnd_W2_File_Ovrpymnt_Pymnt_Mode;
    private DbsField pnd_W2_File_Ovrpymnt_Last_Pymnt_Rcvd_Date;
    private DbsField pnd_W2_File_Pnd_W2_Num_Ovrpy_Yrs;
    private DbsField pnd_Len;
    private DbsField pnd_Num_Of_Fund;
    private DbsField pnd_Error;
    private DbsField pnd_Write_Out_Contract;
    private DbsField pnd_Grand_Tot_Ovrpy;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_Fund_Desc;
    private DbsField pnd_First_Time_Thru;
    private DbsField pnd_Fund_Code_Hold;

    private DbsGroup pnd_Fund_Code_Hold__R_Field_4;
    private DbsField pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1;
    private DbsField pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3;
    private DbsField pnd_Fund_Code_1_3;

    private DbsGroup pnd_Fund_Code_1_3__R_Field_5;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_1;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3;
    private DbsField pnd_Y2_Parm_Date_Yy;

    private DbsGroup pnd_Y2_Parm_Date_Yy__R_Field_6;
    private DbsField pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal584w = new LdaIaal584w();
        registerRecord(ldaIaal584w);
        registerRecord(ldaIaal584w.getVw_iaa_Cntrct_View());
        registerRecord(ldaIaal584w.getVw_new_Tiaa_Rates());
        registerRecord(ldaIaal584w.getVw_iaa_Old_Tiaa_Rates_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cref_Fund = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund", "IAA-CREF-FUND"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS");
        iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        registerRecord(vw_iaa_Cref_Fund);

        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_W_Fund_Code_Tot = localVariables.newFieldInRecord("pnd_W_Fund_Code_Tot", "#W-FUND-CODE-TOT", FieldType.STRING, 3);

        pnd_W_Fund_Code_Tot__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Fund_Code_Tot__R_Field_2", "REDEFINE", pnd_W_Fund_Code_Tot);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1 = pnd_W_Fund_Code_Tot__R_Field_2.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2 = pnd_W_Fund_Code_Tot__R_Field_2.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2", "#W-FUND-CODE-2", 
            FieldType.STRING, 2);

        pnd_W2_File = localVariables.newGroupInRecord("pnd_W2_File", "#W2-FILE");
        pnd_W2_File_Pnd_W2_Fund_Code = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code", "#W2-FUND-CODE", FieldType.STRING, 3);

        pnd_W2_File__R_Field_3 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_3", "REDEFINE", pnd_W2_File_Pnd_W2_Fund_Code);
        pnd_W2_File_Pnd_W2_Fund_Code_1 = pnd_W2_File__R_Field_3.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_1", "#W2-FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_W2_File_Pnd_W2_Fund_Code_2_3 = pnd_W2_File__R_Field_3.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_2_3", "#W2-FUND-CODE-2-3", FieldType.STRING, 
            2);
        pnd_W2_File_Ovrpymnt_Ppcn_Nbr = pnd_W2_File.newFieldInGroup("pnd_W2_File_Ovrpymnt_Ppcn_Nbr", "OVRPYMNT-PPCN-NBR", FieldType.STRING, 10);
        pnd_W2_File_Pnd_W2_Payee_Cde_Alpha = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Payee_Cde_Alpha", "#W2-PAYEE-CDE-ALPHA", FieldType.STRING, 
            2);
        pnd_W2_File_Ovrpymnt_Status_Timestamp = pnd_W2_File.newFieldInGroup("pnd_W2_File_Ovrpymnt_Status_Timestamp", "OVRPYMNT-STATUS-TIMESTAMP", FieldType.PACKED_DECIMAL, 
            12);
        pnd_W2_File_Ovrpymnt_Begin_Fiscal_Mo = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_Begin_Fiscal_Mo", "OVRPYMNT-BEGIN-FISCAL-MO", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_Begin_Fiscal_Yr = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_Begin_Fiscal_Yr", "OVRPYMNT-BEGIN-FISCAL-YR", FieldType.NUMERIC, 
            4, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_End_Fiscal_Mo = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_End_Fiscal_Mo", "OVRPYMNT-END-FISCAL-MO", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_End_Fiscal_Yr = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_End_Fiscal_Yr", "OVRPYMNT-END-FISCAL-YR", FieldType.NUMERIC, 
            4, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_Nbr = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_Nbr", "OVRPYMNT-NBR", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            7));
        pnd_W2_File_Ovrpymnt_Dcdnt_Per_Amt = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_Dcdnt_Per_Amt", "OVRPYMNT-DCDNT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_Ovr_Per_Amt = pnd_W2_File.newFieldArrayInGroup("pnd_W2_File_Ovrpymnt_Ovr_Per_Amt", "OVRPYMNT-OVR-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 7));
        pnd_W2_File_Ovrpymnt_Optn_Cde = pnd_W2_File.newFieldInGroup("pnd_W2_File_Ovrpymnt_Optn_Cde", "OVRPYMNT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_W2_File_Ovrpymnt_Pymnt_Mode = pnd_W2_File.newFieldInGroup("pnd_W2_File_Ovrpymnt_Pymnt_Mode", "OVRPYMNT-PYMNT-MODE", FieldType.NUMERIC, 3);
        pnd_W2_File_Ovrpymnt_Last_Pymnt_Rcvd_Date = pnd_W2_File.newFieldInGroup("pnd_W2_File_Ovrpymnt_Last_Pymnt_Rcvd_Date", "OVRPYMNT-LAST-PYMNT-RCVD-DATE", 
            FieldType.NUMERIC, 8);
        pnd_W2_File_Pnd_W2_Num_Ovrpy_Yrs = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Num_Ovrpy_Yrs", "#W2-NUM-OVRPY-YRS", FieldType.NUMERIC, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Num_Of_Fund = localVariables.newFieldInRecord("pnd_Num_Of_Fund", "#NUM-OF-FUND", FieldType.NUMERIC, 6);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_Write_Out_Contract = localVariables.newFieldInRecord("pnd_Write_Out_Contract", "#WRITE-OUT-CONTRACT", FieldType.NUMERIC, 6);
        pnd_Grand_Tot_Ovrpy = localVariables.newFieldInRecord("pnd_Grand_Tot_Ovrpy", "#GRAND-TOT-OVRPY", FieldType.NUMERIC, 11, 2);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);
        pnd_First_Time_Thru = localVariables.newFieldInRecord("pnd_First_Time_Thru", "#FIRST-TIME-THRU", FieldType.STRING, 1);
        pnd_Fund_Code_Hold = localVariables.newFieldInRecord("pnd_Fund_Code_Hold", "#FUND-CODE-HOLD", FieldType.STRING, 3);

        pnd_Fund_Code_Hold__R_Field_4 = localVariables.newGroupInRecord("pnd_Fund_Code_Hold__R_Field_4", "REDEFINE", pnd_Fund_Code_Hold);
        pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1 = pnd_Fund_Code_Hold__R_Field_4.newFieldInGroup("pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1", "#FUND-CODE-HOLD-1", 
            FieldType.STRING, 1);
        pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3 = pnd_Fund_Code_Hold__R_Field_4.newFieldInGroup("pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3", "#FUND-CODE-HOLD-2-3", 
            FieldType.STRING, 2);
        pnd_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_Fund_Code_1_3", "#FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_Fund_Code_1_3__R_Field_5 = localVariables.newGroupInRecord("pnd_Fund_Code_1_3__R_Field_5", "REDEFINE", pnd_Fund_Code_1_3);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_1 = pnd_Fund_Code_1_3__R_Field_5.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3 = pnd_Fund_Code_1_3__R_Field_5.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3", "#FUND-CODE-2-3", FieldType.STRING, 
            2);
        pnd_Y2_Parm_Date_Yy = localVariables.newFieldInRecord("pnd_Y2_Parm_Date_Yy", "#Y2-PARM-DATE-YY", FieldType.STRING, 2);

        pnd_Y2_Parm_Date_Yy__R_Field_6 = localVariables.newGroupInRecord("pnd_Y2_Parm_Date_Yy__R_Field_6", "REDEFINE", pnd_Y2_Parm_Date_Yy);
        pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N = pnd_Y2_Parm_Date_Yy__R_Field_6.newFieldInGroup("pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N", "#Y2-PARM-DATE-YY-N", 
            FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cref_Fund.reset();

        ldaIaal584w.initializeValues();

        localVariables.reset();
        pnd_Len.setInitialValue(35);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap584() throws Exception
    {
        super("Iaap584");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().write(0, "*** START OF PROGRAM IAAP584 *** ");                                                                                                       //Natural: WRITE '*** START OF PROGRAM IAAP584 *** '
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, ldaIaal584w.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #SET-UP-TABLES
        sub_Pnd_Set_Up_Tables();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  START OF MAIN PROCESS
        //* ***********************************************************************
        pnd_First_Time_Thru.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #FIRST-TIME-THRU
        R1:                                                                                                                                                               //Natural: READ WORK FILE 2 #W2-FILE
        while (condition(getWorkFiles().read(2, pnd_W2_File)))
        {
            pnd_Fund_Code_1_3.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                                     //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-1-3
            //*      WRITE '=' #W2-FUND-CODE /
            if (condition(pnd_First_Time_Thru.equals("Y")))                                                                                                               //Natural: IF #FIRST-TIME-THRU = 'Y'
            {
                pnd_First_Time_Thru.setValue("N");                                                                                                                        //Natural: MOVE 'N' TO #FIRST-TIME-THRU
                pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                                //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                    sub_Pnd_Get_Desc();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W2_File_Pnd_W2_Fund_Code_1.equals("T") && pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1.equals("T")))                                             //Natural: IF #W2-FUND-CODE-1 EQ 'T' AND #FUND-CODE-HOLD-1 EQ 'T'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W2_File_Pnd_W2_Fund_Code.notEquals(pnd_Fund_Code_Hold)))                                                                                //Natural: IF #W2-FUND-CODE NE #FUND-CODE-HOLD
                {
                    if (condition(pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1.equals("T")))                                                                                   //Natural: IF #FUND-CODE-HOLD-1 = 'T'
                    {
                        if (condition(getReports().getAstLinesLeft(1).less(7)))                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 7 LINES LEFT
                        {
                            getReports().newPage(1);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>",ldaIaal584w.getPnd_Grand_Rate_Guar_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>' #GRAND-RATE-GUAR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>",ldaIaal584w.getPnd_Grand_Rate_Divd_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>' #GRAND-RATE-DIVD-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>",ldaIaal584w.getPnd_Grand_Tot_Ovrpy_Tiaa(),  //Natural: WRITE ( 1 ) / 'TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>' #GRAND-TOT-OVRPY-TIAA ( EM = ZZZ,ZZZ,ZZ9.99 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new           //Natural: WRITE ( 1 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                            RepeatItem(59));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "  # OF TIAA TRADITIONAL CONTRACTS => ",ldaIaal584w.getPnd_Num_Of_Tiaa());                                                  //Natural: WRITE '  # OF TIAA TRADITIONAL CONTRACTS => ' #NUM-OF-TIAA
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                        //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                        sub_Pnd_Get_Desc();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                           //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 5 LINES LEFT
                        {
                            getReports().newPage(2);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Comp_Desc,pnd_Fund_Desc,"===>",pnd_Grand_Tot_Ovrpy,         //Natural: WRITE ( 2 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #COMP-DESC #FUND-DESC '===>' #GRAND-TOT-OVRPY ( EM = ZZZ,ZZZ,ZZ9.99 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new           //Natural: WRITE ( 2 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                            RepeatItem(59));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "# OF CONTRACTS FOR",pnd_Comp_Desc,pnd_Fund_Desc,"===> ",pnd_Num_Of_Fund);                                                  //Natural: WRITE '# OF CONTRACTS FOR' #COMP-DESC #FUND-DESC '===> ' #NUM-OF-FUND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                        //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                        sub_Pnd_Get_Desc();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Grand_Tot_Ovrpy.reset();                                                                                                                          //Natural: RESET #GRAND-TOT-OVRPY #NUM-OF-FUND #WRITE-OUT-CONTRACT
                    pnd_Num_Of_Fund.reset();
                    pnd_Write_Out_Contract.reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*      WRITE '=' OVRPYMNT-PPCN-NBR '=' #W-FUND-CODE-TOT
            ldaIaal584w.getPnd_W_Contract().setValue(pnd_W2_File_Ovrpymnt_Ppcn_Nbr);                                                                                      //Natural: MOVE OVRPYMNT-PPCN-NBR TO #W-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-TYPE-OF-CONTRACT
            sub_Pnd_Determine_Type_Of_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!(ldaIaal584w.getPnd_Contract_Type_Field().notEquals(" "))))                                                                                    //Natural: ACCEPT IF #CONTRACT-TYPE-FIELD NOT = ' '
            {
                continue;
            }
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Last_Pay_Date().setValue(pnd_W2_File_Ovrpymnt_Last_Pymnt_Rcvd_Date);                                                                     //Natural: MOVE OVRPYMNT-LAST-PYMNT-RCVD-DATE TO #W-LAST-PAY-DATE
            ldaIaal584w.getPnd_W_Pymnt_Mode().setValue(pnd_W2_File_Ovrpymnt_Pymnt_Mode);                                                                                  //Natural: MOVE OVRPYMNT-PYMNT-MODE TO #W-PYMNT-MODE
            ldaIaal584w.getPnd_W_Payee_Cde().setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                //Natural: MOVE #W2-PAYEE-CDE-ALPHA TO #W-PAYEE-CDE
            //*      DISPLAY #W-CONTRACT #W-PAYEE-CDE
            ldaIaal584w.getPnd_Option_Cde().setValue(pnd_W2_File_Ovrpymnt_Optn_Cde);                                                                                      //Natural: MOVE OVRPYMNT-OPTN-CDE TO #OPTION-CDE
            DbsUtil.callnat(Iaan004.class , getCurrentProcessState(), ldaIaal584w.getPnd_Option_Cde(), ldaIaal584w.getPnd_W_Option_Cde_Desc());                           //Natural: CALLNAT 'IAAN004' #OPTION-CDE #W-OPTION-CDE-DESC
            if (condition(Global.isEscape())) return;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Num_Ovrpy_Yrs().setValue(pnd_W2_File_Pnd_W2_Num_Ovrpy_Yrs);                                                                                //Natural: MOVE #W2-NUM-OVRPY-YRS TO #NUM-OVRPY-YRS
            //* *Y2NCAM
            ldaIaal584w.getPnd_Ovrpymnt_Years_Count().reset();                                                                                                            //Natural: RESET #OVRPYMNT-YEARS-COUNT #EARLIER-DEATH
            ldaIaal584w.getPnd_Earlier_Death().reset();
            //*       WRITE '=' #W-CONTRACT '=' #NUM-OVRPY-YRS
            //* *Y2NCAM
            F7:                                                                                                                                                           //Natural: FOR #X = #NUM-OVRPY-YRS TO 1 STEP -1
            for (ldaIaal584w.getPnd_X().setValue(ldaIaal584w.getPnd_Num_Ovrpy_Yrs()); condition(ldaIaal584w.getPnd_X().greaterOrEqual(1)); ldaIaal584w.getPnd_X().nsubtract(1))
            {
                if (condition(pnd_W2_File_Ovrpymnt_Nbr.getValue(ldaIaal584w.getPnd_X()).greater(getZero())))                                                              //Natural: IF OVRPYMNT-NBR ( #X ) > 0
                {
                    //* *Y2NCAM
                    ldaIaal584w.getPnd_Ovrpymnt_Years_Count().nadd(1);                                                                                                    //Natural: ADD 1 TO #OVRPYMNT-YEARS-COUNT
                }                                                                                                                                                         //Natural: END-IF
                //*        WRITE '=' #OVRPYMNT-YEARS-COUNT
                //* *Y2NCAM
                if (condition(pnd_W2_File_Ovrpymnt_Nbr.getValue(ldaIaal584w.getPnd_X()).equals(getZero()) && ldaIaal584w.getPnd_Ovrpymnt_Years_Count().lessOrEqual(getZero()))) //Natural: IF OVRPYMNT-NBR ( #X ) = 0 AND #OVRPYMNT-YEARS-COUNT NOT > 0
                {
                    ldaIaal584w.getPnd_Earlier_Death().setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #EARLIER-DEATH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     WRITE '=' #OVRPYMNT-YEARS-COUNT '=' #EARLIER-DEATH
            //* *Y2NCAM
            if (condition(ldaIaal584w.getPnd_Num_Ovrpy_Yrs().greater(getZero())))                                                                                         //Natural: IF #NUM-OVRPY-YRS > 0
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Lowest_Year_Yyyymm_Pnd_Lowest_Year_Yyyymm_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W2_File_Ovrpymnt_Begin_Fiscal_Yr.getValue(ldaIaal584w.getPnd_Num_Ovrpy_Yrs()),  //Natural: COMPRESS OVRPYMNT-BEGIN-FISCAL-YR ( #NUM-OVRPY-YRS ) '00' INTO #LOWEST-YEAR-YYYYMM-A LEAVING NO
                    "00"));
                //* *Y2NCAM
                //* *Y2NCAM
                ldaIaal584w.getPnd_Lowest_Year_Yyyymm().nadd(pnd_W2_File_Ovrpymnt_Begin_Fiscal_Mo.getValue(ldaIaal584w.getPnd_Num_Ovrpy_Yrs()));                          //Natural: ADD OVRPYMNT-BEGIN-FISCAL-MO ( #NUM-OVRPY-YRS ) TO #LOWEST-YEAR-YYYYMM
                //* *Y2NCAM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A") || ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))                                  //Natural: IF #CONTRACT-TYPE-FIELD = 'A' OR = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM #LOAD-TEACHERS-RATES
                sub_Pnd_Load_Teachers_Rates();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*    WRITE 'UP TO THIS POINT' '=' #NUM-OVRPY-YRS
            //* *Y2NCAM
            F4:                                                                                                                                                           //Natural: FOR #I = #NUM-OVRPY-YRS TO 1 STEP -1
            for (ldaIaal584w.getPnd_I().setValue(ldaIaal584w.getPnd_Num_Ovrpy_Yrs()); condition(ldaIaal584w.getPnd_I().greaterOrEqual(1)); ldaIaal584w.getPnd_I().nsubtract(1))
            {
                //*      WRITE '=' OVRPYMNT-NBR(1)
                if (condition(pnd_W2_File_Ovrpymnt_Nbr.getValue(ldaIaal584w.getPnd_I()).greater(getZero())))                                                              //Natural: IF OVRPYMNT-NBR ( #I ) > 0
                {
                    ldaIaal584w.getPnd_Ovp_Cnt().nadd(1);                                                                                                                 //Natural: ADD 1 TO #OVP-CNT
                    //* *Y2NCAM
                    ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year().setValue(pnd_W2_File_Ovrpymnt_Begin_Fiscal_Yr.getValue(ldaIaal584w.getPnd_I()));                      //Natural: MOVE OVRPYMNT-BEGIN-FISCAL-YR ( #I ) TO #OVRPYMNT-BEGIN-FISCAL-YEAR
                    //* *Y2NCAM
                    //*    DISPLAY OVRPYMNT-BEGIN-FISCAL-YR(#I) OVRPYMNT-END-FISCAL-YR(#I)
                    //* *Y2NCAM
                    ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year().setValue(pnd_W2_File_Ovrpymnt_End_Fiscal_Yr.getValue(ldaIaal584w.getPnd_I()));                          //Natural: MOVE OVRPYMNT-END-FISCAL-YR ( #I ) TO #OVRPYMNT-END-FISCAL-YEAR
                    ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Mo().setValue(pnd_W2_File_Ovrpymnt_Begin_Fiscal_Mo.getValue(ldaIaal584w.getPnd_I()));                        //Natural: MOVE OVRPYMNT-BEGIN-FISCAL-MO ( #I ) TO #OVRPYMNT-BEGIN-FISCAL-MO
                    ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Mo().setValue(pnd_W2_File_Ovrpymnt_End_Fiscal_Mo.getValue(ldaIaal584w.getPnd_I()));                            //Natural: MOVE OVRPYMNT-END-FISCAL-MO ( #I ) TO #OVRPYMNT-END-FISCAL-MO
                    //* *Y2NCAM
                    ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year().setValue(pnd_W2_File_Ovrpymnt_Nbr.getValue(ldaIaal584w.getPnd_I()));                                       //Natural: MOVE OVRPYMNT-NBR ( #I ) TO #OVRPYMNT-NBR-PER-YEAR
                    ldaIaal584w.getPnd_Ovrpymnt_Dcdnt_Per_Amt().setValue(pnd_W2_File_Ovrpymnt_Dcdnt_Per_Amt.getValue(ldaIaal584w.getPnd_I()));                            //Natural: MOVE OVRPYMNT-DCDNT-PER-AMT ( #I ) TO #OVRPYMNT-DCDNT-PER-AMT
                    ldaIaal584w.getPnd_Ovrpymnt_Ovr_Per_Amt().setValue(pnd_W2_File_Ovrpymnt_Ovr_Per_Amt.getValue(ldaIaal584w.getPnd_I()));                                //Natural: MOVE OVRPYMNT-OVR-PER-AMT ( #I ) TO #OVRPYMNT-OVR-PER-AMT
                    //*  WRITE '=' #EARLIER-DEATH '=' #OVP-CNT
                    //* *Y2NCAM
                    if (condition((ldaIaal584w.getPnd_Num_Ovrpy_Yrs().equals(ldaIaal584w.getPnd_I())) || (ldaIaal584w.getPnd_Earlier_Death().equals("Y")                  //Natural: IF ( #NUM-OVRPY-YRS = #I ) OR ( #EARLIER-DEATH = 'Y' AND #OVP-CNT = 1 )
                        && ldaIaal584w.getPnd_Ovp_Cnt().equals(1))))
                    {
                        //* *Y2NCAM
                        ldaIaal584w.getPnd_Year_Of_Death().setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO #YEAR-OF-DEATH
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* *Y2NCAM
                        ldaIaal584w.getPnd_Year_Of_Death().setValue(" ");                                                                                                 //Natural: MOVE ' ' TO #YEAR-OF-DEATH
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE '!!!!!!!' #OVRPYMNT-NBR-PER-YEAR
                    //* *Y2NCAM
                    F5:                                                                                                                                                   //Natural: FOR #J = 1 TO #OVRPYMNT-NBR-PER-YEAR
                    for (ldaIaal584w.getPnd_J().setValue(1); condition(ldaIaal584w.getPnd_J().lessOrEqual(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year())); 
                        ldaIaal584w.getPnd_J().nadd(1))
                    {
                        ldaIaal584w.getPnd_Gen_Sub().nadd(1);                                                                                                             //Natural: ADD 1 TO #GEN-SUB
                        //* *Y2NCAM
                                                                                                                                                                          //Natural: PERFORM #GET-PAYMENT-DATE
                        sub_Pnd_Get_Payment_Date();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F5"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Error.equals("Y")))                                                                                                             //Natural: IF #ERROR = 'Y'
                        {
                            getReports().write(0, "!!!  ERROR FOR CONTRACT",ldaIaal584w.getPnd_W_Contract(),"CONTACT KEITH NICOLL");                                      //Natural: WRITE '!!!  ERROR FOR CONTRACT' #W-CONTRACT 'CONTACT KEITH NICOLL'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F5"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            ldaIaal584w.getPnd_Gen_Sub().setValue(0);                                                                                                     //Natural: ASSIGN #GEN-SUB := 0
                            if (true) break F4;                                                                                                                           //Natural: ESCAPE BOTTOM ( F4. )
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-PAYMENT-AMT
                        sub_Pnd_Get_Payment_Amt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F5"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F4"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F4"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     WRITE '=' #W-CONTRACT '=' #GEN-SUB
            if (condition(ldaIaal584w.getPnd_Gen_Sub().greater(getZero())))                                                                                               //Natural: IF #GEN-SUB > 0
            {
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-REPORT
                sub_Pnd_Determine_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
            sub_Pnd_Reset_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Write_Out_Contract.greater(getZero())))                                                                                                         //Natural: IF #WRITE-OUT-CONTRACT > 0
        {
            if (condition(pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1.equals("T")))                                                                                           //Natural: IF #FUND-CODE-HOLD-1 = 'T'
            {
                if (condition(getReports().getAstLinesLeft(1).less(7)))                                                                                                   //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 7 LINES LEFT
                {
                    getReports().newPage(1);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>",ldaIaal584w.getPnd_Grand_Rate_Guar_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>' #GRAND-RATE-GUAR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>",ldaIaal584w.getPnd_Grand_Rate_Divd_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>' #GRAND-RATE-DIVD-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>",ldaIaal584w.getPnd_Grand_Tot_Ovrpy_Tiaa(), //Natural: WRITE ( 1 ) / 'TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>' #GRAND-TOT-OVRPY-TIAA ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new                   //Natural: WRITE ( 1 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                    RepeatItem(59));
                if (Global.isEscape()) return;
                getReports().write(0, "  # OF TIAA TRADITIONAL CONTRACTS => ",ldaIaal584w.getPnd_Num_Of_Tiaa());                                                          //Natural: WRITE '  # OF TIAA TRADITIONAL CONTRACTS => ' #NUM-OF-TIAA
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                                   //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 5 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Comp_Desc,pnd_Fund_Desc,"===>",pnd_Grand_Tot_Ovrpy,                 //Natural: WRITE ( 2 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #COMP-DESC #FUND-DESC '===>' #GRAND-TOT-OVRPY ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new                   //Natural: WRITE ( 2 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                    RepeatItem(59));
                if (Global.isEscape()) return;
                getReports().write(0, "# OF CONTRACTS FOR",pnd_Comp_Desc,pnd_Fund_Desc,"===> ",pnd_Num_Of_Fund);                                                          //Natural: WRITE '# OF CONTRACTS FOR' #COMP-DESC #FUND-DESC '===> ' #NUM-OF-FUND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "*** END OF PROGRAM IAAP584 *** ");                                                                                                         //Natural: WRITE '*** END OF PROGRAM IAAP584 *** '
        if (Global.isEscape()) return;
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DESC
        //* ********************************************************************
        //* *******************************************************************
        //* *Y2NCAM
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-PAYMENT-DATE
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONTRACT-MODE-8-PARA
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONTRACT-MODE-1-PARA
        //* *Y2NCAM
        //* *Y2NCAM
        //* *                   #W-LAST-PAY-DATE-MM - #OVRPYMNT-NBR-PER-YEAR + #J
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONTRACT-MODE-6-PARA
        //* *Y2NCAM
        //* *Y2NCAM
        //*                     #W-LAST-PAY-DATE-MM - #OVRPYMNT-NBR-PER-YEAR + #J
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONTRACT-MODE-7-PARA
        //* *Y2NCAM
        //* *Y2NCAM
        //*                     #W-LAST-PAY-DATE-MM - #OVRPYMNT-NBR-PER-YEAR + #J
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-TEACHERS-RATES
        //*  WRITE  #CONT-STAT-FUND
        //* *Y2NCAM
        //* *   DISPLAY #OLD-TIAA-KEY-S #OLD-TIAA-KEY-E
        //* *Y2NCAM
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* *Y2NCAM
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-PAYMENT-AMT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-TEACHERS-RATES
        //* *Y2NCAM
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-REPORT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-CONTRACT
        //* *Y2NCAM
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CREF-CONTRACT
        //* *Y2NCAM
        //* *Y2NCAM
        //* *Y2NCAM
        //*     PERFORM #DECIDE-CREF-TYPE-3
        //*     PERFORM #DECIDE-CREF-TYPE-2
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SET-UP-TABLES
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-TYPE-OF-CONTRACT
        //* *******************************************************************
        //* *Y2NCAM
        //* *Y2NCAM
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #OVERPAY-BREAKDOWN
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-OCCURANCE-MODE-6
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-OCCURANCE-MODE-7
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-OCCURANCE-MODE-1
        //* *******************************************************************
    }
    private void sub_Pnd_Get_Desc() throws Exception                                                                                                                      //Natural: #GET-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_Desc.reset();                                                                                                                                            //Natural: RESET #FUND-DESC #COMP-DESC
        pnd_Comp_Desc.reset();
        //*  WRITE 'BEFORE CALL' '=' #FUND-CODE-2-3 '=' #FUND-DESC '=' #LEN
        //*  CALLNAT 'IAAN050A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        if (condition(Global.isEscape())) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 2 ) ' '
        if (Global.isEscape()) return;
        //*  WRITE 'AFTER CALL' '=' #FUND-CODE-2-3 '=' #FUND-DESC
    }
    private void sub_Pnd_Get_Payment_Date() throws Exception                                                                                                              //Natural: #GET-PAYMENT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  WRITE '=' #CONTRACT-MODE-1
        short decideConditionsMet797 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-MODE-1 = '8'
        if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_1().equals("8")))
        {
            decideConditionsMet797++;
            ldaIaal584w.getPnd_Cnt_Frq().setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CNT-FRQ
                                                                                                                                                                          //Natural: PERFORM #CONTRACT-MODE-8-PARA
            sub_Pnd_Contract_Mode_8_Para();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-MODE-1 = '1'
        else if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_1().equals("1")))
        {
            decideConditionsMet797++;
            ldaIaal584w.getPnd_Cnt_Frq().setValue(12);                                                                                                                    //Natural: MOVE 12 TO #CNT-FRQ
                                                                                                                                                                          //Natural: PERFORM #CONTRACT-MODE-1-PARA
            sub_Pnd_Contract_Mode_1_Para();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-MODE-1 = '6'
        else if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_1().equals("6")))
        {
            decideConditionsMet797++;
            ldaIaal584w.getPnd_Cnt_Frq().setValue(4);                                                                                                                     //Natural: MOVE 4 TO #CNT-FRQ
                                                                                                                                                                          //Natural: PERFORM #CONTRACT-MODE-6-PARA
            sub_Pnd_Contract_Mode_6_Para();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-MODE-1 = '7'
        else if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_1().equals("7")))
        {
            decideConditionsMet797++;
            ldaIaal584w.getPnd_Cnt_Frq().setValue(2);                                                                                                                     //Natural: MOVE 2 TO #CNT-FRQ
                                                                                                                                                                          //Natural: PERFORM #CONTRACT-MODE-7-PARA
            sub_Pnd_Contract_Mode_7_Para();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal584w.getPnd_Cnt_Frq().setValue(0);                                                                                                                     //Natural: MOVE 0 TO #CNT-FRQ
            getReports().write(0, "ERROR IN CONTRACT MODE ",ldaIaal584w.getPnd_W_Contract(),ldaIaal584w.getPnd_W_Pymnt_Mode());                                           //Natural: WRITE 'ERROR IN CONTRACT MODE ' #W-CONTRACT #W-PYMNT-MODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE '=' #ERROR
    }
    private void sub_Pnd_Contract_Mode_8_Para() throws Exception                                                                                                          //Natural: #CONTRACT-MODE-8-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        short decideConditionsMet818 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
        if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
        {
            decideConditionsMet818++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                                   //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3(),  //Natural: COMPRESS #CONTRACT-MODE-2-3 '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
        {
            decideConditionsMet818++;
            if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("01") || ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("02")    //Natural: IF #CONTRACT-MODE-2-3 = '01' OR EQ '02' OR EQ '03' OR EQ '04'
                || ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("03") || ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("04")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3(),  //Natural: COMPRESS #CONTRACT-MODE-2-3 '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
        {
            decideConditionsMet818++;
            if (condition(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("01") || ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("02")    //Natural: IF #CONTRACT-MODE-2-3 = '01' OR EQ '02' OR EQ '03'
                || ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3().equals("03")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_2_3(),  //Natural: COMPRESS #CONTRACT-MODE-2-3 '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WRITE '=' #W-PAY-DATE(#GEN-SUB)
    }
    private void sub_Pnd_Contract_Mode_1_Para() throws Exception                                                                                                          //Natural: #CONTRACT-MODE-1-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*    WRITE '=' #YEAR-OF-DEATH '=' #NUM-OVRPY-YRS '=' #W-CONTRACT
        //*    WRITE '=' #W-LAST-PAY-DATE-MM '=' #OVRPYMNT-YEARS-COUNT '=' #J
        //*  WRITE '=' #OVRPYMNT-NBR-PER-YEAR '=' #TEMP-OCCUR-3 '=' #J '=' #CNT-FRQ
        short decideConditionsMet855 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #YEAR-OF-DEATH = 'Y' AND #OVRPYMNT-YEARS-COUNT = 1
        if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y") && ldaIaal584w.getPnd_Ovrpymnt_Years_Count().equals(1)))
        {
            decideConditionsMet855++;
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-OCCURANCE-MODE-1
            sub_Pnd_Determine_Occurance_Mode_1();
            if (condition(Global.isEscape())) {return;}
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Temp_Occur_3().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #TEMP-OCCUR-3 - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN #YEAR-OF-DEATH = 'Y'
        else if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y")))
        {
            decideConditionsMet855++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Cnt_Frq().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #CNT-FRQ - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal584w.getPnd_Pay_Install().setValue(ldaIaal584w.getPnd_J());                                                                                            //Natural: MOVE #J TO #PAY-INSTALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaIaal584w.getPnd_Pay_Install().lessOrEqual(getZero()) || ldaIaal584w.getPnd_Pay_Install().greater(12)))                                           //Natural: IF #PAY-INSTALL LE 0 OR #PAY-INSTALL > 12
        {
            pnd_Error.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #ERROR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet871 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
        if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
        {
            decideConditionsMet871++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                                   //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            //*   WRITE 'ERROR ' '=' #PAY-INSTALL
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_T1().getValue(ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #T1 ( #PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
        {
            decideConditionsMet871++;
            //*   WRITE '=' #PAY-INSTALL '=' #W-CONTRACT
            if (condition(ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("01") || ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("02")  //Natural: IF #C1 ( #PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03' OR EQ '04'
                || ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("03") || ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("04")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #C1 ( #PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
        {
            decideConditionsMet871++;
            if (condition(ldaIaal584w.getPnd_A1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("01") || ldaIaal584w.getPnd_A1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("02")  //Natural: IF #A1 ( #PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03'
                || ldaIaal584w.getPnd_A1().getValue(ldaIaal584w.getPnd_Pay_Install()).equals("03")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_A1().getValue(ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #A1 ( #PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Contract_Mode_6_Para() throws Exception                                                                                                          //Natural: #CONTRACT-MODE-6-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        short decideConditionsMet906 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #YEAR-OF-DEATH = 'Y' AND #OVRPYMNT-YEARS-COUNT = 1
        if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y") && ldaIaal584w.getPnd_Ovrpymnt_Years_Count().equals(1)))
        {
            decideConditionsMet906++;
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-OCCURANCE-MODE-6
            sub_Pnd_Determine_Occurance_Mode_6();
            if (condition(Global.isEscape())) {return;}
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Temp_Occur().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #TEMP-OCCUR - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN #YEAR-OF-DEATH = 'Y'
        else if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y")))
        {
            decideConditionsMet906++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Cnt_Frq().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #CNT-FRQ - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal584w.getPnd_Pay_Install().setValue(ldaIaal584w.getPnd_J());                                                                                            //Natural: MOVE #J TO #PAY-INSTALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaIaal584w.getPnd_Pay_Install().lessOrEqual(getZero()) || ldaIaal584w.getPnd_Pay_Install().greater(4)))                                            //Natural: IF #PAY-INSTALL LE 0 OR #PAY-INSTALL > 4
        {
            pnd_Error.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #ERROR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF #YEAR-OF-DEATH = 'Y'
        //*     COMPUTE #PAY-INSTALL = #CNT-FRQ - #OVRPYMNT-NBR-PER-YEAR + #J
        //*   ELSE
        //*     MOVE #J TO #PAY-INSTALL
        //*   END-IF
        short decideConditionsMet929 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
        if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
        {
            decideConditionsMet929++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                                   //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_T6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #T6 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
        {
            decideConditionsMet929++;
            //*           WRITE '=' #CONTRACT-MODE-3-N '=' #PAY-INSTALL
            if (condition(ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("01")         //Natural: IF #C6 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03' OR EQ '04'
                || ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("02") 
                || ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("03") 
                || ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("04")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #C6 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
        {
            decideConditionsMet929++;
            if (condition(ldaIaal584w.getPnd_A6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("01")         //Natural: IF #A6 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03'
                || ldaIaal584w.getPnd_A6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("02") 
                || ldaIaal584w.getPnd_A6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("03")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_A6().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #A6 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Contract_Mode_7_Para() throws Exception                                                                                                          //Natural: #CONTRACT-MODE-7-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        short decideConditionsMet963 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #YEAR-OF-DEATH = 'Y' AND #OVRPYMNT-YEARS-COUNT = 1
        if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y") && ldaIaal584w.getPnd_Ovrpymnt_Years_Count().equals(1)))
        {
            decideConditionsMet963++;
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-OCCURANCE-MODE-7
            sub_Pnd_Determine_Occurance_Mode_7();
            if (condition(Global.isEscape())) {return;}
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Temp_Occur_2().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #TEMP-OCCUR-2 - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN #YEAR-OF-DEATH = 'Y'
        else if (condition(ldaIaal584w.getPnd_Year_Of_Death().equals("Y")))
        {
            decideConditionsMet963++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Install().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Pay_Install()), ldaIaal584w.getPnd_Cnt_Frq().subtract(ldaIaal584w.getPnd_Ovrpymnt_Nbr_Per_Year()).add(ldaIaal584w.getPnd_J())); //Natural: COMPUTE #PAY-INSTALL = #CNT-FRQ - #OVRPYMNT-NBR-PER-YEAR + #J
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal584w.getPnd_Pay_Install().setValue(ldaIaal584w.getPnd_J());                                                                                            //Natural: MOVE #J TO #PAY-INSTALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaIaal584w.getPnd_Pay_Install().lessOrEqual(getZero()) || ldaIaal584w.getPnd_Pay_Install().greater(2)))                                            //Natural: IF #PAY-INSTALL LE 0 OR #PAY-INSTALL > 2
        {
            pnd_Error.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #ERROR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #YEAR-OF-DEATH = 'Y'
        //*    COMPUTE #PAY-INSTALL = #CNT-FRQ - #OVRPYMNT-NBR-PER-YEAR + #J
        //*  ELSE
        //*    MOVE #J TO #PAY-INSTALL
        //*  END-IF
        short decideConditionsMet986 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
        if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
        {
            decideConditionsMet986++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                                   //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_T7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #T7 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
        {
            decideConditionsMet986++;
            if (condition(ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("01")         //Natural: IF #C7 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03' OR EQ '04'
                || ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("02") 
                || ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("03") 
                || ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("04")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #C7 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
        {
            decideConditionsMet986++;
            if (condition(ldaIaal584w.getPnd_A7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("01")         //Natural: IF #A7 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) = '01' OR EQ '02' OR EQ '03'
                || ldaIaal584w.getPnd_A7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("02") 
                || ldaIaal584w.getPnd_A7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()).equals("03")))
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_End_Fiscal_Year());                                                                 //Natural: MOVE #OVRPYMNT-END-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Pay_Date_Yr().setValue(ldaIaal584w.getPnd_Ovrpymnt_Begin_Fiscal_Year());                                                               //Natural: MOVE #OVRPYMNT-BEGIN-FISCAL-YEAR TO #PAY-DATE-YR
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_A7().getValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_Contract_Mode_3_N(),ldaIaal584w.getPnd_Pay_Install()),  //Natural: COMPRESS #A7 ( #CONTRACT-MODE-3-N,#PAY-INSTALL ) '/01/' #PAY-DATE-YR INTO #W-PAY-DATE ( #GEN-SUB ) LEAVING NO
                "/01/", ldaIaal584w.getPnd_Pay_Date_Yr()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Load_Teachers_Rates() throws Exception                                                                                                           //Natural: #LOAD-TEACHERS-RATES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_T().reset();                                                                                                                                   //Natural: RESET #T #FUND #MORE-OLD-RATES #ENTER-OLD-RATES-FILE
        ldaIaal584w.getPnd_Cont_Stat_Fund_Pnd_Fund().reset();
        ldaIaal584w.getPnd_More_Old_Rates().reset();
        ldaIaal584w.getPnd_Enter_Old_Rates_File().reset();
        //* *Y2NCAM
        ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue("*").reset();                                                                                                        //Natural: RESET #TIAA-RATE-DATE ( * ) #TIAA-RATE-GUAR ( * ) #TIAA-RATE-DIV ( * )
        ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue("*").reset();
        ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue("*").reset();
        //*     MOVE 02                  TO #W-PAYEE-CDE-NUM
        //*     MOVE 'GA000020  '        TO #W-CONTRACT
        ldaIaal584w.getPnd_Cont_Stat_Fund_Pnd_Cont().setValue(ldaIaal584w.getPnd_W_Contract());                                                                           //Natural: MOVE #W-CONTRACT TO #CONT
        ldaIaal584w.getPnd_Cont_Stat_Fund_Pnd_Stat().setValue(ldaIaal584w.getPnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num());                                                      //Natural: MOVE #W-PAYEE-CDE-NUM TO #STAT
        ldaIaal584w.getVw_new_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) NEW-TIAA-RATES BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CONT-STAT-FUND
        (
        "R3",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", ldaIaal584w.getPnd_Cont_Stat_Fund(), WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R3:
        while (condition(ldaIaal584w.getVw_new_Tiaa_Rates().readNextRow("R3")))
        {
            ldaIaal584w.getPnd_T().nadd(1);                                                                                                                               //Natural: ADD 1 TO #T
            ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getNew_Tiaa_Rates_Tiaa_Tot_Per_Amt());                              //Natural: MOVE TIAA-TOT-PER-AMT TO #TIAA-RATE-GUAR ( #T )
            ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getNew_Tiaa_Rates_Tiaa_Tot_Div_Amt());                               //Natural: MOVE TIAA-TOT-DIV-AMT TO #TIAA-RATE-DIV ( #T )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal584w.getPnd_Old_Tiaa_Key_S_Pnd_Cont_S().setValue(ldaIaal584w.getPnd_W_Contract());                                                                         //Natural: MOVE #W-CONTRACT TO #CONT-S #CONT-E
        ldaIaal584w.getPnd_Old_Tiaa_Key_E_Pnd_Cont_E().setValue(ldaIaal584w.getPnd_W_Contract());
        ldaIaal584w.getPnd_Old_Tiaa_Key_S_Pnd_Stat_S().setValue(ldaIaal584w.getPnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num());                                                    //Natural: MOVE #W-PAYEE-CDE-NUM TO #STAT-S #STAT-E
        ldaIaal584w.getPnd_Old_Tiaa_Key_E_Pnd_Stat_E().setValue(ldaIaal584w.getPnd_W_Payee_Cde_Pnd_W_Payee_Cde_Num());
        ldaIaal584w.getPnd_Old_Tiaa_Key_S_Pnd_Fund_Invrse_Lpd_S().setValue(0);                                                                                            //Natural: MOVE 0 TO #FUND-INVRSE-LPD-S
        ldaIaal584w.getPnd_Old_Tiaa_Key_E_Pnd_Fund_Invrse_Lpd_E().setValue(99999999);                                                                                     //Natural: MOVE 99999999 TO #FUND-INVRSE-LPD-E
        ldaIaal584w.getPnd_Old_Tiaa_Key_S_Pnd_Comp_Fund_Cde_S().setValue("H'00'");                                                                                        //Natural: MOVE H'00' TO #COMP-FUND-CDE-S
        ldaIaal584w.getPnd_Old_Tiaa_Key_E_Pnd_Comp_Fund_Cde_E().setValue("H'FF'");                                                                                        //Natural: MOVE H'FF' TO #COMP-FUND-CDE-E
        ldaIaal584w.getVw_iaa_Old_Tiaa_Rates_View().startDatabaseRead                                                                                                     //Natural: READ IAA-OLD-TIAA-RATES-VIEW BY CNTRCT-PY-DTE-KEY STARTING FROM #OLD-TIAA-KEY-S ENDING AT #OLD-TIAA-KEY-E
        (
        "R4",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", ldaIaal584w.getPnd_Old_Tiaa_Key_S(), "And", WcType.BY) ,
        new Wc("CNTRCT_PY_DTE_KEY", "<=", ldaIaal584w.getPnd_Old_Tiaa_Key_E(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R4:
        while (condition(ldaIaal584w.getVw_iaa_Old_Tiaa_Rates_View().readNextRow("R4")))
        {
            ldaIaal584w.getPnd_Enter_Old_Rates_File().setValue("Y");                                                                                                      //Natural: MOVE 'Y' TO #ENTER-OLD-RATES-FILE
            //* *Y2NCAM
            ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A().setValueEdited(ldaIaal584w.getIaa_Old_Tiaa_Rates_View_Fund_Lst_Pd_Dte(),new ReportEditMask("YYYYMMDD"));               //Natural: MOVE EDITED FUND-LST-PD-DTE ( EM = YYYYMMDD ) TO #FUND-LST-PD-DTE-A
            //* *Y2NCAM
            ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm().nadd(1);                                                                                        //Natural: ADD 1 TO #FUND-LST-PD-DTE-MM
            //* *Y2NCAM
            if (condition(ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm().greater(12)))                                                                     //Natural: IF #FUND-LST-PD-DTE-MM > 12
            {
                //* *Y2NCAM
                ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Mm_Alpha().setValue("01");                                                                       //Natural: MOVE '01' TO #FUND-LST-PD-DTE-MM-ALPHA
                //* *Y2NCAM
                ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyy().nadd(1);                                                                                  //Natural: ADD 1 TO #FUND-LST-PD-DTE-YYYY
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            if (condition(ldaIaal584w.getPnd_Lowest_Year_Yyyymm().greater(ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm())))                            //Natural: IF #LOWEST-YEAR-YYYYMM > #FUND-LST-PD-DTE-YYYYMM
            {
                if (condition(ldaIaal584w.getPnd_T().equals(1)))                                                                                                          //Natural: IF #T = 1
                {
                    //* *Y2NCAM
                    ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm());     //Natural: MOVE #FUND-LST-PD-DTE-YYYYMM TO #TIAA-RATE-DATE ( #T )
                    ldaIaal584w.getPnd_More_Old_Rates().setValue("Y");                                                                                                    //Natural: MOVE 'Y' TO #MORE-OLD-RATES
                    if (true) break R4;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R4. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue(ldaIaal584w.getPnd_T()).setValue(0);                                                                     //Natural: MOVE 0 TO #TIAA-RATE-GUAR ( #T )
                    ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue(ldaIaal584w.getPnd_T()).setValue(0);                                                                      //Natural: MOVE 0 TO #TIAA-RATE-DIV ( #T )
                    ldaIaal584w.getPnd_T().nsubtract(1);                                                                                                                  //Natural: SUBTRACT 1 FROM #T
                    ldaIaal584w.getPnd_More_Old_Rates().setValue("Y");                                                                                                    //Natural: MOVE 'Y' TO #MORE-OLD-RATES
                    if (true) break R4;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R4. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCAM
            ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getPnd_Fund_Lst_Pd_Dte_A_Pnd_Fund_Lst_Pd_Dte_Yyyymm());             //Natural: MOVE #FUND-LST-PD-DTE-YYYYMM TO #TIAA-RATE-DATE ( #T )
            ldaIaal584w.getPnd_T().nadd(1);                                                                                                                               //Natural: ADD 1 TO #T
            ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Per_Amt());                   //Natural: MOVE CNTRCT-TOT-PER-AMT TO #TIAA-RATE-GUAR ( #T )
            ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getIaa_Old_Tiaa_Rates_View_Cntrct_Tot_Div_Amt());                    //Natural: MOVE CNTRCT-TOT-DIV-AMT TO #TIAA-RATE-DIV ( #T )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal584w.getPnd_Enter_Old_Rates_File().equals(" ") || ldaIaal584w.getPnd_More_Old_Rates().equals(" ")))                                          //Natural: IF #ENTER-OLD-RATES-FILE = ' ' OR #MORE-OLD-RATES = ' '
        {
            //* *Y2NCAM
            ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue(ldaIaal584w.getPnd_T()).setValue(ldaIaal584w.getPnd_W_Contract_Issue_Dte());                                     //Natural: MOVE #W-CONTRACT-ISSUE-DTE TO #TIAA-RATE-DATE ( #T )
        }                                                                                                                                                                 //Natural: END-IF
        //* * DISPLAY #TIAA-RATE-DATE(*) #TIAA-RATE-GUAR(*) #TIAA-RATE-DIV(*)
        //* *  DISPLAY  #ENTER-OLD-RATES-FILE #MORE-OLD-RATES
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_Gen_Sub().reset();                                                                                                                             //Natural: RESET #GEN-SUB #ERROR #W-CONTRACT #W-PYMNT-MODE #W-PAYEE-CDE #W-OPTION-CDE-DESC #W-PAY-DATE ( * ) #W-RATE-GROSS ( * ) #W-RATE-OVRPY ( * ) #W-RATE-GUAR ( * ) #W-RATE-DIVD ( * ) #TEMP-OCCUR #TEMP-OCCUR-2 #OVP-CNT #TEMP-OCCUR-3 #T-CONTRACT #T-PAYEE-CDE #C-RATE-GUAR #C-RATE-DIVD #C-RATE-GUAR-OVP #C-RATE-DIVD-OVP #C-RATE-OVRPYMNT #C-CONTRACT #C-PAYEE-CDE #CNT-RATE-GROSS #CNT-RATE-OVRPY
        pnd_Error.reset();
        ldaIaal584w.getPnd_W_Contract().reset();
        ldaIaal584w.getPnd_W_Pymnt_Mode().reset();
        ldaIaal584w.getPnd_W_Payee_Cde().reset();
        ldaIaal584w.getPnd_W_Option_Cde_Desc().reset();
        ldaIaal584w.getPnd_W_Pay_Date().getValue("*").reset();
        ldaIaal584w.getPnd_W_Rate_Gross().getValue("*").reset();
        ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue("*").reset();
        ldaIaal584w.getPnd_W_Rate_Guar().getValue("*").reset();
        ldaIaal584w.getPnd_W_Rate_Divd().getValue("*").reset();
        ldaIaal584w.getPnd_Temp_Occur().reset();
        ldaIaal584w.getPnd_Temp_Occur_2().reset();
        ldaIaal584w.getPnd_Ovp_Cnt().reset();
        ldaIaal584w.getPnd_Temp_Occur_3().reset();
        ldaIaal584w.getPnd_T_Record_Pnd_T_Contract().reset();
        ldaIaal584w.getPnd_T_Record_Pnd_T_Payee_Cde().reset();
        ldaIaal584w.getPnd_C_Rate_Guar().reset();
        ldaIaal584w.getPnd_C_Rate_Divd().reset();
        ldaIaal584w.getPnd_C_Rate_Guar_Ovp().reset();
        ldaIaal584w.getPnd_C_Rate_Divd_Ovp().reset();
        ldaIaal584w.getPnd_C_Rate_Ovrpymnt().reset();
        ldaIaal584w.getPnd_C_Record_Pnd_C_Contract().reset();
        ldaIaal584w.getPnd_C_Record_Pnd_C_Payee_Cde().reset();
        ldaIaal584w.getPnd_Cnt_Rate_Gross().reset();
        ldaIaal584w.getPnd_Cnt_Rate_Ovrpy().reset();
        pnd_W2_File.reset();                                                                                                                                              //Natural: RESET #W2-FILE
    }
    private void sub_Pnd_Get_Payment_Amt() throws Exception                                                                                                               //Natural: #GET-PAYMENT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Ovrpymnt_Ovr_Per_Amt());                                     //Natural: MOVE #OVRPYMNT-OVR-PER-AMT TO #W-RATE-OVRPY ( #GEN-SUB )
        short decideConditionsMet1099 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
        if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
        {
            decideConditionsMet1099++;
            ldaIaal584w.getPnd_W_Rate_Gross().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Ovrpymnt_Dcdnt_Per_Amt());                               //Natural: MOVE #OVRPYMNT-DCDNT-PER-AMT TO #W-RATE-GROSS ( #GEN-SUB )
        }                                                                                                                                                                 //Natural: WHEN #CONTRACT-TYPE-FIELD = 'T' OR = 'A'
        else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T") || ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
        {
            decideConditionsMet1099++;
            //* *Y2NCAM
            ldaIaal584w.getPnd_Py_Dt().setValue(ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_Gen_Sub()));                                                  //Natural: MOVE #W-PAY-DATE ( #GEN-SUB ) TO #PY-DT
            //* *Y2NCAM
            ldaIaal584w.getPnd_Py_Dt_Yyyymm_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_Py_Dt_Pnd_Py_Dt_Yyyy(), ldaIaal584w.getPnd_Py_Dt_Pnd_Py_Dt_Mm())); //Natural: COMPRESS #PY-DT-YYYY #PY-DT-MM INTO #PY-DT-YYYYMM-A LEAVING NO
                                                                                                                                                                          //Natural: PERFORM #GET-TEACHERS-RATES
            sub_Pnd_Get_Teachers_Rates();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Get_Teachers_Rates() throws Exception                                                                                                            //Natural: #GET-TEACHERS-RATES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_Found().reset();                                                                                                                               //Natural: RESET #FOUND
        F6:                                                                                                                                                               //Natural: FOR #P = #T TO 2 STEP -1
        for (ldaIaal584w.getPnd_P().setValue(ldaIaal584w.getPnd_T()); condition(ldaIaal584w.getPnd_P().greaterOrEqual(2)); ldaIaal584w.getPnd_P().nsubtract(1))
        {
            ldaIaal584w.getPnd_H().compute(new ComputeParameters(false, ldaIaal584w.getPnd_H()), ldaIaal584w.getPnd_P().subtract(1));                                     //Natural: COMPUTE #H = #P - 1
            //* *    DISPLAY #PY-DT-YYYYMM #TIAA-RATE-DATE(#P) #TIAA-RATE-DATE(#H)
            //* *Y2NCAM
            if (condition(ldaIaal584w.getPnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm().greaterOrEqual(ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue(ldaIaal584w.getPnd_P()))       //Natural: IF #PY-DT-YYYYMM >= #TIAA-RATE-DATE ( #P ) AND #PY-DT-YYYYMM < #TIAA-RATE-DATE ( #H )
                && ldaIaal584w.getPnd_Py_Dt_Yyyymm_A_Pnd_Py_Dt_Yyyymm().less(ldaIaal584w.getPnd_Tiaa_Rate_Date().getValue(ldaIaal584w.getPnd_H()))))
            {
                ldaIaal584w.getPnd_W_Rate_Guar().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue(ldaIaal584w.getPnd_P()));   //Natural: MOVE #TIAA-RATE-GUAR ( #P ) TO #W-RATE-GUAR ( #GEN-SUB )
                ldaIaal584w.getPnd_W_Rate_Divd().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue(ldaIaal584w.getPnd_P()));    //Natural: MOVE #TIAA-RATE-DIV ( #P ) TO #W-RATE-DIVD ( #GEN-SUB )
                ldaIaal584w.getPnd_Found().setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #FOUND
                if (true) break F6;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F6. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaIaal584w.getPnd_Found().notEquals("Y")))                                                                                                         //Natural: IF #FOUND NE 'Y'
        {
            ldaIaal584w.getPnd_W_Rate_Guar().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Tiaa_Rate_Guar().getValue(1));                            //Natural: MOVE #TIAA-RATE-GUAR ( 1 ) TO #W-RATE-GUAR ( #GEN-SUB )
            ldaIaal584w.getPnd_W_Rate_Divd().getValue(ldaIaal584w.getPnd_Gen_Sub()).setValue(ldaIaal584w.getPnd_Tiaa_Rate_Div().getValue(1));                             //Natural: MOVE #TIAA-RATE-DIV ( 1 ) TO #W-RATE-DIVD ( #GEN-SUB )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Determine_Report() throws Exception                                                                                                              //Natural: #DETERMINE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Write_Out_Contract.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WRITE-OUT-CONTRACT
        if (condition(DbsUtil.maskMatches(ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") && ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z")))          //Natural: IF #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z'
        {
                                                                                                                                                                          //Natural: PERFORM #TIAA-CONTRACT
            sub_Pnd_Tiaa_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CREF-CONTRACT
            sub_Pnd_Cref_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Tiaa_Contract() throws Exception                                                                                                                 //Natural: #TIAA-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_T_Record_Pnd_T_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #T-CONTRACT LEAVING NO
            "-", ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_8()));
        //*  MOVE #W-CONTRACT        TO #T-CONTRACT
        ldaIaal584w.getPnd_T_Record_Pnd_T_Payee_Cde().setValue(ldaIaal584w.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #T-PAYEE-CDE
        ldaIaal584w.getPnd_T_Record_Pnd_T_Option_Cde_Desc().setValue(ldaIaal584w.getPnd_W_Option_Cde_Desc());                                                             //Natural: MOVE #W-OPTION-CDE-DESC TO #T-OPTION-CDE-DESC
        ldaIaal584w.getPnd_T_Record_Pnd_T_Pymnt_Mode().setValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A());                                                    //Natural: MOVE #W-PYMNT-MODE-A TO #T-PYMNT-MODE
        F8:                                                                                                                                                               //Natural: FOR #R = 1 TO #GEN-SUB
        for (ldaIaal584w.getPnd_R().setValue(1); condition(ldaIaal584w.getPnd_R().lessOrEqual(ldaIaal584w.getPnd_Gen_Sub())); ldaIaal584w.getPnd_R().nadd(1))
        {
            //* *Y2NCAM
            ldaIaal584w.getPnd_T_Record_Pnd_T_Pay_Date().setValue(ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_R()));                                      //Natural: MOVE #W-PAY-DATE ( #R ) TO #T-PAY-DATE
            ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Guar().setValue(ldaIaal584w.getPnd_W_Rate_Guar().getValue(ldaIaal584w.getPnd_R()));                                    //Natural: MOVE #W-RATE-GUAR ( #R ) TO #T-RATE-GUAR
            ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Divd().setValue(ldaIaal584w.getPnd_W_Rate_Divd().getValue(ldaIaal584w.getPnd_R()));                                    //Natural: MOVE #W-RATE-DIVD ( #R ) TO #T-RATE-DIVD
            ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Ovrpy().setValue(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                  //Natural: MOVE #W-RATE-OVRPY ( #R ) TO #T-RATE-OVRPY
            ldaIaal584w.getPnd_C_Rate_Guar().nadd(ldaIaal584w.getPnd_W_Rate_Guar().getValue(ldaIaal584w.getPnd_R()));                                                     //Natural: ADD #W-RATE-GUAR ( #R ) TO #C-RATE-GUAR
            ldaIaal584w.getPnd_C_Rate_Divd().nadd(ldaIaal584w.getPnd_W_Rate_Divd().getValue(ldaIaal584w.getPnd_R()));                                                     //Natural: ADD #W-RATE-DIVD ( #R ) TO #C-RATE-DIVD
            ldaIaal584w.getPnd_C_Rate_Ovrpymnt().nadd(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                                //Natural: ADD #W-RATE-OVRPY ( #R ) TO #C-RATE-OVRPYMNT
                                                                                                                                                                          //Natural: PERFORM #OVERPAY-BREAKDOWN
            sub_Pnd_Overpay_Breakdown();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F8"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F8"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal584w.getPnd_Grand_Tot_Ovrpy_Tiaa().nadd(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                           //Natural: ADD #W-RATE-OVRPY ( #R ) TO #GRAND-TOT-OVRPY-TIAA
            //*      WRITE (1) #T-RECORD
            getReports().write(1, ReportOption.NOTITLE,ldaIaal584w.getPnd_T_Record_Pnd_T_Contract(),new TabSetting(12),ldaIaal584w.getPnd_T_Record_Pnd_T_Payee_Cde(),new  //Natural: WRITE ( 1 ) #T-CONTRACT 012T #T-PAYEE-CDE 017T #T-OPTION-CDE-DESC 040T #T-PYMNT-MODE 047T #T-PAY-DATE 060T #T-RATE-GUAR ( EM = Z,ZZZ,ZZ9.99 ) 075T #T-RATE-DIVD ( EM = Z,ZZZ,ZZ9.99 ) 090T #T-RATE-GUAR-OVP ( EM = Z,ZZZ,ZZ9.99 ) 106T #T-RATE-DIVD-OVP ( EM = Z,ZZZ,ZZ9.99 ) 121T #T-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 )
                TabSetting(17),ldaIaal584w.getPnd_T_Record_Pnd_T_Option_Cde_Desc(),new TabSetting(40),ldaIaal584w.getPnd_T_Record_Pnd_T_Pymnt_Mode(),new 
                TabSetting(47),ldaIaal584w.getPnd_T_Record_Pnd_T_Pay_Date(),new TabSetting(60),ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Guar(), new ReportEditMask 
                ("Z,ZZZ,ZZ9.99"),new TabSetting(75),ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Divd(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(90),ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Guar_Ovp(), 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(106),ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Divd_Ovp(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
                TabSetting(121),ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Ovrpy(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F8"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F8"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal584w.getPnd_T_Record().reset();                                                                                                                        //Natural: RESET #T-RECORD
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SKIP (1) 1
        ldaIaal584w.getPnd_T_Record_Pnd_T_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #T-CONTRACT LEAVING NO
            "-", ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_8()));
        ldaIaal584w.getPnd_T_Record_Pnd_T_Payee_Cde().setValue(ldaIaal584w.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #T-PAYEE-CDE
        getReports().write(1, ReportOption.NOTITLE,ldaIaal584w.getPnd_T_Record_Pnd_T_Contract(),new TabSetting(12),ldaIaal584w.getPnd_T_Record_Pnd_T_Payee_Cde(),new      //Natural: WRITE ( 1 ) #T-CONTRACT 012T #T-PAYEE-CDE 021T '-> TOTALS <-' 060T #C-RATE-GUAR ( EM = Z,ZZZ,ZZ9.99 ) 075T #C-RATE-DIVD ( EM = Z,ZZZ,ZZ9.99 ) 090T #C-RATE-GUAR-OVP ( EM = Z,ZZZ,ZZ9.99 ) 106T #C-RATE-DIVD-OVP ( EM = Z,ZZZ,ZZ9.99 ) 121T #C-RATE-OVRPYMNT ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(21),"-> TOTALS <-",new TabSetting(60),ldaIaal584w.getPnd_C_Rate_Guar(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(75),ldaIaal584w.getPnd_C_Rate_Divd(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(90),ldaIaal584w.getPnd_C_Rate_Guar_Ovp(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(106),ldaIaal584w.getPnd_C_Rate_Divd_Ovp(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(121),ldaIaal584w.getPnd_C_Rate_Ovrpymnt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        ldaIaal584w.getPnd_Num_Of_Tiaa().nadd(1);                                                                                                                         //Natural: ADD 1 TO #NUM-OF-TIAA
    }
    private void sub_Pnd_Cref_Contract() throws Exception                                                                                                                 //Natural: #CREF-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_C_Record_Pnd_C_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #C-CONTRACT LEAVING NO
            "-", ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_8()));
        //*  MOVE #W-CONTRACT        TO #C-CONTRACT
        ldaIaal584w.getPnd_C_Record_Pnd_C_Payee_Cde().setValue(ldaIaal584w.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #C-PAYEE-CDE
        ldaIaal584w.getPnd_C_Record_Pnd_C_Option_Cde_Desc().setValue(ldaIaal584w.getPnd_W_Option_Cde_Desc());                                                             //Natural: MOVE #W-OPTION-CDE-DESC TO #C-OPTION-CDE-DESC
        ldaIaal584w.getPnd_C_Record_Pnd_C_Pymnt_Mode().setValue(ldaIaal584w.getPnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A());                                                    //Natural: MOVE #W-PYMNT-MODE-A TO #C-PYMNT-MODE
        F8A:                                                                                                                                                              //Natural: FOR #R = 1 TO #GEN-SUB
        for (ldaIaal584w.getPnd_R().setValue(1); condition(ldaIaal584w.getPnd_R().lessOrEqual(ldaIaal584w.getPnd_Gen_Sub())); ldaIaal584w.getPnd_R().nadd(1))
        {
            //* *Y2NCAM
            ldaIaal584w.getPnd_C_Record_Pnd_C_Pay_Date().setValue(ldaIaal584w.getPnd_W_Pay_Date().getValue(ldaIaal584w.getPnd_R()));                                      //Natural: MOVE #W-PAY-DATE ( #R ) TO #C-PAY-DATE
            ldaIaal584w.getPnd_C_Record_Pnd_C_Rate_Gross().setValue(ldaIaal584w.getPnd_W_Rate_Gross().getValue(ldaIaal584w.getPnd_R()));                                  //Natural: MOVE #W-RATE-GROSS ( #R ) TO #C-RATE-GROSS
            ldaIaal584w.getPnd_C_Record_Pnd_C_Rate_Ovrpy().setValue(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                  //Natural: MOVE #W-RATE-OVRPY ( #R ) TO #C-RATE-OVRPY
            ldaIaal584w.getPnd_Cnt_Rate_Gross().nadd(ldaIaal584w.getPnd_W_Rate_Gross().getValue(ldaIaal584w.getPnd_R()));                                                 //Natural: ADD #W-RATE-GROSS ( #R ) TO #CNT-RATE-GROSS
            ldaIaal584w.getPnd_Cnt_Rate_Ovrpy().nadd(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                                 //Natural: ADD #W-RATE-OVRPY ( #R ) TO #CNT-RATE-OVRPY
            pnd_Grand_Tot_Ovrpy.nadd(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                                                 //Natural: ADD #W-RATE-OVRPY ( #R ) TO #GRAND-TOT-OVRPY
            getReports().write(2, ReportOption.NOTITLE,ldaIaal584w.getPnd_C_Record_Pnd_C_Contract(),new TabSetting(20),ldaIaal584w.getPnd_C_Record_Pnd_C_Payee_Cde(),new  //Natural: WRITE ( 2 ) #C-CONTRACT 020T #C-PAYEE-CDE 030T #C-OPTION-CDE-DESC 056T #C-PYMNT-MODE 071T #C-PAY-DATE 092T #C-RATE-GROSS ( EM = Z,ZZZ,ZZ9.99 ) 114T #C-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 )
                TabSetting(30),ldaIaal584w.getPnd_C_Record_Pnd_C_Option_Cde_Desc(),new TabSetting(56),ldaIaal584w.getPnd_C_Record_Pnd_C_Pymnt_Mode(),new 
                TabSetting(71),ldaIaal584w.getPnd_C_Record_Pnd_C_Pay_Date(),new TabSetting(92),ldaIaal584w.getPnd_C_Record_Pnd_C_Rate_Gross(), new ReportEditMask 
                ("Z,ZZZ,ZZ9.99"),new TabSetting(114),ldaIaal584w.getPnd_C_Record_Pnd_C_Rate_Ovrpy(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F8A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F8A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*       PERFORM #DECIDE-CREF-TYPE-1
            ldaIaal584w.getPnd_C_Record().reset();                                                                                                                        //Natural: RESET #C-RECORD
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal584w.getPnd_C_Record_Pnd_C_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #C-CONTRACT LEAVING NO
            "-", ldaIaal584w.getPnd_W_Contract_Pnd_W_Contract_8()));
        ldaIaal584w.getPnd_C_Record_Pnd_C_Payee_Cde().setValue(ldaIaal584w.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #C-PAYEE-CDE
        getReports().write(2, ReportOption.NOTITLE,ldaIaal584w.getPnd_C_Record_Pnd_C_Contract(),new TabSetting(20),ldaIaal584w.getPnd_C_Record_Pnd_C_Payee_Cde(),new      //Natural: WRITE ( 2 ) #C-CONTRACT 020T #C-PAYEE-CDE 034T '-> TOTALS <-' 092T #CNT-RATE-GROSS ( EM = Z,ZZZ,ZZ9.99 ) 114T #CNT-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(34),"-> TOTALS <-",new TabSetting(92),ldaIaal584w.getPnd_Cnt_Rate_Gross(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(114),ldaIaal584w.getPnd_Cnt_Rate_Ovrpy(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 2 ) 3
        pnd_Num_Of_Fund.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #NUM-OF-FUND
    }
    private void sub_Pnd_Set_Up_Tables() throws Exception                                                                                                                 //Natural: #SET-UP-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_C6().getValue(1,3).setValue("01");                                                                                                             //Natural: MOVE '01' TO #C6 ( 1,3 ) #C7 ( 1,2 ) #A6 ( 1,4 ) #A7 ( 1,2 ) #C1 ( 9 ) #A1 ( 10 )
        ldaIaal584w.getPnd_C7().getValue(1,2).setValue("01");
        ldaIaal584w.getPnd_A6().getValue(1,4).setValue("01");
        ldaIaal584w.getPnd_A7().getValue(1,2).setValue("01");
        ldaIaal584w.getPnd_C1().getValue(9).setValue("01");
        ldaIaal584w.getPnd_A1().getValue(10).setValue("01");
        ldaIaal584w.getPnd_C6().getValue(2,4).setValue("02");                                                                                                             //Natural: MOVE '02' TO #C6 ( 2,4 ) #C7 ( 2,2 ) #A6 ( 2,4 ) #A7 ( 2,2 ) #C1 ( 10 ) #A1 ( 11 )
        ldaIaal584w.getPnd_C7().getValue(2,2).setValue("02");
        ldaIaal584w.getPnd_A6().getValue(2,4).setValue("02");
        ldaIaal584w.getPnd_A7().getValue(2,2).setValue("02");
        ldaIaal584w.getPnd_C1().getValue(10).setValue("02");
        ldaIaal584w.getPnd_A1().getValue(11).setValue("02");
        ldaIaal584w.getPnd_C6().getValue(3,4).setValue("03");                                                                                                             //Natural: MOVE '03' TO #C6 ( 3,4 ) #C7 ( 3,2 ) #A6 ( 3,4 ) #A7 ( 3,2 ) #C1 ( 11 ) #A1 ( 12 )
        ldaIaal584w.getPnd_C7().getValue(3,2).setValue("03");
        ldaIaal584w.getPnd_A6().getValue(3,4).setValue("03");
        ldaIaal584w.getPnd_A7().getValue(3,2).setValue("03");
        ldaIaal584w.getPnd_C1().getValue(11).setValue("03");
        ldaIaal584w.getPnd_A1().getValue(12).setValue("03");
        ldaIaal584w.getPnd_C6().getValue(1,4).setValue("04");                                                                                                             //Natural: MOVE '04' TO #C6 ( 1,4 ) #C7 ( 4,2 ) #A6 ( 1,1 ) #A7 ( 4,1 ) #C1 ( 12 ) #A1 ( 1 )
        ldaIaal584w.getPnd_C7().getValue(4,2).setValue("04");
        ldaIaal584w.getPnd_A6().getValue(1,1).setValue("04");
        ldaIaal584w.getPnd_A7().getValue(4,1).setValue("04");
        ldaIaal584w.getPnd_C1().getValue(12).setValue("04");
        ldaIaal584w.getPnd_A1().getValue(1).setValue("04");
        ldaIaal584w.getPnd_C6().getValue(2,1).setValue("05");                                                                                                             //Natural: MOVE '05' TO #C6 ( 2,1 ) #C7 ( 5,1 ) #A6 ( 2,1 ) #A7 ( 5,1 ) #C1 ( 1 ) #A1 ( 2 )
        ldaIaal584w.getPnd_C7().getValue(5,1).setValue("05");
        ldaIaal584w.getPnd_A6().getValue(2,1).setValue("05");
        ldaIaal584w.getPnd_A7().getValue(5,1).setValue("05");
        ldaIaal584w.getPnd_C1().getValue(1).setValue("05");
        ldaIaal584w.getPnd_A1().getValue(2).setValue("05");
        ldaIaal584w.getPnd_C6().getValue(3,1).setValue("06");                                                                                                             //Natural: MOVE '06' TO #C6 ( 3,1 ) #C7 ( 6,1 ) #A6 ( 3,1 ) #A7 ( 6,1 ) #C1 ( 2 ) #A1 ( 3 )
        ldaIaal584w.getPnd_C7().getValue(6,1).setValue("06");
        ldaIaal584w.getPnd_A6().getValue(3,1).setValue("06");
        ldaIaal584w.getPnd_A7().getValue(6,1).setValue("06");
        ldaIaal584w.getPnd_C1().getValue(2).setValue("06");
        ldaIaal584w.getPnd_A1().getValue(3).setValue("06");
        ldaIaal584w.getPnd_C6().getValue(1,1).setValue("07");                                                                                                             //Natural: MOVE '07' TO #C6 ( 1,1 ) #C7 ( 1,1 ) #A6 ( 1,2 ) #A7 ( 1,1 ) #C1 ( 3 ) #A1 ( 4 )
        ldaIaal584w.getPnd_C7().getValue(1,1).setValue("07");
        ldaIaal584w.getPnd_A6().getValue(1,2).setValue("07");
        ldaIaal584w.getPnd_A7().getValue(1,1).setValue("07");
        ldaIaal584w.getPnd_C1().getValue(3).setValue("07");
        ldaIaal584w.getPnd_A1().getValue(4).setValue("07");
        ldaIaal584w.getPnd_C6().getValue(2,2).setValue("08");                                                                                                             //Natural: MOVE '08' TO #C6 ( 2,2 ) #C7 ( 2,1 ) #A6 ( 2,2 ) #A7 ( 2,1 ) #C1 ( 4 ) #A1 ( 5 )
        ldaIaal584w.getPnd_C7().getValue(2,1).setValue("08");
        ldaIaal584w.getPnd_A6().getValue(2,2).setValue("08");
        ldaIaal584w.getPnd_A7().getValue(2,1).setValue("08");
        ldaIaal584w.getPnd_C1().getValue(4).setValue("08");
        ldaIaal584w.getPnd_A1().getValue(5).setValue("08");
        ldaIaal584w.getPnd_C6().getValue(3,2).setValue("09");                                                                                                             //Natural: MOVE '09' TO #C6 ( 3,2 ) #C7 ( 3,1 ) #A6 ( 3,2 ) #A7 ( 3,1 ) #C1 ( 5 ) #A1 ( 6 )
        ldaIaal584w.getPnd_C7().getValue(3,1).setValue("09");
        ldaIaal584w.getPnd_A6().getValue(3,2).setValue("09");
        ldaIaal584w.getPnd_A7().getValue(3,1).setValue("09");
        ldaIaal584w.getPnd_C1().getValue(5).setValue("09");
        ldaIaal584w.getPnd_A1().getValue(6).setValue("09");
        ldaIaal584w.getPnd_C6().getValue(1,2).setValue("10");                                                                                                             //Natural: MOVE '10' TO #C6 ( 1,2 ) #C7 ( 4,1 ) #A6 ( 1,3 ) #A7 ( 4,2 ) #C1 ( 6 ) #A1 ( 7 )
        ldaIaal584w.getPnd_C7().getValue(4,1).setValue("10");
        ldaIaal584w.getPnd_A6().getValue(1,3).setValue("10");
        ldaIaal584w.getPnd_A7().getValue(4,2).setValue("10");
        ldaIaal584w.getPnd_C1().getValue(6).setValue("10");
        ldaIaal584w.getPnd_A1().getValue(7).setValue("10");
        ldaIaal584w.getPnd_C6().getValue(2,3).setValue("11");                                                                                                             //Natural: MOVE '11' TO #C6 ( 2,3 ) #C7 ( 5,2 ) #A6 ( 2,3 ) #A7 ( 5,2 ) #C1 ( 7 ) #A1 ( 8 )
        ldaIaal584w.getPnd_C7().getValue(5,2).setValue("11");
        ldaIaal584w.getPnd_A6().getValue(2,3).setValue("11");
        ldaIaal584w.getPnd_A7().getValue(5,2).setValue("11");
        ldaIaal584w.getPnd_C1().getValue(7).setValue("11");
        ldaIaal584w.getPnd_A1().getValue(8).setValue("11");
        ldaIaal584w.getPnd_C6().getValue(3,3).setValue("12");                                                                                                             //Natural: MOVE '12' TO #C6 ( 3,3 ) #C7 ( 6,2 ) #A6 ( 3,3 ) #A7 ( 6,2 ) #C1 ( 8 ) #A1 ( 9 )
        ldaIaal584w.getPnd_C7().getValue(6,2).setValue("12");
        ldaIaal584w.getPnd_A6().getValue(3,3).setValue("12");
        ldaIaal584w.getPnd_A7().getValue(6,2).setValue("12");
        ldaIaal584w.getPnd_C1().getValue(8).setValue("12");
        ldaIaal584w.getPnd_A1().getValue(9).setValue("12");
        F1:                                                                                                                                                               //Natural: FOR #A = 1 TO 3
        for (ldaIaal584w.getPnd_A().setValue(1); condition(ldaIaal584w.getPnd_A().lessOrEqual(3)); ldaIaal584w.getPnd_A().nadd(1))
        {
            ldaIaal584w.getPnd_C().setValue(ldaIaal584w.getPnd_A());                                                                                                      //Natural: MOVE #A TO #C
            FOR01:                                                                                                                                                        //Natural: FOR #B = 1 TO 4
            for (ldaIaal584w.getPnd_B().setValue(1); condition(ldaIaal584w.getPnd_B().lessOrEqual(4)); ldaIaal584w.getPnd_B().nadd(1))
            {
                ldaIaal584w.getPnd_T6().getValue(ldaIaal584w.getPnd_A(),ldaIaal584w.getPnd_B()).setValue(ldaIaal584w.getPnd_C_Pnd_C_Alph());                              //Natural: MOVE #C-ALPH TO #T6 ( #A,#B )
                ldaIaal584w.getPnd_C().compute(new ComputeParameters(false, ldaIaal584w.getPnd_C()), DbsField.add(3,ldaIaal584w.getPnd_C()));                             //Natural: ADD 3 #C GIVING #C
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F2:                                                                                                                                                               //Natural: FOR #A = 1 TO 6
        for (ldaIaal584w.getPnd_A().setValue(1); condition(ldaIaal584w.getPnd_A().lessOrEqual(6)); ldaIaal584w.getPnd_A().nadd(1))
        {
            ldaIaal584w.getPnd_C().setValue(ldaIaal584w.getPnd_A());                                                                                                      //Natural: MOVE #A TO #C
            FOR02:                                                                                                                                                        //Natural: FOR #B = 1 TO 2
            for (ldaIaal584w.getPnd_B().setValue(1); condition(ldaIaal584w.getPnd_B().lessOrEqual(2)); ldaIaal584w.getPnd_B().nadd(1))
            {
                ldaIaal584w.getPnd_T7().getValue(ldaIaal584w.getPnd_A(),ldaIaal584w.getPnd_B()).setValue(ldaIaal584w.getPnd_C_Pnd_C_Alph());                              //Natural: MOVE #C-ALPH TO #T7 ( #A,#B )
                ldaIaal584w.getPnd_C().compute(new ComputeParameters(false, ldaIaal584w.getPnd_C()), DbsField.add(6,ldaIaal584w.getPnd_C()));                             //Natural: ADD 6 #C GIVING #C
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F3:                                                                                                                                                               //Natural: FOR #A = 1 TO 12
        for (ldaIaal584w.getPnd_A().setValue(1); condition(ldaIaal584w.getPnd_A().lessOrEqual(12)); ldaIaal584w.getPnd_A().nadd(1))
        {
            ldaIaal584w.getPnd_T1().getValue(ldaIaal584w.getPnd_A()).setValue(ldaIaal584w.getPnd_A_Pnd_A_Alph());                                                         //Natural: MOVE #A-ALPH TO #T1 ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal584w.getPnd_A().reset();                                                                                                                                   //Natural: RESET #A #B #C
        ldaIaal584w.getPnd_B().reset();
        ldaIaal584w.getPnd_C().reset();
    }
    private void sub_Pnd_Determine_Type_Of_Contract() throws Exception                                                                                                    //Natural: #DETERMINE-TYPE-OF-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal584w.getVw_iaa_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-VIEW CNTRCT-PPCN-NBR = #W-CONTRACT
        (
        "FN1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal584w.getPnd_W_Contract(), WcType.WITH) },
        1
        );
        FN1:
        while (condition(ldaIaal584w.getVw_iaa_Cntrct_View().readNextRow("FN1")))
        {
            ldaIaal584w.getVw_iaa_Cntrct_View().setIfNotFoundControlFlag(false);
            //* *Y2NCAM
            ldaIaal584w.getPnd_W_Contract_Issue_Dte().setValue(ldaIaal584w.getIaa_Cntrct_View_Cntrct_Issue_Dte());                                                        //Natural: MOVE CNTRCT-ISSUE-DTE TO #W-CONTRACT-ISSUE-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        short decideConditionsMet1256 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT-VIEW.CNTRCT-ISSUE-DTE >= 199104 AND ( #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z' ) AND IAA-CNTRCT-VIEW.CNTRCT-OPTN-CDE = 21
        if (condition(ldaIaal584w.getIaa_Cntrct_View_Cntrct_Issue_Dte().greaterOrEqual(199104) && DbsUtil.maskMatches(ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") 
            && ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z") && ldaIaal584w.getIaa_Cntrct_View_Cntrct_Optn_Cde().equals(21)))
        {
            decideConditionsMet1256++;
            ldaIaal584w.getPnd_Contract_Type_Field().setValue("A");                                                                                                       //Natural: MOVE 'A' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z'
        else if (condition(DbsUtil.maskMatches(ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") && ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z")))
        {
            decideConditionsMet1256++;
            ldaIaal584w.getPnd_Contract_Type_Field().setValue("T");                                                                                                       //Natural: MOVE 'T' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN #W-CNTRCT-1 = MASK ( N ) OR #W-CNTRCT-1 EQ 'Z'
        else if (condition(DbsUtil.maskMatches(ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1(),"N") || ldaIaal584w.getPnd_W_Contract_Pnd_W_Cntrct_1().equals("Z")))
        {
            decideConditionsMet1256++;
            ldaIaal584w.getPnd_Contract_Type_Field().setValue("C");                                                                                                       //Natural: MOVE 'C' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*    WRITE #W-CONTRACT ' DOES NOT FIT INTO TEACHERS OR CREF CATEGORY'
            ldaIaal584w.getPnd_Contract_Type_Field().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Overpay_Breakdown() throws Exception                                                                                                             //Natural: #OVERPAY-BREAKDOWN
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal584w.getPnd_Ovrpy_Tot().setValue(ldaIaal584w.getPnd_W_Rate_Ovrpy().getValue(ldaIaal584w.getPnd_R()));                                                      //Natural: MOVE #W-RATE-OVRPY ( #R ) TO #OVRPY-TOT
        ldaIaal584w.getPnd_Rate_Guar_Ovp().setValue(ldaIaal584w.getPnd_W_Rate_Guar().getValue(ldaIaal584w.getPnd_R()));                                                   //Natural: MOVE #W-RATE-GUAR ( #R ) TO #RATE-GUAR-OVP
        ldaIaal584w.getPnd_Rate_Divd_Ovp().setValue(ldaIaal584w.getPnd_W_Rate_Divd().getValue(ldaIaal584w.getPnd_R()));                                                   //Natural: MOVE #W-RATE-DIVD ( #R ) TO #RATE-DIVD-OVP
        ldaIaal584w.getPnd_Tot_Guar_Divd().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Tot_Guar_Divd()), ldaIaal584w.getPnd_Rate_Guar_Ovp().add(ldaIaal584w.getPnd_Rate_Divd_Ovp())); //Natural: COMPUTE #TOT-GUAR-DIVD = #RATE-GUAR-OVP + #RATE-DIVD-OVP
        ldaIaal584w.getPnd_Calc_Hold().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Calc_Hold()), ldaIaal584w.getPnd_Rate_Guar_Ovp().divide(ldaIaal584w.getPnd_Tot_Guar_Divd())); //Natural: COMPUTE #CALC-HOLD = #RATE-GUAR-OVP / #TOT-GUAR-DIVD
        ldaIaal584w.getPnd_W_Rate_Guar_Ovp().compute(new ComputeParameters(true, ldaIaal584w.getPnd_W_Rate_Guar_Ovp()), ldaIaal584w.getPnd_Calc_Hold().multiply(ldaIaal584w.getPnd_Ovrpy_Tot())); //Natural: COMPUTE ROUNDED #W-RATE-GUAR-OVP = #CALC-HOLD * #OVRPY-TOT
        ldaIaal584w.getPnd_Calc_Hold().compute(new ComputeParameters(false, ldaIaal584w.getPnd_Calc_Hold()), ldaIaal584w.getPnd_Rate_Divd_Ovp().divide(ldaIaal584w.getPnd_Tot_Guar_Divd())); //Natural: COMPUTE #CALC-HOLD = #RATE-DIVD-OVP / #TOT-GUAR-DIVD
        ldaIaal584w.getPnd_W_Rate_Divd_Ovp().compute(new ComputeParameters(true, ldaIaal584w.getPnd_W_Rate_Divd_Ovp()), ldaIaal584w.getPnd_Calc_Hold().multiply(ldaIaal584w.getPnd_Ovrpy_Tot())); //Natural: COMPUTE ROUNDED #W-RATE-DIVD-OVP = #CALC-HOLD * #OVRPY-TOT
        ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Guar_Ovp().setValue(ldaIaal584w.getPnd_W_Rate_Guar_Ovp());                                                                 //Natural: MOVE #W-RATE-GUAR-OVP TO #T-RATE-GUAR-OVP
        ldaIaal584w.getPnd_T_Record_Pnd_T_Rate_Divd_Ovp().setValue(ldaIaal584w.getPnd_W_Rate_Divd_Ovp());                                                                 //Natural: MOVE #W-RATE-DIVD-OVP TO #T-RATE-DIVD-OVP
        ldaIaal584w.getPnd_C_Rate_Guar_Ovp().nadd(ldaIaal584w.getPnd_W_Rate_Guar_Ovp());                                                                                  //Natural: ADD #W-RATE-GUAR-OVP TO #C-RATE-GUAR-OVP
        ldaIaal584w.getPnd_C_Rate_Divd_Ovp().nadd(ldaIaal584w.getPnd_W_Rate_Divd_Ovp());                                                                                  //Natural: ADD #W-RATE-DIVD-OVP TO #C-RATE-DIVD-OVP
        ldaIaal584w.getPnd_Grand_Rate_Guar_Ovp().nadd(ldaIaal584w.getPnd_W_Rate_Guar_Ovp());                                                                              //Natural: ADD #W-RATE-GUAR-OVP TO #GRAND-RATE-GUAR-OVP
        ldaIaal584w.getPnd_Grand_Rate_Divd_Ovp().nadd(ldaIaal584w.getPnd_W_Rate_Divd_Ovp());                                                                              //Natural: ADD #W-RATE-DIVD-OVP TO #GRAND-RATE-DIVD-OVP
        ldaIaal584w.getPnd_Rate_Guar_Ovp().reset();                                                                                                                       //Natural: RESET #RATE-GUAR-OVP #RATE-DIVD-OVP #OVRPY-TOT #TOT-GUAR-DIVD #CALC-HOLD #W-RATE-GUAR-OVP #W-RATE-DIVD-OVP
        ldaIaal584w.getPnd_Rate_Divd_Ovp().reset();
        ldaIaal584w.getPnd_Ovrpy_Tot().reset();
        ldaIaal584w.getPnd_Tot_Guar_Divd().reset();
        ldaIaal584w.getPnd_Calc_Hold().reset();
        ldaIaal584w.getPnd_W_Rate_Guar_Ovp().reset();
        ldaIaal584w.getPnd_W_Rate_Divd_Ovp().reset();
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCAM
        if (condition(DbsUtil.maskMatches(ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCAM
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCAM
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Determine_Occurance_Mode_6() throws Exception                                                                                                    //Natural: #DETERMINE-OCCURANCE-MODE-6
    {
        if (BLNatReinput.isReinput()) return;

        FD1:                                                                                                                                                              //Natural: FOR #D = 1 TO 3
        for (ldaIaal584w.getPnd_D().setValue(1); condition(ldaIaal584w.getPnd_D().lessOrEqual(3)); ldaIaal584w.getPnd_D().nadd(1))
        {
            FE1:                                                                                                                                                          //Natural: FOR #E = 1 TO 4
            for (ldaIaal584w.getPnd_E().setValue(1); condition(ldaIaal584w.getPnd_E().lessOrEqual(4)); ldaIaal584w.getPnd_E().nadd(1))
            {
                short decideConditionsMet1305 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
                if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
                {
                    decideConditionsMet1305++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_T6().getValue(ldaIaal584w.getPnd_D(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #T6 ( #D,#E )
                        ldaIaal584w.getPnd_E()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur().setValue(ldaIaal584w.getPnd_E());                                                                                 //Natural: MOVE #E TO #TEMP-OCCUR
                        if (true) break FD1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FD1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
                else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
                {
                    decideConditionsMet1305++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_C6().getValue(ldaIaal584w.getPnd_D(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #C6 ( #D,#E )
                        ldaIaal584w.getPnd_E()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur().setValue(ldaIaal584w.getPnd_E());                                                                                 //Natural: MOVE #E TO #TEMP-OCCUR
                        if (true) break FD1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FD1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
                else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
                {
                    decideConditionsMet1305++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_A6().getValue(ldaIaal584w.getPnd_D(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #A6 ( #D,#E )
                        ldaIaal584w.getPnd_E()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur().setValue(ldaIaal584w.getPnd_E());                                                                                 //Natural: MOVE #E TO #TEMP-OCCUR
                        if (true) break FD1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FD1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Determine_Occurance_Mode_7() throws Exception                                                                                                    //Natural: #DETERMINE-OCCURANCE-MODE-7
    {
        if (BLNatReinput.isReinput()) return;

        FQ1:                                                                                                                                                              //Natural: FOR #Q = 1 TO 6
        for (ldaIaal584w.getPnd_Q().setValue(1); condition(ldaIaal584w.getPnd_Q().lessOrEqual(6)); ldaIaal584w.getPnd_Q().nadd(1))
        {
            FX1:                                                                                                                                                          //Natural: FOR #X1 = 1 TO 2
            for (ldaIaal584w.getPnd_X1().setValue(1); condition(ldaIaal584w.getPnd_X1().lessOrEqual(2)); ldaIaal584w.getPnd_X1().nadd(1))
            {
                short decideConditionsMet1335 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
                if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
                {
                    decideConditionsMet1335++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_T7().getValue(ldaIaal584w.getPnd_Q(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #T7 ( #Q,#X1 )
                        ldaIaal584w.getPnd_X1()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur_2().setValue(ldaIaal584w.getPnd_X1());                                                                              //Natural: MOVE #X1 TO #TEMP-OCCUR-2
                        if (true) break FQ1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FQ1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
                else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
                {
                    decideConditionsMet1335++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_C7().getValue(ldaIaal584w.getPnd_Q(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #C7 ( #Q,#X1 )
                        ldaIaal584w.getPnd_X1()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur_2().setValue(ldaIaal584w.getPnd_X1());                                                                              //Natural: MOVE #X1 TO #TEMP-OCCUR-2
                        if (true) break FQ1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FQ1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
                else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
                {
                    decideConditionsMet1335++;
                    //* *Y2NCAM
                    if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_A7().getValue(ldaIaal584w.getPnd_Q(),           //Natural: IF #W-LAST-PAY-DATE-MM-A = #A7 ( #Q,#X1 )
                        ldaIaal584w.getPnd_X1()))))
                    {
                        ldaIaal584w.getPnd_Temp_Occur_2().setValue(ldaIaal584w.getPnd_X1());                                                                              //Natural: MOVE #X1 TO #TEMP-OCCUR-2
                        if (true) break FQ1;                                                                                                                              //Natural: ESCAPE BOTTOM ( FQ1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FQ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FQ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Determine_Occurance_Mode_1() throws Exception                                                                                                    //Natural: #DETERMINE-OCCURANCE-MODE-1
    {
        if (BLNatReinput.isReinput()) return;

        FM1:                                                                                                                                                              //Natural: FOR #M1 = 1 TO 12
        for (ldaIaal584w.getPnd_M1().setValue(1); condition(ldaIaal584w.getPnd_M1().lessOrEqual(12)); ldaIaal584w.getPnd_M1().nadd(1))
        {
            short decideConditionsMet1363 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-TYPE-FIELD = 'T'
            if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("T")))
            {
                decideConditionsMet1363++;
                //*           WRITE '=' #W-LAST-PAY-DATE-MM-A '=' #T1(#M1) '=' #M1
                //* *Y2NCAM
                if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_T1().getValue(ldaIaal584w.getPnd_M1()))))           //Natural: IF #W-LAST-PAY-DATE-MM-A = #T1 ( #M1 )
                {
                    ldaIaal584w.getPnd_Temp_Occur_3().setValue(ldaIaal584w.getPnd_M1());                                                                                  //Natural: MOVE #M1 TO #TEMP-OCCUR-3
                    if (true) break FM1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FM1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-TYPE-FIELD = 'C'
            else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("C")))
            {
                decideConditionsMet1363++;
                //* *Y2NCAM
                if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_C1().getValue(ldaIaal584w.getPnd_M1()))))           //Natural: IF #W-LAST-PAY-DATE-MM-A = #C1 ( #M1 )
                {
                    ldaIaal584w.getPnd_Temp_Occur_3().setValue(ldaIaal584w.getPnd_M1());                                                                                  //Natural: MOVE #M1 TO #TEMP-OCCUR-3
                    if (true) break FM1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FM1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-TYPE-FIELD = 'A'
            else if (condition(ldaIaal584w.getPnd_Contract_Type_Field().equals("A")))
            {
                decideConditionsMet1363++;
                //* *Y2NCAM
                if (condition(ldaIaal584w.getPnd_W_Last_Pay_Date_Pnd_W_Last_Pay_Date_Mm_A().equals(ldaIaal584w.getPnd_A1().getValue(ldaIaal584w.getPnd_M1()))))           //Natural: IF #W-LAST-PAY-DATE-MM-A = #A1 ( #M1 )
                {
                    ldaIaal584w.getPnd_Temp_Occur_3().setValue(ldaIaal584w.getPnd_M1());                                                                                  //Natural: MOVE #M1 TO #TEMP-OCCUR-3
                    if (true) break FM1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FM1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal584w.getPnd_Page_Ctr_1().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-CTR-1
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ","IAAP584A",new TabSetting(47),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new                 //Natural: WRITE ( 1 ) 'PROGRAM ' 'IAAP584A' 47T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR-1
                        TabSetting(119),"PAGE ",ldaIaal584w.getPnd_Page_Ctr_1());
                    //* *Y2NCAM
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(52),"GROSS OVERPAYMENT AMOUNTS RECORDED");                      //Natural: WRITE ( 1 ) '   DATE ' *DATU 52T 'GROSS OVERPAYMENT AMOUNTS RECORDED'
                    //* ***************************
                    //* *  YEAR 2000 FIX STARTS  **
                    //* ***************************
                    //* *Y2CHAM
                    pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N.setValue(ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Yy());                                                  //Natural: MOVE #PARM-DATE-YY TO #Y2-PARM-DATE-YY-N
                    ldaIaal584w.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy));
                    //* *COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY
                    //* ***************************
                    //* *  YEAR 2000 FIX END     **
                    //* ***************************
                    //* *Y2NCAM
                    //* *Y2NCAM
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(62)," FOR",new TabSetting(67),ldaIaal584w.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 62T ' FOR' 67T #W-PARM-DATE
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TIAA TRADITIONAL");                                                                               //Natural: WRITE ( 1 ) / 'TIAA TRADITIONAL'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(12),"ST",new TabSetting(17),"       OPTION",new                //Natural: WRITE ( 1 ) 001T 'CONTRACT' 012T 'ST' 017T '       OPTION' 040T 'MODE' 047T '  PAYMENT ' 060T ' FULL GUAR  ' 075T ' FULL DIVD  ' 090T 'GUARANTEE AMT' 106T 'DIVIDEND AMT' 121T ' OVERPAYMENT'
                        TabSetting(40),"MODE",new TabSetting(47),"  PAYMENT ",new TabSetting(60)," FULL GUAR  ",new TabSetting(75)," FULL DIVD  ",new TabSetting(90),"GUARANTEE AMT",new 
                        TabSetting(106),"DIVIDEND AMT",new TabSetting(121)," OVERPAYMENT");
                    //* *Y2NCAM
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"DATE",new TabSetting(64),"PAID",new TabSetting(79),"PAID",new TabSetting(92),"OVERPAID",new  //Natural: WRITE ( 1 ) 050T 'DATE' 064T 'PAID' 079T 'PAID' 092T 'OVERPAID' 108T 'OVERPAID' 125T 'TOTAL'
                        TabSetting(108),"OVERPAID",new TabSetting(125),"TOTAL");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(12),"--",new TabSetting(17),"--------------------",new        //Natural: WRITE ( 1 ) 001T '---------' 012T '--' 017T '--------------------' 040T '----' 047T '----------' 060T '------------' 075T '------------' 090T '-------------' 106T '------------' 121T '------------'
                        TabSetting(40),"----",new TabSetting(47),"----------",new TabSetting(60),"------------",new TabSetting(75),"------------",new TabSetting(90),"-------------",new 
                        TabSetting(106),"------------",new TabSetting(121),"------------");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    ldaIaal584w.getPnd_Page_Ctr_2().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-CTR-2
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ","IAAP584B",new TabSetting(47),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new                 //Natural: WRITE ( 2 ) 'PROGRAM ' 'IAAP584B' 47T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR-2
                        TabSetting(119),"PAGE ",ldaIaal584w.getPnd_Page_Ctr_2());
                    //* *Y2NCAM
                    getReports().write(2, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(52),"GROSS OVERPAYMENT AMOUNTS RECORDED");                      //Natural: WRITE ( 2 ) '   DATE ' *DATU 52T 'GROSS OVERPAYMENT AMOUNTS RECORDED'
                    //* ***************************
                    //* *  YEAR 2000 FIX STARTS  **
                    //* ***************************
                    //* *Y2CHAM
                    pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N.setValue(ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Yy());                                                  //Natural: MOVE #PARM-DATE-YY TO #Y2-PARM-DATE-YY-N
                    ldaIaal584w.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal584w.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy));
                    //* *COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY
                    //* ***************************
                    //* *  YEAR 2000 FIX END     **
                    //* ***************************
                    //* *Y2NCAM
                    //* *Y2NCAM
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(62)," FOR",new TabSetting(67),ldaIaal584w.getPnd_W_Parm_Date());                            //Natural: WRITE ( 2 ) 62T ' FOR' 67T #W-PARM-DATE
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_Comp_Desc,pnd_Fund_Desc);                                                                      //Natural: WRITE ( 2 ) / #COMP-DESC #FUND-DESC
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(18),"STATUS",new TabSetting(30),"       OPTION",new            //Natural: WRITE ( 2 ) 001T 'CONTRACT' 018T 'STATUS' 030T '       OPTION' 056T 'MODE' 070T 'PAYMENT DATE' 092T 'GROSS AMOUNT' 114T 'OVERPAYMENT'
                        TabSetting(56),"MODE",new TabSetting(70),"PAYMENT DATE",new TabSetting(92),"GROSS AMOUNT",new TabSetting(114),"OVERPAYMENT");
                    //* *Y2NCAM
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(18),"------",new TabSetting(30),"--------------------",new    //Natural: WRITE ( 2 ) 001T '---------' 018T '------' 030T '--------------------' 056T '----' 070T '------------' 092T '------------' 114T '------------'
                        TabSetting(56),"----",new TabSetting(70),"------------",new TabSetting(92),"------------",new TabSetting(114),"------------");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
