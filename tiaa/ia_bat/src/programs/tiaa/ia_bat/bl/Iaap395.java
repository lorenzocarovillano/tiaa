/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:37 PM
**        * FROM NATURAL PROGRAM : Iaap395
************************************************************
**        * FILE NAME            : Iaap395.java
**        * CLASS NAME           : Iaap395
**        * INSTANCE NAME        : Iaap395
************************************************************
************************************************************************
************************************************************************
* PROGRAM  : IAAP395
* SYSTEM   : IA
* TITLE    : SPECIAL FUNCTION TIAA DIVIDEND CHANGE JOR JAN PMNTS
* CREATED  : OCT 31, 96
* FUNCTION : CALL ACTUARIAL MODULE FOR TIAA & GRADED CONTRACTS
* TITLE    : FOR JAN. DIVIDEND RATE CHANGE
*
*
* HISTORY
*
*  11/04     ADDED WRITE SWITCH IN PARM AIAA0380
*            DO SCAN ON 11/04 FOR CHANGES
*
*  06/03     INCREASE RATE OCCURS FROM 40 TO 90
*            INCREASE AMTS FROM 7.2 TO 9.2
*            DO SCAN ON  6/03 FOR CHANGES
*
*  11/02     ADDED NEW ACTUARY MODULE FOR DIV CHANGE
*            DO SCAN ON 11/02 FOR CHANGES
*
*  12/01     ADDED NEW ACTUARY MODULE FOR DIV CHANGE AND LOGIC
*            TO WRITE WORK FILE FOR P/I(OPT 22) CONTRACTS TO BE
*            INPUT TO TPA REINVESTMNET JOB TO UPDATE TPA REINVEST FILE
*            TO REFLECT ANY PMT CHANGES
*
*  01/01     ADDED OPTION CODES 22,25,27,28,29,30 & 31 FOR
*            TPA, IPRO, P/I CONVERSION TO IAIQ
*  12/00     ADDED NEW ACTUARY MODULE AIAN073 IN PLACE OF AIAN070
*            FOR JAN 2001 DIVIDEND CHANGE
*            DO SCAN ON 12/00
*  11/99     ADDED NEW ACTUARY MODULE AIAN070 IN PLACE OF AIAN038
*            FOR JAN 2000 DIVIDEND CHANGE
*            ADD ORIGIN 37 , 38 & 40 FOR PA SELECT
*            DO SCAN ON 11/99
*  11/98     ADDED LOGIC TO HANDLE ACTUARAIL RETURN CODE = 09
*
*   9/98     ADDED LOGIC TO HANDLE OLD-PMT-AMTS FOR CONTRACTS NOT
*            REVALUED IN JAN. OPTION 21
*            DO SCAN ON 9/98
*
*  11/97     ADDED NEW ACTUARY MODULE AIAN038 IN PLACE OF AIAN020
*            FOR JAN 1998 DIVIDEND CHANGE
*            DO SCAN ON 11/97
*
*  04/08     INCREASE RATE OCCURS FROM 90 TO 99
*  11/10     POPULATE NEW FIELDS ON AIAA0380.
*  03/12     RATE BASE EXPANSION - INCREASE OCCURRENCES FROM 99 TO 250
*            DO SCAN ON 3/12
*  11/17     FIX NAT1003 ERROR - GLOBAL LIMIT FOR DATABASE CALLS REACHED
*            DO SCAN ON 11/2017 FOR CHANGES.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap395 extends BLNatBase
{
    // Data Areas
    private PdaAiaa0380 pdaAiaa0380;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Tpar_To_Tpa_Interface;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cpr_Id_Nbr;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Payee_Cde;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Rate_Cnt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp;

    private DbsGroup pnd_Tpar_To_Tpa_Interface__R_Field_1;

    private DbsGroup pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Cde;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Div_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Optn_Cde;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Div_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Div_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Per_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Per_Amt;
    private DbsField pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Filler;

    private DbsGroup pnd_Totl_Tpa_Total_Rec;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Cntrct_Type;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Div_Amt;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Div_Amt;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Per_Amt;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Per_Amt;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_No_Cntrcts;
    private DbsField pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Filler;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_I4;
    private DbsField pnd_K;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_2;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role__R_Field_3;
    private DbsField iaa_Cntrct_Prtcpnt_Role__Filler1;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Pnd_Mode_Mm;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_4;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte;

    private DbsGroup pnd_Trn_Fund_Rec;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_5;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_6;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_7;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf;

    private DbsGroup pnd_Trailer_Fund_Rec;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt;

    private DbsGroup pnd_Trailer_Fund_Rec__R_Field_8;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf;

    private DbsGroup pnd_Grd_Cntrcts_Rec;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Payee_Cde;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Fund_Cde;

    private DbsGroup pnd_Grd_Cntrcts_Rec__R_Field_9;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Cde;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Fund_Cde;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Prev_Amt;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Curr_Amt;
    private DbsField pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt;

    private DbsGroup pnd_Tot_Accumulators;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt;
    private DbsField pnd_Wrk_Dte_Ccyymmdd;

    private DbsGroup pnd_Wrk_Dte_Ccyymmdd__R_Field_10;
    private DbsField pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Tot_Rec_Cnt;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_Wrk_Tot_Fnd_Rec_Changed;
    private DbsField pnd_Wrk_Chg_Tiaa_Payees;
    private DbsField pnd_Wrk_Chg_Fund_Payees;
    private DbsField pnd_Wrk_Chg_Per_Pay_Amt;
    private DbsField pnd_Wrk_Chg_Per_Div_Amt;
    private DbsField pnd_Wrk_Chg_Final_Pay_Amt;
    private DbsField pnd_Wrk_Chg_Final_Div_Amt;
    private DbsField pnd_Wrk_Prev_Pmt_Gross;
    private DbsField pnd_Wrk_Curr_Pmt_Gross;
    private DbsField pnd_Wrk_Old_Final_Pay_Amt;
    private DbsField pnd_Wrk_Old_Final_Div_Amt;
    private DbsField pnd_Wrk_New_Final_Pay_Amt;
    private DbsField pnd_Wrk_New_Final_Div_Amt;
    private DbsField pnd_Last_Updte_Dte;
    private DbsField pnd_Next_Updte_Dte;
    private DbsField pnd_Datd;
    private DbsField pnd_Date8;

    private DbsGroup pnd_Date8__R_Field_11;
    private DbsField pnd_Date8_Pnd_Next_Updt_Ccyymm;
    private DbsField pnd_Date8_Pnd_Next_Updt_Dd;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_12;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_Have_Cntrct_Info_Sw;
    private DbsField pnd_Cntrct_Fnd_Sw;
    private DbsField pnd_Wrk_Rea_Payees;
    private DbsField pnd_Inactive_Payees;
    private DbsField pnd_Idx_Rate_Cde;
    private DbsField pnd_Tot_Rate_Cde;
    private DbsField pnd_Tot_Rate_Div;
    private DbsField pnd_Tot_Rate_Final_Div;
    private DbsField pnd_Gtot_Rate_Div;
    private DbsField pnd_Gtot_Rate_Final_Div;
    private DbsField pnd_Translate_Tab;
    private DbsField pnd_Work_Rate_Cde;

    private DbsGroup pnd_Work_Rate_Cde__R_Field_13;
    private DbsField pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N;

    private DbsGroup pnd_Hd_Txt1;
    private DbsField pnd_Hd_Txt1_Hd_Txt1;
    private DbsField pnd_Hd_Txt1_Hd_Txt2;
    private DbsField pnd_Hd_Txt1_Hd_Txt3;

    private DbsGroup pnd_Hd_Txt1__R_Field_14;
    private DbsField pnd_Hd_Txt1_Pnd_Hd_Txt1_R;

    private DbsGroup pnd_Hd2_Txt1;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt1;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt2;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt3;

    private DbsGroup pnd_Hd2_Txt1__R_Field_15;
    private DbsField pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R;
    private DbsField pnd_Tpa_Ipro_Codes;
    private DbsField pnd_Tpa_Option_Codes;
    private DbsField pnd_Calc_Mthd;

    private DbsGroup pnd_Calc_Mthd__R_Field_16;
    private DbsField pnd_Calc_Mthd_Pnd_Inv_Dte;
    private DbsField pnd_Calc_Mthd_Pnd_C_Mthd;
    private DbsField pnd_Rc;
    private DbsField pnd_W_Date;
    private DbsField pnd_Nxt_Pmt_Dte;

    private DbsGroup pnd_Nxt_Pmt_Dte__R_Field_17;
    private DbsField pnd_Nxt_Pmt_Dte__Filler2;
    private DbsField pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm;

    private DbsGroup pnd_Nxt_Pmt_Dte__R_Field_18;
    private DbsField pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N;
    private DbsField pls_Sub_Sw;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAiaa0380 = new PdaAiaa0380(localVariables);

        // Local Variables

        pnd_Tpar_To_Tpa_Interface = localVariables.newGroupInRecord("pnd_Tpar_To_Tpa_Interface", "#TPAR-TO-TPA-INTERFACE");
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cpr_Id_Nbr = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cpr_Id_Nbr", "#TPAR-CPR-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Ppcn_Nbr = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Ppcn_Nbr", 
            "#TPAR-CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Payee_Cde = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Payee_Cde", 
            "#TPAR-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Rate_Cnt = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Rate_Cnt", "#TPAR-RATE-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp = pnd_Tpar_To_Tpa_Interface.newFieldArrayInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp", 
            "#TPAR-TIAA-RATE-DATA-GRP", FieldType.STRING, 1, new DbsArrayController(1, 1980));

        pnd_Tpar_To_Tpa_Interface__R_Field_1 = pnd_Tpar_To_Tpa_Interface.newGroupInGroup("pnd_Tpar_To_Tpa_Interface__R_Field_1", "REDEFINE", pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp);

        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2 = pnd_Tpar_To_Tpa_Interface__R_Field_1.newGroupArrayInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2", 
            "#TPAR-TIAA-RATE-DATA-GRP2", new DbsArrayController(1, 99));
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Cde = pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Cde", 
            "#TPAR-TIAA-RATE-CDE", FieldType.STRING, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Pay_Amt = pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Pay_Amt", 
            "#TPAR-TIAA-PER-PAY-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Div_Amt = pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Data_Grp2.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Div_Amt", 
            "#TPAR-TIAA-PER-DIV-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Optn_Cde = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Optn_Cde", "#TPAR-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Div_Amt = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Div_Amt", 
            "#TPAR-OLD-TOT-DIV-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Div_Amt = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Div_Amt", 
            "#TPAR-NEW-TOT-DIV-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Per_Amt = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Per_Amt", 
            "#TPAR-OLD-TOT-PER-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Per_Amt = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Per_Amt", 
            "#TPAR-NEW-TOT-PER-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Filler = pnd_Tpar_To_Tpa_Interface.newFieldInGroup("pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Filler", "#TPAR-FILLER", 
            FieldType.STRING, 26);

        pnd_Totl_Tpa_Total_Rec = localVariables.newGroupInRecord("pnd_Totl_Tpa_Total_Rec", "#TOTL-TPA-TOTAL-REC");
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Cntrct_Type = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Cntrct_Type", "#TOTL-CNTRCT-TYPE", 
            FieldType.STRING, 4);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Div_Amt = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Div_Amt", "#TOTL-TOT-OLD-DIV-AMT", 
            FieldType.NUMERIC, 13, 2);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Div_Amt = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Div_Amt", "#TOTL-TOT-NEW-DIV-AMT", 
            FieldType.NUMERIC, 13, 2);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Per_Amt = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Per_Amt", "#TOTL-TOT-OLD-PER-AMT", 
            FieldType.NUMERIC, 13, 2);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Per_Amt = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Per_Amt", "#TOTL-TOT-NEW-PER-AMT", 
            FieldType.NUMERIC, 13, 2);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_No_Cntrcts = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_No_Cntrcts", "#TOTL-TOT-NO-CNTRCTS", 
            FieldType.NUMERIC, 8);
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Filler = pnd_Totl_Tpa_Total_Rec.newFieldInGroup("pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Filler", "#TOTL-TOT-FILLER", 
            FieldType.STRING, 16);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.INTEGER, 4);
        pnd_I4 = localVariables.newFieldInRecord("pnd_I4", "#I4", FieldType.INTEGER, 4);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Cntrct_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_2", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_2.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_2.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_2.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");

        iaa_Cntrct_Prtcpnt_Role__R_Field_3 = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role__R_Field_3", "REDEFINE", 
            iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);
        iaa_Cntrct_Prtcpnt_Role__Filler1 = iaa_Cntrct_Prtcpnt_Role__R_Field_3.newFieldInGroup("iaa_Cntrct_Prtcpnt_Role__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        iaa_Cntrct_Prtcpnt_Role_Pnd_Mode_Mm = iaa_Cntrct_Prtcpnt_Role__R_Field_3.newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Pnd_Mode_Mm", "#MODE-MM", FieldType.STRING, 
            2);
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_4 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_4", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_4.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_4.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Trn_Fund_Rec = localVariables.newGroupInRecord("pnd_Trn_Fund_Rec", "#TRN-FUND-REC");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde", "#TRN-REC-CDE", FieldType.STRING, 3);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type", "#TRN-FUND-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate", "#TRN-BLANK-RATE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type", "#TRN-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr", "#TRN-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde", "#TRN-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde", "#TRN-CMPNY-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Trn_Fund_Rec__R_Field_5 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_5", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde = pnd_Trn_Fund_Rec__R_Field_5.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde", "#TRN-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde = pnd_Trn_Fund_Rec__R_Field_5.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde", "#TRN-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt", "#TRN-RATE-CNT", FieldType.NUMERIC, 
            3);

        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data", "#TRN-RATE-DATA");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde", "#TRN-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic", "#TRN-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt", "#TRN-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt", "#TRN-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt", "#TRN-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt", "#TRN-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt", "#TRN-OLD-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt", "#TRN-OLD-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt", "#TRN-NEW-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt", "#TRN-NEW-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Trn_Fund_Rec__R_Field_6 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_6", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);
        pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees = pnd_Trn_Fund_Rec__R_Field_6.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees", "#TRL-REA-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code", "#TRN-OPTION-CODE", FieldType.NUMERIC, 
            2);

        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info", "#TRN-TRL-INFO");
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees", "#TRN-OLD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees", "#TRN-OLD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt", "#TRN-OLD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt", "#TRN-OLD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt", "#TRN-OLD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt", "#TRN-OLD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees", "#TRN-NEW-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees", "#TRN-NEW-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt", "#TRN-NEW-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt", "#TRN-NEW-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt", "#TRN-NEW-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt", "#TRN-NEW-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trn_Fund_Rec__R_Field_7 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_7", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info);
        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf = pnd_Trn_Fund_Rec__R_Field_7.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf", "#TRN-TRL-INFO-REDF", 
            FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Trailer_Fund_Rec = localVariables.newGroupInRecord("pnd_Trailer_Fund_Rec", "#TRAILER-FUND-REC");
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees", "#TRL-OLD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees", "#TRL-OLD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt", "#TRL-OLD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt", "#TRL-OLD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt", "#TRL-OLD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt", "#TRL-OLD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees", "#TRL-NEW-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees", "#TRL-NEW-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt", "#TRL-NEW-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt", "#TRL-NEW-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt", "#TRL-NEW-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt", "#TRL-NEW-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trailer_Fund_Rec__R_Field_8 = localVariables.newGroupInRecord("pnd_Trailer_Fund_Rec__R_Field_8", "REDEFINE", pnd_Trailer_Fund_Rec);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf = pnd_Trailer_Fund_Rec__R_Field_8.newFieldArrayInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf", 
            "#TRL-FUND-REC-REDF", FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Grd_Cntrcts_Rec = localVariables.newGroupInRecord("pnd_Grd_Cntrcts_Rec", "#GRD-CNTRCTS-REC");
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Ppcn_Nbr = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Ppcn_Nbr", "#GRD-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Payee_Cde = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Payee_Cde", "#GRD-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Fund_Cde = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Fund_Cde", "#GRD-CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Grd_Cntrcts_Rec__R_Field_9 = pnd_Grd_Cntrcts_Rec.newGroupInGroup("pnd_Grd_Cntrcts_Rec__R_Field_9", "REDEFINE", pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Fund_Cde);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Cde = pnd_Grd_Cntrcts_Rec__R_Field_9.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Cde", "#GRD-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Fund_Cde = pnd_Grd_Cntrcts_Rec__R_Field_9.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Fund_Cde", "#GRD-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Prev_Amt = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Prev_Amt", "#GRD-GROSS-PREV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Curr_Amt = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Curr_Amt", "#GRD-GROSS-CURR-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt = pnd_Grd_Cntrcts_Rec.newFieldInGroup("pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt", "#GRD-GROSS-NXT-YR-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Tot_Accumulators = localVariables.newGroupInRecord("pnd_Tot_Accumulators", "#TOT-ACCUMULATORS");
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees", "#OTOT-GRD-STD-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees", 
            "#OTOT-GRD-STD-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt", 
            "#OTOT-GRD-STD-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt", 
            "#OTOT-GRD-STD-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt", 
            "#OTOT-GRD-STD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt", 
            "#OTOT-GRD-STD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees", 
            "#NTOT-GRD-STD-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees", 
            "#NTOT-GRD-STD-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt", 
            "#NTOT-GRD-STD-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt", 
            "#NTOT-GRD-STD-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt", 
            "#NTOT-GRD-STD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt", 
            "#NTOT-GRD-STD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Payees", "#OTOT-STD-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees", "#OTOT-STD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt", "#OTOT-STD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt", "#OTOT-STD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt", "#OTOT-STD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt", "#OTOT-STD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees", "#NTOT-STD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees", "#NTOT-STD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt", "#NTOT-STD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt", "#NTOT-STD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt", "#NTOT-STD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt", "#NTOT-STD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees", "#OTOT-GRD-OTH-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees", 
            "#OTOT-GRD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt", 
            "#OTOT-GRD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt", 
            "#OTOT-GRD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt", 
            "#OTOT-GRD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt", 
            "#OTOT-GRD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees", 
            "#NTOT-GRD-OTH-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees", 
            "#NTOT-GRD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt", 
            "#NTOT-GRD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt", 
            "#NTOT-GRD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt", 
            "#NTOT-GRD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt", 
            "#NTOT-GRD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees", "#OTOT-STD-OTH-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees", 
            "#OTOT-STD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt", 
            "#OTOT-STD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt", 
            "#OTOT-STD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt", 
            "#OTOT-STD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt", 
            "#OTOT-STD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees", 
            "#NTOT-STD-OTH-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees", 
            "#NTOT-STD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt", 
            "#NTOT-STD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt", 
            "#NTOT-STD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt", 
            "#NTOT-STD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt", 
            "#NTOT-STD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees", "#OTOT-STD-TPA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees", 
            "#OTOT-STD-TPA-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt", 
            "#OTOT-STD-TPA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt", 
            "#OTOT-STD-TPA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt", 
            "#OTOT-STD-TPA-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt", 
            "#OTOT-STD-TPA-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees", 
            "#NTOT-STD-TPA-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees", 
            "#NTOT-STD-TPA-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt", 
            "#NTOT-STD-TPA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt", 
            "#NTOT-STD-TPA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt", 
            "#NTOT-STD-TPA-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt", 
            "#NTOT-STD-TPA-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees", "#OTOT-STD-IPRO-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees", 
            "#OTOT-STD-IPRO-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt", 
            "#OTOT-STD-IPRO-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt", 
            "#OTOT-STD-IPRO-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt", 
            "#OTOT-STD-IPRO-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt", 
            "#OTOT-STD-IPRO-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees", 
            "#NTOT-STD-IPRO-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees", 
            "#NTOT-STD-IPRO-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt", 
            "#NTOT-STD-IPRO-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt", 
            "#NTOT-STD-IPRO-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt", 
            "#NTOT-STD-IPRO-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt", 
            "#NTOT-STD-IPRO-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees", "#OTOT-STD-PI-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees", "#OTOT-STD-PI-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt", "#OTOT-STD-PI-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt", "#OTOT-STD-PI-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt", 
            "#OTOT-STD-PI-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt", 
            "#OTOT-STD-PI-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees", "#NTOT-STD-PI-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees", "#NTOT-STD-PI-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt", "#NTOT-STD-PI-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt", "#NTOT-STD-PI-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt", 
            "#NTOT-STD-PI-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt", 
            "#NTOT-STD-PI-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Dte_Ccyymmdd = localVariables.newFieldInRecord("pnd_Wrk_Dte_Ccyymmdd", "#WRK-DTE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Wrk_Dte_Ccyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Wrk_Dte_Ccyymmdd__R_Field_10", "REDEFINE", pnd_Wrk_Dte_Ccyymmdd);
        pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd = pnd_Wrk_Dte_Ccyymmdd__R_Field_10.newFieldInGroup("pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd", 
            "#WRK-DTE-N-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 5);
        pnd_Tot_Rec_Cnt = localVariables.newFieldInRecord("pnd_Tot_Rec_Cnt", "#TOT-REC-CNT", FieldType.NUMERIC, 7);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_Wrk_Tot_Fnd_Rec_Changed = localVariables.newFieldInRecord("pnd_Wrk_Tot_Fnd_Rec_Changed", "#WRK-TOT-FND-REC-CHANGED", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Wrk_Chg_Tiaa_Payees = localVariables.newFieldInRecord("pnd_Wrk_Chg_Tiaa_Payees", "#WRK-CHG-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Wrk_Chg_Fund_Payees = localVariables.newFieldInRecord("pnd_Wrk_Chg_Fund_Payees", "#WRK-CHG-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Wrk_Chg_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Per_Pay_Amt", "#WRK-CHG-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Chg_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Per_Div_Amt", "#WRK-CHG-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Chg_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Final_Pay_Amt", "#WRK-CHG-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_Chg_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Final_Div_Amt", "#WRK-CHG-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_Prev_Pmt_Gross = localVariables.newFieldInRecord("pnd_Wrk_Prev_Pmt_Gross", "#WRK-PREV-PMT-GROSS", FieldType.NUMERIC, 9, 2);
        pnd_Wrk_Curr_Pmt_Gross = localVariables.newFieldInRecord("pnd_Wrk_Curr_Pmt_Gross", "#WRK-CURR-PMT-GROSS", FieldType.NUMERIC, 9, 2);
        pnd_Wrk_Old_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Old_Final_Pay_Amt", "#WRK-OLD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_Old_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Old_Final_Div_Amt", "#WRK-OLD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_New_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_New_Final_Pay_Amt", "#WRK-NEW-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_New_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_New_Final_Div_Amt", "#WRK-NEW-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Last_Updte_Dte = localVariables.newFieldInRecord("pnd_Last_Updte_Dte", "#LAST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Next_Updte_Dte = localVariables.newFieldInRecord("pnd_Next_Updte_Dte", "#NEXT-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);

        pnd_Date8__R_Field_11 = localVariables.newGroupInRecord("pnd_Date8__R_Field_11", "REDEFINE", pnd_Date8);
        pnd_Date8_Pnd_Next_Updt_Ccyymm = pnd_Date8__R_Field_11.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Ccyymm", "#NEXT-UPDT-CCYYMM", FieldType.NUMERIC, 
            6);
        pnd_Date8_Pnd_Next_Updt_Dd = pnd_Date8__R_Field_11.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Dd", "#NEXT-UPDT-DD", FieldType.NUMERIC, 2);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_12 = localVariables.newGroupInRecord("pnd_Year__R_Field_12", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_12.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_Have_Cntrct_Info_Sw = localVariables.newFieldInRecord("pnd_Have_Cntrct_Info_Sw", "#HAVE-CNTRCT-INFO-SW", FieldType.STRING, 1);
        pnd_Cntrct_Fnd_Sw = localVariables.newFieldInRecord("pnd_Cntrct_Fnd_Sw", "#CNTRCT-FND-SW", FieldType.STRING, 1);
        pnd_Wrk_Rea_Payees = localVariables.newFieldInRecord("pnd_Wrk_Rea_Payees", "#WRK-REA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Inactive_Payees = localVariables.newFieldInRecord("pnd_Inactive_Payees", "#INACTIVE-PAYEES", FieldType.NUMERIC, 7);
        pnd_Idx_Rate_Cde = localVariables.newFieldInRecord("pnd_Idx_Rate_Cde", "#IDX-RATE-CDE", FieldType.NUMERIC, 4);
        pnd_Tot_Rate_Cde = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Cde", "#TOT-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Tot_Rate_Div = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Div", "#TOT-RATE-DIV", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            250));
        pnd_Tot_Rate_Final_Div = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Final_Div", "#TOT-RATE-FINAL-DIV", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            250));
        pnd_Gtot_Rate_Div = localVariables.newFieldInRecord("pnd_Gtot_Rate_Div", "#GTOT-RATE-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Gtot_Rate_Final_Div = localVariables.newFieldInRecord("pnd_Gtot_Rate_Final_Div", "#GTOT-RATE-FINAL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Translate_Tab = localVariables.newFieldArrayInRecord("pnd_Translate_Tab", "#TRANSLATE-TAB", FieldType.STRING, 2, new DbsArrayController(1, 
            9));
        pnd_Work_Rate_Cde = localVariables.newFieldInRecord("pnd_Work_Rate_Cde", "#WORK-RATE-CDE", FieldType.STRING, 2);

        pnd_Work_Rate_Cde__R_Field_13 = localVariables.newGroupInRecord("pnd_Work_Rate_Cde__R_Field_13", "REDEFINE", pnd_Work_Rate_Cde);
        pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N = pnd_Work_Rate_Cde__R_Field_13.newFieldInGroup("pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N", "#WORK-RATE-CDE-N", 
            FieldType.NUMERIC, 2);

        pnd_Hd_Txt1 = localVariables.newGroupInRecord("pnd_Hd_Txt1", "#HD-TXT1");
        pnd_Hd_Txt1_Hd_Txt1 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt1", "HD-TXT1", FieldType.STRING, 31);
        pnd_Hd_Txt1_Hd_Txt2 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt2", "HD-TXT2", FieldType.STRING, 57);
        pnd_Hd_Txt1_Hd_Txt3 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt3", "HD-TXT3", FieldType.STRING, 43);

        pnd_Hd_Txt1__R_Field_14 = localVariables.newGroupInRecord("pnd_Hd_Txt1__R_Field_14", "REDEFINE", pnd_Hd_Txt1);
        pnd_Hd_Txt1_Pnd_Hd_Txt1_R = pnd_Hd_Txt1__R_Field_14.newFieldInGroup("pnd_Hd_Txt1_Pnd_Hd_Txt1_R", "#HD-TXT1-R", FieldType.STRING, 131);

        pnd_Hd2_Txt1 = localVariables.newGroupInRecord("pnd_Hd2_Txt1", "#HD2-TXT1");
        pnd_Hd2_Txt1_Hd2_Txt1 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt1", "HD2-TXT1", FieldType.STRING, 31);
        pnd_Hd2_Txt1_Hd2_Txt2 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt2", "HD2-TXT2", FieldType.STRING, 57);
        pnd_Hd2_Txt1_Hd2_Txt3 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt3", "HD2-TXT3", FieldType.STRING, 43);

        pnd_Hd2_Txt1__R_Field_15 = localVariables.newGroupInRecord("pnd_Hd2_Txt1__R_Field_15", "REDEFINE", pnd_Hd2_Txt1);
        pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R = pnd_Hd2_Txt1__R_Field_15.newFieldInGroup("pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R", "#HD2-TXT1-R", FieldType.STRING, 131);
        pnd_Tpa_Ipro_Codes = localVariables.newFieldArrayInRecord("pnd_Tpa_Ipro_Codes", "#TPA-IPRO-CODES", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            10));
        pnd_Tpa_Option_Codes = localVariables.newFieldArrayInRecord("pnd_Tpa_Option_Codes", "#TPA-OPTION-CODES", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            10));
        pnd_Calc_Mthd = localVariables.newFieldInRecord("pnd_Calc_Mthd", "#CALC-MTHD", FieldType.STRING, 9);

        pnd_Calc_Mthd__R_Field_16 = localVariables.newGroupInRecord("pnd_Calc_Mthd__R_Field_16", "REDEFINE", pnd_Calc_Mthd);
        pnd_Calc_Mthd_Pnd_Inv_Dte = pnd_Calc_Mthd__R_Field_16.newFieldInGroup("pnd_Calc_Mthd_Pnd_Inv_Dte", "#INV-DTE", FieldType.NUMERIC, 8);
        pnd_Calc_Mthd_Pnd_C_Mthd = pnd_Calc_Mthd__R_Field_16.newFieldInGroup("pnd_Calc_Mthd_Pnd_C_Mthd", "#C-MTHD", FieldType.STRING, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.STRING, 8);
        pnd_Nxt_Pmt_Dte = localVariables.newFieldInRecord("pnd_Nxt_Pmt_Dte", "#NXT-PMT-DTE", FieldType.STRING, 8);

        pnd_Nxt_Pmt_Dte__R_Field_17 = localVariables.newGroupInRecord("pnd_Nxt_Pmt_Dte__R_Field_17", "REDEFINE", pnd_Nxt_Pmt_Dte);
        pnd_Nxt_Pmt_Dte__Filler2 = pnd_Nxt_Pmt_Dte__R_Field_17.newFieldInGroup("pnd_Nxt_Pmt_Dte__Filler2", "_FILLER2", FieldType.STRING, 4);
        pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm = pnd_Nxt_Pmt_Dte__R_Field_17.newFieldInGroup("pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm", "#NXT-MM", FieldType.STRING, 2);

        pnd_Nxt_Pmt_Dte__R_Field_18 = localVariables.newGroupInRecord("pnd_Nxt_Pmt_Dte__R_Field_18", "REDEFINE", pnd_Nxt_Pmt_Dte);
        pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N = pnd_Nxt_Pmt_Dte__R_Field_18.newFieldInGroup("pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N", "#NXT-PMT-DTE-N", FieldType.NUMERIC, 
            8);
        pls_Sub_Sw = WsIndependent.getInstance().newFieldInRecord("pls_Sub_Sw", "+SUB-SW", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Cntrct_Type.setInitialValue("P&I ");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.setInitialValue("DET");
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.setInitialValue("N");
        pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate.setInitialValue("N");
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.setInitialValue(" ");
        pnd_Have_Cntrct_Info_Sw.setInitialValue("N");
        pnd_Translate_Tab.getValue(1).setInitialValue("A1");
        pnd_Translate_Tab.getValue(2).setInitialValue("B2");
        pnd_Translate_Tab.getValue(3).setInitialValue("C3");
        pnd_Translate_Tab.getValue(4).setInitialValue("D4");
        pnd_Translate_Tab.getValue(5).setInitialValue("E5");
        pnd_Translate_Tab.getValue(6).setInitialValue("F6");
        pnd_Translate_Tab.getValue(7).setInitialValue("G7");
        pnd_Translate_Tab.getValue(8).setInitialValue("H8");
        pnd_Translate_Tab.getValue(9).setInitialValue("I9");
        pnd_Hd_Txt1_Hd_Txt2.setInitialValue("         INPUT            NET CHANGE              OUTPUT ");
        pnd_Hd_Txt1_Hd_Txt3.setInitialValue("         IN CNTRL RECORD                  ");
        pnd_Hd2_Txt1_Hd2_Txt2.setInitialValue("    -----------------  -----------------     ------------");
        pnd_Hd2_Txt1_Hd2_Txt3.setInitialValue("---    ------------------                 ");
        pnd_Tpa_Ipro_Codes.getValue(1).setInitialValue(25);
        pnd_Tpa_Ipro_Codes.getValue(2).setInitialValue(27);
        pnd_Tpa_Ipro_Codes.getValue(3).setInitialValue(28);
        pnd_Tpa_Ipro_Codes.getValue(4).setInitialValue(30);
        pnd_Tpa_Option_Codes.getValue(1).setInitialValue(28);
        pnd_Tpa_Option_Codes.getValue(2).setInitialValue(30);
        pls_Sub_Sw.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap395() throws Exception
    {
        super("Iaap395");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *************
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Updte_Dte, pnd_Next_Updte_Dte);                                                               //Natural: CALLNAT 'IAAN0020' #LAST-UPDTE-DTE #NEXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        //*  TEST-ONLY REMOVE FOLLOWING AFTER TESTING 11/97
        //*  #LAST-UPDTE-DTE := '12/01/2017' /* FORCE JAN CHECK DATE
        //*  #NEXT-UPDTE-DTE := '01/01/2018' /* TEST ONLY REMOVE FOR PROD
        //*  END OF TEST-ONLY                       11/97
        //*  ADDED FOLLOWING 12/01
        pnd_Datd.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Next_Updte_Dte);                                                                                     //Natural: MOVE EDITED #NEXT-UPDTE-DTE TO #DATD ( EM = MM/DD/YYYY )
        //*  11/10 - IA UPDATE DATE TO BE PASSED TO NAZN6032
        pnd_Date8.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                                //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE8
        pnd_W_Date.setValue(pnd_Date8);                                                                                                                                   //Natural: ASSIGN #W-DATE := #DATE8
        //*  END OF ADD  12/01
        if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"MM'/'DD'/'YYYY")))                                                                                          //Natural: IF #LAST-UPDTE-DTE = MASK ( MM'/'DD'/'YYYY )
        {
            pnd_Datd.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Next_Updte_Dte);                                                                                 //Natural: MOVE EDITED #NEXT-UPDTE-DTE TO #DATD ( EM = MM/DD/YYYY )
            pnd_Date8.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE8
            pnd_Year.setValueEdited(pnd_Datd,new ReportEditMask("YYYY"));                                                                                                 //Natural: MOVE EDITED #DATD ( EM = YYYY ) TO #YEAR
            if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"'01'")))                                                                                                //Natural: IF #LAST-UPDTE-DTE = MASK ( '01' )
            {
                pnd_Year_Pnd_Year_N.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #YEAR-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR   ERROR   ERROR  ERROR",NEWLINE,"NO CONTROL RECORD ON FILE",NEWLINE,"PROCESSING WILL NOT CONTINUE");                             //Natural: WRITE 'ERROR   ERROR   ERROR  ERROR' / 'NO CONTROL RECORD ON FILE' / 'PROCESSING WILL NOT CONTINUE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/10 -- START
        DbsUtil.callnat(Iaan0395.class , getCurrentProcessState(), pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N, pnd_Calc_Mthd, pnd_Rc);                                             //Natural: CALLNAT 'IAAN0395' #NXT-PMT-DTE-N #CALC-MTHD #RC
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Rc.equals(getZero())))                                                                                                                          //Natural: IF #RC = 0
        {
            pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N.compute(new ComputeParameters(false, pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N), DbsField.subtract(100000000,pnd_Calc_Mthd_Pnd_Inv_Dte)); //Natural: COMPUTE #NXT-PMT-DTE-N = 100000000 - #INV-DTE
            getWorkFiles().write(6, false, pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N, pnd_Calc_Mthd_Pnd_C_Mthd);                                                                  //Natural: WRITE WORK FILE 6 #NXT-PMT-DTE-N #C-MTHD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "******************************",NEWLINE,"ERROR  ERROR ERROR ERROR ERROR",NEWLINE,"   NO CALC METHOD PROVIDED    ",NEWLINE,             //Natural: WRITE '******************************' / 'ERROR  ERROR ERROR ERROR ERROR' / '   NO CALC METHOD PROVIDED    ' / 'ERROR  ERROR ERROR ERROR ERROR' / '******************************'
                "ERROR  ERROR ERROR ERROR ERROR",NEWLINE,"******************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Nxt_Pmt_Dte.setValue(pnd_W_Date);                                                                                                                             //Natural: ASSIGN #NXT-PMT-DTE := #W-DATE
        //*  11/10 -- END
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY
        (
        "R1",
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("R1")))
        {
            //*  FOLLOWING FOR TEST-ONLY 11/04
            //*  IF CNTRCT-PART-PPCN-NBR LT 'GA311369'
            //*     ESCAPE TOP
            //*  END-IF
            //*  11/2017 - START
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.greater("Y9999999")))                                                                              //Natural: IF CNTRCT-PART-PPCN-NBR GT 'Y9999999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  IF CNTRCT-PART-PPCN-NBR = 'GA164982'
            //*   OR = 'GA205736'
            //*   OR = 'GA224224'
            //*   OR = 'GA251102'
            //*     ESCAPE TOP
            //*  END-IF
            //*  END OF TEST-ONLY 11/04
            pnd_Have_Cntrct_Info_Sw.setValue("N");                                                                                                                        //Natural: ASSIGN #HAVE-CNTRCT-INFO-SW := 'N'
            pnd_Cntrct_Fnd_Sw.setValue("Y");                                                                                                                              //Natural: ASSIGN #CNTRCT-FND-SW := 'Y'
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                           //Natural: IF CNTRCT-ACTVTY-CDE = 9
            {
                pnd_Inactive_Payees.nadd(1);                                                                                                                              //Natural: ADD 1 TO #INACTIVE-PAYEES
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).equals("T") || iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).equals("R")))        //Natural: IF CNTRCT-COMPANY-CD ( 1 ) = 'T' OR = 'R'
            {
                pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees.nadd(1);                                                                                                     //Natural: ADD 1 TO #TRL-OLD-TIAA-PAYEES
                pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees.nadd(1);                                                                                                     //Natural: ADD 1 TO #TRL-NEW-TIAA-PAYEES
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).equals("R")))                                                                             //Natural: IF CNTRCT-COMPANY-CD ( 1 ) = 'R'
            {
                pnd_Wrk_Rea_Payees.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WRK-REA-PAYEES
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                            //Natural: ASSIGN #FUND-PPCN := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
            pnd_Fund_Key_Pnd_Fund_Paye.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                           //Natural: ASSIGN #FUND-PAYE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE
            pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                            //Natural: ASSIGN #FUND-PPCN := #CNTRCT-PPCN-NBR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
            pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);
            //*  ADDED 12/01
            //*  ADDED 12/01
            pnd_Tpar_To_Tpa_Interface.reset();                                                                                                                            //Natural: RESET #TPAR-TO-TPA-INTERFACE #TPAR-TIAA-RATE-CDE ( * ) #TPAR-TIAA-PER-PAY-AMT ( * ) #TPAR-TIAA-PER-DIV-AMT ( * )
            pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Cde.getValue("*").reset();
            pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Pay_Amt.getValue("*").reset();
            pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Div_Amt.getValue("*").reset();
            pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cpr_Id_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr);                                                                   //Natural: ASSIGN #TPAR-CPR-ID-NBR := CPR-ID-NBR
                                                                                                                                                                          //Natural: PERFORM READ-PROCESS-FUND-RECORDS
            sub_Read_Process_Fund_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END OF JOB PROCESSING
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CNTRL-RCRD
        sub_Read_Iaa_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RATE-TOTALS
        sub_Print_Rate_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Trn_Fund_Rec.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #TRN-FUND-REC
        pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees.setValue(pnd_Wrk_Rea_Payees);                                                                                                 //Natural: ASSIGN #TRL-REA-PAYEES := #WRK-REA-PAYEES
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.setValue("TRL");                                                                                                                 //Natural: ASSIGN #TRN-REC-CDE := 'TRL'
        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf.getValue("*").setValue(pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf.getValue("*"));                                          //Natural: ASSIGN #TRN-TRL-INFO-REDF ( * ) := #TRL-FUND-REC-REDF ( * )
        getWorkFiles().write(1, false, pnd_Trn_Fund_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #TRN-FUND-REC
        //* ADDED 12/01 WRITE TOTAL FILE
        getWorkFiles().write(5, false, pnd_Totl_Tpa_Total_Rec);                                                                                                           //Natural: WRITE WORK FILE 5 #TOTL-TPA-TOTAL-REC
        //*  ***************************************************************
        //* * PROCESS TIAA FUND RECORDS
        //* * SELECT TIAA FUND RECORDS ONLY PASS TO ACTUARIAL FOR DIV CHANGE
        //* * WRITE TRANS REC OUT WITH RATE CHANGES TO APPLY TO FUND RECORDS
        //*  ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PROCESS-FUND-RECORDS
        //*   END OF CHANGES    6/03
        //*  END OF ADD 9/98
        //*  CALLNAT 'AIAN056'
        //*  CALLNAT 'AIAN070'
        //*  CALLNAT 'AIAN073'
        //*  CALLNAT 'AIAN080'
        //* ***************************************************
        //*   ACCUMULATE CURR PAYMENT AMOUNTS
        //*   INTO TRAN RECORD
        //* ***************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CURR-PAYMENT-TRAILER-AMTS
        //* ***************************************************
        //*   MOVE IAA-CNTRCT INFO TO ACTUARIAL RECORD
        //* ***************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IAA-CNTRCT-INFO
        //*  ***********************************************************
        //*  CREATE & WRITE TRAN FUND RECORD WITH ANY RATE CHANGES IN
        //*   AIAN035 OUTPUT LINKAGE FROM ACTUARAIL MODULE
        //*   DIV RATE CHANGES WILL BE APPLIED BY ANOTHER JOB & PROG
        //*  ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-WRITE-OUT-TRN-FUND-REC
        //*   MOVE NEW RATE AMTS TO TRAN RECORD
        //*   END OF TEST-ONLY 11/04
        //*  END OF CHANGE 6/03
        //*  **************************************************
        //*   ACCUMULATE NEW  PAYMENT AMOUNTS
        //*   INTO TRAN RECORD
        //*  **************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-NEW-PAYMENT-TRAILER-AMTS
        //*  **********************************************
        //*   GET CNTRACT INFO FOR ACTUARY CALL
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IAA-CNTRCT-INFO
        //*  ADDED FOLLOWING ROUTINE 12/01
        //* ***********************************************************
        //*  THIS FILE USED BY TPA REINVESTMENT TO SYNC PMTS WITH IAIQ
        //*       FOR P/I CONTRACTS
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-P-I-REINVST-FILE
        //*  END ADD ROUTINE 12/01
        //* ***********************************************************
        //*  THIS FILE USED BY NET CHANGE PROGRAM TO PRINT PERCENTS
        //*       ON GRADED PAYMENT STATEMENTS
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRADED-REC
        //* **********************************************************
        //*   PRINT SUMMARY REPORT END OF JOB PROCESSING
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //*  102T CNTRL-TIAA-PAYEES    (EM=ZZZ,ZZZ,ZZ9-) 11/02
        //*   PRINT GRADED OTHER TOTALS FOR OPTION 21 AFTER 199103,
        //*         (ORIGIN 3 AFTER 199312) & (PA CONTRACTS ORIGIN 17 & 18)
        //*   PRINT STANDARD OTHER TOTALS FOR OPTION 21 AFTER 199103,
        //*         (ORIGIN 3 AFTER 199312) & (PA CONTRACTS ORIGIN 17 & 18)
        //*   PRINT P/I TOTALS FOR OPTION 22
        //*   PRINT TPA TOTALS FOR OPTION 28,29,30 & 31
        //*   PRINT IPRO TOTALS FOR OPTION 25 & 27
        //* *******************************************************
        //*  PRINT RATE TOTALS AMTS BY RATE
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-RATE-TOTALS
        //*      15T #I  (EM=99)
        //*  CHANGED ABOVE TO FOLLOWING 6/03
        //*  ***********************************************************
        //*  GET CONTROL RECORD TOTALS
        //*  ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CNTRL-RCRD
        //* ************************************************************
        //*      BAD RETURN CODE FROM ACTUARY MODULE
        //*      PRINT ERROR MSG & TERMINATE JOB IF RETURN-CODE NE 10
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-AIAN035-ERROR-MSG
    }
    private void sub_Read_Process_Fund_Records() throws Exception                                                                                                         //Natural: READ-PROCESS-FUND-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
        (
        "READ01",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ01")))
        {
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.notEquals("T")))                                                                                              //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE NE 'T'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Fund_Key_Pnd_Fund_Ppcn.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr) || pnd_Fund_Key_Pnd_Fund_Paye.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF #FUND-PPCN NE TIAA-CNTRCT-PPCN-NBR OR #FUND-PAYE NE TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Have_Cntrct_Info_Sw.equals("N")))                                                                                                           //Natural: IF #HAVE-CNTRCT-INFO-SW = 'N'
            {
                                                                                                                                                                          //Natural: PERFORM GET-IAA-CNTRCT-INFO
                sub_Get_Iaa_Cntrct_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Have_Cntrct_Info_Sw.setValue("Y");                                                                                                                    //Natural: ASSIGN #HAVE-CNTRCT-INFO-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  RESET INITIAL #AIAN018-LINKAGE
            //*  CHANGED ABOVE TO FOLLOWING   11/97
            pdaAiaa0380.getPnd_Aian035_Linkage().resetInitial();                                                                                                          //Natural: RESET INITIAL #AIAN035-LINKAGE #TRN-FUND-REC #GRD-CNTRCTS-REC #WRK-OLD-FINAL-PAY-AMT #WRK-OLD-FINAL-DIV-AMT #WRK-NEW-FINAL-PAY-AMT #WRK-NEW-FINAL-DIV-AMT
            pnd_Trn_Fund_Rec.resetInitial();
            pnd_Grd_Cntrcts_Rec.resetInitial();
            pnd_Wrk_Old_Final_Pay_Amt.resetInitial();
            pnd_Wrk_Old_Final_Div_Amt.resetInitial();
            pnd_Wrk_New_Final_Pay_Amt.resetInitial();
            pnd_Wrk_New_Final_Div_Amt.resetInitial();
                                                                                                                                                                          //Natural: PERFORM MOVE-IAA-CNTRCT-INFO
            sub_Move_Iaa_Cntrct_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  STANDARD
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1S") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1 ")))                                  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = 'T1S' OR = 'T1 '
            {
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().setValue("S");                                                                                 //Natural: ASSIGN #IN-PAYMENT-METHOD := 'S'
                pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.setValue("S");                                                                                                         //Natural: ASSIGN #TRN-FUND-TYPE := 'S'
                //*  GRADED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().setValue("G");                                                                                 //Natural: ASSIGN #IN-PAYMENT-METHOD := 'G'
                pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.setValue("G");                                                                                                         //Natural: ASSIGN #TRN-FUND-TYPE := 'G'
            }                                                                                                                                                             //Natural: END-IF
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO C*TIAA-RATE-DATA-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp)); pnd_I.nadd(1))
            {
                if (condition(DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I),"'  '") || DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I), //Natural: IF TIAA-RATE-CDE ( #I ) = MASK ( '  ' ) OR = MASK ( ' ' )
                    "' '")))
                {
                    pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate.setValue("Y");                                                                                                    //Natural: ASSIGN #TRN-BLANK-RATE := 'Y'
                    //*  ADDED FOLLOWING 6/03 TO FIX CONTRACT Y9075527 FOR BLANK RATE
                    //*           WITH FINAL-DIV-AMT TO KEEP REPORT IN BALANCE
                    getReports().write(0, "**** CONTRACT WITH BLANK RATE INCLUDED IN RATE-CNT****");                                                                      //Natural: WRITE '**** CONTRACT WITH BLANK RATE INCLUDED IN RATE-CNT****'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "**** CONTRACT-NBR------->",pnd_Fund_Key_Pnd_Fund_Ppcn,pnd_Fund_Key_Pnd_Fund_Paye);                                             //Natural: WRITE '**** CONTRACT-NBR------->' #FUND-PPCN #FUND-PAYE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp,NEWLINE,iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I),         //Natural: WRITE '=' C*TIAA-RATE-DATA-GRP / TIAA-RATE-FINAL-PAY-AMT ( #I ) TIAA-RATE-FINAL-DIV-AMT ( #I ) / TIAA-RATE-FINAL-PAY-AMT ( #I ) TIAA-RATE-FINAL-DIV-AMT ( #I )
                        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I),NEWLINE,iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I),iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Wrk_Old_Final_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                                           //Natural: ADD TIAA-RATE-FINAL-PAY-AMT ( #I ) TO #WRK-OLD-FINAL-PAY-AMT
                    pnd_Wrk_Old_Final_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I));                                                           //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( #I ) TO #WRK-OLD-FINAL-DIV-AMT
                    //*  END OF ADD  6/03
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I2
                pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd.reset();                                                                                                      //Natural: RESET #WRK-DTE-N-CCYYMMDD
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte.getValue(pnd_I).notEquals(getZero())))                                                                     //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) NE 0
                {
                    pnd_Wrk_Dte_Ccyymmdd.setValueEdited(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte.getValue(pnd_I),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) ( EM = YYYYMMDD ) TO #WRK-DTE-CCYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Transfer_Eff_Date().getValue(pnd_I2).setValue(pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd);                //Natural: ASSIGN #IN-RATE-TRANSFER-EFF-DATE ( #I2 ) := #WRK-DTE-N-CCYYMMDD
                //*    #IN-RATE-CODE (#I2)          := VAL(TIAA-RATE-CDE(#I))
                //*   CHANGED ABOVE TO FOLLOWING 6/03
                if (condition(DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I),".' '")))                                                              //Natural: IF TIAA-RATE-CDE ( #I ) = MASK ( .' ' )
                {
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I), MoveOption.RightJustified);               //Natural: MOVE RIGHT TIAA-RATE-CDE ( #I ) TO TIAA-RATE-CDE ( #I )
                    DbsUtil.examine(new ExamineSource(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I)), new ExamineSearch(" "), new ExamineReplace("0"));                //Natural: EXAMINE TIAA-RATE-CDE ( #I ) FOR ' ' REPLACE '0'
                    //*  3/12
                }                                                                                                                                                         //Natural: END-IF
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I));                        //Natural: ASSIGN #IN-RATE-CODE ( #I2 ) := TIAA-RATE-CDE ( #I )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Gic_Code().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic.getValue(pnd_I));                         //Natural: ASSIGN #IN-GIC-CODE ( #I2 ) := TIAA-RATE-GIC ( #I )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I));            //Natural: ASSIGN #IN-GUARANTEED-PAYMENT ( #I2 ) := TIAA-PER-PAY-AMT ( #I )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I));              //Natural: ASSIGN #IN-DIVIDEND-PAYMENT ( #I2 ) := TIAA-PER-DIV-AMT ( #I )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I)); //Natural: ASSIGN #IN-FINAL-GUARANTEED-PAYMENT ( #I2 ) := TIAA-RATE-FINAL-PAY-AMT ( #I )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_I2).setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I)); //Natural: ASSIGN #IN-FINAL-DIVIDEND-PAYMENT ( #I2 ) := TIAA-RATE-FINAL-DIV-AMT ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I));                                                       //Natural: ADD TIAA-PER-PAY-AMT ( #I ) TO #TRN-OLD-TOT-PER-AMT
                pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I));                                                       //Natural: ADD TIAA-PER-DIV-AMT ( #I ) TO #TRN-OLD-TOT-DIV-AMT
                pnd_Wrk_Old_Final_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                                               //Natural: ADD TIAA-RATE-FINAL-PAY-AMT ( #I ) TO #WRK-OLD-FINAL-PAY-AMT
                pnd_Wrk_Old_Final_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I));                                                               //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( #I ) TO #WRK-OLD-FINAL-DIV-AMT
                pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt.setValue(pnd_I2);                                                                                                       //Natural: ASSIGN #TRN-RATE-CNT := #I2
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ACCUM-CURR-PAYMENT-TRAILER-AMTS
            sub_Accum_Curr_Payment_Trailer_Amts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED FOLLOWING IF STMNT TO HANDLE OLD AMTS FOR CONTRACTS    9/98
            //*                     NOT REVALUED IN JANUARY                   9/98
            //*    01/01
            if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(21) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199103))  //Natural: IF ( #IN-PAYMENT-OPTION = 21 AND #IN-ISSUE-YYYY-MM GT 199103 ) OR ( #IN-PAYMENT-OPTION = #TPA-IPRO-CODES ( * ) )
                || (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(pnd_Tpa_Ipro_Codes.getValue("*")))))
            {
                pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt);                                                                   //Natural: MOVE TIAA-OLD-PER-AMT TO #TRN-OLD-TOT-PER-AMT
                pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt);                                                                   //Natural: MOVE TIAA-OLD-DIV-AMT TO #TRN-OLD-TOT-DIV-AMT
                //*  ADDED 11/04
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Write_Output_Switch().setValue("Y");                                                                                //Natural: ASSIGN #IN-WRITE-OUTPUT-SWITCH := 'Y'
            //*  IF #IN-CONTRACT = 'S0107728'
            //*    WRITE WORK 7 #AIAN035-LINKAGE
            //*  END-IF
            //*  ADDED 11/02 NEW ACTUARY MODULE
            DbsUtil.callnat(Aian083.class , getCurrentProcessState(), pdaAiaa0380.getPnd_Aian035_Linkage());                                                              //Natural: CALLNAT 'AIAN083' #AIAN035-LINKAGE
            if (condition(Global.isEscape())) return;
            //*  ADDED FOLLOWING           11/97
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            pnd_Tot_Rec_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-REC-CNT
            if (condition(pnd_Rec_Cnt.greater(5000)))                                                                                                                     //Natural: IF #REC-CNT GT 5000
            {
                getReports().write(1, ReportOption.NOTITLE,pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Payee(), new ReportEditMask ("XXXXXXXX-XX"),Global.getTIMX()," RECORD COUNT-->",pnd_Tot_Rec_Cnt,  //Natural: WRITE ( 1 ) #IN-CONTRACT-PAYEE ( EM = XXXXXXXX-XX ) *TIMX ' RECORD COUNT-->' #TOT-REC-CNT ( EM = Z,ZZZ,ZZ9 )
                    new ReportEditMask ("Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rec_Cnt.reset();                                                                                                                                      //Natural: RESET #REC-CNT
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 11/97
            //*  3/12
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(getZero()) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(99))) //Natural: IF #OUT-AIAN035-RETURN-CODE-NBR = 0 OR = 99
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-AIAN035-ERROR-MSG
                sub_Print_Aian035_Error_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BUILD-WRITE-OUT-TRN-FUND-REC
            sub_Build_Write_Out_Trn_Fund_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED FOLLOWING 12/01
            if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22))))                                                                       //Natural: IF ( #IN-PAYMENT-OPTION = 22 )
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-P-I-REINVST-FILE
                sub_Write_P_I_Reinvst_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 12/01
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'G'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-GRADED-REC
                sub_Write_Graded_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Curr_Payment_Trailer_Amts() throws Exception                                                                                                   //Natural: ACCUM-CURR-PAYMENT-TRAILER-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees.nadd(1);                                                                                                             //Natural: ADD 1 TO #TRL-OLD-FUND-PAYEES
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                                      //Natural: ASSIGN #TRL-OLD-PER-PAY-AMT := #TRL-OLD-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                                      //Natural: ASSIGN #TRL-OLD-PER-DIV-AMT := #TRL-OLD-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                                   //Natural: ASSIGN #TRL-OLD-FINAL-PAY-AMT := #TRL-OLD-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                                   //Natural: ASSIGN #TRL-OLD-FINAL-DIV-AMT := #TRL-OLD-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
        //*  OTHER CONTRACTS
        //*  ADDED 12/01
        if (condition(! (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(25)   //Natural: IF NOT ( #IN-PAYMENT-OPTION = 22 OR = 25 OR = 27 OR = 28 OR = 30 )
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(27) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(28) 
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(30))))
        {
            //* 11/99
            //*  ADDED 11/09
            if (condition(((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(21) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199103))  //Natural: IF ( #IN-PAYMENT-OPTION = 21 AND #IN-ISSUE-YYYY-MM GT 199103 ) OR ( #IN-CONTRACT-ORIGIN = 3 AND #IN-ISSUE-YYYY-MM GT 199312 ) OR ( #IN-CONTRACT-ORIGIN = 17 OR = 18 OR = 37 OR = 38 OR = 40 ) OR ( #IN-CONTRACT-ORIGIN = 35 OR = 36 OR = 43 OR = 44 OR = 45 OR = 46 )
                || (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(3) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199312))) 
                || ((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(17) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(18)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(37)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(38)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(40))) || (((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(35) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(36)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(43)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(44)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(45)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(46)))))
            {
                //*  GRADED
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G")))                                                                    //Natural: IF #IN-PAYMENT-METHOD = 'G'
                {
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #OTOT-GRD-OTH-FUND-PAYEES := #OTOT-GRD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                     //Natural: ASSIGN #OTOT-GRD-OTH-PER-PAY-AMT := #OTOT-GRD-OTH-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                     //Natural: ASSIGN #OTOT-GRD-OTH-PER-DIV-AMT := #OTOT-GRD-OTH-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                  //Natural: ASSIGN #OTOT-GRD-OTH-FINAL-PAY-AMT := #OTOT-GRD-OTH-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                  //Natural: ASSIGN #OTOT-GRD-OTH-FINAL-DIV-AMT := #OTOT-GRD-OTH-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #OTOT-STD-OTH-FUND-PAYEES := #OTOT-STD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                     //Natural: ASSIGN #OTOT-STD-OTH-PER-PAY-AMT := #OTOT-STD-OTH-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                     //Natural: ASSIGN #OTOT-STD-OTH-PER-DIV-AMT := #OTOT-STD-OTH-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                  //Natural: ASSIGN #OTOT-STD-OTH-FINAL-PAY-AMT := #OTOT-STD-OTH-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                  //Natural: ASSIGN #OTOT-STD-OTH-FINAL-DIV-AMT := #OTOT-STD-OTH-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR TPA CONTRACTS                              /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(pnd_Tpa_Option_Codes.getValue("*"))))                                             //Natural: IF #IN-PAYMENT-OPTION = #TPA-OPTION-CODES ( * )
        {
            //*  STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees.nadd(1);                                                                                                //Natural: ASSIGN #OTOT-STD-TPA-FUND-PAYEES := #OTOT-STD-TPA-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                         //Natural: ASSIGN #OTOT-STD-TPA-PER-PAY-AMT := #OTOT-STD-TPA-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                         //Natural: ASSIGN #OTOT-STD-TPA-PER-DIV-AMT := #OTOT-STD-TPA-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                      //Natural: ASSIGN #OTOT-STD-TPA-FINAL-PAY-AMT := #OTOT-STD-TPA-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                      //Natural: ASSIGN #OTOT-STD-TPA-FINAL-DIV-AMT := #OTOT-STD-TPA-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR IPRO CONTRACTS                             /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(25) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(27)))    //Natural: IF #IN-PAYMENT-OPTION = 25 OR = 27
        {
            //*  STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees.nadd(1);                                                                                               //Natural: ASSIGN #OTOT-STD-IPRO-FUND-PAYEES := #OTOT-STD-IPRO-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                        //Natural: ASSIGN #OTOT-STD-IPRO-PER-PAY-AMT := #OTOT-STD-IPRO-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                        //Natural: ASSIGN #OTOT-STD-IPRO-PER-DIV-AMT := #OTOT-STD-IPRO-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                     //Natural: ASSIGN #OTOT-STD-IPRO-FINAL-PAY-AMT := #OTOT-STD-IPRO-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                     //Natural: ASSIGN #OTOT-STD-IPRO-FINAL-DIV-AMT := #OTOT-STD-IPRO-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR P/I CONTRACTS                              /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22)))                                                                             //Natural: IF #IN-PAYMENT-OPTION = 22
        {
            //*  STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees.nadd(1);                                                                                                 //Natural: ASSIGN #OTOT-STD-PI-FUND-PAYEES := #OTOT-STD-PI-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                          //Natural: ASSIGN #OTOT-STD-PI-PER-PAY-AMT := #OTOT-STD-PI-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                          //Natural: ASSIGN #OTOT-STD-PI-PER-DIV-AMT := #OTOT-STD-PI-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                       //Natural: ASSIGN #OTOT-STD-PI-FINAL-PAY-AMT := #OTOT-STD-PI-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                       //Natural: ASSIGN #OTOT-STD-PI-FINAL-DIV-AMT := #OTOT-STD-PI-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRADED
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G")))                                                                            //Natural: IF #IN-PAYMENT-METHOD = 'G'
        {
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #OTOT-GRD-STD-FUND-PAYEES := #OTOT-GRD-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                             //Natural: ASSIGN #OTOT-GRD-STD-PER-PAY-AMT := #OTOT-GRD-STD-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                             //Natural: ASSIGN #OTOT-GRD-STD-PER-DIV-AMT := #OTOT-GRD-STD-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                          //Natural: ASSIGN #OTOT-GRD-STD-FINAL-PAY-AMT := #OTOT-GRD-STD-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                          //Natural: ASSIGN #OTOT-GRD-STD-FINAL-DIV-AMT := #OTOT-GRD-STD-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees.nadd(1);                                                                                                        //Natural: ASSIGN #OTOT-STD-FUND-PAYEES := #OTOT-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                                 //Natural: ASSIGN #OTOT-STD-PER-PAY-AMT := #OTOT-STD-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                                 //Natural: ASSIGN #OTOT-STD-PER-DIV-AMT := #OTOT-STD-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                              //Natural: ASSIGN #OTOT-STD-FINAL-PAY-AMT := #OTOT-STD-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                              //Natural: ASSIGN #OTOT-STD-FINAL-DIV-AMT := #OTOT-STD-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  11/10
    //*  11/10
    //*  11/10
    private void sub_Move_Iaa_Cntrct_Info() throws Exception                                                                                                              //Natural: MOVE-IAA-CNTRCT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Calc_Method().setValue(pnd_Calc_Mthd_Pnd_C_Mthd);                                                                       //Natural: ASSIGN #IN-CALC-METHOD := #C-MTHD
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pp_Pay_Dte().setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte);                                    //Natural: ASSIGN #IN-ACT-STD-FNL-PP-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pay_Dte().setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte);                                           //Natural: ASSIGN #IN-ACT-STD-FNL-PAY-DTE := CNTRCT-FINAL-PAY-DTE
        //*  11/10
        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(100)))                                                                                               //Natural: IF CNTRCT-MODE-IND = 100
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date()),  //Natural: ASSIGN #IN-NEXT-PAYMENT-DATE := VAL ( #W-DATE )
                pnd_W_Date.val());
            //*  11/10
            //*  11/10
            //*  11/10
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm.setValue(iaa_Cntrct_Prtcpnt_Role_Pnd_Mode_Mm);                                                                                     //Natural: ASSIGN #NXT-MM := #MODE-MM
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date().setValue(pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N);                                                    //Natural: ASSIGN #IN-NEXT-PAYMENT-DATE := #NXT-PMT-DTE-N
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date().setValue(pnd_Date8_Pnd_Next_Updt_Ccyymm);                                                             //Natural: ASSIGN #IN-PROCESSING-DATE := #NEXT-UPDT-CCYYMM
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract().setValue(pnd_Fund_Key_Pnd_Fund_Ppcn);                                                                        //Natural: ASSIGN #IN-CONTRACT := #FUND-PPCN
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payee().setValue(pnd_Fund_Key_Pnd_Fund_Paye);                                                                           //Natural: ASSIGN #IN-PAYEE := #FUND-PAYE
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Mode().setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                       //Natural: ASSIGN #IN-PAYMENT-MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        //*  ADDED 9/98
        if (condition(pnd_Cntrct_Fnd_Sw.equals("Y")))                                                                                                                     //Natural: IF #CNTRCT-FND-SW = 'Y'
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().setValue(iaa_Cntrct_Cntrct_Orgn_Cde);                                                             //Natural: ASSIGN #IN-CONTRACT-ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                              //Natural: ASSIGN #IN-PAYMENT-OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                              //Natural: ASSIGN #IN-ISSUE-YYYY-MM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Day().setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                               //Natural: ASSIGN #IN-ISSUE-DAY := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Sex().setValue(iaa_Cntrct_Cntrct_First_Annt_Sex_Cde);                                                     //Natural: ASSIGN #IN-FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Birth_Date().setValue(iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);                                              //Natural: ASSIGN #IN-FIRST-ANN-BIRTH-DATE := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Death_Date().setValue(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);                                              //Natural: ASSIGN #IN-FIRST-ANN-DEATH-DATE := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Sex().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde);                                                     //Natural: ASSIGN #IN-SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Birth_Date().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);                                              //Natural: ASSIGN #IN-SECOND-ANN-BIRTH-DATE := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Death_Date().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                              //Natural: ASSIGN #IN-SECOND-ANN-DEATH-DATE := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
            //*  ADDED THIS 9/98
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Day().equals(getZero())))                                                                       //Natural: IF #IN-ISSUE-DAY = 0
            {
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Day().setValue(1);                                                                                        //Natural: ASSIGN #IN-ISSUE-DAY := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Build_Write_Out_Trn_Fund_Rec() throws Exception                                                                                                      //Natural: BUILD-WRITE-OUT-TRN-FUND-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.setValue("DET");                                                                                                                 //Natural: ASSIGN #TRN-REC-CDE := 'DET'
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr.setValue(pnd_Fund_Key_Pnd_Fund_Ppcn);                                                                                    //Natural: ASSIGN #TRN-CNTRCT-PPCN-NBR := #FUND-PPCN
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde.setValue(pnd_Fund_Key_Pnd_Fund_Paye);                                                                                   //Natural: ASSIGN #TRN-CNTRCT-PAYEE-CDE := #FUND-PAYE
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);                                                                         //Natural: ASSIGN #TRN-CMPNY-FUND-CDE := IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
        pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option());                                                        //Natural: ASSIGN #TRN-OPTION-CODE := #IN-PAYMENT-OPTION
        //*  01/2001
        if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(21) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199103))  //Natural: IF ( #IN-PAYMENT-OPTION = 21 AND #IN-ISSUE-YYYY-MM GT 199103 ) OR ( #IN-PAYMENT-OPTION = #TPA-IPRO-CODES ( * ) )
            || (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(pnd_Tpa_Ipro_Codes.getValue("*")))))
        {
            pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.setValue("A");                                                                                                           //Natural: ASSIGN #TRN-CNTRCT-TYPE := 'A'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(3) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199312)))) //Natural: IF ( #IN-CONTRACT-ORIGIN = 3 AND #IN-ISSUE-YYYY-MM GT 199312 )
            {
                pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.setValue("I");                                                                                                       //Natural: ASSIGN #TRN-CNTRCT-TYPE := 'I'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* 11/99
                //*  ADDED 11/09
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(17) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(18)  //Natural: IF ( #IN-CONTRACT-ORIGIN = 17 OR = 18 OR = 37 OR = 38 OR = 40 ) OR ( #IN-CONTRACT-ORIGIN = 35 OR = 36 OR = 43 OR = 44 OR = 45 OR = 46 )
                    || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(37) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(38) 
                    || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(40) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(35) 
                    || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(36) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(43) 
                    || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(44) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(45) 
                    || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(46)))
                {
                    pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.setValue("P");                                                                                                   //Natural: ASSIGN #TRN-CNTRCT-TYPE := 'P'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #TRN-RATE-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt)); pnd_I.nadd(1))
        {
            //*  3/12
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S") || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G"))) //Natural: IF #IN-PAYMENT-METHOD = 'S' OR = 'G'
            {
                pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_I));                        //Natural: ASSIGN #TRN-RATE-CDE ( #I ) := #IN-RATE-CODE ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Gic_Code().getValue(pnd_I));                         //Natural: ASSIGN #TRN-RATE-GIC ( #I ) := #IN-GIC-CODE ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_I));  //Natural: ASSIGN #TRN-PER-PAY-AMT ( #I ) := #OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_I));   //Natural: ASSIGN #TRN-PER-DIV-AMT ( #I ) := #OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_I)); //Natural: ASSIGN #TRN-FINAL-PAY-AMT ( #I ) := #OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #I )
                pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt.getValue(pnd_I).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_I)); //Natural: ASSIGN #TRN-FINAL-DIV-AMT ( #I ) := #OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #I )
                //*    #IDX-RATE-CDE :=  #TRN-RATE-CDE (#I)
                //*  CHANGED ABOVE TO FOLLOWING 6/03
                //*    #WORK-RATE-CDE := #TRN-RATE-CDE (#I)
                if (condition(DbsUtil.is(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).getText(),"N2")))                                                              //Natural: IF #TRN-RATE-CDE ( #I ) IS ( N2 )
                {
                    pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N.compute(new ComputeParameters(false, pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N), pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).val()); //Natural: ASSIGN #WORK-RATE-CDE-N := VAL ( #TRN-RATE-CDE ( #I ) )
                    pnd_Idx_Rate_Cde.setValue(pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N);                                                                                     //Natural: ASSIGN #IDX-RATE-CDE := #WORK-RATE-CDE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.examine(new ExamineSource(pnd_Tot_Rate_Cde.getValue("*"),true), new ExamineSearch(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I)),         //Natural: EXAMINE FULL #TOT-RATE-CDE ( * ) FOR #TRN-RATE-CDE ( #I ) GIVING INDEX #I3
                        new ExamineGivingIndex(pnd_I3));
                    //*  3/12 FROM 200
                    if (condition(pnd_I3.equals(getZero())))                                                                                                              //Natural: IF #I3 = 0
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #I4 = 100 TO 250
                        for (pnd_I4.setValue(100); condition(pnd_I4.lessOrEqual(250)); pnd_I4.nadd(1))
                        {
                            if (condition(pnd_Tot_Rate_Cde.getValue(pnd_I4).equals("  ")))                                                                                //Natural: IF #TOT-RATE-CDE ( #I4 ) = '  '
                            {
                                pnd_Idx_Rate_Cde.setValue(pnd_I4);                                                                                                        //Natural: ASSIGN #IDX-RATE-CDE := #I4
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_I4.setValue(pnd_I3);                                                                                                                          //Natural: ASSIGN #I4 := #I3
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Idx_Rate_Cde.setValue(pnd_I4);                                                                                                                    //Natural: ASSIGN #IDX-RATE-CDE := #I4
                }                                                                                                                                                         //Natural: END-IF
                //*   FOLLOWING FOR TEST-ONLY 11/04
                //*  3/12
                if (condition(pnd_I4.greater(250)))                                                                                                                       //Natural: IF #I4 GT 250
                {
                    //*  3/12
                    getReports().write(0, "=",pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr,pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde,"=",pnd_I4,"=",pnd_I);                   //Natural: WRITE '=' #TRN-CNTRCT-PPCN-NBR #TRN-CNTRCT-PAYEE-CDE '=' #I4 '=' #I
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Idx_Rate_Cde.setValue(250);                                                                                                                       //Natural: ASSIGN #IDX-RATE-CDE := 250
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Rate_Cde.getValue(pnd_Idx_Rate_Cde).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I));                                                  //Natural: ASSIGN #TOT-RATE-CDE ( #IDX-RATE-CDE ) := #TRN-RATE-CDE ( #I )
                pnd_Tot_Rate_Div.getValue(pnd_Idx_Rate_Cde).nadd(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_I));                //Natural: ASSIGN #TOT-RATE-DIV ( #IDX-RATE-CDE ) := #TOT-RATE-DIV ( #IDX-RATE-CDE ) + #OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #I )
                pnd_Tot_Rate_Final_Div.getValue(pnd_Idx_Rate_Cde).nadd(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_I));         //Natural: ASSIGN #TOT-RATE-FINAL-DIV ( #IDX-RATE-CDE ) := #TOT-RATE-FINAL-DIV ( #IDX-RATE-CDE ) + #OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #I )
                //*  ADDED FOLLOWING 12/01
                if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22))))                                                                   //Natural: IF ( #IN-PAYMENT-OPTION = 22 )
                {
                    //*  3/12 PREVENT INDEXING ERROR
                    if (condition(pnd_I.lessOrEqual(99)))                                                                                                                 //Natural: IF #I LE 99
                    {
                        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Rate_Cde.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I));                     //Natural: ASSIGN #TPAR-TIAA-RATE-CDE ( #I ) := #TRN-RATE-CDE ( #I )
                        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Pay_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt.getValue(pnd_I));               //Natural: ASSIGN #TPAR-TIAA-PER-PAY-AMT ( #I ) := #TRN-PER-PAY-AMT ( #I )
                        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Tiaa_Per_Div_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt.getValue(pnd_I));               //Natural: ASSIGN #TPAR-TIAA-PER-DIV-AMT ( #I ) := #TRN-PER-DIV-AMT ( #I )
                        //*  3/12
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD 12/01
                pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt.getValue(pnd_I));                                                      //Natural: ADD #TRN-PER-PAY-AMT ( #I ) TO #TRN-NEW-TOT-PER-AMT
                pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt.getValue(pnd_I));                                                      //Natural: ADD #TRN-PER-DIV-AMT ( #I ) TO #TRN-NEW-TOT-DIV-AMT
                pnd_Wrk_New_Final_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt.getValue(pnd_I));                                                                   //Natural: ADD #TRN-FINAL-PAY-AMT ( #I ) TO #WRK-NEW-FINAL-PAY-AMT
                pnd_Wrk_New_Final_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt.getValue(pnd_I));                                                                   //Natural: ADD #TRN-FINAL-DIV-AMT ( #I ) TO #WRK-NEW-FINAL-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ACCUM-NEW-PAYMENT-TRAILER-AMTS
        sub_Accum_New_Payment_Trailer_Amts();
        if (condition(Global.isEscape())) {return;}
        //*  ADDED FOLLOWING IF STMNT TO HANDLE OLD AMTS FOR CONTRACTS    9/98
        //*          OPTION 21  NOT REVALUED IN JANUARY BYPASS CHANGES    9/98
        //*  ADDED OPTION 25,27,28, & 30 FOR TPA, IPRO, P/I TPA CONVERSION.
        //*  ADDED 9/98
        //*  ADDED 01/01
        //*  ADDED 9/98
        if (condition((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(21) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199103))  //Natural: IF ( #IN-PAYMENT-OPTION = 21 AND #IN-ISSUE-YYYY-MM GT 199103 ) OR ( #IN-PAYMENT-OPTION = #TPA-IPRO-CODES ( * ) )
            || (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(pnd_Tpa_Ipro_Codes.getValue("*")))))
        {
            ignore();
            //*  ADDED 9/98
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt.notEquals(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt) || pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt.notEquals(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt)  //Natural: IF #TRN-NEW-TOT-PER-AMT NE #TRN-OLD-TOT-PER-AMT OR #TRN-NEW-TOT-DIV-AMT NE #TRN-OLD-TOT-DIV-AMT OR #WRK-NEW-FINAL-PAY-AMT NE #WRK-OLD-FINAL-PAY-AMT OR #WRK-NEW-FINAL-DIV-AMT NE #WRK-OLD-FINAL-DIV-AMT
                || pnd_Wrk_New_Final_Pay_Amt.notEquals(pnd_Wrk_Old_Final_Pay_Amt) || pnd_Wrk_New_Final_Div_Amt.notEquals(pnd_Wrk_Old_Final_Div_Amt)))
            {
                pnd_Wrk_Tot_Fnd_Rec_Changed.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WRK-TOT-FND-REC-CHANGED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(1, false, pnd_Trn_Fund_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #TRN-FUND-REC
    }
    private void sub_Accum_New_Payment_Trailer_Amts() throws Exception                                                                                                    //Natural: ACCUM-NEW-PAYMENT-TRAILER-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees.nadd(1);                                                                                                             //Natural: ADD 1 TO #TRL-NEW-FUND-PAYEES
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                                      //Natural: ASSIGN #TRL-NEW-PER-PAY-AMT := #TRL-NEW-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                                      //Natural: ASSIGN #TRL-NEW-PER-DIV-AMT := #TRL-NEW-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                                   //Natural: ASSIGN #TRL-NEW-FINAL-PAY-AMT := #TRL-NEW-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                                   //Natural: ASSIGN #TRL-NEW-FINAL-DIV-AMT := #TRL-NEW-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
        //*  OTHER CONTRACTS
        //* ADDED 12/01
        if (condition(! (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(25)   //Natural: IF NOT ( #IN-PAYMENT-OPTION = 22 OR = 25 OR = 27 OR = 28 OR = 30 )
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(27) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(28) 
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(30))))
        {
            //* 11/99
            //*  ADDED 11/09
            if (condition(((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(21) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199103))  //Natural: IF ( #IN-PAYMENT-OPTION = 21 AND #IN-ISSUE-YYYY-MM GT 199103 ) OR ( #IN-CONTRACT-ORIGIN = 3 AND #IN-ISSUE-YYYY-MM GT 199312 ) OR ( #IN-CONTRACT-ORIGIN = 17 OR = 18 OR = 37 OR = 38 OR = 40 ) OR ( #IN-CONTRACT-ORIGIN = 35 OR = 36 OR = 43 OR = 44 OR = 45 OR = 46 )
                || (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(3) && pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm().greater(199312))) 
                || ((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(17) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(18)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(37)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(38)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(40))) || (((((pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(35) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(36)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(43)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(44)) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(45)) 
                || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().equals(46)))))
            {
                //*  GRADED
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G")))                                                                    //Natural: IF #IN-PAYMENT-METHOD = 'G'
                {
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #NTOT-GRD-OTH-FUND-PAYEES := #NTOT-GRD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                     //Natural: ASSIGN #NTOT-GRD-OTH-PER-PAY-AMT := #NTOT-GRD-OTH-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                     //Natural: ASSIGN #NTOT-GRD-OTH-PER-DIV-AMT := #NTOT-GRD-OTH-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                  //Natural: ASSIGN #NTOT-GRD-OTH-FINAL-PAY-AMT := #NTOT-GRD-OTH-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                  //Natural: ASSIGN #NTOT-GRD-OTH-FINAL-DIV-AMT := #NTOT-GRD-OTH-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #NTOT-STD-OTH-FUND-PAYEES := #NTOT-STD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                     //Natural: ASSIGN #NTOT-STD-OTH-PER-PAY-AMT := #NTOT-STD-OTH-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                     //Natural: ASSIGN #NTOT-STD-OTH-PER-DIV-AMT := #NTOT-STD-OTH-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                  //Natural: ASSIGN #NTOT-STD-OTH-FINAL-PAY-AMT := #NTOT-STD-OTH-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                  //Natural: ASSIGN #NTOT-STD-OTH-FINAL-DIV-AMT := #NTOT-STD-OTH-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR TPA CONTRACTS                              /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(pnd_Tpa_Option_Codes.getValue("*"))))                                             //Natural: IF #IN-PAYMENT-OPTION = #TPA-OPTION-CODES ( * )
        {
            //*   STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees.nadd(1);                                                                                                //Natural: ASSIGN #NTOT-STD-TPA-FUND-PAYEES := #NTOT-STD-TPA-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                         //Natural: ASSIGN #NTOT-STD-TPA-PER-PAY-AMT := #NTOT-STD-TPA-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                         //Natural: ASSIGN #NTOT-STD-TPA-PER-DIV-AMT := #NTOT-STD-TPA-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                      //Natural: ASSIGN #NTOT-STD-TPA-FINAL-PAY-AMT := #NTOT-STD-TPA-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                      //Natural: ASSIGN #NTOT-STD-TPA-FINAL-DIV-AMT := #NTOT-STD-TPA-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR IPRO CONTRACTS                             /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(25) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(27)))    //Natural: IF #IN-PAYMENT-OPTION = 25 OR = 27
        {
            //*   STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees.nadd(1);                                                                                               //Natural: ASSIGN #NTOT-STD-IPRO-FUND-PAYEES := #NTOT-STD-IPRO-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                        //Natural: ASSIGN #NTOT-STD-IPRO-PER-PAY-AMT := #NTOT-STD-IPRO-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                        //Natural: ASSIGN #NTOT-STD-IPRO-PER-DIV-AMT := #NTOT-STD-IPRO-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                     //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-PAY-AMT := #NTOT-STD-IPRO-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                     //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-DIV-AMT := #NTOT-STD-IPRO-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR IPRO CONTRACTS                             /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(25) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(27)))    //Natural: IF #IN-PAYMENT-OPTION = 25 OR = 27
        {
            //*   STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees.nadd(1);                                                                                               //Natural: ASSIGN #NTOT-STD-IPRO-FUND-PAYEES := #NTOT-STD-IPRO-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                        //Natural: ASSIGN #NTOT-STD-IPRO-PER-PAY-AMT := #NTOT-STD-IPRO-PER-PAY-AMT + #TRN-OLD-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                        //Natural: ASSIGN #NTOT-STD-IPRO-PER-DIV-AMT := #NTOT-STD-IPRO-PER-DIV-AMT + #TRN-OLD-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt.nadd(pnd_Wrk_Old_Final_Pay_Amt);                                                                     //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-PAY-AMT := #NTOT-STD-IPRO-FINAL-PAY-AMT + #WRK-OLD-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt.nadd(pnd_Wrk_Old_Final_Div_Amt);                                                                     //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-DIV-AMT := #NTOT-STD-IPRO-FINAL-DIV-AMT + #WRK-OLD-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TOTALS FOR P/I CONTRACTS                             /*  01/2001
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().equals(22)))                                                                             //Natural: IF #IN-PAYMENT-OPTION = 22
        {
            //*   STANDARD
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("S")))                                                                        //Natural: IF #IN-PAYMENT-METHOD = 'S'
            {
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees.nadd(1);                                                                                                 //Natural: ASSIGN #NTOT-STD-PI-FUND-PAYEES := #NTOT-STD-PI-FUND-PAYEES + 1
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                          //Natural: ASSIGN #NTOT-STD-PI-PER-PAY-AMT := #NTOT-STD-PI-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                          //Natural: ASSIGN #NTOT-STD-PI-PER-DIV-AMT := #NTOT-STD-PI-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                       //Natural: ASSIGN #NTOT-STD-PI-FINAL-PAY-AMT := #NTOT-STD-PI-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
                pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                       //Natural: ASSIGN #NTOT-STD-PI-FINAL-DIV-AMT := #NTOT-STD-PI-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRADED
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().equals("G")))                                                                            //Natural: IF #IN-PAYMENT-METHOD = 'G'
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #NTOT-GRD-STD-FUND-PAYEES := #NTOT-GRD-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                             //Natural: ASSIGN #NTOT-GRD-STD-PER-PAY-AMT := #NTOT-GRD-STD-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                             //Natural: ASSIGN #NTOT-GRD-STD-PER-DIV-AMT := #NTOT-GRD-STD-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                          //Natural: ASSIGN #NTOT-GRD-STD-FINAL-PAY-AMT := #NTOT-GRD-STD-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                          //Natural: ASSIGN #NTOT-GRD-STD-FINAL-DIV-AMT := #NTOT-GRD-STD-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees.nadd(1);                                                                                                        //Natural: ASSIGN #NTOT-STD-FUND-PAYEES := #NTOT-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                                 //Natural: ASSIGN #NTOT-STD-PER-PAY-AMT := #NTOT-STD-PER-PAY-AMT + #TRN-NEW-TOT-PER-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt.nadd(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                                 //Natural: ASSIGN #NTOT-STD-PER-DIV-AMT := #NTOT-STD-PER-DIV-AMT + #TRN-NEW-TOT-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt.nadd(pnd_Wrk_New_Final_Pay_Amt);                                                                              //Natural: ASSIGN #NTOT-STD-FINAL-PAY-AMT := #NTOT-STD-FINAL-PAY-AMT + #WRK-NEW-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt.nadd(pnd_Wrk_New_Final_Div_Amt);                                                                              //Natural: ASSIGN #NTOT-STD-FINAL-DIV-AMT := #NTOT-STD-FINAL-DIV-AMT + #WRK-NEW-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Iaa_Cntrct_Info() throws Exception                                                                                                               //Natural: GET-IAA-CNTRCT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-PPCN-NBR
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Ppcn_Nbr, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_iaa_Cntrct.readNextRow("F1", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IAA-CNTRCT RECORD FOUND FOR THIS PPCN-NBR-->",pnd_Cntrct_Ppcn_Nbr);                                                             //Natural: WRITE 'NO IAA-CNTRCT RECORD FOUND FOR THIS PPCN-NBR-->' #CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cntrct_Fnd_Sw.setValue("N");                                                                                                                          //Natural: ASSIGN #CNTRCT-FND-SW := 'N'
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Write_P_I_Reinvst_File() throws Exception                                                                                                            //Natural: WRITE-P-I-REINVST-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Rate_Cnt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt);                                                                          //Natural: ASSIGN #TPAR-RATE-CNT := #TRN-RATE-CNT
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Optn_Cde.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code);                                                                       //Natural: ASSIGN #TPAR-OPTN-CDE := #TRN-OPTION-CODE
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Ppcn_Nbr.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr);                                                            //Natural: ASSIGN #TPAR-CNTRCT-PPCN-NBR := #TRN-CNTRCT-PPCN-NBR
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Ppcn_Nbr.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr);                                                            //Natural: ASSIGN #TPAR-CNTRCT-PPCN-NBR := #TRN-CNTRCT-PPCN-NBR
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Cntrct_Payee_Cde.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde);                                                          //Natural: ASSIGN #TPAR-CNTRCT-PAYEE-CDE := #TRN-CNTRCT-PAYEE-CDE
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Optn_Cde.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code);                                                                       //Natural: ASSIGN #TPAR-OPTN-CDE := #TRN-OPTION-CODE
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Per_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                            //Natural: ASSIGN #TPAR-OLD-TOT-PER-AMT := #TRN-OLD-TOT-PER-AMT
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Div_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                            //Natural: ASSIGN #TPAR-OLD-TOT-DIV-AMT := #TRN-OLD-TOT-DIV-AMT
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Per_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt);                                                            //Natural: ASSIGN #TPAR-NEW-TOT-PER-AMT := #TRN-NEW-TOT-PER-AMT
        pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Div_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);                                                            //Natural: ASSIGN #TPAR-NEW-TOT-DIV-AMT := #TRN-NEW-TOT-DIV-AMT
        //* ACCUM TOT
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Per_Amt.nadd(pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Per_Amt);                                                         //Natural: ADD #TPAR-OLD-TOT-PER-AMT TO #TOTL-TOT-OLD-PER-AMT
        //* AMTS FOR
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_Old_Div_Amt.nadd(pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_Old_Tot_Div_Amt);                                                         //Natural: ADD #TPAR-OLD-TOT-DIV-AMT TO #TOTL-TOT-OLD-DIV-AMT
        //* TOTAL FILE
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Per_Amt.nadd(pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Per_Amt);                                                         //Natural: ADD #TPAR-NEW-TOT-PER-AMT TO #TOTL-TOT-NEW-PER-AMT
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_New_Div_Amt.nadd(pnd_Tpar_To_Tpa_Interface_Pnd_Tpar_New_Tot_Div_Amt);                                                         //Natural: ADD #TPAR-NEW-TOT-DIV-AMT TO #TOTL-TOT-NEW-DIV-AMT
        pnd_Totl_Tpa_Total_Rec_Pnd_Totl_Tot_No_Cntrcts.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTL-TOT-NO-CNTRCTS
        getWorkFiles().write(4, false, pnd_Tpar_To_Tpa_Interface);                                                                                                        //Natural: WRITE WORK FILE 4 #TPAR-TO-TPA-INTERFACE
    }
    private void sub_Write_Graded_Rec() throws Exception                                                                                                                  //Natural: WRITE-GRADED-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Ppcn_Nbr.setValue(pnd_Fund_Key_Pnd_Fund_Ppcn);                                                                                 //Natural: ASSIGN #GRD-CNTRCT-PPCN-NBR := #FUND-PPCN
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cntrct_Payee_Cde.setValue(pnd_Fund_Key_Pnd_Fund_Paye);                                                                                //Natural: ASSIGN #GRD-CNTRCT-PAYEE-CDE := #FUND-PAYE
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);                                                                      //Natural: ASSIGN #GRD-CMPNY-FUND-CDE := IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Prev_Amt.compute(new ComputeParameters(false, pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Prev_Amt), pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt.add(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt)); //Natural: ASSIGN #GRD-GROSS-PREV-AMT := #TRN-OLD-TOT-PER-AMT + #TRN-OLD-TOT-DIV-AMT
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Curr_Amt.compute(new ComputeParameters(false, pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Curr_Amt), pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt.add(pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt)); //Natural: ASSIGN #GRD-GROSS-CURR-AMT := #TRN-NEW-TOT-PER-AMT + #TRN-NEW-TOT-DIV-AMT
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt.compute(new ComputeParameters(false, pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt), DbsField.add(getZero(),          //Natural: ASSIGN #GRD-GROSS-NXT-YR-AMT := 0 + #OUT-NXT-YR-GRD-GUAR-PAY ( * )
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Nxt_Yr_Grd_Guar_Pay().getValue("*")));
        pnd_Grd_Cntrcts_Rec_Pnd_Grd_Gross_Nxt_Yr_Amt.nadd(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Nxt_Yr_Grd_Dividend_Pay().getValue("*"));                            //Natural: ASSIGN #GRD-GROSS-NXT-YR-AMT := #GRD-GROSS-NXT-YR-AMT + #OUT-NXT-YR-GRD-DIVIDEND-PAY ( * )
        getWorkFiles().write(3, false, pnd_Grd_Cntrcts_Rec);                                                                                                              //Natural: WRITE WORK FILE 3 #GRD-CNTRCTS-REC
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Wrk_Chg_Tiaa_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Tiaa_Payees), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees)); //Natural: ASSIGN #WRK-CHG-TIAA-PAYEES := #TRL-NEW-TIAA-PAYEES - #TRL-OLD-TIAA-PAYEES
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #TRL-NEW-FUND-PAYEES - #TRL-OLD-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #TRL-NEW-PER-PAY-AMT - #TRL-OLD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #TRL-NEW-PER-DIV-AMT - #TRL-OLD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #TRL-NEW-FINAL-PAY-AMT - #TRL-OLD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #TRL-NEW-FINAL-DIV-AMT - #TRL-OLD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA SPECIAL FUNCTION TIAA DIVIDEND CHANGE FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Date8_Pnd_Next_Updt_Ccyymm,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X'IA SPECIAL FUNCTION TIAA DIVIDEND CHANGE FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
            new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE);                                                                                    //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Hd_Txt1_Pnd_Hd_Txt1_R);                                                                                            //Natural: WRITE ( 1 ) #HD-TXT1-R
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R);                                                                                          //Natural: WRITE ( 1 ) #HD2-TXT1-R
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FUND RECORDS    ",new TabSetting(41),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees,                    //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FUND RECORDS    ' 41T #TRL-OLD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #TRL-NEW-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 102T #TRL-NEW-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(102),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL PER PAYMENT     ",new TabSetting(35),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt,                    //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL PER PAYMENT     ' 35T #TRL-OLD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #TRL-NEW-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T CNTRL-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL PER DIVIDEND   ",new TabSetting(35),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt,                     //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL PER DIVIDEND   ' 35T #TRL-OLD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #TRL-NEW-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T CNTRL-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL PAYMENT   ",new TabSetting(35),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt,                  //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FINAL PAYMENT   ' 35T#TRL-OLD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#TRL-NEW-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T CNTRL-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL DIVIDEND ",new TabSetting(35),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt,                   //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FINAL DIVIDEND ' 35T#TRL-OLD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#TRL-NEW-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T CNTRL-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA ACTIVE PAYEES              ",new TabSetting(41),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees,                    //Natural: WRITE ( 1 ) 'TIAA ACTIVE PAYEES              ' 41T #TRL-OLD-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #TRL-NEW-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 102T CNTRL-ACTVE-TIAA-PYS ( EM = ZZZ,ZZZ,ZZ9- ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Tiaa_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(102),iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-GRD-STD-FUND-PAYEES - #OTOT-GRD-STD-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-GRD-STD-PER-PAY-AMT - #OTOT-GRD-STD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-GRD-STD-PER-DIV-AMT - #OTOT-GRD-STD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-GRD-STD-FINAL-PAY-AMT - #OTOT-GRD-STD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-GRD-STD-FINAL-DIV-AMT - #OTOT-GRD-STD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA GRADED   FUND RECORDS    ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA GRADED   FUND RECORDS    ' 41T #OTOT-GRD-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-GRD-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   PER PAYMENT     ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       GRADED   PER PAYMENT     ' 35T #OTOT-GRD-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   PER DIVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt,                 //Natural: WRITE ( 1 ) '       GRADED   PER DIVIDEND  ' 35T #OTOT-GRD-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt,              //Natural: WRITE ( 1 ) '       GRADED   FINAL PAYMENT  ' 35T#OTOT-GRD-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt,               //Natural: WRITE ( 1 ) '       GRADED   FINAL DIVIDEND' 35T#OTOT-GRD-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-FUND-PAYEES - #OTOT-STD-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-PER-PAY-AMT - #OTOT-STD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-PER-DIV-AMT - #OTOT-STD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-FINAL-PAY-AMT - #OTOT-STD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-FINAL-DIV-AMT - #OTOT-STD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STANDARD FUND RECORDS    ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees,                   //Natural: WRITE ( 1 ) '  TIAA STANDARD FUND RECORDS    ' 41T #OTOT-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD PER PAYMENT     ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt,                   //Natural: WRITE ( 1 ) '       STANDARD PER PAYMENT     ' 35T #OTOT-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD PER DIVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt,                     //Natural: WRITE ( 1 ) '       STANDARD PER DIVIDEND  ' 35T #OTOT-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt,                  //Natural: WRITE ( 1 ) '       STANDARD FINAL PAYMENT  ' 35T#OTOT-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt,                   //Natural: WRITE ( 1 ) '       STANDARD FINAL DIVIDEND' 35T#OTOT-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-GRD-OTH-FUND-PAYEES - #OTOT-GRD-OTH-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-GRD-OTH-PER-PAY-AMT - #OTOT-GRD-OTH-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-GRD-OTH-PER-DIV-AMT - #OTOT-GRD-OTH-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-GRD-OTH-FINAL-PAY-AMT - #OTOT-GRD-OTH-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-GRD-OTH-FINAL-DIV-AMT - #OTOT-GRD-OTH-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA GRADED OTH FUND RECORDS  ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA GRADED OTH FUND RECORDS  ' 41T #OTOT-GRD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-GRD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH PER PAYMENT   ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       GRADED OTH PER PAYMENT   ' 35T #OTOT-GRD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH PER DIVIDEND ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt,                //Natural: WRITE ( 1 ) '       GRADED OTH PER DIVIDEND ' 35T #OTOT-GRD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt,             //Natural: WRITE ( 1 ) '       GRADED OTH FINAL PAYMENT ' 35T#OTOT-GRD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt,             //Natural: WRITE ( 1 ) '       GRADED OTH FINAL DIVIDEND' 35T#OTOT-GRD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-OTH-FUND-PAYEES - #OTOT-STD-OTH-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-OTH-PER-PAY-AMT - #OTOT-STD-OTH-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-OTH-PER-DIV-AMT - #OTOT-STD-OTH-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-OTH-FINAL-PAY-AMT - #OTOT-STD-OTH-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-OTH-FINAL-DIV-AMT - #OTOT-STD-OTH-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD OTH FUND RECORDS  ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA STNDRD OTH FUND RECORDS  ' 41T #OTOT-STD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH PER PAYMENT   ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       STNDRD OTH PER PAYMENT   ' 35T #OTOT-STD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH PER DIVIDEND ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt,                //Natural: WRITE ( 1 ) '       STNDRD OTH PER DIVIDEND ' 35T #OTOT-STD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt,             //Natural: WRITE ( 1 ) '       STNDRD OTH FINAL PAYMENT ' 35T#OTOT-STD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt,             //Natural: WRITE ( 1 ) '       STNDRD OTH FINAL DIVIDEND' 35T#OTOT-STD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-PI-FUND-PAYEES - #OTOT-STD-PI-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-PI-PER-PAY-AMT - #OTOT-STD-PI-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-PI-PER-DIV-AMT - #OTOT-STD-PI-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-PI-FINAL-PAY-AMT - #OTOT-STD-PI-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-PI-FINAL-DIV-AMT - #OTOT-STD-PI-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD P/I FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA STNDRD P/I FUND RECORDS   ' 41T #OTOT-STD-PI-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-PI-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I PER PAYMENT    ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       STNDRD P/I PER PAYMENT    ' 35T #OTOT-STD-PI-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PI-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I PER DIVIDEND ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt,                 //Natural: WRITE ( 1 ) '       STNDRD P/I PER DIVIDEND ' 35T #OTOT-STD-PI-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PI-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt,              //Natural: WRITE ( 1 ) '       STNDRD P/I FINAL PAYMENT ' 35T#OTOT-STD-PI-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-PI-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD PI FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt,               //Natural: WRITE ( 1 ) '       STNDRD PI FINAL DIVIDEND' 35T#OTOT-STD-PI-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-PI-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-TPA-FUND-PAYEES - #OTOT-STD-TPA-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-TPA-PER-PAY-AMT - #OTOT-STD-TPA-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-TPA-PER-DIV-AMT - #OTOT-STD-TPA-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-TPA-FINAL-PAY-AMT - #OTOT-STD-TPA-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-TPA-FINAL-DIV-AMT - #OTOT-STD-TPA-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD TPA FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees,              //Natural: WRITE ( 1 ) '  TIAA STNDRD TPA FUND RECORDS   ' 41T #OTOT-STD-TPA-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-TPA-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA PER PAYMENT    ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt,              //Natural: WRITE ( 1 ) '       STNDRD TPA PER PAYMENT    ' 35T #OTOT-STD-TPA-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-TPA-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA PER DIVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt,               //Natural: WRITE ( 1 ) '       STNDRD TPA PER DIVIDEND  ' 35T #OTOT-STD-TPA-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-TPA-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt,            //Natural: WRITE ( 1 ) '       STNDRD TPA FINAL PAYMENT  ' 35T#OTOT-STD-TPA-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-TPA-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt,             //Natural: WRITE ( 1 ) '       STNDRD TPA FINAL DIVIDEND' 35T#OTOT-STD-TPA-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-TPA-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-IPRO-FUND-PAYEES - #OTOT-STD-IPRO-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-IPRO-PER-PAY-AMT - #OTOT-STD-IPRO-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-IPRO-PER-DIV-AMT - #OTOT-STD-IPRO-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-IPRO-FINAL-PAY-AMT - #OTOT-STD-IPRO-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-IPRO-FINAL-DIV-AMT - #OTOT-STD-IPRO-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD IPRO FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees,            //Natural: WRITE ( 1 ) '  TIAA STNDRD IPRO FUND RECORDS   ' 41T #OTOT-STD-IPRO-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-IPRO-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO PER PAYMENT   ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt,             //Natural: WRITE ( 1 ) '       STNDRD IPRO PER PAYMENT   ' 35T #OTOT-STD-IPRO-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-IPRO-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO PER DIVIDEND ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt,              //Natural: WRITE ( 1 ) '       STNDRD IPRO PER DIVIDEND ' 35T #OTOT-STD-IPRO-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-IPRO-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt,           //Natural: WRITE ( 1 ) '       STNDRD IPRO FINAL PAYMENT ' 35T#OTOT-STD-IPRO-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-IPRO-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO FINAL DIVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt,           //Natural: WRITE ( 1 ) '       STNDRD IPRO FINAL DIVIDEND' 35T#OTOT-STD-IPRO-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-IPRO-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL FUND RECORDS CHANGED......",new TabSetting(60),pnd_Wrk_Tot_Fnd_Rec_Changed, new ReportEditMask                  //Natural: WRITE ( 1 ) 'TOTAL FUND RECORDS CHANGED......' 60T #WRK-TOT-FND-REC-CHANGED ( EM = ZZZ,ZZZ,ZZ9- )
            ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
    }
    private void sub_Print_Rate_Totals() throws Exception                                                                                                                 //Natural: PRINT-RATE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA SPECIAL FUNCTION TOTALS BY RATE FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Date8_Pnd_Next_Updt_Ccyymm,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X'IA SPECIAL FUNCTION TOTALS BY RATE FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
            new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE,NEWLINE);                                                                            //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT //
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(7),"     RATE               DIVIDENDS        FINAL DIVIDENDS    ");                                  //Natural: WRITE ( 1 ) 7X'     RATE               DIVIDENDS        FINAL DIVIDENDS    '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(7),"    ------           ---------------     ---------------    ");                                  //Natural: WRITE ( 1 ) 7X'    ------           ---------------     ---------------    '
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 200
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(200)); pnd_I.nadd(1))
        {
            //*  IF #TOT-RATE-DIV (#I) NE 0
            //*  CHANGED ABOVE TO FOLLOWING 6/03
            if (condition(! (pnd_Tot_Rate_Cde.getValue(pnd_I).equals("00") || pnd_Tot_Rate_Cde.getValue(pnd_I).equals("  "))))                                            //Natural: IF NOT ( #TOT-RATE-CDE ( #I ) = '00' OR = '  ' )
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Tot_Rate_Cde.getValue(pnd_I), new ReportEditMask ("XX"),new TabSetting(30),pnd_Tot_Rate_Div.getValue(pnd_I),  //Natural: WRITE ( 1 ) 15T #TOT-RATE-CDE ( #I ) ( EM = XX ) 30T #TOT-RATE-DIV ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 50T#TOT-RATE-FINAL-DIV ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new TabSetting(50),pnd_Tot_Rate_Final_Div.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Gtot_Rate_Div.nadd(pnd_Tot_Rate_Div.getValue(pnd_I));                                                                                                 //Natural: ASSIGN #GTOT-RATE-DIV := #GTOT-RATE-DIV + #TOT-RATE-DIV ( #I )
                pnd_Gtot_Rate_Final_Div.nadd(pnd_Tot_Rate_Final_Div.getValue(pnd_I));                                                                                     //Natural: ASSIGN #GTOT-RATE-FINAL-DIV := #GTOT-RATE-FINAL-DIV + #TOT-RATE-FINAL-DIV ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"GRAND TOTAL....",new TabSetting(27),pnd_Gtot_Rate_Div,new  //Natural: WRITE ( 1 ) / 2T'GRAND TOTAL....' 27T#GTOT-RATE-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 47T#GTOT-RATE-FINAL-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            TabSetting(47),pnd_Gtot_Rate_Final_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Cntrl_Rcrd() throws Exception                                                                                                               //Natural: READ-IAA-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ02")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Print_Aian035_Error_Msg() throws Exception                                                                                                           //Natural: PRINT-AIAN035-ERROR-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //*   FOLLOWING FOR TEST-ONLY 11/02
        //*  3/12
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(11)))                                                                   //Natural: IF #OUT-AIAN035-RETURN-CODE-NBR = 11
        {
            getReports().write(1, ReportOption.NOTITLE,"Msg from PROGRAM IAAP395",NEWLINE,"AIAN035-RETURN-CODE-->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code(), //Natural: WRITE ( 1 ) 'Msg from PROGRAM IAAP395' / 'AIAN035-RETURN-CODE-->' #OUT-AIAN035-RETURN-CODE / 'CONTRACT-NBR -------->' #IN-CONTRACT-PAYEE / 'ISSUE-DATE----------->' #IN-ISSUE-YYYY-MM
                NEWLINE,"CONTRACT-NBR -------->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Payee(),NEWLINE,"ISSUE-DATE----------->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm());
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"***************************************************");                                                            //Natural: WRITE ( 1 ) '***************************************************'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   END OF TEST-ONLY 11/02
        //*   ADDED FOLLOWING STMNT                       11/97
        //*   ADDED 14 TO FOLLOWING AS PER  WANG, LIEPING 10/03
        //*  3/12
        if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(10) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(7)  //Natural: IF #OUT-AIAN035-RETURN-CODE-NBR = 10 OR = 07 OR = 09 OR = 14
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(9) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(14)))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"**********************************************");                                                 //Natural: WRITE ( 1 ) // '**********************************************'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*** NON ZERO RETURN CODE FROM ACTUARIAL MODULE AIAN083-->");                                                      //Natural: WRITE ( 1 ) '*** NON ZERO RETURN CODE FROM ACTUARIAL MODULE AIAN083-->'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code()); //Natural: WRITE ( 1 ) '*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->' #OUT-AIAN035-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*** FOR CONTRACT-NBR & PAYEE ------------------>",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract(),           //Natural: WRITE ( 1 ) '*** FOR CONTRACT-NBR & PAYEE ------------------>' #IN-CONTRACT #IN-PAYEE
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payee());
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE," ISSUE DATE PASSED TO MODULE AIAN083--->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Yyyy_Mm());             //Natural: WRITE ( 1 ) ' ISSUE DATE PASSED TO MODULE AIAN083--->' #IN-ISSUE-YYYY-MM
            if (Global.isEscape()) return;
            FOR05:                                                                                                                                                        //Natural: FOR #K = 1 TO 30
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(30)); pnd_K.nadd(1))
            {
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Transfer_Eff_Date().getValue(pnd_K).equals(getZero())))                                      //Natural: IF #IN-RATE-TRANSFER-EFF-DATE ( #K ) = 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE," RATE-CDE & RATE TRANSFER  DATE PASSED TO AIAN083->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_K), //Natural: WRITE ( 1 ) ' RATE-CDE & RATE TRANSFER  DATE PASSED TO AIAN083->' #IN-RATE-CODE ( #K ) #IN-RATE-TRANSFER-EFF-DATE ( #K )
                    pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Transfer_Eff_Date().getValue(pnd_K));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD  11/97
        if (condition(! (pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(10) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(7)  //Natural: IF NOT ( #OUT-AIAN035-RETURN-CODE-NBR = 10 OR = 07 OR = 09 OR = 14 )
            || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(9) || pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(14))))
        {
            FOR06:                                                                                                                                                        //Natural: FOR #I = 1 TO 4
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"**********************************************");                                             //Natural: WRITE ( 1 ) // '**********************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"*** JOB TERMINATED PREMATURELY FIX ERROR & RERUN JOB");                                                       //Natural: WRITE ( 1 ) '*** JOB TERMINATED PREMATURELY FIX ERROR & RERUN JOB'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"*** ERROR BAD RETURN CODE FROM ACTUARIAL MODULE AIAN083");                                                    //Natural: WRITE ( 1 ) '*** ERROR BAD RETURN CODE FROM ACTUARIAL MODULE AIAN083'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code()); //Natural: WRITE ( 1 ) '*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->' #OUT-AIAN035-RETURN-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"*** CONTRACT-NBR & PAYEE WITH ERROR ----------->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract(),       //Natural: WRITE ( 1 ) '*** CONTRACT-NBR & PAYEE WITH ERROR ----------->' #IN-CONTRACT #IN-PAYEE
                    pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payee());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(5)))                                                            //Natural: IF #OUT-AIAN035-RETURN-CODE-NBR = 5
                {
                    getReports().write(1, ReportOption.NOTITLE,"*--> CHECK DATE-->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date(),"PASSED TO AIAN083 ");    //Natural: WRITE ( 1 ) '*--> CHECK DATE-->' #IN-PROCESSING-DATE 'PASSED TO AIAN083 '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,"IS INVALID PROCESSING CHECK DATE SHOULD BE FOR JANUARY");                                                 //Natural: WRITE ( 1 ) 'IS INVALID PROCESSING CHECK DATE SHOULD BE FOR JANUARY'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "*** JOB TERMINATED PREMATURELY FIX ERROR & RERUN JOB");                                                                            //Natural: WRITE '*** JOB TERMINATED PREMATURELY FIX ERROR & RERUN JOB'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*** ERROR BAD RETURN CODE FROM ACTUARIAL MODULE AIAN083");                                                                         //Natural: WRITE '*** ERROR BAD RETURN CODE FROM ACTUARIAL MODULE AIAN083'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code());               //Natural: WRITE '*** RETURN CODE FROM ACTUARIAL MODULE AIAN083-->' #OUT-AIAN035-RETURN-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*** CONTRACT-NBR & PAYEE WITH ERROR ----------->",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract(),pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payee()); //Natural: WRITE '*** CONTRACT-NBR & PAYEE WITH ERROR ----------->' #IN-CONTRACT #IN-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
