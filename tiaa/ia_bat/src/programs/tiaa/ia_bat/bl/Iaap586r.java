/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:08 PM
**        * FROM NATURAL PROGRAM : Iaap586r
************************************************************
**        * FILE NAME            : Iaap586r.java
**        * CLASS NAME           : Iaap586r
**        * INSTANCE NAME        : Iaap586r
************************************************************
*******************************************************************
* YR2000 COMPLIANT FIX APPLIED     ---> T SHINE 11/30/99          *
*                         LAST PCY STOW      ON 07/28/99 16:36:30 *
*                         REVIEWED BY   JS   ON 12/02/99          *
*******************************************************************
**Y2NCTS YENY13
*
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP586R      PRODUCES DAILY GROSS OVERPAYMENT      *
*      DATE   -  03/99         REPORTS FOR EACH TYPE OF FUND        *
*    AUTHOR   -  ARI G.        EX. TIAA, CREF STOCK, CREF GLOBAL ETC.*
*   HISTORY 05/00  A.G.        INCORPRATED PA SELECT INTO REPORT
*
*   05/07/2002   TD    RESTOWED FOR CHANGES MADE IN IAAN051A
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap586r extends BLNatBase
{
    // Data Areas
    private LdaIaal586r ldaIaal586r;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Y2_Parm_Date_Yy;

    private DbsGroup pnd_Y2_Parm_Date_Yy__R_Field_1;
    private DbsField pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_A;

    private DataAccessProgramView vw_iaa_Cref_Fund;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_2;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_W_Fund_Code_Tot;

    private DbsGroup pnd_W_Fund_Code_Tot__R_Field_3;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2;
    private DbsField pnd_Cntrct;

    private DbsGroup pnd_Cntrct__R_Field_4;
    private DbsField pnd_Cntrct_Pnd_Cntrct_1;

    private DbsGroup pnd_W2_File;
    private DbsField pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr;

    private DbsGroup pnd_W2_File__R_Field_5;
    private DbsField pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8;
    private DbsField pnd_W2_File_Pnd_W2_Payee_Cde_Alpha;

    private DbsGroup pnd_W2_File__R_Field_6;
    private DbsField pnd_W2_File_Pnd_W2_Payee_Cde_Num;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code;

    private DbsGroup pnd_W2_File__R_Field_7;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_1;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_2_3;
    private DbsField pnd_W2_File_Pnd_W2_Desc;
    private DbsField pnd_W2_File_Pnd_W2_Mode;
    private DbsField pnd_W2_File_Pnd_W2_Date;
    private DbsField pnd_W2_File_Pnd_W2_Guar_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Divd_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Unit_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Guar_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Divd_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Unit_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Msg;
    private DbsField pnd_Len;
    private DbsField pnd_Num_Of_Fund;
    private DbsField pnd_Error;
    private DbsField pnd_Write_Out_Contract;
    private DbsField pnd_Grand_Tot_Ovrpy;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_W_Fund_Desc;
    private DbsField pnd_Fund_Desc;
    private DbsField pnd_Fnd_Dsc;
    private DbsField pnd_First_Time_Thru;
    private DbsField pnd_Fund_Code_Hold;

    private DbsGroup pnd_Fund_Code_Hold__R_Field_8;
    private DbsField pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1;
    private DbsField pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3;
    private DbsField pnd_Fund_Code_1_3;

    private DbsGroup pnd_Fund_Code_1_3__R_Field_9;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_1;
    private DbsField pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3;
    private DbsField pnd_Contract_Hold;

    private DbsGroup pnd_Contract_Hold__R_Field_10;
    private DbsField pnd_Contract_Hold_Pnd_H_Contract_1_7;
    private DbsField pnd_Contract_Hold_Pnd_H_Contract_8;
    private DbsField pnd_Payee_Cde_Hold;
    private DbsField pnd_First_Time;
    private DbsField pnd_M;
    private DbsField pnd_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal586r = new LdaIaal586r();
        registerRecord(ldaIaal586r);
        registerRecord(ldaIaal586r.getVw_iaa_Cntrct_View());
        registerRecord(ldaIaal586r.getVw_new_Tiaa_Rates());
        registerRecord(ldaIaal586r.getVw_iaa_Old_Tiaa_Rates_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Y2_Parm_Date_Yy = localVariables.newFieldInRecord("pnd_Y2_Parm_Date_Yy", "#Y2-PARM-DATE-YY", FieldType.NUMERIC, 2);

        pnd_Y2_Parm_Date_Yy__R_Field_1 = localVariables.newGroupInRecord("pnd_Y2_Parm_Date_Yy__R_Field_1", "REDEFINE", pnd_Y2_Parm_Date_Yy);
        pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_A = pnd_Y2_Parm_Date_Yy__R_Field_1.newFieldInGroup("pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_A", "#Y2-PARM-DATE-YY-A", 
            FieldType.STRING, 2);

        vw_iaa_Cref_Fund = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund", "IAA-CREF-FUND"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS");
        iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        registerRecord(vw_iaa_Cref_Fund);

        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_W_Fund_Code_Tot = localVariables.newFieldInRecord("pnd_W_Fund_Code_Tot", "#W-FUND-CODE-TOT", FieldType.STRING, 3);

        pnd_W_Fund_Code_Tot__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Fund_Code_Tot__R_Field_3", "REDEFINE", pnd_W_Fund_Code_Tot);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1 = pnd_W_Fund_Code_Tot__R_Field_3.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2 = pnd_W_Fund_Code_Tot__R_Field_3.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2", "#W-FUND-CODE-2", 
            FieldType.STRING, 2);
        pnd_Cntrct = localVariables.newFieldInRecord("pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10);

        pnd_Cntrct__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct__R_Field_4", "REDEFINE", pnd_Cntrct);
        pnd_Cntrct_Pnd_Cntrct_1 = pnd_Cntrct__R_Field_4.newFieldInGroup("pnd_Cntrct_Pnd_Cntrct_1", "#CNTRCT-1", FieldType.STRING, 1);

        pnd_W2_File = localVariables.newGroupInRecord("pnd_W2_File", "#W2-FILE");
        pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr", "#W2-OVRPYMNT-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_W2_File__R_Field_5 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_5", "REDEFINE", pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);
        pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8 = pnd_W2_File__R_Field_5.newFieldInGroup("pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8", "#W2-OVRPYMNT-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_W2_File_Pnd_W2_Payee_Cde_Alpha = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Payee_Cde_Alpha", "#W2-PAYEE-CDE-ALPHA", FieldType.STRING, 
            2);

        pnd_W2_File__R_Field_6 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_6", "REDEFINE", pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);
        pnd_W2_File_Pnd_W2_Payee_Cde_Num = pnd_W2_File__R_Field_6.newFieldInGroup("pnd_W2_File_Pnd_W2_Payee_Cde_Num", "#W2-PAYEE-CDE-NUM", FieldType.NUMERIC, 
            2);
        pnd_W2_File_Pnd_W2_Fund_Code = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code", "#W2-FUND-CODE", FieldType.STRING, 3);

        pnd_W2_File__R_Field_7 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_7", "REDEFINE", pnd_W2_File_Pnd_W2_Fund_Code);
        pnd_W2_File_Pnd_W2_Fund_Code_1 = pnd_W2_File__R_Field_7.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_1", "#W2-FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_W2_File_Pnd_W2_Fund_Code_2_3 = pnd_W2_File__R_Field_7.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_2_3", "#W2-FUND-CODE-2-3", FieldType.STRING, 
            2);
        pnd_W2_File_Pnd_W2_Desc = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Desc", "#W2-DESC", FieldType.STRING, 20);
        pnd_W2_File_Pnd_W2_Mode = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Mode", "#W2-MODE", FieldType.NUMERIC, 3);
        pnd_W2_File_Pnd_W2_Date = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Date", "#W2-DATE", FieldType.NUMERIC, 8);
        pnd_W2_File_Pnd_W2_Guar_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Guar_Payment", "#W2-GUAR-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Divd_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Divd_Payment", "#W2-DIVD-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Unit_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Unit_Payment", "#W2-UNIT-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Guar_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Guar_Overpayment", "#W2-GUAR-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Divd_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Divd_Overpayment", "#W2-DIVD-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Unit_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Unit_Overpayment", "#W2-UNIT-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Msg = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Msg", "#W2-MSG", FieldType.STRING, 3);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Num_Of_Fund = localVariables.newFieldInRecord("pnd_Num_Of_Fund", "#NUM-OF-FUND", FieldType.NUMERIC, 6);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_Write_Out_Contract = localVariables.newFieldInRecord("pnd_Write_Out_Contract", "#WRITE-OUT-CONTRACT", FieldType.NUMERIC, 6);
        pnd_Grand_Tot_Ovrpy = localVariables.newFieldInRecord("pnd_Grand_Tot_Ovrpy", "#GRAND-TOT-OVRPY", FieldType.NUMERIC, 11, 2);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_W_Fund_Desc = localVariables.newFieldInRecord("pnd_W_Fund_Desc", "#W-FUND-DESC", FieldType.STRING, 45);
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);
        pnd_Fnd_Dsc = localVariables.newFieldInRecord("pnd_Fnd_Dsc", "#FND-DSC", FieldType.STRING, 10);
        pnd_First_Time_Thru = localVariables.newFieldInRecord("pnd_First_Time_Thru", "#FIRST-TIME-THRU", FieldType.STRING, 1);
        pnd_Fund_Code_Hold = localVariables.newFieldInRecord("pnd_Fund_Code_Hold", "#FUND-CODE-HOLD", FieldType.STRING, 3);

        pnd_Fund_Code_Hold__R_Field_8 = localVariables.newGroupInRecord("pnd_Fund_Code_Hold__R_Field_8", "REDEFINE", pnd_Fund_Code_Hold);
        pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1 = pnd_Fund_Code_Hold__R_Field_8.newFieldInGroup("pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1", "#FUND-CODE-HOLD-1", 
            FieldType.STRING, 1);
        pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3 = pnd_Fund_Code_Hold__R_Field_8.newFieldInGroup("pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_2_3", "#FUND-CODE-HOLD-2-3", 
            FieldType.STRING, 2);
        pnd_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_Fund_Code_1_3", "#FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_Fund_Code_1_3__R_Field_9 = localVariables.newGroupInRecord("pnd_Fund_Code_1_3__R_Field_9", "REDEFINE", pnd_Fund_Code_1_3);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_1 = pnd_Fund_Code_1_3__R_Field_9.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3 = pnd_Fund_Code_1_3__R_Field_9.newFieldInGroup("pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3", "#FUND-CODE-2-3", FieldType.STRING, 
            2);
        pnd_Contract_Hold = localVariables.newFieldInRecord("pnd_Contract_Hold", "#CONTRACT-HOLD", FieldType.STRING, 10);

        pnd_Contract_Hold__R_Field_10 = localVariables.newGroupInRecord("pnd_Contract_Hold__R_Field_10", "REDEFINE", pnd_Contract_Hold);
        pnd_Contract_Hold_Pnd_H_Contract_1_7 = pnd_Contract_Hold__R_Field_10.newFieldInGroup("pnd_Contract_Hold_Pnd_H_Contract_1_7", "#H-CONTRACT-1-7", 
            FieldType.STRING, 7);
        pnd_Contract_Hold_Pnd_H_Contract_8 = pnd_Contract_Hold__R_Field_10.newFieldInGroup("pnd_Contract_Hold_Pnd_H_Contract_8", "#H-CONTRACT-8", FieldType.STRING, 
            1);
        pnd_Payee_Cde_Hold = localVariables.newFieldInRecord("pnd_Payee_Cde_Hold", "#PAYEE-CDE-HOLD", FieldType.STRING, 2);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cref_Fund.reset();

        ldaIaal586r.initializeValues();

        localVariables.reset();
        pnd_Len.setInitialValue(35);
        pnd_First_Time.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap586r() throws Exception
    {
        super("Iaap586r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().write(0, "*** START OF PROGRAM IAAP586R *** ");                                                                                                      //Natural: WRITE '*** START OF PROGRAM IAAP586R *** '
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, ldaIaal586r.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  START OF MAIN PROCESS
        //* ***********************************************************************
        pnd_First_Time_Thru.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #FIRST-TIME-THRU
        R1:                                                                                                                                                               //Natural: READ WORK FILE 2 #W2-FILE
        while (condition(getWorkFiles().read(2, pnd_W2_File)))
        {
            if (condition(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8.equals("99999999")))                                                                                     //Natural: IF #W2-OVRPYMNT-PPCN-NBR-8 = '99999999'
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //*      MOVE '444' TO #W2-FUND-CODE /* TEST
            pnd_Fund_Code_1_3.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                                     //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-1-3
            if (condition(pnd_First_Time_Thru.equals("Y")))                                                                                                               //Natural: IF #FIRST-TIME-THRU = 'Y'
            {
                pnd_First_Time_Thru.setValue("N");                                                                                                                        //Natural: MOVE 'N' TO #FIRST-TIME-THRU
                pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                                //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("T")))                                                                                             //Natural: IF #FUND-CODE-1 = 'T'
                {
                    //*          IGNORE
                                                                                                                                                                          //Natural: PERFORM #GET-DESC-TIAA
                    sub_Pnd_Get_Desc_Tiaa();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                    sub_Pnd_Get_Desc();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*      IF #W2-FUND-CODE-1 EQ 'T' AND #FUND-CODE-HOLD-1 EQ 'T'
            //*         IGNORE
            //*      ELSE
            if (condition(pnd_W2_File_Pnd_W2_Fund_Code.notEquals(pnd_Fund_Code_Hold)))                                                                                    //Natural: IF #W2-FUND-CODE NE #FUND-CODE-HOLD
            {
                if (condition(pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1.equals("T")))                                                                                       //Natural: IF #FUND-CODE-HOLD-1 = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-TIAA
                    sub_Pnd_Write_Totals_Tiaa();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Contract_Hold.setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                                     //Natural: MOVE #W2-OVRPYMNT-PPCN-NBR TO #CONTRACT-HOLD
                    pnd_Payee_Cde_Hold.setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                      //Natural: MOVE #W2-PAYEE-CDE-ALPHA TO #PAYEE-CDE-HOLD
                    pnd_M.reset();                                                                                                                                        //Natural: RESET #M
                    if (condition(getReports().getAstLinesLeft(1).less(7)))                                                                                               //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 7 LINES LEFT
                    {
                        getReports().newPage(1);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL GUARANTEED AMOUNT OVERPAID FOR",pnd_Fund_Desc,"===>",ldaIaal586r.getPnd_Grand_Rate_Guar_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL GUARANTEED AMOUNT OVERPAID FOR' #FUND-DESC '===>' #GRAND-RATE-GUAR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             'TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>'
                    //*                 #COMP-DESC
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL DIVIDEND AMOUNT OVERPAID FOR",pnd_Fund_Desc,new ColumnSpacing(3),"===>",ldaIaal586r.getPnd_Grand_Rate_Divd_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL DIVIDEND AMOUNT OVERPAID FOR' #FUND-DESC 3X '===>' #GRAND-RATE-DIVD-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             'TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>'
                    //*                 #COMP-DESC
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Fund_Desc,new ColumnSpacing(9),"===>",ldaIaal586r.getPnd_Grand_Tot_Ovrpy_Tiaa(),  //Natural: WRITE ( 1 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #FUND-DESC 9X '===>' #GRAND-TOT-OVRPY-TIAA ( EM = ZZZ,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             'TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>'
                    //*                 #COMP-DESC
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new               //Natural: WRITE ( 1 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                        RepeatItem(59));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "# OF CONTRACTS FOR",pnd_Fund_Desc,"===> ",ldaIaal586r.getPnd_Num_Of_Tiaa());                                                   //Natural: WRITE '# OF CONTRACTS FOR' #FUND-DESC '===> ' #NUM-OF-TIAA
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*          WRITE '  # OF TIAA TRADITIONAL CONTRACTS => ' #NUM-OF-TIAA
                    pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                            //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                    if (condition(pnd_W2_File_Pnd_W2_Fund_Code_1.equals("T")))                                                                                            //Natural: IF #W2-FUND-CODE-1 EQ 'T'
                    {
                                                                                                                                                                          //Natural: PERFORM #GET-DESC-TIAA
                        sub_Pnd_Get_Desc_Tiaa();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*           WRITE '=' #W2-OVRPYMNT-PPCN-NBR '=' #W2-FUND-CODE-1
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                        sub_Pnd_Get_Desc();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-CREF
                    sub_Pnd_Write_Totals_Cref();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Contract_Hold.setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                                     //Natural: MOVE #W2-OVRPYMNT-PPCN-NBR TO #CONTRACT-HOLD
                    pnd_Payee_Cde_Hold.setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                      //Natural: MOVE #W2-PAYEE-CDE-ALPHA TO #PAYEE-CDE-HOLD
                    pnd_N.reset();                                                                                                                                        //Natural: RESET #N
                    if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                               //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 5 LINES LEFT
                    {
                        getReports().newPage(2);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Comp_Desc,pnd_W_Fund_Desc,"===>",pnd_Grand_Tot_Ovrpy,           //Natural: WRITE ( 2 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #COMP-DESC #W-FUND-DESC '===>' #GRAND-TOT-OVRPY ( EM = ZZZ,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*                  #COMP-DESC #FUND-DESC #FND-DSC '===>'
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new               //Natural: WRITE ( 2 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                        RepeatItem(59));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "# OF CONTRACTS FOR",pnd_Comp_Desc,pnd_W_Fund_Desc,"===> ",pnd_Num_Of_Fund);                                                    //Natural: WRITE '# OF CONTRACTS FOR' #COMP-DESC #W-FUND-DESC '===> ' #NUM-OF-FUND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*                #COMP-DESC #FUND-DESC #FND-DSC '===> ' #NUM-OF-FUND
                    pnd_Fund_Code_Hold.setValue(pnd_W2_File_Pnd_W2_Fund_Code);                                                                                            //Natural: MOVE #W2-FUND-CODE TO #FUND-CODE-HOLD
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                    sub_Pnd_Get_Desc();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Grand_Tot_Ovrpy.reset();                                                                                                                              //Natural: RESET #GRAND-TOT-OVRPY #NUM-OF-FUND #WRITE-OUT-CONTRACT #GRAND-RATE-GUAR-OVP #GRAND-RATE-DIVD-OVP #GRAND-TOT-OVRPY-TIAA #NUM-OF-TIAA
                pnd_Num_Of_Fund.reset();
                pnd_Write_Out_Contract.reset();
                ldaIaal586r.getPnd_Grand_Rate_Guar_Ovp().reset();
                ldaIaal586r.getPnd_Grand_Rate_Divd_Ovp().reset();
                ldaIaal586r.getPnd_Grand_Tot_Ovrpy_Tiaa().reset();
                ldaIaal586r.getPnd_Num_Of_Tiaa().reset();
            }                                                                                                                                                             //Natural: END-IF
            //*     END-IF
            //*      WRITE '=' OVRPYMNT-PPCN-NBR '=' #W-FUND-CODE-TOT
            ldaIaal586r.getPnd_W_Contract().setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                               //Natural: MOVE #W2-OVRPYMNT-PPCN-NBR TO #W-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-TYPE-OF-CONTRACT
            sub_Pnd_Determine_Type_Of_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!(ldaIaal586r.getPnd_Contract_Type_Field().notEquals(" "))))                                                                                    //Natural: ACCEPT IF #CONTRACT-TYPE-FIELD NOT = ' '
            {
                continue;
            }
            //*      MOVE OVRPYMNT-LAST-PYMNT-RCVD-DATE TO #W-LAST-PAY-DATE
            //*       MOVE OVRPYMNT-PYMNT-MODE TO #W-PYMNT-MODE
            ldaIaal586r.getPnd_W_Pymnt_Mode().setValue(pnd_W2_File_Pnd_W2_Mode);                                                                                          //Natural: MOVE #W2-MODE TO #W-PYMNT-MODE
            ldaIaal586r.getPnd_W_Payee_Cde().setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                //Natural: MOVE #W2-PAYEE-CDE-ALPHA TO #W-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM #DETERMINE-REPORT
            sub_Pnd_Determine_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
            sub_Pnd_Reset_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Write_Out_Contract.greater(getZero())))                                                                                                         //Natural: IF #WRITE-OUT-CONTRACT > 0
        {
            if (condition(pnd_Fund_Code_Hold_Pnd_Fund_Code_Hold_1.equals("T")))                                                                                           //Natural: IF #FUND-CODE-HOLD-1 = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-TIAA
                sub_Pnd_Write_Totals_Tiaa();
                if (condition(Global.isEscape())) {return;}
                if (condition(getReports().getAstLinesLeft(1).less(7)))                                                                                                   //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 7 LINES LEFT
                {
                    getReports().newPage(1);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL GUARANTEED AMOUNT OVERPAID FOR",pnd_Fund_Desc,"===>",ldaIaal586r.getPnd_Grand_Rate_Guar_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL GUARANTEED AMOUNT OVERPAID FOR' #FUND-DESC '===>' #GRAND-RATE-GUAR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*         'TOTAL GUARANTEED AMOUNT OVERPAID FOR TIAA TRADITIONAL ==>'
                //*                 #COMP-DESC
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL DIVIDEND AMOUNT OVERPAID FOR",pnd_Fund_Desc,new ColumnSpacing(3),"===>",ldaIaal586r.getPnd_Grand_Rate_Divd_Ovp(),  //Natural: WRITE ( 1 ) / 'TOTAL DIVIDEND AMOUNT OVERPAID FOR' #FUND-DESC 3X '===>' #GRAND-RATE-DIVD-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*         'TOTAL DIVIDEND AMOUNT OVERPAID FOR TIAA TRADITIONAL ====>'
                //*                 #COMP-DESC
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Fund_Desc,new ColumnSpacing(9),"===>",ldaIaal586r.getPnd_Grand_Tot_Ovrpy_Tiaa(),  //Natural: WRITE ( 1 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #FUND-DESC 9X '===>' #GRAND-TOT-OVRPY-TIAA ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*         'TOTAL OVERPAYMENT AMOUNT FOR TIAA TRADITIONAL ==========>'
                //*                 #COMP-DESC
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new                   //Natural: WRITE ( 1 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                    RepeatItem(59));
                if (Global.isEscape()) return;
                getReports().write(0, "# OF CONTRACTS FOR",pnd_Fund_Desc,"===> ",ldaIaal586r.getPnd_Num_Of_Tiaa());                                                       //Natural: WRITE '# OF CONTRACTS FOR' #FUND-DESC '===> ' #NUM-OF-TIAA
                if (Global.isEscape()) return;
                //*      WRITE '  # OF TIAA TRADITIONAL CONTRACTS => ' #NUM-OF-TIAA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-CREF
                sub_Pnd_Write_Totals_Cref();
                if (condition(Global.isEscape())) {return;}
                if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                                   //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 5 LINES LEFT
                {
                    getReports().newPage(2);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL OVERPAYMENT AMOUNT FOR",pnd_Comp_Desc,pnd_W_Fund_Desc,"===>",pnd_Grand_Tot_Ovrpy,               //Natural: WRITE ( 2 ) / 'TOTAL OVERPAYMENT AMOUNT FOR' #COMP-DESC #W-FUND-DESC '===>' #GRAND-TOT-OVRPY ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*                  #COMP-DESC #FUND-DESC #FND-DSC '===>'
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new                   //Natural: WRITE ( 2 ) / '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
                    RepeatItem(59));
                if (Global.isEscape()) return;
                getReports().write(0, "# OF CONTRACTS FOR",pnd_Comp_Desc,pnd_W_Fund_Desc,"===> ",pnd_Num_Of_Fund);                                                        //Natural: WRITE '# OF CONTRACTS FOR' #COMP-DESC #W-FUND-DESC '===> ' #NUM-OF-FUND
                if (Global.isEscape()) return;
                //*               #COMP-DESC #FUND-DESC #FND-DSC '===> ' #NUM-OF-FUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "*** END OF PROGRAM IAAP586R *** ");                                                                                                        //Natural: WRITE '*** END OF PROGRAM IAAP586R *** '
        if (Global.isEscape()) return;
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DESC
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DESC-TIAA
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* *Y2NCTS
        //*      #C-RATE-GUAR #C-RATE-DIVD
        //*      #C-RATE-GUAR-OVP #C-RATE-DIVD-OVP #C-RATE-OVRPYMNT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-REPORT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-CONTRACT
        //* *Y2NCTS
        //* *Y2NCTS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-TIAA
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CREF-CONTRACT
        //* *Y2NCTS
        //* *Y2NCTS
        //* *Y2NCTS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-CREF
        //* *Y2NCTS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DETERMINE-TYPE-OF-CONTRACT
        //* *******************************************************************
        //* *Y2NCTS
        //* *Y2NCTS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #OVERPAY-BREAKDOWN
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Get_Desc() throws Exception                                                                                                                      //Natural: #GET-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_Desc.reset();                                                                                                                                            //Natural: RESET #FUND-DESC #COMP-DESC #W-FUND-DESC
        pnd_Comp_Desc.reset();
        pnd_W_Fund_Desc.reset();
        //*  WRITE 'BEFORE CALL' '=' #FUND-CODE-2-3 '=' #FUND-DESC '=' #LEN
        //* * CALLNAT 'IAAN050A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("2") || pnd_Fund_Code_1_3_Pnd_Fund_Code_1.equals("U")))                                                    //Natural: IF #FUND-CODE-1 = '2' OR = 'U'
        {
            pnd_Fnd_Dsc.setValue(" - ANNUAL");                                                                                                                            //Natural: MOVE ' - ANNUAL' TO #FND-DSC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fnd_Dsc.setValue(" - MONTHLY");                                                                                                                           //Natural: MOVE ' - MONTHLY' TO #FND-DSC
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Fund_Desc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fund_Desc, pnd_Fnd_Dsc));                                                            //Natural: COMPRESS #FUND-DESC #FND-DSC INTO #W-FUND-DESC LEAVING NO
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 2 ) ' '
        if (Global.isEscape()) return;
        //*  WRITE 'AFTER CALL' '=' #FUND-CODE-2-3 '=' #FUND-DESC
    }
    private void sub_Pnd_Get_Desc_Tiaa() throws Exception                                                                                                                 //Natural: #GET-DESC-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_Desc.reset();                                                                                                                                            //Natural: RESET #FUND-DESC #COMP-DESC
        pnd_Comp_Desc.reset();
        //* * CALLNAT 'IAAN050A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Code_1_3_Pnd_Fund_Code_2_3, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2-3 #FUND-DESC #COMP-DESC #LEN
        if (condition(Global.isEscape())) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal586r.getPnd_Gen_Sub().reset();                                                                                                                             //Natural: RESET #GEN-SUB #ERROR #W-CONTRACT #W-PYMNT-MODE #W-PAYEE-CDE #W-OPTION-CDE-DESC #W-PAY-DATE ( * ) #W-RATE-GROSS ( * ) #W-RATE-OVRPY ( * ) #T-RATE-OVRPY #W-RATE-GUAR ( * ) #W-RATE-DIVD ( * ) #TEMP-OCCUR #TEMP-OCCUR-2 #OVP-CNT #TEMP-OCCUR-3 #T-CONTRACT #T-PAYEE-CDE #C-CONTRACT #C-PAYEE-CDE
        pnd_Error.reset();
        ldaIaal586r.getPnd_W_Contract().reset();
        ldaIaal586r.getPnd_W_Pymnt_Mode().reset();
        ldaIaal586r.getPnd_W_Payee_Cde().reset();
        ldaIaal586r.getPnd_W_Option_Cde_Desc().reset();
        ldaIaal586r.getPnd_W_Pay_Date().getValue("*").reset();
        ldaIaal586r.getPnd_W_Rate_Gross().getValue("*").reset();
        ldaIaal586r.getPnd_W_Rate_Ovrpy().getValue("*").reset();
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy().reset();
        ldaIaal586r.getPnd_W_Rate_Guar().getValue("*").reset();
        ldaIaal586r.getPnd_W_Rate_Divd().getValue("*").reset();
        ldaIaal586r.getPnd_Temp_Occur().reset();
        ldaIaal586r.getPnd_Temp_Occur_2().reset();
        ldaIaal586r.getPnd_Ovp_Cnt().reset();
        ldaIaal586r.getPnd_Temp_Occur_3().reset();
        ldaIaal586r.getPnd_T_Record_Pnd_T_Contract().reset();
        ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde().reset();
        ldaIaal586r.getPnd_C_Record_Pnd_C_Contract().reset();
        ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde().reset();
        //*      #CNT-RATE-GROSS #CNT-RATE-OVRPY
        pnd_W2_File.reset();                                                                                                                                              //Natural: RESET #W2-FILE
    }
    private void sub_Pnd_Determine_Report() throws Exception                                                                                                              //Natural: #DETERMINE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(pnd_First_Time.getBoolean()))                                                                                                                       //Natural: IF #FIRST-TIME
        {
            pnd_Contract_Hold.setValue(ldaIaal586r.getPnd_W_Contract());                                                                                                  //Natural: MOVE #W-CONTRACT TO #CONTRACT-HOLD
            pnd_Payee_Cde_Hold.setValue(ldaIaal586r.getPnd_W_Payee_Cde());                                                                                                //Natural: MOVE #W-PAYEE-CDE TO #PAYEE-CDE-HOLD
            pnd_First_Time.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #FIRST-TIME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Write_Out_Contract.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WRITE-OUT-CONTRACT
        if (condition(DbsUtil.maskMatches(ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") && ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z")            //Natural: IF #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z' AND #W2-FUND-CODE-1 EQ 'T'
            && pnd_W2_File_Pnd_W2_Fund_Code_1.equals("T")))
        {
                                                                                                                                                                          //Natural: PERFORM #TIAA-CONTRACT
            sub_Pnd_Tiaa_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CREF-CONTRACT
            sub_Pnd_Cref_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Tiaa_Contract() throws Exception                                                                                                                 //Natural: #TIAA-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(ldaIaal586r.getPnd_W_Contract().notEquals(pnd_Contract_Hold) || ldaIaal586r.getPnd_W_Payee_Cde().notEquals(pnd_Payee_Cde_Hold)))                    //Natural: IF #W-CONTRACT NE #CONTRACT-HOLD OR #W-PAYEE-CDE NE #PAYEE-CDE-HOLD
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-TIAA
            sub_Pnd_Write_Totals_Tiaa();
            if (condition(Global.isEscape())) {return;}
            pnd_Contract_Hold.setValue(ldaIaal586r.getPnd_W_Contract());                                                                                                  //Natural: MOVE #W-CONTRACT TO #CONTRACT-HOLD
            pnd_Payee_Cde_Hold.setValue(ldaIaal586r.getPnd_W_Payee_Cde());                                                                                                //Natural: MOVE #W-PAYEE-CDE TO #PAYEE-CDE-HOLD
            pnd_M.reset();                                                                                                                                                //Natural: RESET #M
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #M
        ldaIaal586r.getPnd_T_Record_Pnd_T_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #T-CONTRACT LEAVING NO
            "-", ldaIaal586r.getPnd_W_Contract_Pnd_W_Contract_8()));
        //*  MOVE #W-CONTRACT        TO #T-CONTRACT
        ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde().setValue(ldaIaal586r.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #T-PAYEE-CDE
        ldaIaal586r.getPnd_T_Record_Pnd_T_Option_Cde_Desc().setValue(pnd_W2_File_Pnd_W2_Desc);                                                                            //Natural: MOVE #W2-DESC TO #T-OPTION-CDE-DESC
        ldaIaal586r.getPnd_T_Record_Pnd_T_Pymnt_Mode().setValue(ldaIaal586r.getPnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A());                                                    //Natural: MOVE #W-PYMNT-MODE-A TO #T-PYMNT-MODE
        //* *Y2NCTS
        ldaIaal586r.getPnd_Ws_Date_Nn().setValue(pnd_W2_File_Pnd_W2_Date);                                                                                                //Natural: MOVE #W2-DATE TO #WS-DATE-NN
        //* *Y2NCTS
        ldaIaal586r.getPnd_T_Record_Pnd_T_Pay_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Mm(),             //Natural: COMPRESS #WS-DATE-MM '/' #WS-DATE-DD '/' #WS-DATE-CCYY INTO #T-PAY-DATE LEAVING NO
            "/", ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Dd(), "/", ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Ccyy()));
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Guar().setValue(pnd_W2_File_Pnd_W2_Guar_Payment);                                                                          //Natural: MOVE #W2-GUAR-PAYMENT TO #T-RATE-GUAR
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Divd().setValue(pnd_W2_File_Pnd_W2_Divd_Payment);                                                                          //Natural: MOVE #W2-DIVD-PAYMENT TO #T-RATE-DIVD
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Guar_Ovp().setValue(pnd_W2_File_Pnd_W2_Guar_Overpayment);                                                                  //Natural: MOVE #W2-GUAR-OVERPAYMENT TO #T-RATE-GUAR-OVP
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Divd_Ovp().setValue(pnd_W2_File_Pnd_W2_Divd_Overpayment);                                                                  //Natural: MOVE #W2-DIVD-OVERPAYMENT TO #T-RATE-DIVD-OVP
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy().compute(new ComputeParameters(false, ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy()), pnd_W2_File_Pnd_W2_Guar_Overpayment.add(pnd_W2_File_Pnd_W2_Divd_Overpayment)); //Natural: COMPUTE #T-RATE-OVRPY = #W2-GUAR-OVERPAYMENT + #W2-DIVD-OVERPAYMENT
        ldaIaal586r.getPnd_T_Record_Pnd_T_Msg().setValue(pnd_W2_File_Pnd_W2_Msg);                                                                                         //Natural: MOVE #W2-MSG TO #T-MSG
        if (condition(pnd_M.equals(1)))                                                                                                                                   //Natural: IF #M = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal586r.getPnd_T_Record_Pnd_T_Contract().reset();                                                                                                         //Natural: RESET #T-CONTRACT #T-PAYEE-CDE #T-OPTION-CDE-DESC #T-PYMNT-MODE
            ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde().reset();
            ldaIaal586r.getPnd_T_Record_Pnd_T_Option_Cde_Desc().reset();
            ldaIaal586r.getPnd_T_Record_Pnd_T_Pymnt_Mode().reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*     RESET #T-RATE-OVRPY
        if (condition(pnd_W2_File_Pnd_W2_Msg.equals(" ")))                                                                                                                //Natural: IF #W2-MSG = ' '
        {
            ldaIaal586r.getPnd_C_Rate_Guar().nadd(pnd_W2_File_Pnd_W2_Guar_Payment);                                                                                       //Natural: ADD #W2-GUAR-PAYMENT TO #C-RATE-GUAR
            ldaIaal586r.getPnd_C_Rate_Divd().nadd(pnd_W2_File_Pnd_W2_Divd_Payment);                                                                                       //Natural: ADD #W2-DIVD-PAYMENT TO #C-RATE-DIVD
            ldaIaal586r.getPnd_C_Rate_Guar_Ovp().nadd(pnd_W2_File_Pnd_W2_Guar_Overpayment);                                                                               //Natural: ADD #W2-GUAR-OVERPAYMENT TO #C-RATE-GUAR-OVP
            ldaIaal586r.getPnd_C_Rate_Divd_Ovp().nadd(pnd_W2_File_Pnd_W2_Divd_Overpayment);                                                                               //Natural: ADD #W2-DIVD-OVERPAYMENT TO #C-RATE-DIVD-OVP
            ldaIaal586r.getPnd_C_Rate_Ovrpymnt().nadd(ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy());                                                                    //Natural: ADD #T-RATE-OVRPY TO #C-RATE-OVRPYMNT
            ldaIaal586r.getPnd_Grand_Rate_Guar_Ovp().nadd(pnd_W2_File_Pnd_W2_Guar_Overpayment);                                                                           //Natural: ADD #W2-GUAR-OVERPAYMENT TO #GRAND-RATE-GUAR-OVP
            ldaIaal586r.getPnd_Grand_Rate_Divd_Ovp().nadd(pnd_W2_File_Pnd_W2_Divd_Overpayment);                                                                           //Natural: ADD #W2-DIVD-OVERPAYMENT TO #GRAND-RATE-DIVD-OVP
            ldaIaal586r.getPnd_Grand_Tot_Ovrpy_Tiaa().nadd(ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy());                                                               //Natural: ADD #T-RATE-OVRPY TO #GRAND-TOT-OVRPY-TIAA
        }                                                                                                                                                                 //Natural: END-IF
        //*    PERFORM #OVERPAY-BREAKDOWN
        //*      WRITE (1) #T-RECORD
        getReports().write(1, ReportOption.NOTITLE,ldaIaal586r.getPnd_T_Record_Pnd_T_Contract(),new TabSetting(12),ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde(),new      //Natural: WRITE ( 1 ) #T-CONTRACT 012T #T-PAYEE-CDE 017T #T-OPTION-CDE-DESC 040T #T-PYMNT-MODE 046T #T-PAY-DATE 058T #T-RATE-GUAR ( EM = Z,ZZZ,ZZ9.99 ) 072T #T-RATE-DIVD ( EM = Z,ZZZ,ZZ9.99 ) 086T #T-RATE-GUAR-OVP ( EM = Z,ZZZ,ZZ9.99 ) 101T #T-RATE-DIVD-OVP ( EM = Z,ZZZ,ZZ9.99 ) 115T #T-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 ) 130T #T-MSG
            TabSetting(17),ldaIaal586r.getPnd_T_Record_Pnd_T_Option_Cde_Desc(),new TabSetting(40),ldaIaal586r.getPnd_T_Record_Pnd_T_Pymnt_Mode(),new TabSetting(46),ldaIaal586r.getPnd_T_Record_Pnd_T_Pay_Date(),new 
            TabSetting(58),ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Guar(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(72),ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Divd(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(86),ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Guar_Ovp(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
            TabSetting(101),ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Divd_Ovp(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(115),ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Ovrpy(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(130),ldaIaal586r.getPnd_T_Record_Pnd_T_Msg());
        if (Global.isEscape()) return;
        ldaIaal586r.getPnd_T_Record().reset();                                                                                                                            //Natural: RESET #T-RECORD
    }
    private void sub_Pnd_Write_Totals_Tiaa() throws Exception                                                                                                             //Natural: #WRITE-TOTALS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  SKIP (1) 1
        ldaIaal586r.getPnd_T_Record_Pnd_T_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Hold_Pnd_H_Contract_1_7, "-",                  //Natural: COMPRESS #H-CONTRACT-1-7 '-' #H-CONTRACT-8 INTO #T-CONTRACT LEAVING NO
            pnd_Contract_Hold_Pnd_H_Contract_8));
        ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde().setValue(pnd_Payee_Cde_Hold);                                                                                       //Natural: MOVE #PAYEE-CDE-HOLD TO #T-PAYEE-CDE
        getReports().write(1, ReportOption.NOTITLE,ldaIaal586r.getPnd_T_Record_Pnd_T_Contract(),new TabSetting(12),ldaIaal586r.getPnd_T_Record_Pnd_T_Payee_Cde(),new      //Natural: WRITE ( 1 ) #T-CONTRACT 012T #T-PAYEE-CDE 021T '-> TOTALS <-' 058T #C-RATE-GUAR ( EM = Z,ZZZ,ZZ9.99 ) 072T #C-RATE-DIVD ( EM = Z,ZZZ,ZZ9.99 ) 086T #C-RATE-GUAR-OVP ( EM = Z,ZZZ,ZZ9.99 ) 101T #C-RATE-DIVD-OVP ( EM = Z,ZZZ,ZZ9.99 ) 115T #C-RATE-OVRPYMNT ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(21),"-> TOTALS <-",new TabSetting(58),ldaIaal586r.getPnd_C_Rate_Guar(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(72),ldaIaal586r.getPnd_C_Rate_Divd(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(86),ldaIaal586r.getPnd_C_Rate_Guar_Ovp(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(101),ldaIaal586r.getPnd_C_Rate_Divd_Ovp(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(115),ldaIaal586r.getPnd_C_Rate_Ovrpymnt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        ldaIaal586r.getPnd_Num_Of_Tiaa().nadd(1);                                                                                                                         //Natural: ADD 1 TO #NUM-OF-TIAA
        ldaIaal586r.getPnd_C_Rate_Guar().reset();                                                                                                                         //Natural: RESET #C-RATE-GUAR #C-RATE-DIVD #C-RATE-GUAR-OVP #C-RATE-DIVD-OVP #C-RATE-OVRPYMNT
        ldaIaal586r.getPnd_C_Rate_Divd().reset();
        ldaIaal586r.getPnd_C_Rate_Guar_Ovp().reset();
        ldaIaal586r.getPnd_C_Rate_Divd_Ovp().reset();
        ldaIaal586r.getPnd_C_Rate_Ovrpymnt().reset();
    }
    private void sub_Pnd_Cref_Contract() throws Exception                                                                                                                 //Natural: #CREF-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*   WRITE 'ENTERING #CREF-CONTRACT'
        if (condition(ldaIaal586r.getPnd_W_Contract().notEquals(pnd_Contract_Hold) || ldaIaal586r.getPnd_W_Payee_Cde().notEquals(pnd_Payee_Cde_Hold)))                    //Natural: IF #W-CONTRACT NE #CONTRACT-HOLD OR #W-PAYEE-CDE NE #PAYEE-CDE-HOLD
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-CREF
            sub_Pnd_Write_Totals_Cref();
            if (condition(Global.isEscape())) {return;}
            pnd_Contract_Hold.setValue(ldaIaal586r.getPnd_W_Contract());                                                                                                  //Natural: MOVE #W-CONTRACT TO #CONTRACT-HOLD
            pnd_Payee_Cde_Hold.setValue(ldaIaal586r.getPnd_W_Payee_Cde());                                                                                                //Natural: MOVE #W-PAYEE-CDE TO #PAYEE-CDE-HOLD
            pnd_N.reset();                                                                                                                                                //Natural: RESET #N
        }                                                                                                                                                                 //Natural: END-IF
        pnd_N.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #N
        ldaIaal586r.getPnd_C_Record_Pnd_C_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getPnd_W_Contract_Pnd_W_Contract_1_7(),         //Natural: COMPRESS #W-CONTRACT-1-7 '-' #W-CONTRACT-8 INTO #C-CONTRACT LEAVING NO
            "-", ldaIaal586r.getPnd_W_Contract_Pnd_W_Contract_8()));
        ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde().setValue(ldaIaal586r.getPnd_W_Payee_Cde());                                                                         //Natural: MOVE #W-PAYEE-CDE TO #C-PAYEE-CDE
        ldaIaal586r.getPnd_C_Record_Pnd_C_Option_Cde_Desc().setValue(pnd_W2_File_Pnd_W2_Desc);                                                                            //Natural: MOVE #W2-DESC TO #C-OPTION-CDE-DESC
        ldaIaal586r.getPnd_C_Record_Pnd_C_Pymnt_Mode().setValue(ldaIaal586r.getPnd_W_Pymnt_Mode_Pnd_W_Pymnt_Mode_A());                                                    //Natural: MOVE #W-PYMNT-MODE-A TO #C-PYMNT-MODE
        //*  MOVE #W2-DATE             TO #C-PAY-DATE
        //* *Y2NCTS
        ldaIaal586r.getPnd_Ws_Date_Nn().setValue(pnd_W2_File_Pnd_W2_Date);                                                                                                //Natural: MOVE #W2-DATE TO #WS-DATE-NN
        //* *Y2NCTS
        ldaIaal586r.getPnd_C_Record_Pnd_C_Pay_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Mm(),             //Natural: COMPRESS #WS-DATE-MM '/' #WS-DATE-DD '/' #WS-DATE-CCYY INTO #C-PAY-DATE LEAVING NO
            "/", ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Dd(), "/", ldaIaal586r.getPnd_Ws_Date_Nn_Pnd_Ws_Date_Ccyy()));
        ldaIaal586r.getPnd_C_Record_Pnd_C_Rate_Gross().setValue(pnd_W2_File_Pnd_W2_Unit_Payment);                                                                         //Natural: MOVE #W2-UNIT-PAYMENT TO #C-RATE-GROSS
        ldaIaal586r.getPnd_C_Record_Pnd_C_Rate_Ovrpy().setValue(pnd_W2_File_Pnd_W2_Unit_Overpayment);                                                                     //Natural: MOVE #W2-UNIT-OVERPAYMENT TO #C-RATE-OVRPY
        ldaIaal586r.getPnd_C_Record_Pnd_C_Msg().setValue(pnd_W2_File_Pnd_W2_Msg);                                                                                         //Natural: MOVE #W2-MSG TO #C-MSG
        if (condition(pnd_N.equals(1)))                                                                                                                                   //Natural: IF #N = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal586r.getPnd_C_Record_Pnd_C_Contract().reset();                                                                                                         //Natural: RESET #C-CONTRACT #C-PAYEE-CDE #C-OPTION-CDE-DESC #C-PYMNT-MODE
            ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde().reset();
            ldaIaal586r.getPnd_C_Record_Pnd_C_Option_Cde_Desc().reset();
            ldaIaal586r.getPnd_C_Record_Pnd_C_Pymnt_Mode().reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W2_File_Pnd_W2_Msg.equals(" ")))                                                                                                                //Natural: IF #W2-MSG = ' '
        {
            ldaIaal586r.getPnd_Cnt_Rate_Gross().nadd(pnd_W2_File_Pnd_W2_Unit_Payment);                                                                                    //Natural: ADD #W2-UNIT-PAYMENT TO #CNT-RATE-GROSS
            ldaIaal586r.getPnd_Cnt_Rate_Ovrpy().nadd(pnd_W2_File_Pnd_W2_Unit_Overpayment);                                                                                //Natural: ADD #W2-UNIT-OVERPAYMENT TO #CNT-RATE-OVRPY
            pnd_Grand_Tot_Ovrpy.nadd(pnd_W2_File_Pnd_W2_Unit_Overpayment);                                                                                                //Natural: ADD #W2-UNIT-OVERPAYMENT TO #GRAND-TOT-OVRPY
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,ldaIaal586r.getPnd_C_Record_Pnd_C_Contract(),new TabSetting(20),ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde(),new      //Natural: WRITE ( 2 ) #C-CONTRACT 020T #C-PAYEE-CDE 030T #C-OPTION-CDE-DESC 056T #C-PYMNT-MODE 071T #C-PAY-DATE 090T #C-RATE-GROSS ( EM = Z,ZZZ,ZZ9.99 ) 110T #C-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 ) 125T #C-MSG
            TabSetting(30),ldaIaal586r.getPnd_C_Record_Pnd_C_Option_Cde_Desc(),new TabSetting(56),ldaIaal586r.getPnd_C_Record_Pnd_C_Pymnt_Mode(),new TabSetting(71),ldaIaal586r.getPnd_C_Record_Pnd_C_Pay_Date(),new 
            TabSetting(90),ldaIaal586r.getPnd_C_Record_Pnd_C_Rate_Gross(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(110),ldaIaal586r.getPnd_C_Record_Pnd_C_Rate_Ovrpy(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(125),ldaIaal586r.getPnd_C_Record_Pnd_C_Msg());
        if (Global.isEscape()) return;
        //*       PERFORM #DECIDE-CREF-TYPE-1
        ldaIaal586r.getPnd_C_Record().reset();                                                                                                                            //Natural: RESET #C-RECORD
    }
    private void sub_Pnd_Write_Totals_Cref() throws Exception                                                                                                             //Natural: #WRITE-TOTALS-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  WRITE 'ENTERING WRITE-TOTALS-CREF'
        ldaIaal586r.getPnd_C_Record_Pnd_C_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Hold_Pnd_H_Contract_1_7, "-",                  //Natural: COMPRESS #H-CONTRACT-1-7 '-' #H-CONTRACT-8 INTO #C-CONTRACT LEAVING NO
            pnd_Contract_Hold_Pnd_H_Contract_8));
        ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde().setValue(pnd_Payee_Cde_Hold);                                                                                       //Natural: MOVE #PAYEE-CDE-HOLD TO #C-PAYEE-CDE
        getReports().write(2, ReportOption.NOTITLE,ldaIaal586r.getPnd_C_Record_Pnd_C_Contract(),new TabSetting(20),ldaIaal586r.getPnd_C_Record_Pnd_C_Payee_Cde(),new      //Natural: WRITE ( 2 ) #C-CONTRACT 020T #C-PAYEE-CDE 034T '-> TOTALS <-' 090T #CNT-RATE-GROSS ( EM = Z,ZZZ,ZZ9.99 ) 110T #CNT-RATE-OVRPY ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(34),"-> TOTALS <-",new TabSetting(90),ldaIaal586r.getPnd_Cnt_Rate_Gross(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(110),ldaIaal586r.getPnd_Cnt_Rate_Ovrpy(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 2 ) 3
        pnd_Num_Of_Fund.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #NUM-OF-FUND
        ldaIaal586r.getPnd_Cnt_Rate_Gross().reset();                                                                                                                      //Natural: RESET #CNT-RATE-GROSS #CNT-RATE-OVRPY
        ldaIaal586r.getPnd_Cnt_Rate_Ovrpy().reset();
        //*  SKIP (1) 1
    }
    private void sub_Pnd_Determine_Type_Of_Contract() throws Exception                                                                                                    //Natural: #DETERMINE-TYPE-OF-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal586r.getVw_iaa_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) IAA-CNTRCT-VIEW CNTRCT-PPCN-NBR = #W-CONTRACT
        (
        "FN1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal586r.getPnd_W_Contract(), WcType.WITH) },
        1
        );
        FN1:
        while (condition(ldaIaal586r.getVw_iaa_Cntrct_View().readNextRow("FN1")))
        {
            ldaIaal586r.getVw_iaa_Cntrct_View().setIfNotFoundControlFlag(false);
            //* *Y2NCTS
            ldaIaal586r.getPnd_W_Contract_Issue_Dte().setValue(ldaIaal586r.getIaa_Cntrct_View_Cntrct_Issue_Dte());                                                        //Natural: MOVE CNTRCT-ISSUE-DTE TO #W-CONTRACT-ISSUE-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        short decideConditionsMet855 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT-VIEW.CNTRCT-ISSUE-DTE >= 199104 AND ( #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z' ) AND IAA-CNTRCT-VIEW.CNTRCT-OPTN-CDE = 21
        if (condition(ldaIaal586r.getIaa_Cntrct_View_Cntrct_Issue_Dte().greaterOrEqual(199104) && DbsUtil.maskMatches(ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") 
            && ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z") && ldaIaal586r.getIaa_Cntrct_View_Cntrct_Optn_Cde().equals(21)))
        {
            decideConditionsMet855++;
            ldaIaal586r.getPnd_Contract_Type_Field().setValue("A");                                                                                                       //Natural: MOVE 'A' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN #W-CNTRCT-1 = MASK ( A ) AND #W-CNTRCT-1 NE 'Z'
        else if (condition(DbsUtil.maskMatches(ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1(),"A") && ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1().notEquals("Z")))
        {
            decideConditionsMet855++;
            ldaIaal586r.getPnd_Contract_Type_Field().setValue("T");                                                                                                       //Natural: MOVE 'T' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN #W-CNTRCT-1 = MASK ( N ) OR #W-CNTRCT-1 EQ 'Z'
        else if (condition(DbsUtil.maskMatches(ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1(),"N") || ldaIaal586r.getPnd_W_Contract_Pnd_W_Cntrct_1().equals("Z")))
        {
            decideConditionsMet855++;
            ldaIaal586r.getPnd_Contract_Type_Field().setValue("C");                                                                                                       //Natural: MOVE 'C' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*    WRITE #W-CONTRACT ' DOES NOT FIT INTO TEACHERS OR CREF CATEGORY'
            ldaIaal586r.getPnd_Contract_Type_Field().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #CONTRACT-TYPE-FIELD
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Overpay_Breakdown() throws Exception                                                                                                             //Natural: #OVERPAY-BREAKDOWN
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal586r.getPnd_Ovrpy_Tot().setValue(ldaIaal586r.getPnd_W_Rate_Ovrpy().getValue(ldaIaal586r.getPnd_R()));                                                      //Natural: MOVE #W-RATE-OVRPY ( #R ) TO #OVRPY-TOT
        ldaIaal586r.getPnd_Rate_Guar_Ovp().setValue(ldaIaal586r.getPnd_W_Rate_Guar().getValue(ldaIaal586r.getPnd_R()));                                                   //Natural: MOVE #W-RATE-GUAR ( #R ) TO #RATE-GUAR-OVP
        ldaIaal586r.getPnd_Rate_Divd_Ovp().setValue(ldaIaal586r.getPnd_W_Rate_Divd().getValue(ldaIaal586r.getPnd_R()));                                                   //Natural: MOVE #W-RATE-DIVD ( #R ) TO #RATE-DIVD-OVP
        ldaIaal586r.getPnd_Tot_Guar_Divd().compute(new ComputeParameters(false, ldaIaal586r.getPnd_Tot_Guar_Divd()), ldaIaal586r.getPnd_Rate_Guar_Ovp().add(ldaIaal586r.getPnd_Rate_Divd_Ovp())); //Natural: COMPUTE #TOT-GUAR-DIVD = #RATE-GUAR-OVP + #RATE-DIVD-OVP
        ldaIaal586r.getPnd_Calc_Hold().compute(new ComputeParameters(false, ldaIaal586r.getPnd_Calc_Hold()), ldaIaal586r.getPnd_Rate_Guar_Ovp().divide(ldaIaal586r.getPnd_Tot_Guar_Divd())); //Natural: COMPUTE #CALC-HOLD = #RATE-GUAR-OVP / #TOT-GUAR-DIVD
        ldaIaal586r.getPnd_W_Rate_Guar_Ovp().compute(new ComputeParameters(true, ldaIaal586r.getPnd_W_Rate_Guar_Ovp()), ldaIaal586r.getPnd_Calc_Hold().multiply(ldaIaal586r.getPnd_Ovrpy_Tot())); //Natural: COMPUTE ROUNDED #W-RATE-GUAR-OVP = #CALC-HOLD * #OVRPY-TOT
        ldaIaal586r.getPnd_Calc_Hold().compute(new ComputeParameters(false, ldaIaal586r.getPnd_Calc_Hold()), ldaIaal586r.getPnd_Rate_Divd_Ovp().divide(ldaIaal586r.getPnd_Tot_Guar_Divd())); //Natural: COMPUTE #CALC-HOLD = #RATE-DIVD-OVP / #TOT-GUAR-DIVD
        ldaIaal586r.getPnd_W_Rate_Divd_Ovp().compute(new ComputeParameters(true, ldaIaal586r.getPnd_W_Rate_Divd_Ovp()), ldaIaal586r.getPnd_Calc_Hold().multiply(ldaIaal586r.getPnd_Ovrpy_Tot())); //Natural: COMPUTE ROUNDED #W-RATE-DIVD-OVP = #CALC-HOLD * #OVRPY-TOT
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Guar_Ovp().setValue(ldaIaal586r.getPnd_W_Rate_Guar_Ovp());                                                                 //Natural: MOVE #W-RATE-GUAR-OVP TO #T-RATE-GUAR-OVP
        ldaIaal586r.getPnd_T_Record_Pnd_T_Rate_Divd_Ovp().setValue(ldaIaal586r.getPnd_W_Rate_Divd_Ovp());                                                                 //Natural: MOVE #W-RATE-DIVD-OVP TO #T-RATE-DIVD-OVP
        ldaIaal586r.getPnd_C_Rate_Guar_Ovp().nadd(ldaIaal586r.getPnd_W_Rate_Guar_Ovp());                                                                                  //Natural: ADD #W-RATE-GUAR-OVP TO #C-RATE-GUAR-OVP
        ldaIaal586r.getPnd_C_Rate_Divd_Ovp().nadd(ldaIaal586r.getPnd_W_Rate_Divd_Ovp());                                                                                  //Natural: ADD #W-RATE-DIVD-OVP TO #C-RATE-DIVD-OVP
        ldaIaal586r.getPnd_Grand_Rate_Guar_Ovp().nadd(ldaIaal586r.getPnd_W_Rate_Guar_Ovp());                                                                              //Natural: ADD #W-RATE-GUAR-OVP TO #GRAND-RATE-GUAR-OVP
        ldaIaal586r.getPnd_Grand_Rate_Divd_Ovp().nadd(ldaIaal586r.getPnd_W_Rate_Divd_Ovp());                                                                              //Natural: ADD #W-RATE-DIVD-OVP TO #GRAND-RATE-DIVD-OVP
        ldaIaal586r.getPnd_Rate_Guar_Ovp().reset();                                                                                                                       //Natural: RESET #RATE-GUAR-OVP #RATE-DIVD-OVP #OVRPY-TOT #TOT-GUAR-DIVD #CALC-HOLD #W-RATE-GUAR-OVP #W-RATE-DIVD-OVP
        ldaIaal586r.getPnd_Rate_Divd_Ovp().reset();
        ldaIaal586r.getPnd_Ovrpy_Tot().reset();
        ldaIaal586r.getPnd_Tot_Guar_Divd().reset();
        ldaIaal586r.getPnd_Calc_Hold().reset();
        ldaIaal586r.getPnd_W_Rate_Guar_Ovp().reset();
        ldaIaal586r.getPnd_W_Rate_Divd_Ovp().reset();
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCTS
        if (condition(DbsUtil.maskMatches(ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            //* ************************
            //* * YEAR 2000 FIX START **
            //* ************************
            //* *Y2CHTS
            //* *  IGNORE
            pnd_Y2_Parm_Date_Yy.setValue(ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date_Yy());                                                                                //Natural: MOVE #PARM-DATE-YY TO #Y2-PARM-DATE-YY
            //* *************************
            //* * YEAR 2000 FIX END *****
            //* *************************
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCTS
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCTS
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal586r.getPnd_Page_Ctr_1().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-CTR-1
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ","IAAP586A",new TabSetting(47),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new                 //Natural: WRITE ( 1 ) 'PROGRAM ' 'IAAP586A' 47T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR-1
                        TabSetting(119),"PAGE ",ldaIaal586r.getPnd_Page_Ctr_1());
                    //* *Y2NCTS
                    //* *Y2NCTS
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(52),"GROSS OVERPAYMENT AMOUNTS RECORDED");                      //Natural: WRITE ( 1 ) '   DATE ' *DATU 52T 'GROSS OVERPAYMENT AMOUNTS RECORDED'
                    //* *Y2NCTS
                    //* *************************
                    //* ** YEAR 2000 FIX START **
                    //* *************************
                    //* *Y2CHTS
                    //* *COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY
                    ldaIaal586r.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY-A INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_A));
                    //* *Y2NCTS
                    //* *Y2NCTS
                    //* *************************
                    //* ** YEAR 2000 FIX END ****
                    //* *************************
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(62)," FOR",new TabSetting(67),ldaIaal586r.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 62T ' FOR' 67T #W-PARM-DATE
                    //*  WRITE (1) / 'TIAA TRADITIONAL'
                    //* *WRITE (1) / #COMP-DESC #FUND-DESC
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Fund_Desc);                                                                                    //Natural: WRITE ( 1 ) / #FUND-DESC
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(12),"ST",new TabSetting(17),"       OPTION",new                //Natural: WRITE ( 1 ) 001T 'CONTRACT' 012T 'ST' 017T '       OPTION' 040T 'MODE' 046T '  PAYMENT ' 058T ' FULL GUAR  ' 072T ' FULL DIVD  ' 086T 'GUARANTEE AMT' 101T 'DIVIDEND AMT' 115T ' OVERPAYMENT' 130T 'MSG'
                        TabSetting(40),"MODE",new TabSetting(46),"  PAYMENT ",new TabSetting(58)," FULL GUAR  ",new TabSetting(72)," FULL DIVD  ",new TabSetting(86),"GUARANTEE AMT",new 
                        TabSetting(101),"DIVIDEND AMT",new TabSetting(115)," OVERPAYMENT",new TabSetting(130),"MSG");
                    //* *Y2NCTS
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"DATE",new TabSetting(62),"PAID",new TabSetting(76),"PAID",new TabSetting(88),"OVERPAID",new  //Natural: WRITE ( 1 ) 050T 'DATE' 062T 'PAID' 076T 'PAID' 088T 'OVERPAID' 103T 'OVERPAID' 119T 'TOTAL'
                        TabSetting(103),"OVERPAID",new TabSetting(119),"TOTAL");
                    //* *Y2NCTS
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(12),"--",new TabSetting(17),"--------------------",new        //Natural: WRITE ( 1 ) 001T '---------' 012T '--' 017T '--------------------' 040T '----' 046T '----------' 058T '------------' 072T '------------' 086T '-------------' 101T '------------' 115T '------------' 130T '---'
                        TabSetting(40),"----",new TabSetting(46),"----------",new TabSetting(58),"------------",new TabSetting(72),"------------",new TabSetting(86),"-------------",new 
                        TabSetting(101),"------------",new TabSetting(115),"------------",new TabSetting(130),"---");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    ldaIaal586r.getPnd_Page_Ctr_2().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-CTR-2
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ","IAAP586B",new TabSetting(47),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new                 //Natural: WRITE ( 2 ) 'PROGRAM ' 'IAAP586B' 47T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR-2
                        TabSetting(119),"PAGE ",ldaIaal586r.getPnd_Page_Ctr_2());
                    //* *Y2NCTS
                    //* *Y2NCTS
                    getReports().write(2, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(52),"GROSS OVERPAYMENT AMOUNTS RECORDED");                      //Natural: WRITE ( 2 ) '   DATE ' *DATU 52T 'GROSS OVERPAYMENT AMOUNTS RECORDED'
                    //* *************************
                    //* ** YEAR 2000 FIX START **
                    //* *************************
                    //* *Y2CHTS
                    //* *COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY
                    ldaIaal586r.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY-A INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal586r.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_A));
                    //* *Y2NCTS
                    //* *Y2NCTS
                    //* *************************
                    //* ** YEAR 2000 FIX END ****
                    //* *************************
                    //* *Y2NCTS
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(62)," FOR",new TabSetting(67),ldaIaal586r.getPnd_W_Parm_Date());                            //Natural: WRITE ( 2 ) 62T ' FOR' 67T #W-PARM-DATE
                    //*  #FUND-DESC #FND-DSC
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_Comp_Desc,pnd_W_Fund_Desc);                                                                    //Natural: WRITE ( 2 ) / #COMP-DESC #W-FUND-DESC
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT",new TabSetting(18),"STATUS",new TabSetting(30),"       OPTION",new            //Natural: WRITE ( 2 ) 001T 'CONTRACT' 018T 'STATUS' 030T '       OPTION' 056T 'MODE' 070T 'PAYMENT DATE' 090T 'GROSS AMOUNT' 110T 'OVERPAYMENT' 125T 'MSG'
                        TabSetting(56),"MODE",new TabSetting(70),"PAYMENT DATE",new TabSetting(90),"GROSS AMOUNT",new TabSetting(110),"OVERPAYMENT",new 
                        TabSetting(125),"MSG");
                    //* *Y2NCTS
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(18),"------",new TabSetting(30),"--------------------",new    //Natural: WRITE ( 2 ) 001T '---------' 018T '------' 030T '--------------------' 056T '----' 070T '------------' 090T '------------' 110T '------------' 125T '---'
                        TabSetting(56),"----",new TabSetting(70),"------------",new TabSetting(90),"------------",new TabSetting(110),"------------",new 
                        TabSetting(125),"---");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
