/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:55 PM
**        * FROM NATURAL PROGRAM : Iaap583r
************************************************************
**        * FILE NAME            : Iaap583r.java
**        * CLASS NAME           : Iaap583r
**        * INSTANCE NAME        : Iaap583r
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP583R   READ WORK FILE (CREATED IN IAAP583S)   *
*      DATE     -  02/95      AND WRITE OUT INITIAL DEATH TRANS. FOR *
*    AUTHOR     -  ARI G.     THE SPECIFIC CHECK CYCLE - FOR BPS/TAX *
*                             IT WILL HAVE ALL CASES WHICH WENT THRU *
*                             THE ID STEP ON THAT DATE.              *
*  04/2017 OS PIN EXPANSION - SC 082017 FOR CHANGES.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap583r extends BLNatBase
{
    // Data Areas
    private LdaIaal583r ldaIaal583r;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal583r = new LdaIaal583r();
        registerRecord(ldaIaal583r);
        registerRecord(ldaIaal583r.getVw_iaa_Cntrl_Rcrd());
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal583r.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap583r() throws Exception
    {
        super("Iaap583r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 134 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "**********************************",NEWLINE,"  START OF IAAP583R REPORT PROGRAM",NEWLINE,"**********************************");            //Natural: WRITE '**********************************' / '  START OF IAAP583R REPORT PROGRAM'/ '**********************************'
        if (Global.isEscape()) return;
        ldaIaal583r.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal583r.getVw_iaa_Cntrl_Rcrd().readNextRow("READ01")))
        {
            //*    MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE (EM=YYYYMMDD) TO
            //*                #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal583r.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal583r.getPnd_Work_Record())))
        {
            ldaIaal583r.getPnd_Work_Records_Read().nadd(1);                                                                                                               //Natural: ADD 1 TO #WORK-RECORDS-READ
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE
            sub_Pnd_Setup_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal583r.getPnd_Lines_Left().lessOrEqual(ldaIaal583r.getPnd_Lines_To_Print())))                                                              //Natural: IF #LINES-LEFT NOT > #LINES-TO-PRINT
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaIaal583r.getPnd_Line_1());                                                                              //Natural: WRITE ( 1 ) / #LINE-1
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal583r.getPnd_Lines_Left().nsubtract(ldaIaal583r.getPnd_Lines_To_Print());                                                                               //Natural: SUBTRACT #LINES-TO-PRINT FROM #LINES-LEFT
                                                                                                                                                                          //Natural: PERFORM #RESET-FIELDS
            sub_Pnd_Reset_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #PRINT-FINAL-LINE
        sub_Pnd_Print_Final_Line();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "WORK FILE READS =====> ",ldaIaal583r.getPnd_Work_Records_Read());                                                                          //Natural: WRITE 'WORK FILE READS =====> ' #WORK-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "**********************************",NEWLINE,"  END OF IAAP583R REPORT PROGRAM",NEWLINE,"**********************************");              //Natural: WRITE '**********************************' / '  END OF IAAP583R REPORT PROGRAM'/ '**********************************'
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-FINAL-LINE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FIELDS
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Setup_Line() throws Exception                                                                                                                    //Natural: #SETUP-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Name().setValue(DbsUtil.compress(ldaIaal583r.getPnd_Work_Record_Pnd_W_Last_Name(), ldaIaal583r.getPnd_Work_Record_Pnd_W_First_Name(),  //Natural: COMPRESS #W-LAST-NAME #W-FIRST-NAME #W-MIDDLE-NAME INTO #LN1-NAME
            ldaIaal583r.getPnd_Work_Record_Pnd_W_Middle_Name()));
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Pin().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Pin());                                                                     //Natural: MOVE #W-PIN TO #LN1-PIN
        if (condition(ldaIaal583r.getPnd_Work_Record_Pnd_W_Ssn().notEquals(" ")))                                                                                         //Natural: IF #W-SSN NE ' '
        {
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal583r.getPnd_Work_Record_Pnd_Ssn_1_3(),                //Natural: COMPRESS #SSN-1-3 '-' #SSN-4-5 '-' #SSN-6-9 INTO #LN1-SSN LEAVING NO
                "-", ldaIaal583r.getPnd_Work_Record_Pnd_Ssn_4_5(), "-", ldaIaal583r.getPnd_Work_Record_Pnd_Ssn_6_9()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-SSN
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Cont_Stat().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Cont_Stat());                                                         //Natural: MOVE #W-CONT-STAT TO #LN1-CONT-STAT
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Annu_Type().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Annu_Type());                                                         //Natural: MOVE #W-ANNU-TYPE TO #LN1-ANNU-TYPE
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Nfp().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Nfp());                                                                     //Natural: MOVE #W-NFP TO #LN1-NFP
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Cont_Mode().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Contract_Mode());                                                     //Natural: MOVE #W-CONTRACT-MODE TO #LN1-CONT-MODE
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Citz().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Prtcpnt_Ctznshp_Cde());                                                    //Natural: MOVE #W-PRTCPNT-CTZNSHP-CDE TO #LN1-CITZ
        ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Resd().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Prtcpnt_Rsdncy_Cde());                                                     //Natural: MOVE #W-PRTCPNT-RSDNCY-CDE TO #LN1-RESD
        if (condition(ldaIaal583r.getPnd_Work_Record_Pnd_W_Dod_Dte().notEquals(" ")))                                                                                     //Natural: IF #W-DOD-DTE NOT = ' '
        {
            ldaIaal583r.getPnd_Date_Ccyymmdd().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Dod_Dte());                                                                  //Natural: MOVE #W-DOD-DTE TO #DATE-CCYYMMDD
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Dod().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal583r.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),              //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-YY INTO #LN1-DOD LEAVING NO
                "/", ldaIaal583r.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", ldaIaal583r.getPnd_Date_Ccyymmdd_Pnd_Date_Yy()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Dod().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-DOD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal583r.getPnd_Work_Record_Pnd_W_Cntrct_Issue_Dte().notEquals(" ")))                                                                            //Natural: IF #W-CNTRCT-ISSUE-DTE NOT = ' '
        {
            ldaIaal583r.getPnd_Date_Ccyymm_1().setValue(ldaIaal583r.getPnd_Work_Record_Pnd_W_Cntrct_Issue_Dte());                                                         //Natural: MOVE #W-CNTRCT-ISSUE-DTE TO #DATE-CCYYMM-1
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Doi().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal583r.getPnd_Date_Ccyymm_1_Pnd_Date_Mm_1(),            //Natural: COMPRESS #DATE-MM-1 '/' #DATE-CCYY-1 INTO #LN1-DOI LEAVING NO
                "/", ldaIaal583r.getPnd_Date_Ccyymm_1_Pnd_Date_Ccyy_1()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal583r.getPnd_Line_1_Pnd_Ln1_Doi().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-DOI
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal583r.getPnd_Lines_To_Print().nadd(2);                                                                                                                      //Natural: ADD 2 TO #LINES-TO-PRINT
    }
    private void sub_Pnd_Print_Final_Line() throws Exception                                                                                                              //Natural: #PRINT-FINAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal583r.getPnd_Lines_Left().lessOrEqual(2)))                                                                                                    //Natural: IF #LINES-LEFT NOT > 2
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Fields() throws Exception                                                                                                                  //Natural: #RESET-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal583r.getPnd_Line_1().reset();                                                                                                                              //Natural: RESET #LINE-1 #LINES-TO-PRINT
        ldaIaal583r.getPnd_Lines_To_Print().reset();
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal583r.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal583r.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal583r.getPnd_Page_Ctr().nadd(1);                                                                                                                //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR
                        TabSetting(119),"PAGE ",ldaIaal583r.getPnd_Page_Ctr());
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(44),"INITIAL DEATH TRANSACTION FOR",ldaIaal583r.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(), //Natural: WRITE ( 1 ) '   DATE ' *DATU 44T 'INITIAL DEATH TRANSACTION FOR' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 'CHECK CYCLE'
                        "CHECK CYCLE");
                    ldaIaal583r.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal583r.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),            //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                        "/", ldaIaal583r.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", ldaIaal583r.getIaa_Parm_Card_Pnd_Parm_Date_Yy()));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(60)," FOR",new TabSetting(65),ldaIaal583r.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 60T ' FOR' 65T #W-PARM-DATE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    //*  WRITE (1) 001T '                  NAME                  ' 082017 START
                    //*    042T '  PIN '
                    //*    051T '  SSN/EIN'
                    //*    064T ' CONT/STAT '
                    //*    077T 'PY'
                    //*    081T 'MODE'
                    //*    087T 'DATE OF'
                    //*    096T 'CTZ'
                    //*    101T 'RES'
                    //*    106T 'DATE OF'
                    //*    115T 'NFP'
                    //*    119T '  COMMENTS    '
                    //*  WRITE (1)
                    //*    087T ' ISSUE '
                    //*    096T 'CDE'
                    //*    101T 'CDE'
                    //*    106T ' DEATH '
                    //*  WRITE (1) 001T '----------------------------------------'
                    //*    042T '-------'
                    //*    051T '-----------'
                    //*    064T '-----------'
                    //*    077T '--'
                    //*    081T '----'
                    //*    087T '------- '
                    //*    096T '--- '
                    //*    101T '--- '
                    //*    106T '--------'
                    //*    115T '---'
                    //*    119T '--------------'
                    //* *
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                  NAME                  ",new TabSetting(42),"  PIN ",new               //Natural: WRITE ( 1 ) 001T '                  NAME                  ' 042T '  PIN ' 056T '  SSN/EIN' 069T ' CONT/STAT ' 082T 'PY' 086T 'MODE' 092T 'DATE OF' 101T 'CTZ' 106T 'RES' 111T 'DATE OF' 120T 'NFP' 124T ' COMMENTS'
                        TabSetting(56),"  SSN/EIN",new TabSetting(69)," CONT/STAT ",new TabSetting(82),"PY",new TabSetting(86),"MODE",new TabSetting(92),"DATE OF",new 
                        TabSetting(101),"CTZ",new TabSetting(106),"RES",new TabSetting(111),"DATE OF",new TabSetting(120),"NFP",new TabSetting(124)," COMMENTS");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(92)," ISSUE ",new TabSetting(101),"CDE",new TabSetting(106),"CDE",new TabSetting(111),      //Natural: WRITE ( 1 ) 092T ' ISSUE ' 101T 'CDE' 106T 'CDE' 111T ' DEATH '
                        " DEATH ");
                    //*  082017 END
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"----------------------------------------",new TabSetting(42),"-------",new              //Natural: WRITE ( 1 ) 001T '----------------------------------------' 042T '-------' 056T '-----------' 069T '-----------' 082T '--' 086T '----' 092T '------- ' 101T '--- ' 106T '--- ' 111T '--------' 120T '---' 124T '----------'
                        TabSetting(56),"-----------",new TabSetting(69),"-----------",new TabSetting(82),"--",new TabSetting(86),"----",new TabSetting(92),"------- ",new 
                        TabSetting(101),"--- ",new TabSetting(106),"--- ",new TabSetting(111),"--------",new TabSetting(120),"---",new TabSetting(124),"----------");
                    ldaIaal583r.getPnd_Lines_Left().setValue(49);                                                                                                         //Natural: ASSIGN #LINES-LEFT := 49
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=134 PS=56");
    }
}
