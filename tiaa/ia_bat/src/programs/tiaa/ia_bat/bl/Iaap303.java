/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:59 PM
**        * FROM NATURAL PROGRAM : Iaap303
************************************************************
**        * FILE NAME            : Iaap303.java
**        * CLASS NAME           : Iaap303
**        * INSTANCE NAME        : Iaap303
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM   -  IAAP303        THIS PROGRAM WILL READ ADABASE FILE    *
*      DATE   -  08/95          200 AND CREATE 1 FLAT EXTRACT FILE     *
*    AUTHOR   -  ARI G.         CONTAING CPR, CONTRACT AND DEDUCTION   *
*                               RECORDS. THIS FILE WILL CONTAIN HEADER *
*                               AND TRAILER RECORDS.                   *
*                                                                      *
*   04/2017      JUN TINIO      PIN EXPANSION - + 5 BYTES
*                               SCAN 04/17
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap303 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Count_Fund_Record_Reads;
    private DbsField pnd_Count_Master_File_Reads;
    private DbsField pnd_Count_Master_Record_Error;
    private DbsField pnd_Count_Fund_Extract;
    private DbsField pnd_Count_Contract_Extract;
    private DbsField pnd_Count_Deduction_Extract;
    private DbsField pnd_Count_Cpr_Extract;
    private DbsField pnd_Count_Active_Payees;
    private DbsField pnd_Count_Inactive_Payees;
    private DbsField pnd_Count;
    private DbsField pnd_Count2;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Payee;
    private DbsField pnd_Work_Record_1_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Optn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orgn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Acctng_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Issue_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Crrncy_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Type_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler2;

    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Record_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_2_Pnd_W2_Actvty_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cash_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Company_Cd;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Percent;
    private DbsField pnd_Work_Record_2_Pnd_W2_Mode_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Hold_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cmbne_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Srce;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte;

    private DbsGroup pnd_Work_Record_3;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Payee;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_3_Pnd_W3_Filler;
    private DbsField pnd_Per_Tot;
    private DbsField pnd_I;
    private DbsField pnd_All_Ok;

    private DataAccessProgramView vw_iaa_Master_File;
    private DbsField iaa_Master_File_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Master_File_Cntrct_Optn_Cde;
    private DbsField iaa_Master_File_Cntrct_Orgn_Cde;
    private DbsField iaa_Master_File_Cntrct_Acctng_Cde;
    private DbsField iaa_Master_File_Cntrct_Issue_Dte;
    private DbsField iaa_Master_File_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Master_File_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Master_File_Cntrct_Crrncy_Cde;
    private DbsField iaa_Master_File_Cntrct_Type_Cde;
    private DbsField iaa_Master_File_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Master_File_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Master_File_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Master_File_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Master_File_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Master_File_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Master_File_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Master_File_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Master_File_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Master_File_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Master_File_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Master_File_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Master_File_Cpr_Id_Nbr;
    private DbsField iaa_Master_File_Lst_Trans_Dte;
    private DbsField iaa_Master_File_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Master_File_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Master_File_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Master_File_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Master_File_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Master_File_Cntrct_Actvty_Cde;
    private DbsField iaa_Master_File_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Master_File_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Master_File_Cntrct_Cash_Cde;
    private DbsField iaa_Master_File_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Master_File_Cntrct_Company_Data;
    private DbsField iaa_Master_File_Cntrct_Company_Cd;
    private DbsField iaa_Master_File_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Master_File_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Master_File_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Master_File_Cntrct_Ivc_Amt;
    private DbsField iaa_Master_File_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Master_File_Cntrct_Rtb_Amt;
    private DbsField iaa_Master_File_Cntrct_Rtb_Percent;
    private DbsField iaa_Master_File_Cntrct_Mode_Ind;
    private DbsField iaa_Master_File_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Master_File_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Master_File_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Master_File_Bnfcry_Xref_Ind;
    private DbsField iaa_Master_File_Bnfcry_Dod_Dte;
    private DbsField iaa_Master_File_Cntrct_Pend_Cde;
    private DbsField iaa_Master_File_Cntrct_Hold_Cde;
    private DbsField iaa_Master_File_Cntrct_Pend_Dte;
    private DbsField iaa_Master_File_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Master_File_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Master_File_Cntrct_Cmbne_Cde;
    private DbsField iaa_Master_File_Cntrct_Spirt_Cde;
    private DbsField iaa_Master_File_Cntrct_Spirt_Amt;
    private DbsField iaa_Master_File_Cntrct_Spirt_Srce;
    private DbsField iaa_Master_File_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Master_File_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Master_File_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Master_File_Cntrct_State_Cde;
    private DbsField iaa_Master_File_Cntrct_State_Tax_Amt;
    private DbsField iaa_Master_File_Cntrct_Local_Cde;
    private DbsField iaa_Master_File_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Master_File_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Master_File_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Master_File_Ddctn_Payee_Cde;
    private DbsField iaa_Master_File_Ddctn_Id_Nbr;
    private DbsField iaa_Master_File_Ddctn_Cde;
    private DbsField iaa_Master_File_Ddctn_Seq_Nbr;
    private DbsField iaa_Master_File_Ddctn_Payee;
    private DbsField iaa_Master_File_Ddctn_Per_Amt;
    private DbsField iaa_Master_File_Ddctn_Ytd_Amt;
    private DbsField iaa_Master_File_Ddctn_Pd_To_Dte;
    private DbsField iaa_Master_File_Ddctn_Tot_Amt;
    private DbsField iaa_Master_File_Ddctn_Intent_Cde;
    private DbsField iaa_Master_File_Ddctn_Strt_Dte;
    private DbsField iaa_Master_File_Ddctn_Stp_Dte;
    private DbsField iaa_Master_File_Ddctn_Final_Dte;
    private DbsField iaa_Master_File_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Master_File_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Master_File_Cpr_Xfr_Iss_Dte;

    private DbsGroup pnd_Work_Record_Header;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Record_Code;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Check_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Process_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_1;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_2;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_3;

    private DbsGroup pnd_Work_Record_Trailer;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Record_Code;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Check_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Process_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_1;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_2;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_3;
    private DbsField pnd_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Check_Date_Ccyymmdd__R_Field_1;
    private DbsField pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Count_Fund_Record_Reads = localVariables.newFieldInRecord("pnd_Count_Fund_Record_Reads", "#COUNT-FUND-RECORD-READS", FieldType.NUMERIC, 8);
        pnd_Count_Master_File_Reads = localVariables.newFieldInRecord("pnd_Count_Master_File_Reads", "#COUNT-MASTER-FILE-READS", FieldType.NUMERIC, 8);
        pnd_Count_Master_Record_Error = localVariables.newFieldInRecord("pnd_Count_Master_Record_Error", "#COUNT-MASTER-RECORD-ERROR", FieldType.NUMERIC, 
            8);
        pnd_Count_Fund_Extract = localVariables.newFieldInRecord("pnd_Count_Fund_Extract", "#COUNT-FUND-EXTRACT", FieldType.NUMERIC, 8);
        pnd_Count_Contract_Extract = localVariables.newFieldInRecord("pnd_Count_Contract_Extract", "#COUNT-CONTRACT-EXTRACT", FieldType.NUMERIC, 8);
        pnd_Count_Deduction_Extract = localVariables.newFieldInRecord("pnd_Count_Deduction_Extract", "#COUNT-DEDUCTION-EXTRACT", FieldType.NUMERIC, 8);
        pnd_Count_Cpr_Extract = localVariables.newFieldInRecord("pnd_Count_Cpr_Extract", "#COUNT-CPR-EXTRACT", FieldType.NUMERIC, 8);
        pnd_Count_Active_Payees = localVariables.newFieldInRecord("pnd_Count_Active_Payees", "#COUNT-ACTIVE-PAYEES", FieldType.NUMERIC, 8);
        pnd_Count_Inactive_Payees = localVariables.newFieldInRecord("pnd_Count_Inactive_Payees", "#COUNT-INACTIVE-PAYEES", FieldType.NUMERIC, 8);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 8);
        pnd_Count2 = localVariables.newFieldInRecord("pnd_Count2", "#COUNT2", FieldType.NUMERIC, 8);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr", "#W1-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Payee = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Record_Code", "#W1-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Optn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Optn_Cde", "#W1-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Orgn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W1_Acctng_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Acctng_Cde", "#W1-ACCTNG-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_1_Pnd_W1_Issue_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Issue_Dte", "#W1-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte", "#W1-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte", "#W1-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_Crrncy_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Crrncy_Cde", "#W1-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_W1_Type_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Type_Cde", "#W1-TYPE-CDE", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd", "#W1-PYMNT-MTHD", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde", "#W1-PNSN-PLN-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind", "#W1-JOINT-CNVRT-RCRCD-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr", "#W1-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde", "#W1-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind", "#W1-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte", "#W1-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte", "#W1-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde", "#W1-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt", "#W1-FIRST-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte", "#W1-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind", "#W1-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte", "#W1-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte", "#W1-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde", "#W1-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt", "#W1-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte", "#W1-SCND-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn", "#W1-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde", "#W1-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde", "#W1-DIV-COLL-CDE", FieldType.STRING, 
            5);
        pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde", "#W1-INST-ISS-CDE", FieldType.STRING, 
            5);
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_1_Pnd_W1_Filler = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler", "#W1-FILLER", FieldType.STRING, 188);
        pnd_Work_Record_1_Pnd_W1_Filler2 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler2", "#W1-FILLER2", FieldType.STRING, 5);

        pnd_Work_Record_2 = localVariables.newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr", "#W2-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde", "#W2-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Record_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Record_Cde", "#W2-RECORD-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr", "#W2-CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde", "#W2-PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde", "#W2-PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw", "#W2-PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr", "#W2-PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ", "#W2-PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Actvty_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Actvty_Cde", "#W2-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn", "#W2-TRMNTE-RSN", FieldType.STRING, 
            2);
        pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind", "#W2-RWRTTN-IND", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Cash_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cash_Cde", "#W2-CASH-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde", "#W2-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Company_Cd = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Company_Cd", "#W2-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind", "#W2-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt", "#W2-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Amt", "#W2-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt", "#W2-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Percent = pnd_Work_Record_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Percent", "#W2-RTB-PERCENT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Mode_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Mode_Ind", "#W2-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte", "#W2-WTHDRWL-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte", "#W2-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte", "#W2-FINAL-PAY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind", "#W2-BNFCRY-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte", "#W2-BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Pend_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Cde", "#W2-PEND-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Hold_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Hold_Cde", "#W2-HOLD-CDE", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Pend_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Dte", "#W2-PEND-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde", "#W2-PREV-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde", "#W2-CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_2_Pnd_W2_Cmbne_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cmbne_Cde", "#W2-CMBNE-CDE", FieldType.STRING, 
            12);
        pnd_Work_Record_2_Pnd_W2_Spirt_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Cde", "#W2-SPIRT-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Amt", "#W2-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Work_Record_2_Pnd_W2_Spirt_Srce = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Srce", "#W2-SPIRT-SRCE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte", "#W2-SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte", "#W2-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt", "#W2-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_State_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Cde", "#W2-STATE-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_State_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Tax_Amt", "#W2-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Local_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Cde", "#W2-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt", "#W2-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte", "#W2-LST-CHNGE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde", "#W2-CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde", "#W2-CPR-LGL-RES-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte", "#W2-CPR-XFR-ISS-DTE", 
            FieldType.DATE);

        pnd_Work_Record_3 = localVariables.newGroupInRecord("pnd_Work_Record_3", "#WORK-RECORD-3");
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr", "#W3-DDCTN-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde", "#W3-DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde", "#W3-DDCTN-RECORD-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr", "#W3-DDCTN-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Cde", "#W3-DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr", "#W3-DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Payee", "#W3-DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt", "#W3-DDCTN-PER-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt", "#W3-DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte", "#W3-DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt", "#W3-DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde", "#W3-DDCTN-INTENT-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte", "#W3-DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte", "#W3-DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte", "#W3-DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte", "#W3-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_3_Pnd_W3_Filler = pnd_Work_Record_3.newFieldInGroup("pnd_Work_Record_3_Pnd_W3_Filler", "#W3-FILLER", FieldType.STRING, 237);
        pnd_Per_Tot = localVariables.newFieldInRecord("pnd_Per_Tot", "#PER-TOT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_All_Ok = localVariables.newFieldInRecord("pnd_All_Ok", "#ALL-OK", FieldType.STRING, 1);

        vw_iaa_Master_File = new DataAccessProgramView(new NameInfo("vw_iaa_Master_File", "IAA-MASTER-FILE"), "IAA_MASTER_FILE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_MASTER_FILE"));
        iaa_Master_File_Cntrct_Ppcn_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Master_File_Cntrct_Optn_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Master_File_Cntrct_Orgn_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Master_File_Cntrct_Acctng_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Master_File_Cntrct_Issue_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Master_File_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Master_File_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Master_File_Cntrct_Crrncy_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Master_File_Cntrct_Type_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Master_File_Cntrct_Pymnt_Mthd = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Master_File_Cntrct_Pnsn_Pln_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Master_File_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Master_File_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Master_File_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Master_File_Cntrct_First_Annt_Xref_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Master_File_Cntrct_First_Annt_Dob_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Master_File_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Master_File_Cntrct_First_Annt_Sex_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Master_File_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Master_File_Cntrct_First_Annt_Dod_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Master_File_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Master_File_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Master_File_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Master_File_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Master_File_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Master_File_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Master_File_Cntrct_Scnd_Annt_Ssn = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Master_File_Cntrct_Div_Payee_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Master_File_Cntrct_Div_Coll_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Master_File_Cntrct_Inst_Iss_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Master_File_Cntrct_Part_Ppcn_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Master_File_Cntrct_Part_Payee_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Master_File_Cpr_Id_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Master_File_Lst_Trans_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Master_File_Prtcpnt_Ctznshp_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Master_File_Prtcpnt_Rsdncy_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Master_File_Prtcpnt_Rsdncy_Sw = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Master_File_Prtcpnt_Tax_Id_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Master_File_Prtcpnt_Tax_Id_Typ = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Master_File_Cntrct_Actvty_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Master_File_Cntrct_Trmnte_Rsn = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Master_File_Cntrct_Rwrttn_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Master_File_Cntrct_Cash_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Master_File_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Master_File_Cntrct_Company_Data = vw_iaa_Master_File.getRecord().newGroupInGroup("iaa_Master_File_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Company_Cd = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Rcvry_Type_Ind = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Per_Ivc_Amt = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Resdl_Ivc_Amt = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Ivc_Amt = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Ivc_Used_Amt = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Rtb_Amt = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Rtb_Percent = iaa_Master_File_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Master_File_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Master_File_Cntrct_Mode_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Master_File_Cntrct_Wthdrwl_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Master_File_Cntrct_Final_Per_Pay_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Master_File_Cntrct_Final_Pay_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Master_File_Bnfcry_Xref_Ind = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Master_File_Bnfcry_Dod_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Master_File_Cntrct_Pend_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Master_File_Cntrct_Hold_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Master_File_Cntrct_Pend_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Master_File_Cntrct_Prev_Dist_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Master_File_Cntrct_Curr_Dist_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Master_File_Cntrct_Cmbne_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Master_File_Cntrct_Spirt_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Master_File_Cntrct_Spirt_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Master_File_Cntrct_Spirt_Srce = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Master_File_Cntrct_Spirt_Arr_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Master_File_Cntrct_Spirt_Prcss_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Master_File_Cntrct_Fed_Tax_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Master_File_Cntrct_State_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Master_File_Cntrct_State_Tax_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Master_File_Cntrct_Local_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Master_File_Cntrct_Local_Tax_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Master_File_Cntrct_Lst_Chnge_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Master_File_Ddctn_Ppcn_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Master_File_Ddctn_Payee_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Master_File_Ddctn_Id_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Master_File_Ddctn_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Master_File_Ddctn_Seq_Nbr = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Master_File_Ddctn_Payee = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Master_File_Ddctn_Per_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Master_File_Ddctn_Ytd_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Master_File_Ddctn_Pd_To_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Master_File_Ddctn_Tot_Amt = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Master_File_Ddctn_Intent_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Master_File_Ddctn_Strt_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Master_File_Ddctn_Stp_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Master_File_Ddctn_Final_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Master_File_Cpr_Xfr_Term_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Master_File_Cpr_Lgl_Res_Cde = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Master_File_Cpr_Xfr_Iss_Dte = vw_iaa_Master_File.getRecord().newFieldInGroup("iaa_Master_File_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        registerRecord(vw_iaa_Master_File);

        pnd_Work_Record_Header = localVariables.newGroupInRecord("pnd_Work_Record_Header", "#WORK-RECORD-HEADER");
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr", "#WH-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde", "#WH-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Record_Code = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Record_Code", "#WH-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Check_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Check_Date", "#WH-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Process_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Process_Date", "#WH-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_1 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_1", "#WH-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Header_Pnd_Wh_Filler_2 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_2", "#WH-FILLER-2", FieldType.STRING, 
            100);
        pnd_Work_Record_Header_Pnd_Wh_Filler_3 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_3", "#WH-FILLER-3", FieldType.STRING, 
            5);

        pnd_Work_Record_Trailer = localVariables.newGroupInRecord("pnd_Work_Record_Trailer", "#WORK-RECORD-TRAILER");
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr", "#WT-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde", "#WT-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Record_Code", "#WT-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Check_Date", "#WT-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Process_Date", "#WT-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records", "#WT-TOT-CNT-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records", "#WT-TOT-CPR-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records", "#WT-TOT-DED-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_1", "#WT-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_2", "#WT-FILLER-2", FieldType.STRING, 
            76);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_3 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_3", "#WT-FILLER-3", FieldType.STRING, 
            5);
        pnd_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Check_Date_Ccyymmdd__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd__R_Field_1", "REDEFINE", pnd_Check_Date_Ccyymmdd);
        pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N = pnd_Check_Date_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N", 
            "#CHECK-DATE-CCYYMMDD-N", FieldType.NUMERIC, 8);

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRL_RCRD_KEY");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key.setSuperDescriptor(true);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Master_File.reset();
        vw_iaa_Cntrl_Rcrd_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap303() throws Exception
    {
        super("Iaap303");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP301 ***");                                                                                                        //Natural: WRITE '*** START OF PROGRAM IAAP301 ***'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        vw_iaa_Cntrl_Rcrd_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "RC",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RC:
        while (condition(vw_iaa_Cntrl_Rcrd_View.readNextRow("RC")))
        {
            pnd_Check_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr.setValue("   CHEADER");                                                                                             //Natural: MOVE '   CHEADER' TO #WH-CNTRCT-PPCN-NBR
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde.setValue(0);                                                                                                       //Natural: MOVE 0 TO #WH-CNTRCT-PAYEE-CDE
        pnd_Work_Record_Header_Pnd_Wh_Record_Code.setValue(0);                                                                                                            //Natural: MOVE 0 TO #WH-RECORD-CODE
        pnd_Work_Record_Header_Pnd_Wh_Check_Date.setValue(pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N);                                                             //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WH-CHECK-DATE
        pnd_Work_Record_Header_Pnd_Wh_Process_Date.setValue(Global.getDATN());                                                                                            //Natural: MOVE *DATN TO #WH-PROCESS-DATE
        pnd_Work_Record_Header_Pnd_Wh_Filler_1.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-1
        pnd_Work_Record_Header_Pnd_Wh_Filler_2.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-2
        getWorkFiles().write(1, false, pnd_Work_Record_Header);                                                                                                           //Natural: WRITE WORK FILE 1 #WORK-RECORD-HEADER
        pnd_Count.reset();                                                                                                                                                //Natural: RESET #COUNT #COUNT2
        pnd_Count2.reset();
        vw_iaa_Master_File.startDatabaseRead                                                                                                                              //Natural: READ IAA-MASTER-FILE PHYSICAL
        (
        "R2",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R2:
        while (condition(vw_iaa_Master_File.readNextRow("R2")))
        {
            pnd_Count_Master_File_Reads.nadd(1);                                                                                                                          //Natural: ADD 1 TO #COUNT-MASTER-FILE-READS
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            pnd_Count2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #COUNT2
            if (condition(pnd_Count.greater(4999)))                                                                                                                       //Natural: IF #COUNT > 4999
            {
                getReports().write(0, "NUMBER OF RECORDS READ",pnd_Count2,Global.getTIMX());                                                                              //Natural: WRITE 'NUMBER OF RECORDS READ' #COUNT2 *TIMX
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Count.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #COUNT
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Ok.reset();                                                                                                                                           //Natural: RESET #ALL-OK
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            short decideConditionsMet333 = 0;                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = ' ' AND CNTRCT-PART-PPCN-NBR = ' ' AND DDCTN-PPCN-NBR = ' '
            if (condition(iaa_Master_File_Cntrct_Ppcn_Nbr.equals(" ") && iaa_Master_File_Cntrct_Part_Ppcn_Nbr.equals(" ") && iaa_Master_File_Ddctn_Ppcn_Nbr.equals(" ")))
            {
                decideConditionsMet333++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",iaa_Master_File_Cntrct_Ppcn_Nbr,"=",iaa_Master_File_Cntrct_Part_Ppcn_Nbr,"=",iaa_Master_File_Ddctn_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN 'ALL CNTR. FIELDS ARE BLANK'
                    ColumnSpacing(7),Global.getAstISN(),"ALL CNTR. FIELDS ARE BLANK");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR NOT = ' ' AND CNTRCT-PART-PPCN-NBR NOT = ' '
            else if (condition(iaa_Master_File_Cntrct_Ppcn_Nbr.notEquals(" ") && iaa_Master_File_Cntrct_Part_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet333++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",iaa_Master_File_Cntrct_Ppcn_Nbr,"=",iaa_Master_File_Cntrct_Part_Ppcn_Nbr,"=",iaa_Master_File_Ddctn_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR NOT = ' ' AND DDCTN-PPCN-NBR NOT = ' '
            else if (condition(iaa_Master_File_Cntrct_Ppcn_Nbr.notEquals(" ") && iaa_Master_File_Ddctn_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet333++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",iaa_Master_File_Cntrct_Ppcn_Nbr,"=",iaa_Master_File_Cntrct_Part_Ppcn_Nbr,"=",iaa_Master_File_Ddctn_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PART-PPCN-NBR NOT = ' ' AND DDCTN-PPCN-NBR NOT = ' '
            else if (condition(iaa_Master_File_Cntrct_Part_Ppcn_Nbr.notEquals(" ") && iaa_Master_File_Ddctn_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet333++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",iaa_Master_File_Cntrct_Ppcn_Nbr,"=",iaa_Master_File_Cntrct_Part_Ppcn_Nbr,"=",iaa_Master_File_Ddctn_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_All_Ok.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #ALL-OK
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_All_Ok.notEquals("Y")))                                                                                                                     //Natural: IF #ALL-OK NOT = 'Y'
            {
                pnd_Count_Master_Record_Error.nadd(1);                                                                                                                    //Natural: ADD 1 TO #COUNT-MASTER-RECORD-ERROR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_All_Ok.equals("Y"))))                                                                                                                     //Natural: ACCEPT IF #ALL-OK = 'Y'
            {
                continue;
            }
            short decideConditionsMet356 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-PPCN-NBR NOT = ' '
            if (condition(iaa_Master_File_Cntrct_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet356++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-CONTRACT-EXTRACT
                sub_Pnd_Write_Contract_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PART-PPCN-NBR NOT = ' '
            else if (condition(iaa_Master_File_Cntrct_Part_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet356++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-CPR-EXTRACT
                sub_Pnd_Write_Cpr_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN DDCTN-PPCN-NBR NOT = ' '
            else if (condition(iaa_Master_File_Ddctn_Ppcn_Nbr.notEquals(" ")))
            {
                decideConditionsMet356++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-DEDUCTION-EXTRACT
                sub_Pnd_Write_Deduction_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr.setValue("99CTRAILER");                                                                                            //Natural: MOVE '99CTRAILER' TO #WT-CNTRCT-PPCN-NBR
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde.setValue(0);                                                                                                      //Natural: MOVE 0 TO #WT-CNTRCT-PAYEE-CDE
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code.setValue(0);                                                                                                           //Natural: MOVE 0 TO #WT-RECORD-CODE
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date.setValue(pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N);                                                            //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WT-CHECK-DATE
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date.setValue(Global.getDATN());                                                                                           //Natural: MOVE *DATN TO #WT-PROCESS-DATE
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records.setValue(pnd_Count_Contract_Extract);                                                                              //Natural: MOVE #COUNT-CONTRACT-EXTRACT TO #WT-TOT-CNT-RECORDS
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records.setValue(pnd_Count_Cpr_Extract);                                                                                   //Natural: MOVE #COUNT-CPR-EXTRACT TO #WT-TOT-CPR-RECORDS
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records.setValue(pnd_Count_Deduction_Extract);                                                                             //Natural: MOVE #COUNT-DEDUCTION-EXTRACT TO #WT-TOT-DED-RECORDS
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-1
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-2
        getWorkFiles().write(1, false, pnd_Work_Record_Trailer);                                                                                                          //Natural: WRITE WORK FILE 1 #WORK-RECORD-TRAILER
        getReports().write(1, ReportOption.NOTITLE,"          CPR EXTRACT ==>",pnd_Count_Cpr_Extract, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                 //Natural: WRITE ( 1 ) '          CPR EXTRACT ==>' #COUNT-CPR-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"     CONTRACT EXTRACT ==>",pnd_Count_Contract_Extract, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( 1 ) '     CONTRACT EXTRACT ==>' #COUNT-CONTRACT-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"    DEDUCTION EXTRACT ==>",pnd_Count_Deduction_Extract, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                           //Natural: WRITE ( 1 ) '    DEDUCTION EXTRACT ==>' #COUNT-DEDUCTION-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"  TOTAL ACTIVE PAYEES ==>",pnd_Count_Active_Payees, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                       //Natural: WRITE ( 1 ) / '  TOTAL ACTIVE PAYEES ==>' #COUNT-ACTIVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL INACTIVE PAYEES ==>",pnd_Count_Inactive_Payees, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 'TOTAL INACTIVE PAYEES ==>' #COUNT-INACTIVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        if (condition(pnd_Count_Master_Record_Error.greater(getZero())))                                                                                                  //Natural: IF #COUNT-MASTER-RECORD-ERROR > 0
        {
            getReports().skip(2, 2);                                                                                                                                      //Natural: SKIP ( 2 ) 2
            getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));              //Natural: WRITE ( 2 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, NEWLINE,"*** END OF PROGRAM IAAP301 ***");                                                                                                  //Natural: WRITE / '*** END OF PROGRAM IAAP301 ***'
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CONTRACT-EXTRACT
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-DEDUCTION-EXTRACT
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CPR-EXTRACT
        //* ***************************************************************
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *  WRITE '===================================================='
        getReports().write(0, NEWLINE,"  MASTER FILE READS =========> ",pnd_Count_Master_File_Reads);                                                                     //Natural: WRITE / '  MASTER FILE READS =========> ' #COUNT-MASTER-FILE-READS
        if (Global.isEscape()) return;
        getReports().write(0, "  MASTER FILE RECORD ERRORS => ",pnd_Count_Master_Record_Error);                                                                           //Natural: WRITE '  MASTER FILE RECORD ERRORS => ' #COUNT-MASTER-RECORD-ERROR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Contract_Extract() throws Exception                                                                                                        //Natural: #WRITE-CONTRACT-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr.setValue(iaa_Master_File_Cntrct_Ppcn_Nbr);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PPCN-NBR TO #W1-PPCN-NBR
        pnd_Work_Record_1_Pnd_W1_Payee.setValue(0);                                                                                                                       //Natural: MOVE 0 TO #W1-PAYEE
        pnd_Work_Record_1_Pnd_W1_Record_Code.setValue(10);                                                                                                                //Natural: MOVE 10 TO #W1-RECORD-CODE
        pnd_Work_Record_1_Pnd_W1_Optn_Cde.setValue(iaa_Master_File_Cntrct_Optn_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-OPTN-CDE TO #W1-OPTN-CDE
        pnd_Work_Record_1_Pnd_W1_Orgn_Cde.setValue(iaa_Master_File_Cntrct_Orgn_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-ORGN-CDE TO #W1-ORGN-CDE
        pnd_Work_Record_1_Pnd_W1_Acctng_Cde.setValue(iaa_Master_File_Cntrct_Acctng_Cde);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-ACCTNG-CDE TO #W1-ACCTNG-CDE
        pnd_Work_Record_1_Pnd_W1_Issue_Dte.setValue(iaa_Master_File_Cntrct_Issue_Dte);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-ISSUE-DTE TO #W1-ISSUE-DTE
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte.setValue(iaa_Master_File_Cntrct_First_Pymnt_Due_Dte);                                                                //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-PYMNT-DUE-DTE TO #W1-FIRST-PYMNT-DUE-DTE
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte.setValue(iaa_Master_File_Cntrct_First_Pymnt_Pd_Dte);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-PYMNT-PD-DTE TO #W1-FIRST-PYMNT-PD-DTE
        pnd_Work_Record_1_Pnd_W1_Crrncy_Cde.setValue(iaa_Master_File_Cntrct_Crrncy_Cde);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-CRRNCY-CDE TO #W1-CRRNCY-CDE
        pnd_Work_Record_1_Pnd_W1_Type_Cde.setValue(iaa_Master_File_Cntrct_Type_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-TYPE-CDE TO #W1-TYPE-CDE
        pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd.setValue(iaa_Master_File_Cntrct_Pymnt_Mthd);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PYMNT-MTHD TO #W1-PYMNT-MTHD
        pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde.setValue(iaa_Master_File_Cntrct_Pnsn_Pln_Cde);                                                                              //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PNSN-PLN-CDE TO #W1-PNSN-PLN-CDE
        pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind.setValue(iaa_Master_File_Cntrct_Joint_Cnvrt_Rcrd_Ind);                                                             //Natural: MOVE IAA-MASTER-FILE.CNTRCT-JOINT-CNVRT-RCRD-IND TO #W1-JOINT-CNVRT-RCRCD-IND
        pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr.setValue(iaa_Master_File_Cntrct_Orig_Da_Cntrct_Nbr);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-ORIG-DA-CNTRCT-NBR TO #W1-ORIG-DA-CNTRCT-NBR
        pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde.setValue(iaa_Master_File_Cntrct_Rsdncy_At_Issue_Cde);                                                                //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RSDNCY-AT-ISSUE-CDE TO #W1-RSDNCY-AT-ISSUE-CDE
        pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind.setValue(iaa_Master_File_Cntrct_First_Annt_Xref_Ind);                                                                //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-XREF-IND TO #W1-FIRST-ANNT-XREF-IND
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte.setValue(iaa_Master_File_Cntrct_First_Annt_Dob_Dte);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-DOB-DTE TO #W1-FIRST-ANNT-DOB-DTE
        pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte.setValue(iaa_Master_File_Cntrct_First_Annt_Mrtlty_Yob_Dte);                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE TO #W1-FIRST-ANNT-MRTLTY-YOB-DTE
        pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde.setValue(iaa_Master_File_Cntrct_First_Annt_Sex_Cde);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-SEX-CDE TO #W1-FIRST-ANNT-SEX-CDE
        pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt.setValue(iaa_Master_File_Cntrct_First_Annt_Lfe_Cnt);                                                                 //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-LFE-CNT TO #W1-FIRST-ANNT-LIFE-CNT
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte.setValue(iaa_Master_File_Cntrct_First_Annt_Dod_Dte);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FIRST-ANNT-DOD-DTE TO #W1-FIRST-ANNT-DOD-DTE
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Xref_Ind);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-XREF-IND TO #W1-SCND-ANNT-XREF-IND
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Dob_Dte);                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-DOB-DTE TO #W1-SCND-ANNT-DOB-DTE
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte);                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE TO #W1-SCND-ANNT-MRTLTY-YOB-DTE
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Sex_Cde);                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-SEX-CDE TO #W1-SCND-ANNT-SEX-CDE
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Life_Cnt);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-LIFE-CNT TO #W1-SCND-ANNT-LIFE-CNT
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Dod_Dte);                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-DOD-DTE TO #W1-SCND-ANNT-DOD-DTE
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn.setValue(iaa_Master_File_Cntrct_Scnd_Annt_Ssn);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SCND-ANNT-SSN TO #W1-SCND-ANNT-SSN
        pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde.setValue(iaa_Master_File_Cntrct_Div_Payee_Cde);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-DIV-PAYEE-CDE TO #W1-DIV-PAYEE-CDE
        pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde.setValue(iaa_Master_File_Cntrct_Div_Coll_Cde);                                                                              //Natural: MOVE IAA-MASTER-FILE.CNTRCT-DIV-COLL-CDE TO #W1-DIV-COLL-CDE
        pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde.setValue(iaa_Master_File_Cntrct_Inst_Iss_Cde);                                                                              //Natural: MOVE IAA-MASTER-FILE.CNTRCT-INST-ISS-CDE TO #W1-INST-ISS-CDE
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte.setValue(iaa_Master_File_Lst_Trans_Dte);                                                                                   //Natural: MOVE IAA-MASTER-FILE.LST-TRANS-DTE TO #W1-LST-TRANS-DTE
        pnd_Work_Record_1_Pnd_W1_Filler.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #W1-FILLER
        getWorkFiles().write(1, false, pnd_Work_Record_1);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
        pnd_Count_Contract_Extract.nadd(1);                                                                                                                               //Natural: ADD 1 TO #COUNT-CONTRACT-EXTRACT
    }
    private void sub_Pnd_Write_Deduction_Extract() throws Exception                                                                                                       //Natural: #WRITE-DEDUCTION-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr.setValue(iaa_Master_File_Ddctn_Ppcn_Nbr);                                                                                 //Natural: MOVE IAA-MASTER-FILE.DDCTN-PPCN-NBR TO #W3-DDCTN-PPCN-NBR
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde.setValue(iaa_Master_File_Ddctn_Payee_Cde);                                                                               //Natural: MOVE IAA-MASTER-FILE.DDCTN-PAYEE-CDE TO #W3-DDCTN-PAYEE-CDE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde.setValue(40);                                                                                                           //Natural: MOVE 40 TO #W3-DDCTN-RECORD-CDE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr.setValue(iaa_Master_File_Ddctn_Id_Nbr);                                                                                     //Natural: MOVE IAA-MASTER-FILE.DDCTN-ID-NBR TO #W3-DDCTN-ID-NBR
        pnd_Work_Record_3_Pnd_W3_Ddctn_Cde.setValue(iaa_Master_File_Ddctn_Cde);                                                                                           //Natural: MOVE IAA-MASTER-FILE.DDCTN-CDE TO #W3-DDCTN-CDE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr.setValue(iaa_Master_File_Ddctn_Seq_Nbr);                                                                                   //Natural: MOVE IAA-MASTER-FILE.DDCTN-SEQ-NBR TO #W3-DDCTN-SEQ-NBR
        pnd_Work_Record_3_Pnd_W3_Ddctn_Payee.setValue(iaa_Master_File_Ddctn_Payee);                                                                                       //Natural: MOVE IAA-MASTER-FILE.DDCTN-PAYEE TO #W3-DDCTN-PAYEE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt.setValue(iaa_Master_File_Ddctn_Per_Amt);                                                                                   //Natural: MOVE IAA-MASTER-FILE.DDCTN-PER-AMT TO #W3-DDCTN-PER-AMT
        pnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt.setValue(iaa_Master_File_Ddctn_Ytd_Amt);                                                                                   //Natural: MOVE IAA-MASTER-FILE.DDCTN-YTD-AMT TO #W3-DDCTN-YTD-AMT
        pnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte.setValue(iaa_Master_File_Ddctn_Pd_To_Dte);                                                                               //Natural: MOVE IAA-MASTER-FILE.DDCTN-PD-TO-DTE TO #W3-DDCTN-PD-TO-DTE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt.setValue(iaa_Master_File_Ddctn_Tot_Amt);                                                                                   //Natural: MOVE IAA-MASTER-FILE.DDCTN-TOT-AMT TO #W3-DDCTN-TOT-AMT
        pnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde.setValue(iaa_Master_File_Ddctn_Intent_Cde);                                                                             //Natural: MOVE IAA-MASTER-FILE.DDCTN-INTENT-CDE TO #W3-DDCTN-INTENT-CDE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte.setValue(iaa_Master_File_Ddctn_Strt_Dte);                                                                                 //Natural: MOVE IAA-MASTER-FILE.DDCTN-STRT-DTE TO #W3-DDCTN-STRT-DTE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte.setValue(iaa_Master_File_Ddctn_Stp_Dte);                                                                                   //Natural: MOVE IAA-MASTER-FILE.DDCTN-STP-DTE TO #W3-DDCTN-STP-DTE
        pnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte.setValue(iaa_Master_File_Ddctn_Final_Dte);                                                                               //Natural: MOVE IAA-MASTER-FILE.DDCTN-FINAL-DTE TO #W3-DDCTN-FINAL-DTE
        pnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte.setValue(iaa_Master_File_Lst_Trans_Dte);                                                                                   //Natural: MOVE IAA-MASTER-FILE.LST-TRANS-DTE TO #W3-LST-TRANS-DTE
        pnd_Work_Record_3_Pnd_W3_Filler.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #W3-FILLER
        getWorkFiles().write(1, false, pnd_Work_Record_3);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-3
        pnd_Count_Deduction_Extract.nadd(1);                                                                                                                              //Natural: ADD 1 TO #COUNT-DEDUCTION-EXTRACT
    }
    private void sub_Pnd_Write_Cpr_Extract() throws Exception                                                                                                             //Natural: #WRITE-CPR-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr.setValue(iaa_Master_File_Cntrct_Part_Ppcn_Nbr);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PART-PPCN-NBR TO #W2-PART-PPCN-NBR
        pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde.setValue(iaa_Master_File_Cntrct_Part_Payee_Cde);                                                                          //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PART-PAYEE-CDE TO #W2-PART-PAYEE-CDE
        pnd_Work_Record_2_Pnd_W2_Record_Cde.setValue(20);                                                                                                                 //Natural: MOVE 20 TO #W2-RECORD-CDE
        pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr.setValue(iaa_Master_File_Cpr_Id_Nbr);                                                                                         //Natural: MOVE IAA-MASTER-FILE.CPR-ID-NBR TO #W2-CPR-ID-NBR
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte.setValue(iaa_Master_File_Lst_Trans_Dte);                                                                                   //Natural: MOVE IAA-MASTER-FILE.LST-TRANS-DTE TO #W2-LST-TRANS-DTE
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde.setValue(iaa_Master_File_Prtcpnt_Ctznshp_Cde);                                                                       //Natural: MOVE IAA-MASTER-FILE.PRTCPNT-CTZNSHP-CDE TO #W2-PRTCPNT-CTZNSHP-CDE
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde.setValue(iaa_Master_File_Prtcpnt_Rsdncy_Cde);                                                                         //Natural: MOVE IAA-MASTER-FILE.PRTCPNT-RSDNCY-CDE TO #W2-PRTCPNT-RSDNCY-CDE
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw.setValue(iaa_Master_File_Prtcpnt_Rsdncy_Sw);                                                                           //Natural: MOVE IAA-MASTER-FILE.PRTCPNT-RSDNCY-SW TO #W2-PRTCPNT-RSDNCY-SW
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr.setValue(iaa_Master_File_Prtcpnt_Tax_Id_Nbr);                                                                         //Natural: MOVE IAA-MASTER-FILE.PRTCPNT-TAX-ID-NBR TO #W2-PRTCPNT-TAX-ID-NBR
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ.setValue(iaa_Master_File_Prtcpnt_Tax_Id_Typ);                                                                         //Natural: MOVE IAA-MASTER-FILE.PRTCPNT-TAX-ID-TYP TO #W2-PRTCPNT-TAX-ID-TYP
        pnd_Work_Record_2_Pnd_W2_Actvty_Cde.setValue(iaa_Master_File_Cntrct_Actvty_Cde);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-ACTVTY-CDE TO #W2-ACTVTY-CDE
        pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn.setValue(iaa_Master_File_Cntrct_Trmnte_Rsn);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-TRMNTE-RSN TO #W2-TRMNTE-RSN
        pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind.setValue(iaa_Master_File_Cntrct_Rwrttn_Ind);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RWRTTN-IND TO #W2-RWRTTN-IND
        pnd_Work_Record_2_Pnd_W2_Cash_Cde.setValue(iaa_Master_File_Cntrct_Cash_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-CASH-CDE TO #W2-CASH-CDE
        pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde.setValue(iaa_Master_File_Cntrct_Emplymnt_Trmnt_Cde);                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-EMPLYMNT-TRMNT-CDE TO #W2-EMPLYMNT-TRMNT-CDE
        pnd_Work_Record_2_Pnd_W2_Company_Cd.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Company_Cd.getValue(1,":",5));                                              //Natural: MOVE IAA-MASTER-FILE.CNTRCT-COMPANY-CD ( 1:5 ) TO #W2-COMPANY-CD ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Rcvry_Type_Ind.getValue(1,":",5));                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RCVRY-TYPE-IND ( 1:5 ) TO #W2-RCVRY-TYPE-IND ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Per_Ivc_Amt.getValue(1,":",5));                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PER-IVC-AMT ( 1:5 ) TO #W2-PER-IVC-AMT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Resdl_Ivc_Amt.getValue(1,":",5));                                        //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RESDL-IVC-AMT ( 1:5 ) TO #W2-RESDL-IVC-AMT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Ivc_Amt.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Ivc_Amt.getValue(1,":",5));                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-IVC-AMT ( 1:5 ) TO #W2-IVC-AMT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Ivc_Used_Amt.getValue(1,":",5));                                          //Natural: MOVE IAA-MASTER-FILE.CNTRCT-IVC-USED-AMT ( 1:5 ) TO #W2-IVC-USED-AMT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Rtb_Amt.getValue(1,":",5));                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RTB-AMT ( 1:5 ) TO #W2-RTB-AMT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Rtb_Percent.getValue(1,":",5).setValue(iaa_Master_File_Cntrct_Rtb_Percent.getValue(1,":",5));                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-RTB-PERCENT ( 1:5 ) TO #W2-RTB-PERCENT ( 1:5 )
        pnd_Work_Record_2_Pnd_W2_Mode_Ind.setValue(iaa_Master_File_Cntrct_Mode_Ind);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-MODE-IND TO #W2-MODE-IND
        pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte.setValue(iaa_Master_File_Cntrct_Wthdrwl_Dte);                                                                                //Natural: MOVE IAA-MASTER-FILE.CNTRCT-WTHDRWL-DTE TO #W2-WTHDRWL-DTE
        pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte.setValue(iaa_Master_File_Cntrct_Final_Per_Pay_Dte);                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FINAL-PER-PAY-DTE TO #W2-FINAL-PER-PAY-DTE
        pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte.setValue(iaa_Master_File_Cntrct_Final_Pay_Dte);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FINAL-PAY-DTE TO #W2-FINAL-PAY-DTE
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind.setValue(iaa_Master_File_Bnfcry_Xref_Ind);                                                                               //Natural: MOVE IAA-MASTER-FILE.BNFCRY-XREF-IND TO #W2-BNFCRY-XREF-IND
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte.setValue(iaa_Master_File_Bnfcry_Dod_Dte);                                                                                 //Natural: MOVE IAA-MASTER-FILE.BNFCRY-DOD-DTE TO #W2-BNFCRY-DOD-DTE
        pnd_Work_Record_2_Pnd_W2_Pend_Cde.setValue(iaa_Master_File_Cntrct_Pend_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PEND-CDE TO #W2-PEND-CDE
        pnd_Work_Record_2_Pnd_W2_Hold_Cde.setValue(iaa_Master_File_Cntrct_Hold_Cde);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-HOLD-CDE TO #W2-HOLD-CDE
        pnd_Work_Record_2_Pnd_W2_Pend_Dte.setValue(iaa_Master_File_Cntrct_Pend_Dte);                                                                                      //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PEND-DTE TO #W2-PEND-DTE
        pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde.setValue(iaa_Master_File_Cntrct_Prev_Dist_Cde);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-PREV-DIST-CDE TO #W2-PREV-DIST-CDE
        pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde.setValue(iaa_Master_File_Cntrct_Curr_Dist_Cde);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-CURR-DIST-CDE TO #W2-CURR-DIST-CDE
        pnd_Work_Record_2_Pnd_W2_Cmbne_Cde.setValue(iaa_Master_File_Cntrct_Cmbne_Cde);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-CMBNE-CDE TO #W2-CMBNE-CDE
        pnd_Work_Record_2_Pnd_W2_Spirt_Cde.setValue(iaa_Master_File_Cntrct_Spirt_Cde);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SPIRT-CDE TO #W2-SPIRT-CDE
        pnd_Work_Record_2_Pnd_W2_Spirt_Amt.setValue(iaa_Master_File_Cntrct_Spirt_Amt);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SPIRT-AMT TO #W2-SPIRT-AMT
        pnd_Work_Record_2_Pnd_W2_Spirt_Srce.setValue(iaa_Master_File_Cntrct_Spirt_Srce);                                                                                  //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SPIRT-SRCE TO #W2-SPIRT-SRCE
        pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte.setValue(iaa_Master_File_Cntrct_Spirt_Arr_Dte);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SPIRT-ARR-DTE TO #W2-SPIRT-ARR-DTE
        pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte.setValue(iaa_Master_File_Cntrct_Spirt_Prcss_Dte);                                                                        //Natural: MOVE IAA-MASTER-FILE.CNTRCT-SPIRT-PRCSS-DTE TO #W2-SPIRT-PRCSS-DTE
        pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt.setValue(iaa_Master_File_Cntrct_Fed_Tax_Amt);                                                                                //Natural: MOVE IAA-MASTER-FILE.CNTRCT-FED-TAX-AMT TO #W2-FED-TAX-AMT
        pnd_Work_Record_2_Pnd_W2_State_Cde.setValue(iaa_Master_File_Cntrct_State_Cde);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-STATE-CDE TO #W2-STATE-CDE
        pnd_Work_Record_2_Pnd_W2_State_Tax_Amt.setValue(iaa_Master_File_Cntrct_State_Tax_Amt);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-STATE-TAX-AMT TO #W2-STATE-TAX-AMT
        pnd_Work_Record_2_Pnd_W2_Local_Cde.setValue(iaa_Master_File_Cntrct_Local_Cde);                                                                                    //Natural: MOVE IAA-MASTER-FILE.CNTRCT-LOCAL-CDE TO #W2-LOCAL-CDE
        pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt.setValue(iaa_Master_File_Cntrct_Local_Tax_Amt);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-LOCAL-TAX-AMT TO #W2-LOCAL-TAX-AMT
        pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte.setValue(iaa_Master_File_Cntrct_Lst_Chnge_Dte);                                                                            //Natural: MOVE IAA-MASTER-FILE.CNTRCT-LST-CHNGE-DTE TO #W2-LST-CHNGE-DTE
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde.setValue(iaa_Master_File_Cpr_Xfr_Term_Cde);                                                                             //Natural: MOVE IAA-MASTER-FILE.CPR-XFR-TERM-CDE TO #W2-CPR-XFR-TERM-CDE
        pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde.setValue(iaa_Master_File_Cpr_Lgl_Res_Cde);                                                                               //Natural: MOVE IAA-MASTER-FILE.CPR-LGL-RES-CDE TO #W2-CPR-LGL-RES-CDE
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte.setValue(iaa_Master_File_Cpr_Xfr_Iss_Dte);                                                                               //Natural: MOVE IAA-MASTER-FILE.CPR-XFR-ISS-DTE TO #W2-CPR-XFR-ISS-DTE
        getWorkFiles().write(1, false, pnd_Work_Record_2);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-2
        pnd_Count_Cpr_Extract.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNT-CPR-EXTRACT
        if (condition(iaa_Master_File_Cntrct_Actvty_Cde.equals(9)))                                                                                                       //Natural: IF IAA-MASTER-FILE.CNTRCT-ACTVTY-CDE = 9
        {
            pnd_Count_Inactive_Payees.nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-INACTIVE-PAYEES
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Count_Active_Payees.nadd(1);                                                                                                                              //Natural: ADD 1 TO #COUNT-ACTIVE-PAYEES
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"TOTAL RECORDS");                                                                    //Natural: WRITE ( 1 ) 29X 'TOTAL RECORDS'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"  EXTRACTED");                                                                      //Natural: WRITE ( 1 ) 29X '  EXTRACTED'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(55),"EXCEPTION REPORT");                                                                    //Natural: WRITE ( 2 ) 55T 'EXCEPTION REPORT'
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(33),"ERROR FIELDS","-",new RepeatItem(40),new TabSetting(89),"------ ISN ------",new    //Natural: WRITE ( 2 ) '-' ( 33 ) 'ERROR FIELDS' '-' ( 40 ) 89T '------ ISN ------' 107T '----- ERROR MESSAGE ------'
                        TabSetting(107),"----- ERROR MESSAGE ------");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
