/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:37 PM
**        * FROM NATURAL PROGRAM : Iaap799a
************************************************************
**        * FILE NAME            : Iaap799a.java
**        * CLASS NAME           : Iaap799a
**        * INSTANCE NAME        : Iaap799a
************************************************************
**********************************************************************
* PROGRAM: IAAP799A                                                 *
* SYSTEM:  IA
* PURPOSE: THIS WILL GENERATE A MONTHLY REPORT OF PARTICIPANTS WHO ARE
*          100 + YEARS OLD.
* HISTORY:
* 03/15/2013 - ORIGINAL CODE - JUN TINIO
* 04/13/2015 - REPLACE READ COR-XREF BY READ OF COR ETL. SCAN 8/15
*              FOR CHANGES.
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap799a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Core_End_Of_File;
    private DbsField pnd_Cor_Rec;

    private DbsGroup pnd_Cor_Rec__R_Field_1;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot;

    private DbsGroup pnd_Cor_Rec__R_Field_2;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Payee;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Pin;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_Rec_Pnd_I_Cor_Dob;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Dod;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Sex_Cde;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Issue_Yyyy;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Iss_St;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Iss;
    private DbsField pnd_Input_Pnd_F3a;
    private DbsField pnd_Input_Pnd_First_Ann_Dob_Dte;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Dob_Yyyymm;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_1st_Dob_8;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob_Dte;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_2nd_Yyyymm;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_5;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_F;
    private DbsField pnd_Input_Pnd_F8;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_17;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_19;
    private DbsField pnd_Input_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_20;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_21;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_22;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Res;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_23;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Pnd_State_Cde;
    private DbsField pnd_Input_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Pnd_Local_Cde;
    private DbsField pnd_Input_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Ia_Dob;
    private DbsField pnd_Cor_Dob;
    private DbsField pnd_Ia_Age;
    private DbsField pnd_Cor_Age;
    private DbsField pnd_Age_Hold;
    private DbsField pnd_Mid_Init;
    private DbsField pnd_Ws_Bday;

    private DbsGroup pnd_Ws_Bday__R_Field_24;
    private DbsField pnd_Ws_Bday_Pnd_Ws_Bday_A;
    private DbsField pnd_Name;
    private DbsField pnd_H_Dob;
    private DbsField pnd_1st_Dob;

    private DbsGroup pnd_1st_Dob__R_Field_25;
    private DbsField pnd_1st_Dob_Pnd_1st_Dob_Yyyymm;
    private DbsField pnd_1st_Dob__Filler3;
    private DbsField pnd_2nd_Dob;

    private DbsGroup pnd_2nd_Dob__R_Field_26;
    private DbsField pnd_2nd_Dob_Pnd_2nd_Dob_Yyyymm;
    private DbsField pnd_2nd_Dob__Filler4;

    private DbsGroup pnd_2nd_Dob__R_Field_27;
    private DbsField pnd_2nd_Dob_Pnd_2nd_Dob_8;
    private DbsField pnd_1st_Dod;

    private DbsGroup pnd_1st_Dod__R_Field_28;
    private DbsField pnd_1st_Dod_Pnd_1st_Dod_Yyyy;
    private DbsField pnd_2nd_Dod;

    private DbsGroup pnd_2nd_Dod__R_Field_29;
    private DbsField pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy;
    private DbsField pnd_F_Dte;

    private DbsGroup pnd_F_Dte__R_Field_30;
    private DbsField pnd_F_Dte_Pnd_F_Yyyymm;
    private DbsField pnd_T_Dte;
    private DbsField pnd_W_Dte_10;
    private DbsField pnd_H_Mode;

    private DbsGroup pnd_H_Mode__R_Field_31;
    private DbsField pnd_H_Mode__Filler5;
    private DbsField pnd_H_Mode_Pnd_H_Mo;
    private DbsField pnd_Pin;
    private DbsField pnd_Dob;
    private DbsField pnd_Dt1;

    private DbsGroup pnd_Dt1__R_Field_32;
    private DbsField pnd_Dt1_Pnd_Yy1;
    private DbsField pnd_Dt1_Pnd_Mm1;

    private DbsGroup pnd_Dt1__R_Field_33;
    private DbsField pnd_Dt1_Pnd_Yyyymm;
    private DbsField pnd_Dt2;

    private DbsGroup pnd_Dt2__R_Field_34;
    private DbsField pnd_Dt2_Pnd_Yy2;
    private DbsField pnd_Dt2_Pnd_Mm2;
    private DbsField pnd_W_Dt1;
    private DbsField pnd_W_Yy1;
    private DbsField pnd_Mo;
    private DbsField pnd_I;
    private DbsField pnd_Pins;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Core_End_Of_File = localVariables.newFieldInRecord("pnd_Core_End_Of_File", "#CORE-END-OF-FILE", FieldType.BOOLEAN, 1);
        pnd_Cor_Rec = localVariables.newFieldInRecord("pnd_Cor_Rec", "#COR-REC", FieldType.STRING, 150);

        pnd_Cor_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Cor_Rec__R_Field_1", "REDEFINE", pnd_Cor_Rec);
        pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot", "#COR-CNTRCT-TOT", FieldType.STRING, 
            12);

        pnd_Cor_Rec__R_Field_2 = pnd_Cor_Rec__R_Field_1.newGroupInGroup("pnd_Cor_Rec__R_Field_2", "REDEFINE", pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot);
        pnd_Cor_Rec_Pnd_Cor_Cntrct = pnd_Cor_Rec__R_Field_2.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_Rec_Pnd_Cor_Payee = pnd_Cor_Rec__R_Field_2.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Pin = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);
        pnd_Cor_Rec_Pnd_Cor_Type_Cde = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Ssn = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_Rec_Pnd_I_Cor_Dob = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_I_Cor_Dob", "#I-COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Dod = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Last_Nm = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_First_Nm = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Middle_Nm = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Status_Cde = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Cor_Rec_Pnd_Cor_Sex_Cde = pnd_Cor_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_4 = pnd_Input__R_Field_3.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_6 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Yyyy = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Issue_Yyyy", "#ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Iss_St = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Iss_St);
        pnd_Input__Filler1 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_Iss = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Iss", "#ISS", FieldType.STRING, 2);
        pnd_Input_Pnd_F3a = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F3a", "#F3A", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob_Dte", "#FIRST-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_Dob_Yyyymm = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Dob_Yyyymm", "#DOB-YYYYMM", FieldType.STRING, 6);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_1st_Dob_8 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_1st_Dob_8", "#1ST-DOB-8", FieldType.STRING, 8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob_Dte", "#SCND-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Scnd_Ann_Dob_Dte);
        pnd_Input_Pnd_2nd_Yyyymm = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_2nd_Yyyymm", "#2ND-YYYYMM", FieldType.STRING, 6);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde_5 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_5", "#PPG-CDE-5", FieldType.STRING, 5);

        pnd_Input__R_Field_14 = pnd_Input__R_Field_6.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Ppg_Cde_5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Ppg_Cde_F = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_F", "#PPG-CDE-F", FieldType.STRING, 1);
        pnd_Input_Pnd_F8 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 8);

        pnd_Input__R_Field_15 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_16 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_17 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_17", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_18 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_18", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);

        pnd_Input__R_Field_19 = pnd_Input__R_Field_18.newGroupInGroup("pnd_Input__R_Field_19", "REDEFINE", pnd_Input_Pnd_Summ_Fund_Cde);
        pnd_Input_Fund_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_20 = pnd_Input__R_Field_18.newGroupInGroup("pnd_Input__R_Field_20", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_21 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_21", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_22 = pnd_Input__R_Field_21.newGroupInGroup("pnd_Input__R_Field_22", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler2 = pnd_Input__R_Field_22.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_Res = pnd_Input__R_Field_22.newFieldInGroup("pnd_Input_Pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Trmnte_Rsn = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_23 = pnd_Input__R_Field_21.newGroupInGroup("pnd_Input__R_Field_23", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_23.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_Spirt_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Amt = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Input_Pnd_Spirt_Srce = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Arr_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Input_Pnd_Spirt_Prcss_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Fed_Tax_Amt = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Pnd_State_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_State_Tax_Amt = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Local_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Local_Tax_Amt = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Lst_Chnge_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cpr_Xfr_Term_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cpr_Lgl_Res_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_21.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ia_Dob = localVariables.newFieldInRecord("pnd_Ia_Dob", "#IA-DOB", FieldType.STRING, 10);
        pnd_Cor_Dob = localVariables.newFieldInRecord("pnd_Cor_Dob", "#COR-DOB", FieldType.STRING, 10);
        pnd_Ia_Age = localVariables.newFieldInRecord("pnd_Ia_Age", "#IA-AGE", FieldType.NUMERIC, 4);
        pnd_Cor_Age = localVariables.newFieldInRecord("pnd_Cor_Age", "#COR-AGE", FieldType.NUMERIC, 4);
        pnd_Age_Hold = localVariables.newFieldInRecord("pnd_Age_Hold", "#AGE-HOLD", FieldType.NUMERIC, 4);
        pnd_Mid_Init = localVariables.newFieldInRecord("pnd_Mid_Init", "#MID-INIT", FieldType.STRING, 1);
        pnd_Ws_Bday = localVariables.newFieldInRecord("pnd_Ws_Bday", "#WS-BDAY", FieldType.NUMERIC, 8);

        pnd_Ws_Bday__R_Field_24 = localVariables.newGroupInRecord("pnd_Ws_Bday__R_Field_24", "REDEFINE", pnd_Ws_Bday);
        pnd_Ws_Bday_Pnd_Ws_Bday_A = pnd_Ws_Bday__R_Field_24.newFieldInGroup("pnd_Ws_Bday_Pnd_Ws_Bday_A", "#WS-BDAY-A", FieldType.STRING, 8);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 40);
        pnd_H_Dob = localVariables.newFieldInRecord("pnd_H_Dob", "#H-DOB", FieldType.NUMERIC, 8);
        pnd_1st_Dob = localVariables.newFieldInRecord("pnd_1st_Dob", "#1ST-DOB", FieldType.NUMERIC, 8);

        pnd_1st_Dob__R_Field_25 = localVariables.newGroupInRecord("pnd_1st_Dob__R_Field_25", "REDEFINE", pnd_1st_Dob);
        pnd_1st_Dob_Pnd_1st_Dob_Yyyymm = pnd_1st_Dob__R_Field_25.newFieldInGroup("pnd_1st_Dob_Pnd_1st_Dob_Yyyymm", "#1ST-DOB-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_1st_Dob__Filler3 = pnd_1st_Dob__R_Field_25.newFieldInGroup("pnd_1st_Dob__Filler3", "_FILLER3", FieldType.STRING, 2);
        pnd_2nd_Dob = localVariables.newFieldInRecord("pnd_2nd_Dob", "#2ND-DOB", FieldType.NUMERIC, 8);

        pnd_2nd_Dob__R_Field_26 = localVariables.newGroupInRecord("pnd_2nd_Dob__R_Field_26", "REDEFINE", pnd_2nd_Dob);
        pnd_2nd_Dob_Pnd_2nd_Dob_Yyyymm = pnd_2nd_Dob__R_Field_26.newFieldInGroup("pnd_2nd_Dob_Pnd_2nd_Dob_Yyyymm", "#2ND-DOB-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_2nd_Dob__Filler4 = pnd_2nd_Dob__R_Field_26.newFieldInGroup("pnd_2nd_Dob__Filler4", "_FILLER4", FieldType.STRING, 2);

        pnd_2nd_Dob__R_Field_27 = localVariables.newGroupInRecord("pnd_2nd_Dob__R_Field_27", "REDEFINE", pnd_2nd_Dob);
        pnd_2nd_Dob_Pnd_2nd_Dob_8 = pnd_2nd_Dob__R_Field_27.newFieldInGroup("pnd_2nd_Dob_Pnd_2nd_Dob_8", "#2ND-DOB-8", FieldType.STRING, 8);
        pnd_1st_Dod = localVariables.newFieldInRecord("pnd_1st_Dod", "#1ST-DOD", FieldType.NUMERIC, 6);

        pnd_1st_Dod__R_Field_28 = localVariables.newGroupInRecord("pnd_1st_Dod__R_Field_28", "REDEFINE", pnd_1st_Dod);
        pnd_1st_Dod_Pnd_1st_Dod_Yyyy = pnd_1st_Dod__R_Field_28.newFieldInGroup("pnd_1st_Dod_Pnd_1st_Dod_Yyyy", "#1ST-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_2nd_Dod = localVariables.newFieldInRecord("pnd_2nd_Dod", "#2ND-DOD", FieldType.NUMERIC, 6);

        pnd_2nd_Dod__R_Field_29 = localVariables.newGroupInRecord("pnd_2nd_Dod__R_Field_29", "REDEFINE", pnd_2nd_Dod);
        pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy = pnd_2nd_Dod__R_Field_29.newFieldInGroup("pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy", "#2ND-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_F_Dte = localVariables.newFieldInRecord("pnd_F_Dte", "#F-DTE", FieldType.STRING, 8);

        pnd_F_Dte__R_Field_30 = localVariables.newGroupInRecord("pnd_F_Dte__R_Field_30", "REDEFINE", pnd_F_Dte);
        pnd_F_Dte_Pnd_F_Yyyymm = pnd_F_Dte__R_Field_30.newFieldInGroup("pnd_F_Dte_Pnd_F_Yyyymm", "#F-YYYYMM", FieldType.NUMERIC, 6);
        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.DATE);
        pnd_W_Dte_10 = localVariables.newFieldInRecord("pnd_W_Dte_10", "#W-DTE-10", FieldType.STRING, 10);
        pnd_H_Mode = localVariables.newFieldInRecord("pnd_H_Mode", "#H-MODE", FieldType.NUMERIC, 3);

        pnd_H_Mode__R_Field_31 = localVariables.newGroupInRecord("pnd_H_Mode__R_Field_31", "REDEFINE", pnd_H_Mode);
        pnd_H_Mode__Filler5 = pnd_H_Mode__R_Field_31.newFieldInGroup("pnd_H_Mode__Filler5", "_FILLER5", FieldType.STRING, 1);
        pnd_H_Mode_Pnd_H_Mo = pnd_H_Mode__R_Field_31.newFieldInGroup("pnd_H_Mode_Pnd_H_Mo", "#H-MO", FieldType.NUMERIC, 2);
        pnd_Pin = localVariables.newFieldInRecord("pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 10);
        pnd_Dt1 = localVariables.newFieldInRecord("pnd_Dt1", "#DT1", FieldType.STRING, 6);

        pnd_Dt1__R_Field_32 = localVariables.newGroupInRecord("pnd_Dt1__R_Field_32", "REDEFINE", pnd_Dt1);
        pnd_Dt1_Pnd_Yy1 = pnd_Dt1__R_Field_32.newFieldInGroup("pnd_Dt1_Pnd_Yy1", "#YY1", FieldType.NUMERIC, 4);
        pnd_Dt1_Pnd_Mm1 = pnd_Dt1__R_Field_32.newFieldInGroup("pnd_Dt1_Pnd_Mm1", "#MM1", FieldType.NUMERIC, 2);

        pnd_Dt1__R_Field_33 = localVariables.newGroupInRecord("pnd_Dt1__R_Field_33", "REDEFINE", pnd_Dt1);
        pnd_Dt1_Pnd_Yyyymm = pnd_Dt1__R_Field_33.newFieldInGroup("pnd_Dt1_Pnd_Yyyymm", "#YYYYMM", FieldType.NUMERIC, 6);
        pnd_Dt2 = localVariables.newFieldInRecord("pnd_Dt2", "#DT2", FieldType.STRING, 6);

        pnd_Dt2__R_Field_34 = localVariables.newGroupInRecord("pnd_Dt2__R_Field_34", "REDEFINE", pnd_Dt2);
        pnd_Dt2_Pnd_Yy2 = pnd_Dt2__R_Field_34.newFieldInGroup("pnd_Dt2_Pnd_Yy2", "#YY2", FieldType.NUMERIC, 4);
        pnd_Dt2_Pnd_Mm2 = pnd_Dt2__R_Field_34.newFieldInGroup("pnd_Dt2_Pnd_Mm2", "#MM2", FieldType.NUMERIC, 2);
        pnd_W_Dt1 = localVariables.newFieldInRecord("pnd_W_Dt1", "#W-DT1", FieldType.STRING, 6);
        pnd_W_Yy1 = localVariables.newFieldInRecord("pnd_W_Yy1", "#W-YY1", FieldType.NUMERIC, 4);
        pnd_Mo = localVariables.newFieldInRecord("pnd_Mo", "#MO", FieldType.NUMERIC, 1, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Pins = localVariables.newFieldArrayInRecord("pnd_Pins", "#PINS", FieldType.NUMERIC, 12, new DbsArrayController(1, 50000));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap799a() throws Exception
    {
        super("Iaap799a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 180 PS = 0
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 180 PS = 0
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20))))                                                               //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_1st_Dob.setValue(pnd_Input_Pnd_First_Ann_Dob_Dte);                                                                                                    //Natural: ASSIGN #1ST-DOB := #FIRST-ANN-DOB-DTE
                pnd_2nd_Dob.setValue(pnd_Input_Pnd_Scnd_Ann_Dob_Dte);                                                                                                     //Natural: ASSIGN #2ND-DOB := #SCND-ANN-DOB-DTE
                pnd_1st_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                        //Natural: ASSIGN #1ST-DOD := #FIRST-ANN-DOD
                pnd_2nd_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                         //Natural: ASSIGN #2ND-DOD := #SCND-ANN-DOD
                pnd_Dt2.reset();                                                                                                                                          //Natural: RESET #DT2 #IA-DOB #COR-DOB #IA-AGE #COR-AGE #H-DOB
                pnd_Ia_Dob.reset();
                pnd_Cor_Dob.reset();
                pnd_Ia_Age.reset();
                pnd_Cor_Age.reset();
                pnd_H_Dob.reset();
                if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_1st_Dob_8,"YYYYMMDD") && pnd_Input_Pnd_First_Ann_Dob_Dte.greater(18000000)))                              //Natural: IF #1ST-DOB-8 = MASK ( YYYYMMDD ) AND #FIRST-ANN-DOB-DTE GT 18000000
                {
                    pnd_Dt2.setValue(pnd_Input_Pnd_Dob_Yyyymm);                                                                                                           //Natural: ASSIGN #DT2 := #DOB-YYYYMM
                    pnd_H_Dob.setValue(pnd_Input_Pnd_First_Ann_Dob_Dte);                                                                                                  //Natural: ASSIGN #H-DOB := #FIRST-ANN-DOB-DTE
                    pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Pnd_1st_Dob_8);                                                                     //Natural: MOVE EDITED #1ST-DOB-8 TO #T-DTE ( EM = YYYYMMDD )
                    pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                                   //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                    pnd_Ia_Dob.setValue(pnd_Dob);                                                                                                                         //Natural: ASSIGN #IA-DOB := #DOB
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                if (condition(pnd_Input_Pnd_Status_Cde.notEquals(1) || pnd_Input_Pnd_Pend_Cde.notEquals("0")))                                                            //Natural: IF #STATUS-CDE NE 1 OR #PEND-CDE NE '0'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  082017
                if (condition(pnd_Input_Pnd_Cpr_Id_Nbr.equals(pnd_Pins.getValue("*")) && pnd_Input_Pnd_Cpr_Id_Nbr.greater(getZero()) || pnd_Input_Pnd_Cpr_Id_Nbr.equals(new  //Natural: IF #CPR-ID-NBR = #PINS ( * ) AND #CPR-ID-NBR GT 0 OR #CPR-ID-NBR = 999999999999
                    DbsDecimal("999999999999"))))
                {
                    //*        OR #CPR-ID-NBR = 9999999
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Payee_Cde.equals(2)))                                                                                                         //Natural: IF #PAYEE-CDE = 02
                {
                    if (condition(pnd_1st_Dod.greater(getZero())))                                                                                                        //Natural: IF #1ST-DOD GT 0
                    {
                        if (condition(DbsUtil.maskMatches(pnd_2nd_Dob_Pnd_2nd_Dob_8,"YYYYMMDD") && pnd_2nd_Dob.greater(18000000)))                                        //Natural: IF #2ND-DOB-8 = MASK ( YYYYMMDD ) AND #2ND-DOB GT 18000000
                        {
                            pnd_Dt2.setValue(pnd_2nd_Dob_Pnd_2nd_Dob_Yyyymm);                                                                                             //Natural: ASSIGN #DT2 := #2ND-DOB-YYYYMM
                            pnd_H_Dob.setValue(pnd_2nd_Dob);                                                                                                              //Natural: ASSIGN #H-DOB := #2ND-DOB
                            pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_2nd_Dob_Pnd_2nd_Dob_8);                                                           //Natural: MOVE EDITED #2ND-DOB-8 TO #T-DTE ( EM = YYYYMMDD )
                            pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                           //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                            pnd_Ia_Dob.setValue(pnd_Dob);                                                                                                                 //Natural: ASSIGN #IA-DOB := #DOB
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Dt2.reset();                                                                                                                              //Natural: RESET #DT2 #H-DOB #IA-DOB #IA-AGE
                            pnd_H_Dob.reset();
                            pnd_Ia_Dob.reset();
                            pnd_Ia_Age.reset();
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Payee_Cde.greater(2)))                                                                                                        //Natural: IF #PAYEE-CDE GT 02
                {
                    pnd_Dt2.reset();                                                                                                                                      //Natural: RESET #DT2 #IA-DOB #IA-AGE #H-DOB
                    pnd_Ia_Dob.reset();
                    pnd_Ia_Age.reset();
                    pnd_H_Dob.reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Dt2.greater(" ")))                                                                                                                      //Natural: IF #DT2 GT ' '
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGE
                    sub_Calculate_Age();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ia_Age.setValue(pnd_Dt1_Pnd_Yy1);                                                                                                                 //Natural: ASSIGN #IA-AGE := #YY1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pin.setValue(pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                                               //Natural: ASSIGN #PIN := #CPR-ID-NBR
                                                                                                                                                                          //Natural: PERFORM GET-NAME
                sub_Get_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ia_Age.greaterOrEqual(100) || pnd_Cor_Age.greaterOrEqual(100)))                                                                         //Natural: IF #IA-AGE GE 100 OR #COR-AGE GE 100
                {
                    if (condition(pnd_Cor_Dob.equals(" ")))                                                                                                               //Natural: IF #COR-DOB = ' '
                    {
                        pnd_Cor_Dob.setValue(pnd_Ia_Dob);                                                                                                                 //Natural: ASSIGN #COR-DOB := #IA-DOB
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Cor_Age.equals(getZero())))                                                                                                         //Natural: IF #COR-AGE = 0
                    {
                        pnd_Cor_Age.setValue(pnd_Ia_Age);                                                                                                                 //Natural: ASSIGN #COR-AGE := #IA-AGE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Name.equals(" ")))                                                                                                                  //Natural: IF #NAME = ' '
                    {
                        pnd_Name.setValue("COR RECORD NOT FOUND");                                                                                                        //Natural: ASSIGN #NAME := 'COR RECORD NOT FOUND'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Pin.equals(pnd_Pins.getValue("*"))))                                                                                                //Natural: IF #PIN = #PINS ( * )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_I.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #I
                        pnd_Pins.getValue(pnd_I).setValue(pnd_Pin);                                                                                                       //Natural: ASSIGN #PINS ( #I ) := #PIN
                        getReports().display(1, ReportOption.NOTITLE,"PIN",                                                                                               //Natural: DISPLAY ( 1 ) NOTITLE 'PIN' #PIN 'Name' #NAME 'IA DOB' #IA-DOB 'IA Age' #IA-AGE 'COR DOB' #COR-DOB 'COR Age' #COR-AGE
                        		pnd_Pin,"Name",
                        		pnd_Name,"IA DOB",
                        		pnd_Ia_Dob,"IA Age",
                        		pnd_Ia_Age,"COR DOB",
                        		pnd_Cor_Dob,"COR Age",
                        		pnd_Cor_Age);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, " ");                                                                                                                                       //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL PARTICIPANT COUNT:",pnd_I, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                                        //Natural: WRITE ( 1 ) 'TOTAL PARTICIPANT COUNT:' #I ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-AGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //*        #F-DTE := PH-DOB-DTE
    }
    private void sub_Calculate_Age() throws Exception                                                                                                                     //Natural: CALCULATE-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Dt1.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #DT1
        pnd_Dt1_Pnd_Yy1.nsubtract(pnd_Dt2_Pnd_Yy2);                                                                                                                       //Natural: SUBTRACT #YY2 FROM #YY1
        if (condition(pnd_Dt2_Pnd_Mm2.greater(pnd_Dt1_Pnd_Mm1)))                                                                                                          //Natural: IF #MM2 GT #MM1
        {
            pnd_Dt1_Pnd_Yy1.nsubtract(1);                                                                                                                                 //Natural: SUBTRACT 1 FROM #YY1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dt1_Pnd_Mm1.nsubtract(pnd_Dt2_Pnd_Mm2);                                                                                                                       //Natural: SUBTRACT #MM2 FROM #MM1
        if (condition(pnd_Dt1_Pnd_Mm1.lessOrEqual(getZero())))                                                                                                            //Natural: IF #MM1 LE 0
        {
            pnd_Dt1_Pnd_Mm1.nadd(12);                                                                                                                                     //Natural: ADD 12 TO #MM1
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Dt1_Pnd_Mm1.equals(12) || pnd_Dt1_Pnd_Mm1.equals(getZero())))                                                                                   //Natural: IF #MM1 = 12 OR = 0
        {
            pnd_Mo.reset();                                                                                                                                               //Natural: RESET #MO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mo.compute(new ComputeParameters(true, pnd_Mo), pnd_Dt1_Pnd_Mm1.divide(12));                                                                                  //Natural: COMPUTE ROUNDED #MO = #MM1 / 12
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *#REC-TYPE := 01 /* 8/15
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME #COR-AGE #COR-DOB
        pnd_Cor_Age.reset();
        pnd_Cor_Dob.reset();
        if (condition(pnd_Core_End_Of_File.getBoolean()))                                                                                                                 //Natural: IF #CORE-END-OF-FILE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * #PIN NE 9999999 AND #PIN GT 0                 /* 082017
        //*  082017
        if (condition(pnd_Pin.notEquals(new DbsDecimal("999999999999")) && pnd_Pin.greater(getZero())))                                                                   //Natural: IF #PIN NE 999999999999 AND #PIN GT 0
        {
            //*  8/15 - START
            //*   READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE STARTING FROM #PIN-KEY
            //*     IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE #PIN OR
            //*         COR-XREF-PH.PH-RCD-TYPE-CDE NE 01
            //*       ESCAPE BOTTOM
            //*     END-IF
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Cor_Rec_Pnd_Cor_Status_Cde.notEquals(" ")))                                                                                             //Natural: IF #COR-STATUS-CDE NE ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                    //Natural: IF #COR-CNTRCT-TOT GT #CNTRCT-PAYEE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                     //Natural: IF #COR-CNTRCT-TOT = #CNTRCT-PAYEE
                    {
                        //*      COMPRESS PH-FIRST-NME PH-MDDLE-INIT PH-LAST-NME INTO #NAME
                        pnd_Name.setValue(DbsUtil.compress(pnd_Cor_Rec_Pnd_Cor_First_Nm, pnd_Cor_Rec_Pnd_Cor_Middle_Nm, pnd_Cor_Rec_Pnd_Cor_Last_Nm));                    //Natural: COMPRESS #COR-FIRST-NM #COR-MIDDLE-NM #COR-LAST-NM INTO #NAME
                        //*      IF (#H-DOB NE PH-DOB-DTE AND PH-DOB-DTE GT 18000000) OR
                        if (condition((pnd_H_Dob.notEquals(pnd_Cor_Rec_Pnd_I_Cor_Dob) && pnd_Cor_Rec_Pnd_I_Cor_Dob.greater(18000000)) || pnd_Input_Pnd_Payee_Cde.greater(2))) //Natural: IF ( #H-DOB NE #I-COR-DOB AND #I-COR-DOB GT 18000000 ) OR #PAYEE-CDE GT 02
                        {
                            pnd_F_Dte.setValue(pnd_Cor_Rec_Pnd_I_Cor_Dob);                                                                                                //Natural: ASSIGN #F-DTE := #I-COR-DOB
                            if (condition(DbsUtil.maskMatches(pnd_F_Dte,"YYYYMMDD")))                                                                                     //Natural: IF #F-DTE = MASK ( YYYYMMDD )
                            {
                                pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_F_Dte);                                                                       //Natural: MOVE EDITED #F-DTE TO #T-DTE ( EM = YYYYMMDD )
                                pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                       //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                                pnd_Cor_Dob.setValue(pnd_Dob);                                                                                                            //Natural: ASSIGN #COR-DOB := #DOB
                                pnd_Dt2.setValue(pnd_F_Dte_Pnd_F_Yyyymm);                                                                                                 //Natural: ASSIGN #DT2 := #F-YYYYMM
                                pnd_Dt1.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #DT1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGE
                                sub_Calculate_Age();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Cor_Age.setValue(pnd_Dt1_Pnd_Yy1);                                                                                                    //Natural: ASSIGN #COR-AGE := #YY1
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Dt1_Pnd_Yy1.reset();                                                                                                                  //Natural: RESET #YY1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().read(2, pnd_Cor_Rec);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #COR-REC
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                    pnd_Core_End_Of_File.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #CORE-END-OF-FILE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-ENDFILE
                if (condition(pnd_Cor_Rec_Pnd_Cor_Status_Cde.notEquals(" ")))                                                                                             //Natural: IF #COR-STATUS-CDE NE ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                         //Natural: IF #COR-CNTRCT-TOT = #CNTRCT-PAYEE
                {
                    pnd_Name.setValue(DbsUtil.compress(pnd_Cor_Rec_Pnd_Cor_First_Nm, pnd_Cor_Rec_Pnd_Cor_Middle_Nm, pnd_Cor_Rec_Pnd_Cor_Last_Nm));                        //Natural: COMPRESS #COR-FIRST-NM #COR-MIDDLE-NM #COR-LAST-NM INTO #NAME
                    if (condition((pnd_H_Dob.notEquals(pnd_Cor_Rec_Pnd_I_Cor_Dob) && pnd_Cor_Rec_Pnd_I_Cor_Dob.greater(18000000)) || pnd_Input_Pnd_Payee_Cde.greater(2))) //Natural: IF ( #H-DOB NE #I-COR-DOB AND #I-COR-DOB GT 18000000 ) OR #PAYEE-CDE GT 02
                    {
                        pnd_F_Dte.setValue(pnd_Cor_Rec_Pnd_I_Cor_Dob);                                                                                                    //Natural: ASSIGN #F-DTE := #I-COR-DOB
                        if (condition(DbsUtil.maskMatches(pnd_F_Dte,"YYYYMMDD")))                                                                                         //Natural: IF #F-DTE = MASK ( YYYYMMDD )
                        {
                            pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_F_Dte);                                                                           //Natural: MOVE EDITED #F-DTE TO #T-DTE ( EM = YYYYMMDD )
                            pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                           //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                            pnd_Cor_Dob.setValue(pnd_Dob);                                                                                                                //Natural: ASSIGN #COR-DOB := #DOB
                            pnd_Dt2.setValue(pnd_F_Dte_Pnd_F_Yyyymm);                                                                                                     //Natural: ASSIGN #DT2 := #F-YYYYMM
                            pnd_Dt1.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #DT1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGE
                            sub_Calculate_Age();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Cor_Age.setValue(pnd_Dt1_Pnd_Yy1);                                                                                                        //Natural: ASSIGN #COR-AGE := #YY1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Dt1_Pnd_Yy1.reset();                                                                                                                      //Natural: RESET #YY1
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*   END-READ
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*  8/15 - END
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(16),"MONTHLY REPORT OF IA PARTICIPANTS AGED 100 +",new ColumnSpacing(16),Global.getDATX(),  //Natural: WRITE ( 1 ) *PROGRAM 16X 'MONTHLY REPORT OF IA PARTICIPANTS AGED 100 +' 16X *DATX ( EM = MM/DD/YYYY )
                        new ReportEditMask ("MM/DD/YYYY"));
                    getReports().write(1, " ");                                                                                                                           //Natural: WRITE ( 1 ) ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=180 PS=0");
        Global.format(2, "LS=180 PS=0");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"PIN",
        		pnd_Pin,"Name",
        		pnd_Name,"IA DOB",
        		pnd_Ia_Dob,"IA Age",
        		pnd_Ia_Age,"COR DOB",
        		pnd_Cor_Dob,"COR Age",
        		pnd_Cor_Age);
    }
}
