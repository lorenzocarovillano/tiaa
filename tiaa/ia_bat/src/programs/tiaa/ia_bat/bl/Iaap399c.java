/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:27:11 PM
**        * FROM NATURAL PROGRAM : Iaap399c
************************************************************
**        * FILE NAME            : Iaap399c.java
**        * CLASS NAME           : Iaap399c
**        * INSTANCE NAME        : Iaap399c
************************************************************
************************************************************************
* PROGRAM - IAAP399C -- CLONE OF IAAP399                               *
* DATE    - 12/16/2016                                                 *
* FORMATS THE DAILY FILE TO RMW TO ADD MONTHLY AUV AND AMOUNT          *
* FOR FRAUD PREVENTION/AML. ALSO POPULATE AUV AND AMOUNT FOR           *
* MONTHLY REVALUED FUNDS.                                              *
*                                                                      *
* HISTORY                                                              *
* 12/16/2016 JUN TINIO
*
* 06/05/2017 : PIN EXPANSION. SCAN 082017 FOR CHANGES.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap399c extends BLNatBase
{
    // Data Areas
    private LdaIaal399 ldaIaal399;
    private LdaIaal050 ldaIaal050;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Record_Cnt_10;
    private DbsField pnd_Record_Cnt_20;
    private DbsField pnd_Record_Cnt_30;
    private DbsField pnd_Record_Cnt_40;
    private DbsField pnd_Record_Cnt_All;
    private DbsField pnd_Record_Cnt_Rest;
    private DbsField pnd_I;
    private DbsField pnd_Parm_Fund_1;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Rtn_Cde;
    private DbsField pnd_Call_Type;
    private DbsField pnd_Hdr_Chck_Dte;
    private DbsField pnd_Per_Div_Amt;

    private DbsGroup pnd_Per_Div_Amt__R_Field_1;
    private DbsField pnd_Per_Div_Amt_Pnd_Per_Auv;
    private DbsField pnd_W_Dte;

    private DbsGroup pnd_W_Dte__R_Field_2;
    private DbsField pnd_W_Dte_Pnd_W_Yyyy;
    private DbsField pnd_W_Dte_Pnd_W_Mm;
    private DbsField pnd_W_Dte_Pnd_W_Dd;
    private DbsField pnd_Reval_Dte;
    private DbsField pnd_R_Dte_A;

    private DbsGroup pnd_R_Dte_A__R_Field_3;
    private DbsField pnd_R_Dte_A_Pnd_R_Yyyy;
    private DbsField pnd_R_Dte_A_Pnd_R_Mm;
    private DbsField pnd_R_Dte_A_Pnd_R_Dd;
    private DbsField pnd_Rc;
    private DbsField pnd_Rec_30;

    private DbsGroup pnd_Rec_30__R_Field_4;
    private DbsField pnd_Rec_30_Pnd_W_Xfr_In_Dte;
    private DbsField pnd_Rec_30_Pnd_Auv_Date;
    private DbsField pnd_S_Pin;

    private DataAccessProgramView vw_iaa_Cpr;
    private DbsField iaa_Cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Cntrct_Local_Tax_Amt;

    private DbsGroup iaa_Cpr_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Cntrct_Rtb_Amt;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal399 = new LdaIaal399();
        registerRecord(ldaIaal399);
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_Record_Cnt_10 = localVariables.newFieldInRecord("pnd_Record_Cnt_10", "#RECORD-CNT-10", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_20 = localVariables.newFieldInRecord("pnd_Record_Cnt_20", "#RECORD-CNT-20", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_30 = localVariables.newFieldInRecord("pnd_Record_Cnt_30", "#RECORD-CNT-30", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_40 = localVariables.newFieldInRecord("pnd_Record_Cnt_40", "#RECORD-CNT-40", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_All = localVariables.newFieldInRecord("pnd_Record_Cnt_All", "#RECORD-CNT-ALL", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_Rest = localVariables.newFieldInRecord("pnd_Record_Cnt_Rest", "#RECORD-CNT-REST", FieldType.NUMERIC, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Parm_Fund_1 = localVariables.newFieldInRecord("pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);
        pnd_Parm_Fund_2 = localVariables.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Rtn_Cde = localVariables.newFieldInRecord("pnd_Parm_Rtn_Cde", "#PARM-RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Call_Type = localVariables.newFieldInRecord("pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        pnd_Hdr_Chck_Dte = localVariables.newFieldInRecord("pnd_Hdr_Chck_Dte", "#HDR-CHCK-DTE", FieldType.STRING, 8);
        pnd_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Per_Div_Amt", "#PER-DIV-AMT", FieldType.NUMERIC, 9, 2);

        pnd_Per_Div_Amt__R_Field_1 = localVariables.newGroupInRecord("pnd_Per_Div_Amt__R_Field_1", "REDEFINE", pnd_Per_Div_Amt);
        pnd_Per_Div_Amt_Pnd_Per_Auv = pnd_Per_Div_Amt__R_Field_1.newFieldInGroup("pnd_Per_Div_Amt_Pnd_Per_Auv", "#PER-AUV", FieldType.NUMERIC, 9, 4);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);

        pnd_W_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Dte__R_Field_2", "REDEFINE", pnd_W_Dte);
        pnd_W_Dte_Pnd_W_Yyyy = pnd_W_Dte__R_Field_2.newFieldInGroup("pnd_W_Dte_Pnd_W_Yyyy", "#W-YYYY", FieldType.STRING, 4);
        pnd_W_Dte_Pnd_W_Mm = pnd_W_Dte__R_Field_2.newFieldInGroup("pnd_W_Dte_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_W_Dte_Pnd_W_Dd = pnd_W_Dte__R_Field_2.newFieldInGroup("pnd_W_Dte_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);
        pnd_Reval_Dte = localVariables.newFieldInRecord("pnd_Reval_Dte", "#REVAL-DTE", FieldType.DATE);
        pnd_R_Dte_A = localVariables.newFieldInRecord("pnd_R_Dte_A", "#R-DTE-A", FieldType.STRING, 8);

        pnd_R_Dte_A__R_Field_3 = localVariables.newGroupInRecord("pnd_R_Dte_A__R_Field_3", "REDEFINE", pnd_R_Dte_A);
        pnd_R_Dte_A_Pnd_R_Yyyy = pnd_R_Dte_A__R_Field_3.newFieldInGroup("pnd_R_Dte_A_Pnd_R_Yyyy", "#R-YYYY", FieldType.NUMERIC, 4);
        pnd_R_Dte_A_Pnd_R_Mm = pnd_R_Dte_A__R_Field_3.newFieldInGroup("pnd_R_Dte_A_Pnd_R_Mm", "#R-MM", FieldType.NUMERIC, 2);
        pnd_R_Dte_A_Pnd_R_Dd = pnd_R_Dte_A__R_Field_3.newFieldInGroup("pnd_R_Dte_A_Pnd_R_Dd", "#R-DD", FieldType.NUMERIC, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Rec_30 = localVariables.newFieldInRecord("pnd_Rec_30", "#REC-30", FieldType.STRING, 231);

        pnd_Rec_30__R_Field_4 = localVariables.newGroupInRecord("pnd_Rec_30__R_Field_4", "REDEFINE", pnd_Rec_30);
        pnd_Rec_30_Pnd_W_Xfr_In_Dte = pnd_Rec_30__R_Field_4.newFieldInGroup("pnd_Rec_30_Pnd_W_Xfr_In_Dte", "#W-XFR-IN-DTE", FieldType.STRING, 8);
        pnd_Rec_30_Pnd_Auv_Date = pnd_Rec_30__R_Field_4.newFieldInGroup("pnd_Rec_30_Pnd_Auv_Date", "#AUV-DATE", FieldType.STRING, 8);
        pnd_S_Pin = localVariables.newFieldInRecord("pnd_S_Pin", "#S-PIN", FieldType.NUMERIC, 12);

        vw_iaa_Cpr = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr", "IAA-CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cpr_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Cntrct_Part_Payee_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Cntrct_State_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        iaa_Cpr_Cntrct_State_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Cntrct_Local_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Cntrct_Local_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");

        iaa_Cpr_Cntrct_Company_Data = vw_iaa_Cpr.getRecord().newGroupInGroup("iaa_Cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Cntrct_Rtb_Amt = iaa_Cpr_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        registerRecord(vw_iaa_Cpr);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cpr.reset();

        ldaIaal399.initializeValues();
        ldaIaal050.initializeValues();

        localVariables.reset();
        pnd_Record_Cnt_10.setInitialValue(0);
        pnd_Record_Cnt_20.setInitialValue(0);
        pnd_Record_Cnt_30.setInitialValue(0);
        pnd_Record_Cnt_40.setInitialValue(0);
        pnd_Record_Cnt_All.setInitialValue(0);
        pnd_Record_Cnt_Rest.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap399c() throws Exception
    {
        super("Iaap399c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP399C", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(1);                                                                                                      //Natural: ASSIGN IA-ISSUE-DAY := 01
        getWorkFiles().read(3, pnd_Call_Type);                                                                                                                            //Natural: READ WORK 3 ONCE #CALL-TYPE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD-IN
        while (condition(getWorkFiles().read(1, ldaIaal399.getPnd_Work_Record_In())))
        {
            if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Contract().equals("   CHEADER")))                                                                 //Natural: IF #WORK-RECORD-IN.#W-RECORD-CONTRACT = '   CHEADER'
            {
                pnd_Hdr_Chck_Dte.setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_Rest_Of_Record().getSubstring(1,8));                                                       //Natural: ASSIGN #HDR-CHCK-DTE := SUBSTR ( #WORK-RECORD-IN.#REST-OF-RECORD,1,8 )
                pnd_R_Dte_A.setValue(pnd_Hdr_Chck_Dte);                                                                                                                   //Natural: ASSIGN #R-DTE-A := #HDR-CHCK-DTE
                pnd_R_Dte_A_Pnd_R_Mm.setValue(4);                                                                                                                         //Natural: ASSIGN #R-MM := 04
                pnd_R_Dte_A_Pnd_R_Dd.setValue(1);                                                                                                                         //Natural: ASSIGN #R-DD := 01
                pnd_W_Dte.setValue(pnd_Hdr_Chck_Dte);                                                                                                                     //Natural: ASSIGN #W-DTE := #HDR-CHCK-DTE
                if (condition(pnd_W_Dte_Pnd_W_Mm.greater(4)))                                                                                                             //Natural: IF #W-MM GT 04
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_R_Dte_A_Pnd_R_Yyyy.nsubtract(1);                                                                                                                  //Natural: SUBTRACT 1 FROM #R-YYYY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Call_Type.equals("L")))                                                                                                                 //Natural: IF #CALL-TYPE = 'L'
                {
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                  //Natural: ASSIGN IA-CHECK-DATE := 99999999
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().compute(new ComputeParameters(false, ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date()),              //Natural: ASSIGN IA-CHECK-DATE := VAL ( #HDR-CHCK-DTE )
                        pnd_Hdr_Chck_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal399.getPnd_Work_Record_Out().reset();                                                                                                                  //Natural: RESET #WORK-RECORD-OUT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Contract().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Contract());                                 //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-CONTRACT := #WORK-RECORD-IN.#W-RECORD-CONTRACT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Payee().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Payee());                                       //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-PAYEE := #WORK-RECORD-IN.#W-RECORD-PAYEE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Code().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code());                                         //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-CODE := #WORK-RECORD-IN.#W-RECORD-CODE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_Rest_Of_Record_Out().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_Rest_Of_Record());                                   //Natural: ASSIGN #REST-OF-RECORD-OUT := #REST-OF-RECORD
            short decideConditionsMet460 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WORK-RECORD-IN.#W-RECORD-CODE;//Natural: VALUE 10
            if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(10))))
            {
                decideConditionsMet460++;
                pnd_Record_Cnt_10.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-10
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Ccyymm().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Issue_Dte());                                         //Natural: ASSIGN IA-ISSUE-CCYYMM := #WORK-RECORD-IN.#W1-ISSUE-DTE
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                           //Natural: IF #WORK-RECORD-IN.#W1-CNTRCT-ISSUE-DTE-DD = 0
                {
                    ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd().setValue(1);                                                                            //Natural: ASSIGN #WORK-RECORD-IN.#W1-CNTRCT-ISSUE-DTE-DD := 01
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Issue_Dte_Dd());                                  //Natural: ASSIGN IA-ISSUE-DAY := #WORK-RECORD-IN.#W1-CNTRCT-ISSUE-DTE-DD
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_10_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_10());                        //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #RECORD-TYPE-10-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W1-LST-TRANS-DTE-N #W1-CNTRCT-ANNTY-STRT-DTE-N #W1-CNTRCT-FNL-PRM-DTE-N ( * )
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N().reset();
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue("*").reset();
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #WORK-RECORD-IN.#W1-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #WORK-RECORD-IN.#W1-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W1-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte().greater(getZero())))                                                        //Natural: IF #W1-CNTRCT-ANNTY-STRT-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED #W1-CNTRCT-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N()),  //Natural: ASSIGN #W1-CNTRCT-ANNTY-STRT-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 4
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte().getValue(pnd_I).greater(getZero())))                                       //Natural: IF #W1-CNTRCT-FNL-PRM-DTE ( #I ) GT 0
                    {
                        pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte().getValue(pnd_I),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED #W1-CNTRCT-FNL-PRM-DTE ( #I ) ( EM = YYYYMMDD ) TO #W-DTE
                        ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue(pnd_I).compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue(pnd_I)),  //Natural: ASSIGN #W1-CNTRCT-FNL-PRM-DTE-N ( #I ) := VAL ( #W-DTE )
                            pnd_W_Dte.val());
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(20))))
            {
                decideConditionsMet460++;
                pnd_Record_Cnt_20.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-20
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_20_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_20());                        //Natural: MOVE BY NAME #RECORD-TYPE-20 TO #RECORD-TYPE-20-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W2-LST-TRANS-DTE-N #W2-CPR-XFR-ISS-DTE-N
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N().reset();
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W2-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W2-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W2-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte().greater(getZero())))                                                              //Natural: IF #W2-CPR-XFR-ISS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED #W2-CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N()),  //Natural: ASSIGN #W2-CPR-XFR-ISS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                pnd_S_Pin.setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Cpr_Id_Nbr());                                                                                 //Natural: ASSIGN #S-PIN := #WORK-RECORD-IN.#W2-CPR-ID-NBR
                //*      IF #WORK-RECORD-OUT.#W2-ACTVTY-CDE NE 9
                //*        IF #WORK-RECORD-OUT.#W2-PEND-CDE = 'A' OR = 'C' OR = 'R'
                //*            OR = 'K' OR = 'M' OR = 'I'
                //*          IGNORE
                //*        ELSE
                //*          IF #CALL-TYPE = 'P'
                //*            PERFORM GET-TAX-WITHHOLDING
                //*          END-IF
                //*        END-IF
                //*      END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(30))))
            {
                decideConditionsMet460++;
                pnd_Record_Cnt_30.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-30
                //*      #REC-30 := #WORK-RECORD-IN.#W3-FILLER
                pnd_Rec_30_Pnd_Auv_Date.reset();                                                                                                                          //Natural: RESET #AUV-DATE
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Cmpny_Cde().equals("4") || ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Cmpny_Cde().equals("W")))        //Natural: IF #WORK-RECORD-IN.#W3-CMPNY-CDE = '4' OR = 'W'
                {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                    sub_Get_Auv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Cmpny_Cde().equals("U") || ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Cmpny_Cde().equals("2")))    //Natural: IF #WORK-RECORD-IN.#W3-CMPNY-CDE = 'U' OR = '2'
                    {
                        pnd_Per_Div_Amt.setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Tot_Div_Amt());                                                                  //Natural: MOVE #WORK-RECORD-IN.#W3-TOT-DIV-AMT TO #PER-DIV-AMT
                        if (condition(pnd_Per_Div_Amt_Pnd_Per_Auv.greater(getZero())))                                                                                    //Natural: IF #PER-AUV GT 0
                        {
                            pnd_W_Dte.setValue(pnd_R_Dte_A);                                                                                                              //Natural: ASSIGN #W-DTE := #R-DTE-A
                            if (condition(ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date_A().greater(pnd_W_Dte)))                                                         //Natural: IF IA-ISSUE-DATE-A GT #W-DTE
                            {
                                pnd_W_Dte.setValue(ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date_A());                                                                   //Natural: ASSIGN #W-DTE := IA-ISSUE-DATE-A
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Reval_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Dte);                                                                       //Natural: MOVE EDITED #W-DTE TO #REVAL-DTE ( EM = YYYYMMDD )
                            pnd_Reval_Dte.nsubtract(1);                                                                                                                   //Natural: SUBTRACT 1 FROM #REVAL-DTE
                            pnd_W_Dte.setValueEdited(pnd_Reval_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #REVAL-DTE ( EM = YYYYMMDD ) TO #W-DTE
                            if (condition(pnd_W_Dte.greater(pnd_Rec_30_Pnd_W_Xfr_In_Dte)))                                                                                //Natural: IF #W-DTE GT #W-XFR-IN-DTE
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_W_Dte.setValue(pnd_Rec_30_Pnd_W_Xfr_In_Dte);                                                                                          //Natural: ASSIGN #W-DTE := #W-XFR-IN-DTE
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Reval_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Dte);                                                                       //Natural: MOVE EDITED #W-DTE TO #REVAL-DTE ( EM = YYYYMMDD )
                            //*            CALLNAT 'ADSN607' #REVAL-DTE #RC
                            //*            MOVE EDITED #REVAL-DTE(EM=YYYYMMDD) TO #AUV-DATE
                            //*            MOVE #REC-30 TO #WORK-RECORD-IN.#W3-FILLER
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_30_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_30());                        //Natural: MOVE BY NAME #RECORD-TYPE-30 TO #RECORD-TYPE-30-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W3-LST-TRANS-DTE-N
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W3-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W3-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W3-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(40))))
            {
                decideConditionsMet460++;
                pnd_Record_Cnt_40.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-40
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_40_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_40());                        //Natural: MOVE BY NAME #RECORD-TYPE-40 TO #RECORD-TYPE-40-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W4-LST-TRANS-DTE-N
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W4-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W4-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W4-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte().equals(getZero()) || (ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Ddctn_Stp_Dte().greater(pnd_Hdr_Chck_Dte.val())  //Natural: IF #WORK-RECORD-OUT.#W4-DDCTN-STP-DTE = 0 OR ( #WORK-RECORD-OUT.#W4-DDCTN-STP-DTE GT VAL ( #HDR-CHCK-DTE ) AND #WORK-RECORD-OUT.#W4-DDCTN-STRT-DTE LE VAL ( #HDR-CHCK-DTE ) )
                    && ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Ddctn_Strt_Dte().lessOrEqual(pnd_Hdr_Chck_Dte.val()))))
                {
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Ddctn_Id_Nbr().setValue(pnd_S_Pin);                                                                          //Natural: ASSIGN #WORK-RECORD-OUT.#W4-DDCTN-ID-NBR := #S-PIN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Record_Cnt_Rest.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORD-CNT-REST
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(2, false, ldaIaal399.getPnd_Work_Record_Out());                                                                                          //Natural: WRITE WORK FILE 2 #WORK-RECORD-OUT
            pnd_Record_Cnt_All.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #RECORD-CNT-ALL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "Total rcds read          :  ",pnd_Record_Cnt_All);                                                                                         //Natural: WRITE 'Total rcds read          :  ' #RECORD-CNT-ALL
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 10 processed  :  ",pnd_Record_Cnt_10);                                                                                          //Natural: WRITE 'Total type 10 processed  :  ' #RECORD-CNT-10
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 20 processed  :  ",pnd_Record_Cnt_20);                                                                                          //Natural: WRITE 'Total type 20 processed  :  ' #RECORD-CNT-20
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 30 procesed   :  ",pnd_Record_Cnt_30);                                                                                          //Natural: WRITE 'Total type 30 procesed   :  ' #RECORD-CNT-30
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 40 processed  :  ",pnd_Record_Cnt_40);                                                                                          //Natural: WRITE 'Total type 40 processed  :  ' #RECORD-CNT-40
        if (Global.isEscape()) return;
        getReports().write(0, "Total other processed    :  ",pnd_Record_Cnt_Rest);                                                                                        //Natural: WRITE 'Total other processed    :  ' #RECORD-CNT-REST
        if (Global.isEscape()) return;
        //* **
        //*                                                                                                                                                               //Natural: ON ERROR
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* ***************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TAX-WITHHOLDING
        //* **********************************************************************
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Fund_1.setValue(" ");                                                                                                                                    //Natural: ASSIGN #PARM-FUND-1 := ' '
        DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Fund_Cde(),  //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL #WORK-RECORD-IN.#W3-FUND-CDE GIVING INDEX #I
            true), new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            pnd_Parm_Fund_1.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                                                      //Natural: ASSIGN #PARM-FUND-1 := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
            ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                                      //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
            ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
            ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Call_Type);                                                                                      //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Parm_Fund_1);                                                                                    //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
            if (condition(Global.isEscape())) return;
            if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().equals(getZero())))                                                                              //Natural: IF RETURN-CODE = 0
            {
                ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Tot_Per_Amt().compute(new ComputeParameters(true, ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Tot_Per_Amt()),         //Natural: COMPUTE ROUNDED #WORK-RECORD-IN.#W3-TOT-PER-AMT = #WORK-RECORD-IN.#W3-UNITS-CNT * AUV-RETURNED
                    ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Units_Cnt().multiply(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned()));
                pnd_Per_Div_Amt_Pnd_Per_Auv.setValue(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                                    //Natural: ASSIGN #PER-AUV := AUV-RETURNED
                ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Tot_Div_Amt().setValue(pnd_Per_Div_Amt);                                                                          //Natural: ASSIGN #WORK-RECORD-IN.#W3-TOT-DIV-AMT := #PER-DIV-AMT
                pnd_Rec_30_Pnd_Auv_Date.setValueEdited(ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return(),new ReportEditMask("99999999"));                                //Natural: MOVE EDITED AUV-DATE-RETURN ( EM = 99999999 ) TO #AUV-DATE
                //*    MOVE #REC-30 TO #WORK-RECORD-IN.#W3-FILLER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "AIAN026 RETURN CODE NE 0 FOR: ",ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Contract(),ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Payee(), //Natural: WRITE 'AIAN026 RETURN CODE NE 0 FOR: ' #WORK-RECORD-IN.#W-RECORD-CONTRACT #WORK-RECORD-IN.#W-RECORD-PAYEE '=' IA-CALL-TYPE '=' IA-FUND-CODE '=' IA-REVAL-METHD '=' IA-CHECK-DATE '=' RETURN-CODE
                    "=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type(),"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code(),"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd(),
                    "=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date(),"=",ldaIaal050.getIa_Aian026_Linkage_Return_Code());
                if (Global.isEscape()) return;
                DbsUtil.terminate(64);  if (true) return;                                                                                                                 //Natural: TERMINATE 64
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "INVALID FUND FOR: ",ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Contract(),ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Payee(),    //Natural: WRITE 'INVALID FUND FOR: ' #WORK-RECORD-IN.#W-RECORD-CONTRACT #WORK-RECORD-IN.#W-RECORD-PAYEE '=' #WORK-RECORD-IN.#W3-FUND-CDE
                "=",ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Fund_Cde());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Tax_Withholding() throws Exception                                                                                                               //Natural: GET-TAX-WITHHOLDING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Contract());                                                            //Natural: ASSIGN #PPCN-NBR := #WORK-RECORD-OUT.#W-RECORD-CONTRACT
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Payee());                                                              //Natural: ASSIGN #PAYEE-CDE := #WORK-RECORD-OUT.#W-RECORD-PAYEE
        vw_iaa_Cpr.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) IAA-CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cpr.readNextRow("READ02")))
        {
            if (condition(iaa_Cpr_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr) || iaa_Cpr_Cntrct_Part_Payee_Cde.notEquals(pnd_Cntrct_Payee_Key_Pnd_Payee_Cde))) //Natural: IF CNTRCT-PART-PPCN-NBR NE #PPCN-NBR OR CNTRCT-PART-PAYEE-CDE NE #PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Fed_Tax_Amt().setValue(iaa_Cpr_Cntrct_Fed_Tax_Amt);                                                                  //Natural: ASSIGN #WORK-RECORD-OUT.#W2-FED-TAX-AMT := CNTRCT-FED-TAX-AMT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_State_Cde().setValue(iaa_Cpr_Cntrct_State_Cde);                                                                      //Natural: ASSIGN #WORK-RECORD-OUT.#W2-STATE-CDE := CNTRCT-STATE-CDE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_State_Tax_Amt().setValue(iaa_Cpr_Cntrct_State_Tax_Amt);                                                              //Natural: ASSIGN #WORK-RECORD-OUT.#W2-STATE-TAX-AMT := CNTRCT-STATE-TAX-AMT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Local_Cde().setValue(iaa_Cpr_Cntrct_Local_Cde);                                                                      //Natural: ASSIGN #WORK-RECORD-OUT.#W2-LOCAL-CDE := CNTRCT-LOCAL-CDE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Local_Tax_Amt().setValue(iaa_Cpr_Cntrct_Local_Tax_Amt);                                                              //Natural: ASSIGN #WORK-RECORD-OUT.#W2-LOCAL-TAX-AMT := CNTRCT-LOCAL-TAX-AMT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* **
        getReports().write(0, "ERROR                   ",Global.getERROR());                                                                                              //Natural: WRITE 'ERROR                   ' *ERROR
        getReports().write(0, "PROGRAM                 ",Global.getPROGRAM());                                                                                            //Natural: WRITE 'PROGRAM                 ' *PROGRAM
        getReports().write(0, "ERROR LINE              ",Global.getERROR_LINE());                                                                                         //Natural: WRITE 'ERROR LINE              ' *ERROR-LINE
        getReports().write(0, "INPUT RECORD NUMBER     ");                                                                                                                //Natural: WRITE 'INPUT RECORD NUMBER     '
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133");
    }
}
