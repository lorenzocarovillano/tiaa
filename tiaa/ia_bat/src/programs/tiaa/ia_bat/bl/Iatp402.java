/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:09 PM
**        * FROM NATURAL PROGRAM : Iatp402
************************************************************
**        * FILE NAME            : Iatp402.java
**        * CLASS NAME           : Iatp402
**        * INSTANCE NAME        : Iatp402
************************************************************
***********************************************************************
*
*   PROGRAM    :- IATP402
*   SYSTEM     :- IA
*   AUTHOR     :- LEN BERNSTEIN
*   DATE       :- 11/26/1997
*
*   DESCRIPTION
*   -----------
*   THIS PROGRAM READS ALL TRANSFER AND SWITCHE RQSTS IN STATUS
*   CMPLTE, AWTING CNFRMTN (M0) AND IN STATUS RJCTD, AWTING
*   CNFRMTN (P0).
*   FOR EACH RECORD WE CALL IATN402 WHICH WILL TRY TO UPDATE THE
*   RQST, MIT AND POST.
*
*   REPORTS:-
*   --------
*   1. RQST,MIT,POST ERROR REPORT
*   2. RQST,MIT,POST COMPLETION REPORT
*
*   HISTORY
*   -------
*  08/29/10    PASSING #IATN400-OUT.CNTRL-TODAYS-DTE TO IATN402.
*              GGG-08/10
*  04/2017  OS RE-STOWED ONLY FOR PIN EXPANSION.
*  07/2017  OS PIN EXPANSION MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp402 extends BLNatBase
{
    // Data Areas
    private PdaIatl402p pdaIatl402p;
    private PdaIatl400p pdaIatl400p;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;
    private DbsField pnd_Ia_Super_De_02;

    private DbsGroup pnd_Ia_Super_De_02__R_Field_1;
    private DbsField pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde;
    private DbsField pnd_Ia_Super_De_02_Pnd_Xfr_Stts_Cde;
    private DbsField pnd_Ia_Super_De_02_Pnd_Rqst_Id;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_Const_Pnd_Active;
    private DbsField pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0;
    private DbsField pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0;

    private DbsGroup pnd_Cnt;
    private DbsField pnd_Cnt_Pnd_Total_Read_Cmplte;
    private DbsField pnd_Cnt_Pnd_Total_Read_Rjctd;
    private DbsField pnd_Cnt_Pnd_Complete_Cm;
    private DbsField pnd_Cnt_Pnd_Error_Cm;
    private DbsField pnd_Cnt_Pnd_Complete_Rj;
    private DbsField pnd_Cnt_Pnd_Error_Rj;
    private DbsField pnd_Cnt_Pnd_Rpt1_Count;
    private DbsField pnd_Cnt_Pnd_Rpt2_Count;
    private DbsField pnd_Cnt_Pnd_Rpt3_Count;

    private DbsGroup pnd_Page_Count;
    private DbsField pnd_Page_Count_Pnd_Report_1;
    private DbsField pnd_Page_Count_Pnd_Report_2;
    private DbsField pnd_Page_Count_Pnd_Report_3;
    private DbsField pnd_Datea;

    private DbsGroup pnd_Datea__R_Field_2;
    private DbsField pnd_Datea_Pnd_Daten;

    private DbsGroup pnd_Control;
    private DbsField pnd_Control_Pnd_Todays_Dte;

    private DbsGroup pnd_Control__R_Field_3;
    private DbsField pnd_Control_Pnd_Todays_Dte_N8;
    private DbsField pnd_Control_Pnd_Next_Bus_Dte;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M400h_Busns_Dte;

    private DbsGroup pnd_M400h_Busns_Dte__R_Field_4;
    private DbsField pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Mm;
    private DbsField pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Dd;
    private DbsField pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Cy;
    private DbsField pnd_M400h_Page;
    private DbsField pnd_Program;

    private DbsGroup pnd_Detail_Ln;
    private DbsField pnd_Detail_Ln_Pnd_Return_Code;
    private DbsField pnd_Detail_Ln_Pnd_Error_Nbr;
    private DbsField pnd_Detail_Ln_Pnd_Msg;
    private DbsField pnd_Detail_Ln_Pnd_Msg_2;
    private DbsField pnd_Rpt1_Header1;
    private DbsField pnd_Rpt1_Header2_T;
    private DbsField pnd_Rpt1_Header2_S;
    private DbsField pnd_Rpt2_Header1;
    private DbsField pnd_Rpt2_Header2_T;
    private DbsField pnd_Rpt2_Header2_S;
    private DbsField pnd_Rpt3_Header2_T;
    private DbsField pnd_Rpt3_Header2_S;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_Transfer_Request;
    private DbsField pnd_L_Pnd_Status_M0;
    private DbsField pnd_L_Pnd_Debug;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl402p = new PdaIatl402p(localVariables);
        pdaIatl400p = new PdaIatl400p(localVariables);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        pnd_Ia_Super_De_02 = localVariables.newFieldInRecord("pnd_Ia_Super_De_02", "#IA-SUPER-DE-02", FieldType.STRING, 37);

        pnd_Ia_Super_De_02__R_Field_1 = localVariables.newGroupInRecord("pnd_Ia_Super_De_02__R_Field_1", "REDEFINE", pnd_Ia_Super_De_02);
        pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde = pnd_Ia_Super_De_02__R_Field_1.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ia_Super_De_02_Pnd_Xfr_Stts_Cde = pnd_Ia_Super_De_02__R_Field_1.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_Xfr_Stts_Cde", "#XFR-STTS-CDE", FieldType.STRING, 
            2);
        pnd_Ia_Super_De_02_Pnd_Rqst_Id = pnd_Ia_Super_De_02__R_Field_1.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 
            34);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_Const_Pnd_Active = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Active", "#ACTIVE", FieldType.STRING, 1);
        pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0", "#CMPLTE-AWTNG-CNFRMTN-M0", FieldType.STRING, 
            2);
        pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0 = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0", "#RJCTD-AWTNG-CNFRMTN-P0", FieldType.STRING, 
            2);

        pnd_Cnt = localVariables.newGroupInRecord("pnd_Cnt", "#CNT");
        pnd_Cnt_Pnd_Total_Read_Cmplte = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Total_Read_Cmplte", "#TOTAL-READ-CMPLTE", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Total_Read_Rjctd = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Total_Read_Rjctd", "#TOTAL-READ-RJCTD", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Complete_Cm = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Complete_Cm", "#COMPLETE-CM", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Error_Cm = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Error_Cm", "#ERROR-CM", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Complete_Rj = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Complete_Rj", "#COMPLETE-RJ", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Error_Rj = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Error_Rj", "#ERROR-RJ", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Pnd_Rpt1_Count = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Rpt1_Count", "#RPT1-COUNT", FieldType.NUMERIC, 4);
        pnd_Cnt_Pnd_Rpt2_Count = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Rpt2_Count", "#RPT2-COUNT", FieldType.NUMERIC, 4);
        pnd_Cnt_Pnd_Rpt3_Count = pnd_Cnt.newFieldInGroup("pnd_Cnt_Pnd_Rpt3_Count", "#RPT3-COUNT", FieldType.NUMERIC, 4);

        pnd_Page_Count = localVariables.newGroupInRecord("pnd_Page_Count", "#PAGE-COUNT");
        pnd_Page_Count_Pnd_Report_1 = pnd_Page_Count.newFieldInGroup("pnd_Page_Count_Pnd_Report_1", "#REPORT-1", FieldType.PACKED_DECIMAL, 9);
        pnd_Page_Count_Pnd_Report_2 = pnd_Page_Count.newFieldInGroup("pnd_Page_Count_Pnd_Report_2", "#REPORT-2", FieldType.PACKED_DECIMAL, 9);
        pnd_Page_Count_Pnd_Report_3 = pnd_Page_Count.newFieldInGroup("pnd_Page_Count_Pnd_Report_3", "#REPORT-3", FieldType.PACKED_DECIMAL, 9);
        pnd_Datea = localVariables.newFieldInRecord("pnd_Datea", "#DATEA", FieldType.STRING, 8);

        pnd_Datea__R_Field_2 = localVariables.newGroupInRecord("pnd_Datea__R_Field_2", "REDEFINE", pnd_Datea);
        pnd_Datea_Pnd_Daten = pnd_Datea__R_Field_2.newFieldInGroup("pnd_Datea_Pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Control = localVariables.newGroupInRecord("pnd_Control", "#CONTROL");
        pnd_Control_Pnd_Todays_Dte = pnd_Control.newFieldInGroup("pnd_Control_Pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);

        pnd_Control__R_Field_3 = pnd_Control.newGroupInGroup("pnd_Control__R_Field_3", "REDEFINE", pnd_Control_Pnd_Todays_Dte);
        pnd_Control_Pnd_Todays_Dte_N8 = pnd_Control__R_Field_3.newFieldInGroup("pnd_Control_Pnd_Todays_Dte_N8", "#TODAYS-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Control_Pnd_Next_Bus_Dte = pnd_Control.newFieldInGroup("pnd_Control_Pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.STRING, 8);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M400h_Busns_Dte = localVariables.newFieldInRecord("pnd_M400h_Busns_Dte", "#M400H-BUSNS-DTE", FieldType.NUMERIC, 8);

        pnd_M400h_Busns_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_M400h_Busns_Dte__R_Field_4", "REDEFINE", pnd_M400h_Busns_Dte);
        pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Mm = pnd_M400h_Busns_Dte__R_Field_4.newFieldInGroup("pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Mm", "#M400H-BUSNS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Dd = pnd_M400h_Busns_Dte__R_Field_4.newFieldInGroup("pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Dd", "#M400H-BUSNS-DTE-DD", 
            FieldType.STRING, 2);
        pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Cy = pnd_M400h_Busns_Dte__R_Field_4.newFieldInGroup("pnd_M400h_Busns_Dte_Pnd_M400h_Busns_Dte_Cy", "#M400H-BUSNS-DTE-CY", 
            FieldType.STRING, 4);
        pnd_M400h_Page = localVariables.newFieldInRecord("pnd_M400h_Page", "#M400H-PAGE", FieldType.NUMERIC, 5);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Detail_Ln = localVariables.newGroupInRecord("pnd_Detail_Ln", "#DETAIL-LN");
        pnd_Detail_Ln_Pnd_Return_Code = pnd_Detail_Ln.newFieldInGroup("pnd_Detail_Ln_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Detail_Ln_Pnd_Error_Nbr = pnd_Detail_Ln.newFieldInGroup("pnd_Detail_Ln_Pnd_Error_Nbr", "#ERROR-NBR", FieldType.STRING, 3);
        pnd_Detail_Ln_Pnd_Msg = pnd_Detail_Ln.newFieldInGroup("pnd_Detail_Ln_Pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Detail_Ln_Pnd_Msg_2 = pnd_Detail_Ln.newFieldInGroup("pnd_Detail_Ln_Pnd_Msg_2", "#MSG-2", FieldType.STRING, 79);
        pnd_Rpt1_Header1 = localVariables.newFieldInRecord("pnd_Rpt1_Header1", "#RPT1-HEADER1", FieldType.STRING, 60);
        pnd_Rpt1_Header2_T = localVariables.newFieldInRecord("pnd_Rpt1_Header2_T", "#RPT1-HEADER2-T", FieldType.STRING, 58);
        pnd_Rpt1_Header2_S = localVariables.newFieldInRecord("pnd_Rpt1_Header2_S", "#RPT1-HEADER2-S", FieldType.STRING, 58);
        pnd_Rpt2_Header1 = localVariables.newFieldInRecord("pnd_Rpt2_Header1", "#RPT2-HEADER1", FieldType.STRING, 60);
        pnd_Rpt2_Header2_T = localVariables.newFieldInRecord("pnd_Rpt2_Header2_T", "#RPT2-HEADER2-T", FieldType.STRING, 58);
        pnd_Rpt2_Header2_S = localVariables.newFieldInRecord("pnd_Rpt2_Header2_S", "#RPT2-HEADER2-S", FieldType.STRING, 58);
        pnd_Rpt3_Header2_T = localVariables.newFieldInRecord("pnd_Rpt3_Header2_T", "#RPT3-HEADER2-T", FieldType.STRING, 58);
        pnd_Rpt3_Header2_S = localVariables.newFieldInRecord("pnd_Rpt3_Header2_S", "#RPT3-HEADER2-S", FieldType.STRING, 58);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_Transfer_Request = pnd_L.newFieldInGroup("pnd_L_Pnd_Transfer_Request", "#TRANSFER-REQUEST", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Status_M0 = pnd_L.newFieldInGroup("pnd_L_Pnd_Status_M0", "#STATUS-M0", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Debug = pnd_L.newFieldInGroup("pnd_L_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();

        localVariables.reset();
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
        pnd_Const_Pnd_Active.setInitialValue("A");
        pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0.setInitialValue("M0");
        pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0.setInitialValue("P0");
        pnd_Rpt1_Header1.setInitialValue("            RQST,MIT,POST REPORT");
        pnd_Rpt1_Header2_T.setInitialValue("         Transfer Error Report");
        pnd_Rpt1_Header2_S.setInitialValue("         Switch Error Report ");
        pnd_Rpt2_Header1.setInitialValue("            RQST,MIT,POST REPORT");
        pnd_Rpt2_Header2_T.setInitialValue("  Transfers in complete status Completed");
        pnd_Rpt2_Header2_S.setInitialValue("  Switches in complete status Completed");
        pnd_Rpt3_Header2_T.setInitialValue("  Transfers in reject status Completed");
        pnd_Rpt3_Header2_S.setInitialValue("  Switches in reject status Completed");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp402() throws Exception
    {
        super("Iatp402");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //* *======================================================================
        //*                       START OF PROGRAM
        //* *======================================================================
        //*  #DEBUG := TRUE
        //*  ERROR REPORT
        //*  COMPLETE RQSTS REPORT
        //*  REJECTED RQSTS REPORT
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: FORMAT ( 2 ) PS = 60 LS = 133;//Natural: FORMAT ( 3 ) PS = 60 LS = 133
        //*  ERROR REPORT
        pnd_Cnt.reset();                                                                                                                                                  //Natural: RESET #CNT
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //* *=====================================================================
        //*                        MAIN BODY OF PROGRAM
        //* *=====================================================================
                                                                                                                                                                          //Natural: PERFORM SETUP-CONTROL-DATES
        sub_Setup_Control_Dates();
        if (condition(Global.isEscape())) {return;}
        //*  ================
        //*   TRANSFER RQSTS
        //*  ================
        pnd_L_Pnd_Transfer_Request.setValue(true);                                                                                                                        //Natural: ASSIGN #TRANSFER-REQUEST := TRUE
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPLETE-TRNSFR-SW
        sub_Process_Complete_Trnsfr_Sw();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CMPLT-TRANSFER-TOTALS
        sub_Write_Cmplt_Transfer_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RJCTD-TRNSFR-SW
        sub_Process_Rjctd_Trnsfr_Sw();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-RJCTD-TRANSFER-TOTALS
        sub_Write_Rjctd_Transfer_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  ==============
        //*   SWITCH RQSTS
        //*  ==============
        pnd_L_Pnd_Transfer_Request.reset();                                                                                                                               //Natural: RESET #TRANSFER-REQUEST #CNT #PAGE-COUNT.#REPORT-1 #PAGE-COUNT.#REPORT-2 #PAGE-COUNT.#REPORT-3
        pnd_Cnt.reset();
        pnd_Page_Count_Pnd_Report_1.reset();
        pnd_Page_Count_Pnd_Report_2.reset();
        pnd_Page_Count_Pnd_Report_3.reset();
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPLETE-TRNSFR-SW
        sub_Process_Complete_Trnsfr_Sw();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CMPLT-SWITCH-TOTALS
        sub_Write_Cmplt_Switch_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RJCTD-TRNSFR-SW
        sub_Process_Rjctd_Trnsfr_Sw();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-RJCTD-SWITCH-TOTALS
        sub_Write_Rjctd_Switch_Totals();
        if (condition(Global.isEscape())) {return;}
        //* *======================================================================
        //*                       START OF SUBROUTINES
        //* *======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-CONTROL-DATES
        //* ***********************************************************************
        //*  GET TODAY's Date and the Next Business Date
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPLETE-TRNSFR-SW
        //* ***********************************************************************
        //*  PROCESS ALL THE COMPLETE TRANSFERS OR SWITCHES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RJCTD-TRNSFR-SW
        //* ***********************************************************************
        //*  PROCESS ALL THE REJECTED TRANSFERS OR SWITCHES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RQST-MIT-POST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RPT1-LINE
        //* ***********************************************************************
        //*  WRITE ERROR REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RPT2-LINE
        //* ***********************************************************************
        //*  COMPLETION REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RPT3-LINE
        //* ***********************************************************************
        //*  REJECTION REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CMPLT-TRANSFER-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CMPLT-SWITCH-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RJCTD-TRANSFER-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RJCTD-SWITCH-TOTALS
    }
    //*  SCD
    private void sub_Setup_Control_Dates() throws Exception                                                                                                               //Natural: SETUP-CONTROL-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'DC'
        //*  FETCH THE LATEST CONTROL RECORD
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        //*  GGG-08/10
        pdaIatl402p.getPnd_Iatn402_In_Pnd_Todays_Dte().setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                  //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #IATN402-IN.#TODAYS-DTE
        pnd_Control_Pnd_Todays_Dte.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #CONTROL.#TODAYS-DTE
        pnd_Control_Pnd_Next_Bus_Dte.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = YYYYMMDD ) TO #CONTROL.#NEXT-BUS-DTE
        if (condition(! (DbsUtil.maskMatches(pnd_Control_Pnd_Todays_Dte,"YYYYMMDD")) || ! (DbsUtil.maskMatches(pnd_Control_Pnd_Next_Bus_Dte,"YYYYMMDD"))))                //Natural: IF #CONTROL.#TODAYS-DTE NE MASK ( YYYYMMDD ) OR #CONTROL.#NEXT-BUS-DTE NE MASK ( YYYYMMDD )
        {
            pnd_Cnt_Pnd_Rpt1_Count.nadd(1);                                                                                                                               //Natural: ASSIGN #RPT1-COUNT := #RPT1-COUNT + 1
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Cnt_Pnd_Rpt1_Count,"Control Record 'DC' returned an invalid date");                                    //Natural: WRITE ( 1 ) / #RPT1-COUNT 'Control Record "DC" returned an invalid date'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Todays date   = ",pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte());                           //Natural: WRITE ( 1 ) 7T 'Todays date   = ' #IATN400-OUT.CNTRL-TODAYS-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Next Bus date = ",pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte());                         //Natural: WRITE ( 1 ) 7T 'Next Bus date = ' #IATN400-OUT.CNTRL-NEXT-BUS-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Fatal error Program Terminated");                                                               //Natural: WRITE ( 1 ) 7T 'Fatal error Program Terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SETUP REPORT DATE
        pnd_Datea.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("MMDDYYYY"));                                                       //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = MMDDYYYY ) TO #DATEA
        pnd_M400h_Busns_Dte.setValue(pnd_Datea_Pnd_Daten);                                                                                                                //Natural: MOVE #DATEN TO #M400H-BUSNS-DTE
        //*  SETUP-CONTROL-DATES
    }
    private void sub_Process_Complete_Trnsfr_Sw() throws Exception                                                                                                        //Natural: PROCESS-COMPLETE-TRNSFR-SW
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        pnd_Cnt_Pnd_Rpt1_Count.reset();                                                                                                                                   //Natural: RESET #RPT1-COUNT #RPT2-COUNT #CNT #PAGE-COUNT.#REPORT-1 #PAGE-COUNT.#REPORT-2
        pnd_Cnt_Pnd_Rpt2_Count.reset();
        pnd_Cnt.reset();
        pnd_Page_Count_Pnd_Report_1.reset();
        pnd_Page_Count_Pnd_Report_2.reset();
        //*  SET FIND KEY
        if (condition(pnd_L_Pnd_Transfer_Request.getBoolean()))                                                                                                           //Natural: IF #TRANSFER-REQUEST
        {
            pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde.setValue(pnd_Const_Pnd_Rcrd_Type_Trnsfr);                                                                                //Natural: ASSIGN #IA-SUPER-DE-02.#RCRD-TYPE-CDE := #RCRD-TYPE-TRNSFR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde.setValue(pnd_Const_Pnd_Rcrd_Type_Switch);                                                                                //Natural: ASSIGN #IA-SUPER-DE-02.#RCRD-TYPE-CDE := #RCRD-TYPE-SWITCH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ia_Super_De_02_Pnd_Xfr_Stts_Cde.setValue(pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0);                                                                              //Natural: ASSIGN #IA-SUPER-DE-02.#XFR-STTS-CDE := #CMPLTE-AWTNG-CNFRMTN-M0
        pnd_Ia_Super_De_02_Pnd_Rqst_Id.setValue(0);                                                                                                                       //Natural: ASSIGN #IA-SUPER-DE-02.#RQST-ID := 0
        pnd_L_Pnd_Status_M0.setValue(true);                                                                                                                               //Natural: ASSIGN #STATUS-M0 := TRUE
        vw_iaa_Trnsfr_Sw_Rqst.startDatabaseRead                                                                                                                           //Natural: READ IAA-TRNSFR-SW-RQST BY IA-SUPER-DE-02 STARTING FROM #IA-SUPER-DE-02
        (
        "RD2",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", pnd_Ia_Super_De_02, WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        RD2:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("RD2")))
        {
            //*  CHECK STATUS AND RECORD TYPE
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.notEquals(pnd_Const_Pnd_Cmplte_Awtng_Cnfrmtn_M0) || iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.notEquals(pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde))) //Natural: IF XFR-STTS-CDE NE #CMPLTE-AWTNG-CNFRMTN-M0 OR RCRD-TYPE-CDE NE #IA-SUPER-DE-02.#RCRD-TYPE-CDE
            {
                if (true) break RD2;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD2. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Ln.reset();                                                                                                                                        //Natural: RESET #DETAIL-LN
            pnd_Cnt_Pnd_Total_Read_Cmplte.nadd(1);                                                                                                                        //Natural: ASSIGN #CNT.#TOTAL-READ-CMPLTE := #CNT.#TOTAL-READ-CMPLTE + 1
            pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().setValue(vw_iaa_Trnsfr_Sw_Rqst.getAstISN("RD2"));                                                                     //Natural: ASSIGN #IATN402-IN.#ISN := *ISN ( RD2. )
            //*  CALL IATN402
                                                                                                                                                                          //Natural: PERFORM PROCESS-RQST-MIT-POST
            sub_Process_Rqst_Mit_Post();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ERROR FORM IATN402
            if (condition(pdaIatl402p.getPnd_Iatn402_Out_Pnd_Return_Code().equals("E")))                                                                                  //Natural: IF #IATN402-OUT.#RETURN-CODE EQ 'E'
            {
                pnd_Cnt_Pnd_Error_Cm.nadd(1);                                                                                                                             //Natural: ASSIGN #CNT.#ERROR-CM := #CNT.#ERROR-CM + 1
                pnd_Detail_Ln.setValuesByName(pdaIatl402p.getPnd_Iatn402_Out());                                                                                          //Natural: MOVE BY NAME #IATN402-OUT TO #DETAIL-LN
                                                                                                                                                                          //Natural: PERFORM WRITE-RPT1-LINE
                sub_Write_Rpt1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt_Pnd_Complete_Cm.nadd(1);                                                                                                                          //Natural: ASSIGN #CNT.#COMPLETE-CM := #CNT.#COMPLETE-CM + 1
                if (condition(pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().equals(" ")))                                                                                      //Natural: IF #IATN402-OUT.#MSG EQ ' '
                {
                    pnd_Detail_Ln_Pnd_Msg.setValue(DbsUtil.compress("RQST closed. MIT and POST were updated successfully.", Global.getPROGRAM()));                        //Natural: COMPRESS 'RQST closed. MIT and POST were updated successfully.' *PROGRAM INTO #DETAIL-LN.#MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-RPT2-LINE
                    sub_Write_Rpt2_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Detail_Ln.setValuesByName(pdaIatl402p.getPnd_Iatn402_Out());                                                                                      //Natural: MOVE BY NAME #IATN402-OUT TO #DETAIL-LN
                                                                                                                                                                          //Natural: PERFORM WRITE-RPT2-LINE
                    sub_Write_Rpt2_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RD2.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PROCESS-COMPLETE-TRNSFR-SW
    }
    private void sub_Process_Rjctd_Trnsfr_Sw() throws Exception                                                                                                           //Natural: PROCESS-RJCTD-TRNSFR-SW
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        pnd_L_Pnd_Status_M0.reset();                                                                                                                                      //Natural: RESET #STATUS-M0 #RPT1-COUNT #RPT3-COUNT #CNT #PAGE-COUNT.#REPORT-1 #PAGE-COUNT.#REPORT-2
        pnd_Cnt_Pnd_Rpt1_Count.reset();
        pnd_Cnt_Pnd_Rpt3_Count.reset();
        pnd_Cnt.reset();
        pnd_Page_Count_Pnd_Report_1.reset();
        pnd_Page_Count_Pnd_Report_2.reset();
        //*  SET THE FIND KEY
        if (condition(pnd_L_Pnd_Transfer_Request.getBoolean()))                                                                                                           //Natural: IF #TRANSFER-REQUEST
        {
            pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde.setValue(pnd_Const_Pnd_Rcrd_Type_Trnsfr);                                                                                //Natural: ASSIGN #IA-SUPER-DE-02.#RCRD-TYPE-CDE := #RCRD-TYPE-TRNSFR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde.setValue(pnd_Const_Pnd_Rcrd_Type_Switch);                                                                                //Natural: ASSIGN #IA-SUPER-DE-02.#RCRD-TYPE-CDE := #RCRD-TYPE-SWITCH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ia_Super_De_02_Pnd_Xfr_Stts_Cde.setValue(pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0);                                                                               //Natural: ASSIGN #IA-SUPER-DE-02.#XFR-STTS-CDE := #RJCTD-AWTNG-CNFRMTN-P0
        pnd_Ia_Super_De_02_Pnd_Rqst_Id.setValue(0);                                                                                                                       //Natural: ASSIGN #IA-SUPER-DE-02.#RQST-ID := 0
        vw_iaa_Trnsfr_Sw_Rqst.startDatabaseRead                                                                                                                           //Natural: READ IAA-TRNSFR-SW-RQST BY IA-SUPER-DE-02 STARTING FROM #IA-SUPER-DE-02
        (
        "RD3",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", pnd_Ia_Super_De_02, WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        RD3:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("RD3")))
        {
            //*  CHECK STATUS AND RECORD TYPE
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.notEquals(pnd_Const_Pnd_Rjctd_Awtng_Cnfrmtn_P0) || iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.notEquals(pnd_Ia_Super_De_02_Pnd_Rcrd_Type_Cde))) //Natural: IF XFR-STTS-CDE NE #RJCTD-AWTNG-CNFRMTN-P0 OR RCRD-TYPE-CDE NE #IA-SUPER-DE-02.#RCRD-TYPE-CDE
            {
                if (true) break RD3;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD3. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_L_Pnd_Debug.getBoolean()))                                                                                                                  //Natural: IF #DEBUG
            {
                getReports().write(0, "=",iaa_Trnsfr_Sw_Rqst_Rqst_Id,"=",iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde);                                                                //Natural: WRITE '=' RQST-ID '=' XFR-STTS-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt_Pnd_Total_Read_Rjctd.nadd(1);                                                                                                                         //Natural: ASSIGN #CNT.#TOTAL-READ-RJCTD := #CNT.#TOTAL-READ-RJCTD + 1
            pdaIatl402p.getPnd_Iatn402_In_Pnd_Isn().setValue(vw_iaa_Trnsfr_Sw_Rqst.getAstISN("RD3"));                                                                     //Natural: ASSIGN #IATN402-IN.#ISN := *ISN ( RD3. )
            //*  CALL IATN402
                                                                                                                                                                          //Natural: PERFORM PROCESS-RQST-MIT-POST
            sub_Process_Rqst_Mit_Post();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ERROR FROM IATN402
            if (condition(pdaIatl402p.getPnd_Iatn402_Out_Pnd_Return_Code().equals("E")))                                                                                  //Natural: IF #IATN402-OUT.#RETURN-CODE EQ 'E'
            {
                pnd_Cnt_Pnd_Error_Rj.nadd(1);                                                                                                                             //Natural: ASSIGN #CNT.#ERROR-RJ := #CNT.#ERROR-RJ + 1
                pnd_Detail_Ln.setValuesByName(pdaIatl402p.getPnd_Iatn402_Out());                                                                                          //Natural: MOVE BY NAME #IATN402-OUT TO #DETAIL-LN
                                                                                                                                                                          //Natural: PERFORM WRITE-RPT1-LINE
                sub_Write_Rpt1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt_Pnd_Complete_Rj.nadd(1);                                                                                                                          //Natural: ASSIGN #CNT.#COMPLETE-RJ := #CNT.#COMPLETE-RJ + 1
                pnd_Detail_Ln.setValuesByName(pdaIatl402p.getPnd_Iatn402_Out());                                                                                          //Natural: MOVE BY NAME #IATN402-OUT TO #DETAIL-LN
                if (condition(pdaIatl402p.getPnd_Iatn402_Out_Pnd_Msg().equals(" ")))                                                                                      //Natural: IF #IATN402-OUT.#MSG EQ ' '
                {
                    pnd_Detail_Ln_Pnd_Msg.setValue(DbsUtil.compress("Rejected Transfer Request closed. MIT and POST were", "updated successfully.", Global.getPROGRAM())); //Natural: COMPRESS 'Rejected Transfer Request closed. MIT and POST were' 'updated successfully.' *PROGRAM INTO #DETAIL-LN.#MSG
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RPT3-LINE
                sub_Write_Rpt3_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  RD3.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  PROCESS-RJCTD-TRNSFR-SW
    }
    private void sub_Process_Rqst_Mit_Post() throws Exception                                                                                                             //Natural: PROCESS-RQST-MIT-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CALL THE SUBPROGRAM TO UPDATE THE RQST STATUS, MIT AND POST
        DbsUtil.callnat(Iatn402.class , getCurrentProcessState(), pdaIatl402p.getPnd_Iatn402_In(), pdaIatl402p.getPnd_Iatn402_Out());                                     //Natural: CALLNAT 'IATN402' #IATN402-IN #IATN402-OUT
        if (condition(Global.isEscape())) return;
        //*  PROCESS-RQST-MIT-POST
    }
    private void sub_Write_Rpt1_Line() throws Exception                                                                                                                   //Natural: WRITE-RPT1-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cnt_Pnd_Rpt1_Count.nadd(1);                                                                                                                                   //Natural: ASSIGN #RPT1-COUNT := #RPT1-COUNT + 1
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Cnt_Pnd_Rpt1_Count,pnd_Detail_Ln_Pnd_Msg);                                                                 //Natural: WRITE ( 1 ) / #RPT1-COUNT #DETAIL-LN.#MSG
        if (Global.isEscape()) return;
        if (condition(pnd_Detail_Ln_Pnd_Msg_2.notEquals(" ")))                                                                                                            //Natural: IF #DETAIL-LN.#MSG-2 NE ' '
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),pnd_Detail_Ln_Pnd_Msg_2);                                                                        //Natural: WRITE ( 1 ) 7T #DETAIL-LN.#MSG-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Return Code                  =",pnd_Detail_Ln_Pnd_Return_Code);                                     //Natural: WRITE ( 1 ) 7T 'Return Code                  =' #DETAIL-LN.#RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Error  Nbr                   =",pnd_Detail_Ln_Pnd_Error_Nbr);                                       //Natural: WRITE ( 1 ) 7T 'Error  Nbr                   =' #DETAIL-LN.#ERROR-NBR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Status returned from IATN402 =",pdaIatl402p.getPnd_Iatn402_Out_Pnd_Xfr_Stts_Cde());                 //Natural: WRITE ( 1 ) 7T 'Status returned from IATN402 =' #IATN402-OUT.#XFR-STTS-CDE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"RQST id                      =",iaa_Trnsfr_Sw_Rqst_Rqst_Id);                                        //Natural: WRITE ( 1 ) 7T 'RQST id                      =' IAA-TRNSFR-SW-RQST.RQST-ID
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Contract Nbr - Payee         =",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct,"-",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee); //Natural: WRITE ( 1 ) 7T 'Contract Nbr - Payee         =' IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT '-' IAA-TRNSFR-SW-RQST.IA-FRM-PAYEE
        if (Global.isEscape()) return;
        //*  WRITE-RPT1-LINE
    }
    private void sub_Write_Rpt2_Line() throws Exception                                                                                                                   //Natural: WRITE-RPT2-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cnt_Pnd_Rpt2_Count.nadd(1);                                                                                                                                   //Natural: ASSIGN #RPT2-COUNT := #RPT2-COUNT + 1
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_Cnt_Pnd_Rpt2_Count,pnd_Detail_Ln_Pnd_Msg);                                                                 //Natural: WRITE ( 2 ) / #RPT2-COUNT #DETAIL-LN.#MSG
        if (Global.isEscape()) return;
        if (condition(pnd_Detail_Ln_Pnd_Msg_2.notEquals(" ")))                                                                                                            //Natural: IF #DETAIL-LN.#MSG-2 NE ' '
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),pnd_Detail_Ln_Pnd_Msg_2);                                                                        //Natural: WRITE ( 2 ) 7T #DETAIL-LN.#MSG-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Detail_Ln_Pnd_Return_Code.notEquals(" ")))                                                                                                      //Natural: IF #DETAIL-LN.#RETURN-CODE NE ' '
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"Return Code                =",pnd_Detail_Ln_Pnd_Return_Code);                                   //Natural: WRITE ( 2 ) 7T 'Return Code                =' #DETAIL-LN.#RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Detail_Ln_Pnd_Error_Nbr.notEquals(" ")))                                                                                                        //Natural: IF #DETAIL-LN.#ERROR-NBR NE ' '
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"Error  Nbr                 =",pnd_Detail_Ln_Pnd_Error_Nbr);                                     //Natural: WRITE ( 2 ) 7T 'Error  Nbr                 =' #DETAIL-LN.#ERROR-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"Status returned from IATN402 =",pdaIatl402p.getPnd_Iatn402_Out_Pnd_Xfr_Stts_Cde());                 //Natural: WRITE ( 2 ) 7T 'Status returned from IATN402 =' #IATN402-OUT.#XFR-STTS-CDE
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"RQST id                      =",iaa_Trnsfr_Sw_Rqst_Rqst_Id);                                        //Natural: WRITE ( 2 ) 7T 'RQST id                      =' IAA-TRNSFR-SW-RQST.RQST-ID
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"Contract Nbr - Payee         =",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct,"-",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee); //Natural: WRITE ( 2 ) 7T 'Contract Nbr - Payee         =' IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT '-' IAA-TRNSFR-SW-RQST.IA-FRM-PAYEE
        if (Global.isEscape()) return;
        //*  WRITE-RPT2-LINE
    }
    private void sub_Write_Rpt3_Line() throws Exception                                                                                                                   //Natural: WRITE-RPT3-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cnt_Pnd_Rpt3_Count.nadd(1);                                                                                                                                   //Natural: ASSIGN #RPT3-COUNT := #RPT3-COUNT + 1
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,pnd_Cnt_Pnd_Rpt3_Count,pnd_Detail_Ln_Pnd_Msg);                                                                 //Natural: WRITE ( 3 ) / #RPT3-COUNT #DETAIL-LN.#MSG
        if (Global.isEscape()) return;
        if (condition(pnd_Detail_Ln_Pnd_Msg_2.notEquals(" ")))                                                                                                            //Natural: IF #DETAIL-LN.#MSG-2 NE ' '
        {
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),pnd_Detail_Ln_Pnd_Msg_2);                                                                        //Natural: WRITE ( 3 ) 7T #DETAIL-LN.#MSG-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Detail_Ln_Pnd_Return_Code.notEquals(" ")))                                                                                                      //Natural: IF #DETAIL-LN.#RETURN-CODE NE ' '
        {
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),"Return Code                =",pnd_Detail_Ln_Pnd_Return_Code);                                   //Natural: WRITE ( 3 ) 7T 'Return Code                =' #DETAIL-LN.#RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Detail_Ln_Pnd_Error_Nbr.notEquals(" ")))                                                                                                        //Natural: IF #DETAIL-LN.#ERROR-NBR NE ' '
        {
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),"Error  Nbr                 =",pnd_Detail_Ln_Pnd_Error_Nbr);                                     //Natural: WRITE ( 3 ) 7T 'Error  Nbr                 =' #DETAIL-LN.#ERROR-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),"Status returned from IATN402 =",pdaIatl402p.getPnd_Iatn402_Out_Pnd_Xfr_Stts_Cde());                 //Natural: WRITE ( 3 ) 7T 'Status returned from IATN402 =' #IATN402-OUT.#XFR-STTS-CDE
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),"RQST id                      =",iaa_Trnsfr_Sw_Rqst_Rqst_Id);                                        //Natural: WRITE ( 3 ) 7T 'RQST id                      =' IAA-TRNSFR-SW-RQST.RQST-ID
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(7),"Contract Nbr - Payee         =",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct,"-",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee); //Natural: WRITE ( 3 ) 7T 'Contract Nbr - Payee         =' IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT '-' IAA-TRNSFR-SW-RQST.IA-FRM-PAYEE
        if (Global.isEscape()) return;
        //*  WRITE-RPT3-LINE
    }
    private void sub_Write_Cmplt_Transfer_Totals() throws Exception                                                                                                       //Natural: WRITE-CMPLT-TRANSFER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(1).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 52
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(2).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 2 ) GT 52
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(3).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 3 ) GT 52
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Transfer Requests (M0).... ........",pnd_Cnt_Pnd_Total_Read_Cmplte);                                   //Natural: WRITE ( 1 ) 'Total Complete Transfer Requests (M0).... ........' #CNT.#TOTAL-READ-CMPLTE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Transfers processed successfully ..",pnd_Cnt_Pnd_Complete_Cm);                                         //Natural: WRITE ( 1 ) 'Total Complete Transfers processed successfully ..' #CNT.#COMPLETE-CM
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Transfers with errors .............",pnd_Cnt_Pnd_Error_Cm);                                            //Natural: WRITE ( 1 ) 'Total Complete Transfers with errors .............' #CNT.#ERROR-CM
        if (Global.isEscape()) return;
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Transfer Requests (M0).... ........",pnd_Cnt_Pnd_Total_Read_Cmplte);                                   //Natural: WRITE ( 2 ) 'Total Complete Transfer Requests (M0).... ........' #CNT.#TOTAL-READ-CMPLTE
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Transfers processed successfully ..",pnd_Cnt_Pnd_Complete_Cm);                                         //Natural: WRITE ( 2 ) 'Total Complete Transfers processed successfully ..' #CNT.#COMPLETE-CM
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Transfers with errors .............",pnd_Cnt_Pnd_Error_Cm);                                            //Natural: WRITE ( 2 ) 'Total Complete Transfers with errors .............' #CNT.#ERROR-CM
        if (Global.isEscape()) return;
        //*  WRITE-CMPLT-TRANSFER-TOTALS
    }
    private void sub_Write_Cmplt_Switch_Totals() throws Exception                                                                                                         //Natural: WRITE-CMPLT-SWITCH-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(1).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 52
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(2).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 2 ) GT 52
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(3).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 3 ) GT 52
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Switch Requests (M0) ..............",pnd_Cnt_Pnd_Total_Read_Cmplte);                                   //Natural: WRITE ( 1 ) 'Total Complete Switch Requests (M0) ..............' #CNT.#TOTAL-READ-CMPLTE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Switches processed successfully ...",pnd_Cnt_Pnd_Complete_Cm);                                         //Natural: WRITE ( 1 ) 'Total Complete Switches processed successfully ...' #CNT.#COMPLETE-CM
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Complete Switches with errors ..............",pnd_Cnt_Pnd_Error_Cm);                                            //Natural: WRITE ( 1 ) 'Total Complete Switches with errors ..............' #CNT.#ERROR-CM
        if (Global.isEscape()) return;
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Switch Requests (M0) ..............",pnd_Cnt_Pnd_Total_Read_Cmplte);                                   //Natural: WRITE ( 2 ) 'Total Complete Switch Requests (M0) ..............' #CNT.#TOTAL-READ-CMPLTE
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Switches processed successfully ...",pnd_Cnt_Pnd_Complete_Cm);                                         //Natural: WRITE ( 2 ) 'Total Complete Switches processed successfully ...' #CNT.#COMPLETE-CM
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Complete Switches with errors ..............",pnd_Cnt_Pnd_Error_Cm);                                            //Natural: WRITE ( 2 ) 'Total Complete Switches with errors ..............' #CNT.#ERROR-CM
        if (Global.isEscape()) return;
        //*  WRITE-CMPLT-SWITCH-TOTALS
    }
    private void sub_Write_Rjctd_Transfer_Totals() throws Exception                                                                                                       //Natural: WRITE-RJCTD-TRANSFER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(1).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 52
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(3).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 3 ) GT 52
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Transfer Rqsts read (P0 and P1)...",pnd_Cnt_Pnd_Total_Read_Rjctd);                                     //Natural: WRITE ( 1 ) 'Total Rejected Transfer Rqsts read (P0 and P1)...' #CNT.#TOTAL-READ-RJCTD
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Transfers processed successfully ..",pnd_Cnt_Pnd_Complete_Rj);                                         //Natural: WRITE ( 1 ) 'Total Rejected Transfers processed successfully ..' #CNT.#COMPLETE-RJ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Transfers with errors .............",pnd_Cnt_Pnd_Error_Rj);                                            //Natural: WRITE ( 1 ) 'Total Rejected Transfers with errors .............' #CNT.#ERROR-RJ
        if (Global.isEscape()) return;
        getReports().skip(3, 2);                                                                                                                                          //Natural: SKIP ( 3 ) 2
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Transfer Rqsts read (P0 and P1) ...",pnd_Cnt_Pnd_Total_Read_Rjctd);                                    //Natural: WRITE ( 3 ) 'Total Rejected Transfer Rqsts read (P0 and P1) ...' #CNT.#TOTAL-READ-RJCTD
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Transfers processed successfully ..",pnd_Cnt_Pnd_Complete_Rj);                                         //Natural: WRITE ( 3 ) 'Total Rejected Transfers processed successfully ..' #CNT.#COMPLETE-RJ
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Transfers with errors .............",pnd_Cnt_Pnd_Error_Rj);                                            //Natural: WRITE ( 3 ) 'Total Rejected Transfers with errors .............' #CNT.#ERROR-RJ
        if (Global.isEscape()) return;
        //*  WRITE-RJCTD-TRANSFER-TOTALS
    }
    private void sub_Write_Rjctd_Switch_Totals() throws Exception                                                                                                         //Natural: WRITE-RJCTD-SWITCH-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(1).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 52
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLineCount(3).greater(52)))                                                                                                       //Natural: IF *LINE-COUNT ( 3 ) GT 52
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Switch Rqsts read (P0 and P1) .....",pnd_Cnt_Pnd_Total_Read_Rjctd);                                    //Natural: WRITE ( 1 ) 'Total Rejected Switch Rqsts read (P0 and P1) .....' #CNT.#TOTAL-READ-RJCTD
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Switches processed successfully ...",pnd_Cnt_Pnd_Complete_Rj);                                         //Natural: WRITE ( 1 ) 'Total Rejected Switches processed successfully ...' #CNT.#COMPLETE-RJ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Rejected Switches with errors ..............",pnd_Cnt_Pnd_Error_Rj);                                            //Natural: WRITE ( 1 ) 'Total Rejected Switches with errors ..............' #CNT.#ERROR-RJ
        if (Global.isEscape()) return;
        getReports().skip(3, 2);                                                                                                                                          //Natural: SKIP ( 3 ) 2
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Switch Rqsts read (P0 and P1) .....",pnd_Cnt_Pnd_Total_Read_Rjctd);                                    //Natural: WRITE ( 3 ) 'Total Rejected Switch Rqsts read (P0 and P1) .....' #CNT.#TOTAL-READ-RJCTD
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Switches processed successfully ...",pnd_Cnt_Pnd_Complete_Rj);                                         //Natural: WRITE ( 3 ) 'Total Rejected Switches processed successfully ...' #CNT.#COMPLETE-RJ
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total Rejected Switches with errors ..............",pnd_Cnt_Pnd_Error_Rj);                                            //Natural: WRITE ( 3 ) 'Total Rejected Switches with errors ..............' #CNT.#ERROR-RJ
        if (Global.isEscape()) return;
        //*  WRITE-RJCTD-TRANSFER-TOTALS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_Count_Pnd_Report_1.nadd(1);                                                                                                                  //Natural: ASSIGN #PAGE-COUNT.#REPORT-1 := #PAGE-COUNT.#REPORT-1 + 1
                    pnd_M400h_Page.setValue(pnd_Page_Count_Pnd_Report_1);                                                                                                 //Natural: ASSIGN #M400H-PAGE := #PAGE-COUNT.#REPORT-1
                    pnd_Header1.setValue(pnd_Rpt1_Header1);                                                                                                               //Natural: MOVE #RPT1-HEADER1 TO #HEADER1
                    if (condition(pnd_L_Pnd_Transfer_Request.getBoolean()))                                                                                               //Natural: IF #TRANSFER-REQUEST
                    {
                        pnd_Header2.setValue(pnd_Rpt1_Header2_T);                                                                                                         //Natural: MOVE #RPT1-HEADER2-T TO #HEADER2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Header2.setValue(pnd_Rpt1_Header2_S);                                                                                                         //Natural: MOVE #RPT1-HEADER2-S TO #HEADER2
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm400h.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM400H'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_L_Pnd_Status_M0.getBoolean()))                                                                                                      //Natural: IF #STATUS-M0
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE," Requests Status Complete, Awaiting Confirmation (M0)",NEWLINE,"-----------------------------------------------------", //Natural: WRITE ( 1 ) / ' Requests Status Complete, Awaiting Confirmation (M0)' / '-----------------------------------------------------' /
                            NEWLINE);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE," Requests Status Rejected, Awaiting Confirmation (P0 or P1)",NEWLINE,"-----------------------------------------------------------", //Natural: WRITE ( 1 ) / ' Requests Status Rejected, Awaiting Confirmation (P0 or P1)' / '-----------------------------------------------------------' /
                            NEWLINE);
                    }                                                                                                                                                     //Natural: END-IF
                    //*  COMPLETE RQSTS REPORT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_Count_Pnd_Report_2.nadd(1);                                                                                                                  //Natural: ASSIGN #PAGE-COUNT.#REPORT-2 := #PAGE-COUNT.#REPORT-2 + 1
                    pnd_M400h_Page.setValue(pnd_Page_Count_Pnd_Report_2);                                                                                                 //Natural: ASSIGN #M400H-PAGE := #PAGE-COUNT.#REPORT-2
                    pnd_Header1.setValue(pnd_Rpt2_Header1);                                                                                                               //Natural: MOVE #RPT2-HEADER1 TO #HEADER1
                    if (condition(pnd_L_Pnd_Transfer_Request.getBoolean()))                                                                                               //Natural: IF #TRANSFER-REQUEST
                    {
                        pnd_Header2.setValue(pnd_Rpt2_Header2_T);                                                                                                         //Natural: MOVE #RPT2-HEADER2-T TO #HEADER2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Header2.setValue(pnd_Rpt2_Header2_S);                                                                                                         //Natural: MOVE #RPT2-HEADER2-S TO #HEADER2
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iatm400h.class));                                                                  //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IATM400H'
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE," Requests Status Complete, Awaiting Confirmation (M0)",NEWLINE,"-----------------------------------------------------", //Natural: WRITE ( 2 ) / ' Requests Status Complete, Awaiting Confirmation (M0)' / '-----------------------------------------------------' /
                        NEWLINE);
                    //*  RJCTD RQSTS REPORT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_Count_Pnd_Report_3.nadd(1);                                                                                                                  //Natural: ASSIGN #PAGE-COUNT.#REPORT-3 := #PAGE-COUNT.#REPORT-3 + 1
                    pnd_M400h_Page.setValue(pnd_Page_Count_Pnd_Report_3);                                                                                                 //Natural: ASSIGN #M400H-PAGE := #PAGE-COUNT.#REPORT-3
                    pnd_Header1.setValue(pnd_Rpt2_Header1);                                                                                                               //Natural: MOVE #RPT2-HEADER1 TO #HEADER1
                    if (condition(pnd_L_Pnd_Transfer_Request.getBoolean()))                                                                                               //Natural: IF #TRANSFER-REQUEST
                    {
                        pnd_Header2.setValue(pnd_Rpt3_Header2_T);                                                                                                         //Natural: MOVE #RPT3-HEADER2-T TO #HEADER2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Header2.setValue(pnd_Rpt3_Header2_S);                                                                                                         //Natural: MOVE #RPT3-HEADER2-S TO #HEADER2
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(3, ReportOption.NOTITLE, writeMapToStringOutput(Iatm400h.class));                                                                  //Natural: WRITE ( 3 ) NOTITLE USING FORM 'IATM400H'
                    getReports().skip(3, 1);                                                                                                                              //Natural: SKIP ( 3 ) 1
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE," Requests Status Rejected, Awaiting Confirmation (P0 or P1)",NEWLINE,"-----------------------------------------------------------", //Natural: WRITE ( 3 ) / ' Requests Status Rejected, Awaiting Confirmation (P0 or P1)' / '-----------------------------------------------------------' /
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
    }
}
