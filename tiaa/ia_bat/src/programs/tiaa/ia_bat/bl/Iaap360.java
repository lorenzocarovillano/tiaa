/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:25:51 PM
**        * FROM NATURAL PROGRAM : Iaap360
************************************************************
**        * FILE NAME            : Iaap360.java
**        * CLASS NAME           : Iaap360
**        * INSTANCE NAME        : Iaap360
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAN360    READS IA EXTRACT FLAT FILE             *
*      DATE     -  2/96       & CREATES OLD IA MASTER RECORD LEVEL 10*
*                             20, & 70 INPUT TO AUTO COMBINE         *
*                             JOB P1620IAM                           *
*   HISTORY                                                          *
*    1/99     - ADD LOGIC TO BYPASS WRITING RECORDS FOR PA ORIGIN    *
*               CODES 37 & 38 CANNOT BE COMBINED                     *
*               DO SCAN ON 1/99                                      *
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017                 *
* 04/2017  O SOTTO  RE-STOWED ONLY.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap360 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Cde;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_W_Check_Date;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orgin_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record_Pnd_Filler_1z;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill2;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill5;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_3a;

    private DbsGroup pnd_Input_Record_Pnd_Cpr_Ivc_Info;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Company_Cd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Percent;

    private DbsGroup pnd_Input_Record__R_Field_5;
    private DbsField pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Xref;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Dod;
    private DbsField pnd_Input_Record_Pnd_Filler_4a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Cde;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Ytd_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Tot_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Intent_Code;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Stp_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Final_Dte;

    private DbsGroup pnd_Input_Record__R_Field_7;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Fund_Cde;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Filler_1c;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Units_Cnt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_2;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_8;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_9;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2;
    private DbsField pnd_Next_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_10;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_11;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Master_Work_Rec;

    private DbsGroup pnd_Master_Work_Rec__R_Field_12;
    private DbsField pnd_Master_Work_Rec_Contract_No_M;
    private DbsField pnd_Master_Work_Rec_Payee_M;
    private DbsField pnd_Master_Work_Rec_Rec_Type_M;

    private DbsGroup pnd_Master_Work_Rec_Rec_Type_10;
    private DbsField pnd_Master_Work_Rec_Filler_1;
    private DbsField pnd_Master_Work_Rec_Product_M;
    private DbsField pnd_Master_Work_Rec_Currency_M;
    private DbsField pnd_Master_Work_Rec_Mode_M;
    private DbsField pnd_Master_Work_Rec_Filler_2;
    private DbsField pnd_Master_Work_Rec_Option_M;
    private DbsField pnd_Master_Work_Rec_Filler_3;
    private DbsField pnd_Master_Work_Rec_J_C_Code_M;
    private DbsField pnd_Master_Work_Rec_Filler_3a;

    private DbsGroup pnd_Master_Work_Rec__R_Field_13;
    private DbsField pnd_Master_Work_Rec_Filler_4;
    private DbsField pnd_Master_Work_Rec_X_Ref_1_M;
    private DbsField pnd_Master_Work_Rec_Sex_1_M;
    private DbsField pnd_Master_Work_Rec_Dob_1_M;
    private DbsField pnd_Master_Work_Rec_Dod_1_M;
    private DbsField pnd_Master_Work_Rec_Filler_5;
    private DbsField pnd_Master_Work_Rec_X_Ref_2_M;
    private DbsField pnd_Master_Work_Rec_Sex2_M;
    private DbsField pnd_Master_Work_Rec_Dob_2_M;
    private DbsField pnd_Master_Work_Rec_Dod_2_M;
    private DbsField pnd_Master_Work_Rec_Filler;

    private DbsGroup pnd_Master_Work_Rec__R_Field_14;
    private DbsField pnd_Master_Work_Rec_Filler_6;
    private DbsField pnd_Master_Work_Rec_Soc_Sec_No_M;
    private DbsField pnd_Master_Work_Rec_Filler_7;

    private DbsGroup pnd_Master_Work_Rec__R_Field_15;
    private DbsField pnd_Master_Work_Rec_High_Value;
    private DbsField pnd_Master_Work_Rec_Filler1;

    private DbsGroup pnd_Sve_10_Rec_Data;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Opt;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M;
    private DbsField pnd_Sve_Cntrct_Nbr;
    private DbsField pnd_Inactive_Cntrct;
    private DbsField pnd_Wrte_700_Sw;
    private DbsField pnd_Gen_700_Trn_Sw;
    private DbsField pnd_Gen_006_Trn_Sw;
    private DbsField pnd_Tot_Trans_Out;
    private DbsField pnd_Tot_Tran_10;
    private DbsField pnd_Tot_Tran_20;
    private DbsField pnd_Tot_Tran_70;
    private DbsField pnd_Fst_Tme;
    private DbsField pnd_Sel_Record;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Input_Record__R_Field_1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1", "#CNTRCT-PPCN-NBR-1", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9", "#CNTRCT-PPCN-NBR-9", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Cde", "#RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            251);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_W_Check_Date = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_W_Check_Date", "#W-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Orgin_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orgin_Cde", "#CNTRCT-ORGIN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Filler_1z = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1z", "#FILLER-1Z", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_Fill1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill1", "#CNTRCT-FILL1", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I", 
            "#CNTRCT-JOINT-CNVRT-RCRCD-I", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Fill2 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill2", "#CNTRCT-FILL2", FieldType.STRING, 
            11);
        pnd_Input_Record_Pnd_Cntrct_1st_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Xref", "#CNTRCT-1ST-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_1st_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dob", "#CNTRCT-1ST-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Fill3 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill3", "#CNTRCT-FILL3", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex", "#CNTRCT-1ST-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt", "#CNTRCT-1ST-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dod", "#CNTRCT-1ST-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Xref", "#CNTRCT-2ND-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dob", "#CNTRCT-2ND-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Fill5 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill5", "#CNTRCT-FILL5", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex", "#CNTRCT-2ND-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt", "#CNTRCT-2ND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dod", "#CNTRCT-2ND-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn", "#CNTRCT-2ND-ANNT-SSN", 
            FieldType.NUMERIC, 9);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cpr_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Record_Pnd_Lst_Trans_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde", "#PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde", "#PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw", "#PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr", "#PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ", "#PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Actvty_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Filler_3a = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3a", "#FILLER-3A", FieldType.STRING, 
            5);

        pnd_Input_Record_Pnd_Cpr_Ivc_Info = pnd_Input_Record__R_Field_4.newGroupInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info", "#CPR-IVC-INFO");
        pnd_Input_Record_Pnd_Cntrct_Company_Cd = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Company_Cd", "#CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind", 
            "#CNTRCT-RCVRY-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt", 
            "#CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt", 
            "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Percent = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Percent", "#CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5));

        pnd_Input_Record__R_Field_5 = pnd_Input_Record__R_Field_4.newGroupInGroup("pnd_Input_Record__R_Field_5", "REDEFINE", pnd_Input_Record_Pnd_Cpr_Ivc_Info);
        pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha", "#CPR-IVC-INFO-ALPHA", 
            FieldType.STRING, 155);
        pnd_Input_Record_Pnd_Cntrct_Mode_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte", "#CNTRCT-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte", "#CNTRCT-FINAL-PER-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte", "#CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Bnfcry_Xref = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Xref", "#BNFCRY-XREF", FieldType.STRING, 
            9);
        pnd_Input_Record_Pnd_Bnfcry_Dod = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Dod", "#BNFCRY-DOD", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_Filler_4a = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_4a", "#FILLER-4A", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde", "#CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4);

        pnd_Input_Record__R_Field_6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Ddctn_Id_Nbr = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Id_Nbr", "#DDCTN-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Record_Pnd_Ddctn_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Ddctn_Seq_Nbr = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Input_Record_Pnd_Ddctn_Payee = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Input_Record_Pnd_Ddctn_Ytd_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Ytd_Amt", "#DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte", "#DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Ddctn_Tot_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Tot_Amt", "#DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Ddctn_Intent_Code = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Intent_Code", "#DDCTN-INTENT-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Ddctn_Strt_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Stp_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Final_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Final_Dte", "#DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Input_Record__R_Field_7 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_7", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cmpny_Cde", "#TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Tiaa_Fund_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Fund_Cde", "#TIAA-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Ivc_Amt", "#TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Rtb_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rtb_Amt", "#TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt", "#TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt", "#TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Old_Per_Amt", "#TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Old_Div_Amt", "#TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Filler_1c = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1c", "#FILLER-1C", FieldType.STRING, 
            6);
        pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt", "#TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Per_Div_Amt", "#TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Units_Cnt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Units_Cnt", "#TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            7, 3);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt", "#TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt", "#TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record_2 = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_2", "#REST-OF-RECORD-2", FieldType.STRING, 
            63);
        pnd_Curr_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Curr_Check_Date_Ccyymmdd", "#CURR-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_8", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy = pnd_Curr_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy", 
            "#CURR-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm", 
            "#CURR-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd = pnd_Curr_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd", 
            "#CURR-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_9 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_9", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_9.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm", 
            "#CURR-CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2 = pnd_Curr_Check_Date_Ccyymmdd__R_Field_9.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2", 
            "#CURR-CHECK-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Next_Check_Date_Ccyymmdd", "#NEXT-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_10", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy = pnd_Next_Check_Date_Ccyymmdd__R_Field_10.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy", 
            "#NEXT-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm = pnd_Next_Check_Date_Ccyymmdd__R_Field_10.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm", 
            "#NEXT-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd = pnd_Next_Check_Date_Ccyymmdd__R_Field_10.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd", 
            "#NEXT-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_11 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_11", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm = pnd_Next_Check_Date_Ccyymmdd__R_Field_11.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm", 
            "#NEXT-CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2 = pnd_Next_Check_Date_Ccyymmdd__R_Field_11.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2", 
            "#NEXT-CHECK-DD2", FieldType.NUMERIC, 2);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 2);
        pnd_Master_Work_Rec = localVariables.newFieldInRecord("pnd_Master_Work_Rec", "#MASTER-WORK-REC", FieldType.STRING, 100);

        pnd_Master_Work_Rec__R_Field_12 = localVariables.newGroupInRecord("pnd_Master_Work_Rec__R_Field_12", "REDEFINE", pnd_Master_Work_Rec);
        pnd_Master_Work_Rec_Contract_No_M = pnd_Master_Work_Rec__R_Field_12.newFieldInGroup("pnd_Master_Work_Rec_Contract_No_M", "CONTRACT-NO-M", FieldType.STRING, 
            8);
        pnd_Master_Work_Rec_Payee_M = pnd_Master_Work_Rec__R_Field_12.newFieldInGroup("pnd_Master_Work_Rec_Payee_M", "PAYEE-M", FieldType.NUMERIC, 2);
        pnd_Master_Work_Rec_Rec_Type_M = pnd_Master_Work_Rec__R_Field_12.newFieldInGroup("pnd_Master_Work_Rec_Rec_Type_M", "REC-TYPE-M", FieldType.NUMERIC, 
            2);

        pnd_Master_Work_Rec_Rec_Type_10 = pnd_Master_Work_Rec__R_Field_12.newGroupInGroup("pnd_Master_Work_Rec_Rec_Type_10", "REC-TYPE-10");
        pnd_Master_Work_Rec_Filler_1 = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_1", "FILLER-1", FieldType.STRING, 3);
        pnd_Master_Work_Rec_Product_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Product_M", "PRODUCT-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Currency_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Currency_M", "CURRENCY-M", FieldType.NUMERIC, 
            1);
        pnd_Master_Work_Rec_Mode_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Mode_M", "MODE-M", FieldType.NUMERIC, 3);
        pnd_Master_Work_Rec_Filler_2 = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_2", "FILLER-2", FieldType.STRING, 6);
        pnd_Master_Work_Rec_Option_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Option_M", "OPTION-M", FieldType.NUMERIC, 
            2);
        pnd_Master_Work_Rec_Filler_3 = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_3", "FILLER-3", FieldType.STRING, 52);
        pnd_Master_Work_Rec_J_C_Code_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_J_C_Code_M", "J-C-CODE-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Filler_3a = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_3a", "FILLER-3A", FieldType.STRING, 
            19);

        pnd_Master_Work_Rec__R_Field_13 = pnd_Master_Work_Rec__R_Field_12.newGroupInGroup("pnd_Master_Work_Rec__R_Field_13", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_Filler_4 = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Filler_4", "FILLER-4", FieldType.STRING, 3);
        pnd_Master_Work_Rec_X_Ref_1_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_X_Ref_1_M", "X-REF-1-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Sex_1_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Sex_1_M", "SEX-1-M", FieldType.NUMERIC, 1);
        pnd_Master_Work_Rec_Dob_1_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Dob_1_M", "DOB-1-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Dod_1_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Dod_1_M", "DOD-1-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Filler_5 = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Filler_5", "FILLER-5", FieldType.STRING, 10);
        pnd_Master_Work_Rec_X_Ref_2_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_X_Ref_2_M", "X-REF-2-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Sex2_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Sex2_M", "SEX2-M", FieldType.NUMERIC, 1);
        pnd_Master_Work_Rec_Dob_2_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Dob_2_M", "DOB-2-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Dod_2_M = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Dod_2_M", "DOD-2-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Filler = pnd_Master_Work_Rec__R_Field_13.newFieldInGroup("pnd_Master_Work_Rec_Filler", "FILLER", FieldType.STRING, 37);

        pnd_Master_Work_Rec__R_Field_14 = pnd_Master_Work_Rec__R_Field_12.newGroupInGroup("pnd_Master_Work_Rec__R_Field_14", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_Filler_6 = pnd_Master_Work_Rec__R_Field_14.newFieldInGroup("pnd_Master_Work_Rec_Filler_6", "FILLER-6", FieldType.STRING, 57);
        pnd_Master_Work_Rec_Soc_Sec_No_M = pnd_Master_Work_Rec__R_Field_14.newFieldInGroup("pnd_Master_Work_Rec_Soc_Sec_No_M", "SOC-SEC-NO-M", FieldType.NUMERIC, 
            9);
        pnd_Master_Work_Rec_Filler_7 = pnd_Master_Work_Rec__R_Field_14.newFieldInGroup("pnd_Master_Work_Rec_Filler_7", "FILLER-7", FieldType.STRING, 22);

        pnd_Master_Work_Rec__R_Field_15 = pnd_Master_Work_Rec__R_Field_12.newGroupInGroup("pnd_Master_Work_Rec__R_Field_15", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_High_Value = pnd_Master_Work_Rec__R_Field_15.newFieldInGroup("pnd_Master_Work_Rec_High_Value", "HIGH-VALUE", FieldType.BINARY, 
            4);
        pnd_Master_Work_Rec_Filler1 = pnd_Master_Work_Rec__R_Field_15.newFieldInGroup("pnd_Master_Work_Rec_Filler1", "FILLER1", FieldType.STRING, 84);

        pnd_Sve_10_Rec_Data = localVariables.newGroupInRecord("pnd_Sve_10_Rec_Data", "#SVE-10-REC-DATA");
        pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde", "#SVE-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Opt = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Opt", "#SVE-OPT", FieldType.NUMERIC, 3);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte", "#SVE-ISSU-DTE", FieldType.NUMERIC, 
            6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M", "#SVE-J-C-CODE-M", FieldType.STRING, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M", "#SVE-X-REF-1-M", FieldType.STRING, 
            9);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M", "#SVE-SEX-1-M", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M", "#SVE-DOB-1-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M", "#SVE-DOD-1-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M", "#SVE-X-REF-2-M", FieldType.STRING, 
            9);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M", "#SVE-SEX2-M", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M", "#SVE-DOB-2-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M", "#SVE-DOD-2-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Sve_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Sve_Cntrct_Nbr", "#SVE-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Inactive_Cntrct = localVariables.newFieldInRecord("pnd_Inactive_Cntrct", "#INACTIVE-CNTRCT", FieldType.STRING, 1);
        pnd_Wrte_700_Sw = localVariables.newFieldInRecord("pnd_Wrte_700_Sw", "#WRTE-700-SW", FieldType.STRING, 1);
        pnd_Gen_700_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_700_Trn_Sw", "#GEN-700-TRN-SW", FieldType.STRING, 1);
        pnd_Gen_006_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_006_Trn_Sw", "#GEN-006-TRN-SW", FieldType.STRING, 1);
        pnd_Tot_Trans_Out = localVariables.newFieldInRecord("pnd_Tot_Trans_Out", "#TOT-TRANS-OUT", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_10 = localVariables.newFieldInRecord("pnd_Tot_Tran_10", "#TOT-TRAN-10", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_20 = localVariables.newFieldInRecord("pnd_Tot_Tran_20", "#TOT-TRAN-20", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_70 = localVariables.newFieldInRecord("pnd_Tot_Tran_70", "#TOT-TRAN-70", FieldType.NUMERIC, 9);
        pnd_Fst_Tme = localVariables.newFieldInRecord("pnd_Fst_Tme", "#FST-TME", FieldType.STRING, 1);
        pnd_Sel_Record = localVariables.newFieldInRecord("pnd_Sel_Record", "#SEL-RECORD", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Fst_Tme.setInitialValue("Y");
        pnd_Sel_Record.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap360() throws Exception
    {
        super("Iaap360");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP360", onError);
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            short decideConditionsMet209 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RECORD-CDE;//Natural: VALUE 00
            if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(0))))
            {
                decideConditionsMet209++;
                if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER")))                                                                                 //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER'
                {
                    pnd_Curr_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #CURR-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #NEXT-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #NEXT-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.nadd(1);                                                                                               //Natural: ADD 1 TO #NEXT-CHECK-MM
                    if (condition(pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.greater(12)))                                                                            //Natural: IF #NEXT-CHECK-MM GT 12
                    {
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.setValue(1);                                                                                       //Natural: MOVE 1 TO #NEXT-CHECK-MM
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy.nadd(1);                                                                                         //Natural: ADD 1 TO #NEXT-CHECK-CCYY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(10))))
            {
                decideConditionsMet209++;
                //*  ADDED FOLLOWING IF STMNT  1/99
                if (condition(pnd_Input_Record_Pnd_Cntrct_Orgin_Cde.equals(37) || pnd_Input_Record_Pnd_Cntrct_Orgin_Cde.equals(38) || pnd_Input_Record_Pnd_Cntrct_Orgin_Cde.equals(40))) //Natural: IF #CNTRCT-ORGIN-CDE = 37 OR = 38 OR = 40
                {
                    pnd_Sel_Record.setValue("N");                                                                                                                         //Natural: ASSIGN #SEL-RECORD := 'N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sel_Record.setValue("Y");                                                                                                                         //Natural: ASSIGN #SEL-RECORD := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD  1/99
                pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde);                                                                  //Natural: ASSIGN #SVE-CRRNCY-CDE := #CNTRCT-CRRNCY-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.setValue(pnd_Input_Record_Pnd_Cntrct_Optn_Cde);                                                                           //Natural: ASSIGN #SVE-OPT := #CNTRCT-OPTN-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte);                                                                     //Natural: ASSIGN #SVE-ISSU-DTE := #CNTRCT-ISSUE-DTE
                pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M.setValue(pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I);                                                         //Natural: ASSIGN #SVE-J-C-CODE-M := #CNTRCT-JOINT-CNVRT-RCRCD-I
                pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Xref);                                                                     //Natural: ASSIGN #SVE-X-REF-1-M := #CNTRCT-1ST-XREF
                pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex);                                                                   //Natural: ASSIGN #SVE-SEX-1-M := #CNTRCT-1ST-ANNT-SEX
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dob);                                                                        //Natural: ASSIGN #SVE-DOB-1-M := #CNTRCT-1ST-DOB
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dod);                                                                        //Natural: ASSIGN #SVE-DOD-1-M := #CNTRCT-1ST-DOD
                pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Xref);                                                                     //Natural: ASSIGN #SVE-X-REF-2-M := #CNTRCT-2ND-XREF
                pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex);                                                                    //Natural: ASSIGN #SVE-SEX2-M := #CNTRCT-2ND-ANNT-SEX
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dob);                                                                        //Natural: ASSIGN #SVE-DOB-2-M := #CNTRCT-2ND-DOB
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dod);                                                                        //Natural: ASSIGN #SVE-DOD-2-M := #CNTRCT-2ND-DOD
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(20))))
            {
                decideConditionsMet209++;
                pnd_Master_Work_Rec_Rec_Type_M.setValue(10);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 10
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_Mode_M.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                //Natural: ASSIGN MODE-M := #CNTRCT-MODE-IND
                pnd_Master_Work_Rec_Currency_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde);                                                                          //Natural: ASSIGN CURRENCY-M := #SVE-CRRNCY-CDE
                pnd_Master_Work_Rec_Option_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt);                                                                                   //Natural: ASSIGN OPTION-M := #SVE-OPT
                pnd_Master_Work_Rec_J_C_Code_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M);                                                                          //Natural: ASSIGN J-C-CODE-M := #SVE-J-C-CODE-M
                //*  ADDED 1/99
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_10.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-10
                    //*  ADDED 1/99
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
                pnd_Master_Work_Rec_Rec_Type_M.setValue(20);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 20
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_X_Ref_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M);                                                                            //Natural: ASSIGN X-REF-1-M := #SVE-X-REF-1-M
                pnd_Master_Work_Rec_Sex_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M);                                                                                //Natural: ASSIGN SEX-1-M := #SVE-SEX-1-M
                pnd_Master_Work_Rec_Dob_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M);                                                                                //Natural: ASSIGN DOB-1-M := #SVE-DOB-1-M
                pnd_Master_Work_Rec_Dod_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M);                                                                                //Natural: ASSIGN DOD-1-M := #SVE-DOD-1-M
                pnd_Master_Work_Rec_X_Ref_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M);                                                                            //Natural: ASSIGN X-REF-2-M := #SVE-X-REF-2-M
                pnd_Master_Work_Rec_Sex2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M);                                                                                  //Natural: ASSIGN SEX2-M := #SVE-SEX2-M
                pnd_Master_Work_Rec_Dob_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M);                                                                                //Natural: ASSIGN DOB-2-M := #SVE-DOB-2-M
                pnd_Master_Work_Rec_Dod_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M);                                                                                //Natural: ASSIGN DOD-2-M := #SVE-DOD-2-M
                //*  ADDED 1/99
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_20.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-20
                    //*  ADDED 1/99
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
                pnd_Master_Work_Rec_Rec_Type_M.setValue(70);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 70
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_Soc_Sec_No_M.setValue(pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr);                                                                       //Natural: ASSIGN SOC-SEC-NO-M := #PRTCPNT-TAX-ID-NBR
                //*  ADDED 1/99
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_70.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-70
                    //*  ADDED 1/99
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Master_Work_Rec.reset();                                                                                                                                      //Natural: RESET #MASTER-WORK-REC
        pnd_Master_Work_Rec_High_Value.setValue(-255);                                                                                                                    //Natural: ASSIGN HIGH-VALUE := -255
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
        sub_Write_Tran_Record();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 10 RECORDS ----------------->",pnd_Tot_Tran_10, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 10 RECORDS ----------------->' #TOT-TRAN-10 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 20 RECORDS ----------------->",pnd_Tot_Tran_20, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 20 RECORDS ----------------->' #TOT-TRAN-20 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 70 RECORDS ----------------->",pnd_Tot_Tran_70, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 70 RECORDS ----------------->' #TOT-TRAN-70 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL MASTER RECORDS GENERATED --------->",pnd_Tot_Trans_Out, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' TOTAL MASTER RECORDS GENERATED --------->' #TOT-TRANS-OUT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE ( 1 ) / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        //*  ----------------------------------------------                                                                                                               //Natural: ON ERROR
        //*  WRITE TRAN RECORD
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAN-RECORD
    }
    private void sub_Write_Tran_Record() throws Exception                                                                                                                 //Natural: WRITE-TRAN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(2, false, pnd_Master_Work_Rec);                                                                                                              //Natural: WRITE WORK FILE 2 #MASTER-WORK-REC
        pnd_Tot_Trans_Out.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-TRANS-OUT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN OLD IA MASTER RECORDS GENERATED FOR AUTO COMBINE FOR CHECK DATE: ",pnd_Next_Check_Date_Ccyymmdd,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN OLD IA MASTER RECORDS GENERATED FOR AUTO COMBINE FOR CHECK DATE: ' #NEXT-CHECK-DATE-CCYYMMDD ( EM = 9999/99/99 ) 4X 'PAGE: ' #W-PAGE-CTR ( EM = Z9 )
                        new ReportEditMask ("9999/99/99"),new ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("Z9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,"                                                      ");                       //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT '                                                      '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                       //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
