/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:59 PM
**        * FROM NATURAL PROGRAM : Iaap720
************************************************************
**        * FILE NAME            : Iaap720.java
**        * CLASS NAME           : Iaap720
**        * INSTANCE NAME        : Iaap720
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM :- IAAP720                                               *
*   DESC    :- PROGRAM CALCULATES AND PRINTS                         *
*              IA ADMINISTRATION CONTROL SUMMARY                     *
*              FOR PAYMENTS DUE                                      *
*                                                                    *
*   WORK FILE 1:- JOIN WORK FILE 2 FROM PROGRAM IAAP300 AND WORK     *
*                 FILE 1 FROM PROGRAM IAAP301 AND THEN SORT          *
*   WORK FILE 2:- READS THE CALL TYPE USED TO RETRIEVE THE ANNUITY   *
*                 UNIT VALUE. (FILE MUST CONTAIN "P" FOR ORIGINAL    *
*                 SETTLEMENT OR "L" FOR LATEST FACTOR                *
*                                                                    *
*   HISTORY:                                                         *
*           -USE EXTERNAL FILE FOR VALID CERF PRODUCTS & DESCRIPTIONS*
*              DO SCAN ON 8/96                                       *
*                                                                    *
*           -KN 03/97 FOR ANNUAL UNIT BASED FUNDS USE PAYMENT AMOUNT *
*                     ON THE FILE                                    *
*                                                                    *
*           -LB 10/97 REPORT CHANGED TO SHOW THE TOTALS FOR ANNUAL   *
*                     AND MONTHLY FUNDS                              *
*                                                                    *
*            10/01  ADDED LOGIC TO ACCUM ROLLOVER & REINVEST         *
*                   CONTRACTS *  *
*                   DO SCAN ON 10/01                                 *
*             6/03  ADDED NEW ROLL DEST CODES 57BT & 57BC            *
*                   CONTRACTS INCREASED INPUT(FILE) TO 346           *
*                   DO SCAN ON  6/03                                 *
*             2/04  ADDED LOGIC TO INTERFACE WITH ACTUARY TO         *
*                   PASS UNITS & PMTS FOR CREF FUNDS WITH CALL TO    *
*                   AN ACTUARY MODULE                                *
*                   DO SCAN ON  2/04                                 *
*             4/08  EXPANDED INPUT FILE FOR ROTH  SCAN 4/08          *
*             9/08  ADDED TIAA ACCESS FUND        SCAN 9/08          *
*             6/09  ADDED ANNUALIZED PMTS/UNITS   SCAN 6/09          *
*             7/09  FIXED ANNUALIZED PMTS/UNITS   SCAN 7/09          *
*            10/09  BYPASSED PEND CODE R WHEN UPDATING AUV           *
*                   COLLECTION FILE               SCAN 10/09         *
*            11/09  TURNED ZERO PRINT = ON        SCAN 11/09         *
*             3/12  RATE BASE EXPANSION CHANGES   SCAN 3/12          *
*            05/17 OS PIN EXPANSION CHANGES MARKED 082017.           *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap720 extends BLNatBase
{
    // Data Areas
    private PdaIaaa0510 pdaIaaa0510;
    private LdaIaal050 ldaIaal050;
    private PdaIaaa051z pdaIaaa051z;
    private PdaAiaa093 pdaAiaa093;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Fund_3;
    private DbsField pnd_Parm_Desc;

    private DbsGroup pnd_Parm_Desc__R_Field_1;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_6;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_Rem;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;
    private DbsField pnd_I;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_Save_Pnd_Cntrct_Per_Ivc;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;

    private DbsGroup pnd_Input_Record_Work_2;
    private DbsField pnd_Input_Record_Work_2_Pnd_Call_Type;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Code;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_344;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Input_Hdr_Rec_Info;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Input_Record__Filler1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record__Filler2;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input_Record__Filler3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record__Filler4;
    private DbsField pnd_Input_Record_Pnd_Cpr_Status_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_3a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Cash_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Term_Cde;

    private DbsGroup pnd_Input_Record_Pnd_Cpr_Ivc_Info;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Company_Cd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Percent;
    private DbsField pnd_Input_Record_Pnd_Cpr_Pay_Mode;
    private DbsField pnd_Input_Record__Filler5;
    private DbsField pnd_Input_Record_Pnd_Pend_Cde;
    private DbsField pnd_Input_Record__Filler6;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Dist;

    private DbsGroup pnd_Input_Record__R_Field_5;
    private DbsField pnd_Input_Record__Filler7;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record__Filler8;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input_Record__R_Field_7;
    private DbsField pnd_Input_Record_Pnd_Summ_Fund_Cde_N;
    private DbsField pnd_Input_Record__Filler9;
    private DbsField pnd_Input_Record_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Record_Pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Input_Record__Filler10;
    private DbsField pnd_Input_Record_Pnd_Summ_Units;
    private DbsField pnd_Input_Record_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Record_Pnd_Summ_Fin_Dvdnd;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_8;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;

    private DbsGroup pnd_Final_Totals_Report;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Inactive_Payees;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Deds_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Deds_Amt;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd;

    private DbsGroup pnd_Final_Totals_Report__R_Field_9;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Divd;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Actv;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Amt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd;

    private DbsGroup pnd_Final_Totals_Report__R_Field_10;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Divd;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units;
    private DbsField pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt;
    private DbsField pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt;
    private DbsField pend_Annual_Annlzd_Units;
    private DbsField pend_Annual_Annlzd_Payts;
    private DbsField pend_Annual_Units;
    private DbsField pend_Annual_Payments;
    private DbsField pend_Monthly_Units;
    private DbsField pend_Monthly_Payments;
    private DbsField pnd_Fctr;
    private DbsField pnd_W_Unit;
    private DbsField pnd_W_Pmt;
    private DbsField pnd_Prod_Cnt;
    private DbsField total_Ivc_Amt;
    private DbsField total_Ivc_Amt_P;
    private DbsField total_Roll_Ac_Ivc_Amt;
    private DbsField total_Roll_Ac_Ivc_Amt_P;
    private DbsField total_Roll_Tpa_Ivc_Amt;
    private DbsField total_Roll_Tpa_Ivc_Amt_P;
    private DbsField pnd_Save_Optn_Cde;
    private DbsField pnd_Save_Pend_Cde;
    private DbsField pnd_Prod_Counter;
    private DbsField pnd_Prod_Counter_Pa;
    private DbsField pnd_Max_Prd_Cde;
    private DbsField c;

    private DbsGroup pnd_Tpa;
    private DbsField pnd_Tpa_Pnd_Fund;
    private DbsField pnd_Tpa_Pnd_Pay_Amt;
    private DbsField pnd_Tpa_Pnd_Div_Amt;
    private DbsField pnd_Tpa_Pnd_Fin_Pay;
    private DbsField pnd_Tpa_Pnd_Fin_Div;
    private DbsField pnd_Tpa_Pnd_Fund_P;
    private DbsField pnd_Tpa_Pnd_Pay_Amt_P;
    private DbsField pnd_Tpa_Pnd_Div_Amt_P;
    private DbsField pnd_Tpa_Pnd_Fin_Pay_P;
    private DbsField pnd_Tpa_Pnd_Fin_Div_P;
    private DbsField pnd_Tpa_Pnd_Fund_1;
    private DbsField pnd_Tpa_Pnd_Pay_Amt_1;
    private DbsField pnd_Tpa_Pnd_Div_Amt_1;
    private DbsField pnd_Tpa_Pnd_Fin_Pay_1;
    private DbsField pnd_Tpa_Pnd_Fin_Div_1;
    private DbsField pnd_Tpa_Pnd_Fund_P_1;
    private DbsField pnd_Tpa_Pnd_Pay_Amt_P_1;
    private DbsField pnd_Tpa_Pnd_Div_Amt_P_1;
    private DbsField pnd_Tpa_Pnd_Fin_Pay_P_1;
    private DbsField pnd_Tpa_Pnd_Fin_Div_P_1;
    private DbsField pnd_Tpa_Pnd_Fund_2;
    private DbsField pnd_Tpa_Pnd_Pay_Amt_2;
    private DbsField pnd_Tpa_Pnd_Div_Amt_2;
    private DbsField pnd_Tpa_Pnd_Fin_Pay_2;
    private DbsField pnd_Tpa_Pnd_Fin_Div_2;
    private DbsField pnd_Tpa_Pnd_Fund_P_2;
    private DbsField pnd_Tpa_Pnd_Pay_Amt_P_2;
    private DbsField pnd_Tpa_Pnd_Div_Amt_P_2;
    private DbsField pnd_Tpa_Pnd_Fin_Pay_P_2;
    private DbsField pnd_Tpa_Pnd_Fin_Div_P_2;

    private DbsGroup pnd_Annual_Monthly_Totals;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Cmpy_Cde;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Divd;

    private DbsGroup pnd_Annual_Monthly_Totals__R_Field_11;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Pymnt;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Divd;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Divd;

    private DbsGroup pnd_Annual_Monthly_Totals__R_Field_12;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Pymnt;
    private DbsField pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Divd;

    private DbsGroup pnd_Page_Headings;
    private DbsField pnd_Page_Headings_Pnd_Page_Heading1;
    private DbsField pnd_Page_Headings_Pnd_Page_Heading2;
    private DbsField pnd_Page_Headings_Pnd_Page_1_Heading2;
    private DbsField pnd_Page_Headings_Pnd_Page_2_Heading2;
    private DbsField pnd_Page_Headings_Pnd_Page_3_Heading2a;
    private DbsField pnd_Page_Headings_Pnd_Page_3_Heading2b;

    private DbsGroup pnd_Iaan0501_Pda;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Fund_1;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Fund_2;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde;

    private DbsGroup pnd_Fund_Desc_Lne;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Desc;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Fill;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Txt;
    private DbsField pnd_Total_Rinv_Fnd;
    private DbsField pnd_Total_Rinv_Fnd_Due;
    private DbsField pnd_Total_Rinv_Pmt;
    private DbsField pnd_Total_Rinv_Div;
    private DbsField pnd_Total_Rinv_Pmt_Due;
    private DbsField pnd_Total_Rinv_Div_Due;
    private DbsField pnd_T_Tpa_Roll_Fnd;
    private DbsField pnd_T_Tpa_Roll_Pmt;
    private DbsField pnd_T_Tpa_Roll_Div;
    private DbsField pnd_T_Tpa_Roll_Due;
    private DbsField pnd_T_Tpa_Roll_Pmt_Due;
    private DbsField pnd_T_Tpa_Roll_Div_Due;
    private DbsField pnd_T_Ipr_Roll_Fnd;
    private DbsField pnd_T_Ipr_Roll_Pmt;
    private DbsField pnd_T_Ipr_Roll_Div;
    private DbsField pnd_T_Ipr_Roll_Fin;
    private DbsField pnd_T_Ipr_Roll_Due;
    private DbsField pnd_T_Ipr_Roll_Pmt_Due;
    private DbsField pnd_T_Ipr_Roll_Div_Due;
    private DbsField pnd_T_Ipr_Roll_Fin_Due;
    private DbsField pnd_T_P_I_Roll_Fnd;
    private DbsField pnd_T_P_I_Roll_Pmt;
    private DbsField pnd_T_P_I_Roll_Div;
    private DbsField pnd_T_P_I_Roll_Fin;
    private DbsField pnd_T_P_I_Roll_Due;
    private DbsField pnd_T_P_I_Roll_Pmt_Due;
    private DbsField pnd_T_P_I_Roll_Div_Due;
    private DbsField pnd_T_P_I_Roll_Fin_Due;
    private DbsField pnd_T_Ac_Roll_Fnd;
    private DbsField pnd_T_Ac_Roll_Pmt;
    private DbsField pnd_T_Ac_Roll_Div;
    private DbsField pnd_T_Ac_Roll_Due;
    private DbsField pnd_T_Ac_Roll_Pmt_Due;
    private DbsField pnd_T_Ac_Roll_Div_Due;
    private DbsField pnd_T_Ac_Roll_Fnd_V;
    private DbsField pnd_T_Ac_Roll_Pmt_V;
    private DbsField pnd_T_Ac_Roll_Due_V;
    private DbsField pnd_T_Ac_Roll_Pmt_Due_V;
    private DbsField pnd_Bypass;
    private DbsField pnd_New_Payee;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Log_Dte;
    private DbsField pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Payee_Cde;
    private DbsField pnd_Save_Cntrct_Record_Code;
    private DbsField pnd_Save_Cpr_Cmpny_Cde;
    private DbsField pnd_Save_Cpr_Pay_Mode;
    private DbsField pnd_Save_Cpr_Status_Cde;
    private DbsField pnd_Save_Summ_Cmpny_Cde;
    private DbsField pnd_Save_Summ_Fund_Cde;
    private DbsField pnd_Save_Strt_Dte;
    private DbsField pnd_Save_Stp_Dte;
    private DbsField pnd_Save_Issue_Dte;
    private DbsField pnd_Save_Issue_Dte_Dd;
    private DbsField pnd_Save_Dist;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_15;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_16;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Ctr;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_17;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Proc_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Monthly_Amount;
    private DbsField pnd_J;
    private DbsField pnd_I5;
    private DbsField pnd_Fnd_I;
    private DbsField pnd_Fdx;
    private DbsField pnd_Basis;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_18;
    private DbsField pnd_Date_A_Pnd_Date_Yyyymm;
    private DbsField pnd_Date_A_Pnd_Date_Dd1;
    private DbsField pnd_Trans_Dates;

    private DbsGroup pnd_Trans_Dates__R_Field_19;
    private DbsField pnd_Trans_Dates_Pnd_Trans_Date_Fil;
    private DbsField pnd_Trans_Dates_Pnd_Trans_Curr_Date;

    private DbsGroup pnd_Trans_Dates__R_Field_20;
    private DbsField pnd_Trans_Dates_Pnd_Trans_Curr_Date_Yyyymm;
    private DbsField pnd_Trans_Dates_Pnd_Trans_Curr_Date_Dd;

    private DbsGroup pnd_Trans_Dates__R_Field_21;
    private DbsField pnd_Trans_Dates_Pnd_Trans_Curr_Date_N;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Latest_Factor;
    private DbsField pnd_Const_Pnd_Payment_Orign_Factor;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa0510 = new PdaIaaa0510(localVariables);
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);
        pdaIaaa051z = new PdaIaaa051z(localVariables);
        pdaAiaa093 = new PdaAiaa093(localVariables);

        // Local Variables
        pnd_Parm_Fund_3 = localVariables.newFieldInRecord("pnd_Parm_Fund_3", "#PARM-FUND-3", FieldType.STRING, 2);
        pnd_Parm_Desc = localVariables.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);

        pnd_Parm_Desc__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Desc__R_Field_1", "REDEFINE", pnd_Parm_Desc);
        pnd_Parm_Desc_Pnd_Parm_Desc_6 = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_6", "#PARM-DESC-6", FieldType.STRING, 6);
        pnd_Parm_Desc_Pnd_Parm_Desc_Rem = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_Rem", "#PARM-DESC-REM", FieldType.STRING, 
            29);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Parm_Len = localVariables.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_Save_Pnd_Cntrct_Per_Ivc = localVariables.newFieldArrayInRecord("pnd_Save_Pnd_Cntrct_Per_Ivc", "#SAVE-#CNTRCT-PER-IVC", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Input_Record_Work_2 = localVariables.newGroupInRecord("pnd_Input_Record_Work_2", "#INPUT-RECORD-WORK-2");
        pnd_Input_Record_Work_2_Pnd_Call_Type = pnd_Input_Record_Work_2.newFieldInGroup("pnd_Input_Record_Work_2_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 
            1);

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Code = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record_344 = pnd_Input_Record.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_344", "#REST-OF-RECORD-344", 
            FieldType.STRING, 1, new DbsArrayController(1, 344));

        pnd_Input_Record__R_Field_2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record_Pnd_Input_Hdr_Rec_Info = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Input_Hdr_Rec_Info", "#INPUT-HDR-REC-INFO", 
            FieldType.STRING, 16);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Orgn_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record__Filler1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record__Filler2 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record__Filler2", "_FILLER2", FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record__Filler3 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record__Filler3", "_FILLER3", FieldType.STRING, 134);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record__Filler4 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record__Filler4", "_FILLER4", FieldType.STRING, 36);
        pnd_Input_Record_Pnd_Cpr_Status_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Status_Cde", "#CPR-STATUS-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Record_Pnd_Filler_3a = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3a", "#FILLER-3A", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde", "#CNTRCT-RWRTTN-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Cash_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Cash_Cde", "#CNTRCT-CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Term_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Term_Cde", "#CNTRCT-TERM-CDE", 
            FieldType.STRING, 1);

        pnd_Input_Record_Pnd_Cpr_Ivc_Info = pnd_Input_Record__R_Field_4.newGroupInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info", "#CPR-IVC-INFO");
        pnd_Input_Record_Pnd_Cntrct_Company_Cd = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Company_Cd", "#CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind", 
            "#CNTRCT-RCVRY-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt", 
            "#CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt", 
            "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Percent = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Percent", "#CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cpr_Pay_Mode = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Pay_Mode", "#CPR-PAY-MODE", FieldType.NUMERIC, 
            3);
        pnd_Input_Record__Filler5 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record__Filler5", "_FILLER5", FieldType.STRING, 35);
        pnd_Input_Record_Pnd_Pend_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Record__Filler6 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record__Filler6", "_FILLER6", FieldType.STRING, 11);
        pnd_Input_Record_Pnd_Cntrct_Dist = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Dist", "#CNTRCT-DIST", FieldType.STRING, 
            4);

        pnd_Input_Record__R_Field_5 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_5", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record__Filler7 = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record__Filler7", "_FILLER7", FieldType.STRING, 23);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Input_Record__Filler8 = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record__Filler8", "_FILLER8", FieldType.STRING, 16);
        pnd_Input_Record_Pnd_Ddctn_Strt_Dte = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Stp_Dte = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input_Record__R_Field_6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record_Pnd_Summ_Cmpny_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Summ_Fund_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 
            2);

        pnd_Input_Record__R_Field_7 = pnd_Input_Record__R_Field_6.newGroupInGroup("pnd_Input_Record__R_Field_7", "REDEFINE", pnd_Input_Record_Pnd_Summ_Fund_Cde);
        pnd_Input_Record_Pnd_Summ_Fund_Cde_N = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fund_Cde_N", "#SUMM-FUND-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record__Filler9 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler9", "_FILLER9", FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Summ_Per_Pymnt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Summ_Per_Dvdnd = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record__Filler10 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler10", "_FILLER10", FieldType.STRING, 26);
        pnd_Input_Record_Pnd_Summ_Units = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Input_Record_Pnd_Summ_Fin_Pymnt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Summ_Fin_Dvdnd = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_8", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_8.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_8.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Final_Totals_Report = localVariables.newGroupInRecord("pnd_Final_Totals_Report", "#FINAL-TOTALS-REPORT");
        pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv", "#ADMN-TIAA-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv", "#ADMN-CREF-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Admn_Inactive_Payees = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Admn_Inactive_Payees", 
            "#ADMN-INACTIVE-PAYEES", FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Admn_Deds_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Admn_Deds_Actv", "#ADMN-DEDS-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Admn_Deds_Amt = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Admn_Deds_Amt", "#ADMN-DEDS-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Final_Totals_Report_Pnd_Admn_Mstr = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr", "#ADMN-MSTR", FieldType.PACKED_DECIMAL, 
            12, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt", 
            "#ADMN-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd", 
            "#ADMN-MSTR-PER-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));

        pnd_Final_Totals_Report__R_Field_9 = pnd_Final_Totals_Report.newGroupInGroup("pnd_Final_Totals_Report__R_Field_9", "REDEFINE", pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd);
        pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units = pnd_Final_Totals_Report__R_Field_9.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units", 
            "#ADMN-MSTR-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Pymnt", 
            "#ADMN-MSTR-FIN-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Divd = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Divd", 
            "#ADMN-MSTR-FIN-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv", "#PYMNT-TIAA-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv", "#PYMNT-CREF-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Actv = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Actv", "#PYMNT-DEDS-ACTV", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Amt = pnd_Final_Totals_Report.newFieldInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Amt", "#PYMNT-DEDS-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr", "#PYMNT-MSTR", 
            FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt", 
            "#PYMNT-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd", 
            "#PYMNT-MSTR-PER-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));

        pnd_Final_Totals_Report__R_Field_10 = pnd_Final_Totals_Report.newGroupInGroup("pnd_Final_Totals_Report__R_Field_10", "REDEFINE", pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd);
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units = pnd_Final_Totals_Report__R_Field_10.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units", 
            "#PYMNT-MSTR-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Pymnt", 
            "#PYMNT-MSTR-FIN-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Divd = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Divd", 
            "#PYMNT-MSTR-FIN-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt", 
            "#PYMNT-MSTR-ANNL-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units", 
            "#PYMNT-MSTR-ANNL-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt", 
            "#PENDR-MSTR-ANNL-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units", 
            "#PENDR-MSTR-ANNL-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt", 
            "#UPEND-MSTR-ANNL-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units", 
            "#UPEND-MSTR-ANNL-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units", "#UPEND-MSTR-UNITS", 
            FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units", "#PENDR-MSTR-UNITS", 
            FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt", 
            "#UPEND-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt = pnd_Final_Totals_Report.newFieldArrayInGroup("pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt", 
            "#PENDR-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 80));
        pend_Annual_Annlzd_Units = localVariables.newFieldArrayInRecord("pend_Annual_Annlzd_Units", "PEND-ANNUAL-ANNLZD-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 20));
        pend_Annual_Annlzd_Payts = localVariables.newFieldArrayInRecord("pend_Annual_Annlzd_Payts", "PEND-ANNUAL-ANNLZD-PAYTS", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 20));
        pend_Annual_Units = localVariables.newFieldArrayInRecord("pend_Annual_Units", "PEND-ANNUAL-UNITS", FieldType.PACKED_DECIMAL, 13, 3, new DbsArrayController(1, 
            20));
        pend_Annual_Payments = localVariables.newFieldArrayInRecord("pend_Annual_Payments", "PEND-ANNUAL-PAYMENTS", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 20));
        pend_Monthly_Units = localVariables.newFieldArrayInRecord("pend_Monthly_Units", "PEND-MONTHLY-UNITS", FieldType.PACKED_DECIMAL, 13, 3, new DbsArrayController(1, 
            20));
        pend_Monthly_Payments = localVariables.newFieldArrayInRecord("pend_Monthly_Payments", "PEND-MONTHLY-PAYMENTS", FieldType.PACKED_DECIMAL, 13, 2, 
            new DbsArrayController(1, 20));
        pnd_Fctr = localVariables.newFieldInRecord("pnd_Fctr", "#FCTR", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Unit = localVariables.newFieldInRecord("pnd_W_Unit", "#W-UNIT", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Pmt = localVariables.newFieldInRecord("pnd_W_Pmt", "#W-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.NUMERIC, 2);
        total_Ivc_Amt = localVariables.newFieldInRecord("total_Ivc_Amt", "TOTAL-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        total_Ivc_Amt_P = localVariables.newFieldInRecord("total_Ivc_Amt_P", "TOTAL-IVC-AMT-P", FieldType.PACKED_DECIMAL, 13, 2);
        total_Roll_Ac_Ivc_Amt = localVariables.newFieldInRecord("total_Roll_Ac_Ivc_Amt", "TOTAL-ROLL-AC-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        total_Roll_Ac_Ivc_Amt_P = localVariables.newFieldInRecord("total_Roll_Ac_Ivc_Amt_P", "TOTAL-ROLL-AC-IVC-AMT-P", FieldType.PACKED_DECIMAL, 13, 2);
        total_Roll_Tpa_Ivc_Amt = localVariables.newFieldInRecord("total_Roll_Tpa_Ivc_Amt", "TOTAL-ROLL-TPA-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        total_Roll_Tpa_Ivc_Amt_P = localVariables.newFieldInRecord("total_Roll_Tpa_Ivc_Amt_P", "TOTAL-ROLL-TPA-IVC-AMT-P", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Save_Optn_Cde = localVariables.newFieldInRecord("pnd_Save_Optn_Cde", "#SAVE-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Pend_Cde = localVariables.newFieldInRecord("pnd_Save_Pend_Cde", "#SAVE-PEND-CDE", FieldType.STRING, 1);
        pnd_Prod_Counter = localVariables.newFieldInRecord("pnd_Prod_Counter", "#PROD-COUNTER", FieldType.NUMERIC, 2);
        pnd_Prod_Counter_Pa = localVariables.newFieldInRecord("pnd_Prod_Counter_Pa", "#PROD-COUNTER-PA", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cde = localVariables.newFieldInRecord("pnd_Max_Prd_Cde", "#MAX-PRD-CDE", FieldType.NUMERIC, 2);
        c = localVariables.newFieldInRecord("c", "C", FieldType.NUMERIC, 9);

        pnd_Tpa = localVariables.newGroupInRecord("pnd_Tpa", "#TPA");
        pnd_Tpa_Pnd_Fund = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund", "#FUND", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Tpa_Pnd_Pay_Amt = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt", "#PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Div_Amt = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt", "#DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Pay = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay", "#FIN-PAY", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div", "#FIN-DIV", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fund_P = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund_P", "#FUND-P", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Tpa_Pnd_Pay_Amt_P = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt_P", "#PAY-AMT-P", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Div_Amt_P = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt_P", "#DIV-AMT-P", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Pay_P = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay_P", "#FIN-PAY-P", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div_P = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div_P", "#FIN-DIV-P", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fund_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund_1", "#FUND-1", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 8));
        pnd_Tpa_Pnd_Pay_Amt_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt_1", "#PAY-AMT-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Div_Amt_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt_1", "#DIV-AMT-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fin_Pay_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay_1", "#FIN-PAY-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div_1", "#FIN-DIV-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fund_P_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund_P_1", "#FUND-P-1", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Pay_Amt_P_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt_P_1", "#PAY-AMT-P-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Div_Amt_P_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt_P_1", "#DIV-AMT-P-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fin_Pay_P_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay_P_1", "#FIN-PAY-P-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div_P_1 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div_P_1", "#FIN-DIV-P-1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            88));
        pnd_Tpa_Pnd_Fund_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund_2", "#FUND-2", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 8));
        pnd_Tpa_Pnd_Pay_Amt_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt_2", "#PAY-AMT-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Div_Amt_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt_2", "#DIV-AMT-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fin_Pay_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay_2", "#FIN-PAY-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div_2", "#FIN-DIV-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fund_P_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund_P_2", "#FUND-P-2", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Pay_Amt_P_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt_P_2", "#PAY-AMT-P-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Div_Amt_P_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt_P_2", "#DIV-AMT-P-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_Tpa_Pnd_Fin_Pay_P_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay_P_2", "#FIN-PAY-P-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div_P_2 = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div_P_2", "#FIN-DIV-P-2", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));

        pnd_Annual_Monthly_Totals = localVariables.newGroupInRecord("pnd_Annual_Monthly_Totals", "#ANNUAL-MONTHLY-TOTALS");
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Cmpy_Cde = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Cmpy_Cde", 
            "#TOT-ADMN-CMPY-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 2));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr", "#TOT-ADMN-MSTR", 
            FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt", 
            "#TOT-ADMN-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Divd = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Divd", 
            "#TOT-ADMN-MSTR-PER-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));

        pnd_Annual_Monthly_Totals__R_Field_11 = pnd_Annual_Monthly_Totals.newGroupInGroup("pnd_Annual_Monthly_Totals__R_Field_11", "REDEFINE", pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Divd);
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units = pnd_Annual_Monthly_Totals__R_Field_11.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units", 
            "#TOT-ADMN-MSTR-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Pymnt = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Pymnt", 
            "#TOT-ADMN-MSTR-FIN-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Divd = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Fin_Divd", 
            "#TOT-ADMN-MSTR-FIN-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr", 
            "#TOT-PYMNT-MSTR", FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt", 
            "#TOT-PYMNT-MSTR-PER-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Divd = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Divd", 
            "#TOT-PYMNT-MSTR-PER-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));

        pnd_Annual_Monthly_Totals__R_Field_12 = pnd_Annual_Monthly_Totals.newGroupInGroup("pnd_Annual_Monthly_Totals__R_Field_12", "REDEFINE", pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Divd);
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units = pnd_Annual_Monthly_Totals__R_Field_12.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units", 
            "#TOT-PYMNT-MSTR-UNITS", FieldType.PACKED_DECIMAL, 12, 3, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Pymnt = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Pymnt", 
            "#TOT-PYMNT-MSTR-FIN-PYMNT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));
        pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Divd = pnd_Annual_Monthly_Totals.newFieldArrayInGroup("pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Fin_Divd", 
            "#TOT-PYMNT-MSTR-FIN-DIVD", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 4));

        pnd_Page_Headings = localVariables.newGroupInRecord("pnd_Page_Headings", "#PAGE-HEADINGS");
        pnd_Page_Headings_Pnd_Page_Heading1 = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_Heading1", "#PAGE-HEADING1", FieldType.STRING, 
            50);
        pnd_Page_Headings_Pnd_Page_Heading2 = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_Heading2", "#PAGE-HEADING2", FieldType.STRING, 
            65);
        pnd_Page_Headings_Pnd_Page_1_Heading2 = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_1_Heading2", "#PAGE-1-HEADING2", FieldType.STRING, 
            65);
        pnd_Page_Headings_Pnd_Page_2_Heading2 = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_2_Heading2", "#PAGE-2-HEADING2", FieldType.STRING, 
            65);
        pnd_Page_Headings_Pnd_Page_3_Heading2a = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_3_Heading2a", "#PAGE-3-HEADING2A", FieldType.STRING, 
            65);
        pnd_Page_Headings_Pnd_Page_3_Heading2b = pnd_Page_Headings.newFieldInGroup("pnd_Page_Headings_Pnd_Page_3_Heading2b", "#PAGE-3-HEADING2B", FieldType.STRING, 
            65);

        pnd_Iaan0501_Pda = localVariables.newGroupInRecord("pnd_Iaan0501_Pda", "#IAAN0501-PDA");
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_1 = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_2 = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde", "#PARM-RTN-CDE", FieldType.NUMERIC, 
            2);

        pnd_Fund_Desc_Lne = localVariables.newGroupInRecord("pnd_Fund_Desc_Lne", "#FUND-DESC-LNE");
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con", "#FUND-CREF-CON", FieldType.STRING, 
            13);
        pnd_Fund_Desc_Lne_Pnd_Fund_Desc = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 6);
        pnd_Fund_Desc_Lne_Pnd_Fund_Fill = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Fill", "#FUND-FILL", FieldType.STRING, 1);
        pnd_Fund_Desc_Lne_Pnd_Fund_Txt = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Txt", "#FUND-TXT", FieldType.STRING, 15);
        pnd_Total_Rinv_Fnd = localVariables.newFieldInRecord("pnd_Total_Rinv_Fnd", "#TOTAL-RINV-FND", FieldType.NUMERIC, 9);
        pnd_Total_Rinv_Fnd_Due = localVariables.newFieldInRecord("pnd_Total_Rinv_Fnd_Due", "#TOTAL-RINV-FND-DUE", FieldType.NUMERIC, 9);
        pnd_Total_Rinv_Pmt = localVariables.newFieldInRecord("pnd_Total_Rinv_Pmt", "#TOTAL-RINV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Total_Rinv_Div = localVariables.newFieldInRecord("pnd_Total_Rinv_Div", "#TOTAL-RINV-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Total_Rinv_Pmt_Due = localVariables.newFieldInRecord("pnd_Total_Rinv_Pmt_Due", "#TOTAL-RINV-PMT-DUE", FieldType.NUMERIC, 13, 2);
        pnd_Total_Rinv_Div_Due = localVariables.newFieldInRecord("pnd_Total_Rinv_Div_Due", "#TOTAL-RINV-DIV-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Tpa_Roll_Fnd = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Fnd", "#T-TPA-ROLL-FND", FieldType.NUMERIC, 9);
        pnd_T_Tpa_Roll_Pmt = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Pmt", "#T-TPA-ROLL-PMT", FieldType.NUMERIC, 13, 2);
        pnd_T_Tpa_Roll_Div = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Div", "#T-TPA-ROLL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_T_Tpa_Roll_Due = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Due", "#T-TPA-ROLL-DUE", FieldType.NUMERIC, 9);
        pnd_T_Tpa_Roll_Pmt_Due = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Pmt_Due", "#T-TPA-ROLL-PMT-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Tpa_Roll_Div_Due = localVariables.newFieldInRecord("pnd_T_Tpa_Roll_Div_Due", "#T-TPA-ROLL-DIV-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Fnd = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Fnd", "#T-IPR-ROLL-FND", FieldType.NUMERIC, 9);
        pnd_T_Ipr_Roll_Pmt = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Pmt", "#T-IPR-ROLL-PMT", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Div = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Div", "#T-IPR-ROLL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Fin = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Fin", "#T-IPR-ROLL-FIN", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Due = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Due", "#T-IPR-ROLL-DUE", FieldType.NUMERIC, 9);
        pnd_T_Ipr_Roll_Pmt_Due = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Pmt_Due", "#T-IPR-ROLL-PMT-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Div_Due = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Div_Due", "#T-IPR-ROLL-DIV-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Ipr_Roll_Fin_Due = localVariables.newFieldInRecord("pnd_T_Ipr_Roll_Fin_Due", "#T-IPR-ROLL-FIN-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Fnd = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Fnd", "#T-P-I-ROLL-FND", FieldType.NUMERIC, 9);
        pnd_T_P_I_Roll_Pmt = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Pmt", "#T-P-I-ROLL-PMT", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Div = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Div", "#T-P-I-ROLL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Fin = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Fin", "#T-P-I-ROLL-FIN", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Due = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Due", "#T-P-I-ROLL-DUE", FieldType.NUMERIC, 9);
        pnd_T_P_I_Roll_Pmt_Due = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Pmt_Due", "#T-P-I-ROLL-PMT-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Div_Due = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Div_Due", "#T-P-I-ROLL-DIV-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_P_I_Roll_Fin_Due = localVariables.newFieldInRecord("pnd_T_P_I_Roll_Fin_Due", "#T-P-I-ROLL-FIN-DUE", FieldType.NUMERIC, 13, 2);
        pnd_T_Ac_Roll_Fnd = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Fnd", "#T-AC-ROLL-FND", FieldType.NUMERIC, 9);
        pnd_T_Ac_Roll_Pmt = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Pmt", "#T-AC-ROLL-PMT", FieldType.NUMERIC, 12, 2);
        pnd_T_Ac_Roll_Div = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Div", "#T-AC-ROLL-DIV", FieldType.NUMERIC, 12, 2);
        pnd_T_Ac_Roll_Due = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Due", "#T-AC-ROLL-DUE", FieldType.NUMERIC, 9);
        pnd_T_Ac_Roll_Pmt_Due = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Pmt_Due", "#T-AC-ROLL-PMT-DUE", FieldType.NUMERIC, 12, 2);
        pnd_T_Ac_Roll_Div_Due = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Div_Due", "#T-AC-ROLL-DIV-DUE", FieldType.NUMERIC, 12, 2);
        pnd_T_Ac_Roll_Fnd_V = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Fnd_V", "#T-AC-ROLL-FND-V", FieldType.NUMERIC, 9);
        pnd_T_Ac_Roll_Pmt_V = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Pmt_V", "#T-AC-ROLL-PMT-V", FieldType.NUMERIC, 12, 2);
        pnd_T_Ac_Roll_Due_V = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Due_V", "#T-AC-ROLL-DUE-V", FieldType.NUMERIC, 9);
        pnd_T_Ac_Roll_Pmt_Due_V = localVariables.newFieldInRecord("pnd_T_Ac_Roll_Pmt_Due_V", "#T-AC-ROLL-PMT-DUE-V", FieldType.NUMERIC, 12, 2);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_New_Payee = localVariables.newFieldInRecord("pnd_New_Payee", "#NEW-PAYEE", FieldType.BOOLEAN, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Log_Dte = localVariables.newFieldInRecord("pnd_Log_Dte", "#LOG-DTE", FieldType.STRING, 15);
        pnd_Save_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Record_Code = localVariables.newFieldInRecord("pnd_Save_Cntrct_Record_Code", "#SAVE-CNTRCT-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Save_Cpr_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Save_Cpr_Cmpny_Cde", "#SAVE-CPR-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Cpr_Pay_Mode = localVariables.newFieldInRecord("pnd_Save_Cpr_Pay_Mode", "#SAVE-CPR-PAY-MODE", FieldType.NUMERIC, 3);
        pnd_Save_Cpr_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Cpr_Status_Cde", "#SAVE-CPR-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Summ_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Save_Summ_Cmpny_Cde", "#SAVE-SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Summ_Fund_Cde = localVariables.newFieldInRecord("pnd_Save_Summ_Fund_Cde", "#SAVE-SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Save_Strt_Dte = localVariables.newFieldInRecord("pnd_Save_Strt_Dte", "#SAVE-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Stp_Dte = localVariables.newFieldInRecord("pnd_Save_Stp_Dte", "#SAVE-STP-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Issue_Dte = localVariables.newFieldInRecord("pnd_Save_Issue_Dte", "#SAVE-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Save_Issue_Dte_Dd = localVariables.newFieldInRecord("pnd_Save_Issue_Dte_Dd", "#SAVE-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Save_Dist = localVariables.newFieldInRecord("pnd_Save_Dist", "#SAVE-DIST", FieldType.STRING, 4);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_13.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_15 = pnd_Save_Check_Dte_A__R_Field_14.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_15", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_15.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_15.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_16 = pnd_Save_Check_Dte_A__R_Field_13.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_16", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);

        pnd_Payment_Due_Dte__R_Field_17 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_17", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_17.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_17.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_17.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_17.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_17.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Proc_Ctr = localVariables.newFieldInRecord("pnd_Proc_Ctr", "#PROC-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Monthly_Amount = localVariables.newFieldInRecord("pnd_Monthly_Amount", "#MONTHLY-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_I5 = localVariables.newFieldInRecord("pnd_I5", "#I5", FieldType.INTEGER, 2);
        pnd_Fnd_I = localVariables.newFieldInRecord("pnd_Fnd_I", "#FND-I", FieldType.INTEGER, 2);
        pnd_Fdx = localVariables.newFieldInRecord("pnd_Fdx", "#FDX", FieldType.NUMERIC, 2);
        pnd_Basis = localVariables.newFieldInRecord("pnd_Basis", "#BASIS", FieldType.STRING, 40);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_18 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_18", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Yyyymm = pnd_Date_A__R_Field_18.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyymm", "#DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Date_A_Pnd_Date_Dd1 = pnd_Date_A__R_Field_18.newFieldInGroup("pnd_Date_A_Pnd_Date_Dd1", "#DATE-DD1", FieldType.NUMERIC, 2);
        pnd_Trans_Dates = localVariables.newFieldInRecord("pnd_Trans_Dates", "#TRANS-DATES", FieldType.STRING, 16);

        pnd_Trans_Dates__R_Field_19 = localVariables.newGroupInRecord("pnd_Trans_Dates__R_Field_19", "REDEFINE", pnd_Trans_Dates);
        pnd_Trans_Dates_Pnd_Trans_Date_Fil = pnd_Trans_Dates__R_Field_19.newFieldInGroup("pnd_Trans_Dates_Pnd_Trans_Date_Fil", "#TRANS-DATE-FIL", FieldType.STRING, 
            8);
        pnd_Trans_Dates_Pnd_Trans_Curr_Date = pnd_Trans_Dates__R_Field_19.newFieldInGroup("pnd_Trans_Dates_Pnd_Trans_Curr_Date", "#TRANS-CURR-DATE", FieldType.STRING, 
            8);

        pnd_Trans_Dates__R_Field_20 = pnd_Trans_Dates__R_Field_19.newGroupInGroup("pnd_Trans_Dates__R_Field_20", "REDEFINE", pnd_Trans_Dates_Pnd_Trans_Curr_Date);
        pnd_Trans_Dates_Pnd_Trans_Curr_Date_Yyyymm = pnd_Trans_Dates__R_Field_20.newFieldInGroup("pnd_Trans_Dates_Pnd_Trans_Curr_Date_Yyyymm", "#TRANS-CURR-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Trans_Dates_Pnd_Trans_Curr_Date_Dd = pnd_Trans_Dates__R_Field_20.newFieldInGroup("pnd_Trans_Dates_Pnd_Trans_Curr_Date_Dd", "#TRANS-CURR-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Trans_Dates__R_Field_21 = pnd_Trans_Dates__R_Field_19.newGroupInGroup("pnd_Trans_Dates__R_Field_21", "REDEFINE", pnd_Trans_Dates_Pnd_Trans_Curr_Date);
        pnd_Trans_Dates_Pnd_Trans_Curr_Date_N = pnd_Trans_Dates__R_Field_21.newFieldInGroup("pnd_Trans_Dates_Pnd_Trans_Curr_Date_N", "#TRANS-CURR-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Latest_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Latest_Factor", "#LATEST-FACTOR", FieldType.STRING, 1);
        pnd_Const_Pnd_Payment_Orign_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Payment_Orign_Factor", "#PAYMENT-ORIGN-FACTOR", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();

        ldaIaal050.initializeValues();

        localVariables.reset();
        pnd_Prod_Cnt.setInitialValue(0);
        total_Ivc_Amt.setInitialValue(0);
        total_Ivc_Amt_P.setInitialValue(0);
        total_Roll_Ac_Ivc_Amt.setInitialValue(0);
        total_Roll_Ac_Ivc_Amt_P.setInitialValue(0);
        total_Roll_Tpa_Ivc_Amt.setInitialValue(0);
        total_Roll_Tpa_Ivc_Amt_P.setInitialValue(0);
        pnd_Prod_Counter.setInitialValue(0);
        pnd_Prod_Counter_Pa.setInitialValue(0);
        pnd_Max_Prd_Cde.setInitialValue(0);
        c.setInitialValue(0);
        pnd_Page_Headings_Pnd_Page_Heading1.setInitialValue("IA ADMINISTRATION CONTROL SUMMARY FOR PAYMENTS DUE");
        pnd_Page_Headings_Pnd_Page_1_Heading2.setInitialValue("TIAA TOTALS");
        pnd_Page_Headings_Pnd_Page_2_Heading2.setInitialValue("CREF ANNUAL TOTALS");
        pnd_Page_Headings_Pnd_Page_3_Heading2a.setInitialValue("CREF MONTHLY TOTALS (Amount based on latest available factor)");
        pnd_Page_Headings_Pnd_Page_3_Heading2b.setInitialValue("CREF MONTHLY TOTALS (Amount based on original settlement)");
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setInitialValue("CREF ");
        pnd_Basis.setInitialValue("(Based on original settlement)");
        pnd_Const_Pnd_Latest_Factor.setInitialValue("L");
        pnd_Const_Pnd_Payment_Orign_Factor.setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap720() throws Exception
    {
        super("Iaap720");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP720", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ======================================================================
        //*                           PAGE HEADINGS
        //*  ======================================================================
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF /* 11/09 - TURN ON ZERO PRINT                                                                                                                  //Natural: FORMAT LS = 133 PS = 56
        //*  ZP=OFF SG=OFF /* 11/09                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*  ZP=OFF SG=OFF /* 11/09                                                                                                                                       //Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 3 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  ======================================================================
        //*                          START OF PROGRAM
        //*  ======================================================================
        //*  ADDED 2/04
        pnd_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-A
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        //*  8/96
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN0500-READ-EXTERN-FILE
        sub_Call_Iaan0500_Read_Extern_File();
        if (condition(Global.isEscape())) {return;}
        //*  CALL TYPE TO BE SENT TO (AIAN026) TO GET THE ANNUITY UNIT VALUE
        getWorkFiles().read(2, pnd_Input_Record_Work_2);                                                                                                                  //Natural: READ WORK 2 ONCE #INPUT-RECORD-WORK-2
        //*  LATEST OR PAYMENT FACTOR
        if (condition(! (pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor) || pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Payment_Orign_Factor)))) //Natural: IF NOT ( #CALL-TYPE = #LATEST-FACTOR OR = #PAYMENT-ORIGN-FACTOR )
        {
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*           ERROR READING WORK FILE 2");                                                                                               //Natural: WRITE '*           ERROR READING WORK FILE 2'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                                         //Natural: WRITE '*' '=' #CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*  CALL TYPE MUST BE 'L' OR 'P' ");                                                                                                    //Natural: WRITE '*  CALL TYPE MUST BE "L" OR "P" '
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                                         //Natural: IF #CALL-TYPE = #LATEST-FACTOR
        {
            pnd_Basis.setValue("(Based on latest available factor)");                                                                                                     //Natural: ASSIGN #BASIS := '(Based on latest available factor)'
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIN PROCESSING LOOP
        RW1:                                                                                                                                                              //Natural: READ WORK 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            //*  ADDED FOLLOWING 2/04
            if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER")))                                                                                     //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER'
            {
                pnd_Trans_Dates.setValue(pnd_Input_Record_Pnd_Input_Hdr_Rec_Info);                                                                                        //Natural: ASSIGN #TRANS-DATES := #INPUT-HDR-REC-INFO
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 2/04
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   SHEADER")  //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet674 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
            if (condition((pnd_Input_Record_Pnd_Record_Code.equals(10))))
            {
                decideConditionsMet674++;
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                pnd_Save_Issue_Dte.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte);                                                                                       //Natural: ASSIGN #SAVE-ISSUE-DTE := #CNTRCT-ISSUE-DTE
                pnd_Save_Issue_Dte_Dd.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd);                                                                                 //Natural: ASSIGN #SAVE-ISSUE-DTE-DD := #CNTRCT-ISSUE-DTE-DD
                pnd_Save_Optn_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Optn_Cde);                                                                                         //Natural: ASSIGN #SAVE-OPTN-CDE := #CNTRCT-OPTN-CDE
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Record_Pnd_Record_Code.equals(20))))
            {
                decideConditionsMet674++;
                //*  ADDED 10/09
                pnd_New_Payee.reset();                                                                                                                                    //Natural: RESET #NEW-PAYEE
                pnd_Save_Pend_Cde.setValue(pnd_Input_Record_Pnd_Pend_Cde);                                                                                                //Natural: ASSIGN #SAVE-PEND-CDE := #PEND-CDE
                if (condition(pnd_Save_Ppcn_Nbr.notEquals(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr) || pnd_Save_Payee_Cde.notEquals(pnd_Input_Record_Pnd_Cntrct_Payee_Cde)))  //Natural: IF #SAVE-PPCN-NBR NE #CNTRCT-PPCN-NBR OR #SAVE-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
                {
                    pnd_New_Payee.setValue(true);                                                                                                                         //Natural: ASSIGN #NEW-PAYEE := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bypass.getBoolean()))                                                                                                                   //Natural: IF #BYPASS
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Record_Pnd_Cpr_Status_Cde.equals(9)))                                                                                         //Natural: IF #CPR-STATUS-CDE = 9
                    {
                        pnd_Bypass_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #BYPASS-CTR
                        pnd_Final_Totals_Report_Pnd_Admn_Inactive_Payees.nadd(1);                                                                                         //Natural: ADD 1 TO #ADMN-INACTIVE-PAYEES
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Proc_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-CPR
                        sub_Save_Cpr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RW1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
                        sub_Check_Mode();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RW1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ADDED 6/03
                                                                                                                                                                          //Natural: PERFORM ACCUM-ROLL-IVC
                        sub_Accum_Roll_Ivc();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RW1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SAVE-CPR
                    sub_Save_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IAA-TIAA-FUND-RCRD-VIEW
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Input_Record_Pnd_Record_Code.equals(30))))
            {
                decideConditionsMet674++;
                if (condition(pnd_Bypass.getBoolean() || pnd_Save_Cpr_Status_Cde.equals(9)))                                                                              //Natural: IF #BYPASS OR #SAVE-CPR-STATUS-CDE = 9
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SAVE-FUND-SUMM
                    sub_Save_Fund_Summ();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND
                    sub_Process_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((pnd_Input_Record_Pnd_Record_Code.equals(40))))
            {
                decideConditionsMet674++;
                if (condition(pnd_Bypass.getBoolean() || pnd_Save_Cpr_Status_Cde.equals(9)))                                                                              //Natural: IF #BYPASS OR #SAVE-CPR-STATUS-CDE = 9
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Proc_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PROC-CTR
                    if (condition(pnd_Input_Record_Pnd_Ddctn_Stp_Dte.equals(getZero())))                                                                                  //Natural: IF #DDCTN-STP-DTE = 0
                    {
                        pnd_Final_Totals_Report_Pnd_Admn_Deds_Actv.nadd(1);                                                                                               //Natural: ADD 1 TO #ADMN-DEDS-ACTV
                        pnd_Final_Totals_Report_Pnd_Admn_Deds_Amt.nadd(pnd_Input_Record_Pnd_Ddctn_Per_Amt);                                                               //Natural: ADD #DDCTN-PER-AMT TO #ADMN-DEDS-AMT
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
                        sub_Check_Mode();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RW1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                      //Natural: IF #PAYMENT-DUE
                        {
                            pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Actv.nadd(1);                                                                                          //Natural: ADD 1 TO #PYMNT-DEDS-ACTV
                            pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Amt.nadd(pnd_Input_Record_Pnd_Ddctn_Per_Amt);                                                          //Natural: ADD #DDCTN-PER-AMT TO #PYMNT-DEDS-AMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CNTRL-SUMMARY
        sub_Write_Cntrl_Summary();
        if (condition(Global.isEscape())) {return;}
        //*   ADDED FOLLOWING PASS CREF SUMMARY AMTS TO ACTUARY    2/04
        //*  CALL ONLY IF IN IA PMT-CYCLE
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals("P") && pnd_Trans_Dates_Pnd_Trans_Curr_Date_Dd.greater(16) && pnd_Trans_Dates_Pnd_Trans_Curr_Date_Dd.less(22))) //Natural: IF #CALL-TYPE = 'P' AND #TRANS-CURR-DATE-DD GT 16 AND #TRANS-CURR-DATE-DD LT 22
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I5 = 1 TO 20
            for (pnd_I5.setValue(1); condition(pnd_I5.lessOrEqual(20)); pnd_I5.nadd(1))
            {
                //*  7/09
                //*  6/09
                //*  6/09
                if (condition(pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5).equals(" ")))                                                                    //Natural: IF FUND-CODE ( #I5 ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(2, ReportOption.NOTITLE,"Fnd/cde",                                                                                                   //Natural: DISPLAY ( 2 ) NOTITLE 'Fnd/cde' FUND-CODE ( #I5 ) '/Annualized Units' FUND-ANNUAL-ANNLZD-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) '/Annual-Units' FUND-ANNUAL-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) '/Annualized Pmt' FUND-ANNUAL-ANNLZD-PAYTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) '/Annual-pmt' FUND-ANNUAL-PAYMENTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) '/Mnthly-Units' FUND-MONTHLY-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) '/Mnthly-pmt' FUND-MONTHLY-PAYMENTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
                		pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5),"/Annualized Units",
                		pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(pnd_I5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Annual-Units",
                		pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(pnd_I5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Annualized Pmt",
                		pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(pnd_I5), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),"/Annual-pmt",
                		pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(pnd_I5), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),"/Mnthly-Units",
                		pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(pnd_I5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Mnthly-pmt",
                		pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(pnd_I5), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(2, 2);                                                                                                                                      //Natural: SKIP ( 2 ) 2
            getReports().write(2, ReportOption.NOTITLE,"Fnd      Pended            Pended           Pended             Pended            Pended           Pended");       //Natural: WRITE ( 2 ) 'Fnd      Pended            Pended           Pended             Pended            Pended           Pended'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"cde Annualized Units    Annual-Units     Annualized Pmt      Annual-pmt       Mnthly-Units       Mnthly-pmt    "); //Natural: WRITE ( 2 ) 'cde Annualized Units    Annual-Units     Annualized Pmt      Annual-pmt       Mnthly-Units       Mnthly-pmt    '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"--- ----------------- ----------------- ----------------- ----------------- ----------------- -----------------"); //Natural: WRITE ( 2 ) '--- ----------------- ----------------- ----------------- ----------------- ----------------- -----------------'
            if (Global.isEscape()) return;
            FOR02:                                                                                                                                                        //Natural: FOR #I5 1 TO 20
            for (pnd_I5.setValue(1); condition(pnd_I5.lessOrEqual(20)); pnd_I5.nadd(1))
            {
                if (condition(pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5).equals(" ")))                                                                    //Natural: IF FUND-CODE ( #I5 ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5),pend_Annual_Annlzd_Units.getValue(pnd_I5),          //Natural: WRITE ( 2 ) NOTITLE FUND-CODE ( #I5 ) PEND-ANNUAL-ANNLZD-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) PEND-ANNUAL-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) PEND-ANNUAL-ANNLZD-PAYTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) PEND-ANNUAL-PAYMENTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) PEND-MONTHLY-UNITS ( #I5 ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) PEND-MONTHLY-PAYMENTS ( #I5 ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),pend_Annual_Units.getValue(pnd_I5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),pend_Annual_Annlzd_Payts.getValue(pnd_I5), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),pend_Annual_Payments.getValue(pnd_I5), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),pend_Monthly_Units.getValue(pnd_I5), 
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),pend_Monthly_Payments.getValue(pnd_I5), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*                                                /* END - 10/09 ADDED
            getReports().write(0, "** Above information passed to actuary module       ****");                                                                            //Natural: WRITE '** Above information passed to actuary module       ****'
            if (Global.isEscape()) return;
            getReports().write(0, "** Making call to actuary to update collection file ****");                                                                            //Natural: WRITE '** Making call to actuary to update collection file ****'
            if (Global.isEscape()) return;
            DbsUtil.callnat(Aian093.class , getCurrentProcessState(), pdaAiaa093.getAian093_Linkage());                                                                   //Natural: CALLNAT 'AIAN093' AIAN093-LINKAGE
            if (condition(Global.isEscape())) return;
            //*  3/12
            if (condition(pdaAiaa093.getAian093_Linkage_Aian093_Return_Code_Nbr().notEquals(getZero())))                                                                  //Natural: IF AIAN093-RETURN-CODE-NBR NE 0
            {
                getReports().write(0, "**  Actuary Module AIAN093 RETURN CODE ****");                                                                                     //Natural: WRITE '**  Actuary Module AIAN093 RETURN CODE ****'
                if (Global.isEscape()) return;
                getReports().write(0, "** NOT EQUAL TO ZERO AIAN093-RETURN-CODE ==>",pdaAiaa093.getAian093_Linkage_Aian093_Return_Code());                                //Natural: WRITE '** NOT EQUAL TO ZERO AIAN093-RETURN-CODE ==>' AIAN093-RETURN-CODE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "** CURRENT-DATE FOUND ON TRANS FILE-->",pnd_Trans_Dates_Pnd_Trans_Curr_Date_N, new ReportEditMask ("9999-99-99"));                         //Natural: WRITE '** CURRENT-DATE FOUND ON TRANS FILE-->' #TRANS-CURR-DATE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
        //*   END OF ADD  2/04
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",pnd_Ctr);                                                                                        //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",pnd_Proc_Ctr);                                                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #PROC-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Bypass_Ctr);                                                                                 //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #BYPASS-CTR
        if (Global.isEscape()) return;
        //* ****************************************
        //*   SAVE CPR INFO ACCUM IVC ROLL TPA & AC
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-CPR
        //*  ADDED FOLLOWING ROUTINE 6/03
        //* ****************************************
        //*   ACCUM IVC ROLL TPA & AC
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-ROLL-IVC
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-FUND-SUMM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FUND
        //*  ACCUMULATE TOTAL FOR EACH CREF ANNUAL  FUND          /* IK 3/1/2000
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUB-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
        //* **********************************************************************
        //*  RETRIEVE LATEST AVAILABLE FACTOR
        //*  CONVERT THE NUMERIC FUND CODE INTO A ALPHA FUND CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CNTRL-SUMMARY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAGE-1
        //*  ADDED FOLLOWING 6/03
        //*  ADDED FOLLOWING 6/03
        //*   ADDED FOLLOWING  ACCUM REAL-ESTATE     2/04
        //* *FUND-ANNUAL-UNITS (9)      := #PYMNT-MSTR-UNITS(9)
        //* *FUND-ANNUAL-PAYMENTS (9)   := #PYMNT-MSTR-PER-PYMNT(9)
        //* *FUND-MONTHLY-UNITS (9)     := #PYMNT-MSTR-UNITS(29)
        //* *FUND-MONTHLY-PAYMENTS (9)  := #PYMNT-MSTR-PER-PYMNT(29)
        //* *FUND-ANNUAL-ANNLZD-UNITS(9) := #PYMNT-MSTR-ANNL-UNITS(9)
        //* *FUND-ANNUAL-ANNLZD-PAYTS(9) := #PYMNT-MSTR-ANNL-PYMNT(9)
        //*  END OF ADD  2/04
        //*   ADDED FOLLOWING  ACCUM TIAA ACCESS     9/08
        //* *FUND-ANNUAL-UNITS (10)     := #PYMNT-MSTR-UNITS(11)
        //* *FUND-ANNUAL-PAYMENTS (10)  := #PYMNT-MSTR-PER-PYMNT(11)
        //* *FUND-MONTHLY-UNITS (10)    := #PYMNT-MSTR-UNITS(31)
        //* *FUND-MONTHLY-PAYMENTS (10) := #PYMNT-MSTR-PER-PYMNT(31)
        //* *FUND-ANNUAL-ANNLZD-UNITS(10) := #PYMNT-MSTR-ANNL-UNITS(11)
        //* *FUND-ANNUAL-ANNLZD-PAYTS(10) := #PYMNT-MSTR-ANNL-PYMNT(11)
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAGE-2
        //*   ADDED FOLLOWING  ACCUM CREF ANNUAL FUNDS    2/04
        //* *  FUND-ANNUAL-UNITS (#I5)      := #PYMNT-MSTR-UNITS(#I)
        //* *  FUND-ANNUAL-PAYMENTS (#I5)   := #PYMNT-MSTR-PER-PYMNT(#I)
        //*      #PYMNT-MSTR-ANNL-UNITS(#I)
        //*      #PYMNT-MSTR-ANNL-PYMNT(#I)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAGE-3
        //* *FUND-MONTHLY-UNITS (#I5)     := #PYMNT-MSTR-UNITS(#J)
        //* *FUND-MONTHLY-PAYMENTS (#I5)  := #PYMNT-MSTR-PER-PYMNT(#J)
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAGE-4
        //*   ADDED FOLLOWING  ACCUM PA SELECT ANNUAL  FUNDS    2/04
        //* *  FUND-ANNUAL-UNITS (#I5)      := #PYMNT-MSTR-UNITS(#J)
        //* *  FUND-ANNUAL-PAYMENTS (#I5)   := #PYMNT-MSTR-PER-PYMNT(#J)
        //*      #PYMNT-MSTR-ANNL-UNITS(#J)
        //*      #PYMNT-MSTR-ANNL-PYMNT(#J)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAGE-5
        //*  WRITE TOTALS FOR EACH MONTHLY CREF FUND
        //*   ADDED FOLLOWING  ACCUM PA SELECT MONTHLY FUNDS    2/04
        //* *  FUND-MONTHLY-UNITS (#I5)     := #PYMNT-MSTR-UNITS(#J)
        //* *  FUND-MONTHLY-PAYMENTS (#I5)  := #PYMNT-MSTR-PER-PYMNT(#J)
        //*  ADD FOLLOWING ROUTINE 10/15/2001
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-ROLLOVER-CONTRACTS
        //*  ADDED FOLLOWING ROUTINE 2/04
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-ALPHA-FUND-CODE
        //* **********************************************************************
        //*  GET ALPHA 1 BYTE FUND CODE USING INDEX NUMBER
        //*  END OF ADD ROUTINE 2/04
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN0500-READ-EXTERN-FILE
        //* **************** ON ERROR ******************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Save_Cpr() throws Exception                                                                                                                          //Natural: SAVE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Ppcn_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #SAVE-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Save_Payee_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                                               //Natural: ASSIGN #SAVE-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Save_Cpr_Pay_Mode.setValue(pnd_Input_Record_Pnd_Cpr_Pay_Mode);                                                                                                //Natural: ASSIGN #SAVE-CPR-PAY-MODE := #CPR-PAY-MODE
        pnd_Save_Cpr_Status_Cde.setValue(pnd_Input_Record_Pnd_Cpr_Status_Cde);                                                                                            //Natural: ASSIGN #SAVE-CPR-STATUS-CDE := #CPR-STATUS-CDE
        pnd_Save_Dist.setValue(pnd_Input_Record_Pnd_Cntrct_Dist);                                                                                                         //Natural: ASSIGN #SAVE-DIST := #CNTRCT-DIST
    }
    private void sub_Accum_Roll_Ivc() throws Exception                                                                                                                    //Natural: ACCUM-ROLL-IVC
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Input_Record_Pnd_Cpr_Status_Cde.notEquals(9)))                                                                                                  //Natural: IF #CPR-STATUS-CDE NE 9
        {
            if (condition(pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))                                                                                  //Natural: IF #SAVE-OPTN-CDE = 28 OR = 30
            {
                if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Dist,"...'T'") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Dist,"...'C'")))          //Natural: IF #CNTRCT-DIST = MASK ( ...'T' ) OR = MASK ( ...'C' )
                {
                    total_Roll_Tpa_Ivc_Amt.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue("*"));                                                                   //Natural: ADD #CNTRCT-PER-IVC-AMT ( * ) TO TOTAL-ROLL-TPA-IVC-AMT
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        total_Roll_Tpa_Ivc_Amt_P.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue("*"));                                                             //Natural: ADD #CNTRCT-PER-IVC-AMT ( * ) TO TOTAL-ROLL-TPA-IVC-AMT-P
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cpr_Status_Cde.notEquals(9)))                                                                                                  //Natural: IF #CPR-STATUS-CDE NE 9
        {
            if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                                  //Natural: IF #SAVE-OPTN-CDE = 21
            {
                if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Dist,"...'T'") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Dist,"...'C'")))          //Natural: IF #CNTRCT-DIST = MASK ( ...'T' ) OR = MASK ( ...'C' )
                {
                    total_Roll_Ac_Ivc_Amt.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue("*"));                                                                    //Natural: ADD #CNTRCT-PER-IVC-AMT ( * ) TO TOTAL-ROLL-AC-IVC-AMT
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        total_Roll_Ac_Ivc_Amt_P.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue("*"));                                                              //Natural: ADD #CNTRCT-PER-IVC-AMT ( * ) TO TOTAL-ROLL-AC-IVC-AMT-P
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 6/03
        if (condition(pnd_Save_Optn_Cde.greaterOrEqual(28) && pnd_Save_Optn_Cde.lessOrEqual(30)))                                                                         //Natural: IF #SAVE-OPTN-CDE = 28 THRU 30
        {
            FOR03:                                                                                                                                                        //Natural: FOR #I2 1 TO 5
            for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(5)); pnd_I2.nadd(1))
            {
                total_Ivc_Amt.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue(pnd_I2));                                                                             //Natural: ADD #CNTRCT-PER-IVC-AMT ( #I2 ) TO TOTAL-IVC-AMT
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    total_Ivc_Amt_P.nadd(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue(pnd_I2));                                                                       //Natural: ADD #CNTRCT-PER-IVC-AMT ( #I2 ) TO TOTAL-IVC-AMT-P
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Save_Fund_Summ() throws Exception                                                                                                                    //Natural: SAVE-FUND-SUMM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Summ_Cmpny_Cde.setValue(pnd_Input_Record_Pnd_Summ_Cmpny_Cde);                                                                                            //Natural: ASSIGN #SAVE-SUMM-CMPNY-CDE := #SUMM-CMPNY-CDE
        if (condition(pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("1") || pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("G") || pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("1S"))) //Natural: IF #SUMM-FUND-CDE = '1' OR = 'G' OR = '1S'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Input_Record_Pnd_Summ_Fund_Cde.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde, MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT #SUMM-FUND-CDE TO #SUMM-FUND-CDE
            DbsUtil.examine(new ExamineSource(pnd_Input_Record_Pnd_Summ_Fund_Cde), new ExamineSearch(" "), new ExamineReplace("0"));                                      //Natural: EXAMINE #SUMM-FUND-CDE FOR ' ' REPLACE '0'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Summ_Fund_Cde.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde);                                                                                              //Natural: ASSIGN #SAVE-SUMM-FUND-CDE := #SUMM-FUND-CDE
    }
    private void sub_Process_Fund() throws Exception                                                                                                                      //Natural: PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Proc_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
        sub_Check_Mode();
        if (condition(Global.isEscape())) {return;}
        //*  FOR MONTHLY FUNDS WE NEED TO RETRIEVE THE UNIT VALUE       /* LB 10/97
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))                                                //Natural: IF #SUMM-CMPNY-CDE = '4' OR = 'W'
        {
            //*  CALLS "AIAN026"
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
            sub_Retrieve_Monthly_Annuity_Unit_Value();
            if (condition(Global.isEscape())) {return;}
            //*  CALCULATE THE FUNDS VALUE
            pnd_Monthly_Amount.compute(new ComputeParameters(true, pnd_Monthly_Amount), ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().multiply(pnd_Input_Record_Pnd_Summ_Units)); //Natural: COMPUTE ROUNDED #MONTHLY-AMOUNT = AUV-RETURNED * #SUMM-UNITS
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED 10/01
        //*  ADDED 10/01
        if (condition(pnd_Save_Optn_Cde.equals(21) || pnd_Save_Optn_Cde.equals(22) || pnd_Save_Optn_Cde.equals(25) || pnd_Save_Optn_Cde.equals(27) ||                     //Natural: IF #SAVE-OPTN-CDE = 21 OR = 22 OR = 25 OR = 27 OR = 28 OR = 30
            pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))
        {
            //*  ADDED 10/01
                                                                                                                                                                          //Natural: PERFORM ACCUM-ROLLOVER-CONTRACTS
            sub_Accum_Rollover_Contracts();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED 10/01
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD TIAA TOTALS
        if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1 '") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1G'")                    //Natural: IF #SUMM-FUND-CDE = MASK ( '1 ' ) OR = MASK ( '1G' ) OR = MASK ( '1S' )
            || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1S'")))
        {
            //* TOTAL TIAA FUND RECS
            pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #ADMN-MSTR ( 1 )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                        //Natural: ADD #SUMM-PER-PYMNT TO #ADMN-MSTR-PER-PYMNT ( 1 )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                         //Natural: ADD #SUMM-PER-DVDND TO #ADMN-MSTR-PER-DIVD ( 1 )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                        //Natural: ADD #SUMM-FIN-PYMNT TO #ADMN-MSTR-FIN-PYMNT ( 1 )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Divd.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                         //Natural: ADD #SUMM-FIN-DVDND TO #ADMN-MSTR-FIN-DIVD ( 1 )
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv.nadd(1);                                                                                                       //Natural: ADD 1 TO #ADMN-TIAA-ACTV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                //*  TOTAL TIAA PYMNT FUND RECS
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(1).nadd(1);                                                                                               //Natural: ADD 1 TO #PYMNT-MSTR ( 1 )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                   //Natural: ADD #SUMM-PER-PYMNT TO #PYMNT-MSTR-PER-PYMNT ( 1 )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                    //Natural: ADD #SUMM-PER-DVDND TO #PYMNT-MSTR-PER-DIVD ( 1 )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                   //Natural: ADD #SUMM-FIN-PYMNT TO #PYMNT-MSTR-FIN-PYMNT ( 1 )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Divd.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                    //Natural: ADD #SUMM-FIN-DVDND TO #PYMNT-MSTR-FIN-DIVD ( 1 )
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv.nadd(1);                                                                                                  //Natural: ADD 1 TO #PYMNT-TIAA-ACTV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUB-TOTALS
            sub_Sub_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_New_Payee.reset();                                                                                                                                        //Natural: RESET #NEW-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACCUMULATE TOTAL CREF PRODUCTS
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.greater(40)))                                               //Natural: IF #SUMM-CMPNY-CDE = 'U' AND #SUMM-FUND-CDE-N > 40
        {
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(3).nadd(1);                                                                                              //Natural: ADD 1 TO #TOT-ADMN-MSTR ( 3 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO #TOT-ADMN-MSTR-UNITS ( 3 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                  //Natural: ADD #SUMM-PER-PYMNT TO #TOT-ADMN-MSTR-PER-PYMNT ( 3 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(3).nadd(1);                                                                                         //Natural: ADD 1 TO #TOT-PYMNT-MSTR ( 3 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                     //Natural: ADD #SUMM-UNITS TO #TOT-PYMNT-MSTR-UNITS ( 3 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                             //Natural: ADD #SUMM-PER-PYMNT TO #TOT-PYMNT-MSTR-PER-PYMNT ( 3 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde_N);                                                                                                         //Natural: ASSIGN #I := #SUMM-FUND-CDE-N
            pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #ADMN-MSTR ( #I )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                            //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #I )
            //*  KN 03/97
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                    //Natural: ADD #SUMM-PER-PYMNT TO #ADMN-MSTR-PER-PYMNT ( #I )
            //*  ANNUALIZE PMT 6/09
            pnd_W_Pmt.compute(new ComputeParameters(true, pnd_W_Pmt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.multiply(pnd_Fctr));                                            //Natural: COMPUTE ROUNDED #W-PMT = #SUMM-PER-PYMNT * #FCTR
            //*  ANNUALIZE UNITS 6/09
            pnd_W_Unit.compute(new ComputeParameters(true, pnd_W_Unit), pnd_Input_Record_Pnd_Summ_Units.multiply(pnd_Fctr));                                              //Natural: COMPUTE ROUNDED #W-UNIT = #SUMM-UNITS * #FCTR
            //*  ACCUMULATE ANNL PMT
            pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                            //Natural: ADD #W-PMT TO #PYMNT-MSTR-ANNL-PYMNT ( #I )
            //*  ACCUMULATE ANNL UNITS
            pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                           //Natural: ADD #W-UNIT TO #PYMNT-MSTR-ANNL-UNITS ( #I )
            //*  ADDED 10/09
            if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                                 //Natural: IF #SAVE-PEND-CDE = 'R'
            {
                //*  10/09 ACCUM PEND R
                pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                        //Natural: ADD #W-PMT TO #PENDR-MSTR-ANNL-PYMNT ( #I )
                //*  10/09
                pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                       //Natural: ADD #W-UNIT TO #PENDR-MSTR-ANNL-UNITS ( #I )
                //*  10/09
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  10/09 ACCUM NO PEND R
                pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                        //Natural: ADD #W-PMT TO #UPEND-MSTR-ANNL-PYMNT ( #I )
                //*  10/09
                pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                       //Natural: ADD #W-UNIT TO #UPEND-MSTR-ANNL-UNITS ( #I )
                //*  10/09
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv.nadd(1);                                                                                                       //Natural: ADD 1 TO #ADMN-TIAA-ACTV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #PYMNT-MSTR ( #I )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                       //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #I )
                //*  KN 03/97
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                               //Natural: ADD #SUMM-PER-PYMNT TO #PYMNT-MSTR-PER-PYMNT ( #I )
                //*  BEGIN - ADDED 10/09
                if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                             //Natural: IF #SAVE-PEND-CDE = 'R'
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                           //Natural: ADD #SUMM-PER-PYMNT TO #PENDR-MSTR-PER-PYMNT ( #I )
                    //*  10/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                           //Natural: ADD #SUMM-PER-PYMNT TO #UPEND-MSTR-PER-PYMNT ( #I )
                    //*  END - ADDED 10/09
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv.nadd(1);                                                                                                  //Natural: ADD 1 TO #PYMNT-TIAA-ACTV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Payee.reset();                                                                                                                                        //Natural: RESET #NEW-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.greater(40)))                                               //Natural: IF #SUMM-CMPNY-CDE = 'W' AND #SUMM-FUND-CDE-N > 40
        {
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(4).nadd(1);                                                                                              //Natural: ADD 1 TO #TOT-ADMN-MSTR ( 4 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(4).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO #TOT-ADMN-MSTR-UNITS ( 4 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(4).nadd(pnd_Monthly_Amount);                                                                   //Natural: ADD #MONTHLY-AMOUNT TO #TOT-ADMN-MSTR-PER-PYMNT ( 4 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(4).nadd(1);                                                                                         //Natural: ADD 1 TO #TOT-PYMNT-MSTR ( 4 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(4).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                     //Natural: ADD #SUMM-UNITS TO #TOT-PYMNT-MSTR-UNITS ( 4 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(4).nadd(pnd_Monthly_Amount);                                                              //Natural: ADD #MONTHLY-AMOUNT TO #TOT-PYMNT-MSTR-PER-PYMNT ( 4 )
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv.nadd(1);                                                                                                  //Natural: ADD 1 TO #PYMNT-TIAA-ACTV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Input_Record_Pnd_Summ_Fund_Cde_N.add(20));                                                             //Natural: ASSIGN #I := #SUMM-FUND-CDE-N +20
            pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #ADMN-MSTR ( #I )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                            //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #I )
            //*  KN 03/97
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Monthly_Amount);                                                                     //Natural: ADD #MONTHLY-AMOUNT TO #ADMN-MSTR-PER-PYMNT ( #I )
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv.nadd(1);                                                                                                       //Natural: ADD 1 TO #ADMN-TIAA-ACTV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #PYMNT-MSTR ( #I )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                       //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #I )
                //*  KN 03/97
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Monthly_Amount);                                                                //Natural: ADD #MONTHLY-AMOUNT TO #PYMNT-MSTR-PER-PYMNT ( #I )
                //*  BEGIN - ADDED 10/09
                if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                             //Natural: IF #SAVE-PEND-CDE = 'R'
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Monthly_Amount);                                                            //Natural: ADD #MONTHLY-AMOUNT TO #PENDR-MSTR-PER-PYMNT ( #I )
                    //*  10/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Monthly_Amount);                                                            //Natural: ADD #MONTHLY-AMOUNT TO #UPEND-MSTR-PER-PYMNT ( #I )
                    //*  END - ADDED 10/09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Payee.reset();                                                                                                                                        //Natural: RESET #NEW-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.less(41)))                                                  //Natural: IF #SUMM-CMPNY-CDE = '2' AND #SUMM-FUND-CDE-N < 41
        {
            //*  TOTAL CREF FUND RECS
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(1).nadd(1);                                                                                              //Natural: ADD 1 TO #TOT-ADMN-MSTR ( 1 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO #TOT-ADMN-MSTR-UNITS ( 1 )
            //*  KN 03/97
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                  //Natural: ADD #SUMM-PER-PYMNT TO #TOT-ADMN-MSTR-PER-PYMNT ( 1 )
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv.nadd(1);                                                                                                       //Natural: ADD 1 TO #ADMN-CREF-ACTV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(1).nadd(1);                                                                                         //Natural: ADD 1 TO #TOT-PYMNT-MSTR ( 1 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                     //Natural: ADD #SUMM-UNITS TO #TOT-PYMNT-MSTR-UNITS ( 1 )
                //*  KN 03/97
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                             //Natural: ADD #SUMM-PER-PYMNT TO #TOT-PYMNT-MSTR-PER-PYMNT ( 1 )
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv.nadd(1);                                                                                                  //Natural: ADD 1 TO #PYMNT-CREF-ACTV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Payee.reset();                                                                                                                                        //Natural: RESET #NEW-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACCUMULATE TOTAL FOR CREF MONTHLY FUNDS                    /* LB 10/97
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.less(41)))                                                  //Natural: IF #SUMM-CMPNY-CDE = '4' AND #SUMM-FUND-CDE-N < 41
        {
            //*  TOTAL CREF MONTHLY FUND RECS
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(2).nadd(1);                                                                                              //Natural: ADD 1 TO #TOT-ADMN-MSTR ( 2 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO #TOT-ADMN-MSTR-UNITS ( 2 )
            pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(2).nadd(pnd_Monthly_Amount);                                                                   //Natural: ADD #MONTHLY-AMOUNT TO #TOT-ADMN-MSTR-PER-PYMNT ( 2 )
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv.nadd(1);                                                                                                       //Natural: ADD 1 TO #ADMN-CREF-ACTV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(2).nadd(1);                                                                                         //Natural: ADD 1 TO #TOT-PYMNT-MSTR ( 2 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                     //Natural: ADD #SUMM-UNITS TO #TOT-PYMNT-MSTR-UNITS ( 2 )
                pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(2).nadd(pnd_Monthly_Amount);                                                              //Natural: ADD #MONTHLY-AMOUNT TO #TOT-PYMNT-MSTR-PER-PYMNT ( 2 )
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv.nadd(1);                                                                                                  //Natural: ADD 1 TO #PYMNT-CREF-ACTV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Payee.reset();                                                                                                                                        //Natural: RESET #NEW-PAYEE
            //*   #SUMM-CMPNY-CDE  =  4                             /* LB 10/97
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACCUMULATE TOTAL REAL ESTATE ANNUAL PAYMENTS               /* 8/96
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U")))                                                                                                   //Natural: IF #SUMM-CMPNY-CDE = 'U'
        {
            if (condition(pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("09") || pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("11")))                                            //Natural: IF #SUMM-FUND-CDE = '09' OR = '11'
            {
                pnd_Fdx.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde_N);                                                                                                   //Natural: ASSIGN #FDX := #SUMM-FUND-CDE-N
                //*  TOTAL CREF FUND RECS
                pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_Fdx).nadd(1);                                                                                          //Natural: ADD 1 TO #ADMN-MSTR ( #FDX )
                pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                      //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #FDX )
                //*  KN 03/97
                pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                              //Natural: ADD #SUMM-PER-PYMNT TO #ADMN-MSTR-PER-PYMNT ( #FDX )
                //*  ANNUALIZE PMT 6/09
                pnd_W_Pmt.compute(new ComputeParameters(true, pnd_W_Pmt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.multiply(pnd_Fctr));                                        //Natural: COMPUTE ROUNDED #W-PMT = #SUMM-PER-PYMNT * #FCTR
                //*  ANNUALIZE UNITS 6/09
                pnd_W_Unit.compute(new ComputeParameters(true, pnd_W_Unit), pnd_Input_Record_Pnd_Summ_Units.multiply(pnd_Fctr));                                          //Natural: COMPUTE ROUNDED #W-UNIT = #SUMM-UNITS * #FCTR
                //*  ACCUMULATE ANNL PMT
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt.getValue(pnd_Fdx).nadd(pnd_W_Pmt);                                                                      //Natural: ADD #W-PMT TO #PYMNT-MSTR-ANNL-PYMNT ( #FDX )
                //*  ACCUMULATE ANNL UNITS
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units.getValue(pnd_Fdx).nadd(pnd_W_Unit);                                                                     //Natural: ADD #W-UNIT TO #PYMNT-MSTR-ANNL-UNITS ( #FDX )
                //*  ADDED 10/09
                if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                             //Natural: IF #SAVE-PEND-CDE = 'R'
                {
                    //*  10/09 ACCUM PEND R
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(pnd_Fdx).nadd(pnd_W_Pmt);                                                                  //Natural: ADD #W-PMT TO #PENDR-MSTR-ANNL-PYMNT ( #FDX )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(pnd_Fdx).nadd(pnd_W_Unit);                                                                 //Natural: ADD #W-UNIT TO #PENDR-MSTR-ANNL-UNITS ( #FDX )
                    //*  10/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  10/09 ACCUM NO PEND
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(pnd_Fdx).nadd(pnd_W_Pmt);                                                                  //Natural: ADD #W-PMT TO #UPEND-MSTR-ANNL-PYMNT ( #FDX )
                    //*  10/09 R
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(pnd_Fdx).nadd(pnd_W_Unit);                                                                 //Natural: ADD #W-UNIT TO #UPEND-MSTR-ANNL-UNITS ( #FDX )
                    //*  10/09
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv.nadd(1);                                                                                                   //Natural: ADD 1 TO #ADMN-TIAA-ACTV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_Fdx).nadd(1);                                                                                     //Natural: ADD 1 TO #PYMNT-MSTR ( #FDX )
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                 //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #FDX )
                    //*  KN 03/97
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                         //Natural: ADD #SUMM-PER-PYMNT TO #PYMNT-MSTR-PER-PYMNT ( #FDX )
                    //*  BEGIN - ADDED 10/09
                    if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                         //Natural: IF #SAVE-PEND-CDE = 'R'
                    {
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                             //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #FDX )
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                     //Natural: ADD #SUMM-PER-PYMNT TO #PENDR-MSTR-PER-PYMNT ( #FDX )
                        //*  10/09
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                             //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #FDX )
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                     //Natural: ADD #SUMM-PER-PYMNT TO #UPEND-MSTR-PER-PYMNT ( #FDX )
                        //*  END - ADDED 10/09
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Payee.getBoolean()))                                                                                                            //Natural: IF #NEW-PAYEE
                    {
                        pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv.nadd(1);                                                                                              //Natural: ADD 1 TO #PYMNT-TIAA-ACTV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_New_Payee.reset();                                                                                                                                    //Natural: RESET #NEW-PAYEE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD   8/96
        //*  ACCUMULATE TOTAL REAL ESTATE MONTHLY FUNDS                 /* LB 10/97
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))                                                                                                   //Natural: IF #SUMM-CMPNY-CDE = 'W'
        {
            if (condition(pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("09") || pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("11")))                                            //Natural: IF #SUMM-FUND-CDE = '09' OR = '11'
            {
                pnd_Fdx.compute(new ComputeParameters(false, pnd_Fdx), pnd_Input_Record_Pnd_Summ_Fund_Cde_N.add(20));                                                     //Natural: ASSIGN #FDX := #SUMM-FUND-CDE-N + 20
                //*  TOTAL TIAA REAL ESTATE MONTHLY RECS
                pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_Fdx).nadd(1);                                                                                          //Natural: ADD 1 TO #ADMN-MSTR ( #FDX )
                pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                      //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #FDX )
                pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Monthly_Amount);                                                               //Natural: ADD #MONTHLY-AMOUNT TO #ADMN-MSTR-PER-PYMNT ( #FDX )
                if (condition(pnd_New_Payee.getBoolean()))                                                                                                                //Natural: IF #NEW-PAYEE
                {
                    pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv.nadd(1);                                                                                                   //Natural: ADD 1 TO #ADMN-TIAA-ACTV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_Fdx).nadd(1);                                                                                     //Natural: ADD 1 TO #PYMNT-MSTR ( #FDX )
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                 //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #FDX )
                    pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Monthly_Amount);                                                          //Natural: ADD #MONTHLY-AMOUNT TO #PYMNT-MSTR-PER-PYMNT ( #FDX )
                    //*  BEGIN - ADDED 10/09
                    if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                         //Natural: IF #SAVE-PEND-CDE = 'R'
                    {
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                             //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #FDX )
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Monthly_Amount);                                                      //Natural: ADD #MONTHLY-AMOUNT TO #PENDR-MSTR-PER-PYMNT ( #FDX )
                        //*  10/09
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_Fdx).nadd(pnd_Input_Record_Pnd_Summ_Units);                                             //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #FDX )
                        //*  10/09
                        pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_Fdx).nadd(pnd_Monthly_Amount);                                                      //Natural: ADD #MONTHLY-AMOUNT TO #UPEND-MSTR-PER-PYMNT ( #FDX )
                        //*  END - ADDED 10/09
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Payee.getBoolean()))                                                                                                            //Natural: IF #NEW-PAYEE
                    {
                        pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv.nadd(1);                                                                                              //Natural: ADD 1 TO #PYMNT-TIAA-ACTV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_New_Payee.reset();                                                                                                                                    //Natural: RESET #NEW-PAYEE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING      8/96
        //*  REPLACED DECIDE ON CODE CHECKING FOR #SUMM-FUND-CDE VALUES
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.less(41)))                                                  //Natural: IF #SUMM-CMPNY-CDE = '2' AND #SUMM-FUND-CDE-N < 41
        {
            pnd_I.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde_N);                                                                                                         //Natural: ASSIGN #I := #SUMM-FUND-CDE-N
            pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #ADMN-MSTR ( #I )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                            //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #I )
            //*  KN 03/97
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                    //Natural: ADD #SUMM-PER-PYMNT TO #ADMN-MSTR-PER-PYMNT ( #I )
            //*  ANNUALIZE PMT 6/09
            pnd_W_Pmt.compute(new ComputeParameters(true, pnd_W_Pmt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.multiply(pnd_Fctr));                                            //Natural: COMPUTE ROUNDED #W-PMT = #SUMM-PER-PYMNT * #FCTR
            //*  ANNUALIZE UNITS 6/09
            pnd_W_Unit.compute(new ComputeParameters(true, pnd_W_Unit), pnd_Input_Record_Pnd_Summ_Units.multiply(pnd_Fctr));                                              //Natural: COMPUTE ROUNDED #W-UNIT = #SUMM-UNITS * #FCTR
            //*  ACCUMULATE ANNL PMT
            pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                            //Natural: ADD #W-PMT TO #PYMNT-MSTR-ANNL-PYMNT ( #I )
            //*  ACCUMULATE ANNL UNITS
            pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                           //Natural: ADD #W-UNIT TO #PYMNT-MSTR-ANNL-UNITS ( #I )
            //*  ADDED 10/09
            if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                                 //Natural: IF #SAVE-PEND-CDE = 'R'
            {
                //*  10/09 ACCUM PEND R
                pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                        //Natural: ADD #W-PMT TO #PENDR-MSTR-ANNL-PYMNT ( #I )
                //*  10/09
                pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                       //Natural: ADD #W-UNIT TO #PENDR-MSTR-ANNL-UNITS ( #I )
                //*  10/09
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  10/09 ACCUM NO PEND R
                pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(pnd_I).nadd(pnd_W_Pmt);                                                                        //Natural: ADD #W-PMT TO #UPEND-MSTR-ANNL-PYMNT ( #I )
                //*  10/09
                pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(pnd_I).nadd(pnd_W_Unit);                                                                       //Natural: ADD #W-UNIT TO #UPEND-MSTR-ANNL-UNITS ( #I )
                //*  10/09
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #PYMNT-MSTR ( #I )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                       //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #I )
                //*  KN 03/97
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                               //Natural: ADD #SUMM-PER-PYMNT TO #PYMNT-MSTR-PER-PYMNT ( #I )
                //*  BEGIN - ADDED 10/09
                if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                             //Natural: IF #SAVE-PEND-CDE = 'R'
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                           //Natural: ADD #SUMM-PER-PYMNT TO #PENDR-MSTR-PER-PYMNT ( #I )
                    //*  10/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #I )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_I).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                           //Natural: ADD #SUMM-PER-PYMNT TO #UPEND-MSTR-PER-PYMNT ( #I )
                    //*  END - ADDED 10/09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD           8/96
        //*  ACCUMULATE TOTAL FOR EACH CREF MONTHLY FUND          /* LB 10/97 START
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4") && pnd_Input_Record_Pnd_Summ_Fund_Cde_N.less(41)))                                                  //Natural: IF #SUMM-CMPNY-CDE = '4' AND #SUMM-FUND-CDE-N < 41
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_Input_Record_Pnd_Summ_Fund_Cde_N.add(20));                                                             //Natural: ASSIGN #J := #SUMM-FUND-CDE-N + 20
            pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_J).nadd(1);                                                                                                //Natural: ADD 1 TO #ADMN-MSTR ( #J )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_J).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                            //Natural: ADD #SUMM-UNITS TO #ADMN-MSTR-UNITS ( #J )
            pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_J).nadd(pnd_Monthly_Amount);                                                                     //Natural: ADD #MONTHLY-AMOUNT TO #ADMN-MSTR-PER-PYMNT ( #J )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_J).nadd(1);                                                                                           //Natural: ADD 1 TO #PYMNT-MSTR ( #J )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_J).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                       //Natural: ADD #SUMM-UNITS TO #PYMNT-MSTR-UNITS ( #J )
                pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_J).nadd(pnd_Monthly_Amount);                                                                //Natural: ADD #MONTHLY-AMOUNT TO #PYMNT-MSTR-PER-PYMNT ( #J )
                //*  BEGIN - ADDED 10/09
                if (condition(pnd_Save_Pend_Cde.equals("R")))                                                                                                             //Natural: IF #SAVE-PEND-CDE = 'R'
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_J).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #PENDR-MSTR-UNITS ( #J )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_J).nadd(pnd_Monthly_Amount);                                                            //Natural: ADD #MONTHLY-AMOUNT TO #PENDR-MSTR-PER-PYMNT ( #J )
                    //*  10/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_J).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                   //Natural: ADD #SUMM-UNITS TO #UPEND-MSTR-UNITS ( #J )
                    //*  10/09
                    pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_J).nadd(pnd_Monthly_Amount);                                                            //Natural: ADD #MONTHLY-AMOUNT TO #UPEND-MSTR-PER-PYMNT ( #J )
                    //*  END - ADDED 10/09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  #SUMM-CMPNY-CDE  = '4'                        /* LB 10/97 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-FUND
    }
    private void sub_Sub_Totals() throws Exception                                                                                                                        //Natural: SUB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1381 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #SAVE-OPTN-CDE;//Natural: VALUE 28:30
        if (condition(((pnd_Save_Optn_Cde.greaterOrEqual(28) && pnd_Save_Optn_Cde.lessOrEqual(30)))))
        {
            decideConditionsMet1381++;
            pnd_I1.setValue(1);                                                                                                                                           //Natural: ASSIGN #I1 := 1
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_Save_Optn_Cde.equals(25) || pnd_Save_Optn_Cde.equals(27))))
        {
            decideConditionsMet1381++;
            pnd_I1.setValue(2);                                                                                                                                           //Natural: ASSIGN #I1 := 2
        }                                                                                                                                                                 //Natural: VALUE 22
        else if (condition((pnd_Save_Optn_Cde.equals(22))))
        {
            decideConditionsMet1381++;
            pnd_I1.setValue(3);                                                                                                                                           //Natural: ASSIGN #I1 := 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_I1.setValue(4);                                                                                                                                           //Natural: ASSIGN #I1 := 4
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Tpa_Pnd_Fund.getValue(pnd_I1).nadd(1);                                                                                                                        //Natural: ADD 1 TO #FUND ( #I1 )
        pnd_Tpa_Pnd_Pay_Amt.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                   //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT ( #I1 )
        pnd_Tpa_Pnd_Div_Amt.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                   //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT ( #I1 )
        pnd_Tpa_Pnd_Fin_Pay.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                                   //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY ( #I1 )
        pnd_Tpa_Pnd_Fin_Div.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                                   //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( #I1 )
        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                      //Natural: IF #PAYMENT-DUE
        {
            pnd_Tpa_Pnd_Fund_P.getValue(pnd_I1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #FUND-P ( #I1 )
            pnd_Tpa_Pnd_Pay_Amt_P.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P ( #I1 )
            pnd_Tpa_Pnd_Div_Amt_P.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                             //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P ( #I1 )
            pnd_Tpa_Pnd_Fin_Pay_P.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                             //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-P ( #I1 )
            pnd_Tpa_Pnd_Fin_Div_P.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                             //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-P ( #I1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                                   //Natural: IF #CNTRCT-PAYEE-CDE = 1
        {
            pnd_Tpa_Pnd_Fund_1.getValue(pnd_I1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #FUND-1 ( #I1 )
            pnd_Tpa_Pnd_Pay_Amt_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( #I1 )
            pnd_Tpa_Pnd_Div_Amt_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                             //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( #I1 )
            pnd_Tpa_Pnd_Fin_Pay_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                             //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-1 ( #I1 )
            pnd_Tpa_Pnd_Fin_Div_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                             //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-1 ( #I1 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Tpa_Pnd_Fund_P_1.getValue(pnd_I1).nadd(1);                                                                                                            //Natural: ADD 1 TO #FUND-P-1 ( #I1 )
                pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                       //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-1 ( #I1 )
                pnd_Tpa_Pnd_Div_Amt_P_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                       //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-1 ( #I1 )
                pnd_Tpa_Pnd_Fin_Pay_P_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                       //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-P-1 ( #I1 )
                pnd_Tpa_Pnd_Fin_Div_P_1.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                       //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-P-1 ( #I1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                                  //Natural: IF #CNTRCT-PAYEE-CDE > 1
        {
            pnd_Tpa_Pnd_Fund_2.getValue(pnd_I1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #FUND-2 ( #I1 )
            pnd_Tpa_Pnd_Pay_Amt_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( #I1 )
            pnd_Tpa_Pnd_Div_Amt_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                             //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( #I1 )
            pnd_Tpa_Pnd_Fin_Pay_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                             //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-2 ( #I1 )
            pnd_Tpa_Pnd_Fin_Div_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                             //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-2 ( #I1 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_Tpa_Pnd_Fund_P_2.getValue(pnd_I1).nadd(1);                                                                                                            //Natural: ADD 1 TO #FUND-P-2 ( #I1 )
                pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                       //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-2 ( #I1 )
                pnd_Tpa_Pnd_Div_Amt_P_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                       //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-2 ( #I1 )
                pnd_Tpa_Pnd_Fin_Pay_P_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                       //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-P-2 ( #I1 )
                pnd_Tpa_Pnd_Fin_Div_P_2.getValue(pnd_I1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                       //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-P-2 ( #I1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUB-TOTALS
    }
    private void sub_Check_Mode() throws Exception                                                                                                                        //Natural: CHECK-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Payment_Due.reset();                                                                                                                                          //Natural: RESET #PAYMENT-DUE
        short decideConditionsMet1438 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #SAVE-CPR-PAY-MODE;//Natural: VALUE 100
        if (condition((pnd_Save_Cpr_Pay_Mode.equals(100))))
        {
            decideConditionsMet1438++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(601))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))) //Natural: IF #MM = 01 OR = 04 OR = 07 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(602))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))) //Natural: IF #MM = 02 OR = 05 OR = 08 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(603))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))) //Natural: IF #MM = 03 OR = 06 OR = 09 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(701))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                //Natural: IF #MM = 01 OR = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(702))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                //Natural: IF #MM = 02 OR = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(703))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                //Natural: IF #MM = 03 OR = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(704))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                               //Natural: IF #MM = 04 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(705))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                               //Natural: IF #MM = 05 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(706))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                               //Natural: IF #MM = 06 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fctr.setValue(2);                                                                                                                                         //Natural: ASSIGN #FCTR := 2
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(801))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1)))                                                                                                         //Natural: IF #MM = 01
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(802))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2)))                                                                                                         //Natural: IF #MM = 02
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(803))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3)))                                                                                                         //Natural: IF #MM = 03
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(804))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4)))                                                                                                         //Natural: IF #MM = 04
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(805))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5)))                                                                                                         //Natural: IF #MM = 05
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(806))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6)))                                                                                                         //Natural: IF #MM = 06
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(807))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                                                         //Natural: IF #MM = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(808))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                                                         //Natural: IF #MM = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(809))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                                                         //Natural: IF #MM = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(810))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                                                                        //Natural: IF #MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(811))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                                                                        //Natural: IF #MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Save_Cpr_Pay_Mode.equals(812))))
        {
            decideConditionsMet1438++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                                                                        //Natural: IF #MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
                //*  PAYMENT FACTOR 6/09
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet1438 > 0))
        {
            //*  SEMI-ANNUAL
            if (condition(pnd_Save_Cpr_Pay_Mode.greaterOrEqual(701) && pnd_Save_Cpr_Pay_Mode.lessOrEqual(706)))                                                           //Natural: IF #SAVE-CPR-PAY-MODE = 701 THRU 706
            {
                pnd_Fctr.setValue(2);                                                                                                                                     //Natural: ASSIGN #FCTR := 2
            }                                                                                                                                                             //Natural: END-IF
            //*  ANNUAL
            if (condition(pnd_Save_Cpr_Pay_Mode.greaterOrEqual(801) && pnd_Save_Cpr_Pay_Mode.lessOrEqual(812)))                                                           //Natural: IF #SAVE-CPR-PAY-MODE = 801 THRU 812
            {
                pnd_Fctr.setValue(1);                                                                                                                                     //Natural: ASSIGN #FCTR := 1
            }                                                                                                                                                             //Natural: END-IF
            //*  QUARTERLY
            if (condition(pnd_Save_Cpr_Pay_Mode.greaterOrEqual(601) && pnd_Save_Cpr_Pay_Mode.lessOrEqual(603)))                                                           //Natural: IF #SAVE-CPR-PAY-MODE = 601 THRU 603
            {
                pnd_Fctr.setValue(4);                                                                                                                                     //Natural: ASSIGN #FCTR := 4
            }                                                                                                                                                             //Natural: END-IF
            //*  MONTHLY
            if (condition(pnd_Save_Cpr_Pay_Mode.equals(100)))                                                                                                             //Natural: IF #SAVE-CPR-PAY-MODE = 100
            {
                pnd_Fctr.setValue(12);                                                                                                                                    //Natural: ASSIGN #FCTR := 12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-MODE
    }
    private void sub_Retrieve_Monthly_Annuity_Unit_Value() throws Exception                                                                                               //Natural: RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.setValue(" ");                                                                                                                   //Natural: ASSIGN #PARM-FUND-1 := ' '
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_2.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde);                                                                                    //Natural: ASSIGN #PARM-FUND-2 := #SUMM-FUND-CDE
        //*  CONVERT CODE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Iaan0501_Pda_Pnd_Parm_Fund_1, pnd_Iaan0501_Pda_Pnd_Parm_Fund_2, pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde); //Natural: CALLNAT 'IAAN0511' #PARM-FUND-1 #PARM-FUND-2 #PARM-RTN-CDE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde.notEquals(getZero())))                                                                                            //Natural: IF #PARM-RTN-CDE NE 0
        {
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, " ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511");                                                                                   //Natural: WRITE ' ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511'
            if (Global.isEscape()) return;
            getReports().write(0, "PARM RETURN CODE ",pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde," FOR: ",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,pnd_Input_Record_Pnd_Cntrct_Payee_Cde); //Natural: WRITE 'PARM RETURN CODE ' #PARM-RTN-CDE ' FOR: ' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  USE THE ALPHA FUND CODE TO RETRIEVE THE FUNDS VALUE
        ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();                                                                                                           //Natural: RESET RETURN-CODE AUV-RETURNED AUV-DATE-RETURN
        ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();
        ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
        //*  L FOR LATEST AVAILABLE FACTOR
        //*  MONTHLY
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                                         //Natural: IF #CALL-TYPE = #LATEST-FACTOR
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                                   //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                          //Natural: ASSIGN IA-CHECK-DATE := 99999999
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date().reset();                                                                                                     //Natural: RESET IA-ISSUE-DATE
            //*  "P"= PAYMENT FOR ORIGINAL SETTLEMENT UNITS (FIRST OF MONTH ONLY)
            //*  P FOR FACTOR USED FOR THE PAYMENT
            //*  MONTHLY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                                   //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date_A().setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO IA-CHECK-DATE-A
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Ccyymm().setValue(pnd_Save_Issue_Dte);                                                                              //Natural: ASSIGN IA-ISSUE-CCYYMM := #SAVE-ISSUE-DTE
            if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd,"DD")))                                                                            //Natural: IF #CNTRCT-ISSUE-DTE-DD EQ MASK ( DD )
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(pnd_Save_Issue_Dte_Dd);                                                                          //Natural: ASSIGN IA-ISSUE-DAY := #SAVE-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(1);                                                                                              //Natural: ASSIGN IA-ISSUE-DAY := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FETCH THE ANNUITY UNIT VALUE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().notEquals(getZero())))                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  ERROR                                 *");                                                                          //Natural: WRITE '*                  ERROR                                 *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an RETURN CODE of =",ldaIaal050.getIa_Aian026_Linkage_Return_Code());                                              //Natural: WRITE '* and returned with an RETURN CODE of =' RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().equals(getZero())))                                                                                 //Natural: IF AUV-RETURNED = 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                 WARNING                                *");                                                                          //Natural: WRITE '*                 WARNING                                *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                        *");                                                                          //Natural: WRITE '*                                                        *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an ANNUITY-UNIT-VALUE of '0'         *");                                                                          //Natural: WRITE '* and returned with an ANNUITY-UNIT-VALUE of "0"         *'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  LB 10/97
        pnd_Page_Headings_Pnd_Page_Heading2.setValue(pnd_Page_Headings_Pnd_Page_1_Heading2);                                                                              //Natural: MOVE #PAGE-1-HEADING2 TO #PAGE-HEADING2
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Save_Check_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm.setValue(pnd_Save_Check_Dte_A_Pnd_Mm);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-MM := #MM
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd.setValue(pnd_Save_Check_Dte_A_Pnd_Dd);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-DD := #DD
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy.setValue(pnd_Save_Check_Dte_A_Pnd_Yyyy);                                                                                 //Natural: ASSIGN #PAYMENT-DUE-YYYY := #YYYY
        pnd_Payment_Due_Dte_Pnd_Slash1.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH1 := '/'
        pnd_Payment_Due_Dte_Pnd_Slash2.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH2 := '/'
        //*  INITIALIZATION
    }
    private void sub_Write_Cntrl_Summary() throws Exception                                                                                                               //Natural: WRITE-CNTRL-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  TIAA AND TIAA REAL ESTATE ANNUAL AND MONTHLY TOT
                                                                                                                                                                          //Natural: PERFORM WRITE-PAGE-1
        sub_Write_Page_1();
        if (condition(Global.isEscape())) {return;}
        pnd_Page_Headings_Pnd_Page_Heading2.setValue(pnd_Page_Headings_Pnd_Page_2_Heading2);                                                                              //Natural: MOVE #PAGE-2-HEADING2 TO #PAGE-HEADING2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  CREF ANNUAL TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-PAGE-2
        sub_Write_Page_2();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                                         //Natural: IF #CALL-TYPE = #LATEST-FACTOR
        {
            pnd_Page_Headings_Pnd_Page_Heading2.setValue(pnd_Page_Headings_Pnd_Page_3_Heading2a);                                                                         //Natural: MOVE #PAGE-3-HEADING2A TO #PAGE-HEADING2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Page_Headings_Pnd_Page_Heading2.setValue(pnd_Page_Headings_Pnd_Page_3_Heading2b);                                                                         //Natural: MOVE #PAGE-3-HEADING2B TO #PAGE-HEADING2
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  CREF MONTHLY TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-PAGE-3
        sub_Write_Page_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Page_Headings_Pnd_Page_Heading2.setValue("T/L ANNUAL TOTALS");                                                                                                //Natural: MOVE 'T/L ANNUAL TOTALS'TO #PAGE-HEADING2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  T/L  ANNUAL TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-PAGE-4
        sub_Write_Page_4();
        if (condition(Global.isEscape())) {return;}
        pnd_Page_Headings_Pnd_Page_Heading2.setValue("T/L MONTHLY TOTALS");                                                                                               //Natural: MOVE 'T/L MONTHLY TOTALS' TO #PAGE-HEADING2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  T/L MONTHLY TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-PAGE-5
        sub_Write_Page_5();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE-CNTRL-SUMMARY
    }
    private void sub_Write_Page_1() throws Exception                                                                                                                      //Natural: WRITE-PAGE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE TIAA TRADITIONAL, TIAA ANNUAL AND MONTHLY REAL ESTATE,
        //*  DEDUCTION AND PAYEES TOTALS                                /* LB 10/97
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA TRADITIONAL FUND RECORDS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(1),  //Natural: WRITE ( 1 ) / 1T 'TIAA TRADITIONAL FUND RECORDS' 45T #ADMN-MSTR ( 1 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( 1 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA TRADITIONAL PERIODIC PAYMENTS",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(1),  //Natural: WRITE ( 1 ) 1T 'TIAA TRADITIONAL PERIODIC PAYMENTS' 44T #ADMN-MSTR-PER-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA TRADITIONAL PERIODIC DIVIDENDS",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Divd.getValue(1),  //Natural: WRITE ( 1 ) 1T 'TIAA TRADITIONAL PERIODIC DIVIDENDS' 44T #ADMN-MSTR-PER-DIVD ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-DIVD ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Divd.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA TRADITIONAL FINAL PAYMENTS",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Pymnt.getValue(1),  //Natural: WRITE ( 1 ) 1T 'TIAA TRADITIONAL FINAL PAYMENTS' 44T #ADMN-MSTR-FIN-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-FIN-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Pymnt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA TRADITIONAL FINAL DIVIDENDS",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Fin_Divd.getValue(1),  //Natural: WRITE ( 1 ) 1T 'TIAA TRADITIONAL FINAL DIVIDENDS' 44T #ADMN-MSTR-FIN-DIVD ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-FIN-DIVD ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 1T 'TPA  PER IVC AMOUNT                    ' 44T TOTAL-IVC-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T TOTAL-IVC-AMT-P ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Fin_Divd.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(1),"TPA  PER IVC AMOUNT                    ",new TabSetting(44),total_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),total_Ivc_Amt_P, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *********************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TPA  TRADITIONAL FUND RECORDS",new TabSetting(50),pnd_Tpa_Pnd_Fund.getValue(1),             //Natural: WRITE ( 1 ) / 1T 'TPA  TRADITIONAL FUND RECORDS' 50T #FUND ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  TRADITIONAL PERIODIC PAYMENTS     ' 44T #PAY-AMT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL PERIODIC DIVIDEND     ' 44T #DIV-AMT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL FINAL PAYMENTS        ' 44T #FIN-PAY ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL FINAL  DIVIDEND       ' 44T #FIN-DIV ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  TRADITIONAL PERIODIC PAYMENTS     ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL PERIODIC DIVIDEND     ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL FINAL PAYMENTS        ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL FINAL  DIVIDEND       ",new TabSetting(44),pnd_Tpa_Pnd_Fin_Div.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ******************************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  PAYEES = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(1), new ReportEditMask       //Natural: WRITE ( 1 ) 'TPA  PAYEES = 1  FUND RECORDS           ' 50T #FUND-1 ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  PAYEES = 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES = 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES = 1  FINAL PAYMENTS         ' 44T #FIN-PAY-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-1 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  PAYEES = 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES = 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES = 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_1.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_1.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  PAYEES > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(1), new ReportEditMask       //Natural: WRITE ( 1 ) 'TPA  PAYEES > 1  FUND RECORDS           ' 50T #FUND-2 ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  PAYEES > 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES > 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES > 1  FINAL PAYMENTS         ' 44T #FIN-PAY-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-2 ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  PAYEES > 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES > 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES > 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_2.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_2.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ********************************************************
        //*  ADDED FOLLOWING 10/01
        //*  TPA REINVESTMENT FUNDS
        getReports().write(1, ReportOption.NOTITLE,"TPA  REINVESTMENT FUND RECORDS          ",new TabSetting(49),pnd_Total_Rinv_Fnd, new ReportEditMask                   //Natural: WRITE ( 1 ) 'TPA  REINVESTMENT FUND RECORDS          ' 49T #TOTAL-RINV-FND ( EM = ZZZ,ZZZ,ZZ9 ) 69T #TOTAL-RINV-FND-DUE ( EM = ZZZ,ZZZ,ZZ9 ) / 'TPA  REINVESTMENT PERIODIC PAYMENTS     ' 44T #TOTAL-RINV-PMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #TOTAL-RINV-PMT-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  REINVESTMENT PERIODIC DIVIDEND     ' 44T #TOTAL-RINV-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #TOTAL-RINV-DIV-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_Total_Rinv_Fnd_Due, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TPA  REINVESTMENT PERIODIC PAYMENTS     ",new 
            TabSetting(44),pnd_Total_Rinv_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Total_Rinv_Pmt_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  REINVESTMENT PERIODIC DIVIDEND     ",new 
            TabSetting(44),pnd_Total_Rinv_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Total_Rinv_Div_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TPA  RINV   = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(5), new ReportEditMask       //Natural: WRITE ( 1 ) 'TPA  RINV   = 1  FUND RECORDS           ' 50T #FUND-1 ( 5 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 5 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  RINV   = 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  RINV   = 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  RINV   = 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  RINV   = 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  RINV   > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(5), new ReportEditMask       //Natural: WRITE ( 1 ) 'TPA  RINV   > 1  FUND RECORDS           ' 50T #FUND-2 ( 5 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 5 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  RINV   > 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  RINV   > 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  RINV   > 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  RINV   > 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **************************************************************
        //*  TPA ROLLOVER
        getReports().write(1, ReportOption.NOTITLE,"TPA  ROLLOVER FUND RECORDS              ",new TabSetting(49),pnd_T_Tpa_Roll_Fnd, new ReportEditMask                   //Natural: WRITE ( 1 ) 'TPA  ROLLOVER FUND RECORDS              ' 49T #T-TPA-ROLL-FND ( EM = ZZZ,ZZZ,ZZ9 ) 69T #T-TPA-ROLL-DUE ( EM = ZZZ,ZZZ,ZZ9 ) / 'TPA  ROLLOVER PERIODIC PAYMENTS         ' 44T #T-TPA-ROLL-PMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-TPA-ROLL-PMT-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  ROLLOVER PERIODIC DIVIDEND         ' 44T #T-TPA-ROLL-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-TPA-ROLL-DIV-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  ROLLOVER IVC AMOUNT                ' 44T TOTAL-ROLL-TPA-IVC-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T TOTAL-ROLL-TPA-IVC-AMT-P ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_T_Tpa_Roll_Due, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TPA  ROLLOVER PERIODIC PAYMENTS         ",new 
            TabSetting(44),pnd_T_Tpa_Roll_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Tpa_Roll_Pmt_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  ROLLOVER PERIODIC DIVIDEND         ",new 
            TabSetting(44),pnd_T_Tpa_Roll_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Tpa_Roll_Div_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  ROLLOVER IVC AMOUNT                ",new 
            TabSetting(44),total_Roll_Tpa_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),total_Roll_Tpa_Ivc_Amt_P, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 6/03
        getReports().write(1, ReportOption.NOTITLE,"TPA  ROLLOVER  = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(6), new                   //Natural: WRITE ( 1 ) 'TPA  ROLLOVER  = 1  FUND RECORDS           ' 50T #FUND-1 ( 6 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 6 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  ROLLOVER  = 1  PERIODIC PAYMENTS ' 44T #PAY-AMT-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  ROLLOVER  = 1  PERIODIC DIVIDEND ' 44T #DIV-AMT-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(6), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  ROLLOVER  = 1  PERIODIC PAYMENTS ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  ROLLOVER  = 1  PERIODIC DIVIDEND ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  ROLLOVER  > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(6), new                   //Natural: WRITE ( 1 ) 'TPA  ROLLOVER  > 1  FUND RECORDS           ' 50T #FUND-2 ( 6 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 6 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  ROLLOVER  > 1  PERIODIC PAYMENTS  ' 44T #PAY-AMT-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  ROLLOVER  > 1  PERIODIC DIVIDEND  ' 44T #DIV-AMT-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(6), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  ROLLOVER  > 1  PERIODIC PAYMENTS  ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  ROLLOVER  > 1  PERIODIC DIVIDEND  ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 10/01
        //* ********************************************************
        //* *        P/I  FUNDS
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"P&I  TRADITIONAL FUND RECORDS",new TabSetting(50),pnd_Tpa_Pnd_Fund.getValue(3),             //Natural: WRITE ( 1 ) / 1T 'P&I  TRADITIONAL FUND RECORDS' 50T #FUND ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  TRADITIONAL PERIODIC PAYMENTS     ' 44T #PAY-AMT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL PERIODIC DIVIDEND     ' 44T #DIV-AMT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL FINAL PAYMENTS        ' 44T #FIN-PAY ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL FINAL  DIVIDEND       ' 44T #FIN-DIV ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  TRADITIONAL PERIODIC PAYMENTS     ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL PERIODIC DIVIDEND     ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL FINAL PAYMENTS        ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL FINAL  DIVIDEND       ",new TabSetting(44),pnd_Tpa_Pnd_Fin_Div.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ******************************************************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  PAYEES = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(3), new ReportEditMask       //Natural: WRITE ( 1 ) 'P&I  PAYEES = 1  FUND RECORDS           ' 50T #FUND-1 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  PAYEES = 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES = 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES = 1  FINAL PAYMENTS         ' 44T #FIN-PAY-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  PAYEES = 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES = 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES = 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_1.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  PAYEES > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(3), new ReportEditMask       //Natural: WRITE ( 1 ) 'P&I  PAYEES > 1  FUND RECORDS           ' 50T #FUND-2 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  PAYEES > 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES > 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES > 1  FINAL PAYMENTS         ' 44T #FIN-PAY-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  PAYEES > 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES > 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES > 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_2.getValue(3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ********************************************************
        //*  ADDED FOLLOWING 10/01
        //*  P/I ROLLOVER FUNDS
        //* ****************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  ROLLOVER FUND RECORDS              ",new TabSetting(49),pnd_T_P_I_Roll_Fnd, new ReportEditMask                   //Natural: WRITE ( 1 ) 'P&I  ROLLOVER FUND RECORDS              ' 49T #T-P-I-ROLL-FND ( EM = ZZZ,ZZZ,ZZ9 ) 69T #T-P-I-ROLL-DUE ( EM = ZZZ,ZZZ,ZZ9 ) / 'P&I  ROLLOVER PERIODIC PAYMENTS         ' 44T #T-P-I-ROLL-PMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-P-I-ROLL-PMT-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER PERIODIC DIVIDEND         ' 44T #T-P-I-ROLL-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-P-I-ROLL-DIV-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER FINAL PAYMENT             ' 44T #T-P-I-ROLL-FIN ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-P-I-ROLL-FIN-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_T_P_I_Roll_Due, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"P&I  ROLLOVER PERIODIC PAYMENTS         ",new 
            TabSetting(44),pnd_T_P_I_Roll_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_P_I_Roll_Pmt_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER PERIODIC DIVIDEND         ",new 
            TabSetting(44),pnd_T_P_I_Roll_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_P_I_Roll_Div_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER FINAL PAYMENT             ",new 
            TabSetting(44),pnd_T_P_I_Roll_Fin, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_P_I_Roll_Fin_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"P&I  ROLLOVER   = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(8), new                  //Natural: WRITE ( 1 ) 'P&I  ROLLOVER   = 1  FUND RECORDS           ' 50T #FUND-1 ( 8 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 8 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  ROLLOVER   = 1  PERIODIC PAYMENTS   ' 44T #PAY-AMT-1 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER   = 1  PERIODIC DIVIDEND  ' 44T #DIV-AMT-1 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER   = 1  FINAL PAYMENT ' 44T #FIN-DIV-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P-1 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(8), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  ROLLOVER   = 1  PERIODIC PAYMENTS   ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(8), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(8), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER   = 1  PERIODIC DIVIDEND  ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(8), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(8), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER   = 1  FINAL PAYMENT ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Div_1.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P_1.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  ROLLOVER   > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(8), new                  //Natural: WRITE ( 1 ) 'P&I  ROLLOVER   > 1  FUND RECORDS           ' 50T #FUND-2 ( 8 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 8 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  ROLLOVER   > 1  PERIODIC PAYMENTS  ' 44T #PAY-AMT-2 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER   > 1  PERIODIC DIVIDEND  ' 44T #DIV-AMT-2 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 8 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  ROLLOVER   > 1  FINAL PAYMENT ' 44T #FIN-DIV-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P-2 ( 6 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(8), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  ROLLOVER   > 1  PERIODIC PAYMENTS  ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(8), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(8), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER   > 1  PERIODIC DIVIDEND  ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(8), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(8), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  ROLLOVER   > 1  FINAL PAYMENT ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Div_2.getValue(6), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P_2.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 10/01
        //* **********************************************************
        //* *   IPRO FUNDS
        //* ********************************************************
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IPRO TRADITIONAL FUND RECORDS",new TabSetting(50),pnd_Tpa_Pnd_Fund.getValue(2),             //Natural: WRITE ( 1 ) / 1T 'IPRO TRADITIONAL FUND RECORDS' 50T #FUND ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO TRADITIONAL PERIODIC PAYMENTS     ' 44T #PAY-AMT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL PERIODIC DIVIDEND     ' 44T #DIV-AMT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL FINAL PAYMENTS        ' 44T #FIN-PAY ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL FINAL  DIVIDEND       ' 44T #FIN-DIV ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO TRADITIONAL PERIODIC PAYMENTS     ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL PERIODIC DIVIDEND     ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL FINAL PAYMENTS        ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL FINAL  DIVIDEND       ",new TabSetting(44),pnd_Tpa_Pnd_Fin_Div.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"IPRO PAYEES = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(2), new ReportEditMask       //Natural: WRITE ( 1 ) 'IPRO PAYEES = 1  FUND RECORDS           ' 50T #FUND-1 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO PAYEES = 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES = 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES = 1  FINAL PAYMENTS         ' 44T #FIN-PAY-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO PAYEES = 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES = 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES = 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_1.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"IPRO PAYEES > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(2), new ReportEditMask       //Natural: WRITE ( 1 ) 'IPRO PAYEES > 1  FUND RECORDS           ' 50T #FUND-2 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO PAYEES > 1  PERIODIC PAYMENTS      ' 44T #PAY-AMT-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES > 1  PERIODIC DIVIDEND      ' 44T #DIV-AMT-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES > 1  FINAL PAYMENTS         ' 44T #FIN-PAY-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO PAYEES > 1  PERIODIC PAYMENTS      ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES > 1  PERIODIC DIVIDEND      ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES > 1  FINAL PAYMENTS         ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay_2.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ***
        //*  ADDED FOLLOWING 10/01
        //*  IPRO ROLLOVER FUNDS
        getReports().write(1, ReportOption.NOTITLE,"IPRO ROLLOVER FUND RECORDS              ",new TabSetting(49),pnd_T_Ipr_Roll_Fnd, new ReportEditMask                   //Natural: WRITE ( 1 ) 'IPRO ROLLOVER FUND RECORDS              ' 49T #T-IPR-ROLL-FND ( EM = ZZZ,ZZZ,ZZ9 ) 69T #T-IPR-ROLL-DUE ( EM = ZZZ,ZZZ,ZZ9 ) / 'IPRO ROLLOVER PERIODIC PAYMENTS         ' 44T #T-IPR-ROLL-PMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-IPR-ROLL-PMT-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER PERIODIC DIVIDEND         ' 44T #T-IPR-ROLL-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-IPR-ROLL-DIV-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER FINAL PAYMENT             ' 44T #T-IPR-ROLL-FIN ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-IPR-ROLL-FIN-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_T_Ipr_Roll_Due, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"IPRO ROLLOVER PERIODIC PAYMENTS         ",new 
            TabSetting(44),pnd_T_Ipr_Roll_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ipr_Roll_Pmt_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER PERIODIC DIVIDEND         ",new 
            TabSetting(44),pnd_T_Ipr_Roll_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ipr_Roll_Div_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER FINAL PAYMENT             ",new 
            TabSetting(44),pnd_T_Ipr_Roll_Fin, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ipr_Roll_Fin_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO ROLLOVER   = 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_1.getValue(7), new                  //Natural: WRITE ( 1 ) 'IPRO ROLLOVER   = 1  FUND RECORDS           ' 50T #FUND-1 ( 7 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-1 ( 7 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO ROLLOVER   = 1  PERIODIC PAYMENTS  ' 44T #PAY-AMT-1 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-1 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER   = 1  PERIODIC DIVIDEND  ' 44T #DIV-AMT-1 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-1 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER   = 1  FINAL PAYMENT ' 44T #FIN-DIV-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P-1 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_1.getValue(7), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO ROLLOVER   = 1  PERIODIC PAYMENTS  ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_1.getValue(7), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(7), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER   = 1  PERIODIC DIVIDEND  ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_1.getValue(7), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_1.getValue(7), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER   = 1  FINAL PAYMENT ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Div_1.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P_1.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,"IPRO ROLLOVER   > 1  FUND RECORDS           ",new TabSetting(50),pnd_Tpa_Pnd_Fund_2.getValue(7), new                  //Natural: WRITE ( 1 ) 'IPRO ROLLOVER   > 1  FUND RECORDS           ' 50T #FUND-2 ( 7 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P-2 ( 7 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO ROLLOVER   > 1  PERIODIC PAYMENTS   ' 44T #PAY-AMT-2 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P-2 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER   > 1  PERIODIC DIVIDEND   ' 44T #DIV-AMT-2 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P-2 ( 7 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO ROLLOVER   > 1  FINAL PAYMENT ' 44T #FIN-DIV-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P-2 ( 5 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P_2.getValue(7), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO ROLLOVER   > 1  PERIODIC PAYMENTS   ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt_2.getValue(7), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(7), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER   > 1  PERIODIC DIVIDEND   ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt_2.getValue(7), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P_2.getValue(7), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO ROLLOVER   > 1  FINAL PAYMENT ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Div_2.getValue(5), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P_2.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 10/01
        //* ********************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"OTHER TRADITIONAL FUND RECORDS",new TabSetting(50),pnd_Tpa_Pnd_Fund.getValue(4),            //Natural: WRITE ( 1 ) / 1T 'OTHER TRADITIONAL FUND RECORDS' 50T #FUND ( 4 ) ( EM = ZZ,ZZZ,ZZ9 ) 70T #FUND-P ( 4 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'OTHER TRADITIONAL PERIODIC PAYMENTS     ' 44T #PAY-AMT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #PAY-AMT-P ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL PERIODIC DIVIDEND     ' 44T #DIV-AMT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #DIV-AMT-P ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL FINAL PAYMENTS        ' 44T #FIN-PAY ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-PAY-P ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL FINAL  DIVIDEND       ' 44T #FIN-DIV ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #FIN-DIV-P ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new TabSetting(70),pnd_Tpa_Pnd_Fund_P.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"OTHER TRADITIONAL PERIODIC PAYMENTS     ",new 
            TabSetting(44),pnd_Tpa_Pnd_Pay_Amt.getValue(4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Pay_Amt_P.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL PERIODIC DIVIDEND     ",new TabSetting(44),pnd_Tpa_Pnd_Div_Amt.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Div_Amt_P.getValue(4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL FINAL PAYMENTS        ",new 
            TabSetting(44),pnd_Tpa_Pnd_Fin_Pay.getValue(4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Pay_P.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL FINAL  DIVIDEND       ",new TabSetting(44),pnd_Tpa_Pnd_Fin_Div.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_Tpa_Pnd_Fin_Div_P.getValue(4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ********************************************************
        //* ********************************************************
        //*  ADDED FOLLOWING 10/01
        //*  ANNUITY CERTAIN ROLLLOVER FUNDS
        //* ********************************************************
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"AC   ROLLOVER FUND RECORDS              ",new TabSetting(49),pnd_T_Ac_Roll_Fnd, new ReportEditMask                    //Natural: WRITE ( 1 ) 'AC   ROLLOVER FUND RECORDS              ' 49T #T-AC-ROLL-FND ( EM = ZZZ,ZZZ,ZZ9 ) 69T #T-AC-ROLL-DUE ( EM = ZZZ,ZZZ,ZZ9 ) / 'AC   ROLLOVER PERIODIC PAYMENTS         ' 44T #T-AC-ROLL-PMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-AC-ROLL-PMT-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'AC   ROLLOVER PERIODIC DIVIDEND         ' 44T #T-AC-ROLL-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-AC-ROLL-DIV-DUE ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'AC  ROLLOVER IVC AMOUNT                ' 44T TOTAL-ROLL-AC-IVC-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T TOTAL-ROLL-AC-IVC-AMT-P ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_T_Ac_Roll_Due, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"AC   ROLLOVER PERIODIC PAYMENTS         ",new 
            TabSetting(44),pnd_T_Ac_Roll_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ac_Roll_Pmt_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"AC   ROLLOVER PERIODIC DIVIDEND         ",new 
            TabSetting(44),pnd_T_Ac_Roll_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ac_Roll_Div_Due, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"AC  ROLLOVER IVC AMOUNT                ",new 
            TabSetting(44),total_Roll_Ac_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),total_Roll_Ac_Ivc_Amt_P, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 6/03
        //*  WRITE (1) '-'(130)
        getReports().write(1, ReportOption.NOTITLE,"AC VARIABLE ROLLOVER FUND RECORDS       ",new TabSetting(49),pnd_T_Ac_Roll_Fnd_V, new ReportEditMask                  //Natural: WRITE ( 1 ) 'AC VARIABLE ROLLOVER FUND RECORDS       ' 49T #T-AC-ROLL-FND-V ( EM = ZZZ,ZZZ,ZZ9 ) 69T #T-AC-ROLL-DUE-V ( EM = ZZZ,ZZZ,ZZ9 ) / 'AC VARIABLE ROLLOVER PAYMENTS           ' 44T #T-AC-ROLL-PMT-V ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 64T #T-AC-ROLL-PMT-DUE-V ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9"),new TabSetting(69),pnd_T_Ac_Roll_Due_V, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"AC VARIABLE ROLLOVER PAYMENTS           ",new 
            TabSetting(44),pnd_T_Ac_Roll_Pmt_V, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),pnd_T_Ac_Roll_Pmt_Due_V, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  END OF ADD 10/01
        //* **********************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA ANNUAL REAL-ESTATE RECORDS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(9),  //Natural: WRITE ( 1 ) / 1T 'TIAA ANNUAL REAL-ESTATE RECORDS' 45T #ADMN-MSTR ( 9 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( 9 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA ANNUAL REAL-ESTATE UNITS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(9),  //Natural: WRITE ( 1 ) 1T 'TIAA ANNUAL REAL-ESTATE UNITS' 45T #ADMN-MSTR-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA ANNUAL REAL-ESTATE AMOUNT",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(9),  //Natural: WRITE ( 1 ) 1T 'TIAA ANNUAL REAL-ESTATE AMOUNT' 44T #ADMN-MSTR-PER-PYMNT ( 9 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( 9 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(9), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  WRITE THE TOTALS FOR TIAA REAL ESTATE MONTHLY FUNDS
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA MONTHLY REAL-ESTATE RECORDS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(29),  //Natural: WRITE ( 1 ) / 1T 'TIAA MONTHLY REAL-ESTATE RECORDS' 45T #ADMN-MSTR ( 29 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( 29 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA MONTHLY REAL-ESTATE UNITS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(29),  //Natural: WRITE ( 1 ) 1T 'TIAA MONTHLY REAL-ESTATE UNITS' 45T #ADMN-MSTR-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA MONTHLY REAL-ESTATE AMOUNT",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(29),  //Natural: WRITE ( 1 ) 1T 'TIAA MONTHLY REAL-ESTATE AMOUNT' 44T #ADMN-MSTR-PER-PYMNT ( 29 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( 29 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 2X #BASIS
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(29), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Basis);
        if (Global.isEscape()) return;
        //* **********************************************************  9/08 START
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA ANNUAL ACCESS RECORDS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(11),  //Natural: WRITE ( 1 ) / 1T 'TIAA ANNUAL ACCESS RECORDS' 45T #ADMN-MSTR ( 11 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( 11 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA ANNUAL ACCESS UNITS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(11),  //Natural: WRITE ( 1 ) 1T 'TIAA ANNUAL ACCESS UNITS' 45T #ADMN-MSTR-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA ANNUAL ACCESS AMOUNT",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(11),  //Natural: WRITE ( 1 ) 1T 'TIAA ANNUAL ACCESS AMOUNT' 44T #ADMN-MSTR-PER-PYMNT ( 11 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( 11 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(11), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  WRITE THE TOTALS FOR TIAA ACCESS MONTHLY FUNDS
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA MONTHLY ACCESS RECORDS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(31),  //Natural: WRITE ( 1 ) / 1T 'TIAA MONTHLY ACCESS RECORDS' 45T #ADMN-MSTR ( 31 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( 31 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA MONTHLY ACCESS UNITS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(31),  //Natural: WRITE ( 1 ) 1T 'TIAA MONTHLY ACCESS UNITS' 45T #ADMN-MSTR-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  10/09
        //*  10/09
        //*  10/09
        //*  10/09
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TIAA MONTHLY ACCESS AMOUNT",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(31),  //Natural: WRITE ( 1 ) 1T 'TIAA MONTHLY ACCESS AMOUNT' 44T #ADMN-MSTR-PER-PYMNT ( 31 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( 31 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 2X #BASIS
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(31), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Basis);
        if (Global.isEscape()) return;
        pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(9).setValue("R");                                                                                              //Natural: ASSIGN FUND-CODE ( 9 ) := 'R'
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(9));                                 //Natural: ASSIGN FUND-ANNUAL-UNITS ( 9 ) := #UPEND-MSTR-UNITS ( 9 )
        pend_Annual_Units.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(9));                                                                 //Natural: ASSIGN PEND-ANNUAL-UNITS ( 9 ) := #PENDR-MSTR-UNITS ( 9 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(9));                          //Natural: ASSIGN FUND-ANNUAL-PAYMENTS ( 9 ) := #UPEND-MSTR-PER-PYMNT ( 9 )
        pend_Annual_Payments.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(9));                                                          //Natural: ASSIGN PEND-ANNUAL-PAYMENTS ( 9 ) := #PENDR-MSTR-PER-PYMNT ( 9 )
        pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(29));                               //Natural: ASSIGN FUND-MONTHLY-UNITS ( 9 ) := #UPEND-MSTR-UNITS ( 29 )
        pend_Monthly_Units.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(29));                                                               //Natural: ASSIGN PEND-MONTHLY-UNITS ( 9 ) := #PENDR-MSTR-UNITS ( 29 )
        pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(29));                        //Natural: ASSIGN FUND-MONTHLY-PAYMENTS ( 9 ) := #UPEND-MSTR-PER-PYMNT ( 29 )
        pend_Monthly_Payments.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(29));                                                        //Natural: ASSIGN PEND-MONTHLY-PAYMENTS ( 9 ) := #PENDR-MSTR-PER-PYMNT ( 29 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(9));                     //Natural: ASSIGN FUND-ANNUAL-ANNLZD-UNITS ( 9 ) := #UPEND-MSTR-ANNL-UNITS ( 9 )
        pend_Annual_Annlzd_Units.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(9));                                                     //Natural: ASSIGN PEND-ANNUAL-ANNLZD-UNITS ( 9 ) := #PENDR-MSTR-ANNL-UNITS ( 9 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(9));                     //Natural: ASSIGN FUND-ANNUAL-ANNLZD-PAYTS ( 9 ) := #UPEND-MSTR-ANNL-PYMNT ( 9 )
        pend_Annual_Annlzd_Payts.getValue(9).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(9));                                                     //Natural: ASSIGN PEND-ANNUAL-ANNLZD-PAYTS ( 9 ) := #PENDR-MSTR-ANNL-PYMNT ( 9 )
        pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(10).setValue("D");                                                                                             //Natural: ASSIGN FUND-CODE ( 10 ) := 'D'
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(11));                               //Natural: ASSIGN FUND-ANNUAL-UNITS ( 10 ) := #UPEND-MSTR-UNITS ( 11 )
        pend_Annual_Units.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(11));                                                               //Natural: ASSIGN PEND-ANNUAL-UNITS ( 10 ) := #PENDR-MSTR-UNITS ( 11 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(11));                        //Natural: ASSIGN FUND-ANNUAL-PAYMENTS ( 10 ) := #UPEND-MSTR-PER-PYMNT ( 11 )
        pend_Annual_Payments.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(11));                                                        //Natural: ASSIGN PEND-ANNUAL-PAYMENTS ( 10 ) := #PENDR-MSTR-PER-PYMNT ( 11 )
        pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(31));                              //Natural: ASSIGN FUND-MONTHLY-UNITS ( 10 ) := #UPEND-MSTR-UNITS ( 31 )
        pend_Monthly_Units.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(31));                                                              //Natural: ASSIGN PEND-MONTHLY-UNITS ( 10 ) := #PENDR-MSTR-UNITS ( 31 )
        pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(31));                       //Natural: ASSIGN FUND-MONTHLY-PAYMENTS ( 10 ) := #UPEND-MSTR-PER-PYMNT ( 31 )
        pend_Monthly_Payments.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(31));                                                       //Natural: ASSIGN PEND-MONTHLY-PAYMENTS ( 10 ) := #PENDR-MSTR-PER-PYMNT ( 31 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(11));                   //Natural: ASSIGN FUND-ANNUAL-ANNLZD-UNITS ( 10 ) := #UPEND-MSTR-ANNL-UNITS ( 11 )
        pend_Annual_Annlzd_Units.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(11));                                                   //Natural: ASSIGN PEND-ANNUAL-ANNLZD-UNITS ( 10 ) := #PENDR-MSTR-ANNL-UNITS ( 11 )
        pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(11));                   //Natural: ASSIGN FUND-ANNUAL-ANNLZD-PAYTS ( 10 ) := #UPEND-MSTR-ANNL-PYMNT ( 11 )
        pend_Annual_Annlzd_Payts.getValue(10).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(11));                                                   //Natural: ASSIGN PEND-ANNUAL-ANNLZD-PAYTS ( 10 ) := #PENDR-MSTR-ANNL-PYMNT ( 11 )
        //*  10/09 - END - USE TOTALS FROM UNPENDED
        //*  END OF ADD  2/08
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),"ACTIVE DEDUCTIONS",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Deds_Actv,   //Natural: WRITE ( 1 ) // 1T 'ACTIVE DEDUCTIONS' 45T #ADMN-DEDS-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-DEDS-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Actv, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"DEDUCTIONS",new TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Deds_Amt, new ReportEditMask        //Natural: WRITE ( 1 ) 1T 'DEDUCTIONS' 44T #ADMN-DEDS-AMT ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-DEDS-AMT ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Deds_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TIAA ACTIVE PAYEES",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Tiaa_Actv,          //Natural: WRITE ( 1 ) / 1T 'TIAA ACTIVE PAYEES' 45T #ADMN-TIAA-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-TIAA-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Tiaa_Actv, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CREF ACTIVE PAYEES",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Cref_Actv,                  //Natural: WRITE ( 1 ) 1T 'CREF ACTIVE PAYEES' 45T #ADMN-CREF-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-CREF-ACTV ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Cref_Actv, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"INACTIVE    PAYEES",new TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Inactive_Payees,            //Natural: WRITE ( 1 ) 1T 'INACTIVE    PAYEES' 45T #ADMN-INACTIVE-PAYEES ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T '                               '
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),"                               ");
        if (Global.isEscape()) return;
        //*  WRITE-PAGE-1
    }
    private void sub_Write_Page_2() throws Exception                                                                                                                      //Natural: WRITE-PAGE-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  CREF ANNUAL FUND TOTALS                                    /* LB 10/97
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"CREF ANNUAL ACTIVE FUND RECORDS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(1),  //Natural: WRITE ( 1 ) / 1T 'CREF ANNUAL ACTIVE FUND RECORDS' 45T #TOT-ADMN-MSTR ( 1 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #TOT-PYMNT-MSTR ( 1 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CREF ANNUAL ACTIVE UNITS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(1),  //Natural: WRITE ( 1 ) 1T 'CREF ANNUAL ACTIVE UNITS' 45T #TOT-ADMN-MSTR-UNITS ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #TOT-PYMNT-MSTR-UNITS ( 1 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(1), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CREF ANNUAL ACTIVE AMOUNT",new TabSetting(44),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(1),  //Natural: WRITE ( 1 ) 1T 'CREF ANNUAL ACTIVE AMOUNT' 44T #TOT-ADMN-MSTR-PER-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #TOT-PYMNT-MSTR-PER-PYMNT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*   ADDED FOLLOWING PRINT CREF PRODS    8/96
        //*  ADDED 2/04
        //*  #PROD-CNT
        pnd_I5.reset();                                                                                                                                                   //Natural: RESET #I5
        FOR04:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            //*  BYPASS REAL ESTATE AND ACCESS /* 9/08
            if (condition(pnd_I.equals(9) || pnd_I.equals(11)))                                                                                                           //Natural: IF #I = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_3.setValue(pnd_I);                                                                                                                              //Natural: ASSIGN #PARM-FUND-3 := #I
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF ANNUAL");                                                                                                  //Natural: ASSIGN #FUND-CREF-CON := 'CREF ANNUAL'
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS  ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS  '
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T #ADMN-MSTR ( #I ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( #I ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS         ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'UNITS         '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 45T #ADMN-MSTR-UNITS ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("AMOUNT        ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'AMOUNT        '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 44T #ADMN-MSTR-PER-PYMNT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_I), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fnd_I.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #FND-I := #I
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ALPHA-FUND-CODE
            sub_Retrieve_Alpha_Fund_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.notEquals("U")))                                                                                               //Natural: IF #PARM-FUND-1 NE 'U'
            {
                //*  10/09
                //*  10/09
                //*  10/09
                //*  10/09
                //*  ADDED 10/09
                //*  10/09
                //*  ADDED 10/09
                //*  10/09
                pnd_I5.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I5
                pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5).setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                    //Natural: ASSIGN FUND-CODE ( #I5 ) := #PARM-FUND-1
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_I));                //Natural: ASSIGN FUND-ANNUAL-UNITS ( #I5 ) := #UPEND-MSTR-UNITS ( #I )
                pend_Annual_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_I));                                                //Natural: ASSIGN PEND-ANNUAL-UNITS ( #I5 ) := #PENDR-MSTR-UNITS ( #I )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_I));         //Natural: ASSIGN FUND-ANNUAL-PAYMENTS ( #I5 ) := #UPEND-MSTR-PER-PYMNT ( #I )
                pend_Annual_Payments.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_I));                                         //Natural: ASSIGN PEND-ANNUAL-PAYMENTS ( #I5 ) := #PENDR-MSTR-PER-PYMNT ( #I )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(pnd_I));    //Natural: ASSIGN FUND-ANNUAL-ANNLZD-UNITS ( #I5 ) := #UPEND-MSTR-ANNL-UNITS ( #I )
                pend_Annual_Annlzd_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(pnd_I));                                    //Natural: ASSIGN PEND-ANNUAL-ANNLZD-UNITS ( #I5 ) := #PENDR-MSTR-ANNL-UNITS ( #I )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(pnd_I));    //Natural: ASSIGN FUND-ANNUAL-ANNLZD-PAYTS ( #I5 ) := #UPEND-MSTR-ANNL-PYMNT ( #I )
                pend_Annual_Annlzd_Payts.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(pnd_I));                                    //Natural: ASSIGN PEND-ANNUAL-ANNLZD-PAYTS ( #I5 ) := #PENDR-MSTR-ANNL-PYMNT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD  2/04
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   END OF ADD   8/96
        //*  WRITE-PAGE-2
    }
    private void sub_Write_Page_3() throws Exception                                                                                                                      //Natural: WRITE-PAGE-3
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CREF MONTHLY FUND TOTALS                                   /* LB 10/97
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"CREF MONTHLY ACTIVE FUND RECORDS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(2),  //Natural: WRITE ( 1 ) / 1T 'CREF MONTHLY ACTIVE FUND RECORDS' 45T #TOT-ADMN-MSTR ( 2 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #TOT-PYMNT-MSTR ( 2 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CREF MONTHLY ACTIVE UNITS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(2),  //Natural: WRITE ( 1 ) 1T 'CREF MONTHLY ACTIVE UNITS' 45T #TOT-ADMN-MSTR-UNITS ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #TOT-PYMNT-MSTR-UNITS ( 2 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(2), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CREF MONTHLY ACTIVE AMOUNT",new TabSetting(44),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(2),  //Natural: WRITE ( 1 ) 1T 'CREF MONTHLY ACTIVE AMOUNT' 44T #TOT-ADMN-MSTR-PER-PYMNT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #TOT-PYMNT-MSTR-PER-PYMNT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(2), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  WRITE TOTALS FOR EACH MONTHLY CREF FUND
        //*  ADDED 2/04
        //*  #PROD-CNT
        pnd_I5.reset();                                                                                                                                                   //Natural: RESET #I5
        FOR05:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            //*  BYPASS REAL ESTATE AND ACCESS /* 9/08
            if (condition(pnd_I.equals(9) || pnd_I.equals(11)))                                                                                                           //Natural: IF #I = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_3.setValue(pnd_I);                                                                                                                              //Natural: ASSIGN #PARM-FUND-3 := #I
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF MONTHLY");                                                                                                 //Natural: ASSIGN #FUND-CREF-CON := 'CREF MONTHLY'
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            //*  CREF MONTHLY FUND TOTALS ARE STORED FROM POSSITION 20 ON THE ARRAY
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(20));                                                                                            //Natural: ASSIGN #J = #I + 20
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS  ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS  '
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T #ADMN-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS         ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'UNITS         '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 45T #ADMN-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("AMOUNT        ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'AMOUNT        '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 44T #ADMN-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_J), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   ADDED FOLLOWING  ACCUM CREF MONTHLY  FUNDS    2/04
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ALPHA-FUND-CODE
            sub_Retrieve_Alpha_Fund_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I5.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I5
            pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_J));                   //Natural: ASSIGN FUND-MONTHLY-UNITS ( #I5 ) := #UPEND-MSTR-UNITS ( #J )
            pend_Monthly_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_J));                                                   //Natural: ASSIGN PEND-MONTHLY-UNITS ( #I5 ) := #PENDR-MSTR-UNITS ( #J )
            pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_J));            //Natural: ASSIGN FUND-MONTHLY-PAYMENTS ( #I5 ) := #UPEND-MSTR-PER-PYMNT ( #J )
            pend_Monthly_Payments.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_J));                                            //Natural: ASSIGN PEND-MONTHLY-PAYMENTS ( #I5 ) := #PENDR-MSTR-PER-PYMNT ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-PAGE-3
    }
    private void sub_Write_Page_4() throws Exception                                                                                                                      //Natural: WRITE-PAGE-4
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  CREF ANNUAL FUND TOTALS                                    /* LB 10/97
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"T/L  ANNUAL ACTIVE FUND RECORDS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(3),  //Natural: WRITE ( 1 ) / 1T 'T/L  ANNUAL ACTIVE FUND RECORDS' 45T #TOT-ADMN-MSTR ( 3 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #TOT-PYMNT-MSTR ( 3 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"T/L  ANNUAL ACTIVE UNITS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(3),  //Natural: WRITE ( 1 ) 1T 'T/L  ANNUAL ACTIVE UNITS' 45T #TOT-ADMN-MSTR-UNITS ( 3 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #TOT-PYMNT-MSTR-UNITS ( 3 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(3), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        //*  7/09
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"T/L  ANNUAL ACTIVE AMOUNT",new TabSetting(44),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(3),  //Natural: WRITE ( 1 ) 1T 'T/L  ANNUAL ACTIVE AMOUNT' 44T #TOT-ADMN-MSTR-PER-PYMNT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #TOT-PYMNT-MSTR-PER-PYMNT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_I5.setValue(11);                                                                                                                                              //Natural: ASSIGN #I5 := 11
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J = #I + 40
            pnd_Parm_Fund_3.setValue(pnd_J);                                                                                                                              //Natural: ASSIGN #PARM-FUND-3 := #J
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc.equals(" ") && pnd_Cmpny_Desc.equals(" ")))                                                                                       //Natural: IF #PARM-DESC = ' ' AND #CMPNY-DESC = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS  ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS  '
            pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L  ANNUAL");                                                                                                  //Natural: ASSIGN #FUND-CREF-CON := 'T/L  ANNUAL'
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T #ADMN-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS         ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'UNITS         '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 45T #ADMN-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("AMOUNT        ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'AMOUNT        '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 44T #ADMN-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_J), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fnd_I.setValue(pnd_J);                                                                                                                                    //Natural: ASSIGN #FND-I := #J
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ALPHA-FUND-CODE
            sub_Retrieve_Alpha_Fund_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  BYPASS FREE LOOK
            //*  10/09
            //*  10/09
            //*  10/09
            //*  10/09
            //*  6/09 /* ADDED 10/09
            //*  10/09
            //*  6/09 /* ADDED 10/09
            //*  10/09
            if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.notEquals("U")))                                                                                               //Natural: IF #PARM-FUND-1 NE 'U'
            {
                pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5).setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                    //Natural: ASSIGN FUND-CODE ( #I5 ) := #PARM-FUND-1
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_J));                //Natural: ASSIGN FUND-ANNUAL-UNITS ( #I5 ) := #UPEND-MSTR-UNITS ( #J )
                pend_Annual_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_J));                                                //Natural: ASSIGN PEND-ANNUAL-UNITS ( #I5 ) := #PENDR-MSTR-UNITS ( #J )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_J));         //Natural: ASSIGN FUND-ANNUAL-PAYMENTS ( #I5 ) := #UPEND-MSTR-PER-PYMNT ( #J )
                pend_Annual_Payments.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_J));                                         //Natural: ASSIGN PEND-ANNUAL-PAYMENTS ( #I5 ) := #PENDR-MSTR-PER-PYMNT ( #J )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Units.getValue(pnd_J));    //Natural: ASSIGN FUND-ANNUAL-ANNLZD-UNITS ( #I5 ) := #UPEND-MSTR-ANNL-UNITS ( #J )
                pend_Annual_Annlzd_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Units.getValue(pnd_J));                                    //Natural: ASSIGN PEND-ANNUAL-ANNLZD-UNITS ( #I5 ) := #PENDR-MSTR-ANNL-UNITS ( #J )
                pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Annl_Pymnt.getValue(pnd_J));    //Natural: ASSIGN FUND-ANNUAL-ANNLZD-PAYTS ( #I5 ) := #UPEND-MSTR-ANNL-PYMNT ( #J )
                pend_Annual_Annlzd_Payts.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Annl_Pymnt.getValue(pnd_J));                                    //Natural: ASSIGN PEND-ANNUAL-ANNLZD-PAYTS ( #I5 ) := #PENDR-MSTR-ANNL-PYMNT ( #J )
                pnd_I5.nadd(1);                                                                                                                                           //Natural: ASSIGN #I5 := #I5 + 1
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF ADD ACCUM PA SELECT ANNUAL  FUNDS    2/04
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-PAGE-4
    }
    private void sub_Write_Page_5() throws Exception                                                                                                                      //Natural: WRITE-PAGE-5
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CREF MONTHLY FUND TOTALS                                   /* LB 10/97
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"T/L MONTHLY ACTIVE FUND RECORDS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr.getValue(4),  //Natural: WRITE ( 1 ) / 1T 'T/L MONTHLY ACTIVE FUND RECORDS' 45T #TOT-ADMN-MSTR ( 4 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #TOT-PYMNT-MSTR ( 4 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"T/L MONTHLY ACTIVE UNITS",new TabSetting(45),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Units.getValue(4),  //Natural: WRITE ( 1 ) 1T 'T/L MONTHLY ACTIVE UNITS' 45T #TOT-ADMN-MSTR-UNITS ( 4 ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #TOT-PYMNT-MSTR-UNITS ( 4 ) ( EM = ZZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Units.getValue(4), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        //*  ADDED 2/04 FOR ACCUM ACTUARY TOTALS    /* 7/09
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"T/L MONTHLY ACTIVE AMOUNT",new TabSetting(44),pnd_Annual_Monthly_Totals_Pnd_Tot_Admn_Mstr_Per_Pymnt.getValue(4),  //Natural: WRITE ( 1 ) 1T 'T/L MONTHLY ACTIVE AMOUNT' 44T #TOT-ADMN-MSTR-PER-PYMNT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #TOT-PYMNT-MSTR-PER-PYMNT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Annual_Monthly_Totals_Pnd_Tot_Pymnt_Mstr_Per_Pymnt.getValue(4), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_I5.setValue(11);                                                                                                                                              //Natural: ASSIGN #I5 := 11
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J = #I + 40
            pnd_Parm_Fund_3.setValue(pnd_J);                                                                                                                              //Natural: ASSIGN #PARM-FUND-3 := #J
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc.equals(" ") && pnd_Cmpny_Desc.equals(" ")))                                                                                       //Natural: IF #PARM-DESC = ' ' AND #CMPNY-DESC = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            //*  CREF MONTHLY FUND TOTALS ARE STORED FROM POSSITION 60 ON THE ARRAY
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(60));                                                                                            //Natural: ASSIGN #J = #I + 60
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS  ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS  '
            pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L MONTHLY");                                                                                                  //Natural: ASSIGN #FUND-CREF-CON := 'T/L MONTHLY'
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T #ADMN-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 ) 65T #PYMNT-MSTR ( #J ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS         ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'UNITS         '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 45T #ADMN-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 ) 65T #PYMNT-MSTR-UNITS ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999 )
                TabSetting(45),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Units.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"),new TabSetting(65),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Units.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("AMOUNT        ");                                                                                                    //Natural: ASSIGN #FUND-TXT := 'AMOUNT        '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 44T #ADMN-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 64T #PYMNT-MSTR-PER-PYMNT ( #J ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                TabSetting(44),pnd_Final_Totals_Report_Pnd_Admn_Mstr_Per_Pymnt.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),pnd_Final_Totals_Report_Pnd_Pymnt_Mstr_Per_Pymnt.getValue(pnd_J), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fnd_I.compute(new ComputeParameters(false, pnd_Fnd_I), pnd_I.add(40));                                                                                    //Natural: ASSIGN #FND-I := #I + 40
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ALPHA-FUND-CODE
            sub_Retrieve_Alpha_Fund_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  BYPASS FREE LOOK
            //*  10/09
            //*  10/09
            //*  10/09
            //*  10/09
            if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.notEquals("U")))                                                                                               //Natural: IF #PARM-FUND-1 NE 'U'
            {
                pdaAiaa093.getAian093_Linkage_Fund_Code().getValue(pnd_I5).setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                    //Natural: ASSIGN FUND-CODE ( #I5 ) := #PARM-FUND-1
                pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Units.getValue(pnd_J));               //Natural: ASSIGN FUND-MONTHLY-UNITS ( #I5 ) := #UPEND-MSTR-UNITS ( #J )
                pend_Monthly_Units.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Units.getValue(pnd_J));                                               //Natural: ASSIGN PEND-MONTHLY-UNITS ( #I5 ) := #PENDR-MSTR-UNITS ( #J )
                pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments().getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Upend_Mstr_Per_Pymnt.getValue(pnd_J));        //Natural: ASSIGN FUND-MONTHLY-PAYMENTS ( #I5 ) := #UPEND-MSTR-PER-PYMNT ( #J )
                pend_Monthly_Payments.getValue(pnd_I5).setValue(pnd_Final_Totals_Report_Pnd_Pendr_Mstr_Per_Pymnt.getValue(pnd_J));                                        //Natural: ASSIGN PEND-MONTHLY-PAYMENTS ( #I5 ) := #PENDR-MSTR-PER-PYMNT ( #J )
                pnd_I5.nadd(1);                                                                                                                                           //Natural: ASSIGN #I5 := #I5 + 1
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF ADD ACCUM PA SELECT ANNUAL  FUNDS    2/04
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-PAGE-5
    }
    private void sub_Accum_Rollover_Contracts() throws Exception                                                                                                          //Natural: ACCUM-ROLLOVER-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ADDED FOLLOWING 10/01
        if (condition(pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 28 OR = 30
        {
            if (condition(pnd_Save_Dist.equals("RINV")))                                                                                                                  //Natural: IF #SAVE-DIST = 'RINV'
            {
                pnd_Total_Rinv_Fnd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-RINV-FND
                pnd_Total_Rinv_Pmt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL-RINV-PMT
                pnd_Total_Rinv_Div.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                             //Natural: ADD #SUMM-PER-DVDND TO #TOTAL-RINV-DIV
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_Total_Rinv_Fnd_Due.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-RINV-FND-DUE
                    pnd_Total_Rinv_Pmt_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                     //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL-RINV-PMT-DUE
                    pnd_Total_Rinv_Div_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                     //Natural: ADD #SUMM-PER-DVDND TO #TOTAL-RINV-DIV-DUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 28 OR = 30
        {
            if (condition(pnd_Save_Dist.equals("RINV")))                                                                                                                  //Natural: IF #SAVE-DIST = 'RINV'
            {
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                           //Natural: IF #CNTRCT-PAYEE-CDE = 1
                {
                    pnd_Tpa_Pnd_Fund_1.getValue(5).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-1 ( 5 )
                    pnd_Tpa_Pnd_Pay_Amt_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( 5 )
                    pnd_Tpa_Pnd_Div_Amt_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( 5 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_1.getValue(5).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-1 ( 5 )
                        pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-1 ( 5 )
                        pnd_Tpa_Pnd_Div_Amt_P_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-1 ( 5 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                          //Natural: IF #CNTRCT-PAYEE-CDE > 1
                {
                    pnd_Tpa_Pnd_Fund_2.getValue(5).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-2 ( 5 )
                    pnd_Tpa_Pnd_Pay_Amt_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( 5 )
                    pnd_Tpa_Pnd_Div_Amt_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( 5 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_2.getValue(5).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-2 ( 5 )
                        pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-2 ( 5 )
                        pnd_Tpa_Pnd_Div_Amt_P_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-2 ( 5 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ************************************************************
        if (condition(pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 28 OR = 30
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                pnd_T_Tpa_Roll_Fnd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #T-TPA-ROLL-FND
                pnd_T_Tpa_Roll_Pmt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #T-TPA-ROLL-PMT
                pnd_T_Tpa_Roll_Div.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                             //Natural: ADD #SUMM-PER-DVDND TO #T-TPA-ROLL-DIV
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_T_Tpa_Roll_Due.nadd(1);                                                                                                                           //Natural: ADD 1 TO #T-TPA-ROLL-DUE
                    pnd_T_Tpa_Roll_Pmt_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                     //Natural: ADD #SUMM-PER-PYMNT TO #T-TPA-ROLL-PMT-DUE
                    pnd_T_Tpa_Roll_Div_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                     //Natural: ADD #SUMM-PER-DVDND TO #T-TPA-ROLL-DIV-DUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Optn_Cde.equals(28) || pnd_Save_Optn_Cde.equals(30)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 28 OR = 30
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                           //Natural: IF #CNTRCT-PAYEE-CDE = 1
                {
                    pnd_Tpa_Pnd_Fund_1.getValue(6).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-1 ( 6 )
                    pnd_Tpa_Pnd_Pay_Amt_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( 6 )
                    pnd_Tpa_Pnd_Div_Amt_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( 6 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_1.getValue(6).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-1 ( 6 )
                        pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-1 ( 6 )
                        pnd_Tpa_Pnd_Div_Amt_P_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-1 ( 6 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                          //Natural: IF #CNTRCT-PAYEE-CDE > 1
                {
                    pnd_Tpa_Pnd_Fund_2.getValue(6).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-2 ( 6 )
                    pnd_Tpa_Pnd_Pay_Amt_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( 6 )
                    pnd_Tpa_Pnd_Div_Amt_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( 6 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_2.getValue(6).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-2 ( 6 )
                        pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-2 ( 6 )
                        pnd_Tpa_Pnd_Div_Amt_P_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-2 ( 6 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************************************************
        if (condition(pnd_Save_Optn_Cde.equals(25) || pnd_Save_Optn_Cde.equals(27)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 25 OR = 27
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                pnd_T_Ipr_Roll_Fnd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #T-IPR-ROLL-FND
                pnd_T_Ipr_Roll_Pmt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #T-IPR-ROLL-PMT
                pnd_T_Ipr_Roll_Div.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                             //Natural: ADD #SUMM-PER-DVDND TO #T-IPR-ROLL-DIV
                pnd_T_Ipr_Roll_Fin.nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                                             //Natural: ADD #SUMM-FIN-PYMNT TO #T-IPR-ROLL-FIN
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_T_Ipr_Roll_Due.nadd(1);                                                                                                                           //Natural: ADD 1 TO #T-IPR-ROLL-DUE
                    pnd_T_Ipr_Roll_Pmt_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                     //Natural: ADD #SUMM-PER-PYMNT TO #T-IPR-ROLL-PMT-DUE
                    pnd_T_Ipr_Roll_Div_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                     //Natural: ADD #SUMM-PER-DVDND TO #T-IPR-ROLL-DIV-DUE
                    pnd_T_Ipr_Roll_Fin_Due.nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                                     //Natural: ADD #SUMM-FIN-PYMNT TO #T-IPR-ROLL-FIN-DUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Optn_Cde.equals(25) || pnd_Save_Optn_Cde.equals(27)))                                                                                      //Natural: IF #SAVE-OPTN-CDE = 25 OR = 27
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                           //Natural: IF #CNTRCT-PAYEE-CDE = 1
                {
                    pnd_Tpa_Pnd_Fund_1.getValue(7).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-1 ( 7 )
                    pnd_Tpa_Pnd_Pay_Amt_1.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( 7 )
                    pnd_Tpa_Pnd_Div_Amt_1.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( 7 )
                    pnd_Tpa_Pnd_Fin_Div_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                          //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-1 ( 5 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_1.getValue(7).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-1 ( 7 )
                        pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-1 ( 7 )
                        pnd_Tpa_Pnd_Div_Amt_P_1.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-1 ( 7 )
                        pnd_Tpa_Pnd_Fin_Div_P_1.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                    //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-P-1 ( 5 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                          //Natural: IF #CNTRCT-PAYEE-CDE > 1
                {
                    pnd_Tpa_Pnd_Fund_2.getValue(7).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-2 ( 7 )
                    pnd_Tpa_Pnd_Pay_Amt_2.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( 7 )
                    pnd_Tpa_Pnd_Div_Amt_2.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( 7 )
                    pnd_Tpa_Pnd_Fin_Div_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                          //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-2 ( 5 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_2.getValue(7).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-2 ( 7 )
                        pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-2 ( 7 )
                        pnd_Tpa_Pnd_Div_Amt_P_2.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-2 ( 7 )
                        pnd_Tpa_Pnd_Fin_Div_P_2.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                    //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-P-2 ( 5 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************************************
        if (condition(pnd_Save_Optn_Cde.equals(22)))                                                                                                                      //Natural: IF #SAVE-OPTN-CDE = 22
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                pnd_T_P_I_Roll_Fnd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #T-P-I-ROLL-FND
                pnd_T_P_I_Roll_Pmt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                             //Natural: ADD #SUMM-PER-PYMNT TO #T-P-I-ROLL-PMT
                pnd_T_P_I_Roll_Div.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                             //Natural: ADD #SUMM-PER-DVDND TO #T-P-I-ROLL-DIV
                pnd_T_P_I_Roll_Fin.nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                                             //Natural: ADD #SUMM-FIN-PYMNT TO #T-P-I-ROLL-FIN
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_T_P_I_Roll_Due.nadd(1);                                                                                                                           //Natural: ADD 1 TO #T-P-I-ROLL-DUE
                    pnd_T_P_I_Roll_Pmt_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                     //Natural: ADD #SUMM-PER-PYMNT TO #T-P-I-ROLL-PMT-DUE
                    pnd_T_P_I_Roll_Div_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                     //Natural: ADD #SUMM-PER-DVDND TO #T-P-I-ROLL-DIV-DUE
                    pnd_T_P_I_Roll_Fin_Due.nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                                     //Natural: ADD #SUMM-FIN-PYMNT TO #T-P-I-ROLL-FIN-DUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Optn_Cde.equals(22)))                                                                                                                      //Natural: IF #SAVE-OPTN-CDE = 22
        {
            //* ADDED 6/03
            if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                    //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
            {
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                           //Natural: IF #CNTRCT-PAYEE-CDE = 1
                {
                    pnd_Tpa_Pnd_Fund_1.getValue(8).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-1 ( 8 )
                    pnd_Tpa_Pnd_Pay_Amt_1.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( 8 )
                    pnd_Tpa_Pnd_Div_Amt_1.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( 8 )
                    pnd_Tpa_Pnd_Fin_Div_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                          //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-1 ( 6 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_1.getValue(8).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-1 ( 8 )
                        pnd_Tpa_Pnd_Pay_Amt_P_1.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-1 ( 8 )
                        pnd_Tpa_Pnd_Div_Amt_P_1.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-1 ( 8 )
                        pnd_Tpa_Pnd_Fin_Div_P_1.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                    //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-P-1 ( 6 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                          //Natural: IF #CNTRCT-PAYEE-CDE > 1
                {
                    pnd_Tpa_Pnd_Fund_2.getValue(8).nadd(1);                                                                                                               //Natural: ADD 1 TO #FUND-2 ( 8 )
                    pnd_Tpa_Pnd_Pay_Amt_2.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( 8 )
                    pnd_Tpa_Pnd_Div_Amt_2.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                          //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( 8 )
                    pnd_Tpa_Pnd_Fin_Div_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                          //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-2 ( 6 )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_Tpa_Pnd_Fund_P_2.getValue(6).nadd(1);                                                                                                         //Natural: ADD 1 TO #FUND-P-2 ( 6 )
                        pnd_Tpa_Pnd_Pay_Amt_P_2.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                    //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-P-2 ( 8 )
                        pnd_Tpa_Pnd_Div_Amt_P_2.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                    //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-P-2 ( 8 )
                        pnd_Tpa_Pnd_Fin_Div_P_2.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                    //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-DIV-P-2 ( 6 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1 '") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1G'")                    //Natural: IF #SUMM-FUND-CDE = MASK ( '1 ' ) OR = MASK ( '1G' ) OR = MASK ( '1S' )
            || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1S'")))
        {
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                              //Natural: IF #SAVE-OPTN-CDE = 21
                {
                    //* ADDED 6/03
                    if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")            //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                        || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
                    {
                        pnd_T_Ac_Roll_Fnd.nadd(1);                                                                                                                        //Natural: ADD 1 TO #T-AC-ROLL-FND
                        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                      //Natural: IF #PAYMENT-DUE
                        {
                            pnd_T_Ac_Roll_Due.nadd(1);                                                                                                                    //Natural: ADD 1 TO #T-AC-ROLL-DUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4")  //Natural: IF #SUMM-CMPNY-CDE = '2' OR = 'U' OR = '4' OR = 'W'
            || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))
        {
            if (condition(pnd_New_Payee.getBoolean()))                                                                                                                    //Natural: IF #NEW-PAYEE
            {
                if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                              //Natural: IF #SAVE-OPTN-CDE = 21
                {
                    //* ADDED 6/03
                    if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")            //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                        || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
                    {
                        pnd_T_Ac_Roll_Fnd_V.nadd(1);                                                                                                                      //Natural: ADD 1 TO #T-AC-ROLL-FND-V
                        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                      //Natural: IF #PAYMENT-DUE
                        {
                            pnd_T_Ac_Roll_Due_V.nadd(1);                                                                                                                  //Natural: ADD 1 TO #T-AC-ROLL-DUE-V
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #NEW-PAYEE
        if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1 '") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1G'")                    //Natural: IF #SUMM-FUND-CDE = MASK ( '1 ' ) OR = MASK ( '1G' ) OR = MASK ( '1S' )
            || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Summ_Fund_Cde,"'1S'")))
        {
            if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                                  //Natural: IF #SAVE-OPTN-CDE = 21
            {
                //* ADDED 6/03
                if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                    || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
                {
                    pnd_T_Ac_Roll_Pmt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #T-AC-ROLL-PMT
                    pnd_T_Ac_Roll_Div.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                          //Natural: ADD #SUMM-PER-DVDND TO #T-AC-ROLL-DIV
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_T_Ac_Roll_Pmt_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                  //Natural: ADD #SUMM-PER-PYMNT TO #T-AC-ROLL-PMT-DUE
                        pnd_T_Ac_Roll_Div_Due.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                  //Natural: ADD #SUMM-PER-DVDND TO #T-AC-ROLL-DIV-DUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LEFT
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U")))                                                //Natural: IF #SUMM-CMPNY-CDE = '2' OR = 'U'
        {
            if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                                  //Natural: IF #SAVE-OPTN-CDE = 21
            {
                //* ADDED 6/03
                if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                    || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
                {
                    pnd_T_Ac_Roll_Pmt_V.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ADD #SUMM-PER-PYMNT TO #T-AC-ROLL-PMT-V
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_T_Ac_Roll_Pmt_Due_V.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                //Natural: ADD #SUMM-PER-PYMNT TO #T-AC-ROLL-PMT-DUE-V
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))                                                //Natural: IF #SUMM-CMPNY-CDE = '4' OR = 'W'
        {
            if (condition(pnd_Save_Optn_Cde.equals(21)))                                                                                                                  //Natural: IF #SAVE-OPTN-CDE = 21
            {
                //* ADDED 6/03
                if (condition(pnd_Save_Dist.equals("IRAT") || pnd_Save_Dist.equals("IRAC") || pnd_Save_Dist.equals("03BT") || pnd_Save_Dist.equals("03BC")                //Natural: IF #SAVE-DIST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = '57BT' OR = '57BC' OR = 'QPLT' OR = 'QPLC'
                    || pnd_Save_Dist.equals("57BT") || pnd_Save_Dist.equals("57BC") || pnd_Save_Dist.equals("QPLT") || pnd_Save_Dist.equals("QPLC")))
                {
                    pnd_T_Ac_Roll_Pmt_V.nadd(pnd_Monthly_Amount);                                                                                                         //Natural: ADD #MONTHLY-AMOUNT TO #T-AC-ROLL-PMT-V
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        pnd_T_Ac_Roll_Pmt_Due_V.nadd(pnd_Monthly_Amount);                                                                                                 //Natural: ADD #MONTHLY-AMOUNT TO #T-AC-ROLL-PMT-DUE-V
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADDED  10/01
    }
    private void sub_Retrieve_Alpha_Fund_Code() throws Exception                                                                                                          //Natural: RETRIEVE-ALPHA-FUND-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.setValue(" ");                                                                                                                   //Natural: ASSIGN #PARM-FUND-1 := ' '
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_2.setValue(pnd_Fnd_I);                                                                                                             //Natural: ASSIGN #PARM-FUND-2 := #FND-I
        //*  CONVERT CODE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Iaan0501_Pda_Pnd_Parm_Fund_1, pnd_Iaan0501_Pda_Pnd_Parm_Fund_2, pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde); //Natural: CALLNAT 'IAAN0511' #PARM-FUND-1 #PARM-FUND-2 #PARM-RTN-CDE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde.notEquals(getZero())))                                                                                            //Natural: IF #PARM-RTN-CDE NE 0
        {
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, " ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511");                                                                                   //Natural: WRITE ' ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511'
            if (Global.isEscape()) return;
            getReports().write(0, "PARM RETURN CODE ",pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde," FOR NUMERIC FUND-CODE->",pnd_I);                                                //Natural: WRITE 'PARM RETURN CODE ' #PARM-RTN-CDE ' FOR NUMERIC FUND-CODE->' #I
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Iaan0500_Read_Extern_File() throws Exception                                                                                                    //Natural: CALL-IAAN0500-READ-EXTERN-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READS EXTERNALIZATION FILE FOR VALID PRODUCT CODES
        //*  GET TIAA & CREF ACTIVE PRODUCTS
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Prod_Counter_Pa.setValue(pdaIaaa051z.getIaaa051z_Pnd_Pa_Sel_Cnt());                                                                                           //Natural: ASSIGN #PROD-COUNTER-PA := IAAA051Z.#PA-SEL-CNT
        pnd_Max_Prd_Cde.setValue(pdaIaaa051z.getIaaa051z_Pnd_Cref_Cnt());                                                                                                 //Natural: ASSIGN #MAX-PRD-CDE := IAAA051Z.#CREF-CNT
        //*  CALL-IAAN0500-READ-EXTERN-FILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM ",Global.getPROGRAM(),new TabSetting(35),pnd_Page_Headings_Pnd_Page_Heading1,pnd_Payment_Due_Dte,new  //Natural: WRITE ( 1 ) NOTITLE 1T 'PROGRAM ' *PROGRAM 35T #PAGE-HEADING1 #PAYMENT-DUE-DTE 102T 'PAGE ' *PAGE-NUMBER ( 1 ) ( AD = L )
                        TabSetting(102),"PAGE ",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),       //Natural: WRITE ( 1 ) 1T 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 35T #PAGE-HEADING2
                        pnd_Page_Headings_Pnd_Page_Heading2);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"FINAL TOTALS",new TabSetting(45),"IA ADMIN MASTER",new TabSetting(68),                  //Natural: WRITE ( 1 ) 1T 'FINAL TOTALS' 45T 'IA ADMIN MASTER' 68T 'PAYMENTS DUE'
                        "PAYMENTS DUE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  CYCLE' #PAYMENT-DUE-DTE
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM ",Global.getPROGRAM(),new TabSetting(45),"IA ADMINISTRATION CONTROL SUMMARY",new  //Natural: WRITE ( 2 ) NOTITLE 1T 'PROGRAM ' *PROGRAM 45T 'IA ADMINISTRATION CONTROL SUMMARY' 102T 'PAGE ' *PAGE-NUMBER ( 2 ) ( AD = L )
                        TabSetting(102),"PAGE ",getReports().getPageNumberDbs(2), new FieldAttributes ("AD=L"));
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),       //Natural: WRITE ( 2 ) 1T 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 35T 'AUV COLLECTION UPDATE FOR PAYMENT DATE' #PAYMENT-DUE-DTE
                        "AUV COLLECTION UPDATE FOR PAYMENT DATE",pnd_Payment_Due_Dte);
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"FOR CONTRACT #: ",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,pnd_Input_Record_Pnd_Cntrct_Payee_Cde,NEWLINE);                  //Natural: WRITE NOHDR 'FOR CONTRACT #: ' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56");
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
        Global.format(3, "LS=133 PS=56");

        getReports().setDisplayColumns(2, ReportOption.NOTITLE,"Fnd/cde",
        		pdaAiaa093.getAian093_Linkage_Fund_Code(),"/Annualized Units",
        		pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Units(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Annual-Units",
        		pdaAiaa093.getAian093_Linkage_Fund_Annual_Units(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Annualized Pmt",
        		pdaAiaa093.getAian093_Linkage_Fund_Annual_Annlzd_Payts(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),"/Annual-pmt",
        		pdaAiaa093.getAian093_Linkage_Fund_Annual_Payments(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),"/Mnthly-Units",
        		pdaAiaa093.getAian093_Linkage_Fund_Monthly_Units(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),"/Mnthly-pmt",
        		pdaAiaa093.getAian093_Linkage_Fund_Monthly_Payments(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
    }
}
