/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:42 PM
**        * FROM NATURAL PROGRAM : Fcpp105
************************************************************
**        * FILE NAME            : Fcpp105.java
**        * CLASS NAME           : Fcpp105
**        * INSTANCE NAME        : Fcpp105
************************************************************
************************************************************************
* PROGRAM   : FCPP105
* SYSTEM    : CPS
* TITLE     : NET CHANGE LETTER PROCESSING - OBTAIN PIN# VIA CORE FILE
* GENERATED :
* FUNCTION  : GET PIN # FOR GIVING PPCN-PAYEE VIA CORE FILE
*           : GENERATE A WORK FILE AND PRODUCE A ERROR REPORT CONTAINS
*           : RECORD WITH MISSING PIN#
*           :
* HISTORY   : CREATED ON 03/14/97
* 03/18/02  : ADD NEW FIELD OPTION CODE(FROM IA) AND REJECT IPRO, TPA
*           : CONTRACTS.
* 05/23/02  : ROXAN CARREON.  FOR NETCHANGE, USE SYSTEM DATE TO VERIFY
*           : RUN SCHEDULE.
* 03/22/13  : J BREMER   RBE PROJECT. INCREASE RATES FROM 99 OCC TO 250
* 04/2015   : J.OSTEEN   COR/NAS SUNSET.                     /* JWO1
* 08/2016   : F.ENDAYA   COR/NAS SUNSET.  FE201608
* 04/2017   : SAI K      PIN EXPANSION
* 01/15/2020: CTS        CTS CPS SUNSET (CHG694525) TAG: CTS-0115
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp105 extends BLNatBase
{
    // Data Areas
    private PdaFcpa105 pdaFcpa105;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Out_Rec;
    private DbsField pnd_Out_Rec_Pnd_Pin;
    private DbsField pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Out_Rec_Pnd_Cntrct_Payee_Cde;

    private DbsGroup pnd_Trn_Fund_Rec;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_1;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_2;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_3;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_4;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf;
    private DbsField pnd_Valid_Rec_Cnt;
    private DbsField pnd_Invalid_Rec_Cnt;
    private DbsField pnd_Ws_Date;

    private DbsGroup pnd_Ws_Date__R_Field_5;
    private DbsField pnd_Ws_Date_Pnd_Ws_Yyyy;
    private DbsField pnd_Ws_Date_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Date_Pnd_Ws_Dd;
    private DbsField pnd_Ctrl_Record;

    private DbsGroup pnd_Ctrl_Record__R_Field_6;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Parm_Type;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Date;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Proc_Month;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Cntrct_Payee_Key;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_7;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr_N;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee;
    private DbsField pnd_Wk_Ppcn_Inv_Orgn_Prcss_Inst;
    private DbsField pnd_Sv_Pin;
    private DbsField pnd_Sv_Payee;
    private DbsField pnd_Cntr_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa105 = new PdaFcpa105(localVariables);

        // Local Variables

        pnd_Out_Rec = localVariables.newGroupInRecord("pnd_Out_Rec", "#OUT-REC");
        pnd_Out_Rec_Pnd_Pin = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Out_Rec_Pnd_Cntrct_Payee_Cde = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);

        pnd_Trn_Fund_Rec = localVariables.newGroupInRecord("pnd_Trn_Fund_Rec", "#TRN-FUND-REC");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde", "#TRN-REC-CDE", FieldType.STRING, 3);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type", "#TRN-FUND-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate", "#TRN-BLANK-RATE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type", "#TRN-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr", "#TRN-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde", "#TRN-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde", "#TRN-CMPNY-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Trn_Fund_Rec__R_Field_1 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_1", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde = pnd_Trn_Fund_Rec__R_Field_1.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde", "#TRN-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde = pnd_Trn_Fund_Rec__R_Field_1.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde", "#TRN-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt", "#TRN-RATE-CNT", FieldType.NUMERIC, 
            3);

        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data", "#TRN-RATE-DATA");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde", "#TRN-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic", "#TRN-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt", "#TRN-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt", "#TRN-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt", "#TRN-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt", "#TRN-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt", "#TRN-OLD-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt", "#TRN-OLD-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt", "#TRN-NEW-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt", "#TRN-NEW-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Trn_Fund_Rec__R_Field_2 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_2", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);
        pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees = pnd_Trn_Fund_Rec__R_Field_2.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees", "#TRL-REA-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code", "#TRN-OPTION-CODE", FieldType.NUMERIC, 
            2);

        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info", "#TRN-TRL-INFO");
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees", "#TRN-OLD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees", "#TRN-OLD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt", "#TRN-OLD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt", "#TRN-OLD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt", "#TRN-OLD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt", "#TRN-OLD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees", "#TRN-NEW-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees", "#TRN-NEW-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt", "#TRN-NEW-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt", "#TRN-NEW-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt", "#TRN-NEW-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt", "#TRN-NEW-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trn_Fund_Rec__R_Field_3 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_3", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info);
        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf = pnd_Trn_Fund_Rec__R_Field_3.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf", "#TRN-TRL-INFO-REDF", 
            FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Trn_Fund_Rec__R_Field_4 = localVariables.newGroupInRecord("pnd_Trn_Fund_Rec__R_Field_4", "REDEFINE", pnd_Trn_Fund_Rec);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf = pnd_Trn_Fund_Rec__R_Field_4.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf", "#TRN-FUND-REC-REDF", 
            FieldType.STRING, 1, new DbsArrayController(1, 1000));
        pnd_Valid_Rec_Cnt = localVariables.newFieldInRecord("pnd_Valid_Rec_Cnt", "#VALID-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Invalid_Rec_Cnt = localVariables.newFieldInRecord("pnd_Invalid_Rec_Cnt", "#INVALID-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 8);

        pnd_Ws_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_5", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Yyyy = pnd_Ws_Date__R_Field_5.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Yyyy", "#WS-YYYY", FieldType.NUMERIC, 4);
        pnd_Ws_Date_Pnd_Ws_Mm = pnd_Ws_Date__R_Field_5.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Mm", "#WS-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Date_Pnd_Ws_Dd = pnd_Ws_Date__R_Field_5.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Dd", "#WS-DD", FieldType.NUMERIC, 2);
        pnd_Ctrl_Record = localVariables.newFieldInRecord("pnd_Ctrl_Record", "#CTRL-RECORD", FieldType.STRING, 10);

        pnd_Ctrl_Record__R_Field_6 = localVariables.newGroupInRecord("pnd_Ctrl_Record__R_Field_6", "REDEFINE", pnd_Ctrl_Record);
        pnd_Ctrl_Record_Pnd_Cr_Parm_Type = pnd_Ctrl_Record__R_Field_6.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Parm_Type", "#CR-PARM-TYPE", FieldType.STRING, 
            1);
        pnd_Ctrl_Record_Pnd_Cr_Run_Date = pnd_Ctrl_Record__R_Field_6.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Date", "#CR-RUN-DATE", FieldType.STRING, 
            8);
        pnd_Ctrl_Record_Pnd_Cr_Proc_Month = pnd_Ctrl_Record__R_Field_6.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Proc_Month", "#CR-PROC-MONTH", FieldType.STRING, 
            1);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Cntrct_Payee_Key = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Payee_Key", "CNTRCT-PAYEE-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_KEY");
        cpr_Cntrct_Payee_Key.setSuperDescriptor(true);
        registerRecord(vw_cpr);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_7", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr_N = pnd_Cntrct_Payee_Key__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr_N", "#CNTRCT-PPCN-NBR-N", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee = pnd_Cntrct_Payee_Key__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Wk_Ppcn_Inv_Orgn_Prcss_Inst = localVariables.newFieldInRecord("pnd_Wk_Ppcn_Inv_Orgn_Prcss_Inst", "#WK-PPCN-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29);
        pnd_Sv_Pin = localVariables.newFieldInRecord("pnd_Sv_Pin", "#SV-PIN", FieldType.NUMERIC, 12);
        pnd_Sv_Payee = localVariables.newFieldInRecord("pnd_Sv_Payee", "#SV-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cntr_Found = localVariables.newFieldInRecord("pnd_Cntr_Found", "#CNTR-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cpr.reset();

        localVariables.reset();
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.setInitialValue("DET");
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.setInitialValue("N");
        pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate.setInitialValue("N");
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp105() throws Exception
    {
        super("Fcpp105");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp105|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*  CHECK FOR ANNUITY CERTAIN RUN
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ctrl_Record);                                                                                      //Natural: INPUT #CTRL-RECORD
                //*  ROXAN 05-23-02
                pnd_Ws_Date.setValue(Global.getDATN());                                                                                                                   //Natural: MOVE *DATN TO #WS-DATE
                if (condition(pnd_Ws_Date_Pnd_Ws_Mm.notEquals(3)))                                                                                                        //Natural: IF #WS-MM NE 3
                {
                    getReports().write(0, "****************************************************",NEWLINE,"*                                                  *",          //Natural: WRITE '****************************************************' / '*                                                  *' / '*                                                  *' / '*                                                  *' / '*    PROGRAM WAS NOT SCHEDULED TO RUN THIS MONTH   *' / '*                                                  *' / '*                                                  *' / '*                                                  *' / '****************************************************'
                        NEWLINE,"*                                                  *",NEWLINE,"*                                                  *",NEWLINE,
                        "*    PROGRAM WAS NOT SCHEDULED TO RUN THIS MONTH   *",NEWLINE,"*                                                  *",NEWLINE,"*                                                  *",
                        NEWLINE,"*                                                  *",NEWLINE,"****************************************************");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(0);  if (true) return;                                                                                                              //Natural: TERMINATE 0
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 #TRN-FUND-REC
                while (condition(getWorkFiles().read(1, pnd_Trn_Fund_Rec)))
                {
                    //*  NEW 3/18/2002
                    if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(25) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(27) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(28)  //Natural: IF #TRN-OPTION-CODE = 25 OR = 27 OR = 28 OR = 30
                        || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(30)))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Out_Rec.reset();                                                                                                                                  //Natural: RESET #OUT-REC #P-VARS
                    pdaFcpa105.getPnd_P_Vars().reset();
                    if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.equals("DET")))                                                                                        //Natural: IF #TRN-REC-CDE = 'DET'
                    {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-PIN-FROM-CORE
                        sub_Obtain_Pin_From_Core();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getWorkFiles().write(2, false, pnd_Out_Rec);                                                                                                      //Natural: WRITE WORK FILE 2 #OUT-REC
                        if (condition(pnd_Out_Rec_Pnd_Pin.equals(getZero())))                                                                                             //Natural: IF #OUT-REC.#PIN = 0
                        {
                            pnd_Invalid_Rec_Cnt.nadd(1);                                                                                                                  //Natural: ADD 1 TO #INVALID-REC-CNT
                            getReports().display(2, "CONTRACT#",                                                                                                          //Natural: DISPLAY ( 2 ) 'CONTRACT#' #OUT-REC.#CNTRCT-PPCN-NBR 'PAYEE' #OUT-REC.#CNTRCT-PAYEE-CDE 'PIN#' #OUT-REC.#PIN
                            		pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
                            		pnd_Out_Rec_Pnd_Cntrct_Payee_Cde,"PIN#",
                            		pnd_Out_Rec_Pnd_Pin);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Valid_Rec_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #VALID-REC-CNT
                            getReports().display(1, "CONTRACT#",                                                                                                          //Natural: DISPLAY ( 1 ) 'CONTRACT#' #OUT-REC.#CNTRCT-PPCN-NBR 'PAYEE' #OUT-REC.#CNTRCT-PAYEE-CDE 'PIN#' #OUT-REC.#PIN
                            		pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
                            		pnd_Out_Rec_Pnd_Cntrct_Payee_Cde,"PIN#",
                            		pnd_Out_Rec_Pnd_Pin);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"TOTAL NUMBER OF RECORDS:",pnd_Valid_Rec_Cnt);                                                                 //Natural: WRITE ( 1 ) 'TOTAL NUMBER OF RECORDS:' #VALID-REC-CNT
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"TOTAL NUMBER OF RECORDS:",pnd_Invalid_Rec_Cnt);                                                               //Natural: WRITE ( 2 ) 'TOTAL NUMBER OF RECORDS:' #INVALID-REC-CNT
                if (Global.isEscape()) return;
                //* *------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-PIN-FROM-CORE
                //* *------------------------------------
                //*  RF. READ     CPS-PYMNT WITH PPCN-INV-ORGN-PRCSS-INST
                //*                EQ       #WK-PPCN-INV-ORGN-PRCSS-INST
                //*   IF #P-VARS.#CNTRCT-PPCN-NBR NE RF.CNTRCT-PPCN-NBR
                //*     ESCAPE BOTTOM
                //*   END-IF
                //*   IF #P-VARS.#CNTRCT-PAYEE-CDE NE SUBSTR(RF.CNTRCT-PAYEE-CDE,1,2)
                //*     #CNTR-FOUND  := TRUE
                //*     IF #SV-PAYEE NE RF.CNTRCT-PAYEE-CDE
                //*       #SV-PIN   := RF.CNTRCT-UNQ-ID-NBR
                //*       #SV-PAYEE := RF.CNTRCT-PAYEE-CDE
                //*     END-IF
                //*     ESCAPE TOP
                //*   END-IF
                //*   #P-VARS.#PIN  := RF.CNTRCT-UNQ-ID-NBR
                //*   ESCAPE BOTTOM
                //*  END-READ
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Obtain_Pin_From_Core() throws Exception                                                                                                              //Natural: OBTAIN-PIN-FROM-CORE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa105.getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr);                                                                //Natural: ASSIGN #P-VARS.#CNTRCT-PPCN-NBR := #TRN-CNTRCT-PPCN-NBR
        pdaFcpa105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde_N().setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde);                                                            //Natural: ASSIGN #P-VARS.#CNTRCT-PAYEE-CDE-N := #TRN-CNTRCT-PAYEE-CDE
        pdaFcpa105.getPnd_P_Vars_Pnd_Get_Pin().setValue(true);                                                                                                            //Natural: MOVE TRUE TO #P-VARS.#GET-PIN
        //*  CALLNAT 'FCPN105' #P-VARS                  /* FE201608 START
        pdaFcpa105.getPnd_P_Vars_Pnd_Pin().reset();                                                                                                                       //Natural: RESET #P-VARS.#PIN #WK-PPCN-INV-ORGN-PRCSS-INST #SV-PIN #SV-PAYEE
        pnd_Wk_Ppcn_Inv_Orgn_Prcss_Inst.reset();
        pnd_Sv_Pin.reset();
        pnd_Sv_Payee.reset();
        pnd_Cntr_Found.setValue(false);                                                                                                                                   //Natural: ASSIGN #CNTR-FOUND := FALSE
        setValueToSubstring(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr,pnd_Wk_Ppcn_Inv_Orgn_Prcss_Inst,1,10);                                                               //Natural: MOVE #TRN-CNTRCT-PPCN-NBR TO SUBSTR ( #WK-PPCN-INV-ORGN-PRCSS-INST,1,10 )
        //*  CTS-0115 >>
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr_N.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr);                                                                    //Natural: MOVE #TRN-CNTRCT-PPCN-NBR TO #CNTRCT-PPCN-NBR-N
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde);                                                                        //Natural: MOVE #TRN-CNTRCT-PAYEE-CDE TO #CNTRCT-PAYEE
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "RF",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
        );
        RF:
        while (condition(vw_cpr.readNextRow("RF", true)))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
            if (condition(vw_cpr.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaFcpa105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde_N().notEquals(cpr_Cntrct_Part_Payee_Cde)))                                                        //Natural: IF #P-VARS.#CNTRCT-PAYEE-CDE-N NE CPR.CNTRCT-PART-PAYEE-CDE
            {
                pnd_Cntr_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #CNTR-FOUND := TRUE
                if (condition(pnd_Sv_Payee.notEquals(cpr_Cntrct_Part_Payee_Cde)))                                                                                         //Natural: IF #SV-PAYEE NE CPR.CNTRCT-PART-PAYEE-CDE
                {
                    pnd_Sv_Pin.setValue(cpr_Cpr_Id_Nbr);                                                                                                                  //Natural: ASSIGN #SV-PIN := CPR.CPR-ID-NBR
                    pnd_Sv_Payee.setValue(cpr_Cntrct_Part_Payee_Cde);                                                                                                     //Natural: ASSIGN #SV-PAYEE := CPR.CNTRCT-PART-PAYEE-CDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa105.getPnd_P_Vars_Pnd_Pin().setValue(cpr_Cpr_Id_Nbr);                                                                                                  //Natural: ASSIGN #P-VARS.#PIN := CPR.CPR-ID-NBR
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CTS-0115 <<
        if (condition(pnd_Cntr_Found.getBoolean() && pdaFcpa105.getPnd_P_Vars_Pnd_Pin().equals(getZero())))                                                               //Natural: IF #CNTR-FOUND AND #P-VARS.#PIN EQ 0
        {
            pdaFcpa105.getPnd_P_Vars_Pnd_Pin().setValue(pnd_Sv_Pin);                                                                                                      //Natural: ASSIGN #P-VARS.#PIN := #SV-PIN
            //*  FE201608 END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Out_Rec.setValuesByName(pdaFcpa105.getPnd_P_Vars());                                                                                                          //Natural: MOVE BY NAME #P-VARS TO #OUT-REC
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),"- 1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new          //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM '- 1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'NET CHANGE LETTER' / *TIMX ( EM = HH:II' 'AP ) 53T 'VALID PIN DETAIL LISTING' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"NET CHANGE LETTER",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(53),
                        "VALID PIN DETAIL LISTING",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),"- 2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new          //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM '- 2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'NET CHANGE LETTER' / *TIMX ( EM = HH:II' 'AP ) 53T 'MISSING PIN ERROR REPORT' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"NET CHANGE LETTER",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(53),
                        "MISSING PIN ERROR REPORT",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");

        getReports().setDisplayColumns(2, "CONTRACT#",
        		pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
        		pnd_Out_Rec_Pnd_Cntrct_Payee_Cde,"PIN#",
        		pnd_Out_Rec_Pnd_Pin);
        getReports().setDisplayColumns(1, "CONTRACT#",
        		pnd_Out_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
        		pnd_Out_Rec_Pnd_Cntrct_Payee_Cde,"PIN#",
        		pnd_Out_Rec_Pnd_Pin);
    }
}
