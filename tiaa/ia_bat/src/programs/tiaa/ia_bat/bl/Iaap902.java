/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:09 PM
**        * FROM NATURAL PROGRAM : Iaap902
************************************************************
**        * FILE NAME            : Iaap902.java
**        * CLASS NAME           : Iaap902
**        * INSTANCE NAME        : Iaap902
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP902    READS IAA TRANS RECORD                 *
*      DATE     -  8/94       CREATES TRANSACTION FILE               *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >   REMOVED 42 TRANSACTION PROCESS CODING
* >   ENHANCED TO ALLOW DEATH CLAIMS PROCESSING
* CHANGED ON APRIL 20, 1995 BY D ROBINSON
* >   ENHANCED TO BYPASS REVERSALS OF DEATH
* CHANGED ON MAY 10, 1995 BY D ROBINSON
* >   CHANGED TO BYPASS CONTRACTUAL CORRECTIONS WHEN NEW ISSUE
* >   EXIST FOR DEATH CLAIMS
* CHANGED ON JUNE 10, 1995 BY D ROBINSON
* >   CHANGED TO BYPASS TRANSACTIONS WITHOUT CONTRACT RECORD
* CHANGED ON AUGUST 8, 1995 BY D ROBINSON
* >   CHANGED TO BYPASS REVERSAL TRANSACTIONS '037' WITH TRANS
* >   SUB CODE OF '37B'
* CHANGED ON SEPT 1, 1995  BY D ROBINSON
* >   CHANGED TO PROCESS CONTRACTURAL CORRECTIONS AND SUSPEND PAYMENT
* >   TRANSACTIONS DIFFERENTLY IF CONTRACT HAS REVERSAL
* >
* CHANGED ON JAN 9, 2001
* >   THE FIELD IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE WAS CHANGED FROM
* >   N 8 TO D6
* >
* > 04/06/2017 RCC : RESTOWED FOR PIN EXPANSION
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap902 extends BLNatBase
{
    // Data Areas
    private LdaIaal902 ldaIaal902;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Reason;
    private DbsField pnd_Trans_Sw;
    private DbsField pnd_Save_33_Ppcn_Nbr;
    private DbsField pnd_Save_33_Payee_Cde;
    private DbsField pnd_Save_37_Ppcn_Nbr;
    private DbsField pnd_Save_37_Payee_Cde;
    private DbsField pnd_No_Mstr_Bypass;
    private DbsField pnd_Trans_Effective_Dte_A;

    private DbsGroup pnd_Trans_Effective_Dte_A__R_Field_1;
    private DbsField pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal902 = new LdaIaal902();
        registerRecord(ldaIaal902);
        registerRecord(ldaIaal902.getVw_iaa_Cntrct());
        registerRecord(ldaIaal902.getVw_iaa_Cntrl_Rcrd());
        registerRecord(ldaIaal902.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaIaal902.getVw_iaa_Cpr_Trans());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Reason = localVariables.newFieldInRecord("pnd_Reason", "#REASON", FieldType.STRING, 2);
        pnd_Trans_Sw = localVariables.newFieldInRecord("pnd_Trans_Sw", "#TRANS-SW", FieldType.STRING, 1);
        pnd_Save_33_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_33_Ppcn_Nbr", "#SAVE-33-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_33_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_33_Payee_Cde", "#SAVE-33-PAYEE-CDE", FieldType.NUMERIC, 3);
        pnd_Save_37_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_37_Ppcn_Nbr", "#SAVE-37-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_37_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_37_Payee_Cde", "#SAVE-37-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_No_Mstr_Bypass = localVariables.newFieldInRecord("pnd_No_Mstr_Bypass", "#NO-MSTR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Trans_Effective_Dte_A = localVariables.newFieldInRecord("pnd_Trans_Effective_Dte_A", "#TRANS-EFFECTIVE-DTE-A", FieldType.STRING, 8);

        pnd_Trans_Effective_Dte_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Trans_Effective_Dte_A__R_Field_1", "REDEFINE", pnd_Trans_Effective_Dte_A);
        pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte = pnd_Trans_Effective_Dte_A__R_Field_1.newFieldInGroup("pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte", 
            "#TRANS-EFFECTIVE-DTE", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal902.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap902() throws Exception
    {
        super("Iaap902");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
                                                                                                                                                                          //Natural: PERFORM READ-CNTRL-RCRD
        sub_Read_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
        ldaIaal902.getVw_iaa_Trans_Rcrd().startDatabaseRead                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #PARM-CHECK-DTE-N
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", ldaIaal902.getPnd_Parm_Check_Dte_N(), WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaIaal902.getVw_iaa_Trans_Rcrd().readNextRow("READ01")))
        {
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Ctr().nadd(1);                                                                                                 //Natural: ADD 1 TO #RECORDS-CTR
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Verify_Cde().equals("V")))                                                                                   //Natural: IF TRANS-VERIFY-CDE = 'V'
            {
                ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Verified_Ctr().nadd(1);                                                                                    //Natural: ADD 1 TO #RECORDS-VERIFIED-CTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Trans_Effective_Dte_A.setValueEdited(ldaIaal902.getIaa_Trans_Rcrd_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #TRANS-EFFECTIVE-DTE-A
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde().equals("37B")))                                                                                    //Natural: IF TRANS-SUB-CDE = '37B'
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("R")))                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                ldaIaal902.getPnd_Save_Trans_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                                          //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := TRANS-PPCN-NBR
                ldaIaal902.getPnd_Save_Trans_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                                        //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := TRANS-PAYEE-CDE
                ldaIaal902.getPnd_Save_Trans_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde());                                                                    //Natural: ASSIGN #SAVE-TRANS-CDE := TRANS-CDE
                if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(37)))                                                                                       //Natural: IF TRANS-CDE = 037
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS-RCRD
                    sub_Process_Trans_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Reason.setValue("R ");                                                                                                                            //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("A")))                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'A'
            {
                if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(902) || ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(906)))                              //Natural: IF TRANS-CDE = 902 OR = 906
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS-RCRD
                    sub_Process_Trans_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal902.getPnd_Save_Trans_Ppcn_Nbr().equals(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr()) && ldaIaal902.getPnd_Save_Trans_Payee_Cde().equals(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde())  //Natural: IF #SAVE-TRANS-PPCN-NBR = TRANS-PPCN-NBR AND #SAVE-TRANS-PAYEE-CDE = TRANS-PAYEE-CDE AND #SAVE-TRANS-CDE = TRANS-CDE
                        && ldaIaal902.getPnd_Save_Trans_Cde().equals(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde())))
                    {
                        pnd_Reason.setValue("D ");                                                                                                                        //Natural: ASSIGN #REASON := 'D '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                        sub_Bypass_Transaction();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS-RCRD
                        sub_Process_Trans_Rcrd();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal902.getPnd_Save_Trans_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                                  //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := TRANS-PPCN-NBR
                        ldaIaal902.getPnd_Save_Trans_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                                //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := TRANS-PAYEE-CDE
                        ldaIaal902.getPnd_Save_Trans_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde());                                                            //Natural: ASSIGN #SAVE-TRANS-CDE := TRANS-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaIaal902.getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr().greater(getZero())))                                                              //Natural: IF #304-TRANS-TO-PROCESS-CTR > 0
        {
                                                                                                                                                                          //Natural: PERFORM COMBINE-CHECK-TRANS
            sub_Combine_Check_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO 304 TRANSACTIONS TO PROCESS");                                                                                                      //Natural: WRITE 'NO 304 TRANSACTIONS TO PROCESS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Ctr());                                           //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #RECORDS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr());                                 //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #RECORDS-PROCESSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr());                                  //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #RECORDS-BYPASSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REVERSED            : ",ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Reversed_Ctr());                                  //Natural: WRITE 'NUMBER OF RECORDS REVERSED            : ' #RECORDS-REVERSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS UN-VERIFIED         : ",ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Verified_Ctr());                                  //Natural: WRITE 'NUMBER OF RECORDS UN-VERIFIED         : ' #RECORDS-VERIFIED-CTR
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRANS-RCRD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-MASTER
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PREVIOUS-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CNTRL-RCRD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISC-NON-TAX-TRANS
        //*                     IAA-TRANS-RCRD.TRANS-EFFCTVE-DTE
        //*                      IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MANUAL-NEW-ISSUE-TRANS
        //*                     IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAX-TRANS
        //*                     IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMBINE-CHECK-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MULTIPLE-DED-TRANS
        //*                     IAA-TRANS-RCRD.TRANS-EFFECTIVE-DTE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASS-TRANSACTION
    }
    private void sub_Process_Trans_Rcrd() throws Exception                                                                                                                //Natural: PROCESS-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_No_Mstr_Bypass.setValue(false);                                                                                                                               //Natural: ASSIGN #NO-MSTR-BYPASS := FALSE
        short decideConditionsMet298 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TRANS-CDE;//Natural: VALUE 020
        if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(20))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && ldaIaal902.getIaa_Cpr_Trans_Cntrct_Actvty_Cde().notEquals(9)))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 033
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(33))))
        {
            decideConditionsMet298++;
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde().equals("066") || ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde().equals("66U")))                      //Natural: IF TRANS-SUB-CDE = '066' OR = '66U'
            {
                ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                    //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
                ldaIaal902.getPnd_Logical_Variables_Pnd_Records_Processed().setValue(false);                                                                              //Natural: ASSIGN #RECORDS-PROCESSED := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
                sub_Get_Current_Master();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                           //Natural: IF #NO-MSTR-BYPASS
                {
                    pnd_Reason.setValue("R ");                                                                                                                            //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*        IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
                sub_Manual_New_Issue_Trans();
                if (condition(Global.isEscape())) {return;}
                ldaIaal902.getPnd_Logical_Variables_Pnd_Records_Processed().setValue(true);                                                                               //Natural: ASSIGN #RECORDS-PROCESSED := TRUE
                if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde().greater(3)))                                                                                 //Natural: IF TRANS-PAYEE-CDE > 3
                {
                    pnd_Save_33_Ppcn_Nbr.setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                                                         //Natural: ASSIGN #SAVE-33-PPCN-NBR := TRANS-PPCN-NBR
                    pnd_Save_33_Payee_Cde.setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                                                       //Natural: ASSIGN #SAVE-33-PAYEE-CDE := TRANS-PAYEE-CDE
                }                                                                                                                                                         //Natural: END-IF
                //*        ELSE
                //*          #REASON := 'S '
                //*          #RECORDS-PROCESSED := FALSE
                //*        END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(35))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9) && ldaIaal902.getIaa_Cpr_Trans_Cntrct_Actvty_Cde().equals(9)))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9 AND IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE = 9
            {
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
                sub_Manual_New_Issue_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 037
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(37))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                  //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
            ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
            ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
            1
            );
            FIND01:
            while (condition(ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND01", true)))
            {
                ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                      //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOUND FOR: ",ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(),ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde()); //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOUND FOR: ' TRANS-PPCN-NBR TRANS-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Reason.setValue("R ");                                                                                                                            //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
            sub_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 040
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(40))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && ldaIaal902.getIaa_Cpr_Trans_Cntrct_Actvty_Cde().notEquals(9)))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 050
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(50))))
        {
            decideConditionsMet298++;
            if (condition((ldaIaal902.getPnd_Save_Trans_Ppcn_Nbr().equals(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr()) && ldaIaal902.getPnd_Save_Trans_Payee_Cde().equals(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde())  //Natural: IF ( #SAVE-TRANS-PPCN-NBR = TRANS-PPCN-NBR AND #SAVE-TRANS-PAYEE-CDE = TRANS-PAYEE-CDE AND #RECORDS-PROCESSED )
                && ldaIaal902.getPnd_Logical_Variables_Pnd_Records_Processed().getBoolean())))
            {
                pnd_Reason.setValue("M ");                                                                                                                                //Natural: ASSIGN #REASON := 'M '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
                sub_Get_Current_Master();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                           //Natural: IF #NO-MSTR-BYPASS
                {
                    pnd_Reason.setValue("R ");                                                                                                                            //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                    sub_Bypass_Transaction();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
                sub_Get_Previous_Trans();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MANUAL-NEW-ISSUE-TRANS
                sub_Manual_New_Issue_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 064
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(64))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && ldaIaal902.getIaa_Cpr_Trans_Cntrct_Actvty_Cde().notEquals(9)))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(66))))
        {
            decideConditionsMet298++;
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("R")))                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Reason.setValue("R");                                                                                                                                 //Natural: ASSIGN #REASON := 'R'
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            //*  AND
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9)))                                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
            {
                //*         IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE NE 9
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 070
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(70))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PREVIOUS-TRANS
            sub_Get_Previous_Trans();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && ldaIaal902.getIaa_Cpr_Trans_Cntrct_Actvty_Cde().notEquals(9)))           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CPR-TRANS.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(102))))
        {
            decideConditionsMet298++;
            if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Actvty_Cde().equals("R")))                                                                                   //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                pnd_Reason.setValue("R");                                                                                                                                 //Natural: ASSIGN #REASON := 'R'
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(104))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(106))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MISC-NON-TAX-TRANS
                sub_Misc_Non_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 304
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(304))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Cntrl_Frst_Trans_Dte().setValue(ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte());                             //Natural: ASSIGN #WS-TRANS-304-RCRD.#CNTRL-FRST-TRANS-DTE := IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Dte().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte());                                                   //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Lst_Trans_Dte().setValue(ldaIaal902.getIaa_Trans_Rcrd_Lst_Trans_Dte());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#LST-TRANS-DTE := IAA-TRANS-RCRD.LST-TRANS-DTE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                         //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Sub_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-SUB-CDE := IAA-TRANS-RCRD.TRANS-SUB-CDE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde());                                                   //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CDE := IAA-TRANS-RCRD.TRANS-CDE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Actvty_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Actvty_Cde());                                     //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-ACTVTY-CDE := IAA-TRANS-RCRD.TRANS-ACTVTY-CDE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Check_Dte().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Check_Dte());                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CHECK-DTE := IAA-TRANS-RCRD.TRANS-CHECK-DTE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Id().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Id());                                           //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-USER-ID := IAA-TRANS-RCRD.TRANS-USER-ID
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_Cmbne_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cmbne_Cde());                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-CMBNE-CDE := IAA-TRANS-RCRD.TRANS-CMBNE-CDE
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Trans_User_Area().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area());                                       //Natural: ASSIGN #WS-TRANS-304-RCRD.#TRANS-USER-AREA := IAA-TRANS-RCRD.TRANS-USER-AREA
                ldaIaal902.getPnd_Ws_Trans_304_Rcrd_Pnd_Invrse_Trans_Dte().setValue(ldaIaal902.getIaa_Trans_Rcrd_Invrse_Trans_Dte());                                     //Natural: ASSIGN #WS-TRANS-304-RCRD.#INVRSE-TRANS-DTE := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                getWorkFiles().write(2, false, ldaIaal902.getPnd_Ws_Trans_304_Rcrd());                                                                                    //Natural: WRITE WORK FILE 2 #WS-TRANS-304-RCRD
                ldaIaal902.getPnd_Packed_Variables_Pnd_304_Trans_To_Process_Ctr().nadd(1);                                                                                //Natural: ADD 1 TO #304-TRANS-TO-PROCESS-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(724))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM TAX-TRANS
                sub_Tax_Trans();
                if (condition(Global.isEscape())) {return;}
                ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                   //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 902
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(902))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MULTIPLE-DED-TRANS
                sub_Multiple_Ded_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(906))))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-MASTER
            sub_Get_Current_Master();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_No_Mstr_Bypass.getBoolean()))                                                                                                               //Natural: IF #NO-MSTR-BYPASS
            {
                pnd_Reason.setValue("R ");                                                                                                                                //Natural: ASSIGN #REASON := 'R '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal902.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9)))                                                                        //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                                                                                                                                                                          //Natural: PERFORM MULTIPLE-DED-TRANS
                sub_Multiple_Ded_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reason.setValue("S ");                                                                                                                                //Natural: ASSIGN #REASON := 'S '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
                sub_Bypass_Transaction();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Current_Master() throws Exception                                                                                                                //Natural: GET-CURRENT-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_No_Mstr_Bypass.setValue(false);                                                                                                                               //Natural: ASSIGN #NO-MSTR-BYPASS := FALSE
        ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                      //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
        ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                    //Natural: ASSIGN #IAA-CNTRCT-PRTCPNT-KEY.#CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
        ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                      //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #IAA-CNTRCT-PRTCPNT-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal902.getPnd_Iaa_Cntrct_Prtcpnt_Key(), WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
        {
            ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal902.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                          //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO CONTRACT PARTICIPANT RECORD FOUND FOR: ",ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                        //Natural: WRITE 'NO CONTRACT PARTICIPANT RECORD FOUND FOR: ' TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Mstr_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #NO-MSTR-BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Previous_Trans() throws Exception                                                                                                                //Natural: GET-PREVIOUS-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal902.getPnd_Iaa_Cpr_Trans_Key_Pnd_Bfre_Imge_Id().setValue("1");                                                                                             //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#BFRE-IMGE-ID := '1'
        ldaIaal902.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                           //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
        ldaIaal902.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                         //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
        ldaIaal902.getPnd_Iaa_Cpr_Trans_Key_Pnd_Trans_Dte().setValue(ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte());                                                //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#TRANS-DTE := IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE
        ldaIaal902.getVw_iaa_Cpr_Trans().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #IAA-CPR-TRANS-KEY
        (
        "READ02",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", ldaIaal902.getPnd_Iaa_Cpr_Trans_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(ldaIaal902.getVw_iaa_Cpr_Trans().readNextRow("READ02")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cntrl_Rcrd() throws Exception                                                                                                                   //Natural: READ-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "=",ldaIaal902.getPnd_Iaa_Parm_Card());                                                                                                     //Natural: WRITE '=' #IAA-PARM-CARD
        if (Global.isEscape()) return;
        if (condition(ldaIaal902.getPnd_Iaa_Parm_Card().equals(" ")))                                                                                                     //Natural: IF #IAA-PARM-CARD = ' '
        {
            ldaIaal902.getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde().setValue("AA");                                                                                              //Natural: ASSIGN #CNTRL-CDE := 'AA'
            ldaIaal902.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
            (
            "READ03",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", ldaIaal902.getPnd_Cntrl_Rcrd_Key(), WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ03:
            while (condition(ldaIaal902.getVw_iaa_Cntrl_Rcrd().readNextRow("READ03")))
            {
                ldaIaal902.getPnd_Parm_Check_Dte_N_Pnd_Parm_Check_Dte_A().setValueEdited(ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));  //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #PARM-CHECK-DTE-A
                ldaIaal902.getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte().compute(new ComputeParameters(false, ldaIaal902.getPnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte()),    //Natural: COMPUTE #CNTRL-INVRSE-DTE = 100000000 - #PARM-CHECK-DTE-N
                    DbsField.subtract(100000000,ldaIaal902.getPnd_Parm_Check_Dte_N()));
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte());                                                                                    //Natural: WRITE '=' CNTRL-CHECK-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Misc_Non_Tax_Trans() throws Exception                                                                                                                //Natural: MISC-NON-TAX-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Iaan902a.class , getCurrentProcessState(), ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN902A' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-SUB-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.TRANS-VERIFY-ID #LAST-BATCH-NBR #REASON #RECORDS-BYPASSED-CTR #RECORDS-PROCESSED-CTR #SAVE-33-PPCN-NBR #SAVE-33-PAYEE-CDE #SAVE-37-PPCN-NBR #SAVE-37-PAYEE-CDE
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde(), 
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Check_Dte(), pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area(), 
            ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Id(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Verify_Id(), ldaIaal902.getPnd_Last_Batch_Nbr(), pnd_Reason, 
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr(), ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr(), pnd_Save_33_Ppcn_Nbr, 
            pnd_Save_33_Payee_Cde, pnd_Save_37_Ppcn_Nbr, pnd_Save_37_Payee_Cde);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Manual_New_Issue_Trans() throws Exception                                                                                                            //Natural: MANUAL-NEW-ISSUE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Sw.setValue(" ");                                                                                                                                       //Natural: ASSIGN #TRANS-SW := ' '
        DbsUtil.callnat(Iaan902b.class , getCurrentProcessState(), ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN902B' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA #LAST-BATCH-NBR #TRANS-SW #SAVE-37-PPCN-NBR #SAVE-37-PAYEE-CDE
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal902.getPnd_Last_Batch_Nbr(), pnd_Trans_Sw, 
            pnd_Save_37_Ppcn_Nbr, pnd_Save_37_Payee_Cde);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Trans_Sw.equals("Y")))                                                                                                                          //Natural: IF #TRANS-SW = 'Y'
        {
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr().nadd(1);                                                                                       //Natural: ADD 1 TO #RECORDS-PROCESSED-CTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Reason.setValue("I ");                                                                                                                                    //Natural: ASSIGN #REASON := 'I '
                                                                                                                                                                          //Natural: PERFORM BYPASS-TRANSACTION
            sub_Bypass_Transaction();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tax_Trans() throws Exception                                                                                                                         //Natural: TAX-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Iaan902c.class , getCurrentProcessState(), ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN902C' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA #LAST-BATCH-NBR
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal902.getPnd_Last_Batch_Nbr());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Combine_Check_Trans() throws Exception                                                                                                               //Natural: COMBINE-CHECK-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Iaan902d.class , getCurrentProcessState(), ldaIaal902.getPnd_Last_Batch_Nbr(), ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr(),     //Natural: CALLNAT 'IAAN902D' #LAST-BATCH-NBR #RECORDS-BYPASSED-CTR #RECORDS-PROCESSED-CTR
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Multiple_Ded_Trans() throws Exception                                                                                                                //Natural: MULTIPLE-DED-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Iaan902e.class , getCurrentProcessState(), ldaIaal902.getIaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte(),         //Natural: CALLNAT 'IAAN902E' IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE IAA-TRANS-RCRD.TRANS-DTE IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-CHECK-DTE #TRANS-EFFECTIVE-DTE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.TRANS-VERIFY-ID IAA-TRANS-RCRD.TRANS-CMBNE-CDE #LAST-BATCH-NBR #REASON #RECORDS-BYPASSED-CTR #RECORDS-PROCESSED-CTR #SAVE-TRANS-PPCN-NBR #SAVE-TRANS-PAYEE-CDE #SAVE-TRANS-CDE #SEQ-DDCTN-TBL ( * )
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Check_Dte(), 
            pnd_Trans_Effective_Dte_A_Pnd_Trans_Effective_Dte, ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Id(), 
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Verify_Id(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cmbne_Cde(), ldaIaal902.getPnd_Last_Batch_Nbr(), pnd_Reason, 
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr(), ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Processed_Ctr(), ldaIaal902.getPnd_Save_Trans_Ppcn_Nbr(), 
            ldaIaal902.getPnd_Save_Trans_Payee_Cde(), ldaIaal902.getPnd_Save_Trans_Cde(), ldaIaal902.getPnd_Seq_Ddctn_Tbl().getValue("*"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Bypass_Transaction() throws Exception                                                                                                                //Natural: BYPASS-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde().equals(33) && ldaIaal902.getIaa_Trans_Rcrd_Trans_Sub_Cde().equals("066") && (ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde().equals(2)  //Natural: IF IAA-TRANS-RCRD.TRANS-CDE = 33 AND IAA-TRANS-RCRD.TRANS-SUB-CDE = '066' AND ( IAA-TRANS-RCRD.TRANS-PAYEE-CDE = 02 OR IAA-TRANS-RCRD.TRANS-PAYEE-CDE = 03 )
            || ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde().equals(3))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Reason.equals("R ")))                                                                                                                           //Natural: IF #REASON = 'R '
        {
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Reversed_Ctr().nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-REVERSED-CTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal902.getPnd_Packed_Variables_Pnd_Records_Bypassed_Ctr().nadd(1);                                                                                        //Natural: ADD 1 TO #RECORDS-BYPASSED-CTR
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(9, false, ldaIaal902.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Payee_Cde(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Cde(),  //Natural: WRITE WORK FILE 9 IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-USER-AREA IAA-TRANS-RCRD.TRANS-USER-ID IAA-TRANS-RCRD.TRANS-VERIFY-ID IAA-TRANS-RCRD.TRANS-DTE #REASON
            ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Area(), ldaIaal902.getIaa_Trans_Rcrd_Trans_User_Id(), ldaIaal902.getIaa_Trans_Rcrd_Trans_Verify_Id(), 
            ldaIaal902.getIaa_Trans_Rcrd_Trans_Dte(), pnd_Reason);
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
