/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:37 PM
**        * FROM NATURAL PROGRAM : Iaap595
************************************************************
**        * FILE NAME            : Iaap595.java
**        * CLASS NAME           : Iaap595
**        * INSTANCE NAME        : Iaap595
************************************************************
************************************************************************
* PROGRAM  : IAAP595
* SYSTEM   : IA
* TITLE    : READ DEDS, CPR & NAME/ADDRESS
* GENERATED: DEC 2003
* FUNCTION : PRODUCE CONTRACTS WITH REMAINING OVERPAYMENT AMOUNTS
*            AND CONTRACT TERMINATED DO TO GUARANTEED PERIOD HAS
*            HAS EXPIRED AND RECOVERY HAS NOT NOT BEEN MADE IN FULL
*
* HISTORY
* 9/04        INCREASED TABLE SIZE FOR SVE-PIN
*             DO SCAN ON 9/04
* 8/15        COR/NAAD RETIREMENT.  CHANGES MARKED 082515.
* 04/06/2019 RCC : PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap595 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Ded;
    private DbsField iaa_Ded_Lst_Trans_Dte;
    private DbsField iaa_Ded_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ded_Ddctn_Payee_Cde;
    private DbsField iaa_Ded_Ddctn_Id_Nbr;
    private DbsField iaa_Ded_Ddctn_Cde;
    private DbsField iaa_Ded_Ddctn_Seq_Nbr;
    private DbsField iaa_Ded_Ddctn_Payee;
    private DbsField iaa_Ded_Ddctn_Per_Amt;
    private DbsField iaa_Ded_Ddctn_Ytd_Amt;
    private DbsField iaa_Ded_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ded_Ddctn_Tot_Amt;
    private DbsField iaa_Ded_Ddctn_Intent_Cde;
    private DbsField iaa_Ded_Ddctn_Strt_Dte;
    private DbsField iaa_Ded_Ddctn_Stp_Dte;
    private DbsField iaa_Ded_Ddctn_Final_Dte;

    private DataAccessProgramView vw_iaa_Ded_2;
    private DbsField iaa_Ded_2_Lst_Trans_Dte;
    private DbsField iaa_Ded_2_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ded_2_Ddctn_Payee_Cde;
    private DbsField iaa_Ded_2_Ddctn_Id_Nbr;
    private DbsField iaa_Ded_2_Ddctn_Cde;
    private DbsField iaa_Ded_2_Ddctn_Seq_Nbr;
    private DbsField iaa_Ded_2_Ddctn_Payee;
    private DbsField iaa_Ded_2_Ddctn_Per_Amt;
    private DbsField iaa_Ded_2_Ddctn_Ytd_Amt;
    private DbsField iaa_Ded_2_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ded_2_Ddctn_Tot_Amt;
    private DbsField iaa_Ded_2_Ddctn_Intent_Cde;
    private DbsField iaa_Ded_2_Ddctn_Strt_Dte;
    private DbsField iaa_Ded_2_Ddctn_Stp_Dte;
    private DbsField iaa_Ded_2_Ddctn_Final_Dte;
    private DbsField pnd_Iaa_Ded_Key;

    private DbsGroup pnd_Iaa_Ded_Key__R_Field_1;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Iaa_Ded_Key_Pnd_Ddctn_Cde;
    private DbsField pnd_Sve_Ddctn_Per_Amt;
    private DbsField pnd_Sve_Ddctn_Tot_Amt;
    private DbsField pnd_Sve_Ddctn_Pd_To_Dte;
    private DbsField pnd_Sve_Ded_Cntrct_Nbr;
    private DbsField pnd_Sve_Ded_Payee;
    private DbsField pnd_Wrk_Name;
    private DbsField pnd_Fnd_Sw;
    private DbsField pnd_Wrt_Nme_Sw;
    private DbsField pnd_Name_Key;

    private DbsGroup pnd_Name_Key__R_Field_2;
    private DbsField pnd_Name_Key_Pnd_Type_N;
    private DbsField pnd_Name_Key_Pnd_Contrct_N;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_3;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;

    private DbsGroup iaa_Trans_Rcrd__R_Field_4;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr8;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr2;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_5;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte_Yyyymm;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte_Dd;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_6;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_7;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Sve_Pin;

    private DbsGroup pnd_Sve_Pin__R_Field_8;
    private DbsField pnd_Sve_Pin_Pnd_Sve_Pin_A;
    private DbsField pnd_Sve_Trns_User_Id;
    private DbsField pnd_Sve_Pin_Nbrs;

    private DbsGroup pnd_Sve_Pin_Nbrs__R_Field_9;
    private DbsField pnd_Sve_Pin_Nbrs_Pnd_Sve_Pin_Nbrs_A;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_iaa_Ded = new DataAccessProgramView(new NameInfo("vw_iaa_Ded", "IAA-DED"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Ded_Lst_Trans_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Ded_Ddctn_Ppcn_Nbr = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "DDCTN_PPCN_NBR");
        iaa_Ded_Ddctn_Payee_Cde = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE_CDE");
        iaa_Ded_Ddctn_Id_Nbr = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "DDCTN_ID_NBR");
        iaa_Ded_Ddctn_Cde = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Ded_Ddctn_Seq_Nbr = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "DDCTN_SEQ_NBR");
        iaa_Ded_Ddctn_Payee = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Ded_Ddctn_Per_Amt = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, 
            "DDCTN_PER_AMT");
        iaa_Ded_Ddctn_Ytd_Amt = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "DDCTN_YTD_AMT");
        iaa_Ded_Ddctn_Pd_To_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ded_Ddctn_Tot_Amt = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "DDCTN_TOT_AMT");
        iaa_Ded_Ddctn_Intent_Cde = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "DDCTN_INTENT_CDE");
        iaa_Ded_Ddctn_Strt_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_STRT_DTE");
        iaa_Ded_Ddctn_Stp_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_STP_DTE");
        iaa_Ded_Ddctn_Final_Dte = vw_iaa_Ded.getRecord().newFieldInGroup("iaa_Ded_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Ded);

        vw_iaa_Ded_2 = new DataAccessProgramView(new NameInfo("vw_iaa_Ded_2", "IAA-DED-2"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Ded_2_Lst_Trans_Dte = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Ded_2_Ddctn_Ppcn_Nbr = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "DDCTN_PPCN_NBR");
        iaa_Ded_2_Ddctn_Payee_Cde = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE_CDE");
        iaa_Ded_2_Ddctn_Id_Nbr = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "DDCTN_ID_NBR");
        iaa_Ded_2_Ddctn_Cde = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Ded_2_Ddctn_Seq_Nbr = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "DDCTN_SEQ_NBR");
        iaa_Ded_2_Ddctn_Payee = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Ded_2_Ddctn_Per_Amt = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 7, 2, 
            RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ded_2_Ddctn_Ytd_Amt = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ded_2_Ddctn_Pd_To_Dte = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ded_2_Ddctn_Tot_Amt = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ded_2_Ddctn_Intent_Cde = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ded_2_Ddctn_Strt_Dte = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_STRT_DTE");
        iaa_Ded_2_Ddctn_Stp_Dte = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_STP_DTE");
        iaa_Ded_2_Ddctn_Final_Dte = vw_iaa_Ded_2.getRecord().newFieldInGroup("iaa_Ded_2_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Ded_2);

        pnd_Iaa_Ded_Key = localVariables.newFieldInRecord("pnd_Iaa_Ded_Key", "#IAA-DED-KEY", FieldType.STRING, 18);

        pnd_Iaa_Ded_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaa_Ded_Key__R_Field_1", "REDEFINE", pnd_Iaa_Ded_Key);
        pnd_Iaa_Ded_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Iaa_Ded_Key__R_Field_1.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Iaa_Ded_Key_Pnd_Ddctn_Payee_Cde = pnd_Iaa_Ded_Key__R_Field_1.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Iaa_Ded_Key_Pnd_Ddctn_Seq_Nbr = pnd_Iaa_Ded_Key__R_Field_1.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Iaa_Ded_Key_Pnd_Ddctn_Cde = pnd_Iaa_Ded_Key__R_Field_1.newFieldInGroup("pnd_Iaa_Ded_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);
        pnd_Sve_Ddctn_Per_Amt = localVariables.newFieldInRecord("pnd_Sve_Ddctn_Per_Amt", "#SVE-DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Sve_Ddctn_Tot_Amt = localVariables.newFieldInRecord("pnd_Sve_Ddctn_Tot_Amt", "#SVE-DDCTN-TOT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Sve_Ddctn_Pd_To_Dte = localVariables.newFieldInRecord("pnd_Sve_Ddctn_Pd_To_Dte", "#SVE-DDCTN-PD-TO-DTE", FieldType.NUMERIC, 9, 2);
        pnd_Sve_Ded_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Sve_Ded_Cntrct_Nbr", "#SVE-DED-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Sve_Ded_Payee = localVariables.newFieldInRecord("pnd_Sve_Ded_Payee", "#SVE-DED-PAYEE", FieldType.NUMERIC, 2);
        pnd_Wrk_Name = localVariables.newFieldInRecord("pnd_Wrk_Name", "#WRK-NAME", FieldType.STRING, 35);
        pnd_Fnd_Sw = localVariables.newFieldInRecord("pnd_Fnd_Sw", "#FND-SW", FieldType.STRING, 1);
        pnd_Wrt_Nme_Sw = localVariables.newFieldInRecord("pnd_Wrt_Nme_Sw", "#WRT-NME-SW", FieldType.STRING, 1);
        pnd_Name_Key = localVariables.newFieldInRecord("pnd_Name_Key", "#NAME-KEY", FieldType.STRING, 12);

        pnd_Name_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Name_Key__R_Field_2", "REDEFINE", pnd_Name_Key);
        pnd_Name_Key_Pnd_Type_N = pnd_Name_Key__R_Field_2.newFieldInGroup("pnd_Name_Key_Pnd_Type_N", "#TYPE-N", FieldType.STRING, 2);
        pnd_Name_Key_Pnd_Contrct_N = pnd_Name_Key__R_Field_2.newFieldInGroup("pnd_Name_Key_Pnd_Contrct_N", "#CONTRCT-N", FieldType.STRING, 10);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");

        iaa_Trans_Rcrd__R_Field_3 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_3", "REDEFINE", iaa_Trans_Rcrd_Lst_Trans_Dte);
        iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd = iaa_Trans_Rcrd__R_Field_3.newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd", "LST-TRANS-DTE-RCRD", FieldType.TIME);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");

        iaa_Trans_Rcrd__R_Field_4 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_4", "REDEFINE", iaa_Trans_Rcrd_Trans_Ppcn_Nbr);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr8 = iaa_Trans_Rcrd__R_Field_4.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr8", "TRANS-PPCN-NBR8", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr2 = iaa_Trans_Rcrd__R_Field_4.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr2", "TRANS-PPCN-NBR2", FieldType.STRING, 
            2);
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");

        iaa_Trans_Rcrd__R_Field_5 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_5", "REDEFINE", iaa_Trans_Rcrd_Trans_Todays_Dte);
        iaa_Trans_Rcrd_Trans_Todays_Dte_Yyyymm = iaa_Trans_Rcrd__R_Field_5.newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte_Yyyymm", "TRANS-TODAYS-DTE-YYYYMM", 
            FieldType.NUMERIC, 6);
        iaa_Trans_Rcrd_Trans_Todays_Dte_Dd = iaa_Trans_Rcrd__R_Field_5.newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte_Dd", "TRANS-TODAYS-DTE-DD", FieldType.NUMERIC, 
            2);
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 24);

        pnd_Trans_Cntrct_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_6", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_7", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Sve_Pin = localVariables.newFieldInRecord("pnd_Sve_Pin", "#SVE-PIN", FieldType.NUMERIC, 12);

        pnd_Sve_Pin__R_Field_8 = localVariables.newGroupInRecord("pnd_Sve_Pin__R_Field_8", "REDEFINE", pnd_Sve_Pin);
        pnd_Sve_Pin_Pnd_Sve_Pin_A = pnd_Sve_Pin__R_Field_8.newFieldInGroup("pnd_Sve_Pin_Pnd_Sve_Pin_A", "#SVE-PIN-A", FieldType.STRING, 12);
        pnd_Sve_Trns_User_Id = localVariables.newFieldInRecord("pnd_Sve_Trns_User_Id", "#SVE-TRNS-USER-ID", FieldType.STRING, 8);
        pnd_Sve_Pin_Nbrs = localVariables.newFieldArrayInRecord("pnd_Sve_Pin_Nbrs", "#SVE-PIN-NBRS", FieldType.NUMERIC, 12, new DbsArrayController(1, 
            200));

        pnd_Sve_Pin_Nbrs__R_Field_9 = localVariables.newGroupInRecord("pnd_Sve_Pin_Nbrs__R_Field_9", "REDEFINE", pnd_Sve_Pin_Nbrs);
        pnd_Sve_Pin_Nbrs_Pnd_Sve_Pin_Nbrs_A = pnd_Sve_Pin_Nbrs__R_Field_9.newFieldArrayInGroup("pnd_Sve_Pin_Nbrs_Pnd_Sve_Pin_Nbrs_A", "#SVE-PIN-NBRS-A", 
            FieldType.STRING, 12, new DbsArrayController(1, 200));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.NUMERIC, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Ded.reset();
        vw_iaa_Ded_2.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap595() throws Exception
    {
        super("Iaap595");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE FORMATS
        //*                                                                                                                                                               //Natural: FORMAT KD = ON LS = 133 SG = OFF ES = OFF ZP = OFF;//Natural: FORMAT ( 1 ) LS = 133 SG = OFF ES = OFF ZP = OFF;//Natural: FORMAT ( 2 ) LS = 133 SG = OFF ES = OFF ZP = OFF
        //*  082515 OPEN MDM QUEUE
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: FORMAT ( 1 ) LS = 132 PS = 56
        vw_iaa_Ded.startDatabaseRead                                                                                                                                      //Natural: READ IAA-DED BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM 'GA000000'
        (
        "READ_FILE",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", "GA000000", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ_FILE:
        while (condition(vw_iaa_Ded.readNextRow("READ_FILE")))
        {
            if (condition(iaa_Ded_Ddctn_Stp_Dte.equals(getZero()) && iaa_Ded_Ddctn_Cde.equals("001")))                                                                    //Natural: IF IAA-DED.DDCTN-STP-DTE = 0 AND IAA-DED.DDCTN-CDE = '001'
            {
                pnd_Sve_Ddctn_Per_Amt.setValue(iaa_Ded_Ddctn_Per_Amt);                                                                                                    //Natural: ASSIGN #SVE-DDCTN-PER-AMT := IAA-DED.DDCTN-PER-AMT
                pnd_Sve_Ddctn_Tot_Amt.setValue(iaa_Ded_Ddctn_Tot_Amt);                                                                                                    //Natural: ASSIGN #SVE-DDCTN-TOT-AMT := IAA-DED.DDCTN-TOT-AMT
                pnd_Sve_Ddctn_Pd_To_Dte.setValue(iaa_Ded_Ddctn_Pd_To_Dte);                                                                                                //Natural: ASSIGN #SVE-DDCTN-PD-TO-DTE := IAA-DED.DDCTN-PD-TO-DTE
                                                                                                                                                                          //Natural: PERFORM READ-CPR
                sub_Read_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ==>
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR
        //* *****
        //*  ===> READ CPR BY PIN GET ALL CONTRACTS FOR A PIN
        //* *****
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR-BY-PIN
        //*  ===> READ IAA TRANS RCRD FOR TRANSACTION USER-ID
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-TRANS-RCRD
        //*  ===> READ IAA DEDUCTION RECORD FOR OVERPAYMENT DEDUCTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DEDUCTION-FILE
        //*  =============================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-ADDRESS
        //*  82515 START
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  082515 CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  082515 END
    }
    private void sub_Read_Cpr() throws Exception                                                                                                                          //Natural: READ-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Ded_Ddctn_Ppcn_Nbr);                                                                                        //Natural: ASSIGN #CNTRCT-PPCN-NBR := IAA-DED.DDCTN-PPCN-NBR
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(iaa_Ded_Ddctn_Payee_Cde);                                                                                      //Natural: ASSIGN #CNTRCT-PAYEE-CDE := IAA-DED.DDCTN-PAYEE-CDE
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        F1:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("F1")))
        {
            if (condition(!(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9))))                                                                                        //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE = 9
            {
                continue;
            }
            if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr) || pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde))) //Natural: IF #CNTRCT-PPCN-NBR NE CNTRCT-PART-PPCN-NBR OR #CNTRCT-PAYEE-CDE NE CNTRCT-PART-PAYEE-CDE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  ACCUM BY PIN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sve_Pin.setValue(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr);                                                                                                     //Natural: ASSIGN #SVE-PIN := CPR-ID-NBR
                                                                                                                                                                          //Natural: PERFORM READ-NAME-ADDRESS
            sub_Read_Name_Address();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.examine(new ExamineSource(pnd_Sve_Pin_Nbrs_Pnd_Sve_Pin_Nbrs_A.getValue("*"),true), new ExamineSearch(pnd_Sve_Pin_Pnd_Sve_Pin_A), new                  //Natural: EXAMINE FULL #SVE-PIN-NBRS-A ( * ) FOR #SVE-PIN-A GIVING INDEX #I
                ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I = 0
            {
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I2
                pnd_Sve_Pin_Nbrs.getValue(pnd_I2).setValue(pnd_Sve_Pin);                                                                                                  //Natural: ASSIGN #SVE-PIN-NBRS ( #I2 ) := #SVE-PIN
                                                                                                                                                                          //Natural: PERFORM READ-CPR-BY-PIN
                sub_Read_Cpr_By_Pin();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cpr_By_Pin() throws Exception                                                                                                                   //Natural: READ-CPR-BY-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //*  #SVE-DED-CNTRCT-NBR :=
        //*  #SVE-DED-PAYEE      :=
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #SVE-PIN
        (
        "READ01",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Sve_Pin, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ01")))
        {
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.notEquals(9)))                                                                                        //Natural: REJECT IF CNTRCT-ACTVTY-CDE NE 9
            {
                continue;
            }
            //*  ACCUM BY PIN
            if (condition(pnd_Sve_Pin.notEquals(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr)))                                                                                     //Natural: IF #SVE-PIN NE CPR-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.equals(iaa_Ded_Ddctn_Payee_Cde)))                                                                 //Natural: IF CNTRCT-PART-PAYEE-CDE = IAA-DED.DDCTN-PAYEE-CDE
            {
                //*  GET TRANS FOR USER ID
                                                                                                                                                                          //Natural: PERFORM READ-IAA-TRANS-RCRD
                sub_Read_Iaa_Trans_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-DEDUCTION-FILE
                sub_Read_Deduction_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Sve_Ddctn_Per_Amt.greater(getZero())))                                                                                                  //Natural: IF #SVE-DDCTN-PER-AMT GT 0
                {
                    getReports().display(1, ReportOption.NOTITLE,"/Name ",                                                                                                //Natural: DISPLAY ( 1 ) NOTITLE '/Name ' #WRK-NAME '/SSN  ' PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) '/Cntract  ' CNTRCT-PART-PPCN-NBR '/Py' CNTRCT-PART-PAYEE-CDE ( EM = 99 ) 'Last-Trans/Date' TRANS-TODAYS-DTE ( EM = 9999-99-99 ) 'Ded-Per/Amt' #SVE-DDCTN-PER-AMT ( EM = ZZ,ZZZ.99 ) 'Total-Over-Pmt/Amount' #SVE-DDCTN-TOT-AMT ( EM = Z,ZZZ,ZZZ.99 ) 'Total-Over-Pmt/Recovered' #SVE-DDCTN-PD-TO-DTE ( EM = Z,ZZZ,ZZZ.99 ) 'Associate Name' #SVE-TRNS-USER-ID
                    		pnd_Wrk_Name,"/SSN  ",
                    		iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"/Cntract  ",
                    		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr,"/Py",
                    		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"Last-Trans/Date",
                    		iaa_Trans_Rcrd_Trans_Todays_Dte, new ReportEditMask ("9999-99-99"),"Ded-Per/Amt",
                    		pnd_Sve_Ddctn_Per_Amt, new ReportEditMask ("ZZ,ZZZ.99"),"Total-Over-Pmt/Amount",
                    		pnd_Sve_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Total-Over-Pmt/Recovered",
                    		pnd_Sve_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Associate Name",
                    		pnd_Sve_Trns_User_Id);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Trans_Rcrd() throws Exception                                                                                                               //Natural: READ-IAA-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                   //Natural: ASSIGN #TRANS-PPCN-NBR := CNTRCT-PART-PPCN-NBR
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                 //Natural: ASSIGN #TRANS-PAYEE-CDE := CNTRCT-PART-PAYEE-CDE
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-TRANS-RCRD BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            if (condition(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.notEquals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) || iaa_Trans_Rcrd_Trans_Payee_Cde.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF #TRANS-PPCN-NBR NE TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sve_Trns_User_Id.setValue(iaa_Trans_Rcrd_Trans_User_Id);                                                                                                  //Natural: ASSIGN #SVE-TRNS-USER-ID := TRANS-USER-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Deduction_File() throws Exception                                                                                                               //Natural: READ-DEDUCTION-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaa_Ded_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                        //Natural: ASSIGN #DDCTN-PPCN-NBR := CNTRCT-PART-PPCN-NBR
        pnd_Iaa_Ded_Key_Pnd_Ddctn_Payee_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                      //Natural: ASSIGN #DDCTN-PAYEE-CDE := CNTRCT-PART-PAYEE-CDE
        pnd_Sve_Ddctn_Per_Amt.reset();                                                                                                                                    //Natural: RESET #SVE-DDCTN-PER-AMT #SVE-DDCTN-TOT-AMT #SVE-DDCTN-PD-TO-DTE
        pnd_Sve_Ddctn_Tot_Amt.reset();
        pnd_Sve_Ddctn_Pd_To_Dte.reset();
        vw_iaa_Ded_2.startDatabaseRead                                                                                                                                    //Natural: READ IAA-DED-2 BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #IAA-DED-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Iaa_Ded_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Ded_2.readNextRow("READ03")))
        {
            if (condition(iaa_Ded_2_Ddctn_Ppcn_Nbr.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr) || iaa_Ded_2_Ddctn_Payee_Cde.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde))) //Natural: IF IAA-DED-2.DDCTN-PPCN-NBR NE CNTRCT-PART-PPCN-NBR OR IAA-DED-2.DDCTN-PAYEE-CDE NE CNTRCT-PART-PAYEE-CDE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Ded_2_Ddctn_Stp_Dte.equals(getZero()) && iaa_Ded_2_Ddctn_Cde.equals("001")))                                                                //Natural: IF IAA-DED-2.DDCTN-STP-DTE = 0 AND IAA-DED-2.DDCTN-CDE = '001'
            {
                pnd_Sve_Ddctn_Per_Amt.setValue(iaa_Ded_2_Ddctn_Per_Amt);                                                                                                  //Natural: ASSIGN #SVE-DDCTN-PER-AMT := IAA-DED-2.DDCTN-PER-AMT
                pnd_Sve_Ddctn_Tot_Amt.setValue(iaa_Ded_2_Ddctn_Tot_Amt);                                                                                                  //Natural: ASSIGN #SVE-DDCTN-TOT-AMT := IAA-DED-2.DDCTN-TOT-AMT
                pnd_Sve_Ddctn_Pd_To_Dte.setValue(iaa_Ded_2_Ddctn_Pd_To_Dte);                                                                                              //Natural: ASSIGN #SVE-DDCTN-PD-TO-DTE := IAA-DED-2.DDCTN-PD-TO-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Name_Address() throws Exception                                                                                                                 //Natural: READ-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //*  082515 START
        //* *RESET  #FND-SW
        //* *#TYPE-N  := 'IO'
        //* *#CONTRCT-N  := CNTRCT-PART-PPCN-NBR
        //* *READ (1) NAS-NAME-ADDRESS BY CNTRCT-TYPE-CORR-CNTRCT-KEY
        //* *    STARTING FROM #NAME-KEY
        //* *  IF  CNTRCT-PART-PPCN-NBR   NE NAS-NAME-ADDRESS.CNTRCT-NMBR
        //* *    WRITE 'COULDN''T FIND NAME & ADDRESS '
        //* *      '=' CNTRCT-PART-PPCN-NBR
        //* *    ESCAPE BOTTOM
        //* *  END-IF
        //* *  #WRK-NAME := NAS-NAME-ADDRESS.CNTRCT-NAME-FREE
        //* *  IF  CNTRCT-PART-PPCN-NBR   = NAS-NAME-ADDRESS.CNTRCT-NMBR
        //* *    #FND-SW  := 'Y'
        //* *    ESCAPE BOTTOM
        //* *  END-IF
        //* *END-READ
        //*  RESET #MDMA100 #WRK-NAME              /* PIN EXPANSION
        //*  #I-PIN  := #SVE-PIN                   /*
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101 #WRK-NAME
        pnd_Wrk_Name.reset();
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Sve_Pin);                                                                                                  //Natural: ASSIGN #I-PIN-N12 := #SVE-PIN
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #O-RETURN-CODE EQ '0000'
        {
            pnd_Wrk_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),  //Natural: COMPRESS #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO #WRK-NAME
                pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "BAD RETURN FROM MDMN101A. RC=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                                                         //Natural: WRITE 'BAD RETURN FROM MDMN101A. RC=' #O-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text(), new AlphanumericLength (70));                                                            //Natural: WRITE #O-RETURN-TEXT ( AL = 70 )
            if (Global.isEscape()) return;
            getReports().write(0, "PIN=",pnd_Sve_Pin);                                                                                                                    //Natural: WRITE 'PIN=' #SVE-PIN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  082515 END
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I).equals(" ")))                                                           //Natural: IF ##DATA-RESPONSE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(15),"Remaining Overpayment Guaranteed Period Expired ",new  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 15X 'Remaining Overpayment Guaranteed Period Expired ' 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
                        ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"Run Date: ",Global.getDATU());                                                                            //Natural: WRITE ( 1 ) 'Run Date: ' *DATU
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "KD=ON LS=133 SG=OFF ES=OFF ZP=OFF");
        Global.format(1, "LS=133 SG=OFF ES=OFF ZP=OFF");
        Global.format(2, "LS=133 SG=OFF ES=OFF ZP=OFF");
        Global.format(1, "LS=132 PS=56");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"/Name ",
        		pnd_Wrk_Name,"/SSN  ",
        		iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"/Cntract  ",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr,"/Py",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"Last-Trans/Date",
        		iaa_Trans_Rcrd_Trans_Todays_Dte, new ReportEditMask ("9999-99-99"),"Ded-Per/Amt",
        		pnd_Sve_Ddctn_Per_Amt, new ReportEditMask ("ZZ,ZZZ.99"),"Total-Over-Pmt/Amount",
        		pnd_Sve_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Total-Over-Pmt/Recovered",
        		pnd_Sve_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Associate Name",
        		pnd_Sve_Trns_User_Id);
    }
}
