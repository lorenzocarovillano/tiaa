/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:08 PM
**        * FROM NATURAL PROGRAM : Aiap008
************************************************************
**        * FILE NAME            : Aiap008.java
**        * CLASS NAME           : Aiap008
**        * INSTANCE NAME        : Aiap008
************************************************************
************************************************************************
*
***** AIAP008
***** OLD NAME AIAN059 - VERSION 2.1 - 12/15/98
* HISTORY:
* 04/30/15 - MODIFIED TO USE EXTERNALIZATION TO DERIVE ONE-BYTE FUND
*            CODE REPLACING HARDCODING BASED ON THE RATE CODE.
*            SCAN ON 04/15 FOR CHANGES.
*
* 05/06/15 - USE FUND CODE F FOR ILB. RECONNET USES A DIFFERENT VALUE
*            FROM WHAT EXTERNALIZATION HAS. SCAN ON 05/15 FOR CHANGES.
* 02/14/18 - BYPASS AUTOMATED ACCELERATED ANNUITIZATION SETTLEMENT AND
*            EXCLUDE FROM IA_MANUAL.TXT SOURCE DATA.
*            SCAN ON 022018 FOR CHANGES.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap008 extends BLNatBase
{
    // Data Areas
    private LdaAial0590 ldaAial0590;
    private LdaAial0591 ldaAial0591;
    private LdaAial0592 ldaAial0592;
    private LdaAial0594 ldaAial0594;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Header_Rec;

    private DbsGroup pnd_Header_Rec__R_Field_1;
    private DbsField pnd_Header_Rec_Pnd_Start_Date_A;

    private DbsGroup pnd_Header_Rec__R_Field_2;
    private DbsField pnd_Header_Rec_Pnd_Start_Date;
    private DbsField pnd_Header_Rec_Pnd_Filler1;
    private DbsField pnd_Header_Rec_Pnd_End_Date_A;

    private DbsGroup pnd_Header_Rec__R_Field_3;
    private DbsField pnd_Header_Rec_Pnd_End_Date;
    private DbsField pnd_Header_Rec_Pnd_Filler_Ef1;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Date_A;

    private DbsGroup pnd_Header_Rec__R_Field_4;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Date;
    private DbsField pnd_Header_Rec_Pnd_Filler_Ef2;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Date_A;

    private DbsGroup pnd_Header_Rec__R_Field_5;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Date;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_6;
    private DbsField pnd_Start_Key_Pnd_Start_Key_Date;
    private DbsField pnd_Start_Key_Pnd_Filler31;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_7;
    private DbsField pnd_End_Key_Pnd_End_Key_Date;
    private DbsField pnd_End_Key_Pnd_Filler32;
    private DbsField pnd_Check_Date_Ccyymmdd_A;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_8;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_9;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd;
    private DbsField pnd_W_Trans_Dte;
    private DbsField pnd_W_Trans_Dte_Next;
    private DbsField pnd_W_Trans_Dte_Prior;
    private DbsField pnd_W_Trans_Input_Dte;
    private DbsField pnd_W_Count;
    private DbsField pnd_Num_Read;
    private DbsField pnd_Num_Written;
    private DbsField pnd_Record_Found_Switch;
    private DbsField pnd_Before_Record_Switch;
    private DbsField pnd_After_Record_Switch;
    private DbsField pnd_Record_Found_Switch_1;
    private DbsField pnd_Record_Found_Switch_2;
    private DbsField pnd_Bfre_Aftr_Switch;
    private DbsField pnd_Bfre_Aftr_Code;

    private DbsGroup pnd_Bfre_Aftr_Code__R_Field_10;
    private DbsField pnd_Bfre_Aftr_Code_Pnd_Bfre_Aftr_Code_A;
    private DbsField pnd_Print_Num;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_Ia_Output_File;

    private DbsGroup pnd_Ia_Output_File__R_Field_11;
    private DbsField pnd_Ia_Output_File_Pnd_Record_Type;
    private DbsField pnd_Ia_Output_File_Pnd_Filler2;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Contract_Num;

    private DbsGroup pnd_Ia_Output_File__R_Field_12;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Contract_Num_1;
    private DbsField pnd_Ia_Output_File_Pnd_Filler3;
    private DbsField pnd_Ia_Output_File_Pnd_Filler4;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Payee_Code;
    private DbsField pnd_Ia_Output_File_Pnd_Filler5;
    private DbsField pnd_Ia_Output_File_Pnd_Da_Contract_Num;
    private DbsField pnd_Ia_Output_File_Pnd_Filler6;
    private DbsField pnd_Ia_Output_File_Pnd_Trans_Type;
    private DbsField pnd_Ia_Output_File_Pnd_Filler7;
    private DbsField pnd_Ia_Output_File_Pnd_Trans_Date;
    private DbsField pnd_Ia_Output_File_Pnd_Filler8;
    private DbsField pnd_Ia_Output_File_Pnd_Effective_Date;
    private DbsField pnd_Ia_Output_File_Pnd_Filler9;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Cycle_Date;
    private DbsField pnd_Ia_Output_File_Pnd_Filler10;
    private DbsField pnd_Ia_Output_File_Pnd_Fund_Sequence_Num;
    private DbsField pnd_Ia_Output_File_Pnd_Filler11;
    private DbsField pnd_Ia_Output_File_Pnd_Fund_Code;
    private DbsField pnd_Ia_Output_File_Pnd_Filler12;
    private DbsField pnd_Ia_Output_File_Pnd_Rate_Basis;
    private DbsField pnd_Ia_Output_File_Pnd_Filler13;
    private DbsField pnd_Ia_Output_File_Pnd_Reval_Ind;
    private DbsField pnd_Ia_Output_File_Pnd_Filler14;
    private DbsField pnd_Ia_Output_File_Pnd_Per_Units;

    private DbsGroup pnd_Ia_Output_File__R_Field_13;
    private DbsField pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_G;
    private DbsField pnd_Ia_Output_File_Pnd_Filler15;
    private DbsField pnd_Ia_Output_File_Pnd_Per_Pmt;

    private DbsGroup pnd_Ia_Output_File__R_Field_14;
    private DbsField pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_T;
    private DbsField pnd_Ia_Output_File_Pnd_Filler16;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Option;
    private DbsField pnd_Ia_Output_File_Pnd_Filler17;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Mode;
    private DbsField pnd_Ia_Output_File_Pnd_Filler18;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Origin;
    private DbsField pnd_Ia_Output_File_Pnd_Filler19;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Issue_Date;
    private DbsField pnd_Ia_Output_File_Pnd_Filler20;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Final_Pmt_Date;
    private DbsField pnd_Ia_Output_File_Pnd_Filler21;
    private DbsField pnd_Ia_Output_File_Pnd_First_Annt_Xref;
    private DbsField pnd_Ia_Output_File_Pnd_Filler22;
    private DbsField pnd_Ia_Output_File_Pnd_First_Annt_Dob;
    private DbsField pnd_Ia_Output_File_Pnd_Filler23;
    private DbsField pnd_Ia_Output_File_Pnd_First_Annt_Sex;
    private DbsField pnd_Ia_Output_File_Pnd_Filler24;
    private DbsField pnd_Ia_Output_File_Pnd_First_Annt_Dod;
    private DbsField pnd_Ia_Output_File_Pnd_Filler25;
    private DbsField pnd_Ia_Output_File_Pnd_Second_Annt_Xref;
    private DbsField pnd_Ia_Output_File_Pnd_Filler26;
    private DbsField pnd_Ia_Output_File_Pnd_Second_Annt_Dob;
    private DbsField pnd_Ia_Output_File_Pnd_Filler27;
    private DbsField pnd_Ia_Output_File_Pnd_Second_Annt_Sex;
    private DbsField pnd_Ia_Output_File_Pnd_Filler28;
    private DbsField pnd_Ia_Output_File_Pnd_Second_Annt_Dod;
    private DbsField pnd_Ia_Output_File_Pnd_Filler29;
    private DbsField pnd_Ia_Output_File_Pnd_Beneficiary_Annt_Xref;
    private DbsField pnd_Ia_Output_File_Pnd_Filler30;
    private DbsField pnd_Ia_Output_File_Pnd_Mode_Change_Ind;
    private DbsField pnd_Ia_Output_File_Pnd_Filler31;

    private DbsGroup pnd_Ia_Output_File__R_Field_15;
    private DbsField pnd_Ia_Output_File_Pnd_Ia_Output_Pos;
    private DbsField pnd_Cpr_Bfre_Key_Temp;

    private DbsGroup pnd_Cpr_Bfre_Key_Temp__R_Field_16;
    private DbsField pnd_Cpr_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp;
    private DbsField pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_B;
    private DbsField pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_B;
    private DbsField pnd_Cpr_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_B;
    private DbsField pnd_Cpr_Aftr_Key_Temp;

    private DbsGroup pnd_Cpr_Aftr_Key_Temp__R_Field_17;
    private DbsField pnd_Cpr_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp;
    private DbsField pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp;
    private DbsField pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp;
    private DbsField pnd_Cpr_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp;
    private DbsField pnd_Cntrct_Aftr_Key_Temp;

    private DbsGroup pnd_Cntrct_Aftr_Key_Temp__R_Field_18;
    private DbsField pnd_Cntrct_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_1;
    private DbsField pnd_Cntrct_Aftr_Key_Temp_Pnd_Cntrct_Ppcn_Nbr_Temp;
    private DbsField pnd_Cntrct_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_1;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp_2;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Fund_Bfre_Key_Temp_2;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp_B;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp_B;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_2;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_21;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_1;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_22;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_2;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Fund_Aftr_Key_Temp_2;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_2;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_25;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_1;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_3;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_26;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Aftr_Image_Key_End;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Fund_Aftr_Key_End_2;

    private DbsGroup pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Ppcn_Nbr_End;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Payee_Cde_End;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Invrse_Trans_Dte_End;
    private DbsField pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cmpny_Fund_Cde_End;
    private DbsField pnd_Cntrct_Payee_Key_Temp;

    private DbsGroup pnd_Cntrct_Payee_Key_Temp__R_Field_29;
    private DbsField pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_1;
    private DbsField pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_1;

    private DbsGroup pnd_Cntrct_Payee_Key_Temp__R_Field_30;
    private DbsField pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Num;
    private DbsField pnd_Parm_Card;

    private DbsGroup pnd_Parm_Card__R_Field_31;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date;

    private DbsGroup pnd_Parm_Card__R_Field_32;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm;

    private DbsGroup pnd_Parm_Card__R_Field_33;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Mm;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Dd;
    private DbsField pnd_Parm_Card_Pnd_Parm_Filler;
    private DbsField pnd_Parm_Check_Date_Next;

    private DbsGroup pnd_Parm_Check_Date_Next__R_Field_34;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm;

    private DbsGroup pnd_Parm_Check_Date_Next__R_Field_35;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd;
    private DbsField pnd_Eff_Date_Ccyymmdd_A;

    private DbsGroup pnd_Eff_Date_Ccyymmdd_A__R_Field_36;
    private DbsField pnd_Eff_Date_Ccyymmdd_A_Pnd_Eff_Date_Ccyymmdd;
    private DbsField pnd_Trans_Date_Ccyymmdd_A;

    private DbsGroup pnd_Trans_Date_Ccyymmdd_A__R_Field_37;
    private DbsField pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd;
    private DbsField pnd_Eof_Switch;
    private DbsField pnd_Tiaa_Cmpny_Fund_Cde_Temp_2;

    private DbsGroup pnd_Tiaa_Cmpny_Fund_Cde_Temp_2__R_Field_38;
    private DbsField pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1;
    private DbsField pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Filler29;
    private DbsField pnd_Ia_Date_Temp_A8;

    private DbsGroup pnd_Ia_Date_Temp_A8__R_Field_39;
    private DbsField pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp;

    private DbsGroup pnd_Ia_Date_Temp_A8__R_Field_40;
    private DbsField pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyymm;

    private DbsGroup pnd_Ia_Date_Temp_A8__R_Field_41;
    private DbsField pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyy;
    private DbsField pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm;
    private DbsField pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd;
    private DbsField pnd_Fnl_Prm_Count;
    private DbsField pnd_Aftr_Image_Key_Temp_2_A;

    private DbsGroup pnd_Aftr_Image_Key_Temp_2_A__R_Field_42;
    private DbsField pnd_Aftr_Image_Key_Temp_2_A_Pnd_Aftr_Image_Key_Temp_2_N;

    private DbsGroup pnd_Cref_Array;
    private DbsField pnd_Cref_Array_Pnd_Reval_Ind_Temp;
    private DbsField pnd_Cref_Array_Pnd_Per_Pmt_Temp;
    private DbsField pnd_Cref_Array_Pnd_Per_Units_Temp;
    private DbsField pnd_Days_Per_Month_Table;

    private DbsGroup pnd_Days_Per_Month_Table__R_Field_43;
    private DbsField pnd_Days_Per_Month_Table_Pnd_Days_Per_Month;
    private DbsField pnd_Temp_Ia_Contract_Num;

    private DbsGroup pnd_Temp_Ia_Contract_Num__R_Field_44;
    private DbsField pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8;
    private DbsField pnd_Temp_Ia_Contract_Num_Pnd_Filler30;
    private DbsField pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Fund_Sequence_Num_Temp;
    private DbsField pnd_Prev_Mode;
    private DbsField pnd_Mode_Change_Switch;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;

    private DbsGroup pls_Fund_Num_Cde__R_Field_45;
    private DbsField pls_Fund_Num_Cde_Pnd_Fund_Num_Cde_A;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0590 = new LdaAial0590();
        registerRecord(ldaAial0590);
        registerRecord(ldaAial0590.getVw_iaa_Trans_Rcrd_View());
        ldaAial0591 = new LdaAial0591();
        registerRecord(ldaAial0591);
        registerRecord(ldaAial0591.getVw_iaa_Cpr_Trans_View());
        ldaAial0592 = new LdaAial0592();
        registerRecord(ldaAial0592);
        registerRecord(ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View());
        ldaAial0594 = new LdaAial0594();
        registerRecord(ldaAial0594);
        registerRecord(ldaAial0594.getVw_iaa_Cntrct_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Header_Rec = localVariables.newFieldInRecord("pnd_Header_Rec", "#HEADER-REC", FieldType.STRING, 80);

        pnd_Header_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Header_Rec__R_Field_1", "REDEFINE", pnd_Header_Rec);
        pnd_Header_Rec_Pnd_Start_Date_A = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 
            8);

        pnd_Header_Rec__R_Field_2 = pnd_Header_Rec__R_Field_1.newGroupInGroup("pnd_Header_Rec__R_Field_2", "REDEFINE", pnd_Header_Rec_Pnd_Start_Date_A);
        pnd_Header_Rec_Pnd_Start_Date = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        pnd_Header_Rec_Pnd_Filler1 = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_End_Date_A = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);

        pnd_Header_Rec__R_Field_3 = pnd_Header_Rec__R_Field_1.newGroupInGroup("pnd_Header_Rec__R_Field_3", "REDEFINE", pnd_Header_Rec_Pnd_End_Date_A);
        pnd_Header_Rec_Pnd_End_Date = pnd_Header_Rec__R_Field_3.newFieldInGroup("pnd_Header_Rec_Pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);
        pnd_Header_Rec_Pnd_Filler_Ef1 = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_Filler_Ef1", "#FILLER-EF1", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_Start_Eff_Date_A = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Date_A", "#START-EFF-DATE-A", FieldType.STRING, 
            8);

        pnd_Header_Rec__R_Field_4 = pnd_Header_Rec__R_Field_1.newGroupInGroup("pnd_Header_Rec__R_Field_4", "REDEFINE", pnd_Header_Rec_Pnd_Start_Eff_Date_A);
        pnd_Header_Rec_Pnd_Start_Eff_Date = pnd_Header_Rec__R_Field_4.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Date", "#START-EFF-DATE", FieldType.NUMERIC, 
            8);
        pnd_Header_Rec_Pnd_Filler_Ef2 = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_Filler_Ef2", "#FILLER-EF2", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_End_Eff_Date_A = pnd_Header_Rec__R_Field_1.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Date_A", "#END-EFF-DATE-A", FieldType.STRING, 
            8);

        pnd_Header_Rec__R_Field_5 = pnd_Header_Rec__R_Field_1.newGroupInGroup("pnd_Header_Rec__R_Field_5", "REDEFINE", pnd_Header_Rec_Pnd_End_Eff_Date_A);
        pnd_Header_Rec_Pnd_End_Eff_Date = pnd_Header_Rec__R_Field_5.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Date", "#END-EFF-DATE", FieldType.NUMERIC, 
            8);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 16);

        pnd_Start_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_6", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Start_Key_Date = pnd_Start_Key__R_Field_6.newFieldInGroup("pnd_Start_Key_Pnd_Start_Key_Date", "#START-KEY-DATE", FieldType.NUMERIC, 
            8);
        pnd_Start_Key_Pnd_Filler31 = pnd_Start_Key__R_Field_6.newFieldInGroup("pnd_Start_Key_Pnd_Filler31", "#FILLER31", FieldType.STRING, 8);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 16);

        pnd_End_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_7", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Pnd_End_Key_Date = pnd_End_Key__R_Field_7.newFieldInGroup("pnd_End_Key_Pnd_End_Key_Date", "#END-KEY-DATE", FieldType.NUMERIC, 8);
        pnd_End_Key_Pnd_Filler32 = pnd_End_Key__R_Field_7.newFieldInGroup("pnd_End_Key_Pnd_Filler32", "#FILLER32", FieldType.STRING, 8);
        pnd_Check_Date_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd_A", "#CHECK-DATE-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_8", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd = pnd_Check_Date_Ccyymmdd_A__R_Field_8.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd", 
            "#CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_9", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm = pnd_Check_Date_Ccyymmdd_A__R_Field_9.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm", 
            "#CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd = pnd_Check_Date_Ccyymmdd_A__R_Field_9.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd", 
            "#CHECK-DATE-DD", FieldType.NUMERIC, 2);
        pnd_W_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Dte", "#W-TRANS-DTE", FieldType.STRING, 8);
        pnd_W_Trans_Dte_Next = localVariables.newFieldInRecord("pnd_W_Trans_Dte_Next", "#W-TRANS-DTE-NEXT", FieldType.STRING, 8);
        pnd_W_Trans_Dte_Prior = localVariables.newFieldInRecord("pnd_W_Trans_Dte_Prior", "#W-TRANS-DTE-PRIOR", FieldType.STRING, 8);
        pnd_W_Trans_Input_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Input_Dte", "#W-TRANS-INPUT-DTE", FieldType.STRING, 8);
        pnd_W_Count = localVariables.newFieldInRecord("pnd_W_Count", "#W-COUNT", FieldType.NUMERIC, 9);
        pnd_Num_Read = localVariables.newFieldInRecord("pnd_Num_Read", "#NUM-READ", FieldType.NUMERIC, 3);
        pnd_Num_Written = localVariables.newFieldInRecord("pnd_Num_Written", "#NUM-WRITTEN", FieldType.NUMERIC, 3);
        pnd_Record_Found_Switch = localVariables.newFieldInRecord("pnd_Record_Found_Switch", "#RECORD-FOUND-SWITCH", FieldType.STRING, 1);
        pnd_Before_Record_Switch = localVariables.newFieldInRecord("pnd_Before_Record_Switch", "#BEFORE-RECORD-SWITCH", FieldType.STRING, 1);
        pnd_After_Record_Switch = localVariables.newFieldInRecord("pnd_After_Record_Switch", "#AFTER-RECORD-SWITCH", FieldType.STRING, 1);
        pnd_Record_Found_Switch_1 = localVariables.newFieldInRecord("pnd_Record_Found_Switch_1", "#RECORD-FOUND-SWITCH-1", FieldType.STRING, 1);
        pnd_Record_Found_Switch_2 = localVariables.newFieldInRecord("pnd_Record_Found_Switch_2", "#RECORD-FOUND-SWITCH-2", FieldType.STRING, 1);
        pnd_Bfre_Aftr_Switch = localVariables.newFieldInRecord("pnd_Bfre_Aftr_Switch", "#BFRE-AFTR-SWITCH", FieldType.STRING, 1);
        pnd_Bfre_Aftr_Code = localVariables.newFieldInRecord("pnd_Bfre_Aftr_Code", "#BFRE-AFTR-CODE", FieldType.NUMERIC, 1);

        pnd_Bfre_Aftr_Code__R_Field_10 = localVariables.newGroupInRecord("pnd_Bfre_Aftr_Code__R_Field_10", "REDEFINE", pnd_Bfre_Aftr_Code);
        pnd_Bfre_Aftr_Code_Pnd_Bfre_Aftr_Code_A = pnd_Bfre_Aftr_Code__R_Field_10.newFieldInGroup("pnd_Bfre_Aftr_Code_Pnd_Bfre_Aftr_Code_A", "#BFRE-AFTR-CODE-A", 
            FieldType.STRING, 1);
        pnd_Print_Num = localVariables.newFieldInRecord("pnd_Print_Num", "#PRINT-NUM", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Ia_Output_File = localVariables.newFieldInRecord("pnd_Ia_Output_File", "#IA-OUTPUT-FILE", FieldType.STRING, 228);

        pnd_Ia_Output_File__R_Field_11 = localVariables.newGroupInRecord("pnd_Ia_Output_File__R_Field_11", "REDEFINE", pnd_Ia_Output_File);
        pnd_Ia_Output_File_Pnd_Record_Type = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Filler2 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Contract_Num = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Contract_Num", "#IA-CONTRACT-NUM", 
            FieldType.STRING, 8);

        pnd_Ia_Output_File__R_Field_12 = pnd_Ia_Output_File__R_Field_11.newGroupInGroup("pnd_Ia_Output_File__R_Field_12", "REDEFINE", pnd_Ia_Output_File_Pnd_Ia_Contract_Num);
        pnd_Ia_Output_File_Pnd_Ia_Contract_Num_1 = pnd_Ia_Output_File__R_Field_12.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Contract_Num_1", "#IA-CONTRACT-NUM-1", 
            FieldType.STRING, 1);
        pnd_Ia_Output_File_Pnd_Filler3 = pnd_Ia_Output_File__R_Field_12.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler3", "#FILLER3", FieldType.STRING, 
            7);
        pnd_Ia_Output_File_Pnd_Filler4 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler4", "#FILLER4", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Payee_Code = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Payee_Code", "#IA-PAYEE-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Output_File_Pnd_Filler5 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler5", "#FILLER5", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Da_Contract_Num = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Da_Contract_Num", "#DA-CONTRACT-NUM", 
            FieldType.STRING, 8);
        pnd_Ia_Output_File_Pnd_Filler6 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler6", "#FILLER6", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Trans_Type = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Trans_Type", "#TRANS-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Ia_Output_File_Pnd_Filler7 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler7", "#FILLER7", FieldType.STRING, 
            13);
        pnd_Ia_Output_File_Pnd_Trans_Date = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Trans_Date", "#TRANS-DATE", FieldType.NUMERIC, 
            8);
        pnd_Ia_Output_File_Pnd_Filler8 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler8", "#FILLER8", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Effective_Date = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Effective_Date", "#EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler9 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler9", "#FILLER9", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Cycle_Date = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Cycle_Date", "#IA-CYCLE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler10 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler10", "#FILLER10", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Fund_Sequence_Num = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Fund_Sequence_Num", "#FUND-SEQUENCE-NUM", 
            FieldType.NUMERIC, 2);
        pnd_Ia_Output_File_Pnd_Filler11 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler11", "#FILLER11", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Fund_Code = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Fund_Code", "#FUND-CODE", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Filler12 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler12", "#FILLER12", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Rate_Basis = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Rate_Basis", "#RATE-BASIS", FieldType.NUMERIC, 
            2);
        pnd_Ia_Output_File_Pnd_Filler13 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler13", "#FILLER13", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Reval_Ind = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Reval_Ind", "#REVAL-IND", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Filler14 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler14", "#FILLER14", FieldType.STRING, 
            15);
        pnd_Ia_Output_File_Pnd_Per_Units = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Per_Units", "#PER-UNITS", FieldType.NUMERIC, 
            10, 3);

        pnd_Ia_Output_File__R_Field_13 = pnd_Ia_Output_File__R_Field_11.newGroupInGroup("pnd_Ia_Output_File__R_Field_13", "REDEFINE", pnd_Ia_Output_File_Pnd_Per_Units);
        pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_G = pnd_Ia_Output_File__R_Field_13.newFieldInGroup("pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_G", "#PER-TIAA-PMT-G", 
            FieldType.NUMERIC, 10, 2);
        pnd_Ia_Output_File_Pnd_Filler15 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler15", "#FILLER15", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Per_Pmt = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Per_Pmt", "#PER-PMT", FieldType.NUMERIC, 
            9, 2);

        pnd_Ia_Output_File__R_Field_14 = pnd_Ia_Output_File__R_Field_11.newGroupInGroup("pnd_Ia_Output_File__R_Field_14", "REDEFINE", pnd_Ia_Output_File_Pnd_Per_Pmt);
        pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_T = pnd_Ia_Output_File__R_Field_14.newFieldInGroup("pnd_Ia_Output_File_Pnd_Per_Tiaa_Pmt_T", "#PER-TIAA-PMT-T", 
            FieldType.NUMERIC, 9, 2);
        pnd_Ia_Output_File_Pnd_Filler16 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler16", "#FILLER16", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Option = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Option", "#IA-OPTION", FieldType.NUMERIC, 
            2);
        pnd_Ia_Output_File_Pnd_Filler17 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler17", "#FILLER17", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Mode = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Mode", "#IA-MODE", FieldType.NUMERIC, 
            3);
        pnd_Ia_Output_File_Pnd_Filler18 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler18", "#FILLER18", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Origin = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Origin", "#IA-ORIGIN", FieldType.NUMERIC, 
            2);
        pnd_Ia_Output_File_Pnd_Filler19 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler19", "#FILLER19", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Issue_Date = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Issue_Date", "#IA-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler20 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler20", "#FILLER20", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Ia_Final_Pmt_Date = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Ia_Final_Pmt_Date", "#IA-FINAL-PMT-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Ia_Output_File_Pnd_Filler21 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler21", "#FILLER21", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_First_Annt_Xref = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_First_Annt_Xref", "#FIRST-ANNT-XREF", 
            FieldType.STRING, 9);
        pnd_Ia_Output_File_Pnd_Filler22 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler22", "#FILLER22", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_First_Annt_Dob = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_First_Annt_Dob", "#FIRST-ANNT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler23 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler23", "#FILLER23", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_First_Annt_Sex = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_First_Annt_Sex", "#FIRST-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Ia_Output_File_Pnd_Filler24 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler24", "#FILLER24", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_First_Annt_Dod = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_First_Annt_Dod", "#FIRST-ANNT-DOD", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler25 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler25", "#FILLER25", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Second_Annt_Xref = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Second_Annt_Xref", "#SECOND-ANNT-XREF", 
            FieldType.STRING, 9);
        pnd_Ia_Output_File_Pnd_Filler26 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler26", "#FILLER26", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Second_Annt_Dob = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Second_Annt_Dob", "#SECOND-ANNT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler27 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler27", "#FILLER27", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Second_Annt_Sex = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Second_Annt_Sex", "#SECOND-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Ia_Output_File_Pnd_Filler28 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler28", "#FILLER28", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Second_Annt_Dod = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Second_Annt_Dod", "#SECOND-ANNT-DOD", 
            FieldType.NUMERIC, 8);
        pnd_Ia_Output_File_Pnd_Filler29 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler29", "#FILLER29", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Beneficiary_Annt_Xref = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Beneficiary_Annt_Xref", 
            "#BENEFICIARY-ANNT-XREF", FieldType.STRING, 9);
        pnd_Ia_Output_File_Pnd_Filler30 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler30", "#FILLER30", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_Mode_Change_Ind = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Mode_Change_Ind", "#MODE-CHANGE-IND", 
            FieldType.STRING, 1);
        pnd_Ia_Output_File_Pnd_Filler31 = pnd_Ia_Output_File__R_Field_11.newFieldInGroup("pnd_Ia_Output_File_Pnd_Filler31", "#FILLER31", FieldType.STRING, 
            21);

        pnd_Ia_Output_File__R_Field_15 = localVariables.newGroupInRecord("pnd_Ia_Output_File__R_Field_15", "REDEFINE", pnd_Ia_Output_File);
        pnd_Ia_Output_File_Pnd_Ia_Output_Pos = pnd_Ia_Output_File__R_Field_15.newFieldArrayInGroup("pnd_Ia_Output_File_Pnd_Ia_Output_Pos", "#IA-OUTPUT-POS", 
            FieldType.STRING, 1, new DbsArrayController(1, 228));
        pnd_Cpr_Bfre_Key_Temp = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key_Temp", "#CPR-BFRE-KEY-TEMP", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key_Temp__R_Field_16 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key_Temp__R_Field_16", "REDEFINE", pnd_Cpr_Bfre_Key_Temp);
        pnd_Cpr_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp = pnd_Cpr_Bfre_Key_Temp__R_Field_16.newFieldInGroup("pnd_Cpr_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp", 
            "#BFRE-IMAGE-KEY-TEMP", FieldType.STRING, 1);
        pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_B = pnd_Cpr_Bfre_Key_Temp__R_Field_16.newFieldInGroup("pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_B", 
            "#CNTRCT-PART-PPCN-NBR-TEMP-B", FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_B = pnd_Cpr_Bfre_Key_Temp__R_Field_16.newFieldInGroup("pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_B", 
            "#CNTRCT-PART-PAYEE-CDE-TEMP-B", FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_B = pnd_Cpr_Bfre_Key_Temp__R_Field_16.newFieldInGroup("pnd_Cpr_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_B", "#TRANS-DTE-TEMP-B", 
            FieldType.TIME);
        pnd_Cpr_Aftr_Key_Temp = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key_Temp", "#CPR-AFTR-KEY-TEMP", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key_Temp__R_Field_17 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key_Temp__R_Field_17", "REDEFINE", pnd_Cpr_Aftr_Key_Temp);
        pnd_Cpr_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp = pnd_Cpr_Aftr_Key_Temp__R_Field_17.newFieldInGroup("pnd_Cpr_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp", 
            "#AFTR-IMAGE-KEY-TEMP", FieldType.STRING, 1);
        pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp = pnd_Cpr_Aftr_Key_Temp__R_Field_17.newFieldInGroup("pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp", 
            "#CNTRCT-PART-PPCN-NBR-TEMP", FieldType.STRING, 10);
        pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp = pnd_Cpr_Aftr_Key_Temp__R_Field_17.newFieldInGroup("pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp", 
            "#CNTRCT-PART-PAYEE-CDE-TEMP", FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp = pnd_Cpr_Aftr_Key_Temp__R_Field_17.newFieldInGroup("pnd_Cpr_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp", 
            "#INVRSE-TRANS-DTE-TEMP", FieldType.NUMERIC, 12);
        pnd_Cntrct_Aftr_Key_Temp = localVariables.newFieldInRecord("pnd_Cntrct_Aftr_Key_Temp", "#CNTRCT-AFTR-KEY-TEMP", FieldType.STRING, 25);

        pnd_Cntrct_Aftr_Key_Temp__R_Field_18 = localVariables.newGroupInRecord("pnd_Cntrct_Aftr_Key_Temp__R_Field_18", "REDEFINE", pnd_Cntrct_Aftr_Key_Temp);
        pnd_Cntrct_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_1 = pnd_Cntrct_Aftr_Key_Temp__R_Field_18.newFieldInGroup("pnd_Cntrct_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_1", 
            "#AFTR-IMAGE-KEY-TEMP-1", FieldType.STRING, 1);
        pnd_Cntrct_Aftr_Key_Temp_Pnd_Cntrct_Ppcn_Nbr_Temp = pnd_Cntrct_Aftr_Key_Temp__R_Field_18.newFieldInGroup("pnd_Cntrct_Aftr_Key_Temp_Pnd_Cntrct_Ppcn_Nbr_Temp", 
            "#CNTRCT-PPCN-NBR-TEMP", FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_1 = pnd_Cntrct_Aftr_Key_Temp__R_Field_18.newFieldInGroup("pnd_Cntrct_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_1", 
            "#INVRSE-TRANS-DTE-TEMP-1", FieldType.NUMERIC, 12);
        pnd_Tiaa_Fund_Bfre_Key_Temp = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Bfre_Key_Temp", "#TIAA-FUND-BFRE-KEY-TEMP", FieldType.STRING, 28);

        pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19", "REDEFINE", pnd_Tiaa_Fund_Bfre_Key_Temp);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp_2 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp_2", 
            "#BFRE-IMAGE-KEY-TEMP-2", FieldType.STRING, 1);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Fund_Bfre_Key_Temp_2 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Fund_Bfre_Key_Temp_2", 
            "#TIAA-FUND-BFRE-KEY-TEMP-2", FieldType.STRING, 22);

        pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_19.newGroupInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20", "REDEFINE", 
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Fund_Bfre_Key_Temp_2);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp_B = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp_B", 
            "#TIAA-CNTRCT-PPCN-NBR-TEMP-B", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp_B = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp_B", 
            "#TIAA-CNTRCT-PAYEE-CDE-TEMP-B", FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_2 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_2", 
            "#TRANS-DTE-TEMP-2", FieldType.TIME);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-B", FieldType.STRING, 3);

        pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_21 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newGroupInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_21", "REDEFINE", 
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_1 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_21.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_1", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-B-1", FieldType.STRING, 1);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_21.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-B-2-3", FieldType.NUMERIC, 2);

        pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_22 = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_20.newGroupInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_22", "REDEFINE", 
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B);
        pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B = pnd_Tiaa_Fund_Bfre_Key_Temp__R_Field_22.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-NUM-B", FieldType.NUMERIC, 3);
        pnd_Tiaa_Fund_Aftr_Key_Temp = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Aftr_Key_Temp", "#TIAA-FUND-AFTR-KEY-TEMP", FieldType.STRING, 28);

        pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23", "REDEFINE", pnd_Tiaa_Fund_Aftr_Key_Temp);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_2 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_2", 
            "#AFTR-IMAGE-KEY-TEMP-2", FieldType.STRING, 1);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Fund_Aftr_Key_Temp_2 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Fund_Aftr_Key_Temp_2", 
            "#TIAA-FUND-AFTR-KEY-TEMP-2", FieldType.STRING, 27);

        pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_23.newGroupInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24", "REDEFINE", 
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Fund_Aftr_Key_Temp_2);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp", 
            "#TIAA-CNTRCT-PPCN-NBR-TEMP", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp", 
            "#TIAA-CNTRCT-PAYEE-CDE-TEMP", FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_2 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_2", 
            "#INVRSE-TRANS-DTE-TEMP-2", FieldType.NUMERIC, 12);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp", 
            "#TIAA-CMPNY-FUND-CDE-TEMP", FieldType.STRING, 3);

        pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_25 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newGroupInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_25", "REDEFINE", 
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_1 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_25.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_1", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-1", FieldType.STRING, 1);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_3 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_25.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_3", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-2-3", FieldType.STRING, 2);

        pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_26 = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_24.newGroupInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_26", "REDEFINE", 
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp);
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num = pnd_Tiaa_Fund_Aftr_Key_Temp__R_Field_26.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num", 
            "#TIAA-CMPNY-FUND-CDE-TEMP-NUM", FieldType.NUMERIC, 3);
        pnd_Tiaa_Fund_Aftr_Key_End = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Aftr_Key_End", "#TIAA-FUND-AFTR-KEY-END", FieldType.STRING, 28);

        pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27", "REDEFINE", pnd_Tiaa_Fund_Aftr_Key_End);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Aftr_Image_Key_End = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Aftr_Image_Key_End", 
            "#AFTR-IMAGE-KEY-END", FieldType.STRING, 1);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Fund_Aftr_Key_End_2 = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Fund_Aftr_Key_End_2", 
            "#TIAA-FUND-AFTR-KEY-END-2", FieldType.STRING, 27);

        pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28 = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_27.newGroupInGroup("pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28", "REDEFINE", 
            pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Fund_Aftr_Key_End_2);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Ppcn_Nbr_End = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Ppcn_Nbr_End", 
            "#TIAA-CNTRCT-PPCN-NBR-END", FieldType.STRING, 10);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Payee_Cde_End = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cntrct_Payee_Cde_End", 
            "#TIAA-CNTRCT-PAYEE-CDE-END", FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Invrse_Trans_Dte_End = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Invrse_Trans_Dte_End", 
            "#INVRSE-TRANS-DTE-END", FieldType.NUMERIC, 12);
        pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cmpny_Fund_Cde_End = pnd_Tiaa_Fund_Aftr_Key_End__R_Field_28.newFieldInGroup("pnd_Tiaa_Fund_Aftr_Key_End_Pnd_Tiaa_Cmpny_Fund_Cde_End", 
            "#TIAA-CMPNY-FUND-CDE-END", FieldType.STRING, 3);
        pnd_Cntrct_Payee_Key_Temp = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key_Temp", "#CNTRCT-PAYEE-KEY-TEMP", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key_Temp__R_Field_29 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key_Temp__R_Field_29", "REDEFINE", pnd_Cntrct_Payee_Key_Temp);
        pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_1 = pnd_Cntrct_Payee_Key_Temp__R_Field_29.newFieldInGroup("pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_1", 
            "#CNTRCT-PART-PPCN-NBR-TEMP-1", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_1 = pnd_Cntrct_Payee_Key_Temp__R_Field_29.newFieldInGroup("pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_1", 
            "#CNTRCT-PART-PAYEE-CDE-TEMP-1", FieldType.STRING, 2);

        pnd_Cntrct_Payee_Key_Temp__R_Field_30 = pnd_Cntrct_Payee_Key_Temp__R_Field_29.newGroupInGroup("pnd_Cntrct_Payee_Key_Temp__R_Field_30", "REDEFINE", 
            pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_1);
        pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Num = pnd_Cntrct_Payee_Key_Temp__R_Field_30.newFieldInGroup("pnd_Cntrct_Payee_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Num", 
            "#CNTRCT-PART-PAYEE-CDE-NUM", FieldType.NUMERIC, 2);
        pnd_Parm_Card = localVariables.newFieldInRecord("pnd_Parm_Card", "#PARM-CARD", FieldType.STRING, 80);

        pnd_Parm_Card__R_Field_31 = localVariables.newGroupInRecord("pnd_Parm_Card__R_Field_31", "REDEFINE", pnd_Parm_Card);
        pnd_Parm_Card_Pnd_Parm_Check_Date = pnd_Parm_Card__R_Field_31.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date", "#PARM-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Parm_Card__R_Field_32 = pnd_Parm_Card__R_Field_31.newGroupInGroup("pnd_Parm_Card__R_Field_32", "REDEFINE", pnd_Parm_Card_Pnd_Parm_Check_Date);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm = pnd_Parm_Card__R_Field_32.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm", "#PARM-CHECK-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);

        pnd_Parm_Card__R_Field_33 = pnd_Parm_Card__R_Field_32.newGroupInGroup("pnd_Parm_Card__R_Field_33", "REDEFINE", pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy = pnd_Parm_Card__R_Field_33.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy", "#PARM-CHECK-DATE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Mm = pnd_Parm_Card__R_Field_33.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Mm", "#PARM-CHECK-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Dd = pnd_Parm_Card__R_Field_32.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Dd", "#PARM-CHECK-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Card_Pnd_Parm_Filler = pnd_Parm_Card__R_Field_31.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Filler", "#PARM-FILLER", FieldType.STRING, 72);
        pnd_Parm_Check_Date_Next = localVariables.newFieldInRecord("pnd_Parm_Check_Date_Next", "#PARM-CHECK-DATE-NEXT", FieldType.NUMERIC, 8);

        pnd_Parm_Check_Date_Next__R_Field_34 = localVariables.newGroupInRecord("pnd_Parm_Check_Date_Next__R_Field_34", "REDEFINE", pnd_Parm_Check_Date_Next);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm = pnd_Parm_Check_Date_Next__R_Field_34.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm", 
            "#PARM-CHECK-DATE-NEXT-CCYYMM", FieldType.NUMERIC, 6);

        pnd_Parm_Check_Date_Next__R_Field_35 = pnd_Parm_Check_Date_Next__R_Field_34.newGroupInGroup("pnd_Parm_Check_Date_Next__R_Field_35", "REDEFINE", 
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy = pnd_Parm_Check_Date_Next__R_Field_35.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy", 
            "#PARM-CHECK-DATE-NEXT-CCYY", FieldType.NUMERIC, 4);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm = pnd_Parm_Check_Date_Next__R_Field_35.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm", 
            "#PARM-CHECK-DATE-NEXT-MM", FieldType.NUMERIC, 2);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd = pnd_Parm_Check_Date_Next__R_Field_34.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd", 
            "#PARM-CHECK-DATE-NEXT-DD", FieldType.NUMERIC, 2);
        pnd_Eff_Date_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Eff_Date_Ccyymmdd_A", "#EFF-DATE-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Eff_Date_Ccyymmdd_A__R_Field_36 = localVariables.newGroupInRecord("pnd_Eff_Date_Ccyymmdd_A__R_Field_36", "REDEFINE", pnd_Eff_Date_Ccyymmdd_A);
        pnd_Eff_Date_Ccyymmdd_A_Pnd_Eff_Date_Ccyymmdd = pnd_Eff_Date_Ccyymmdd_A__R_Field_36.newFieldInGroup("pnd_Eff_Date_Ccyymmdd_A_Pnd_Eff_Date_Ccyymmdd", 
            "#EFF-DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Trans_Date_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Trans_Date_Ccyymmdd_A", "#TRANS-DATE-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Trans_Date_Ccyymmdd_A__R_Field_37 = localVariables.newGroupInRecord("pnd_Trans_Date_Ccyymmdd_A__R_Field_37", "REDEFINE", pnd_Trans_Date_Ccyymmdd_A);
        pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd = pnd_Trans_Date_Ccyymmdd_A__R_Field_37.newFieldInGroup("pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd", 
            "#TRANS-DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Eof_Switch = localVariables.newFieldInRecord("pnd_Eof_Switch", "#EOF-SWITCH", FieldType.STRING, 1);
        pnd_Tiaa_Cmpny_Fund_Cde_Temp_2 = localVariables.newFieldInRecord("pnd_Tiaa_Cmpny_Fund_Cde_Temp_2", "#TIAA-CMPNY-FUND-CDE-TEMP-2", FieldType.STRING, 
            3);

        pnd_Tiaa_Cmpny_Fund_Cde_Temp_2__R_Field_38 = localVariables.newGroupInRecord("pnd_Tiaa_Cmpny_Fund_Cde_Temp_2__R_Field_38", "REDEFINE", pnd_Tiaa_Cmpny_Fund_Cde_Temp_2);
        pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1 = pnd_Tiaa_Cmpny_Fund_Cde_Temp_2__R_Field_38.newFieldInGroup("pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1", 
            "#TIAA-CMPNY-TEMP-CDE-1", FieldType.STRING, 1);
        pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Filler29 = pnd_Tiaa_Cmpny_Fund_Cde_Temp_2__R_Field_38.newFieldInGroup("pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Filler29", 
            "#FILLER29", FieldType.STRING, 2);
        pnd_Ia_Date_Temp_A8 = localVariables.newFieldInRecord("pnd_Ia_Date_Temp_A8", "#IA-DATE-TEMP-A8", FieldType.STRING, 8);

        pnd_Ia_Date_Temp_A8__R_Field_39 = localVariables.newGroupInRecord("pnd_Ia_Date_Temp_A8__R_Field_39", "REDEFINE", pnd_Ia_Date_Temp_A8);
        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp = pnd_Ia_Date_Temp_A8__R_Field_39.newFieldInGroup("pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp", "#IA-DATE-TEMP", 
            FieldType.NUMERIC, 8);

        pnd_Ia_Date_Temp_A8__R_Field_40 = pnd_Ia_Date_Temp_A8__R_Field_39.newGroupInGroup("pnd_Ia_Date_Temp_A8__R_Field_40", "REDEFINE", pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp);
        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyymm = pnd_Ia_Date_Temp_A8__R_Field_40.newFieldInGroup("pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyymm", "#IA-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);

        pnd_Ia_Date_Temp_A8__R_Field_41 = pnd_Ia_Date_Temp_A8__R_Field_40.newGroupInGroup("pnd_Ia_Date_Temp_A8__R_Field_41", "REDEFINE", pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyymm);
        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyy = pnd_Ia_Date_Temp_A8__R_Field_41.newFieldInGroup("pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyy", "#IA-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm = pnd_Ia_Date_Temp_A8__R_Field_41.newFieldInGroup("pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm", "#IA-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd = pnd_Ia_Date_Temp_A8__R_Field_40.newFieldInGroup("pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd", "#IA-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Fnl_Prm_Count = localVariables.newFieldInRecord("pnd_Fnl_Prm_Count", "#FNL-PRM-COUNT", FieldType.NUMERIC, 2);
        pnd_Aftr_Image_Key_Temp_2_A = localVariables.newFieldInRecord("pnd_Aftr_Image_Key_Temp_2_A", "#AFTR-IMAGE-KEY-TEMP-2-A", FieldType.STRING, 1);

        pnd_Aftr_Image_Key_Temp_2_A__R_Field_42 = localVariables.newGroupInRecord("pnd_Aftr_Image_Key_Temp_2_A__R_Field_42", "REDEFINE", pnd_Aftr_Image_Key_Temp_2_A);
        pnd_Aftr_Image_Key_Temp_2_A_Pnd_Aftr_Image_Key_Temp_2_N = pnd_Aftr_Image_Key_Temp_2_A__R_Field_42.newFieldInGroup("pnd_Aftr_Image_Key_Temp_2_A_Pnd_Aftr_Image_Key_Temp_2_N", 
            "#AFTR-IMAGE-KEY-TEMP-2-N", FieldType.NUMERIC, 1);

        pnd_Cref_Array = localVariables.newGroupArrayInRecord("pnd_Cref_Array", "#CREF-ARRAY", new DbsArrayController(1, 2));
        pnd_Cref_Array_Pnd_Reval_Ind_Temp = pnd_Cref_Array.newFieldInGroup("pnd_Cref_Array_Pnd_Reval_Ind_Temp", "#REVAL-IND-TEMP", FieldType.STRING, 1);
        pnd_Cref_Array_Pnd_Per_Pmt_Temp = pnd_Cref_Array.newFieldInGroup("pnd_Cref_Array_Pnd_Per_Pmt_Temp", "#PER-PMT-TEMP", FieldType.NUMERIC, 9, 2);
        pnd_Cref_Array_Pnd_Per_Units_Temp = pnd_Cref_Array.newFieldInGroup("pnd_Cref_Array_Pnd_Per_Units_Temp", "#PER-UNITS-TEMP", FieldType.NUMERIC, 
            10, 3);
        pnd_Days_Per_Month_Table = localVariables.newFieldInRecord("pnd_Days_Per_Month_Table", "#DAYS-PER-MONTH-TABLE", FieldType.NUMERIC, 24);

        pnd_Days_Per_Month_Table__R_Field_43 = localVariables.newGroupInRecord("pnd_Days_Per_Month_Table__R_Field_43", "REDEFINE", pnd_Days_Per_Month_Table);
        pnd_Days_Per_Month_Table_Pnd_Days_Per_Month = pnd_Days_Per_Month_Table__R_Field_43.newFieldArrayInGroup("pnd_Days_Per_Month_Table_Pnd_Days_Per_Month", 
            "#DAYS-PER-MONTH", FieldType.NUMERIC, 2, new DbsArrayController(1, 12));
        pnd_Temp_Ia_Contract_Num = localVariables.newFieldInRecord("pnd_Temp_Ia_Contract_Num", "#TEMP-IA-CONTRACT-NUM", FieldType.STRING, 10);

        pnd_Temp_Ia_Contract_Num__R_Field_44 = localVariables.newGroupInRecord("pnd_Temp_Ia_Contract_Num__R_Field_44", "REDEFINE", pnd_Temp_Ia_Contract_Num);
        pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8 = pnd_Temp_Ia_Contract_Num__R_Field_44.newFieldInGroup("pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8", 
            "#TEMP-IA-CONTRACT-NUM-1-8", FieldType.STRING, 8);
        pnd_Temp_Ia_Contract_Num_Pnd_Filler30 = pnd_Temp_Ia_Contract_Num__R_Field_44.newFieldInGroup("pnd_Temp_Ia_Contract_Num_Pnd_Filler30", "#FILLER30", 
            FieldType.STRING, 2);
        pnd_Save_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", FieldType.STRING, 8);
        pnd_Fund_Sequence_Num_Temp = localVariables.newFieldInRecord("pnd_Fund_Sequence_Num_Temp", "#FUND-SEQUENCE-NUM-TEMP", FieldType.NUMERIC, 2);
        pnd_Prev_Mode = localVariables.newFieldInRecord("pnd_Prev_Mode", "#PREV-MODE", FieldType.NUMERIC, 3);
        pnd_Mode_Change_Switch = localVariables.newFieldInRecord("pnd_Mode_Change_Switch", "#MODE-CHANGE-SWITCH", FieldType.STRING, 1);
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));

        pls_Fund_Num_Cde__R_Field_45 = localVariables.newGroupInRecord("pls_Fund_Num_Cde__R_Field_45", "REDEFINE", pls_Fund_Num_Cde);
        pls_Fund_Num_Cde_Pnd_Fund_Num_Cde_A = pls_Fund_Num_Cde__R_Field_45.newFieldArrayInGroup("pls_Fund_Num_Cde_Pnd_Fund_Num_Cde_A", "#FUND-NUM-CDE-A", 
            FieldType.STRING, 2, new DbsArrayController(1, 20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();

        ldaAial0590.initializeValues();
        ldaAial0591.initializeValues();
        ldaAial0592.initializeValues();
        ldaAial0594.initializeValues();

        localVariables.reset();
        pnd_Num_Read.setInitialValue(0);
        pnd_Num_Written.setInitialValue(0);
        pnd_Record_Found_Switch_1.setInitialValue("N");
        pnd_Record_Found_Switch_2.setInitialValue("N");
        pnd_Print_Num.setInitialValue(0);
        pnd_Fund_1.setInitialValue("R");
        pnd_Eof_Switch.setInitialValue("N");
        pnd_Fnl_Prm_Count.setInitialValue(0);
        pnd_Days_Per_Month_Table.setInitialValue(-2147483648);
        pnd_Save_Ppcn_Nbr.setInitialValue("        ");
        pnd_Mode_Change_Switch.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Aiap008() throws Exception
    {
        super("Aiap008");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  WRITE 'UPDT AIAP'                                                                                                                                            //Natural: FORMAT ( 01 ) LS = 132 PS = 60;//Natural: FORMAT ( 02 ) LS = 132 PS = 55;//Natural: FORMAT ( 03 ) LS = 132 PS = 60;//Natural: FORMAT ( 04 ) LS = 132 PS = 60
        //*  READ WORK FILE 2 ONCE RECORD #HEADER-REC
        //*  AT END
        //*   ESCAPE ROUTINE
        //*  END-ENDFILE
        //*  04/15 START - GET FUND CODE FROM EXTERNALIZATION
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtn_Cde);                                                                  //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTN-CDE
        if (condition(Global.isEscape())) return;
        //*  04/15 END
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Header_Rec_Pnd_Start_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #START-DATE-A
            pnd_Header_Rec_Pnd_End_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #END-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Start_Key_Pnd_Start_Key_Date.setValue(pnd_Header_Rec_Pnd_Start_Date);                                                                                         //Natural: MOVE #START-DATE TO #START-KEY-DATE
        pnd_End_Key_Pnd_End_Key_Date.setValue(pnd_Header_Rec_Pnd_End_Date);                                                                                               //Natural: MOVE #END-DATE TO #END-KEY-DATE
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd.setValue(Global.getDATN());                                                                                     //Natural: MOVE *DATN TO #CHECK-DATE-CCYYMMDD #W-TRANS-DTE
        pnd_W_Trans_Dte.setValue(Global.getDATN());
        pnd_Parm_Check_Date_Next.setValue(pnd_Parm_Card_Pnd_Parm_Check_Date);                                                                                             //Natural: MOVE #PARM-CHECK-DATE TO #PARM-CHECK-DATE-NEXT
        if (condition(pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.equals(12)))                                                                                   //Natural: IF #PARM-CHECK-DATE-NEXT-MM = 12
        {
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy.nadd(1);                                                                                               //Natural: ADD 1 TO #PARM-CHECK-DATE-NEXT-CCYY
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.setValue(1);                                                                                             //Natural: MOVE 1 TO #PARM-CHECK-DATE-NEXT-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.nadd(1);                                                                                                 //Natural: ADD 1 TO #PARM-CHECK-DATE-NEXT-MM
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ02")))
        {
            pnd_Header_Rec_Pnd_Start_Eff_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #START-EFF-DATE-A
            pnd_Header_Rec_Pnd_End_Eff_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #END-EFF-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAial0590.getVw_iaa_Trans_Rcrd_View().startDatabaseRead                                                                                                         //Natural: READ IAA-TRANS-RCRD-VIEW BY TRANS-CHCK-DTE-KEY STARTING FROM #START-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Start_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ03:
        while (condition(ldaAial0590.getVw_iaa_Trans_Rcrd_View().readNextRow("READ03")))
        {
            if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Check_Dte().greater(pnd_Start_Key_Pnd_Start_Key_Date)))                                                //Natural: IF TRANS-CHECK-DTE GT #START-KEY-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*    STARTING FROM #START-KEY THRU #END-KEY
            //*     STARTING FROM '19980605Z0088808' THRU '19980605Z0088809'
            //*    STARTING FROM '19980201Y       ' THRU '199811016N999999'
            //*    STARTING FROM '19980201G       '
            //*    STARTING FROM '19980501Y       '
            //*     STARTING FROM '199805010' THRU '199809016N999999'
            pnd_Trans_Date_Ccyymmdd_A.setValueEdited(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DATE-CCYYMMDD-A
            //*     ACCEPT IF (TRANS-CDE = 20 OR = 33  OR = 35 OR = 36 OR = 40 OR = 50
            //*  ADDED 1/98
            if (condition(!(((((((((((ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(35) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(36))             //Natural: ACCEPT IF ( ( TRANS-CDE = 35 OR = 36 OR = 40 OR = 51 OR = 52 OR = 53 OR = 61 OR = 62 ) OR ( TRANS-CDE = 20 OR = 33 AND TRANS-PAYEE-CDE = 01 ) OR ( TRANS-CDE = 50 AND TRANS-PAYEE-CDE < 03 ) ) AND TRANS-VERIFY-CDE = ' '
                || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(40)) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(51)) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(52)) 
                || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(53)) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(61)) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(62)) 
                || ((ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(20) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(33)) && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde().equals(1))) 
                || (ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(50) && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde().less(3))) && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Verify_Cde().equals(" ")))))
            {
                continue;
            }
            //*  TRANS-PAYEE-CDE = 01
            //*    AND
            //*    AND
            //*    ((TRANS-TODAYS-DTE > #START-EFF-DATE OR
            //*  TRANS-TODAYS-DTE = #START-EFF-DATE)
            //*    OR (TRANS-TODAYS-DTE = 0 AND (#TRANS-DATE-CCYYMMDD >
            //*    #START-EFF-DATE OR #TRANS-DATE-CCYYMMDD =  #START-EFF-DATE)))
            //*    AND
            //*    ((TRANS-TODAYS-DTE < #END-EFF-DATE OR
            //*  TRANS-TODAYS-DTE = #END-EFF-DATE)
            //*    OR (TRANS-TODAYS-DTE = 0 AND (#TRANS-DATE-CCYYMMDD <
            //*    #END-EFF-DATE OR #TRANS-DATE-CCYYMMDD =  #END-EFF-DATE)))
            //*    (TRANS-TODAYS-DTE > 19980330
            //*    OR (TRANS-TODAYS-DTE = 0 AND #TRANS-DATE-CCYYMMDD > 19980330))
            //*  022018 - START ==> ACCELERATED ANNUITIZATION TO BYPASS
            if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_User_Id().equals("PIA2250D") && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(33)              //Natural: IF TRANS-USER-ID = 'PIA2250D' AND TRANS-CDE = 33 AND TRANS-SUB-CDE = 'ACC'
                && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Sub_Cde().equals("ACC")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  022018 - END
            if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte().equals(getZero())))                                                                       //Natural: IF TRANS-TODAYS-DTE = 0
            {
                if (condition(pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd.notEquals(pnd_Header_Rec_Pnd_Start_Eff_Date) || pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd.greater(pnd_Header_Rec_Pnd_End_Eff_Date))) //Natural: IF #TRANS-DATE-CCYYMMDD NE #START-EFF-DATE OR #TRANS-DATE-CCYYMMDD > #END-EFF-DATE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte().notEquals(pnd_Header_Rec_Pnd_Start_Eff_Date) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte().greater(pnd_Header_Rec_Pnd_End_Eff_Date))) //Natural: IF TRANS-TODAYS-DTE NE #START-EFF-DATE OR TRANS-TODAYS-DTE > #END-EFF-DATE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 1/98
            if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Actvty_Cde().equals("R")))                                                                             //Natural: IF TRANS-ACTVTY-CDE = 'R'
            {
                //*  ADDED 1/98
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  ADDED 1/98
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 228
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(228)); pnd_I.nadd(1))
            {
                pnd_Ia_Output_File_Pnd_Ia_Output_Pos.getValue(pnd_I).setValue(" ");                                                                                       //Natural: MOVE ' ' TO #IA-OUTPUT-POS ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE (5)  IAA-TRANS-RCRD-VIEW
            pnd_Temp_Ia_Contract_Num.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                                                       //Natural: MOVE TRANS-PPCN-NBR TO #TEMP-IA-CONTRACT-NUM
            //*  WRITE '#TEMP-IA-CONTRACT-NUM-1-8 = ' #TEMP-IA-CONTRACT-NUM-1-8
            //*  WRITE '#SAVE-PPCN-NBR = ' #SAVE-PPCN-NBR
            //*  WRITE '**** BEFORE IF CONDITION ******'
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            if (condition(pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8.equals(pnd_Save_Ppcn_Nbr)))                                                               //Natural: IF #TEMP-IA-CONTRACT-NUM-1-8 = #SAVE-PPCN-NBR
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fund_Sequence_Num_Temp.setValue(0);                                                                                                                   //Natural: MOVE 0 TO #FUND-SEQUENCE-NUM-TEMP
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            pnd_Ia_Output_File_Pnd_Record_Type.setValue("I");                                                                                                             //Natural: MOVE 'I' TO #RECORD-TYPE
            pnd_Ia_Output_File_Pnd_Ia_Contract_Num.setValue(pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8);                                                       //Natural: MOVE #TEMP-IA-CONTRACT-NUM-1-8 TO #IA-CONTRACT-NUM #SAVE-PPCN-NBR
            pnd_Save_Ppcn_Nbr.setValue(pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8);
            //*  IF #IA-CONTRACT-NUM-1 = '0' OR = '6' OR = 'Z' OR = 'Y'
            //*  IF #IA-CONTRACT-NUM = 'Z0047283' OR = 'Z0058546'
            //*    IGNORE
            //*  ELSE
            //*    ESCAPE TOP
            //*  END-IF
            //*  WRITE TRANS-PPCN-NBR TRANS-CDE TRANS-TODAYS-DTE #TRANS-DATE-CCYYMMDD
            //*  WRITE TRANS-PPCN-NBR TRANS-TODAYS-DTE
            //*  WRITE 'TRANS-CHCK-DTE-KEY = ' TRANS-CHCK-DTE-KEY
            //*  ADD 1 TO #NUM-READ
            //*  IF #NUM-READ > 100
            //*    ESCAPE ROUTINE
            //*  END-IF
            getReports().write(0, "CONTRACT IS ",pnd_Temp_Ia_Contract_Num_Pnd_Temp_Ia_Contract_Num_1_8,"  ");                                                             //Natural: WRITE 'CONTRACT IS ' #TEMP-IA-CONTRACT-NUM-1-8 '  '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fnl_Prm_Count.setValue(0);                                                                                                                                //Natural: MOVE 0 TO #FNL-PRM-COUNT
            pnd_Ia_Output_File_Pnd_Ia_Payee_Code.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde());                                                          //Natural: MOVE TRANS-PAYEE-CDE TO #IA-PAYEE-CODE
            pnd_Ia_Output_File_Pnd_Trans_Type.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde());                                                                   //Natural: MOVE TRANS-CDE TO #TRANS-TYPE
            pnd_Ia_Output_File_Pnd_Ia_Cycle_Date.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Check_Dte());                                                          //Natural: MOVE TRANS-CHECK-DTE TO #IA-CYCLE-DATE
            if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte().equals(getZero())))                                                                       //Natural: IF TRANS-TODAYS-DTE = 0
            {
                pnd_Ia_Output_File_Pnd_Trans_Date.setValue(pnd_Trans_Date_Ccyymmdd_A_Pnd_Trans_Date_Ccyymmdd);                                                            //Natural: MOVE #TRANS-DATE-CCYYMMDD TO #TRANS-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ia_Output_File_Pnd_Trans_Date.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte());                                                        //Natural: MOVE TRANS-TODAYS-DTE TO #TRANS-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Eff_Date_Ccyymmdd_A.setValueEdited(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Effective_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED TRANS-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #EFF-DATE-CCYYMMDD-A
            //*  MOVE #EFF-DATE-CCYYMMDD TO #EFF-DATE
            //*  WRITE 'TRANS-PPCN-NBR     = ' TRANS-PPCN-NBR
            //*  WRITE 'TRANS-PAYEE-CDE    = ' TRANS-PAYEE-CDE
            //*  WRITE 'TRANS-CDE          = ' TRANS-CDE
            //*  WRITE 'TRANS-CHECK-DTE    = ' TRANS-CHECK-DTE
            //*  WRITE 'TRANS-TODAYS-DTE   = ' TRANS-TODAYS-DTE
            //*  WRITE '#EFF-DATE-CCYYMMDD = ' #EFF-DATE-CCYYMMDD
            pnd_Prev_Mode.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #PREV-MODE
            pnd_Mode_Change_Switch.setValue("N");                                                                                                                         //Natural: MOVE 'N' TO #MODE-CHANGE-SWITCH
            pnd_Cpr_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp.setValue(2);                                                                                                    //Natural: MOVE 2 TO #AFTR-IMAGE-KEY-TEMP
            pnd_Cpr_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp.setValue(1);                                                                                                    //Natural: MOVE 1 TO #BFRE-IMAGE-KEY-TEMP
            pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                            //Natural: MOVE TRANS-PPCN-NBR TO #CNTRCT-PART-PPCN-NBR-TEMP #CNTRCT-PART-PPCN-NBR-TEMP-B
            pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Ppcn_Nbr_Temp_B.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());
            pnd_Cpr_Aftr_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde());                                          //Natural: MOVE TRANS-PAYEE-CDE TO #CNTRCT-PART-PAYEE-CDE-TEMP #CNTRCT-PART-PAYEE-CDE-TEMP-B
            pnd_Cpr_Bfre_Key_Temp_Pnd_Cntrct_Part_Payee_Cde_Temp_B.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde());
            pnd_Cpr_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_B.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Dte());                                                          //Natural: MOVE TRANS-DTE TO #TRANS-DTE-TEMP-B
            pnd_Cpr_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Invrse_Trans_Dte());                                              //Natural: MOVE INVRSE-TRANS-DTE TO #INVRSE-TRANS-DTE-TEMP
            ldaAial0591.getVw_iaa_Cpr_Trans_View().startDatabaseFind                                                                                                      //Natural: FIND IAA-CPR-TRANS-VIEW WITH CPR-BFRE-KEY = #CPR-BFRE-KEY-TEMP
            (
            "FIND01",
            new Wc[] { new Wc("CPR_BFRE_KEY", "=", pnd_Cpr_Bfre_Key_Temp.getBinary(), WcType.WITH) }
            );
            FIND01:
            while (condition(ldaAial0591.getVw_iaa_Cpr_Trans_View().readNextRow("FIND01", true)))
            {
                ldaAial0591.getVw_iaa_Cpr_Trans_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAial0591.getVw_iaa_Cpr_Trans_View().getAstCOUNTER().equals(0)))                                                                          //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO CPR RECORD FOUND BFR ",ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                                //Natural: WRITE 'NO CPR RECORD FOUND BFR ' TRANS-PPCN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ignore();
                }                                                                                                                                                         //Natural: END-NOREC
                //*    WRITE (6) 'ONE' IAA-CPR-TRANS-VIEW
                pnd_Prev_Mode.setValue(ldaAial0591.getIaa_Cpr_Trans_View_Cntrct_Mode_Ind());                                                                              //Natural: MOVE CNTRCT-MODE-IND TO #PREV-MODE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAial0591.getVw_iaa_Cpr_Trans_View().startDatabaseFind                                                                                                      //Natural: FIND IAA-CPR-TRANS-VIEW WITH CPR-AFTR-KEY = #CPR-AFTR-KEY-TEMP
            (
            "FIND02",
            new Wc[] { new Wc("CPR_AFTR_KEY", "=", pnd_Cpr_Aftr_Key_Temp, WcType.WITH) }
            );
            FIND02:
            while (condition(ldaAial0591.getVw_iaa_Cpr_Trans_View().readNextRow("FIND02", true)))
            {
                ldaAial0591.getVw_iaa_Cpr_Trans_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAial0591.getVw_iaa_Cpr_Trans_View().getAstCOUNTER().equals(0)))                                                                          //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO CPR RECORD FOUND FOR ",ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                                //Natural: WRITE 'NO CPR RECORD FOUND FOR ' TRANS-PPCN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                //*    WRITE (6) 'TWO' IAA-CPR-TRANS-VIEW
                pnd_Ia_Output_File_Pnd_Ia_Mode.setValue(ldaAial0591.getIaa_Cpr_Trans_View_Cntrct_Mode_Ind());                                                             //Natural: MOVE CNTRCT-MODE-IND TO #IA-MODE
                pnd_Ia_Output_File_Pnd_Ia_Final_Pmt_Date.setValue(ldaAial0591.getIaa_Cpr_Trans_View_Cntrct_Final_Per_Pay_Dte());                                          //Natural: MOVE CNTRCT-FINAL-PER-PAY-DTE TO #IA-FINAL-PMT-DATE
                pnd_Ia_Output_File_Pnd_Beneficiary_Annt_Xref.setValue(ldaAial0591.getIaa_Cpr_Trans_View_Bnfcry_Xref_Ind());                                               //Natural: MOVE BNFCRY-XREF-IND TO #BENEFICIARY-ANNT-XREF
                if (condition(pnd_Ia_Output_File_Pnd_Ia_Mode.notEquals(getZero()) && pnd_Ia_Output_File_Pnd_Ia_Mode.notEquals(pnd_Prev_Mode)))                            //Natural: IF #IA-MODE NOT = 0 AND #IA-MODE NOT = #PREV-MODE
                {
                    pnd_Mode_Change_Switch.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #MODE-CHANGE-SWITCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MOVE 2 TO #AFTR-IMAGE-KEY-TEMP-1
            //*  MOVE TRANS-PPCN-NBR TO #CNTRCT-PPCN-NBR-TEMP
            //*  MOVE INVRSE-TRANS-DTE TO #INVRSE-TRANS-DTE-TEMP-1
            //*  FIND IAA-CNTRCT-TRANS-VIEW
            //*      WITH CNTRCT-AFTR-KEY = #CNTRCT-AFTR-KEY-TEMP
            //*    IF NO RECORDS FOUND
            //*      WRITE 'NO CONTRACT RECORD FOUND FOR ' #CNTRCT-AFTR-KEY-TEMP
            //*      ESCAPE TOP
            //*    END-NOREC
            ldaAial0594.getVw_iaa_Cntrct_View().startDatabaseFind                                                                                                         //Natural: FIND IAA-CNTRCT-VIEW WITH CNTRCT-PPCN-NBR = TRANS-PPCN-NBR
            (
            "FIND03",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr(), WcType.WITH) }
            );
            FIND03:
            while (condition(ldaAial0594.getVw_iaa_Cntrct_View().readNextRow("FIND03", true)))
            {
                ldaAial0594.getVw_iaa_Cntrct_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAial0594.getVw_iaa_Cntrct_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO IAA-CNTRCT RECORD FOUND FOR ",ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                         //Natural: WRITE 'NO IAA-CNTRCT RECORD FOUND FOR ' TRANS-PPCN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                //*    WRITE (7) IAA-CNTRCT-VIEW
                pnd_Ia_Output_File_Pnd_Ia_Option.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Optn_Cde());                                                              //Natural: MOVE CNTRCT-OPTN-CDE TO #IA-OPTION
                pnd_Ia_Output_File_Pnd_Ia_Origin.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Orgn_Cde());                                                              //Natural: MOVE CNTRCT-ORGN-CDE TO #IA-ORIGIN
                pnd_Ia_Output_File_Pnd_First_Annt_Xref.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_First_Annt_Xref_Ind());                                             //Natural: MOVE CNTRCT-FIRST-ANNT-XREF-IND TO #FIRST-ANNT-XREF
                pnd_Ia_Output_File_Pnd_First_Annt_Dob.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_First_Annt_Dob_Dte());                                               //Natural: MOVE CNTRCT-FIRST-ANNT-DOB-DTE TO #FIRST-ANNT-DOB
                pnd_Ia_Output_File_Pnd_First_Annt_Sex.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_First_Annt_Sex_Cde());                                               //Natural: MOVE CNTRCT-FIRST-ANNT-SEX-CDE TO #FIRST-ANNT-SEX
                pnd_Ia_Output_File_Pnd_First_Annt_Dod.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_First_Annt_Dod_Dte());                                               //Natural: MOVE CNTRCT-FIRST-ANNT-DOD-DTE TO #FIRST-ANNT-DOD
                pnd_Ia_Output_File_Pnd_Second_Annt_Xref.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Scnd_Annt_Xref_Ind());                                             //Natural: MOVE CNTRCT-SCND-ANNT-XREF-IND TO #SECOND-ANNT-XREF
                pnd_Ia_Output_File_Pnd_Second_Annt_Dob.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Scnd_Annt_Dob_Dte());                                               //Natural: MOVE CNTRCT-SCND-ANNT-DOB-DTE TO #SECOND-ANNT-DOB
                pnd_Ia_Output_File_Pnd_Second_Annt_Sex.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Scnd_Annt_Sex_Cde());                                               //Natural: MOVE CNTRCT-SCND-ANNT-SEX-CDE TO #SECOND-ANNT-SEX
                pnd_Ia_Output_File_Pnd_Second_Annt_Dod.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Scnd_Annt_Dod_Dte());                                               //Natural: MOVE CNTRCT-SCND-ANNT-DOD-DTE TO #SECOND-ANNT-DOD
                if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(60) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(61)))                    //Natural: IF TRANS-CDE = 60 OR = 61
                {
                    pnd_Fnl_Prm_Count.nadd(1);                                                                                                                            //Natural: ADD 1 TO #FNL-PRM-COUNT
                    if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Todays_Dte().less(19980401)))                                                                  //Natural: IF TRANS-TODAYS-DTE < 19980401
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ia_Date_Temp_A8.setValueEdited(ldaAial0594.getIaa_Cntrct_View_Cntrct_Fnl_Prm_Dte().getValue(pnd_Fnl_Prm_Count),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CNTRCT-FNL-PRM-DTE ( #FNL-PRM-COUNT ) ( EM = YYYYMMDD ) TO #IA-DATE-TEMP-A8
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyymm.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Issue_Dte());                                                   //Natural: MOVE CNTRCT-ISSUE-DTE TO #IA-DATE-YYYYMM
                    if (condition(ldaAial0594.getIaa_Cntrct_View_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                //Natural: IF CNTRCT-ISSUE-DTE-DD = 0
                    {
                        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd.setValue(1);                                                                                                   //Natural: MOVE 1 TO #IA-DATE-DD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Issue_Dte_Dd());                                                //Natural: MOVE CNTRCT-ISSUE-DTE-DD TO #IA-DATE-DD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ia_Output_File_Pnd_Ia_Issue_Date.setValue(pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp);                                                                      //Natural: MOVE #IA-DATE-TEMP TO #IA-ISSUE-DATE
                pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd.nsubtract(1);                                                                                                          //Natural: SUBTRACT 1 FROM #IA-DATE-DD
                if (condition(pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd.equals(getZero())))                                                                                      //Natural: IF #IA-DATE-DD = 0
                {
                    pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm.nsubtract(1);                                                                                                      //Natural: SUBTRACT 1 FROM #IA-DATE-MM
                    if (condition(pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm.equals(getZero())))                                                                                  //Natural: IF #IA-DATE-MM = 0
                    {
                        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm.setValue(12);                                                                                                  //Natural: MOVE 12 TO #IA-DATE-MM
                        pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Yyyy.nsubtract(1);                                                                                                //Natural: SUBTRACT 1 FROM #IA-DATE-YYYY
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Dd.setValue(pnd_Days_Per_Month_Table_Pnd_Days_Per_Month.getValue(pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Mm));                //Natural: MOVE #DAYS-PER-MONTH ( #IA-DATE-MM ) TO #IA-DATE-DD
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(30) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(31) ||                   //Natural: IF TRANS-CDE = 30 OR = 31 OR = 33
                    ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(33)))
                {
                    pnd_Ia_Output_File_Pnd_Effective_Date.setValue(pnd_Ia_Date_Temp_A8_Pnd_Ia_Date_Temp);                                                                 //Natural: MOVE #IA-DATE-TEMP TO #EFFECTIVE-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ia_Output_File_Pnd_Effective_Date.setValue(pnd_Ia_Output_File_Pnd_Trans_Date);                                                                    //Natural: MOVE #TRANS-DATE TO #EFFECTIVE-DATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ia_Output_File_Pnd_Da_Contract_Num.setValue(ldaAial0594.getIaa_Cntrct_View_Cntrct_Orig_Da_Cntrct_Nbr());                                              //Natural: MOVE CNTRCT-ORIG-DA-CNTRCT-NBR TO #DA-CONTRACT-NUM
                //*    WRITE 'CNTRCT-OPTN-CDE = ' CNTRCT-OPTN-CDE
                //*    WRITE 'CNTRCT-ORGN-CDE = ' CNTRCT-ORGN-CDE
                //*    WRITE 'CNTRCT-ISSUE-DTE = ' CNTRCT-ISSUE-DTE
                //*    WRITE 'CNTRCT-FIRST-ANNT-XREF-IND = ' CNTRCT-FIRST-ANNT-XREF-IND
                //*    WRITE 'CNTRCT-FIRST-ANNT-DOB-DTE  = ' CNTRCT-FIRST-ANNT-DOB-DTE
                //*    WRITE 'CNTRCT-FIRST-ANNT-SEX-CDE  = ' CNTRCT-FIRST-ANNT-SEX-CDE
                //*    WRITE 'CNTRCT-FIRST-ANNT-DOD-DTE  = ' CNTRCT-FIRST-ANNT-DOD-DTE
                //*    WRITE 'CNTRCT-SCND-ANNT-XREF-IND  = ' CNTRCT-SCND-ANNT-XREF-IND
                //*    WRITE 'CNTRCT-SCND-ANNT-DOB-DTE   = ' CNTRCT-SCND-ANNT-DOB-DTE
                //*    WRITE 'CNTRCT-SCND-ANNT-SEX-CDE   = ' CNTRCT-SCND-ANNT-SEX-CDE
                //*    WRITE 'CNTRCT-SCND-ANNT-DOD-DTE   = ' CNTRCT-SCND-ANNT-DOB-DTE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp_2.setValue("1");                                                                                          //Natural: MOVE '1' TO #BFRE-IMAGE-KEY-TEMP-2
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_2.setValue("2");                                                                                          //Natural: MOVE '2' TO #AFTR-IMAGE-KEY-TEMP-2
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());                                      //Natural: MOVE TRANS-PPCN-NBR TO #TIAA-CNTRCT-PPCN-NBR-TEMP #TIAA-CNTRCT-PPCN-NBR-TEMP-B
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp_B.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr());
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde());                                    //Natural: MOVE TRANS-PAYEE-CDE TO #TIAA-CNTRCT-PAYEE-CDE-TEMP #TIAA-CNTRCT-PAYEE-CDE-TEMP-B
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp_B.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Payee_Cde());
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Trans_Dte_Temp_2.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Dte());                                                    //Natural: MOVE TRANS-DTE TO #TRANS-DTE-TEMP-2
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Invrse_Trans_Dte_Temp_2.setValue(ldaAial0590.getIaa_Trans_Rcrd_View_Invrse_Trans_Dte());                                      //Natural: MOVE INVRSE-TRANS-DTE TO #INVRSE-TRANS-DTE-TEMP-2
            pnd_Before_Record_Switch.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #BEFORE-RECORD-SWITCH #AFTER-RECORD-SWITCH
            pnd_After_Record_Switch.setValue("N");
            pnd_Bfre_Aftr_Switch.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #BFRE-AFTR-SWITCH
            FOR02:                                                                                                                                                        //Natural: FOR #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B = 202 TO 210
            for (pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.setValue(202); condition(pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.lessOrEqual(210)); 
                pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.nadd(1))
            {
                FOR03:                                                                                                                                                    //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
                for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
                {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                    sub_Find_Fund_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MOVE 'U' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-1
            //*  MOVE '09' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-2-3
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B.setValue("U09");                                                                                   //Natural: MOVE 'U09' TO #TIAA-CMPNY-FUND-CDE-TEMP-B
            FOR04:                                                                                                                                                        //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
            for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                sub_Find_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED FOR TIAA ACCESS
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B.setValue("U11");                                                                                   //Natural: MOVE 'U11' TO #TIAA-CMPNY-FUND-CDE-TEMP-B
            FOR05:                                                                                                                                                        //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
            for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                sub_Find_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_1.setValue("U");                                                                                   //Natural: MOVE 'U' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-1
            FOR06:                                                                                                                                                        //Natural: FOR #TIAA-CMPNY-FUND-CDE-TEMP-B-2-3 = 41 TO 49
            for (pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.setValue(41); condition(pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.lessOrEqual(49)); 
                pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.nadd(1))
            {
                FOR07:                                                                                                                                                    //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
                for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
                {
                    //*      WRITE #TIAA-CMPNY-FUND-CDE-TEMP-B
                    //*      WRITE #BFRE-AFTR-CODE
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                    sub_Find_Fund_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF #RECORD-FOUND-SWITCH-1 = 'N' AND #RECORD-FOUND-SWITCH-2 = 'N'
            //*      #BEFORE-RECORD-SWITCH
            //*      #AFTER-RECORD-SWITCH
            pnd_Before_Record_Switch.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #BEFORE-RECORD-SWITCH #AFTER-RECORD-SWITCH
            pnd_After_Record_Switch.setValue("N");
            FOR08:                                                                                                                                                        //Natural: FOR #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B = 402 TO 410
            for (pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.setValue(402); condition(pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.lessOrEqual(410)); 
                pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B.nadd(1))
            {
                FOR09:                                                                                                                                                    //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
                for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
                {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                    sub_Find_Fund_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MOVE 'W' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-1
            //*  MOVE '09' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-2-3
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B.setValue("W09");                                                                                   //Natural: MOVE 'W09' TO #TIAA-CMPNY-FUND-CDE-TEMP-B
            FOR10:                                                                                                                                                        //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
            for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                sub_Find_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED FOR TIAA ACCESS
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B.setValue("W11");                                                                                   //Natural: MOVE 'W11' TO #TIAA-CMPNY-FUND-CDE-TEMP-B
            FOR11:                                                                                                                                                        //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
            for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                sub_Find_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_1.setValue("W");                                                                                   //Natural: MOVE 'W' TO #TIAA-CMPNY-FUND-CDE-TEMP-B-1
            FOR12:                                                                                                                                                        //Natural: FOR #TIAA-CMPNY-FUND-CDE-TEMP-B-2-3 = 41 TO 49
            for (pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.setValue(41); condition(pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.lessOrEqual(49)); 
                pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_B_2_3.nadd(1))
            {
                FOR13:                                                                                                                                                    //Natural: FOR #BFRE-AFTR-CODE = 1 TO 2
                for (pnd_Bfre_Aftr_Code.setValue(1); condition(pnd_Bfre_Aftr_Code.lessOrEqual(2)); pnd_Bfre_Aftr_Code.nadd(1))
                {
                    //*      WRITE #TIAA-CMPNY-FUND-CDE-TEMP-B
                    //*      WRITE #BFRE-AFTR-CODE
                                                                                                                                                                          //Natural: PERFORM FIND-FUND-RECORD
                    sub_Find_Fund_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ELSE
            //*    MOVE 'N' TO #BFRE-AFTR-SWITCH
            //*  WRITE '#BEFORE-RECORD-SWITCH = ' #BEFORE-RECORD-SWITCH
            //*    IF #BEFORE-RECORD-SWITCH = 'N'
            //*      MOVE 'N' TO #RECORD-FOUND-SWITCH-2
            //*      MOVE 1 TO #BFRE-AFTR-CODE
            //*      FOR #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B = 402 TO 410
            //*        PERFORM FIND-FUND-RECORD
            //*      END-FOR
            //*    ELSE
            //*  WRITE '#AFTER-RECORD-SWITCH = ' #AFTER-RECORD-SWITCH
            //*      IF #AFTER-RECORD-SWITCH = 'N'
            //*        MOVE 'N' TO #RECORD-FOUND-SWITCH-1
            //*        MOVE 2 TO #BFRE-AFTR-CODE
            //*        FOR #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B = 402 TO 410
            //*          PERFORM FIND-FUND-RECORD
            //*        END-FOR
            //*      END-IF
            //*    END-IF
            //*  END-IF
            //*  END-REPEAT
            pnd_W_Trans_Input_Dte.setValueEdited(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #W-TRANS-INPUT-DTE
            //*  WRITE 'TRANS-CHECK-DTE = ' TRANS-CHECK-DTE
            //*  WRITE '#CHECK-DATE-CCYYMMDD = ' #CHECK-DATE-CCYYMMDD
            //*  IF TRANS-CHECK-DTE > #CHECK-DATE-CCYYMMDD
            //*    ESCAPE BOTTOM
            //*  END-IF
            pnd_W_Count.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #W-COUNT
            //*  WRITE 'PPCN / CHECK DATE: ' TRANS-PPCN-NBR ' ' TRANS-CHECK-DTE
            //*   WRITE WORK 01 IAA-TRANS-RCRD-VIEW
            //*  END-FIND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ACCEPTED: ",pnd_W_Count);                                                                                                            //Natural: WRITE 'TOTAL ACCEPTED: ' #W-COUNT
        if (Global.isEscape()) return;
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-FUND-RECORD
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORDS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FUND-RECORD
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REVAL-CODE
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORDS-BY-REVAL
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-OUTPUT-FILE
    }
    private void sub_Find_Fund_Record() throws Exception                                                                                                                  //Natural: FIND-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'FIND-FUND-RECORD'
        //*  WRITE '#TIAA-CMPNY-FUND-CDE-TEMP-NUM-B = '
        //*   #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B
        //*  WRITE '#BFRE-AFTR-CODE = ' #BFRE-AFTR-CODE
        pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num.setValue(pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Tiaa_Cmpny_Fund_Cde_Temp_Num_B);                            //Natural: MOVE #TIAA-CMPNY-FUND-CDE-TEMP-NUM-B TO #TIAA-CMPNY-FUND-CDE-TEMP-NUM
        //*  WRITE '#TIAA-FUND-AFTR-KEY-TEMP = ' #TIAA-FUND-AFTR-KEY-TEMP
        //*  WRITE '#TIAA-FUND-AFTR-KEY-END  = ' #TIAA-FUND-AFTR-KEY-END
        if (condition((pnd_Bfre_Aftr_Code.equals(1) && ((ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(30) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(31))  //Natural: IF #BFRE-AFTR-CODE = 1 AND TRANS-CDE = 30 OR = 31 OR = 33
            || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(33)))))
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Bfre_Aftr_Code.equals(1)))                                                                                                                      //Natural: IF #BFRE-AFTR-CODE = 1
        {
            pnd_Tiaa_Fund_Bfre_Key_Temp_Pnd_Bfre_Image_Key_Temp_2.setValue("1");                                                                                          //Natural: MOVE '1' TO #BFRE-IMAGE-KEY-TEMP-2
            ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().startDatabaseFind                                                                                                //Natural: FIND IAA-TIAA-FUND-TRANS-VIEW WITH TIAA-FUND-BFRE-KEY-2 = #TIAA-FUND-BFRE-KEY-TEMP
            (
            "FIND04",
            new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", "=", pnd_Tiaa_Fund_Bfre_Key_Temp.getBinary(), WcType.WITH) }
            );
            FIND04:
            while (condition(ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().readNextRow("FIND04", true)))
            {
                ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().getAstCOUNTER().equals(0)))                                                                    //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO FUND RECORD FOUND FOR ",pnd_Tiaa_Fund_Bfre_Key_Temp);                                                                       //Natural: WRITE 'NO FUND RECORD FOUND FOR ' #TIAA-FUND-BFRE-KEY-TEMP
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        ADD 1 TO #TIAA-CMPNY-FUND-CDE-TEMP-NUM
                    pnd_Record_Found_Switch_1.setValue("N");                                                                                                              //Natural: MOVE 'N' TO #RECORD-FOUND-SWITCH-1
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*    WRITE (8) 'ONE' TRANS-DTE TIAA-CNTRCT-PPCN-NBR TIAA-CNTRCT-PAYEE-CDE
                //*      TIAA-CMPNY-FUND-CDE TIAA-TOT-PER-AMT TIAA-UNITS-CNT (1)
                //*      TIAA-FUND-BFRE-KEY TIAA-FUND-BFRE-KEY-2
                pnd_Record_Found_Switch_1.setValue("Y");                                                                                                                  //Natural: MOVE 'Y' TO #RECORD-FOUND-SWITCH-1 #BEFORE-RECORD-SWITCH
                pnd_Before_Record_Switch.setValue("Y");
                                                                                                                                                                          //Natural: PERFORM MOVE-FUND-RECORD
                sub_Move_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Trans_Date_Ccyymmdd_A.setValueEdited(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DATE-CCYYMMDD-A
                //*        IF TRANS-CDE = 50
                //*          WRITE TIAA-CNTRCT-PPCN-NBR ' BEFORE ' #FUND-CODE
                //*            #TRANS-DATE-CCYYMMDD-A TIAA-UNITS-CNT (1) TIAA-TOT-PER-AMT
                //*        END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Aftr_Image_Key_Temp_2.setValue("2");                                                                                          //Natural: MOVE '2' TO #AFTR-IMAGE-KEY-TEMP-2
            ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().startDatabaseFind                                                                                                //Natural: FIND IAA-TIAA-FUND-TRANS-VIEW WITH TIAA-FUND-AFTR-KEY-2 = #TIAA-FUND-AFTR-KEY-TEMP
            (
            "FIND05",
            new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", "=", pnd_Tiaa_Fund_Aftr_Key_Temp, WcType.WITH) }
            );
            FIND05:
            while (condition(ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().readNextRow("FIND05", true)))
            {
                ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAial0592.getVw_iaa_Tiaa_Fund_Trans_View().getAstCOUNTER().equals(0)))                                                                    //Natural: IF NO RECORDS FOUND
                {
                    //*      WRITE 'NO FUND RECORD FOUND FOR ' #TIAA-FUND-AFTR-KEY-TEMP
                    //*        ADD 1 TO #TIAA-CMPNY-FUND-CDE-TEMP-NUM
                    pnd_Record_Found_Switch_2.setValue("N");                                                                                                              //Natural: MOVE 'N' TO #RECORD-FOUND-SWITCH-2
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*    WRITE (8) 'TWO' TRANS-DTE TIAA-CNTRCT-PPCN-NBR TIAA-CNTRCT-PAYEE-CDE
                //*      TIAA-CMPNY-FUND-CDE TIAA-TOT-PER-AMT TIAA-UNITS-CNT (1)
                //*      TIAA-FUND-AFTR-KEY  TIAA-FUND-AFTR-KEY-2
                pnd_Record_Found_Switch_2.setValue("Y");                                                                                                                  //Natural: MOVE 'Y' TO #RECORD-FOUND-SWITCH-2 #AFTER-RECORD-SWITCH
                pnd_After_Record_Switch.setValue("Y");
                                                                                                                                                                          //Natural: PERFORM MOVE-FUND-RECORD
                sub_Move_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Trans_Date_Ccyymmdd_A.setValueEdited(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DATE-CCYYMMDD-A
                //*    IF TRANS-CDE = 50
                //*      WRITE TIAA-CNTRCT-PPCN-NBR ' AFTER  ' #FUND-CODE
                //*        #TRANS-DATE-CCYYMMDD-A TIAA-UNITS-CNT (1) TIAA-TOT-PER-AMT
                //*    END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '#BFRE-AFTR-CODE = ' #BFRE-AFTR-CODE
        //*  WRITE '#BEFORE-RECORD-SWITCH = ' #BEFORE-RECORD-SWITCH
        //*  WRITE '#AFTER-RECORD-SWITCH  = ' #AFTER-RECORD-SWITCH
        if (condition(pnd_Bfre_Aftr_Code.equals(2) && (pnd_Before_Record_Switch.equals("Y") || pnd_After_Record_Switch.equals("Y"))))                                     //Natural: IF #BFRE-AFTR-CODE = 2 AND ( #BEFORE-RECORD-SWITCH = 'Y' OR #AFTER-RECORD-SWITCH = 'Y' )
        {
            pnd_Fund_Sequence_Num_Temp.nadd(1);                                                                                                                           //Natural: ADD 1 TO #FUND-SEQUENCE-NUM-TEMP
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            pnd_Before_Record_Switch.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #BEFORE-RECORD-SWITCH #AFTER-RECORD-SWITCH
            pnd_After_Record_Switch.setValue("N");
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORDS
            sub_Write_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Records() throws Exception                                                                                                                     //Natural: WRITE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'WRITE-RECORDS'
        //*  WRITE '#RECORD-FOUND-SWITCH-1 = ' #RECORD-FOUND-SWITCH-1
        //*  WRITE '#RECORD-FOUND-SWITCH-2 = ' #RECORD-FOUND-SWITCH-2
        //*  WRITE 'TRANS-CODE = ' TRANS-CDE
        if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().notEquals(50) && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().notEquals(51) &&                     //Natural: IF TRANS-CDE <> 50 AND TRANS-CDE <> 51 AND TRANS-CDE <> 61 AND TRANS-CDE <> 62 AND #RECORD-FOUND-SWITCH-2 = 'Y'
            ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().notEquals(61) && ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().notEquals(62) && pnd_Record_Found_Switch_2.equals("Y")))
        {
            pnd_Ia_Output_File_Pnd_Reval_Ind.setValue(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(2));                                                                     //Natural: MOVE #REVAL-IND-TEMP ( 2 ) TO #REVAL-IND
            short decideConditionsMet818 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF TRANS-CDE;//Natural: VALUE 20,40,52
            if (condition((ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(20) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(40) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(52))))
            {
                decideConditionsMet818++;
                pnd_Ia_Output_File_Pnd_Per_Units.compute(new ComputeParameters(false, pnd_Ia_Output_File_Pnd_Per_Units), DbsField.multiply(-1,pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(2))); //Natural: COMPUTE #PER-UNITS = ( -1 ) * #PER-UNITS-TEMP ( 2 )
                pnd_Ia_Output_File_Pnd_Per_Pmt.compute(new ComputeParameters(false, pnd_Ia_Output_File_Pnd_Per_Pmt), DbsField.multiply(-1,pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(2))); //Natural: COMPUTE #PER-PMT = ( -1 ) * #PER-PMT-TEMP ( 2 )
            }                                                                                                                                                             //Natural: VALUE 33,35,36,53
            else if (condition((ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(33) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(35) 
                || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(36) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(53))))
            {
                decideConditionsMet818++;
                pnd_Ia_Output_File_Pnd_Per_Units.setValue(pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(2));                                                                 //Natural: MOVE #PER-UNITS-TEMP ( 2 ) TO #PER-UNITS
                pnd_Ia_Output_File_Pnd_Per_Pmt.setValue(pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(2));                                                                     //Natural: MOVE #PER-PMT-TEMP ( 2 ) TO #PER-PMT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            pnd_Ia_Output_File_Pnd_Fund_Sequence_Num.setValue(pnd_Fund_Sequence_Num_Temp);                                                                                //Natural: MOVE #FUND-SEQUENCE-NUM-TEMP TO #FUND-SEQUENCE-NUM
            getWorkFiles().write(1, false, pnd_Ia_Output_File);                                                                                                           //Natural: WRITE WORK FILE 01 #IA-OUTPUT-FILE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #RECORD-FOUND-SWITCH-1 = 'Y' AND RECORD-FOUND-SWITCH-2 = 'Y'
        if (condition(ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(50) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(51) || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(61)  //Natural: IF ( TRANS-CDE = 50 OR = 51 OR = 61 OR = 62 )
            || ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(62)))
        {
            if (condition(pnd_Record_Found_Switch_1.equals("Y") && pnd_Record_Found_Switch_2.equals("Y")))                                                                //Natural: IF #RECORD-FOUND-SWITCH-1 = 'Y' AND #RECORD-FOUND-SWITCH-2 = 'Y'
            {
                if (condition(pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(1).equals(pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(2)) && pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(1).equals(pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(2)))) //Natural: IF #PER-UNITS-TEMP ( 1 ) = #PER-UNITS-TEMP ( 2 ) AND #PER-PMT-TEMP ( 1 ) = #PER-PMT-TEMP ( 2 )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(1).notEquals(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(2)) || (ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Cde().equals(50)  //Natural: IF #REVAL-IND-TEMP ( 1 ) <> #REVAL-IND-TEMP ( 2 ) OR ( TRANS-CDE = 50 AND #MODE-CHANGE-SWITCH = 'Y' )
                        && pnd_Mode_Change_Switch.equals("Y"))))
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORDS-BY-REVAL
                        sub_Write_Records_By_Reval();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ia_Output_File_Pnd_Reval_Ind.setValue(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(1));                                                         //Natural: MOVE #REVAL-IND-TEMP ( 1 ) TO #REVAL-IND
                        pnd_Ia_Output_File_Pnd_Per_Units.compute(new ComputeParameters(true, pnd_Ia_Output_File_Pnd_Per_Units), pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(2).subtract(pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(1))); //Natural: COMPUTE ROUNDED #PER-UNITS = #PER-UNITS-TEMP ( 2 ) - #PER-UNITS-TEMP ( 1 )
                        pnd_Ia_Output_File_Pnd_Per_Pmt.compute(new ComputeParameters(true, pnd_Ia_Output_File_Pnd_Per_Pmt), pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(2).subtract(pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(1))); //Natural: COMPUTE ROUNDED #PER-PMT = #PER-PMT-TEMP ( 2 ) - #PER-PMT-TEMP ( 1 )
                        //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
                        pnd_Ia_Output_File_Pnd_Fund_Sequence_Num.setValue(pnd_Fund_Sequence_Num_Temp);                                                                    //Natural: MOVE #FUND-SEQUENCE-NUM-TEMP TO #FUND-SEQUENCE-NUM
                        getWorkFiles().write(1, false, pnd_Ia_Output_File);                                                                                               //Natural: WRITE WORK FILE 01 #IA-OUTPUT-FILE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ELSE IF (#BFRE-AFTR-CODE = 1 AND #RECORD-FOUND-SWITCH-1 = 'Y')
                //*        OR (#BFRE-AFTR-CODE = 2 AND #RECORD-FOUND-SWITCH-2 = 'Y')
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Record_Found_Switch_1.equals("Y") || pnd_Record_Found_Switch_2.equals("Y")))                                                            //Natural: IF #RECORD-FOUND-SWITCH-1 = 'Y' OR #RECORD-FOUND-SWITCH-2 = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORDS-BY-REVAL
                    sub_Write_Records_By_Reval();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Fund_Record() throws Exception                                                                                                                  //Natural: MOVE-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'MOVE-FUND-RECORD'
        //*  WRITE '#BFRE-AFTR-CODE = ' #BFRE-AFTR-CODE
        pnd_Record_Found_Switch.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #RECORD-FOUND-SWITCH
        //*  WRITE '#RECORD-FOUND-SWITCH = ' #RECORD-FOUND-SWITCH
        //*  WRITE '#TIAA-CNTRCT-PPCN-NBR-TEMP  = ' #TIAA-CNTRCT-PPCN-NBR-TEMP
        //*  WRITE 'TIAA-CNTRCT-PPCN-NBR        = ' TIAA-CNTRCT-PPCN-NBR
        //*  WRITE '#TIAA-CNTRCT-PAYEE-CDE-TEMP = ' #TIAA-CNTRCT-PAYEE-CDE-TEMP
        //*  WRITE 'TIAA-CNTRCT-PAYEE-CDE       = ' TIAA-CNTRCT-PAYEE-CDE
        if (condition(pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Ppcn_Nbr_Temp.notEquals(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Ppcn_Nbr())                 //Natural: IF #TIAA-CNTRCT-PPCN-NBR-TEMP <> TIAA-CNTRCT-PPCN-NBR OR #TIAA-CNTRCT-PAYEE-CDE-TEMP <> TIAA-CNTRCT-PAYEE-CDE
            || pnd_Tiaa_Fund_Aftr_Key_Temp_Pnd_Tiaa_Cntrct_Payee_Cde_Temp.notEquals(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Cntrct_Payee_Cde())))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE 'TIAA-CMPNY-FUND-CDE = ' TIAA-CMPNY-FUND-CDE
        //*  WRITE 'TIAA-RATE-CDE (1)   = ' TIAA-RATE-CDE (1)
        //*  WRITE 'TIAA-TOT-PER-AMT    = ' TIAA-TOT-PER-AMT
        //*  WRITE 'TIAA-UNITS-CNT (1)  = ' TIAA-UNITS-CNT (1)
        //*    IF ((TRANS-CDE = 60 OR = 61) AND #AFTR-IMAGE-KEY-TEMP-2-N = 1)
        //*        OR (TRANS-CDE �= 60 AND TRANS-CDE �= 61 AND
        //*      #AFTR-IMAGE-KEY-TEMP-2-N = 2)
        //*  IF (#BFRE-AFTR-CODE = 1 AND TRANS-CDE = 50)
        //*      OR (#BFRE-AFTR-CODE = 2 AND TRANS-CDE �= 50)
        //*  ADD 1 TO #FUND-SEQUENCE-NUM
        //*  WRITE '#FUND-SEQUENCE-NUM = ' #FUND-SEQUENCE-NUM
        //*  04/15 - START - FUND LOOKUP USING EXTERNALIZATION
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        DbsUtil.examine(new ExamineSource(pls_Fund_Num_Cde_Pnd_Fund_Num_Cde_A.getValue("*"),true), new ExamineSearch(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde(),  //Natural: EXAMINE FULL #FUND-NUM-CDE-A ( * ) FOR FULL TIAA-FUND-CDE GIVING INDEX #J
            true), new ExamineGivingIndex(pnd_J));
        if (condition(pnd_J.greater(getZero())))                                                                                                                          //Natural: IF #J GT 0
        {
            pnd_Ia_Output_File_Pnd_Fund_Code.setValue(pls_Fund_Alpha_Cde.getValue(pnd_J));                                                                                //Natural: ASSIGN #FUND-CODE := +FUND-ALPHA-CDE ( #J )
            //*  05/15 CHANGE EXTERNALIZATION VALUE FOR ILB
            //*  05/15 FROM I TO F SO RECONNET CAN RECOGNIZE IT
            if (condition(pnd_Ia_Output_File_Pnd_Fund_Code.equals("I")))                                                                                                  //Natural: IF #FUND-CODE = 'I'
            {
                pnd_Ia_Output_File_Pnd_Fund_Code.setValue("F");                                                                                                           //Natural: ASSIGN #FUND-CODE := 'F'
                //*  05/15
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "INVALID FUND CODE:",ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Fund_Cde());                                                          //Natural: WRITE 'INVALID FUND CODE:' TIAA-FUND-CDE
            if (Global.isEscape()) return;
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *DECIDE ON FIRST VALUE OF TIAA-RATE-CDE (1)
        //* *  VALUE '41','40','38'
        //* *    MOVE 'C' TO #FUND-CODE
        //* *  VALUE '20'
        //* *    MOVE 'M' TO #FUND-CODE
        //* *  VALUE '19'
        //* *    MOVE 'B' TO #FUND-CODE
        //* *  VALUE '18'
        //* *    MOVE 'S' TO #FUND-CODE
        //* *  VALUE '17'
        //* *    MOVE 'W' TO #FUND-CODE
        //* *  VALUE '16'
        //* *    MOVE 'L' TO #FUND-CODE
        //* *  VALUE '15'
        //* *    MOVE 'E' TO #FUND-CODE
        //* *  VALUE '14'
        //* *    MOVE 'R' TO #FUND-CODE
        //* *  VALUE '13'
        //* *    MOVE 'F' TO #FUND-CODE
        //* *  VALUE '02'
        //* *    MOVE 'V' TO #FUND-CODE
        //* *  VALUE '03'
        //* *    MOVE 'N' TO #FUND-CODE
        //* *  VALUE '04'
        //* *    MOVE 'O' TO #FUND-CODE
        //* *  VALUE '05'
        //* *    MOVE 'P' TO #FUND-CODE
        //* *  VALUE '06'
        //* *    MOVE 'Q' TO #FUND-CODE
        //* *  VALUE '07'
        //* *    MOVE 'J' TO #FUND-CODE
        //* *  VALUE '08'
        //* *    MOVE 'K' TO #FUND-CODE
        //* *  VALUE '09'
        //* *    MOVE 'X' TO #FUND-CODE
        //* *  VALUE 'LA'
        //* *    MOVE 'D' TO #FUND-CODE
        //* *  NONE
        //* *    WRITE 'INVALID RATE BASIS ' TRANS-PPCN-NBR ' '
        //* *      TIAA-RATE-CDE (1)
        //*    ESCAPE BOTTOM
        //* *END-DECIDE
        //*  04/15 - END - FUND LOOKUP USING EXTERNALIZATION
        //*  MOVE TIAA-CMPNY-FUND-CDE TO #TIAA-CMPNY-FUND-CDE-TEMP-2
        //*  DECIDE ON FIRST VALUE OF #TIAA-CMPNY-TEMP-CDE-1
        //*    VALUE '2','U'
        //*      MOVE 'A' TO #REVAL-IND
        //*    VALUE '4','W'
        //*      MOVE 'M' TO #REVAL-IND
        //*    NONE
        //*      WRITE 'INVALID FUND CODE FOR ' TRANS-PPCN-NBR ' '
        //*        TIAA-CMPNY-FUND-CDE
        //*  END-DECIDE
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-REVAL-CODE
        sub_Check_Reval_Code();
        if (condition(Global.isEscape())) {return;}
        pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(pnd_Bfre_Aftr_Code).setValue(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Units_Cnt().getValue(1));                    //Natural: MOVE TIAA-UNITS-CNT ( 1 ) TO #PER-UNITS-TEMP ( #BFRE-AFTR-CODE )
        pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(pnd_Bfre_Aftr_Code).setValue(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Tot_Per_Amt());                                //Natural: MOVE TIAA-TOT-PER-AMT TO #PER-PMT-TEMP ( #BFRE-AFTR-CODE )
        //*  WRITE 'TIAA-UNITS-CNT (1) = ' TIAA-UNITS-CNT (1)
        //*  WRITE 'TIAA-TOT-PER-AMT   = ' TIAA-TOT-PER-AMT
        //*  WRITE #IA-CONTRACT-NUM
    }
    private void sub_Check_Reval_Code() throws Exception                                                                                                                  //Natural: CHECK-REVAL-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Tiaa_Cmpny_Fund_Cde_Temp_2.setValue(ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde());                                                           //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #TIAA-CMPNY-FUND-CDE-TEMP-2
        short decideConditionsMet969 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TIAA-CMPNY-TEMP-CDE-1;//Natural: VALUE '2','U'
        if (condition((pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1.equals("2") || pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1.equals("U"))))
        {
            decideConditionsMet969++;
            pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(pnd_Bfre_Aftr_Code).setValue("A");                                                                                 //Natural: MOVE 'A' TO #REVAL-IND-TEMP ( #BFRE-AFTR-CODE )
        }                                                                                                                                                                 //Natural: VALUE '4','W'
        else if (condition((pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1.equals("4") || pnd_Tiaa_Cmpny_Fund_Cde_Temp_2_Pnd_Tiaa_Cmpny_Temp_Cde_1.equals("W"))))
        {
            decideConditionsMet969++;
            pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(pnd_Bfre_Aftr_Code).setValue("M");                                                                                 //Natural: MOVE 'M' TO #REVAL-IND-TEMP ( #BFRE-AFTR-CODE )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, "INVALID FUND CODE FOR ",ldaAial0590.getIaa_Trans_Rcrd_View_Trans_Ppcn_Nbr()," ",ldaAial0592.getIaa_Tiaa_Fund_Trans_View_Tiaa_Cmpny_Fund_Cde()); //Natural: WRITE 'INVALID FUND CODE FOR ' TRANS-PPCN-NBR ' ' TIAA-CMPNY-FUND-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_Records_By_Reval() throws Exception                                                                                                            //Natural: WRITE-RECORDS-BY-REVAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'WRITE-RECORDS-BY-REVAL'
        //*  WRITE '#RECORD-FOUND-SWITCH-1 = ' #RECORD-FOUND-SWITCH-1
        //*  WRITE '#RECORD-FOUND-SWITCH-2 = ' #RECORD-FOUND-SWITCH-2
        if (condition(pnd_Mode_Change_Switch.equals("Y")))                                                                                                                //Natural: IF #MODE-CHANGE-SWITCH = 'Y'
        {
            pnd_Ia_Output_File_Pnd_Mode_Change_Ind.setValue("M");                                                                                                         //Natural: MOVE 'M' TO #MODE-CHANGE-IND
            pnd_Ia_Output_File_Pnd_Ia_Mode.setValue(pnd_Prev_Mode);                                                                                                       //Natural: MOVE #PREV-MODE TO #IA-MODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Record_Found_Switch_1.equals("Y")))                                                                                                             //Natural: IF #RECORD-FOUND-SWITCH-1 = 'Y'
        {
            pnd_Ia_Output_File_Pnd_Reval_Ind.setValue(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(1));                                                                     //Natural: MOVE #REVAL-IND-TEMP ( 1 ) TO #REVAL-IND
            pnd_Ia_Output_File_Pnd_Per_Units.compute(new ComputeParameters(false, pnd_Ia_Output_File_Pnd_Per_Units), DbsField.multiply(-1,pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(1))); //Natural: COMPUTE #PER-UNITS = ( -1 ) * #PER-UNITS-TEMP ( 1 )
            pnd_Ia_Output_File_Pnd_Per_Pmt.compute(new ComputeParameters(false, pnd_Ia_Output_File_Pnd_Per_Pmt), DbsField.multiply(-1,pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(1))); //Natural: COMPUTE #PER-PMT = ( -1 ) * #PER-PMT-TEMP ( 1 )
            //*    WRITE #IA-CONTRACT-NUM #REVAL-IND #PER-UNITS #PER-PMT
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            pnd_Ia_Output_File_Pnd_Fund_Sequence_Num.setValue(pnd_Fund_Sequence_Num_Temp);                                                                                //Natural: MOVE #FUND-SEQUENCE-NUM-TEMP TO #FUND-SEQUENCE-NUM
            getWorkFiles().write(1, false, pnd_Ia_Output_File);                                                                                                           //Natural: WRITE WORK FILE 01 #IA-OUTPUT-FILE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ia_Output_File_Pnd_Ia_Mode.setValue(ldaAial0591.getIaa_Cpr_Trans_View_Cntrct_Mode_Ind());                                                                     //Natural: MOVE CNTRCT-MODE-IND TO #IA-MODE
        if (condition(pnd_Record_Found_Switch_2.equals("Y")))                                                                                                             //Natural: IF #RECORD-FOUND-SWITCH-2 = 'Y'
        {
            pnd_Ia_Output_File_Pnd_Reval_Ind.setValue(pnd_Cref_Array_Pnd_Reval_Ind_Temp.getValue(2));                                                                     //Natural: MOVE #REVAL-IND-TEMP ( 2 ) TO #REVAL-IND
            pnd_Ia_Output_File_Pnd_Per_Units.setValue(pnd_Cref_Array_Pnd_Per_Units_Temp.getValue(2));                                                                     //Natural: MOVE #PER-UNITS-TEMP ( 2 ) TO #PER-UNITS
            pnd_Ia_Output_File_Pnd_Per_Pmt.setValue(pnd_Cref_Array_Pnd_Per_Pmt_Temp.getValue(2));                                                                         //Natural: MOVE #PER-PMT-TEMP ( 2 ) TO #PER-PMT
            //*   WRITE #IA-CONTRACT-NUM #REVAL-IND #PER-UNITS #PER-PMT
            //*  WRITE '#FUND-SEQUENCE-NUM-TEMP = ' #FUND-SEQUENCE-NUM-TEMP
            pnd_Ia_Output_File_Pnd_Fund_Sequence_Num.setValue(pnd_Fund_Sequence_Num_Temp);                                                                                //Natural: MOVE #FUND-SEQUENCE-NUM-TEMP TO #FUND-SEQUENCE-NUM
            getWorkFiles().write(1, false, pnd_Ia_Output_File);                                                                                                           //Natural: WRITE WORK FILE 01 #IA-OUTPUT-FILE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Output_File() throws Exception                                                                                                                 //Natural: PRINT-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        if (condition(pnd_Fund_Sequence_Num_Temp.equals(1)))                                                                                                              //Natural: IF #FUND-SEQUENCE-NUM-TEMP = 1
        {
            //*  ADD 1 TO #PRINT-NUM
            //*  IF #PRINT-NUM = 1
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            //*  ELSE
            //*   MOVE 0 TO #PRINT-NUM
            //*  END-IF
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) NOTITLE ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#RECORD-TYPE           ",pnd_Ia_Output_File_Pnd_Record_Type);                                                     //Natural: WRITE ( 2 ) '#RECORD-TYPE           ' #RECORD-TYPE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-CONTRACT-NUM       ",pnd_Ia_Output_File_Pnd_Ia_Contract_Num);                                                 //Natural: WRITE ( 2 ) '#IA-CONTRACT-NUM       ' #IA-CONTRACT-NUM
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-PAYEE-CODE         ",pnd_Ia_Output_File_Pnd_Ia_Payee_Code);                                                   //Natural: WRITE ( 2 ) '#IA-PAYEE-CODE         ' #IA-PAYEE-CODE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#DA-CONTRACT-NUM       ",pnd_Ia_Output_File_Pnd_Da_Contract_Num);                                                 //Natural: WRITE ( 2 ) '#DA-CONTRACT-NUM       ' #DA-CONTRACT-NUM
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#TRANS-TYPE            ",pnd_Ia_Output_File_Pnd_Trans_Type);                                                      //Natural: WRITE ( 2 ) '#TRANS-TYPE            ' #TRANS-TYPE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#TRANS-DATE            ",pnd_Ia_Output_File_Pnd_Trans_Date);                                                      //Natural: WRITE ( 2 ) '#TRANS-DATE            ' #TRANS-DATE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-CYCLE-DATE         ",pnd_Ia_Output_File_Pnd_Ia_Cycle_Date);                                                   //Natural: WRITE ( 2 ) '#IA-CYCLE-DATE         ' #IA-CYCLE-DATE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-OPTION             ",pnd_Ia_Output_File_Pnd_Ia_Option);                                                       //Natural: WRITE ( 2 ) '#IA-OPTION             ' #IA-OPTION
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-MODE               ",pnd_Ia_Output_File_Pnd_Ia_Mode);                                                         //Natural: WRITE ( 2 ) '#IA-MODE               ' #IA-MODE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-ORIGIN             ",pnd_Ia_Output_File_Pnd_Ia_Origin);                                                       //Natural: WRITE ( 2 ) '#IA-ORIGIN             ' #IA-ORIGIN
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-ISSUE-DATE         ",pnd_Ia_Output_File_Pnd_Ia_Issue_Date);                                                   //Natural: WRITE ( 2 ) '#IA-ISSUE-DATE         ' #IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#IA-FINAL-PMT-DATE     ",pnd_Ia_Output_File_Pnd_Ia_Final_Pmt_Date);                                               //Natural: WRITE ( 2 ) '#IA-FINAL-PMT-DATE     ' #IA-FINAL-PMT-DATE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#FIRST-ANNT-XREF       ",pnd_Ia_Output_File_Pnd_First_Annt_Xref);                                                 //Natural: WRITE ( 2 ) '#FIRST-ANNT-XREF       ' #FIRST-ANNT-XREF
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#FIRST-ANNT-DOB        ",pnd_Ia_Output_File_Pnd_First_Annt_Dob);                                                  //Natural: WRITE ( 2 ) '#FIRST-ANNT-DOB        ' #FIRST-ANNT-DOB
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#FIRST-ANNT-SEX        ",pnd_Ia_Output_File_Pnd_First_Annt_Sex);                                                  //Natural: WRITE ( 2 ) '#FIRST-ANNT-SEX        ' #FIRST-ANNT-SEX
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#FIRST-ANNT-DOD        ",pnd_Ia_Output_File_Pnd_First_Annt_Dod);                                                  //Natural: WRITE ( 2 ) '#FIRST-ANNT-DOD        ' #FIRST-ANNT-DOD
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#SECOND-ANNT-XREF      ",pnd_Ia_Output_File_Pnd_Second_Annt_Xref);                                                //Natural: WRITE ( 2 ) '#SECOND-ANNT-XREF      ' #SECOND-ANNT-XREF
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#SECOND-ANNT-DOB       ",pnd_Ia_Output_File_Pnd_Second_Annt_Dob);                                                 //Natural: WRITE ( 2 ) '#SECOND-ANNT-DOB       ' #SECOND-ANNT-DOB
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#SECOND-ANNT-SEX       ",pnd_Ia_Output_File_Pnd_Second_Annt_Sex);                                                 //Natural: WRITE ( 2 ) '#SECOND-ANNT-SEX       ' #SECOND-ANNT-SEX
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#SECOND-ANNT-DOD       ",pnd_Ia_Output_File_Pnd_Second_Annt_Dod);                                                 //Natural: WRITE ( 2 ) '#SECOND-ANNT-DOD       ' #SECOND-ANNT-DOD
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"#BENEFICIARY-ANNT-XREF ",pnd_Ia_Output_File_Pnd_Beneficiary_Annt_Xref);                                           //Natural: WRITE ( 2 ) '#BENEFICIARY-ANNT-XREF ' #BENEFICIARY-ANNT-XREF
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,"#FUND-SEQUENCE-NUM ",pnd_Ia_Output_File_Pnd_Fund_Sequence_Num);                                                       //Natural: WRITE ( 2 ) '#FUND-SEQUENCE-NUM ' #FUND-SEQUENCE-NUM
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"#FUND-CODE         ",pnd_Ia_Output_File_Pnd_Fund_Code);                                                               //Natural: WRITE ( 2 ) '#FUND-CODE         ' #FUND-CODE
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"#REVAL-IND         ",pnd_Ia_Output_File_Pnd_Reval_Ind);                                                               //Natural: WRITE ( 2 ) '#REVAL-IND         ' #REVAL-IND
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"#PER-UNITS         ",pnd_Ia_Output_File_Pnd_Per_Units);                                                               //Natural: WRITE ( 2 ) '#PER-UNITS         ' #PER-UNITS
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"#PER-PMT           ",pnd_Ia_Output_File_Pnd_Per_Pmt);                                                                 //Natural: WRITE ( 2 ) '#PER-PMT           ' #PER-PMT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=60");
        Global.format(4, "LS=132 PS=60");
    }
}
