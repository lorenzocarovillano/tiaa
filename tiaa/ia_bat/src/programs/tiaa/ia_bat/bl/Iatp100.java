/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:27 PM
**        * FROM NATURAL PROGRAM : Iatp100
************************************************************
**        * FILE NAME            : Iatp100.java
**        * CLASS NAME           : Iatp100
**        * INSTANCE NAME        : Iatp100
************************************************************
************************************************************************
* PROGRAM:  IATP100
* FUNCTION: ETNAT REPORT GIVING ESTIMATE OF TOTAL ASSET TRANSFER AMOUNT
*           REQUESTED EACH DAY.
*
* CREATED:  11/25/95 BY ARI GROSSMAN
*
* HISTORY: 12/16/96 : ADDED MULTI FUND FUNCTIONALITY
*          10/25/97 : ADDED ILB FUND, MANY TO MANY PROCESSING, BREAKING
*                     BY EFFECTIVE DATE INTO SEPARATE REPORTS.
*          02/24/98 : ADDED RETRO TOTALS REPORT AND FUTURE TOTALS REPORT
*          06/01/99 : ADDED TIAA TO CREF LOGIC. BROKE DOWN TIAA TO
*                     GRADED AND STANDARD BUCKETS
*          01/16/09 OS TIAA ACCESS CHANGES. SC 011609.
*          04/06/12 OS RATE BASE EXPANSION CHANGES. SC 040612.
*          04/2017  OS RE-STOWED ONLY FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp100 extends BLNatBase
{
    // Data Areas
    private PdaIatl400p pdaIatl400p;
    private PdaAial0130 pdaAial0130;
    private LdaIatl100 ldaIatl100;
    private LdaIatl010 ldaIatl010;
    private PdaAial0190 pdaAial0190;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;

    private DbsGroup pnd_Tiaa_Rates;
    private DbsField pnd_Tiaa_Rates_Pnd_Rate_Code;
    private DbsField pnd_Tiaa_Rates_Pnd_Gtd_Pmt;
    private DbsField pnd_Tiaa_Rates_Pnd_Dvd_Pmt;
    private DbsField pnd_Dte_Time_Hold_A;
    private DbsField pnd_To_Qty_Total;
    private DbsField pnd_Max_Acct;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_To_Units;
    private DbsField pnd_W_Units;
    private DbsField pnd_D_Units;
    private DbsField pnd_E_Units;
    private DbsField pnd_To_Pymts;
    private DbsField pnd_W_Pymts;
    private DbsField pnd_D_Pymts;
    private DbsField pnd_E_Pymts;
    private DbsField pnd_Date8;
    private DbsField pnd_Auv_Dte;
    private DbsField pnd_Cnt_Retro;
    private DbsField pnd_Cnt_Future;
    private DbsField pnd_Cnt_Eft;
    private DbsField pnd_Cnt_T_To_C;
    private DbsField pnd_D;
    private DbsField pnd_W_Variable;
    private DbsField pnd_W_To_Date;
    private DbsField pnd_W_From_Date;
    private DbsField pnd_Bus_Date;

    private DbsGroup pnd_Bus_Date__R_Field_2;
    private DbsField pnd_Bus_Date_Pnd_Bus_Date_Cc;
    private DbsField pnd_Bus_Date_Pnd_Bus_Date_Yy;
    private DbsField pnd_Bus_Date_Pnd_Bus_Date_Mm;
    private DbsField pnd_Bus_Date_Pnd_Bus_Date_Dd;

    private DbsGroup pnd_Bus_Date__R_Field_3;
    private DbsField pnd_Bus_Date_Pnd_Bus_Date_N;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_4;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_5;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Eff_Date_Hold;

    private DbsGroup pnd_Eff_Date_Hold__R_Field_6;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd;
    private DbsField pnd_Cmpny_Fund_3;
    private DbsField pnd_Invrse_Recvd_Time;

    private DbsGroup pnd_Invrse_Recvd_Time__R_Field_7;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Fund;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Dte;
    private DbsField pnd_Read_File;
    private DbsField pnd_W_Date;
    private DbsField pnd_Acct_Cde;
    private DbsField pnd_T;
    private DbsField pnd_S;
    private DbsField pnd_W_Pct;
    private DbsField pnd_W_Pct_To;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Rc_Code;

    private DbsGroup pnd_A26_Fields__R_Field_8;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;
    private DbsField pnd_Cref_Prorate;
    private DbsField pnd_Tiaa_Fund_Pymt;
    private DbsField pnd_Xfr_Units;
    private DbsField pnd_Transfer_Dollars;
    private DbsField pnd_Per;
    private DbsField pnd_W_Per;
    private DbsField pnd_Next_Pay_Dte;
    private DbsField pnd_Next_Chk_Dt;

    private DbsGroup pnd_Next_Chk_Dt__R_Field_9;
    private DbsField pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N;
    private DbsField pnd_Print_Reports;
    private DbsField pnd_Last_Dt;
    private DbsField pnd_Next_Dt;

    private DbsGroup pnd_Next_Dt__R_Field_10;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Mm;
    private DbsField pnd_Next_Dt_Pnd_Filler_1;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Dd;
    private DbsField pnd_Next_Dt_Pnd_Filler_2;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Yyyy;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_11;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_12;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2;
    private DbsField pnd_Nbr_Acct;
    private DbsField pnd_Acct_Std_Alpha_Cde;
    private DbsField pnd_Acct_Nme_5;
    private DbsField pnd_Acct_Tckr;
    private DbsField pnd_Acct_Effctve_Dte;
    private DbsField pnd_Acct_Unit_Rte_Ind;
    private DbsField pnd_Acct_Std_Nm_Cde;
    private DbsField pnd_Acct_Rpt_Seq;
    private DbsField pnd_Sub_Tot;
    private DbsField pnd_Ovrall_Tot;

    private DbsGroup pnd_Totals_Table;
    private DbsField pnd_Totals_Table_Pnd_To_Fnd;
    private DbsField pnd_Totals_Table_Pnd_To_Tot;
    private DbsField pnd_Totals_Table_Pnd_Fr_Fnd;
    private DbsField pnd_Totals_Table_Pnd_Fr_Tot;

    private DbsGroup pnd_Net_Total;
    private DbsField pnd_Net_Total_Pnd_Net_Fnd;
    private DbsField pnd_Net_Total_Pnd_Net_Amt;
    private DbsField pnd_Name_From;
    private DbsField pnd_Name_To;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Fund_Rcvd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);
        pdaAial0130 = new PdaAial0130(localVariables);
        ldaIatl100 = new LdaIatl100();
        registerRecord(ldaIatl100);
        registerRecord(ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View());
        ldaIatl010 = new LdaIatl010();
        registerRecord(ldaIatl010);
        registerRecord(ldaIatl010.getVw_iatl010());
        pdaAial0190 = new PdaAial0190(localVariables);

        // Local Variables

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Tiaa_Rates = localVariables.newGroupArrayInRecord("pnd_Tiaa_Rates", "#TIAA-RATES", new DbsArrayController(1, 250));
        pnd_Tiaa_Rates_Pnd_Rate_Code = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Rate_Code", "#RATE-CODE", FieldType.STRING, 2);
        pnd_Tiaa_Rates_Pnd_Gtd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Gtd_Pmt", "#GTD-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tiaa_Rates_Pnd_Dvd_Pmt = pnd_Tiaa_Rates.newFieldInGroup("pnd_Tiaa_Rates_Pnd_Dvd_Pmt", "#DVD-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dte_Time_Hold_A = localVariables.newFieldInRecord("pnd_Dte_Time_Hold_A", "#DTE-TIME-HOLD-A", FieldType.STRING, 14);
        pnd_To_Qty_Total = localVariables.newFieldInRecord("pnd_To_Qty_Total", "#TO-QTY-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Max_Acct = localVariables.newFieldInRecord("pnd_Max_Acct", "#MAX-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_To_Units = localVariables.newFieldInRecord("pnd_To_Units", "#TO-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Units = localVariables.newFieldInRecord("pnd_W_Units", "#W-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_D_Units = localVariables.newFieldInRecord("pnd_D_Units", "#D-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_E_Units = localVariables.newFieldInRecord("pnd_E_Units", "#E-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_To_Pymts = localVariables.newFieldInRecord("pnd_To_Pymts", "#TO-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Pymts = localVariables.newFieldInRecord("pnd_W_Pymts", "#W-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Pymts = localVariables.newFieldInRecord("pnd_D_Pymts", "#D-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Pymts = localVariables.newFieldInRecord("pnd_E_Pymts", "#E-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);
        pnd_Auv_Dte = localVariables.newFieldInRecord("pnd_Auv_Dte", "#AUV-DTE", FieldType.NUMERIC, 8);
        pnd_Cnt_Retro = localVariables.newFieldInRecord("pnd_Cnt_Retro", "#CNT-RETRO", FieldType.NUMERIC, 6);
        pnd_Cnt_Future = localVariables.newFieldInRecord("pnd_Cnt_Future", "#CNT-FUTURE", FieldType.NUMERIC, 6);
        pnd_Cnt_Eft = localVariables.newFieldInRecord("pnd_Cnt_Eft", "#CNT-EFT", FieldType.NUMERIC, 6);
        pnd_Cnt_T_To_C = localVariables.newFieldInRecord("pnd_Cnt_T_To_C", "#CNT-T-TO-C", FieldType.NUMERIC, 6);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Variable = localVariables.newFieldInRecord("pnd_W_Variable", "#W-VARIABLE", FieldType.STRING, 30);
        pnd_W_To_Date = localVariables.newFieldInRecord("pnd_W_To_Date", "#W-TO-DATE", FieldType.STRING, 8);
        pnd_W_From_Date = localVariables.newFieldInRecord("pnd_W_From_Date", "#W-FROM-DATE", FieldType.STRING, 8);
        pnd_Bus_Date = localVariables.newFieldInRecord("pnd_Bus_Date", "#BUS-DATE", FieldType.STRING, 8);

        pnd_Bus_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Bus_Date__R_Field_2", "REDEFINE", pnd_Bus_Date);
        pnd_Bus_Date_Pnd_Bus_Date_Cc = pnd_Bus_Date__R_Field_2.newFieldInGroup("pnd_Bus_Date_Pnd_Bus_Date_Cc", "#BUS-DATE-CC", FieldType.STRING, 2);
        pnd_Bus_Date_Pnd_Bus_Date_Yy = pnd_Bus_Date__R_Field_2.newFieldInGroup("pnd_Bus_Date_Pnd_Bus_Date_Yy", "#BUS-DATE-YY", FieldType.STRING, 2);
        pnd_Bus_Date_Pnd_Bus_Date_Mm = pnd_Bus_Date__R_Field_2.newFieldInGroup("pnd_Bus_Date_Pnd_Bus_Date_Mm", "#BUS-DATE-MM", FieldType.STRING, 2);
        pnd_Bus_Date_Pnd_Bus_Date_Dd = pnd_Bus_Date__R_Field_2.newFieldInGroup("pnd_Bus_Date_Pnd_Bus_Date_Dd", "#BUS-DATE-DD", FieldType.STRING, 2);

        pnd_Bus_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Bus_Date__R_Field_3", "REDEFINE", pnd_Bus_Date);
        pnd_Bus_Date_Pnd_Bus_Date_N = pnd_Bus_Date__R_Field_3.newFieldInGroup("pnd_Bus_Date_Pnd_Bus_Date_N", "#BUS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_4", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Cc = pnd_Eff_Date__R_Field_4.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Cc", "#EFF-DATE-CC", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Yy = pnd_Eff_Date__R_Field_4.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yy", "#EFF-DATE-YY", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_4.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_4.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_5", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Eff_Date_Hold = localVariables.newFieldInRecord("pnd_Eff_Date_Hold", "#EFF-DATE-HOLD", FieldType.STRING, 8);

        pnd_Eff_Date_Hold__R_Field_6 = localVariables.newGroupInRecord("pnd_Eff_Date_Hold__R_Field_6", "REDEFINE", pnd_Eff_Date_Hold);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc = pnd_Eff_Date_Hold__R_Field_6.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc", "#HLD-EFF-DATE-CC", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy = pnd_Eff_Date_Hold__R_Field_6.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy", "#HLD-EFF-DATE-YY", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm = pnd_Eff_Date_Hold__R_Field_6.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm", "#HLD-EFF-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd = pnd_Eff_Date_Hold__R_Field_6.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd", "#HLD-EFF-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Cmpny_Fund_3 = localVariables.newFieldInRecord("pnd_Cmpny_Fund_3", "#CMPNY-FUND-3", FieldType.STRING, 3);
        pnd_Invrse_Recvd_Time = localVariables.newFieldInRecord("pnd_Invrse_Recvd_Time", "#INVRSE-RECVD-TIME", FieldType.NUMERIC, 14);

        pnd_Invrse_Recvd_Time__R_Field_7 = localVariables.newGroupInRecord("pnd_Invrse_Recvd_Time__R_Field_7", "REDEFINE", pnd_Invrse_Recvd_Time);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Fund = pnd_Invrse_Recvd_Time__R_Field_7.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Fund", "#IRT-FUND", FieldType.NUMERIC, 
            2);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Dte = pnd_Invrse_Recvd_Time__R_Field_7.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Dte", "#IRT-DTE", FieldType.NUMERIC, 
            12);
        pnd_Read_File = localVariables.newFieldInRecord("pnd_Read_File", "#READ-FILE", FieldType.NUMERIC, 6);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.STRING, 8);
        pnd_Acct_Cde = localVariables.newFieldInRecord("pnd_Acct_Cde", "#ACCT-CDE", FieldType.STRING, 1);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 3);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.NUMERIC, 3);
        pnd_W_Pct = localVariables.newFieldInRecord("pnd_W_Pct", "#W-PCT", FieldType.PACKED_DECIMAL, 6, 3);
        pnd_W_Pct_To = localVariables.newFieldInRecord("pnd_W_Pct_To", "#W-PCT-TO", FieldType.PACKED_DECIMAL, 6, 3);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Rc_Code = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Code", "#RC-CODE", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_8 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_8", "REDEFINE", pnd_A26_Fields_Pnd_Rc_Code);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields__R_Field_8.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_8.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Cref_Prorate = localVariables.newFieldInRecord("pnd_Cref_Prorate", "#CREF-PRORATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tiaa_Fund_Pymt = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Pymt", "#TIAA-FUND-PYMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Units = localVariables.newFieldInRecord("pnd_Xfr_Units", "#XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Transfer_Dollars = localVariables.newFieldInRecord("pnd_Transfer_Dollars", "#TRANSFER-DOLLARS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per = localVariables.newFieldInRecord("pnd_Per", "#PER", FieldType.NUMERIC, 7, 4);
        pnd_W_Per = localVariables.newFieldInRecord("pnd_W_Per", "#W-PER", FieldType.NUMERIC, 7, 4);
        pnd_Next_Pay_Dte = localVariables.newFieldInRecord("pnd_Next_Pay_Dte", "#NEXT-PAY-DTE", FieldType.DATE);
        pnd_Next_Chk_Dt = localVariables.newFieldInRecord("pnd_Next_Chk_Dt", "#NEXT-CHK-DT", FieldType.STRING, 8);

        pnd_Next_Chk_Dt__R_Field_9 = localVariables.newGroupInRecord("pnd_Next_Chk_Dt__R_Field_9", "REDEFINE", pnd_Next_Chk_Dt);
        pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N = pnd_Next_Chk_Dt__R_Field_9.newFieldInGroup("pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N", "#NEXT-CHK-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Print_Reports = localVariables.newFieldInRecord("pnd_Print_Reports", "#PRINT-REPORTS", FieldType.NUMERIC, 8);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 10);
        pnd_Next_Dt = localVariables.newFieldInRecord("pnd_Next_Dt", "#NEXT-DT", FieldType.STRING, 10);

        pnd_Next_Dt__R_Field_10 = localVariables.newGroupInRecord("pnd_Next_Dt__R_Field_10", "REDEFINE", pnd_Next_Dt);
        pnd_Next_Dt_Pnd_Next_Dt_Mm = pnd_Next_Dt__R_Field_10.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Mm", "#NEXT-DT-MM", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_1 = pnd_Next_Dt__R_Field_10.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Dd = pnd_Next_Dt__R_Field_10.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Dd", "#NEXT-DT-DD", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_2 = pnd_Next_Dt__R_Field_10.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Yyyy = pnd_Next_Dt__R_Field_10.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Yyyy", "#NEXT-DT-YYYY", FieldType.STRING, 4);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_11", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_11.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_11.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_11.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);

        pnd_Cntrct_Fund_Key__R_Field_12 = pnd_Cntrct_Fund_Key__R_Field_11.newGroupInGroup("pnd_Cntrct_Fund_Key__R_Field_12", "REDEFINE", pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1 = pnd_Cntrct_Fund_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2 = pnd_Cntrct_Fund_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code_2", "#W-FUND-CODE-2", 
            FieldType.STRING, 2);
        pnd_Nbr_Acct = localVariables.newFieldInRecord("pnd_Nbr_Acct", "#NBR-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Acct_Std_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Alpha_Cde", "#ACCT-STD-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Acct_Nme_5 = localVariables.newFieldArrayInRecord("pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 6, new DbsArrayController(1, 20));
        pnd_Acct_Tckr = localVariables.newFieldArrayInRecord("pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Acct_Effctve_Dte = localVariables.newFieldArrayInRecord("pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            20));
        pnd_Acct_Unit_Rte_Ind = localVariables.newFieldArrayInRecord("pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 
            20));
        pnd_Acct_Std_Nm_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Nm_Cde", "#ACCT-STD-NM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pnd_Acct_Rpt_Seq = localVariables.newFieldArrayInRecord("pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 20));
        pnd_Sub_Tot = localVariables.newFieldInRecord("pnd_Sub_Tot", "#SUB-TOT", FieldType.NUMERIC, 10);
        pnd_Ovrall_Tot = localVariables.newFieldInRecord("pnd_Ovrall_Tot", "#OVRALL-TOT", FieldType.NUMERIC, 10);

        pnd_Totals_Table = localVariables.newGroupArrayInRecord("pnd_Totals_Table", "#TOTALS-TABLE", new DbsArrayController(1, 20));
        pnd_Totals_Table_Pnd_To_Fnd = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_To_Fnd", "#TO-FND", FieldType.STRING, 6);
        pnd_Totals_Table_Pnd_To_Tot = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_To_Tot", "#TO-TOT", FieldType.NUMERIC, 10);
        pnd_Totals_Table_Pnd_Fr_Fnd = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_Fr_Fnd", "#FR-FND", FieldType.STRING, 6);
        pnd_Totals_Table_Pnd_Fr_Tot = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_Fr_Tot", "#FR-TOT", FieldType.NUMERIC, 10);

        pnd_Net_Total = localVariables.newGroupArrayInRecord("pnd_Net_Total", "#NET-TOTAL", new DbsArrayController(1, 20));
        pnd_Net_Total_Pnd_Net_Fnd = pnd_Net_Total.newFieldInGroup("pnd_Net_Total_Pnd_Net_Fnd", "#NET-FND", FieldType.STRING, 6);
        pnd_Net_Total_Pnd_Net_Amt = pnd_Net_Total.newFieldInGroup("pnd_Net_Total_Pnd_Net_Amt", "#NET-AMT", FieldType.NUMERIC, 10);
        pnd_Name_From = localVariables.newFieldInRecord("pnd_Name_From", "#NAME-FROM", FieldType.STRING, 6);
        pnd_Name_To = localVariables.newFieldInRecord("pnd_Name_To", "#NAME-TO", FieldType.STRING, 6);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Fund_Rcvd = localVariables.newFieldInRecord("pnd_Fund_Rcvd", "#FUND-RCVD", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        ldaIatl100.initializeValues();
        ldaIatl010.initializeValues();

        localVariables.reset();
        pnd_Max_Acct.setInitialValue(20);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp100() throws Exception
    {
        super("Iatp100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: FORMAT ( 3 ) LS = 133 PS = 56
        //*  011609
                                                                                                                                                                          //Natural: PERFORM GET-ACCT-INFO
        sub_Get_Acct_Info();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        getReports().write(0, "***************************",NEWLINE,"  START OF IATP100 PROGRAM",NEWLINE,"***************************");                                  //Natural: WRITE '***************************' / '  START OF IATP100 PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #TEXT-TABLE-PARA
        sub_Pnd_Text_Table_Para();
        if (condition(Global.isEscape())) {return;}
        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: MOVE 'DC' TO #IATN400-IN.CNTRL-CDE
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        pnd_Bus_Date.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #BUS-DATE
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 IATL010
        while (condition(getWorkFiles().read(1, ldaIatl010.getVw_iatl010())))
        {
            pnd_Read_File.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-FILE
            //*      WRITE '='  IATL010.RQST-EFFCTV-DTE(EM=YYYYMMDD)
            pnd_Eff_Date.setValueEdited(ldaIatl010.getIatl010_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
            //*     MOVE EDITED IATL010.RQST-EFFCTV-DTE(EM=MM/DD/YY) TO #W-DATE
            //*     COMPRESS #EFF-DATE-MM '/' #EFF-DATE-DD '/' #EFF-DATE-YY
            //*       INTO #W-DATE LEAVING NO
            if (condition(pnd_Read_File.equals(1)))                                                                                                                       //Natural: IF #READ-FILE = 1
            {
                pnd_Eff_Date_Hold.setValue(pnd_Eff_Date);                                                                                                                 //Natural: MOVE #EFF-DATE TO #EFF-DATE-HOLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Eff_Date.notEquals(pnd_Eff_Date_Hold)))                                                                                                 //Natural: IF #EFF-DATE NE #EFF-DATE-HOLD
                {
                                                                                                                                                                          //Natural: PERFORM #PRINT-REPORT
                    sub_Pnd_Print_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM #RESET-PRINT-REPORT
                    sub_Pnd_Reset_Print_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Eff_Date_Hold.setValue(pnd_Eff_Date);                                                                                                             //Natural: MOVE #EFF-DATE TO #EFF-DATE-HOLD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "CONTRACT NUMBER ==> ","=",ldaIatl010.getIatl010_Ia_Frm_Cntrct(),"=",ldaIatl010.getIatl010_Ia_Frm_Payee(),"=",ldaIatl010.getIatl010_Rqst_Effctv_Dte(),  //Natural: WRITE 'CONTRACT NUMBER ==> ' '=' IATL010.IA-FRM-CNTRCT '=' IATL010.IA-FRM-PAYEE '=' IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD )
                new ReportEditMask ("YYYYMMDD"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIatl010.getIatl010_Rqst_Xfr_Type().notEquals(" ")))                                                                                          //Natural: IF IATL010.RQST-XFR-TYPE NOT = ' '
            {
                DbsUtil.callnat(Iatn100a.class , getCurrentProcessState(), ldaIatl010.getVw_iatl010(), pdaAial0190.getPnd_Aian019_Linkage());                             //Natural: CALLNAT 'IATN100A' IATL010 #AIAN019-LINKAGE
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM #TEACHERS-TO-CREF
                sub_Pnd_Teachers_To_Cref();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cnt_T_To_C.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CNT-T-TO-C
                pdaAial0190.getPnd_Aian019_Linkage().reset();                                                                                                             //Natural: RESET #AIAN019-LINKAGE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number().setValue(ldaIatl010.getIatl010_Ia_Frm_Cntrct());                                                     //Natural: ASSIGN #CONTRACT-NUMBER := IATL010.IA-FRM-CNTRCT
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().compute(new ComputeParameters(false, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()),                //Natural: ASSIGN #PAYEE-CODE := VAL ( IATL010.IA-FRM-PAYEE )
                ldaIatl010.getIatl010_Ia_Frm_Payee().val());
            vw_iaa_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CONTRACT-NUMBER
            (
            "F1",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(), WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_iaa_Cntrct.readNextRow("F1", true)))
            {
                vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE," CONTRACT",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),"NOT FOUND");                 //Natural: WRITE ( 2 ) / ' CONTRACT' #CONTRACT-NUMBER 'NOT FOUND'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-NOREC
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Origin().setValue(iaa_Cntrct_Cntrct_Orgn_Cde);                                                                     //Natural: ASSIGN #ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                     //Natural: ASSIGN #OPTION := IAA-CNTRCT.CNTRCT-OPTN-CDE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date().setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                //Natural: ASSIGN #ISSUE-DATE := IAA-CNTRCT.CNTRCT-ISSUE-DTE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob().setValue(iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);                                                    //Natural: ASSIGN #FIRST-ANN-DOB := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex().setValue(iaa_Cntrct_Cntrct_First_Annt_Sex_Cde);                                                    //Natural: ASSIGN #FIRST-ANN-SEX := IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().setValue(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);                                                    //Natural: ASSIGN #FIRST-ANN-DOD := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);                                                    //Natural: ASSIGN #SECOND-ANN-DOB := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde);                                                    //Natural: ASSIGN #SECOND-ANN-SEX := IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod().setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                    //Natural: ASSIGN #SECOND-ANN-DOD := IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number());                                                         //Natural: ASSIGN #PPCN-NBR := #CONTRACT-NUMBER
            pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code());                                                             //Natural: ASSIGN #PAYEE-CDE := #PAYEE-CODE
            //*    WRITE '=' #PPCN-NBR '=' #PAYEE-CDE
            vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                  //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
            (
            "R1",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
            1
            );
            R1:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("R1")))
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number()) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #CONTRACT-NUMBER OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #PAYEE-CODE
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE," CONTRACT PARTICIPANT",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),                  //Natural: WRITE ( 2 ) / ' CONTRACT PARTICIPANT' #CONTRACT-NUMBER #PAYEE-CODE 'NOT FOUND'
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code(),"NOT FOUND");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break R1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                       //Natural: REJECT IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
                {
                    continue;
                }
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode().setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                          //Natural: ASSIGN #MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date().setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte);                                   //Natural: ASSIGN #FINAL-PER-PAY-DATE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Last_Dt.reset();                                                                                                                                          //Natural: RESET #LAST-DT #NEXT-DT #NEXT-PAY-DTE #NEXT-CHK-DT
            pnd_Next_Dt.reset();
            pnd_Next_Pay_Dte.reset();
            pnd_Next_Chk_Dt.reset();
            DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Dt, pnd_Next_Dt);                                                                         //Natural: CALLNAT 'IAAN0020' #LAST-DT #NEXT-DT
            if (condition(Global.isEscape())) return;
            pnd_Next_Chk_Dt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Next_Dt_Pnd_Next_Dt_Yyyy, pnd_Next_Dt_Pnd_Next_Dt_Mm, pnd_Next_Dt_Pnd_Next_Dt_Dd)); //Natural: COMPRESS #NEXT-DT-YYYY #NEXT-DT-MM #NEXT-DT-DD INTO #NEXT-CHK-DT LEAVING NO
            //*  !!! FOR TESTING ONLY !!!!
            if (condition(pnd_Next_Chk_Dt_Pnd_Next_Chk_Dt_N.less(19980501)))                                                                                              //Natural: IF #NEXT-CHK-DT-N < 19980501
            {
                pnd_Next_Chk_Dt.setValue(19980501);                                                                                                                       //Natural: ASSIGN #NEXT-CHK-DT := 19980501
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Nazn6032.class , getCurrentProcessState(), pnd_Next_Pay_Dte, pnd_Next_Chk_Dt, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode());                 //Natural: CALLNAT 'NAZN6032' #NEXT-PAY-DTE #NEXT-CHK-DT #MODE
            if (condition(Global.isEscape())) return;
            //*    WRITE  '=' #NEXT-PAY-DTE '=' #NEXT-CHK-DT '=' #MODE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte().setValue(pnd_Next_Pay_Dte);                                                                           //Natural: ASSIGN #NEXT-PYMNT-DTE := #NEXT-PAY-DTE
            //*    WRITE 'NAZN6032' '=' #NEXT-PYMNT-DTE
            pnd_Date8.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE8
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(99999999);                                                                            //Natural: ASSIGN #ILLUSTRATION-EFF-DATE := 99999999
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                       //Natural: ASSIGN #TRANSFER-EFFECTIVE-DATE := #EFF-DATE-N
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date().compute(new ComputeParameters(false, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date()),      //Natural: ASSIGN #PROCESSING-DATE := VAL ( #DATE8 )
                pnd_Date8.val());
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().setValue("I");                                                                                           //Natural: ASSIGN #TYPE-OF-RUN := 'I'
            short decideConditionsMet751 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IATL010.RCRD-TYPE-CDE = '1'
            if (condition(ldaIatl010.getIatl010_Rcrd_Type_Cde().equals("1")))
            {
                decideConditionsMet751++;
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("0");                                                                             //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '0'
            }                                                                                                                                                             //Natural: WHEN IATL010.RCRD-TYPE-CDE = '2' AND IATL010.XFR-FRM-UNIT-TYP ( 1 ) = 'M'
            else if (condition(ldaIatl010.getIatl010_Rcrd_Type_Cde().equals("2") && ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(1).equals("M")))
            {
                decideConditionsMet751++;
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("1");                                                                             //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '1'
            }                                                                                                                                                             //Natural: WHEN IATL010.RCRD-TYPE-CDE = '2' AND IATL010.XFR-FRM-UNIT-TYP ( 1 ) = 'A'
            else if (condition(ldaIatl010.getIatl010_Rcrd_Type_Cde().equals("2") && ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(1).equals("A")))
            {
                decideConditionsMet751++;
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue("2");                                                                             //Natural: ASSIGN #TRANSFER-REVAL-SWITCH := '2'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            FZ:                                                                                                                                                           //Natural: FOR #T = 1 TO 20
            for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
            {
                if (condition(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                      //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) EQ ' '
                {
                    if (true) break FZ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FZ. )
                }                                                                                                                                                         //Natural: END-IF
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                    //Natural: ASSIGN #FUND-CODE := IATL010.XFR-FRM-ACCT-CDE ( #T )
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().setValue(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                        //Natural: ASSIGN #REVALUATION-INDICATOR := IATL010.XFR-FRM-UNIT-TYP ( #T )
                pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                           //Natural: RESET #AUV #AUV-RET-DTE #RC #A26-PRTC-DTE
                pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
                pnd_A26_Fields_Pnd_Rc.reset();
                pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
                pnd_A26_Fields_Pnd_A26_Call_Type.setValue("F");                                                                                                           //Natural: ASSIGN #A26-CALL-TYPE := 'F'
                pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                        //Natural: ASSIGN #A26-FND-CDE := IATL010.XFR-FRM-ACCT-CDE ( #T )
                pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                                                     //Natural: ASSIGN #A26-REVAL-METH := IATL010.XFR-FRM-UNIT-TYP ( #T )
                pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_Eff_Date_Pnd_Eff_Date_N);                                                                                     //Natural: ASSIGN #A26-REQ-DTE := #EFF-DATE-N
                //*      WRITE '=' #A26-FIELDS
                //*  040612
                DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                //Natural: CALLNAT 'AIAN026' #A26-FIELDS
                if (condition(Global.isEscape())) return;
                //*      #RC #AUV #AUV-RET-DTE        /* 040612
                //*      #DAYS-IN-REQUEST-MONTH       /* 040612
                //*      #DAYS-IN-PARTICIP-MONTH      /* 040612
                //*      WRITE 'AIAN026' '=' #RC '=' #AUV
                if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                                  //Natural: IF #RC > 0
                {
                    //*        WRITE (2) / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC
                    //*        WRITE (2)  'CONTRACT NUMBER =>'  #CONTRACT-NUMBER #PAYEE-CODE
                    pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                       //Natural: RESET #AUV #AUV-RET-DTE #RC #A26-PRTC-DTE
                    pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
                    pnd_A26_Fields_Pnd_Rc.reset();
                    pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
                    pnd_A26_Fields_Pnd_A26_Call_Type.setValue("L");                                                                                                       //Natural: ASSIGN #A26-CALL-TYPE := 'L'
                    pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                    //Natural: ASSIGN #A26-FND-CDE := IATL010.XFR-FRM-ACCT-CDE ( #T )
                    pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T));                                                 //Natural: ASSIGN #A26-REVAL-METH := IATL010.XFR-FRM-UNIT-TYP ( #T )
                    pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(99999999);                                                                                                    //Natural: ASSIGN #A26-REQ-DTE := 99999999
                    //*  040612
                    DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                            //Natural: CALLNAT 'AIAN026' #A26-FIELDS
                    if (condition(Global.isEscape())) return;
                    //*        #RC #AUV #AUV-RET-DTE       /* 040612
                    //*        #DAYS-IN-REQUEST-MONTH      /* 040612
                    //*        #DAYS-IN-PARTICIP-MONTH     /* 040612
                    //*      WRITE 'AIAN026' '=' #RC '=' #AUV
                    if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                              //Natural: IF #RC > 0
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Rc);                          //Natural: WRITE ( 2 ) / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, ReportOption.NOTITLE,"CONTRACT NUMBER =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()); //Natural: WRITE ( 2 ) 'CONTRACT NUMBER =>' #CONTRACT-NUMBER #PAYEE-CODE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_A26_Fields_Pnd_Rc.equals(getZero())))                                                                                                   //Natural: IF #RC = 0
                {
                                                                                                                                                                          //Natural: PERFORM #GET-TOTAL-UNITS-AND-DOLLARS
                    sub_Pnd_Get_Total_Units_And_Dollars();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_To_Qty_Total.reset();                                                                                                                             //Natural: RESET #TO-QTY-TOTAL
                    pnd_To_Qty_Total.nadd(ldaIatl010.getIatl010_Xfr_To_Qty().getValue("*"));                                                                              //Natural: ADD IATL010.XFR-TO-QTY ( * ) TO #TO-QTY-TOTAL
                    if (condition(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("M")))                                                                  //Natural: IF IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'M'
                    {
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-TRNSFR-UNITS
                        sub_Pnd_Monthly_Trnsfr_Units();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #MONTHLY-TRANSFER-TO-ARRAYS
                        sub_Pnd_Monthly_Transfer_To_Arrays();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-UNITS
                        sub_Pnd_Rounding_Units();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
                        sub_Pnd_Annual_Trnsfr_Units_And_Dollars();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #ANNUAL-TRANSFER-TO-ARRAYS
                        sub_Pnd_Annual_Transfer_To_Arrays();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-UNITS
                        sub_Pnd_Rounding_Units();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #ROUNDING-DOLLARS
                        sub_Pnd_Rounding_Dollars();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FZ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*      WRITE '=' #TRANSFER-UNITS '=' #TO-UNITS
                }                                                                                                                                                         //Natural: END-IF
                //*   WRITE // '******** BEFORE LINKAGE AIAN013 ********* '
                //*    PERFORM #DISPLAY-TOTAL-LINKAGE
                DbsUtil.callnat(Aian013.class , getCurrentProcessState(), pdaAial0130.getPnd_Aian013_Linkage());                                                          //Natural: CALLNAT 'AIAN013' #AIAN013-LINKAGE
                if (condition(Global.isEscape())) return;
                //*  040612
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().notEquals(getZero())))                                                             //Natural: IF #AIAN013-LINKAGE.#RETURN-CODE-NBR NE 0
                {
                    //*  040612
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,"ERROR IN LINKAGE AIAN013 - ERROR CODE ===> ",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr()); //Natural: WRITE ( 2 ) / 'ERROR IN LINKAGE AIAN013 - ERROR CODE ===> ' #AIAN013-LINKAGE.#RETURN-CODE-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(2, ReportOption.NOTITLE,"CONTRACT NUMBER =>",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),pnd_Cntrct_Payee_Key_Pnd_Payee_Cde); //Natural: WRITE ( 2 ) 'CONTRACT NUMBER =>' #CONTRACT-NUMBER #PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*    PERFORM #DISPLAY-TOTAL-LINKAGE
                ldaIatl100.getPnd_Fund_Cd_From().setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                      //Natural: ASSIGN #FUND-CD-FROM := IATL010.XFR-FRM-ACCT-CDE ( #T )
                                                                                                                                                                          //Natural: PERFORM #FROM-SUB-CONVERT
                sub_Pnd_From_Sub_Convert();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  040612
                if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code_Nbr().equals(getZero())))                                                                //Natural: IF #AIAN013-LINKAGE.#RETURN-CODE-NBR = 0
                {
                    F5:                                                                                                                                                   //Natural: FOR #C = 1 TO 19
                    for (ldaIatl100.getPnd_C().setValue(1); condition(ldaIatl100.getPnd_C().lessOrEqual(19)); ldaIatl100.getPnd_C().nadd(1))
                    {
                        //*    WRITE '=' #FUND-CODE-OUT(#C)
                        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(ldaIatl100.getPnd_C()).notEquals(" ")))                             //Natural: IF #FUND-CODE-OUT ( #C ) NE ' '
                        {
                            ldaIatl100.getPnd_Fund_Cd_To().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(ldaIatl100.getPnd_C()));              //Natural: ASSIGN #FUND-CD-TO := #FUND-CODE-OUT ( #C )
                                                                                                                                                                          //Natural: PERFORM #TO-SUB-CONVERT
                            sub_Pnd_To_Sub_Convert();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F5"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            ldaIatl100.getPnd_To_From_Table().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #C ) TO #TO-FROM-TABLE ( #TO-SUB,#FROM-SUB )
                            //*  011609
                            //*  011609
                            ldaIatl100.getPnd_To_From_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                            pnd_Cnt_Eft.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CNT-EFT
                            //* **
                            //*        ADDED 03/98 TO REPORT ON RETRO TOTALS,
                            //*          FUTURE TOTALS AND GRAND TOTALS
                            //*        WRITE '=' #EFF-DATE '=' #BUS-DATE
                            if (condition(pnd_Eff_Date.less(pnd_Bus_Date)))                                                                                               //Natural: IF #EFF-DATE < #BUS-DATE
                            {
                                ldaIatl100.getPnd_To_From_Table_Retro().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #C ) TO #TO-FROM-TABLE-RETRO ( #TO-SUB,#FROM-SUB )
                                //*  011609
                                //*  011609
                                ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-TABLE-RETRO-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                    ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                                pnd_Cnt_Retro.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CNT-RETRO
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Eff_Date.greater(pnd_Bus_Date)))                                                                                        //Natural: IF #EFF-DATE > #BUS-DATE
                                {
                                    ldaIatl100.getPnd_To_From_Table_Future().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #C ) TO #TO-FROM-TABLE-FUTURE ( #TO-SUB,#FROM-SUB )
                                    //*  011609
                                    //*  011609
                                    ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-TABLE-FUTURE-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                        ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                                    pnd_Cnt_Future.nadd(1);                                                                                                               //Natural: ADD 1 TO #CNT-FUTURE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //*        ADD #TRANSFER-AMT-OUT-CREF(#C)
                            //*             TO #TO-FROM-TABLE-TOTAL(#TO-SUB,#FROM-SUB)
                            //*    WRITE / '=' #TRANSFER-AMT-OUT-CREF(#C) '=' #TO-SUB '=' #FROM-SUB
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   WRITE '== PUT IN TEACHER BUCKETS =='
                    F6:                                                                                                                                                   //Natural: FOR #C = 1 TO 2
                    for (ldaIatl100.getPnd_C().setValue(1); condition(ldaIatl100.getPnd_C().lessOrEqual(2)); ldaIatl100.getPnd_C().nadd(1))
                    {
                        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(ldaIatl100.getPnd_C()).notEquals(" ")))                             //Natural: IF #RATE-CODE-OUT ( #C ) NE ' '
                        {
                            ldaIatl100.getPnd_Tiaa_Fund_Cd_To().setValue(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(ldaIatl100.getPnd_C()));   //Natural: ASSIGN #TIAA-FUND-CD-TO := #PMT-METHOD-CODE-OUT ( #C )
                                                                                                                                                                          //Natural: PERFORM #TIAA-TO-SUB-CONVERT
                            sub_Pnd_Tiaa_To_Sub_Convert();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F6"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F6"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            ldaIatl100.getPnd_To_From_Table().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-TIAA ( #C ) TO #TO-FROM-TABLE ( #TO-SUB,#FROM-SUB )
                            //*  011609 START
                            if (condition(ldaIatl100.getPnd_To_Sub().equals(2)))                                                                                          //Natural: IF #TO-SUB = 2
                            {
                                ldaIatl100.getPnd_To_From_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'T' #FUND-CD-FROM INTO #TO-FROM-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                    "T", ldaIatl100.getPnd_Fund_Cd_From()));
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaIatl100.getPnd_To_From_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'G' #FUND-CD-FROM INTO #TO-FROM-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                    "G", ldaIatl100.getPnd_Fund_Cd_From()));
                                //*  011609 END
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Cnt_Eft.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CNT-EFT
                            //* **************
                            //*        ADDED 03/98 TO REPORT ON RETRO TOTALS,
                            //*           FUTURE TOTALS AND GRAND TOTALS
                            if (condition(pnd_Eff_Date.less(pnd_Bus_Date)))                                                                                               //Natural: IF #EFF-DATE < #BUS-DATE
                            {
                                ldaIatl100.getPnd_To_From_Table_Retro().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-TIAA ( #C ) TO #TO-FROM-TABLE-RETRO ( #TO-SUB,#FROM-SUB )
                                //*  011609 START
                                if (condition(ldaIatl100.getPnd_To_Sub().equals(2)))                                                                                      //Natural: IF #TO-SUB = 2
                                {
                                    ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'T' #FUND-CD-FROM INTO #TO-FROM-TABLE-RETRO-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                        "T", ldaIatl100.getPnd_Fund_Cd_From()));
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'G' #FUND-CD-FROM INTO #TO-FROM-TABLE-RETRO-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                        "G", ldaIatl100.getPnd_Fund_Cd_From()));
                                    //*  011609 END
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Cnt_Retro.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CNT-RETRO
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Eff_Date.greater(pnd_Bus_Date)))                                                                                        //Natural: IF #EFF-DATE > #BUS-DATE
                                {
                                    ldaIatl100.getPnd_To_From_Table_Future().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #TRANSFER-AMT-OUT-TIAA ( #C ) TO #TO-FROM-TABLE-FUTURE ( #TO-SUB,#FROM-SUB )
                                    //*  011609 START
                                    if (condition(ldaIatl100.getPnd_To_Sub().equals(2)))                                                                                  //Natural: IF #TO-SUB = 2
                                    {
                                        ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'T' #FUND-CD-FROM INTO #TO-FROM-TABLE-FUTURE-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                            "T", ldaIatl100.getPnd_Fund_Cd_From()));
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS 'G' #FUND-CD-FROM INTO #TO-FROM-TABLE-FUTURE-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                            "G", ldaIatl100.getPnd_Fund_Cd_From()));
                                        //*  011609 END
                                    }                                                                                                                                     //Natural: END-IF
                                    pnd_Cnt_Future.nadd(1);                                                                                                               //Natural: ADD 1 TO #CNT-FUTURE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //*         ADD #TRANSFER-AMT-OUT-TIAA(#C)
                            //*              TO #TO-FROM-TABLE-TOTAL(#TO-SUB,#FROM-SUB)
                            //* **************
                            //*    WRITE / '=' #TIAA-TRNSFR-AMT-OUT(#C) '=' #TO-SUB '=' #FROM-SUB
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #RESET-FUND-INFO
                sub_Pnd_Reset_Fund_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FZ.
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
            sub_Pnd_Reset_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
        pnd_W_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm, "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd,            //Natural: COMPRESS #HLD-EFF-DATE-MM '/' #HLD-EFF-DATE-DD '/' #HLD-EFF-DATE-YY INTO #W-DATE LEAVING NO
            "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy));
                                                                                                                                                                          //Natural: PERFORM #PRINT-REPORT
        sub_Pnd_Print_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_W_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Bus_Date_Pnd_Bus_Date_Mm, "/", pnd_Bus_Date_Pnd_Bus_Date_Dd, "/", pnd_Bus_Date_Pnd_Bus_Date_Yy)); //Natural: COMPRESS #BUS-DATE-MM '/' #BUS-DATE-DD '/' #BUS-DATE-YY INTO #W-DATE LEAVING NO
                                                                                                                                                                          //Natural: PERFORM #PRINT-REPORT-RETRO-FUTURE
        sub_Pnd_Print_Report_Retro_Future();
        if (condition(Global.isEscape())) {return;}
        //*   WRITE 'PASSED WORK FILE LOOP - NOW PRINT TOTALS'
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(0, "RECORDS READ    =>",pnd_Read_File, new ReportEditMask ("ZZZ,ZZ9"));                                                                        //Natural: WRITE 'RECORDS READ    =>' #READ-FILE ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  WRITE     'REPORTS PRINTED =>' #PRINT-REPORTS(EM=ZZZ,ZZ9)
        getReports().write(0, "EFCTV COUNT     =>",pnd_Cnt_Eft, new ReportEditMask ("ZZZ,ZZ9"));                                                                          //Natural: WRITE 'EFCTV COUNT     =>' #CNT-EFT ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "RETRO COUNT     =>",pnd_Cnt_Retro, new ReportEditMask ("ZZZ,ZZ9"));                                                                        //Natural: WRITE 'RETRO COUNT     =>' #CNT-RETRO ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "FUTURE COUNT    =>",pnd_Cnt_Future, new ReportEditMask ("ZZZ,ZZ9"));                                                                       //Natural: WRITE 'FUTURE COUNT    =>' #CNT-FUTURE ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "TEACHERS TO CREF >",pnd_Cnt_T_To_C, new ReportEditMask ("ZZZ,ZZ9"));                                                                       //Natural: WRITE 'TEACHERS TO CREF >' #CNT-T-TO-C ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        //* *******************************************************011609**********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-TO-RECEIVE
        //* *******************************************************011609**********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ACCT-INFO
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-REPORT
        //*  WRITE (1) #TEXT-TABLE(#I)
        //*    2X #CNV-TABLE(#I,1)(EM=ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,2)(EM=Z,ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,3)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,4)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,5)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,6)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,7)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,8)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,9)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,10)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,11)(EM=Z,ZZZ,ZZ9)
        //*    2X #TOT-ACROSS-CNV(#I)(EM=ZZ,ZZZ,ZZ9)
        //* *
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-REPORT-RETRO-FUTURE
        //*  WRITE (3) #TEXT-TABLE(#I)
        //*    2X #CNV-TABLE(#I,1)(EM=ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,2)(EM=ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,3)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,4)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,5)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,6)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,7)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,8)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,9)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,10)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,11)(EM=Z,ZZZ,ZZ9)
        //*    2X #TOT-ACROSS-CNV(#I)(EM=ZZ,ZZZ,ZZ9)
        //*  WRITE (3) #TEXT-TABLE(#I)
        //*    2X #CNV-TABLE(#I,1)(EM=ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,2)(EM=ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,3)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,4)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,5)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,6)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,7)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,8)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,9)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,10)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,11)(EM=Z,ZZZ,ZZ9)
        //*    2X #TOT-ACROSS-CNV(#I)(EM=ZZ,ZZZ,ZZ9)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-RETRO-FUTURE-NET
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-TOTAL-UNITS-AND-DOLLARS
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TO-SUB-CONVERT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-TO-SUB-CONVERT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DISPLAY-TOTAL-LINKAGE
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TEXT-TABLE-PARA
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FROM-SUB-CONVERT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONVERT-FUND-ANNUAL
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONVERT-FUND-MONTHLY
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-AIAN019-PARA
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FUND-INFO
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PRINT-REPORT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PRINT-REPORT-TOTALS
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-TRNSFR-UNITS
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
        //*   WRITE '=' #XFR-UNITS '=' #TRANSFER-DOLLARS
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MONTHLY-TRANSFER-TO-ARRAYS
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ANNUAL-TRANSFER-TO-ARRAYS
        //* *********************************************************************
        //*      #FUND-CDE := IATL010.XFR-TO-ACCT-CDE(#I)
        //*      PERFORM GET-FUND-TO-RECEIVE
        //* ******************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-TO-SIDE
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ROUNDING-UNITS
        //* ******************************************************************
        //* **     RESET #TO-UNITS
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ROUNDING-DOLLARS
        //* ******************************************************************
        //* **     RESET #TO-PYMTS
        //* ******************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND-RECORD
        //* *****************************************************************
        //* ***********************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TEACHERS-TO-CREF
        //* *****************************************************************
        //*  WRITE 'AFTER' CALLNAT 'ARI015' #AIAN019-LINKAGE
        //* *
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN019-PARMS
        //*  '=' #AIAN019-CONTRACT-PAYEE /
    }
    private void sub_Get_Fund_To_Receive() throws Exception                                                                                                               //Natural: GET-FUND-TO-RECEIVE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn261.class , getCurrentProcessState(), pnd_Fund_Cde, iaa_Cntrct_Cntrct_Orgn_Cde, pnd_Fund_Rcvd);                                               //Natural: CALLNAT 'IATN261' #FUND-CDE IAA-CNTRCT.CNTRCT-ORGN-CDE #FUND-RCVD
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Acct_Info() throws Exception                                                                                                                     //Natural: GET-ACCT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), pnd_Nbr_Acct, pnd_Acct_Std_Alpha_Cde.getValue("*"), pnd_Acct_Nme_5.getValue("*"), pnd_Acct_Tckr.getValue("*"),  //Natural: CALLNAT 'IATN216' #NBR-ACCT #ACCT-STD-ALPHA-CDE ( * ) #ACCT-NME-5 ( * ) #ACCT-TCKR ( * ) #ACCT-EFFCTVE-DTE ( * ) #ACCT-UNIT-RTE-IND ( * ) #ACCT-STD-NM-CDE ( * ) #ACCT-RPT-SEQ ( * )
            pnd_Acct_Effctve_Dte.getValue("*"), pnd_Acct_Unit_Rte_Ind.getValue("*"), pnd_Acct_Std_Nm_Cde.getValue("*"), pnd_Acct_Rpt_Seq.getValue("*"));
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Nbr_Acct.equals(getZero())))                                                                                                                    //Natural: IF #NBR-ACCT = 0
        {
            getReports().write(0, "*** SOMETHING IS WRONG WITH EXTERNALIZATION! ***");                                                                                    //Natural: WRITE '*** SOMETHING IS WRONG WITH EXTERNALIZATION! ***'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Print_Report() throws Exception                                                                                                                  //Natural: #PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*  WRITE '#PRINT-REPORT'
        pnd_Print_Reports.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PRINT-REPORTS
        //*  WRITE '=' #W-DATE
        pnd_W_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm, "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd,            //Natural: COMPRESS #HLD-EFF-DATE-MM '/' #HLD-EFF-DATE-DD '/' #HLD-EFF-DATE-YY INTO #W-DATE LEAVING NO
            "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy));
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FR1:                                                                                                                                                              //Natural: FOR #I = 1 TO #MAX-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Acct)); pnd_I.nadd(1))
        {
            FR2:                                                                                                                                                          //Natural: FOR #J = 1 TO #MAX-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
            {
                ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).compute(new ComputeParameters(true, ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J)),             //Natural: COMPUTE ROUNDED #CNV-TABLE ( #I,#J ) = #TO-FROM-TABLE ( #I,#J ) / 1000
                    ldaIatl100.getPnd_To_From_Table().getValue(pnd_I,pnd_J).divide(1000));
                //*    WRITE '=' #CNV-TABLE(#I,#J) '=' #TO-FROM-TABLE(#I,#J) '=' #I '=' #J
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl100.getPnd_Tot_Across_Cnv().getValue(pnd_I).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,"*"));                                                   //Natural: ADD #CNV-TABLE ( #I,* ) TO #TOT-ACROSS-CNV ( #I )
            FOR01:                                                                                                                                                        //Natural: FOR #J = 1 TO #MAX-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
            {
                if (condition(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).greater(getZero())))                                                                    //Natural: IF #CNV-TABLE ( #I,#J ) GT 0
                {
                    pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Funds().getValue(pnd_I,pnd_J).getSubstring(1,1));                                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-FUNDS ( #I,#J ) ,1,1 )
                    short decideConditionsMet1168 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                    if (condition(pnd_Fund_Cde.equals("T")))
                    {
                        decideConditionsMet1168++;
                        pnd_Name_To.setValue("STNDRD");                                                                                                                   //Natural: ASSIGN #NAME-TO := 'STNDRD'
                    }                                                                                                                                                     //Natural: WHEN #FUND-CDE = 'G'
                    else if (condition(pnd_Fund_Cde.equals("G")))
                    {
                        decideConditionsMet1168++;
                        pnd_Name_To.setValue("GRADED");                                                                                                                   //Natural: ASSIGN #NAME-TO := 'GRADED'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));         //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                        if (condition(pnd_A.greater(getZero())))                                                                                                          //Natural: IF #A GT 0
                        {
                            pnd_Name_To.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                         //Natural: ASSIGN #NAME-TO := #ACCT-NME-5 ( #A )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Name_To.setValue("UNKWN");                                                                                                                //Natural: ASSIGN #NAME-TO := 'UNKWN'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-DECIDE
                    DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_To_Fnd.getValue("*")), new ExamineSearch(pnd_Name_To), new ExamineGivingIndex(pnd_B));         //Natural: EXAMINE #TO-FND ( * ) FOR #NAME-TO GIVING INDEX #B
                    if (condition(pnd_B.greater(getZero())))                                                                                                              //Natural: IF #B GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        FOR02:                                                                                                                                            //Natural: FOR #B 1 #MAX-ACCT
                        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                        {
                            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).equals(" ")))                                                                       //Natural: IF #TO-FND ( #B ) = ' '
                            {
                                pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).setValue(pnd_Name_To);                                                                        //Natural: ASSIGN #TO-FND ( #B ) := #NAME-TO
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_B).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                //Natural: ADD #CNV-TABLE ( #I,#J ) TO #TO-TOT ( #B )
                    pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Funds().getValue(pnd_I,pnd_J).getSubstring(2,1));                                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-FUNDS ( #I,#J ) ,2,1 )
                    short decideConditionsMet1194 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                    if (condition(pnd_Fund_Cde.equals("T")))
                    {
                        decideConditionsMet1194++;
                        pnd_Name_From.setValue("STNDRD");                                                                                                                 //Natural: ASSIGN #NAME-FROM := 'STNDRD'
                    }                                                                                                                                                     //Natural: WHEN #FUND-CDE = 'G'
                    else if (condition(pnd_Fund_Cde.equals("G")))
                    {
                        decideConditionsMet1194++;
                        pnd_Name_From.setValue("GRADED");                                                                                                                 //Natural: ASSIGN #NAME-FROM := 'GRADED'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));         //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                        if (condition(pnd_A.greater(getZero())))                                                                                                          //Natural: IF #A GT 0
                        {
                            pnd_Name_From.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                       //Natural: ASSIGN #NAME-FROM := #ACCT-NME-5 ( #A )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Name_From.setValue("UNKWN");                                                                                                              //Natural: ASSIGN #NAME-FROM := 'UNKWN'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-DECIDE
                    DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*")), new ExamineSearch(pnd_Name_From), new ExamineGivingIndex(pnd_B));       //Natural: EXAMINE #FR-FND ( * ) FOR #NAME-FROM GIVING INDEX #B
                    if (condition(pnd_B.greater(getZero())))                                                                                                              //Natural: IF #B GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #B 1 #MAX-ACCT
                        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                        {
                            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).equals(" ")))                                                                       //Natural: IF #FR-FND ( #B ) = ' '
                            {
                                pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).setValue(pnd_Name_From);                                                                      //Natural: ASSIGN #FR-FND ( #B ) := #NAME-FROM
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_B).nsubtract(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                           //Natural: SUBTRACT #CNV-TABLE ( #I,#J ) FROM #FR-TOT ( #B )
                    pnd_Sub_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                                //Natural: ADD #CNV-TABLE ( #I,#J ) TO #SUB-TOT
                    pnd_Ovrall_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                             //Natural: ADD #CNV-TABLE ( #I,#J ) TO #OVRALL-TOT
                    getReports().write(1, ReportOption.NOTITLE,pnd_Name_From,new TabSetting(11),pnd_Name_To,new TabSetting(22),ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J),  //Natural: WRITE ( 1 ) #NAME-FROM 11T #NAME-TO 22T #CNV-TABLE ( #I,#J ) ( EM = Z,ZZZ,ZZ9 )
                        new ReportEditMask ("Z,ZZZ,ZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Sub_Tot.greater(getZero())))                                                                                                                //Natural: IF #SUB-TOT GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Subtotal:",new TabSetting(18),pnd_Sub_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) / 'Subtotal:' 18T #SUB-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FR1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Sub_Tot.reset();                                                                                                                                      //Natural: RESET #SUB-TOT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total   :",new TabSetting(18),pnd_Ovrall_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) // 'Total   :' 18T #OVRALL-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* *FR3. FOR #I = 1 TO 11
        //* *  ADD  #CNV-TABLE(*,#I)  TO #TOT-DOWN-CNV(#I)
        //* *END-FOR
        //* *ADD #TOT-DOWN-CNV(*) TO #TOT-DOWN-TOT
        //* *WRITE (1) 'TOTAL:'
        //* *  2X #TOT-DOWN-CNV(1)(EM=ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(2)(EM=Z,ZZZ,ZZ9)
        //* *  3X #TOT-DOWN-CNV(3)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(4)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(5)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(6)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(7)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(8)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(9)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(10)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(11)(EM=Z,ZZZ,ZZ9)
        //* *  2X #TOT-DOWN-TOT(EM=ZZ,ZZZ,ZZ9)
        //* *WRITE (1) 8X '-'(34) ' (IN THOUSANDS  $000) ----- NET -----' '-'(49)
        //* *WRITE (1) 10T 'GRADED'
        //* *  20T 'STNDRD'
        //* *  33T 'STOCK'
        //* *  45T 'MMA'
        //* *  52T 'SOCIAL'
        //* *  64T 'BOND'
        //* *  72T 'GLOBAL'
        //* *  82T 'GROWTH'
        //* *  092T 'EQUITY'
        //* *  105T 'REA'
        //* *  113T '  ILB '
        //* *  125T 'XFOOT'
        //*  ADD #TOT-ACROSS-CNV(1) TO #TOT-ACROSS-CNV(2)
        //*  WRITE '=' #SUB-TOT-DOWN-ACROSS(1)
        //*        '=' #TOT-ACROSS-CNV(1) '=' #TOT-DOWN-CNV(1)
        //* *COMPUTE #SUB-TOT-DOWN-ACROSS(1) =
        //* *  #TOT-ACROSS-CNV(1) - #TOT-DOWN-CNV(1)
        //* ** #D := 2
        //* *FR4. FOR #I = 1 TO 11
        //* *  COMPUTE #SUB-TOT-DOWN-ACROSS(#I) =
        //* *    #TOT-ACROSS-CNV(#I) - #TOT-DOWN-CNV(#I)
        //* **      COMPUTE #SUB-TOT-DOWN-ACROSS(#D) =
        //* **            #TOT-ACROSS-CNV(#D) - #TOT-DOWN-CNV(#I)
        //*  WRITE '=' #SUB-TOT-DOWN-ACROSS(#D) '=' #D '=' #I
        //*        '=' #TOT-ACROSS-CNV(#D) '=' #TOT-DOWN-CNV(#I)
        //*        ADD 1 TO #D
        //* *END-FOR
        //* *ADD #SUB-TOT-DOWN-ACROSS(*) TO #SUB-TOT-DOWN-ACROSS-TOT
        //* *WRITE (1) 7X #SUB-TOT-DOWN-ACROSS(1)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(2)(EM=+ZZZ,ZZ9)
        //* *4X #SUB-TOT-DOWN-ACROSS(3)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(4)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(5)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(6)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(7)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(8)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(9)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(10)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS(11)(EM=+ZZZ,ZZ9)
        //* *  2X #SUB-TOT-DOWN-ACROSS-TOT(EM=+Z,ZZZ,ZZ9)
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(10),"(TOTALS IN THOUSANDS  $000) ----------");                                              //Natural: WRITE ( 1 ) / '-' ( 10 ) '(TOTALS IN THOUSANDS  $000) ----------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FROM:");                                                                                                      //Natural: WRITE ( 1 ) / 'FROM:'
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                    //Natural: IF #FR-FND ( #A ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_A), //Natural: WRITE ( 1 ) #FR-FND ( #A ) 10X #FR-TOT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TO:");                                                                                                        //Natural: WRITE ( 1 ) / 'TO:'
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                    //Natural: IF #TO-FND ( #A ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_A), //Natural: WRITE ( 1 ) #TO-FND ( #A ) 10X #TO-TOT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #TO-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue("*").equals(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A))))                                                   //Natural: IF #NET-FND ( * ) = #TO-FND ( #A )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR07:                                                                                                                                                    //Natural: FOR #B 1 #MAX-ACCT
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                {
                    if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).equals(" ")))                                                                                 //Natural: IF #NET-FND ( #B ) = ' '
                    {
                        pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).setValue(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A));                                                  //Natural: ASSIGN #NET-FND ( #B ) := #TO-FND ( #A )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #FR-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue("*").equals(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A))))                                                   //Natural: IF #NET-FND ( * ) = #FR-FND ( #A )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR09:                                                                                                                                                    //Natural: FOR #B 1 #MAX-ACCT
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                {
                    if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).equals(" ")))                                                                                 //Natural: IF #NET-FND ( #B ) = ' '
                    {
                        pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).setValue(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A));                                                  //Natural: ASSIGN #NET-FND ( #B ) := #FR-FND ( #A )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #TO-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Net_Total_Pnd_Net_Fnd.getValue("*")), new ExamineSearch(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A)),                   //Natural: EXAMINE #NET-FND ( * ) FOR #TO-FND ( #A ) GIVING INDEX #B
                new ExamineGivingIndex(pnd_B));
            pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_B).nadd(pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_A));                                                                  //Natural: ADD #TO-TOT ( #A ) TO #NET-AMT ( #B )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #FR-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Net_Total_Pnd_Net_Fnd.getValue("*")), new ExamineSearch(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A)),                   //Natural: EXAMINE #NET-FND ( * ) FOR #FR-FND ( #A ) GIVING INDEX #B
                new ExamineGivingIndex(pnd_B));
            pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_B).nadd(pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_A));                                                                  //Natural: ADD #FR-TOT ( #A ) TO #NET-AMT ( #B )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-------- (NET TOTALS IN THOUSANDS  $000) --------");                                                                  //Natural: WRITE ( 1 ) '-------- (NET TOTALS IN THOUSANDS  $000) --------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FUNDS:");                                                                                                     //Natural: WRITE ( 1 ) / 'FUNDS:'
        if (Global.isEscape()) return;
        FOR12:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                      //Natural: IF #NET-FND ( #A ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_A),     //Natural: WRITE ( 1 ) #NET-FND ( #A ) 10X #NET-AMT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #SUB-TOT #OVRALL-TOT #TO-FND ( * ) #TO-TOT ( * ) #FR-FND ( * ) #FR-TOT ( * ) #NET-FND ( * ) #NET-AMT ( * )
        pnd_Sub_Tot.reset();
        pnd_Ovrall_Tot.reset();
        pnd_Totals_Table_Pnd_To_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_To_Tot.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Tot.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Fnd.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Amt.getValue("*").reset();
        //*                                                      /* 011609 END
    }
    private void sub_Pnd_Print_Report_Retro_Future() throws Exception                                                                                                     //Natural: #PRINT-REPORT-RETRO-FUTURE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_W_Variable.setValue(" RETROACTIVE REQUESTS PRIOR TO");                                                                                                        //Natural: MOVE ' RETROACTIVE REQUESTS PRIOR TO' TO #W-VARIABLE
        //*  011609
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Cnt_Retro.greater(getZero())))                                                                                                                  //Natural: IF #CNT-RETRO GT 0
        {
            ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                      //Natural: RESET #CNV-TABLE ( *,* ) #TOT-ACROSS-CNV ( * )
            ldaIatl100.getPnd_Tot_Across_Cnv().getValue("*").reset();
            FF1:                                                                                                                                                          //Natural: FOR #I = 1 TO #MAX-ACCT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Acct)); pnd_I.nadd(1))
            {
                FF2:                                                                                                                                                      //Natural: FOR #J = 1 TO #MAX-ACCT
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
                {
                    ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).compute(new ComputeParameters(true, ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J)),         //Natural: COMPUTE ROUNDED #CNV-TABLE ( #I,#J ) = #TO-FROM-TABLE-RETRO ( #I,#J ) / 1000
                        ldaIatl100.getPnd_To_From_Table_Retro().getValue(pnd_I,pnd_J).divide(1000));
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FF1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FF1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIatl100.getPnd_Tot_Across_Cnv().getValue(pnd_I).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,"*"));                                               //Natural: ADD #CNV-TABLE ( #I,* ) TO #TOT-ACROSS-CNV ( #I )
                FOR13:                                                                                                                                                    //Natural: FOR #J = 1 TO #MAX-ACCT
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
                {
                    if (condition(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).greater(getZero())))                                                                //Natural: IF #CNV-TABLE ( #I,#J ) GT 0
                    {
                        pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(pnd_I,pnd_J).getSubstring(1,1));                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-TABLE-RETRO-FUNDS ( #I,#J ) ,1,1 )
                        short decideConditionsMet1377 = 0;                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                        if (condition(pnd_Fund_Cde.equals("T")))
                        {
                            decideConditionsMet1377++;
                            pnd_Name_To.setValue("STNDRD");                                                                                                               //Natural: ASSIGN #NAME-TO := 'STNDRD'
                        }                                                                                                                                                 //Natural: WHEN #FUND-CDE = 'G'
                        else if (condition(pnd_Fund_Cde.equals("G")))
                        {
                            decideConditionsMet1377++;
                            pnd_Name_To.setValue("GRADED");                                                                                                               //Natural: ASSIGN #NAME-TO := 'GRADED'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));     //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                            if (condition(pnd_A.greater(getZero())))                                                                                                      //Natural: IF #A GT 0
                            {
                                pnd_Name_To.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                     //Natural: ASSIGN #NAME-TO := #ACCT-NME-5 ( #A )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Name_To.setValue("UNKWN");                                                                                                            //Natural: ASSIGN #NAME-TO := 'UNKWN'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-DECIDE
                        DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_To_Fnd.getValue("*")), new ExamineSearch(pnd_Name_To), new ExamineGivingIndex(pnd_B));     //Natural: EXAMINE #TO-FND ( * ) FOR #NAME-TO GIVING INDEX #B
                        if (condition(pnd_B.greater(getZero())))                                                                                                          //Natural: IF #B GT 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR14:                                                                                                                                        //Natural: FOR #B 1 #MAX-ACCT
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).equals(" ")))                                                                   //Natural: IF #TO-FND ( #B ) = ' '
                                {
                                    pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).setValue(pnd_Name_To);                                                                    //Natural: ASSIGN #TO-FND ( #B ) := #NAME-TO
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_B).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                            //Natural: ADD #CNV-TABLE ( #I,#J ) TO #TO-TOT ( #B )
                        pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(pnd_I,pnd_J).getSubstring(2,1));                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-TABLE-RETRO-FUNDS ( #I,#J ) ,2,1 )
                        short decideConditionsMet1403 = 0;                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                        if (condition(pnd_Fund_Cde.equals("T")))
                        {
                            decideConditionsMet1403++;
                            pnd_Name_From.setValue("STNDRD");                                                                                                             //Natural: ASSIGN #NAME-FROM := 'STNDRD'
                        }                                                                                                                                                 //Natural: WHEN #FUND-CDE = 'G'
                        else if (condition(pnd_Fund_Cde.equals("G")))
                        {
                            decideConditionsMet1403++;
                            pnd_Name_From.setValue("GRADED");                                                                                                             //Natural: ASSIGN #NAME-FROM := 'GRADED'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));     //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                            if (condition(pnd_A.greater(getZero())))                                                                                                      //Natural: IF #A GT 0
                            {
                                pnd_Name_From.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                   //Natural: ASSIGN #NAME-FROM := #ACCT-NME-5 ( #A )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Name_From.setValue("UNKWN");                                                                                                          //Natural: ASSIGN #NAME-FROM := 'UNKWN'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-DECIDE
                        DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*")), new ExamineSearch(pnd_Name_From), new ExamineGivingIndex(pnd_B));   //Natural: EXAMINE #FR-FND ( * ) FOR #NAME-FROM GIVING INDEX #B
                        if (condition(pnd_B.greater(getZero())))                                                                                                          //Natural: IF #B GT 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR15:                                                                                                                                        //Natural: FOR #B 1 #MAX-ACCT
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).equals(" ")))                                                                   //Natural: IF #FR-FND ( #B ) = ' '
                                {
                                    pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).setValue(pnd_Name_From);                                                                  //Natural: ASSIGN #FR-FND ( #B ) := #NAME-FROM
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_B).nsubtract(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                       //Natural: SUBTRACT #CNV-TABLE ( #I,#J ) FROM #FR-TOT ( #B )
                        pnd_Sub_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                            //Natural: ADD #CNV-TABLE ( #I,#J ) TO #SUB-TOT
                        pnd_Ovrall_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                         //Natural: ADD #CNV-TABLE ( #I,#J ) TO #OVRALL-TOT
                        getReports().write(3, ReportOption.NOTITLE,pnd_Name_From,new TabSetting(11),pnd_Name_To,new TabSetting(22),ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J),  //Natural: WRITE ( 3 ) #NAME-FROM 11T #NAME-TO 22T #CNV-TABLE ( #I,#J ) ( EM = Z,ZZZ,ZZ9 )
                            new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FF1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FF1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Sub_Tot.greater(getZero())))                                                                                                            //Natural: IF #SUB-TOT GT 0
                {
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE,"Subtotal:",new TabSetting(18),pnd_Sub_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                 //Natural: WRITE ( 3 ) / 'Subtotal:' 18T #SUB-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FF1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FF1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sub_Tot.reset();                                                                                                                                  //Natural: RESET #SUB-TOT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total   :",new TabSetting(18),pnd_Ovrall_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));              //Natural: WRITE ( 3 ) // 'Total   :' 18T #OVRALL-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            //* *RESET #TOT-DOWN-CNV(*) #TOT-DOWN-TOT
            //* *FF3. FOR #I = 1 TO 11
            //* *  ADD  #CNV-TABLE(*,#I)  TO #TOT-DOWN-CNV(#I)
            //* *END-FOR
            //* *ADD #TOT-DOWN-CNV(*) TO #TOT-DOWN-TOT
            //* *WRITE (3) 'TOTAL:'
            //* *  2X #TOT-DOWN-CNV(1)(EM=ZZZ,ZZ9)
            //* *  3X #TOT-DOWN-CNV(2)(EM=ZZZ,ZZ9)
            //* *  3X #TOT-DOWN-CNV(3)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(4)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(5)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(6)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(7)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(8)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(9)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(10)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(11)(EM=Z,ZZZ,ZZ9)
            //* *  2X #TOT-DOWN-TOT(EM=ZZ,ZZZ,ZZ9)
            //* *WRITE (3) 8X '-'(34) ' (IN THOUSANDS  $000) ----- NET -----' '-'(49)
            //* *WRITE (3) 10T 'GRADED'
            //* *  20T 'STNDRD'
            //* *  33T 'STOCK'
            //* *  45T 'MMA'
            //* *  52T 'SOCIAL'
            //* *  64T 'BOND'
            //* *  72T 'GLOBAL'
            //* *  82T 'GROWTH'
            //* *  092T 'EQUITY'
            //* *  105T 'REA'
            //* *  113T '  ILB '
            //* *  125T 'XFOOT'
            //*  ADD #TOT-ACROSS-CNV(1) TO #TOT-ACROSS-CNV(2)
            //*    COMPUTE #SUB-TOT-DOWN-ACROSS-R(1) =
            //*          #TOT-ACROSS-CNV(1) - #TOT-DOWN-CNV(1)
            //*  #D := 2
            //* *RESET #SUB-TOT-DOWN-ACROSS(*) #SUB-TOT-DOWN-ACROSS-TOT
            //* *FF4. FOR #I = 1 TO 11
            //* *  COMPUTE #SUB-TOT-DOWN-ACROSS(#I) =
            //* *    #TOT-ACROSS-CNV(#I) - #TOT-DOWN-CNV(#I)
            //*       COMPUTE #SUB-TOT-DOWN-ACROSS-R(#D) =
            //*             #TOT-ACROSS-CNV-R(#D) - #TOT-DOWN-CNV-R(#I)
            //*        ADD 1 TO #D
            //* *END-FOR
            //* *ADD #SUB-TOT-DOWN-ACROSS(*) TO #SUB-TOT-DOWN-ACROSS-TOT
            //* *WRITE (3) 7X #SUB-TOT-DOWN-ACROSS(1)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(2)(EM=+ZZZ,ZZ9)
            //* *  4X #SUB-TOT-DOWN-ACROSS(3)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(4)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(5)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(6)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(7)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(8)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(9)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(10)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(11)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS-TOT(EM=+Z,ZZZ,ZZ9)
            //* *PERFORM #RESET-PRINT-REPORT-TOTALS
                                                                                                                                                                          //Natural: PERFORM PRINT-RETRO-FUTURE-NET
            sub_Print_Retro_Future_Net();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"***** NO RETROACTIVE REQUESTS FOR THIS RUN *****");                                               //Natural: WRITE ( 3 ) // '***** NO RETROACTIVE REQUESTS FOR THIS RUN *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Variable.setValue("FUTURE TRANSFER REQUESTS AFTER");                                                                                                        //Natural: MOVE 'FUTURE TRANSFER REQUESTS AFTER' TO #W-VARIABLE
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Cnt_Future.greater(getZero())))                                                                                                                 //Natural: IF #CNT-FUTURE GT 0
        {
            FG1:                                                                                                                                                          //Natural: FOR #I = 1 TO 11
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
            {
                FG2:                                                                                                                                                      //Natural: FOR #J = 1 TO 11
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(11)); pnd_J.nadd(1))
                {
                    ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).compute(new ComputeParameters(true, ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J)),         //Natural: COMPUTE ROUNDED #CNV-TABLE ( #I,#J ) = #TO-FROM-TABLE-FUTURE ( #I,#J ) / 1000
                        ldaIatl100.getPnd_To_From_Table_Future().getValue(pnd_I,pnd_J).divide(1000));
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FG1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FG1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIatl100.getPnd_Tot_Across_Cnv().getValue(pnd_I).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,"*"));                                               //Natural: ADD #CNV-TABLE ( #I,* ) TO #TOT-ACROSS-CNV ( #I )
                FOR16:                                                                                                                                                    //Natural: FOR #J = 1 TO #MAX-ACCT
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
                {
                    if (condition(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).greater(getZero())))                                                                //Natural: IF #CNV-TABLE ( #I,#J ) GT 0
                    {
                        pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(pnd_I,pnd_J).getSubstring(1,1));                                    //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-TABLE-FUTURE-FUNDS ( #I,#J ) ,1,1 )
                        short decideConditionsMet1520 = 0;                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                        if (condition(pnd_Fund_Cde.equals("T")))
                        {
                            decideConditionsMet1520++;
                            pnd_Name_To.setValue("STNDRD");                                                                                                               //Natural: ASSIGN #NAME-TO := 'STNDRD'
                        }                                                                                                                                                 //Natural: WHEN #FUND-CDE = 'G'
                        else if (condition(pnd_Fund_Cde.equals("G")))
                        {
                            decideConditionsMet1520++;
                            pnd_Name_To.setValue("GRADED");                                                                                                               //Natural: ASSIGN #NAME-TO := 'GRADED'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));     //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                            if (condition(pnd_A.greater(getZero())))                                                                                                      //Natural: IF #A GT 0
                            {
                                pnd_Name_To.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                     //Natural: ASSIGN #NAME-TO := #ACCT-NME-5 ( #A )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Name_To.setValue("UNKWN");                                                                                                            //Natural: ASSIGN #NAME-TO := 'UNKWN'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-DECIDE
                        DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_To_Fnd.getValue("*")), new ExamineSearch(pnd_Name_To), new ExamineGivingIndex(pnd_B));     //Natural: EXAMINE #TO-FND ( * ) FOR #NAME-TO GIVING INDEX #B
                        if (condition(pnd_B.greater(getZero())))                                                                                                          //Natural: IF #B GT 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR17:                                                                                                                                        //Natural: FOR #B 1 #MAX-ACCT
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).equals(" ")))                                                                   //Natural: IF #TO-FND ( #B ) = ' '
                                {
                                    pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).setValue(pnd_Name_To);                                                                    //Natural: ASSIGN #TO-FND ( #B ) := #NAME-TO
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_B).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                            //Natural: ADD #CNV-TABLE ( #I,#J ) TO #TO-TOT ( #B )
                        pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(pnd_I,pnd_J).getSubstring(2,1));                                    //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-TABLE-FUTURE-FUNDS ( #I,#J ) ,2,1 )
                        short decideConditionsMet1546 = 0;                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                        if (condition(pnd_Fund_Cde.equals("T")))
                        {
                            decideConditionsMet1546++;
                            pnd_Name_From.setValue("STNDRD");                                                                                                             //Natural: ASSIGN #NAME-FROM := 'STNDRD'
                        }                                                                                                                                                 //Natural: WHEN #FUND-CDE = 'G'
                        else if (condition(pnd_Fund_Cde.equals("G")))
                        {
                            decideConditionsMet1546++;
                            pnd_Name_From.setValue("GRADED");                                                                                                             //Natural: ASSIGN #NAME-FROM := 'GRADED'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));     //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                            if (condition(pnd_A.greater(getZero())))                                                                                                      //Natural: IF #A GT 0
                            {
                                pnd_Name_From.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                   //Natural: ASSIGN #NAME-FROM := #ACCT-NME-5 ( #A )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Name_From.setValue("UNKWN");                                                                                                          //Natural: ASSIGN #NAME-FROM := 'UNKWN'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-DECIDE
                        DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*")), new ExamineSearch(pnd_Name_From), new ExamineGivingIndex(pnd_B));   //Natural: EXAMINE #FR-FND ( * ) FOR #NAME-FROM GIVING INDEX #B
                        if (condition(pnd_B.greater(getZero())))                                                                                                          //Natural: IF #B GT 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR18:                                                                                                                                        //Natural: FOR #B 1 #MAX-ACCT
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).equals(" ")))                                                                   //Natural: IF #FR-FND ( #B ) = ' '
                                {
                                    pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).setValue(pnd_Name_From);                                                                  //Natural: ASSIGN #FR-FND ( #B ) := #NAME-FROM
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_B).nsubtract(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                       //Natural: SUBTRACT #CNV-TABLE ( #I,#J ) FROM #FR-TOT ( #B )
                        pnd_Sub_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                            //Natural: ADD #CNV-TABLE ( #I,#J ) TO #SUB-TOT
                        pnd_Ovrall_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                         //Natural: ADD #CNV-TABLE ( #I,#J ) TO #OVRALL-TOT
                        getReports().write(3, ReportOption.NOTITLE,pnd_Name_From,new TabSetting(11),pnd_Name_To,new TabSetting(22),ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J),  //Natural: WRITE ( 3 ) #NAME-FROM 11T #NAME-TO 22T #CNV-TABLE ( #I,#J ) ( EM = Z,ZZZ,ZZ9 )
                            new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FG1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FG1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Sub_Tot.greater(getZero())))                                                                                                            //Natural: IF #SUB-TOT GT 0
                {
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE,"Subtotal:",new TabSetting(18),pnd_Sub_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                 //Natural: WRITE ( 3 ) / 'Subtotal:' 18T #SUB-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FG1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FG1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sub_Tot.reset();                                                                                                                                  //Natural: RESET #SUB-TOT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total   :",new TabSetting(18),pnd_Ovrall_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));              //Natural: WRITE ( 3 ) // 'Total   :' 18T #OVRALL-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            //* *FG3. FOR #I = 1 TO 11
            //* *  ADD  #CNV-TABLE(*,#I)  TO #TOT-DOWN-CNV(#I)
            //* *END-FOR
            //* *ADD #TOT-DOWN-CNV(*) TO #TOT-DOWN-TOT
            //* *WRITE (3) 'TOTAL:'
            //* *  2X #TOT-DOWN-CNV(1)(EM=ZZZ,ZZ9)
            //* *  3X #TOT-DOWN-CNV(2)(EM=ZZZ,ZZ9)
            //* *  3X #TOT-DOWN-CNV(3)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(4)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(5)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(6)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(7)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(8)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(9)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(10)(EM=Z,ZZZ,ZZ9)
            //* *  1X #TOT-DOWN-CNV(11)(EM=Z,ZZZ,ZZ9)
            //* *  2X #TOT-DOWN-TOT(EM=ZZ,ZZZ,ZZ9)
            //* *WRITE (3) 8X '-'(34) ' (IN THOUSANDS  $000) ----- NET -----' '-'(49)
            //* *WRITE (3) 10T 'GRADED'
            //* *  20T 'STNDRD'
            //* *  33T 'STOCK'
            //* *  45T 'MMA'
            //* *  52T 'SOCIAL'
            //* *  64T 'BOND'
            //* *  72T 'GLOBAL'
            //* *  82T 'GROWTH'
            //* *  092T 'EQUITY'
            //* *  105T 'REA'
            //* *  113T '  ILB '
            //* *  125T 'XFOOT'
            //*  ADD #TOT-ACROSS-CNV(1) TO #TOT-ACROSS-CNV(2)
            //*    COMPUTE #SUB-TOT-DOWN-ACROSS(1) =
            //*          #TOT-ACROSS-CNV(1) - #TOT-DOWN-CNV(1)
            //*  #D := 2
            //* *FG4. FOR #I = 1 TO 11
            //* *  COMPUTE #SUB-TOT-DOWN-ACROSS(#I) =
            //* *    #TOT-ACROSS-CNV(#I) - #TOT-DOWN-CNV(#I)
            //*       COMPUTE #SUB-TOT-DOWN-ACROSS-R(#D) =
            //*             #TOT-ACROSS-CNV-R(#D) - #TOT-DOWN-CNV-R(#I)
            //* *  ADD 1 TO #D
            //* *END-FOR
            //* *ADD #SUB-TOT-DOWN-ACROSS(*) TO #SUB-TOT-DOWN-ACROSS-TOT
            //* *WRITE (3) 7X #SUB-TOT-DOWN-ACROSS(1)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(2)(EM=+ZZZ,ZZ9)
            //* *  4X #SUB-TOT-DOWN-ACROSS(3)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(4)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(5)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(6)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(7)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(8)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(9)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(10)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS(11)(EM=+ZZZ,ZZ9)
            //* *  2X #SUB-TOT-DOWN-ACROSS-TOT(EM=+Z,ZZZ,ZZ9)
                                                                                                                                                                          //Natural: PERFORM PRINT-RETRO-FUTURE-NET
            sub_Print_Retro_Future_Net();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"***** NO FUTURE REQUESTS FOR THIS RUN *****");                                                    //Natural: WRITE ( 3 ) // '***** NO FUTURE REQUESTS FOR THIS RUN *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Retro_Future_Net() throws Exception                                                                                                            //Natural: PRINT-RETRO-FUTURE-NET
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(10),"(TOTALS IN THOUSANDS  $000) ----------");                                              //Natural: WRITE ( 3 ) / '-' ( 10 ) '(TOTALS IN THOUSANDS  $000) ----------'
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"FROM:");                                                                                                      //Natural: WRITE ( 3 ) / 'FROM:'
        if (Global.isEscape()) return;
        FOR19:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                    //Natural: IF #FR-FND ( #A ) NE ' '
            {
                getReports().write(3, ReportOption.NOTITLE,pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_A), //Natural: WRITE ( 3 ) #FR-FND ( #A ) 10X #FR-TOT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"TO:");                                                                                                        //Natural: WRITE ( 3 ) / 'TO:'
        if (Global.isEscape()) return;
        FOR20:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                    //Natural: IF #TO-FND ( #A ) NE ' '
            {
                getReports().write(3, ReportOption.NOTITLE,pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_A), //Natural: WRITE ( 3 ) #TO-FND ( #A ) 10X #TO-TOT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR21:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #TO-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue("*").equals(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A))))                                                   //Natural: IF #NET-FND ( * ) = #TO-FND ( #A )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR22:                                                                                                                                                    //Natural: FOR #B 1 #MAX-ACCT
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                {
                    if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).equals(" ")))                                                                                 //Natural: IF #NET-FND ( #B ) = ' '
                    {
                        pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).setValue(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A));                                                  //Natural: ASSIGN #NET-FND ( #B ) := #TO-FND ( #A )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR23:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #FR-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue("*").equals(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A))))                                                   //Natural: IF #NET-FND ( * ) = #FR-FND ( #A )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR24:                                                                                                                                                    //Natural: FOR #B 1 #MAX-ACCT
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                {
                    if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).equals(" ")))                                                                                 //Natural: IF #NET-FND ( #B ) = ' '
                    {
                        pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_B).setValue(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A));                                                  //Natural: ASSIGN #NET-FND ( #B ) := #FR-FND ( #A )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR25:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #TO-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Net_Total_Pnd_Net_Fnd.getValue("*")), new ExamineSearch(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_A)),                   //Natural: EXAMINE #NET-FND ( * ) FOR #TO-FND ( #A ) GIVING INDEX #B
                new ExamineGivingIndex(pnd_B));
            pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_B).nadd(pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_A));                                                                  //Natural: ADD #TO-TOT ( #A ) TO #NET-AMT ( #B )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR26:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A).equals(" ")))                                                                                       //Natural: IF #FR-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Net_Total_Pnd_Net_Fnd.getValue("*")), new ExamineSearch(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_A)),                   //Natural: EXAMINE #NET-FND ( * ) FOR #FR-FND ( #A ) GIVING INDEX #B
                new ExamineGivingIndex(pnd_B));
            pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_B).nadd(pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_A));                                                                  //Natural: ADD #FR-TOT ( #A ) TO #NET-AMT ( #B )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"-------- (NET TOTALS IN THOUSANDS  $000) --------");                                                                  //Natural: WRITE ( 3 ) '-------- (NET TOTALS IN THOUSANDS  $000) --------'
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"FUNDS:");                                                                                                     //Natural: WRITE ( 3 ) / 'FUNDS:'
        if (Global.isEscape()) return;
        FOR27:                                                                                                                                                            //Natural: FOR #A 1 #MAX-ACCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Acct)); pnd_A.nadd(1))
        {
            if (condition(pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_A).notEquals(" ")))                                                                                      //Natural: IF #NET-FND ( #A ) NE ' '
            {
                getReports().write(3, ReportOption.NOTITLE,pnd_Net_Total_Pnd_Net_Fnd.getValue(pnd_A),new ColumnSpacing(10),pnd_Net_Total_Pnd_Net_Amt.getValue(pnd_A),     //Natural: WRITE ( 3 ) #NET-FND ( #A ) 10X #NET-AMT ( #A ) ( EM = +Z,ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("+Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #SUB-TOT #OVRALL-TOT #TO-FND ( * ) #TO-TOT ( * ) #FR-FND ( * ) #FR-TOT ( * ) #NET-FND ( * ) #NET-AMT ( * )
        pnd_Sub_Tot.reset();
        pnd_Ovrall_Tot.reset();
        pnd_Totals_Table_Pnd_To_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_To_Tot.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Tot.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Fnd.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Amt.getValue("*").reset();
        //*  011609 END
    }
    private void sub_Pnd_Get_Total_Units_And_Dollars() throws Exception                                                                                                   //Natural: #GET-TOTAL-UNITS-AND-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*  WRITE 'GET TOTAL UNITS'
        ldaIatl100.getPnd_Tiaa_Units_Cnt().reset();                                                                                                                       //Natural: RESET #TIAA-UNITS-CNT #TIAA-CNTRCT #TIAA-PAYEE #TIAA-FUND #CMPNY-FUND-3 #ACCT-CDE #TIAA-FUND-PYMT
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct().reset();
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee().reset();
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund().reset();
        pnd_Cmpny_Fund_3.reset();
        pnd_Acct_Cde.reset();
        pnd_Tiaa_Fund_Pymt.reset();
        //*    WRITE 'GETTING TOTAL UNITS'
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct().setValue(ldaIatl010.getIatl010_Ia_Frm_Cntrct());                                                         //Natural: MOVE IATL010.IA-FRM-CNTRCT TO #TIAA-CNTRCT
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee().setValue(ldaIatl010.getIatl010_Ia_Frm_Payee());                                                           //Natural: MOVE IATL010.IA-FRM-PAYEE TO #TIAA-PAYEE
        pnd_Acct_Cde.setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                                                  //Natural: MOVE IATL010.XFR-FRM-ACCT-CDE ( #T ) TO #ACCT-CDE
        if (condition(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("A")))                                                                              //Natural: IF IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'A'
        {
                                                                                                                                                                          //Natural: PERFORM #CONVERT-FUND-ANNUAL
            sub_Pnd_Convert_Fund_Annual();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #CONVERT-FUND-MONTHLY
            sub_Pnd_Convert_Fund_Monthly();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund().setValue(pnd_Cmpny_Fund_3);                                                                                //Natural: MOVE #CMPNY-FUND-3 TO #TIAA-FUND
        ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD-VIEW BY TIAA-CNTRCT-FUND-KEY STARTING FROM #TIAA-CNTRCT-FUND-KEY
        (
        "RI",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key(), WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        RI:
        while (condition(ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("RI")))
        {
            if (condition(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().notEquals(ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct())                //Natural: IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT OR IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE NE #TIAA-PAYEE-N OR IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE NE #TIAA-FUND
                || ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde().notEquals(ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee_N()) || 
                ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde().notEquals(ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund())))
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE," NO FUND RECORD FOR CONTRACT/PAYEE CODE","=",ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct(), //Natural: WRITE ( 2 ) / ' NO FUND RECORD FOR CONTRACT/PAYEE CODE' '=' #TIAA-CNTRCT '=' #TIAA-PAYEE '=' #TIAA-FUND 'FOUND'
                    "=",ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Payee(),"=",ldaIatl100.getPnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Fund(),"FOUND");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RI"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RI"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break RI;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RI. )
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl100.getPnd_Tiaa_Units_Cnt().compute(new ComputeParameters(false, ldaIatl100.getPnd_Tiaa_Units_Cnt()), ldaIatl100.getPnd_Tiaa_Units_Cnt().add(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(1)).add(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(2))); //Natural: ADD TIAA-UNITS-CNT ( 1 ) TIAA-UNITS-CNT ( 2 ) TO #TIAA-UNITS-CNT
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Unit_Typ().getValue(pnd_T).equals("M")))                                                                          //Natural: IF IATL010.XFR-FRM-UNIT-TYP ( #T ) = 'M'
            {
                pnd_Tiaa_Fund_Pymt.compute(new ComputeParameters(true, pnd_Tiaa_Fund_Pymt), ldaIatl100.getPnd_Tiaa_Units_Cnt().multiply(pnd_A26_Fields_Pnd_Auv));         //Natural: COMPUTE ROUNDED #TIAA-FUND-PYMT = #TIAA-UNITS-CNT * #AUV
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Fund_Pymt.setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                                                    //Natural: MOVE IAA-TIAA-FUND-RCRD-VIEW.TIAA-TOT-PER-AMT TO #TIAA-FUND-PYMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_To_Sub_Convert() throws Exception                                                                                                                //Natural: #TO-SUB-CONVERT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_To_Sub().reset();                                                                                                                               //Natural: RESET #TO-SUB
        short decideConditionsMet1752 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #FUND-CD-TO;//Natural: VALUE 'C'
        if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("C"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(3);                                                                                                                       //Natural: MOVE 3 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("M"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(4);                                                                                                                       //Natural: MOVE 4 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("S"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(5);                                                                                                                       //Natural: MOVE 5 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("B"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(6);                                                                                                                       //Natural: MOVE 6 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("W"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(7);                                                                                                                       //Natural: MOVE 7 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("L"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(8);                                                                                                                       //Natural: MOVE 8 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("E"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(9);                                                                                                                       //Natural: MOVE 9 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("R"))))
        {
            decideConditionsMet1752++;
            ldaIatl100.getPnd_To_Sub().setValue(10);                                                                                                                      //Natural: MOVE 10 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("I"))))
        {
            decideConditionsMet1752++;
            //*  011609
            ldaIatl100.getPnd_To_Sub().setValue(11);                                                                                                                      //Natural: MOVE 11 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("D"))))
        {
            decideConditionsMet1752++;
            //*  011609
            ldaIatl100.getPnd_To_Sub().setValue(12);                                                                                                                      //Natural: MOVE 12 TO #TO-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "#TO-SUB-CONVERT","=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()); //Natural: WRITE '#TO-SUB-CONVERT' '=' #CONTRACT-NUMBER '=' #PAYEE-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "FUND CODE TO ERROR",ldaIatl100.getPnd_Fund_Cd_To());                                                                                   //Natural: WRITE 'FUND CODE TO ERROR' #FUND-CD-TO
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WRITE '=' #TO-SUB
    }
    private void sub_Pnd_Tiaa_To_Sub_Convert() throws Exception                                                                                                           //Natural: #TIAA-TO-SUB-CONVERT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_To_Sub().reset();                                                                                                                               //Natural: RESET #TO-SUB
        //*  STANDARD
        short decideConditionsMet1785 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #TIAA-FUND-CD-TO;//Natural: VALUE '1','3','5','7','T'
        if (condition((ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("1") || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("3") || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("5") 
            || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("7") || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("T"))))
        {
            decideConditionsMet1785++;
            //*  GRADED
            ldaIatl100.getPnd_To_Sub().setValue(2);                                                                                                                       //Natural: MOVE 2 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE '2','4','6','8'
        else if (condition((ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("2") || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("4") || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("6") 
            || ldaIatl100.getPnd_Tiaa_Fund_Cd_To().equals("8"))))
        {
            decideConditionsMet1785++;
            ldaIatl100.getPnd_To_Sub().setValue(1);                                                                                                                       //Natural: MOVE 1 TO #TO-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "#TIAA-TO-SUB-CONVERT","=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code()); //Natural: WRITE '#TIAA-TO-SUB-CONVERT' '=' #CONTRACT-NUMBER '=' #PAYEE-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "FUND CODE TO ERROR",ldaIatl100.getPnd_Tiaa_Fund_Cd_To());                                                                              //Natural: WRITE 'FUND CODE TO ERROR' #TIAA-FUND-CD-TO
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Display_Total_Linkage() throws Exception                                                                                                         //Natural: #DISPLAY-TOTAL-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //* '=' #CONTRACT-PAYEE
        getReports().write(0, NEWLINE,NEWLINE,"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code());      //Natural: WRITE // '=' #CONTRACT-NUMBER '=' #PAYEE-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code());                                                                                    //Natural: WRITE '=' #FUND-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode());                                                                                         //Natural: WRITE '=' #MODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option());                                                                                       //Natural: WRITE '=' #OPTION
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date());                                                                                   //Natural: WRITE '=' #ISSUE-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date());                                                                           //Natural: WRITE '=' #FINAL-PER-PAY-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob());                                                                                //Natural: WRITE '=' #FIRST-ANN-DOB
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex());                                                                                //Natural: WRITE '=' #FIRST-ANN-SEX
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod());                                                                                //Natural: WRITE '=' #FIRST-ANN-DOD
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob());                                                                               //Natural: WRITE '=' #SECOND-ANN-DOB
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex());                                                                               //Natural: WRITE '=' #SECOND-ANN-SEX
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod());                                                                               //Natural: WRITE '=' #SECOND-ANN-DOD
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date());                                                                      //Natural: WRITE '=' #TRANSFER-EFFECTIVE-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch());                                                                        //Natural: WRITE '=' #TRANSFER-REVAL-SWITCH
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run());                                                                                  //Natural: WRITE '=' #TYPE-OF-RUN
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator());                                                                        //Natural: WRITE '=' #REVALUATION-INDICATOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date());                                                                              //Natural: WRITE '=' #PROCESSING-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date());                                                                        //Natural: WRITE '=' #ILLUSTRATION-EFF-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units());                                                                               //Natural: WRITE '=' #TRANSFER-UNITS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1,":",21));                                                           //Natural: WRITE '=' #FUND-TO-RECEIVE ( 1:21 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1,":",21));                                                          //Natural: WRITE '=' #UNITS-TO-RECEIVE ( 1:21 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1,":",21));                                                           //Natural: WRITE '=' #PMTS-TO-RECEIVE ( 1:21 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte());                                                                               //Natural: WRITE '=' #NEXT-PYMNT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code());                                                                                  //Natural: WRITE '=' #AIAN013-LINKAGE.#RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(1,":",19));                                                             //Natural: WRITE '=' #FUND-CODE-OUT ( 1:19 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(1,":",19));                                                                 //Natural: WRITE '=' #UNITS-OUT ( 1:19 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1,":",19));                                                                   //Natural: WRITE '=' #AUV-OUT ( 1:19 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1,":",19));                                                     //Natural: WRITE '=' #TRANSFER-AMT-OUT-CREF ( 1:19 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(1,":",2));                                                        //Natural: WRITE '=' #PMT-METHOD-CODE-OUT ( 1:2 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(1,":",2));                                                              //Natural: WRITE '=' #RATE-CODE-OUT ( 1:2 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(1,":",2));                                                                //Natural: WRITE '=' #GTD-PMT-OUT ( 1:2 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(1,":",2));                                                                //Natural: WRITE '=' #DVD-PMT-OUT ( 1:2 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(1,":",2));                                                      //Natural: WRITE '=' #TRANSFER-AMT-OUT-TIAA ( 1:2 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out(),NEWLINE,NEWLINE);                                                       //Natural: WRITE '=' #TOTAL-TRANSFER-AMT-OUT //
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Text_Table_Para() throws Exception                                                                                                               //Natural: #TEXT-TABLE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_Text_Table().getValue(1).setValue("GRADED");                                                                                                    //Natural: MOVE 'GRADED' TO #TEXT-TABLE ( 1 )
        ldaIatl100.getPnd_Text_Table().getValue(2).setValue("STNDRD");                                                                                                    //Natural: MOVE 'STNDRD' TO #TEXT-TABLE ( 2 )
        ldaIatl100.getPnd_Text_Table().getValue(3).setValue("STOCK ");                                                                                                    //Natural: MOVE 'STOCK ' TO #TEXT-TABLE ( 3 )
        ldaIatl100.getPnd_Text_Table().getValue(4).setValue("MMA   ");                                                                                                    //Natural: MOVE 'MMA   ' TO #TEXT-TABLE ( 4 )
        ldaIatl100.getPnd_Text_Table().getValue(5).setValue("SOCIAL");                                                                                                    //Natural: MOVE 'SOCIAL' TO #TEXT-TABLE ( 5 )
        ldaIatl100.getPnd_Text_Table().getValue(6).setValue("BOND  ");                                                                                                    //Natural: MOVE 'BOND  ' TO #TEXT-TABLE ( 6 )
        ldaIatl100.getPnd_Text_Table().getValue(7).setValue("GLOBAL");                                                                                                    //Natural: MOVE 'GLOBAL' TO #TEXT-TABLE ( 7 )
        ldaIatl100.getPnd_Text_Table().getValue(8).setValue("GROWTH");                                                                                                    //Natural: MOVE 'GROWTH' TO #TEXT-TABLE ( 8 )
        ldaIatl100.getPnd_Text_Table().getValue(9).setValue("EQUITY");                                                                                                    //Natural: MOVE 'EQUITY' TO #TEXT-TABLE ( 9 )
        ldaIatl100.getPnd_Text_Table().getValue(10).setValue("REA   ");                                                                                                   //Natural: MOVE 'REA   ' TO #TEXT-TABLE ( 10 )
        ldaIatl100.getPnd_Text_Table().getValue(11).setValue("ILB   ");                                                                                                   //Natural: MOVE 'ILB   ' TO #TEXT-TABLE ( 11 )
    }
    private void sub_Pnd_From_Sub_Convert() throws Exception                                                                                                              //Natural: #FROM-SUB-CONVERT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*   WRITE 'FROM-SUB-CONVERT' '=' #FUND-CD-FROM
        ldaIatl100.getPnd_From_Sub().reset();                                                                                                                             //Natural: RESET #FROM-SUB
        short decideConditionsMet1854 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #FUND-CD-FROM;//Natural: VALUE 'G'
        if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("G"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(1);                                                                                                                     //Natural: MOVE 1 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("T"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(2);                                                                                                                     //Natural: MOVE 2 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("C"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(3);                                                                                                                     //Natural: MOVE 3 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("M"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(4);                                                                                                                     //Natural: MOVE 4 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("S"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(5);                                                                                                                     //Natural: MOVE 5 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("B"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(6);                                                                                                                     //Natural: MOVE 6 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("W"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(7);                                                                                                                     //Natural: MOVE 7 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("L"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(8);                                                                                                                     //Natural: MOVE 8 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("E"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(9);                                                                                                                     //Natural: MOVE 9 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("R"))))
        {
            decideConditionsMet1854++;
            ldaIatl100.getPnd_From_Sub().setValue(10);                                                                                                                    //Natural: MOVE 10 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("I"))))
        {
            decideConditionsMet1854++;
            //*  011609
            ldaIatl100.getPnd_From_Sub().setValue(11);                                                                                                                    //Natural: MOVE 11 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("D"))))
        {
            decideConditionsMet1854++;
            //*  011609
            ldaIatl100.getPnd_From_Sub().setValue(12);                                                                                                                    //Natural: MOVE 12 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "ERROR IN FROM FUND",ldaIatl100.getPnd_Fund_Cd_From());                                                                                 //Natural: WRITE 'ERROR IN FROM FUND' #FUND-CD-FROM
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WRITE '=' #FROM-SUB
    }
    private void sub_Pnd_Convert_Fund_Annual() throws Exception                                                                                                           //Natural: #CONVERT-FUND-ANNUAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet1888 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #ACCT-CDE;//Natural: VALUE 'G'
        if (condition((pnd_Acct_Cde.equals("G"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("TIG");                                                                                                                             //Natural: MOVE 'TIG' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Acct_Cde.equals("T"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("TIS");                                                                                                                             //Natural: MOVE 'TIS' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Acct_Cde.equals("C"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("202");                                                                                                                             //Natural: MOVE '202' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((pnd_Acct_Cde.equals("M"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("203");                                                                                                                             //Natural: MOVE '203' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Acct_Cde.equals("S"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("204");                                                                                                                             //Natural: MOVE '204' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((pnd_Acct_Cde.equals("B"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("205");                                                                                                                             //Natural: MOVE '205' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((pnd_Acct_Cde.equals("W"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("206");                                                                                                                             //Natural: MOVE '206' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((pnd_Acct_Cde.equals("E"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("207");                                                                                                                             //Natural: MOVE '207' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Acct_Cde.equals("L"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("208");                                                                                                                             //Natural: MOVE '208' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Acct_Cde.equals("R"))))
        {
            decideConditionsMet1888++;
            //*  011609
            pnd_Cmpny_Fund_3.setValue("U09");                                                                                                                             //Natural: MOVE 'U09' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pnd_Acct_Cde.equals("D"))))
        {
            decideConditionsMet1888++;
            //*  011609
            pnd_Cmpny_Fund_3.setValue("U11");                                                                                                                             //Natural: MOVE 'U11' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pnd_Acct_Cde.equals("I"))))
        {
            decideConditionsMet1888++;
            pnd_Cmpny_Fund_3.setValue("210");                                                                                                                             //Natural: MOVE '210' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Cmpny_Fund_3.setValue("   ");                                                                                                                             //Natural: MOVE '   ' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Convert_Fund_Monthly() throws Exception                                                                                                          //Natural: #CONVERT-FUND-MONTHLY
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet1921 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #ACCT-CDE;//Natural: VALUE 'G'
        if (condition((pnd_Acct_Cde.equals("G"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("TIG");                                                                                                                             //Natural: MOVE 'TIG' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Acct_Cde.equals("T"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("TIS");                                                                                                                             //Natural: MOVE 'TIS' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Acct_Cde.equals("C"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("402");                                                                                                                             //Natural: MOVE '402' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((pnd_Acct_Cde.equals("M"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("403");                                                                                                                             //Natural: MOVE '403' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Acct_Cde.equals("S"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("404");                                                                                                                             //Natural: MOVE '404' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((pnd_Acct_Cde.equals("B"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("405");                                                                                                                             //Natural: MOVE '405' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((pnd_Acct_Cde.equals("W"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("406");                                                                                                                             //Natural: MOVE '406' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((pnd_Acct_Cde.equals("E"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("407");                                                                                                                             //Natural: MOVE '407' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Acct_Cde.equals("L"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("408");                                                                                                                             //Natural: MOVE '408' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Acct_Cde.equals("R"))))
        {
            decideConditionsMet1921++;
            //*  011609
            pnd_Cmpny_Fund_3.setValue("W09");                                                                                                                             //Natural: MOVE 'W09' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pnd_Acct_Cde.equals("D"))))
        {
            decideConditionsMet1921++;
            //*  011609
            pnd_Cmpny_Fund_3.setValue("W11");                                                                                                                             //Natural: MOVE 'W11' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pnd_Acct_Cde.equals("I"))))
        {
            decideConditionsMet1921++;
            pnd_Cmpny_Fund_3.setValue("410");                                                                                                                             //Natural: MOVE '410' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Cmpny_Fund_3.setValue("   ");                                                                                                                             //Natural: MOVE '   ' TO #CMPNY-FUND-3
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number().reset();                                                                                                 //Natural: RESET #CONTRACT-NUMBER #PAYEE-CODE #FUND-CODE #MODE #OPTION #ORIGIN #ISSUE-DATE #FINAL-PER-PAY-DATE #FIRST-ANN-DOB #FIRST-ANN-SEX #FIRST-ANN-DOD #SECOND-ANN-DOB #SECOND-ANN-SEX #SECOND-ANN-DOD #TRANSFER-EFFECTIVE-DATE #TRANSFER-REVAL-SWITCH #TYPE-OF-RUN #REVALUATION-INDICATOR #PROCESSING-DATE #ILLUSTRATION-EFF-DATE #NEXT-PYMNT-DTE #TRANSFER-UNITS #FUND-TO-RECEIVE ( 1:21 ) #UNITS-TO-RECEIVE ( 1:21 ) #PMTS-TO-RECEIVE ( 1:21 ) #AIAN013-LINKAGE.#RETURN-CODE #FUND-CODE-OUT ( 1:19 ) #UNITS-OUT ( 1:19 ) #AUV-OUT ( 1:19 ) #TRANSFER-AMT-OUT-CREF ( 1:19 ) #PMT-METHOD-CODE-OUT ( 1:2 ) #RATE-CODE-OUT ( 1:2 ) #GTD-PMT-OUT ( 1:2 ) #DVD-PMT-OUT ( 1:2 ) #TRANSFER-AMT-OUT-TIAA ( 1:2 ) #TOTAL-TRANSFER-AMT-OUT #J #TO-UNITS #D-UNITS #W-UNITS #E-UNITS #TO-PYMTS #D-PYMTS #W-PYMTS #E-PYMTS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Origin().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Date().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Processing_Date().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().reset();
        pnd_J.reset();
        pnd_To_Units.reset();
        pnd_D_Units.reset();
        pnd_W_Units.reset();
        pnd_E_Units.reset();
        pnd_To_Pymts.reset();
        pnd_D_Pymts.reset();
        pnd_W_Pymts.reset();
        pnd_E_Pymts.reset();
    }
    private void sub_Pnd_Reset_Aian019_Para() throws Exception                                                                                                            //Natural: #RESET-AIAN019-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_To_Cref_Arrays().getValue("*").reset();                                                                            //Natural: RESET #AIAN019-TO-CREF-ARRAYS ( * ) #AIAN019-TIAA-ARRAYS ( *,* ) #CREF-PRORATE #AIAN019-CREF-ARRAYS ( * ) #AIAN019-PAYMENT-METHOD
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tiaa_Arrays().getValue("*","*").reset();
        pnd_Cref_Prorate.reset();
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Arrays().getValue("*").reset();
        pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method().reset();
    }
    private void sub_Pnd_Reset_Fund_Info() throws Exception                                                                                                               //Natural: #RESET-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().reset();                                                                                                  //Natural: RESET #TRANSFER-UNITS #FUND-CODE #FUND-TO-RECEIVE ( 1:21 ) #UNITS-TO-RECEIVE ( 1:21 ) #PMTS-TO-RECEIVE ( 1:21 ) #AIAN013-LINKAGE.#RETURN-CODE #FUND-CODE-OUT ( 1:19 ) #UNITS-OUT ( 1:19 ) #AUV-OUT ( 1:19 ) #TRANSFER-AMT-OUT-CREF ( 1:19 ) #PMT-METHOD-CODE-OUT ( 1:2 ) #RATE-CODE-OUT ( 1:2 ) #GTD-PMT-OUT ( 1:2 ) #DVD-PMT-OUT ( 1:2 ) #TRANSFER-AMT-OUT-TIAA ( 1:2 ) #TOTAL-TRANSFER-AMT-OUT #J #TO-UNITS #D-UNITS #W-UNITS #E-UNITS #TO-PYMTS #D-PYMTS #W-PYMTS #E-PYMTS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(1,":",21).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Return_Code().reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1,":",19).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmt_Method_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Rate_Code_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Gtd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Dvd_Pmt_Out().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Tiaa().getValue(1,":",2).reset();
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Total_Transfer_Amt_Out().reset();
        pnd_J.reset();
        pnd_To_Units.reset();
        pnd_D_Units.reset();
        pnd_W_Units.reset();
        pnd_E_Units.reset();
        pnd_To_Pymts.reset();
        pnd_D_Pymts.reset();
        pnd_W_Pymts.reset();
        pnd_E_Pymts.reset();
    }
    private void sub_Pnd_Reset_Print_Report() throws Exception                                                                                                            //Natural: #RESET-PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*  016109
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #TO-FROM-FUNDS ( *,* ) #TO-FROM-TABLE ( *,* ) #TOT-ACROSS-CNV ( * ) #TOT-DOWN-CNV ( * ) #SUB-TOT-DOWN-ACROSS ( * ) #SUB-TOT-DOWN-ACROSS-TOT #TOT-DOWN-TOT
        ldaIatl100.getPnd_To_From_Funds().getValue("*","*").reset();
        ldaIatl100.getPnd_To_From_Table().getValue("*","*").reset();
        ldaIatl100.getPnd_Tot_Across_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Tot_Down_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across_Tot().reset();
        ldaIatl100.getPnd_Tot_Down_Tot().reset();
    }
    private void sub_Pnd_Reset_Print_Report_Totals() throws Exception                                                                                                     //Natural: #RESET-PRINT-REPORT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #TOT-ACROSS-CNV ( * ) #TOT-DOWN-CNV ( * ) #SUB-TOT-DOWN-ACROSS ( * ) #SUB-TOT-DOWN-ACROSS-TOT #TOT-DOWN-TOT
        ldaIatl100.getPnd_Tot_Across_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Tot_Down_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across_Tot().reset();
        ldaIatl100.getPnd_Tot_Down_Tot().reset();
    }
    private void sub_Pnd_Monthly_Trnsfr_Units() throws Exception                                                                                                          //Natural: #MONTHLY-TRNSFR-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIatl010.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))                                                                                   //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'D'
        {
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Fund_Pymt)))                                                               //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-FUND-PYMT
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = #TIAA-FUND-PYMT / #AUV
                    pnd_Tiaa_Fund_Pymt.divide(pnd_A26_Fields_Pnd_Auv));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = IATL010.XFR-FRM-QTY ( #T ) / #AUV
                    ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(pnd_A26_Fields_Pnd_Auv));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                               //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()),     //Natural: COMPUTE ROUNDED #TRANSFER-UNITS = #TIAA-UNITS-CNT * IATL010.XFR-FRM-QTY ( #T ) / 100
                    ldaIatl100.getPnd_Tiaa_Units_Cnt().multiply(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(ldaIatl100.getPnd_Tiaa_Units_Cnt())))                                           //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-UNITS-CNT
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(ldaIatl100.getPnd_Tiaa_Units_Cnt());                                                 //Natural: ASSIGN #TRANSFER-UNITS := #TIAA-UNITS-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                //Natural: ASSIGN #TRANSFER-UNITS := IATL010.XFR-FRM-QTY ( #T )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Annual_Trnsfr_Units_And_Dollars() throws Exception                                                                                               //Natural: #ANNUAL-TRNSFR-UNITS-AND-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Per.reset();                                                                                                                                                  //Natural: RESET #PER #TRANSFER-DOLLARS #XFR-UNITS
        pnd_Transfer_Dollars.reset();
        pnd_Xfr_Units.reset();
        if (condition(ldaIatl010.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("D")))                                                                                   //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'D'
        {
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(pnd_Tiaa_Fund_Pymt)))                                                               //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-FUND-PYMT
            {
                pnd_Transfer_Dollars.setValue(pnd_Tiaa_Fund_Pymt);                                                                                                        //Natural: MOVE #TIAA-FUND-PYMT TO #TRANSFER-DOLLARS
                pnd_Xfr_Units.setValue(ldaIatl100.getPnd_Tiaa_Units_Cnt());                                                                                               //Natural: MOVE #TIAA-UNITS-CNT TO #XFR-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Per.compute(new ComputeParameters(true, pnd_Per), ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(pnd_Tiaa_Fund_Pymt).multiply(100));      //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-FRM-QTY ( #T ) / #TIAA-FUND-PYMT * 100
                //*            WRITE '='  #PER
                //*              '=' IATL010.XFR-FRM-QTY(#T) '=' #TIAA-FUND-PYMT
                pnd_Xfr_Units.compute(new ComputeParameters(false, pnd_Xfr_Units), pnd_Per.multiply(ldaIatl100.getPnd_Tiaa_Units_Cnt()).divide(100));                     //Natural: COMPUTE #XFR-UNITS = #PER * #TIAA-UNITS-CNT / 100
                pnd_Transfer_Dollars.setValue(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                       //Natural: MOVE IATL010.XFR-FRM-QTY ( #T ) TO #TRANSFER-DOLLARS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Typ().getValue(pnd_T).equals("P")))                                                                               //Natural: IF IATL010.XFR-FRM-TYP ( #T ) = 'P'
            {
                pnd_Xfr_Units.compute(new ComputeParameters(true, pnd_Xfr_Units), ldaIatl100.getPnd_Tiaa_Units_Cnt().multiply(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #XFR-UNITS = #TIAA-UNITS-CNT * IATL010.XFR-FRM-QTY ( #T ) / 100
                pnd_Transfer_Dollars.compute(new ComputeParameters(true, pnd_Transfer_Dollars), pnd_Tiaa_Fund_Pymt.multiply(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100)); //Natural: COMPUTE ROUNDED #TRANSFER-DOLLARS = #TIAA-FUND-PYMT * IATL010.XFR-FRM-QTY ( #T ) / 100
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*         WRITE '=' IATL010.XFR-FRM-QTY(#T) '=' #TIAA-UNITS-CNT
                if (condition(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).greater(ldaIatl100.getPnd_Tiaa_Units_Cnt())))                                           //Natural: IF IATL010.XFR-FRM-QTY ( #T ) > #TIAA-UNITS-CNT
                {
                    pnd_Transfer_Dollars.setValue(pnd_Tiaa_Fund_Pymt);                                                                                                    //Natural: MOVE #TIAA-FUND-PYMT TO #TRANSFER-DOLLARS
                    pnd_Xfr_Units.setValue(ldaIatl100.getPnd_Tiaa_Units_Cnt());                                                                                           //Natural: MOVE #TIAA-UNITS-CNT TO #XFR-UNITS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Per.compute(new ComputeParameters(true, pnd_Per), ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T).divide(ldaIatl100.getPnd_Tiaa_Units_Cnt()).multiply(100)); //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-FRM-QTY ( #T ) / #TIAA-UNITS-CNT * 100
                    pnd_Transfer_Dollars.compute(new ComputeParameters(false, pnd_Transfer_Dollars), pnd_Per.multiply(pnd_Tiaa_Fund_Pymt).divide(100));                   //Natural: COMPUTE #TRANSFER-DOLLARS = #PER * #TIAA-FUND-PYMT / 100
                    pnd_Xfr_Units.setValue(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                          //Natural: ASSIGN #XFR-UNITS := IATL010.XFR-FRM-QTY ( #T )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(pnd_Xfr_Units);                                                                                  //Natural: ASSIGN #TRANSFER-UNITS := #XFR-UNITS
    }
    private void sub_Pnd_Monthly_Transfer_To_Arrays() throws Exception                                                                                                    //Natural: #MONTHLY-TRANSFER-TO-ARRAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W_Per.reset();                                                                                                                                                //Natural: RESET #W-PER
        FOR28:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                        //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                //*         WRITE / '='IATL010.XFR-TO-ACCT-CD(#I) '='IATL010.XFR-TO-TYP(#I)
                //*               '=' IATL010.XFR-TO-QTY(#I)
                if (condition(ldaIatl010.getIatl010_Xfr_To_Typ().getValue(pnd_I).equals("P")))                                                                            //Natural: IF IATL010.XFR-TO-TYP ( #I ) = 'P'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #TRANSFER-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().multiply(ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Per.compute(new ComputeParameters(true, pnd_W_Per), ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_I).divide(pnd_To_Qty_Total).multiply(100)); //Natural: COMPUTE ROUNDED #W-PER = IATL010.XFR-TO-QTY ( #I ) / #TO-QTY-TOTAL * 100
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #W-PER * #TRANSFER-UNITS / 100
                        pnd_W_Per.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()).divide(100));
                    //*          COMPUTE ROUNDED IATL010.XFR-TO-QTY(#I) =
                    //*            #W-PER * IATL010.XFR-FRM-QTY(#T)  / 100
                    //*           COMPUTE ROUNDED #UNITS-TO-RECEIVE(#I) =
                    //*            IATL010.XFR-TO-QTY(#I) / #AUV
                    //*            WRITE '=' #UNITS-TO-RECEIVE(#I)
                }                                                                                                                                                         //Natural: END-IF
                //*  011609 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(#I) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(#I) := '2'
                //*      ELSE
                //*  = 'G'         02/10
                //*  02/10
                if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue("2");                                                               //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := '2'
                    //*  011609 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));           //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I));                                                             //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Annual_Transfer_To_Arrays() throws Exception                                                                                                     //Natural: #ANNUAL-TRANSFER-TO-ARRAYS
    {
        if (BLNatReinput.isReinput()) return;

        FOR29:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).notEquals(" ")))                                                                        //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) NE ' '
            {
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                //*         WRITE / '='IATL010.XFR-TO-ACCT-CD(#I) '='IATL010.XFR-TO-TYP(#I)
                //*               '=' IATL010.XFR-TO-QTY(#I)
                                                                                                                                                                          //Natural: PERFORM #FILL-TO-SIDE
                sub_Pnd_Fill_To_Side();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  011609 START
                //*    IF IATL010.XFR-TO-ACCT-CDE(#I) = 'T'
                //*      #FUND-TO-RECEIVE(#I) := '1'
                //*    ELSE
                //*      IF IATL010.XFR-TO-ACCT-CDE(#I) = 'G'
                //*        #FUND-TO-RECEIVE(#I) := '2'
                //*  = 'G'          02/10
                //*  02/10
                if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I).equals("G")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #I ) = 'G'
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue("2");                                                               //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := '2'
                    //*  011609 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(pnd_I).setValue(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_I));           //Natural: ASSIGN #FUND-TO-RECEIVE ( #I ) := IATL010.XFR-TO-ACCT-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                pnd_To_Units.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I));                                                             //Natural: ASSIGN #TO-UNITS := #TO-UNITS + #UNITS-TO-RECEIVE ( #I )
                pnd_To_Pymts.nadd(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I));                                                              //Natural: ASSIGN #TO-PYMTS := #TO-PYMTS + #PMTS-TO-RECEIVE ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_To_Side() throws Exception                                                                                                                  //Natural: #FILL-TO-SIDE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  WRITE 'FILL-TO-SIDE'
        pnd_Per.reset();                                                                                                                                                  //Natural: RESET #PER
        if (condition(ldaIatl010.getIatl010_Xfr_To_Typ().getValue(pnd_I).equals("P")))                                                                                    //Natural: IF IATL010.XFR-TO-TYP ( #I ) = 'P'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #XFR-UNITS * IATL010.XFR-TO-QTY ( #I ) / 100
                pnd_Xfr_Units.multiply(ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #PMTS-TO-RECEIVE ( #I ) = #TRANSFER-DOLLARS * IATL010.XFR-TO-QTY ( #I ) / 100
                pnd_Transfer_Dollars.multiply(ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_I)).divide(100));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Per.compute(new ComputeParameters(true, pnd_Per), ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_I).divide(pnd_To_Qty_Total).multiply(100));             //Natural: COMPUTE ROUNDED #PER = IATL010.XFR-TO-QTY ( #I ) / #TO-QTY-TOTAL * 100
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #UNITS-TO-RECEIVE ( #I ) = #PER * #TRANSFER-UNITS / 100
                pnd_Per.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units()).divide(100));
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).compute(new ComputeParameters(true, pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED #PMTS-TO-RECEIVE ( #I ) = #PER * IATL010.XFR-FRM-QTY ( #T ) / 100
                pnd_Per.multiply(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T)).divide(100));
            //*     COMPUTE ROUNDED IATL010.XFR-TO-QTY(#I) =
            //*           #PER * IATL010.XFR-FRM-QTY(#T)  / 100
            //*       #PMTS-TO-RECEIVE(#I) := IATL010.XFR-TO-QTY(#I)
            //*   WRITE '=' IATL010.XFR-TO-QTY(#I)  '=' #TRANSFER-DOLLARS
            //*       COMPUTE ROUNDED #PER =
            //*           IATL010.XFR-TO-QTY(#I) / #TRANSFER-DOLLARS * 100
            //*       COMPUTE ROUNDED #UNITS-TO-RECEIVE(#I) =
            //*           #PER * #XFR-UNITS / 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Rounding_Units() throws Exception                                                                                                                //Natural: #ROUNDING-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_D_Units.compute(new ComputeParameters(false, pnd_D_Units), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().subtract(pnd_To_Units));                   //Natural: ASSIGN #D-UNITS := #TRANSFER-UNITS - #TO-UNITS
        //*  WRITE '=' #J '=' #D-UNITS
        if (condition(pnd_D_Units.greater(getZero())))                                                                                                                    //Natural: IF #D-UNITS GT 0
        {
            if (condition(pnd_J.greater(2)))                                                                                                                              //Natural: IF #J GT 2
            {
                pnd_W_Units.compute(new ComputeParameters(false, pnd_W_Units), pnd_J.multiply(new DbsDecimal(".001")));                                                   //Natural: ASSIGN #W-UNITS := #J * .001
                short decideConditionsMet2124 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #D-UNITS LE #W-UNITS
                if (condition(pnd_D_Units.lessOrEqual(pnd_W_Units)))
                {
                    decideConditionsMet2124++;
                    pnd_E_Units.setValue(0);                                                                                                                              //Natural: ASSIGN #E-UNITS := .001
                }                                                                                                                                                         //Natural: WHEN #D-UNITS > #W-UNITS
                else if (condition(pnd_D_Units.greater(pnd_W_Units)))
                {
                    decideConditionsMet2124++;
                    pnd_E_Units.compute(new ComputeParameters(true, pnd_E_Units), pnd_D_Units.divide(pnd_J));                                                             //Natural: COMPUTE ROUNDED #E-UNITS = #D-UNITS / #J
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet2124 > 0))
                {
                    F8:                                                                                                                                                   //Natural: FOR #I = 1 TO #J
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
                    {
                        if (condition((pnd_To_Units.add(pnd_E_Units)).less(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units())))                                     //Natural: IF ( #TO-UNITS + #E-UNITS ) LT #TRANSFER-UNITS
                        {
                            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_I).nadd(pnd_E_Units);                                                  //Natural: ADD #E-UNITS TO #UNITS-TO-RECEIVE ( #I )
                            //* ***         ADD #UNITS-TO-RECEIVE(#I) TO #TO-UNITS
                            pnd_To_Units.nadd(pnd_E_Units);                                                                                                               //Natural: ADD #E-UNITS TO #TO-UNITS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().notEquals(pnd_To_Units)))                                                       //Natural: IF #TRANSFER-UNITS NE #TO-UNITS
                    {
                        pnd_E_Units.compute(new ComputeParameters(false, pnd_E_Units), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().subtract(pnd_To_Units));   //Natural: ASSIGN #E-UNITS := #TRANSFER-UNITS - #TO-UNITS
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J).nadd(pnd_E_Units);                                                      //Natural: ADD #E-UNITS TO #UNITS-TO-RECEIVE ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(2).nadd(pnd_D_Units);                                                                  //Natural: ADD #D-UNITS TO #UNITS-TO-RECEIVE ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(pnd_J).nadd(pnd_D_Units);                                                                  //Natural: ADD #D-UNITS TO #UNITS-TO-RECEIVE ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Rounding_Dollars() throws Exception                                                                                                              //Natural: #ROUNDING-DOLLARS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_D_Pymts.compute(new ComputeParameters(false, pnd_D_Pymts), pnd_Transfer_Dollars.subtract(pnd_To_Pymts));                                                      //Natural: ASSIGN #D-PYMTS := #TRANSFER-DOLLARS - #TO-PYMTS
        //*  WRITE '=' #J '=' #D-PYMTS
        if (condition(pnd_D_Pymts.greater(getZero())))                                                                                                                    //Natural: IF #D-PYMTS GT 0
        {
            if (condition(pnd_J.greater(2)))                                                                                                                              //Natural: IF #J GT 2
            {
                pnd_W_Pymts.compute(new ComputeParameters(false, pnd_W_Pymts), pnd_J.multiply(new DbsDecimal(".01")));                                                    //Natural: ASSIGN #W-PYMTS := #J * .01
                short decideConditionsMet2160 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #D-PYMTS LE #W-PYMTS
                if (condition(pnd_D_Pymts.lessOrEqual(pnd_W_Pymts)))
                {
                    decideConditionsMet2160++;
                    pnd_E_Pymts.setValue(0);                                                                                                                              //Natural: ASSIGN #E-PYMTS := .01
                }                                                                                                                                                         //Natural: WHEN #D-PYMTS > #W-PYMTS
                else if (condition(pnd_D_Pymts.greater(pnd_W_Pymts)))
                {
                    decideConditionsMet2160++;
                    pnd_E_Pymts.compute(new ComputeParameters(true, pnd_E_Pymts), pnd_D_Pymts.divide(pnd_J));                                                             //Natural: COMPUTE ROUNDED #E-PYMTS = #D-PYMTS / #J
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet2160 > 0))
                {
                    F9:                                                                                                                                                   //Natural: FOR #I = 1 TO #J
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
                    {
                        if (condition((pnd_To_Pymts.add(pnd_E_Pymts)).less(pnd_Transfer_Dollars)))                                                                        //Natural: IF ( #TO-PYMTS + #E-PYMTS ) LT #TRANSFER-DOLLARS
                        {
                            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_I).nadd(pnd_E_Pymts);                                                   //Natural: ADD #E-PYMTS TO #PMTS-TO-RECEIVE ( #I )
                            //* ***         ADD #PYMTS-TO-RECEIVE(#I) TO #TO-PYMTS
                            pnd_To_Pymts.nadd(pnd_E_Pymts);                                                                                                               //Natural: ADD #E-PYMTS TO #TO-PYMTS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    if (condition(pnd_Transfer_Dollars.notEquals(pnd_To_Pymts)))                                                                                          //Natural: IF #TRANSFER-DOLLARS NE #TO-PYMTS
                    {
                        pnd_E_Pymts.compute(new ComputeParameters(false, pnd_E_Pymts), pnd_Transfer_Dollars.subtract(pnd_To_Pymts));                                      //Natural: ASSIGN #E-PYMTS := #TRANSFER-DOLLARS - #TO-PYMTS
                        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J).nadd(pnd_E_Pymts);                                                       //Natural: ADD #E-PYMTS TO #PMTS-TO-RECEIVE ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(2).nadd(pnd_D_Pymts);                                                                   //Natural: ADD #D-PYMTS TO #PMTS-TO-RECEIVE ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Pmts_To_Receive().getValue(pnd_J).nadd(pnd_D_Pymts);                                                                   //Natural: ADD #D-PYMTS TO #PMTS-TO-RECEIVE ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Read_Fund_Record() throws Exception                                                                                                              //Natural: #READ-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ IAA-TIAA-FUND-RCRD-VIEW BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1X",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1X:
        while (condition(ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("R1X")))
        {
            if (condition(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD-VIEW.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                if (condition(!(DbsUtil.maskMatches(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Fund_Cde(),"'T'"))))                                                 //Natural: ACCEPT IF IAA-TIAA-FUND-RCRD-VIEW.TIAA-CMPNY-FUND-CDE = MASK ( 'T' )
                {
                    continue;
                }
                //*        WRITE '='  IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE(1:5)
                //*  '='  IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT(1:5) '=' #W-PCT
                //*  040612
                DbsUtil.callnat(Iatn270.class , getCurrentProcessState(), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*"),  //Natural: CALLNAT 'IATN270' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * ) #W-PCT
                    ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"), pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                //*  02/10
                //*  040612
                //*  02/10
                //*  040612
                //*  02/10
                DbsUtil.callnat(Iatn270.class , getCurrentProcessState(), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*"),  //Natural: CALLNAT 'IATN270' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * ) #W-PCT
                    ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"), pnd_W_Pct);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_S,"*").setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*")); //Natural: ASSIGN #AIAN019-RATE-CODE ( #S,* ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * )
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gic_Code().getValue(pnd_S,"*").setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*")); //Natural: ASSIGN #AIAN019-GIC-CODE ( #S,* ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * )
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Date().getValue(pnd_S,"*").setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte().getValue("*")); //Natural: ASSIGN #AIAN019-TRANSFER-DATE ( #S,* ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-DTE ( * )
                pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*").setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue("*"));                                //Natural: ASSIGN #GTD-PMT ( * ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT ( * )
                pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*").setValue(ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue("*"));                                //Natural: ASSIGN #DVD-PMT ( * ) := IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-DIV-AMT ( * )
                //*    #AIAN019-GTD-PMT(#S,*) :=
                //*      IAA-TIAA-FUND-RCRD-VIEW.TIAA-PER-PAY-AMT(*)
                //*  040612
                DbsUtil.callnat(Iatn270.class , getCurrentProcessState(), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*"),  //Natural: CALLNAT 'IATN270' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * ) #GTD-PMT ( * ) #W-PCT-TO
                    pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*"), pnd_W_Pct_To);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue(pnd_S,"*").setValue(pnd_Tiaa_Rates_Pnd_Gtd_Pmt.getValue("*"));                          //Natural: ASSIGN #AIAN019-GTD-PMT ( #S,* ) := #GTD-PMT ( * )
                //*  02/10
                //*  040612
                //*  02/10
                //*  02/10
                DbsUtil.callnat(Iatn270.class , getCurrentProcessState(), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue("*"), ldaIatl100.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Gic().getValue("*"),  //Natural: CALLNAT 'IATN270' IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD-VIEW.TIAA-RATE-GIC ( * ) #DVD-PMT ( * ) #W-PCT-TO
                    pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*"), pnd_W_Pct_To);
                if (condition(Global.isEscape())) return;
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Div_Pmt().getValue(pnd_S,"*").setValue(pnd_Tiaa_Rates_Pnd_Dvd_Pmt.getValue("*"));                          //Natural: ASSIGN #AIAN019-DIV-PMT ( #S,* ) := #DVD-PMT ( * )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1X;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1X. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Teachers_To_Cref() throws Exception                                                                                                              //Natural: #TEACHERS-TO-CREF
    {
        if (BLNatReinput.isReinput()) return;

        FZ1:                                                                                                                                                              //Natural: FOR #T = 1 TO 20
        for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
        {
            if (condition(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                          //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) EQ ' '
            {
                if (true) break FZ1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FZ1. )
            }                                                                                                                                                             //Natural: END-IF
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method().setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                           //Natural: ASSIGN #AIAN019-PAYMENT-METHOD := IATL010.XFR-FRM-ACCT-CDE ( #T )
            pnd_W_Pct.setValue(ldaIatl010.getIatl010_Xfr_Frm_Qty().getValue(pnd_T));                                                                                      //Natural: ASSIGN #W-PCT := IATL010.XFR-FRM-QTY ( #T )
            FX:                                                                                                                                                           //Natural: FOR #S = 1 TO 20
            for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(20)); pnd_S.nadd(1))
            {
                if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_S).equals(" ")))                                                                       //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #S ) EQ ' '
                {
                    if (true) break FX;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FX. )
                }                                                                                                                                                         //Natural: END-IF
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_S).setValue(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(pnd_S));          //Natural: MOVE IATL010.XFR-TO-ACCT-CDE ( #S ) TO #AIAN019-CREF-FUND-IN ( #S )
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Reval_Method().getValue(pnd_S).setValue(ldaIatl010.getIatl010_Xfr_To_Unit_Typ().getValue(pnd_S));     //Natural: MOVE IATL010.XFR-TO-UNIT-TYP ( #S ) TO #AIAN019-CREF-REVAL-METHOD ( #S )
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number());                                     //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #AIAN019-CONTRACT-NUMBER
                pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code());                                             //Natural: ASSIGN #W-CNTRCT-PAYEE = #AIAN019-PAYEE-CODE
                pnd_W_Pct_To.setValue(ldaIatl010.getIatl010_Xfr_To_Qty().getValue(pnd_S));                                                                                //Natural: ASSIGN #W-PCT-TO := IATL010.XFR-TO-QTY ( #S )
                                                                                                                                                                          //Natural: PERFORM #READ-FUND-RECORD
                sub_Pnd_Read_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   WRITE 'BEFORE' CALLNAT 'ARI015' #AIAN019-LINKAGE
            //*  OS 061510 START
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code().reset();                                                                                         //Natural: RESET #AIAN019-RETURN-CODE
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Processing_Date().setValue(pnd_Bus_Date_Pnd_Bus_Date_N);                                                       //Natural: ASSIGN #AIAN019-PROCESSING-DATE := #BUS-DATE-N
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().equals(getZero())))                                                                  //Natural: IF #AIAN019-ISSUE-DAY = 0
            {
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Day().setValue(1);                                                                                   //Natural: ASSIGN #AIAN019-ISSUE-DAY := 1
                //*  OS 061510 END
            }                                                                                                                                                             //Natural: END-IF
            //*  TPA FROM IPRO  06/10
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(54) || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(62)        //Natural: IF #AIAN019-ORIGIN = 54 OR = 62 OR = 74
                || pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin().equals(74)))
            {
                pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Tpa_Ipro_Issue_Date().setValue(iaa_Cntrct_Cntrct_Ssnng_Dte);                                               //Natural: ASSIGN #AIAN019-TPA-IPRO-ISSUE-DATE := CNTRCT-SSNNG-DTE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN019-PARMS
            sub_Display_Aian019_Parms();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.callnat(Aian019.class , getCurrentProcessState(), pdaAial0190.getPnd_Aian019_Linkage());                                                              //Natural: CALLNAT 'AIAN019' #AIAN019-LINKAGE
            if (condition(Global.isEscape())) return;
            //*  040612
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code_Nbr().greater(getZero())))                                                           //Natural: IF #AIAN019-RETURN-CODE-NBR GT 0
            {
                //*  040612
                getReports().write(0, NEWLINE,"ERROR IN AIAN019 - ERROR CD =>",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code_Nbr());                         //Natural: WRITE / 'ERROR IN AIAN019 - ERROR CD =>' #AIAN019-RETURN-CODE-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "CONTRACT =>",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Contract_Number(),pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payee_Code()); //Natural: WRITE 'CONTRACT =>' #AIAN019-CONTRACT-NUMBER #AIAN019-PAYEE-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl100.getPnd_Fund_Cd_From().setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                          //Natural: ASSIGN #FUND-CD-FROM := IATL010.XFR-FRM-ACCT-CDE ( #T )
                                                                                                                                                                          //Natural: PERFORM #FROM-SUB-CONVERT
            sub_Pnd_From_Sub_Convert();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            F7:                                                                                                                                                           //Natural: FOR #C = 1 TO 20
            for (ldaIatl100.getPnd_C().setValue(1); condition(ldaIatl100.getPnd_C().lessOrEqual(20)); ldaIatl100.getPnd_C().nadd(1))
            {
                if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(ldaIatl100.getPnd_C()).notEquals(" ")))                             //Natural: IF #AIAN019-CREF-FUND-OUT ( #C ) NE ' '
                {
                    ldaIatl100.getPnd_Fund_Cd_To().setValue(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(ldaIatl100.getPnd_C()));              //Natural: ASSIGN #FUND-CD-TO := #AIAN019-CREF-FUND-OUT ( #C )
                                                                                                                                                                          //Natural: PERFORM #TO-SUB-CONVERT
                    sub_Pnd_To_Sub_Convert();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F7"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F7"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *RITE '=' #C '=' #TO-SUB '=' #FROM-SUB '=' #CREF-PRORATE XFR-TO-QTY(#C)
                    //*        ADD #CREF-PRORATE
                    ldaIatl100.getPnd_To_From_Table().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #C ) TO #TO-FROM-TABLE ( #TO-SUB,#FROM-SUB )
                    //*  011609
                    //*  011609
                    ldaIatl100.getPnd_To_From_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                        ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                    pnd_Cnt_Eft.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CNT-EFT
                    //*        WRITE '=' #EFF-DATE '=' #BUS-DATE
                    if (condition(pnd_Eff_Date.less(pnd_Bus_Date)))                                                                                                       //Natural: IF #EFF-DATE < #BUS-DATE
                    {
                        ldaIatl100.getPnd_To_From_Table_Retro().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #C ) TO #TO-FROM-TABLE-RETRO ( #TO-SUB,#FROM-SUB )
                        //*  011609
                        //*  011609
                        ldaIatl100.getPnd_To_From_Table_Retro_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-TABLE-RETRO-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                            ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                        pnd_Cnt_Retro.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-RETRO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Eff_Date.greater(pnd_Bus_Date)))                                                                                                //Natural: IF #EFF-DATE > #BUS-DATE
                        {
                            ldaIatl100.getPnd_To_From_Table_Future().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(ldaIatl100.getPnd_C())); //Natural: ADD #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #C ) TO #TO-FROM-TABLE-FUTURE ( #TO-SUB,#FROM-SUB )
                            //*  011609
                            //*  011609
                            ldaIatl100.getPnd_To_From_Table_Future_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-TABLE-FUTURE-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                                ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                            pnd_Cnt_Future.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CNT-FUTURE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*       WRITE / '=' #TRANSFER-AMT-OUT-CREF(#C) '=' #TO-SUB '=' #FROM-SUB
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #RESET-AIAN019-PARA
            sub_Pnd_Reset_Aian019_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FZ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FZ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian019_Parms() throws Exception                                                                                                             //Natural: DISPLAY-AIAN019-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number(),"=",pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code(),NEWLINE,               //Natural: WRITE '=' #CONTRACT-NUMBER '=' #PAYEE-CODE / '=' #AIAN019-MODE / '=' #AIAN019-PAYMENT-METHOD / '=' #AIAN019-ORIGIN / '=' #AIAN019-OPTION / '=' #AIAN019-ISSUE-DATE / '=' #AIAN019-FINAL-PER-PAY-DATE / '=' #AIAN019-FIRST-ANN-DOB / '=' #AIAN019-FIRST-ANN-SEX / '=' #AIAN019-FIRST-ANN-DOD / '=' #AIAN019-SECOND-ANN-DOB / '=' #AIAN019-SECOND-ANN-SEX / '=' #AIAN019-SECOND-ANN-DOD / '=' #AIAN019-TRANSFER-EFFECTIVE-DATE / '=' #AIAN019-PROCESSING-DATE / '=' #AIAN019-TYPE-OF-RUN /
            "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Mode(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Payment_Method(),NEWLINE,
            "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Origin(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Option(),NEWLINE,"=",
            pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Issue_Date(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Final_Per_Pay_Date(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dob(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Sex(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_First_Ann_Dod(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dob(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Sex(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Second_Ann_Dod(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Effective_Date(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Processing_Date(),
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Type_Of_Run(),NEWLINE);
        if (Global.isEscape()) return;
        FOR30:                                                                                                                                                            //Natural: FOR #A 1 20
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(20)); pnd_A.nadd(1))
        {
            if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_A).equals(" ")))                                                     //Natural: IF #AIAN019-CREF-FUND-IN ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_In().getValue(pnd_A),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Reval_Method().getValue(pnd_A)); //Natural: WRITE '=' #AIAN019-CREF-FUND-IN ( #A ) / '=' #AIAN019-CREF-REVAL-METHOD ( #A )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR31:                                                                                                                                                        //Natural: FOR #B 1 #MAX-RATE
            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Rate)); pnd_B.nadd(1))
            {
                if (condition(pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_A,pnd_B).equals(" ")))                                              //Natural: IF #AIAN019-RATE-CODE ( #A,#B ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Rate_Code().getValue(pnd_A,pnd_B),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gtd_Pmt().getValue(pnd_A, //Natural: WRITE '=' #AIAN019-RATE-CODE ( #A,#B ) / '=' #AIAN019-GTD-PMT ( #A,#B ) / '=' #AIAN019-TRANSFER-DATE ( #A,#B )
                    pnd_B),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Transfer_Date().getValue(pnd_A,pnd_B));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Next_Pymnt_Dte());                                                                       //Natural: WRITE '=' #AIAN019-NEXT-PYMNT-DTE
        if (Global.isEscape()) return;
        FOR32:                                                                                                                                                            //Natural: FOR #A 1 20
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(20)); pnd_A.nadd(1))
        {
            getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Fund_Out().getValue(pnd_A),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Units().getValue(pnd_A), //Natural: WRITE '=' #AIAN019-CREF-FUND-OUT ( #A ) / '=' #AIAN019-CREF-UNITS ( #A ) / '=' #AIAN019-CREF-AUV ( #A ) / '=' #AIAN019-CREF-PMT ( #A ) / '=' #AIAN019-TOTAL-TRANSFER-AMT-OUT ( #A )
                NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Auv().getValue(pnd_A),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Cref_Pmt().getValue(pnd_A),
                NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Total_Transfer_Amt_Out().getValue(pnd_A));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Pct(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Rate(),       //Natural: WRITE '=' #AIAN019-GBPM-PCT / '=' #AIAN019-GBPM-RATE / '=' #AIAN019-GBPM-PRORATE-IND / '=' #AIAN019-RETURN-CODE
            NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Gbpm_Prorate_Ind(),NEWLINE,"=",pdaAial0190.getPnd_Aian019_Linkage_Pnd_Aian019_Return_Code());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(44),"PAYOUT TRANSFER INTERFUND COMPANY REPORT FOR",new       //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 44T 'PAYOUT TRANSFER INTERFUND COMPANY REPORT FOR' 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(46),"TRANSFER REQUESTS EFFECTIVE DATE",pnd_W_Date);                                         //Natural: WRITE ( 1 ) 46T 'TRANSFER REQUESTS EFFECTIVE DATE' #W-DATE
                    //*                                                     /* 011609 START
                    //* *ITE (1) / 10T '-'(22) '(IN THOUSANDS  $000 )' '-'(9) ' FROM  ' '-'(57)
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"------------- (IN THOUSANDS  $000 ) -------------");                                              //Natural: WRITE ( 1 ) / '------------- (IN THOUSANDS  $000 ) -------------'
                    getReports().write(1, ReportOption.NOTITLE,"FROM",new TabSetting(11),"TO",new TabSetting(25),"AMOUNT");                                               //Natural: WRITE ( 1 ) 'FROM' 11T 'TO' 25T 'AMOUNT'
                    //*  WRITE (1) 001T 'TO:'
                    //*    010T 'GRADED'
                    //*    020T 'STNDRD'
                    //*    033T 'STOCK'
                    //*    045T 'MMA'
                    //*    052T 'SOCIAL'
                    //*    064T 'BOND'
                    //*    072T 'GLOBAL'
                    //*    082T 'GROWTH'
                    //*    092T 'EQUITY'
                    //*    105T 'REA'
                    //*    112T '   ILB'
                    //*    125T 'TOTAL'                                      /* 011609 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(42),"PAYOUT TRANSFER INTERFUND COMPANY ERROR REPORT FOR",new //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 42T 'PAYOUT TRANSFER INTERFUND COMPANY ERROR REPORT FOR' 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(46),"TRANSFER REQUESTS ENTERED",pnd_W_Date);                                                //Natural: WRITE ( 2 ) 46T 'TRANSFER REQUESTS ENTERED' #W-DATE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) NOTITLE ' '
                    getReports().write(3, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(44),"PAYOUT TRANSFER INTERFUND COMPANY REPORT FOR",new       //Natural: WRITE ( 3 ) 'PROGRAM ' *PROGRAM 44T 'PAYOUT TRANSFER INTERFUND COMPANY REPORT FOR' 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(46),pnd_W_Variable,pnd_W_Date);                                                             //Natural: WRITE ( 3 ) 46T #W-VARIABLE #W-DATE
                    //*                                                     /* 011609 START
                    //* *ITE (3) / 10T '-'(22) '(IN THOUSANDS  $000 )' '-'(9) ' FROM  ' '-'(57)
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE,"------------- (IN THOUSANDS  $000 ) -------------");                                              //Natural: WRITE ( 3 ) / '------------- (IN THOUSANDS  $000 ) -------------'
                    getReports().write(3, ReportOption.NOTITLE,"FROM",new TabSetting(11),"TO",new TabSetting(25),"AMOUNT");                                               //Natural: WRITE ( 3 ) 'FROM' 11T 'TO' 25T 'AMOUNT'
                    //*  WRITE (3) 001T 'TO:'
                    //*    010T 'GRADED'
                    //*    020T 'STNDRD'
                    //*    033T 'STOCK'
                    //*    045T 'MMA'
                    //*    052T 'SOCIAL'
                    //*    064T 'BOND'
                    //*    072T 'GLOBAL'
                    //*    082T 'GROWTH'
                    //*    092T 'EQUITY'
                    //*    105T 'REA'
                    //*    112T '   ILB'
                    //*    125T 'TOTAL'                                      /* 011609 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
        Global.format(3, "LS=133 PS=56");
    }
}
