/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:10 PM
**        * FROM NATURAL PROGRAM : Iaap307b
************************************************************
**        * FILE NAME            : Iaap307b.java
**        * CLASS NAME           : Iaap307b
**        * INSTANCE NAME        : Iaap307b
************************************************************
************************************************************************
* PROGRAM:  IAAP307B - CLONED FROM IAAP307.
* FUNCTION: WRITE OUT A TAPE FILE AND A REPORT OF EVERY THOUSANDTH REC
*           WRITTEN ON TAPE. ALSO A REPORT OF ZERO SSN WITH JOINT AND
*           2/3 ACTIVE CONTRACTS.
* CREATED:  07/12/98 : BY ARI GROSSMAN - ORIGINAL IAAP307
* CONVERTED 09/18/14 : BY JUN TINIO - REMOVED COR/NAAD ACCESS.
*
* 09/18/2014 J TINIO BYPASS ADDRESS RETRIEVAL. THIS WILL BE DONE IN
*                    ANOTHER STEP USING THE NAAD DATA EXTRACTS REPLACING
*                    DIRECT ADABAS READ AS PART COR/NAAD SUNSET PROJECT.
*                    SCAN ON 080614 FOR CHANGES
* 04/2017    O SOTTO PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap307b extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_W1_Rec;

    private DbsGroup pnd_W1_Rec__R_Field_1;
    private DbsField pnd_W1_Rec_Pnd_W1_Ssn;

    private DbsGroup pnd_W1_Rec__R_Field_2;
    private DbsField pnd_W1_Rec_Pnd_W1_Ssn_A;
    private DbsField pnd_W1_Rec_Pnd_W1_Last_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_First_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Mddle_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Dob;

    private DbsGroup pnd_W1_Rec__R_Field_3;
    private DbsField pnd_W1_Rec_Pnd_Dob_Yyyy;
    private DbsField pnd_W1_Rec_Pnd_Dob_Mm;
    private DbsField pnd_W1_Rec_Pnd_Dob_Dd;
    private DbsField pnd_W1_Rec_Pnd_W1_Contract;
    private DbsField pnd_W1_Rec_Pnd_W1_Payee;
    private DbsField pnd_W1_Rec_Pnd_W1_Pend;
    private DbsField pnd_W1_Rec_Pnd_W1_Option_Code;

    private DbsGroup pnd_W1_Rec__R_Field_4;
    private DbsField pnd_W1_Rec_Pnd_W1_Option_Code_A;
    private DbsField pnd_W1_Rec_Pnd_W1_Pin;
    private DbsField pnd_W1_Rec_Pnd_W1_Orgn_Cde;
    private DbsField pnd_W1_Rec_Pnd_W1_Sex_Cde;
    private DbsField pnd_Write_Recs;
    private DbsField pnd_Write_Recs_2;
    private DbsField pnd_Write_Recs_3;
    private DbsField pnd_Read_Recs;
    private DbsField pnd_Tape_Writes;
    private DbsField pnd_Tape_Write3;
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Page_Ctr_2;
    private DbsField pnd_Page_Ctr_3;
    private DbsField pnd_Counter_1;
    private DbsField pnd_Counter_2;
    private DbsField pnd_Cnt_Zero_Ssn;
    private DbsField pnd_Cnt_Dup;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Hold_J23;
    private DbsField pnd_Print_Dob;

    private DbsGroup pnd_Print_Dob__R_Field_5;
    private DbsField pnd_Print_Dob_Pnd_P_Mm;
    private DbsField pnd_Print_Dob_Pnd_Filler_1;
    private DbsField pnd_Print_Dob_Pnd_P_Dd;
    private DbsField pnd_Print_Dob_Pnd_Filler_2;
    private DbsField pnd_Print_Dob_Pnd_P_Yyyy;
    private DbsField pnd_Excluded_Options;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_W1_Rec = localVariables.newFieldInRecord("pnd_W1_Rec", "#W1-REC", FieldType.STRING, 97);

        pnd_W1_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_W1_Rec__R_Field_1", "REDEFINE", pnd_W1_Rec);
        pnd_W1_Rec_Pnd_W1_Ssn = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Ssn", "#W1-SSN", FieldType.NUMERIC, 9);

        pnd_W1_Rec__R_Field_2 = pnd_W1_Rec__R_Field_1.newGroupInGroup("pnd_W1_Rec__R_Field_2", "REDEFINE", pnd_W1_Rec_Pnd_W1_Ssn);
        pnd_W1_Rec_Pnd_W1_Ssn_A = pnd_W1_Rec__R_Field_2.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Ssn_A", "#W1-SSN-A", FieldType.STRING, 9);
        pnd_W1_Rec_Pnd_W1_Last_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Last_Name", "#W1-LAST-NAME", FieldType.STRING, 20);
        pnd_W1_Rec_Pnd_W1_First_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_First_Name", "#W1-FIRST-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Mddle_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Mddle_Name", "#W1-MDDLE-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Dob = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Dob", "#W1-DOB", FieldType.STRING, 8);

        pnd_W1_Rec__R_Field_3 = pnd_W1_Rec__R_Field_1.newGroupInGroup("pnd_W1_Rec__R_Field_3", "REDEFINE", pnd_W1_Rec_Pnd_W1_Dob);
        pnd_W1_Rec_Pnd_Dob_Yyyy = pnd_W1_Rec__R_Field_3.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.STRING, 4);
        pnd_W1_Rec_Pnd_Dob_Mm = pnd_W1_Rec__R_Field_3.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_Dob_Dd = pnd_W1_Rec__R_Field_3.newFieldInGroup("pnd_W1_Rec_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_W1_Contract = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Contract", "#W1-CONTRACT", FieldType.STRING, 10);
        pnd_W1_Rec_Pnd_W1_Payee = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_W1_Rec_Pnd_W1_Pend = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pend", "#W1-PEND", FieldType.STRING, 1);
        pnd_W1_Rec_Pnd_W1_Option_Code = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Option_Code", "#W1-OPTION-CODE", FieldType.NUMERIC, 2);

        pnd_W1_Rec__R_Field_4 = pnd_W1_Rec__R_Field_1.newGroupInGroup("pnd_W1_Rec__R_Field_4", "REDEFINE", pnd_W1_Rec_Pnd_W1_Option_Code);
        pnd_W1_Rec_Pnd_W1_Option_Code_A = pnd_W1_Rec__R_Field_4.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Option_Code_A", "#W1-OPTION-CODE-A", FieldType.STRING, 
            2);
        pnd_W1_Rec_Pnd_W1_Pin = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pin", "#W1-PIN", FieldType.NUMERIC, 12);
        pnd_W1_Rec_Pnd_W1_Orgn_Cde = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_W1_Sex_Cde = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Sex_Cde", "#W1-SEX-CDE", FieldType.STRING, 1);
        pnd_Write_Recs = localVariables.newFieldInRecord("pnd_Write_Recs", "#WRITE-RECS", FieldType.NUMERIC, 8);
        pnd_Write_Recs_2 = localVariables.newFieldInRecord("pnd_Write_Recs_2", "#WRITE-RECS-2", FieldType.NUMERIC, 8);
        pnd_Write_Recs_3 = localVariables.newFieldInRecord("pnd_Write_Recs_3", "#WRITE-RECS-3", FieldType.NUMERIC, 8);
        pnd_Read_Recs = localVariables.newFieldInRecord("pnd_Read_Recs", "#READ-RECS", FieldType.NUMERIC, 8);
        pnd_Tape_Writes = localVariables.newFieldInRecord("pnd_Tape_Writes", "#TAPE-WRITES", FieldType.NUMERIC, 8);
        pnd_Tape_Write3 = localVariables.newFieldInRecord("pnd_Tape_Write3", "#TAPE-WRITE3", FieldType.NUMERIC, 8);
        pnd_Page_Ctr = localVariables.newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 8);
        pnd_Page_Ctr_2 = localVariables.newFieldInRecord("pnd_Page_Ctr_2", "#PAGE-CTR-2", FieldType.NUMERIC, 8);
        pnd_Page_Ctr_3 = localVariables.newFieldInRecord("pnd_Page_Ctr_3", "#PAGE-CTR-3", FieldType.NUMERIC, 8);
        pnd_Counter_1 = localVariables.newFieldInRecord("pnd_Counter_1", "#COUNTER-1", FieldType.NUMERIC, 8);
        pnd_Counter_2 = localVariables.newFieldInRecord("pnd_Counter_2", "#COUNTER-2", FieldType.NUMERIC, 8);
        pnd_Cnt_Zero_Ssn = localVariables.newFieldInRecord("pnd_Cnt_Zero_Ssn", "#CNT-ZERO-SSN", FieldType.NUMERIC, 8);
        pnd_Cnt_Dup = localVariables.newFieldInRecord("pnd_Cnt_Dup", "#CNT-DUP", FieldType.NUMERIC, 8);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.NUMERIC, 9);
        pnd_Hold_J23 = localVariables.newFieldInRecord("pnd_Hold_J23", "#HOLD-J23", FieldType.STRING, 1);
        pnd_Print_Dob = localVariables.newFieldInRecord("pnd_Print_Dob", "#PRINT-DOB", FieldType.STRING, 10);

        pnd_Print_Dob__R_Field_5 = localVariables.newGroupInRecord("pnd_Print_Dob__R_Field_5", "REDEFINE", pnd_Print_Dob);
        pnd_Print_Dob_Pnd_P_Mm = pnd_Print_Dob__R_Field_5.newFieldInGroup("pnd_Print_Dob_Pnd_P_Mm", "#P-MM", FieldType.STRING, 2);
        pnd_Print_Dob_Pnd_Filler_1 = pnd_Print_Dob__R_Field_5.newFieldInGroup("pnd_Print_Dob_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Print_Dob_Pnd_P_Dd = pnd_Print_Dob__R_Field_5.newFieldInGroup("pnd_Print_Dob_Pnd_P_Dd", "#P-DD", FieldType.STRING, 2);
        pnd_Print_Dob_Pnd_Filler_2 = pnd_Print_Dob__R_Field_5.newFieldInGroup("pnd_Print_Dob_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Print_Dob_Pnd_P_Yyyy = pnd_Print_Dob__R_Field_5.newFieldInGroup("pnd_Print_Dob_Pnd_P_Yyyy", "#P-YYYY", FieldType.STRING, 4);
        pnd_Excluded_Options = localVariables.newFieldArrayInRecord("pnd_Excluded_Options", "#EXCLUDED-OPTIONS", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            6));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Excluded_Options.getValue(1).setInitialValue(1);
        pnd_Excluded_Options.getValue(2).setInitialValue(5);
        pnd_Excluded_Options.getValue(3).setInitialValue(6);
        pnd_Excluded_Options.getValue(4).setInitialValue(9);
        pnd_Excluded_Options.getValue(5).setInitialValue(21);
        pnd_Excluded_Options.getValue(6).setInitialValue(2);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap307b() throws Exception
    {
        super("Iaap307b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  051410                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 3 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 )
        R2:                                                                                                                                                               //Natural: READ WORK FILE 1 #W1-REC
        while (condition(getWorkFiles().read(1, pnd_W1_Rec)))
        {
            pnd_Read_Recs.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-RECS
            if (condition(pnd_Read_Recs.equals(1)))                                                                                                                       //Natural: IF #READ-RECS = 1
            {
                pnd_Hold_Ssn.setValue(pnd_W1_Rec_Pnd_W1_Ssn);                                                                                                             //Natural: MOVE #W1-SSN TO #HOLD-SSN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W1_Rec_Pnd_W1_Ssn.equals(getZero())))                                                                                                       //Natural: IF #W1-SSN = 0
            {
                if (condition(pnd_W1_Rec_Pnd_W1_Payee.lessOrEqual(2) && pnd_W1_Rec_Pnd_W1_Pend.equals("0") && ! (pnd_W1_Rec_Pnd_W1_Option_Code.equals(pnd_Excluded_Options.getValue("*"))))) //Natural: IF #W1-PAYEE LE 02 AND #W1-PEND = '0' AND NOT #W1-OPTION-CODE = #EXCLUDED-OPTIONS ( * )
                {
                    pnd_Cnt_Zero_Ssn.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNT-ZERO-SSN
                                                                                                                                                                          //Natural: PERFORM #CONVERT-DATE
                    sub_Pnd_Convert_Date();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(3, false, "    ", pnd_W1_Rec_Pnd_W1_Ssn_A, pnd_W1_Rec_Pnd_W1_Contract, pnd_W1_Rec_Pnd_W1_Payee);                                 //Natural: WRITE WORK FILE 3 '    ' #W1-SSN-A #W1-CONTRACT #W1-PAYEE
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),pnd_W1_Rec_Pnd_W1_Ssn_A,new TabSetting(19),pnd_W1_Rec_Pnd_W1_Last_Name,new               //Natural: WRITE ( 3 ) 001T #W1-SSN-A 019T #W1-LAST-NAME 041T #W1-FIRST-NAME 064T #PRINT-DOB 085T #W1-CONTRACT 106T #W1-PAYEE 117T #W1-OPTION-CODE
                        TabSetting(41),pnd_W1_Rec_Pnd_W1_First_Name,new TabSetting(64),pnd_Print_Dob,new TabSetting(85),pnd_W1_Rec_Pnd_W1_Contract,new TabSetting(106),pnd_W1_Rec_Pnd_W1_Payee,new 
                        TabSetting(117),pnd_W1_Rec_Pnd_W1_Option_Code);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Write_Recs_3.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WRITE-RECS-3
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((pnd_W1_Rec_Pnd_W1_Payee.lessOrEqual(2) && pnd_W1_Rec_Pnd_W1_Pend.equals("0")) && (((pnd_W1_Rec_Pnd_W1_Option_Code.equals(7)               //Natural: IF #W1-PAYEE LE 02 AND #W1-PEND = '0' AND ( #W1-OPTION-CODE = 07 OR = 08 OR = 10 OR = 13 )
                    || pnd_W1_Rec_Pnd_W1_Option_Code.equals(8)) || pnd_W1_Rec_Pnd_W1_Option_Code.equals(10)) || pnd_W1_Rec_Pnd_W1_Option_Code.equals(13)))))
                {
                    getWorkFiles().write(3, false, "2/3 ", pnd_W1_Rec_Pnd_W1_Ssn_A, pnd_W1_Rec_Pnd_W1_Contract, pnd_W1_Rec_Pnd_W1_Payee);                                 //Natural: WRITE WORK FILE 3 '2/3 ' #W1-SSN-A #W1-CONTRACT #W1-PAYEE
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_W1_Rec_Pnd_W1_Ssn_A,new TabSetting(19),pnd_W1_Rec_Pnd_W1_Last_Name,new               //Natural: WRITE ( 2 ) 001T #W1-SSN-A 019T #W1-LAST-NAME 041T #W1-FIRST-NAME 064T #PRINT-DOB 085T #W1-CONTRACT 106T #W1-PAYEE 117T #W1-OPTION-CODE
                        TabSetting(41),pnd_W1_Rec_Pnd_W1_First_Name,new TabSetting(64),pnd_Print_Dob,new TabSetting(85),pnd_W1_Rec_Pnd_W1_Contract,new TabSetting(106),pnd_W1_Rec_Pnd_W1_Payee,new 
                        TabSetting(117),pnd_W1_Rec_Pnd_W1_Option_Code);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Write_Recs_2.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WRITE-RECS-2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W1_Rec_Pnd_W1_Ssn.equals(pnd_Hold_Ssn) && pnd_Read_Recs.notEquals(1)))                                                                  //Natural: IF #W1-SSN = #HOLD-SSN AND #READ-RECS NOT = 1
                {
                    pnd_Cnt_Dup.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CNT-DUP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Hold_Ssn.setValue(pnd_W1_Rec_Pnd_W1_Ssn);                                                                                                         //Natural: MOVE #W1-SSN TO #HOLD-SSN
                    //*  080614
                    getWorkFiles().write(2, false, pnd_W1_Rec);                                                                                                           //Natural: WRITE WORK FILE 2 #W1-REC
                    pnd_Counter_1.nadd(1);                                                                                                                                //Natural: ADD 1 TO #COUNTER-1
                    if (condition(pnd_Counter_1.equals(1000)))                                                                                                            //Natural: IF #COUNTER-1 = 1000
                    {
                        pnd_Counter_1.setValue(0);                                                                                                                        //Natural: ASSIGN #COUNTER-1 := 0
                                                                                                                                                                          //Natural: PERFORM #CONVERT-DATE
                        sub_Pnd_Convert_Date();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_W1_Rec_Pnd_W1_Ssn_A,new TabSetting(19),pnd_W1_Rec_Pnd_W1_Last_Name,new           //Natural: WRITE ( 1 ) 001T #W1-SSN-A 019T #W1-LAST-NAME 041T #W1-FIRST-NAME 064T #PRINT-DOB 085T #W1-CONTRACT 106T #W1-PAYEE 117T #W1-OPTION-CODE
                            TabSetting(41),pnd_W1_Rec_Pnd_W1_First_Name,new TabSetting(64),pnd_Print_Dob,new TabSetting(85),pnd_W1_Rec_Pnd_W1_Contract,new 
                            TabSetting(106),pnd_W1_Rec_Pnd_W1_Payee,new TabSetting(117),pnd_W1_Rec_Pnd_W1_Option_Code);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Write_Recs.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WRITE-RECS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        R2_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "IA READS ================================> ",pnd_Read_Recs);                                                                               //Natural: WRITE 'IA READS ================================> ' #READ-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "ZERO SOCIAL SECURITY NUMBERS ============> ",pnd_Cnt_Zero_Ssn);                                                                            //Natural: WRITE 'ZERO SOCIAL SECURITY NUMBERS ============> ' #CNT-ZERO-SSN
        if (Global.isEscape()) return;
        getReports().write(0, "DUPLICATE SOCIAL SECURITY #S ============> ",pnd_Cnt_Dup);                                                                                 //Natural: WRITE 'DUPLICATE SOCIAL SECURITY #S ============> ' #CNT-DUP
        if (Global.isEscape()) return;
        getReports().write(0, "EXTRACT WRITES ==========================> ",pnd_Write_Recs);                                                                              //Natural: WRITE 'EXTRACT WRITES ==========================> ' #WRITE-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "J2/3 ACTIVE CNTS WITH NO SSN AND PEND 0 => ",pnd_Write_Recs_2);                                                                            //Natural: WRITE 'J2/3 ACTIVE CNTS WITH NO SSN AND PEND 0 => ' #WRITE-RECS-2
        if (Global.isEscape()) return;
        getReports().write(0, "ACTIVE CNTS WITH NO SSN AND PEND 0 ======> ",pnd_Write_Recs_3);                                                                            //Natural: WRITE 'ACTIVE CNTS WITH NO SSN AND PEND 0 ======> ' #WRITE-RECS-3
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS WRITTEN TO TAPE =================> ",pnd_Tape_Writes);                                                                             //Natural: WRITE 'RECORDS WRITTEN TO TAPE =================> ' #TAPE-WRITES
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CONVERT-DATE
    }
    private void sub_Pnd_Convert_Date() throws Exception                                                                                                                  //Natural: #CONVERT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_W1_Rec_Pnd_W1_Dob.notEquals(" ")))                                                                                                              //Natural: IF #W1-DOB NOT = ' '
        {
            pnd_Print_Dob_Pnd_P_Yyyy.setValue(pnd_W1_Rec_Pnd_Dob_Yyyy);                                                                                                   //Natural: MOVE #DOB-YYYY TO #P-YYYY
            pnd_Print_Dob_Pnd_Filler_1.setValue("/");                                                                                                                     //Natural: MOVE '/' TO #FILLER-1
            pnd_Print_Dob_Pnd_Filler_2.setValue("/");                                                                                                                     //Natural: MOVE '/' TO #FILLER-2
            pnd_Print_Dob_Pnd_P_Mm.setValue(pnd_W1_Rec_Pnd_Dob_Mm);                                                                                                       //Natural: MOVE #DOB-MM TO #P-MM
            pnd_Print_Dob_Pnd_P_Dd.setValue(pnd_W1_Rec_Pnd_Dob_Dd);                                                                                                       //Natural: MOVE #DOB-DD TO #P-DD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Print_Dob.setValue(" ");                                                                                                                                  //Natural: MOVE ' ' TO #PRINT-DOB
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    pnd_Page_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(51),"BERWYN GROUP, INC.  -  EXTRACT REPORT  ",new            //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 51T 'BERWYN GROUP, INC.  -  EXTRACT REPORT  ' 118T 'PAGE' #PAGE-CTR
                        TabSetting(118),"PAGE",pnd_Page_Ctr);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(4),"SSN",new TabSetting(11),"        LAST NAME             FIRST NAME   ",new               //Natural: WRITE ( 1 ) 004T 'SSN' 011T '        LAST NAME             FIRST NAME   ' 062T ' DATE OF BIRTH ' 083T 'CONTRACT NUMBER' 106T 'PAYEE' 116T 'OPTION'
                        TabSetting(62)," DATE OF BIRTH ",new TabSetting(83),"CONTRACT NUMBER",new TabSetting(106),"PAYEE",new TabSetting(116),"OPTION");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(106),"CODE ",new TabSetting(116)," CODE ");                                                 //Natural: WRITE ( 1 ) 106T 'CODE ' 116T ' CODE '
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    pnd_Page_Ctr_2.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-CTR-2
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"JOINT 2/3 ACTIVE CONTRACTS WITH NO SOCIAL SECURITY NUMBER",new  //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 46T 'JOINT 2/3 ACTIVE CONTRACTS WITH NO SOCIAL SECURITY NUMBER' 118T 'PAGE' #PAGE-CTR-2
                        TabSetting(118),"PAGE",pnd_Page_Ctr_2);
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(4),"SSN",new TabSetting(11),"        LAST NAME             FIRST NAME   ",new               //Natural: WRITE ( 2 ) 004T 'SSN' 011T '        LAST NAME             FIRST NAME   ' 062T ' DATE OF BIRTH ' 083T 'CONTRACT NUMBER' 106T 'PAYEE' 116T 'OPTION'
                        TabSetting(62)," DATE OF BIRTH ",new TabSetting(83),"CONTRACT NUMBER",new TabSetting(106),"PAYEE",new TabSetting(116),"OPTION");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(106),"CODE ",new TabSetting(116)," CODE ");                                                 //Natural: WRITE ( 2 ) 106T 'CODE ' 116T ' CODE '
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    //*  051410 START
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) NOTITLE ' '
                    pnd_Page_Ctr_3.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-CTR-3
                    getReports().write(3, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"    ACTIVE CONTRACTS WITH NO SOCIAL SECURITY NUMBER      ",new  //Natural: WRITE ( 3 ) 'PROGRAM ' *PROGRAM 46T '    ACTIVE CONTRACTS WITH NO SOCIAL SECURITY NUMBER      ' 118T 'PAGE' #PAGE-CTR-3
                        TabSetting(118),"PAGE",pnd_Page_Ctr_3);
                    getReports().skip(3, 1);                                                                                                                              //Natural: SKIP ( 3 ) 1
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(4),"SSN",new TabSetting(11),"        LAST NAME             FIRST NAME   ",new               //Natural: WRITE ( 3 ) 004T 'SSN' 011T '        LAST NAME             FIRST NAME   ' 062T ' DATE OF BIRTH ' 083T 'CONTRACT NUMBER' 106T 'PAYEE' 116T 'OPTION'
                        TabSetting(62)," DATE OF BIRTH ",new TabSetting(83),"CONTRACT NUMBER",new TabSetting(106),"PAYEE",new TabSetting(116),"OPTION");
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(106),"CODE ",new TabSetting(116)," CODE ");                                                 //Natural: WRITE ( 3 ) 106T 'CODE ' 116T ' CODE '
                    getReports().skip(3, 1);                                                                                                                              //Natural: SKIP ( 3 ) 1
                    //*  051410 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
        Global.format(3, "LS=133 PS=56");
    }
}
