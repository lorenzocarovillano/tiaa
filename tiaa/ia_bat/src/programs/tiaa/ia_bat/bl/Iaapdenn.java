/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:57 PM
**        * FROM NATURAL PROGRAM : Iaapdenn
************************************************************
**        * FILE NAME            : Iaapdenn.java
**        * CLASS NAME           : Iaapdenn
**        * INSTANCE NAME        : Iaapdenn
************************************************************
**********************************************************************
* PROGRAM:  IAAPMNAD                                                 *
* DATE   :  9/18/2014                                                *
* PURPOSE:  CREATE DAILY FILE EXTRACT OF IA CONTRACTS TO BE SENT TO  *
*           MDM FOR NO NAME AND ADDRESS REPORT                       *
*                                                                    *
* 07/2017  RC - PIN EXPANSION                                        *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapdenn extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Opt_A;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Issue_Yyyy;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Iss_St;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Iss;
    private DbsField pnd_Input_Pnd_F3a;
    private DbsField pnd_Input_Pnd_First_Ann_Dob_Dte;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Dob_Yyyymm;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_1st_Dob_8;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob_Dte;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_5;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_F;
    private DbsField pnd_Input_Pnd_F8;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_17;
    private DbsField pnd_Input_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_19;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_20;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Res;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_21;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Pnd_State_Cde;
    private DbsField pnd_Input_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Pnd_Local_Cde;
    private DbsField pnd_Input_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Core;

    private DbsGroup pnd_Core__R_Field_22;
    private DbsField pnd_Core_Cntrct_Payee;

    private DbsGroup pnd_Core__R_Field_23;
    private DbsField pnd_Core_Cntrct_Nbr;
    private DbsField pnd_Core_Cntrct_Payee_Cde;
    private DbsField pnd_Core_Ph_Unique_Id_Nbr;
    private DbsField pnd_Core_Ph_Rcd_Type_Cde;
    private DbsField pnd_Core_Ph_Social_Security_No;
    private DbsField pnd_Core_Ph_Dob_Dte;
    private DbsField pnd_Core_Ph_Dod_Dte;

    private DbsGroup pnd_Core_Ph_Nme;
    private DbsField pnd_Core_Ph_Last_Nme;

    private DbsGroup pnd_Core__R_Field_24;
    private DbsField pnd_Core_Ph_Last_Nme_16;
    private DbsField pnd_Core_Ph_First_Nme;

    private DbsGroup pnd_Core__R_Field_25;
    private DbsField pnd_Core_Ph_First_Nme_10;
    private DbsField pnd_Core_Ph_Mddle_Nme;

    private DbsGroup pnd_Core__R_Field_26;
    private DbsField pnd_Core_Ph_Mddle_Nme_1;
    private DbsField pnd_Core_Pnd_Cor_Status_Cde;
    private DbsField pnd_Core_Pnd_Cor_Sex_Cde;

    private DbsGroup pnd_Check_Address;
    private DbsField pnd_Check_Address_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Check_Address_Cntrct_Payee;

    private DbsGroup pnd_Check_Address__R_Field_27;
    private DbsField pnd_Check_Address_Cntrct_Nbr;
    private DbsField pnd_Check_Address_Cntrct_Payee_Cde;
    private DbsField pnd_Check_Address_Pnd_Cntrct_Name_Add_K;
    private DbsField pnd_Check_Address_Addrss_Postal_Data_K;
    private DbsField pnd_Check_Address_Pnd_Rest_Of_Record_K;
    private DbsField pnd_Check_Address_Pnd_Cntrct_Name_Add_R;
    private DbsField pnd_Check_Address_Addrss_Postal_Data_R;
    private DbsField pnd_Check_Address_Pnd_Rest_Of_Record_R;

    private DbsGroup pnd_Mail_Address;
    private DbsField pnd_Mail_Address_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Mail_Address_Cntrct_Py;

    private DbsGroup pnd_Mail_Address__R_Field_28;
    private DbsField pnd_Mail_Address_Cntrct_Nbr;
    private DbsField pnd_Mail_Address_Cntrct_Payee_Cde;
    private DbsField pnd_Mail_Address_Pnd_Cntrct_Name_Add;
    private DbsField pnd_Mail_Address_Addrss_Postal_Data;
    private DbsField pnd_Mail_Address_Pnd_Rest_Of_Record;
    private DbsField pnd_Name;
    private DbsField pnd_Addr_Found;
    private DbsField pnd_Cor_Eof;
    private DbsField pnd_Chck_Addr_Eof;
    private DbsField pnd_Ml_Addr_Eof;
    private DbsField pnd_1st_Dod;

    private DbsGroup pnd_1st_Dod__R_Field_29;
    private DbsField pnd_1st_Dod_Pnd_1st_Dod_Yyyy;
    private DbsField pnd_2nd_Dod;

    private DbsGroup pnd_2nd_Dod__R_Field_30;
    private DbsField pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy;
    private DbsField pnd_H_Opt;
    private DbsField pnd_H_Issue_Date;

    private DbsGroup pnd_H_Issue_Date__R_Field_31;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Dte;

    private DbsGroup pnd_H_Issue_Date__R_Field_32;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Yyyy;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Mm;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Dd;
    private DbsField pnd_Use_Payee_01;
    private DbsField pnd_Chck_Payee;

    private DbsGroup pnd_Chck_Payee__R_Field_33;
    private DbsField pnd_Chck_Payee_Pnd_Chck_Cntrct;
    private DbsField pnd_Chck_Payee_Pnd_Chck_Py;
    private DbsField pnd_Corr_Payee;

    private DbsGroup pnd_Corr_Payee__R_Field_34;
    private DbsField pnd_Corr_Payee_Pnd_Corr_Cntrct;
    private DbsField pnd_Corr_Payee_Pnd_Corr_Py;
    private DbsField pnd_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Optn_Cde);
        pnd_Input_Pnd_Opt_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Opt_A", "#OPT-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Yyyy = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Issue_Yyyy", "#ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Iss_St = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Iss_St);
        pnd_Input__Filler1 = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_Iss = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Iss", "#ISS", FieldType.STRING, 2);
        pnd_Input_Pnd_F3a = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3a", "#F3A", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob_Dte", "#FIRST-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_Dob_Yyyymm = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Dob_Yyyymm", "#DOB-YYYYMM", FieldType.STRING, 6);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_1st_Dob_8 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_1st_Dob_8", "#1ST-DOB-8", FieldType.STRING, 8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob_Dte", "#SCND-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde_5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_5", "#PPG-CDE-5", FieldType.STRING, 5);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Ppg_Cde_5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Ppg_Cde_F = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_F", "#PPG-CDE-F", FieldType.STRING, 1);
        pnd_Input_Pnd_F8 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 8);

        pnd_Input__R_Field_13 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_14 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_16 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);

        pnd_Input__R_Field_17 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_17", "REDEFINE", pnd_Input_Pnd_Summ_Fund_Cde);
        pnd_Input_Fund_Cde = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_18 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_18", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_19 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_19", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_20 = pnd_Input__R_Field_19.newGroupInGroup("pnd_Input__R_Field_20", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler2 = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_Res = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input_Pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Trmnte_Rsn = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_21 = pnd_Input__R_Field_19.newGroupInGroup("pnd_Input__R_Field_21", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_Spirt_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Input_Pnd_Spirt_Srce = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Arr_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Input_Pnd_Spirt_Prcss_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Fed_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Pnd_State_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_State_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Local_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Local_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Lst_Chnge_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cpr_Xfr_Term_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cpr_Lgl_Res_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Core = localVariables.newFieldInRecord("pnd_Core", "#CORE", FieldType.STRING, 150);

        pnd_Core__R_Field_22 = localVariables.newGroupInRecord("pnd_Core__R_Field_22", "REDEFINE", pnd_Core);
        pnd_Core_Cntrct_Payee = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Core__R_Field_23 = pnd_Core__R_Field_22.newGroupInGroup("pnd_Core__R_Field_23", "REDEFINE", pnd_Core_Cntrct_Payee);
        pnd_Core_Cntrct_Nbr = pnd_Core__R_Field_23.newFieldInGroup("pnd_Core_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Core_Cntrct_Payee_Cde = pnd_Core__R_Field_23.newFieldInGroup("pnd_Core_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Ph_Unique_Id_Nbr = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Core_Ph_Rcd_Type_Cde = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Ph_Rcd_Type_Cde", "PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Ph_Social_Security_No = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Ph_Social_Security_No", "PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 
            9);
        pnd_Core_Ph_Dob_Dte = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Ph_Dob_Dte", "PH-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Core_Ph_Dod_Dte = pnd_Core__R_Field_22.newFieldInGroup("pnd_Core_Ph_Dod_Dte", "PH-DOD-DTE", FieldType.NUMERIC, 8);

        pnd_Core_Ph_Nme = pnd_Core__R_Field_22.newGroupInGroup("pnd_Core_Ph_Nme", "PH-NME");
        pnd_Core_Ph_Last_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_24 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_24", "REDEFINE", pnd_Core_Ph_Last_Nme);
        pnd_Core_Ph_Last_Nme_16 = pnd_Core__R_Field_24.newFieldInGroup("pnd_Core_Ph_Last_Nme_16", "PH-LAST-NME-16", FieldType.STRING, 16);
        pnd_Core_Ph_First_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_25 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_25", "REDEFINE", pnd_Core_Ph_First_Nme);
        pnd_Core_Ph_First_Nme_10 = pnd_Core__R_Field_25.newFieldInGroup("pnd_Core_Ph_First_Nme_10", "PH-FIRST-NME-10", FieldType.STRING, 10);
        pnd_Core_Ph_Mddle_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Mddle_Nme", "PH-MDDLE-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_26 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_26", "REDEFINE", pnd_Core_Ph_Mddle_Nme);
        pnd_Core_Ph_Mddle_Nme_1 = pnd_Core__R_Field_26.newFieldInGroup("pnd_Core_Ph_Mddle_Nme_1", "PH-MDDLE-NME-1", FieldType.STRING, 1);
        pnd_Core_Pnd_Cor_Status_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 1);
        pnd_Core_Pnd_Cor_Sex_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);

        pnd_Check_Address = localVariables.newGroupInRecord("pnd_Check_Address", "#CHECK-ADDRESS");
        pnd_Check_Address_Ph_Unque_Id_Nmbr = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 
            12);
        pnd_Check_Address_Cntrct_Payee = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Check_Address__R_Field_27 = pnd_Check_Address.newGroupInGroup("pnd_Check_Address__R_Field_27", "REDEFINE", pnd_Check_Address_Cntrct_Payee);
        pnd_Check_Address_Cntrct_Nbr = pnd_Check_Address__R_Field_27.newFieldInGroup("pnd_Check_Address_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Check_Address_Cntrct_Payee_Cde = pnd_Check_Address__R_Field_27.newFieldInGroup("pnd_Check_Address_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Check_Address_Pnd_Cntrct_Name_Add_K = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);
        pnd_Check_Address_Addrss_Postal_Data_K = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);
        pnd_Check_Address_Pnd_Rest_Of_Record_K = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 
            148);
        pnd_Check_Address_Pnd_Cntrct_Name_Add_R = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);
        pnd_Check_Address_Addrss_Postal_Data_R = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);
        pnd_Check_Address_Pnd_Rest_Of_Record_R = pnd_Check_Address.newFieldInGroup("pnd_Check_Address_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 
            76);

        pnd_Mail_Address = localVariables.newGroupInRecord("pnd_Mail_Address", "#MAIL-ADDRESS");
        pnd_Mail_Address_Ph_Unque_Id_Nmbr = pnd_Mail_Address.newFieldInGroup("pnd_Mail_Address_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 
            12);
        pnd_Mail_Address_Cntrct_Py = pnd_Mail_Address.newFieldInGroup("pnd_Mail_Address_Cntrct_Py", "CNTRCT-PY", FieldType.STRING, 12);

        pnd_Mail_Address__R_Field_28 = pnd_Mail_Address.newGroupInGroup("pnd_Mail_Address__R_Field_28", "REDEFINE", pnd_Mail_Address_Cntrct_Py);
        pnd_Mail_Address_Cntrct_Nbr = pnd_Mail_Address__R_Field_28.newFieldInGroup("pnd_Mail_Address_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Mail_Address_Cntrct_Payee_Cde = pnd_Mail_Address__R_Field_28.newFieldInGroup("pnd_Mail_Address_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Mail_Address_Pnd_Cntrct_Name_Add = pnd_Mail_Address.newFieldInGroup("pnd_Mail_Address_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);
        pnd_Mail_Address_Addrss_Postal_Data = pnd_Mail_Address.newFieldInGroup("pnd_Mail_Address_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);
        pnd_Mail_Address_Pnd_Rest_Of_Record = pnd_Mail_Address.newFieldInGroup("pnd_Mail_Address_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            76);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 60);
        pnd_Addr_Found = localVariables.newFieldInRecord("pnd_Addr_Found", "#ADDR-FOUND", FieldType.BOOLEAN, 1);
        pnd_Cor_Eof = localVariables.newFieldInRecord("pnd_Cor_Eof", "#COR-EOF", FieldType.BOOLEAN, 1);
        pnd_Chck_Addr_Eof = localVariables.newFieldInRecord("pnd_Chck_Addr_Eof", "#CHCK-ADDR-EOF", FieldType.BOOLEAN, 1);
        pnd_Ml_Addr_Eof = localVariables.newFieldInRecord("pnd_Ml_Addr_Eof", "#ML-ADDR-EOF", FieldType.BOOLEAN, 1);
        pnd_1st_Dod = localVariables.newFieldInRecord("pnd_1st_Dod", "#1ST-DOD", FieldType.NUMERIC, 6);

        pnd_1st_Dod__R_Field_29 = localVariables.newGroupInRecord("pnd_1st_Dod__R_Field_29", "REDEFINE", pnd_1st_Dod);
        pnd_1st_Dod_Pnd_1st_Dod_Yyyy = pnd_1st_Dod__R_Field_29.newFieldInGroup("pnd_1st_Dod_Pnd_1st_Dod_Yyyy", "#1ST-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_2nd_Dod = localVariables.newFieldInRecord("pnd_2nd_Dod", "#2ND-DOD", FieldType.NUMERIC, 6);

        pnd_2nd_Dod__R_Field_30 = localVariables.newGroupInRecord("pnd_2nd_Dod__R_Field_30", "REDEFINE", pnd_2nd_Dod);
        pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy = pnd_2nd_Dod__R_Field_30.newFieldInGroup("pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy", "#2ND-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_H_Opt = localVariables.newFieldInRecord("pnd_H_Opt", "#H-OPT", FieldType.NUMERIC, 2);
        pnd_H_Issue_Date = localVariables.newFieldInRecord("pnd_H_Issue_Date", "#H-ISSUE-DATE", FieldType.STRING, 8);

        pnd_H_Issue_Date__R_Field_31 = localVariables.newGroupInRecord("pnd_H_Issue_Date__R_Field_31", "REDEFINE", pnd_H_Issue_Date);
        pnd_H_Issue_Date_Pnd_H_Iss_Dte = pnd_H_Issue_Date__R_Field_31.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Dte", "#H-ISS-DTE", FieldType.STRING, 
            6);

        pnd_H_Issue_Date__R_Field_32 = pnd_H_Issue_Date__R_Field_31.newGroupInGroup("pnd_H_Issue_Date__R_Field_32", "REDEFINE", pnd_H_Issue_Date_Pnd_H_Iss_Dte);
        pnd_H_Issue_Date_Pnd_H_Iss_Yyyy = pnd_H_Issue_Date__R_Field_32.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Yyyy", "#H-ISS-YYYY", FieldType.NUMERIC, 
            4);
        pnd_H_Issue_Date_Pnd_H_Iss_Mm = pnd_H_Issue_Date__R_Field_32.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Mm", "#H-ISS-MM", FieldType.NUMERIC, 
            2);
        pnd_H_Issue_Date_Pnd_H_Iss_Dd = pnd_H_Issue_Date__R_Field_31.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Dd", "#H-ISS-DD", FieldType.NUMERIC, 
            2);
        pnd_Use_Payee_01 = localVariables.newFieldInRecord("pnd_Use_Payee_01", "#USE-PAYEE-01", FieldType.BOOLEAN, 1);
        pnd_Chck_Payee = localVariables.newFieldInRecord("pnd_Chck_Payee", "#CHCK-PAYEE", FieldType.STRING, 12);

        pnd_Chck_Payee__R_Field_33 = localVariables.newGroupInRecord("pnd_Chck_Payee__R_Field_33", "REDEFINE", pnd_Chck_Payee);
        pnd_Chck_Payee_Pnd_Chck_Cntrct = pnd_Chck_Payee__R_Field_33.newFieldInGroup("pnd_Chck_Payee_Pnd_Chck_Cntrct", "#CHCK-CNTRCT", FieldType.STRING, 
            10);
        pnd_Chck_Payee_Pnd_Chck_Py = pnd_Chck_Payee__R_Field_33.newFieldInGroup("pnd_Chck_Payee_Pnd_Chck_Py", "#CHCK-PY", FieldType.NUMERIC, 2);
        pnd_Corr_Payee = localVariables.newFieldInRecord("pnd_Corr_Payee", "#CORR-PAYEE", FieldType.STRING, 12);

        pnd_Corr_Payee__R_Field_34 = localVariables.newGroupInRecord("pnd_Corr_Payee__R_Field_34", "REDEFINE", pnd_Corr_Payee);
        pnd_Corr_Payee_Pnd_Corr_Cntrct = pnd_Corr_Payee__R_Field_34.newFieldInGroup("pnd_Corr_Payee_Pnd_Corr_Cntrct", "#CORR-CNTRCT", FieldType.STRING, 
            10);
        pnd_Corr_Payee_Pnd_Corr_Py = pnd_Corr_Payee__R_Field_34.newFieldInGroup("pnd_Corr_Payee_Pnd_Corr_Py", "#CORR-PY", FieldType.NUMERIC, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapdenn() throws Exception
    {
        super("Iaapdenn");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 180 PS = 0
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20))))                                                               //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_Use_Payee_01.reset();                                                                                                                                 //Natural: RESET #USE-PAYEE-01
                pnd_H_Opt.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                               //Natural: ASSIGN #H-OPT := #OPTN-CDE
                pnd_H_Issue_Date_Pnd_H_Iss_Dte.setValue(pnd_Input_Pnd_Issue_Dte_A);                                                                                       //Natural: ASSIGN #H-ISS-DTE := #ISSUE-DTE-A
                pnd_H_Issue_Date_Pnd_H_Iss_Dd.setValue(pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd);                                                                             //Natural: ASSIGN #H-ISS-DD := #W1-CNTRCT-ISSUE-DTE-DD
                if (condition(pnd_H_Issue_Date_Pnd_H_Iss_Dd.equals(getZero())))                                                                                           //Natural: IF #H-ISS-DD = 0
                {
                    pnd_H_Issue_Date_Pnd_H_Iss_Dd.setValue(1);                                                                                                            //Natural: ASSIGN #H-ISS-DD := 01
                }                                                                                                                                                         //Natural: END-IF
                pnd_1st_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                        //Natural: ASSIGN #1ST-DOD := #FIRST-ANN-DOD
                pnd_2nd_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                         //Natural: ASSIGN #2ND-DOD := #SCND-ANN-DOD
                if (condition(pnd_1st_Dod.equals(getZero()) && pnd_2nd_Dod.greater(getZero())))                                                                           //Natural: IF #1ST-DOD = 0 AND #2ND-DOD GT 0
                {
                    pnd_Use_Payee_01.setValue(true);                                                                                                                      //Natural: ASSIGN #USE-PAYEE-01 := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                if (condition(pnd_Input_Pnd_Status_Cde.notEquals(1) || pnd_Input_Pnd_Pend_Cde.notEquals("0")))                                                            //Natural: IF #STATUS-CDE NE 1 OR #PEND-CDE NE '0'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-NAME-FROM-COR
                sub_Get_Name_From_Cor();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT-ADDRESS
                sub_Get_Payment_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Addr_Found.equals(true)))                                                                                                               //Natural: IF #ADDR-FOUND = TRUE
                {
                                                                                                                                                                          //Natural: PERFORM GET-MAIL-ADDRESS
                    sub_Get_Mail_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Addr_Found.equals(false)))                                                                                                              //Natural: IF #ADDR-FOUND = FALSE
                {
                    getReports().display(1, "PIN",                                                                                                                        //Natural: DISPLAY ( 1 ) 'PIN' #CPR-ID-NBR 'CONTRACT' #PPCN-NBR 'PAYEE' #PAYEE-CDE-A 'NAME' #NAME 'ISSUE DATE' #H-ISSUE-DATE 'OPTION' #H-OPT ( EM = 99 ) 'MODE' #MODE-IND 'PEND' #PEND-CDE
                    		pnd_Input_Pnd_Cpr_Id_Nbr,"CONTRACT",
                    		pnd_Input_Pnd_Ppcn_Nbr,"PAYEE",
                    		pnd_Input_Pnd_Payee_Cde_A,"NAME",
                    		pnd_Name,"ISSUE DATE",
                    		pnd_H_Issue_Date,"OPTION",
                    		pnd_H_Opt, new ReportEditMask ("99"),"MODE",
                    		pnd_Input_Pnd_Mode_Ind,"PEND",
                    		pnd_Input_Pnd_Pend_Cde);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL MISSING NAME AND ADDRESS:",pnd_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));                                          //Natural: WRITE ( 1 ) 'TOTAL MISSING NAME AND ADDRESS:' #CNT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PAYMENT-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MAIL-ADDRESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-FROM-COR
        //* ***********************************************************************
    }
    private void sub_Get_Payment_Address() throws Exception                                                                                                               //Natural: GET-PAYMENT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Addr_Found.reset();                                                                                                                                           //Natural: RESET #ADDR-FOUND
        if (condition(pnd_Chck_Addr_Eof.getBoolean()))                                                                                                                    //Natural: IF #CHCK-ADDR-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Check_Address_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                            //Natural: IF #CHECK-ADDRESS.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(pnd_Use_Payee_01.getBoolean()))                                                                                                             //Natural: IF #USE-PAYEE-01
                {
                    if (condition(pnd_Chck_Payee_Pnd_Chck_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr) && pnd_Input_Pnd_Payee_Cde.equals(2)))                                    //Natural: IF #CHCK-CNTRCT = #PPCN-NBR AND #PAYEE-CDE = 02
                    {
                        pnd_Addr_Found.setValue(true);                                                                                                                    //Natural: ASSIGN #ADDR-FOUND := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Check_Address_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                             //Natural: IF #CHECK-ADDRESS.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
            {
                pnd_Addr_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #ADDR-FOUND := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(3, pnd_Check_Address);                                                                                                                    //Natural: READ WORK 3 ONCE #CHECK-ADDRESS
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Chck_Addr_Eof.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #CHCK-ADDR-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Use_Payee_01.getBoolean()))                                                                                                                 //Natural: IF #USE-PAYEE-01
            {
                if (condition(pnd_Check_Address_Cntrct_Payee_Cde.equals(1)))                                                                                              //Natural: IF #CHECK-ADDRESS.CNTRCT-PAYEE-CDE = 01
                {
                    pnd_Chck_Payee.setValue(pnd_Check_Address_Cntrct_Payee);                                                                                              //Natural: ASSIGN #CHCK-PAYEE := #CHECK-ADDRESS.CNTRCT-PAYEE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Mail_Address() throws Exception                                                                                                                  //Natural: GET-MAIL-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Addr_Found.reset();                                                                                                                                           //Natural: RESET #ADDR-FOUND
        if (condition(pnd_Ml_Addr_Eof.getBoolean()))                                                                                                                      //Natural: IF #ML-ADDR-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Mail_Address_Cntrct_Py.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                //Natural: IF #MAIL-ADDRESS.CNTRCT-PY GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(pnd_Use_Payee_01.getBoolean()))                                                                                                             //Natural: IF #USE-PAYEE-01
                {
                    if (condition(pnd_Corr_Payee_Pnd_Corr_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr) && pnd_Input_Pnd_Payee_Cde.equals(2)))                                    //Natural: IF #CORR-CNTRCT = #PPCN-NBR AND #PAYEE-CDE = 02
                    {
                        pnd_Addr_Found.setValue(true);                                                                                                                    //Natural: ASSIGN #ADDR-FOUND := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Mail_Address_Cntrct_Py.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                 //Natural: IF #MAIL-ADDRESS.CNTRCT-PY = #INPUT.#CNTRCT-PAYEE
            {
                pnd_Addr_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #ADDR-FOUND := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(4, pnd_Mail_Address);                                                                                                                     //Natural: READ WORK 4 ONCE #MAIL-ADDRESS
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Ml_Addr_Eof.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #ML-ADDR-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Use_Payee_01.getBoolean()))                                                                                                                 //Natural: IF #USE-PAYEE-01
            {
                if (condition(pnd_Mail_Address_Cntrct_Payee_Cde.equals(1)))                                                                                               //Natural: IF #MAIL-ADDRESS.CNTRCT-PAYEE-CDE = 01
                {
                    pnd_Corr_Payee.setValue(pnd_Check_Address_Cntrct_Payee);                                                                                              //Natural: ASSIGN #CORR-PAYEE := #CHECK-ADDRESS.CNTRCT-PAYEE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Name_From_Cor() throws Exception                                                                                                                 //Natural: GET-NAME-FROM-COR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name.setValue("COR RECORD NOT FOUND");                                                                                                                        //Natural: ASSIGN #NAME := 'COR RECORD NOT FOUND'
        if (condition(pnd_Cor_Eof.getBoolean()))                                                                                                                          //Natural: IF #COR-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Core_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                     //Natural: IF #CORE.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Core_Pnd_Cor_Status_Cde.equals(" ")))                                                                                                       //Natural: IF #COR-STATUS-CDE = ' '
            {
                if (condition(pnd_Core_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                  //Natural: IF #CORE.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
                {
                    pnd_Name.setValue(DbsUtil.compress(pnd_Core_Ph_First_Nme, pnd_Core_Ph_Mddle_Nme, pnd_Core_Ph_Last_Nme));                                              //Natural: COMPRESS PH-FIRST-NME PH-MDDLE-NME PH-LAST-NME INTO #NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Core);                                                                                                                             //Natural: READ WORK 2 ONCE #CORE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Cor_Eof.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #COR-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(31),"IA MISSING NAME AND ADDRESS REPORT",new      //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 31X 'IA MISSING NAME AND ADDRESS REPORT' 31X *DATX ( EM = MM/DD/YYYY )
                        ColumnSpacing(31),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=180 PS=0");

        getReports().setDisplayColumns(1, "PIN",
        		pnd_Input_Pnd_Cpr_Id_Nbr,"CONTRACT",
        		pnd_Input_Pnd_Ppcn_Nbr,"PAYEE",
        		pnd_Input_Pnd_Payee_Cde_A,"NAME",
        		pnd_Name,"ISSUE DATE",
        		pnd_H_Issue_Date,"OPTION",
        		pnd_H_Opt, new ReportEditMask ("99"),"MODE",
        		pnd_Input_Pnd_Mode_Ind,"PEND",
        		pnd_Input_Pnd_Pend_Cde);
    }
}
