/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:12 PM
**        * FROM NATURAL PROGRAM : Iaap382
************************************************************
**        * FILE NAME            : Iaap382.java
**        * CLASS NAME           : Iaap382
**        * INSTANCE NAME        : Iaap382
************************************************************
*********************************************************************
*
* THIS PROGRAM WILL DELETE OR RESET THE IAA DEDUCTION RECORDS BASED
* ON SPECIFIC EDITING CRITERIA.  IT ALSO WILL GENERATE REPORTS SHOWING
* THE DELETED RECORDS, RESET RECORDS AND A GRAND TOTAL PAGE WITH THE
* TOTAL NUMBER OF RECORDS AFFECTED.
*
*********************  MAINTENANCE LOG ******************************
*
* DATE       PROGRAMMER       DESCRIPTION
* 11/97RM              PUT FIX IN TO FORCE CHECK DATE OF 12/97
*                      COMMENTED OUT
*                      DO SCAN ON 11/97
* 04/2017 OS PIN EXPANSION  - RE-STOWED FOR MAP CHANGE.
*********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap382 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;

    private DataAccessProgramView vw_iaa_Deduction_View;
    private DbsField iaa_Deduction_View_Lst_Trans_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_View_Ddctn_Payee;
    private DbsField iaa_Deduction_View_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_View_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_View_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_View_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_View_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_View_Ddctn_Final_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role_View;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;

    private DbsGroup pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Check_Dte;

    private DbsGroup pnd_Check_Dte__R_Field_2;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_N;
    private DbsField pnd_Total_Deleted;
    private DbsField pnd_Total_Reset;
    private DbsField pnd_Total_Active;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Active_Per_Amt;
    private DbsField pnd_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_View);

        vw_iaa_Deduction_View = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction_View", "IAA-DEDUCTION-VIEW"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_View_Lst_Trans_Dte = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Deduction_View_Ddctn_Ppcn_Nbr = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_View_Ddctn_Payee_Cde = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_View_Ddctn_Id_Nbr = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_View_Ddctn_Cde = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DDCTN_CDE");
        iaa_Deduction_View_Ddctn_Seq_Nbr = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_View_Ddctn_Payee = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Deduction_View_Ddctn_Per_Amt = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_View_Ddctn_Ytd_Amt = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_View_Ddctn_Pd_To_Dte = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_View_Ddctn_Tot_Amt = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_View_Ddctn_Intent_Cde = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_View_Ddctn_Strt_Dte = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_View_Ddctn_Stp_Dte = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_View_Ddctn_Final_Dte = vw_iaa_Deduction_View.getRecord().newFieldInGroup("iaa_Deduction_View_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Deduction_View);

        vw_iaa_Cntrct_Prtcpnt_Role_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role_View", "IAA-CNTRCT-PRTCPNT-ROLE-VIEW"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role_View.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role_View);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R = pnd_Cntrct_Payee_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R", "#CNTRCT-PAYEE-KEY-R");
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Key_R.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 8);

        pnd_Check_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Dte__R_Field_2", "REDEFINE", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_N = pnd_Check_Dte__R_Field_2.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_N", "#CHECK-DTE-N", FieldType.NUMERIC, 8);
        pnd_Total_Deleted = localVariables.newFieldInRecord("pnd_Total_Deleted", "#TOTAL-DELETED", FieldType.INTEGER, 4);
        pnd_Total_Reset = localVariables.newFieldInRecord("pnd_Total_Reset", "#TOTAL-RESET", FieldType.INTEGER, 4);
        pnd_Total_Active = localVariables.newFieldInRecord("pnd_Total_Active", "#TOTAL-ACTIVE", FieldType.INTEGER, 4);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.INTEGER, 4);
        pnd_Total_Active_Per_Amt = localVariables.newFieldInRecord("pnd_Total_Active_Per_Amt", "#TOTAL-ACTIVE-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_View.reset();
        vw_iaa_Deduction_View.reset();
        vw_iaa_Cntrct_Prtcpnt_Role_View.reset();

        localVariables.reset();
        pnd_Cntrct_Payee_Key.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap382() throws Exception
    {
        super("Iaap382");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132;//Natural: FORMAT ( 3 ) PS = 60 LS = 132
        vw_iaa_Cntrl_Rcrd_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW WITH CNTRL-RCRD-KEY = 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_View.readNextRow("READ01")))
        {
            pnd_Check_Dte.setValueEdited(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DTE
            //*  REMOVE FOLLOWING AFTER 12/97 RUN                  11/97
            //*    #CHECK-DTE-N := 19971201
            //*  END OF REMOVE  11/97
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Deduction_View.startDatabaseRead                                                                                                                           //Natural: READ IAA-DEDUCTION-VIEW LOGICAL BY CNTRCT-PAYEE-DDCTN-KEY
        (
        "PND_PND_L0750",
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        PND_PND_L0750:
        while (condition(vw_iaa_Deduction_View.readNextRow("PND_PND_L0750")))
        {
            pnd_Total.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #TOTAL
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            if (condition(iaa_Deduction_View_Ddctn_Stp_Dte.greater(getZero()) && iaa_Deduction_View_Ddctn_Stp_Dte.lessOrEqual(pnd_Check_Dte_Pnd_Check_Dte_N)))            //Natural: IF DDCTN-STP-DTE GT 0 AND DDCTN-STP-DTE LE #CHECK-DTE-N
            {
                pnd_Total_Deleted.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-DELETED
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382a.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IAAM382A'
                vw_iaa_Deduction_View.deleteDBRow("PND_PND_L0750");                                                                                                       //Natural: DELETE ( ##L0750. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaa_Deduction_View_Ddctn_Stp_Dte.equals(getZero())))                                                                                        //Natural: IF DDCTN-STP-DTE = 0
                {
                    pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Deduction_View_Ddctn_Ppcn_Nbr);                                                            //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR = DDCTN-PPCN-NBR
                    pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Deduction_View_Ddctn_Payee_Cde);                                                          //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE = DDCTN-PAYEE-CDE
                    vw_iaa_Cntrct_Prtcpnt_Role_View.startDatabaseFind                                                                                                     //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE-VIEW WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                    (
                    "FIND01",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
                    );
                    FIND01:
                    while (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.readNextRow("FIND01", true)))
                    {
                        vw_iaa_Cntrct_Prtcpnt_Role_View.setIfNotFoundControlFlag(false);
                        if (condition(vw_iaa_Cntrct_Prtcpnt_Role_View.getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                        {
                            getReports().write(0, "*******************************************",NEWLINE,"NO CNTRCT RECORD FOUND FOR DEDUCTION RECORD",                    //Natural: WRITE '*******************************************' / 'NO CNTRCT RECORD FOUND FOR DEDUCTION RECORD' / 'PPCN NUMBER = ' DDCTN-PPCN-NBR / 'PAYEE CODE  = ' DDCTN-PAYEE-CDE / '*******************************************' /
                                NEWLINE,"PPCN NUMBER = ",iaa_Deduction_View_Ddctn_Ppcn_Nbr,NEWLINE,"PAYEE CODE  = ",iaa_Deduction_View_Ddctn_Payee_Cde,NEWLINE,
                                "*******************************************",NEWLINE);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-NOREC
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0750"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0750"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(iaa_Cntrct_Prtcpnt_Role_View_Cntrct_Actvty_Cde.equals(9)))                                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE-VIEW.CNTRCT-ACTVTY-CDE = 9
                    {
                        pnd_Total_Deleted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOTAL-DELETED
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382a.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IAAM382A'
                        vw_iaa_Deduction_View.deleteDBRow("PND_PND_L0750");                                                                                               //Natural: DELETE ( ##L0750. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Total_Reset.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-RESET
                        pnd_Total_Active.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-ACTIVE
                        pnd_Total_Active_Per_Amt.nadd(iaa_Deduction_View_Ddctn_Per_Amt);                                                                                  //Natural: ADD DDCTN-PER-AMT TO #TOTAL-ACTIVE-PER-AMT
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382a.class));                                                              //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM382A'
                        iaa_Deduction_View_Ddctn_Ytd_Amt.reset();                                                                                                         //Natural: RESET DDCTN-YTD-AMT
                        vw_iaa_Deduction_View.updateDBRow("PND_PND_L0750");                                                                                               //Natural: UPDATE ( ##L0750. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Count.equals(50)))                                                                                                                          //Natural: IF #COUNT = 50
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Count.reset();                                                                                                                                        //Natural: RESET #COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Count.greater(getZero())))                                                                                                                      //Natural: IF #COUNT > 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(3, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382d.class));                                                                              //Natural: WRITE ( 3 ) NOTITLE USING FORM 'IAAM382D'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382c.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IAAM382C'
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IAAM382'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382b.class));                                                                  //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM382B'
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam382.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM382'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");
    }
}
