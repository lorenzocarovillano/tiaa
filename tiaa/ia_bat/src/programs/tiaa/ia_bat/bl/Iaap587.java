/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:19 PM
**        * FROM NATURAL PROGRAM : Iaap587
************************************************************
**        * FILE NAME            : Iaap587.java
**        * CLASS NAME           : Iaap587
**        * INSTANCE NAME        : Iaap587
************************************************************
**********************************************************************
* YR2000 COMPLIANT FIX APPLIED     ---> T SHINE 11/30/99
*                         LAST PCY STOW      ON 04/13/99 12:07:04
*                         REVIEWED BY   JS   ON 12/02/99
**********************************************************************
**Y2NCTS YENY13
*
**********************************************************************
*
*   PROGRAM   -  IAAP587    A DEATH CLAIMS BALANCING REPORT FOR ALL
*      DATE   -  04/99      PAYMENTS THAT DAY PULLED FROM THE AUDIT
*    AUTHOR   -  ARI G.     FILE. THE REPORT IS BROKEN UP INTO TIAA
*                           AND CREF COLUMNS WITH SECTIONS FOR THE
*                           SAME CHECKING AND ACCOUNTING DATE & FOR
*                           DIFFERENT CHECKING & ACCOUNTING DATES
*   HISTORY   -  05/00 A.G. ADDED PA SELECT LOGIC
*
*                05/2002    RESTOWED FOR CHANGES IN IAAN051A,
*                               IAAN051F & IAAN0511
*                10/2000    WRITE OUT RECORDS WITH INSTLLMNT-TYP = LS TO
*                           A WORK FILE
* 03/25/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE AUDIT FILE.   *
*                   SC 032510.
*
* 10/18/11  J TINIO - MEOW CHANGES. ADDED 2 FIELDS TO PASS TO AIAN026.
* 04/05/12  O. SOTTO  RATE BASE CHANGES. SC 040512.
* JUN 2017 J BREMER       PIN EXPANSION RE-STOW ONLY (FOR GDG)
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap587 extends BLNatBase
{
    // Data Areas
    private GdaIaag588r gdaIaag588r;
    private LdaIaal587 ldaIaal587;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal205a ldaIaal205a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Y2_Yy_N;

    private DbsGroup pnd_Y2_Yy_N__R_Field_1;
    private DbsField pnd_Y2_Yy_N_Pnd_Y2_Yy_A;
    private DbsField pnd_Payment;
    private DbsField pnd_Total_Payment;
    private DbsField pnd_Total_Dci;
    private DbsField pnd_Total_Rem;
    private DbsField pnd_Dci_Rem;
    private DbsField pnd_Dci;
    private DbsField pnd_Pct;
    private DbsField pnd_Tab_Gross_Total;
    private DbsField pnd_Tab_Check_Total;
    private DbsField pnd_Tab_Eft_Total;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_3;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Return_Code;

    private DbsGroup pnd_A26_Fields__R_Field_4;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;
    private DbsField pnd_W_Auv;
    private DbsField pnd_W_Amt;
    private DbsField pnd_Val_Meth;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Z;
    private DbsField pnd_W;
    private DbsField pnd_Len;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_W_Fund_Code;

    private DbsGroup pnd_W_Fund_Code__R_Field_5;
    private DbsField pnd_W_Fund_Code_Pnd_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_Pnd_Fund_Code_2;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_T_Fund_Code_Hold;

    private DbsGroup pnd_T_Fund_Code_Hold__R_Field_6;
    private DbsField pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1;
    private DbsField pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3;
    private DbsField pnd_T_Fund_Code_1_3;

    private DbsGroup pnd_T_Fund_Code_1_3__R_Field_7;
    private DbsField pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1;
    private DbsField pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3;
    private DbsField pnd_Fund_Code_Monthly;

    private DbsGroup pnd_Fund_Code_Monthly__R_Field_8;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1;

    private DbsGroup pnd_Fund_Code_Monthly__R_Field_9;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2;
    private DbsField pnd_T_Fund_Code_M;
    private DbsField pnd_Table_Comp_Desc;
    private DbsField pnd_Table_Fund_Desc;
    private DbsField pnd_Fund_Desc;

    private DbsGroup pnd_Fund_Desc__R_Field_10;
    private DbsField pnd_Fund_Desc_Pnd_Fund_Desc_14;
    private DbsField pnd_Sub;
    private DbsField pnd_G;
    private DbsField pnd_Gg;
    private DbsField pnd_B;
    private DbsField pnd_Pay_Field;
    private DbsField pnd_W_Contract;
    private DbsField pnd_W_Payee;
    private DbsField pnd_Tab_Install_Date;

    private DbsGroup pnd_Tab_Install_Date__R_Field_11;
    private DbsField pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N;
    private DbsField pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_File;
    private DbsField pnd_Tab_Inverse_Date;
    private DbsField pnd_Num_Of_Instlmnts;
    private DbsField pnd_W_Payee_1;
    private DbsField pnd_W_Payee_2;
    private DbsField pnd_Nmb;
    private DbsField pnd_Bottom_Date;
    private DbsField pnd_Ov;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_A;
    private DbsField pnd_L;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_12;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code;
    private DbsField pnd_W_Fund_Code_1_3;

    private DbsGroup pnd_W_Fund_Code_1_3__R_Field_13;
    private DbsField pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3;
    private DbsField pnd_W_Fund_Dte;

    private DbsGroup pnd_W_Fund_Dte__R_Field_14;
    private DbsField pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A;
    private DbsField pnd_W_Issue_Date;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_15;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date_A__R_Field_16;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A__R_Field_17;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Mm;

    private DbsGroup pnd_Issue_Date_A__R_Field_18;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd;
    private DbsField pnd_Xfr_Issue_Date_A;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_19;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_20;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_21;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_22;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd;
    private DbsField pnd_Ia_Rpt_Cmpny_Cd_A;
    private DbsField pnd_Ia_Rpt_Nm_Cd;
    private DbsField pnd_Write_Work2;

    private DbsGroup pnd_Work_Rec_3;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Num_Payments;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Gross_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Check_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Eft_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Check_Count;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Eft_Count;
    private DbsField pnd_Dated;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaIaag588r = GdaIaag588r.getInstance(getCallnatLevel());
        registerRecord(gdaIaag588r);
        if (gdaOnly) return;

        ldaIaal587 = new LdaIaal587();
        registerRecord(ldaIaal587);
        registerRecord(ldaIaal587.getVw_audit_View());
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Y2_Yy_N = localVariables.newFieldInRecord("pnd_Y2_Yy_N", "#Y2-YY-N", FieldType.NUMERIC, 2);

        pnd_Y2_Yy_N__R_Field_1 = localVariables.newGroupInRecord("pnd_Y2_Yy_N__R_Field_1", "REDEFINE", pnd_Y2_Yy_N);
        pnd_Y2_Yy_N_Pnd_Y2_Yy_A = pnd_Y2_Yy_N__R_Field_1.newFieldInGroup("pnd_Y2_Yy_N_Pnd_Y2_Yy_A", "#Y2-YY-A", FieldType.STRING, 2);
        pnd_Payment = localVariables.newFieldInRecord("pnd_Payment", "#PAYMENT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Payment = localVariables.newFieldInRecord("pnd_Total_Payment", "#TOTAL-PAYMENT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Dci = localVariables.newFieldInRecord("pnd_Total_Dci", "#TOTAL-DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Rem = localVariables.newFieldInRecord("pnd_Total_Rem", "#TOTAL-REM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dci_Rem = localVariables.newFieldInRecord("pnd_Dci_Rem", "#DCI-REM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dci = localVariables.newFieldInRecord("pnd_Dci", "#DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pct = localVariables.newFieldInRecord("pnd_Pct", "#PCT", FieldType.PACKED_DECIMAL, 5, 2);
        pnd_Tab_Gross_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Gross_Total", "#TAB-GROSS-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Tab_Check_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Check_Total", "#TAB-CHECK-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Tab_Eft_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Eft_Total", "#TAB-EFT-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr", 
            "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee", "#OLD-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte", 
            "#OLD-INVERSE-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code = pnd_Cntrct_Py_Dte_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code", "#OLD-FUND-CODE", 
            FieldType.STRING, 3);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Return_Code = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_4 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_4", "REDEFINE", pnd_A26_Fields_Pnd_Return_Code);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields__R_Field_4.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_4.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_W_Auv = localVariables.newFieldInRecord("pnd_W_Auv", "#W-AUV", FieldType.NUMERIC, 6, 2);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Val_Meth = localVariables.newFieldInRecord("pnd_Val_Meth", "#VAL-METH", FieldType.STRING, 1);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 2);
        pnd_W = localVariables.newFieldInRecord("pnd_W", "#W", FieldType.NUMERIC, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_W_Fund_Code = localVariables.newFieldInRecord("pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 3);

        pnd_W_Fund_Code__R_Field_5 = localVariables.newGroupInRecord("pnd_W_Fund_Code__R_Field_5", "REDEFINE", pnd_W_Fund_Code);
        pnd_W_Fund_Code_Pnd_Fund_Code_1 = pnd_W_Fund_Code__R_Field_5.newFieldInGroup("pnd_W_Fund_Code_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_W_Fund_Code_Pnd_Fund_Code_2 = pnd_W_Fund_Code__R_Field_5.newFieldInGroup("pnd_W_Fund_Code_Pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 
            2);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 3, new DbsArrayController(1, 60));
        pnd_T_Fund_Code_Hold = localVariables.newFieldInRecord("pnd_T_Fund_Code_Hold", "#T-FUND-CODE-HOLD", FieldType.STRING, 3);

        pnd_T_Fund_Code_Hold__R_Field_6 = localVariables.newGroupInRecord("pnd_T_Fund_Code_Hold__R_Field_6", "REDEFINE", pnd_T_Fund_Code_Hold);
        pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1 = pnd_T_Fund_Code_Hold__R_Field_6.newFieldInGroup("pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1", "#T-FUND-CODE-HOLD-1", 
            FieldType.STRING, 1);
        pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3 = pnd_T_Fund_Code_Hold__R_Field_6.newFieldInGroup("pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3", 
            "#T-FUND-CODE-HOLD-2-3", FieldType.STRING, 2);
        pnd_T_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_T_Fund_Code_1_3", "#T-FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_T_Fund_Code_1_3__R_Field_7 = localVariables.newGroupInRecord("pnd_T_Fund_Code_1_3__R_Field_7", "REDEFINE", pnd_T_Fund_Code_1_3);
        pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1 = pnd_T_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1", "#T-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3 = pnd_T_Fund_Code_1_3__R_Field_7.newFieldInGroup("pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3", "#T-FUND-CODE-2-3", 
            FieldType.STRING, 2);
        pnd_Fund_Code_Monthly = localVariables.newFieldInRecord("pnd_Fund_Code_Monthly", "#FUND-CODE-MONTHLY", FieldType.STRING, 3);

        pnd_Fund_Code_Monthly__R_Field_8 = localVariables.newGroupInRecord("pnd_Fund_Code_Monthly__R_Field_8", "REDEFINE", pnd_Fund_Code_Monthly);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1 = pnd_Fund_Code_Monthly__R_Field_8.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1", 
            "#FUND-CODE-MONTHLY-1", FieldType.STRING, 1);

        pnd_Fund_Code_Monthly__R_Field_9 = pnd_Fund_Code_Monthly__R_Field_8.newGroupInGroup("pnd_Fund_Code_Monthly__R_Field_9", "REDEFINE", pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N = pnd_Fund_Code_Monthly__R_Field_9.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N", 
            "#FUND-CODE-MONTHLY-1-N", FieldType.NUMERIC, 1);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2 = pnd_Fund_Code_Monthly__R_Field_8.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2", 
            "#FUND-CODE-MONTHLY-2", FieldType.STRING, 2);
        pnd_T_Fund_Code_M = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code_M", "#T-FUND-CODE-M", FieldType.STRING, 3, new DbsArrayController(1, 
            60));
        pnd_Table_Comp_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Comp_Desc", "#TABLE-COMP-DESC", FieldType.STRING, 4, new DbsArrayController(1, 
            60));
        pnd_Table_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Fund_Desc", "#TABLE-FUND-DESC", FieldType.STRING, 8, new DbsArrayController(1, 
            60));
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);

        pnd_Fund_Desc__R_Field_10 = localVariables.newGroupInRecord("pnd_Fund_Desc__R_Field_10", "REDEFINE", pnd_Fund_Desc);
        pnd_Fund_Desc_Pnd_Fund_Desc_14 = pnd_Fund_Desc__R_Field_10.newFieldInGroup("pnd_Fund_Desc_Pnd_Fund_Desc_14", "#FUND-DESC-14", FieldType.STRING, 
            14);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.STRING, 1);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 2);
        pnd_Gg = localVariables.newFieldInRecord("pnd_Gg", "#GG", FieldType.NUMERIC, 2);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_Pay_Field = localVariables.newFieldArrayInRecord("pnd_Pay_Field", "#PAY-FIELD", FieldType.STRING, 7, new DbsArrayController(1, 60));
        pnd_W_Contract = localVariables.newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 10);
        pnd_W_Payee = localVariables.newFieldInRecord("pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 2);
        pnd_Tab_Install_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", FieldType.STRING, 8, new DbsArrayController(1, 
            120));

        pnd_Tab_Install_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Tab_Install_Date__R_Field_11", "REDEFINE", pnd_Tab_Install_Date);
        pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N = pnd_Tab_Install_Date__R_Field_11.newFieldArrayInGroup("pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N", 
            "#TAB-INSTALL-DATE-N", FieldType.NUMERIC, 8, new DbsArrayController(1, 120));
        pnd_Tab_Per_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Per_Amt", "#TAB-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_Div_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Div_Amt", "#TAB-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_File = localVariables.newFieldArrayInRecord("pnd_Tab_File", "#TAB-FILE", FieldType.STRING, 1, new DbsArrayController(1, 120));
        pnd_Tab_Inverse_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Inverse_Date", "#TAB-INVERSE-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            120));
        pnd_Num_Of_Instlmnts = localVariables.newFieldInRecord("pnd_Num_Of_Instlmnts", "#NUM-OF-INSTLMNTS", FieldType.NUMERIC, 3);
        pnd_W_Payee_1 = localVariables.newFieldInRecord("pnd_W_Payee_1", "#W-PAYEE-1", FieldType.NUMERIC, 2);
        pnd_W_Payee_2 = localVariables.newFieldInRecord("pnd_W_Payee_2", "#W-PAYEE-2", FieldType.NUMERIC, 2);
        pnd_Nmb = localVariables.newFieldInRecord("pnd_Nmb", "#NMB", FieldType.NUMERIC, 2);
        pnd_Bottom_Date = localVariables.newFieldInRecord("pnd_Bottom_Date", "#BOTTOM-DATE", FieldType.NUMERIC, 8);
        pnd_Ov = localVariables.newFieldInRecord("pnd_Ov", "#OV", FieldType.STRING, 1);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_12", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code = pnd_Cntrct_Fund_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code", "#W-FND-CODE", FieldType.STRING, 
            3);
        pnd_W_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_W_Fund_Code_1_3", "#W-FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_W_Fund_Code_1_3__R_Field_13 = localVariables.newGroupInRecord("pnd_W_Fund_Code_1_3__R_Field_13", "REDEFINE", pnd_W_Fund_Code_1_3);
        pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1 = pnd_W_Fund_Code_1_3__R_Field_13.newFieldInGroup("pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3 = pnd_W_Fund_Code_1_3__R_Field_13.newFieldInGroup("pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3", "#W-FUND-CODE-2-3", 
            FieldType.STRING, 2);
        pnd_W_Fund_Dte = localVariables.newFieldInRecord("pnd_W_Fund_Dte", "#W-FUND-DTE", FieldType.NUMERIC, 8);

        pnd_W_Fund_Dte__R_Field_14 = localVariables.newGroupInRecord("pnd_W_Fund_Dte__R_Field_14", "REDEFINE", pnd_W_Fund_Dte);
        pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A = pnd_W_Fund_Dte__R_Field_14.newFieldInGroup("pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A", "#W-FUND-DTE-A", FieldType.STRING, 
            8);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_15", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_16 = pnd_Issue_Date_A__R_Field_15.newGroupInGroup("pnd_Issue_Date_A__R_Field_16", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Dd = pnd_Issue_Date_A__R_Field_16.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_17 = pnd_Issue_Date_A__R_Field_15.newGroupInGroup("pnd_Issue_Date_A__R_Field_17", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_Mm = pnd_Issue_Date_A__R_Field_17.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_18 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_18", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Ccyy", "#ISSUE-DATE-A8-CCYY", 
            FieldType.STRING, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Mm", "#ISSUE-DATE-A8-MM", 
            FieldType.STRING, 2);
        pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd = pnd_Issue_Date_A__R_Field_18.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_A8_Dd", "#ISSUE-DATE-A8-DD", 
            FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A = localVariables.newFieldInRecord("pnd_Xfr_Issue_Date_A", "#XFR-ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Xfr_Issue_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_19", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N = pnd_Xfr_Issue_Date_A__R_Field_19.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N", "#XFR-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Xfr_Issue_Date_A__R_Field_20 = pnd_Xfr_Issue_Date_A__R_Field_19.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_20", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm = pnd_Xfr_Issue_Date_A__R_Field_20.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm", 
            "#XFR-ISSUE-DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd = pnd_Xfr_Issue_Date_A__R_Field_20.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd", "#XFR-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_21 = pnd_Xfr_Issue_Date_A__R_Field_19.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_21", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy = pnd_Xfr_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy", 
            "#XFR-ISSUE-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm = pnd_Xfr_Issue_Date_A__R_Field_21.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm", "#XFR-ISSUE-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_22", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy", 
            "#XFR-ISSUE-DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm", 
            "#XFR-ISSUE-DATE-A8-MM", FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd = pnd_Xfr_Issue_Date_A__R_Field_22.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd", 
            "#XFR-ISSUE-DATE-A8-DD", FieldType.STRING, 2);
        pnd_Ia_Rpt_Cmpny_Cd_A = localVariables.newFieldArrayInRecord("pnd_Ia_Rpt_Cmpny_Cd_A", "#IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_Ia_Rpt_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_Ia_Rpt_Nm_Cd", "#IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_Write_Work2 = localVariables.newFieldInRecord("pnd_Write_Work2", "#WRITE-WORK2", FieldType.NUMERIC, 9);

        pnd_Work_Rec_3 = localVariables.newGroupInRecord("pnd_Work_Rec_3", "#WORK-REC-3");
        pnd_Work_Rec_3_Pnd_Pp_Num_Payments = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Num_Payments", "#PP-NUM-PAYMENTS", FieldType.NUMERIC, 
            7);
        pnd_Work_Rec_3_Pnd_Pp_Gross_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Gross_Total", "#PP-GROSS-TOTAL", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Check_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Check_Total", "#PP-CHECK-TOTAL", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Eft_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Eft_Total", "#PP-EFT-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Check_Count = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Check_Count", "#PP-CHECK-COUNT", FieldType.NUMERIC, 
            7);
        pnd_Work_Rec_3_Pnd_Pp_Eft_Count = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Eft_Count", "#PP-EFT-COUNT", FieldType.NUMERIC, 7);
        pnd_Dated = localVariables.newFieldInRecord("pnd_Dated", "#DATED", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal587.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal205a.initializeValues();

        localVariables.reset();
        pnd_Len.setInitialValue(10);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap587() throws Exception
    {
        super("Iaap587");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        //* *   PERFORM #SETUP-SYSTEM-DATE-TIME
        //* *
        getReports().write(0, "*** START OF PROGRAM IAAP587 ***",new ColumnSpacing(3)," VERSION 11/15/2000");                                                             //Natural: WRITE '*** START OF PROGRAM IAAP587 ***' 3X ' VERSION 11/15/2000'
        if (Global.isEscape()) return;
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, ldaIaal587.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Iaan051f.class , getCurrentProcessState(), pnd_Ia_Rpt_Nm_Cd.getValue("*"), pnd_Ia_Rpt_Cmpny_Cd_A.getValue("*"));                                  //Natural: CALLNAT 'IAAN051F' #IA-RPT-NM-CD ( * ) #IA-RPT-CMPNY-CD-A ( * )
        if (condition(Global.isEscape())) return;
        FE:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Gg.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #GG
            pnd_Sub.setValue(pnd_Ia_Rpt_Cmpny_Cd_A.getValue(pnd_G));                                                                                                      //Natural: MOVE #IA-RPT-CMPNY-CD-A ( #G ) TO #SUB
            if (condition(pnd_Sub.equals(" ")))                                                                                                                           //Natural: IF #SUB = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_T_Fund_Code.getValue(pnd_Gg).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ia_Rpt_Cmpny_Cd_A.getValue(pnd_G), pnd_Ia_Rpt_Nm_Cd.getValue(pnd_G))); //Natural: COMPRESS #IA-RPT-CMPNY-CD-A ( #G ) #IA-RPT-NM-CD ( #G ) INTO #T-FUND-CODE ( #GG ) LEAVING NO
            if (condition(pnd_T_Fund_Code.getValue(pnd_Gg).equals("U09") || pnd_T_Fund_Code.getValue(pnd_Gg).equals("U11") || DbsUtil.maskMatches(pnd_T_Fund_Code.getValue(pnd_Gg), //Natural: IF #T-FUND-CODE ( #GG ) = 'U09' OR = 'U11' OR #T-FUND-CODE ( #GG ) = MASK ( 'U4' )
                "'U4'")))
            {
                pnd_Gg.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #GG
                pnd_T_Fund_Code.getValue(pnd_Gg).setValue(pnd_T_Fund_Code.getValue(pnd_Gg.getDec().subtract(1)));                                                         //Natural: MOVE #T-FUND-CODE ( #GG -1 ) TO #T-FUND-CODE ( #GG )
                DbsUtil.examine(new ExamineSource(pnd_T_Fund_Code.getValue(pnd_Gg),true), new ExamineSearch("U"), new ExamineReplace("W"));                               //Natural: EXAMINE FULL #T-FUND-CODE ( #GG ) FOR 'U' REPLACE WITH 'W'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.reset();                                                                                                                                                    //Natural: RESET #B
        FG:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Fund_Code_Monthly.setValue(pnd_T_Fund_Code.getValue(pnd_G));                                                                                              //Natural: MOVE #T-FUND-CODE ( #G ) TO #FUND-CODE-MONTHLY
            if (condition(pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1.equals("2")))                                                                                     //Natural: IF #FUND-CODE-MONTHLY-1 = '2'
            {
                pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N.nadd(2);                                                                                                  //Natural: ADD 2 TO #FUND-CODE-MONTHLY-1-N
                pnd_B.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #B
                pnd_T_Fund_Code_M.getValue(pnd_B).setValue(pnd_Fund_Code_Monthly);                                                                                        //Natural: MOVE #FUND-CODE-MONTHLY TO #T-FUND-CODE-M ( #B )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FF:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            if (condition(pnd_T_Fund_Code.getValue(pnd_G).equals(" ")))                                                                                                   //Natural: IF #T-FUND-CODE ( #G ) = ' '
            {
                pnd_W.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #W
                if (condition(pnd_T_Fund_Code_M.getValue(pnd_W).notEquals(" ")))                                                                                          //Natural: IF #T-FUND-CODE-M ( #W ) NE ' '
                {
                    pnd_T_Fund_Code.getValue(pnd_G).setValue(pnd_T_Fund_Code_M.getValue(pnd_W));                                                                          //Natural: MOVE #T-FUND-CODE-M ( #W ) TO #T-FUND-CODE ( #G )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F9:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Fund_Desc.reset();                                                                                                                                        //Natural: RESET #FUND-DESC #COMP-DESC
            pnd_Comp_Desc.reset();
            if (condition(pnd_T_Fund_Code.getValue(pnd_G).equals(" ")))                                                                                                   //Natural: IF #T-FUND-CODE ( #G ) = ' '
            {
                if (true) break F9;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F9. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Fund_Code.setValue(pnd_T_Fund_Code.getValue(pnd_G));                                                                                                    //Natural: MOVE #T-FUND-CODE ( #G ) TO #W-FUND-CODE
            pnd_Len.setValue(6);                                                                                                                                          //Natural: MOVE 06 TO #LEN
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_W_Fund_Code_Pnd_Fund_Code_2, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2 #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            pnd_Table_Fund_Desc.getValue(pnd_G).setValue(pnd_Fund_Desc);                                                                                                  //Natural: MOVE #FUND-DESC TO #TABLE-FUND-DESC ( #G )
            pnd_Table_Comp_Desc.getValue(pnd_G).setValue(pnd_Comp_Desc);                                                                                                  //Natural: MOVE #COMP-DESC TO #TABLE-COMP-DESC ( #G )
            if (condition(pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("2") || pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("U")))                                                    //Natural: IF #FUND-CODE-1 = '2' OR = 'U'
            {
                pnd_Pay_Field.getValue(pnd_G).setValue("ANNUAL");                                                                                                         //Natural: MOVE 'ANNUAL' TO #PAY-FIELD ( #G )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("4") || pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("W")))                                                //Natural: IF #FUND-CODE-1 = '4' OR = 'W'
                {
                    pnd_Pay_Field.getValue(pnd_G).setValue("MONTHLY");                                                                                                    //Natural: MOVE 'MONTHLY' TO #PAY-FIELD ( #G )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pay_Field.getValue(pnd_G).setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #PAY-FIELD ( #G )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  032510
            //*  032510
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *. READ  AUDIT-VIEW PHYSICAL
        ldaIaal587.getVw_audit_View().startDatabaseRead                                                                                                                   //Natural: READ AUDIT-VIEW BY PAUDIT-STATUS-TIMESTAMP STARTING FROM #DATED
        (
        "RD",
        new Wc[] { new Wc("PAUDIT_STATUS_TIMESTAMP", ">=", pnd_Dated, WcType.BY) },
        new Oc[] { new Oc("PAUDIT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(ldaIaal587.getVw_audit_View().readNextRow("RD")))
        {
            ldaIaal587.getPnd_Audit_Reads().nadd(1);                                                                                                                      //Natural: ADD 1 TO #AUDIT-READS
            //* *Y2NCTS
            ldaIaal587.getPnd_Fl_Date_Yyyymmdd_Alph().setValueEdited(ldaIaal587.getAudit_View_Paudit_Status_Timestamp(),new ReportEditMask("YYYYMMDD"));                  //Natural: MOVE EDITED PAUDIT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            //* *Y2NCTS
            //* *Y2NCTS
            //*  032510
            if (condition(ldaIaal587.getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num().notEquals(ldaIaal587.getIaa_Parm_Card_Pnd_Parm_Date_N())))                   //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                //*  032510
                if (true) break RD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RD. )
                //*  032510
            }                                                                                                                                                             //Natural: END-IF
            //*  ACCEPT IF #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N /* 032510
            ldaIaal587.getPnd_Audit_Time_Selects().nadd(1);                                                                                                               //Natural: ADD 1 TO #AUDIT-TIME-SELECTS
            ldaIaal587.getPnd_Num().setValue(ldaIaal587.getAudit_View_Count_Castpaudit_Installments());                                                                   //Natural: MOVE C*PAUDIT-INSTALLMENTS TO #NUM
            //*  10/2000
            //* **
            if (condition(ldaIaal587.getAudit_View_Paudit_Instllmnt_Typ().getValue(1).equals("LS")))                                                                      //Natural: IF PAUDIT-INSTLLMNT-TYP ( 1 ) = 'LS'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-WORKFILE2
                sub_Write_Data_To_Workfile2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Write_Work2.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WRITE-WORK2
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* **
            if (condition(!(ldaIaal587.getPnd_Num().greater(getZero()))))                                                                                                 //Natural: ACCEPT IF #NUM > 0
            {
                continue;
            }
            ldaIaal587.getPnd_Payment_Records().nadd(1);                                                                                                                  //Natural: ADD 1 TO #PAYMENT-RECORDS
            //* *Y2NCTS
            if (condition(ldaIaal587.getPnd_Diff_Accounting_Date().notEquals("Y")))                                                                                       //Natural: IF #DIFF-ACCOUNTING-DATE NE 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-HEADERS
                sub_Pnd_Fill_Up_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCTS
            if (condition(ldaIaal587.getAudit_View_Paudit_Acctg_Dte().equals(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte())))                                               //Natural: IF PAUDIT-ACCTG-DTE EQ PAUDIT-PYMNT-DTE
            {
                ldaIaal587.getPnd_M().setValue(1);                                                                                                                        //Natural: MOVE 1 TO #M
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal587.getPnd_M().setValue(2);                                                                                                                        //Natural: MOVE 2 TO #M
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal587.getPnd_Tab_Total_Num_Of_Pay().getValue(ldaIaal587.getPnd_M()).nadd(1);                                                                             //Natural: ADD 1 TO #TAB-TOTAL-NUM-OF-PAY ( #M )
            ldaIaal587.getPnd_Check().reset();                                                                                                                            //Natural: RESET #CHECK #EFT #EFT-HOLD
            ldaIaal587.getPnd_Eft().reset();
            ldaIaal587.getPnd_Eft_Hold().reset();
            short decideConditionsMet850 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PAUDIT-TYPE-REQ-IND = 1
            if (condition(ldaIaal587.getAudit_View_Paudit_Type_Req_Ind().equals(1)))
            {
                decideConditionsMet850++;
                ldaIaal587.getPnd_Check().setValue("Y");                                                                                                                  //Natural: MOVE 'Y' TO #CHECK
            }                                                                                                                                                             //Natural: WHEN PAUDIT-TYPE-REQ-IND = 2
            else if (condition(ldaIaal587.getAudit_View_Paudit_Type_Req_Ind().equals(2)))
            {
                decideConditionsMet850++;
                ldaIaal587.getPnd_Eft().setValue("Y");                                                                                                                    //Natural: MOVE 'Y' TO #EFT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, NEWLINE,"PAUDIT-TYPE-REQ-IND IS NOT 1 OR 2  PIN ==>",ldaIaal587.getAudit_View_Paudit_Id_Nbr());                                     //Natural: WRITE / 'PAUDIT-TYPE-REQ-IND IS NOT 1 OR 2  PIN ==>' PAUDIT-ID-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_W_Contract.setValue(ldaIaal587.getAudit_View_Paudit_Ppcn_Nbr());                                                                                          //Natural: ASSIGN #W-CONTRACT := PAUDIT-PPCN-NBR
            pnd_W_Payee.setValue(ldaIaal587.getAudit_View_Paudit_Payee_Cde());                                                                                            //Natural: ASSIGN #W-PAYEE := PAUDIT-PAYEE-CDE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CNTRCT
            sub_Pnd_Process_Cntrct();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR
            sub_Pnd_Process_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Nmb.setValue(ldaIaal587.getAudit_View_Count_Castpaudit_Installments());                                                                                   //Natural: MOVE C*PAUDIT-INSTALLMENTS TO #NMB
            //* *Y2NCTS
            //*     #BOTTOM-DATE := PAUDIT-INSTLLMNT-DTE(#NMB)
            pnd_Bottom_Date.setValue(ldaIaal587.getAudit_View_Paudit_Lst_Pymnt_Dte());                                                                                    //Natural: ASSIGN #BOTTOM-DATE := PAUDIT-LST-PYMNT-DTE
            DbsUtil.callnat(Iaan460.class , getCurrentProcessState(), pnd_W_Contract, pnd_W_Payee, pnd_Tab_Install_Date.getValue(1,":",120), pnd_Tab_Per_Amt.getValue(1,":",120),  //Natural: CALLNAT 'IAAN460' #W-CONTRACT #W-PAYEE #TAB-INSTALL-DATE ( 1:120 ) #TAB-PER-AMT ( 1:120 ) #TAB-DIV-AMT ( 1:120 ) #TAB-FILE ( 1:120 ) #TAB-INVERSE-DATE ( 1:120 ) #NUM-OF-INSTLMNTS #BOTTOM-DATE #RET-CDE
                pnd_Tab_Div_Amt.getValue(1,":",120), pnd_Tab_File.getValue(1,":",120), pnd_Tab_Inverse_Date.getValue(1,":",120), pnd_Num_Of_Instlmnts, pnd_Bottom_Date, 
                pnd_Ret_Cde);
            if (condition(Global.isEscape())) return;
            //* *Y2NCTS
            //* *Y2NCTS
            //* *Y2NCTS
            short decideConditionsMet874 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #EFT = 'Y'
            if (condition(ldaIaal587.getPnd_Eft().equals("Y")))
            {
                decideConditionsMet874++;
                ldaIaal587.getPnd_Tab_Num_Eft().getValue(ldaIaal587.getPnd_M()).nadd(1);                                                                                  //Natural: ADD 1 TO #TAB-NUM-EFT ( #M )
                pnd_Work_Rec_3_Pnd_Pp_Num_Payments.nadd(1);                                                                                                               //Natural: ADD 1 TO #PP-NUM-PAYMENTS
            }                                                                                                                                                             //Natural: WHEN #CHECK = 'Y'
            else if (condition(ldaIaal587.getPnd_Check().equals("Y")))
            {
                decideConditionsMet874++;
                ldaIaal587.getPnd_Tab_Num_Check().getValue(ldaIaal587.getPnd_M()).nadd(1);                                                                                //Natural: ADD 1 TO #TAB-NUM-CHECK ( #M )
                pnd_Work_Rec_3_Pnd_Pp_Num_Payments.nadd(1);                                                                                                               //Natural: ADD 1 TO #PP-NUM-PAYMENTS
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet874 > 0))
            {
                ldaIaal587.getPnd_Tab_Num_Paym().getValue(ldaIaal587.getPnd_M()).nadd(1);                                                                                 //Natural: ADD 1 TO #TAB-NUM-PAYM ( #M )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #NUM
            for (ldaIaal587.getPnd_I().setValue(1); condition(ldaIaal587.getPnd_I().lessOrEqual(ldaIaal587.getPnd_Num())); ldaIaal587.getPnd_I().nadd(1))
            {
                FM:                                                                                                                                                       //Natural: FOR #A = 1 TO #NUM-OF-INSTLMNTS
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Instlmnts)); pnd_A.nadd(1))
                {
                    //* *Y2NCTS
                    if (condition(ldaIaal587.getAudit_View_Paudit_Instllmnt_Dte().getValue(ldaIaal587.getPnd_I()).equals(pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N.getValue(pnd_A)))) //Natural: IF PAUDIT-INSTLLMNT-DTE ( #I ) = #TAB-INSTALL-DATE-N ( #A )
                    {
                        if (true) break FM;                                                                                                                               //Natural: ESCAPE BOTTOM ( FM. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Dci_Rem.setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Dci().getValue(ldaIaal587.getPnd_I()));                                                    //Natural: ASSIGN #DCI-REM := PAUDIT-INSTLLMNT-DCI ( #I )
                pnd_Total_Dci.setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Dci().getValue(ldaIaal587.getPnd_I()));                                                  //Natural: ASSIGN #TOTAL-DCI := PAUDIT-INSTLLMNT-DCI ( #I )
                pnd_Total_Rem.setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Gross().getValue(ldaIaal587.getPnd_I()));                                                //Natural: ASSIGN #TOTAL-REM := PAUDIT-INSTLLMNT-GROSS ( #I )
                pnd_Total_Payment.setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Gross().getValue(ldaIaal587.getPnd_I()));                                            //Natural: ASSIGN #TOTAL-PAYMENT := PAUDIT-INSTLLMNT-GROSS ( #I )
                if (condition(pnd_Tab_File.getValue(pnd_A).equals("F")))                                                                                                  //Natural: IF #TAB-FILE ( #A ) = 'F'
                {
                                                                                                                                                                          //Natural: PERFORM #READ-FUND
                    sub_Pnd_Read_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Tab_File.getValue(pnd_A).equals("H")))                                                                                              //Natural: IF #TAB-FILE ( #A ) = 'H'
                    {
                                                                                                                                                                          //Natural: PERFORM #READ-HISTORY
                        sub_Pnd_Read_History();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "ERROR IN IAAN460 TABLE-FILE FIELD",pnd_Tab_File.getValue(pnd_A));                                                          //Natural: WRITE 'ERROR IN IAAN460 TABLE-FILE FIELD' #TAB-FILE ( #A )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #LOAD-TABLE-HEADER
        sub_Pnd_Load_Table_Header();
        if (condition(Global.isEscape())) {return;}
        //*  *********  START OF PRINTING REPORT *********
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        F1:                                                                                                                                                               //Natural: FOR #J = 1 TO 2
        for (ldaIaal587.getPnd_J().setValue(1); condition(ldaIaal587.getPnd_J().lessOrEqual(2)); ldaIaal587.getPnd_J().nadd(1))
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(ldaIaal587.getPnd_Title_Variable1().getValue(ldaIaal587.getPnd_J()).notEquals(getZero())))                                                      //Natural: IF #TITLE-VARIABLE1 ( #J ) NOT = 0
            {
                //* *Y2NCTS
                ldaIaal587.getPnd_Date_Ccyymmdd().setValue(ldaIaal587.getPnd_Title_Variable1().getValue(ldaIaal587.getPnd_J()));                                          //Natural: MOVE #TITLE-VARIABLE1 ( #J ) TO #DATE-CCYYMMDD
                //* ************************
                //* * YEAR 2000 FIX START **
                //* ************************
                //* *Y2CHTS
                pnd_Y2_Yy_N.setValue(ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Yy());                                                                                      //Natural: MOVE #DATE-YY TO #Y2-YY-N
                //* *Y2CHTS
                ldaIaal587.getPnd_W_Title_Variable1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),             //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #Y2-YY-A INTO #W-TITLE-VARIABLE1 LEAVING NO
                    "/", ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", pnd_Y2_Yy_N_Pnd_Y2_Yy_A));
                //* *************************
                //* * YEAR 2000 FIX END *****
                //* *************************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal587.getPnd_W_Title_Variable1().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO #W-TITLE-VARIABLE1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal587.getPnd_Title_Variable2().getValue(ldaIaal587.getPnd_J()).notEquals(getZero())))                                                      //Natural: IF #TITLE-VARIABLE2 ( #J ) NOT = 0
            {
                //* *Y2NCTS
                ldaIaal587.getPnd_Date_Ccyymmdd().setValue(ldaIaal587.getPnd_Title_Variable2().getValue(ldaIaal587.getPnd_J()));                                          //Natural: MOVE #TITLE-VARIABLE2 ( #J ) TO #DATE-CCYYMMDD
                //* ************************
                //* * YEAR 2000 FIX START **
                //* ************************
                //* *Y2CHTS
                pnd_Y2_Yy_N.setValue(ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Yy());                                                                                      //Natural: MOVE #DATE-YY TO #Y2-YY-N
                //* *Y2CHTS
                //* *   COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-YY
                ldaIaal587.getPnd_W_Title_Variable2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),             //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #Y2-YY-A INTO #W-TITLE-VARIABLE2 LEAVING NO
                    "/", ldaIaal587.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", pnd_Y2_Yy_N_Pnd_Y2_Yy_A));
                //* *************************
                //* * YEAR 2000 FIX END *****
                //* *************************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal587.getPnd_W_Title_Variable2().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO #W-TITLE-VARIABLE2
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCTS
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(24),"CHECK DATE: ",ldaIaal587.getPnd_W_Title_Variable1(),new TabSetting(72),                //Natural: WRITE ( 1 ) / 24T 'CHECK DATE: ' #W-TITLE-VARIABLE1 72T 'ACCOUNTING DATE: ' #W-TITLE-VARIABLE2
                "ACCOUNTING DATE: ",ldaIaal587.getPnd_W_Title_Variable2());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *Y2NCTS
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(31),"GROSS GTD AMT",new TabSetting(54),"GROSS DIV AMT",new TabSetting(87),"DCI AMT",new             //Natural: WRITE ( 1 ) 031T 'GROSS GTD AMT' 054T 'GROSS DIV AMT' 087T 'DCI AMT' 107T 'OVERPAY AMT'
                TabSetting(107),"OVERPAY AMT");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(31),"-------------",new TabSetting(54),"-------------",new TabSetting(87),"-------",new             //Natural: WRITE ( 1 ) 031T '-------------' 054T '-------------' 087T '-------' 107T '-----------'
                TabSetting(107),"-----------");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            FZ:                                                                                                                                                           //Natural: FOR #W = 2 TO 60
            for (pnd_W.setValue(2); condition(pnd_W.lessOrEqual(60)); pnd_W.nadd(1))
            {
                if (condition(pnd_T_Fund_Code.getValue(pnd_W).equals(" ")))                                                                                               //Natural: IF #T-FUND-CODE ( #W ) = ' '
                {
                    if (true) break FZ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FZ. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_T_Fund_Code_1_3.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                                        //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-1-3
                    if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3.greater("40")))                                                                                 //Natural: IF #T-FUND-CODE-2-3 > '40'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1.notEquals(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1)))                                      //Natural: IF #T-FUND-CODE-1 NE #T-FUND-CODE-HOLD-1
                        {
                            pnd_T_Fund_Code_Hold.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                               //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-HOLD
                            if (condition(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1.equals("W")))                                                                       //Natural: IF #T-FUND-CODE-HOLD-1 = 'W'
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().skip(1, 1);                                                                                                                  //Natural: SKIP ( 1 ) 1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Table_Comp_Desc.getValue(pnd_W),new TabSetting(6),pnd_Table_Fund_Desc.getValue(pnd_W),new  //Natural: WRITE ( 1 ) 001T #TABLE-COMP-DESC ( #W ) 006T #TABLE-FUND-DESC ( #W ) 015T #PAY-FIELD ( #W ) 030T #TABLE-GUAR ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 053T #TABLE-DIVD ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 082T #TABLE-DCI ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 106T #TABLE-OVRPY ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                        TabSetting(15),pnd_Pay_Field.getValue(pnd_W),new TabSetting(30),gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(53),gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal587.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(82),gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal587.getPnd_J(),pnd_W), new 
                        ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(106),gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal587.getPnd_J(),pnd_W), new 
                        ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   THIS WILL PRINT OUT THE NEW PA SELECT FUNDS
            FQ:                                                                                                                                                           //Natural: FOR #W = 2 TO 60
            for (pnd_W.setValue(2); condition(pnd_W.lessOrEqual(60)); pnd_W.nadd(1))
            {
                if (condition(pnd_T_Fund_Code.getValue(pnd_W).equals(" ")))                                                                                               //Natural: IF #T-FUND-CODE ( #W ) = ' '
                {
                    if (true) break FQ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FQ. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_T_Fund_Code_1_3.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                                        //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-1-3
                    if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3.lessOrEqual("40")))                                                                             //Natural: IF #T-FUND-CODE-2-3 <= '40'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1.notEquals(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1)))                                      //Natural: IF #T-FUND-CODE-1 NE #T-FUND-CODE-HOLD-1
                        {
                            pnd_T_Fund_Code_Hold.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                               //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-HOLD
                            if (condition(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1.equals("W")))                                                                       //Natural: IF #T-FUND-CODE-HOLD-1 = 'W'
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().skip(1, 1);                                                                                                                  //Natural: SKIP ( 1 ) 1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Table_Comp_Desc.getValue(pnd_W),new TabSetting(6),pnd_Table_Fund_Desc.getValue(pnd_W),new  //Natural: WRITE ( 1 ) 001T #TABLE-COMP-DESC ( #W ) 006T #TABLE-FUND-DESC ( #W ) 015T #PAY-FIELD ( #W ) 030T #TABLE-GUAR ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 053T #TABLE-DIVD ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 082T #TABLE-DCI ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 106T #TABLE-OVRPY ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                        TabSetting(15),pnd_Pay_Field.getValue(pnd_W),new TabSetting(30),gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(53),gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal587.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(82),gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal587.getPnd_J(),pnd_W), new 
                        ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(106),gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal587.getPnd_J(),pnd_W), new 
                        ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FQ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FQ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,"GROSS TOTAL",pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_J()), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new       //Natural: WRITE ( 1 ) 'GROSS TOTAL' #TAB-GROSS-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X 'CHECK COUNT' 9X #TAB-NUM-CHECK ( #J ) ( EM = ZZ9 )
                ColumnSpacing(17),"CHECK COUNT",new ColumnSpacing(9),ldaIaal587.getPnd_Tab_Num_Check().getValue(ldaIaal587.getPnd_J()), new ReportEditMask 
                ("ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,"CHECK TOTAL",pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_J()), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new       //Natural: WRITE ( 1 ) 'CHECK TOTAL' #TAB-CHECK-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 17X 'EFT   COUNT' 9X #TAB-NUM-EFT ( #J ) ( EM = ZZ9 )
                ColumnSpacing(17),"EFT   COUNT",new ColumnSpacing(9),ldaIaal587.getPnd_Tab_Num_Eft().getValue(ldaIaal587.getPnd_J()), new ReportEditMask 
                ("ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,"EFT   TOTAL",pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_J()), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));           //Natural: WRITE ( 1 ) 'EFT   TOTAL' #TAB-EFT-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(ldaIaal587.getPnd_J().equals(1)))                                                                                                               //Natural: IF #J = 1
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_J()).equals(getZero())))                                                                         //Natural: IF #TAB-GROSS-TOTAL ( #J ) EQ 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK3
                sub_Write_Work3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  NEWPAGE (1)
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "*** END OF PROGRAM IAAP587 ***");                                                                                                          //Natural: WRITE '*** END OF PROGRAM IAAP587 ***'
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-UP-HEADERS
        //* *Y2NCTS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-TABLE-HEADER
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ********************************************************************
        //* *******************************************************************
        //* *Y2NCTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-SYSTEM-DATE-TIME
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* ********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND
        //* *Y2NCTS
        //* *Y2NCTS
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-HISTORY
        //* *Y2NCTS
        //* *Y2NCTS
        //* *Y2NCTS
        //* *Y2NCTS
        //*       OLD-TIAA-RATES.CMPNY-FUND-CDE := '243'
        //*        COMPUTE #W-FUND-DTE = 100000000 - FUND-INVRSE-LST-PD-DTE
        //* *Y2NCTS
        //* **********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-SINGLE-BYTE-FUND
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-UNIT-VALUE
        //* * #RC
        //* *Y2NCTS
        //* *Y2NCTS
        //* ******************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CNTRCT
        //* *Y2NCTS
        //* *Y2NCTS
        //* *Y2NCTS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CPR
        //* ***********************************************************************
        //* *Y2NCTS
        //* *Y2NCTS
        //* *Y2NCTS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DCI-PARA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-WORKFILE2
        //* * 10/2000 *************************************************************
        //* * WORKFILE WILL BE USED IN IAAP588R TO PRINT LUMP SUM PAYMENTS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK3
    }
    private void sub_Pnd_Fill_Up_Headers() throws Exception                                                                                                               //Natural: #FILL-UP-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCTS
        if (condition(ldaIaal587.getAudit_View_Paudit_Acctg_Dte().greater(getZero()) && ldaIaal587.getAudit_View_Paudit_Pymnt_Dte().greater(getZero())))                  //Natural: IF AUDIT-VIEW.PAUDIT-ACCTG-DTE > 0 AND AUDIT-VIEW.PAUDIT-PYMNT-DTE > 0
        {
            //* *Y2NCTS
            if (condition(ldaIaal587.getAudit_View_Paudit_Acctg_Dte().notEquals(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte())))                                            //Natural: IF AUDIT-VIEW.PAUDIT-ACCTG-DTE NE AUDIT-VIEW.PAUDIT-PYMNT-DTE
            {
                //* *Y2NCTS
                ldaIaal587.getPnd_Title_Variable2().getValue(2).setValue(ldaIaal587.getAudit_View_Paudit_Acctg_Dte());                                                    //Natural: MOVE AUDIT-VIEW.PAUDIT-ACCTG-DTE TO #TITLE-VARIABLE2 ( 2 )
                //* *Y2NCTS
                ldaIaal587.getPnd_Title_Variable1().getValue(1).setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());                                                    //Natural: MOVE AUDIT-VIEW.PAUDIT-PYMNT-DTE TO #TITLE-VARIABLE1 ( 1 ) #TITLE-VARIABLE2 ( 1 ) #TITLE-VARIABLE1 ( 2 )
                ldaIaal587.getPnd_Title_Variable2().getValue(1).setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());
                ldaIaal587.getPnd_Title_Variable1().getValue(2).setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());
                //* *Y2NCTS
                ldaIaal587.getPnd_Diff_Accounting_Date().setValue("Y");                                                                                                   //Natural: MOVE 'Y' TO #DIFF-ACCOUNTING-DATE
                //* *Y2NCTS
                getReports().write(0, "NOTE !!! REPORT HAS 2 DIFFERENT ACCOUNTING DATES");                                                                                //Natural: WRITE 'NOTE !!! REPORT HAS 2 DIFFERENT ACCOUNTING DATES'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *Y2NCTS
                ldaIaal587.getPnd_Title_Variable1().getValue(1).setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());                                                    //Natural: MOVE AUDIT-VIEW.PAUDIT-PYMNT-DTE TO #TITLE-VARIABLE1 ( 1 ) #TITLE-VARIABLE2 ( 1 )
                ldaIaal587.getPnd_Title_Variable2().getValue(1).setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Load_Table_Header() throws Exception                                                                                                             //Natural: #LOAD-TABLE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal587.getPnd_Tab_Header().getValue(1).setValue("GROSS GUAR  ");                                                                                              //Natural: MOVE 'GROSS GUAR  ' TO #TAB-HEADER ( 1 )
        ldaIaal587.getPnd_Tab_Header().getValue(2).setValue("GROSS DIV   ");                                                                                              //Natural: MOVE 'GROSS DIV   ' TO #TAB-HEADER ( 2 )
        ldaIaal587.getPnd_Tab_Header().getValue(3).setValue("GROSS TOTAL ");                                                                                              //Natural: MOVE 'GROSS TOTAL ' TO #TAB-HEADER ( 3 )
        ldaIaal587.getPnd_Tab_Header().getValue(4).setValue("EFT GROSS   ");                                                                                              //Natural: MOVE 'EFT GROSS   ' TO #TAB-HEADER ( 4 )
        ldaIaal587.getPnd_Tab_Header().getValue(5).setValue("CHECK GROSS ");                                                                                              //Natural: MOVE 'CHECK GROSS ' TO #TAB-HEADER ( 5 )
        ldaIaal587.getPnd_Tab_Header().getValue(6).setValue("EFT/H GROSS ");                                                                                              //Natural: MOVE 'EFT/H GROSS ' TO #TAB-HEADER ( 6 )
        ldaIaal587.getPnd_Tab_Header().getValue(7).setValue("TOT DCI AMT");                                                                                               //Natural: MOVE 'TOT DCI AMT' TO #TAB-HEADER ( 7 )
        ldaIaal587.getPnd_Tab_Header().getValue(8).setValue("TOT OVPY AMT");                                                                                              //Natural: MOVE 'TOT OVPY AMT' TO #TAB-HEADER ( 8 )
        ldaIaal587.getPnd_Tab_Header().getValue(9).setValue("NO. OF PYMTS");                                                                                              //Natural: MOVE 'NO. OF PYMTS' TO #TAB-HEADER ( 9 )
        ldaIaal587.getPnd_Tab_Header().getValue(10).setValue("NO. OF EFT  ");                                                                                             //Natural: MOVE 'NO. OF EFT  ' TO #TAB-HEADER ( 10 )
        ldaIaal587.getPnd_Tab_Header().getValue(11).setValue("NO. OF CHKS ");                                                                                             //Natural: MOVE 'NO. OF CHKS ' TO #TAB-HEADER ( 11 )
        ldaIaal587.getPnd_Tab_Header().getValue(12).setValue("NO. OF EFT/H");                                                                                             //Natural: MOVE 'NO. OF EFT/H' TO #TAB-HEADER ( 12 )
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE READS ==========> ",ldaIaal587.getPnd_Audit_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                     //Natural: WRITE '  AUDIT FILE READS ==========> ' #AUDIT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE TIME SELECTS ===> ",ldaIaal587.getPnd_Audit_Time_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                              //Natural: WRITE '  AUDIT FILE TIME SELECTS ===> ' #AUDIT-TIME-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT PAYMENT SELECTS =====> ",ldaIaal587.getPnd_Payment_Records(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE '  AUDIT PAYMENT SELECTS =====> ' #PAYMENT-RECORDS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WRITE WORK RECORDS ========> ",pnd_Write_Work2, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE '  WRITE WORK RECORDS ========> ' #WRITE-WORK2 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Setup_System_Date_Time() throws Exception                                                                                                        //Natural: #SETUP-SYSTEM-DATE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCTS
        ldaIaal587.getPnd_Sys_Date().setValue(Global.getDATX());                                                                                                          //Natural: MOVE *DATX TO #SYS-DATE
        //* *Y2NCTS
        ldaIaal587.getPnd_Sy_Date_Yymmdd_Alph().setValueEdited(ldaIaal587.getPnd_Sys_Date(),new ReportEditMask("YYMMDD"));                                                //Natural: MOVE EDITED #SYS-DATE ( EM = YYMMDD ) TO #SY-DATE-YYMMDD-ALPH
        ldaIaal587.getPnd_Sys_Time().setValue(Global.getTIMX());                                                                                                          //Natural: MOVE *TIMX TO #SYS-TIME
        ldaIaal587.getPnd_Sy_Time_Hhiiss_Alph().setValueEdited(ldaIaal587.getPnd_Sys_Time(),new ReportEditMask("HHIISS"));                                                //Natural: MOVE EDITED #SYS-TIME ( EM = HHIISS ) TO #SY-TIME-HHIISS-ALPH
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCTS
        if (condition(DbsUtil.maskMatches(ldaIaal587.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                       //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCTS
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCTS
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal587.getIaa_Parm_Card_Pnd_Parm_Date());                                                           //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032510
            ldaIaal587.getIaa_Parm_Card_Pnd_Parm_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032510
        pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIaal587.getIaa_Parm_Card_Pnd_Parm_Date());                                                             //Natural: MOVE EDITED #PARM-DATE TO #DATED ( EM = YYYYMMDD )
    }
    private void sub_Pnd_Read_Fund() throws Exception                                                                                                                     //Natural: #READ-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE 'READ FUND'
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                               //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_W_Payee);                                                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code.setValue(" ");                                                                                                                 //Natural: ASSIGN #W-FND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RQ",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RQ:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RQ")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                FP:                                                                                                                                                       //Natural: FOR #N = 1 TO 60
                for (ldaIaal587.getPnd_N().setValue(1); condition(ldaIaal587.getPnd_N().lessOrEqual(60)); ldaIaal587.getPnd_N().nadd(1))
                {
                    if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals(pnd_T_Fund_Code.getValue(ldaIaal587.getPnd_N()))))                       //Natural: IF TIAA-CMPNY-FUND-CDE = #T-FUND-CODE ( #N )
                    {
                        if (true) break FP;                                                                                                                               //Natural: ESCAPE BOTTOM ( FP. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RQ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W_Fund_Code_1_3.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                                    //Natural: MOVE TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE-1-3
                pnd_L.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L
                if (condition(pnd_L.equals(1)))                                                                                                                           //Natural: IF #L = 1
                {
                    //* ***  ADD PAUDIT-INSTLLMNT-DCI(#I)      TO #TABLE-DCI(#M,#N)
                    gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal587.getAudit_View_Paudit_Instllmnt_Ovrpymnt().getValue(ldaIaal587.getPnd_I())); //Natural: ADD PAUDIT-INSTLLMNT-OVRPYMNT ( #I ) TO #TABLE-OVRPY ( #M,#N )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("T")))                                                                                         //Natural: IF #W-FUND-CODE-1 = 'T'
                {
                    gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());     //Natural: ADD TIAA-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                    gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());     //Natural: ADD TIAA-TOT-DIV-AMT TO #TABLE-DIVD ( #M,#N )
                    if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                       //Natural: IF #TOTAL-DCI = 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Payment.reset();                                                                                                                              //Natural: RESET #PAYMENT
                        pnd_Payment.compute(new ComputeParameters(false, pnd_Payment), pnd_Payment.add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt())); //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                        sub_Pnd_Dci_Para();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M())),        //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-GROSS-TOTAL ( #M )
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                    if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                                   //Natural: IF #EFT = 'Y'
                    {
                        pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M())),        //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-EFT-TOTAL ( #M )
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                             //Natural: IF #CHECK = 'Y'
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M())),  //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-CHECK-TOTAL ( #M )
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A.setValue(pnd_Tab_Install_Date.getValue(pnd_A));                                                                       //Natural: ASSIGN #W-FUND-DTE-A := #TAB-INSTALL-DATE ( #A )
                    if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                                //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                    {
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()); //Natural: ADD TIAA-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                                                   //Natural: MOVE TIAA-TOT-PER-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RQ"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                   //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                               //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                 //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                         //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                           //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3);                                                                                     //Natural: ASSIGN #FUND-2 := #W-FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                            //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-UNITS-CNT ( 1 ) * #AUV
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(pnd_W_Amt);                                            //Natural: ADD #W-AMT TO #TABLE-GUAR ( #M,#N )
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(pnd_W_Amt);                                                                                                              //Natural: MOVE #W-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RQ"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //* *
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                              //Natural: ADD #W-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                               //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                            //Natural: ADD #W-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                         //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                      //Natural: ADD #W-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break RQ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RQ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_History() throws Exception                                                                                                                  //Natural: #READ-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE 'READ HISTORY'
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                           //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pnd_W_Payee);                                                                                                 //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(pnd_Tab_Inverse_Date.getValue(pnd_A));                                                                      //Natural: ASSIGN #OLD-INVERSE-PD-DTE := #TAB-INVERSE-DATE ( #A )
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R3")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee)  //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #OLD-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #OLD-CNTRCT-PAYEE AND OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = #OLD-INVERSE-PD-DTE
                && ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte)))
            {
                FY:                                                                                                                                                       //Natural: FOR #N = 1 TO 60
                for (ldaIaal587.getPnd_N().setValue(1); condition(ldaIaal587.getPnd_N().lessOrEqual(60)); ldaIaal587.getPnd_N().nadd(1))
                {
                    if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde().equals(pnd_T_Fund_Code.getValue(ldaIaal587.getPnd_N()))))                                //Natural: IF OLD-TIAA-RATES.CMPNY-FUND-CDE = #T-FUND-CODE ( #N )
                    {
                        if (true) break FY;                                                                                                                               //Natural: ESCAPE BOTTOM ( FY. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W_Fund_Code_1_3.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                                             //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #W-FUND-CODE-1-3
                pnd_L.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L
                if (condition(pnd_L.equals(1)))                                                                                                                           //Natural: IF #L = 1
                {
                    gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal587.getAudit_View_Paudit_Instllmnt_Ovrpymnt().getValue(ldaIaal587.getPnd_I())); //Natural: ADD PAUDIT-INSTLLMNT-OVRPYMNT ( #I ) TO #TABLE-OVRPY ( #M,#N )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("T")))                                                                                         //Natural: IF #W-FUND-CODE-1 = 'T'
                {
                    gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());       //Natural: ADD CNTRCT-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                    gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());       //Natural: ADD CNTRCT-TOT-DIV-AMT TO #TABLE-DIVD ( #M,#N )
                    if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                       //Natural: IF #TOTAL-DCI = 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Payment.reset();                                                                                                                              //Natural: RESET #PAYMENT
                        pnd_Payment.compute(new ComputeParameters(false, pnd_Payment), pnd_Payment.add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt())); //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                        sub_Pnd_Dci_Para();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M())),        //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-GROSS-TOTAL ( #M )
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                    if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                                   //Natural: IF #EFT = 'Y'
                    {
                        pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M())),        //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-EFT-TOTAL ( #M )
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                             //Natural: IF #CHECK = 'Y'
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M())),  //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-CHECK-TOTAL ( #M )
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A.setValue(pnd_Tab_Install_Date.getValue(pnd_A));                                                                       //Natural: ASSIGN #W-FUND-DTE-A := #TAB-INSTALL-DATE ( #A )
                    //* *Y2NCTS
                    if (condition(((pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")) && pnd_W_Fund_Dte.greater(19970401)))) //Natural: IF ( #W-FUND-CODE-1 = 'U' OR = '2' ) AND #W-FUND-DTE > 19970401
                    {
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());   //Natural: ADD CNTRCT-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                                     //Natural: MOVE CNTRCT-TOT-PER-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                     //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                               //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                   //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                         //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                             //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3);                                                                                     //Natural: ASSIGN #FUND-2 := #W-FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                            //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-NO-UNITS ( 1 ) * #AUV
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(pnd_W_Amt);                                                                                                              //Natural: MOVE #W-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //* *
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(pnd_W_Amt);                                            //Natural: ADD #W-AMT TO #TABLE-GUAR ( #M,#N )
                        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                              //Natural: ADD #W-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal587.getPnd_Eft().equals("Y")))                                                                                               //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                            //Natural: ADD #W-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal587.getPnd_Check().equals("Y")))                                                                                         //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_M()).nadd(pnd_W_Amt);                                                                      //Natural: ADD #W-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Single_Byte_Fund() throws Exception                                                                                                          //Natural: #GET-SINGLE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Fund_1.reset();                                                                                                                                               //Natural: RESET #FUND-1
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtn_Cde);                                                                  //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTN-CDE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Get_Unit_Value() throws Exception                                                                                                                //Natural: #GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  WRITE ' INSIDE #GET-UNIT-VALUE'
        //* *Y2NCTS
        //*  040512
        pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                                   //Natural: RESET #AUV #AUV-RET-DTE #RETURN-CODE #A26-PRTC-DTE
        pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
        pnd_A26_Fields_Pnd_Return_Code.reset();
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
        pnd_A26_Fields_Pnd_A26_Call_Type.setValue("P");                                                                                                                   //Natural: ASSIGN #A26-CALL-TYPE := 'P'
        pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(pnd_Fund_1);                                                                                                              //Natural: ASSIGN #A26-FND-CDE := #FUND-1
        pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(pnd_Val_Meth);                                                                                                         //Natural: ASSIGN #A26-REVAL-METH := #VAL-METH
        pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_W_Fund_Dte);                                                                                                          //Natural: ASSIGN #A26-REQ-DTE := #W-FUND-DTE
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.setValue(pnd_W_Issue_Date);                                                                                                       //Natural: ASSIGN #A26-PRTC-DTE := #W-ISSUE-DATE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                        //Natural: CALLNAT 'AIAN026' #A26-FIELDS
        if (condition(Global.isEscape())) return;
        //* *Y2NCTS
        //*  #RC                                  /* 040512
        //*  #RETURN-CODE                         /* 040512
        //*  #AUV #AUV-RET-DTE
        //*  #DAYS-IN-REQUEST-MONTH
        //*  #DAYS-IN-PARTICIP-MONTH
        if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                                          //Natural: IF #RC > 0
        {
            //*  WRITE  / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC  /* 040512
            //*  040512
            getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Return_Code);                                                  //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER =>",pnd_W_Contract,pnd_W_Payee);                                                                                       //Natural: WRITE 'CONTRACT NUMBER =>' #W-CONTRACT #W-PAYEE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE #AUV TO #W-AUV
    }
    private void sub_Pnd_Process_Cntrct() throws Exception                                                                                                                //Natural: #PROCESS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *Y2NCTS
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CONTRACT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_W_Contract, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pnd_W_Contract);                                                                                           //Natural: WRITE 'NO CONTRACT FOUND FOR ' #W-CONTRACT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //* *Y2NCTS
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(1);                                                                                                           //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                             //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                                //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cpr() throws Exception                                                                                                                   //Natural: #PROCESS-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                                //Natural: ASSIGN #CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_W_Payee);                                                                                                  //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #W-PAYEE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CPR RECORD FOUND FOR ",pnd_W_Contract,pnd_W_Payee);                                                                             //Natural: WRITE 'NO CPR RECORD FOUND FOR ' #W-CONTRACT #W-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //* *Y2NCTS
            pnd_Xfr_Issue_Date_A.setValueEdited(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #XFR-ISSUE-DATE-A
            //* *Y2NCTS
            if (condition(pnd_Xfr_Issue_Date_A.equals(" ")))                                                                                                              //Natural: IF #XFR-ISSUE-DATE-A = ' '
            {
                pnd_W_Issue_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                                             //Natural: ASSIGN #W-ISSUE-DATE := #ISSUE-DATE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Issue_Date.setValue(pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);                                                                                     //Natural: ASSIGN #W-ISSUE-DATE := #XFR-ISSUE-DATE-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Dci_Para() throws Exception                                                                                                                      //Natural: #DCI-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan587b.class , getCurrentProcessState(), pnd_Payment, pnd_Total_Payment, pnd_Total_Dci, pnd_Dci, pnd_Total_Rem, pnd_Dci_Rem);                   //Natural: CALLNAT 'IAAN587B' #PAYMENT #TOTAL-PAYMENT #TOTAL-DCI #DCI #TOTAL-REM #DCI-REM
        if (condition(Global.isEscape())) return;
        gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal587.getPnd_M(),ldaIaal587.getPnd_N()).nadd(pnd_Dci);                                                               //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
    }
    private void sub_Write_Data_To_Workfile2() throws Exception                                                                                                           //Natural: WRITE-DATA-TO-WORKFILE2
    {
        if (BLNatReinput.isReinput()) return;

        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Id_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Id_Nbr());                                                              //Natural: ASSIGN #WK2-ID-NBR := PAUDIT-ID-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Tax_Id_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Tax_Id_Nbr());                                                      //Natural: ASSIGN #WK2-TAX-ID-NBR := PAUDIT-TAX-ID-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Ppcn_Nbr());                                                   //Natural: ASSIGN #WK2-CNTRCT-PPCN-NBR := PAUDIT-PPCN-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Payee_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Payee_Cde());                                                        //Natural: ASSIGN #WK2-PAYEE-CDE := PAUDIT-PAYEE-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Req_Seq_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Req_Seq_Nbr());                                                    //Natural: ASSIGN #WK2-REQ-SEQ-NBR := PAUDIT-REQ-SEQ-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Timestamp().setValue(ldaIaal587.getPnd_Fl_Date_Yyyymmdd_Alph());                                                          //Natural: ASSIGN #WK2-TIMESTAMP := #FL-DATE-YYYYMMDD-ALPH
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Status_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Status_Cde());                                                      //Natural: ASSIGN #WK2-STATUS-CDE := PAUDIT-STATUS-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Status_Timestamp().setValue(ldaIaal587.getPnd_Fl_Date_Yyyymmdd_Alph());                                                   //Natural: ASSIGN #WK2-STATUS-TIMESTAMP := #FL-DATE-YYYYMMDD-ALPH
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Pyee_Tax_Id_Nbr());                                            //Natural: ASSIGN #WK2-PYEE-TAX-ID-NBR := PAUDIT-PYEE-TAX-ID-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Typ().setValue(ldaIaal587.getAudit_View_Paudit_Pyee_Tax_Id_Typ());                                            //Natural: ASSIGN #WK2-PYEE-TAX-ID-TYP := PAUDIT-PYEE-TAX-ID-TYP
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Dob_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Dob_Dte());                                                            //Natural: ASSIGN #WK2-DOB-DTE := PAUDIT-DOB-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Dod_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Dod_Dte());                                                            //Natural: ASSIGN #WK2-DOD-DTE := PAUDIT-DOD-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Proof_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Proof_Dte());                                                        //Natural: ASSIGN #WK2-PROOF-DTE := PAUDIT-PROOF-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Lst_Pymnt_Dte());                                                //Natural: ASSIGN #WK2-LST-PYMNT-DTE := PAUDIT-LST-PYMNT-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Notify_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Notify_Dte());                                                      //Natural: ASSIGN #WK2-NOTIFY-DTE := PAUDIT-NOTIFY-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cycle_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Cycle_Dte());                                                        //Natural: ASSIGN #WK2-CYCLE-DTE := PAUDIT-CYCLE-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Acctg_Dte());                                                        //Natural: ASSIGN #WK2-ACCTG-DTE := PAUDIT-ACCTG-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte().setValue(ldaIaal587.getAudit_View_Paudit_Pymnt_Dte());                                                        //Natural: ASSIGN #WK2-PYMNT-DTE := PAUDIT-PYMNT-DTE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Type_Req_Ind().setValue(ldaIaal587.getAudit_View_Paudit_Type_Req_Ind());                                                  //Natural: ASSIGN #WK2-TYPE-REQ-IND := PAUDIT-TYPE-REQ-IND
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Type_Ind().setValue(ldaIaal587.getAudit_View_Paudit_Type_Ind());                                                          //Natural: ASSIGN #WK2-TYPE-IND := PAUDIT-TYPE-IND
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Roll_Dest_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Roll_Dest_Cde());                                                //Natural: ASSIGN #WK2-ROLL-DEST-CDE := PAUDIT-ROLL-DEST-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Eft_Acct_Nbr().setValue(ldaIaal587.getAudit_View_Paudit_Eft_Acct_Nbr());                                                  //Natural: ASSIGN #WK2-EFT-ACCT-NBR := PAUDIT-EFT-ACCT-NBR
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Eft_Transit_Id().setValue(ldaIaal587.getAudit_View_Paudit_Eft_Transit_Id());                                              //Natural: ASSIGN #WK2-EFT-TRANSIT-ID := PAUDIT-EFT-TRANSIT-ID
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Chk_Sav_Ind().setValue(ldaIaal587.getAudit_View_Paudit_Chk_Sav_Ind());                                                    //Natural: ASSIGN #WK2-CHK-SAV-IND := PAUDIT-CHK-SAV-IND
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnts().setValue(ldaIaal587.getPnd_Num());                                                                           //Natural: ASSIGN #WK2-INSTLLMNTS := #NUM
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Dte().getValue(1,":",                    //Natural: ASSIGN #WK2-INSTLLMNT-DTE ( * ) := PAUDIT-INSTLLMNT-DTE ( 1:99 )
            99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Typ().getValue(1).setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Typ().getValue(1));                        //Natural: ASSIGN #WK2-INSTLLMNT-TYP ( 1 ) := PAUDIT-INSTLLMNT-TYP ( 1 )
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Gross().getValue(1,                    //Natural: ASSIGN #WK2-INSTLLMNT-GROSS ( * ) := PAUDIT-INSTLLMNT-GROSS ( 1:99 )
            ":",99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Guar().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Guar().getValue(1,":",                  //Natural: ASSIGN #WK2-INSTLLMNT-GUAR ( * ) := PAUDIT-INSTLLMNT-GUAR ( 1:99 )
            99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Divd().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Divd().getValue(1,":",                  //Natural: ASSIGN #WK2-INSTLLMNT-DIVD ( * ) := PAUDIT-INSTLLMNT-DIVD ( 1:99 )
            99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ivc().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Ivc().getValue(1,":",                    //Natural: ASSIGN #WK2-INSTLLMNT-IVC ( * ) := PAUDIT-INSTLLMNT-IVC ( 1:99 )
            99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Dci().getValue(1,":",                    //Natural: ASSIGN #WK2-INSTLLMNT-DCI ( * ) := PAUDIT-INSTLLMNT-DCI ( 1:99 )
            99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Units().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Units().getValue(1,                    //Natural: ASSIGN #WK2-INSTLLMNT-UNITS ( * ) := PAUDIT-INSTLLMNT-UNITS ( 1:99 )
            ":",99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Ovrpymnt().getValue(1,              //Natural: ASSIGN #WK2-INSTLLMNT-OVRPYMNT ( * ) := PAUDIT-INSTLLMNT-OVRPYMNT ( 1:99 )
            ":",99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Futr_Ind().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Instllmnt_Futr_Ind().getValue(1,              //Natural: ASSIGN #WK2-INSTLLMNT-FUTR-IND ( * ) := PAUDIT-INSTLLMNT-FUTR-IND ( 1:99 )
            ":",99));
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_New_Acct_Ind().setValue(ldaIaal587.getAudit_View_Paudit_New_Acct_Ind());                                                  //Natural: ASSIGN #WK2-NEW-ACCT-IND := PAUDIT-NEW-ACCT-IND
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Ctznshp_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Ctznshp_Cde());                                                    //Natural: ASSIGN #WK2-CTZNSHP-CDE := PAUDIT-CTZNSHP-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Rsdncy_Cde());                                                      //Natural: ASSIGN #WK2-RSDNCY-CDE := PAUDIT-RSDNCY-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Locality_Cde().setValue(ldaIaal587.getAudit_View_Paudit_Locality_Cde());                                                  //Natural: ASSIGN #WK2-LOCALITY-CDE := PAUDIT-LOCALITY-CDE
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Rate_Basis().getValue("*"));                          //Natural: ASSIGN #WK2-RATE-BASIS ( * ) := PAUDIT-RATE-BASIS ( * )
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Per_Pymnt().getValue("*"));                            //Natural: ASSIGN #WK2-PER-PYMNT ( * ) := PAUDIT-PER-PYMNT ( * )
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Per_Dvdnd().getValue("*"));                            //Natural: ASSIGN #WK2-PER-DVDND ( * ) := PAUDIT-PER-DVDND ( * )
        gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Units().getValue("*").setValue(ldaIaal587.getAudit_View_Paudit_Per_Units().getValue("*"));                            //Natural: ASSIGN #WK2-PER-UNITS ( * ) := PAUDIT-PER-UNITS ( * )
        getWorkFiles().write(2, false, gdaIaag588r.getPnd_Work_Record2());                                                                                                //Natural: WRITE WORK FILE 2 #WORK-RECORD2
    }
    private void sub_Write_Work3() throws Exception                                                                                                                       //Natural: WRITE-WORK3
    {
        if (BLNatReinput.isReinput()) return;

        //*  WRITE '=' #TAB-GROSS-TOTAL(#J) '=' #J
        pnd_Work_Rec_3_Pnd_Pp_Gross_Total.setValue(pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_J()));                                                                  //Natural: MOVE #TAB-GROSS-TOTAL ( #J ) TO #PP-GROSS-TOTAL
        pnd_Work_Rec_3_Pnd_Pp_Check_Count.setValue(ldaIaal587.getPnd_Tab_Num_Check().getValue(ldaIaal587.getPnd_J()));                                                    //Natural: MOVE #TAB-NUM-CHECK ( #J ) TO #PP-CHECK-COUNT
        pnd_Work_Rec_3_Pnd_Pp_Check_Total.setValue(pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_J()));                                                                  //Natural: MOVE #TAB-CHECK-TOTAL ( #J ) TO #PP-CHECK-TOTAL
        pnd_Work_Rec_3_Pnd_Pp_Eft_Count.setValue(ldaIaal587.getPnd_Tab_Num_Eft().getValue(ldaIaal587.getPnd_J()));                                                        //Natural: MOVE #TAB-NUM-EFT ( #J ) TO #PP-EFT-COUNT
        pnd_Work_Rec_3_Pnd_Pp_Eft_Total.setValue(pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_J()));                                                                      //Natural: MOVE #TAB-EFT-TOTAL ( #J ) TO #PP-EFT-TOTAL
        getWorkFiles().write(3, false, pnd_Work_Rec_3);                                                                                                                   //Natural: WRITE WORK FILE 3 #WORK-REC-3
        pnd_Tab_Gross_Total.getValue(ldaIaal587.getPnd_J()).reset();                                                                                                      //Natural: RESET #TAB-GROSS-TOTAL ( #J ) #TAB-NUM-CHECK ( #J ) #TAB-CHECK-TOTAL ( #J ) #TAB-NUM-EFT ( #J ) #TAB-EFT-TOTAL ( #J )
        ldaIaal587.getPnd_Tab_Num_Check().getValue(ldaIaal587.getPnd_J()).reset();
        pnd_Tab_Check_Total.getValue(ldaIaal587.getPnd_J()).reset();
        ldaIaal587.getPnd_Tab_Num_Eft().getValue(ldaIaal587.getPnd_J()).reset();
        pnd_Tab_Eft_Total.getValue(ldaIaal587.getPnd_J()).reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 46T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 113T 'DATE ' *DATU
                        TabSetting(113),"DATE ",Global.getDATU());
                    //* *Y2NCTS
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(57),"DAILY PAYMENT REPORT (CONTINUED) ");                                                   //Natural: WRITE ( 1 ) 57T 'DAILY PAYMENT REPORT (CONTINUED) '
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 46T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 113T 'DATE ' *DATU
                        TabSetting(113),"DATE ",Global.getDATU());
                    //* *Y2NCTS
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(57),"DAILY PAYMENT REPORT (CONTINUED) ");                                                   //Natural: WRITE ( 2 ) 57T 'DAILY PAYMENT REPORT (CONTINUED) '
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
