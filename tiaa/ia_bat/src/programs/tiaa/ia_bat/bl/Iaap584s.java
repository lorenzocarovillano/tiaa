/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:03 PM
**        * FROM NATURAL PROGRAM : Iaap584s
************************************************************
**        * FILE NAME            : Iaap584s.java
**        * CLASS NAME           : Iaap584s
**        * INSTANCE NAME        : Iaap584s
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP584S      READ OVERPAYMENT FILE AND WRITE OUT   *
*      DATE   -  08/96         TO A SORT FILE TO GET RECORDS IN FUND *
*    AUTHOR   -  ARI G.        ORDER.                                *
*                                                                    *
*   HISTORY   -  10/17/2000    ADDED CONDITION OVRPYMNT-IND NOT = 'W'*
*                                                                    *
* 03/26/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE OVRPYMNT FILE.*
*                   SC 032610.
* 01/18/11  O SOTTO PROD FIX. SC 011811.
* JUN 2017 J BREMER       PIN EXPANSION RE-STWOW ONLY
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap584s extends BLNatBase
{
    // Data Areas
    private LdaIaal584 ldaIaal584;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cref_Fund;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_1;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_W_Fund_Code_Tot;

    private DbsGroup pnd_W_Fund_Code_Tot__R_Field_2;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2;

    private DbsGroup pnd_Work_File_2;
    private DbsField pnd_Work_File_2_Pnd_W2_Fund_Code;
    private DbsField pnd_Work_File_2_Pnd_W2_Ppcn_Nbr;
    private DbsField pnd_Work_File_2_Pnd_W2_Payee_Cde_A;
    private DbsField pnd_Work_File_2_Pnd_W2_Status_Timestamp;
    private DbsField pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Mo;
    private DbsField pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Yr;
    private DbsField pnd_Work_File_2_Pnd_W2_End_Fiscal_Mo;
    private DbsField pnd_Work_File_2_Pnd_W2_End_Fiscal_Yr;
    private DbsField pnd_Work_File_2_Pnd_W2_Nbr;
    private DbsField pnd_Work_File_2_Pnd_W2_Dcdnt_Per_Amt;
    private DbsField pnd_Work_File_2_Pnd_W2_Ovr_Per_Amt;
    private DbsField pnd_Work_File_2_Pnd_W2_Optn_Cde;
    private DbsField pnd_Work_File_2_Pnd_W2_Pymnt_Mode;
    private DbsField pnd_Work_File_2_Pnd_W2_Last_Pymnt_Rcvd_Date;
    private DbsField pnd_Work_File_2_Pnd_W2_Num_Ovrpy_Yrs;
    private DbsField pnd_Fund_Reads;
    private DbsField pnd_W2_Writes;
    private DbsField pnd_Datd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal584 = new LdaIaal584();
        registerRecord(ldaIaal584);
        registerRecord(ldaIaal584.getVw_iaa_Ovrpymnt_View());
        registerRecord(ldaIaal584.getVw_iaa_Cntrct_View());
        registerRecord(ldaIaal584.getVw_new_Tiaa_Rates());
        registerRecord(ldaIaal584.getVw_iaa_Old_Tiaa_Rates_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cref_Fund = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund", "IAA-CREF-FUND"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS");
        iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund.getRecord().newFieldInGroup("IAA_CREF_FUND_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        registerRecord(vw_iaa_Cref_Fund);

        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_W_Fund_Code_Tot = localVariables.newFieldInRecord("pnd_W_Fund_Code_Tot", "#W-FUND-CODE-TOT", FieldType.STRING, 3);

        pnd_W_Fund_Code_Tot__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Fund_Code_Tot__R_Field_2", "REDEFINE", pnd_W_Fund_Code_Tot);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1 = pnd_W_Fund_Code_Tot__R_Field_2.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2 = pnd_W_Fund_Code_Tot__R_Field_2.newFieldInGroup("pnd_W_Fund_Code_Tot_Pnd_W_Fund_Code_2", "#W-FUND-CODE-2", 
            FieldType.STRING, 2);

        pnd_Work_File_2 = localVariables.newGroupInRecord("pnd_Work_File_2", "#WORK-FILE-2");
        pnd_Work_File_2_Pnd_W2_Fund_Code = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Fund_Code", "#W2-FUND-CODE", FieldType.STRING, 3);
        pnd_Work_File_2_Pnd_W2_Ppcn_Nbr = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Ppcn_Nbr", "#W2-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_File_2_Pnd_W2_Payee_Cde_A = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Payee_Cde_A", "#W2-PAYEE-CDE-A", FieldType.STRING, 
            2);
        pnd_Work_File_2_Pnd_W2_Status_Timestamp = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Status_Timestamp", "#W2-STATUS-TIMESTAMP", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Mo = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Mo", "#W2-BEGIN-FISCAL-MO", 
            FieldType.NUMERIC, 2, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Yr = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Yr", "#W2-BEGIN-FISCAL-YR", 
            FieldType.NUMERIC, 4, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_End_Fiscal_Mo = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_End_Fiscal_Mo", "#W2-END-FISCAL-MO", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_End_Fiscal_Yr = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_End_Fiscal_Yr", "#W2-END-FISCAL-YR", FieldType.NUMERIC, 
            4, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_Nbr = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_Nbr", "#W2-NBR", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            7));
        pnd_Work_File_2_Pnd_W2_Dcdnt_Per_Amt = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_Dcdnt_Per_Amt", "#W2-DCDNT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_Ovr_Per_Amt = pnd_Work_File_2.newFieldArrayInGroup("pnd_Work_File_2_Pnd_W2_Ovr_Per_Amt", "#W2-OVR-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 7));
        pnd_Work_File_2_Pnd_W2_Optn_Cde = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Optn_Cde", "#W2-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Work_File_2_Pnd_W2_Pymnt_Mode = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Pymnt_Mode", "#W2-PYMNT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Work_File_2_Pnd_W2_Last_Pymnt_Rcvd_Date = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Last_Pymnt_Rcvd_Date", "#W2-LAST-PYMNT-RCVD-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_File_2_Pnd_W2_Num_Ovrpy_Yrs = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_W2_Num_Ovrpy_Yrs", "#W2-NUM-OVRPY-YRS", FieldType.NUMERIC, 
            2);
        pnd_Fund_Reads = localVariables.newFieldInRecord("pnd_Fund_Reads", "#FUND-READS", FieldType.NUMERIC, 9);
        pnd_W2_Writes = localVariables.newFieldInRecord("pnd_W2_Writes", "#W2-WRITES", FieldType.NUMERIC, 9);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cref_Fund.reset();

        ldaIaal584.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap584s() throws Exception
    {
        super("Iaap584s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().write(0, "*** START OF PROGRAM IAAP584S *** ");                                                                                                      //Natural: WRITE '*** START OF PROGRAM IAAP584S *** '
        if (Global.isEscape()) return;
        pnd_Work_File_2_Pnd_W2_Fund_Code.reset();                                                                                                                         //Natural: RESET #W2-FUND-CODE #W2-PPCN-NBR #W2-PAYEE-CDE-A #W2-STATUS-TIMESTAMP #W2-BEGIN-FISCAL-MO ( 1:7 ) #W2-BEGIN-FISCAL-YR ( 1:7 ) #W2-END-FISCAL-MO ( 1:7 ) #W2-END-FISCAL-YR ( 1:7 ) #W2-NBR ( 1:7 ) #W2-DCDNT-PER-AMT ( 1:7 ) #W2-OVR-PER-AMT ( 1:7 ) #W2-OPTN-CDE #W2-PYMNT-MODE #W2-LAST-PYMNT-RCVD-DATE #W2-NUM-OVRPY-YRS
        pnd_Work_File_2_Pnd_W2_Ppcn_Nbr.reset();
        pnd_Work_File_2_Pnd_W2_Payee_Cde_A.reset();
        pnd_Work_File_2_Pnd_W2_Status_Timestamp.reset();
        pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Mo.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Yr.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_End_Fiscal_Mo.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_End_Fiscal_Yr.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_Nbr.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_Dcdnt_Per_Amt.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_Ovr_Per_Amt.getValue(1,":",7).reset();
        pnd_Work_File_2_Pnd_W2_Optn_Cde.reset();
        pnd_Work_File_2_Pnd_W2_Pymnt_Mode.reset();
        pnd_Work_File_2_Pnd_W2_Last_Pymnt_Rcvd_Date.reset();
        pnd_Work_File_2_Pnd_W2_Num_Ovrpy_Yrs.reset();
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, ldaIaal584.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  START OF MAIN PROCESS
        //* ***********************************************************************
        //* *. READ  IAA-OVRPYMNT-VIEW PHYSICAL
        ldaIaal584.getVw_iaa_Ovrpymnt_View().startDatabaseRead                                                                                                            //Natural: READ IAA-OVRPYMNT-VIEW BY OVRPYMNT-STATUS-TIMESTAMP STARTING FROM #DATD
        (
        "RD",
        new Wc[] { new Wc("OVRPYMNT_STATUS_TIMESTAMP", ">=", pnd_Datd, WcType.BY) },
        new Oc[] { new Oc("OVRPYMNT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(ldaIaal584.getVw_iaa_Ovrpymnt_View().readNextRow("RD")))
        {
            ldaIaal584.getPnd_Fl_Date_Yyyymmdd_Alph().setValueEdited(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Status_Timestamp(),new ReportEditMask("YYYYMMDD"));         //Natural: MOVE EDITED OVRPYMNT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            if (condition(ldaIaal584.getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num().notEquals(ldaIaal584.getIaa_Parm_Card_Pnd_Parm_Date_N())))                   //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  032610 END
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.reset();                                                                                                                  //Natural: RESET #W-FUND-CODE
            ldaIaal584.getPnd_Ovrpymnt_Reads().nadd(1);                                                                                                                   //Natural: ADD 1 TO #OVRPYMNT-READS
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Ppcn_Nbr());                                                      //Natural: MOVE OVRPYMNT-PPCN-NBR TO #W-CNTRCT-PPCN-NBR
            pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Payee_Cde());                                                        //Natural: MOVE OVRPYMNT-PAYEE-CDE TO #W-CNTRCT-PAYEE
            vw_iaa_Cref_Fund.startDatabaseRead                                                                                                                            //Natural: READ IAA-CREF-FUND BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
            (
            "R1B",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
            );
            R1B:
            while (condition(vw_iaa_Cref_Fund.readNextRow("R1B")))
            {
                if (condition(iaa_Cref_Fund_Cref_Cntrct_Ppcn_Nbr.equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && iaa_Cref_Fund_Cref_Cntrct_Payee_Cde.equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-CREF-FUND.CREF-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-CREF-FUND.CREF-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
                {
                    pnd_Work_File_2_Pnd_W2_Fund_Code.setValue(iaa_Cref_Fund_Cref_Cmpny_Fund_Cde);                                                                         //Natural: MOVE IAA-CREF-FUND.CREF-CMPNY-FUND-CDE TO #W2-FUND-CODE
                    pnd_Fund_Reads.nadd(1);                                                                                                                               //Natural: ADD 1 TO #FUND-READS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) break R1B;                                                                                                                                  //Natural: ESCAPE BOTTOM ( R1B. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ACCEPT IF #W2-FUND-CODE NE ' '                      /* 011811
            //*  MOVE EDITED OVRPYMNT-STATUS-TIMESTAMP (EM=YYYYMMDD) /* 032610
            //*    TO #FL-DATE-YYYYMMDD-ALPH                         /* 032610
            //*  ACCEPT IF  #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N AND NOT /* 011811
            //*    (OVRPYMNT-IND = 'N'  OR  OVRPYMNT-IND =  'W')          /* 011811
            if (condition(!((pnd_Work_File_2_Pnd_W2_Fund_Code.notEquals(" ") && ! ((ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Ind().equals("N") || ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Ind().equals("W"))))))) //Natural: ACCEPT IF #W2-FUND-CODE NE ' ' AND NOT ( OVRPYMNT-IND = 'N' OR = 'W' )
            {
                continue;
            }
            ldaIaal584.getPnd_Ovrpymnt_Time_Selects().nadd(1);                                                                                                            //Natural: ADD 1 TO #OVRPYMNT-TIME-SELECTS
            //*      WRITE '=' OVRPYMNT-PPCN-NBR '=' #W-FUND-CODE-TOT
            pnd_Work_File_2_Pnd_W2_Ppcn_Nbr.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Ppcn_Nbr());                                                                //Natural: MOVE OVRPYMNT-PPCN-NBR TO #W2-PPCN-NBR
            pnd_Work_File_2_Pnd_W2_Payee_Cde_A.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Pnd_Payee_Cde_Alpha());                                                           //Natural: MOVE #PAYEE-CDE-ALPHA TO #W2-PAYEE-CDE-A
            pnd_Work_File_2_Pnd_W2_Status_Timestamp.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Status_Timestamp());                                                //Natural: MOVE OVRPYMNT-STATUS-TIMESTAMP TO #W2-STATUS-TIMESTAMP
            pnd_Work_File_2_Pnd_W2_Num_Ovrpy_Yrs.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Count_Castovrpymnt_Data());                                                     //Natural: MOVE C*OVRPYMNT-DATA TO #W2-NUM-OVRPY-YRS
            pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Mo.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Begin_Fiscal_Mo().getValue(1,":",                  //Natural: MOVE OVRPYMNT-BEGIN-FISCAL-MO ( 1:7 ) TO #W2-BEGIN-FISCAL-MO ( 1:7 )
                7));
            pnd_Work_File_2_Pnd_W2_Begin_Fiscal_Yr.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Begin_Fiscal_Yr().getValue(1,":",                  //Natural: MOVE OVRPYMNT-BEGIN-FISCAL-YR ( 1:7 ) TO #W2-BEGIN-FISCAL-YR ( 1:7 )
                7));
            pnd_Work_File_2_Pnd_W2_End_Fiscal_Mo.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_End_Fiscal_Mo().getValue(1,":",7));                  //Natural: MOVE OVRPYMNT-END-FISCAL-MO ( 1:7 ) TO #W2-END-FISCAL-MO ( 1:7 )
            pnd_Work_File_2_Pnd_W2_End_Fiscal_Yr.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_End_Fiscal_Yr().getValue(1,":",7));                  //Natural: MOVE OVRPYMNT-END-FISCAL-YR ( 1:7 ) TO #W2-END-FISCAL-YR ( 1:7 )
            pnd_Work_File_2_Pnd_W2_Nbr.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Nbr().getValue(1,":",7));                                      //Natural: MOVE OVRPYMNT-NBR ( 1:7 ) TO #W2-NBR ( 1:7 )
            pnd_Work_File_2_Pnd_W2_Dcdnt_Per_Amt.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Dcdnt_Per_Amt().getValue(1,":",7));                  //Natural: MOVE OVRPYMNT-DCDNT-PER-AMT ( 1:7 ) TO #W2-DCDNT-PER-AMT ( 1:7 )
            pnd_Work_File_2_Pnd_W2_Ovr_Per_Amt.getValue(1,":",7).setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Ovr_Per_Amt().getValue(1,":",7));                      //Natural: MOVE OVRPYMNT-OVR-PER-AMT ( 1:7 ) TO #W2-OVR-PER-AMT ( 1:7 )
            pnd_Work_File_2_Pnd_W2_Optn_Cde.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Optn_Cde());                                                                //Natural: MOVE OVRPYMNT-OPTN-CDE TO #W2-OPTN-CDE
            pnd_Work_File_2_Pnd_W2_Pymnt_Mode.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Pymnt_Mode());                                                            //Natural: MOVE OVRPYMNT-PYMNT-MODE TO #W2-PYMNT-MODE
            pnd_Work_File_2_Pnd_W2_Last_Pymnt_Rcvd_Date.setValue(ldaIaal584.getIaa_Ovrpymnt_View_Ovrpymnt_Last_Pymnt_Rcvd_Date());                                        //Natural: MOVE OVRPYMNT-LAST-PYMNT-RCVD-DATE TO #W2-LAST-PYMNT-RCVD-DATE
            //*   WRITE /// '=' #W2-FUND-CODE
            //*  /  '=' #W2-PPCN-NBR
            //*  /  '=' #W2-PAYEE-CDE-A
            //*  /  '=' #W2-STATUS-TIMESTAMP
            //*  /  '=' #W2-BEGIN-FISCAL-MO(1:7)
            //*  /  '=' #W2-BEGIN-FISCAL-YR(1:7)
            //*  /  '=' #W2-END-FISCAL-MO(1:7)
            //*  /  '=' #W2-END-FISCAL-YR(1:7)
            //*  /  '=' #W2-NBR(1:7)
            //*  /  '=' #W2-DCDNT-PER-AMT(1:7)
            //*  /  '=' #W2-OVR-PER-AMT(1:7)
            //*  /  '=' #W2-OPTN-CDE
            //*  /  '=' #W2-PYMNT-MODE
            //*  /  '=' #W2-LAST-PYMNT-RCVD-DATE
            //*  /  '=' #W2-NUM-OVRPY-YRS
            //*    IF #W2-PPCN-NBR = '0M313441  '
            //*      MOVE 'U09' TO #W2-FUND-CODE
            //*    END-IF
            getWorkFiles().write(2, false, pnd_Work_File_2);                                                                                                              //Natural: WRITE WORK FILE 2 #WORK-FILE-2
            pnd_W2_Writes.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #W2-WRITES
            pnd_Work_File_2.reset();                                                                                                                                      //Natural: RESET #WORK-FILE-2
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "===========  IAAP584S DISPLAYS  ======================",NEWLINE);                                                                          //Natural: WRITE '===========  IAAP584S DISPLAYS  ======================' /
        if (Global.isEscape()) return;
        getReports().write(0, "  OVERPAYMENT READS ==========> ",ldaIaal584.getPnd_Ovrpymnt_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE '  OVERPAYMENT READS ==========> ' #OVRPYMNT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  FUND READS =================> ",pnd_Fund_Reads, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE '  FUND READS =================> ' #FUND-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  OVERPAYMENT TIME SELECTS ===> ",ldaIaal584.getPnd_Ovrpymnt_Time_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE '  OVERPAYMENT TIME SELECTS ===> ' #OVRPYMNT-TIME-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK FILE WRITES ===========> ",pnd_W2_Writes, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                      //Natural: WRITE '  WORK FILE WRITES ===========> ' #W2-WRITES ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "**** END OF PROGRAM IAAP584S **** ");                                                                                                      //Natural: WRITE '**** END OF PROGRAM IAAP584S **** '
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal584.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                       //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal584.getIaa_Parm_Card_Pnd_Parm_Date());                                                           //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032610
            ldaIaal584.getIaa_Parm_Card_Pnd_Parm_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032610
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIaal584.getIaa_Parm_Card_Pnd_Parm_Date());                                                              //Natural: MOVE EDITED #PARM-DATE TO #DATD ( EM = YYYYMMDD )
    }

    //
}
