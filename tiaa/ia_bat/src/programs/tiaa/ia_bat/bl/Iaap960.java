/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:25 PM
**        * FROM NATURAL PROGRAM : Iaap960
************************************************************
**        * FILE NAME            : Iaap960.java
**        * CLASS NAME           : Iaap960
**        * INSTANCE NAME        : Iaap960
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP960    READS IAA CNTRL RECORD                 *
*      DATE     -  10/94      UPDATES CNTRL-ACTVTY-CDE TO BLANK      *
*                             MAKE CONTROL RECORD INACTIVE           *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap960 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt;
    private DbsField pnd_Check_Date_Ccyymmdd;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap960() throws Exception
    {
        super("Iaap960");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP960", onError);
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        //*  #CNTRL-CDE := 'AA'
        //*  COMPUTE #CNTRL-INVRSE-DTE = 100000000 - #PARM-CHECK-DTE-N
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Check_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
            getReports().write(0, ReportOption.NOTITLE,new ColumnSpacing(2),iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ColumnSpacing(9),iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte,      //Natural: WRITE ( 0 ) 2X IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 9X IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE ( EM = MM/DD/YY ) 1X IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE ( EM = HH:II:SS ) 4X IAA-CNTRL-RCRD.CNTRL-ACTVTY-CDE 4X '<--CONTROL RECORD BEFORE UPDATE '
                new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(1),iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte, new ReportEditMask ("HH:II:SS"),new ColumnSpacing(4),iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde,new 
                ColumnSpacing(4),"<--CONTROL RECORD BEFORE UPDATE ");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            GET01:                                                                                                                                                        //Natural: GET IAA-CNTRL-RCRD *ISN
            vw_iaa_Cntrl_Rcrd.readByID(vw_iaa_Cntrl_Rcrd.getAstISN("Read01"), "GET01");
            iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde.setValue(" ");                                                                                                                //Natural: ASSIGN IAA-CNTRL-RCRD.CNTRL-ACTVTY-CDE := ' '
            vw_iaa_Cntrl_Rcrd.updateDBRow("GET01");                                                                                                                       //Natural: UPDATE
            getReports().write(0, ReportOption.NOTITLE,new ColumnSpacing(2),iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ColumnSpacing(9),iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte,      //Natural: WRITE ( 0 ) 2X IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 9X IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE ( EM = MM/DD/YY ) 1X IAA-CNTRL-RCRD.CNTRL-FRST-TRANS-DTE ( EM = HH:II:SS ) 4X IAA-CNTRL-RCRD.CNTRL-ACTVTY-CDE 4X '<--CONTROL RECORD AFTER  UPDATE '
                new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(1),iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte, new ReportEditMask ("HH:II:SS"),new ColumnSpacing(4),iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde,new 
                ColumnSpacing(4),"<--CONTROL RECORD AFTER  UPDATE ");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(0, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMINISTRATION STATUS OF CONTROL RECORD INACTIVE        FOR CHECK DATE: ",iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new  //Natural: WRITE ( 0 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMINISTRATION STATUS OF CONTROL RECORD INACTIVE        FOR CHECK DATE: ' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 4X 'PAGE: ' #W-PAGE-CTR ( EM = Z9 )
                        ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("Z9"));
                    getReports().write(0, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out);                                                                                //Natural: WRITE ( 0 ) 'RUN DATE' #W-DATE-OUT
                    getReports().write(0, ReportOption.NOTITLE," CONTROL-CHECK     FIRST-TRANS-DATE  ACTIVITY                           ");                               //Natural: WRITE ( 0 ) ' CONTROL-CHECK     FIRST-TRANS-DATE  ACTIVITY                           '
                    getReports().write(0, ReportOption.NOTITLE,"    DATE            DATE     TIME    CODE           ","                                                      "); //Natural: WRITE ( 0 ) '    DATE            DATE     TIME    CODE           ' '                                                      '
                    getReports().write(0, ReportOption.NOTITLE,"--------------     -------- -------- --------       ","                                                      "); //Natural: WRITE ( 0 ) '--------------     -------- -------- --------       ' '                                                      '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE," ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                  //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
