/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:01 PM
**        * FROM NATURAL PROGRAM : Iatp367
************************************************************
**        * FILE NAME            : Iatp367.java
**        * CLASS NAME           : Iatp367
**        * INSTANCE NAME        : Iatp367
************************************************************
************************************************************************
* PROGRAM  : IATP367
* SYSTEM   : IA
* GENERATED: 11/07/1997
* AUTHOR   : LEN BERNSTEIN
* FUNCTION : THIS PROGRAM PRODUCES THE IA SWITCH REQUESTS NOT APPROVED
*          : BY STATE REPORT.
*
*    MAPS USED ARE IATM361H - HEADINGS, PAGE COUNT, DATE AND TIME
*                  IATM367I - DETAIL LINE HEADINGS
*                  IATM367D - DETAIL LINES
*
*    WORK FILE 1 :- ALL SWITCH RECORDS THAT WERE WRITTEN IN PROGRAM
*                   IATP360 TO WORK FILE 4 AND THEN SORTED.
*    WORK FILE 5 :- RUN DATE
*
* HISTORY
* -------
* MAY  1999     ADDED LOGIC TO CALL IATN36X TO PASS UNITS TO REPORT ON
*                REPORTS DONE BY RQST-UNIT-CDE TO REPORT BY WRK-AREA
*                DO SCAN ON 5/99
* 01/20/09  OS  TIAA ACCESS CHANGES. SC 012009.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* AUG 2017 R CARREON      RESTOWED CHANGES IN MAPS
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp367 extends BLNatBase
{
    // Data Areas
    private LdaIatl365 ldaIatl365;
    private PdaIaapda_M pdaIaapda_M;
    private PdaIata211 pdaIata211;
    private PdaIata201 pdaIata201;
    private PdaIata36x pdaIata36x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I2;

    private DbsGroup pnd_Iatn211_In;
    private DbsField pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Page_Cntrl;
    private DbsField pnd_I1;
    private DbsField pnd_Max_Acct;
    private DbsField pnd_Status_1_2;

    private DbsGroup pnd_Status_1_2__R_Field_1;
    private DbsField pnd_Status_1_2_Pnd_Status_1;
    private DbsField pnd_Report_Date_A8;

    private DbsGroup pnd_Report_Date_A8__R_Field_2;
    private DbsField pnd_Report_Date_A8_Pnd_Report_Date_N8;
    private DbsField pnd_Currnt_Dte;

    private DbsGroup pnd_Currnt_Dte__R_Field_3;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_A;

    private DbsGroup pnd_Currnt_Dte__R_Field_4;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd;
    private DbsField pnd_Rqst_Date;

    private DbsGroup pnd_Rqst_Date__R_Field_5;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_N;

    private DbsGroup pnd_Rqst_Date__R_Field_6;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Mm;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Dd;
    private DbsField pnd_Rqst_Entry_Date;

    private DbsGroup pnd_Rqst_Entry_Date__R_Field_7;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N;

    private DbsGroup pnd_Rqst_Entry_Date__R_Field_8;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_9;
    private DbsField pnd_Date_A_Pnd_Date_N;

    private DbsGroup pnd_Issue_Pnd_Rsdnce;
    private DbsField pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste;
    private DbsField pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_Debug_On;
    private DbsField pnd_L_Pnd_First_Rec;
    private DbsField pnd_L_Pnd_In_Detail;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Status_Await_Fact;
    private DbsField pnd_Const_Pnd_Status_Pend_Appl;
    private DbsField pnd_Const_Pnd_Status_Supvr_Ovrd;
    private DbsField pnd_Const_Pnd_Yes;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Cref;
    private DbsField pnd_Totals_Pnd_Rea;
    private DbsField pnd_Totals_Pnd_Access;
    private DbsField pnd_Totals_Pnd_Monthly;
    private DbsField pnd_Totals_Pnd_Annual;
    private DbsField pnd_Totals_Pnd_Contracts;
    private DbsField pnd_Totals_Pnd_Accounts;

    private DbsGroup pnd_Iatn204_Pda;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N;

    private DbsGroup pnd_Iatn204_Pda__R_Field_10;
    private DbsField pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A;
    private DbsField pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste;
    private DbsField pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste;
    private DbsField pnd_Iatn204_Pda_Pnd_Pend_Cde;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M361h_Busns_Dte;

    private DbsGroup pnd_M361h_Busns_Dte__R_Field_11;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy;
    private DbsField pnd_M361h_Page;
    private DbsField pnd_Program;

    private DbsGroup iatm367d_Dtl;
    private DbsField iatm367d_Dtl_Pnd_M367d_Rqst_Entry_Dte_Ot;
    private DbsField iatm367d_Dtl_Pnd_M367d_Rcvd_Dte_Ot;
    private DbsField iatm367d_Dtl_Pnd_M367d_Frm_Cntrct;
    private DbsField iatm367d_Dtl_Pnd_M367d_Frm_Fnd;
    private DbsField iatm367d_Dtl_Pnd_M367d_Frm_Mthd;
    private DbsField iatm367d_Dtl_Pnd_M367d_Frm_Qunty;
    private DbsField iatm367d_Dtl_Pnd_M367d_Frm_Typ;
    private DbsField iatm367d_Dtl_Pnd_M367d_Prtcpnt_Nme;
    private DbsField iatm367d_Dtl_Pnd_M367d_To_Fnd;
    private DbsField iatm367d_Dtl_Pnd_M367d_To_Mthd;
    private DbsField iatm367d_Dtl_Pnd_M367d_To_Qunty;
    private DbsField iatm367d_Dtl_Pnd_M367d_To_Typ;
    private DbsField iatm367d_Dtl_Pnd_M367d_Unique_Id;
    private DbsField iatm367d_Dtl_Pnd_M367d_Rsdnce_State;
    private DbsField iatm367d_Dtl_Pnd_M367d_Issue_State;
    private DbsField iatm367d_Dtl_Pnd_M367d_Status;
    private DbsField pnd_M367i_Cntct_Mthd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatl365 = new LdaIatl365();
        registerRecord(ldaIatl365);
        localVariables = new DbsRecord();
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        pdaIata211 = new PdaIata211(localVariables);
        pdaIata201 = new PdaIata201(localVariables);
        pdaIata36x = new PdaIata36x(localVariables);

        // Local Variables
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);

        pnd_Iatn211_In = localVariables.newGroupInRecord("pnd_Iatn211_In", "#IATN211-IN");
        pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte = pnd_Iatn211_In.newFieldInGroup("pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", FieldType.NUMERIC, 
            8);
        pnd_Page_Cntrl = localVariables.newFieldInRecord("pnd_Page_Cntrl", "#PAGE-CNTRL", FieldType.PACKED_DECIMAL, 3);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Acct = localVariables.newFieldInRecord("pnd_Max_Acct", "#MAX-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Status_1_2 = localVariables.newFieldInRecord("pnd_Status_1_2", "#STATUS-1-2", FieldType.STRING, 2);

        pnd_Status_1_2__R_Field_1 = localVariables.newGroupInRecord("pnd_Status_1_2__R_Field_1", "REDEFINE", pnd_Status_1_2);
        pnd_Status_1_2_Pnd_Status_1 = pnd_Status_1_2__R_Field_1.newFieldInGroup("pnd_Status_1_2_Pnd_Status_1", "#STATUS-1", FieldType.STRING, 1);
        pnd_Report_Date_A8 = localVariables.newFieldInRecord("pnd_Report_Date_A8", "#REPORT-DATE-A8", FieldType.STRING, 8);

        pnd_Report_Date_A8__R_Field_2 = localVariables.newGroupInRecord("pnd_Report_Date_A8__R_Field_2", "REDEFINE", pnd_Report_Date_A8);
        pnd_Report_Date_A8_Pnd_Report_Date_N8 = pnd_Report_Date_A8__R_Field_2.newFieldInGroup("pnd_Report_Date_A8_Pnd_Report_Date_N8", "#REPORT-DATE-N8", 
            FieldType.NUMERIC, 8);
        pnd_Currnt_Dte = localVariables.newFieldInRecord("pnd_Currnt_Dte", "#CURRNT-DTE", FieldType.NUMERIC, 8);

        pnd_Currnt_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_3", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_A = pnd_Currnt_Dte__R_Field_3.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_A", "#CURRNT-DTE-A", FieldType.STRING, 
            8);

        pnd_Currnt_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_4", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy", "#CURRNT-DTE-CY", FieldType.STRING, 
            4);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm", "#CURRNT-DTE-MM", FieldType.STRING, 
            2);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd", "#CURRNT-DTE-DD", FieldType.STRING, 
            2);
        pnd_Rqst_Date = localVariables.newFieldInRecord("pnd_Rqst_Date", "#RQST-DATE", FieldType.STRING, 8);

        pnd_Rqst_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_5", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_N = pnd_Rqst_Date__R_Field_5.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_N", "#RQST-DATE-N", FieldType.NUMERIC, 8);

        pnd_Rqst_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_6", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy = pnd_Rqst_Date__R_Field_6.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy", "#RQST-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Rqst_Date_Pnd_Rqst_Date_Mm = pnd_Rqst_Date__R_Field_6.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Mm", "#RQST-DATE-MM", FieldType.STRING, 
            2);
        pnd_Rqst_Date_Pnd_Rqst_Date_Dd = pnd_Rqst_Date__R_Field_6.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Dd", "#RQST-DATE-DD", FieldType.STRING, 
            2);
        pnd_Rqst_Entry_Date = localVariables.newFieldInRecord("pnd_Rqst_Entry_Date", "#RQST-ENTRY-DATE", FieldType.STRING, 8);

        pnd_Rqst_Entry_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Rqst_Entry_Date__R_Field_7", "REDEFINE", pnd_Rqst_Entry_Date);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N = pnd_Rqst_Entry_Date__R_Field_7.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N", "#RQST-ENTRY-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Rqst_Entry_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Rqst_Entry_Date__R_Field_8", "REDEFINE", pnd_Rqst_Entry_Date);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy = pnd_Rqst_Entry_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy", 
            "#RQST-ENTRY-DATE-YYYY", FieldType.STRING, 4);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm = pnd_Rqst_Entry_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm", "#RQST-ENTRY-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd = pnd_Rqst_Entry_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd", "#RQST-ENTRY-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_9", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_9.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);

        pnd_Issue_Pnd_Rsdnce = localVariables.newGroupInRecord("pnd_Issue_Pnd_Rsdnce", "#ISSUE-#RSDNCE");
        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste = pnd_Issue_Pnd_Rsdnce.newFieldInGroup("pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste", "#NUMERIC-STE", FieldType.STRING, 
            2);
        pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste = pnd_Issue_Pnd_Rsdnce.newFieldInGroup("pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste", "#ALPHA-STE", FieldType.STRING, 
            2);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_Debug_On = pnd_L.newFieldInGroup("pnd_L_Pnd_Debug_On", "#DEBUG-ON", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_First_Rec = pnd_L.newFieldInGroup("pnd_L_Pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_In_Detail = pnd_L.newFieldInGroup("pnd_L_Pnd_In_Detail", "#IN-DETAIL", FieldType.BOOLEAN, 1);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Status_Await_Fact = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Status_Await_Fact", "#STATUS-AWAIT-FACT", FieldType.STRING, 1);
        pnd_Const_Pnd_Status_Pend_Appl = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Status_Pend_Appl", "#STATUS-PEND-APPL", FieldType.STRING, 1);
        pnd_Const_Pnd_Status_Supvr_Ovrd = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Status_Supvr_Ovrd", "#STATUS-SUPVR-OVRD", FieldType.STRING, 1);
        pnd_Const_Pnd_Yes = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Yes", "#YES", FieldType.STRING, 1);

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Cref = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Cref", "#CREF", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Rea = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Rea", "#REA", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Access = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Access", "#ACCESS", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Monthly = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Monthly", "#MONTHLY", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Annual = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Annual", "#ANNUAL", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Contracts = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Contracts", "#CONTRACTS", FieldType.PACKED_DECIMAL, 6);
        pnd_Totals_Pnd_Accounts = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Accounts", "#ACCOUNTS", FieldType.PACKED_DECIMAL, 6);

        pnd_Iatn204_Pda = localVariables.newGroupInRecord("pnd_Iatn204_Pda", "#IATN204-PDA");
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N", "#IA-FRM-PAYEE-N", FieldType.NUMERIC, 
            2);

        pnd_Iatn204_Pda__R_Field_10 = pnd_Iatn204_Pda.newGroupInGroup("pnd_Iatn204_Pda__R_Field_10", "REDEFINE", pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_N);
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A = pnd_Iatn204_Pda__R_Field_10.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A", "#IA-FRM-PAYEE-A", FieldType.STRING, 
            2);
        pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste", "#CNTRCT-ISSUE-STE", FieldType.STRING, 
            3);
        pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste", "#CNTRCT-RSDNCE-STE", FieldType.STRING, 
            3);
        pnd_Iatn204_Pda_Pnd_Pend_Cde = pnd_Iatn204_Pda.newFieldInGroup("pnd_Iatn204_Pda_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M361h_Busns_Dte = localVariables.newFieldInRecord("pnd_M361h_Busns_Dte", "#M361H-BUSNS-DTE", FieldType.NUMERIC, 8);

        pnd_M361h_Busns_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_M361h_Busns_Dte__R_Field_11", "REDEFINE", pnd_M361h_Busns_Dte);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm = pnd_M361h_Busns_Dte__R_Field_11.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm", "#M361H-BUSNS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd = pnd_M361h_Busns_Dte__R_Field_11.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd", "#M361H-BUSNS-DTE-DD", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy = pnd_M361h_Busns_Dte__R_Field_11.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy", "#M361H-BUSNS-DTE-CY", 
            FieldType.STRING, 4);
        pnd_M361h_Page = localVariables.newFieldInRecord("pnd_M361h_Page", "#M361H-PAGE", FieldType.NUMERIC, 5);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        iatm367d_Dtl = localVariables.newGroupInRecord("iatm367d_Dtl", "IATM367D-DTL");
        iatm367d_Dtl_Pnd_M367d_Rqst_Entry_Dte_Ot = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Rqst_Entry_Dte_Ot", "#M367D-RQST-ENTRY-DTE-OT", 
            FieldType.STRING, 10);
        iatm367d_Dtl_Pnd_M367d_Rcvd_Dte_Ot = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Rcvd_Dte_Ot", "#M367D-RCVD-DTE-OT", FieldType.STRING, 
            10);
        iatm367d_Dtl_Pnd_M367d_Frm_Cntrct = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Frm_Cntrct", "#M367D-FRM-CNTRCT", FieldType.STRING, 10);
        iatm367d_Dtl_Pnd_M367d_Frm_Fnd = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Frm_Fnd", "#M367D-FRM-FND", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_Frm_Mthd = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Frm_Mthd", "#M367D-FRM-MTHD", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_Frm_Qunty = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Frm_Qunty", "#M367D-FRM-QUNTY", FieldType.NUMERIC, 11, 
            2);
        iatm367d_Dtl_Pnd_M367d_Frm_Typ = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Frm_Typ", "#M367D-FRM-TYP", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_Prtcpnt_Nme = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Prtcpnt_Nme", "#M367D-PRTCPNT-NME", FieldType.STRING, 
            30);
        iatm367d_Dtl_Pnd_M367d_To_Fnd = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_To_Fnd", "#M367D-TO-FND", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_To_Mthd = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_To_Mthd", "#M367D-TO-MTHD", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_To_Qunty = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_To_Qunty", "#M367D-TO-QUNTY", FieldType.NUMERIC, 11, 2);
        iatm367d_Dtl_Pnd_M367d_To_Typ = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_To_Typ", "#M367D-TO-TYP", FieldType.STRING, 1);
        iatm367d_Dtl_Pnd_M367d_Unique_Id = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Unique_Id", "#M367D-UNIQUE-ID", FieldType.STRING, 12);
        iatm367d_Dtl_Pnd_M367d_Rsdnce_State = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Rsdnce_State", "#M367D-RSDNCE-STATE", FieldType.STRING, 
            2);
        iatm367d_Dtl_Pnd_M367d_Issue_State = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Issue_State", "#M367D-ISSUE-STATE", FieldType.STRING, 
            2);
        iatm367d_Dtl_Pnd_M367d_Status = iatm367d_Dtl.newFieldInGroup("iatm367d_Dtl_Pnd_M367d_Status", "#M367D-STATUS", FieldType.STRING, 17);
        pnd_M367i_Cntct_Mthd = localVariables.newFieldInRecord("pnd_M367i_Cntct_Mthd", "#M367I-CNTCT-MTHD", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIatl365.initializeValues();

        localVariables.reset();
        pnd_Max_Acct.setInitialValue(20);
        pnd_L_Pnd_First_Rec.setInitialValue(true);
        pnd_L_Pnd_In_Detail.setInitialValue(true);
        pnd_Const_Pnd_Status_Await_Fact.setInitialValue("F");
        pnd_Const_Pnd_Status_Pend_Appl.setInitialValue("C");
        pnd_Const_Pnd_Status_Supvr_Ovrd.setInitialValue("D");
        pnd_Const_Pnd_Yes.setInitialValue("Y");
        pnd_Header1.setInitialValue("            IA REPORT FOR SWITCHES   ");
        pnd_Header2.setInitialValue("  SWITCH REQUESTS NOT YET APPROVED BY STATE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp367() throws Exception
    {
        super("Iatp367");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IATP367", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ======================================================================
        //*                     START OF MAIN PROGRAM LOGIC
        //* ======================================================================
        //*  #DEBUG-ON := TRUE
        //*  ZP=OFF SG=OFF
                                                                                                                                                                          //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: PERFORM INITIALIZATIONS
        sub_Initializations();
        if (condition(Global.isEscape())) {return;}
        //*  ADDED 5/99 GET WRK-AREA IDS
        DbsUtil.callnat(Iatn36x.class , getCurrentProcessState(), pdaIata36x.getIata36x());                                                                               //Natural: CALLNAT 'IATN36X' IATA36X
        if (condition(Global.isEscape())) return;
        //*  PAGE HEADINGS
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* **==> ADDED FOLLOWING LINES  5/99
        //* *
        R1:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_I2.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I2
            if (condition(pnd_I2.greater(pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids())))                                                                               //Natural: IF #I2 GT IATA36X-NBR-OF-UNIT-IDS
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //* * END OF ADD  5/99
            //*  READ WORK FILE
            boolean endOfDataRw1 = true;                                                                                                                                  //Natural: READ WORK FILE 1 IAT-SWITCH-RPT
            boolean firstRw1 = true;
            RW1:
            while (condition(getWorkFiles().read(1, ldaIatl365.getIat_Switch_Rpt())))
            {
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRw1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom()))
                            break;
                        else if (condition(Global.isEscapeBottomImmediate()))
                        {
                            endOfDataRw1 = false;
                            break;
                        }
                        else if (condition(Global.isEscapeTop()))
                        continue;
                        else if (condition())
                        return;
                    }
                }
                //* *
                //* ** ADDED FOLLOWING IF STMNTS. TO PROCESS ONLY EXISTING REQUESTS  5/99
                if (condition(DbsUtil.maskMatches(pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I2),"'ALL'")))                                                    //Natural: IF IATA36X-UNIT-CDE ( #I2 ) = MASK ( 'ALL' )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(!pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I2).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt()).equals(ldaIatl365.getIat_Switch_Rpt_Rqst_Unit_Cde().getSubstring(1, //Natural: IF SUBSTR ( IATA36X-UNIT-CDE ( #I2 ) ,1,IATA36X-LNGTH ) NE SUBSTR ( RQST-UNIT-CDE,1,IATA36X-LNGTH )
                        pdaIata36x.getIata36x_Iata36x_Lngth().getInt()))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ** END OF ADD  5/99
                pnd_Status_1_2.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                                     //Natural: ASSIGN #STATUS-1-2 := XFR-STTS-CDE
                //*  '2' = SWITCH
                if (condition(ldaIatl365.getIat_Switch_Rpt_Rcrd_Type_Cde().notEquals("2") || ! (pnd_Status_1_2_Pnd_Status_1.equals(pnd_Const_Pnd_Status_Await_Fact)       //Natural: REJECT IF RCRD-TYPE-CDE NE '2' OR NOT ( #STATUS-1 = #STATUS-AWAIT-FACT OR = #STATUS-PEND-APPL OR = #STATUS-SUPVR-OVRD )
                    || pnd_Status_1_2_Pnd_Status_1.equals(pnd_Const_Pnd_Status_Pend_Appl) || pnd_Status_1_2_Pnd_Status_1.equals(pnd_Const_Pnd_Status_Supvr_Ovrd))))
                {
                    continue;
                }
                if (condition(pnd_L_Pnd_Debug_On.getBoolean()))                                                                                                           //Natural: IF #DEBUG-ON
                {
                                                                                                                                                                          //Natural: PERFORM #WRITE-DEBUG-FIELDS
                    sub_Pnd_Write_Debug_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_L_Pnd_First_Rec.getBoolean()))                                                                                                          //Natural: IF #FIRST-REC
                {
                    pnd_L_Pnd_First_Rec.reset();                                                                                                                          //Natural: RESET #FIRST-REC
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MTHD
                    sub_Get_Contact_Mthd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: AT BREAK OF IAT-SWITCH-RPT.RQST-CNTCT-MDE
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                sub_Get_State();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CONVERT-STATE-FROM-N-TO-A
                sub_Convert_State_From_N_To_A();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-STATE-APPROVED
                sub_Check_If_State_Approved();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaIata211.getIata211_Pnd_O_Accept_Request().getValue(1).notEquals(pnd_Const_Pnd_Yes)))                                                     //Natural: IF IATA211.#O-ACCEPT-REQUEST ( 1 ) NE #YES
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-LINE
                    sub_Write_Report_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-WORK
            RW1_Exit:
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRw1(endOfDataRw1);
            }
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED 5/99
            //*  ADDED 5/99
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL #M361H-PAGE
            pnd_M361h_Page.reset();
            //*  ADDED 5/99
            //*  ADDED 5/99
            //*  ADDED 5/99
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_L_Pnd_In_Detail.setValue(true);                                                                                                                           //Natural: ASSIGN #IN-DETAIL := TRUE
            pnd_L_Pnd_First_Rec.setValue(true);                                                                                                                           //Natural: ASSIGN #FIRST-REC := TRUE
            pnd_Totals_Pnd_Cref.reset();                                                                                                                                  //Natural: RESET #CREF #I1 #REA #MONTHLY #ANNUAL #CONTRACTS #ACCOUNTS
            pnd_I1.reset();
            pnd_Totals_Pnd_Rea.reset();
            pnd_Totals_Pnd_Monthly.reset();
            pnd_Totals_Pnd_Annual.reset();
            pnd_Totals_Pnd_Contracts.reset();
            pnd_Totals_Pnd_Accounts.reset();
            //*  ADDED 5/99  TO REPORT ALL WRK-AREAS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ======================================================================
        //*                       START OF SUBROUTINES
        //* ======================================================================
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTACT-MTHD
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE
        //* *********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-STATE-FROM-N-TO-A
        //* ***********************************************************************
        //*  CONVERT ISSUE STATE FROM A NUMERIC VALUE TO AN ALPH VALUE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CALL-NAZN031
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-STATE-APPROVED
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-LINE
        //*  PIN AND CONTRACTS
        //*  SWITCH DETAILS AND NAME
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATUS-DESC
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACTS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-ACCOUNTS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-DEBUG-FIELDS
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        //*  INITIAL DETAIL SETUP
        //*  GET THE DATE FOR WHICH THIS REPORT MUST BE RUN
        getWorkFiles().read(5, pnd_Report_Date_A8);                                                                                                                       //Natural: READ WORK FILE 5 ONCE #REPORT-DATE-A8
        if (condition(pnd_Report_Date_A8.equals("99999999")))                                                                                                             //Natural: IF #REPORT-DATE-A8 = '99999999'
        {
            pnd_Currnt_Dte.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #CURRNT-DTE := *DATN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CHECK DATE FORMAT
            if (condition(DbsUtil.maskMatches(pnd_Report_Date_A8,"YYYYMMDD")))                                                                                            //Natural: IF #REPORT-DATE-A8 EQ MASK ( YYYYMMDD )
            {
                pnd_Currnt_Dte.setValue(pnd_Report_Date_A8_Pnd_Report_Date_N8);                                                                                           //Natural: ASSIGN #CURRNT-DTE := #REPORT-DATE-N8
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "* ================ ERROR ======================= *");                                                                              //Natural: WRITE '* ================ ERROR ======================= *'
                if (Global.isEscape()) return;
                getReports().write(0, "* The Requested Date for the Report is           *");                                                                              //Natural: WRITE '* The Requested Date for the Report is           *'
                if (Global.isEscape()) return;
                getReports().write(0, "* not in the format YYYYMMDD.                    *");                                                                              //Natural: WRITE '* not in the format YYYYMMDD.                    *'
                if (Global.isEscape()) return;
                getReports().write(0, "* Request Date = ",pnd_Report_Date_A8);                                                                                            //Natural: WRITE '* Request Date = ' #REPORT-DATE-A8
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-CY := #CURRNT-DTE-CY
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-MM := #CURRNT-DTE-MM
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-DD := #CURRNT-DTE-DD
        //*  INITIALIZATION
    }
    private void sub_Get_Contact_Mthd() throws Exception                                                                                                                  //Natural: GET-CONTACT-MTHD
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata201.getIata201_Pnd_Naz_Table_Key().setValue(ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde());                                                                //Natural: ASSIGN #NAZ-TABLE-KEY := IAT-SWITCH-RPT.RQST-CNTCT-MDE
        pdaIata201.getIata201_Pnd_Table_Code().setValue("CMDE");                                                                                                          //Natural: ASSIGN #TABLE-CODE := 'CMDE'
        DbsUtil.callnat(Iatn201.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pdaIata201.getIata201());                                                //Natural: CALLNAT 'IATN201' MSG-INFO-SUB IATA201
        if (condition(Global.isEscape())) return;
        if (condition(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
        {
            pnd_M367i_Cntct_Mthd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaIata201.getIata201_Pnd_Long_Desc()));                                        //Natural: COMPRESS #LONG-DESC INTO #M367I-CNTCT-MTHD LEAVING NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_M367i_Cntct_Mthd.setValue("CNTCT MTHD NOT ON FILE");                                                                                                      //Natural: ASSIGN #M367I-CNTCT-MTHD := 'CNTCT MTHD NOT ON FILE'
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTACT-MTHD
    }
    private void sub_Get_State() throws Exception                                                                                                                         //Natural: GET-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iatn204_Pda_Pnd_Ia_Frm_Cntrct.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct());                                                                         //Natural: ASSIGN #IATN204-PDA.#IA-FRM-CNTRCT := IA-FRM-CNTRCT
        pnd_Iatn204_Pda_Pnd_Ia_Frm_Payee_A.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Payee());                                                                         //Natural: ASSIGN #IATN204-PDA.#IA-FRM-PAYEE-A := IA-FRM-PAYEE
        DbsUtil.callnat(Iatn204.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Iatn204_Pda);                                                        //Natural: CALLNAT 'IATN204' MSG-INFO-SUB #IATN204-PDA
        if (condition(Global.isEscape())) return;
        //*  GET-STATE
    }
    private void sub_Convert_State_From_N_To_A() throws Exception                                                                                                         //Natural: CONVERT-STATE-FROM-N-TO-A
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste.setValue(pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste);                                                                              //Natural: ASSIGN #NUMERIC-STE := #CNTRCT-ISSUE-STE
                                                                                                                                                                          //Natural: PERFORM #CALL-NAZN031
        sub_Pnd_Call_Nazn031();
        if (condition(Global.isEscape())) {return;}
        pdaIata211.getIata211_Pnd_I_From_Issue_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                      //Natural: ASSIGN IATA211.#I-FROM-ISSUE-STATE := #ALPHA-STE
        if (condition(pnd_L_Pnd_Debug_On.getBoolean()))                                                                                                                   //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "=",pnd_Iatn204_Pda_Pnd_Cntrct_Issue_Ste,"=",pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                       //Natural: WRITE '=' #CNTRCT-ISSUE-STE '=' #ALPHA-STE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste.setValue(pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste);                                                                             //Natural: ASSIGN #NUMERIC-STE := #CNTRCT-RSDNCE-STE
                                                                                                                                                                          //Natural: PERFORM #CALL-NAZN031
        sub_Pnd_Call_Nazn031();
        if (condition(Global.isEscape())) {return;}
        pdaIata211.getIata211_Pnd_I_From_Residence_State().setValue(pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                                  //Natural: ASSIGN IATA211.#I-FROM-RESIDENCE-STATE := #ALPHA-STE
        if (condition(pnd_L_Pnd_Debug_On.getBoolean()))                                                                                                                   //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "=",pnd_Iatn204_Pda_Pnd_Cntrct_Rsdnce_Ste,"=",pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste);                                                      //Natural: WRITE '=' #CNTRCT-RSDNCE-STE '=' #ALPHA-STE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONVERT-STATE-FROM-N-TO-A
    }
    private void sub_Pnd_Call_Nazn031() throws Exception                                                                                                                  //Natural: #CALL-NAZN031
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CONVERT STATE FROM NUMERIC TO ALPHA
        pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste.reset();                                                                                                                       //Natural: RESET #ALPHA-STE
        DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Issue_Pnd_Rsdnce_Pnd_Alpha_Ste, pnd_Issue_Pnd_Rsdnce_Pnd_Numeric_Ste); //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
        if (condition(Global.isEscape())) return;
        //*  CONVERT-STATE-FROM-N-TO-A #CALL-NAZN031
    }
    //*  SWITCH
    private void sub_Check_If_State_Approved() throws Exception                                                                                                           //Natural: CHECK-IF-STATE-APPROVED
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata211.getIata211_Pnd_I_Transfer_Switch().setValue("S");                                                                                                      //Natural: ASSIGN IATA211.#I-TRANSFER-SWITCH := 'S'
        //*  REAL ESTATE
        if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(1,":",20).equals("R")))                                                                    //Natural: IF XFR-FRM-ACCT-CDE ( 1:20 ) EQ 'R'
        {
            pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue("R");                                                                                     //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := 'R'
            pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue("R");                                                                                               //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := 'R'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  012009 START
            if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(1,":",20).equals("D")))                                                                //Natural: IF XFR-FRM-ACCT-CDE ( 1:20 ) EQ 'D'
            {
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue("D");                                                                                 //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := 'D'
                pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue("D");                                                                                           //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := 'D'
                //*  012009 END
                //*  CREF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIata211.getIata211_Pnd_I_To_Transfer_Type().getValue(1).setValue("C");                                                                                 //Natural: ASSIGN IATA211.#I-TO-TRANSFER-TYPE ( 1 ) := 'C'
                pdaIata211.getIata211_Pnd_I_From_Transfer_Type().setValue("C");                                                                                           //Natural: ASSIGN IATA211.#I-FROM-TRANSFER-TYPE := 'C'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Iatn211_In_Pnd_Rqst_Effctv_Dte.setValue(pnd_Date_A_Pnd_Date_N);                                                                                               //Natural: ASSIGN #RQST-EFFCTV-DTE := #DATE-N
        DbsUtil.callnat(Iatn211.class , getCurrentProcessState(), pdaIata211.getIata211(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Iatn211_In);                                //Natural: CALLNAT 'IATN211' IATA211 MSG-INFO-SUB #IATN211-IN
        if (condition(Global.isEscape())) return;
        if (condition(pnd_L_Pnd_Debug_On.getBoolean()))                                                                                                                   //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "=",pdaIata211.getIata211_Pnd_O_Accept_Request().getValue(1));                                                                          //Natural: WRITE '=' #O-ACCEPT-REQUEST ( 1 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-STATE-APPROVED
    }
    private void sub_Write_Report_Line() throws Exception                                                                                                                 //Natural: WRITE-REPORT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSURE THE FULL REQUEST WILL FIT ON THE CURRENT PAGE
        if (condition(pnd_Page_Cntrl.greater(40)))                                                                                                                        //Natural: IF #PAGE-CNTRL > 40
        {
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET UP AND PRINT THE FIRST LINE OF THE REQUEST
        iatm367d_Dtl.reset();                                                                                                                                             //Natural: RESET IATM367D-DTL
        pnd_I1.setValue(1);                                                                                                                                               //Natural: ASSIGN #I1 = 1
        iatm367d_Dtl_Pnd_M367d_Unique_Id.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                                           //Natural: ASSIGN #M367D-UNIQUE-ID := IA-UNIQUE-ID
        iatm367d_Dtl_Pnd_M367d_Frm_Cntrct.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct());                                                                         //Natural: ASSIGN #M367D-FRM-CNTRCT := IA-FRM-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-STATUS-DESC
        sub_Get_Status_Desc();
        if (condition(Global.isEscape())) {return;}
        //*  RECEIVED DATE AND ENTRY DATE
        pnd_Rqst_Date.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED RQST-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-DATE
        iatm367d_Dtl_Pnd_M367d_Rcvd_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rqst_Date_Pnd_Rqst_Date_Mm, "/", pnd_Rqst_Date_Pnd_Rqst_Date_Dd,  //Natural: COMPRESS #RQST-DATE-MM '/' #RQST-DATE-DD '/' #RQST-DATE-YYYY INTO #M367D-RCVD-DTE-OT LEAVING NO SPACE
            "/", pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy));
        pnd_Rqst_Entry_Date.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Dte(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED RQST-ENTRY-DTE ( EM = YYYYMMDD ) TO #RQST-ENTRY-DATE
        iatm367d_Dtl_Pnd_M367d_Rqst_Entry_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm,                     //Natural: COMPRESS #RQST-ENTRY-DATE-MM '/' #RQST-ENTRY-DATE-DD '/' #RQST-ENTRY-DATE-YYYY INTO #M367D-RQST-ENTRY-DTE-OT LEAVING NO
            "/", pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd, "/", pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy));
        iatm367d_Dtl_Pnd_M367d_Frm_Fnd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I1));                                                        //Natural: ASSIGN #M367D-FRM-FND := XFR-FRM-ACCT-CDE ( #I1 )
        iatm367d_Dtl_Pnd_M367d_Frm_Qunty.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(pnd_I1));                                                           //Natural: ASSIGN #M367D-FRM-QUNTY := XFR-FRM-QTY ( #I1 )
        iatm367d_Dtl_Pnd_M367d_Frm_Typ.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(pnd_I1));                                                             //Natural: ASSIGN #M367D-FRM-TYP := XFR-FRM-TYP ( #I1 )
        iatm367d_Dtl_Pnd_M367d_Prtcpnt_Nme.setValue(ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme());                                                                          //Natural: ASSIGN #M367D-PRTCPNT-NME := PRTCPNT-NME
        iatm367d_Dtl_Pnd_M367d_Frm_Mthd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(pnd_I1));                                                       //Natural: ASSIGN #M367D-FRM-MTHD := XFR-FRM-UNIT-TYP ( #I1 )
        iatm367d_Dtl_Pnd_M367d_Issue_State.setValue(pdaIata211.getIata211_Pnd_I_From_Issue_State());                                                                      //Natural: ASSIGN #M367D-ISSUE-STATE := IATA211.#I-FROM-ISSUE-STATE
        iatm367d_Dtl_Pnd_M367d_Rsdnce_State.setValue(pdaIata211.getIata211_Pnd_I_From_Residence_State());                                                                 //Natural: ASSIGN #M367D-RSDNCE-STATE := IATA211.#I-FROM-RESIDENCE-STATE
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm367d.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM367D'
        pnd_Page_Cntrl.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CNTRL
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACTS
        sub_Add_Contracts();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-ACCOUNTS
        sub_Add_Accounts();
        if (condition(Global.isEscape())) {return;}
        //*  SET UP AND PRINT THE REMAINING LINE(S) OF THE REQUEST
        iatm367d_Dtl.reset();                                                                                                                                             //Natural: RESET IATM367D-DTL
        FOR01:                                                                                                                                                            //Natural: FOR #I1 2 TO #MAX-ACCT
        for (pnd_I1.setValue(2); condition(pnd_I1.lessOrEqual(pnd_Max_Acct)); pnd_I1.nadd(1))
        {
            if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I1).equals(" ")))                                                                  //Natural: IF XFR-FRM-ACCT-CDE ( #I1 ) = ' '
            {
                pnd_Max_Acct.setValue(21);                                                                                                                                //Natural: ASSIGN #MAX-ACCT := 21
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            iatm367d_Dtl_Pnd_M367d_Frm_Fnd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I1));                                                    //Natural: ASSIGN #M367D-FRM-FND := XFR-FRM-ACCT-CDE ( #I1 )
            iatm367d_Dtl_Pnd_M367d_Frm_Qunty.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(pnd_I1));                                                       //Natural: ASSIGN #M367D-FRM-QUNTY := XFR-FRM-QTY ( #I1 )
            iatm367d_Dtl_Pnd_M367d_Frm_Typ.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(pnd_I1));                                                         //Natural: ASSIGN #M367D-FRM-TYP := XFR-FRM-TYP ( #I1 )
            iatm367d_Dtl_Pnd_M367d_Frm_Mthd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(pnd_I1));                                                   //Natural: ASSIGN #M367D-FRM-MTHD := XFR-FRM-UNIT-TYP ( #I1 )
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm367d.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM367D'
            pnd_Page_Cntrl.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PAGE-CNTRL
                                                                                                                                                                          //Natural: PERFORM ADD-ACCOUNTS
            sub_Add_Accounts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-REPORT-LINE
    }
    private void sub_Get_Status_Desc() throws Exception                                                                                                                   //Natural: GET-STATUS-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata201.getIata201_Pnd_Naz_Table_Key().setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                  //Natural: ASSIGN #NAZ-TABLE-KEY := IAT-SWITCH-RPT.XFR-STTS-CDE
        pdaIata201.getIata201_Pnd_Table_Code().setValue("PSC");                                                                                                           //Natural: ASSIGN #TABLE-CODE := 'PSC'
        DbsUtil.callnat(Iatn201.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pdaIata201.getIata201());                                                //Natural: CALLNAT 'IATN201' MSG-INFO-SUB IATA201
        if (condition(Global.isEscape())) return;
        if (condition(pdaIaapda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
        {
            iatm367d_Dtl_Pnd_M367d_Status.setValue(pdaIata201.getIata201_Pnd_Short_Desc());                                                                               //Natural: ASSIGN #M367D-STATUS := #SHORT-DESC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde().notEquals(" ")))                                                                                    //Natural: IF IAT-SWITCH-RPT.XFR-STTS-CDE NE ' '
            {
                iatm367d_Dtl_Pnd_M367d_Status.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                      //Natural: ASSIGN #M367D-STATUS := IAT-SWITCH-RPT.XFR-STTS-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iatm367d_Dtl_Pnd_M367d_Status.setValue("NOT ON FILE");                                                                                                    //Natural: ASSIGN #M367D-STATUS := 'NOT ON FILE'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-STATUS-DESC
    }
    private void sub_Add_Contracts() throws Exception                                                                                                                     //Natural: ADD-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  REAL ESTATE
        if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I1).equals("R")))                                                                      //Natural: IF XFR-FRM-ACCT-CDE ( #I1 ) = 'R'
        {
            pnd_Totals_Pnd_Rea.nadd(1);                                                                                                                                   //Natural: ASSIGN #TOTALS.#REA := #TOTALS.#REA + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  012009 START
            if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I1).equals("D")))                                                                  //Natural: IF XFR-FRM-ACCT-CDE ( #I1 ) = 'D'
            {
                pnd_Totals_Pnd_Access.nadd(1);                                                                                                                            //Natural: ASSIGN #TOTALS.#ACCESS := #TOTALS.#ACCESS + 1
                //*  CREF                                        /* 012009 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Totals_Pnd_Cref.nadd(1);                                                                                                                              //Natural: ASSIGN #TOTALS.#CREF := #TOTALS.#CREF + 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-CONTRACTS
    }
    private void sub_Add_Accounts() throws Exception                                                                                                                      //Natural: ADD-ACCOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  ANNUAL
        if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(pnd_I1).equals("A")))                                                                      //Natural: IF XFR-FRM-UNIT-TYP ( #I1 ) = 'A'
        {
            pnd_Totals_Pnd_Annual.compute(new ComputeParameters(false, pnd_Totals_Pnd_Annual), pnd_Totals_Pnd_Annual.add(1));                                             //Natural: ASSIGN #TOTALS.#ANNUAL := #ANNUAL + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Totals_Pnd_Monthly.nadd(1);                                                                                                                               //Natural: ASSIGN #MONTHLY := #MONTHLY + 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-TO-TOTALS
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_L_Pnd_In_Detail.reset();                                                                                                                                      //Natural: RESET #IN-DETAIL
        //*  012009
        //*  012009
        //*  012009
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Totals_Pnd_Accounts.compute(new ComputeParameters(false, pnd_Totals_Pnd_Accounts), pnd_Totals_Pnd_Annual.add(pnd_Totals_Pnd_Monthly));                        //Natural: ASSIGN #TOTALS.#ACCOUNTS := #TOTALS.#ANNUAL + #TOTALS.#MONTHLY
        pnd_Totals_Pnd_Contracts.compute(new ComputeParameters(false, pnd_Totals_Pnd_Contracts), pnd_Totals_Pnd_Cref.add(pnd_Totals_Pnd_Rea).add(pnd_Totals_Pnd_Access)); //Natural: ASSIGN #TOTALS.#CONTRACTS := #TOTALS.#CREF + #TOTALS.#REA + #TOTALS.#ACCESS
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(30),"   Totals for Switch Requests not approved by State",NEWLINE,new  //Natural: WRITE ( 1 ) //// / 30X '   Totals for Switch Requests not approved by State' / 30X '-------------------------------------------------------' ///30X 'Total Switch Requests from Cref .............' #TOTALS.#CREF / 30X 'Total Switch Requests from Real Estate ......' #TOTALS.#REA / 30X 'Total Switch Requests from TIAA Access ......' #TOTALS.#ACCESS / 30X 'Total Contracts not approved by State .......' #TOTALS.#CONTRACTS // / 30X 'Total Monthly to Annual Switches ............' #TOTALS.#MONTHLY / 30X 'Total Annual to Monthly Switches ............' #TOTALS.#ANNUAL / 30X 'Total Accounts not approved by State ........' #TOTALS.#ACCOUNTS
            ColumnSpacing(30),"-------------------------------------------------------",NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(30),"Total Switch Requests from Cref .............",pnd_Totals_Pnd_Cref,NEWLINE,new 
            ColumnSpacing(30),"Total Switch Requests from Real Estate ......",pnd_Totals_Pnd_Rea,NEWLINE,new ColumnSpacing(30),"Total Switch Requests from TIAA Access ......",pnd_Totals_Pnd_Access,NEWLINE,new 
            ColumnSpacing(30),"Total Contracts not approved by State .......",pnd_Totals_Pnd_Contracts,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(30),"Total Monthly to Annual Switches ............",pnd_Totals_Pnd_Monthly,NEWLINE,new 
            ColumnSpacing(30),"Total Annual to Monthly Switches ............",pnd_Totals_Pnd_Annual,NEWLINE,new ColumnSpacing(30),"Total Accounts not approved by State ........",
            pnd_Totals_Pnd_Accounts);
        if (Global.isEscape()) return;
        if (condition(pnd_Totals_Pnd_Contracts.equals(getZero()) && pnd_Totals_Pnd_Accounts.equals(getZero())))                                                           //Natural: IF #TOTALS.#CONTRACTS EQ 0 AND #TOTALS.#ACCOUNTS EQ 0
        {
            getReports().write(1, ReportOption.NOTITLE," ",NEWLINE,NEWLINE,new ColumnSpacing(35),"                  ****************************************** ",NEWLINE,new  //Natural: WRITE ( 1 ) ' ' //35X'                  ****************************************** ' / 35X'                  NO DATA WAS SELECTED FOR THIS REPORT TODAY ' / 35X'                  ****************************************** '
                ColumnSpacing(35),"                  NO DATA WAS SELECTED FOR THIS REPORT TODAY ",NEWLINE,new ColumnSpacing(35),"                  ****************************************** ");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-TOTALS
    }
    private void sub_Pnd_Write_Debug_Fields() throws Exception                                                                                                            //Natural: #WRITE-DEBUG-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  WRITE THE FOLLOWING FIELDS WHEN DEBUG IS TRUE
        getReports().write(0, "************************  DEBUG ON IN PROG IATP367 *************","*****************");                                                    //Natural: WRITE '************************  DEBUG ON IN PROG IATP367 *************' '*****************'
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde(),"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde(),NEWLINE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Id(), //Natural: WRITE '=' RQST-CNTCT-MDE '=' XFR-STTS-CDE / '=' RQST-ID / '=' RQST-CNTCT-MDE '=' RCRD-TYPE-CDE / '=' XFR-FRM-ACCT-CDE ( * ) / '=' XFR-FRM-TYP ( * ) '=' XFR-FRM-QTY ( * ) / '=' XFR-FRM-UNIT-TYP ( * )
            NEWLINE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde(),"=",ldaIatl365.getIat_Switch_Rpt_Rcrd_Type_Cde(),NEWLINE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue("*"),
            NEWLINE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue("*"),"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue("*"),NEWLINE,"=",
            ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue("*"));
        if (Global.isEscape()) return;
        //*  #WRITE-DEBUG-FIELDS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_M361h_Page.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361H-PAGE
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm361h.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM361H'
                    //* * ADDED FOLLOWING    5/99
                    getReports().write(1, ReportOption.NOTITLE,"REPORT FOR UNIT:",pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I2));                             //Natural: WRITE ( 1 ) 'REPORT FOR UNIT:' IATA36X-UNIT-CDE ( #I2 )
                    //* * END OF ADD         5/99
                    if (condition(pnd_L_Pnd_In_Detail.getBoolean()))                                                                                                      //Natural: IF #IN-DETAIL
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm367i.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM367I'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DO UNTIL END OF ENTRIES IN PARM TABLE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct(),ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Payee()); //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' IA-FRM-CNTRCT IA-FRM-PAYEE
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventRw1() throws Exception {atBreakEventRw1(false);}
    private void atBreakEventRw1(boolean endOfData) throws Exception
    {
        boolean ldaIatl365_getIat_Switch_Rpt_Rqst_Cntct_MdeIsBreak = ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().isBreak(endOfData);
        if (condition(ldaIatl365_getIat_Switch_Rpt_Rqst_Cntct_MdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MTHD
            sub_Get_Contact_Mthd();
            if (condition(Global.isEscape())) {return;}
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
