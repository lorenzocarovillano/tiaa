/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:20 PM
**        * FROM NATURAL PROGRAM : Iaap386
************************************************************
**        * FILE NAME            : Iaap386.java
**        * CLASS NAME           : Iaap386
**        * INSTANCE NAME        : Iaap386
************************************************************
*********************************************************************
*
* THIS PROGRAM WILL READ THE WORK FILE WRITTEN BY PROGRAM IAAP384
* CONTAINING IAA-OLD-TIAA-RATES AND IAA-OLD-CREF-RATES RECORDS AND
* WRITE THEM OUT TO ADABAS FILE 205 ON DB 003.  BEFORE WRITING EACH
* RECORD IT WILL CHECK FOR THE EXISTENCE OF A DUPLICATE ONE ON THE
* ADABAS FILE.  IF A DUPLICATE EXISTS, THAT RECORD WILL NOT BE WRITTEN
* TO FILE 205.
* THE PROGRAM ALSO WILL GENERATE A SMALL REPORT SHOWING THE NUMBER
* OF RECORDS READ AND WRITTEN.
*
*********************  MAINTENANCE LOG ******************************
*
* DATE    PROGRAMMER   DESCRIPTION
*
* 10/97                LOGIC TO MOVE CREF PER-PMT & UNIT-VALUE INTO
*                      CREF FUND RECORDS
*
* 06/98   J.F.TINIO    POPULATE STATUS CODE AND SOURCE CODE FOR CREF
*                      HISTORICAL RATES RECORDS.
*
* 03/12   J.F.TINIO    RATE BASE EXPANSION TO 250 OCCURRENCES AND
*                      NEW FIELD LENGTH FOR UNITS-CNT.
*
*********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap386 extends BLNatBase
{
    // Data Areas
    private LdaIaal386 ldaIaal386;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Old_Cref_Rates_View;
    private DbsField iaa_Old_Cref_Rates_View_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Trans_User_Area;
    private DbsField iaa_Old_Cref_Rates_View_Trans_User_Id;
    private DbsField iaa_Old_Cref_Rates_View_Trans_Verify_Id;
    private DbsField iaa_Old_Cref_Rates_View_Trans_Verify_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_View_Rcrd_Srce;
    private DbsField iaa_Old_Cref_Rates_View_Rcrd_Status;
    private DbsField iaa_Old_Cref_Rates_View_Cmpny_Fund_Cde;

    private DbsGroup iaa_Old_Cref_Rates_View__R_Field_1;
    private DbsField iaa_Old_Cref_Rates_View_Cmpny_Cde;
    private DbsField iaa_Old_Cref_Rates_View_Fund_Cde;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Unit_Val;
    private DbsField iaa_Old_Cref_Rates_View_Cntrct_Tot_Units;
    private DbsField iaa_Old_Cref_Rates_View_Lst_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp;
    private DbsField iaa_Old_Cref_Rates_View_Cref_Rate_Cde;
    private DbsField iaa_Old_Cref_Rates_View_Cref_Rate_Dte;
    private DbsField iaa_Old_Cref_Rates_View_Cref_Per_Pay_Amt;
    private DbsField iaa_Old_Cref_Rates_View_Cref_No_Units;
    private DbsField iaa_Old_Cref_Rates_View_Cref_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_View_Cref_Old_Cmpny_Fund;

    private DbsGroup iaa_Tiaa_Cref_Old_Rates_Rec;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_B;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_C;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_D;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_E;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_F;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_G;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_H;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_I;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_J;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_K;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_L;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_M;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_N;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_O;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_P;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Part_Q;

    private DbsGroup iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_2;

    private DbsGroup iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte;

    private DbsGroup iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_3;

    private DbsGroup iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Unit_Val_A;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Units;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Cde;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Dte;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_No_Units;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Mode_Ind;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte_A;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_4;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Duplicate;
    private DbsField pnd_Total_Written;
    private DbsField pnd_Count;
    private DbsField pnd_Record_Length;
    private DbsField pnd_Duplicate_Rec;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal386 = new LdaIaal386();
        registerRecord(ldaIaal386);
        registerRecord(ldaIaal386.getVw_iaa_Old_Tiaa_Rates_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Old_Cref_Rates_View = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Cref_Rates_View", "IAA-OLD-CREF-RATES-VIEW"), "IAA_OLD_CREF_RATES", 
            "IA_OLD_RATES", DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_CREF_RATES"));
        iaa_Old_Cref_Rates_View_Fund_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Cref_Rates_View_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Cref_Rates_View_Trans_Dte = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Cref_Rates_View_Trans_User_Area = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Cref_Rates_View_Trans_User_Id = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Trans_User_Id", "TRANS-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Cref_Rates_View_Trans_Verify_Id = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Cref_Rates_View_Trans_Verify_Dte = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Trans_Verify_Dte", 
            "TRANS-VERIFY-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Cref_Rates_View_Cntrct_Ppcn_Nbr = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Cref_Rates_View_Cntrct_Payee_Cde = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Cref_Rates_View_Cntrct_Mode_Ind = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Cref_Rates_View_Rcrd_Srce = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Cref_Rates_View_Rcrd_Status = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Rcrd_Status", "RCRD-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Cref_Rates_View_Cmpny_Fund_Cde = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");

        iaa_Old_Cref_Rates_View__R_Field_1 = vw_iaa_Old_Cref_Rates_View.getRecord().newGroupInGroup("iaa_Old_Cref_Rates_View__R_Field_1", "REDEFINE", 
            iaa_Old_Cref_Rates_View_Cmpny_Fund_Cde);
        iaa_Old_Cref_Rates_View_Cmpny_Cde = iaa_Old_Cref_Rates_View__R_Field_1.newFieldInGroup("iaa_Old_Cref_Rates_View_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Old_Cref_Rates_View_Fund_Cde = iaa_Old_Cref_Rates_View__R_Field_1.newFieldInGroup("iaa_Old_Cref_Rates_View_Fund_Cde", "FUND-CDE", FieldType.STRING, 
            2);
        iaa_Old_Cref_Rates_View_Cntrct_Tot_Per_Amt = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cntrct_Tot_Per_Amt", 
            "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Cref_Rates_View_Cntrct_Unit_Val = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("IAA_OLD_CREF_RATES_VIEW_CNTRCT_UNIT_VAL", "CNTRCT-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Cref_Rates_View_Cntrct_Tot_Units = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Cntrct_Tot_Units", 
            "CNTRCT-TOT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, RepeatingFieldStrategy.None, "CNTRCT_TOT_UNITS");
        iaa_Old_Cref_Rates_View_Lst_Trans_Dte = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Cref_Rates_View_Count_Castcref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_View_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_CREF_RATE_DATA_GRP");

        iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates_View.getRecord().newGroupInGroup("iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp", 
            "CREF-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_View_Cref_Rate_Cde = iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_RATE_CDE", 
            "CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_View_Cref_Rate_Dte = iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_RATE_DTE", 
            "CREF-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_View_Cref_Per_Pay_Amt = iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_PER_PAY_AMT", 
            "CREF-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_View_Cref_No_Units = iaa_Old_Cref_Rates_View_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_NO_UNITS", 
            "CREF-NO-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_NO_UNITS", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_View_Cref_Mode_Ind = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_MODE_IND", "CREF-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Old_Cref_Rates_View_Cref_Old_Cmpny_Fund = vw_iaa_Old_Cref_Rates_View.getRecord().newFieldInGroup("IAA_OLD_CREF_RATES_VIEW_CREF_OLD_CMPNY_FUND", 
            "CREF-OLD-CMPNY-FUND", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        registerRecord(vw_iaa_Old_Cref_Rates_View);

        iaa_Tiaa_Cref_Old_Rates_Rec = localVariables.newGroupInRecord("iaa_Tiaa_Cref_Old_Rates_Rec", "IAA-TIAA-CREF-OLD-RATES-REC");
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_A = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_A", "PART-A", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_B = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_B", "PART-B", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_C = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_C", "PART-C", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_D = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_D", "PART-D", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_E = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_E", "PART-E", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_F = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_F", "PART-F", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_G = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_G", "PART-G", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_H = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_H", "PART-H", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_I = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_I", "PART-I", FieldType.STRING, 
            253);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_J = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_J", "PART-J", FieldType.STRING, 
            174);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_K = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_K", "PART-K", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_L = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_L", "PART-L", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_M = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_M", "PART-M", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_N = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_N", "PART-N", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_O = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_O", "PART-O", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_P = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_P", "PART-P", FieldType.STRING, 
            1000);
        iaa_Tiaa_Cref_Old_Rates_Rec_Part_Q = iaa_Tiaa_Cref_Old_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Part_Q", "PART-Q", FieldType.STRING, 
            891);

        iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_2 = localVariables.newGroupInRecord("iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_2", "REDEFINE", iaa_Tiaa_Cref_Old_Rates_Rec);

        iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec = iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_2.newGroupInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec", 
            "IAA-OLD-TIAA-RATES-REC");
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte", 
            "FUND-LST-PD-DTE", FieldType.DATE);
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte", 
            "TRANS-DTE", FieldType.TIME);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area", 
            "TRANS-USER-AREA", FieldType.STRING, 6);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id", 
            "TRANS-USER-ID", FieldType.STRING, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id", 
            "TRANS-VERIFY-ID", FieldType.STRING, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte", 
            "TRANS-VERIFY-DTE", FieldType.TIME);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind", 
            "CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde", 
            "CMPNY-FUND-CDE", FieldType.STRING, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce", 
            "RCRD-SRCE", FieldType.STRING, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status", 
            "RCRD-STATUS", FieldType.STRING, 1);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt", 
            "CNTRCT-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Div_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Div_Amt", 
            "CNTRCT-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Cde = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Cde", 
            "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Dte", 
            "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Pay_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Pay_Amt", 
            "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Div_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Per_Div_Amt", 
            "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Gic = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Rate_Gic", 
            "TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1, 250));
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Mode_Ind = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Mode_Ind", 
            "TIAA-MODE-IND", FieldType.NUMERIC, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Old_Cmpny_Fund = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Tiaa_Old_Cmpny_Fund", 
            "TIAA-OLD-CMPNY-FUND", FieldType.STRING, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte", 
            "LST-TRANS-DTE", FieldType.TIME);

        iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_3 = localVariables.newGroupInRecord("iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_3", "REDEFINE", iaa_Tiaa_Cref_Old_Rates_Rec);

        iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec = iaa_Tiaa_Cref_Old_Rates_Rec__R_Field_3.newGroupInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec", 
            "IAA-OLD-CREF-RATES-REC");
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte_A", 
            "FUND-LST-PD-DTE-A", FieldType.DATE);
        iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A", 
            "FUND-INVRSE-LST-PD-DTE-A", FieldType.NUMERIC, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte_A", 
            "TRANS-DTE-A", FieldType.TIME);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Area_A", 
            "TRANS-USER-AREA-A", FieldType.STRING, 6);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_User_Id_A", 
            "TRANS-USER-ID-A", FieldType.STRING, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id_A", 
            "TRANS-VERIFY-ID-A", FieldType.STRING, 8);
        iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte_A", 
            "TRANS-VERIFY-DTE-A", FieldType.TIME);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A", 
            "CNTRCT-PPCN-NBR-A", FieldType.STRING, 10);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A", 
            "CNTRCT-PAYEE-CDE-A", FieldType.NUMERIC, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind_A", 
            "CNTRCT-MODE-IND-A", FieldType.NUMERIC, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A", 
            "CMPNY-FUND-CDE-A", FieldType.STRING, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce_A", 
            "RCRD-SRCE-A", FieldType.STRING, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status_A", 
            "RCRD-STATUS-A", FieldType.STRING, 1);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt_A", 
            "CNTRCT-TOT-PER-AMT-A", FieldType.PACKED_DECIMAL, 9, 2);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Unit_Val_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Unit_Val_A", 
            "CNTRCT-UNIT-VAL-A", FieldType.PACKED_DECIMAL, 9, 4);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Units = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Units", 
            "CNTRCT-TOT-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Cde = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Cde", 
            "CREF-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 15));
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Dte = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Dte", 
            "CREF-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 15));
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Per_Pay_Amt = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Per_Pay_Amt", 
            "CREF-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 15));
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_No_Units = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldArrayInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_No_Units", 
            "CREF-NO-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15));
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Mode_Ind = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Mode_Ind", 
            "CREF-MODE-IND", FieldType.NUMERIC, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Old_Cmpny_Fund = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Old_Cmpny_Fund", 
            "CREF-OLD-CMPNY-FUND", FieldType.STRING, 3);
        iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte_A = iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Cref_Rates_Rec.newFieldInGroup("iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte_A", 
            "LST-TRANS-DTE-A", FieldType.TIME);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte", 
            "#FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", 
            FieldType.STRING, 3);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.INTEGER, 4);
        pnd_Total_Duplicate = localVariables.newFieldInRecord("pnd_Total_Duplicate", "#TOTAL-DUPLICATE", FieldType.INTEGER, 4);
        pnd_Total_Written = localVariables.newFieldInRecord("pnd_Total_Written", "#TOTAL-WRITTEN", FieldType.INTEGER, 4);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.INTEGER, 4);
        pnd_Record_Length = localVariables.newFieldInRecord("pnd_Record_Length", "#RECORD-LENGTH", FieldType.INTEGER, 4);
        pnd_Duplicate_Rec = localVariables.newFieldInRecord("pnd_Duplicate_Rec", "#DUPLICATE-REC", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Old_Cref_Rates_View.reset();

        ldaIaal386.initializeValues();

        localVariables.reset();
        pnd_Cntrct_Py_Dte_Key.setInitialValue(" ");
        pnd_Duplicate_Rec.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap386() throws Exception
    {
        super("Iaap386");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD IAA-OLD-TIAA-RATES-REC GIVING LENGTH #RECORD-LENGTH
        while (condition(getWorkFiles().read(1, iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec)))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            pnd_Duplicate_Rec.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #DUPLICATE-REC
                                                                                                                                                                          //Natural: PERFORM CHECK-DUPLICATE-RECS-ON-RATES-FILE
            sub_Check_Duplicate_Recs_On_Rates_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Duplicate_Rec.getBoolean()))                                                                                                                //Natural: IF #DUPLICATE-REC
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #RECORD-LENGTH GT 335  /* 3/12 + 2-BYTES
            //*  3/12
            if (condition(pnd_Record_Length.greater(337)))                                                                                                                //Natural: IF #RECORD-LENGTH GT 337
            {
                ldaIaal386.getVw_iaa_Old_Tiaa_Rates_View().setValuesByName(iaa_Tiaa_Cref_Old_Rates_Rec_Iaa_Old_Tiaa_Rates_Rec);                                           //Natural: MOVE BY NAME IAA-OLD-TIAA-RATES-REC TO IAA-OLD-TIAA-RATES-VIEW
                ldaIaal386.getVw_iaa_Old_Tiaa_Rates_View().insertDBRow();                                                                                                 //Natural: STORE IAA-OLD-TIAA-RATES-VIEW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM FILL-OLD-CREF-RATES-REC-FIELDS
                sub_Fill_Old_Cref_Rates_Rec_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                vw_iaa_Old_Cref_Rates_View.insertDBRow();                                                                                                                 //Natural: STORE IAA-OLD-CREF-RATES-VIEW
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Written.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-WRITTEN
            if (condition(pnd_Count.greaterOrEqual(50)))                                                                                                                  //Natural: IF #COUNT >= 50
            {
                pnd_Count.reset();                                                                                                                                        //Natural: RESET #COUNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Count.greater(getZero())))                                                                                                                      //Natural: IF #COUNT > 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Total_Duplicate.equals(getZero())))                                                                                                             //Natural: IF #TOTAL-DUPLICATE = 0
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"NO DUPLICATE RECORDS ON FILE");                                                           //Natural: WRITE ( 2 ) NOTITLE /// 'NO DUPLICATE RECORDS ON FILE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iaam386.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IAAM386'
        //* ***************************************************
        //* ***********************************************
        //*  END OF ADD        10/97
        //*  ADDED BY JFT TO POPULATE STATUS CODE AND SOURCE CODE -- 06/98
        //*  END OF ADDED CODE                                    -- 06/98
        //*  ADDED BY JFT TO POPULATE TRANS-VERIFY-ID,            -- 11/98
        //*  TRANS-VERIFY-DTE, MODE-IND, TRANS-DTE, LST-TRANS-DTE -- 11/98
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
    }
    private void sub_Check_Duplicate_Recs_On_Rates_File() throws Exception                                                                                                //Natural: CHECK-DUPLICATE-RECS-ON-RATES-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************
        //* *IF #RECORD-LENGTH GT 335  /* 3/12
        //*  3/12
        if (condition(pnd_Record_Length.greater(337)))                                                                                                                    //Natural: IF #RECORD-LENGTH GT 337
        {
            pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr);                                                              //Natural: ASSIGN #CNTRCT-PPCN-NBR = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR
            pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde);                                                            //Natural: ASSIGN #CNTRCT-PAYEE-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE
            pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte);                                                //Natural: ASSIGN #FUND-INVRSE-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE
            pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde);                                                                //Natural: ASSIGN #CMPNY-FUND-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE
            ldaIaal386.getVw_iaa_Old_Tiaa_Rates_View().getTotalRowCount                                                                                                   //Natural: FIND NUMBER IAA-OLD-TIAA-RATES-VIEW WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
            (
            new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) }
            );
            if (condition(ldaIaal386.getVw_iaa_Old_Tiaa_Rates_View().getAstNUMBER().greater(getZero())))                                                                  //Natural: IF *NUMBER ( ##L1990. ) > 0
            {
                pnd_Total_Duplicate.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-DUPLICATE
                pnd_Duplicate_Rec.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #DUPLICATE-REC
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam386c.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM386C'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A);                                                            //Natural: ASSIGN #CNTRCT-PPCN-NBR = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR-A
            pnd_Cntrct_Py_Dte_Key_Pnd_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A);                                                          //Natural: ASSIGN #CNTRCT-PAYEE-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE-A
            pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A);                                              //Natural: ASSIGN #FUND-INVRSE-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE-A
            pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A);                                                              //Natural: ASSIGN #CMPNY-FUND-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE-A
            vw_iaa_Old_Cref_Rates_View.getTotalRowCount                                                                                                                   //Natural: FIND NUMBER IAA-OLD-CREF-RATES-VIEW WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
            (
            new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) }
            );
            if (condition(vw_iaa_Old_Cref_Rates_View.getAstNUMBER().greater(getZero())))                                                                                  //Natural: IF *NUMBER ( ##L2160. ) > 0
            {
                pnd_Total_Duplicate.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-DUPLICATE
                pnd_Duplicate_Rec.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #DUPLICATE-REC
                iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A);                                                      //Natural: ASSIGN IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR-A
                iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A);                                                    //Natural: ASSIGN IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE-A
                iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A);                                        //Natural: ASSIGN IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE-A
                iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A);                                                        //Natural: ASSIGN IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE-A
                iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte_A);                                                      //Natural: ASSIGN IAA-TIAA-CREF-OLD-RATES-REC.FUND-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-LST-PD-DTE-A
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam386c.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM386C'
                Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                   //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fill_Old_Cref_Rates_Rec_Fields() throws Exception                                                                                                    //Natural: FILL-OLD-CREF-RATES-REC-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        iaa_Old_Cref_Rates_View_Fund_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Lst_Pd_Dte_A);                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.FUND-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-LST-PD-DTE-A
        iaa_Old_Cref_Rates_View_Fund_Invrse_Lst_Pd_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Fund_Invrse_Lst_Pd_Dte_A);                                                    //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.FUND-INVRSE-LST-PD-DTE = IAA-TIAA-CREF-OLD-RATES-REC.FUND-INVRSE-LST-PD-DTE-A
        iaa_Old_Cref_Rates_View_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Ppcn_Nbr_A);                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-PPCN-NBR = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PPCN-NBR-A
        iaa_Old_Cref_Rates_View_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Payee_Cde_A);                                                                //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-PAYEE-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-PAYEE-CDE-A
        iaa_Old_Cref_Rates_View_Cmpny_Fund_Cde.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cmpny_Fund_Cde_A);                                                                    //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CMPNY-FUND-CDE = IAA-TIAA-CREF-OLD-RATES-REC.CMPNY-FUND-CDE-A
        iaa_Old_Cref_Rates_View_Cref_Rate_Cde.getValue("*").setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Cde.getValue("*"));                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CREF-RATE-CDE ( * ) = IAA-TIAA-CREF-OLD-RATES-REC.CREF-RATE-CDE ( * )
        iaa_Old_Cref_Rates_View_Cref_Rate_Dte.getValue("*").setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Rate_Dte.getValue("*"));                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CREF-RATE-DTE ( * ) = IAA-TIAA-CREF-OLD-RATES-REC.CREF-RATE-DTE ( * )
        iaa_Old_Cref_Rates_View_Cref_No_Units.getValue("*").setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_No_Units.getValue("*"));                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CREF-NO-UNITS ( * ) = IAA-TIAA-CREF-OLD-RATES-REC.CREF-NO-UNITS ( * )
        iaa_Old_Cref_Rates_View_Cntrct_Tot_Units.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_No_Units.getValue(1));                                                         //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-TOT-UNITS = IAA-TIAA-CREF-OLD-RATES-REC.CREF-NO-UNITS ( 1 )
        //*  ADDED FOLLOWING   10/97
        iaa_Old_Cref_Rates_View_Cntrct_Tot_Per_Amt.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Tot_Per_Amt_A);                                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-TOT-PER-AMT = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-TOT-PER-AMT-A
        //*  3/12
        //*  3/12
        //*  3/12
        //*  3/12
        iaa_Old_Cref_Rates_View_Cntrct_Unit_Val.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Unit_Val_A);                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-UNIT-VAL = IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-UNIT-VAL-A
        iaa_Old_Cref_Rates_View_Rcrd_Srce.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Srce);                                                                                //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.RCRD-SRCE := IAA-TIAA-CREF-OLD-RATES-REC.RCRD-SRCE
        iaa_Old_Cref_Rates_View_Rcrd_Status.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Rcrd_Status);                                                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.RCRD-STATUS := IAA-TIAA-CREF-OLD-RATES-REC.RCRD-STATUS
        iaa_Old_Cref_Rates_View_Trans_Verify_Id.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Id);                                                                    //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.TRANS-VERIFY-ID := IAA-TIAA-CREF-OLD-RATES-REC.TRANS-VERIFY-ID
        iaa_Old_Cref_Rates_View_Trans_Verify_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Verify_Dte);                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.TRANS-VERIFY-DTE := IAA-TIAA-CREF-OLD-RATES-REC.TRANS-VERIFY-DTE
        iaa_Old_Cref_Rates_View_Trans_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Trans_Dte);                                                                                //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.TRANS-DTE := IAA-TIAA-CREF-OLD-RATES-REC.TRANS-DTE
        iaa_Old_Cref_Rates_View_Lst_Trans_Dte.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Lst_Trans_Dte_A);                                                                      //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.LST-TRANS-DTE := IAA-TIAA-CREF-OLD-RATES-REC.LST-TRANS-DTE-A
        iaa_Old_Cref_Rates_View_Cntrct_Mode_Ind.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cntrct_Mode_Ind);                                                                    //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CNTRCT-MODE-IND := IAA-TIAA-CREF-OLD-RATES-REC.CNTRCT-MODE-IND
        iaa_Old_Cref_Rates_View_Cref_Mode_Ind.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Mode_Ind);                                                                        //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CREF-MODE-IND := IAA-TIAA-CREF-OLD-RATES-REC.CREF-MODE-IND
        iaa_Old_Cref_Rates_View_Cref_Old_Cmpny_Fund.setValue(iaa_Tiaa_Cref_Old_Rates_Rec_Cref_Old_Cmpny_Fund);                                                            //Natural: ASSIGN IAA-OLD-CREF-RATES-VIEW.CREF-OLD-CMPNY-FUND := IAA-TIAA-CREF-OLD-RATES-REC.CREF-OLD-CMPNY-FUND
        //*  END OF ADDED CODE                                    -- 11/98
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam386a.class));                                                                  //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM386A'
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Iaam386b.class));                                                                  //Natural: WRITE ( 2 ) NOTITLE USING FORM 'IAAM386B'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
