/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:44 PM
**        * FROM NATURAL PROGRAM : Iatp180
************************************************************
**        * FILE NAME            : Iatp180.java
**        * CLASS NAME           : Iatp180
**        * INSTANCE NAME        : Iatp180
************************************************************
************************************************************************
*               TRANSFER AND SWITCH CONTROL NET CHANGE REPORT
*               ---------------------------------------------
*  PROGRAM: IATP180
*  DATE   : 12/29/97
*  BY     : LEN BERNSTEIN
*  DESC   : THIS REPORT PRODUCES THE FOLLOWING PAGES:-
*           1. TRANSFER TOTALS FOR ALL ANNUAL ACCOUNTS
*           2. TRANSFER TOTALS FOR ALL MONTHLY ACCOUNTS
*           3. SWITCH  TOTALS FROM ANNUAL  TO  MONTHLY ACCOUNTS
*           4. SWITCH  TOTALS FROM MONTHLY TO  ANNUAL ACCOUNTS
*           5. GRAND TOTAL FOR ANNUAL ACCOUNTS
*           6. GRAND TOTAL FOR MONTHLY ACCOUNTS
*  HISTORY: 6/99 KN ADDED TIAA TO CREF TRANSFERS
*  -------
*  05/10/2002  TD  CHANGE IAAN050A TO IAAN051A
*  03/02/2009  OS  TIAA ACCESS CHANGE. SC 030209.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp180 extends BLNatBase
{
    // Data Areas
    private PdaIatl400p pdaIatl400p;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Cntrl;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data;

    private DbsGroup iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs;
    private DbsField iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt;
    private DbsField iaa_Xfr_Cntrl_Lst_Chnge_Dte;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1;

    private DbsGroup pnd_Iaxfr_Ctl_Super_De_1__R_Field_1;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte;
    private DbsField pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte;

    private DbsGroup pnd_Iaan050a_Pda;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Fund_2;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Desc;
    private DbsField pnd_Iaan050a_Pda_Pnd_Cmpny_Desc;
    private DbsField pnd_Iaan050a_Pda_Pnd_Parm_Len;

    private DbsGroup pnd_Headings;
    private DbsField pnd_Headings_Pnd_Transfer_Annual;
    private DbsField pnd_Headings_Pnd_Transfer_Monthly;
    private DbsField pnd_Headings_Pnd_Switch_From_Annual;
    private DbsField pnd_Headings_Pnd_Switch_From_Monthly;
    private DbsField pnd_Headings_Pnd_Grand_Tot_Annual;
    private DbsField pnd_Headings_Pnd_Grand_Tot_Monthly;

    private DbsGroup pnd_Index;
    private DbsField pnd_Index_Pnd_Index_1g;
    private DbsField pnd_Index_Pnd_Index_1s;
    private DbsField pnd_Index_Pnd_Index_09;
    private DbsField pnd_Index_Pnd_Index_11;
    private DbsField pnd_Index_Pnd_Write_Index;
    private DbsField pnd_W_Iaxfr_Ctl_Frm_Fund_Cde;

    private DbsGroup pnd_Write_Line;
    private DbsField pnd_Write_Line_Pnd_Desc;

    private DbsGroup pnd_Tot;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Units;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_To_Units;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_To_Dollars;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Ft_Fund_Rcrds;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Full_New;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Ft_Units;
    private DbsField pnd_Tot_Pnd_Net_Ctl_Ft_Dollars;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_New_Tiaa_Payees;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_New_Cref_Payees;
    private DbsField pnd_Tot_Pnd_Tot_Ctl_New_Inactive_Payees;

    private DbsGroup pnd_Grand;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Rcrds;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Per_Divid_Amt;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Units;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_Frm_Dollars;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_To_Fund_Rcrds;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_New_Fund_Recs;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_To_Cntrctrl_Amt;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_To_Per_Divid_Amt;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_To_Units;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_To_Dollars;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_New_Tiaa_Payees;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_New_Cref_Payees;
    private DbsField pnd_Grand_Pnd_Tot_Ctl_New_Inactive_Payees;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_Transfer;
    private DbsField pnd_L_Pnd_Annual;
    private DbsField pnd_L_Pnd_Grand_Totals;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_I;
    private DbsField pnd_Fund_Desc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIatl400p = new PdaIatl400p(localVariables);

        // Local Variables

        vw_iaa_Xfr_Cntrl = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Cntrl", "IAA-XFR-CNTRL"), "IAA_XFR_CNTRL", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_CNTRL"));
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type", "IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_CTL_RCRD_TYPE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Effctve_Dte", "IAXFR-CTL-EFFCTVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CTL_EFFCTVE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte", "IAXFR-CTL-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CTL_CYCLE_DTE");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees", "IAXFR-CTL-NEW-TIAA-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_TIAA_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees", "IAXFR-CTL-NEW-CREF-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_CREF_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees", "IAXFR-CTL-NEW-INACTIVE-PAYEES", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "IAXFR_CTL_NEW_INACTIVE_PAYEES");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_Frm_Asset_Amt", "IAXFR-CTL-TOTAL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_FRM_ASSET_AMT");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Total_To_Asset_Amt", "IAXFR-CTL-TOTAL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "IAXFR_CTL_TOTAL_TO_ASSET_AMT");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_Frm_Data", "C*IAXFR-CTL-FRM-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data", "IAXFR-CTL-FRM-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde", "IAXFR-CTL-FRM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units", "IAXFR-CTL-FRM-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_UNITS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt", "IAXFR-CTL-FRM-GUAR-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_GUAR_AMT", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt", 
            "IAXFR-CTL-FRM-PER-DIVID-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_PER_DIVID_AMT", 
            "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars", "IAXFR-CTL-FRM-DOLLARS", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_DOLLARS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds", "IAXFR-CTL-FRM-FUND-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FUND_RCRDS", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds", 
            "IAXFR-CTL-FRM-FULL-OUT-RCRDS", FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_FULL_OUT_RCRDS", 
            "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Asset_Amt", "IAXFR-CTL-FRM-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_FRM_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_FRM_DATA");
        iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Count_Castiaxfr_Ctl_To_Data", "C*IAXFR-CTL-TO-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CTL_TO_DATA");

        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data = vw_iaa_Xfr_Cntrl.getRecord().newGroupInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data", "IAXFR-CTL-TO-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde", "IAXFR-CTL-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units", "IAXFR-CTL-TO-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_UNITS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Unit_Typ", "IAXFR-CTL-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt", "IAXFR-CTL-TO-GUAR-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_GUAR_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt", "IAXFR-CTL-TO-PER-DIVID-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_PER_DIVID_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars", "IAXFR-CTL-TO-DOLLARS", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_DOLLARS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds", "IAXFR-CTL-TO-FUND-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_FUND_RCRDS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs", "IAXFR-CTL-NEW-FUND-RECS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_NEW_FUND_RECS", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt = iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Data.newFieldArrayInGroup("iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Asset_Amt", "IAXFR-CTL-TO-ASSET-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_CTL_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CTL_TO_DATA");
        iaa_Xfr_Cntrl_Lst_Chnge_Dte = vw_iaa_Xfr_Cntrl.getRecord().newFieldInGroup("iaa_Xfr_Cntrl_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        registerRecord(vw_iaa_Xfr_Cntrl);

        pnd_Iaxfr_Ctl_Super_De_1 = localVariables.newFieldInRecord("pnd_Iaxfr_Ctl_Super_De_1", "#IAXFR-CTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Iaxfr_Ctl_Super_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaxfr_Ctl_Super_De_1__R_Field_1", "REDEFINE", pnd_Iaxfr_Ctl_Super_De_1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type", 
            "#IAXFR-CTL-RCRD-TYPE", FieldType.STRING, 1);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte", 
            "#IAXFR-CTL-CYCLE-DTE", FieldType.DATE);
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte = pnd_Iaxfr_Ctl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte", 
            "#IAXFR-CTL-EFFCTVE-DTE", FieldType.DATE);

        pnd_Iaan050a_Pda = localVariables.newGroupInRecord("pnd_Iaan050a_Pda", "#IAAN050A-PDA");
        pnd_Iaan050a_Pda_Pnd_Parm_Fund_2 = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Iaan050a_Pda_Pnd_Parm_Desc = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);
        pnd_Iaan050a_Pda_Pnd_Cmpny_Desc = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Iaan050a_Pda_Pnd_Parm_Len = pnd_Iaan050a_Pda.newFieldInGroup("pnd_Iaan050a_Pda_Pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);

        pnd_Headings = localVariables.newGroupInRecord("pnd_Headings", "#HEADINGS");
        pnd_Headings_Pnd_Transfer_Annual = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Transfer_Annual", "#TRANSFER-ANNUAL", FieldType.STRING, 58);
        pnd_Headings_Pnd_Transfer_Monthly = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Transfer_Monthly", "#TRANSFER-MONTHLY", FieldType.STRING, 59);
        pnd_Headings_Pnd_Switch_From_Annual = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Switch_From_Annual", "#SWITCH-FROM-ANNUAL", FieldType.STRING, 
            57);
        pnd_Headings_Pnd_Switch_From_Monthly = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Switch_From_Monthly", "#SWITCH-FROM-MONTHLY", FieldType.STRING, 
            58);
        pnd_Headings_Pnd_Grand_Tot_Annual = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Grand_Tot_Annual", "#GRAND-TOT-ANNUAL", FieldType.STRING, 58);
        pnd_Headings_Pnd_Grand_Tot_Monthly = pnd_Headings.newFieldInGroup("pnd_Headings_Pnd_Grand_Tot_Monthly", "#GRAND-TOT-MONTHLY", FieldType.STRING, 
            59);

        pnd_Index = localVariables.newGroupInRecord("pnd_Index", "#INDEX");
        pnd_Index_Pnd_Index_1g = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_1g", "#INDEX-1G", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_1s = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_1s", "#INDEX-1S", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_09 = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_09", "#INDEX-09", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Index_11 = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Index_11", "#INDEX-11", FieldType.PACKED_DECIMAL, 2);
        pnd_Index_Pnd_Write_Index = pnd_Index.newFieldInGroup("pnd_Index_Pnd_Write_Index", "#WRITE-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_W_Iaxfr_Ctl_Frm_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_W_Iaxfr_Ctl_Frm_Fund_Cde", "#W-IAXFR-CTL-FRM-FUND-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));

        pnd_Write_Line = localVariables.newGroupInRecord("pnd_Write_Line", "#WRITE-LINE");
        pnd_Write_Line_Pnd_Desc = pnd_Write_Line.newFieldInGroup("pnd_Write_Line_Pnd_Desc", "#DESC", FieldType.STRING, 43);

        pnd_Tot = localVariables.newGroupInRecord("pnd_Tot", "#TOT");
        pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds", "#TOT-CTL-FRM-FUND-RCRDS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds", "#TOT-CTL-FRM-FULL-OUT-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt", "#TOT-CTL-FRM-CNTRCTRL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt", "#TOT-CTL-FRM-PER-DIVID-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_Frm_Units = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Units", "#TOT-CTL-FRM-UNITS", FieldType.NUMERIC, 11, 3, 
            new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars", "#TOT-CTL-FRM-DOLLARS", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds", "#TOT-CTL-TO-FUND-RCRDS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs", "#TOT-CTL-NEW-FUND-RECS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt", "#TOT-CTL-TO-CNTRCTRL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt", "#TOT-CTL-TO-PER-DIVID-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_To_Units = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_To_Units", "#TOT-CTL-TO-UNITS", FieldType.NUMERIC, 11, 3, new 
            DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_To_Dollars = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Tot_Ctl_To_Dollars", "#TOT-CTL-TO-DOLLARS", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Net_Ctl_Ft_Fund_Rcrds = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Ft_Fund_Rcrds", "#NET-CTL-FT-FUND-RCRDS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Net_Ctl_Full_New = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Full_New", "#NET-CTL-FULL-NEW", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            40));
        pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt", "#NET-CTL-FT-CNTRCTRL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt", "#NET-CTL-FT-PER-DIVID-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Net_Ctl_Ft_Units = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Ft_Units", "#NET-CTL-FT-UNITS", FieldType.NUMERIC, 11, 3, new 
            DbsArrayController(1, 40));
        pnd_Tot_Pnd_Net_Ctl_Ft_Dollars = pnd_Tot.newFieldArrayInGroup("pnd_Tot_Pnd_Net_Ctl_Ft_Dollars", "#NET-CTL-FT-DOLLARS", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 40));
        pnd_Tot_Pnd_Tot_Ctl_New_Tiaa_Payees = pnd_Tot.newFieldInGroup("pnd_Tot_Pnd_Tot_Ctl_New_Tiaa_Payees", "#TOT-CTL-NEW-TIAA-PAYEES", FieldType.NUMERIC, 
            7);
        pnd_Tot_Pnd_Tot_Ctl_New_Cref_Payees = pnd_Tot.newFieldInGroup("pnd_Tot_Pnd_Tot_Ctl_New_Cref_Payees", "#TOT-CTL-NEW-CREF-PAYEES", FieldType.NUMERIC, 
            7);
        pnd_Tot_Pnd_Tot_Ctl_New_Inactive_Payees = pnd_Tot.newFieldInGroup("pnd_Tot_Pnd_Tot_Ctl_New_Inactive_Payees", "#TOT-CTL-NEW-INACTIVE-PAYEES", FieldType.NUMERIC, 
            7);

        pnd_Grand = localVariables.newGroupInRecord("pnd_Grand", "#GRAND");
        pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde", "#TOT-CTL-FRM-FUND-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Rcrds = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Rcrds", "#TOT-CTL-FRM-FUND-RCRDS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds", "#TOT-CTL-FRM-FULL-OUT-RCRDS", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt", "#TOT-CTL-FRM-CNTRCTRL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Per_Divid_Amt = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Per_Divid_Amt", "#TOT-CTL-FRM-PER-DIVID-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Units = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Units", "#TOT-CTL-FRM-UNITS", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_Frm_Dollars = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_Frm_Dollars", "#TOT-CTL-FRM-DOLLARS", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_To_Fund_Rcrds = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_To_Fund_Rcrds", "#TOT-CTL-TO-FUND-RCRDS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_New_Fund_Recs = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_New_Fund_Recs", "#TOT-CTL-NEW-FUND-RECS", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_To_Cntrctrl_Amt = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_To_Cntrctrl_Amt", "#TOT-CTL-TO-CNTRCTRL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_To_Per_Divid_Amt = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_To_Per_Divid_Amt", "#TOT-CTL-TO-PER-DIVID-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_To_Units = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_To_Units", "#TOT-CTL-TO-UNITS", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_To_Dollars = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Tot_Ctl_To_Dollars", "#TOT-CTL-TO-DOLLARS", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Grand_Pnd_Tot_Ctl_New_Tiaa_Payees = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Tot_Ctl_New_Tiaa_Payees", "#TOT-CTL-NEW-TIAA-PAYEES", FieldType.NUMERIC, 
            7);
        pnd_Grand_Pnd_Tot_Ctl_New_Cref_Payees = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Tot_Ctl_New_Cref_Payees", "#TOT-CTL-NEW-CREF-PAYEES", FieldType.NUMERIC, 
            7);
        pnd_Grand_Pnd_Tot_Ctl_New_Inactive_Payees = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Tot_Ctl_New_Inactive_Payees", "#TOT-CTL-NEW-INACTIVE-PAYEES", 
            FieldType.NUMERIC, 7);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_Transfer = pnd_L.newFieldInGroup("pnd_L_Pnd_Transfer", "#TRANSFER", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Annual = pnd_L.newFieldInGroup("pnd_L_Pnd_Annual", "#ANNUAL", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Grand_Totals = pnd_L.newFieldInGroup("pnd_L_Pnd_Grand_Totals", "#GRAND-TOTALS", FieldType.BOOLEAN, 1);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35, new DbsArrayController(1, 40));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Cntrl.reset();

        localVariables.reset();
        pnd_Headings_Pnd_Transfer_Annual.setInitialValue("FOR ANNUAL TRANSFERS PROCESSED WITH AN ACCOUNTING DATE OF ");
        pnd_Headings_Pnd_Transfer_Monthly.setInitialValue("FOR MONTHLY TRANSFERS PROCESSED WITH AN ACCOUNTING DATE OF ");
        pnd_Headings_Pnd_Switch_From_Annual.setInitialValue("FOR ANNUAL SWITCHES PROCESSED WITH AN ACCOUNTING DATE OF ");
        pnd_Headings_Pnd_Switch_From_Monthly.setInitialValue("FOR MONTHLY SWITCHES PROCESSED WITH AN ACCOUNTING DATE OF ");
        pnd_Headings_Pnd_Grand_Tot_Annual.setInitialValue("GRAND TOTAL FOR ANNUAL TRANSFERS AND SWITCHES FOR ACC DATE");
        pnd_Headings_Pnd_Grand_Tot_Monthly.setInitialValue("GRAND TOTAL FOR MONTHLY TRANSFERS AND SWITCHES FOR ACC DATE");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp180() throws Exception
    {
        super("Iatp180");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ======================================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ======================================================================
        //*                           START OF PROGRAM
        //* ======================================================================
        //*  030209
        pnd_Index_Pnd_Index_1g.reset();                                                                                                                                   //Natural: RESET #INDEX-1G #INDEX-1S #INDEX-09 #WRITE-INDEX #INDEX-11
        pnd_Index_Pnd_Index_1s.reset();
        pnd_Index_Pnd_Index_09.reset();
        pnd_Index_Pnd_Write_Index.reset();
        pnd_Index_Pnd_Index_11.reset();
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DATE
        sub_Get_Business_Date();
        if (condition(Global.isEscape())) {return;}
        //*  ========================================
        //*  PORCESS THE TRANSFER PAGES OF THE REPORT
        //*  ========================================
        pnd_L_Pnd_Transfer.setValue(true);                                                                                                                                //Natural: ASSIGN #TRANSFER := TRUE
        //*  030209
                                                                                                                                                                          //Natural: PERFORM ADD-TRNSFR-SW-REPORT-TOTALS
        sub_Add_Trnsfr_Sw_Report_Totals();
        if (condition(Global.isEscape())) {return;}
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*").setValue(pnd_W_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*"));                                                          //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( * ) := #W-IAXFR-CTL-FRM-FUND-CDE ( * )
                                                                                                                                                                          //Natural: PERFORM CALC-NET-VAL
        sub_Calc_Net_Val();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  ======================================
        //*  PROCESS THE SWITCH PAGES OF THE REPORT
        //*  ======================================
        pnd_L_Pnd_Transfer.reset();                                                                                                                                       //Natural: RESET #TRANSFER
        //*  030209
                                                                                                                                                                          //Natural: PERFORM ADD-TRNSFR-SW-REPORT-TOTALS
        sub_Add_Trnsfr_Sw_Report_Totals();
        if (condition(Global.isEscape())) {return;}
        iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*").setValue(pnd_W_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*"));                                                          //Natural: ASSIGN IAXFR-CTL-FRM-FUND-CDE ( * ) := #W-IAXFR-CTL-FRM-FUND-CDE ( * )
                                                                                                                                                                          //Natural: PERFORM CALC-NET-VAL
        sub_Calc_Net_Val();
        if (condition(Global.isEscape())) {return;}
        //*  FOR PAGE HEADING
        //*  THIS LETS US ALSO SHOW TIAA FIXED TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  ========================
        //*  PROCESS THE GRAND TOTALS
        //*  ========================
        pnd_L_Pnd_Grand_Totals.setValue(true);                                                                                                                            //Natural: ASSIGN #GRAND-TOTALS := TRUE
        pnd_L_Pnd_Transfer.setValue(true);                                                                                                                                //Natural: ASSIGN #TRANSFER := TRUE
        pnd_Tot.setValuesByName(pnd_Grand);                                                                                                                               //Natural: MOVE BY NAME #GRAND TO #TOT
                                                                                                                                                                          //Natural: PERFORM CALC-NET-VAL
        sub_Calc_Net_Val();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        //* ======================================================================
        //*                           START OF SUBROUTINES
        //* ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TRNSFR-SW-REPORT-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-NET-VAL
        //* ***********************************************************************
        //*  COMPUTE THE NET TRANSFER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* ***********************************************************************
        //*  =====================
        //*   WRITE ANNUAL TOTALS
        //*  =====================
        //*   WRITE ALL CREF ANNUAL FUNDS
        //*  AT THE END OF EACH ANNUAL PAGE WE WRITE THE FOLLOWING TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-1-LINE
        //*  ==================
        //*    WRITE THE LINE
        //*  ==================
    }
    private void sub_Get_Business_Date() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'DC'
        //*  FETCH THE LATEST CONTROL RECORD FOR CNTRL CD
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
        if (condition(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte().equals(getZero())))                                                                               //Natural: IF #IATN400-OUT.CNTRL-TODAYS-DTE EQ 0
        {
            getReports().write(0, "Control Record 'DC' does not have a business date.");                                                                                  //Natural: WRITE 'Control Record "DC" does not have a business date.'
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-BUSINESS-DATE
    }
    private void sub_Add_Trnsfr_Sw_Report_Totals() throws Exception                                                                                                       //Natural: ADD-TRNSFR-SW-REPORT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Effctve_Dte.reset();                                                                                                       //Natural: RESET #IAXFR-CTL-EFFCTVE-DTE #TOT
        pnd_Tot.reset();
        //*  SET SEARCH KEY
        if (condition(pnd_L_Pnd_Transfer.getBoolean()))                                                                                                                   //Natural: IF #TRANSFER
        {
            pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.setValue(pnd_Const_Pnd_Rcrd_Type_Trnsfr);                                                                    //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE := #RCRD-TYPE-TRNSFR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type.setValue(pnd_Const_Pnd_Rcrd_Type_Switch);                                                                    //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE := #RCRD-TYPE-SWITCH
            //*  BUSINESS DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte.setValue(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte());                                                     //Natural: ASSIGN #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE := #IATN400-OUT.CNTRL-TODAYS-DTE
        //*   READ THE CONTROL FILE
        vw_iaa_Xfr_Cntrl.startDatabaseRead                                                                                                                                //Natural: READ IAA-XFR-CNTRL BY IAXFR-CTL-SUPER-DE-1 STARTING FROM #IAXFR-CTL-SUPER-DE-1
        (
        "RD1",
        new Wc[] { new Wc("IAXFR_CTL_SUPER_DE_1", ">=", pnd_Iaxfr_Ctl_Super_De_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IAXFR_CTL_SUPER_DE_1", "ASC") }
        );
        RD1:
        while (condition(vw_iaa_Xfr_Cntrl.readNextRow("RD1")))
        {
            if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Rcrd_Type.notEquals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Rcrd_Type) || iaa_Xfr_Cntrl_Iaxfr_Ctl_Cycle_Dte.notEquals(pnd_Iaxfr_Ctl_Super_De_1_Pnd_Iaxfr_Ctl_Cycle_Dte))) //Natural: IF IAXFR-CTL-RCRD-TYPE NE #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-RCRD-TYPE OR IAXFR-CTL-CYCLE-DTE NE #IAXFR-CTL-SUPER-DE-1.#IAXFR-CTL-CYCLE-DTE
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
                //*  030209
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*").setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue("*"));                                                      //Natural: ASSIGN #W-IAXFR-CTL-FRM-FUND-CDE ( * ) := IAXFR-CTL-FRM-FUND-CDE ( * )
            //*  ADD THE FROM AND TO TOTALS
            FR1:                                                                                                                                                          //Natural: FOR #I = 1 TO 40
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
            {
                if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I).equals(" ") && iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Cde.getValue(pnd_I).equals(" ")))       //Natural: IF IAXFR-CTL-FRM-FUND-CDE ( #I ) = ' ' AND IAXFR-CTL-TO-FUND-CDE ( #I ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde.getValue(pnd_I).setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I));                                        //Natural: ASSIGN #GRAND.#TOT-CTL-FRM-FUND-CDE ( #I ) := IAXFR-CTL-FRM-FUND-CDE ( #I )
                //* ====================
                //*  ADD FROM TOTALS
                //* ====================
                pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds.getValue(pnd_I));                                          //Natural: ADD IAXFR-CTL-FRM-FUND-RCRDS ( #I ) TO #TOT.#TOT-CTL-FRM-FUND-RCRDS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Rcrds.getValue(pnd_I));                                        //Natural: ADD IAXFR-CTL-FRM-FUND-RCRDS ( #I ) TO #GRAND.#TOT-CTL-FRM-FUND-RCRDS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_I));                                  //Natural: ADD IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #I ) TO #TOT.#TOT-CTL-FRM-FULL-OUT-RCRDS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_I));                                //Natural: ADD IAXFR-CTL-FRM-FULL-OUT-RCRDS ( #I ) TO #GRAND.#TOT-CTL-FRM-FULL-OUT-RCRDS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt.getValue(pnd_I));                                          //Natural: ADD IAXFR-CTL-FRM-GUAR-AMT ( #I ) TO #TOT.#TOT-CTL-FRM-CNTRCTRL-AMT ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Guar_Amt.getValue(pnd_I));                                        //Natural: ADD IAXFR-CTL-FRM-GUAR-AMT ( #I ) TO #GRAND.#TOT-CTL-FRM-CNTRCTRL-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt.getValue(pnd_I));                                    //Natural: ADD IAXFR-CTL-FRM-PER-DIVID-AMT ( #I ) TO #TOT.#TOT-CTL-FRM-PER-DIVID-AMT ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Per_Divid_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Per_Divid_Amt.getValue(pnd_I));                                  //Natural: ADD IAXFR-CTL-FRM-PER-DIVID-AMT ( #I ) TO #GRAND.#TOT-CTL-FRM-PER-DIVID-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_Frm_Units.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units.getValue(pnd_I));                                                    //Natural: ADD IAXFR-CTL-FRM-UNITS ( #I ) TO #TOT.#TOT-CTL-FRM-UNITS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Units.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Units.getValue(pnd_I));                                                  //Natural: ADD IAXFR-CTL-FRM-UNITS ( #I ) TO #GRAND.#TOT-CTL-FRM-UNITS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars.getValue(pnd_I));                                                //Natural: ADD IAXFR-CTL-FRM-DOLLARS ( #I ) TO #TOT.#TOT-CTL-FRM-DOLLARS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_Frm_Dollars.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Dollars.getValue(pnd_I));                                              //Natural: ADD IAXFR-CTL-FRM-DOLLARS ( #I ) TO #GRAND.#TOT-CTL-FRM-DOLLARS ( #I )
                //* ================
                //*  ADD TO TOTALS
                //* ================
                pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds.getValue(pnd_I));                                            //Natural: ADD IAXFR-CTL-TO-FUND-RCRDS ( #I ) TO #TOT.#TOT-CTL-TO-FUND-RCRDS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_To_Fund_Rcrds.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Fund_Rcrds.getValue(pnd_I));                                          //Natural: ADD IAXFR-CTL-TO-FUND-RCRDS ( #I ) TO #GRAND.#TOT-CTL-TO-FUND-RCRDS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_I));                                            //Natural: ADD IAXFR-CTL-NEW-FUND-RECS ( #I ) TO #TOT.#TOT-CTL-NEW-FUND-RECS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_New_Fund_Recs.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Fund_Recs.getValue(pnd_I));                                          //Natural: ADD IAXFR-CTL-NEW-FUND-RECS ( #I ) TO #GRAND.#TOT-CTL-NEW-FUND-RECS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt.getValue(pnd_I));                                            //Natural: ADD IAXFR-CTL-TO-GUAR-AMT ( #I ) TO #TOT.#TOT-CTL-TO-CNTRCTRL-AMT ( #I )
                pnd_Grand_Pnd_Tot_Ctl_To_Cntrctrl_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Guar_Amt.getValue(pnd_I));                                          //Natural: ADD IAXFR-CTL-TO-GUAR-AMT ( #I ) TO #GRAND.#TOT-CTL-TO-CNTRCTRL-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt.getValue(pnd_I));                                      //Natural: ADD IAXFR-CTL-TO-PER-DIVID-AMT ( #I ) TO #TOT.#TOT-CTL-TO-PER-DIVID-AMT ( #I )
                pnd_Grand_Pnd_Tot_Ctl_To_Per_Divid_Amt.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Per_Divid_Amt.getValue(pnd_I));                                    //Natural: ADD IAXFR-CTL-TO-PER-DIVID-AMT ( #I ) TO #GRAND.#TOT-CTL-TO-PER-DIVID-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Units.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units.getValue(pnd_I));                                                      //Natural: ADD IAXFR-CTL-TO-UNITS ( #I ) TO #TOT.#TOT-CTL-TO-UNITS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_To_Units.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Units.getValue(pnd_I));                                                    //Natural: ADD IAXFR-CTL-TO-UNITS ( #I ) TO #GRAND.#TOT-CTL-TO-UNITS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Dollars.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars.getValue(pnd_I));                                                  //Natural: ADD IAXFR-CTL-TO-DOLLARS ( #I ) TO #TOT.#TOT-CTL-TO-DOLLARS ( #I )
                pnd_Grand_Pnd_Tot_Ctl_To_Dollars.getValue(pnd_I).nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_To_Dollars.getValue(pnd_I));                                                //Natural: ADD IAXFR-CTL-TO-DOLLARS ( #I ) TO #GRAND.#TOT-CTL-TO-DOLLARS ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADD OTHER FIELDS
            pnd_Tot_Pnd_Tot_Ctl_New_Tiaa_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees);                                                                            //Natural: ADD IAXFR-CTL-NEW-TIAA-PAYEES TO #TOT.#TOT-CTL-NEW-TIAA-PAYEES
            pnd_Grand_Pnd_Tot_Ctl_New_Tiaa_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Tiaa_Payees);                                                                          //Natural: ADD IAXFR-CTL-NEW-TIAA-PAYEES TO #GRAND.#TOT-CTL-NEW-TIAA-PAYEES
            pnd_Tot_Pnd_Tot_Ctl_New_Cref_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees);                                                                            //Natural: ADD IAXFR-CTL-NEW-CREF-PAYEES TO #TOT.#TOT-CTL-NEW-CREF-PAYEES
            pnd_Grand_Pnd_Tot_Ctl_New_Cref_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Cref_Payees);                                                                          //Natural: ADD IAXFR-CTL-NEW-CREF-PAYEES TO #GRAND.#TOT-CTL-NEW-CREF-PAYEES
            pnd_Tot_Pnd_Tot_Ctl_New_Inactive_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees);                                                                    //Natural: ADD IAXFR-CTL-NEW-INACTIVE-PAYEES TO #TOT.#TOT-CTL-NEW-INACTIVE-PAYEES
            pnd_Grand_Pnd_Tot_Ctl_New_Inactive_Payees.nadd(iaa_Xfr_Cntrl_Iaxfr_Ctl_New_Inactive_Payees);                                                                  //Natural: ADD IAXFR-CTL-NEW-INACTIVE-PAYEES TO #GRAND.#TOT-CTL-NEW-INACTIVE-PAYEES
            //*  RD1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   ADD-TRNSFR-SW-REPORT-TOTALS
    }
    private void sub_Calc_Net_Val() throws Exception                                                                                                                      //Natural: CALC-NET-VAL
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            pnd_Tot_Pnd_Net_Ctl_Ft_Fund_Rcrds.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Ft_Fund_Rcrds.getValue(pnd_I)),                    //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-FUND-RCRDS ( #I ) FROM #TOT.#TOT-CTL-TO-FUND-RCRDS ( #I ) GIVING #NET-CTL-FT-FUND-RCRDS ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds.getValue(pnd_I)));
            pnd_Tot_Pnd_Net_Ctl_Full_New.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Full_New.getValue(pnd_I)), pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_I))); //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-FULL-OUT-RCRDS ( #I ) FROM #TOT.#TOT-CTL-NEW-FUND-RECS ( #I ) GIVING #NET-CTL-FULL-NEW ( #I )
            pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt.getValue(pnd_I)),                //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-CNTRCTRL-AMT ( #I ) FROM #TOT.#TOT-CTL-TO-CNTRCTRL-AMT ( #I ) GIVING #NET-CTL-FT-CNTRCTRL-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt.getValue(pnd_I)));
            pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt.getValue(pnd_I)),              //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-PER-DIVID-AMT ( #I ) FROM #TOT.#TOT-CTL-TO-PER-DIVID-AMT ( #I ) GIVING #NET-CTL-FT-PER-DIVID-AMT ( #I )
                pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt.getValue(pnd_I)));
            pnd_Tot_Pnd_Net_Ctl_Ft_Units.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Ft_Units.getValue(pnd_I)), pnd_Tot_Pnd_Tot_Ctl_To_Units.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Units.getValue(pnd_I))); //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-UNITS ( #I ) FROM #TOT.#TOT-CTL-TO-UNITS ( #I ) GIVING #NET-CTL-FT-UNITS ( #I )
            pnd_Tot_Pnd_Net_Ctl_Ft_Dollars.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tot_Pnd_Net_Ctl_Ft_Dollars.getValue(pnd_I)), pnd_Tot_Pnd_Tot_Ctl_To_Dollars.getValue(pnd_I).subtract(pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars.getValue(pnd_I))); //Natural: SUBTRACT #TOT.#TOT-CTL-FRM-DOLLARS ( #I ) FROM #TOT.#TOT-CTL-TO-DOLLARS ( #I ) GIVING #NET-CTL-FT-DOLLARS ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CALC-NET-VAL
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_L_Pnd_Annual.setValue(true);                                                                                                                                  //Natural: ASSIGN #ANNUAL := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  CHECK THE POSITIONS OF TIAA STANDARD, TIAA GRADED AND TIAA REAL EST
        //*  1G SHOULD = "1", 1S SHOULD = "21" AND 09 SHOULD = "09"
        //*  CHANGED 1S TO "22" IN IATN420X
        DbsUtil.examine(new ExamineSource(pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde.getValue(1,":",40)), new ExamineSearch("1G"), new ExamineGivingIndex(pnd_Index_Pnd_Index_1g)); //Natural: EXAMINE #TOT-CTL-FRM-FUND-CDE ( 1:40 ) FOR '1G' GIVING INDEX #INDEX-1G
        DbsUtil.examine(new ExamineSource(pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde.getValue(1,":",40)), new ExamineSearch("1S"), new ExamineGivingIndex(pnd_Index_Pnd_Index_1s)); //Natural: EXAMINE #TOT-CTL-FRM-FUND-CDE ( 1:40 ) FOR '1S' GIVING INDEX #INDEX-1S
        DbsUtil.examine(new ExamineSource(pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde.getValue(1,":",20)), new ExamineSearch("09"), new ExamineGivingIndex(pnd_Index_Pnd_Index_09)); //Natural: EXAMINE #TOT-CTL-FRM-FUND-CDE ( 1:20 ) FOR '09' GIVING INDEX #INDEX-09
        //*  030209
        DbsUtil.examine(new ExamineSource(pnd_Grand_Pnd_Tot_Ctl_Frm_Fund_Cde.getValue(1,":",20)), new ExamineSearch("11"), new ExamineGivingIndex(pnd_Index_Pnd_Index_11)); //Natural: EXAMINE #TOT-CTL-FRM-FUND-CDE ( 1:20 ) FOR '11' GIVING INDEX #INDEX-11
        //*  030209
        if (condition(pnd_Index_Pnd_Index_1g.notEquals(1) || pnd_Index_Pnd_Index_1s.notEquals(21) || pnd_Index_Pnd_Index_09.notEquals(9) || pnd_Index_Pnd_Index_11.notEquals(11))) //Natural: IF #INDEX-1G NE 1 OR #INDEX-1S NE 21 OR #INDEX-09 NE 9 OR #INDEX-11 NE 11
        {
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  Warning                       *");                                                                                  //Natural: WRITE '*                  Warning                       *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  =======                       *");                                                                                  //Natural: WRITE '*                  =======                       *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                *");                                                                                  //Natural: WRITE '*                                                *'
            if (Global.isEscape()) return;
            getReports().write(0, "* The positions of certain funds are not         *");                                                                                  //Natural: WRITE '* The positions of certain funds are not         *'
            if (Global.isEscape()) return;
            getReports().write(0, "* correct. This may cause an error in the Totals *");                                                                                  //Natural: WRITE '* correct. This may cause an error in the Totals *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Position of TIAA Graded should be '1'          *");                                                                                  //Natural: WRITE '* Position of TIAA Graded should be "1"          *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Position of TIAA Standard Should be '21'       *");                                                                                  //Natural: WRITE '* Position of TIAA Standard Should be "21"       *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Position of Annual Real Estate should be '09'  *");                                                                                  //Natural: WRITE '* Position of Annual Real Estate should be "09"  *'
            if (Global.isEscape()) return;
            //*  030209
            getReports().write(0, "* Position of Annual Access should be '11'       *");                                                                                  //Natural: WRITE '* Position of Annual Access should be "11"       *'
            if (Global.isEscape()) return;
            getReports().write(0, "*  TIAA Graded pos        = ",pnd_Index_Pnd_Index_1g);                                                                                 //Natural: WRITE '*  TIAA Graded pos        = ' #INDEX-1G
            if (Global.isEscape()) return;
            getReports().write(0, "*  TIAA Standard pos      = ",pnd_Index_Pnd_Index_1s);                                                                                 //Natural: WRITE '*  TIAA Standard pos      = ' #INDEX-1S
            if (Global.isEscape()) return;
            getReports().write(0, "*  Annual Real Estate pos = ",pnd_Index_Pnd_Index_09);                                                                                 //Natural: WRITE '*  Annual Real Estate pos = ' #INDEX-09
            if (Global.isEscape()) return;
            //*  030209
            getReports().write(0, "*  Annual Access pos      = ",pnd_Index_Pnd_Index_11);                                                                                 //Natural: WRITE '*  Annual Access pos      = ' #INDEX-11
            if (Global.isEscape()) return;
            getReports().write(0, "*  Program                =  ",Global.getPROGRAM());                                                                                   //Natural: WRITE '*  Program                =  ' *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE TIAA STANDARD ANNUAL
        if (condition(pnd_Index_Pnd_Index_1s.notEquals(getZero()) && pnd_L_Pnd_Transfer.getBoolean()))                                                                    //Natural: IF #INDEX-1S NE 0 AND #TRANSFER
        {
            pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_1s);                                                                                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-1S
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE TIAA GRADED ANNUAL
        if (condition(pnd_Index_Pnd_Index_1g.notEquals(getZero()) && pnd_L_Pnd_Transfer.getBoolean()))                                                                    //Natural: IF #INDEX-1G NE 0 AND #TRANSFER
        {
            pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_1g);                                                                                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-1G
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE TIAA REAL ESTATE ANNUAL
        if (condition(pnd_Index_Pnd_Index_09.notEquals(getZero())))                                                                                                       //Natural: IF #INDEX-09 NE 0
        {
            pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_09);                                                                                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-09
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE TIAA ACCESS ANNUAL                       /* 030209 START
        if (condition(pnd_Index_Pnd_Index_11.notEquals(getZero())))                                                                                                       //Natural: IF #INDEX-11 NE 0
        {
            pnd_Index_Pnd_Write_Index.setValue(pnd_Index_Pnd_Index_11);                                                                                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-11
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
            //*  030209 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SKIP TIAA STD,GRAD AND REA
            //*  030209
            if (condition(pnd_I.equals(pnd_Index_Pnd_Index_1g) || pnd_I.equals(pnd_Index_Pnd_Index_1s) || pnd_I.equals(pnd_Index_Pnd_Index_09) || pnd_I.equals(pnd_Index_Pnd_Index_11))) //Natural: IF #I = #INDEX-1G OR = #INDEX-1S OR = #INDEX-09 OR = #INDEX-11
            {
                //*  FOR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK THAT A FUND EXISTS
            if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I).notEquals(" ")))                                                                           //Natural: IF IAXFR-CTL-FRM-FUND-CDE ( #I ) NE ' '
            {
                pnd_Index_Pnd_Write_Index.setValue(pnd_I);                                                                                                                //Natural: ASSIGN #WRITE-INDEX := #I
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"TIAA NEW PAYEES",new ColumnSpacing(40),pnd_Tot_Pnd_Tot_Ctl_New_Tiaa_Payees, new ReportEditMask ("Z,ZZZ,ZZ9"));        //Natural: WRITE ( 1 ) 'TIAA NEW PAYEES' 40X #TOT.#TOT-CTL-NEW-TIAA-PAYEES ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"CREF NEW PAYEES",new ColumnSpacing(40),pnd_Tot_Pnd_Tot_Ctl_New_Cref_Payees, new ReportEditMask ("Z,ZZZ,ZZ9"));        //Natural: WRITE ( 1 ) 'CREF NEW PAYEES' 40X #TOT.#TOT-CTL-NEW-CREF-PAYEES ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NEW INACTIVE PAYEES",new ColumnSpacing(36),pnd_Tot_Pnd_Tot_Ctl_New_Inactive_Payees, new ReportEditMask                //Natural: WRITE ( 1 ) 'NEW INACTIVE PAYEES' 36X #TOT.#TOT-CTL-NEW-INACTIVE-PAYEES ( EM = Z,ZZZ,ZZ9 )
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  ======================
        //*   WRITE MONTHLY TOTALS
        //*  ======================
        pnd_L_Pnd_Annual.reset();                                                                                                                                         //Natural: RESET #ANNUAL
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  PROCESS TIAA REAL ESTATE MONTHLY
        if (condition(pnd_Index_Pnd_Index_09.notEquals(getZero())))                                                                                                       //Natural: IF #INDEX-09 NE 0
        {
            pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_Index_Pnd_Index_09.add(20));                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-09 + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS TIAA ACCESS MONTHLY                   /* 030209 START
        if (condition(pnd_Index_Pnd_Index_11.notEquals(getZero())))                                                                                                       //Natural: IF #INDEX-11 NE 0
        {
            pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_Index_Pnd_Index_11.add(20));                                   //Natural: ASSIGN #WRITE-INDEX := #INDEX-11 + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
            sub_Write_1_Line();
            if (condition(Global.isEscape())) {return;}
            //*  030209 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  SKIP TIAA STD,GRAD AND REA
            //*  030209
            if (condition(pnd_I.equals(pnd_Index_Pnd_Index_1g) || pnd_I.equals(pnd_Index_Pnd_Index_1s) || pnd_I.equals(pnd_Index_Pnd_Index_09) || pnd_I.equals(pnd_Index_Pnd_Index_11))) //Natural: IF #I = #INDEX-1G OR = #INDEX-1S OR = #INDEX-09 OR = #INDEX-11
            {
                //*  FOR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK THAT A FUND EXISTS
            if (condition(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_I.getDec().add(20)).notEquals(" ")))                                                          //Natural: IF IAXFR-CTL-FRM-FUND-CDE ( #I + 20 ) NE ' '
            {
                pnd_Index_Pnd_Write_Index.compute(new ComputeParameters(false, pnd_Index_Pnd_Write_Index), pnd_I.add(20));                                                //Natural: ASSIGN #WRITE-INDEX := #I + 20
                                                                                                                                                                          //Natural: PERFORM WRITE-1-LINE
                sub_Write_1_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   ADD-TRNSFR-SW-REPORT-TOTALS
    }
    private void sub_Write_1_Line() throws Exception                                                                                                                      //Natural: WRITE-1-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  THIS CODE ASSUMES THAT FOR ALL RECORDS ON THE FILE THE POSITIONS OF
        //*  A FUND REMAINS THE SAME.
        //*  IE:- I ONLY FETCH A FUND DESCRIPTION ONCE AND PLACE IT IN THE CORRECT
        //*  POSITION.
        if (condition(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).equals(" ") && pnd_L_Pnd_Annual.getBoolean()))                                                    //Natural: IF #FUND-DESC ( #WRITE-INDEX ) = ' ' AND #ANNUAL
        {
            pnd_Iaan050a_Pda_Pnd_Parm_Desc.reset();                                                                                                                       //Natural: RESET #PARM-DESC
            pnd_Iaan050a_Pda_Pnd_Parm_Fund_2.setValue(iaa_Xfr_Cntrl_Iaxfr_Ctl_Frm_Fund_Cde.getValue(pnd_Index_Pnd_Write_Index));                                          //Natural: ASSIGN #PARM-FUND-2 := IAXFR-CTL-FRM-FUND-CDE ( #WRITE-INDEX )
            pnd_Iaan050a_Pda_Pnd_Parm_Len.setValue(25);                                                                                                                   //Natural: ASSIGN #PARM-LEN := 25
            //*  CALLNAT 'IAAN050A'
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Iaan050a_Pda);                                                                                 //Natural: CALLNAT 'IAAN051A' #IAAN050A-PDA
            if (condition(Global.isEscape())) return;
            //*  TIAA FUNDS
            if (condition(pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1g) || pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1s)))                          //Natural: IF #WRITE-INDEX = #INDEX-1G OR = #INDEX-1S
            {
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                                               //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX ) := #PARM-DESC
                //*  ANNUAL
                //*  MONTHLY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                                               //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX ) := #PARM-DESC
                pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index.getDec().add(20)).setValue(pnd_Iaan050a_Pda_Pnd_Parm_Desc);                                              //Natural: ASSIGN #FUND-DESC ( #WRITE-INDEX + 20 ) := #PARM-DESC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "FUND RECORDS"));                                            //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) 'FUND RECORDS' INTO #WRITE-LINE.#DESC
        getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(58),pnd_Tot_Pnd_Tot_Ctl_Frm_Fund_Rcrds.getValue(pnd_Index_Pnd_Write_Index),     //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 58T #TOT.#TOT-CTL-FRM-FUND-RCRDS ( #WRITE-INDEX ) ( EM = Z,ZZZ,ZZ9 ) 87T #TOT.#TOT-CTL-TO-FUND-RCRDS ( #WRITE-INDEX ) ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(87),pnd_Tot_Pnd_Tot_Ctl_To_Fund_Rcrds.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  FOR TIAA STANDARD CONTRACTS
        //*  6/99 KN REMOVE TIAA STANDARD TEST
        //*  IF #INDEX-1S = #WRITE-INDEX
        //*   /*
        //*   COMPRESS #FUND-DESC(#WRITE-INDEX) 'NEW FUND RECS'
        //*     INTO #WRITE-LINE.#DESC
        //*   WRITE(1) #WRITE-LINE.#DESC
        //*     64T 'N/A'
        //*     87T #TOT.#TOT-CTL-NEW-FUND-RECS(#WRITE-INDEX)  (EM=Z,ZZZ,ZZ9)
        //*     115T #TOT.#TOT-CTL-NEW-FUND-RECS(#WRITE-INDEX)  (EM=Z,ZZZ,ZZ9)
        //*   /*
        //*  ELSE  /* FOR ALL OTHER CONTRACTS
        pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "100% OUT/NEW FUND RECS"));                                  //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) '100% OUT/NEW FUND RECS' INTO #WRITE-LINE.#DESC
        getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(58),pnd_Tot_Pnd_Tot_Ctl_Frm_Full_Out_Rcrds.getValue(pnd_Index_Pnd_Write_Index), //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 58T #TOT.#TOT-CTL-FRM-FULL-OUT-RCRDS ( #WRITE-INDEX ) ( EM = Z,ZZZ,ZZ9 ) 87T #TOT.#TOT-CTL-NEW-FUND-RECS ( #WRITE-INDEX ) ( EM = Z,ZZZ,ZZ9 ) 115T #NET-CTL-FULL-NEW ( #WRITE-INDEX ) ( EM = Z,ZZZ,ZZ9- )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(87),pnd_Tot_Pnd_Tot_Ctl_New_Fund_Recs.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(115),pnd_Tot_Pnd_Net_Ctl_Full_New.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask ("Z,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        //*  END-IF
        //*  FOR TIAA STANDARD AND GRADED CONTRACTS
        if (condition(pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1s) || pnd_Index_Pnd_Write_Index.equals(pnd_Index_Pnd_Index_1g)))                              //Natural: IF #WRITE-INDEX = #INDEX-1S OR = #INDEX-1G
        {
            pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "PERIODIC PAYMENTS"));                                   //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) 'PERIODIC PAYMENTS' INTO #WRITE-LINE.#DESC
            getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(53),pnd_Tot_Pnd_Tot_Ctl_Frm_Cntrctrl_Amt.getValue(pnd_Index_Pnd_Write_Index),  //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 53T #TOT.#TOT-CTL-FRM-CNTRCTRL-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 82T #TOT.#TOT-CTL-TO-CNTRCTRL-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 110T #NET-CTL-FT-CNTRCTRL-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Tot_Pnd_Tot_Ctl_To_Cntrctrl_Amt.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tot_Pnd_Net_Ctl_Ft_Cntrctrl_Amt.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "PERIODIC DIVIDENDS"));                                  //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) 'PERIODIC DIVIDENDS' INTO #WRITE-LINE.#DESC
            getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(53),pnd_Tot_Pnd_Tot_Ctl_Frm_Per_Divid_Amt.getValue(pnd_Index_Pnd_Write_Index),  //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 53T #TOT.#TOT-CTL-FRM-PER-DIVID-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 82T #TOT.#TOT-CTL-TO-PER-DIVID-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 110T #NET-CTL-FT-PER-DIVID-AMT ( #WRITE-INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Tot_Pnd_Tot_Ctl_To_Per_Divid_Amt.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tot_Pnd_Net_Ctl_Ft_Per_Divid_Amt.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            //*  FOR ALL OTHER FUNDS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "UNITS"));                                               //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) 'UNITS' INTO #WRITE-LINE.#DESC
            getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(53),pnd_Tot_Pnd_Tot_Ctl_Frm_Units.getValue(pnd_Index_Pnd_Write_Index),      //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 53T #TOT.#TOT-CTL-FRM-UNITS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.999 ) 82T #TOT.#TOT-CTL-TO-UNITS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.999 ) 110T #NET-CTL-FT-UNITS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new TabSetting(82),pnd_Tot_Pnd_Tot_Ctl_To_Units.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new TabSetting(110),pnd_Tot_Pnd_Net_Ctl_Ft_Units.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask ("ZZ,ZZZ,ZZ9.999-"));
            if (Global.isEscape()) return;
            pnd_Write_Line_Pnd_Desc.setValue(DbsUtil.compress(pnd_Fund_Desc.getValue(pnd_Index_Pnd_Write_Index), "AMOUNT"));                                              //Natural: COMPRESS #FUND-DESC ( #WRITE-INDEX ) 'AMOUNT' INTO #WRITE-LINE.#DESC
            getReports().write(1, ReportOption.NOTITLE,pnd_Write_Line_Pnd_Desc,new TabSetting(54),pnd_Tot_Pnd_Tot_Ctl_Frm_Dollars.getValue(pnd_Index_Pnd_Write_Index),    //Natural: WRITE ( 1 ) #WRITE-LINE.#DESC 54T #TOT.#TOT-CTL-FRM-DOLLARS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.99 ) 83T #TOT.#TOT-CTL-TO-DOLLARS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.99 ) 111T #NET-CTL-FT-DOLLARS ( #WRITE-INDEX ) ( EM = ZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new TabSetting(83),pnd_Tot_Pnd_Tot_Ctl_To_Dollars.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.99"),new TabSetting(111),pnd_Tot_Pnd_Net_Ctl_Ft_Dollars.getValue(pnd_Index_Pnd_Write_Index), new ReportEditMask ("ZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-1-LINE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN DATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new                 //Natural: WRITE ( 1 ) NOTITLE 01X 'RUN DATE : ' *DATX ( EM = MM/DD/YYYY ) 15X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBITY SYSTEM ' 113T 'PROGRAM : ' *PROGRAM
                        ColumnSpacing(15),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBITY SYSTEM ",new TabSetting(113),"PROGRAM : ",Global.getPROGRAM());
                    //*  SET SUB HEADINGS
                    if (condition(pnd_L_Pnd_Grand_Totals.getBoolean()))                                                                                                   //Natural: IF #GRAND-TOTALS
                    {
                        if (condition(pnd_L_Pnd_Annual.getBoolean()))                                                                                                     //Natural: IF #ANNUAL
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(15),pnd_Headings_Pnd_Grand_Tot_Annual,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 15X #GRAND-TOT-ANNUAL #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(15),pnd_Headings_Pnd_Grand_Tot_Monthly,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 15X #GRAND-TOT-MONTHLY #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  FOR TRANSFERS
                        if (condition(pnd_L_Pnd_Transfer.getBoolean()))                                                                                                   //Natural: IF #TRANSFER
                        {
                            if (condition(pnd_L_Pnd_Annual.getBoolean()))                                                                                                 //Natural: IF #ANNUAL
                            {
                                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(), new ReportEditMask ("HH:MM:SS"),new       //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX ( EM = HH:MM:SS ) 15X #TRANSFER-ANNUAL #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                    ColumnSpacing(15),pnd_Headings_Pnd_Transfer_Annual,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(), new ReportEditMask 
                                    ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                                //*  MONTHLY
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(15),pnd_Headings_Pnd_Transfer_Monthly,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 15X #TRANSFER-MONTHLY #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                    new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                            }                                                                                                                                             //Natural: END-IF
                            //*  SWITCHES
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_L_Pnd_Annual.getBoolean()))                                                                                                 //Natural: IF #ANNUAL
                            {
                                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(15),pnd_Headings_Pnd_Switch_From_Annual,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 15X #SWITCH-FROM-ANNUAL #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                    new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(15),pnd_Headings_Pnd_Switch_From_Monthly,pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Next_Bus_Dte(),  //Natural: WRITE ( 1 ) 01X 'RUN TIME : ' *TIMX 15X #SWITCH-FROM-MONTHLY #IATN400-OUT.CNTRL-NEXT-BUS-DTE ( EM = MM'/'DD'/'YYYY ) 113T 'PAGE    :  ' *PAGE-NUMBER ( 1 )
                                    new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(113),"PAGE    :  ",getReports().getPageNumberDbs(1));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SKIP(1) 1
                    //*  WRITE(1)
                    //*    01X 'EFFECTIVE DATE : ' IAXFR-CTL-EFFCTVE-DTE (EM=MM'/'DD'/'YYYY)
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(55),"TRANSFERRED/                TRANSFERRED/");                                         //Natural: WRITE ( 1 ) 55X 'TRANSFERRED/                TRANSFERRED/'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(55),"SWITCHED                    SWITCHED    ",new ColumnSpacing(16),                    //Natural: WRITE ( 1 ) 55X 'SWITCHED                    SWITCHED    ' 16X 'NET TRANSFER'
                        "NET TRANSFER");
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(57),"FROM (-)                      TO (+)",new ColumnSpacing(21),"(+OR-)",               //Natural: WRITE ( 1 ) 57X 'FROM (-)                      TO (+)' 21X '(+OR-)' /
                        NEWLINE);
                    //*  (1)
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
