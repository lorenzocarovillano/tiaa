/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:27:07 PM
**        * FROM NATURAL PROGRAM : Iaap399a
************************************************************
**        * FILE NAME            : Iaap399a.java
**        * CLASS NAME           : Iaap399a
**        * INSTANCE NAME        : Iaap399a
************************************************************
************************************************************************
* PROGRAM  : IAAP399A
* SYSTEM   : IA
* TITLE    : FILE REFORMATTER AND SPLITTER PROGRAM
* FUNCTION : THIS MODULE CREATES 5 OUTPUT FILES FROM ONE INPUT FILE
*          : BASED ON THE RECORD TYPE OF THE RECORDS (00,10,20,30,40).
*
* DETAILS  : IN ADDITION TO SPLITTING THE INPUT RECORDS AMONG 5 FILES,
*          : CERTAIN INCOMING DATA FIELDS ARE REFORMATTED AS BELOW:
*          :     1) PACKED DECIMAL FIELDS ARE MOVED TO FIXED DECIMAL
*          :        FIELDS WITH THE SAME NUMBER OF DIGITS.
*          :     2) ANY DATE AND TIME FIELDS IN D / T FORMAT ARE
*          :        CONVERTED TO YYYYMMDD FORMAT.
*          :
*          : AFTER THE FILES ARE CREATED, OTHER JOBS WILL NDM THEM
*          : TO DATASTAGE FOR FURTHER DATAWAREHOUSE PROCESSING.
*          :
*
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J. CRUZ  06/28/12 COMPLETED INITIAL VERSION OF PROGRAM
* J. TINIO 09/16/16 RECATALOGED FOR PIN EXPANSION
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap399a extends BLNatBase
{
    // Data Areas
    private LdaIaal399x ldaIaal399x;
    private LdaIaal399y ldaIaal399y;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Out_500;
    private DbsField pnd_Buff_372;

    private DbsGroup pnd_Buff_372__R_Field_1;
    private DbsField pnd_Buff_372_Pnd_B367_Filler;
    private DbsField pnd_Buff_372_Pnd_B367_Rectype;
    private DbsField pnd_Delim;
    private DbsField pnd_Nonzero_Cnt;
    private DbsField pnd_Label;
    private DbsField pnd_I;
    private DbsField pnd_Inpt;
    private DbsField pnd_Cnt1;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Cnt4;
    private DbsField pnd_Cnt5;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal399x = new LdaIaal399x();
        registerRecord(ldaIaal399x);
        ldaIaal399y = new LdaIaal399y();
        registerRecord(ldaIaal399y);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Out_500 = localVariables.newFieldInRecord("pnd_Out_500", "#OUT-500", FieldType.STRING, 500);
        pnd_Buff_372 = localVariables.newFieldInRecord("pnd_Buff_372", "#BUFF-372", FieldType.STRING, 372);

        pnd_Buff_372__R_Field_1 = localVariables.newGroupInRecord("pnd_Buff_372__R_Field_1", "REDEFINE", pnd_Buff_372);
        pnd_Buff_372_Pnd_B367_Filler = pnd_Buff_372__R_Field_1.newFieldInGroup("pnd_Buff_372_Pnd_B367_Filler", "#B367-FILLER", FieldType.STRING, 12);
        pnd_Buff_372_Pnd_B367_Rectype = pnd_Buff_372__R_Field_1.newFieldInGroup("pnd_Buff_372_Pnd_B367_Rectype", "#B367-RECTYPE", FieldType.STRING, 2);
        pnd_Delim = localVariables.newFieldInRecord("pnd_Delim", "#DELIM", FieldType.STRING, 2);
        pnd_Nonzero_Cnt = localVariables.newFieldInRecord("pnd_Nonzero_Cnt", "#NONZERO-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Label = localVariables.newFieldInRecord("pnd_Label", "#LABEL", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Inpt = localVariables.newFieldInRecord("pnd_Inpt", "#INPT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt1 = localVariables.newFieldInRecord("pnd_Cnt1", "#CNT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt4 = localVariables.newFieldInRecord("pnd_Cnt4", "#CNT4", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt5 = localVariables.newFieldInRecord("pnd_Cnt5", "#CNT5", FieldType.PACKED_DECIMAL, 7);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal399x.initializeValues();
        ldaIaal399y.initializeValues();

        localVariables.reset();
        pnd_Delim.setInitialValue("||");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap399a() throws Exception
    {
        super("Iaap399a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 133
        READWORK01:                                                                                                                                                       //Natural: READ WORK 06 #BUFF-372
        while (condition(getWorkFiles().read(6, pnd_Buff_372)))
        {
            pnd_Inpt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #INPT
            short decideConditionsMet390 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #B367-RECTYPE;//Natural: VALUE '10'
            if (condition((pnd_Buff_372_Pnd_B367_Rectype.equals("10"))))
            {
                decideConditionsMet390++;
                ldaIaal399x.getPnd_Work_Record_10_Pnd_Work_Record_10_Data().setValue(pnd_Buff_372);                                                                       //Natural: ASSIGN #WORK-RECORD-10-DATA := #BUFF-372
                                                                                                                                                                          //Natural: PERFORM REFORMAT-10
                sub_Reformat_10();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      COMPRESS #WORK-RECORD-10Y-DATA #DELIM INTO #OUT-500 LEAVING NO
                getWorkFiles().write(1, false, ldaIaal399y.getPnd_Work_Record_10y_Pnd_Work_Record_10y_Data());                                                            //Natural: WRITE WORK FILE 01 #WORK-RECORD-10Y-DATA
                pnd_Cnt1.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT1
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((pnd_Buff_372_Pnd_B367_Rectype.equals("20"))))
            {
                decideConditionsMet390++;
                ldaIaal399x.getPnd_Work_Record_20_Pnd_Work_Record_20_Data().setValue(pnd_Buff_372);                                                                       //Natural: ASSIGN #WORK-RECORD-20-DATA := #BUFF-372
                                                                                                                                                                          //Natural: PERFORM REFORMAT-20
                sub_Reformat_20();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      COMPRESS #WORK-RECORD-20Y-DATA #DELIM INTO #OUT-500 LEAVING NO
                getWorkFiles().write(2, false, ldaIaal399y.getPnd_Work_Record_20y_Pnd_Work_Record_20y_Data());                                                            //Natural: WRITE WORK FILE 02 #WORK-RECORD-20Y-DATA
                pnd_Cnt2.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT2
            }                                                                                                                                                             //Natural: VALUE '30'
            else if (condition((pnd_Buff_372_Pnd_B367_Rectype.equals("30"))))
            {
                decideConditionsMet390++;
                ldaIaal399x.getPnd_Work_Record_30_Pnd_Work_Record_30_Data().setValue(pnd_Buff_372);                                                                       //Natural: ASSIGN #WORK-RECORD-30-DATA := #BUFF-372
                                                                                                                                                                          //Natural: PERFORM REFORMAT-30
                sub_Reformat_30();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      COMPRESS #WORK-RECORD-30Y-DATA #DELIM INTO #OUT-500 LEAVING NO
                getWorkFiles().write(3, false, ldaIaal399y.getPnd_Work_Record_30y_Pnd_Work_Record_30y_Data());                                                            //Natural: WRITE WORK FILE 03 #WORK-RECORD-30Y-DATA
                pnd_Cnt3.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT3
            }                                                                                                                                                             //Natural: VALUE '40'
            else if (condition((pnd_Buff_372_Pnd_B367_Rectype.equals("40"))))
            {
                decideConditionsMet390++;
                ldaIaal399x.getPnd_Work_Record_40_Pnd_Work_Record_40_Data().setValue(pnd_Buff_372);                                                                       //Natural: ASSIGN #WORK-RECORD-40-DATA := #BUFF-372
                                                                                                                                                                          //Natural: PERFORM REFORMAT-40
                sub_Reformat_40();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      COMPRESS #WORK-RECORD-40Y-DATA #DELIM INTO #OUT-500 LEAVING NO
                getWorkFiles().write(4, false, ldaIaal399y.getPnd_Work_Record_40y_Pnd_Work_Record_40y_Data());                                                            //Natural: WRITE WORK FILE 04 #WORK-RECORD-40Y-DATA
                pnd_Cnt4.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT4
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //*      COMPRESS #BUFF-367 #DELIM INTO #OUT-500 LEAVING NO
                getWorkFiles().write(5, false, pnd_Buff_372);                                                                                                             //Natural: WRITE WORK FILE 05 #BUFF-372
                pnd_Cnt5.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT5
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, pnd_Inpt,"INPUT RECORDS WERE PROCESSED",NEWLINE,NEWLINE,NEWLINE,NEWLINE,pnd_Cnt1,"TYPE 10 RECORDS SELECTED FOR OUTPUT",NEWLINE,             //Natural: WRITE #INPT 'INPUT RECORDS WERE PROCESSED' // // #CNT1 'TYPE 10 RECORDS SELECTED FOR OUTPUT' // #CNT2 'TYPE 20 RECORDS SELECTED FOR OUTPUT' // #CNT3 'TYPE 30 RECORDS SELECTED FOR OUTPUT' // #CNT4 'TYPE 40 RECORDS SELECTED FOR OUTPUT' // #CNT5 'HEADER/TRAILERS SELECTED FOR OUTPUT'
            NEWLINE,pnd_Cnt2,"TYPE 20 RECORDS SELECTED FOR OUTPUT",NEWLINE,NEWLINE,pnd_Cnt3,"TYPE 30 RECORDS SELECTED FOR OUTPUT",NEWLINE,NEWLINE,pnd_Cnt4,
            "TYPE 40 RECORDS SELECTED FOR OUTPUT",NEWLINE,NEWLINE,pnd_Cnt5,"HEADER/TRAILERS SELECTED FOR OUTPUT");
        if (Global.isEscape()) return;
        //* **
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-10
        //* **
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-20
        //* **
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-30
        //* **
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-40
        //* ***
    }
    private void sub_Reformat_10() throws Exception                                                                                                                       //Natural: REFORMAT-10
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaIaal399y.getPnd_Work_Record_10y().reset();                                                                                                                     //Natural: RESET #WORK-RECORD-10Y
        ldaIaal399y.getPnd_Work_Record_10y().setValuesByName(ldaIaal399x.getPnd_Work_Record_10());                                                                        //Natural: MOVE BY NAME #WORK-RECORD-10 TO #WORK-RECORD-10Y
        //*  FOR #I = 1 TO 5
        //*    IF #WORK-RECORD-10.#W10-CNTRCT-FNL-PRM-DTE(#I) LE 0
        //*      MOVE '00000000' TO #WORK-RECORD-10Y.#W10-CNTRCT-FNL-PRM-DTE-A8(#I)
        //*    ELSE
        //*    MOVE EDITED #WORK-RECORD-10.#W10-CNTRCT-FNL-PRM-DTE(#I)(EM=YYYYMMDD)
        //*      TO #WORK-RECORD-10Y.#W10-CNTRCT-FNL-PRM-DTE-A8(#I)
        //*    END-IF
        //*  END-FOR
        //*  IF #WORK-RECORD-10.#W10-CNTRCT-ANNTY-STRT-DTE GT 0
        //*    MOVE EDITED #WORK-RECORD-10.#W10-CNTRCT-ANNTY-STRT-DTE (EM=YYYYMMDD)
        //*      TO #WORK-RECORD-10Y.#W10-CNTRCT-ANNTY-STRT-DTE-A8
        //*  ELSE
        //*    MOVE '00000000' TO #WORK-RECORD-10Y.#W10-CNTRCT-ANNTY-STRT-DTE-A8
        //*  END-IF
    }
    private void sub_Reformat_20() throws Exception                                                                                                                       //Natural: REFORMAT-20
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaIaal399y.getPnd_Work_Record_20y().reset();                                                                                                                     //Natural: RESET #WORK-RECORD-20Y
        ldaIaal399y.getPnd_Work_Record_20y().setValuesByName(ldaIaal399x.getPnd_Work_Record_20());                                                                        //Natural: MOVE BY NAME #WORK-RECORD-20 TO #WORK-RECORD-20Y
        //*  IF #WORK-RECORD-20.#W20-LST-TRANS-DTE GT 0
        //*    MOVE EDITED #WORK-RECORD-20.#W20-LST-TRANS-DTE (EM=YYYYMMDD)
        //*      TO #WORK-RECORD-20Y.#W20-LST-TRANS-DTE-A8
        //*  ELSE
        //*    MOVE '00000000' TO #WORK-RECORD-20Y.#W20-LST-TRANS-DTE-A8
        //*  END-IF
        //*  IF #WORK-RECORD-20.#W20-CPR-XFR-ISS-DTE GT 0
        //*    MOVE EDITED #WORK-RECORD-20.#W20-CPR-XFR-ISS-DTE (EM=YYYYMMDD)
        //*      TO #WORK-RECORD-20Y.#W20-CPR-XFR-ISS-DTE-A8
        //*  ELSE
        //*    MOVE '00000000' TO #WORK-RECORD-20Y.#W20-CPR-XFR-ISS-DTE-A8
        //*  END-IF
    }
    private void sub_Reformat_30() throws Exception                                                                                                                       //Natural: REFORMAT-30
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaIaal399y.getPnd_Work_Record_30y().reset();                                                                                                                     //Natural: RESET #WORK-RECORD-30Y
        ldaIaal399y.getPnd_Work_Record_30y().setValuesByName(ldaIaal399x.getPnd_Work_Record_30());                                                                        //Natural: MOVE BY NAME #WORK-RECORD-30 TO #WORK-RECORD-30Y
    }
    private void sub_Reformat_40() throws Exception                                                                                                                       //Natural: REFORMAT-40
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaIaal399y.getPnd_Work_Record_40y().reset();                                                                                                                     //Natural: RESET #WORK-RECORD-40Y
        ldaIaal399y.getPnd_Work_Record_40y().setValuesByName(ldaIaal399x.getPnd_Work_Record_40());                                                                        //Natural: MOVE BY NAME #WORK-RECORD-40 TO #WORK-RECORD-40Y
        //*  IF #WORK-RECORD-40.#W40-LST-TRANS-DTE GT 0
        //*    MOVE EDITED #WORK-RECORD-40.#W40-LST-TRANS-DTE (EM=YYYYMMDD)
        //*      TO #WORK-RECORD-40Y.#W40-LST-TRANS-DTE-A8
        //*  ELSE
        //*    MOVE '00000000' TO #WORK-RECORD-40Y.#W40-LST-TRANS-DTE-A8
        //*  END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=133");
    }
}
