/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:32:25 PM
**        * FROM NATURAL PROGRAM : Iaap795
************************************************************
**        * FILE NAME            : Iaap795.java
**        * CLASS NAME           : Iaap795
**        * INSTANCE NAME        : Iaap795
************************************************************
************************************************************************
*
* PROGRAM:- IAAP795
* DATE   :- 02/21/96
* AUTHOR :- JEFF BERINGER
*
* HISTORY
* -------
* KN MODIFIED ON 06/99    - CHANGED FOR TIIA TO CREF TRANS (512-518)
*
* KN MODIFIED ON 11/98    - CHANGED CALCULATION USED TO OBTAIN
*                         - INPUT CONTROL RECORD
* RM MODIFIED ON 05/01/98 - CHANGED LOGIC FOR 102 TRANS
*                         - NOT TO RESET BFR AMTS TO PREVENT OUT/BAL
*                         - ADDED 053 SAME AS 035
*                           DO SCAN ON 5/98
* KB MODIFIED ON 05/15/96 - ADDED 051 SAME AS 050
*                         - ADDED 052 SAME AS 020
*                         - ADDED 053 SAME AS 035
*                         - ADDED 036 SAME AS 033
* AG MODIFIED ON 05/06/97 - ADDED 030 - SAME LOGIC AS 031
*                         - ADDED 060 061 062 - SAME LOGIC AS 050
*                         - REWROTE PROGRAM
* LEN B ON 01/07/97       - ADDED TRANS CODE = 520 (FOR SWITHES)
*                           THIS IS THE SAME AS TRANS CODE 500
*
*  DO SCAN ON 1/98
*
*  07/09/2002             - REMOVE THE HARD CODED PRODUCTS IN ERROR ROUT
*
*  09/23/2008             - ADD TIAA ACCESS FUND     DO SCAN ON 9/08
*
*  06/05/2009             - ADJUST COLUMN FOR FINAL PAYMENT   06/09
*  03/27/2012             - RATE BASE EXPANSION    SCAN ON 3/12
*  09/19/2012             - ADJUST COLUMN FOR UNITS-CNT    9/12
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap795 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte;

    private DbsGroup pnd_Work_Record_1__R_Field_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Date;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W_Sub_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Activity_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Sub_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Work_Record_1__R_Field_2;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde;

    private DbsGroup pnd_Work_Record_1__R_Field_3;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde;

    private DbsGroup pnd_Work_Record_1__R_Field_4;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde_Num;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Critical_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W_Company_Cd;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Payment;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Dividend;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Payment_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Dividend_B;
    private DbsField iaan051a_Fund;
    private DbsField iaan051a_Desc;
    private DbsField iaan051a_Cmpy_Desc;
    private DbsField iaan051a_Length;
    private DbsField pnd_Text;
    private DbsField pnd_Total_Cref_Products;
    private DbsField pnd_Total_Cref_Sw;
    private DbsField pnd_Cntrl_Fund_Cde;

    private DbsGroup pnd_Cntrl_Fund_Cde__R_Field_5;
    private DbsField pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1;
    private DbsField pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Mode_Hold;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_6;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Pnd_Date_Ccyy;
    private DbsField pnd_C_Trans_Check_Dte;

    private DbsGroup pnd_C_Trans_Check_Dte__R_Field_7;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Test_Prod;
    private DbsField pnd_Total_Test_Read;
    private DbsField pnd_Total_Records_Read;
    private DbsField pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Mode_Code;
    private DbsField pnd_Search_Key_Rcrd;

    private DbsGroup pnd_Search_Key_Rcrd__R_Field_8;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte;
    private DbsField pnd_Product_Name_Rpt3;

    private DbsGroup pnd_Product_Name_Rpt3__R_Field_9;
    private DbsField pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod;
    private DbsField pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode;
    private DbsField pnd_Rept1_In_Out;
    private DbsField pnd_Rept1_Net_Per_Pmt;
    private DbsField pnd_Rept1_Net_Per_Dvd;
    private DbsField pnd_Rept1_Net_Final_Pmt;
    private DbsField pnd_Rept1_Net_Final_Divd;
    private DbsField pnd_Rept1_Net_Units;
    private DbsField pnd_Rept2_Prod_Titles_T;
    private DbsField pnd_Rept2_Prod_Titles_C;
    private DbsField pnd_R_Prod_Titles_C;

    private DbsGroup pnd_R_Prod_Titles_C__R_Field_10;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler;
    private DbsField pnd_Rept2_Prod_Titles_2_C;
    private DbsField pnd_R_Prod_Titles_2_C;

    private DbsGroup pnd_R_Prod_Titles_2_C__R_Field_11;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler;
    private DbsField pnd_Rept2_Mode;
    private DbsField pnd_Rept2_Tot_Per_Pmt_In;
    private DbsField pnd_Rept2_Tot_Per_Dvd_In;
    private DbsField pnd_Rept2_Tot_Fin_Pay_In;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_In;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_In;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_In;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_In;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_In;
    private DbsField pnd_Rept2_Tot_Units_Prod_In;
    private DbsField pnd_Rept2_Tot_Units_Pymt_In;
    private DbsField pnd_Rept2_Tot_Units_Prod_Out;
    private DbsField pnd_Rept2_Tot_Units_Pymt_Out;
    private DbsField pnd_Rept2_Tot_Units_Prod_Ct;
    private DbsField pnd_Rept2_Tot_Units_Pymt_Ct;
    private DbsField pnd_Rept2_Tot_Unit_Prod_N_Crit;
    private DbsField pnd_Rept2_Tot_Unit_Pymt_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Units_Prod_In;
    private DbsField pnd_Rept2_G_Tot_Units_Pymt_In;
    private DbsField pnd_Rept2_G_Tot_Units_Prod_Out;
    private DbsField pnd_Rept2_G_Tot_Units_Pymt_Out;
    private DbsField pnd_Rept2_G_Tot_Units_Prod_Ct;
    private DbsField pnd_Rept2_G_Tot_Units_Pymt_Ct;
    private DbsField pnd_Rept2_G_Tot_Unit_Prod_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Unit_Pymt_N_Crit;
    private DbsField pnd_Rept2_Total_Units_Prod_In_M;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_M;
    private DbsField pnd_Rept2_Total_Units_Prod_Ct_M;
    private DbsField pnd_Rept2_Total_Units_Pymt_Ct_M;
    private DbsField pnd_Rept2_Total_Units_Prod_In_M_P;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_M_P;
    private DbsField pnd_Rept2_Total_Units_Prod_Ct_M_P;
    private DbsField pnd_Rept2_Total_Units_Pymt_Ct_M_P;
    private DbsField pnd_Rept2_Total_Units_Prod_In_A;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_A;
    private DbsField pnd_Rept2_Total_Units_Prod_Ct_A;
    private DbsField pnd_Rept2_Total_Units_Pymt_Ct_A;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_A;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_A;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Crit_A;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Crit_A;
    private DbsField pnd_Rept2_Total_Units_Prod_In_A_P;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_A_P;
    private DbsField pnd_Rept2_Total_Units_Prod_Ct_A_P;
    private DbsField pnd_Rept2_Total_Units_Pymt_Ct_A_P;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_A_P;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_A_P;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Critap;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Critap;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_M;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_M;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Crit_M;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Crit_M;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_M_P;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_M_P;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Critmp;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Critmp;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_A;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_A;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Ct_A;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Ct_A;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_A_P;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_A_P;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Ct_A_P;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Ct_A_P;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_M;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_M;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Ct_M;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Ct_M;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_M_P;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_M_P;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Ct_M_P;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Ct_M_P;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_A;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_A;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Crita;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Crita;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_Ap;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_Ap;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Criap;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Criap;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_M;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_M;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Critm;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Critm;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_Mp;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_Mp;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Crimp;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Crimp;
    private DbsField pnd_Rept2_Tot_Per_Pmt_Out;
    private DbsField pnd_Rept2_Tot_Per_Dvd_Out;
    private DbsField pnd_Rept2_Tot_Per_Pmt_Ct;
    private DbsField pnd_Rept2_Tot_Per_Dvd_Ct;
    private DbsField pnd_Rept2_Tot_Per_Pmt_N_Crit;
    private DbsField pnd_Rept2_Tot_Per_Dvd_N_Crit;
    private DbsField pnd_Rept2_Tot_Fin_Pay_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_Ct;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_Out;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_Out;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_Ct;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_N_Crit;
    private DbsField pnd_Rept2_Tot_Fin_Pay_Out;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_Out;
    private DbsField pnd_Rept2_Tot_Fin_Pay_Ct;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_Ct;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_Ct;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_Out;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_Out;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_Ct;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_N_Crit;
    private DbsField pnd_Rept2_Tot_Diff;
    private DbsField pnd_Rept2_Tot_Diff_Unit;
    private DbsField pnd_Rept2_Tot_Diff_Unit_Pymt;
    private DbsField pnd_Rept2_Title_Sub;
    private DbsField pnd_Rept2_Mode_Sub;
    private DbsField pnd_Rept2_Cntl_Sub;
    private DbsField pnd_Rept2_Prod_Sub;
    private DbsField pnd_Prod_Sub;
    private DbsField pnd_W_Print_Sw;
    private DbsField pnd_Seperator;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Frst_Trans_Dte", 
            "CNTRL-FRST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_VIEW_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt", 
            "CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt", 
            "CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees", 
            "CNTRL-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", 
            "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units", "CNTRL-UNITS", 
            FieldType.PACKED_DECIMAL, 13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Amt = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Tiaa_Pys", 
            "CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Actve_Cref_Pys", 
            "CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Inactve_Payees", 
            "CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte", "#W-TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_Record_1__R_Field_1 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_1", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Check_Dte);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy", "#W-TRANS-CHECK-DTE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm", "#W-TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd", "#W-TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Date = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Date", "#W-TRANS-DATE", FieldType.TIME);
        pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind", "#W-CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_1_Pnd_W_Trans_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Cde", "#W-TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_1_Pnd_W_Sub_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Sub_Code", "#W-SUB-CODE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W_Activity_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Activity_Code", "#W-ACTIVITY-CODE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde", "#W-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte", "#W-CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W_Trans_Sub_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Sub_Code", "#W-TRANS-SUB-CODE", FieldType.STRING, 
            3);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr", "#W-TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Work_Record_1__R_Field_2 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_2", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1 = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1", "#W-TRANS-PPCN-NBR-1", 
            FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2 = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2", "#W-TRANS-PPCN-NBR-2", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde", "#W-TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Work_Record_1__R_Field_3 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_3", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde);
        pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A", "#W-TRANS-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W_Product_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde", "#W-PRODUCT-CDE", FieldType.STRING, 
            2);

        pnd_Work_Record_1__R_Field_4 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_4", "REDEFINE", pnd_Work_Record_1_Pnd_W_Product_Cde);
        pnd_Work_Record_1_Pnd_W_Product_Cde_Num = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde_Num", "#W-PRODUCT-CDE-NUM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt", "#W-TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt", "#W-TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt", "#W-CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B", "#W-TIAA-TOT-PER-AMT-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B", "#W-TIAA-TOT-DIV-AMT-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B", "#W-CREF-UNITS-CNT-B", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Work_Record_1_Pnd_W_Critical_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Critical_Ind", "#W-CRITICAL-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Company_Cd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Company_Cd", "#W-COMPANY-CD", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Final_Payment = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Payment", "#W-FINAL-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W_Final_Dividend = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Dividend", "#W-FINAL-DIVIDEND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W_Final_Payment_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Payment_B", "#W-FINAL-PAYMENT-B", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W_Final_Dividend_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Dividend_B", "#W-FINAL-DIVIDEND-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        iaan051a_Fund = localVariables.newFieldInRecord("iaan051a_Fund", "IAAN051A-FUND", FieldType.STRING, 2);
        iaan051a_Desc = localVariables.newFieldInRecord("iaan051a_Desc", "IAAN051A-DESC", FieldType.STRING, 35);
        iaan051a_Cmpy_Desc = localVariables.newFieldInRecord("iaan051a_Cmpy_Desc", "IAAN051A-CMPY-DESC", FieldType.STRING, 4);
        iaan051a_Length = localVariables.newFieldInRecord("iaan051a_Length", "IAAN051A-LENGTH", FieldType.PACKED_DECIMAL, 3);
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 10);
        pnd_Total_Cref_Products = localVariables.newFieldInRecord("pnd_Total_Cref_Products", "#TOTAL-CREF-PRODUCTS", FieldType.NUMERIC, 3);
        pnd_Total_Cref_Sw = localVariables.newFieldInRecord("pnd_Total_Cref_Sw", "#TOTAL-CREF-SW", FieldType.STRING, 1);
        pnd_Cntrl_Fund_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Fund_Cde", "#CNTRL-FUND-CDE", FieldType.STRING, 3);

        pnd_Cntrl_Fund_Cde__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrl_Fund_Cde__R_Field_5", "REDEFINE", pnd_Cntrl_Fund_Cde);
        pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1 = pnd_Cntrl_Fund_Cde__R_Field_5.newFieldInGroup("pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1", "#CNTRL-FUND-CDE-1", 
            FieldType.STRING, 1);
        pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2 = pnd_Cntrl_Fund_Cde__R_Field_5.newFieldInGroup("pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2", "#CNTRL-FUND-CDE-2", 
            FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Mode_Hold = localVariables.newFieldInRecord("pnd_Mode_Hold", "#MODE-HOLD", FieldType.NUMERIC, 3);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Date__R_Field_6", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_C_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_C_Trans_Check_Dte", "#C-TRANS-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_C_Trans_Check_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_C_Trans_Check_Dte__R_Field_7", "REDEFINE", pnd_C_Trans_Check_Dte);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy = pnd_C_Trans_Check_Dte__R_Field_7.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy", 
            "#TRANS-CHECK-DTE-CCYY", FieldType.NUMERIC, 4);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm = pnd_C_Trans_Check_Dte__R_Field_7.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm", 
            "#TRANS-CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd = pnd_C_Trans_Check_Dte__R_Field_7.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd", 
            "#TRANS-CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Test_Prod = localVariables.newFieldInRecord("pnd_Test_Prod", "#TEST-PROD", FieldType.NUMERIC, 1);
        pnd_Total_Test_Read = localVariables.newFieldInRecord("pnd_Total_Test_Read", "#TOTAL-TEST-READ", FieldType.NUMERIC, 9);
        pnd_Total_Records_Read = localVariables.newFieldInRecord("pnd_Total_Records_Read", "#TOTAL-RECORDS-READ", FieldType.NUMERIC, 7);
        pnd_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 11);
        pnd_Mode_Code = localVariables.newFieldInRecord("pnd_Mode_Code", "#MODE-CODE", FieldType.STRING, 2);
        pnd_Search_Key_Rcrd = localVariables.newFieldInRecord("pnd_Search_Key_Rcrd", "#SEARCH-KEY-RCRD", FieldType.STRING, 10);

        pnd_Search_Key_Rcrd__R_Field_8 = localVariables.newGroupInRecord("pnd_Search_Key_Rcrd__R_Field_8", "REDEFINE", pnd_Search_Key_Rcrd);
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde = pnd_Search_Key_Rcrd__R_Field_8.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde", "#SEARCH-CNTRL-CDE", 
            FieldType.STRING, 2);
        pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte = pnd_Search_Key_Rcrd__R_Field_8.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte", "#SEARCH-TRANS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Product_Name_Rpt3 = localVariables.newFieldInRecord("pnd_Product_Name_Rpt3", "#PRODUCT-NAME-RPT3", FieldType.STRING, 5);

        pnd_Product_Name_Rpt3__R_Field_9 = localVariables.newGroupInRecord("pnd_Product_Name_Rpt3__R_Field_9", "REDEFINE", pnd_Product_Name_Rpt3);
        pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod = pnd_Product_Name_Rpt3__R_Field_9.newFieldInGroup("pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod", 
            "#PRODUCT-NAME-RPT3-PROD", FieldType.STRING, 3);
        pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode = pnd_Product_Name_Rpt3__R_Field_9.newFieldInGroup("pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode", 
            "#PRODUCT-NAME-RPT3-MODE", FieldType.STRING, 2);
        pnd_Rept1_In_Out = localVariables.newFieldInRecord("pnd_Rept1_In_Out", "#REPT1-IN-OUT", FieldType.STRING, 4);
        pnd_Rept1_Net_Per_Pmt = localVariables.newFieldInRecord("pnd_Rept1_Net_Per_Pmt", "#REPT1-NET-PER-PMT", FieldType.NUMERIC, 9, 2);
        pnd_Rept1_Net_Per_Dvd = localVariables.newFieldInRecord("pnd_Rept1_Net_Per_Dvd", "#REPT1-NET-PER-DVD", FieldType.NUMERIC, 9, 2);
        pnd_Rept1_Net_Final_Pmt = localVariables.newFieldInRecord("pnd_Rept1_Net_Final_Pmt", "#REPT1-NET-FINAL-PMT", FieldType.NUMERIC, 9, 2);
        pnd_Rept1_Net_Final_Divd = localVariables.newFieldInRecord("pnd_Rept1_Net_Final_Divd", "#REPT1-NET-FINAL-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_Rept1_Net_Units = localVariables.newFieldInRecord("pnd_Rept1_Net_Units", "#REPT1-NET-UNITS", FieldType.NUMERIC, 9, 3);
        pnd_Rept2_Prod_Titles_T = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_T", "#REPT2-PROD-TITLES-T", FieldType.STRING, 23, new DbsArrayController(1, 
            4));
        pnd_Rept2_Prod_Titles_C = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_C", "#REPT2-PROD-TITLES-C", FieldType.STRING, 23, new DbsArrayController(1, 
            84));
        pnd_R_Prod_Titles_C = localVariables.newFieldInRecord("pnd_R_Prod_Titles_C", "#R-PROD-TITLES-C", FieldType.STRING, 23);

        pnd_R_Prod_Titles_C__R_Field_10 = localVariables.newGroupInRecord("pnd_R_Prod_Titles_C__R_Field_10", "REDEFINE", pnd_R_Prod_Titles_C);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co = pnd_R_Prod_Titles_C__R_Field_10.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co", "#RPT-TITLE-C-CO", 
            FieldType.STRING, 5);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod = pnd_R_Prod_Titles_C__R_Field_10.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod", "#RPT-TITLE-C-PROD", 
            FieldType.STRING, 4);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units = pnd_R_Prod_Titles_C__R_Field_10.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units", "#RPT-TITLE-C-UNITS", 
            FieldType.STRING, 8);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler = pnd_R_Prod_Titles_C__R_Field_10.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler", "#RPT-TITLE-C-FILLER", 
            FieldType.STRING, 6);
        pnd_Rept2_Prod_Titles_2_C = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_2_C", "#REPT2-PROD-TITLES-2-C", FieldType.STRING, 23, 
            new DbsArrayController(1, 84));
        pnd_R_Prod_Titles_2_C = localVariables.newFieldInRecord("pnd_R_Prod_Titles_2_C", "#R-PROD-TITLES-2-C", FieldType.STRING, 23);

        pnd_R_Prod_Titles_2_C__R_Field_11 = localVariables.newGroupInRecord("pnd_R_Prod_Titles_2_C__R_Field_11", "REDEFINE", pnd_R_Prod_Titles_2_C);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co = pnd_R_Prod_Titles_2_C__R_Field_11.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co", "#RPT-TITLE-2-C-CO", 
            FieldType.STRING, 5);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod = pnd_R_Prod_Titles_2_C__R_Field_11.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod", 
            "#RPT-TITLE-2-C-PROD", FieldType.STRING, 4);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments = pnd_R_Prod_Titles_2_C__R_Field_11.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments", 
            "#RPT-TITLE-2-C-PAYMENTS", FieldType.STRING, 8);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler = pnd_R_Prod_Titles_2_C__R_Field_11.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler", 
            "#RPT-TITLE-2-C-FILLER", FieldType.STRING, 6);
        pnd_Rept2_Mode = localVariables.newFieldInRecord("pnd_Rept2_Mode", "#REPT2-MODE", FieldType.STRING, 5);
        pnd_Rept2_Tot_Per_Pmt_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_In", "#REPT2-TOT-PER-PMT-IN", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_In", "#REPT2-TOT-PER-DVD-IN", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Fin_Pay_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_In", "#REPT2-TOT-FIN-PAY-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_In", "#REPT2-TOT-FIN-DVD-IN", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Tot_Per_Pmt_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_In", "#REPT2-G-TOT-PER-PMT-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_In", "#REPT2-G-TOT-PER-DVD-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Pay_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_In", "#REPT2-G-TOT-FIN-PAY-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_In", "#REPT2-G-TOT-FIN-DVD-IN", FieldType.NUMERIC, 12, 2);
        pnd_Rept2_Tot_Units_Prod_In = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Prod_In", "#REPT2-TOT-UNITS-PROD-IN", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Units_Pymt_In = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Pymt_In", "#REPT2-TOT-UNITS-PYMT-IN", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Units_Prod_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Prod_Out", "#REPT2-TOT-UNITS-PROD-OUT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Units_Pymt_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Pymt_Out", "#REPT2-TOT-UNITS-PYMT-OUT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Units_Prod_Ct = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Prod_Ct", "#REPT2-TOT-UNITS-PROD-CT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Units_Pymt_Ct = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Pymt_Ct", "#REPT2-TOT-UNITS-PYMT-CT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Unit_Prod_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Unit_Prod_N_Crit", "#REPT2-TOT-UNIT-PROD-N-CRIT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_Tot_Unit_Pymt_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Unit_Pymt_N_Crit", "#REPT2-TOT-UNIT-PYMT-N-CRIT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Prod_In = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Prod_In", "#REPT2-G-TOT-UNITS-PROD-IN", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Pymt_In = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Pymt_In", "#REPT2-G-TOT-UNITS-PYMT-IN", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Prod_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Prod_Out", "#REPT2-G-TOT-UNITS-PROD-OUT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Pymt_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Pymt_Out", "#REPT2-G-TOT-UNITS-PYMT-OUT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Prod_Ct = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Prod_Ct", "#REPT2-G-TOT-UNITS-PROD-CT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Units_Pymt_Ct = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Pymt_Ct", "#REPT2-G-TOT-UNITS-PYMT-CT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Unit_Prod_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Unit_Prod_N_Crit", "#REPT2-G-TOT-UNIT-PROD-N-CRIT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 80));
        pnd_Rept2_G_Tot_Unit_Pymt_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Unit_Pymt_N_Crit", "#REPT2-G-TOT-UNIT-PYMT-N-CRIT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 80));
        pnd_Rept2_Total_Units_Prod_In_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_M", "#REPT2-TOTAL-UNITS-PROD-IN-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_In_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_M", "#REPT2-TOTAL-UNITS-PYMT-IN-M", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Ct_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Ct_M", "#REPT2-TOTAL-UNITS-PROD-CT-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_Ct_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Ct_M", "#REPT2-TOTAL-UNITS-PYMT-CT-M", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_In_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_M_P", "#REPT2-TOTAL-UNITS-PROD-IN-M-P", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_In_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_M_P", "#REPT2-TOTAL-UNITS-PYMT-IN-M-P", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Ct_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Ct_M_P", "#REPT2-TOTAL-UNITS-PROD-CT-M-P", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_Ct_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Ct_M_P", "#REPT2-TOTAL-UNITS-PYMT-CT-M-P", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_In_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_A", "#REPT2-TOTAL-UNITS-PROD-IN-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_In_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_A", "#REPT2-TOTAL-UNITS-PYMT-IN-A", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Ct_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Ct_A", "#REPT2-TOTAL-UNITS-PROD-CT-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_Ct_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Ct_A", "#REPT2-TOTAL-UNITS-PYMT-CT-A", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Pymt_Out_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_A", "#REPT2-TOTAL-UNITS-PYMT-OUT-A", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Out_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_A", "#REPT2-TOTAL-UNITS-PROD-OUT-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Unit_Prod_N_Crit_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Crit_A", "#REPT2-TOTAL-UNIT-PROD-N-CRIT-A", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Crit_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Crit_A", "#REPT2-TOTAL-UNIT-PYMT-N-CRIT-A", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Total_Units_Prod_In_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_A_P", "#REPT2-TOTAL-UNITS-PROD-IN-A-P", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_In_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_A_P", "#REPT2-TOTAL-UNITS-PYMT-IN-A-P", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Ct_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Ct_A_P", "#REPT2-TOTAL-UNITS-PROD-CT-A-P", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Units_Pymt_Ct_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Ct_A_P", "#REPT2-TOTAL-UNITS-PYMT-CT-A-P", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Pymt_Out_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_A_P", "#REPT2-TOTAL-UNITS-PYMT-OUT-A-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Total_Units_Prod_Out_A_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_A_P", "#REPT2-TOTAL-UNITS-PROD-OUT-A-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Prod_N_Critap = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Critap", "#REPT2-TOTAL-UNIT-PROD-N-CRITAP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Critap = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Critap", "#REPT2-TOTAL-UNIT-PYMT-N-CRITAP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Total_Units_Pymt_Out_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_M", "#REPT2-TOTAL-UNITS-PYMT-OUT-M", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Total_Units_Prod_Out_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_M", "#REPT2-TOTAL-UNITS-PROD-OUT-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_Total_Unit_Prod_N_Crit_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Crit_M", "#REPT2-TOTAL-UNIT-PROD-N-CRIT-M", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Crit_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Crit_M", "#REPT2-TOTAL-UNIT-PYMT-N-CRIT-M", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Total_Units_Pymt_Out_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_M_P", "#REPT2-TOTAL-UNITS-PYMT-OUT-M-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Total_Units_Prod_Out_M_P = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_M_P", "#REPT2-TOTAL-UNITS-PROD-OUT-M-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Prod_N_Critmp = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Critmp", "#REPT2-TOTAL-UNIT-PROD-N-CRITMP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Critmp = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Critmp", "#REPT2-TOTAL-UNIT-PYMT-N-CRITMP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_In_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_A", "#REPT2-G-TOTAL-UNITS-PROD-IN-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_A", "#REPT2-G-TOTAL-UNITS-PYMT-IN-A", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_G_Total_Units_Prod_Ct_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Ct_A", "#REPT2-G-TOTAL-UNITS-PROD-CT-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Ct_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Ct_A", "#REPT2-G-TOTAL-UNITS-PYMT-CT-A", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_G_Total_Units_Prod_In_A_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_A_P", "#REPT2-G-TOTAL-UNITS-PROD-IN-A-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_A_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_A_P", "#REPT2-G-TOTAL-UNITS-PYMT-IN-A-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Ct_A_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Ct_A_P", "#REPT2-G-TOTAL-UNITS-PROD-CT-A-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Ct_A_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Ct_A_P", "#REPT2-G-TOTAL-UNITS-PYMT-CT-A-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_In_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_M", "#REPT2-G-TOTAL-UNITS-PROD-IN-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_M", "#REPT2-G-TOTAL-UNITS-PYMT-IN-M", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_G_Total_Units_Prod_Ct_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Ct_M", "#REPT2-G-TOTAL-UNITS-PROD-CT-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Ct_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Ct_M", "#REPT2-G-TOTAL-UNITS-PYMT-CT-M", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_G_Total_Units_Prod_In_M_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_M_P", "#REPT2-G-TOTAL-UNITS-PROD-IN-M-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_M_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_M_P", "#REPT2-G-TOTAL-UNITS-PYMT-IN-M-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Ct_M_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Ct_M_P", "#REPT2-G-TOTAL-UNITS-PROD-CT-M-P", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Ct_M_P = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Ct_M_P", "#REPT2-G-TOTAL-UNITS-PYMT-CT-M-P", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_A", "#REPT2-G-TOTAL-UNITS-PROD-OUT-A", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_A", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-A", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Crita = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Crita", "#REPT2-G-TOTAL-UNIT-PROD-N-CRITA", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Unit_Pymt_N_Crita = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Crita", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRITA", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_Ap = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_Ap", "#REPT2-G-TOTAL-UNITS-PROD-OUT-AP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_Ap = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_Ap", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-AP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Criap = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Criap", "#REPT2-G-TOTAL-UNIT-PROD-N-CRIAP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Unit_Pymt_N_Criap = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Criap", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRIAP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_M", "#REPT2-G-TOTAL-UNITS-PROD-OUT-M", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_M", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-M", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Critm = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Critm", "#REPT2-G-TOTAL-UNIT-PROD-N-CRITM", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Unit_Pymt_N_Critm = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Critm", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRITM", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_Mp = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_Mp", "#REPT2-G-TOTAL-UNITS-PROD-OUT-MP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_Mp = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_Mp", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-MP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Crimp = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Crimp", "#REPT2-G-TOTAL-UNIT-PROD-N-CRIMP", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Unit_Pymt_N_Crimp = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Crimp", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRIMP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Pmt_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_Out", "#REPT2-TOT-PER-PMT-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_Out", "#REPT2-TOT-PER-DVD-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Pmt_Ct = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_Ct", "#REPT2-TOT-PER-PMT-CT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Dvd_Ct = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_Ct", "#REPT2-TOT-PER-DVD-CT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Pmt_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_N_Crit", "#REPT2-TOT-PER-PMT-N-CRIT", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Tot_Per_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_N_Crit", "#REPT2-TOT-PER-DVD-N-CRIT", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_Tot_Fin_Pay_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_N_Crit", "#REPT2-TOT-FIN-PAY-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Per_Dvd_Ct = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_Ct", "#REPT2-G-TOT-PER-DVD-CT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Pmt_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_Out", "#REPT2-G-TOT-PER-PMT-OUT", FieldType.NUMERIC, 13, 
            2);
        pnd_Rept2_G_Tot_Per_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_Out", "#REPT2-G-TOT-PER-DVD-OUT", FieldType.NUMERIC, 13, 
            2);
        pnd_Rept2_G_Tot_Per_Pmt_Ct = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_Ct", "#REPT2-G-TOT-PER-PMT-CT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Pmt_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_N_Crit", "#REPT2-G-TOT-PER-PMT-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Per_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_N_Crit", "#REPT2-G-TOT-PER-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Tot_Fin_Pay_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_Out", "#REPT2-TOT-FIN-PAY-OUT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_Out", "#REPT2-TOT-FIN-DVD-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Fin_Pay_Ct = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_Ct", "#REPT2-TOT-FIN-PAY-CT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Dvd_Ct = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_Ct", "#REPT2-TOT-FIN-DVD-CT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Fin_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_N_Crit", "#REPT2-TOT-FIN-DVD-N-CRIT", FieldType.NUMERIC, 
            11, 2);
        pnd_Rept2_G_Tot_Fin_Dvd_Ct = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_Ct", "#REPT2-G-TOT-FIN-DVD-CT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Pay_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_Out", "#REPT2-G-TOT-FIN-PAY-OUT", FieldType.NUMERIC, 13, 
            2);
        pnd_Rept2_G_Tot_Fin_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_Out", "#REPT2-G-TOT-FIN-DVD-OUT", FieldType.NUMERIC, 13, 
            2);
        pnd_Rept2_G_Tot_Fin_Pay_Ct = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_Ct", "#REPT2-G-TOT-FIN-PAY-CT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Pay_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_N_Crit", "#REPT2-G-TOT-FIN-PAY-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Fin_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_N_Crit", "#REPT2-G-TOT-FIN-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Tot_Diff = localVariables.newFieldInRecord("pnd_Rept2_Tot_Diff", "#REPT2-TOT-DIFF", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Diff_Unit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Diff_Unit", "#REPT2-TOT-DIFF-UNIT", FieldType.NUMERIC, 14, 3);
        pnd_Rept2_Tot_Diff_Unit_Pymt = localVariables.newFieldInRecord("pnd_Rept2_Tot_Diff_Unit_Pymt", "#REPT2-TOT-DIFF-UNIT-PYMT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Title_Sub = localVariables.newFieldInRecord("pnd_Rept2_Title_Sub", "#REPT2-TITLE-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Mode_Sub = localVariables.newFieldInRecord("pnd_Rept2_Mode_Sub", "#REPT2-MODE-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Cntl_Sub = localVariables.newFieldInRecord("pnd_Rept2_Cntl_Sub", "#REPT2-CNTL-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Prod_Sub = localVariables.newFieldInRecord("pnd_Rept2_Prod_Sub", "#REPT2-PROD-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub = localVariables.newFieldInRecord("pnd_Prod_Sub", "#PROD-SUB", FieldType.NUMERIC, 2);
        pnd_W_Print_Sw = localVariables.newFieldInRecord("pnd_W_Print_Sw", "#W-PRINT-SW", FieldType.STRING, 1);
        pnd_Seperator = localVariables.newFieldInRecord("pnd_Seperator", "#SEPERATOR", FieldType.STRING, 120);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap795() throws Exception
    {
        super("Iaap795");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT ( 3 ) LS = 132 PS = 60;//Natural: FORMAT ( 4 ) LS = 132 PS = 60;//Natural: FORMAT ( 5 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        pnd_Seperator.moveAll("=");                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 );//Natural: MOVE ALL '=' TO #SEPERATOR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD-1
        while (condition(getWorkFiles().read(1, pnd_Work_Record_1)))
        {
            pnd_Total_Records_Read.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-RECORDS-READ
            if (condition(pnd_Total_Records_Read.equals(1)))                                                                                                              //Natural: IF #TOTAL-RECORDS-READ = 1
            {
                pnd_Mode_Hold.setValue(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind);                                                                                          //Natural: MOVE #W-CNTRCT-MODE-IND TO #MODE-HOLD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Pnd_Date_Ccyy.setValue(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy);                                                                                //Natural: MOVE #W-TRANS-CHECK-DTE-CCYY TO #DATE-CCYY
            pnd_Date_Pnd_Date_Mm.setValue(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm);                                                                                    //Natural: MOVE #W-TRANS-CHECK-DTE-MM TO #DATE-MM
            pnd_Date_Pnd_Date_Dd.setValue(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd);                                                                                    //Natural: MOVE #W-TRANS-CHECK-DTE-DD TO #DATE-DD
            pnd_C_Trans_Check_Dte.setValue(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte);                                                                                      //Natural: MOVE #W-TRANS-CHECK-DTE TO #C-TRANS-CHECK-DTE
            if (condition(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte.equals(getZero())))                                                                                     //Natural: IF #W-TRANS-CHECK-DTE = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  11/98  USES PRIOR MONTH TO PROCESS
                if (condition(pnd_Date_Pnd_Date_Mm.equals(1)))                                                                                                            //Natural: IF #DATE-MM = 1
                {
                    pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm.setValue(12);                                                                                            //Natural: MOVE 12 TO #TRANS-CHECK-DTE-MM
                    pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy.nsubtract(1);                                                                                          //Natural: SUBTRACT 1 FROM #TRANS-CHECK-DTE-CCYY
                    pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000000, //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000000 - #C-TRANS-CHECK-DTE
                        pnd_C_Trans_Check_Dte));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000100, //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000100 - #C-TRANS-CHECK-DTE
                        pnd_C_Trans_Check_Dte));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.notEquals(pnd_Mode_Hold)))                                                                              //Natural: IF #W-CNTRCT-MODE-IND NOT = #MODE-HOLD
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-2
                sub_Pnd_Write_Report_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
                sub_Pnd_Reset_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Mode_Hold.setValue(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind);                                                                                          //Natural: MOVE #W-CNTRCT-MODE-IND TO #MODE-HOLD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr.equals("9999999999")))                                                                                   //Natural: IF #W-TRANS-PPCN-NBR = '9999999999'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #EDIT-CHECK
            sub_Pnd_Edit_Check();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #PRODUCT-CODE-TRANS
            sub_Pnd_Product_Code_Trans();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Record_1_Pnd_W_Activity_Code.equals("R")))                                                                                             //Natural: IF #W-ACTIVITY-CODE = 'R'
            {
                pnd_W_Print_Sw.setValue("R");                                                                                                                             //Natural: MOVE 'R' TO #W-PRINT-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Print_Sw.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #W-PRINT-SW
            }                                                                                                                                                             //Natural: END-IF
            pnd_Trans_Ppcn_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1, "-", pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A)); //Natural: COMPRESS #W-TRANS-PPCN-NBR-1 '-' #W-TRANS-PAYEE-CDE-A INTO #TRANS-PPCN-NBR LEAVING NO SPACE
            if (condition((pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(37) && pnd_Work_Record_1_Pnd_W_Sub_Code.equals("37B")) || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(66)  //Natural: IF ( #W-TRANS-CDE = 037 AND #W-SUB-CODE = '37B' ) OR ( #W-TRANS-CDE = 066 AND #W-ACTIVITY-CODE NE 'R' ) OR ( #W-TRANS-CDE = 006 ) OR ( #W-TRANS-CDE = 040 ) OR ( #W-TRANS-CDE = 031 AND #W-ACTIVITY-CODE = 'R' ) OR ( #W-TRANS-CDE = 030 AND #W-ACTIVITY-CODE = 'R' ) OR ( #W-TRANS-CDE = 033 AND #W-ACTIVITY-CODE = 'R' )
                && pnd_Work_Record_1_Pnd_W_Activity_Code.notEquals("R")) || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(6)) || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(40)) 
                || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(31) && pnd_Work_Record_1_Pnd_W_Activity_Code.equals("R")) || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(30) 
                && pnd_Work_Record_1_Pnd_W_Activity_Code.equals("R")) || (pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(33) && pnd_Work_Record_1_Pnd_W_Activity_Code.equals("R"))))
            {
                pnd_Rept1_In_Out.setValue("IN ");                                                                                                                         //Natural: MOVE 'IN ' TO #REPT1-IN-OUT
                getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) #W-PRINT-SW #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #W-TIAA-TOT-PER-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-TIAA-TOT-DIV-AMT ( EM = ZZZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT ( EM = ZZZZZZ.999 ) 02X #W-FINAL-PAYMENT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-FINAL-DIVIDEND ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #REPT1-IN-OUT
                    ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),pnd_W_Print_Sw,pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                    ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                    ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt, new ReportEditMask ("ZZZZZ,ZZZ.99"),new 
                    ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, new ReportEditMask ("ZZZZZZ.999"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Payment, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Dividend, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(2),pnd_Rept1_In_Out);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept1_Net_Per_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Pmt), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt)); //Natural: COMPUTE #REPT1-NET-PER-PMT = #W-TIAA-TOT-PER-AMT-B - #W-TIAA-TOT-PER-AMT
                pnd_Rept1_Net_Per_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt)); //Natural: COMPUTE #REPT1-NET-PER-DVD = #W-TIAA-TOT-DIV-AMT-B - #W-TIAA-TOT-DIV-AMT
                pnd_Rept1_Net_Units.compute(new ComputeParameters(false, pnd_Rept1_Net_Units), pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B.subtract(pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt)); //Natural: COMPUTE #REPT1-NET-UNITS = #W-CREF-UNITS-CNT-B - #W-CREF-UNITS-CNT
                pnd_Rept1_Net_Final_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Pmt), pnd_Work_Record_1_Pnd_W_Final_Payment_B.subtract(pnd_Work_Record_1_Pnd_W_Final_Payment)); //Natural: COMPUTE #REPT1-NET-FINAL-PMT = #W-FINAL-PAYMENT-B - #W-FINAL-PAYMENT
                pnd_Rept1_Net_Final_Divd.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Divd), pnd_Work_Record_1_Pnd_W_Final_Dividend_B.subtract(pnd_Work_Record_1_Pnd_W_Final_Dividend)); //Natural: COMPUTE #REPT1-NET-FINAL-DIVD = #W-FINAL-DIVIDEND-B - #W-FINAL-DIVIDEND
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-REPORT2
                sub_Pnd_Fill_Up_Report2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept1_In_Out.setValue("OUT ");                                                                                                                        //Natural: MOVE 'OUT ' TO #REPT1-IN-OUT
                getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 71X #REPT1-IN-OUT
                    ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                    ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                    ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(71),pnd_Rept1_In_Out);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(50) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(51) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(500)  //Natural: IF #W-TRANS-CDE = 050 OR = 051 OR = 500 OR = 502 OR = 504 OR = 506 OR = 512 OR = 514 OR = 516 OR = 518 OR = 508 OR = 520 OR = 060 OR = 061 OR = 062
                    || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(502) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(504) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(506) 
                    || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(512) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(514) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(516) 
                    || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(518) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(508) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(520) 
                    || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(60) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(61) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(62)))
                {
                    //*  ADD -B 1119
                    pnd_Rept1_In_Out.setValue("IN ");                                                                                                                     //Natural: MOVE 'IN ' TO #REPT1-IN-OUT
                    getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) #W-PRINT-SW #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #W-TIAA-TOT-PER-AMT-B ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-TIAA-TOT-DIV-AMT-B ( EM = ZZZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT-B ( EM = ZZZZZZ.999 ) 02X #W-FINAL-PAYMENT-B ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-FINAL-DIVIDEND-B ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #REPT1-IN-OUT
                        ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),pnd_W_Print_Sw,pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                        ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                        ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B, new ReportEditMask ("ZZZZZ,ZZZ.99"),new 
                        ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B, new ReportEditMask ("ZZZZZZ.999"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Payment_B, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Dividend_B, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                        ColumnSpacing(2),pnd_Rept1_In_Out);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rept1_In_Out.setValue("OUT ");                                                                                                                    //Natural: MOVE 'OUT ' TO #REPT1-IN-OUT
                    getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #W-TIAA-TOT-PER-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-TIAA-TOT-DIV-AMT ( EM = ZZZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT ( EM = ZZZZZZ.999 ) 02X #W-FINAL-PAYMENT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-FINAL-DIVIDEND ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #REPT1-IN-OUT
                        ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                        ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                        ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt, new ReportEditMask ("ZZZZZ,ZZZ.99"),new 
                        ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, new ReportEditMask ("ZZZZZZ.999"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Payment, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Dividend, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                        ColumnSpacing(2),pnd_Rept1_In_Out);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rept1_Net_Per_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Pmt), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-PMT = #W-TIAA-TOT-PER-AMT - #W-TIAA-TOT-PER-AMT-B
                    pnd_Rept1_Net_Per_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-DVD = #W-TIAA-TOT-DIV-AMT - #W-TIAA-TOT-DIV-AMT-B
                    pnd_Rept1_Net_Units.compute(new ComputeParameters(false, pnd_Rept1_Net_Units), pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.subtract(pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B)); //Natural: COMPUTE #REPT1-NET-UNITS = #W-CREF-UNITS-CNT - #W-CREF-UNITS-CNT-B
                    pnd_Rept1_Net_Final_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Pmt), pnd_Work_Record_1_Pnd_W_Final_Payment.subtract(pnd_Work_Record_1_Pnd_W_Final_Payment_B)); //Natural: COMPUTE #REPT1-NET-FINAL-PMT = #W-FINAL-PAYMENT - #W-FINAL-PAYMENT-B
                    //*  CHANGED 11082001
                    pnd_Rept1_Net_Final_Divd.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Divd), pnd_Work_Record_1_Pnd_W_Final_Dividend.subtract(pnd_Work_Record_1_Pnd_W_Final_Dividend_B)); //Natural: COMPUTE #REPT1-NET-FINAL-DIVD = #W-FINAL-DIVIDEND - #W-FINAL-DIVIDEND-B
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-REPORT2
                    sub_Pnd_Fill_Up_Report2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(20) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(52)))                                          //Natural: IF #W-TRANS-CDE = 020 OR = 052
                    {
                        //*  9/12
                        pnd_Rept1_In_Out.setValue("IN ");                                                                                                                 //Natural: MOVE 'IN ' TO #REPT1-IN-OUT
                        getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) #W-PRINT-SW #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #W-TIAA-TOT-PER-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-TIAA-TOT-DIV-AMT ( EM = ZZZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT ( EM = ZZZZZZ.999 ) 02X #W-FINAL-PAYMENT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-FINAL-DIVIDEND ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #REPT1-IN-OUT
                            ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),pnd_W_Print_Sw,pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                            ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                            ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt, 
                            new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt, new ReportEditMask ("ZZZZZ,ZZZ.99"),new 
                            ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, new ReportEditMask ("ZZZZZZ.999"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Payment, 
                            new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Dividend, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                            ColumnSpacing(2),pnd_Rept1_In_Out);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rept1_In_Out.setValue("OUT ");                                                                                                                //Natural: MOVE 'OUT ' TO #REPT1-IN-OUT
                        getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 71X #REPT1-IN-OUT
                            ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                            ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                            ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(71),pnd_Rept1_In_Out);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rept1_Net_Per_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Pmt), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt)); //Natural: COMPUTE #REPT1-NET-PER-PMT = #W-TIAA-TOT-PER-AMT-B - #W-TIAA-TOT-PER-AMT
                        pnd_Rept1_Net_Per_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt)); //Natural: COMPUTE #REPT1-NET-PER-DVD = #W-TIAA-TOT-DIV-AMT-B - #W-TIAA-TOT-DIV-AMT
                        pnd_Rept1_Net_Units.compute(new ComputeParameters(false, pnd_Rept1_Net_Units), pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B.subtract(pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt)); //Natural: COMPUTE #REPT1-NET-UNITS = #W-CREF-UNITS-CNT-B - #W-CREF-UNITS-CNT
                        pnd_Rept1_Net_Final_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Pmt), pnd_Work_Record_1_Pnd_W_Final_Payment_B.subtract(pnd_Work_Record_1_Pnd_W_Final_Payment)); //Natural: COMPUTE #REPT1-NET-FINAL-PMT = #W-FINAL-PAYMENT-B - #W-FINAL-PAYMENT
                        pnd_Rept1_Net_Final_Divd.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Divd), pnd_Work_Record_1_Pnd_W_Final_Dividend_B.subtract(pnd_Work_Record_1_Pnd_W_Final_Dividend)); //Natural: COMPUTE #REPT1-NET-FINAL-DIVD = #W-FINAL-DIVIDEND-B - #W-FINAL-DIVIDEND
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-REPORT2
                        sub_Pnd_Fill_Up_Report2();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Rept1_In_Out.setValue("IN ");                                                                                                                 //Natural: MOVE 'IN ' TO #REPT1-IN-OUT
                        getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) #W-PRINT-SW #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 71X #REPT1-IN-OUT
                            ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),pnd_W_Print_Sw,pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                            ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                            ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(71),pnd_Rept1_In_Out);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rept1_In_Out.setValue("OUT ");                                                                                                                //Natural: MOVE 'OUT ' TO #REPT1-IN-OUT
                        getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new  //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #W-TIAA-TOT-PER-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-TIAA-TOT-DIV-AMT ( EM = ZZZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT ( EM = ZZZZZZ.999 ) 02X #W-FINAL-PAYMENT ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #W-FINAL-DIVIDEND ( EM = ZZ,ZZZ,ZZZ.99 ) 02X #REPT1-IN-OUT
                            ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                            ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                            ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt, 
                            new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt, new ReportEditMask ("ZZZZZ,ZZZ.99"),new 
                            ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, new ReportEditMask ("ZZZZZZ.999"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Payment, 
                            new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Final_Dividend, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                            ColumnSpacing(2),pnd_Rept1_In_Out);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rept1_Net_Per_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Pmt), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-PMT = #W-TIAA-TOT-PER-AMT - #W-TIAA-TOT-PER-AMT-B
                        pnd_Rept1_Net_Per_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-DVD = #W-TIAA-TOT-DIV-AMT - #W-TIAA-TOT-DIV-AMT-B
                        pnd_Rept1_Net_Units.compute(new ComputeParameters(false, pnd_Rept1_Net_Units), pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.subtract(pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B)); //Natural: COMPUTE #REPT1-NET-UNITS = #W-CREF-UNITS-CNT - #W-CREF-UNITS-CNT-B
                        pnd_Rept1_Net_Final_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Pmt), pnd_Work_Record_1_Pnd_W_Final_Payment.subtract(pnd_Work_Record_1_Pnd_W_Final_Payment_B)); //Natural: COMPUTE #REPT1-NET-FINAL-PMT = #W-FINAL-PAYMENT - #W-FINAL-PAYMENT-B
                        pnd_Rept1_Net_Final_Divd.compute(new ComputeParameters(false, pnd_Rept1_Net_Final_Divd), pnd_Work_Record_1_Pnd_W_Final_Dividend.subtract(pnd_Work_Record_1_Pnd_W_Final_Dividend_B)); //Natural: COMPUTE #REPT1-NET-FINAL-DIVD = #W-FINAL-DIVIDEND - #W-FINAL-DIVIDEND-B
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-REPORT2
                        sub_Pnd_Fill_Up_Report2();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  9/12
                //*  9/12
                //*  9/12
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new    //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 03X #REPT1-NET-PER-PMT ( EM = ZZ,ZZZ,ZZZ.99- ) #REPT1-NET-PER-DVD ( EM = ZZZZ,ZZZ.99- ) 01X #REPT1-NET-UNITS ( EM = ZZZ,ZZZ.999- ) #REPT1-NET-FINAL-PMT ( EM = ZZ,ZZZ,ZZZ.99- ) #REPT1-NET-FINAL-DIVD ( EM = ZZ,ZZZ,ZZZ.99- ) 01X 'NET' /
                ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(3),pnd_Rept1_Net_Per_Pmt, new ReportEditMask 
                ("Z,ZZZ,ZZZ.99-"),pnd_Rept1_Net_Per_Dvd, new ReportEditMask ("ZZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Rept1_Net_Units, new ReportEditMask 
                ("ZZZ,ZZZ.999-"),pnd_Rept1_Net_Final_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),pnd_Rept1_Net_Final_Divd, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),"NET",NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING FOR END FILE PROCESSING GET PREV CNTRL-REC 1/98
        if (condition(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte.equals(getZero())))                                                                                         //Natural: IF #W-TRANS-CHECK-DTE = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Date_Pnd_Date_Mm.equals(1)))                                                                                                                //Natural: IF #DATE-MM = 1
            {
                pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000000,     //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000000 - #C-TRANS-CHECK-DTE
                    pnd_C_Trans_Check_Dte));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000100,     //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000100 - #C-TRANS-CHECK-DTE
                    pnd_C_Trans_Check_Dte));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-2
        sub_Pnd_Write_Report_2();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-G-A
        sub_Pnd_Write_Report_G_A();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-G-M
        sub_Pnd_Write_Report_G_M();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "==================================");                                                                                                      //Natural: WRITE '=================================='
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL RECORDS PROCESSED ",pnd_Total_Records_Read);                                                                                         //Natural: WRITE 'TOTAL RECORDS PROCESSED ' #TOTAL-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "==================================");                                                                                                      //Natural: WRITE '=================================='
        if (Global.isEscape()) return;
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MODE-TRANS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-CONTROL-RECORD
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-A
        //*             PRINT REPORT 2
        //*     PRINT TIAA PAYMENT LINE
        //*     PRINT TIAA DIVIDEND LINE
        //*     PRINT TIAA FINAL PAYMENT
        //*     PRINT TIAA FINAL DIVIDEND
        //*     PRINT CREF PAYMENT LINE   REAL ESTATE AND TIAA ACCESS ONLY 9/08
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-M
        //* ******************************************************************
        //*     PRINT CREF PAYMENT LINE   REAL ESTATE AND ACCESS 9/08
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-G-A
        //*   TOTAL FOR REAL ESTATE AND ACCESS    9/08
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-G-M
        //*   TOTAL FOR REAL ESTATE AND ACCESS
        //*  ========  PA SELECT FUNDS
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-UP-REPORT2
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-2
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRODUCT-CODE-TRANS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #EDIT-CHECK
    }
    private void sub_Pnd_Mode_Trans() throws Exception                                                                                                                    //Natural: #MODE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet662 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MODE-HOLD;//Natural: VALUE 100
        if (condition((pnd_Mode_Hold.equals(100))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MA");                                                                                                                                 //Natural: MOVE 'MA' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Mode_Hold.equals(601))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MB");                                                                                                                                 //Natural: MOVE 'MB' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Mode_Hold.equals(602))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MC");                                                                                                                                 //Natural: MOVE 'MC' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Mode_Hold.equals(603))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MD");                                                                                                                                 //Natural: MOVE 'MD' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Mode_Hold.equals(701))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("ME");                                                                                                                                 //Natural: MOVE 'ME' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Mode_Hold.equals(702))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MF");                                                                                                                                 //Natural: MOVE 'MF' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Mode_Hold.equals(703))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MG");                                                                                                                                 //Natural: MOVE 'MG' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Mode_Hold.equals(704))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MH");                                                                                                                                 //Natural: MOVE 'MH' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Mode_Hold.equals(705))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MI");                                                                                                                                 //Natural: MOVE 'MI' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Mode_Hold.equals(706))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MJ");                                                                                                                                 //Natural: MOVE 'MJ' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Mode_Hold.equals(801))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MK");                                                                                                                                 //Natural: MOVE 'MK' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Mode_Hold.equals(802))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("ML");                                                                                                                                 //Natural: MOVE 'ML' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Mode_Hold.equals(803))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MM");                                                                                                                                 //Natural: MOVE 'MM' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Mode_Hold.equals(804))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MN");                                                                                                                                 //Natural: MOVE 'MN' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Mode_Hold.equals(805))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MO");                                                                                                                                 //Natural: MOVE 'MO' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Mode_Hold.equals(806))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MP");                                                                                                                                 //Natural: MOVE 'MP' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Mode_Hold.equals(807))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MQ");                                                                                                                                 //Natural: MOVE 'MQ' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Mode_Hold.equals(808))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MR");                                                                                                                                 //Natural: MOVE 'MR' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Mode_Hold.equals(809))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MS");                                                                                                                                 //Natural: MOVE 'MS' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Mode_Hold.equals(810))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MT");                                                                                                                                 //Natural: MOVE 'MT' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Mode_Hold.equals(811))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MU");                                                                                                                                 //Natural: MOVE 'MU' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Mode_Hold.equals(812))))
        {
            decideConditionsMet662++;
            pnd_Mode_Code.setValue("MV");                                                                                                                                 //Natural: MOVE 'MV' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "ERROR IN MODE","=",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,"=",pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind);                                //Natural: WRITE 'ERROR IN MODE' '=' #W-TRANS-PPCN-NBR '=' #W-CNTRCT-MODE-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Read_Control_Record() throws Exception                                                                                                           //Natural: #READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde.setValue(pnd_Mode_Code);                                                                                                 //Natural: MOVE #MODE-CODE TO #SEARCH-CNTRL-CDE
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseFind                                                                                                                        //Natural: FIND IAA-CNTRL-RCRD-1-VIEW WITH CNTRL-RCRD-KEY = #SEARCH-KEY-RCRD
        (
        "FIND01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", "=", pnd_Search_Key_Rcrd, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrl_Rcrd_1_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrl_Rcrd_1_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, " ");                                                                                                                               //Natural: WRITE ' '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***************** NO IAA-CNTRL-RCRD-1 ****************A");                                                                         //Natural: WRITE '***************** NO IAA-CNTRL-RCRD-1 ****************A'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "NO RECORD WITH KEY ",pnd_Search_Key_Rcrd,"=",pnd_C_Trans_Check_Dte, new ReportEditMask ("9999/99/99"));                            //Natural: WRITE 'NO RECORD WITH KEY ' #SEARCH-KEY-RCRD '=' #C-TRANS-CHECK-DTE ( EM = 9999/99/99 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*******************************************************");                                                                         //Natural: WRITE '*******************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Rept2_Tot_Per_Pmt_In.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt);                                                                                   //Natural: MOVE CNTRL-PER-PAY-AMT TO #REPT2-TOT-PER-PMT-IN
            pnd_Rept2_G_Tot_Per_Pmt_In.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt);                                                                                     //Natural: ADD CNTRL-PER-PAY-AMT TO #REPT2-G-TOT-PER-PMT-IN
            pnd_Rept2_Tot_Per_Dvd_In.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt);                                                                                   //Natural: MOVE CNTRL-PER-DIV-AMT TO #REPT2-TOT-PER-DVD-IN
            pnd_Rept2_G_Tot_Per_Dvd_In.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt);                                                                                     //Natural: ADD CNTRL-PER-DIV-AMT TO #REPT2-G-TOT-PER-DVD-IN
            pnd_Rept2_Tot_Fin_Pay_In.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt);                                                                                 //Natural: MOVE CNTRL-FINAL-PAY-AMT TO #REPT2-TOT-FIN-PAY-IN
            pnd_Rept2_G_Tot_Fin_Pay_In.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt);                                                                                   //Natural: ADD CNTRL-FINAL-PAY-AMT TO #REPT2-G-TOT-FIN-PAY-IN
            pnd_Rept2_Tot_Fin_Dvd_In.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt);                                                                                 //Natural: MOVE CNTRL-FINAL-DIV-AMT TO #REPT2-TOT-FIN-DVD-IN
            pnd_Rept2_G_Tot_Fin_Dvd_In.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt);                                                                                   //Natural: ADD CNTRL-FINAL-DIV-AMT TO #REPT2-G-TOT-FIN-DVD-IN
            pnd_Rept2_Prod_Titles_T.getValue(1).setValue("TIAA PERIODIC PAYMENT ");                                                                                       //Natural: MOVE 'TIAA PERIODIC PAYMENT ' TO #REPT2-PROD-TITLES-T ( 1 )
            pnd_Rept2_Prod_Titles_T.getValue(2).setValue("TIAA PERIODIC DIVIDEND");                                                                                       //Natural: MOVE 'TIAA PERIODIC DIVIDEND' TO #REPT2-PROD-TITLES-T ( 2 )
            pnd_Rept2_Prod_Titles_T.getValue(3).setValue("TIAA FINAL PAYMENT    ");                                                                                       //Natural: MOVE 'TIAA FINAL PAYMENT    ' TO #REPT2-PROD-TITLES-T ( 3 )
            pnd_Rept2_Prod_Titles_T.getValue(4).setValue("TIAA FINAL DIVIDEND   ");                                                                                       //Natural: MOVE 'TIAA FINAL DIVIDEND   ' TO #REPT2-PROD-TITLES-T ( 4 )
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 80
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
            {
                if (condition(pnd_I.greaterOrEqual(2) && pnd_I.lessOrEqual(80) && iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).greater(" ") &&                    //Natural: IF #I = 2 THRU 80 AND CNTRL-FUND-CDE ( #I ) > ' ' AND #TOTAL-CREF-SW = ' '
                    pnd_Total_Cref_Sw.equals(" ")))
                {
                    pnd_Total_Cref_Products.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-CREF-PRODUCTS
                }                                                                                                                                                         //Natural: END-IF
                //*  FOR MONTHLY
                //* CHANGE 2/04/2002
                //* CHANGE 2/04/2002
                if (condition(((iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).equals(" ")) && (pnd_I.less(41))) || ((iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).equals(" "))  //Natural: IF ( ( CNTRL-FUND-CDE ( #I ) EQ ' ' ) AND ( #I < 41 ) ) OR ( ( CNTRL-FUND-CDE ( #I ) EQ ' ' ) AND ( #I > 49 AND #I < 61 ) ) OR ( ( CNTRL-FUND-CDE ( #I ) EQ ' ' ) AND ( #I > 69 ) )
                    && (pnd_I.greater(49) && pnd_I.less(61))) || ((iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).equals(" ")) && (pnd_I.greater(69)))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                              //Natural: MOVE CNTRL-UNITS ( #I ) TO #REPT2-TOT-UNITS-PROD-IN ( #I )
                    pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                //Natural: MOVE CNTRL-AMT ( #I ) TO #REPT2-TOT-UNITS-PYMT-IN ( #I )
                    pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOT-UNITS-PROD-IN ( #I )
                    pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                  //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOT-UNITS-PYMT-IN ( #I )
                    //*  ANNUAL CREF
                    if (condition(pnd_I.greaterOrEqual(1) && pnd_I.lessOrEqual(20)))                                                                                      //Natural: IF ( #I = 1 THRU 20 )
                    {
                        pnd_Rept2_G_Total_Units_Prod_In_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                        //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-IN-A
                        pnd_Rept2_G_Total_Units_Pymt_In_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                          //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-IN-A
                        pnd_Rept2_Total_Units_Prod_In_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                          //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-IN-A
                        pnd_Rept2_Total_Units_Pymt_In_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                            //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-IN-A
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ANNUAL PA
                        if (condition(pnd_I.greaterOrEqual(41) && pnd_I.lessOrEqual(60)))                                                                                 //Natural: IF ( #I = 41 THRU 60 )
                        {
                            pnd_Rept2_G_Total_Units_Prod_In_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                  //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-IN-A-P
                            pnd_Rept2_G_Total_Units_Pymt_In_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                    //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-IN-A-P
                            pnd_Rept2_Total_Units_Prod_In_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                    //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-IN-A-P
                            pnd_Rept2_Total_Units_Pymt_In_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                      //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-IN-A-P
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  MONTHLY CREF
                            if (condition(pnd_I.greaterOrEqual(21) && pnd_I.lessOrEqual(40)))                                                                             //Natural: IF ( #I = 21 THRU 40 )
                            {
                                pnd_Rept2_G_Total_Units_Prod_In_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-IN-M
                                pnd_Rept2_G_Total_Units_Pymt_In_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                  //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-IN-M
                                pnd_Rept2_Total_Units_Prod_In_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                  //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-IN-M
                                pnd_Rept2_Total_Units_Pymt_In_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                    //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-IN-M
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  MONTHLY PA
                                if (condition(pnd_I.greaterOrEqual(61) && pnd_I.lessOrEqual(80)))                                                                         //Natural: IF ( #I = 61 THRU 80 )
                                {
                                    pnd_Rept2_G_Total_Units_Prod_In_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                          //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-IN-M-P
                                    pnd_Rept2_G_Total_Units_Pymt_In_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                            //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-IN-M-P
                                    pnd_Rept2_Total_Units_Prod_In_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                            //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-IN-M-P
                                    pnd_Rept2_Total_Units_Pymt_In_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                              //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-IN-M-P
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //* CHANGE 2/04/2002
                    //* CHANGE 2/04/2002
                    if (condition(((((pnd_I.greaterOrEqual(2) && pnd_I.lessOrEqual(80)) && iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).greater(" "))             //Natural: IF ( #I = 2 THRU 80 AND CNTRL-FUND-CDE ( #I ) > ' ' ) OR ( #I = 41 THRU 49 ) OR ( #I = 61 THRU 69 )
                        || (pnd_I.greaterOrEqual(41) && pnd_I.lessOrEqual(49))) || (pnd_I.greaterOrEqual(61) && pnd_I.lessOrEqual(69)))))
                    {
                        pnd_Cntrl_Fund_Cde.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I));                                                                //Natural: MOVE CNTRL-FUND-CDE ( #I ) TO #CNTRL-FUND-CDE
                        //* CHANGE 2/04/2002
                        if (condition(pnd_I.greaterOrEqual(41) && pnd_I.lessOrEqual(49)))                                                                                 //Natural: IF #I = 41 THRU 49
                        {
                            pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2.setValue(pnd_I);                                                                                      //Natural: MOVE #I TO #CNTRL-FUND-CDE-2
                        }                                                                                                                                                 //Natural: END-IF
                        //* CHANGE 2/04/2002
                        if (condition(pnd_I.greaterOrEqual(61) && pnd_I.lessOrEqual(69)))                                                                                 //Natural: IF #I = 61 THRU 69
                        {
                            pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2.compute(new ComputeParameters(false, pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2), pnd_I.subtract(20));   //Natural: SUBTRACT 20 FROM #I GIVING #CNTRL-FUND-CDE-2
                        }                                                                                                                                                 //Natural: END-IF
                        iaan051a_Fund.setValue(pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2);                                                                                  //Natural: MOVE #CNTRL-FUND-CDE-2 TO IAAN051A-FUND
                        iaan051a_Length.setValue(3);                                                                                                                      //Natural: MOVE 3 TO IAAN051A-LENGTH
                        iaan051a_Desc.reset();                                                                                                                            //Natural: RESET IAAN051A-DESC IAAN051A-CMPY-DESC #R-PROD-TITLES-C #R-PROD-TITLES-2-C
                        iaan051a_Cmpy_Desc.reset();
                        pnd_R_Prod_Titles_C.reset();
                        pnd_R_Prod_Titles_2_C.reset();
                        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), iaan051a_Fund, iaan051a_Desc, iaan051a_Cmpy_Desc, iaan051a_Length);                    //Natural: CALLNAT 'IAAN051A' USING IAAN051A-FUND IAAN051A-DESC IAAN051A-CMPY-DESC IAAN051A-LENGTH
                        if (condition(Global.isEscape())) return;
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co.setValue(iaan051a_Cmpy_Desc);                                                                              //Natural: MOVE IAAN051A-CMPY-DESC TO #RPT-TITLE-C-CO #RPT-TITLE-2-C-CO
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co.setValue(iaan051a_Cmpy_Desc);
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #RPT-TITLE-C-PROD #RPT-TITLE-2-C-PROD
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod.setValue(iaan051a_Desc);
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units.setValue("UNITS");                                                                                      //Natural: MOVE 'UNITS' TO #RPT-TITLE-C-UNITS
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments.setValue("PAYMENTS");                                                                            //Natural: MOVE 'PAYMENTS' TO #RPT-TITLE-2-C-PAYMENTS
                        pnd_Rept2_Prod_Titles_C.getValue(pnd_I).setValue(pnd_R_Prod_Titles_C);                                                                            //Natural: MOVE #R-PROD-TITLES-C TO #REPT2-PROD-TITLES-C ( #I )
                        pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I).setValue(pnd_R_Prod_Titles_2_C);                                                                        //Natural: MOVE #R-PROD-TITLES-2-C TO #REPT2-PROD-TITLES-2-C ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Cref_Sw.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #TOTAL-CREF-SW
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  ADDED A READ TO START WITH THE MOST CURRENT CONTROL RECORD
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'MA'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "MA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ02")))
        {
            pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Invrse_Dte);                                                                    //Natural: MOVE CNTRL-INVRSE-DTE TO #SEARCH-TRANS-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde.setValue(pnd_Mode_Code);                                                                                                 //Natural: MOVE #MODE-CODE TO #SEARCH-CNTRL-CDE
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseFind                                                                                                                        //Natural: FIND IAA-CNTRL-RCRD-1-VIEW WITH CNTRL-RCRD-KEY = #SEARCH-KEY-RCRD
        (
        "FIND02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", "=", pnd_Search_Key_Rcrd, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("FIND02", true)))
        {
            vw_iaa_Cntrl_Rcrd_1_View.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrl_Rcrd_1_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, " ");                                                                                                                               //Natural: WRITE ' '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "****************** NO IAA-CNTRL-RCRD-1 ***************B");                                                                         //Natural: WRITE '****************** NO IAA-CNTRL-RCRD-1 ***************B'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "NO RECORD WITH KEY ",pnd_Search_Key_Rcrd);                                                                                         //Natural: WRITE 'NO RECORD WITH KEY ' #SEARCH-KEY-RCRD
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*******************************************************");                                                                         //Natural: WRITE '*******************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Rept2_Tot_Per_Pmt_Ct.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt);                                                                                   //Natural: MOVE CNTRL-PER-PAY-AMT TO #REPT2-TOT-PER-PMT-CT
            pnd_Rept2_G_Tot_Per_Pmt_Ct.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Pay_Amt);                                                                                     //Natural: ADD CNTRL-PER-PAY-AMT TO #REPT2-G-TOT-PER-PMT-CT
            pnd_Rept2_Tot_Per_Dvd_Ct.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt);                                                                                   //Natural: MOVE CNTRL-PER-DIV-AMT TO #REPT2-TOT-PER-DVD-CT
            pnd_Rept2_G_Tot_Per_Dvd_Ct.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Per_Div_Amt);                                                                                     //Natural: ADD CNTRL-PER-DIV-AMT TO #REPT2-G-TOT-PER-DVD-CT
            pnd_Rept2_Tot_Fin_Pay_Ct.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt);                                                                                 //Natural: MOVE CNTRL-FINAL-PAY-AMT TO #REPT2-TOT-FIN-PAY-CT
            pnd_Rept2_G_Tot_Fin_Pay_Ct.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Pay_Amt);                                                                                   //Natural: ADD CNTRL-FINAL-PAY-AMT TO #REPT2-G-TOT-FIN-PAY-CT
            pnd_Rept2_Tot_Fin_Dvd_Ct.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt);                                                                                 //Natural: MOVE CNTRL-FINAL-DIV-AMT TO #REPT2-TOT-FIN-DVD-CT
            pnd_Rept2_G_Tot_Fin_Dvd_Ct.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Final_Div_Amt);                                                                                   //Natural: ADD CNTRL-FINAL-DIV-AMT TO #REPT2-G-TOT-FIN-DVD-CT
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 80
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
            {
                if (condition(iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).equals(" ")))                                                                          //Natural: IF CNTRL-FUND-CDE ( #I ) EQ ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I).setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                              //Natural: MOVE CNTRL-UNITS ( #I ) TO #REPT2-TOT-UNITS-PROD-CT ( #I )
                    pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I).setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                //Natural: MOVE CNTRL-AMT ( #I ) TO #REPT2-TOT-UNITS-PYMT-CT ( #I )
                    pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I).nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOT-UNITS-PROD-CT ( #I )
                    pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I).nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                  //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOT-UNITS-PYMT-CT ( #I )
                    //*  ANNUAL CREF FUNDS
                    if (condition(pnd_I.greaterOrEqual(1) && pnd_I.lessOrEqual(20)))                                                                                      //Natural: IF ( #I = 1 THRU 20 )
                    {
                        pnd_Rept2_Total_Units_Pymt_Ct_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                            //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-CT-A
                        pnd_Rept2_Total_Units_Prod_Ct_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                          //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-CT-A
                        pnd_Rept2_G_Total_Units_Pymt_Ct_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                          //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-CT-A
                        pnd_Rept2_G_Total_Units_Prod_Ct_A.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                        //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-CT-A
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ANNUAL PA FUNDS
                        if (condition(pnd_I.greaterOrEqual(41) && pnd_I.lessOrEqual(60)))                                                                                 //Natural: IF ( #I = 41 THRU 60 )
                        {
                            pnd_Rept2_Total_Units_Pymt_Ct_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                      //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-CT-A-P
                            pnd_Rept2_Total_Units_Prod_Ct_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                    //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-CT-A-P
                            pnd_Rept2_G_Total_Units_Pymt_Ct_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                    //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-CT-A-P
                            pnd_Rept2_G_Total_Units_Prod_Ct_A_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                  //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-CT-A-P
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  MONTHLY CREF FUNDS
                            if (condition(pnd_I.greaterOrEqual(21) && pnd_I.lessOrEqual(40)))                                                                             //Natural: IF ( #I = 21 THRU 40 )
                            {
                                pnd_Rept2_Total_Units_Pymt_Ct_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                    //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-CT-M
                                pnd_Rept2_Total_Units_Prod_Ct_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                  //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-CT-M
                                pnd_Rept2_G_Total_Units_Pymt_Ct_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                                  //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-CT-M
                                pnd_Rept2_G_Total_Units_Prod_Ct_M.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                                //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-CT-M
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  MONTHLY PA FUNDS
                                if (condition(pnd_I.greaterOrEqual(61) && pnd_I.lessOrEqual(80)))                                                                         //Natural: IF ( #I = 61 THRU 80 )
                                {
                                    pnd_Rept2_Total_Units_Pymt_Ct_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                              //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-CT-M-P
                                    pnd_Rept2_Total_Units_Prod_Ct_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                            //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-TOTAL-UNITS-PROD-CT-M-P
                                    pnd_Rept2_G_Total_Units_Pymt_Ct_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Amt.getValue(pnd_I));                                            //Natural: ADD CNTRL-AMT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-CT-M-P
                                    pnd_Rept2_G_Total_Units_Prod_Ct_M_P.nadd(iaa_Cntrl_Rcrd_1_View_Cntrl_Units.getValue(pnd_I));                                          //Natural: ADD CNTRL-UNITS ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-CT-M-P
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Report_A() throws Exception                                                                                                                //Natural: #WRITE-REPORT-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Rept2_Title_Sub.setValue(1);                                                                                                                                  //Natural: MOVE 1 TO #REPT2-TITLE-SUB
        pnd_Rept2_Mode_Sub.setValue(1);                                                                                                                                   //Natural: MOVE 1 TO #REPT2-MODE-SUB
        pnd_Rept2_Mode.setValue(pnd_Mode_Hold);                                                                                                                           //Natural: MOVE #MODE-HOLD TO #REPT2-MODE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Text.setValue("(ANNUAL)");                                                                                                                                    //Natural: MOVE '(ANNUAL)' TO #TEXT
        pnd_Rept2_Tot_Per_Pmt_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Pmt_Out), pnd_Rept2_Tot_Per_Pmt_In.add(pnd_Rept2_Tot_Per_Pmt_N_Crit));           //Natural: COMPUTE #REPT2-TOT-PER-PMT-OUT = #REPT2-TOT-PER-PMT-IN + #REPT2-TOT-PER-PMT-N-CRIT
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_Tot_Per_Pmt_Ct.subtract(pnd_Rept2_Tot_Per_Pmt_Out));                       //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-TOT-PER-PMT-CT - #REPT2-TOT-PER-PMT-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(1),new ColumnSpacing(3),pnd_Rept2_Tot_Per_Pmt_In, //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 1 ) 03X #REPT2-TOT-PER-PMT-IN ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-PMT-N-CRIT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-PMT-OUT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-PMT-CT ( EM = ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Pmt_N_Crit, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Pmt_Out, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Pmt_Ct, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ADD 1 TO #REPT2-TITLE-SUB
        pnd_Rept2_Tot_Per_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Dvd_Out), pnd_Rept2_Tot_Per_Dvd_In.add(pnd_Rept2_Tot_Per_Dvd_N_Crit));           //Natural: COMPUTE #REPT2-TOT-PER-DVD-OUT = #REPT2-TOT-PER-DVD-IN + #REPT2-TOT-PER-DVD-N-CRIT
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_Tot_Per_Dvd_Ct.subtract(pnd_Rept2_Tot_Per_Dvd_Out));                       //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-TOT-PER-DVD-CT - #REPT2-TOT-PER-DVD-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(2),new ColumnSpacing(3),pnd_Rept2_Tot_Per_Dvd_In, //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 2 ) 03X #REPT2-TOT-PER-DVD-IN ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-DVD-N-CRIT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-DVD-OUT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-DVD-CT ( EM = ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Dvd_N_Crit, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Dvd_Out, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Dvd_Ct, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Fin_Pay_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Pay_Out), pnd_Rept2_Tot_Fin_Pay_In.add(pnd_Rept2_Tot_Fin_Pay_N_Crit));           //Natural: COMPUTE #REPT2-TOT-FIN-PAY-OUT = #REPT2-TOT-FIN-PAY-IN + #REPT2-TOT-FIN-PAY-N-CRIT
        //*  10/01
        //*  06/09
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_Tot_Fin_Pay_Ct.subtract(pnd_Rept2_Tot_Fin_Pay_Out));                       //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-TOT-FIN-PAY-CT - #REPT2-TOT-FIN-PAY-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(3),pnd_Rept2_Tot_Fin_Pay_In, new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 3 ) #REPT2-TOT-FIN-PAY-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-FIN-PAY-N-CRIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-FIN-PAY-OUT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-FIN-PAY-CT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Fin_Pay_N_Crit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Fin_Pay_Out, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Fin_Pay_Ct, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Fin_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Dvd_Out), pnd_Rept2_Tot_Fin_Dvd_In.add(pnd_Rept2_Tot_Fin_Dvd_N_Crit));           //Natural: COMPUTE #REPT2-TOT-FIN-DVD-OUT = #REPT2-TOT-FIN-DVD-IN + #REPT2-TOT-FIN-DVD-N-CRIT
        //*  9/08 START
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_Tot_Fin_Dvd_Ct.subtract(pnd_Rept2_Tot_Fin_Dvd_Out));                       //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-TOT-FIN-DVD-CT - #REPT2-TOT-FIN-DVD-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(4),new ColumnSpacing(3),pnd_Rept2_Tot_Fin_Dvd_In, //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 4 ) 03X #REPT2-TOT-FIN-DVD-IN ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-FIN-DVD-N-CRIT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-FIN-DVD-OUT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-FIN-DVD-CT ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Fin_Dvd_N_Crit, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Fin_Dvd_Out, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Fin_Dvd_Ct, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_J).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #J ) = #REPT2-TOT-UNITS-PROD-IN ( #J ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #J )
            pnd_Rept2_Total_Units_Prod_Out_A.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #J ) TO #REPT2-TOTAL-UNITS-PROD-OUT-A
            pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_J).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #J ) - #REPT2-TOT-UNITS-PROD-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #J ) 03X #REPT2-TOT-UNITS-PROD-IN ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_J).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #J ) = #REPT2-TOT-UNITS-PYMT-IN ( #J ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #J )
            pnd_Rept2_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #J ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-A
            pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_J).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #J ) - #REPT2-TOT-UNITS-PYMT-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #J ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_J.greater(11)))                                                                                                                             //Natural: IF #J GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        //*  CHANGED FR 60 TO CREATE SEPERATE PAGE FOR PA SELECT
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  1 = TIAA  9 = REAL ESTATE 11=ACCESS 9/08
            if (condition(pnd_I.equals(1) || pnd_I.equals(9) || pnd_I.equals(11) || pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                   //Natural: IF #I = 1 OR = 9 OR = 11 OR #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_A.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-A
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #I ) - #REPT2-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-A
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Total_Units_Prod_Ct_A.subtract(pnd_Rept2_Total_Units_Prod_Out_A)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOTAL-UNITS-PROD-CT-A - #REPT2-TOTAL-UNITS-PROD-OUT-A
        pnd_Rept2_Prod_Titles_C.getValue(81).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 81 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(81),new ColumnSpacing(3),pnd_Rept2_Total_Units_Prod_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 81 ) 03X #REPT2-TOTAL-UNITS-PROD-IN-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNIT-PROD-N-CRIT-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-OUT-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-CT-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Prod_N_Crit_A, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Out_A, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Ct_A, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Total_Units_Pymt_Ct_A.subtract(pnd_Rept2_Total_Units_Pymt_Out_A)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOTAL-UNITS-PYMT-CT-A - #REPT2-TOTAL-UNITS-PYMT-OUT-A
        pnd_Rept2_Prod_Titles_2_C.getValue(82).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 82 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(82),new ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 82 ) 03X #REPT2-TOTAL-UNITS-PYMT-IN-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-OUT-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-CT-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Pymt_N_Crit_A, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Out_A, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Ct_A, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  =====================================================================
        //*  WILL POINT TO PA SELECT FUNDS
        //*  TO CREATE SEPERATE PAGE FOR PA SELECT
        pnd_Rept2_Title_Sub.nadd(20);                                                                                                                                     //Natural: ADD 20 TO #REPT2-TITLE-SUB
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        FOR04:                                                                                                                                                            //Natural: FOR #I = 41 TO 49
        for (pnd_I.setValue(41); condition(pnd_I.lessOrEqual(49)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                                                                             //Natural: IF #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_A_P.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                    //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-A-P
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #I ) - #REPT2-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_A_P.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                    //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-A-P
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Total_Units_Prod_Ct_A_P.subtract(pnd_Rept2_Total_Units_Prod_Out_A_P)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOTAL-UNITS-PROD-CT-A-P - #REPT2-TOTAL-UNITS-PROD-OUT-A-P
        pnd_Rept2_Prod_Titles_C.getValue(83).setValue("T/L TOTAL        UNITS");                                                                                          //Natural: MOVE 'T/L TOTAL        UNITS' TO #REPT2-PROD-TITLES-C ( 83 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(83),new ColumnSpacing(3),pnd_Rept2_Total_Units_Prod_In_A_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 83 ) 03X #REPT2-TOTAL-UNITS-PROD-IN-A-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNIT-PROD-N-CRITAP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-OUT-A-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-CT-A-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Prod_N_Critap, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Out_A_P, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Ct_A_P, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Total_Units_Pymt_Ct_A_P.subtract(pnd_Rept2_Total_Units_Pymt_Out_A_P)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOTAL-UNITS-PYMT-CT-A-P - #REPT2-TOTAL-UNITS-PYMT-OUT-A-P
        pnd_Rept2_Prod_Titles_2_C.getValue(84).setValue("T/L TOTAL     PAYMENTS");                                                                                        //Natural: MOVE 'T/L TOTAL     PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 84 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(84),new ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_In_A_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 84 ) 03X #REPT2-TOTAL-UNITS-PYMT-IN-A-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNIT-PYMT-N-CRITAP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-OUT-A-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-CT-A-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Pymt_N_Critap, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Out_A_P, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Ct_A_P, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Report_M() throws Exception                                                                                                                //Natural: #WRITE-REPORT-M
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        //*  9/08 START (ADD TIAA ACCESS)
        pnd_Text.setValue("(MONTHLY)");                                                                                                                                   //Natural: MOVE '(MONTHLY)' TO #TEXT
        pnd_J.setValue(29);                                                                                                                                               //Natural: ASSIGN #J := 29
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_J).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #J ) = #REPT2-TOT-UNITS-PROD-IN ( #J ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #J )
            pnd_Rept2_Total_Units_Prod_Out_M.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #J ) TO #REPT2-TOTAL-UNITS-PROD-OUT-M
            pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_J).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #J ) - #REPT2-TOT-UNITS-PROD-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #I ) 03X #REPT2-TOT-UNITS-PROD-IN ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_J).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #J ) = #REPT2-TOT-UNITS-PYMT-IN ( #J ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #J )
            pnd_Rept2_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #J ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-M
            pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_J).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #J ) - #REPT2-TOT-UNITS-PYMT-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #I ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_J.greater(31)))                                                                                                                             //Natural: IF #J GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.setValue(21);                                                                                                                                 //Natural: MOVE 21 TO #REPT2-TITLE-SUB
        FOR05:                                                                                                                                                            //Natural: FOR #I = 22 TO 40
        for (pnd_I.setValue(22); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  21 = TIAA  29 = REAL ESTATE 31=ACCESS 9/08
            if (condition(pnd_I.equals(29) || pnd_I.equals(31) || pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                                     //Natural: IF #I = 29 OR = 31 OR #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_M.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-M
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #I ) - #REPT2-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-M
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Total_Units_Prod_Ct_M.subtract(pnd_Rept2_Total_Units_Prod_Out_M)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOTAL-UNITS-PROD-CT-M - #REPT2-TOTAL-UNITS-PROD-OUT-M
        pnd_Rept2_Prod_Titles_C.getValue(81).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 81 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(81),new ColumnSpacing(3),pnd_Rept2_Total_Units_Prod_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 81 ) 03X #REPT2-TOTAL-UNITS-PROD-IN-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNIT-PROD-N-CRIT-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-OUT-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-CT-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Prod_N_Crit_M, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Out_M, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Ct_M, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Total_Units_Pymt_Ct_M.subtract(pnd_Rept2_Total_Units_Pymt_Out_M)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOTAL-UNITS-PYMT-CT-M - #REPT2-TOTAL-UNITS-PYMT-OUT-M
        pnd_Rept2_Prod_Titles_2_C.getValue(82).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 82 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(82),new ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 82 ) 03X #REPT2-TOTAL-UNITS-PYMT-IN-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-OUT-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-CT-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Pymt_N_Crit_M, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Out_M, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Ct_M, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ====== PA SELECT FUNDS FOR MONTHLY
        //*  WILL POINT TO PA SELECT FUNDS
        pnd_Rept2_Title_Sub.nadd(20);                                                                                                                                     //Natural: ADD 20 TO #REPT2-TITLE-SUB
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        FOR06:                                                                                                                                                            //Natural: FOR #I = 61 TO 80
        for (pnd_I.setValue(61); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                                                                             //Natural: IF #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_M_P.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                    //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-M-P
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOT-UNITS-PROD-CT ( #I ) - #REPT2-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_M_P.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                    //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-M-P
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_Total_Units_Prod_Ct_M_P.subtract(pnd_Rept2_Total_Units_Prod_Out_M_P)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-TOTAL-UNITS-PROD-CT-M-P - #REPT2-TOTAL-UNITS-PROD-OUT-M-P
        pnd_Rept2_Prod_Titles_C.getValue(83).setValue("T/L TOTAL        UNITS");                                                                                          //Natural: MOVE 'T/L TOTAL        UNITS' TO #REPT2-PROD-TITLES-C ( 83 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(83),new ColumnSpacing(3),pnd_Rept2_Total_Units_Prod_In_M_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 83 ) 03X #REPT2-TOTAL-UNITS-PROD-IN-M-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNIT-PROD-N-CRITMP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-OUT-M-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOTAL-UNITS-PROD-CT-M-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Prod_N_Critmp, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Out_M_P, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Prod_Ct_M_P, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_Total_Units_Pymt_Ct_M_P.subtract(pnd_Rept2_Total_Units_Pymt_Out_M_P)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-TOTAL-UNITS-PYMT-CT-M-P - #REPT2-TOTAL-UNITS-PYMT-OUT-M-P
        pnd_Rept2_Prod_Titles_2_C.getValue(84).setValue("T/L TOTAL     PAYMENTS");                                                                                        //Natural: MOVE 'T/L TOTAL     PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 84 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(84),new ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_In_M_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 84 ) 03X #REPT2-TOTAL-UNITS-PYMT-IN-M-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNIT-PYMT-N-CRITMP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-OUT-M-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOTAL-UNITS-PYMT-CT-M-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Unit_Pymt_N_Critmp, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Out_M_P, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Total_Units_Pymt_Ct_M_P, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Report_G_A() throws Exception                                                                                                              //Natural: #WRITE-REPORT-G-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Rept2_Mode.setValue("TOTAL");                                                                                                                                 //Natural: MOVE 'TOTAL' TO #REPT2-MODE
        pnd_Text.setValue("(ANNUAL)");                                                                                                                                    //Natural: MOVE '(ANNUAL)' TO #TEXT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        //*                TIAA PERIODIC PAYMENT
        pnd_Rept2_G_Tot_Per_Pmt_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Pmt_Out), pnd_Rept2_G_Tot_Per_Pmt_In.add(pnd_Rept2_G_Tot_Per_Pmt_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-PER-PMT-OUT = #REPT2-G-TOT-PER-PMT-IN + #REPT2-G-TOT-PER-PMT-N-CRIT
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_G_Tot_Per_Pmt_Ct.subtract(pnd_Rept2_G_Tot_Per_Pmt_Out));                   //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-G-TOT-PER-PMT-CT - #REPT2-G-TOT-PER-PMT-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(1),pnd_Rept2_G_Tot_Per_Pmt_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 1 ) #REPT2-G-TOT-PER-PMT-IN ( EM = ZZZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-PMT-N-CRIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-PMT-OUT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-PMT-CT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Pmt_N_Crit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Pmt_Out, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Pmt_Ct, new 
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  TOTAL TIAA PERIODIC DIVIDEND
        pnd_Rept2_G_Tot_Per_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Dvd_Out), pnd_Rept2_G_Tot_Per_Dvd_In.add(pnd_Rept2_G_Tot_Per_Dvd_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-PER-DVD-OUT = #REPT2-G-TOT-PER-DVD-IN + #REPT2-G-TOT-PER-DVD-N-CRIT
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_G_Tot_Per_Dvd_Ct.subtract(pnd_Rept2_G_Tot_Per_Dvd_Out));                   //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-G-TOT-PER-DVD-CT - #REPT2-G-TOT-PER-DVD-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(2),pnd_Rept2_G_Tot_Per_Dvd_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 2 ) #REPT2-G-TOT-PER-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-DVD-N-CRIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-DVD-OUT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-PER-DVD-CT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Dvd_N_Crit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Dvd_Out, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Per_Dvd_Ct, new 
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  TOTAL TIAA FINAL PAYMENT
        pnd_Rept2_G_Tot_Fin_Pay_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Pay_Out), pnd_Rept2_G_Tot_Fin_Pay_In.add(pnd_Rept2_G_Tot_Fin_Pay_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-FIN-PAY-OUT = #REPT2-G-TOT-FIN-PAY-IN + #REPT2-G-TOT-FIN-PAY-N-CRIT
        //*  10/01
        //*  10/15
        //*  10/01
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_G_Tot_Fin_Pay_Ct.subtract(pnd_Rept2_G_Tot_Fin_Pay_Out));                   //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-G-TOT-FIN-PAY-CT - #REPT2-G-TOT-FIN-PAY-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(3),pnd_Rept2_G_Tot_Fin_Pay_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 3 ) #REPT2-G-TOT-FIN-PAY-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-PAY-N-CRIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-PAY-OUT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-PAY-CT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Pay_N_Crit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Pay_Out, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Pay_Ct, new 
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  TOTAL TIAA FINAL DIVIDEND
        pnd_Rept2_G_Tot_Fin_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Dvd_Out), pnd_Rept2_G_Tot_Fin_Dvd_In.add(pnd_Rept2_G_Tot_Fin_Dvd_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-FIN-DVD-OUT = #REPT2-G-TOT-FIN-DVD-IN + #REPT2-G-TOT-FIN-DVD-N-CRIT
        //*  9/08 START (ADD ACCESS)
        pnd_Rept2_Tot_Diff.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff), pnd_Rept2_G_Tot_Fin_Dvd_Ct.subtract(pnd_Rept2_G_Tot_Fin_Dvd_Out));                   //Natural: COMPUTE #REPT2-TOT-DIFF = #REPT2-G-TOT-FIN-DVD-CT - #REPT2-G-TOT-FIN-DVD-OUT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(4),pnd_Rept2_G_Tot_Fin_Dvd_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 4 ) #REPT2-G-TOT-FIN-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-DVD-N-CRIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-DVD-OUT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-G-TOT-FIN-DVD-CT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Dvd_N_Crit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Dvd_Out, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Fin_Dvd_Ct, new 
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_J).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) = #REPT2-G-TOT-UNITS-PROD-IN ( #J ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #J )
            pnd_Rept2_G_Total_Units_Prod_Out_A.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-A
            pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_J).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #J ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #J ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 01X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(1),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_J).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #J ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #J )
            pnd_Rept2_G_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-A
            pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_J).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #J ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #J ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_J.greater(11)))                                                                                                                             //Natural: IF #J GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  1 = TIAA, 9 = REAL ESTATE 11=ACCESS 9/08
            if (condition(pnd_I.equals(1) || pnd_I.equals(9) || pnd_I.equals(11) || pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                   //Natural: IF #I = 1 OR = 9 OR = 11 OR #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_A.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-A
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #I ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 02X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-A
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 02X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Total_Units_Prod_Ct_A.subtract(pnd_Rept2_G_Total_Units_Prod_Out_A)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOTAL-UNITS-PROD-CT-A - #REPT2-G-TOTAL-UNITS-PROD-OUT-A
        pnd_Rept2_Prod_Titles_C.getValue(81).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 81 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(81),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 81 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRITA ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-CT-A ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Crita, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_A, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Ct_A, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Total_Units_Pymt_Ct_A.subtract(pnd_Rept2_G_Total_Units_Pymt_Out_A)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOTAL-UNITS-PYMT-CT-A - #REPT2-G-TOTAL-UNITS-PYMT-OUT-A
        pnd_Rept2_Prod_Titles_C.getValue(82).setValue("CREF TOTAL    PAYMENTS");                                                                                          //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-C ( 82 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(82),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 82 ) 03X #REPT2-G-TOTAL-UNITS-PYMT-IN-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-OUT-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-CT-A ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Pymt_N_Crita, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Out_A, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Ct_A, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ========= PA SELECT FUNDS   ================================
        pnd_Rept2_Title_Sub.setValue(40);                                                                                                                                 //Natural: MOVE 40 TO #REPT2-TITLE-SUB
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        FOR08:                                                                                                                                                            //Natural: FOR #I = 41 TO 60
        for (pnd_I.setValue(41); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                                                                             //Natural: IF #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_Ap.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                 //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-AP
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #I ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_Ap.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                 //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-AP
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Total_Units_Prod_Ct_A_P.subtract(pnd_Rept2_G_Total_Units_Prod_Out_Ap)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOTAL-UNITS-PROD-CT-A-P - #REPT2-G-TOTAL-UNITS-PROD-OUT-AP
        pnd_Rept2_Prod_Titles_C.getValue(83).setValue("T/L TOTAL        UNITS");                                                                                          //Natural: MOVE 'T/L TOTAL        UNITS' TO #REPT2-PROD-TITLES-C ( 83 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(83),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_A_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 83 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-A-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRIAP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-AP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-CT-A-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Criap, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_Ap, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Ct_A_P, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Total_Units_Pymt_Ct_A_P.subtract(pnd_Rept2_G_Total_Units_Pymt_Out_Ap)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOTAL-UNITS-PYMT-CT-A-P - #REPT2-G-TOTAL-UNITS-PYMT-OUT-AP
        pnd_Rept2_Prod_Titles_C.getValue(84).setValue("T/L TOTAL     PAYMENTS");                                                                                          //Natural: MOVE 'T/L TOTAL     PAYMENTS' TO #REPT2-PROD-TITLES-C ( 84 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(84),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_In_A_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 84 ) 03X #REPT2-G-TOTAL-UNITS-PYMT-IN-A-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNIT-PYMT-N-CRIAP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-OUT-AP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-CT-A-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Pymt_N_Criap, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Out_Ap, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Ct_A_P, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Report_G_M() throws Exception                                                                                                              //Natural: #WRITE-REPORT-G-M
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Rept2_Mode.setValue("TOTAL");                                                                                                                                 //Natural: MOVE 'TOTAL' TO #REPT2-MODE
        //*  9/08 START (ADD ACCESS)
        pnd_Text.setValue("(MONTHLY)");                                                                                                                                   //Natural: MOVE '(MONTHLY)' TO #TEXT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_J.setValue(29);                                                                                                                                               //Natural: ASSIGN #J := 29
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_J).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) = #REPT2-G-TOT-UNITS-PROD-IN ( #J ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #J )
            pnd_Rept2_G_Total_Units_Prod_Out_M.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-M
            pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_J).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #J ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #I ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #J ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_J), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_J).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_J))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #J ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #J )
            pnd_Rept2_G_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-M
            pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_J).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #J ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #J )
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_J),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #I ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #J ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_J.greater(31)))                                                                                                                             //Natural: IF #J GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        FOR09:                                                                                                                                                            //Natural: FOR #I = 21 TO 40
        for (pnd_I.setValue(21); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  21 = TIAA 29 = REAL ESTATE 31=ACCESS
            //*  9/08
            if (condition(pnd_I.equals(21) || pnd_I.equals(29) || pnd_I.equals(31) || pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                 //Natural: IF #I = 21 OR = 29 OR = 31 OR #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_M.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-M
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #I ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-M
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Total_Units_Prod_Ct_M.subtract(pnd_Rept2_G_Total_Units_Prod_Out_M)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOTAL-UNITS-PROD-CT-M - #REPT2-G-TOTAL-UNITS-PROD-OUT-M
        pnd_Rept2_Prod_Titles_C.getValue(81).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 81 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(81),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 81 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRITM ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-CT-M ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Critm, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_M, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Ct_M, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Total_Units_Pymt_Ct_M.subtract(pnd_Rept2_G_Total_Units_Pymt_Out_M)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOTAL-UNITS-PYMT-CT-M - #REPT2-G-TOTAL-UNITS-PYMT-OUT-M
        pnd_Rept2_Prod_Titles_C.getValue(82).setValue("CREF TOTAL    PAYMENTS");                                                                                          //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-C ( 82 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(82),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 82 ) 03X #REPT2-G-TOTAL-UNITS-PYMT-IN-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-OUT-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-CT-M ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Pymt_N_Critm, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Out_M, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Ct_M, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Rept2_Title_Sub.setValue(60);                                                                                                                                 //Natural: MOVE 60 TO #REPT2-TITLE-SUB
        FOR10:                                                                                                                                                            //Natural: FOR #I = 61 TO 80
        for (pnd_I.setValue(61); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub).equals(" ")))                                                                             //Natural: IF #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_Mp.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                 //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-MP
                pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOT-UNITS-PROD-CT ( #I ) - #REPT2-G-TOT-UNITS-PROD-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNITS-PROD-CT ( #I ) ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Prod_Ct.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_Mp.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                 //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-MP
                pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I).subtract(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) - #REPT2-G-TOT-UNITS-PYMT-OUT ( #I )
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOT-UNITS-PYMT-CT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(5),pnd_Rept2_G_Tot_Units_Pymt_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Tot_Diff_Unit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit), pnd_Rept2_G_Total_Units_Prod_Ct_M_P.subtract(pnd_Rept2_G_Total_Units_Prod_Out_Mp)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT = #REPT2-G-TOTAL-UNITS-PROD-CT-M-P - #REPT2-G-TOTAL-UNITS-PROD-OUT-MP
        pnd_Rept2_Prod_Titles_C.getValue(83).setValue("T/L TOTAL        UNITS");                                                                                          //Natural: MOVE 'T/L TOTAL        UNITS' TO #REPT2-PROD-TITLES-C ( 83 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(83),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_M_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 83 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-M-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRIMP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-MP ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-CT-M-P ( EM = ZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-DIFF-UNIT ( EM = ZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Crimp, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_Mp, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Ct_M_P, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Tot_Diff_Unit_Pymt.compute(new ComputeParameters(false, pnd_Rept2_Tot_Diff_Unit_Pymt), pnd_Rept2_G_Total_Units_Pymt_Ct_M_P.subtract(pnd_Rept2_G_Total_Units_Pymt_Out_Mp)); //Natural: COMPUTE #REPT2-TOT-DIFF-UNIT-PYMT = #REPT2-G-TOTAL-UNITS-PYMT-CT-M-P - #REPT2-G-TOTAL-UNITS-PYMT-OUT-MP
        pnd_Rept2_Prod_Titles_C.getValue(84).setValue("T/L TOTAL     PAYMENTS");                                                                                          //Natural: MOVE 'T/L TOTAL     PAYMENTS' TO #REPT2-PROD-TITLES-C ( 84 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(84),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_In_M_P,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 84 ) 03X #REPT2-G-TOTAL-UNITS-PYMT-IN-M-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNIT-PYMT-N-CRIMP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-OUT-MP ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-G-TOTAL-UNITS-PYMT-CT-M-P ( EM = ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-DIFF-UNIT-PYMT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Pymt_N_Crimp, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Out_Mp, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Units_Pymt_Ct_M_P, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Diff_Unit_Pymt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Up_Report2() throws Exception                                                                                                               //Natural: #FILL-UP-REPORT2
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*   CALCULATE THE TOTAL AT THE END OF THE PAGE FOR MONTHLY, ANNUAL PLUS
        //*   CREF AND PA SELECT FUNDS
        if (condition(pnd_Prod_Sub.equals(getZero())))                                                                                                                    //Natural: IF #PROD-SUB = 0
        {
            //*   PERIODIC PAYMENT & DIVIDENED
            pnd_Rept2_Tot_Per_Pmt_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Pmt_N_Crit), pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Per_Pmt_N_Crit));    //Natural: COMPUTE #REPT2-TOT-PER-PMT-N-CRIT = #REPT1-NET-PER-PMT + #REPT2-TOT-PER-PMT-N-CRIT
            pnd_Rept2_G_Tot_Per_Pmt_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Pmt_N_Crit), pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_G_Tot_Per_Pmt_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-PER-PMT-N-CRIT = #REPT1-NET-PER-PMT + #REPT2-G-TOT-PER-PMT-N-CRIT
            pnd_Rept2_Tot_Per_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Dvd_N_Crit), pnd_Rept1_Net_Per_Dvd.add(pnd_Rept2_Tot_Per_Dvd_N_Crit));    //Natural: COMPUTE #REPT2-TOT-PER-DVD-N-CRIT = #REPT1-NET-PER-DVD + #REPT2-TOT-PER-DVD-N-CRIT
            pnd_Rept2_G_Tot_Per_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Dvd_N_Crit), pnd_Rept1_Net_Per_Dvd.add(pnd_Rept2_G_Tot_Per_Dvd_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-PER-DVD-N-CRIT = #REPT1-NET-PER-DVD + #REPT2-G-TOT-PER-DVD-N-CRIT
            //*  FINAL PAYMENT & DIVIDEND
            pnd_Rept2_Tot_Fin_Pay_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Pay_N_Crit), pnd_Rept1_Net_Final_Pmt.add(pnd_Rept2_Tot_Fin_Pay_N_Crit));  //Natural: COMPUTE #REPT2-TOT-FIN-PAY-N-CRIT = #REPT1-NET-FINAL-PMT + #REPT2-TOT-FIN-PAY-N-CRIT
            //*  11052001
            pnd_Rept2_G_Tot_Fin_Pay_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Pay_N_Crit), pnd_Rept1_Net_Final_Pmt.add(pnd_Rept2_G_Tot_Fin_Pay_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-FIN-PAY-N-CRIT = #REPT1-NET-FINAL-PMT + #REPT2-G-TOT-FIN-PAY-N-CRIT
            pnd_Rept2_Tot_Fin_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Dvd_N_Crit), pnd_Rept1_Net_Final_Divd.add(pnd_Rept2_Tot_Fin_Dvd_N_Crit)); //Natural: COMPUTE #REPT2-TOT-FIN-DVD-N-CRIT = #REPT1-NET-FINAL-DIVD + #REPT2-TOT-FIN-DVD-N-CRIT
            //*  11082001
            pnd_Rept2_G_Tot_Fin_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Dvd_N_Crit), pnd_Rept1_Net_Final_Divd.add(pnd_Rept2_G_Tot_Fin_Dvd_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-FIN-DVD-N-CRIT = #REPT1-NET-FINAL-DIVD + #REPT2-G-TOT-FIN-DVD-N-CRIT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUAL CREF PRODUCTS
        if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("U") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("2")))                                                  //Natural: IF ( #W-COMPANY-CD = 'U' OR = '2' )
        {
            if (condition(pnd_Prod_Sub.greaterOrEqual(1) && pnd_Prod_Sub.lessOrEqual(20)))                                                                                //Natural: IF ( #PROD-SUB = 1 THRU 20 )
            {
                pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Pymt_N_Crit_A.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                           //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A = #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                      //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Total_Unit_Pymt_N_Crita.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                          //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA = #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA + #REPT1-NET-PER-PMT
                pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Prod_N_Crit_A.nadd(pnd_Rept1_Net_Units);                                                                                             //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRIT-A = #REPT2-TOTAL-UNIT-PROD-N-CRIT-A + #REPT1-NET-UNITS
                pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                        //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
                pnd_Rept2_G_Total_Unit_Prod_N_Crita.nadd(pnd_Rept1_Net_Units);                                                                                            //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRITA = #REPT2-G-TOTAL-UNIT-PROD-N-CRITA + #REPT1-NET-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ANNUAL PA SELECT FUND
                if (condition(pnd_Prod_Sub.greaterOrEqual(40) && pnd_Prod_Sub.lessOrEqual(60)))                                                                           //Natural: IF ( #PROD-SUB = 40 THRU 60 )
                {
                    pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),    //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                        pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
                    pnd_Rept2_Total_Unit_Pymt_N_Critap.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                       //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRITAP = #REPT2-TOTAL-UNIT-PYMT-N-CRITAP + #REPT1-NET-PER-PMT
                    pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                  //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
                    pnd_Rept2_G_Total_Unit_Pymt_N_Criap.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                      //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRIAP = #REPT2-G-TOTAL-UNIT-PYMT-N-CRIAP + #REPT1-NET-PER-PMT
                    pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),    //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                        pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
                    pnd_Rept2_Total_Unit_Prod_N_Critap.nadd(pnd_Rept1_Net_Units);                                                                                         //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRITAP = #REPT2-TOTAL-UNIT-PROD-N-CRITAP + #REPT1-NET-UNITS
                    pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                    //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
                    pnd_Rept2_G_Total_Unit_Prod_N_Criap.nadd(pnd_Rept1_Net_Units);                                                                                        //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRIAP = #REPT2-G-TOTAL-UNIT-PROD-N-CRIAP + #REPT1-NET-UNITS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRODCESS MONTHLY RECORDS
        //*  MONTHLY CREF PRODUCTS ONLY
        if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("W") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("4")))                                                  //Natural: IF #W-COMPANY-CD = 'W' OR = '4'
        {
            pnd_Prod_Sub.compute(new ComputeParameters(false, pnd_Prod_Sub), (pnd_Prod_Sub.add(20)));                                                                     //Natural: COMPUTE #PROD-SUB = ( #PROD-SUB + 20 )
            if (condition(pnd_Prod_Sub.greaterOrEqual(20) && pnd_Prod_Sub.lessOrEqual(40)))                                                                               //Natural: IF ( #PROD-SUB = 20 THRU 40 )
            {
                pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Pymt_N_Crit_M.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                           //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M = #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                      //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Total_Unit_Pymt_N_Critm.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                          //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM = #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM + #REPT1-NET-PER-PMT
                pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Prod_N_Crit_M.nadd(pnd_Rept1_Net_Units);                                                                                             //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRIT-M = #REPT2-TOTAL-UNIT-PROD-N-CRIT-M + #REPT1-NET-UNITS
                pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                        //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
                pnd_Rept2_G_Total_Unit_Prod_N_Critm.nadd(pnd_Rept1_Net_Units);                                                                                            //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRITM = #REPT2-G-TOTAL-UNIT-PROD-N-CRITM + #REPT1-NET-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  MONTHLY PA SELECT ONLY
                if (condition(pnd_Prod_Sub.greaterOrEqual(60) && pnd_Prod_Sub.lessOrEqual(80)))                                                                           //Natural: IF ( #PROD-SUB = 60 THRU 80 )
                {
                    pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),    //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                        pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
                    pnd_Rept2_Total_Unit_Pymt_N_Critmp.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                       //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRITMP = #REPT2-TOTAL-UNIT-PYMT-N-CRITMP + #REPT1-NET-PER-PMT
                    pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                  //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
                    pnd_Rept2_G_Total_Unit_Pymt_N_Critm.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                      //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM = #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM + #REPT1-NET-PER-PMT
                    pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),    //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                        pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
                    pnd_Rept2_Total_Unit_Prod_N_Critmp.nadd(pnd_Rept1_Net_Units);                                                                                         //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRITMP = #REPT2-TOTAL-UNIT-PROD-N-CRITMP + #REPT1-NET-UNITS
                    pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                    //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
                    pnd_Rept2_G_Total_Unit_Prod_N_Crimp.nadd(pnd_Rept1_Net_Units);                                                                                        //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRIMP = #REPT2-G-TOTAL-UNIT-PROD-N-CRIMP + #REPT1-NET-UNITS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Write_Report_2() throws Exception                                                                                                                //Natural: #WRITE-REPORT-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
                                                                                                                                                                          //Natural: PERFORM #MODE-TRANS
        sub_Pnd_Mode_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #READ-CONTROL-RECORD
        sub_Pnd_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-A
        sub_Pnd_Write_Report_A();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-M
        sub_Pnd_Write_Report_M();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Pnd_Product_Code_Trans() throws Exception                                                                                                            //Natural: #PRODUCT-CODE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1 ") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1G") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1S"))) //Natural: IF #W-PRODUCT-CDE = '1 ' OR = '1G' OR = '1S'
        {
            pnd_Prod_Sub.setValue(0);                                                                                                                                     //Natural: MOVE 0 TO #PROD-SUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Record_1_Pnd_W_Product_Cde.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde, MoveOption.RightJustified);                                                 //Natural: MOVE RIGHT #W-PRODUCT-CDE TO #W-PRODUCT-CDE
            DbsUtil.examine(new ExamineSource(pnd_Work_Record_1_Pnd_W_Product_Cde), new ExamineSearch(" "), new ExamineReplace("0"));                                     //Natural: EXAMINE #W-PRODUCT-CDE FOR ' ' REPLACE '0'
            pnd_Prod_Sub.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde_Num);                                                                                               //Natural: MOVE #W-PRODUCT-CDE-NUM TO #PROD-SUB
        }                                                                                                                                                                 //Natural: END-IF
        iaan051a_Fund.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde);                                                                                                      //Natural: MOVE #W-PRODUCT-CDE TO IAAN051A-FUND
        iaan051a_Length.setValue(3);                                                                                                                                      //Natural: MOVE 3 TO IAAN051A-LENGTH
        iaan051a_Desc.reset();                                                                                                                                            //Natural: RESET IAAN051A-DESC IAAN051A-CMPY-DESC #PRODUCT-NAME-RPT3
        iaan051a_Cmpy_Desc.reset();
        pnd_Product_Name_Rpt3.reset();
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), iaan051a_Fund, iaan051a_Desc, iaan051a_Cmpy_Desc, iaan051a_Length);                                    //Natural: CALLNAT 'IAAN051A' USING IAAN051A-FUND IAAN051A-DESC IAAN051A-CMPY-DESC IAAN051A-LENGTH
        if (condition(Global.isEscape())) return;
        if (condition(iaan051a_Desc.equals(" ")))                                                                                                                         //Natural: IF IAAN051A-DESC = ' '
        {
            pnd_Product_Name_Rpt3.setValue("CHECK PROD. CODE");                                                                                                           //Natural: MOVE 'CHECK PROD. CODE' TO #PRODUCT-NAME-RPT3
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUAL
        if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("U") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("2")))                                                  //Natural: IF #W-COMPANY-CD = 'U' OR = '2'
        {
            pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                     //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
            pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode.setValue("-A");                                                                                              //Natural: MOVE '-A' TO #PRODUCT-NAME-RPT3-MODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MONTHLY
            if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("W") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("4")))                                              //Natural: IF #W-COMPANY-CD = 'W' OR = '4'
            {
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode.setValue("-M");                                                                                          //Natural: MOVE '-M' TO #PRODUCT-NAME-RPT3-MODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
                if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1")))                                                                                           //Natural: IF #W-PRODUCT-CDE = '1'
                {
                    pnd_Product_Name_Rpt3.setValue("TIAA GROUP      ");                                                                                                   //Natural: MOVE 'TIAA GROUP      ' TO #PRODUCT-NAME-RPT3
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Rept2_Tot_Per_Pmt_In.reset();                                                                                                                                 //Natural: RESET #REPT2-TOT-PER-PMT-IN #REPT2-TOT-PER-DVD-IN #REPT2-TOT-PER-PMT-OUT #REPT2-TOT-PER-DVD-OUT #REPT2-TOT-PER-PMT-CT #REPT2-TOT-PER-DVD-CT #REPT2-TOT-DIFF #REPT2-TOT-DIFF-UNIT #REPT2-TOT-DIFF-UNIT-PYMT #REPT2-TOT-PER-PMT-N-CRIT #REPT2-TOT-PER-DVD-N-CRIT #REPT2-TOT-FIN-PAY-OUT #REPT2-TOT-FIN-DVD-OUT #REPT2-TOT-FIN-PAY-CT #REPT2-TOT-FIN-DVD-CT #REPT2-TOT-FIN-PAY-N-CRIT #REPT2-TOT-FIN-DVD-N-CRIT #REPT2-TOT-UNITS-PROD-IN ( 1:80 ) #REPT2-TOT-UNITS-PYMT-IN ( 1:80 ) #REPT2-TOT-UNIT-PROD-N-CRIT ( 1:80 ) #REPT2-TOT-UNIT-PYMT-N-CRIT ( 1:80 ) #REPT2-TOT-UNITS-PROD-OUT ( 1:80 ) #REPT2-TOT-UNITS-PYMT-OUT ( 1:80 ) #REPT2-TOT-UNITS-PROD-CT ( 1:80 ) #REPT2-TOT-UNITS-PYMT-CT ( 1:80 ) #REPT2-TOTAL-UNITS-PYMT-IN-M #REPT2-TOTAL-UNITS-PROD-IN-M #REPT2-TOTAL-UNITS-PROD-OUT-M #REPT2-TOTAL-UNITS-PYMT-OUT-M #REPT2-TOTAL-UNITS-PROD-CT-M #REPT2-TOTAL-UNITS-PYMT-CT-M #REPT2-TOTAL-UNIT-PROD-N-CRIT-M #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M #REPT2-TOTAL-UNITS-PYMT-IN-M-P #REPT2-TOTAL-UNITS-PROD-IN-M-P #REPT2-TOTAL-UNITS-PROD-OUT-M-P #REPT2-TOTAL-UNITS-PYMT-OUT-M-P #REPT2-TOTAL-UNITS-PROD-CT-M-P #REPT2-TOTAL-UNITS-PYMT-CT-M-P #REPT2-TOTAL-UNIT-PROD-N-CRITMP #REPT2-TOTAL-UNIT-PYMT-N-CRITMP #REPT2-TOTAL-UNITS-PYMT-IN-A #REPT2-TOTAL-UNITS-PROD-IN-A #REPT2-TOTAL-UNITS-PROD-OUT-A #REPT2-TOTAL-UNITS-PYMT-OUT-A #REPT2-TOTAL-UNITS-PROD-CT-A #REPT2-TOTAL-UNITS-PYMT-CT-A #REPT2-TOTAL-UNIT-PROD-N-CRIT-A #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A #REPT2-TOTAL-UNITS-PYMT-IN-A-P #REPT2-TOTAL-UNITS-PROD-IN-A-P #REPT2-TOTAL-UNITS-PROD-OUT-A-P #REPT2-TOTAL-UNITS-PYMT-OUT-A-P #REPT2-TOTAL-UNITS-PROD-CT-A-P #REPT2-TOTAL-UNITS-PYMT-CT-A-P #REPT2-TOTAL-UNIT-PROD-N-CRITAP #REPT2-TOTAL-UNIT-PYMT-N-CRITAP #REPT2-TOTAL-UNITS-PYMT-IN-M #REPT2-TOTAL-UNITS-PROD-IN-M #REPT2-TOTAL-UNITS-PROD-OUT-M #REPT2-TOTAL-UNITS-PYMT-OUT-M #REPT2-TOTAL-UNITS-PROD-CT-M #REPT2-TOTAL-UNITS-PYMT-IN-M-P #REPT2-TOTAL-UNITS-PROD-IN-M-P #REPT2-TOTAL-UNITS-PROD-OUT-M-P #REPT2-TOTAL-UNITS-PYMT-OUT-M-P #REPT2-TOTAL-UNITS-PROD-CT-M-P #REPT2-TOTAL-UNITS-PYMT-CT-M #REPT2-TOTAL-UNIT-PROD-N-CRIT-M #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M #REPT2-TOTAL-UNITS-PYMT-CT-M-P #REPT2-TOTAL-UNIT-PROD-N-CRITMP #REPT2-TOTAL-UNIT-PYMT-N-CRITMP
        pnd_Rept2_Tot_Per_Dvd_In.reset();
        pnd_Rept2_Tot_Per_Pmt_Out.reset();
        pnd_Rept2_Tot_Per_Dvd_Out.reset();
        pnd_Rept2_Tot_Per_Pmt_Ct.reset();
        pnd_Rept2_Tot_Per_Dvd_Ct.reset();
        pnd_Rept2_Tot_Diff.reset();
        pnd_Rept2_Tot_Diff_Unit.reset();
        pnd_Rept2_Tot_Diff_Unit_Pymt.reset();
        pnd_Rept2_Tot_Per_Pmt_N_Crit.reset();
        pnd_Rept2_Tot_Per_Dvd_N_Crit.reset();
        pnd_Rept2_Tot_Fin_Pay_Out.reset();
        pnd_Rept2_Tot_Fin_Dvd_Out.reset();
        pnd_Rept2_Tot_Fin_Pay_Ct.reset();
        pnd_Rept2_Tot_Fin_Dvd_Ct.reset();
        pnd_Rept2_Tot_Fin_Pay_N_Crit.reset();
        pnd_Rept2_Tot_Fin_Dvd_N_Crit.reset();
        pnd_Rept2_Tot_Units_Prod_In.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Units_Pymt_In.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Units_Prod_Out.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Units_Pymt_Out.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Units_Prod_Ct.getValue(1,":",80).reset();
        pnd_Rept2_Tot_Units_Pymt_Ct.getValue(1,":",80).reset();
        pnd_Rept2_Total_Units_Pymt_In_M.reset();
        pnd_Rept2_Total_Units_Prod_In_M.reset();
        pnd_Rept2_Total_Units_Prod_Out_M.reset();
        pnd_Rept2_Total_Units_Pymt_Out_M.reset();
        pnd_Rept2_Total_Units_Prod_Ct_M.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_M.reset();
        pnd_Rept2_Total_Unit_Prod_N_Crit_M.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Crit_M.reset();
        pnd_Rept2_Total_Units_Pymt_In_M_P.reset();
        pnd_Rept2_Total_Units_Prod_In_M_P.reset();
        pnd_Rept2_Total_Units_Prod_Out_M_P.reset();
        pnd_Rept2_Total_Units_Pymt_Out_M_P.reset();
        pnd_Rept2_Total_Units_Prod_Ct_M_P.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_M_P.reset();
        pnd_Rept2_Total_Unit_Prod_N_Critmp.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Critmp.reset();
        pnd_Rept2_Total_Units_Pymt_In_A.reset();
        pnd_Rept2_Total_Units_Prod_In_A.reset();
        pnd_Rept2_Total_Units_Prod_Out_A.reset();
        pnd_Rept2_Total_Units_Pymt_Out_A.reset();
        pnd_Rept2_Total_Units_Prod_Ct_A.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_A.reset();
        pnd_Rept2_Total_Unit_Prod_N_Crit_A.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Crit_A.reset();
        pnd_Rept2_Total_Units_Pymt_In_A_P.reset();
        pnd_Rept2_Total_Units_Prod_In_A_P.reset();
        pnd_Rept2_Total_Units_Prod_Out_A_P.reset();
        pnd_Rept2_Total_Units_Pymt_Out_A_P.reset();
        pnd_Rept2_Total_Units_Prod_Ct_A_P.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_A_P.reset();
        pnd_Rept2_Total_Unit_Prod_N_Critap.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Critap.reset();
        pnd_Rept2_Total_Units_Pymt_In_M.reset();
        pnd_Rept2_Total_Units_Prod_In_M.reset();
        pnd_Rept2_Total_Units_Prod_Out_M.reset();
        pnd_Rept2_Total_Units_Pymt_Out_M.reset();
        pnd_Rept2_Total_Units_Prod_Ct_M.reset();
        pnd_Rept2_Total_Units_Pymt_In_M_P.reset();
        pnd_Rept2_Total_Units_Prod_In_M_P.reset();
        pnd_Rept2_Total_Units_Prod_Out_M_P.reset();
        pnd_Rept2_Total_Units_Pymt_Out_M_P.reset();
        pnd_Rept2_Total_Units_Prod_Ct_M_P.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_M.reset();
        pnd_Rept2_Total_Unit_Prod_N_Crit_M.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Crit_M.reset();
        pnd_Rept2_Total_Units_Pymt_Ct_M_P.reset();
        pnd_Rept2_Total_Unit_Prod_N_Critmp.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Critmp.reset();
    }
    private void sub_Pnd_Edit_Check() throws Exception                                                                                                                    //Natural: #EDIT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        if (condition(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(100) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(601) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(602)  //Natural: IF #W-CNTRCT-MODE-IND = 100 OR = 601 OR = 602 OR = 603 OR = 701 OR = 702 OR = 703 OR = 704 OR = 705 OR = 706 OR = 801 OR = 802 OR = 803 OR = 804 OR = 805 OR = 806 OR = 807 OR = 808 OR = 809 OR = 810 OR = 811 OR = 812
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(603) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(701) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(702) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(703) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(704) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(705) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(706) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(801) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(802) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(803) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(804) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(805) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(806) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(807) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(808) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(809) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(810) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(811) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(812)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOHDR,"| INVALID MODE FOUND ",pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,"    |");                                            //Natural: WRITE NOHDR '| INVALID MODE FOUND ' #W-CNTRCT-MODE-IND '    |'
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);                                                                             //Natural: WRITE 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((((pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1 ") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1G")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1S"))  //Natural: IF ( #W-PRODUCT-CDE = '1 ' OR = '1G' OR = '1S' ) AND ( #W-TIAA-TOT-PER-AMT = 0 ) AND ( #W-TIAA-TOT-DIV-AMT = 0 )
            && pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.equals(getZero())) && pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.equals(getZero()))))
        {
            getReports().write(0, ReportOption.NOHDR,"| NO MONEY TEACHERS PRODUCT","TIAA-TOT-PER-AMT = ",pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt,"TIAA-TOT-DIV-AMT = ",  //Natural: WRITE NOHDR '| NO MONEY TEACHERS PRODUCT' 'TIAA-TOT-PER-AMT = ' #W-TIAA-TOT-PER-AMT 'TIAA-TOT-DIV-AMT = ' #W-TIAA-TOT-DIV-AMT 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt,"CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((((((((((pnd_Work_Record_1_Pnd_W_Product_Cde.equals("02") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("03")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("04"))  //Natural: IF ( #W-PRODUCT-CDE = '02' OR = '03' OR = '04' OR = '05' OR = '06' OR = '07' OR = '08' OR = '09' OR = '10' OR = '11' ) AND #W-CREF-UNITS-CNT = 0
            || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("05")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("06")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("07")) 
            || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("08")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("09")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("10")) 
            || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("11")) && pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.equals(getZero()))))
        {
            getReports().write(0, ReportOption.NOHDR,"| NO UNITS CREF FUND ",pnd_Work_Record_1_Pnd_W_Product_Cde,"CREF-UNITS-CNT = ",pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, //Natural: WRITE NOHDR '| NO UNITS CREF FUND ' #W-PRODUCT-CDE 'CREF-UNITS-CNT = ' #W-CREF-UNITS-CNT 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
                "CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((((((((pnd_Work_Record_1_Pnd_W_Product_Cde.equals("41") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("42")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("43"))  //Natural: IF ( #W-PRODUCT-CDE = '41' OR = '42' OR = '43' OR = '44' OR = '45' OR = '46' OR = '47' OR = '48' OR = '49' ) AND #W-CREF-UNITS-CNT = 0
            || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("44")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("45")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("46")) 
            || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("47")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("48")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("49")) 
            && pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.equals(getZero()))))
        {
            getReports().write(0, ReportOption.NOHDR,"| NO UNITS PA  FUND ",pnd_Work_Record_1_Pnd_W_Product_Cde,"CREF-UNITS-CNT = ",pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, //Natural: WRITE NOHDR '| NO UNITS PA  FUND ' #W-PRODUCT-CDE 'CREF-UNITS-CNT = ' #W-CREF-UNITS-CNT 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
                "CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("  ")))                                                                                                  //Natural: IF #W-PRODUCT-CDE = '  '
        {
            getReports().write(0, ReportOption.NOHDR,"| NO PRODUCT FOUND ",pnd_Work_Record_1_Pnd_W_Product_Cde,pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,                    //Natural: WRITE NOHDR '| NO PRODUCT FOUND ' #W-PRODUCT-CDE #W-TRANS-PPCN-NBR '    |'
                "    |");
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr.equals(" ")))                                                                                                //Natural: IF #W-TRANS-PPCN-NBR = ' '
        {
            getReports().write(0, ReportOption.NOHDR,"| PPCN-NBR",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,"    |");                                                        //Natural: WRITE NOHDR '| PPCN-NBR' #W-TRANS-PPCN-NBR '    |'
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Seperator);                                                                                                                         //Natural: WRITE #SEPERATOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(6),"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(12),"IA ADMINISTRATION MODE CONTROL FOR PAYMENTS DUE ",pnd_Date,  //Natural: WRITE ( 1 ) NOTITLE NOHDR 06X 'PROGRAM:' *PROGRAM 12X 'IA ADMINISTRATION MODE CONTROL FOR PAYMENTS DUE ' #DATE ( EM = ZZ/ZZ/9999 ) 24X 'PAGE :' *PAGE-NUMBER ( 1 ) / 04X 'RUN DATE : ' *DATU 22X '     IA TRANSACTIONS   ' // 26X 'FINAL          '/ '    TRAN   MO TR    CON  PAYMENT ANNUT   CONTRACT' 14X 'PERIODIC      PERIODIC   TIAA/CREF         FINAL         FINAL' 2X 'INP' / '    DATE   DE ANN   OPT    DATE  DEATH     NUMBER' 01X ' PROD.        PAYMENT      DIVIDEND ' 06X 'UNITS       PAYMENT      DIVIDEND  OUT' / '--------  --- ---   ---  ------- ----- -----------' '-----  ------------- -------------' 01X '-----------   -----------   -----------  ---' /
                        new ReportEditMask ("ZZ/ZZ/9999"),new ColumnSpacing(24),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(4),"RUN DATE : ",Global.getDATU(),new 
                        ColumnSpacing(22),"     IA TRANSACTIONS   ",NEWLINE,NEWLINE,new ColumnSpacing(26),"FINAL          ",NEWLINE,"    TRAN   MO TR    CON  PAYMENT ANNUT   CONTRACT",new 
                        ColumnSpacing(14),"PERIODIC      PERIODIC   TIAA/CREF         FINAL         FINAL",new ColumnSpacing(2),"INP",NEWLINE,"    DATE   DE ANN   OPT    DATE  DEATH     NUMBER",new 
                        ColumnSpacing(1)," PROD.        PAYMENT      DIVIDEND ",new ColumnSpacing(6),"UNITS       PAYMENT      DIVIDEND  OUT",NEWLINE,"--------  --- ---   ---  ------- ----- -----------","-----  ------------- -------------",new 
                        ColumnSpacing(1),"-----------   -----------   -----------  ---",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(6),"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(12),"IA ADMINISTRATION MODE CONTROL FOR PAYMENTS DUE ",pnd_Date,  //Natural: WRITE ( 2 ) NOTITLE NOHDR 06X 'PROGRAM:' *PROGRAM 12X 'IA ADMINISTRATION MODE CONTROL FOR PAYMENTS DUE ' #DATE ( EM = ZZ/ZZ/9999 ) 24X 'PAGE :' *PAGE-NUMBER ( 2 ) / 04X 'RUN DATE : ' *DATU 23X 'IA TRANSACTIONS' 1X #TEXT // 57X ' IA    '/ ' MODE  FUND                       MASTER INPUT' '      TRANSACTIONS        MASTER OUTPUT' '      CONTROL FILE          DIFFERENCE' / 01X '----  ------------------------  --------------' '     --------------      --------------' '     --------------       --------------' /
                        new ReportEditMask ("ZZ/ZZ/9999"),new ColumnSpacing(24),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,new ColumnSpacing(4),"RUN DATE : ",Global.getDATU(),new 
                        ColumnSpacing(23),"IA TRANSACTIONS",new ColumnSpacing(1),pnd_Text,NEWLINE,NEWLINE,new ColumnSpacing(57)," IA    ",NEWLINE," MODE  FUND                       MASTER INPUT","      TRANSACTIONS        MASTER OUTPUT","      CONTROL FILE          DIFFERENCE",NEWLINE,new 
                        ColumnSpacing(1),"----  ------------------------  --------------","     --------------      --------------","     --------------       --------------",
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(3, "LS=132 PS=60");
        Global.format(4, "LS=132 PS=60");
        Global.format(5, "LS=132 PS=60");
    }
}
