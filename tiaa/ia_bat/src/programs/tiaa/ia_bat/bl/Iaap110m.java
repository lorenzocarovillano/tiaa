/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:34 PM
**        * FROM NATURAL PROGRAM : Iaap110m
************************************************************
**        * FILE NAME            : Iaap110m.java
**        * CLASS NAME           : Iaap110m
**        * INSTANCE NAME        : Iaap110m
************************************************************
***********************************************************************
*                  MONTHLY MODE CONTROL REPORT                        *
*                                                                     *
*   PROGRAM    :-  IAAP110M                                           *
*   DATE       :-  11/17/1997                                         *
*   AUTHOR     :-  LEN BERNSTEIN                                      *
*   DESC       :-  THIS REPORT READS THE MONTHLY WORK FILE WHICH IS   *
*                  CREATED BY PROGRAM IAAP100 AND SORTED BY COMPANY/  *
*                  FUND.  IT PRODUCES A SERIES OF MODE CONTROL        *
*                  REPORTS. THERE ARE A FIXED NUMBER OF ONE           *
*                  PAGE REPORTS AND A SERIES OF TOTAL REPORTS         *
*                                                                     *
*                  PAGE 8 :- TOTALS REPORT                            *
*   HISTORY                                                           *
*  MM/DD/YY  NAME                                                     *
*  -------- -------- ------------------------------------------------ *
*  04/11/00          PA SELECT FUNDS CHANGED 'IAAN0500' TO 'IAAN0510' *
*                    'IAAN050A' TO 'IAAN051A' 'IAAN0501' TO 'IAAN0511'*
*  -  05/00          PA SELECT FUNDS CHANGES, NO LONGER USES          *
*                    'IAAA0500', 'IAAN051E', CHANGED 'IAAN050A' TO    *
*                    'IAAN051A',                                      *
*  04/29/02          RESTOWED FOR OIA CHANGES  IN IAAN051A, IAAN051E  *
*                    AND IAAN0511                                     *
*                                                                     *
*  06/18/03          ADDED NEW ROLL DEST CODES 57BT & 57BC            *
*                    DO SCAN ON 6/03                                  *
*
*  08/28/06          ADDED PREFIX S0 FOR REA
*                    DO SCAN ON 8/06
*                                                                     *
*  09/24/08          ADDED NEW FUND FOR TIAA ACCESS - TO BE PROCESSED
*                    LIKE REA       DO SCAN ON 9/08
*  03/16/12          FIELD EXPANSION FOR RATE BASE - RECATALOG ONLY   *
*                    DUE TO CHANGES IN IAAL110.
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap110m extends BLNatBase
{
    // Data Areas
    private LdaIaal110 ldaIaal110;
    private LdaIaal050 ldaIaal050;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup iaaa0510;
    private DbsField iaaa0510_Pnd_Cm_Std_Srt_Seq;
    private DbsField iaaa0510_Pnd_Ia_Std_Nm_Cd;

    private DbsGroup pnd_Input_Record_Work_2;
    private DbsField pnd_Input_Record_Work_2_Pnd_Call_Type;
    private DbsField pnd_Z;
    private DbsField pnd_Blnk5;
    private DbsField pnd_U;
    private DbsField pnd_Y;
    private DbsField pnd_G;
    private DbsField pnd_Sub;
    private DbsField pnd_Pa_Contract;
    private DbsField pnd_Cref_Contract;
    private DbsField pnd_End_Of_Tiaa;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Table_Date;
    private DbsField pnd_W_Save_Check_Dte_A;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_1;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_2;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_3;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Cc;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Yy;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Mm;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_4;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Dd;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_5;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_6;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_7;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_8;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_9;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_10;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_11;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_12;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Return_Code;

    private DbsGroup pnd_Table;
    private DbsField pnd_Table_Pnd_Tbl_Fund_Cde;
    private DbsField pnd_Table_Pnd_Tbl_Unit_Value;
    private DbsField pnd_Table_Pnd_Tbl_Issue_Date;
    private DbsField pnd_Cnt_Tot;
    private DbsField pnd_Cnt1;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Cnt_None;
    private DbsField pnd_Cnt_1s;
    private DbsField pnd_Cnt_1s1;
    private DbsField pnd_Cnt_1s2;
    private DbsField pnd_Cnt_1s3;
    private DbsField pnd_Cnt_1s_None;
    private DbsField pnd_Cnt_1z;
    private DbsField pnd_Cnt_1z1;
    private DbsField pnd_Cnt_1z2;
    private DbsField pnd_Cnt_1z_None;
    private DbsField pnd_Cnt_Cref;
    private DbsField pnd_Cnt_Cref1;
    private DbsField pnd_Cnt_Cref2;
    private DbsField pnd_Cnt_Cref3;
    private DbsField pnd_Cnt_Cref_None;
    private DbsField pnd_Fn_Code;
    private DbsField pnd_Ck_Date;
    private DbsField pnd_U_Val;
    private DbsField pnd_Rc;
    private DbsField pnd_Issu_Date;
    private DbsField pnd_Is_Date;

    private DbsGroup pnd_Is_Date__R_Field_13;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Yyyy;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Mm;

    private DbsGroup pnd_Is_Date__R_Field_14;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Mm_A;
    private DbsField pnd_Fund_Code_2;
    private DbsField pnd_Len;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_Fund_Desc;

    private DbsGroup pnd_Fund_Desc__R_Field_15;
    private DbsField pnd_Fund_Desc_Pnd_Fund_Desc_10;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_Table_Fund_Desc;
    private DbsField pnd_Fnd_Desc;
    private DbsField pnd_W_Variable;
    private DbsField pnd_Heading1;
    private DbsField pnd_Old_Tiaa_Cmpny_Cde;
    private DbsField pnd_Debug_1;
    private DbsField pnd_Debug_2;

    private DbsGroup pnd_Iaan0501_Pda;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Fund_1;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Fund_2;
    private DbsField pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Latest_Factor;
    private DbsField pnd_Const_Pnd_Payment_Orign_Factor;
    private DbsField pnd_Pmt_Modes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal110 = new LdaIaal110();
        registerRecord(ldaIaal110);
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);

        // Local Variables
        localVariables = new DbsRecord();

        iaaa0510 = localVariables.newGroupInRecord("iaaa0510", "IAAA0510");
        iaaa0510_Pnd_Cm_Std_Srt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Cm_Std_Srt_Seq", "#CM-STD-SRT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            80));
        iaaa0510_Pnd_Ia_Std_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Nm_Cd", "#IA-STD-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            80));

        pnd_Input_Record_Work_2 = localVariables.newGroupInRecord("pnd_Input_Record_Work_2", "#INPUT-RECORD-WORK-2");
        pnd_Input_Record_Work_2_Pnd_Call_Type = pnd_Input_Record_Work_2.newFieldInGroup("pnd_Input_Record_Work_2_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 
            1);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 1);
        pnd_Blnk5 = localVariables.newFieldInRecord("pnd_Blnk5", "#BLNK5", FieldType.STRING, 5);
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.NUMERIC, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 2);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 2);
        pnd_Pa_Contract = localVariables.newFieldInRecord("pnd_Pa_Contract", "#PA-CONTRACT", FieldType.STRING, 1);
        pnd_Cref_Contract = localVariables.newFieldInRecord("pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 1);
        pnd_End_Of_Tiaa = localVariables.newFieldInRecord("pnd_End_Of_Tiaa", "#END-OF-TIAA", FieldType.STRING, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Table_Date = localVariables.newFieldInRecord("pnd_Table_Date", "#TABLE-DATE", FieldType.NUMERIC, 6);
        pnd_W_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_W_Save_Check_Dte_A", "#W-SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_W_Save_Check_Dte_A__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Save_Check_Dte_A__R_Field_1", "REDEFINE", pnd_W_Save_Check_Dte_A);
        pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte = pnd_W_Save_Check_Dte_A__R_Field_1.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte", 
            "#W-SAVE-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_W_Save_Check_Dte_A__R_Field_2 = pnd_W_Save_Check_Dte_A__R_Field_1.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_2", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);
        pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy = pnd_W_Save_Check_Dte_A__R_Field_2.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 
            4);

        pnd_W_Save_Check_Dte_A__R_Field_3 = pnd_W_Save_Check_Dte_A__R_Field_2.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_3", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy);
        pnd_W_Save_Check_Dte_A_Pnd_W_Cc = pnd_W_Save_Check_Dte_A__R_Field_3.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Cc", "#W-CC", FieldType.NUMERIC, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Yy = pnd_W_Save_Check_Dte_A__R_Field_3.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Yy", "#W-YY", FieldType.NUMERIC, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Mm = pnd_W_Save_Check_Dte_A__R_Field_2.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Save_Check_Dte_A__R_Field_4 = pnd_W_Save_Check_Dte_A__R_Field_2.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_4", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Mm);
        pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A = pnd_W_Save_Check_Dte_A__R_Field_4.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A", "#W-MM-A", FieldType.STRING, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Dd = pnd_W_Save_Check_Dte_A__R_Field_2.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Save_Check_Dte_A__R_Field_5 = pnd_W_Save_Check_Dte_A__R_Field_1.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_5", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);
        pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm = pnd_W_Save_Check_Dte_A__R_Field_5.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm", 
            "#W-SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_6", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_6.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_7 = pnd_Save_Check_Dte_A__R_Field_6.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_7", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Save_Check_Dte_A__R_Field_8 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_8", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_9 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_9", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_10 = pnd_Save_Check_Dte_A__R_Field_6.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_10", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);

        pnd_Payment_Due_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_11", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_12", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);

        pnd_Table = localVariables.newGroupArrayInRecord("pnd_Table", "#TABLE", new DbsArrayController(1, 240));
        pnd_Table_Pnd_Tbl_Fund_Cde = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Fund_Cde", "#TBL-FUND-CDE", FieldType.STRING, 2);
        pnd_Table_Pnd_Tbl_Unit_Value = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Unit_Value", "#TBL-UNIT-VALUE", FieldType.NUMERIC, 5, 2);
        pnd_Table_Pnd_Tbl_Issue_Date = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Issue_Date", "#TBL-ISSUE-DATE", FieldType.NUMERIC, 6);
        pnd_Cnt_Tot = localVariables.newFieldInRecord("pnd_Cnt_Tot", "#CNT-TOT", FieldType.NUMERIC, 9);
        pnd_Cnt1 = localVariables.newFieldInRecord("pnd_Cnt1", "#CNT1", FieldType.NUMERIC, 9);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.NUMERIC, 9);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.NUMERIC, 9);
        pnd_Cnt_None = localVariables.newFieldInRecord("pnd_Cnt_None", "#CNT-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_1s = localVariables.newFieldInRecord("pnd_Cnt_1s", "#CNT-1S", FieldType.NUMERIC, 9);
        pnd_Cnt_1s1 = localVariables.newFieldInRecord("pnd_Cnt_1s1", "#CNT-1S1", FieldType.NUMERIC, 9);
        pnd_Cnt_1s2 = localVariables.newFieldInRecord("pnd_Cnt_1s2", "#CNT-1S2", FieldType.NUMERIC, 9);
        pnd_Cnt_1s3 = localVariables.newFieldInRecord("pnd_Cnt_1s3", "#CNT-1S3", FieldType.NUMERIC, 9);
        pnd_Cnt_1s_None = localVariables.newFieldInRecord("pnd_Cnt_1s_None", "#CNT-1S-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_1z = localVariables.newFieldInRecord("pnd_Cnt_1z", "#CNT-1Z", FieldType.NUMERIC, 9);
        pnd_Cnt_1z1 = localVariables.newFieldInRecord("pnd_Cnt_1z1", "#CNT-1Z1", FieldType.NUMERIC, 9);
        pnd_Cnt_1z2 = localVariables.newFieldInRecord("pnd_Cnt_1z2", "#CNT-1Z2", FieldType.NUMERIC, 9);
        pnd_Cnt_1z_None = localVariables.newFieldInRecord("pnd_Cnt_1z_None", "#CNT-1Z-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref = localVariables.newFieldInRecord("pnd_Cnt_Cref", "#CNT-CREF", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref1 = localVariables.newFieldInRecord("pnd_Cnt_Cref1", "#CNT-CREF1", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref2 = localVariables.newFieldInRecord("pnd_Cnt_Cref2", "#CNT-CREF2", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref3 = localVariables.newFieldInRecord("pnd_Cnt_Cref3", "#CNT-CREF3", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref_None = localVariables.newFieldInRecord("pnd_Cnt_Cref_None", "#CNT-CREF-NONE", FieldType.NUMERIC, 9);
        pnd_Fn_Code = localVariables.newFieldInRecord("pnd_Fn_Code", "#FN-CODE", FieldType.STRING, 1);
        pnd_Ck_Date = localVariables.newFieldInRecord("pnd_Ck_Date", "#CK-DATE", FieldType.NUMERIC, 6);
        pnd_U_Val = localVariables.newFieldInRecord("pnd_U_Val", "#U-VAL", FieldType.NUMERIC, 5, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Issu_Date = localVariables.newFieldInRecord("pnd_Issu_Date", "#ISSU-DATE", FieldType.NUMERIC, 6);
        pnd_Is_Date = localVariables.newFieldInRecord("pnd_Is_Date", "#IS-DATE", FieldType.NUMERIC, 6);

        pnd_Is_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_Is_Date__R_Field_13", "REDEFINE", pnd_Is_Date);
        pnd_Is_Date_Pnd_Is_Date_Yyyy = pnd_Is_Date__R_Field_13.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Yyyy", "#IS-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Is_Date_Pnd_Is_Date_Mm = pnd_Is_Date__R_Field_13.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Mm", "#IS-DATE-MM", FieldType.NUMERIC, 2);

        pnd_Is_Date__R_Field_14 = pnd_Is_Date__R_Field_13.newGroupInGroup("pnd_Is_Date__R_Field_14", "REDEFINE", pnd_Is_Date_Pnd_Is_Date_Mm);
        pnd_Is_Date_Pnd_Is_Date_Mm_A = pnd_Is_Date__R_Field_14.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Mm_A", "#IS-DATE-MM-A", FieldType.STRING, 2);
        pnd_Fund_Code_2 = localVariables.newFieldInRecord("pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);

        pnd_Fund_Desc__R_Field_15 = localVariables.newGroupInRecord("pnd_Fund_Desc__R_Field_15", "REDEFINE", pnd_Fund_Desc);
        pnd_Fund_Desc_Pnd_Fund_Desc_10 = pnd_Fund_Desc__R_Field_15.newFieldInGroup("pnd_Fund_Desc_Pnd_Fund_Desc_10", "#FUND-DESC-10", FieldType.STRING, 
            10);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Table_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Fund_Desc", "#TABLE-FUND-DESC", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pnd_Fnd_Desc = localVariables.newFieldInRecord("pnd_Fnd_Desc", "#FND-DESC", FieldType.STRING, 10);
        pnd_W_Variable = localVariables.newFieldInRecord("pnd_W_Variable", "#W-VARIABLE", FieldType.STRING, 2);
        pnd_Heading1 = localVariables.newFieldInRecord("pnd_Heading1", "#HEADING1", FieldType.STRING, 70);
        pnd_Old_Tiaa_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Old_Tiaa_Cmpny_Cde", "#OLD-TIAA-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Debug_1 = localVariables.newFieldInRecord("pnd_Debug_1", "#DEBUG-1", FieldType.BOOLEAN, 1);
        pnd_Debug_2 = localVariables.newFieldInRecord("pnd_Debug_2", "#DEBUG-2", FieldType.BOOLEAN, 1);

        pnd_Iaan0501_Pda = localVariables.newGroupInRecord("pnd_Iaan0501_Pda", "#IAAN0501-PDA");
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_1 = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_2 = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde = pnd_Iaan0501_Pda.newFieldInGroup("pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde", "#PARM-RTN-CDE", FieldType.NUMERIC, 
            2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Latest_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Latest_Factor", "#LATEST-FACTOR", FieldType.STRING, 1);
        pnd_Const_Pnd_Payment_Orign_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Payment_Orign_Factor", "#PAYMENT-ORIGN-FACTOR", FieldType.STRING, 
            1);
        pnd_Pmt_Modes = localVariables.newFieldArrayInRecord("pnd_Pmt_Modes", "#PMT-MODES", FieldType.NUMERIC, 3, new DbsArrayController(1, 22));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal110.initializeValues();
        ldaIaal050.initializeValues();

        localVariables.reset();
        pnd_Len.setInitialValue(6);
        pnd_Heading1.setInitialValue("IA ADMINISTRATION - MONTHLY REVALUATION MODE CONTROL FOR PAYMENTS DUE ");
        pnd_Const_Pnd_Latest_Factor.setInitialValue("L");
        pnd_Const_Pnd_Payment_Orign_Factor.setInitialValue("P");
        pnd_Pmt_Modes.getValue(1).setInitialValue(100);
        pnd_Pmt_Modes.getValue(2).setInitialValue(601);
        pnd_Pmt_Modes.getValue(3).setInitialValue(602);
        pnd_Pmt_Modes.getValue(4).setInitialValue(603);
        pnd_Pmt_Modes.getValue(5).setInitialValue(701);
        pnd_Pmt_Modes.getValue(6).setInitialValue(702);
        pnd_Pmt_Modes.getValue(7).setInitialValue(703);
        pnd_Pmt_Modes.getValue(8).setInitialValue(704);
        pnd_Pmt_Modes.getValue(9).setInitialValue(705);
        pnd_Pmt_Modes.getValue(10).setInitialValue(706);
        pnd_Pmt_Modes.getValue(11).setInitialValue(801);
        pnd_Pmt_Modes.getValue(12).setInitialValue(802);
        pnd_Pmt_Modes.getValue(13).setInitialValue(803);
        pnd_Pmt_Modes.getValue(14).setInitialValue(804);
        pnd_Pmt_Modes.getValue(15).setInitialValue(805);
        pnd_Pmt_Modes.getValue(16).setInitialValue(806);
        pnd_Pmt_Modes.getValue(17).setInitialValue(807);
        pnd_Pmt_Modes.getValue(18).setInitialValue(808);
        pnd_Pmt_Modes.getValue(19).setInitialValue(809);
        pnd_Pmt_Modes.getValue(20).setInitialValue(810);
        pnd_Pmt_Modes.getValue(21).setInitialValue(811);
        pnd_Pmt_Modes.getValue(22).setInitialValue(812);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap110m() throws Exception
    {
        super("Iaap110m");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        getReports().atTopOfPage(atTopEventRpt8, 8);
        setupReports();
        //* *=====================================================================
        //*                     SET AT TOP OF PAGE
        //* *=====================================================================
        //*  PAGE 2                                                                                                                                                       //Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: FORMAT ( 3 ) LS = 133 PS = 56;//Natural: FORMAT ( 4 ) LS = 133 PS = 56;//Natural: FORMAT ( 5 ) LS = 133 PS = 56;//Natural: FORMAT ( 6 ) LS = 133 PS = 56;//Natural: FORMAT ( 7 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 8 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 7 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 8 )
        //* *======================================================================
        //*                    START OF MAIN PROGRAM LOGIC
        //* *======================================================================
        getReports().write(0, "       *********************************");                                                                                                //Natural: WRITE '       *********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "           START OF IAAP110M PROGRAM");                                                                                                    //Natural: WRITE '           START OF IAAP110M PROGRAM'
        if (Global.isEscape()) return;
        getReports().write(0, "       *********************************");                                                                                                //Natural: WRITE '       *********************************'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #INITIALIZATION
        sub_Pnd_Initialization();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #SETUP-MODE-TABLE2
        sub_Pnd_Setup_Mode_Table2();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #SETUP-OTHER-TABLES
        sub_Pnd_Setup_Other_Tables();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug_2.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-2
        {
            getReports().write(0, "=",pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);                                                                                       //Natural: WRITE '=' #W-SAVE-CHECK-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ WORK FILE 1
        boolean endOfDataR1 = true;                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstR1 = true;
        R1:
        while (condition(getWorkFiles().read(1, ldaIaal110.getPnd_Input_Record())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventR1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataR1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #I-TIAA-FUND-CDE-TOT
            if (condition(pnd_Debug_2.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-2
            {
                getReports().write(0, "=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot(),"=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde(),             //Natural: WRITE '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-CMPNY-CDE '=' #I-CNT-NBR
                    "=",ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal110.getPnd_Fund_Cde_Hold_Tot().setValue(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot());                                                     //Natural: MOVE #I-TIAA-FUND-CDE-TOT TO #FUND-CDE-HOLD-TOT
                                                                                                                                                                          //Natural: PERFORM #FIND-MODE-SUB
            sub_Pnd_Find_Mode_Sub();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND
            sub_Pnd_Process_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #CHECK-PAYMENT-MODE
            sub_Pnd_Check_Payment_Mode();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TOTALS-REPORTS
                sub_Pnd_Process_Totals_Reports();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  #INPUT-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventR1(endOfDataR1);
        }
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(20),"TOTAL",new ColumnSpacing(9),ldaIaal110.getPnd_Fmt4_Cnt_Amt_Tot(),               //Natural: WRITE ( 4 ) // 20X 'TOTAL' 09X #FMT4-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT4-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt4_Per_Div_Tot(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS
        sub_Pnd_Write_Totals_Reports();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS-5
        sub_Pnd_Write_Totals_Reports_5();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS-6
        sub_Pnd_Write_Totals_Reports_6();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS-7
        sub_Pnd_Write_Totals_Reports_7();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug_2.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-2
        {
            getReports().write(0, "NUM OF FUNDS",pnd_U);                                                                                                                  //Natural: WRITE 'NUM OF FUNDS' #U
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *====================================================================
        //*                     START OF SUBROUTINES
        //* *====================================================================
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CNT-TYPE-SUB-PARA
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FUND-INFORMATION
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TOTALS-REPORTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CREF-CONTRACTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-CREF
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-PA-CONTRACTS
        //* *****************************************************************
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-PA-SELECT-FUND
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-09-11
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FIND-MODE-SUB
        //* *****************************************************************
        //*  9/08
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-MODE-TABLE2
        //* *****************************************************************
        //*  9/08
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-OTHER-TABLES
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS-5
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS-6
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS-7
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CREF-FORMAT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #INITIALIZATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DOLLAR-AMOUNT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
        //* **********************************************************************
        //*  RETRIEVE LATEST AVAILABLE FACTOR
        //*  CONVERT THE NUMERIC FUND CODE INTO A ALPHA FUND CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PAYMENT-MODE
        //* ***********************************************************************
    }
    private void sub_Pnd_Process_Fund() throws Exception                                                                                                                  //Natural: #PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Cref_Contract.reset();                                                                                                                                        //Natural: RESET #CREF-CONTRACT #PA-CONTRACT
        pnd_Pa_Contract.reset();
        //*  PA SELECT FUND CHANGES
        if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("41") || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("42")              //Natural: IF #I-TIAA-FUND-CDE = '41' OR = '42' OR = '43' OR = '44' OR = '45' OR = '46' OR = '47' OR = '48' OR = '49'
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("43") || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("44") || 
            ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("45") || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("46") || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("47") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("48") || ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde().equals("49")))
        {
            pnd_Pa_Contract.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #PA-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
            sub_Pnd_Cnt_Type_Sub_Para();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-PA-SELECT-FUND
            sub_Pnd_Process_Pa_Select_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet692 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #I-TIAA-CMPNY-CDE;//Natural: VALUE '4'
            if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde().equals("4"))))
            {
                decideConditionsMet692++;
                pnd_Cref_Contract.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #CREF-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
                sub_Pnd_Cnt_Type_Sub_Para();
                if (condition(Global.isEscape())) {return;}
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-CREF
                sub_Pnd_Process_Fund_Cref();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'W'
            else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde().equals("W"))))
            {
                decideConditionsMet692++;
                pnd_Cref_Contract.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #CREF-CONTRACT
                //*  9/08 NEW
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
                sub_Pnd_Cnt_Type_Sub_Para();
                if (condition(Global.isEscape())) {return;}
                //*    MOVE 1 TO #CNT-TYPE-SUB       /* 9/08
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                //*  9/08
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-09-11
                sub_Pnd_Process_Fund_09_11();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-FUND
    }
    private void sub_Pnd_Cnt_Type_Sub_Para() throws Exception                                                                                                             //Natural: #CNT-TYPE-SUB-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_W_Variable.setValue(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde());                                                                                    //Natural: MOVE #I-TIAA-FUND-CDE TO #W-VARIABLE
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #U
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(pnd_U)); ldaIaal110.getPnd_I().nadd(1))
        {
            if (condition(pnd_W_Variable.equals(pnd_T_Fund_Code.getValue(ldaIaal110.getPnd_I()))))                                                                        //Natural: IF #W-VARIABLE = #T-FUND-CODE ( #I )
            {
                ldaIaal110.getPnd_Cnt_Type_Sub().setValue(ldaIaal110.getPnd_I());                                                                                         //Natural: ASSIGN #CNT-TYPE-SUB := #I
                //*  9/08
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #CNT-TYPE-SUB-PARA
    }
    private void sub_Pnd_Fund_Information() throws Exception                                                                                                              //Natural: #FUND-INFORMATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Fund_Desc.reset();                                                                                                                                            //Natural: RESET #FUND-DESC #COMP-DESC #FUND-TYPES
        pnd_Comp_Desc.reset();
        ldaIaal110.getPnd_Fund_Types().reset();
        ldaIaal110.getPnd_Fund_Types().setValue(3);                                                                                                                       //Natural: MOVE 3 TO #FUND-TYPES
        short decideConditionsMet733 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #CMPNY-CDE-HOLD;//Natural: VALUE '4'
        if (condition((ldaIaal110.getPnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold().equals("4"))))
        {
            decideConditionsMet733++;
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), ldaIaal110.getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold(), pnd_Fund_Desc, pnd_Comp_Desc,             //Natural: CALLNAT 'IAAN051A' #FUND-CDE-HOLD #FUND-DESC #COMP-DESC #LEN
                pnd_Len);
            if (condition(Global.isEscape())) return;
            ldaIaal110.getPnd_Cref_Title_Tab().getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0L 6L"));                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0L 6L' TO #CREF-TITLE-TAB ( 1 )
            ldaIaal110.getPnd_Cref_Title_Tab().getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0M 0T 0U 6M Z"));                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0M 0T 0U 6M Z' TO #CREF-TITLE-TAB ( 2 )
            ldaIaal110.getPnd_Cref_Title_Tab().getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0N 6N"));                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0N 6N' TO #CREF-TITLE-TAB ( 3 )
            ldaIaal110.getPnd_Cref_Title_Tab().getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaIaal110.getPnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold().equals("W"))))
        {
            decideConditionsMet733++;
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), ldaIaal110.getPnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold(), pnd_Fund_Desc, pnd_Comp_Desc,             //Natural: CALLNAT 'IAAN051A' #FUND-CDE-HOLD #FUND-DESC #COMP-DESC #LEN
                pnd_Len);
            if (condition(Global.isEscape())) return;
            //*  PA SELECT FUND CHANGES
            if (condition(pnd_Comp_Desc.equals("T/L ")))                                                                                                                  //Natural: IF #COMP-DESC = 'T/L '
            {
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PSI GW WO"));                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PSI GW WO' TO #CREF-TITLE-TAB ( 1 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PSI IA-IF"));                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PSI IA-IF' TO #CREF-TITLE-TAB ( 2 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PS SO"));                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PS SO' TO #CREF-TITLE-TAB ( 3 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6L GW W0"));                     //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6L GW W0' TO #CREF-TITLE-TAB ( 1 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6M GA IA-IF Y"));                //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6M GA IA-IF Y' TO #CREF-TITLE-TAB ( 2 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6N"));                           //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6N' TO #CREF-TITLE-TAB ( 3 )
                ldaIaal110.getPnd_Cref_Title_Tab().getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #FUND-INFORMATION
    }
    private void sub_Pnd_Process_Totals_Reports() throws Exception                                                                                                        //Natural: #PROCESS-TOTALS-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        FP:                                                                                                                                                               //Natural: FOR #Z = 1 TO 4
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(4)); pnd_Z.nadd(1))
        {
            if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(ldaIaal110.getPnd_Pay_Mode_Table().getValue(pnd_Z))))                                //Natural: IF #I-CNT-MODE-IND = #PAY-MODE-TABLE ( #Z )
            {
                ldaIaal110.getPnd_Fmt3_Mode_Sub().setValue(pnd_Z);                                                                                                        //Natural: MOVE #Z TO #FMT3-MODE-SUB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Cref_Contract.equals("Y")))                                                                                                                     //Natural: IF #CREF-CONTRACT = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CREF-CONTRACTS
            sub_Pnd_Process_Cref_Contracts();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Pa_Contract.equals("Y")))                                                                                                                   //Natural: IF #PA-CONTRACT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-PA-CONTRACTS
                sub_Pnd_Process_Pa_Contracts();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-TOTALS-REPORTS
    }
    private void sub_Pnd_Process_Cref_Contracts() throws Exception                                                                                                        //Natural: #PROCESS-CREF-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        ldaIaal110.getPnd_Fmt3_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT3-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        ldaIaal110.getPnd_Fmt5_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT5-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
        sub_Pnd_Get_Dollar_Amount();
        if (condition(Global.isEscape())) {return;}
        ldaIaal110.getPnd_Fmt3_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT3-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        ldaIaal110.getPnd_Fmt5_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT5-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        //*  ADDED 6/03
        if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("IRAT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("IRAC")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("03BT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("03BC") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("QPLT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("QPLC") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("57BC") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("57BT")))
        {
            ldaIaal110.getPnd_Fmt7_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT7-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            ldaIaal110.getPnd_Fmt7_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount()); //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT7-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal110.getPnd_Fmt6_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT6-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            ldaIaal110.getPnd_Fmt6_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount()); //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT6-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-CREF-CONTRACTS
    }
    private void sub_Pnd_Process_Fund_Cref() throws Exception                                                                                                             //Natural: #PROCESS-FUND-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet805 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = '0L' OR = '6L'
        if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("0L") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6L")))
        {
            decideConditionsMet805++;
            ldaIaal110.getPnd_Cref_Units_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '0M' OR = '0T' OR = '0U' OR = '6M' OR #I-CNT-NBR-2 = 'Z0' THRU 'Z9'
        else if (condition(((((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("0M") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("0T")) 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("0U")) || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6M")) || (ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().greaterOrEqual("Z0") 
            && ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().lessOrEqual("Z9")))))
        {
            decideConditionsMet805++;
            ldaIaal110.getPnd_Cref_Units_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '0N' OR = '6N'
        else if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("0N") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6N")))
        {
            decideConditionsMet805++;
            ldaIaal110.getPnd_Cref_Units_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "CREF FUND ERROR  ","=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde(),"=",ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr());       //Natural: WRITE 'CREF FUND ERROR  ' '=' #I-TIAA-FUND-CDE '=' #I-CNT-NBR
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-CREF
    }
    private void sub_Pnd_Process_Pa_Contracts() throws Exception                                                                                                          //Natural: #PROCESS-PA-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        ldaIaal110.getPnd_Fmt3_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT3-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        ldaIaal110.getPnd_Fmt5_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT5-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
        sub_Pnd_Get_Dollar_Amount();
        if (condition(Global.isEscape())) {return;}
        ldaIaal110.getPnd_Fmt3_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT3-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        ldaIaal110.getPnd_Fmt5_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT5-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        //*  ADDED 6/03
        if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("IRAT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("IRAC")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("03BT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("03BC") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("QPLT") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("QPLC") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("57BC") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde().equals("57BT")))
        {
            ldaIaal110.getPnd_Fmt7_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT7-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            ldaIaal110.getPnd_Fmt7_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount()); //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT7-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal110.getPnd_Fmt6_Units().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt()); //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT6-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            ldaIaal110.getPnd_Fmt6_Dollars().getValue(ldaIaal110.getPnd_Cnt_Type_Sub(),ldaIaal110.getPnd_Fmt3_Mode_Sub()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount()); //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT6-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-PA-CONTRACTS
    }
    private void sub_Pnd_Process_Pa_Select_Fund() throws Exception                                                                                                        //Natural: #PROCESS-PA-SELECT-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet860 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = 'GW' OR = 'W0'
        if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("GW") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("W0")))
        {
            decideConditionsMet860++;
            ldaIaal110.getPnd_Cref_Units_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'IA' OR = 'IB' OR = 'IC' OR = 'ID' OR = 'IE' OR = 'IF' OR = 'IG'
        else if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IA") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IB") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IC") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("ID") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IE") 
            || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IF") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("IG")))
        {
            decideConditionsMet860++;
            ldaIaal110.getPnd_Cref_Units_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'S0'
        else if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("S0")))
        {
            decideConditionsMet860++;
            ldaIaal110.getPnd_Cref_Units_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "PA SELECT FUND ERROR  ","=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde(),"=",ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr());  //Natural: WRITE 'PA SELECT FUND ERROR  ' '=' #I-TIAA-FUND-CDE '=' #I-CNT-NBR
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-PA-SELECT-FUND
    }
    //*  9/08
    private void sub_Pnd_Process_Fund_09_11() throws Exception                                                                                                            //Natural: #PROCESS-FUND-09-11
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet893 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = '6L' OR = 'GW' OR ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 > 0 AND #I-CNT-NBR-3-7 < 25000 ) )
        if (condition(((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6L") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("GW")) 
            || (ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("W0") && (ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_3_7().greater(getZero()) 
            && ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_3_7().less(25000))))))
        {
            decideConditionsMet893++;
            ldaIaal110.getPnd_Cref_Units_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_1().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '6M' OR = 'GA' OR #I-CNT-NBR-2 = 'IA' THRU 'IG' OR #I-CNT-NBR-2 = 'Y0' THRU 'Y9'
        else if (condition((((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6M") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("GA")) 
            || (ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().greaterOrEqual("IA") && ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().lessOrEqual("IG"))) 
            || (ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().greaterOrEqual("Y0") && ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().lessOrEqual("Y9")))))
        {
            decideConditionsMet893++;
            ldaIaal110.getPnd_Cref_Units_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_2().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            //*  OPTION 21 REA 8/06
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '6N' OR = 'S0'
        else if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("6N") || ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr_2().equals("S0")))
        {
            decideConditionsMet893++;
            ldaIaal110.getPnd_Cref_Units_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            ldaIaal110.getPnd_Cref_Dollars_3().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt());                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_Mode_1()).nadd(ldaIaal110.getPnd_Generic_Dollar_Amount());                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "FUND 09-11 ERROR  ","=",ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr(),"=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde());      //Natural: WRITE 'FUND 09-11 ERROR  ' '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-09
    }
    private void sub_Pnd_Find_Mode_Sub() throws Exception                                                                                                                 //Natural: #FIND-MODE-SUB
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 22
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
        {
            if (condition(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(pnd_Pmt_Modes.getValue(ldaIaal110.getPnd_I()))))                                     //Natural: IF #I-CNT-MODE-IND = #PMT-MODES ( #I )
            {
                ldaIaal110.getPnd_Mode_1().setValue(ldaIaal110.getPnd_I());                                                                                               //Natural: ASSIGN #MODE-1 := #I
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #FIND-MODE-SUB
    }
    private void sub_Pnd_Setup_Mode_Table2() throws Exception                                                                                                             //Natural: #SETUP-MODE-TABLE2
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 22
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
        {
            ldaIaal110.getPnd_Mode_Table_2().getValue(ldaIaal110.getPnd_I()).setValueEdited(pnd_Pmt_Modes.getValue(ldaIaal110.getPnd_I()),new ReportEditMask("999"));     //Natural: MOVE EDITED #PMT-MODES ( #I ) ( EM = 999 ) TO #MODE-TABLE-2 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #SETUP-MODE-TABLE2
    }
    private void sub_Pnd_Setup_Other_Tables() throws Exception                                                                                                            //Natural: #SETUP-OTHER-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        ldaIaal110.getPnd_Pay_Mode_Table().getValue(1).setValue(100);                                                                                                     //Natural: MOVE 100 TO #PAY-MODE-TABLE ( 1 )
        short decideConditionsMet946 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MM;//Natural: VALUE 01
        if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(1))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(601);                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(701);                                                                                                 //Natural: MOVE 701 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(801);                                                                                                 //Natural: MOVE 801 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(2))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(602);                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(702);                                                                                                 //Natural: MOVE 702 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(802);                                                                                                 //Natural: MOVE 802 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 03
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(3))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(603);                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(703);                                                                                                 //Natural: MOVE 703 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(803);                                                                                                 //Natural: MOVE 803 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 04
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(4))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(601);                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(704);                                                                                                 //Natural: MOVE 704 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(804);                                                                                                 //Natural: MOVE 804 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 05
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(5))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(602);                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(705);                                                                                                 //Natural: MOVE 705 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(805);                                                                                                 //Natural: MOVE 805 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 06
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(6))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(603);                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(706);                                                                                                 //Natural: MOVE 706 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(806);                                                                                                 //Natural: MOVE 806 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 07
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(7))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(601);                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(701);                                                                                                 //Natural: MOVE 701 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(807);                                                                                                 //Natural: MOVE 807 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 08
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(8))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(602);                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(702);                                                                                                 //Natural: MOVE 702 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(808);                                                                                                 //Natural: MOVE 808 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 09
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(9))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(603);                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(703);                                                                                                 //Natural: MOVE 703 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(809);                                                                                                 //Natural: MOVE 809 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(601);                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(704);                                                                                                 //Natural: MOVE 704 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(810);                                                                                                 //Natural: MOVE 810 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 11
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(602);                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(705);                                                                                                 //Natural: MOVE 705 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(811);                                                                                                 //Natural: MOVE 811 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 12
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))))
        {
            decideConditionsMet946++;
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(2).setValue(603);                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(3).setValue(706);                                                                                                 //Natural: MOVE 706 TO #PAY-MODE-TABLE ( 3 )
            ldaIaal110.getPnd_Pay_Mode_Table().getValue(4).setValue(812);                                                                                                 //Natural: MOVE 812 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  *********************************************************************
        DbsUtil.callnat(Iaan051e.class , getCurrentProcessState(), iaaa0510);                                                                                             //Natural: CALLNAT 'IAAN051E' IAAA0510
        if (condition(Global.isEscape())) return;
        FE:                                                                                                                                                               //Natural: FOR #G = 4 TO 80
        for (pnd_G.setValue(4); condition(pnd_G.lessOrEqual(80)); pnd_G.nadd(1))
        {
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_G,"=",pnd_Sub,"=",iaaa0510_Pnd_Cm_Std_Srt_Seq.getValue(pnd_G),"=",iaaa0510_Pnd_Ia_Std_Nm_Cd.getValue(pnd_G));               //Natural: WRITE '=' #G '=' #SUB '=' #CM-STD-SRT-SEQ ( #G ) '=' #IA-STD-NM-CD ( #G )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sub.setValue(iaaa0510_Pnd_Cm_Std_Srt_Seq.getValue(pnd_G));                                                                                                //Natural: MOVE #CM-STD-SRT-SEQ ( #G ) TO #SUB
            if (condition(pnd_Sub.equals(getZero())))                                                                                                                     //Natural: IF #SUB = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_U.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #U
            pnd_T_Fund_Code.getValue(pnd_Sub).setValue(iaaa0510_Pnd_Ia_Std_Nm_Cd.getValue(pnd_G));                                                                        //Natural: MOVE #IA-STD-NM-CD ( #G ) TO #T-FUND-CODE ( #SUB )
            //*  FE.
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, "=",pnd_T_Fund_Code.getValue(1,":",20),"=",pnd_U);                                                                                      //Natural: WRITE '=' #T-FUND-CODE ( 1:20 ) '=' #U
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        F9:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (ldaIaal110.getPnd_J().setValue(1); condition(ldaIaal110.getPnd_J().lessOrEqual(20)); ldaIaal110.getPnd_J().nadd(1))
        {
            pnd_Fund_Desc.reset();                                                                                                                                        //Natural: RESET #FUND-DESC #COMP-DESC
            pnd_Comp_Desc.reset();
            pnd_Fund_Code_2.setValue(pnd_T_Fund_Code.getValue(ldaIaal110.getPnd_J()));                                                                                    //Natural: MOVE #T-FUND-CODE ( #J ) TO #FUND-CODE-2
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Code_2, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2 #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            pnd_Table_Fund_Desc.getValue(ldaIaal110.getPnd_J()).setValue(pnd_Fund_Desc);                                                                                  //Natural: MOVE #FUND-DESC TO #TABLE-FUND-DESC ( #J )
            ldaIaal110.getPnd_Unit_Text_Table().getValue(ldaIaal110.getPnd_J()).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10,                  //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'UNITS' TO #UNIT-TEXT-TABLE ( #J )
                "UNITS"));
            ldaIaal110.getPnd_Dollars_Text_Table().getValue(ldaIaal110.getPnd_J()).setValue(DbsUtil.compress(pnd_Blnk5, pnd_Fund_Desc_Pnd_Fund_Desc_10,                   //Natural: COMPRESS #BLNK5 #FUND-DESC-10 'DOLLARS' TO #DOLLARS-TEXT-TABLE ( #J )
                "DOLLARS"));
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Table_Fund_Desc.getValue(ldaIaal110.getPnd_J()));                                                                           //Natural: WRITE '=' #TABLE-FUND-DESC ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",ldaIaal110.getPnd_Unit_Text_Table().getValue(ldaIaal110.getPnd_J()));                                                           //Natural: WRITE '=' #UNIT-TEXT-TABLE ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",ldaIaal110.getPnd_Dollars_Text_Table().getValue(ldaIaal110.getPnd_J()));                                                        //Natural: WRITE '=' #DOLLARS-TEXT-TABLE ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #SETUP-OTHER-TABLES
    }
    private void sub_Pnd_Write_Totals_Reports() throws Exception                                                                                                          //Natural: #WRITE-TOTALS-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(3, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),ldaIaal110.getPnd_Pay_Mode_Table().getValue(1),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(2),new  //Natural: WRITE ( 3 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(3),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(4),new ColumnSpacing(16),
            "TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(3, 2);                                                                                                                                          //Natural: SKIP ( 3 ) 2
        ldaIaal110.getPnd_Fmt3_Tiaa_Table_Tot().reset();                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        FY:                                                                                                                                                               //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(3, 1);                                                                                                                                      //Natural: SKIP ( 3 ) 1
            ldaIaal110.getPnd_Fmt3_Units_Tot().nadd(ldaIaal110.getPnd_Fmt3_Units().getValue(pnd_Y,"*"));                                                                  //Natural: ADD #FMT3-UNITS ( #Y,* ) TO #FMT3-UNITS-TOT
            getReports().write(3, ReportOption.NOTITLE,ldaIaal110.getPnd_Unit_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Units().getValue(pnd_Y,1),  //Natural: WRITE ( 3 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT3-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT3-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT3-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT3-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT3-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Units().getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Units().getValue(pnd_Y,3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Units().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Units_Tot(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt3_Units_Tot().reset();                                                                                                                   //Natural: RESET #FMT3-UNITS-TOT
            ldaIaal110.getPnd_Fmt3_Dollars_Tot().nadd(ldaIaal110.getPnd_Fmt3_Dollars().getValue(pnd_Y,"*"));                                                              //Natural: ADD #FMT3-DOLLARS ( #Y,* ) TO #FMT3-DOLLARS-TOT
            getReports().write(3, ReportOption.NOTITLE,ldaIaal110.getPnd_Dollars_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Dollars().getValue(pnd_Y,1),  //Natural: WRITE ( 3 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT3-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT3-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT3-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT3-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT3-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Dollars().getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Dollars().getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Dollars().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt3_Dollars_Tot(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt3_Dollars_Tot().reset();                                                                                                                 //Natural: RESET #FMT3-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS
    }
    private void sub_Pnd_Write_Totals_Reports_5() throws Exception                                                                                                        //Natural: #WRITE-TOTALS-REPORTS-5
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(5, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),ldaIaal110.getPnd_Pay_Mode_Table().getValue(1),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(2),new  //Natural: WRITE ( 5 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(3),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(4),new ColumnSpacing(16),
            "TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(5, 2);                                                                                                                                          //Natural: SKIP ( 5 ) 2
        ldaIaal110.getPnd_Fmt5_Tiaa_Table_Tot().reset();                                                                                                                  //Natural: RESET #FMT5-TIAA-TABLE-TOT
        FY5:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1
            ldaIaal110.getPnd_Fmt5_Units_Tot().nadd(ldaIaal110.getPnd_Fmt5_Units().getValue(pnd_Y,"*"));                                                                  //Natural: ADD #FMT5-UNITS ( #Y,* ) TO #FMT5-UNITS-TOT
            getReports().write(5, ReportOption.NOTITLE,ldaIaal110.getPnd_Unit_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Units().getValue(pnd_Y,1),  //Natural: WRITE ( 5 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT5-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Units().getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Units().getValue(pnd_Y,3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Units().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Units_Tot(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY5"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY5"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt5_Units_Tot().reset();                                                                                                                   //Natural: RESET #FMT5-UNITS-TOT
            ldaIaal110.getPnd_Fmt5_Dollars_Tot().nadd(ldaIaal110.getPnd_Fmt5_Dollars().getValue(pnd_Y,"*"));                                                              //Natural: ADD #FMT5-DOLLARS ( #Y,* ) TO #FMT5-DOLLARS-TOT
            getReports().write(5, ReportOption.NOTITLE,ldaIaal110.getPnd_Dollars_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Dollars().getValue(pnd_Y,1),  //Natural: WRITE ( 5 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT5-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Dollars().getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Dollars().getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Dollars().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt5_Dollars_Tot(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY5"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY5"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt5_Dollars_Tot().reset();                                                                                                                 //Natural: RESET #FMT5-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS-5
    }
    private void sub_Pnd_Write_Totals_Reports_6() throws Exception                                                                                                        //Natural: #WRITE-TOTALS-REPORTS-6
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(6, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),ldaIaal110.getPnd_Pay_Mode_Table().getValue(1),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(2),new  //Natural: WRITE ( 6 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(3),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(4),new ColumnSpacing(16),
            "TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(6, 2);                                                                                                                                          //Natural: SKIP ( 6 ) 2
        ldaIaal110.getPnd_Fmt6_Tiaa_Table_Tot().reset();                                                                                                                  //Natural: RESET #FMT6-TIAA-TABLE-TOT
        FY6:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1
            ldaIaal110.getPnd_Fmt6_Units_Tot().nadd(ldaIaal110.getPnd_Fmt6_Units().getValue(pnd_Y,"*"));                                                                  //Natural: ADD #FMT6-UNITS ( #Y,* ) TO #FMT6-UNITS-TOT
            getReports().write(6, ReportOption.NOTITLE,ldaIaal110.getPnd_Unit_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Units().getValue(pnd_Y,1),  //Natural: WRITE ( 6 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT6-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Units().getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Units().getValue(pnd_Y,3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Units().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Units_Tot(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY6"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY6"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt6_Units_Tot().reset();                                                                                                                   //Natural: RESET #FMT6-UNITS-TOT
            ldaIaal110.getPnd_Fmt6_Dollars_Tot().nadd(ldaIaal110.getPnd_Fmt6_Dollars().getValue(pnd_Y,"*"));                                                              //Natural: ADD #FMT6-DOLLARS ( #Y,* ) TO #FMT6-DOLLARS-TOT
            getReports().write(6, ReportOption.NOTITLE,ldaIaal110.getPnd_Dollars_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Dollars().getValue(pnd_Y,1),  //Natural: WRITE ( 6 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT6-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Dollars().getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Dollars().getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Dollars().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt6_Dollars_Tot(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY6"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY6"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt6_Dollars_Tot().reset();                                                                                                                 //Natural: RESET #FMT6-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS-6
    }
    private void sub_Pnd_Write_Totals_Reports_7() throws Exception                                                                                                        //Natural: #WRITE-TOTALS-REPORTS-7
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(7, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),ldaIaal110.getPnd_Pay_Mode_Table().getValue(1),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(2),new  //Natural: WRITE ( 7 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(3),new ColumnSpacing(17),ldaIaal110.getPnd_Pay_Mode_Table().getValue(4),new ColumnSpacing(16),
            "TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(7, 2);                                                                                                                                          //Natural: SKIP ( 7 ) 2
        ldaIaal110.getPnd_Fmt7_Tiaa_Table_Tot().reset();                                                                                                                  //Natural: RESET #FMT7-TIAA-TABLE-TOT
        FY7:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1
            ldaIaal110.getPnd_Fmt7_Units_Tot().nadd(ldaIaal110.getPnd_Fmt7_Units().getValue(pnd_Y,"*"));                                                                  //Natural: ADD #FMT7-UNITS ( #Y,* ) TO #FMT7-UNITS-TOT
            getReports().write(7, ReportOption.NOTITLE,ldaIaal110.getPnd_Unit_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Units().getValue(pnd_Y,1),  //Natural: WRITE ( 7 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT7-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT7-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Units().getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Units().getValue(pnd_Y,3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Units().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Units_Tot(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY7"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY7"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt7_Units_Tot().reset();                                                                                                                   //Natural: RESET #FMT7-UNITS-TOT
            ldaIaal110.getPnd_Fmt7_Dollars_Tot().nadd(ldaIaal110.getPnd_Fmt7_Dollars().getValue(pnd_Y,"*"));                                                              //Natural: ADD #FMT7-DOLLARS ( #Y,* ) TO #FMT7-DOLLARS-TOT
            getReports().write(7, ReportOption.NOTITLE,ldaIaal110.getPnd_Dollars_Text_Table().getValue(pnd_Y),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Dollars().getValue(pnd_Y,1),  //Natural: WRITE ( 7 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT7-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT7-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Dollars().getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Dollars().getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Dollars().getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaIaal110.getPnd_Fmt7_Dollars_Tot(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY7"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY7"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal110.getPnd_Fmt7_Dollars_Tot().reset();                                                                                                                 //Natural: RESET #FMT7-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS-7
    }
    private void sub_Pnd_Write_Cref_Format() throws Exception                                                                                                             //Natural: #WRITE-CREF-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        ldaIaal110.getPnd_Cref_Title().setValue(ldaIaal110.getPnd_Cref_Title_Tab().getValue(1));                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 1 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        F1B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),ldaIaal110.getPnd_Mode_Table_2().getValue(ldaIaal110.getPnd_I()),new ColumnSpacing(16),ldaIaal110.getPnd_Cref_Units_1().getValue(ldaIaal110.getPnd_I()),  //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-1 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-1 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_1().getValue(ldaIaal110.getPnd_I()), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*               22X #CREF-DEDUCT-1(#I) (EM=ZZZ,ZZZ,ZZ9.99)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal110.getPnd_Cref_Units_Total().nadd(ldaIaal110.getPnd_Cref_Units_1().getValue("*"));                                                                        //Natural: ADD #CREF-UNITS-1 ( * ) TO #CREF-UNITS-TOTAL
        ldaIaal110.getPnd_Cref_Dollars_Total().nadd(ldaIaal110.getPnd_Cref_Dollars_1().getValue("*"));                                                                    //Natural: ADD #CREF-DOLLARS-1 ( * ) TO #CREF-DOLLARS-TOTAL
        //*     ADD #CREF-DEDUCTION-1(*) TO #CREF-DEDUCTION-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),ldaIaal110.getPnd_Cref_Units_Total(), new ReportEditMask       //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_Total(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*               22X #CREF-DEDUCT-TOTAL (EM=ZZZ,ZZZ,ZZ9.99
        ldaIaal110.getPnd_Cref_Units_Total().reset();                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
        ldaIaal110.getPnd_Cref_Dollars_Total().reset();
        ldaIaal110.getPnd_Cref_Title().setValue(ldaIaal110.getPnd_Cref_Title_Tab().getValue(2));                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 2 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        F2B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),ldaIaal110.getPnd_Mode_Table_2().getValue(ldaIaal110.getPnd_I()),new ColumnSpacing(16),ldaIaal110.getPnd_Cref_Units_2().getValue(ldaIaal110.getPnd_I()),  //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-2 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-2 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_2().getValue(ldaIaal110.getPnd_I()), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F2B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F2B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*               22X #CREF-DEDUCT-2(#I) (EM=ZZZ,ZZZ,ZZ9.99)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal110.getPnd_Cref_Units_Total().nadd(ldaIaal110.getPnd_Cref_Units_2().getValue("*"));                                                                        //Natural: ADD #CREF-UNITS-2 ( * ) TO #CREF-UNITS-TOTAL
        ldaIaal110.getPnd_Cref_Dollars_Total().nadd(ldaIaal110.getPnd_Cref_Dollars_2().getValue("*"));                                                                    //Natural: ADD #CREF-DOLLARS-2 ( * ) TO #CREF-DOLLARS-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),ldaIaal110.getPnd_Cref_Units_Total(), new ReportEditMask       //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_Total(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        ldaIaal110.getPnd_Cref_Units_Total().reset();                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
        ldaIaal110.getPnd_Cref_Dollars_Total().reset();
        //*           #CREF-DEDUCTION-TOTAL
        if (condition(ldaIaal110.getPnd_Fund_Types().equals(3)))                                                                                                          //Natural: IF #FUND-TYPES = 3
        {
            ldaIaal110.getPnd_Cref_Title().setValue(ldaIaal110.getPnd_Cref_Title_Tab().getValue(3));                                                                      //Natural: MOVE #CREF-TITLE-TAB ( 3 ) TO #CREF-TITLE
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            F3B:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
            {
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),ldaIaal110.getPnd_Mode_Table_2().getValue(ldaIaal110.getPnd_I()),new ColumnSpacing(16),ldaIaal110.getPnd_Cref_Units_3().getValue(ldaIaal110.getPnd_I()),  //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-3 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-3 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_3().getValue(ldaIaal110.getPnd_I()), new 
                    ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3B"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3B"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*               22X #CREF-DEDUCT-3(#I) (EM=ZZZ,ZZZ,ZZ9.99)
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaIaal110.getPnd_Cref_Units_Total().nadd(ldaIaal110.getPnd_Cref_Units_3().getValue("*"));                                                                    //Natural: ADD #CREF-UNITS-3 ( * ) TO #CREF-UNITS-TOTAL
            ldaIaal110.getPnd_Cref_Dollars_Total().nadd(ldaIaal110.getPnd_Cref_Dollars_3().getValue("*"));                                                                //Natural: ADD #CREF-DOLLARS-3 ( * ) TO #CREF-DOLLARS-TOTAL
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),ldaIaal110.getPnd_Cref_Units_Total(), new                  //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_Total(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            ldaIaal110.getPnd_Cref_Units_Total().reset();                                                                                                                 //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
            ldaIaal110.getPnd_Cref_Dollars_Total().reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  =========================
        //*   WRITE TOTALS REPORT (8)
        //*  =========================
        ldaIaal110.getPnd_Cref_Title().setValue(ldaIaal110.getPnd_Cref_Title_Tab().getValue(4));                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 4 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F4B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (ldaIaal110.getPnd_I().setValue(1); condition(ldaIaal110.getPnd_I().lessOrEqual(22)); ldaIaal110.getPnd_I().nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),ldaIaal110.getPnd_Mode_Table_2().getValue(ldaIaal110.getPnd_I()),new ColumnSpacing(16),ldaIaal110.getPnd_Cref_Units_Tot().getValue(ldaIaal110.getPnd_I()),  //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-TOT ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_Tot().getValue(ldaIaal110.getPnd_I()), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F4B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F4B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*               22X #CREF-DEDUCT-TOT(#I) (EM=ZZZ,ZZZ,ZZ9.99)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaal110.getPnd_Cref_Units_Total().nadd(ldaIaal110.getPnd_Cref_Units_Tot().getValue("*"));                                                                      //Natural: ADD #CREF-UNITS-TOT ( * ) TO #CREF-UNITS-TOTAL
        ldaIaal110.getPnd_Cref_Dollars_Total().nadd(ldaIaal110.getPnd_Cref_Dollars_Tot().getValue("*"));                                                                  //Natural: ADD #CREF-DOLLARS-TOT ( * ) TO #CREF-DOLLARS-TOTAL
        //*       ADD #CREF-DEDUCTION-TOT(*) TO #CREF-DEDUCTION-TOTAL
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),ldaIaal110.getPnd_Cref_Units_Total(), new ReportEditMask       //Natural: WRITE ( 8 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),ldaIaal110.getPnd_Cref_Dollars_Total(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*                22X #CREF-DEDUCT-TOTAL (EM=ZZZ,ZZZ,ZZ9.99
        //*  ================
        //*   RESET VALUES
        //*  ================
        ldaIaal110.getPnd_Cref_Units_Total().reset();                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL #CREF-UNITS-1 ( * ) #CREF-UNITS-2 ( * ) #CREF-UNITS-3 ( * ) #CREF-DOLLARS-1 ( * ) #CREF-DOLLARS-2 ( * ) #CREF-DOLLARS-3 ( * ) #CREF-DEDUCT-1 ( * ) #CREF-DEDUCT-2 ( * ) #CREF-DEDUCT-3 ( * ) #CREF-UNITS-TOT ( * ) #CREF-DOLLARS-TOT ( * ) #CREF-DEDUCT-TOT ( * )
        ldaIaal110.getPnd_Cref_Dollars_Total().reset();
        ldaIaal110.getPnd_Cref_Units_1().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Units_2().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Units_3().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Dollars_1().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Dollars_2().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Dollars_3().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Deduct_1().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Deduct_2().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Deduct_3().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Units_Tot().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Dollars_Tot().getValue("*").reset();
        ldaIaal110.getPnd_Cref_Deduct_Tot().getValue("*").reset();
        //*  #WRITE-CREF-FORMAT
    }
    private void sub_Pnd_Initialization() throws Exception                                                                                                                //Natural: #INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().read(3, ldaIaal110.getPnd_W_Rec_3());                                                                                                              //Natural: READ WORK FILE 3 ONCE #W-REC-3
        pnd_Save_Check_Dte_A.setValue(ldaIaal110.getPnd_W_Rec_3_Pnd_W_Rec_3_A());                                                                                         //Natural: MOVE #W-REC-3-A TO #SAVE-CHECK-DTE-A
        if (condition(pnd_Debug_2.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-2
        {
            getReports().write(0, pnd_Save_Check_Dte_A,ldaIaal110.getPnd_W_Rec_3_Pnd_W_Rec_3_A());                                                                        //Natural: WRITE #SAVE-CHECK-DTE-A #W-REC-3-A
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Save_Check_Dte_A.setValue(pnd_Save_Check_Dte_A);                                                                                                            //Natural: MOVE #SAVE-CHECK-DTE-A TO #W-SAVE-CHECK-DTE-A
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm.setValue(pnd_Save_Check_Dte_A_Pnd_Mm);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-MM := #MM
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd.setValue(pnd_Save_Check_Dte_A_Pnd_Dd);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-DD := #DD
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy.setValue(pnd_Save_Check_Dte_A_Pnd_Yyyy);                                                                                 //Natural: ASSIGN #PAYMENT-DUE-YYYY := #YYYY
        pnd_Payment_Due_Dte_Pnd_Slash1.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH1 := '/'
        pnd_Payment_Due_Dte_Pnd_Slash2.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH2 := '/'
        //*  CHECK IF THE CALL TYPE IS 'L' OR 'P'
        //*  IE FOR DAILY REPORTS WE MUST GET THE LATEST FACTOR (L) BUT ON THE
        //*  1ST OF THE MONTH WE NEED TO GET THE FACTOR USED FOR THE SETTLEMENT (P)
        getWorkFiles().read(2, pnd_Input_Record_Work_2);                                                                                                                  //Natural: READ WORK 2 ONCE #INPUT-RECORD-WORK-2
        //*  LATEST OR PAYMENT FACTOR
        if (condition(! (pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor) || pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Payment_Orign_Factor)))) //Natural: IF NOT ( #CALL-TYPE = #LATEST-FACTOR OR = #PAYMENT-ORIGN-FACTOR )
        {
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*           ERROR READING WORK FILE 2");                                                                                               //Natural: WRITE '*           ERROR READING WORK FILE 2'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                                         //Natural: WRITE '*' '=' #CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*  CALL TYPE MUST BE 'L' OR 'P' ");                                                                                                    //Natural: WRITE '*  CALL TYPE MUST BE "L" OR "P" '
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        //*  #INITIALIZATION
    }
    private void sub_Pnd_Get_Dollar_Amount() throws Exception                                                                                                             //Natural: #GET-DOLLAR-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal110.getPnd_Generic_Unit_Value().reset();                                                                                                                   //Natural: RESET #GENERIC-UNIT-VALUE #GENERIC-DOLLAR-AMOUNT
        ldaIaal110.getPnd_Generic_Dollar_Amount().reset();
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
        sub_Retrieve_Monthly_Annuity_Unit_Value();
        if (condition(Global.isEscape())) {return;}
        ldaIaal110.getPnd_Generic_Dollar_Amount().compute(new ComputeParameters(true, ldaIaal110.getPnd_Generic_Dollar_Amount()), ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().multiply(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Units_Cnt())); //Natural: COMPUTE ROUNDED #GENERIC-DOLLAR-AMOUNT = AUV-RETURNED * #I-TIAA-UNITS-CNT
        //*   #GET-DOLLAR-AMOUNT
    }
    private void sub_Retrieve_Monthly_Annuity_Unit_Value() throws Exception                                                                                               //Natural: RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaan0501_Pda_Pnd_Parm_Fund_1.setValue(" ");                                                                                                                   //Natural: ASSIGN #PARM-FUND-1 := ' '
        pnd_Iaan0501_Pda_Pnd_Parm_Fund_2.setValue(ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde());                                                                  //Natural: ASSIGN #PARM-FUND-2 := #I-TIAA-FUND-CDE
        //*  CONVERT CODE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Iaan0501_Pda_Pnd_Parm_Fund_1, pnd_Iaan0501_Pda_Pnd_Parm_Fund_2, pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde); //Natural: CALLNAT 'IAAN0511' #PARM-FUND-1 #PARM-FUND-2 #PARM-RTN-CDE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde.notEquals(getZero())))                                                                                            //Natural: IF #PARM-RTN-CDE NE 0
        {
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, " ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511");                                                                                   //Natural: WRITE ' ERROR NON ZERO RETURN CODE FROM CALL TO IAAN0511'
            if (Global.isEscape()) return;
            getReports().write(0, "PARM RETURN CODE ",pnd_Iaan0501_Pda_Pnd_Parm_Rtn_Cde," FOR: ","=",ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Nbr(),"=",                  //Natural: WRITE 'PARM RETURN CODE ' #PARM-RTN-CDE ' FOR: ' '=' #I-CNT-NBR '=' #I-CNT-PAYEE-CDE
                ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Payee_Cde());
            if (Global.isEscape()) return;
            getReports().write(0, "PARM-FUND-1 ",pnd_Iaan0501_Pda_Pnd_Parm_Fund_1," PARM-FUND-2 ",pnd_Iaan0501_Pda_Pnd_Parm_Fund_2);                                      //Natural: WRITE 'PARM-FUND-1 ' #PARM-FUND-1 ' PARM-FUND-2 ' #PARM-FUND-2
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_2.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-2
        {
            getReports().write(0, "**");                                                                                                                                  //Natural: WRITE '**'
            if (Global.isEscape()) return;
            getReports().write(0, "Cref fund ",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde(),"converted to ",pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                    //Natural: WRITE 'Cref fund ' #I-TIAA-FUND-CDE 'converted to ' #PARM-FUND-1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  USE THE ALPHA FUND CODE TO RETRIEVE THE FUNDS VALUE
        ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();                                                                                                           //Natural: RESET RETURN-CODE AUV-RETURNED AUV-DATE-RETURN
        ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();
        ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
        //*  'L'
        //*  M = MONTHLY
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                                         //Natural: IF #CALL-TYPE = #LATEST-FACTOR
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                                   //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                          //Natural: ASSIGN IA-CHECK-DATE := 99999999
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date().reset();                                                                                                     //Natural: RESET IA-ISSUE-DATE
            //*  "P"= PAYMENT FOR ORIGINAL SETTLEMENT UNITS (FIRST OF MONTH ONLY)
            //*  P FOR FACTOR USED FOR THE PAYMENT
            //*  MONTHLY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Iaan0501_Pda_Pnd_Parm_Fund_1);                                                                   //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);                                                       //Natural: ASSIGN IA-CHECK-DATE := #W-SAVE-CHECK-DTE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Ccyymm().setValue(ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Issue_Dte());                                            //Natural: ASSIGN IA-ISSUE-CCYYMM := #I-CNT-ISSUE-DTE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(1);                                                                                                  //Natural: ASSIGN IA-ISSUE-DAY := 01
        }                                                                                                                                                                 //Natural: END-IF
        //*  FETCH THE MONTHLY UNIT VALUE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().notEquals(getZero())))                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  ERROR                                 *");                                                                          //Natural: WRITE '*                  ERROR                                 *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an RETURN CODE of =",ldaIaal050.getIa_Aian026_Linkage_Return_Code());                                              //Natural: WRITE '* and returned with an RETURN CODE of =' RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().equals(getZero()) && ldaIaal050.getIa_Aian026_Linkage_Return_Code().equals(getZero())))             //Natural: IF AUV-RETURNED = 0 AND RETURN-CODE = 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                 WARNING                                *");                                                                          //Natural: WRITE '*                 WARNING                                *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                        *");                                                                          //Natural: WRITE '*                                                        *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an ANNUITY-UNIT-VALUE of '0'         *");                                                                          //Natural: WRITE '* and returned with an ANNUITY-UNIT-VALUE of "0"         *'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
    }
    private void sub_Pnd_Check_Payment_Mode() throws Exception                                                                                                            //Natural: #CHECK-PAYMENT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Payment_Due.setValue(false);                                                                                                                                  //Natural: ASSIGN #PAYMENT-DUE := FALSE
        short decideConditionsMet1335 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #I-CNT-MODE-IND;//Natural: VALUE 100
        if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(100))))
        {
            decideConditionsMet1335++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(601))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))) //Natural: IF #MM = 01 OR = 04 OR = 07 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(602))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))) //Natural: IF #MM = 02 OR = 05 OR = 08 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(603))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))) //Natural: IF #MM = 03 OR = 06 OR = 09 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(701))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                //Natural: IF #MM = 01 OR = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(702))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                //Natural: IF #MM = 02 OR = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(703))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                //Natural: IF #MM = 03 OR = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(704))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                               //Natural: IF #MM = 04 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(705))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                               //Natural: IF #MM = 05 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(706))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                               //Natural: IF #MM = 06 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(801))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1)))                                                                                                         //Natural: IF #MM = 01
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(802))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2)))                                                                                                         //Natural: IF #MM = 02
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Payment_Due.setValue(false);                                                                                                                          //Natural: ASSIGN #PAYMENT-DUE := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(803))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3)))                                                                                                         //Natural: IF #MM = 03
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(804))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4)))                                                                                                         //Natural: IF #MM = 04
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(805))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5)))                                                                                                         //Natural: IF #MM = 05
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(806))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6)))                                                                                                         //Natural: IF #MM = 06
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(807))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                                                         //Natural: IF #MM = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(808))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                                                         //Natural: IF #MM = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(809))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                                                         //Natural: IF #MM = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(810))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                                                                        //Natural: IF #MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(811))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                                                                        //Natural: IF #MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((ldaIaal110.getPnd_Input_Record_Pnd_I_Cnt_Mode_Ind().equals(812))))
        {
            decideConditionsMet1335++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                                                                        //Natural: IF #MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #CHECK-PAYMENT-MODE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(2, ReportOption.NOTITLE,"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),ldaIaal110.getPnd_Cref_Title()); //Natural: WRITE ( 2 ) 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 62T #CREF-TITLE
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                             //Natural: IF #CALL-TYPE = #LATEST-FACTOR
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(56),"  TIAA/CREF");                                  //Natural: WRITE ( 2 ) 023T '  TIAA/CREF  ' 056T '  TIAA/CREF'
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(57),"  DOLLARS ");          //Natural: WRITE ( 2 ) 003T 'MODE' 024T '   UNITS     ' 057T '  DOLLARS '
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(53),"(Using Latest Factor)");                                                           //Natural: WRITE ( 2 ) 053T '(Using Latest Factor)'
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"---------------------"); //Natural: WRITE ( 2 ) 003T '----' 022T '--------------' 053T '---------------------'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(59),"  TIAA/CREF");                                  //Natural: WRITE ( 2 ) 023T '  TIAA/CREF  ' 059T '  TIAA/CREF'
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(60),"  DOLLARS ");          //Natural: WRITE ( 2 ) 003T 'MODE' 024T '   UNITS     ' 060T '  DOLLARS '
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(53),"(Using Original Settlement)");                                                     //Natural: WRITE ( 2 ) 053T '(Using Original Settlement)'
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"---------------------------"); //Natural: WRITE ( 2 ) 003T '----' 022T '--------------' 053T '---------------------------'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    //*  PAGE 2
                    //*  PAGE 3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) NOTITLE ' '
                    getReports().write(3, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 3 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(3, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 3 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 3 ) 51T '                CURRENT MODE                 '
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(51),"(INCLUDING PERIODIC PAYMENTS VALUED NOT PAID)");                                       //Natural: WRITE ( 3 ) 51T '(INCLUDING PERIODIC PAYMENTS VALUED NOT PAID)'
                    getReports().skip(3, 2);                                                                                                                              //Natural: SKIP ( 3 ) 2
                    //*  PAGE 3
                    //*  PAGE 4
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 4 ) NOTITLE ' '
                    getReports().write(4, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 4 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(4, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 4 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 4 ) 51T '                CURRENT MODE                 '
                    getReports().write(4, ReportOption.NOTITLE,NEWLINE,new TabSetting(51),"PERIODIC PAYMENTS VALUED NOT PAID (DETAILS)");                                 //Natural: WRITE ( 4 ) / 51T 'PERIODIC PAYMENTS VALUED NOT PAID (DETAILS)'
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 4 ) 1
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(35),"  CONTRACTUAL",new TabSetting(56),"   PERIODIC");                                      //Natural: WRITE ( 4 ) 035T '  CONTRACTUAL' 056T '   PERIODIC'
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT/RS",new TabSetting(22),"MODE",new TabSetting(35),"    AMOUNT",new              //Natural: WRITE ( 4 ) 001T 'CONTRACT/RS' 022T 'MODE' 035T '    AMOUNT' 056T '   DIVIDEND'
                        TabSetting(56),"   DIVIDEND");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"-----------",new TabSetting(22),"----",new TabSetting(35),"--------------",new          //Natural: WRITE ( 4 ) 001T '-----------' 022T '----' 035T '--------------' 056T '--------------'
                        TabSetting(56),"--------------");
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 4 ) 1
                    //*  PAGE 4
                    //*  PAGE 5
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 5 ) NOTITLE ' '
                    getReports().write(5, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 5 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(5, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 5 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 5 ) 51T '                CURRENT MODE                 '
                    getReports().skip(5, 2);                                                                                                                              //Natural: SKIP ( 5 ) 2
                    //*  PAGE 5
                    //*  PAGE 6
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 6 ) NOTITLE ' '
                    getReports().write(6, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 6 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(6, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 6 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 6 ) 51T '                CURRENT MODE                 '
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(51),"               (PAYMENTS PAID)               ");                                       //Natural: WRITE ( 6 ) 51T '               (PAYMENTS PAID)               '
                    getReports().skip(6, 2);                                                                                                                              //Natural: SKIP ( 6 ) 2
                    //*  PAGE 6
                    //*  PAGE 7
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 7 ) NOTITLE ' '
                    getReports().write(7, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 7 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(7, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 7 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 7 ) 51T '                CURRENT MODE                 '
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(51),"                 (ROLLOVERS)                 ");                                       //Natural: WRITE ( 7 ) 51T '                 (ROLLOVERS)                 '
                    getReports().skip(7, 2);                                                                                                                              //Natural: SKIP ( 7 ) 2
                    //*  PAGE 7
                    //*  PAGE 8 TOTALS REPORT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt8 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(8, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 8 ) NOTITLE ' '
                    getReports().write(8, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 8 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(8, ReportOption.NOTITLE,"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),ldaIaal110.getPnd_Cref_Title()); //Natural: WRITE ( 8 ) 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 62T #CREF-TITLE
                    getReports().skip(8, 1);                                                                                                                              //Natural: SKIP ( 8 ) 1
                    if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                             //Natural: IF #CALL-TYPE = #LATEST-FACTOR
                    {
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(56),"  TIAA/CREF");                                  //Natural: WRITE ( 8 ) 023T '  TIAA/CREF  ' 056T '  TIAA/CREF'
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(57),"  DOLLARS ");          //Natural: WRITE ( 8 ) 003T 'MODE' 024T '   UNITS     ' 057T '  DOLLARS '
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(53),"(Using Latest Factor)");                                                           //Natural: WRITE ( 8 ) 053T '(Using Latest Factor)'
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"---------------------"); //Natural: WRITE ( 8 ) 003T '----' 022T '--------------' 053T '---------------------'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(59),"  TIAA/CREF");                                  //Natural: WRITE ( 8 ) 023T '  TIAA/CREF  ' 059T '  TIAA/CREF'
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(60),"  DOLLARS ");          //Natural: WRITE ( 8 ) 003T 'MODE' 024T '   UNITS     ' 060T '  DOLLARS '
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(53),"(Using Original Settlement)");                                                     //Natural: WRITE ( 8 ) 053T '(Using Original Settlement)'
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"---------------------------"); //Natural: WRITE ( 8 ) 003T '----' 022T '--------------' 053T '---------------------------'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    //*  PAGE 8
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventR1() throws Exception {atBreakEventR1(false);}
    private void atBreakEventR1(boolean endOfData) throws Exception
    {
        boolean ldaIaal110_getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotIsBreak = ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot().isBreak(endOfData);
        if (condition(ldaIaal110_getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotIsBreak))
        {
            pnd_Cnt_Tot.reset();                                                                                                                                          //Natural: RESET #CNT-TOT #CNT1 #CNT2 #CNT3 #CNT-NONE
            pnd_Cnt1.reset();
            pnd_Cnt2.reset();
            pnd_Cnt3.reset();
            pnd_Cnt_None.reset();
                                                                                                                                                                          //Natural: PERFORM #FUND-INFORMATION
            sub_Pnd_Fund_Information();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-CREF-FORMAT
            sub_Pnd_Write_Cref_Format();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Debug_2.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-2
            {
                getReports().write(0, "AT BREAK","=",ldaIaal110.getPnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot());                                                           //Natural: WRITE 'AT BREAK' '=' #I-TIAA-FUND-CDE-TOT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=56");
        Global.format(3, "LS=133 PS=56");
        Global.format(4, "LS=133 PS=56");
        Global.format(5, "LS=133 PS=56");
        Global.format(6, "LS=133 PS=56");
        Global.format(7, "LS=133 PS=56");
        Global.format(8, "LS=133 PS=56");
    }
}
