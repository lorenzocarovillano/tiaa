/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:25:06 PM
**        * FROM NATURAL PROGRAM : Iaap342
************************************************************
**        * FILE NAME            : Iaap342.java
**        * CLASS NAME           : Iaap342
**        * INSTANCE NAME        : Iaap342
************************************************************
************************************************************************
* PROGRAM  : IAAP342
* SYSTEM   : IAD
* TITLE    : UPDATE IA CPR & DEDUCTION VIEWS
* CREATED  : JAN 29, 96
* FUNCTION : APPLY AUTO GENERATED TRANSACTIONS 006,700 900 TO
* TITLE    : CPR & DEDUCTION VIEWS UPDATE IA-CNTRL-RCRD-1 TO REFLECT
*            ANY CHANGES
*
* HISTORY
*
*  3/98   LEN B :- ADDED 3 DAY FIELDS TO IAA-CNTRCT AND IAA-CNTRCT-TRANS
*                  VIEWS.
*
*  9/01   ADDED LOGIC TO HANDLE 102 TRANS PEND-CODE (Q) USED FOR IPRO
*         DO SCAN ON 9/01
*
* 04/02   RESTOWED FOR OIA CHANGES IN IAAN051A
*
* 11/03   INCREASE RATE OCCURS FROM 1:60 TO 1:90
*         DO SCAN ON 11/03
*
* 3/06    CHANGE REFERENCE TO #W-PAGE-CTR WITH *PAGE-NUMBER(1)
*         DO SCAN ON 3/06.
*
* 04/08   INCREASE RATE OCCURS TO 99 AND ADDED ROTH FIELDS
*         DO SCAN ON  4/08
* 03/12   INCREASE RATE OCCURS TO 250
*         DO SCAN ON  3/12
* 04/2017 OS RE-STOWED ONLY FOR PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap342 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Desc;

    private DbsGroup pnd_Parm_Desc__R_Field_1;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_6;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_Rem;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Save_Payee_Key;

    private DbsGroup pnd_Save_Payee_Key__R_Field_2;
    private DbsField pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Payee_Key_Pnd_Save_Payee_Cde;
    private DbsField pnd_Fund_Rcrd_Key;

    private DbsGroup pnd_Fund_Rcrd_Key__R_Field_3;
    private DbsField pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr;
    private DbsField pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde;
    private DbsField pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Ppcn_Nbr_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Cref_Cntrct_Fund_Key;

    private DbsGroup pnd_Cref_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Ppcn_Nbr_Payee;
    private DbsField pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde;
    private DbsField pnd_Iaa_Ddctn_Key;

    private DbsGroup pnd_Iaa_Ddctn_Key__R_Field_7;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Iaa_Ddctn_Key__R_Field_8;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Cntrct_Nmbr;

    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_Trans_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_9;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Trans__R_Field_10;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund;

    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Rcrd__R_Field_11;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund;
    private DbsField iaa_Cref_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Cref_Fund_Trans;
    private DbsField iaa_Cref_Fund_Trans_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Trans__R_Field_12;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Trans_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_Cref_Mode_Ind;
    private DbsField iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund;
    private DbsField iaa_Cref_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_13;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte_A;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;

    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Id_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;

    private DbsGroup iaa_Ddctn_Trans__R_Field_14;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ytd_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Intent_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Trans_Check_Dte;
    private DbsField iaa_Ddctn_Trans_Bfre_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Lst_Trans_Dte;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;

    private DbsGroup pnd_Cntrl_Wrk;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt;

    private DbsGroup pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde;

    private DbsGroup pnd_Cntrl_Wrk__R_Field_15;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2;

    private DbsGroup pnd_Cntrl_Wrk__R_Field_16;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Rec_Desc_Lne;

    private DbsGroup pnd_Cntrl_Wrk__R_Field_17;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Units;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne;

    private DbsGroup pnd_Cntrl_Wrk__R_Field_18;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Amt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys;
    private DbsField pnd_Cntrl_Wrk_Pnd_Cntrl_Inactve_Payees;

    private DbsGroup pnd_Tran_Rec_In;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tran_Code;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye;

    private DbsGroup pnd_Tran_Rec_In__R_Field_19;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr;

    private DbsGroup pnd_Tran_Rec_In__R_Field_20;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_2;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Processed;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde;

    private DbsGroup pnd_Tran_Rec_In__R_Field_21;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Cmpny_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Fund_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Filler1;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Xref;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Rcvry_Type;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Free;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Amt;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tot_Units;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Pmt;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Div;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Pmt;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Div;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Int_Code;

    private DbsGroup pnd_Tran_Rec_In__R_Field_22;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Pend_Cde;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ded_Code;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt;

    private DbsGroup pnd_Tran_Rec_In__R_Field_23;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte;

    private DbsGroup pnd_Tran_Rec_In__R_Field_24;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Ccyy;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Mm;
    private DbsField pnd_Tran_Rec_In_Pnd_Tr_Fill;
    private DbsField pnd_Sve_Old_Per_Ded_Amt;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr;

    private DbsGroup pnd_W_Ded_Cde_Seq_Nbr__R_Field_25;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code;
    private DbsField pnd_Check_D;
    private DbsField pnd_Check_A;

    private DbsGroup pnd_Check_A__R_Field_26;
    private DbsField pnd_Check_A_Pnd_Check_N;

    private DbsGroup pnd_Check_A__R_Field_27;
    private DbsField pnd_Check_A_Pnd_Check_Ccyymm;
    private DbsField pnd_Check_A_Pnd_Check_Dd;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Sve_Trans_Dte;

    private DbsGroup pnd_Prog_Misc_Area;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Int_Code;
    private DbsField pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read;
    private DbsField pnd_Prog_Misc_Area_Pnd_Cpr_Fnd;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Isn;
    private DbsField pnd_Prog_Misc_Area_Pnd_Total_Trans;
    private DbsField pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt;
    private DbsField pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading2;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading3;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Fnd_Cnt;
    private DbsField pnd_Wrk_Timx;
    private DbsField pnd_Sve_Ppcn_Nbr_Pye;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm_Fund_2 = localVariables.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Desc = localVariables.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);

        pnd_Parm_Desc__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Desc__R_Field_1", "REDEFINE", pnd_Parm_Desc);
        pnd_Parm_Desc_Pnd_Parm_Desc_6 = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_6", "#PARM-DESC-6", FieldType.STRING, 6);
        pnd_Parm_Desc_Pnd_Parm_Desc_Rem = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_Rem", "#PARM-DESC-REM", FieldType.STRING, 
            29);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Parm_Len = localVariables.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.INTEGER, 2);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Save_Payee_Key = localVariables.newFieldInRecord("pnd_Save_Payee_Key", "#SAVE-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Save_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Save_Payee_Key__R_Field_2", "REDEFINE", pnd_Save_Payee_Key);
        pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr = pnd_Save_Payee_Key__R_Field_2.newFieldInGroup("pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Save_Payee_Key_Pnd_Save_Payee_Cde = pnd_Save_Payee_Key__R_Field_2.newFieldInGroup("pnd_Save_Payee_Key_Pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Rcrd_Key = localVariables.newFieldInRecord("pnd_Fund_Rcrd_Key", "#FUND-RCRD-KEY", FieldType.STRING, 15);

        pnd_Fund_Rcrd_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Fund_Rcrd_Key__R_Field_3", "REDEFINE", pnd_Fund_Rcrd_Key);
        pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr = pnd_Fund_Rcrd_Key__R_Field_3.newFieldInGroup("pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr", "#FUND-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde = pnd_Fund_Rcrd_Key__R_Field_3.newFieldInGroup("pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde", "#FUND-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde = pnd_Fund_Rcrd_Key__R_Field_3.newFieldInGroup("pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde", "#FUND-CMPNY-FUND-CDE", 
            FieldType.STRING, 3);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Ppcn_Nbr_Payee = pnd_Tiaa_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Ppcn_Nbr_Payee", 
            "#TIAA-PPCN-NBR-PAYEE", FieldType.STRING, 12);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", 
            "#TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Cref_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cref_Cntrct_Fund_Key", "#CREF-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cref_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cref_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cref_Cntrct_Fund_Key);
        pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Ppcn_Nbr_Payee = pnd_Cref_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Ppcn_Nbr_Payee", 
            "#CREF-PPCN-NBR-PAYEE", FieldType.STRING, 12);
        pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde = pnd_Cref_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cref_Cntrct_Fund_Key_Pnd_Cref_Cmpny_Fund_Cde", 
            "#CREF-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Iaa_Ddctn_Key = localVariables.newFieldInRecord("pnd_Iaa_Ddctn_Key", "#IAA-DDCTN-KEY", FieldType.STRING, 18);

        pnd_Iaa_Ddctn_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Iaa_Ddctn_Key__R_Field_7", "REDEFINE", pnd_Iaa_Ddctn_Key);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);

        pnd_Iaa_Ddctn_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Iaa_Ddctn_Key__R_Field_8", "REDEFINE", pnd_Iaa_Ddctn_Key);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee = pnd_Iaa_Ddctn_Key__R_Field_8.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee", "#DDCTN-PPCN-NBR-PAYEE", 
            FieldType.STRING, 12);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key = pnd_Iaa_Ddctn_Key__R_Field_8.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key", "#DDCTN-REST-OF-KEY", 
            FieldType.STRING, 6);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Roth_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Tax_Exmpt_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_Orig_Ownr_Dob = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_Orig_Ownr_Dod = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("IAA_CNTRCT_TRANS_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        registerRecord(vw_iaa_Cntrct_Trans);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cpr_Trans_Roth_Dsblty_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");
        registerRecord(vw_iaa_Cpr_Trans);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_9 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_9", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_9.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_9.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_CMPNY_FUND", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Trans__R_Field_10 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans__R_Field_10", "REDEFINE", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans__R_Field_10.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans__R_Field_10.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("IAA_TIAA_FUND_TRANS_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        registerRecord(vw_iaa_Tiaa_Fund_Trans);

        vw_iaa_Cref_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd", "IAA-CREF-FUND-RCRD"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Rcrd__R_Field_11 = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd__R_Field_11", "REDEFINE", iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd__R_Field_11.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Cref_Fund_Rcrd_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd__R_Field_11.newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_UNIT_VAL", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_OLD_PER_AMT", "CREF-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd.getRecord().newGroupInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_RATE_DTE", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_Cref_Mode_Ind = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_MODE_IND", "CREF-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Old_Cmpny_Fund", "CREF-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");
        iaa_Cref_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_CREF_XFR_ISS_DTE", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Cref_Fund_Rcrd);

        vw_iaa_Cref_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans", "IAA-CREF-FUND-TRANS"), "IAA_CREF_FUND_TRANS_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Trans__R_Field_12 = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans__R_Field_12", "REDEFINE", iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans__R_Field_12.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Cref_Fund_Trans_Cref_Fund_Cde = iaa_Cref_Fund_Trans__R_Field_12.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Trans_Cref_Unit_Val = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Unit_Val", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Cde = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Dte = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Units_Cnt = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Mode_Ind = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_MODE_IND", "CREF-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Cref_Fund_Trans_Cref_Old_Cmpny_Fund = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_OLD_CMPNY_FUND", "CREF-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        iaa_Cref_Fund_Trans_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cref_Fund_Trans_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_XFR_ISS_DTE", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Trans_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_LST_XFR_IN_DTE", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Cref_Fund_Trans);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");

        iaa_Trans_Rcrd__R_Field_13 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_13", "REDEFINE", iaa_Trans_Rcrd_Trans_Todays_Dte);
        iaa_Trans_Rcrd_Trans_Todays_Dte_A = iaa_Trans_Rcrd__R_Field_13.newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte_A", "TRANS-TODAYS-DTE-A", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Id_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");

        iaa_Ddctn_Trans__R_Field_14 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans__R_Field_14", "REDEFINE", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Seq_Nbr = iaa_Ddctn_Trans__R_Field_14.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans__R_Field_14.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Ytd_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ddctn_Trans_Ddctn_Pd_To_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Intent_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Trans_Check_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Ddctn_Trans_Bfre_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Ddctn_Trans_Aftr_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Ddctn_Trans);

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Ytd_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_Ddctn_Pd_To_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Deduction);

        pnd_Cntrl_Wrk = localVariables.newGroupInRecord("pnd_Cntrl_Wrk", "#CNTRL-WRK");
        pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees", "#CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt", "#CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt", "#CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt", "#CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt", "#CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt", "#CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts = pnd_Cntrl_Wrk.newGroupArrayInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts", "#CNTRL-FUND-CNTS", new DbsArrayController(1, 
            80));
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde", "#CNTRL-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Cntrl_Wrk__R_Field_15 = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newGroupInGroup("pnd_Cntrl_Wrk__R_Field_15", "REDEFINE", pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde = pnd_Cntrl_Wrk__R_Field_15.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde", "#CNTRL-CMP-CDE", FieldType.STRING, 
            1);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2 = pnd_Cntrl_Wrk__R_Field_15.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2", "#CNTRL-FUND-CDE-2", FieldType.STRING, 
            2);

        pnd_Cntrl_Wrk__R_Field_16 = pnd_Cntrl_Wrk__R_Field_15.newGroupInGroup("pnd_Cntrl_Wrk__R_Field_16", "REDEFINE", pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf = pnd_Cntrl_Wrk__R_Field_16.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf", "#CNTRL-FUND-CDE-2-REDF", 
            FieldType.NUMERIC, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Rec_Desc_Lne = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Rec_Desc_Lne", "#CNTRL-FUND-REC-DESC-LNE", 
            FieldType.STRING, 33);

        pnd_Cntrl_Wrk__R_Field_17 = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newGroupInGroup("pnd_Cntrl_Wrk__R_Field_17", "REDEFINE", pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Rec_Desc_Lne);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant = pnd_Cntrl_Wrk__R_Field_17.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant", "#CNTRL-REC-CONSTANT", 
            FieldType.STRING, 5);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc = pnd_Cntrl_Wrk__R_Field_17.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc", "#CNTRL-REC-FUND-DESC", 
            FieldType.STRING, 7);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt = pnd_Cntrl_Wrk__R_Field_17.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt", "#CNTRL-REC-FUND-TXT", 
            FieldType.STRING, 21);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees", "#CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Units = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Units", "#CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne", "#CNTRL-FUND-UNT-DESC-LNE", 
            FieldType.STRING, 33);

        pnd_Cntrl_Wrk__R_Field_18 = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newGroupInGroup("pnd_Cntrl_Wrk__R_Field_18", "REDEFINE", pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant = pnd_Cntrl_Wrk__R_Field_18.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant", "#CNTRL-UNT-CONSTANT", 
            FieldType.STRING, 5);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc = pnd_Cntrl_Wrk__R_Field_18.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc", "#CNTRL-UNT-FUND-DESC", 
            FieldType.STRING, 7);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt = pnd_Cntrl_Wrk__R_Field_18.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt", "#CNTRL-UNT-FUND-TXT", 
            FieldType.STRING, 21);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Amt = pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cnts.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Amt", "#CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt", "#CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt", "#CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys", "#CNTRL-ACTVE-TIAA-PYS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys", "#CNTRL-ACTVE-CREF-PYS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cntrl_Wrk_Pnd_Cntrl_Inactve_Payees = pnd_Cntrl_Wrk.newFieldInGroup("pnd_Cntrl_Wrk_Pnd_Cntrl_Inactve_Payees", "#CNTRL-INACTVE-PAYEES", FieldType.PACKED_DECIMAL, 
            9);

        pnd_Tran_Rec_In = localVariables.newGroupInRecord("pnd_Tran_Rec_In", "#TRAN-REC-IN");
        pnd_Tran_Rec_In_Pnd_Tr_Tran_Code = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tran_Code", "#TR-TRAN-CODE", FieldType.NUMERIC, 3);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye", "#TR-PPCN-NBR-PYE", FieldType.STRING, 
            12);

        pnd_Tran_Rec_In__R_Field_19 = pnd_Tran_Rec_In.newGroupInGroup("pnd_Tran_Rec_In__R_Field_19", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr = pnd_Tran_Rec_In__R_Field_19.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr", "#TR-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Tran_Rec_In__R_Field_20 = pnd_Tran_Rec_In__R_Field_19.newGroupInGroup("pnd_Tran_Rec_In__R_Field_20", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8 = pnd_Tran_Rec_In__R_Field_20.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8", "#TR-PPCN-NBR-8", FieldType.STRING, 
            8);
        pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_2 = pnd_Tran_Rec_In__R_Field_20.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_2", "#TR-PPCN-NBR-2", FieldType.STRING, 
            2);
        pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde = pnd_Tran_Rec_In__R_Field_19.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde", "#TR-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Processed = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Processed", "#TR-CHK-DTE-PROCESSED", 
            FieldType.NUMERIC, 8);
        pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due", "#TR-CHK-DTE-DUE", FieldType.NUMERIC, 
            8);
        pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde", "#TR-CMP-FND-CDE", FieldType.STRING, 
            3);

        pnd_Tran_Rec_In__R_Field_21 = pnd_Tran_Rec_In.newGroupInGroup("pnd_Tran_Rec_In__R_Field_21", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde);
        pnd_Tran_Rec_In_Pnd_Tr_Cmpny_Cde = pnd_Tran_Rec_In__R_Field_21.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Cmpny_Cde", "#TR-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Tran_Rec_In_Pnd_Tr_Fund_Cde = pnd_Tran_Rec_In__R_Field_21.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Fund_Cde", "#TR-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Tran_Rec_In_Pnd_Tr_Filler1 = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Filler1", "#TR-FILLER1", FieldType.STRING, 1);
        pnd_Tran_Rec_In_Pnd_Tr_Xref = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Xref", "#TR-XREF", FieldType.STRING, 9);
        pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde = pnd_Tran_Rec_In.newFieldArrayInGroup("pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde", "#TR-CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Tran_Rec_In_Pnd_Tr_Rcvry_Type = pnd_Tran_Rec_In.newFieldArrayInGroup("pnd_Tran_Rec_In_Pnd_Tr_Rcvry_Type", "#TR-RCVRY-TYPE", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Free = pnd_Tran_Rec_In.newFieldArrayInGroup("pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Free", "#TR-PER-TAX-FREE", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Amt = pnd_Tran_Rec_In.newFieldArrayInGroup("pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Amt", "#TR-PER-TAX-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Tran_Rec_In_Pnd_Tr_Tot_Units = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tot_Units", "#TR-TOT-UNITS", FieldType.NUMERIC, 13, 
            3);
        pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Pmt = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Pmt", "#TR-TOT-PER-PMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Div = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Div", "#TR-TOT-PER-DIV", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Pmt = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Pmt", "#TR-TOT-FIN-PMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Div = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Div", "#TR-TOT-FIN-DIV", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_In_Pnd_Tr_Int_Code = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Int_Code", "#TR-INT-CODE", FieldType.NUMERIC, 1);

        pnd_Tran_Rec_In__R_Field_22 = pnd_Tran_Rec_In.newGroupInGroup("pnd_Tran_Rec_In__R_Field_22", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Int_Code);
        pnd_Tran_Rec_In_Pnd_Tr_Pend_Cde = pnd_Tran_Rec_In__R_Field_22.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Pend_Cde", "#TR-PEND-CDE", FieldType.STRING, 
            1);
        pnd_Tran_Rec_In_Pnd_Tr_Ded_Code = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ded_Code", "#TR-DED-CODE", FieldType.STRING, 3);
        pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr", "#TR-DED-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt = pnd_Tran_Rec_In.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt", "#TR-PER-DED-AMT", FieldType.NUMERIC, 
            7, 2);

        pnd_Tran_Rec_In__R_Field_23 = pnd_Tran_Rec_In.newGroupInGroup("pnd_Tran_Rec_In__R_Field_23", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt);
        pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte = pnd_Tran_Rec_In__R_Field_23.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte", "#TR-FIN-PER-DTE", FieldType.NUMERIC, 
            6);

        pnd_Tran_Rec_In__R_Field_24 = pnd_Tran_Rec_In__R_Field_23.newGroupInGroup("pnd_Tran_Rec_In__R_Field_24", "REDEFINE", pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte);
        pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Ccyy = pnd_Tran_Rec_In__R_Field_24.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Ccyy", "#TR-FIN-PER-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Mm = pnd_Tran_Rec_In__R_Field_24.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Mm", "#TR-FIN-PER-MM", FieldType.NUMERIC, 
            2);
        pnd_Tran_Rec_In_Pnd_Tr_Fill = pnd_Tran_Rec_In__R_Field_23.newFieldInGroup("pnd_Tran_Rec_In_Pnd_Tr_Fill", "#TR-FILL", FieldType.NUMERIC, 1);
        pnd_Sve_Old_Per_Ded_Amt = localVariables.newFieldInRecord("pnd_Sve_Old_Per_Ded_Amt", "#SVE-OLD-PER-DED-AMT", FieldType.NUMERIC, 7, 2);
        pnd_W_Ded_Cde_Seq_Nbr = localVariables.newFieldInRecord("pnd_W_Ded_Cde_Seq_Nbr", "#W-DED-CDE-SEQ-NBR", FieldType.STRING, 6);

        pnd_W_Ded_Cde_Seq_Nbr__R_Field_25 = localVariables.newGroupInRecord("pnd_W_Ded_Cde_Seq_Nbr__R_Field_25", "REDEFINE", pnd_W_Ded_Cde_Seq_Nbr);
        pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr = pnd_W_Ded_Cde_Seq_Nbr__R_Field_25.newFieldInGroup("pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr", "#W-DED-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code = pnd_W_Ded_Cde_Seq_Nbr__R_Field_25.newFieldInGroup("pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code", "#W-DED-CODE", 
            FieldType.STRING, 3);
        pnd_Check_D = localVariables.newFieldInRecord("pnd_Check_D", "#CHECK-D", FieldType.DATE);
        pnd_Check_A = localVariables.newFieldInRecord("pnd_Check_A", "#CHECK-A", FieldType.STRING, 8);

        pnd_Check_A__R_Field_26 = localVariables.newGroupInRecord("pnd_Check_A__R_Field_26", "REDEFINE", pnd_Check_A);
        pnd_Check_A_Pnd_Check_N = pnd_Check_A__R_Field_26.newFieldInGroup("pnd_Check_A_Pnd_Check_N", "#CHECK-N", FieldType.NUMERIC, 8);

        pnd_Check_A__R_Field_27 = localVariables.newGroupInRecord("pnd_Check_A__R_Field_27", "REDEFINE", pnd_Check_A);
        pnd_Check_A_Pnd_Check_Ccyymm = pnd_Check_A__R_Field_27.newFieldInGroup("pnd_Check_A_Pnd_Check_Ccyymm", "#CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Check_A_Pnd_Check_Dd = pnd_Check_A__R_Field_27.newFieldInGroup("pnd_Check_A_Pnd_Check_Dd", "#CHECK-DD", FieldType.NUMERIC, 2);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Sve_Trans_Dte = localVariables.newFieldInRecord("pnd_Sve_Trans_Dte", "#SVE-TRANS-DTE", FieldType.TIME);

        pnd_Prog_Misc_Area = localVariables.newGroupInRecord("pnd_Prog_Misc_Area", "#PROG-MISC-AREA");
        pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code", "#SVE-TRAN-CODE", FieldType.NUMERIC, 
            3);
        pnd_Prog_Misc_Area_Pnd_Sve_Int_Code = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Int_Code", "#SVE-INT-CODE", FieldType.NUMERIC, 
            1);
        pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read", "#FST-TRN-READ", FieldType.STRING, 
            1);
        pnd_Prog_Misc_Area_Pnd_Cpr_Fnd = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Cpr_Fnd", "#CPR-FND", FieldType.STRING, 1);
        pnd_Prog_Misc_Area_Pnd_Sve_Isn = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Isn", "#SVE-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prog_Misc_Area_Pnd_Total_Trans = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Total_Trans", "#TOTAL-TRANS", FieldType.NUMERIC, 
            6);
        pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt", "#W-NEW-DED-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc", "#TYPE-TRANS-DESC", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Term", "#DETAIL-HEAD-TERM", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax", "#DETAIL-HEAD-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng", "#DETAIL-HEAD-DED-CHNG", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop", "#DETAIL-HEAD-DED-STOP", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend", "#DETAIL-HEAD-102-PEND", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading", "#DETAIL-HEADING", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading2 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading2", "#DETAIL-HEADING2", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading3 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading3", "#DETAIL-HEADING3", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term", "#DETAIL-HEAD1-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term", "#DETAIL-HEAD2-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term", "#DETAIL-HEAD3-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_102", "#DETAIL-HEAD1-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_102", "#DETAIL-HEAD2-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_102", "#DETAIL-HEAD3-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax", "#DETAIL-HEAD1-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax", "#DETAIL-HEAD2-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax", "#DETAIL-HEAD3-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded", "#DETAIL-HEAD1-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded", "#DETAIL-HEAD2-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded", "#DETAIL-HEAD3-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl", "#DETAIL-HEAD1-CTL", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl", "#DETAIL-HEAD2-CTL", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl", "#DETAIL-HEAD3-CTL", FieldType.STRING, 
            90);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 2);
        pnd_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Fnd_Cnt", "#FND-CNT", FieldType.NUMERIC, 3);
        pnd_Wrk_Timx = localVariables.newFieldInRecord("pnd_Wrk_Timx", "#WRK-TIMX", FieldType.TIME);
        pnd_Sve_Ppcn_Nbr_Pye = localVariables.newFieldInRecord("pnd_Sve_Ppcn_Nbr_Pye", "#SVE-PPCN-NBR-PYE", FieldType.STRING, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Rcrd.reset();
        vw_iaa_Cref_Fund_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Ddctn_Trans.reset();
        vw_iaa_Deduction.reset();

        localVariables.reset();
        pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setInitialValue(999);
        pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setInitialValue(0);
        pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read.setInitialValue("N");
        pnd_Prog_Misc_Area_Pnd_Cpr_Fnd.setInitialValue("N");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Term.setInitialValue("  TERMINATIONS (TRANS 006)                             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax.setInitialValue("  TAX TRANSACTIONS (TRANS 700)                         ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng.setInitialValue("  DEDUCTIONS FROM NET (TRANS 900) CHANGES              ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop.setInitialValue("  DEDUCTIONS FROM NET (TRANS 900) STOP DEDUCTIONS      ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend.setInitialValue("  SUSPEND-PAYMENT (TRANS 102)                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term.setInitialValue(" CONTRACT PAYEE    CROSS-REF                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term.setInitialValue("  NUMBER   CODE    NUMBER                             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term.setInitialValue("--------- ------   ---------                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_102.setInitialValue(" CONTRACT PAYEE    CROSS-REF  PEND      EFFECTIVE            ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_102.setInitialValue("  NUMBER   CODE    NUMBER     CODE      DATE                 ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_102.setInitialValue("--------- ------   ---------  ------    -------              ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax.setInitialValue(" CONTRACT PAYEE    CROSS-REF  RECVRY    PER TAX       PER TAX");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax.setInitialValue("  NUMBER   CODE    NUMBER     TYPE      DEDUCT        FREE   ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax.setInitialValue("--------- ------   ---------  ------    -------       -------");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded.setInitialValue(" CONTRACT PAYEE    CROSS-REF    NEW       SEQ    DED    OLD    ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded.setInitialValue("  NUMBER   CODE    NUMBER       AMT       NO     CODE   AMT    ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded.setInitialValue("--------- ------   ---------    ------    ----   -----  -------");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl.setInitialValue("                                         FILE                ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl.setInitialValue("                                         CHANGES             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl.setInitialValue("                                         --------------      ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap342() throws Exception
    {
        super("Iaap342");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: FORMAT ( 2 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-CREF-ACCUM-TABLE
        sub_Initialize_Cref_Accum_Table();
        if (condition(Global.isEscape())) {return;}
        //*        1/98 GET BUSINESS DATE FROM LATEST  'DC' CONTROL RECORD
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        RD1:                                                                                                                                                              //Natural: READ WORK 1 #TRAN-REC-IN
        while (condition(getWorkFiles().read(1, pnd_Tran_Rec_In)))
        {
            if (condition(pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read.equals("N")))                                                                                               //Natural: IF #FST-TRN-READ = 'N'
            {
                pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read.setValue("Y");                                                                                                        //Natural: ASSIGN #FST-TRN-READ := 'Y'
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.notEquals(getZero())))                                                                                     //Natural: IF #TR-TRAN-CODE NE 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
                    sub_Error_Processing();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Check_A_Pnd_Check_N.setValue(pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Due);                                                                                 //Natural: ASSIGN #CHECK-N := #TR-CHK-DTE-DUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  FIRST TIME THRU INITIALIZED TO 999
            if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(999)))                                                                                              //Natural: IF #SVE-TRAN-CODE = 999
            {
                pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setValue(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code);                                                                          //Natural: ASSIGN #SVE-TRAN-CODE := #TR-TRAN-CODE
                pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setValue(pnd_Tran_Rec_In_Pnd_Tr_Int_Code);                                                                            //Natural: ASSIGN #SVE-INT-CODE := #TR-INT-CODE
                                                                                                                                                                          //Natural: PERFORM SET-HEADING-LINES
                sub_Set_Heading_Lines();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.notEquals(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code) || pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.notEquals(pnd_Tran_Rec_In_Pnd_Tr_Int_Code))) //Natural: IF #SVE-TRAN-CODE NE #TR-TRAN-CODE OR #SVE-INT-CODE NE #TR-INT-CODE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-LINE
                    sub_Print_Total_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM SET-HEADING-LINES
                    sub_Set_Heading_Lines();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 ) EVEN IF TOP OF PAGE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setValue(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code);                                                                      //Natural: ASSIGN #SVE-TRAN-CODE := #TR-TRAN-CODE
                    pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setValue(pnd_Tran_Rec_In_Pnd_Tr_Int_Code);                                                                        //Natural: ASSIGN #SVE-INT-CODE := #TR-INT-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED FOLLOWING CODE TO PREVENT DUPLICATE KEY ERRORS 4/00
            pnd_Wrk_Timx.setValue(Global.getTIMX());                                                                                                                      //Natural: ASSIGN #WRK-TIMX := *TIMX
            //*     TO PREVENT DUPLICATE DESCRIPTOR KEY ON STORE CPR-TRANS
            //*     WHEN HAVE 2 TRANS FOR SAME CONTRACT & PAYEE
            //*     VARIABLE *TIMX IS USED AS PART OF KEY FOR UNIQUENESS
            if (condition(pnd_Sve_Ppcn_Nbr_Pye.equals(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye)))                                                                              //Natural: IF #SVE-PPCN-NBR-PYE = #TR-PPCN-NBR-PYE
            {
                pnd_Wrk_Timx.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WRK-TIMX
                //*  ADDED REPLACED *TIMX WITH #WRK-TIMX 11/03
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sve_Ppcn_Nbr_Pye.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_Pye);                                                                                           //Natural: ASSIGN #SVE-PPCN-NBR-PYE := #TR-PPCN-NBR-PYE
            //*  END OF ADD CODE TO PREVENT DUPLICATE KEY ERRORS 4/00
            //*  CREATE A TRANSACTION TYPE RECORD
            iaa_Trans_Rcrd_Lst_Trans_Dte.setValue(pnd_Wrk_Timx);                                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE := #WRK-TIMX
            iaa_Trans_Rcrd_Trans_Dte.setValue(pnd_Wrk_Timx);
            pnd_Trans_Dte.setValue(pnd_Wrk_Timx);
            pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(iaa_Trans_Rcrd_Lst_Trans_Dte));                 //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - IAA-TRANS-RCRD.LST-TRANS-DTE
            iaa_Trans_Rcrd_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #TR-PPCN-NBR
            iaa_Trans_Rcrd_Trans_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #TR-PAYEE-CDE
            iaa_Trans_Rcrd_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Trans_Rcrd_Trans_Check_Dte), pnd_Check_A.val());                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Trans_Rcrd_Trans_User_Area.setValue("BATCH");                                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
            iaa_Trans_Rcrd_Trans_User_Id.setValue("IAAP342");                                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP342'
            iaa_Trans_Rcrd_Trans_Actvty_Cde.setValue("A");                                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
            iaa_Trans_Rcrd_Trans_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code);                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := #TR-TRAN-CODE
            iaa_Trans_Rcrd_Trans_Todays_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO IAA-TRANS-RCRD.TRANS-TODAYS-DTE-A
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(900)))                                                                                                  //Natural: IF #TR-TRAN-CODE = 900
            {
                pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ded_Code);                                                                           //Natural: ASSIGN #W-DED-CODE := #TR-DED-CODE
                pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr);                                                                     //Natural: ASSIGN #W-DED-SEQ-NBR := #TR-DED-SEQ-NBR
                iaa_Trans_Rcrd_Trans_Cmbne_Cde.setValue(pnd_W_Ded_Cde_Seq_Nbr);                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #W-DED-CDE-SEQ-NBR
            }                                                                                                                                                             //Natural: END-IF
            iaa_Trans_Rcrd_Trans_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code);                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := #TR-TRAN-CODE
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                           //Natural: ASSIGN #CNTRCT-PPCN-NBR := #TR-PPCN-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                         //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #TR-PAYEE-CDE
            short decideConditionsMet749 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #TR-TRAN-CODE;//Natural: VALUE 006
            if (condition((pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(6))))
            {
                decideConditionsMet749++;
                //*  ADDED FOLLOWING                2/97          /* LOGIC TO HANDLE
                //*  TWO 006 TRANS
                if (condition(pnd_Save_Payee_Key.equals(pnd_Cntrct_Payee_Key)))                                                                                           //Natural: IF #SAVE-PAYEE-KEY = #CNTRCT-PAYEE-KEY
                {
                    //*  FOR MUTLI FUNDED
                                                                                                                                                                          //Natural: PERFORM PROCESS-INACTIVE-FUND-RECORD
                    sub_Process_Inactive_Fund_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  CONTRACTS WRITE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ONLY 1 TRANS RECORD
                                                                                                                                                                          //Natural: PERFORM PROCESS-006-700-TRANS-UPDT-CPR
                    sub_Process_006_700_Trans_Updt_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-TRANS
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE
                    sub_Print_Detail_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Save_Payee_Key.setValue(pnd_Cntrct_Payee_Key);                                                                                                    //Natural: ASSIGN #SAVE-PAYEE-KEY := #CNTRCT-PAYEE-KEY
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED FOLLOWING NEW 102 IPRO TRANS PEND IPRO CONTRACT 9/01
            }                                                                                                                                                             //Natural: VALUE 102
            else if (condition((pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(102))))
            {
                decideConditionsMet749++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-006-700-TRANS-UPDT-CPR
                sub_Process_006_700_Trans_Updt_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-TRANS
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE
                sub_Print_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 700
            else if (condition((pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(700))))
            {
                decideConditionsMet749++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-006-700-TRANS-UPDT-CPR
                sub_Process_006_700_Trans_Updt_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  KEY TO FIND DED RECORD
                pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-TRANS
            }                                                                                                                                                             //Natural: VALUE 900
            else if (condition((pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(900))))
            {
                decideConditionsMet749++;
                pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                           //Natural: ASSIGN #DDCTN-PPCN-NBR := #TR-PPCN-NBR
                pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                         //Natural: ASSIGN #DDCTN-PAYEE-CDE := #TR-PAYEE-CDE
                pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr);                                                                         //Natural: ASSIGN #DDCTN-SEQ-NBR := #TR-DED-SEQ-NBR
                pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ded_Code);                                                                                //Natural: ASSIGN #DDCTN-CDE := #TR-DED-CODE
                pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-TRANS
                //*  IAA-TRANS FOR 900 TRANS
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
                //*  BEFORE IMAGE
                vw_iaa_Deduction.startDatabaseFind                                                                                                                        //Natural: FIND ( 1 ) IAA-DEDUCTION WITH CNTRCT-PAYEE-DDCTN-KEY = #IAA-DDCTN-KEY
                (
                "F2",
                new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", "=", pnd_Iaa_Ddctn_Key, WcType.WITH) },
                1
                );
                F2:
                while (condition(vw_iaa_Deduction.readNextRow("F2")))
                {
                    vw_iaa_Deduction.setIfNotFoundControlFlag(false);
                    vw_iaa_Ddctn_Trans.setValuesByName(vw_iaa_Deduction);                                                                                                 //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
                    iaa_Ddctn_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                            //Natural: ASSIGN IAA-DDCTN-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
                    iaa_Ddctn_Trans_Bfre_Imge_Id.setValue("1");                                                                                                           //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID := '1'
                    iaa_Ddctn_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                           //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID := ' '
                    iaa_Ddctn_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Ddctn_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
                    iaa_Ddctn_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE := #TRANS-DTE
                    vw_iaa_Ddctn_Trans.insertDBRow();                                                                                                                     //Natural: STORE IAA-DDCTN-TRANS
                    pnd_Sve_Old_Per_Ded_Amt.setValue(iaa_Deduction_Ddctn_Per_Amt);                                                                                        //Natural: ASSIGN #SVE-OLD-PER-DED-AMT := IAA-DEDUCTION.DDCTN-PER-AMT
                    //*  2 = CHANGE 3 = STOP
                    if (condition(pnd_Tran_Rec_In_Pnd_Tr_Int_Code.equals(3)))                                                                                             //Natural: IF #TR-INT-CODE = 3
                    {
                        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.nadd(iaa_Deduction_Ddctn_Per_Amt);                                                                              //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #CNTRL-DDCTN-AMT
                        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTRL-DDCTN-CNT
                        iaa_Deduction_Ddctn_Per_Amt.setValue(0);                                                                                                          //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PER-AMT := 0
                        iaa_Deduction_Ddctn_Stp_Dte.setValue(pnd_Tran_Rec_In_Pnd_Tr_Chk_Dte_Processed);                                                                   //Natural: ASSIGN IAA-DEDUCTION.DDCTN-STP-DTE := #TR-CHK-DTE-PROCESSED
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  OLD AMT
                        //*  NEW AMT
                        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.nadd(iaa_Deduction_Ddctn_Per_Amt);                                                                              //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #CNTRL-DDCTN-AMT
                        iaa_Deduction_Ddctn_Per_Amt.setValue(pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt);                                                                         //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PER-AMT := #TR-PER-DED-AMT
                        //*  NEWAMT
                        pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt.nadd(iaa_Deduction_Ddctn_Per_Amt);                                                                           //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #W-NEW-DED-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    iaa_Deduction_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                  //Natural: ASSIGN IAA-DEDUCTION.LST-TRANS-DTE := #TRANS-DTE
                    vw_iaa_Deduction.updateDBRow("F2");                                                                                                                   //Natural: UPDATE ( F2. )
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE
                    sub_Print_Detail_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  AFTER  IMAGE DEDUCTION TRANS
                    vw_iaa_Ddctn_Trans.setValuesByName(vw_iaa_Deduction);                                                                                                 //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
                    iaa_Ddctn_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                            //Natural: ASSIGN IAA-DDCTN-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
                    iaa_Ddctn_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                           //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID := ' '
                    iaa_Ddctn_Trans_Aftr_Imge_Id.setValue("2");                                                                                                           //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID := '2'
                    iaa_Ddctn_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Ddctn_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
                    iaa_Ddctn_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE := #TRANS-DTE
                    vw_iaa_Ddctn_Trans.insertDBRow();                                                                                                                     //Natural: STORE IAA-DDCTN-TRANS
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-LINE
        sub_Print_Total_Line();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-RCRD-CHANGES
        sub_Print_Control_Rcrd_Changes();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-006-700-TRANS-UPDT-CPR
        //*   UPDATE CPR MASTER RECORD
        //*  -----------------------------------------------------
        //*   USED FOR 006 TRANS ACCUM FUND RECORD AMTS
        //*  1)  PROCESS TERMINATED PAYEE ACCUM CNTRL-TOTALS
        //*      FOR REPORTING & UPDATE OF CNTRL-RECORD
        //*  2) SEARCH CNTRL-TABLE FOR FUND CODE MATCH
        //*      TO ACCUM AMTS BY FUND FOR CREF & REA FUNDS
        //*  ------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INACTIVE-FUND-RECORD
        //*  ---------------------------------------------------------------
        //*   PROCESS TIAA FUND RECORDS
        //*   WRITE B4 & AFTER IMAGES TIAA FUND RECORDS FOR INACTIVE CPR REC
        //*   006 TRANSACTIONS TO TERMINATE A CPR RECORD
        //*  ---------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-FUND-RCRD-TRANS
        //*  ---------------------------------------------------------------
        //*   PROCESS CREF FUND RECORDS
        //*   WRITE B4 & AFTER IMAGES CREF FUND RECORDS FOR INACTIVE CPR REC
        //*   006 TRANSACTIONS TO TERMINATE A CPR RECORD
        //*  ---------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-FUND-RCRD-TRANS
        //*  ---------------------------------------------------
        //*   GET CNTRACT INFO WRITE B4 & AFTER IMAGE TRANS
        //*       FOR TERMINATED CPR RECORD
        //*  ---------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IAA-CNTRCT-TRANS-RCRD
        //*  WRITE AFTER IMAGE
        //*  END OF ADD   6/97
        //*  *******************************************************
        //*  INTIALIZE CREF ACCUM CHANGES FROM TRANS
        //*  *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-CREF-ACCUM-TABLE
        //*  ADDED FOLLOWING FOR PA SELECT FUNDS  4/00
        //*  -----------------------------------------------------------
        //*  CALL IAAN051A PASS 2 BYTE FUND CDE & LENGTH OF DESC NEEDED
        //*  PASSES BACK FUND NAME
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN051A
        //*  -------------------------------------------------
        //*  SET UP PROPER HEADINGS FOR TRANSACTION REPORT
        //*  -------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-HEADING-LINES
        //*  ------------------------------------
        //*  PRINT DETAIL LINES
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINE
        //*  ------------------------------------
        //*  PRINT TOTAL FOR TRANS & NEWPAGE
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTAL-LINE
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-RCRD-CHANGES
        //*  3X 'DEDUCTION CHANGED AMOUNT-->'
        //*  1X#W-NEW-DED-AMT(EM=ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99+)
        //*  ------------------------------------------------------
        //*  READ DED FOR CPR CNTRACTS TO BE TERMINATED FOR ANY
        //*  ACTIVE DEDUCTIONS & BACK ANY AMTS FROM CONTROL TOTALS
        //*  ------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-DEDUCTIONS
        //*  --------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-PROCESSING
    }
    private void sub_Process_006_700_Trans_Updt_Cpr() throws Exception                                                                                                    //Natural: PROCESS-006-700-TRANS-UPDT-CPR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------
        //*   PROCESS TIAA-CNTRCT PART ROLE RCRD
        //*   THIS ROUTINE DONE FOR 006 & 700 TRANSACTIONS
        //*   UPDATE CPR RECORD FOR TRANS 006 & 700
        //*   FOR 006 TRANS CREATE B4 & AFTER IMAGE FOR CNTRCT & FUND RECORDS
        //*  ----------------------------------------------------------------
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                      //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("F1", true)))
        {
            vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct_Prtcpnt_Role.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORDS FOUND
            {
                getReports().write(1, ReportOption.NOTITLE,"**** NO IAA-CPR RECORD FOUND FOR THIS PPCN-NBR-->",pnd_Cntrct_Payee_Key, new ReportEditMask                   //Natural: WRITE ( 1 ) '**** NO IAA-CPR RECORD FOUND FOR THIS PPCN-NBR-->' #CNTRCT-PAYEE-KEY ( EM = XXXXXXXXXX-XX ) 'TRANSACTION NOT APPLIED FOR THIS CONTRACT *******'
                    ("XXXXXXXXXX-XX"),"TRANSACTION NOT APPLIED FOR THIS CONTRACT *******");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            //*  STORE TRANS REC FOR 006 & 700 TRANS
            vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                              //Natural: STORE IAA-TRANS-RCRD
            //*  CREATE BEFORE IMAGE CONTRACT-PARTICIPANT-ROLE HISTORY RECORD
            vw_iaa_Cpr_Trans.setValuesByName(vw_iaa_Cntrct_Prtcpnt_Role);                                                                                                 //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
            iaa_Cpr_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                      //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Cpr_Trans_Bfre_Imge_Id.setValue("1");                                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := '1'
            iaa_Cpr_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := ' '
            iaa_Cpr_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cpr_Trans_Trans_Check_Dte), pnd_Check_A.val());                                        //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cpr_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                              //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := #TRANS-DTE
            //*  STORE BEFORE IMAGE CPR TRANS
            vw_iaa_Cpr_Trans.insertDBRow();                                                                                                                               //Natural: STORE IAA-CPR-TRANS
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(700)))                                                                                                  //Natural: IF #TR-TRAN-CODE = 700
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO 5
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).notEquals(" ")))                                                              //Natural: IF #TR-CNTRCT-COMPANY-CDE ( #I ) NE ' '
                    {
                        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(pnd_I).setValue(pnd_Tran_Rec_In_Pnd_Tr_Rcvry_Type.getValue(pnd_I));                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( #I ) := #TR-RCVRY-TYPE ( #I )
                        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(pnd_I).setValue(pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Free.getValue(pnd_I));                         //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( #I ) := #TR-PER-TAX-FREE ( #I )
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE
                        sub_Print_Detail_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED FOLOWING FOR IPRO 102 PEND-TRANS   9/01
            //*  ADDED 9/01
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(102)))                                                                                                  //Natural: IF #TR-TRAN-CODE = 102
            {
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Pend_Cde);                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE := #TR-PEND-CDE
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte.setValue(pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte);                                                                     //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-DTE := #TR-FIN-PER-DTE
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte.setValue(pnd_Check_A_Pnd_Check_Ccyymm);                                                                      //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-LST-CHNGE-DTE := #CHECK-CCYYMM
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 9/01
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(6)))                                                                                                    //Natural: IF #TR-TRAN-CODE = 006
            {
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(9);                                                                                                    //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 9
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte.setValue(pnd_Check_A_Pnd_Check_Ccyymm);                                                                      //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-LST-CHNGE-DTE := #CHECK-CCYYMM
                pnd_Cntrl_Wrk_Pnd_Cntrl_Inactve_Payees.nadd(1);                                                                                                           //Natural: ADD 1 TO #CNTRL-INACTVE-PAYEES
            }                                                                                                                                                             //Natural: END-IF
            iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE := #TRANS-DTE
            vw_iaa_Cntrct_Prtcpnt_Role.updateDBRow("F1");                                                                                                                 //Natural: UPDATE ( F1. )
            //*  AFTER  IMAGE  CPR TRANS
            vw_iaa_Cpr_Trans.setValuesByName(vw_iaa_Cntrct_Prtcpnt_Role);                                                                                                 //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
            iaa_Cpr_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                      //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Cpr_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cpr_Trans_Trans_Check_Dte), pnd_Check_A.val());                                        //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cpr_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                              //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := #TRANS-DTE
            iaa_Cpr_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := ' '
            iaa_Cpr_Trans_Aftr_Imge_Id.setValue("2");                                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := '2'
            //*  STORE CPR TRANS AFTER-IMAGE
            vw_iaa_Cpr_Trans.insertDBRow();                                                                                                                               //Natural: STORE IAA-CPR-TRANS
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(6)))                                                                                                    //Natural: IF #TR-TRAN-CODE = 006
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-INACTIVE-FUND-RECORD
                sub_Process_Inactive_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-IAA-CNTRCT-TRANS-RCRD
                sub_Write_Iaa_Cntrct_Trans_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-IAA-DEDUCTIONS
                sub_Read_Iaa_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(DbsUtil.maskMatches(pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde.getValue(1),"'T'") || DbsUtil.maskMatches(pnd_Tran_Rec_In_Pnd_Tr_Cntrct_Company_Cde.getValue(1), //Natural: IF #TR-CNTRCT-COMPANY-CDE ( 1 ) = MASK ( 'T' ) OR = MASK ( 'R' )
                    "'R'")))
                {
                    //*  TIAA STANDARD & REA
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys.nadd(1);                                                                                                       //Natural: ADD 1 TO #CNTRL-ACTVE-TIAA-PYS
                    //*  ACCUM FOR INACTIVE PAYEE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys.nadd(1);                                                                                                       //Natural: ADD 1 TO #CNTRL-ACTVE-CREF-PYS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Process_Inactive_Fund_Record() throws Exception                                                                                                      //Natural: PROCESS-INACTIVE-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(! (DbsUtil.maskMatches(pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde,"'T'"))))                                                                                 //Natural: IF #TR-CMP-FND-CDE NE MASK ( 'T' )
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 40
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
            {
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde.equals(pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde.getValue(pnd_I))))                                               //Natural: IF #TR-CMP-FND-CDE = #CNTRL-FUND-CDE ( #I )
                {
                    //*  ACCUM INACTIVE
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees.getValue(pnd_I).nadd(1);                                                                                          //Natural: ADD 1 TO #CNTRL-FUND-PAYEES ( #I )
                    //*  CREF FUND RECORD
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Units.getValue(pnd_I).nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Units);                                                                 //Natural: ADD #TR-TOT-UNITS TO #CNTRL-UNITS ( #I )
                    //*  COUNTS
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt.nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Units);                                                                             //Natural: ADD #TR-TOT-UNITS TO #CNTRL-UNITS-CNT
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I).nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Pmt);                                                                 //Natural: ADD #TR-TOT-PER-PMT TO #CNTRL-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-FUND-RCRD-TRANS
            sub_Write_Cref_Fund_Rcrd_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde,"'T'")))                                                                                     //Natural: IF #TR-CMP-FND-CDE = MASK ( 'T' )
        {
            //*  TIAA STANDARD FUND RECORDS
            pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNTRL-TIAA-PAYEES
            pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt.nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Pmt);                                                                                 //Natural: ADD #TR-TOT-PER-PMT TO #CNTRL-PER-PAY-AMT
            pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt.nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Per_Div);                                                                                 //Natural: ADD #TR-TOT-PER-DIV TO #CNTRL-PER-DIV-AMT
            pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt.nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Pmt);                                                                               //Natural: ADD #TR-TOT-FIN-PMT TO #CNTRL-FINAL-PAY-AMT
            pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt.nadd(pnd_Tran_Rec_In_Pnd_Tr_Tot_Fin_Div);                                                                               //Natural: ADD #TR-TOT-FIN-DIV TO #CNTRL-FINAL-DIV-AMT
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-FUND-RCRD-TRANS
            sub_Write_Tiaa_Fund_Rcrd_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Tiaa_Fund_Rcrd_Trans() throws Exception                                                                                                        //Natural: WRITE-TIAA-FUND-RCRD-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                                    //Natural: ASSIGN #FUND-PPCN-NBR := #TR-PPCN-NBR
        pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                                  //Natural: ASSIGN #FUND-PAYEE-CDE := #TR-PAYEE-CDE
        pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde);                                                                           //Natural: ASSIGN #FUND-CMPNY-FUND-CDE := #TR-CMP-FND-CDE
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-RCRD-KEY
        (
        "READ02",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ02")))
        {
            if (condition(pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr) || pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde)  //Natural: IF #FUND-PPCN-NBR NE IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR OR #FUND-PAYEE-CDE NE IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE OR #FUND-CMPNY-FUND-CDE NE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE
                || pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("T"))))                                                                                              //Natural: ACCEPT IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE = 'T'
            {
                continue;
            }
            //*  BEFORE IMAGE
            vw_iaa_Tiaa_Fund_Trans.setValuesByName(vw_iaa_Tiaa_Fund_Rcrd);                                                                                                //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
            iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.setValue(iaa_Cpr_Trans_Invrse_Trans_Dte);                                                                                //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := IAA-CPR-TRANS.INVRSE-TRANS-DTE
            iaa_Tiaa_Fund_Trans_Bfre_Imge_Id.setValue("1");                                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := '1'
            iaa_Tiaa_Fund_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := ' '
            iaa_Tiaa_Fund_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Tiaa_Fund_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Tiaa_Fund_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Tiaa_Fund_Trans.insertDBRow();                                                                                                                         //Natural: STORE IAA-TIAA-FUND-TRANS
            //*  AFTER  IMAGE
            vw_iaa_Tiaa_Fund_Trans.setValuesByName(vw_iaa_Tiaa_Fund_Rcrd);                                                                                                //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
            iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte.setValue(iaa_Cpr_Trans_Invrse_Trans_Dte);                                                                                //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := IAA-CPR-TRANS.INVRSE-TRANS-DTE
            iaa_Tiaa_Fund_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := ' '
            iaa_Tiaa_Fund_Trans_Aftr_Imge_Id.setValue("2");                                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := '2'
            iaa_Tiaa_Fund_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Tiaa_Fund_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Tiaa_Fund_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Tiaa_Fund_Trans.insertDBRow();                                                                                                                         //Natural: STORE IAA-TIAA-FUND-TRANS
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Cref_Fund_Rcrd_Trans() throws Exception                                                                                                        //Natural: WRITE-CREF-FUND-RCRD-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                                    //Natural: ASSIGN #FUND-PPCN-NBR := #TR-PPCN-NBR
        pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                                  //Natural: ASSIGN #FUND-PAYEE-CDE := #TR-PAYEE-CDE
        pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Cmp_Fnd_Cde);                                                                           //Natural: ASSIGN #FUND-CMPNY-FUND-CDE := #TR-CMP-FND-CDE
        vw_iaa_Cref_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) IAA-CREF-FUND-RCRD BY CREF-CNTRCT-FUND-KEY STARTING FROM #FUND-RCRD-KEY
        (
        "READ03",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Cref_Fund_Rcrd.readNextRow("READ03")))
        {
            if (condition(pnd_Fund_Rcrd_Key_Pnd_Fund_Ppcn_Nbr.notEquals(iaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr) || pnd_Fund_Rcrd_Key_Pnd_Fund_Payee_Cde.notEquals(iaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde)  //Natural: IF #FUND-PPCN-NBR NE IAA-CREF-FUND-RCRD.CREF-CNTRCT-PPCN-NBR OR #FUND-PAYEE-CDE NE IAA-CREF-FUND-RCRD.CREF-CNTRCT-PAYEE-CDE OR #FUND-CMPNY-FUND-CDE NE IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE
                || pnd_Fund_Rcrd_Key_Pnd_Fund_Cmpny_Fund_Cde.notEquals(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cref_Fund_Rcrd_Cref_Cmpny_Cde.equals("T")))                                                                                                 //Natural: REJECT IF IAA-CREF-FUND-RCRD.CREF-CMPNY-CDE = 'T'
            {
                continue;
            }
            //*  BEFORE IMAGE
            vw_iaa_Cref_Fund_Trans.setValuesByName(vw_iaa_Cref_Fund_Rcrd);                                                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
            iaa_Cref_Fund_Trans_Invrse_Trans_Dte.setValue(iaa_Cpr_Trans_Invrse_Trans_Dte);                                                                                //Natural: ASSIGN IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE := IAA-CPR-TRANS.INVRSE-TRANS-DTE
            iaa_Cref_Fund_Trans_Bfre_Imge_Id.setValue("1");                                                                                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID := '1'
            iaa_Cref_Fund_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID := ' '
            iaa_Cref_Fund_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cref_Fund_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cref_Fund_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                              //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Cref_Fund_Trans.insertDBRow();                                                                                                                         //Natural: STORE IAA-CREF-FUND-TRANS
            //*  AFTER  IMAGE
            vw_iaa_Cref_Fund_Trans.setValuesByName(vw_iaa_Cref_Fund_Rcrd);                                                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
            iaa_Cref_Fund_Trans_Invrse_Trans_Dte.setValue(iaa_Cpr_Trans_Invrse_Trans_Dte);                                                                                //Natural: ASSIGN IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE := IAA-CPR-TRANS.INVRSE-TRANS-DTE
            iaa_Cref_Fund_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID := ' '
            iaa_Cref_Fund_Trans_Aftr_Imge_Id.setValue("2");                                                                                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID := '2'
            iaa_Cref_Fund_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cref_Fund_Trans_Trans_Check_Dte), pnd_Check_A.val());                            //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cref_Fund_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                              //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Cref_Fund_Trans.insertDBRow();                                                                                                                         //Natural: STORE IAA-CREF-FUND-TRANS
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Iaa_Cntrct_Trans_Rcrd() throws Exception                                                                                                       //Natural: WRITE-IAA-CNTRCT-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED 6/97 TO PREVENT
        if (condition(pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr.equals(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr)))                                                             //Natural: IF #SAVE-PPCN-NBR = #CNTRCT-PPCN-NBR
        {
            //*  WRITING DUPLICATE RECORDS
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                               //Natural: ASSIGN #CNTRCT-PPCN-NBR := #TR-PPCN-NBR
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(1, ReportOption.NOTITLE,"NO IAA-CNTRCT RECORD FOUND FOR THIS PPCN-NBR-->",pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);                   //Natural: WRITE ( 1 ) 'NO IAA-CNTRCT RECORD FOUND FOR THIS PPCN-NBR-->' #CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            //*  WRITE B4 IMAGE
            vw_iaa_Cntrct_Trans.setValuesByName(vw_iaa_Cntrct);                                                                                                           //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
            iaa_Cntrct_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                   //Natural: ASSIGN IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Cntrct_Trans_Bfre_Imge_Id.setValue("1");                                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := '1'
            iaa_Cntrct_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := ' '
            iaa_Cntrct_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Trans_Trans_Check_Dte), pnd_Check_A.val());                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cntrct_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Cntrct_Trans.insertDBRow();                                                                                                                            //Natural: STORE IAA-CNTRCT-TRANS
            iaa_Cntrct_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                   //Natural: ASSIGN IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Cntrct_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := ' '
            iaa_Cntrct_Trans_Aftr_Imge_Id.setValue("2");                                                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := '2'
            iaa_Cntrct_Trans_Trans_Check_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Trans_Trans_Check_Dte), pnd_Check_A.val());                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE := VAL ( #CHECK-A )
            iaa_Cntrct_Trans_Trans_Dte.setValue(iaa_Cpr_Trans_Trans_Dte);                                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE := IAA-CPR-TRANS.TRANS-DTE
            vw_iaa_Cntrct_Trans.insertDBRow();                                                                                                                            //Natural: STORE IAA-CNTRCT-TRANS
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Initialize_Cref_Accum_Table() throws Exception                                                                                                       //Natural: INITIALIZE-CREF-ACCUM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_I2.setValue(21);                                                                                                                                              //Natural: ASSIGN #I2 := 21
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  ANNUAL
            //*  MONTHLY
            //*  ANNUAL
            //*  MONTHLY
            if (condition(pnd_I.add(1).equals(9) || pnd_I.add(1).equals(11)))                                                                                             //Natural: IF #I + 1 = 9 OR = 11
            {
                pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I).setValue("U");                                                                                            //Natural: ASSIGN #CNTRL-CMP-CDE ( #I ) := 'U'
                pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I2).setValue("W");                                                                                           //Natural: ASSIGN #CNTRL-CMP-CDE ( #I2 ) := 'W'
                pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I).setValue("TIAA ");                                                                                   //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I ) := 'TIAA '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I).setValue("TIAA ");                                                                                   //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I ) := 'TIAA '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I2).setValue("TIAA ");                                                                                  //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I2 ) := 'TIAA '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I2).setValue("TIAA ");                                                                                  //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I2 ) := 'TIAA '
                //*  ANNUAL
                //*  MONTHLY UNIT VAL FOR CREF PMT
                //*  ANNUAL
                //*  MONTHLY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I).setValue("2");                                                                                            //Natural: ASSIGN #CNTRL-CMP-CDE ( #I ) := '2'
                pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I2).setValue("4");                                                                                           //Natural: ASSIGN #CNTRL-CMP-CDE ( #I2 ) := '4'
                pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I).setValue("CREF ");                                                                                   //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I ) := 'CREF '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I).setValue("CREF ");                                                                                   //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I ) := 'CREF '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I2).setValue("CREF ");                                                                                  //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I2 ) := 'CREF '
                pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I2).setValue("CREF ");                                                                                  //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I2 ) := 'CREF '
                //*  CREF FUNDS START AT 2
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I)),        //Natural: ASSIGN #CNTRL-FUND-CDE-2-REDF ( #I ) := #I + 1
                pnd_I.add(1));
            pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I2).setValue(pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I));                                   //Natural: ASSIGN #CNTRL-FUND-CDE-2-REDF ( #I2 ) := #CNTRL-FUND-CDE-2-REDF ( #I )
            //*  ADDED FOLLOWING GET FUND DESC FROM EXTERNALIZATION FILE
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN051A
            sub_Call_Iaan051a();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc.getValue(pnd_I).setValue(pnd_Parm_Desc);                                                                                //Natural: ASSIGN #CNTRL-REC-FUND-DESC ( #I ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt.getValue(pnd_I).setValue("ANNUAL FUND RECORDS");                                                                         //Natural: ASSIGN #CNTRL-REC-FUND-TXT ( #I ) := 'ANNUAL FUND RECORDS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc.getValue(pnd_I).setValue(pnd_Parm_Desc);                                                                                //Natural: ASSIGN #CNTRL-UNT-FUND-DESC ( #I ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I).setValue("ANNUAL UNITS");                                                                                //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I ) := 'ANNUAL UNITS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc.getValue(pnd_I2).setValue(pnd_Parm_Desc);                                                                               //Natural: ASSIGN #CNTRL-REC-FUND-DESC ( #I2 ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt.getValue(pnd_I2).setValue("MONTHLY FUND RECORDS");                                                                       //Natural: ASSIGN #CNTRL-REC-FUND-TXT ( #I2 ) := 'MONTHLY FUND RECORDS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc.getValue(pnd_I2).setValue(pnd_Parm_Desc);                                                                               //Natural: ASSIGN #CNTRL-UNT-FUND-DESC ( #I2 ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I2).setValue("MONTHLY UNITS");                                                                              //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I2 ) := 'MONTHLY UNITS'
            pnd_I2.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I2
            //*  ANNUAL
            //*  MONTHLY
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_I2.setValue(61);                                                                                                                                              //Natural: ASSIGN #I2 := 61
        FOR04:                                                                                                                                                            //Natural: FOR #I = 41 TO 60
        for (pnd_I.setValue(41); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I).setValue("U");                                                                                                //Natural: ASSIGN #CNTRL-CMP-CDE ( #I ) := 'U'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Cmp_Cde.getValue(pnd_I2).setValue("W");                                                                                               //Natural: ASSIGN #CNTRL-CMP-CDE ( #I2 ) := 'W'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I).setValue(pnd_I);                                                                                      //Natural: ASSIGN #CNTRL-FUND-CDE-2-REDF ( #I ) := #I
            pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I2).setValue(pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Cde_2_Redf.getValue(pnd_I));                                   //Natural: ASSIGN #CNTRL-FUND-CDE-2-REDF ( #I2 ) := #CNTRL-FUND-CDE-2-REDF ( #I )
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN051A
            sub_Call_Iaan051a();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I).setValue(pnd_Cmpny_Desc);                                                                                //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I ) := #CMPNY-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc.getValue(pnd_I).setValue(pnd_Parm_Desc);                                                                                //Natural: ASSIGN #CNTRL-REC-FUND-DESC ( #I ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt.getValue(pnd_I).setValue("ANNUAL FUND RECORDS");                                                                         //Natural: ASSIGN #CNTRL-REC-FUND-TXT ( #I ) := 'ANNUAL FUND RECORDS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I).setValue(pnd_Cmpny_Desc);                                                                                //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I ) := #CMPNY-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc.getValue(pnd_I).setValue(pnd_Parm_Desc);                                                                                //Natural: ASSIGN #CNTRL-UNT-FUND-DESC ( #I ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I).setValue("ANNUAL UNITS");                                                                                //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I ) := 'ANNUAL UNITS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Constant.getValue(pnd_I2).setValue(pnd_Cmpny_Desc);                                                                               //Natural: ASSIGN #CNTRL-REC-CONSTANT ( #I2 ) := #CMPNY-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc.getValue(pnd_I2).setValue(pnd_Parm_Desc);                                                                               //Natural: ASSIGN #CNTRL-REC-FUND-DESC ( #I2 ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Txt.getValue(pnd_I2).setValue("MONTHLY FUND RECORDS");                                                                       //Natural: ASSIGN #CNTRL-REC-FUND-TXT ( #I2 ) := 'MONTHLY FUND RECORDS'
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Constant.getValue(pnd_I2).setValue(pnd_Cmpny_Desc);                                                                               //Natural: ASSIGN #CNTRL-UNT-CONSTANT ( #I2 ) := #CMPNY-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Desc.getValue(pnd_I2).setValue(pnd_Parm_Desc);                                                                               //Natural: ASSIGN #CNTRL-UNT-FUND-DESC ( #I2 ) := #PARM-DESC
            pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I2).setValue("MONTHLY UNITS");                                                                              //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I2 ) := 'MONTHLY UNITS'
            pnd_I2.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I2
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  END OF ADD FOR PA SELECT FUNDS  4/00
    }
    private void sub_Call_Iaan051a() throws Exception                                                                                                                     //Natural: CALL-IAAN051A
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Desc.reset();                                                                                                                                            //Natural: RESET #PARM-DESC
        //* #CNTRL-FUND-CDE-2-REDF (#I)
        if (condition(pnd_I.less(41)))                                                                                                                                    //Natural: IF #I LT 41
        {
            pnd_Parm_Fund_2.compute(new ComputeParameters(false, pnd_Parm_Fund_2), pnd_I.add(1));                                                                         //Natural: ASSIGN #PARM-FUND-2 := #I + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Parm_Fund_2.setValue(pnd_I);                                                                                                                              //Natural: ASSIGN #PARM-FUND-2 := #I
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Parm_Len.setValue(6);                                                                                                                                         //Natural: ASSIGN #PARM-LEN := 6
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                         //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
        if (condition(Global.isEscape())) return;
    }
    private void sub_Set_Heading_Lines() throws Exception                                                                                                                 //Natural: SET-HEADING-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED FOLLOWING FOR IPRO 9/01
        if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(102)))                                                                                                      //Natural: IF #TR-TRAN-CODE = 102
        {
            pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend);                                                                 //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-102-PEND
            pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_102);                                                                      //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-102
            pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_102);                                                                     //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-102
            pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_102);                                                                     //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-102
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 9/01
        if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(6)))                                                                                                        //Natural: IF #TR-TRAN-CODE = 006
        {
            pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Term);                                                                     //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term);                                                                     //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term);                                                                    //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term);                                                                    //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-TERM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(700)))                                                                                                  //Natural: IF #TR-TRAN-CODE = 700
            {
                pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax);                                                                  //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax);                                                                  //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax);                                                                 //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax);                                                                 //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-TAX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(900) && pnd_Tran_Rec_In_Pnd_Tr_Int_Code.equals(2)))                                                 //Natural: IF #TR-TRAN-CODE = 900 AND #TR-INT-CODE = 2
                {
                    pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng);                                                         //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-CHNG
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded);                                                              //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-DED
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded);                                                             //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-DED
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded);                                                             //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-DED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(900) && pnd_Tran_Rec_In_Pnd_Tr_Int_Code.equals(3)))                                             //Natural: IF #TR-TRAN-CODE = 900 AND #TR-INT-CODE = 3
                    {
                        pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop);                                                     //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-STOP
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded);                                                          //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-DED
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded);                                                         //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-DED
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded);                                                         //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-DED
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_Line() throws Exception                                                                                                                 //Natural: PRINT-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED FOLLOWING FOR IPRO 102 PEND-CODE Q TRANS 9/01
        if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(102)))                                                                                                      //Natural: IF #TR-TRAN-CODE = 102
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 4X #TR-XREF 4X #TR-PEND-CDE 9X #TR-FIN-PER-DTE ( EM = 9999/99 )
                new ReportEditMask ("99"),new ColumnSpacing(4),pnd_Tran_Rec_In_Pnd_Tr_Xref,new ColumnSpacing(4),pnd_Tran_Rec_In_Pnd_Tr_Pend_Cde,new ColumnSpacing(9),pnd_Tran_Rec_In_Pnd_Tr_Fin_Per_Dte, 
                new ReportEditMask ("9999/99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 102 PEND-CODE Q TRANS 9/01
        if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(6)))                                                                                                        //Natural: IF #TR-TRAN-CODE = 006
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 4X #TR-XREF
                new ReportEditMask ("99"),new ColumnSpacing(4),pnd_Tran_Rec_In_Pnd_Tr_Xref);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(700)))                                                                                                  //Natural: IF #TR-TRAN-CODE = 700
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 5X #TR-XREF 3X#TR-RCVRY-TYPE ( #I ) 3X#TR-PER-TAX-AMT ( #I ) 4X#TR-PER-TAX-FREE ( #I )
                    new ReportEditMask ("99"),new ColumnSpacing(5),pnd_Tran_Rec_In_Pnd_Tr_Xref,new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Rcvry_Type.getValue(pnd_I),new 
                    ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Amt.getValue(pnd_I),new ColumnSpacing(4),pnd_Tran_Rec_In_Pnd_Tr_Per_Tax_Free.getValue(pnd_I));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tran_Rec_In_Pnd_Tr_Tran_Code.equals(900)))                                                                                              //Natural: IF #TR-TRAN-CODE = 900
                {
                    pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng);                                                         //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-CHNG
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 5X#TR-XREF 1X#TR-PER-DED-AMT ( EM = ZZ,ZZZ.99- ) 2X#TR-DED-SEQ-NBR 4X#TR-DED-CODE 2X#SVE-OLD-PER-DED-AMT ( EM = ZZ,ZZZ.99- )
                        new ReportEditMask ("99"),new ColumnSpacing(5),pnd_Tran_Rec_In_Pnd_Tr_Xref,new ColumnSpacing(1),pnd_Tran_Rec_In_Pnd_Tr_Per_Ded_Amt, 
                        new ReportEditMask ("ZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Tran_Rec_In_Pnd_Tr_Ded_Seq_Nbr,new ColumnSpacing(4),pnd_Tran_Rec_In_Pnd_Tr_Ded_Code,new 
                        ColumnSpacing(2),pnd_Sve_Old_Per_Ded_Amt, new ReportEditMask ("ZZ,ZZZ.99-"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Total_Line() throws Exception                                                                                                                  //Natural: PRINT-TOTAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED THE FOLLOWING FOR IPRO 9/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(102)))                                                                                                  //Natural: IF #SVE-TRAN-CODE = 102
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL IPRO PEND-CODE(Q) TRANS....:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask                //Natural: WRITE ( 1 ) / 'TOTAL IPRO PEND-CODE(Q) TRANS....:' #TOTAL-TRANS ( EM = ZZZ,ZZ9 )
                ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 9/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(6)))                                                                                                    //Natural: IF #SVE-TRAN-CODE = 006
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TERMINATION TRANSACTIONS APPLIED : ",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask         //Natural: WRITE ( 1 ) / 'TOTAL TERMINATION TRANSACTIONS APPLIED : ' #TOTAL-TRANS ( EM = ZZZ,ZZ9 )
                ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(700)))                                                                                              //Natural: IF #SVE-TRAN-CODE = 700
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"        TOTAL TAX TRANSACTIONS APPLIED : ",pnd_Prog_Misc_Area_Pnd_Total_Trans, new                    //Natural: WRITE ( 1 ) / '        TOTAL TAX TRANSACTIONS APPLIED : ' #TOTAL-TRANS ( EM = ZZZ,ZZ9 )
                    ReportEditMask ("ZZZ,ZZ9"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(900) && pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.equals(2)))                                         //Natural: IF #SVE-TRAN-CODE = 900 AND #SVE-INT-CODE = 2
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"       TOTAL DEDUCTION CHANGES APPLIED : ",pnd_Prog_Misc_Area_Pnd_Total_Trans,                    //Natural: WRITE ( 1 ) / '       TOTAL DEDUCTION CHANGES APPLIED : ' #TOTAL-TRANS ( EM = ZZZ,ZZ9 )
                        new ReportEditMask ("ZZZ,ZZ9"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(900) && pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.equals(3)))                                     //Natural: IF #SVE-TRAN-CODE = 900 AND #SVE-INT-CODE = 3
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"         TOTAL STOP DEDUCTIONS APPLIED : ",pnd_Prog_Misc_Area_Pnd_Total_Trans,                //Natural: WRITE ( 1 ) / '         TOTAL STOP DEDUCTIONS APPLIED : ' #TOTAL-TRANS ( EM = ZZZ,ZZ9 )
                            new ReportEditMask ("ZZZ,ZZ9"));
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prog_Misc_Area_Pnd_Total_Trans.reset();                                                                                                                       //Natural: RESET #TOTAL-TRANS
    }
    //*  CHANGED FROM 1:40 TO 1:80   4/00
    private void sub_Print_Control_Rcrd_Changes() throws Exception                                                                                                        //Natural: PRINT-CONTROL-RCRD-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 80
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees.getValue(pnd_I)),                //Natural: ASSIGN #CNTRL-FUND-PAYEES ( #I ) := #CNTRL-FUND-PAYEES ( #I ) * -1
                pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees.getValue(pnd_I).multiply(-1));
            pnd_Cntrl_Wrk_Pnd_Cntrl_Units.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Units.getValue(pnd_I)), pnd_Cntrl_Wrk_Pnd_Cntrl_Units.getValue(pnd_I).multiply(-1)); //Natural: ASSIGN #CNTRL-UNITS ( #I ) := #CNTRL-UNITS ( #I ) * -1
            pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I)), pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I).multiply(-1)); //Natural: ASSIGN #CNTRL-AMT ( #I ) := #CNTRL-AMT ( #I ) * -1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.nsubtract(pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt);                                                                                //Natural: COMPUTE #CNTRL-DDCTN-AMT = #CNTRL-DDCTN-AMT - #W-NEW-DED-AMT
        pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys), pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys.multiply(-1)); //Natural: ASSIGN #CNTRL-ACTVE-TIAA-PYS := #CNTRL-ACTVE-TIAA-PYS * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys), pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys.multiply(-1)); //Natural: ASSIGN #CNTRL-ACTVE-CREF-PYS := #CNTRL-ACTVE-CREF-PYS * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees), pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees.multiply(-1)); //Natural: ASSIGN #CNTRL-TIAA-PAYEES := #CNTRL-TIAA-PAYEES * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt), pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt.multiply(-1)); //Natural: ASSIGN #CNTRL-PER-PAY-AMT := #CNTRL-PER-PAY-AMT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt), pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt.multiply(-1)); //Natural: ASSIGN #CNTRL-PER-DIV-AMT := #CNTRL-PER-DIV-AMT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt), pnd_Cntrl_Wrk_Pnd_Cntrl_Units_Cnt.multiply(-1));       //Natural: ASSIGN #CNTRL-UNITS-CNT := #CNTRL-UNITS-CNT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt), pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt.multiply(-1)); //Natural: ASSIGN #CNTRL-FINAL-PAY-AMT := #CNTRL-FINAL-PAY-AMT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt), pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt.multiply(-1)); //Natural: ASSIGN #CNTRL-FINAL-DIV-AMT := #CNTRL-FINAL-DIV-AMT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt), pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.multiply(-1));       //Natural: ASSIGN #CNTRL-DDCTN-AMT := #CNTRL-DDCTN-AMT * -1
        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt.compute(new ComputeParameters(false, pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt), pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt.multiply(-1));       //Natural: ASSIGN #CNTRL-DDCTN-CNT := #CNTRL-DDCTN-CNT * -1
        getReports().write(2, ReportOption.NOTITLE,"TIAA TRADTIONAL FUND RECORDS    ",new ColumnSpacing(11),pnd_Cntrl_Wrk_Pnd_Cntrl_Tiaa_Payees, new ReportEditMask       //Natural: WRITE ( 2 ) 'TIAA TRADTIONAL FUND RECORDS    ' 11X #CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9- )
            ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA TRADTIONAL PER PAYMENT     ",new ColumnSpacing(5),pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Pay_Amt, new ReportEditMask        //Natural: WRITE ( 2 ) 'TIAA TRADTIONAL PER PAYMENT     ' 5X #CNTRL-PER-PAY-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99- )
            ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA TRADTIONAL PER DIDVIDEND   ",new ColumnSpacing(5),pnd_Cntrl_Wrk_Pnd_Cntrl_Per_Div_Amt, new ReportEditMask        //Natural: WRITE ( 2 ) 'TIAA TRADTIONAL PER DIDVIDEND   ' 5X #CNTRL-PER-DIV-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99- )
            ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL PAYMENT   ",new ColumnSpacing(5),pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Pay_Amt, new                     //Natural: WRITE ( 2 ) 'TIAA TRADTIONAL FINAL PAYMENT   ' 5X #CNTRL-FINAL-PAY-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL DIDVIDEND ",new ColumnSpacing(5),pnd_Cntrl_Wrk_Pnd_Cntrl_Final_Div_Amt, new                     //Natural: WRITE ( 2 ) 'TIAA TRADTIONAL FINAL DIDVIDEND ' 5X #CNTRL-FINAL-DIV-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Fnd_Cnt.nadd(1);                                                                                                                                              //Natural: ASSIGN #FND-CNT := #FND-CNT + 1
        //*  CHANGED FROM 40 TO 80 4/00
        pnd_I2.reset();                                                                                                                                                   //Natural: RESET #I2
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 80
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(21) || pnd_I.equals(41) || pnd_I.equals(61)))                                                                                      //Natural: IF #I = 21 OR = 41 OR = 61
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Cntrl_Wrk_Pnd_Cntrl_Rec_Fund_Desc.getValue(pnd_I),"UU") || pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I).notEquals(getZero()))) //Natural: IF #CNTRL-REC-FUND-DESC ( #I ) = MASK ( UU ) OR #CNTRL-AMT ( #I ) NE 0
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Rec_Desc_Lne.getValue(pnd_I),new ColumnSpacing(10),pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Payees.getValue(pnd_I),  //Natural: WRITE ( 2 ) / #CNTRL-FUND-REC-DESC-LNE ( #I ) 10X #CNTRL-FUND-PAYEES ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9- )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne.getValue(pnd_I),new ColumnSpacing(4),pnd_Cntrl_Wrk_Pnd_Cntrl_Units.getValue(pnd_I),  //Natural: WRITE ( 2 ) #CNTRL-FUND-UNT-DESC-LNE ( #I ) 4X #CNTRL-UNITS ( #I ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.999- )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADDED FOLLOWING TO PRINT UNIT DOLLAR AMTS      6/97
                if (condition(pnd_I.less(21) || (pnd_I.greater(40) && pnd_I.less(61))))                                                                                   //Natural: IF #I LT 21 OR ( #I GT 40 AND #I LT 61 )
                {
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I).setValue("ANNUAL AMOUNT");                                                                       //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I ) := 'ANNUAL AMOUNT'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrl_Wrk_Pnd_Cntrl_Unt_Fund_Txt.getValue(pnd_I).setValue("MONTHLY AMOUNT");                                                                      //Natural: ASSIGN #CNTRL-UNT-FUND-TXT ( #I ) := 'MONTHLY AMOUNT'
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,pnd_Cntrl_Wrk_Pnd_Cntrl_Fund_Unt_Desc_Lne.getValue(pnd_I),new ColumnSpacing(4),pnd_Cntrl_Wrk_Pnd_Cntrl_Amt.getValue(pnd_I),  //Natural: WRITE ( 2 ) #CNTRL-FUND-UNT-DESC-LNE ( #I ) 4X #CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99- )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"ACTIVE DEDUCTIONS               ",new ColumnSpacing(11),pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt,                    //Natural: WRITE ( 2 ) /'ACTIVE DEDUCTIONS               ' 11X#CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"DEDUCTIONS                      ",new ColumnSpacing(5),pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt, new ReportEditMask          //Natural: WRITE ( 2 ) 'DEDUCTIONS                      ' 5X#CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99- )
            ("ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TIAA ACTIVE PAYEES              ",new ColumnSpacing(11),pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Tiaa_Pys,               //Natural: WRITE ( 2 ) /'TIAA ACTIVE PAYEES              ' 11X#CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF ACTIVE PAYEES              ",new ColumnSpacing(11),pnd_Cntrl_Wrk_Pnd_Cntrl_Actve_Cref_Pys, new                   //Natural: WRITE ( 2 ) 'CREF ACTIVE PAYEES              ' 11X#CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9- )
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"INACTIVE PAYESS                 ",new ColumnSpacing(11),pnd_Cntrl_Wrk_Pnd_Cntrl_Inactve_Payees, new                   //Natural: WRITE ( 2 ) 'INACTIVE PAYESS                 ' 11X#CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9+ )
            ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9+"));
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Deductions() throws Exception                                                                                                               //Natural: READ-IAA-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(pnd_Tran_Rec_In_Pnd_Tr_Ppcn_Nbr);                                                                                   //Natural: ASSIGN #DDCTN-PPCN-NBR := #TR-PPCN-NBR
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde.setValue(pnd_Tran_Rec_In_Pnd_Tr_Payee_Cde);                                                                                 //Natural: ASSIGN #DDCTN-PAYEE-CDE := #TR-PAYEE-CDE
        vw_iaa_Deduction.startDatabaseRead                                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #DDCTN-PPCN-NBR-PAYEE
        (
        "READ04",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Deduction.readNextRow("READ04")))
        {
            if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.greater(pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr)))                                                                    //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR GT #DDCTN-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.equals(pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr) && iaa_Deduction_Ddctn_Payee_Cde.equals(pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde))) //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR = #DDCTN-PPCN-NBR AND IAA-DEDUCTION.DDCTN-PAYEE-CDE = #DDCTN-PAYEE-CDE
                {
                    if (condition(iaa_Deduction_Ddctn_Stp_Dte.equals(getZero())))                                                                                         //Natural: IF IAA-DEDUCTION.DDCTN-STP-DTE = 0
                    {
                        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Amt.nadd(iaa_Deduction_Ddctn_Per_Amt);                                                                              //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #CNTRL-DDCTN-AMT
                        pnd_Cntrl_Wrk_Pnd_Cntrl_Ddctn_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTRL-DDCTN-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Error_Processing() throws Exception                                                                                                                  //Natural: ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"**** ERROR NO HEADER RECORD ON AUTO TRANS FILE ****");                                                                //Natural: WRITE ( 1 ) '**** ERROR NO HEADER RECORD ON AUTO TRANS FILE ****'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"**** BAD AUTO GENERATED TRANSACTION FILE WORK 1****");                                                                //Natural: WRITE ( 1 ) '**** BAD AUTO GENERATED TRANSACTION FILE WORK 1****'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"**** JOB ABORTED FIX ERROR AND RERUN JOB       ****");                                                                //Natural: WRITE ( 1 ) '**** JOB ABORTED FIX ERROR AND RERUN JOB       ****'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"**** RERUN JOB THAT CREATED FILE               ****");                                                                //Natural: WRITE ( 1 ) '**** RERUN JOB THAT CREATED FILE               ****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** ERROR NO HEADER RECORD ON AUTO TRANS FILE ****");                                                                                     //Natural: WRITE '**** ERROR NO HEADER RECORD ON AUTO TRANS FILE ****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** BAD AUTO GENERATED TRANSACTION FILE WORK 1****");                                                                                     //Natural: WRITE '**** BAD AUTO GENERATED TRANSACTION FILE WORK 1****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** JOB ABORTED FIX ERROR AND RERUN JOB       ****");                                                                                     //Natural: WRITE '**** JOB ABORTED FIX ERROR AND RERUN JOB       ****'
        if (Global.isEscape()) return;
        getReports().write(0, "**** RERUN JOB THAT CREATED FILE               ****");                                                                                     //Natural: WRITE '**** RERUN JOB THAT CREATED FILE               ****'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  ADD 1 TO #W-PAGE-CTR  /* 3/06 REPLACE WITH *PAGE-NUMBER(1)
                    //*  TO PREVENT NUMERIC TRUNCATION.
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN AUTO GENERATED TRANSACTIONS APPLIED FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Check_A_Pnd_Check_N,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN AUTO GENERATED TRANSACTIONS APPLIED FOR PAYMENTS DUE ON CHECK DATE: ' #CHECK-N ( EM = 9999/99/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 )
                        new ReportEditMask ("9999/99/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1));
                    //*    4X 'PAGE: ' #W-PAGE-CTR (EM=Z9)
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE,NEWLINE);                                                                //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT //
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc,NEWLINE);                                                           //Natural: WRITE ( 1 ) #TYPE-TRANS-DESC /
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading);                                                                    //Natural: WRITE ( 1 ) #DETAIL-HEADING
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading2);                                                                   //Natural: WRITE ( 1 ) #DETAIL-HEADING2
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading3);                                                                   //Natural: WRITE ( 1 ) #DETAIL-HEADING3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  ADD 1 TO #W-PAGE-CTR
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN FILE CONTROL CHANGES FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Check_A_Pnd_Check_N,  //Natural: WRITE ( 2 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN FILE CONTROL CHANGES FOR PAYMENTS DUE ON CHECK DATE: ' #CHECK-N ( EM = 9999/99/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 2 )
                        new ReportEditMask ("9999/99/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(2));
                    getReports().write(2, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,new ColumnSpacing(13)," FROM AUTO GENERATED TRANSACTIONS",NEWLINE,               //Natural: WRITE ( 2 ) 'RUN DATE' #W-DATE-OUT 13X' FROM AUTO GENERATED TRANSACTIONS'//
                        NEWLINE);
                    getReports().write(2, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl);                                                                  //Natural: WRITE ( 2 ) #DETAIL-HEAD1-CTL
                    getReports().write(2, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl);                                                                  //Natural: WRITE ( 2 ) #DETAIL-HEAD2-CTL
                    getReports().write(2, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl);                                                                  //Natural: WRITE ( 2 ) #DETAIL-HEAD3-CTL
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
        Global.format(2, "LS=132 PS=56");
    }
}
