/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:10 PM
**        * FROM NATURAL PROGRAM : Iaap586s
************************************************************
**        * FILE NAME            : Iaap586s.java
**        * CLASS NAME           : Iaap586s
**        * INSTANCE NAME        : Iaap586s
************************************************************
************************************************************************
* PROGRAM: IAAP586S
* DATE   : 02/26/99
* AUTHOR : ARI G
* DESC   : THIS DRIVER PROGRAM WILL READ OVERPAYMENT FILE, GET CONTRACT
*          PRODUCE PAYMENT TABLES AND WRITE OUT OVERPAYMENT WORK RECORDS
*          IN A WORK FILE TO BE SORTED AND PASSED TO IAAP586R FOR REPORT
* HISTORY:
*
* 03/26/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE OVRPYMNT FILE.*
*                   SC 032610.
* 04/15/14  O SOTTO PASS OVRPYMNT-ID-NBR TO IAAN586A. CHANGES MARKED
*                   041514.
* JUN 2017 J BREMER PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO  RE-STOWED ONLY.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap586s extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_W_Contract;
    private DbsField pnd_W_Payee;
    private DbsField pnd_W_Fund;
    private DbsField pnd_W_Desc;
    private DbsField pnd_W_Mode;
    private DbsField pnd_W_Date;
    private DbsField pnd_W_Guar_Payment;
    private DbsField pnd_W_Divd_Payment;
    private DbsField pnd_W_Unit_Payment;
    private DbsField pnd_W_Guar_Overpayment;
    private DbsField pnd_W_Divd_Overpayment;
    private DbsField pnd_W_Unit_Overpayment;
    private DbsField pnd_W_Msg;
    private DbsField pnd_W_Pin;
    private DbsField pnd_Fund_Lst_Pd_Dte;
    private DbsField pnd_Year;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Numb;

    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;

    private DbsGroup iaa_Parm_Card__R_Field_1;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph;

    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alph__R_Field_2;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num;
    private DbsField pnd_I;
    private DbsField pnd_Tab_Install_Date;
    private DbsField pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_File;
    private DbsField pnd_Tab_Inverse_Date;
    private DbsField pnd_Num_Of_Instlmnts;
    private DbsField pnd_W_Payee_1;
    private DbsField pnd_W_Payee_2;
    private DbsField pnd_Overpay_Years;
    private DbsField pnd_Bottom_Date;
    private DbsField pnd_Ovrpymnt_Reads;
    private DbsField pnd_Ovrpymnt_Time_Selects;

    private DataAccessProgramView vw_iaa_Ovrpymnt;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Ppcn_Nbr;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Payee_Cde;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Id_Nbr;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Status_Timestamp;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Timestamp;

    private DbsGroup iaa_Ovrpymnt_Ovrpymnt_Data;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date;
    private DbsField iaa_Ovrpymnt_Ovrpymnt_Ind;
    private DbsField iaa_Ovrpymnt_Count_Castovrpymnt_Data;
    private DbsField pnd_Datd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_W_Contract = localVariables.newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 10);
        pnd_W_Payee = localVariables.newFieldInRecord("pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 2);
        pnd_W_Fund = localVariables.newFieldInRecord("pnd_W_Fund", "#W-FUND", FieldType.STRING, 3);
        pnd_W_Desc = localVariables.newFieldInRecord("pnd_W_Desc", "#W-DESC", FieldType.STRING, 20);
        pnd_W_Mode = localVariables.newFieldInRecord("pnd_W_Mode", "#W-MODE", FieldType.NUMERIC, 3);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);
        pnd_W_Guar_Payment = localVariables.newFieldInRecord("pnd_W_Guar_Payment", "#W-GUAR-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Divd_Payment = localVariables.newFieldInRecord("pnd_W_Divd_Payment", "#W-DIVD-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Unit_Payment = localVariables.newFieldInRecord("pnd_W_Unit_Payment", "#W-UNIT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Guar_Overpayment = localVariables.newFieldInRecord("pnd_W_Guar_Overpayment", "#W-GUAR-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Divd_Overpayment = localVariables.newFieldInRecord("pnd_W_Divd_Overpayment", "#W-DIVD-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Unit_Overpayment = localVariables.newFieldInRecord("pnd_W_Unit_Overpayment", "#W-UNIT-OVERPAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Msg = localVariables.newFieldInRecord("pnd_W_Msg", "#W-MSG", FieldType.STRING, 3);
        pnd_W_Pin = localVariables.newFieldInRecord("pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Fund_Lst_Pd_Dte = localVariables.newFieldInRecord("pnd_Fund_Lst_Pd_Dte", "#FUND-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);
        pnd_Numb = localVariables.newFieldArrayInRecord("pnd_Numb", "#NUMB", FieldType.NUMERIC, 4, new DbsArrayController(1, 7));

        iaa_Parm_Card = localVariables.newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);

        iaa_Parm_Card__R_Field_1 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card__R_Field_1", "REDEFINE", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card__R_Field_1.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alph = localVariables.newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alph", "#FL-DATE-YYYYMMDD-ALPH", FieldType.STRING, 8);

        pnd_Fl_Date_Yyyymmdd_Alph__R_Field_2 = localVariables.newGroupInRecord("pnd_Fl_Date_Yyyymmdd_Alph__R_Field_2", "REDEFINE", pnd_Fl_Date_Yyyymmdd_Alph);
        pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num = pnd_Fl_Date_Yyyymmdd_Alph__R_Field_2.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num", 
            "#FL-DATE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Tab_Install_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", FieldType.STRING, 8, new DbsArrayController(1, 
            120));
        pnd_Tab_Per_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Per_Amt", "#TAB-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_Div_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Div_Amt", "#TAB-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_File = localVariables.newFieldArrayInRecord("pnd_Tab_File", "#TAB-FILE", FieldType.STRING, 1, new DbsArrayController(1, 120));
        pnd_Tab_Inverse_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Inverse_Date", "#TAB-INVERSE-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            120));
        pnd_Num_Of_Instlmnts = localVariables.newFieldInRecord("pnd_Num_Of_Instlmnts", "#NUM-OF-INSTLMNTS", FieldType.NUMERIC, 3);
        pnd_W_Payee_1 = localVariables.newFieldInRecord("pnd_W_Payee_1", "#W-PAYEE-1", FieldType.NUMERIC, 2);
        pnd_W_Payee_2 = localVariables.newFieldInRecord("pnd_W_Payee_2", "#W-PAYEE-2", FieldType.NUMERIC, 2);
        pnd_Overpay_Years = localVariables.newFieldInRecord("pnd_Overpay_Years", "#OVERPAY-YEARS", FieldType.NUMERIC, 1);
        pnd_Bottom_Date = localVariables.newFieldInRecord("pnd_Bottom_Date", "#BOTTOM-DATE", FieldType.NUMERIC, 8);
        pnd_Ovrpymnt_Reads = localVariables.newFieldInRecord("pnd_Ovrpymnt_Reads", "#OVRPYMNT-READS", FieldType.NUMERIC, 9);
        pnd_Ovrpymnt_Time_Selects = localVariables.newFieldInRecord("pnd_Ovrpymnt_Time_Selects", "#OVRPYMNT-TIME-SELECTS", FieldType.NUMERIC, 9);

        vw_iaa_Ovrpymnt = new DataAccessProgramView(new NameInfo("vw_iaa_Ovrpymnt", "IAA-OVRPYMNT"), "IAA_DC_OVRPYMNT", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_OVRPYMNT"));
        iaa_Ovrpymnt_Ovrpymnt_Ppcn_Nbr = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Ppcn_Nbr", "OVRPYMNT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "OVRPYMNT_PPCN_NBR");
        iaa_Ovrpymnt_Ovrpymnt_Payee_Cde = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Payee_Cde", "OVRPYMNT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "OVRPYMNT_PAYEE_CDE");
        iaa_Ovrpymnt_Ovrpymnt_Id_Nbr = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Id_Nbr", "OVRPYMNT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "OVRPYMNT_ID_NBR");
        iaa_Ovrpymnt_Ovrpymnt_Status_Timestamp = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Status_Timestamp", "OVRPYMNT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "OVRPYMNT_STATUS_TIMESTAMP");
        iaa_Ovrpymnt_Ovrpymnt_Timestamp = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Timestamp", "OVRPYMNT-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "OVRPYMNT_TIMESTAMP");

        iaa_Ovrpymnt_Ovrpymnt_Data = vw_iaa_Ovrpymnt.getRecord().newGroupInGroup("iaa_Ovrpymnt_Ovrpymnt_Data", "OVRPYMNT-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr = iaa_Ovrpymnt_Ovrpymnt_Data.newFieldArrayInGroup("iaa_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr", "OVRPYMNT-END-FISCAL-YR", 
            FieldType.NUMERIC, 4, new DbsArrayController(1, 7) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_END_FISCAL_YR", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date", "OVRPYMNT-LAST-PYMNT-RCVD-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "OVRPYMNT_LAST_PYMNT_RCVD_DATE");
        iaa_Ovrpymnt_Ovrpymnt_Ind = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Ovrpymnt_Ind", "OVRPYMNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "OVRPYMNT_IND");
        iaa_Ovrpymnt_Count_Castovrpymnt_Data = vw_iaa_Ovrpymnt.getRecord().newFieldInGroup("iaa_Ovrpymnt_Count_Castovrpymnt_Data", "C*OVRPYMNT-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        registerRecord(vw_iaa_Ovrpymnt);

        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Ovrpymnt.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap586s() throws Exception
    {
        super("Iaap586s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, iaa_Parm_Card)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        pnd_Year.setValue(9999);                                                                                                                                          //Natural: MOVE 9999 TO #YEAR
        //* *. READ IAA-OVRPYMNT PHYSICAL
        vw_iaa_Ovrpymnt.startDatabaseRead                                                                                                                                 //Natural: READ IAA-OVRPYMNT BY OVRPYMNT-TIMESTAMP STARTING FROM #DATD
        (
        "RD",
        new Wc[] { new Wc("OVRPYMNT_TIMESTAMP", ">=", pnd_Datd, WcType.BY) },
        new Oc[] { new Oc("OVRPYMNT_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(vw_iaa_Ovrpymnt.readNextRow("RD")))
        {
            pnd_Fl_Date_Yyyymmdd_Alph.setValueEdited(iaa_Ovrpymnt_Ovrpymnt_Timestamp,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED OVRPYMNT-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            if (condition(pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num.notEquals(iaa_Parm_Card_Pnd_Parm_Date_N)))                                                   //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  032610 END
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ovrpymnt_Reads.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #OVRPYMNT-READS
            //*  MOVE EDITED OVRPYMNT-STATUS-TIMESTAMP (EM=YYYYMMDD)
            //*  MOVE EDITED OVRPYMNT-TIMESTAMP (EM=YYYYMMDD)   /* 032610
            //*    TO #FL-DATE-YYYYMMDD-ALPH                    /* 032610
            if (condition(!(pnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num.equals(iaa_Parm_Card_Pnd_Parm_Date_N) && iaa_Ovrpymnt_Ovrpymnt_Ind.equals("N"))))          //Natural: ACCEPT IF #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N AND OVRPYMNT-IND = 'N'
            {
                continue;
            }
            pnd_Ovrpymnt_Time_Selects.nadd(1);                                                                                                                            //Natural: ADD 1 TO #OVRPYMNT-TIME-SELECTS
            pnd_W_Contract.setValue(iaa_Ovrpymnt_Ovrpymnt_Ppcn_Nbr);                                                                                                      //Natural: MOVE OVRPYMNT-PPCN-NBR TO #W-CONTRACT
            pnd_W_Payee.setValue(iaa_Ovrpymnt_Ovrpymnt_Payee_Cde);                                                                                                        //Natural: MOVE OVRPYMNT-PAYEE-CDE TO #W-PAYEE
            pnd_Fund_Lst_Pd_Dte.setValue(iaa_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date);                                                                                     //Natural: MOVE OVRPYMNT-LAST-PYMNT-RCVD-DATE TO #FUND-LST-PD-DTE
            pnd_Numb.getValue(1,":",7).setValue(iaa_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr.getValue(1,":",7));                                                                   //Natural: MOVE OVRPYMNT-END-FISCAL-YR ( 1:7 ) TO #NUMB ( 1:7 )
            pnd_Overpay_Years.setValue(iaa_Ovrpymnt_Count_Castovrpymnt_Data);                                                                                             //Natural: MOVE C*OVRPYMNT-DATA TO #OVERPAY-YEARS
            DbsUtil.callnat(Iaan460g.class , getCurrentProcessState(), pnd_W_Contract, pnd_Bottom_Date);                                                                  //Natural: CALLNAT 'IAAN460G' #W-CONTRACT #BOTTOM-DATE
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            DbsUtil.callnat(Iaan460.class , getCurrentProcessState(), pnd_W_Contract, pnd_W_Payee, pnd_Tab_Install_Date.getValue(1,":",120), pnd_Tab_Per_Amt.getValue(1,":",120),  //Natural: CALLNAT 'IAAN460' #W-CONTRACT #W-PAYEE #TAB-INSTALL-DATE ( 1:120 ) #TAB-PER-AMT ( 1:120 ) #TAB-DIV-AMT ( 1:120 ) #TAB-FILE ( 1:120 ) #TAB-INVERSE-DATE ( 1:120 ) #NUM-OF-INSTLMNTS #BOTTOM-DATE #RET-CDE
                pnd_Tab_Div_Amt.getValue(1,":",120), pnd_Tab_File.getValue(1,":",120), pnd_Tab_Inverse_Date.getValue(1,":",120), pnd_Num_Of_Instlmnts, pnd_Bottom_Date, 
                pnd_Ret_Cde);
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            DbsUtil.callnat(Iaan460a.class , getCurrentProcessState(), pnd_W_Contract, pnd_W_Payee, pnd_W_Payee_1, pnd_W_Payee_2);                                        //Natural: CALLNAT 'IAAN460A' #W-CONTRACT #W-PAYEE #W-PAYEE-1 #W-PAYEE-2
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            //*  041514
            DbsUtil.callnat(Iaan586a.class , getCurrentProcessState(), pnd_W_Contract, pnd_W_Payee_1, pnd_W_Payee_2, pnd_Tab_Install_Date.getValue(1,":",120),            //Natural: CALLNAT 'IAAN586A' #W-CONTRACT #W-PAYEE-1 #W-PAYEE-2 #TAB-INSTALL-DATE ( 1:120 ) #TAB-PER-AMT ( 1:120 ) #TAB-DIV-AMT ( 1:120 ) #TAB-FILE ( 1:120 ) #TAB-INVERSE-DATE ( 1:120 ) #NUM-OF-INSTLMNTS #FUND-LST-PD-DTE #YEAR #NUMB ( 1:7 ) #OVERPAY-YEARS OVRPYMNT-ID-NBR
                pnd_Tab_Per_Amt.getValue(1,":",120), pnd_Tab_Div_Amt.getValue(1,":",120), pnd_Tab_File.getValue(1,":",120), pnd_Tab_Inverse_Date.getValue(1,":",120), 
                pnd_Num_Of_Instlmnts, pnd_Fund_Lst_Pd_Dte, pnd_Year, pnd_Numb.getValue(1,":",7), pnd_Overpay_Years, iaa_Ovrpymnt_Ovrpymnt_Id_Nbr);
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ovrpymnt_Time_Selects.equals(getZero())))                                                                                                       //Natural: IF #OVRPYMNT-TIME-SELECTS = 0
        {
            pnd_W_Contract.setValue("99999999");                                                                                                                          //Natural: MOVE '99999999' TO #W-CONTRACT
            pnd_W_Payee.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #W-PAYEE #W-MODE #W-DATE #W-GUAR-PAYMENT #W-DIVD-PAYMENT #W-UNIT-PAYMENT #W-GUAR-OVERPAYMENT #W-DIVD-OVERPAYMENT #W-UNIT-OVERPAYMENT
            pnd_W_Mode.setValue(0);
            pnd_W_Date.setValue(0);
            pnd_W_Guar_Payment.setValue(0);
            pnd_W_Divd_Payment.setValue(0);
            pnd_W_Unit_Payment.setValue(0);
            pnd_W_Guar_Overpayment.setValue(0);
            pnd_W_Divd_Overpayment.setValue(0);
            pnd_W_Unit_Overpayment.setValue(0);
            //*  041514
            getWorkFiles().write(2, false, pnd_W_Contract, pnd_W_Payee, pnd_W_Fund, pnd_W_Desc, pnd_W_Mode, pnd_W_Date, pnd_W_Guar_Payment, pnd_W_Divd_Payment,           //Natural: WRITE WORK FILE 2 #W-CONTRACT #W-PAYEE #W-FUND #W-DESC #W-MODE #W-DATE #W-GUAR-PAYMENT #W-DIVD-PAYMENT #W-UNIT-PAYMENT #W-GUAR-OVERPAYMENT #W-DIVD-OVERPAYMENT #W-UNIT-OVERPAYMENT #W-MSG #W-PIN
                pnd_W_Unit_Payment, pnd_W_Guar_Overpayment, pnd_W_Divd_Overpayment, pnd_W_Unit_Overpayment, pnd_W_Msg, pnd_W_Pin);
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "===========  IAAP586S DISPLAYS  ======================",NEWLINE);                                                                          //Natural: WRITE '===========  IAAP586S DISPLAYS  ======================' /
        if (Global.isEscape()) return;
        getReports().write(0, "  OVERPAYMENT READS ==========> ",pnd_Ovrpymnt_Reads, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                 //Natural: WRITE '  OVERPAYMENT READS ==========> ' #OVRPYMNT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  OVERPAYMENT TIME SELECTS ===> ",pnd_Ovrpymnt_Time_Selects, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                          //Natural: WRITE '  OVERPAYMENT TIME SELECTS ===> ' #OVRPYMNT-TIME-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*     WRITE '  WORK FILE WRITES ===========> ' #W2-WRITES
        getReports().write(0, "**** END OF PROGRAM IAAP586S **** ");                                                                                                      //Natural: WRITE '**** END OF PROGRAM IAAP586S **** '
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(iaa_Parm_Card_Pnd_Parm_Date,"YYYYMMDD")))                                                                                       //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",iaa_Parm_Card_Pnd_Parm_Date);                                                                           //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032610
            iaa_Parm_Card_Pnd_Parm_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032610
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),iaa_Parm_Card_Pnd_Parm_Date);                                                                              //Natural: MOVE EDITED #PARM-DATE TO #DATD ( EM = YYYYMMDD )
    }

    //
}
