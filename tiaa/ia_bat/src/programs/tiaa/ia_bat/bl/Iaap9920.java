/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:38 PM
**        * FROM NATURAL PROGRAM : Iaap9920
************************************************************
**        * FILE NAME            : Iaap9920.java
**        * CLASS NAME           : Iaap9920
**        * INSTANCE NAME        : Iaap9920
************************************************************
**********************************************************************
* PROGRAM:  IAAP9920                                                 *
* DATE   :  9/20/2013                                                *
* PURPOSE:  CREATE DAILY FILE EXTRACT OF IA CONTRACTS BY PLAN/PPG TO *
*           BE LOADED TO SQL SERVER DATABASE.                        *
*                                                                    *
* 08/2015  R. CARREON  - COR/NAAD DECOMM. USE CORE ETL. SCAN 082015  *
* 04/2017  O. SOTTO    - PIN EXPANSION CHANGES MARKED 082017.       *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap9920 extends BLNatBase
{
    // Data Areas
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Core;

    private DbsGroup pnd_Core__R_Field_1;
    private DbsField pnd_Core_Cntrct_Payee;

    private DbsGroup pnd_Core__R_Field_2;
    private DbsField pnd_Core_Cntrct_Nbr;
    private DbsField pnd_Core_Cntrct_Payee_Cde;
    private DbsField pnd_Core_Ph_Unique_Id_Nbr;
    private DbsField pnd_Core_Ph_Rcd_Type_Cde;
    private DbsField pnd_Core_Ph_Social_Security_No;
    private DbsField pnd_Core_Ph_Dob_Dte;
    private DbsField pnd_Core_Ph_Dod_Dte;

    private DbsGroup pnd_Core_Ph_Nme;
    private DbsField pnd_Core_Ph_Last_Nme;

    private DbsGroup pnd_Core__R_Field_3;
    private DbsField pnd_Core_Ph_Last_Nme_16;
    private DbsField pnd_Core_Ph_First_Nme;

    private DbsGroup pnd_Core__R_Field_4;
    private DbsField pnd_Core_Ph_First_Nme_10;
    private DbsField pnd_Core_Ph_Mddle_Nme;

    private DbsGroup pnd_Core__R_Field_5;
    private DbsField pnd_Core_Ph_Mddle_Nme_12;
    private DbsField pnd_Core_Pnd_Cor_Status_Cde;
    private DbsField pnd_Core_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Core_Eof;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Lst_Trans_Dte;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Actvty_Cde;
    private DbsField trans_Trans_Check_Dte;
    private DbsField trans_Trans_Todays_Dte;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_6;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_In_Aian026;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_7;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_8;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_9;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Out_Aian026;
    private DbsField aian026_Pnd_Rtrn__Pgm_Cd;

    private DbsGroup aian026__R_Field_10;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_11;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Mnth1;
    private DbsField aian026_Pnd_Mnth2;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Optn_Cde;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Opt_A;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_17;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Pnd_Issue_Yyyy;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_19;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Iss_St;

    private DbsGroup pnd_Input__R_Field_20;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Iss;
    private DbsField pnd_Input_Pnd_F3a;
    private DbsField pnd_Input_Pnd_First_Ann_Dob_Dte;

    private DbsGroup pnd_Input__R_Field_21;
    private DbsField pnd_Input_Pnd_Dob_Yyyymm;

    private DbsGroup pnd_Input__R_Field_22;
    private DbsField pnd_Input_Pnd_1st_Dob_8;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob_Dte;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_5;

    private DbsGroup pnd_Input__R_Field_23;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_F;
    private DbsField pnd_Input_Pnd_F8;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input__R_Field_24;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_25;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_26;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_27;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_28;
    private DbsField pnd_Input_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_29;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_30;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_31;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Res;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_32;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Pnd_State_Cde;
    private DbsField pnd_Input_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Pnd_Local_Cde;
    private DbsField pnd_Input_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Type_Call;
    private DbsField pnd_Residency;
    private DbsField pnd_Found;
    private DbsField pnd_Skip;
    private DbsField pnd_Name;
    private DbsField pnd_H_Dob;

    private DbsGroup pnd_H_Dob__R_Field_33;
    private DbsField pnd_H_Dob_Pnd_H_Dob_A;
    private DbsField pnd_H_Ppcn;
    private DbsField pnd_1st_Dob_Dte;

    private DbsGroup pnd_1st_Dob_Dte__R_Field_34;
    private DbsField pnd_1st_Dob_Dte_Pnd_1st_Dob_Yyyymm;
    private DbsField pnd_1st_Dob_Dte__Filler3;
    private DbsField pnd_2nd_Dob_Dte;

    private DbsGroup pnd_2nd_Dob_Dte__R_Field_35;
    private DbsField pnd_2nd_Dob_Dte_Pnd_2nd_Dob_Yyyymm;
    private DbsField pnd_2nd_Dob_Dte__Filler4;
    private DbsField pnd_1st_Dod;

    private DbsGroup pnd_1st_Dod__R_Field_36;
    private DbsField pnd_1st_Dod_Pnd_1st_Dod_Yyyy;
    private DbsField pnd_2nd_Dod;

    private DbsGroup pnd_2nd_Dod__R_Field_37;
    private DbsField pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy;
    private DbsField pnd_F_Dte;

    private DbsGroup pnd_F_Dte__R_Field_38;
    private DbsField pnd_F_Dte_Pnd_F_Yyyymm;
    private DbsField pnd_H_Pmt_Dte;

    private DbsGroup pnd_H_Pmt_Dte__R_Field_39;
    private DbsField pnd_H_Pmt_Dte_Pnd_H_Yyyy;
    private DbsField pnd_H_Pmt_Dte_Pnd_H_Mm;
    private DbsField pnd_H_Pmt_Dte_Pnd_H_Dd;

    private DbsGroup pnd_H_Pmt_Dte__R_Field_40;
    private DbsField pnd_H_Pmt_Dte_Pnd_H_Yyyymm;

    private DbsGroup pnd_H_Pmt_Dte__R_Field_41;
    private DbsField pnd_H_Pmt_Dte_Pnd_H_Pmt_Dte_N;
    private DbsField pnd_T_Pmt_Dte;

    private DbsGroup pnd_T_Pmt_Dte__R_Field_42;
    private DbsField pnd_T_Pmt_Dte_Pnd_T_Yyyy;
    private DbsField pnd_T_Pmt_Dte_Pnd_T_Mm;
    private DbsField pnd_T_Pmt_Dte_Pnd_T_Dd;

    private DbsGroup pnd_T_Pmt_Dte__R_Field_43;
    private DbsField pnd_T_Pmt_Dte_Pnd_T_Yyyymm;

    private DbsGroup pnd_T_Pmt_Dte__R_Field_44;
    private DbsField pnd_T_Pmt_Dte_Pnd_T_Pmt_Dte_N;
    private DbsField pnd_T_Dte;
    private DbsField pnd_W_Dte_10;
    private DbsField pnd_H_Mode;

    private DbsGroup pnd_H_Mode__R_Field_45;
    private DbsField pnd_H_Mode__Filler5;
    private DbsField pnd_H_Mode_Pnd_H_Mo;
    private DbsField pnd_H_Opt;

    private DbsGroup pnd_H_Opt__R_Field_46;
    private DbsField pnd_H_Opt_Pnd_H_Opt_A;
    private DbsField pnd_H_Org;
    private DbsField pnd_H_Ppg;
    private DbsField pnd_H_Cntrct;
    private DbsField pnd_H_Py;
    private DbsField pnd_Pin;
    private DbsField pnd_Pin_A;
    private DbsField pnd_H_Res;
    private DbsField pnd_H_Issue_Date;

    private DbsGroup pnd_H_Issue_Date__R_Field_47;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Dte;

    private DbsGroup pnd_H_Issue_Date__R_Field_48;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Yyyy;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Mm;
    private DbsField pnd_H_Issue_Date_Pnd_H_Iss_Dd;
    private DbsField pnd_Write_It;
    private DbsField pnd_Dob;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_H_Ssn;

    private DbsGroup pnd_H_Ssn__R_Field_49;
    private DbsField pnd_H_Ssn_Pnd_H_Ssn_A;
    private DbsField pnd_H_Stts;
    private DbsField pnd_H_Pend;
    private DbsField pnd_Cnt;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Dt1;

    private DbsGroup pnd_Dt1__R_Field_50;
    private DbsField pnd_Dt1_Pnd_Yy1;
    private DbsField pnd_Dt1_Pnd_Mm1;

    private DbsGroup pnd_Dt1__R_Field_51;
    private DbsField pnd_Dt1_Pnd_Yyyymm;
    private DbsField pnd_Dt2;

    private DbsGroup pnd_Dt2__R_Field_52;
    private DbsField pnd_Dt2_Pnd_Yy2;
    private DbsField pnd_Dt2_Pnd_Mm2;
    private DbsField pnd_W_Dt1;
    private DbsField pnd_W_Yy1;
    private DbsField pnd_W_Year;
    private DbsField pnd_Bypass;
    private DbsField pnd_Cpr_Bypass;
    private DbsField pnd_Mo;
    private DbsField pnd_H_Plan;
    private DbsField pnd_First;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_53;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Py;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_54;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Cntrct;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Py;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Fu;

    private DataAccessProgramView vw_cntrct;
    private DbsField cntrct_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Cntrct_Issue_Dte;
    private DbsField cntrct_Cntrct_Issue_Dte_Dd;

    private DbsGroup cntrct__R_Field_55;
    private DbsField cntrct_Iss_Dd;
    private DbsField cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Optn_Cde;

    private DbsGroup cntrct_Lgcy_Plan_Sub_Plan;
    private DbsField cntrct_Lgcy_Plan_Nbr;
    private DbsField pnd_Plan_Subplan;

    private DbsGroup pnd_Plan_Subplan__R_Field_56;

    private DbsGroup pnd_Plan_Subplan_Pnd_Plan_Sub;
    private DbsField pnd_Plan_Subplan_Pnd_P_Subp;
    private DbsField pnd_Plan_Subplan_Pnd_Comma;
    private DbsField pnd_R_Opt_A;
    private DbsField pnd_R_Cnt;
    private DbsField pnd_R_Dsc;
    private DbsField pnd_Opt_Desc;
    private DbsField pnd_S_Issue_Yyyy;
    private DbsField pnd_Status;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_In_Ppg;
    private DbsField pnd_In_Plan;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_H_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Pmts;
    private DbsField pnd_Pmts_A;
    private DbsField pnd_Stts;
    private DbsField pnd_Frqncy;
    private DbsField pnd_Annual_Pmt;
    private DbsField pnd_Annual_Gross;
    private DbsField pnd_Sex;
    private DbsField pnd_Counter;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);

        // Local Variables
        pnd_Core = localVariables.newFieldInRecord("pnd_Core", "#CORE", FieldType.STRING, 150);

        pnd_Core__R_Field_1 = localVariables.newGroupInRecord("pnd_Core__R_Field_1", "REDEFINE", pnd_Core);
        pnd_Core_Cntrct_Payee = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Core__R_Field_2 = pnd_Core__R_Field_1.newGroupInGroup("pnd_Core__R_Field_2", "REDEFINE", pnd_Core_Cntrct_Payee);
        pnd_Core_Cntrct_Nbr = pnd_Core__R_Field_2.newFieldInGroup("pnd_Core_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Core_Cntrct_Payee_Cde = pnd_Core__R_Field_2.newFieldInGroup("pnd_Core_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Core_Ph_Unique_Id_Nbr = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Core_Ph_Rcd_Type_Cde = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Ph_Rcd_Type_Cde", "PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Ph_Social_Security_No = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Ph_Social_Security_No", "PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 
            9);
        pnd_Core_Ph_Dob_Dte = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Ph_Dob_Dte", "PH-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Core_Ph_Dod_Dte = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Ph_Dod_Dte", "PH-DOD-DTE", FieldType.NUMERIC, 8);

        pnd_Core_Ph_Nme = pnd_Core__R_Field_1.newGroupInGroup("pnd_Core_Ph_Nme", "PH-NME");
        pnd_Core_Ph_Last_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_3 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_3", "REDEFINE", pnd_Core_Ph_Last_Nme);
        pnd_Core_Ph_Last_Nme_16 = pnd_Core__R_Field_3.newFieldInGroup("pnd_Core_Ph_Last_Nme_16", "PH-LAST-NME-16", FieldType.STRING, 16);
        pnd_Core_Ph_First_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_4 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_4", "REDEFINE", pnd_Core_Ph_First_Nme);
        pnd_Core_Ph_First_Nme_10 = pnd_Core__R_Field_4.newFieldInGroup("pnd_Core_Ph_First_Nme_10", "PH-FIRST-NME-10", FieldType.STRING, 10);
        pnd_Core_Ph_Mddle_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Mddle_Nme", "PH-MDDLE-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_5 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_5", "REDEFINE", pnd_Core_Ph_Mddle_Nme);
        pnd_Core_Ph_Mddle_Nme_12 = pnd_Core__R_Field_5.newFieldInGroup("pnd_Core_Ph_Mddle_Nme_12", "PH-MDDLE-NME-12", FieldType.STRING, 12);
        pnd_Core_Pnd_Cor_Status_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 1);
        pnd_Core_Pnd_Cor_Sex_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Core_Eof = localVariables.newFieldInRecord("pnd_Core_Eof", "#CORE-EOF", FieldType.BOOLEAN, 1);

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Lst_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Actvty_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_ACTVTY_CDE");
        trans_Trans_Check_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        trans_Trans_Todays_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_TODAYS_DTE");
        registerRecord(vw_trans);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_6", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_6.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_In_Aian026 = aian026.newGroupInGroup("aian026_Pnd_In_Aian026", "#IN-AIAN026");
        aian026_Pnd_Call_Type = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_7 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_7", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_7.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_8 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_8", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_9 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_9", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Out_Aian026 = aian026.newGroupInGroup("aian026_Pnd_Out_Aian026", "#OUT-AIAN026");
        aian026_Pnd_Rtrn__Pgm_Cd = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Rtrn__Pgm_Cd", "#RTRN--PGM-CD", FieldType.STRING, 11);

        aian026__R_Field_10 = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026__R_Field_10", "REDEFINE", aian026_Pnd_Rtrn__Pgm_Cd);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_10.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_10.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_11 = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026__R_Field_11", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_11.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Mnth1 = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Mnth1", "#MNTH1", FieldType.NUMERIC, 2);
        aian026_Pnd_Mnth2 = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Mnth2", "#MNTH2", FieldType.NUMERIC, 2);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_12 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_12.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_14 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_15 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_16 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Optn_Cde);
        pnd_Input_Pnd_Opt_A = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Opt_A", "#OPT-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_17 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_17", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);

        pnd_Input__R_Field_18 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_18", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Yyyy = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Issue_Yyyy", "#ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_19 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_19", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Iss_St = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);

        pnd_Input__R_Field_20 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_20", "REDEFINE", pnd_Input_Pnd_Iss_St);
        pnd_Input__Filler1 = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_Iss = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input_Pnd_Iss", "#ISS", FieldType.STRING, 2);
        pnd_Input_Pnd_F3a = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F3a", "#F3A", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob_Dte", "#FIRST-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_21 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_21", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_Dob_Yyyymm = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Dob_Yyyymm", "#DOB-YYYYMM", FieldType.STRING, 6);

        pnd_Input__R_Field_22 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_22", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_1st_Dob_8 = pnd_Input__R_Field_22.newFieldInGroup("pnd_Input_Pnd_1st_Dob_8", "#1ST-DOB-8", FieldType.STRING, 8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob_Dte", "#SCND-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde_5 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_5", "#PPG-CDE-5", FieldType.STRING, 5);

        pnd_Input__R_Field_23 = pnd_Input__R_Field_15.newGroupInGroup("pnd_Input__R_Field_23", "REDEFINE", pnd_Input_Pnd_Ppg_Cde_5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_23.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Ppg_Cde_F = pnd_Input__R_Field_23.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_F", "#PPG-CDE-F", FieldType.STRING, 1);
        pnd_Input_Pnd_F8 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 8);

        pnd_Input__R_Field_24 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_24", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_25 = pnd_Input__R_Field_24.newGroupInGroup("pnd_Input__R_Field_25", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_25.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_26 = pnd_Input__R_Field_24.newGroupInGroup("pnd_Input__R_Field_26", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_26.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_24.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_27 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_27", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);

        pnd_Input__R_Field_28 = pnd_Input__R_Field_27.newGroupInGroup("pnd_Input__R_Field_28", "REDEFINE", pnd_Input_Pnd_Summ_Fund_Cde);
        pnd_Input_Fund_Cde = pnd_Input__R_Field_28.newFieldInGroup("pnd_Input_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_29 = pnd_Input__R_Field_27.newGroupInGroup("pnd_Input__R_Field_29", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_29.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_27.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_30 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_30", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_31 = pnd_Input__R_Field_30.newGroupInGroup("pnd_Input__R_Field_31", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler2 = pnd_Input__R_Field_31.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_Res = pnd_Input__R_Field_31.newFieldInGroup("pnd_Input_Pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Trmnte_Rsn = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_32 = pnd_Input__R_Field_30.newGroupInGroup("pnd_Input__R_Field_32", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_32.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_Spirt_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Amt = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Input_Pnd_Spirt_Srce = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Arr_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Input_Pnd_Spirt_Prcss_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Fed_Tax_Amt = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Pnd_State_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_State_Tax_Amt = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Local_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Local_Tax_Amt = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Lst_Chnge_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cpr_Xfr_Term_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cpr_Lgl_Res_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_30.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_30.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_Residency = localVariables.newFieldInRecord("pnd_Residency", "#RESIDENCY", FieldType.STRING, 20);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Skip = localVariables.newFieldInRecord("pnd_Skip", "#SKIP", FieldType.BOOLEAN, 1);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 40);
        pnd_H_Dob = localVariables.newFieldInRecord("pnd_H_Dob", "#H-DOB", FieldType.NUMERIC, 8);

        pnd_H_Dob__R_Field_33 = localVariables.newGroupInRecord("pnd_H_Dob__R_Field_33", "REDEFINE", pnd_H_Dob);
        pnd_H_Dob_Pnd_H_Dob_A = pnd_H_Dob__R_Field_33.newFieldInGroup("pnd_H_Dob_Pnd_H_Dob_A", "#H-DOB-A", FieldType.STRING, 8);
        pnd_H_Ppcn = localVariables.newFieldInRecord("pnd_H_Ppcn", "#H-PPCN", FieldType.STRING, 10);
        pnd_1st_Dob_Dte = localVariables.newFieldInRecord("pnd_1st_Dob_Dte", "#1ST-DOB-DTE", FieldType.NUMERIC, 8);

        pnd_1st_Dob_Dte__R_Field_34 = localVariables.newGroupInRecord("pnd_1st_Dob_Dte__R_Field_34", "REDEFINE", pnd_1st_Dob_Dte);
        pnd_1st_Dob_Dte_Pnd_1st_Dob_Yyyymm = pnd_1st_Dob_Dte__R_Field_34.newFieldInGroup("pnd_1st_Dob_Dte_Pnd_1st_Dob_Yyyymm", "#1ST-DOB-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_1st_Dob_Dte__Filler3 = pnd_1st_Dob_Dte__R_Field_34.newFieldInGroup("pnd_1st_Dob_Dte__Filler3", "_FILLER3", FieldType.STRING, 2);
        pnd_2nd_Dob_Dte = localVariables.newFieldInRecord("pnd_2nd_Dob_Dte", "#2ND-DOB-DTE", FieldType.NUMERIC, 8);

        pnd_2nd_Dob_Dte__R_Field_35 = localVariables.newGroupInRecord("pnd_2nd_Dob_Dte__R_Field_35", "REDEFINE", pnd_2nd_Dob_Dte);
        pnd_2nd_Dob_Dte_Pnd_2nd_Dob_Yyyymm = pnd_2nd_Dob_Dte__R_Field_35.newFieldInGroup("pnd_2nd_Dob_Dte_Pnd_2nd_Dob_Yyyymm", "#2ND-DOB-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_2nd_Dob_Dte__Filler4 = pnd_2nd_Dob_Dte__R_Field_35.newFieldInGroup("pnd_2nd_Dob_Dte__Filler4", "_FILLER4", FieldType.STRING, 2);
        pnd_1st_Dod = localVariables.newFieldInRecord("pnd_1st_Dod", "#1ST-DOD", FieldType.NUMERIC, 6);

        pnd_1st_Dod__R_Field_36 = localVariables.newGroupInRecord("pnd_1st_Dod__R_Field_36", "REDEFINE", pnd_1st_Dod);
        pnd_1st_Dod_Pnd_1st_Dod_Yyyy = pnd_1st_Dod__R_Field_36.newFieldInGroup("pnd_1st_Dod_Pnd_1st_Dod_Yyyy", "#1ST-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_2nd_Dod = localVariables.newFieldInRecord("pnd_2nd_Dod", "#2ND-DOD", FieldType.NUMERIC, 6);

        pnd_2nd_Dod__R_Field_37 = localVariables.newGroupInRecord("pnd_2nd_Dod__R_Field_37", "REDEFINE", pnd_2nd_Dod);
        pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy = pnd_2nd_Dod__R_Field_37.newFieldInGroup("pnd_2nd_Dod_Pnd_2nd_Dod_Yyyy", "#2ND-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_F_Dte = localVariables.newFieldInRecord("pnd_F_Dte", "#F-DTE", FieldType.STRING, 8);

        pnd_F_Dte__R_Field_38 = localVariables.newGroupInRecord("pnd_F_Dte__R_Field_38", "REDEFINE", pnd_F_Dte);
        pnd_F_Dte_Pnd_F_Yyyymm = pnd_F_Dte__R_Field_38.newFieldInGroup("pnd_F_Dte_Pnd_F_Yyyymm", "#F-YYYYMM", FieldType.NUMERIC, 6);
        pnd_H_Pmt_Dte = localVariables.newFieldInRecord("pnd_H_Pmt_Dte", "#H-PMT-DTE", FieldType.STRING, 8);

        pnd_H_Pmt_Dte__R_Field_39 = localVariables.newGroupInRecord("pnd_H_Pmt_Dte__R_Field_39", "REDEFINE", pnd_H_Pmt_Dte);
        pnd_H_Pmt_Dte_Pnd_H_Yyyy = pnd_H_Pmt_Dte__R_Field_39.newFieldInGroup("pnd_H_Pmt_Dte_Pnd_H_Yyyy", "#H-YYYY", FieldType.NUMERIC, 4);
        pnd_H_Pmt_Dte_Pnd_H_Mm = pnd_H_Pmt_Dte__R_Field_39.newFieldInGroup("pnd_H_Pmt_Dte_Pnd_H_Mm", "#H-MM", FieldType.NUMERIC, 2);
        pnd_H_Pmt_Dte_Pnd_H_Dd = pnd_H_Pmt_Dte__R_Field_39.newFieldInGroup("pnd_H_Pmt_Dte_Pnd_H_Dd", "#H-DD", FieldType.NUMERIC, 2);

        pnd_H_Pmt_Dte__R_Field_40 = localVariables.newGroupInRecord("pnd_H_Pmt_Dte__R_Field_40", "REDEFINE", pnd_H_Pmt_Dte);
        pnd_H_Pmt_Dte_Pnd_H_Yyyymm = pnd_H_Pmt_Dte__R_Field_40.newFieldInGroup("pnd_H_Pmt_Dte_Pnd_H_Yyyymm", "#H-YYYYMM", FieldType.NUMERIC, 6);

        pnd_H_Pmt_Dte__R_Field_41 = localVariables.newGroupInRecord("pnd_H_Pmt_Dte__R_Field_41", "REDEFINE", pnd_H_Pmt_Dte);
        pnd_H_Pmt_Dte_Pnd_H_Pmt_Dte_N = pnd_H_Pmt_Dte__R_Field_41.newFieldInGroup("pnd_H_Pmt_Dte_Pnd_H_Pmt_Dte_N", "#H-PMT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_T_Pmt_Dte = localVariables.newFieldInRecord("pnd_T_Pmt_Dte", "#T-PMT-DTE", FieldType.STRING, 8);

        pnd_T_Pmt_Dte__R_Field_42 = localVariables.newGroupInRecord("pnd_T_Pmt_Dte__R_Field_42", "REDEFINE", pnd_T_Pmt_Dte);
        pnd_T_Pmt_Dte_Pnd_T_Yyyy = pnd_T_Pmt_Dte__R_Field_42.newFieldInGroup("pnd_T_Pmt_Dte_Pnd_T_Yyyy", "#T-YYYY", FieldType.NUMERIC, 4);
        pnd_T_Pmt_Dte_Pnd_T_Mm = pnd_T_Pmt_Dte__R_Field_42.newFieldInGroup("pnd_T_Pmt_Dte_Pnd_T_Mm", "#T-MM", FieldType.NUMERIC, 2);
        pnd_T_Pmt_Dte_Pnd_T_Dd = pnd_T_Pmt_Dte__R_Field_42.newFieldInGroup("pnd_T_Pmt_Dte_Pnd_T_Dd", "#T-DD", FieldType.NUMERIC, 2);

        pnd_T_Pmt_Dte__R_Field_43 = localVariables.newGroupInRecord("pnd_T_Pmt_Dte__R_Field_43", "REDEFINE", pnd_T_Pmt_Dte);
        pnd_T_Pmt_Dte_Pnd_T_Yyyymm = pnd_T_Pmt_Dte__R_Field_43.newFieldInGroup("pnd_T_Pmt_Dte_Pnd_T_Yyyymm", "#T-YYYYMM", FieldType.NUMERIC, 6);

        pnd_T_Pmt_Dte__R_Field_44 = localVariables.newGroupInRecord("pnd_T_Pmt_Dte__R_Field_44", "REDEFINE", pnd_T_Pmt_Dte);
        pnd_T_Pmt_Dte_Pnd_T_Pmt_Dte_N = pnd_T_Pmt_Dte__R_Field_44.newFieldInGroup("pnd_T_Pmt_Dte_Pnd_T_Pmt_Dte_N", "#T-PMT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.DATE);
        pnd_W_Dte_10 = localVariables.newFieldInRecord("pnd_W_Dte_10", "#W-DTE-10", FieldType.STRING, 10);
        pnd_H_Mode = localVariables.newFieldInRecord("pnd_H_Mode", "#H-MODE", FieldType.NUMERIC, 3);

        pnd_H_Mode__R_Field_45 = localVariables.newGroupInRecord("pnd_H_Mode__R_Field_45", "REDEFINE", pnd_H_Mode);
        pnd_H_Mode__Filler5 = pnd_H_Mode__R_Field_45.newFieldInGroup("pnd_H_Mode__Filler5", "_FILLER5", FieldType.STRING, 1);
        pnd_H_Mode_Pnd_H_Mo = pnd_H_Mode__R_Field_45.newFieldInGroup("pnd_H_Mode_Pnd_H_Mo", "#H-MO", FieldType.NUMERIC, 2);
        pnd_H_Opt = localVariables.newFieldInRecord("pnd_H_Opt", "#H-OPT", FieldType.NUMERIC, 2);

        pnd_H_Opt__R_Field_46 = localVariables.newGroupInRecord("pnd_H_Opt__R_Field_46", "REDEFINE", pnd_H_Opt);
        pnd_H_Opt_Pnd_H_Opt_A = pnd_H_Opt__R_Field_46.newFieldInGroup("pnd_H_Opt_Pnd_H_Opt_A", "#H-OPT-A", FieldType.STRING, 2);
        pnd_H_Org = localVariables.newFieldInRecord("pnd_H_Org", "#H-ORG", FieldType.NUMERIC, 2);
        pnd_H_Ppg = localVariables.newFieldInRecord("pnd_H_Ppg", "#H-PPG", FieldType.STRING, 5);
        pnd_H_Cntrct = localVariables.newFieldInRecord("pnd_H_Cntrct", "#H-CNTRCT", FieldType.STRING, 10);
        pnd_H_Py = localVariables.newFieldInRecord("pnd_H_Py", "#H-PY", FieldType.STRING, 2);
        pnd_Pin = localVariables.newFieldInRecord("pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Pin_A = localVariables.newFieldInRecord("pnd_Pin_A", "#PIN-A", FieldType.STRING, 12);
        pnd_H_Res = localVariables.newFieldInRecord("pnd_H_Res", "#H-RES", FieldType.STRING, 3);
        pnd_H_Issue_Date = localVariables.newFieldInRecord("pnd_H_Issue_Date", "#H-ISSUE-DATE", FieldType.STRING, 8);

        pnd_H_Issue_Date__R_Field_47 = localVariables.newGroupInRecord("pnd_H_Issue_Date__R_Field_47", "REDEFINE", pnd_H_Issue_Date);
        pnd_H_Issue_Date_Pnd_H_Iss_Dte = pnd_H_Issue_Date__R_Field_47.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Dte", "#H-ISS-DTE", FieldType.STRING, 
            6);

        pnd_H_Issue_Date__R_Field_48 = pnd_H_Issue_Date__R_Field_47.newGroupInGroup("pnd_H_Issue_Date__R_Field_48", "REDEFINE", pnd_H_Issue_Date_Pnd_H_Iss_Dte);
        pnd_H_Issue_Date_Pnd_H_Iss_Yyyy = pnd_H_Issue_Date__R_Field_48.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Yyyy", "#H-ISS-YYYY", FieldType.NUMERIC, 
            4);
        pnd_H_Issue_Date_Pnd_H_Iss_Mm = pnd_H_Issue_Date__R_Field_48.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Mm", "#H-ISS-MM", FieldType.NUMERIC, 
            2);
        pnd_H_Issue_Date_Pnd_H_Iss_Dd = pnd_H_Issue_Date__R_Field_47.newFieldInGroup("pnd_H_Issue_Date_Pnd_H_Iss_Dd", "#H-ISS-DD", FieldType.NUMERIC, 
            2);
        pnd_Write_It = localVariables.newFieldInRecord("pnd_Write_It", "#WRITE-IT", FieldType.BOOLEAN, 1);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 10);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_H_Ssn = localVariables.newFieldInRecord("pnd_H_Ssn", "#H-SSN", FieldType.NUMERIC, 9);

        pnd_H_Ssn__R_Field_49 = localVariables.newGroupInRecord("pnd_H_Ssn__R_Field_49", "REDEFINE", pnd_H_Ssn);
        pnd_H_Ssn_Pnd_H_Ssn_A = pnd_H_Ssn__R_Field_49.newFieldInGroup("pnd_H_Ssn_Pnd_H_Ssn_A", "#H-SSN-A", FieldType.STRING, 9);
        pnd_H_Stts = localVariables.newFieldInRecord("pnd_H_Stts", "#H-STTS", FieldType.NUMERIC, 1);
        pnd_H_Pend = localVariables.newFieldInRecord("pnd_H_Pend", "#H-PEND", FieldType.STRING, 1);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.PACKED_DECIMAL, 7);
        pnd_Dt1 = localVariables.newFieldInRecord("pnd_Dt1", "#DT1", FieldType.STRING, 6);

        pnd_Dt1__R_Field_50 = localVariables.newGroupInRecord("pnd_Dt1__R_Field_50", "REDEFINE", pnd_Dt1);
        pnd_Dt1_Pnd_Yy1 = pnd_Dt1__R_Field_50.newFieldInGroup("pnd_Dt1_Pnd_Yy1", "#YY1", FieldType.NUMERIC, 4);
        pnd_Dt1_Pnd_Mm1 = pnd_Dt1__R_Field_50.newFieldInGroup("pnd_Dt1_Pnd_Mm1", "#MM1", FieldType.NUMERIC, 2);

        pnd_Dt1__R_Field_51 = localVariables.newGroupInRecord("pnd_Dt1__R_Field_51", "REDEFINE", pnd_Dt1);
        pnd_Dt1_Pnd_Yyyymm = pnd_Dt1__R_Field_51.newFieldInGroup("pnd_Dt1_Pnd_Yyyymm", "#YYYYMM", FieldType.NUMERIC, 6);
        pnd_Dt2 = localVariables.newFieldInRecord("pnd_Dt2", "#DT2", FieldType.STRING, 6);

        pnd_Dt2__R_Field_52 = localVariables.newGroupInRecord("pnd_Dt2__R_Field_52", "REDEFINE", pnd_Dt2);
        pnd_Dt2_Pnd_Yy2 = pnd_Dt2__R_Field_52.newFieldInGroup("pnd_Dt2_Pnd_Yy2", "#YY2", FieldType.NUMERIC, 4);
        pnd_Dt2_Pnd_Mm2 = pnd_Dt2__R_Field_52.newFieldInGroup("pnd_Dt2_Pnd_Mm2", "#MM2", FieldType.NUMERIC, 2);
        pnd_W_Dt1 = localVariables.newFieldInRecord("pnd_W_Dt1", "#W-DT1", FieldType.STRING, 6);
        pnd_W_Yy1 = localVariables.newFieldInRecord("pnd_W_Yy1", "#W-YY1", FieldType.NUMERIC, 4);
        pnd_W_Year = localVariables.newFieldInRecord("pnd_W_Year", "#W-YEAR", FieldType.NUMERIC, 4);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Cpr_Bypass = localVariables.newFieldInRecord("pnd_Cpr_Bypass", "#CPR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Mo = localVariables.newFieldInRecord("pnd_Mo", "#MO", FieldType.NUMERIC, 1, 1);
        pnd_H_Plan = localVariables.newFieldInRecord("pnd_H_Plan", "#H-PLAN", FieldType.STRING, 6);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_53 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_53", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cpr_Cntrct = pnd_Cpr_Key__R_Field_53.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Cntrct", "#CPR-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cpr_Py = pnd_Cpr_Key__R_Field_53.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Py", "#CPR-PY", FieldType.STRING, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_54 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_54", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Cntrct = pnd_Tiaa_Cntrct_Fund_Key__R_Field_54.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Cntrct", "#T-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Py = pnd_Tiaa_Cntrct_Fund_Key__R_Field_54.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Py", "#T-PY", FieldType.NUMERIC, 
            2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Fu = pnd_Tiaa_Cntrct_Fund_Key__R_Field_54.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_T_Fu", "#T-FU", FieldType.STRING, 
            3);

        vw_cntrct = new DataAccessProgramView(new NameInfo("vw_cntrct", "CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT"));
        cntrct_Cntrct_Ppcn_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Cntrct_Issue_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        cntrct_Cntrct_Issue_Dte_Dd = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");

        cntrct__R_Field_55 = vw_cntrct.getRecord().newGroupInGroup("cntrct__R_Field_55", "REDEFINE", cntrct_Cntrct_Issue_Dte_Dd);
        cntrct_Iss_Dd = cntrct__R_Field_55.newFieldInGroup("cntrct_Iss_Dd", "ISS-DD", FieldType.STRING, 2);
        cntrct_Cntrct_First_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        cntrct_Cntrct_Optn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");

        cntrct_Lgcy_Plan_Sub_Plan = vw_cntrct.getRecord().newGroupInGroup("cntrct_Lgcy_Plan_Sub_Plan", "LGCY-PLAN-SUB-PLAN", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        cntrct_Lgcy_Plan_Nbr = cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("cntrct_Lgcy_Plan_Nbr", "LGCY-PLAN-NBR", FieldType.STRING, 6, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        registerRecord(vw_cntrct);

        pnd_Plan_Subplan = localVariables.newFieldArrayInRecord("pnd_Plan_Subplan", "#PLAN-SUBPLAN", FieldType.STRING, 7, new DbsArrayController(1, 21));

        pnd_Plan_Subplan__R_Field_56 = localVariables.newGroupInRecord("pnd_Plan_Subplan__R_Field_56", "REDEFINE", pnd_Plan_Subplan);

        pnd_Plan_Subplan_Pnd_Plan_Sub = pnd_Plan_Subplan__R_Field_56.newGroupArrayInGroup("pnd_Plan_Subplan_Pnd_Plan_Sub", "#PLAN-SUB", new DbsArrayController(1, 
            21));
        pnd_Plan_Subplan_Pnd_P_Subp = pnd_Plan_Subplan_Pnd_Plan_Sub.newFieldInGroup("pnd_Plan_Subplan_Pnd_P_Subp", "#P-SUBP", FieldType.STRING, 6);
        pnd_Plan_Subplan_Pnd_Comma = pnd_Plan_Subplan_Pnd_Plan_Sub.newFieldInGroup("pnd_Plan_Subplan_Pnd_Comma", "#COMMA", FieldType.STRING, 1);
        pnd_R_Opt_A = localVariables.newFieldArrayInRecord("pnd_R_Opt_A", "#R-OPT-A", FieldType.STRING, 2, new DbsArrayController(1, 50));
        pnd_R_Cnt = localVariables.newFieldArrayInRecord("pnd_R_Cnt", "#R-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 50));
        pnd_R_Dsc = localVariables.newFieldArrayInRecord("pnd_R_Dsc", "#R-DSC", FieldType.STRING, 50, new DbsArrayController(1, 50));
        pnd_Opt_Desc = localVariables.newFieldInRecord("pnd_Opt_Desc", "#OPT-DESC", FieldType.STRING, 50);
        pnd_S_Issue_Yyyy = localVariables.newFieldInRecord("pnd_S_Issue_Yyyy", "#S-ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 10);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_In_Ppg = localVariables.newFieldInRecord("pnd_In_Ppg", "#IN-PPG", FieldType.STRING, 5);
        pnd_In_Plan = localVariables.newFieldInRecord("pnd_In_Plan", "#IN-PLAN", FieldType.STRING, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_H_Orig_Da_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_H_Orig_Da_Cntrct_Nbr", "#H-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Pmts = localVariables.newFieldInRecord("pnd_Pmts", "#PMTS", FieldType.NUMERIC, 11, 2);
        pnd_Pmts_A = localVariables.newFieldInRecord("pnd_Pmts_A", "#PMTS-A", FieldType.STRING, 12);
        pnd_Stts = localVariables.newFieldInRecord("pnd_Stts", "#STTS", FieldType.STRING, 1);
        pnd_Frqncy = localVariables.newFieldInRecord("pnd_Frqncy", "#FRQNCY", FieldType.STRING, 20);
        pnd_Annual_Pmt = localVariables.newFieldInRecord("pnd_Annual_Pmt", "#ANNUAL-PMT", FieldType.STRING, 14);
        pnd_Annual_Gross = localVariables.newFieldInRecord("pnd_Annual_Gross", "#ANNUAL-GROSS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sex = localVariables.newFieldInRecord("pnd_Sex", "#SEX", FieldType.STRING, 1);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 7);
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trans.reset();
        vw_cntrct.reset();

        localVariables.reset();
        pnd_Type_Call.setInitialValue("R");
        pnd_First.setInitialValue(true);
        pnd_Fund_2.setInitialValue("09");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap9920() throws Exception
    {
        super("Iaap9920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP9920", onError);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 0 ) LS = 180 PS = 0
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 180 PS = 0
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 180 PS = 0
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        pnd_Plan_Subplan_Pnd_Comma.getValue(1,":",20).setValue(",");                                                                                                      //Natural: MOVE ',' TO #COMMA ( 1:20 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20) || pnd_Input_Pnd_Record_Code.equals(30))))                       //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20 OR = 30
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS #CPR-BYPASS
                pnd_Cpr_Bypass.reset();
                if (condition(pnd_Write_It.getBoolean()))                                                                                                                 //Natural: IF #WRITE-IT
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-IT
                    sub_Write_It();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Ppg_Cde_5.notEquals(" ") || pnd_Input_Pnd_Plan_Nmbr.notEquals(" ")))                                                          //Natural: IF #PPG-CDE-5 NE ' ' OR #PLAN-NMBR NE ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Optn_Cde.equals(28) || pnd_Input_Pnd_Optn_Cde.equals(30) || pnd_Input_Pnd_Optn_Cde.equals(25) || pnd_Input_Pnd_Optn_Cde.equals(27))) //Natural: IF #OPTN-CDE = 28 OR = 30 OR = 25 OR = 27
                {
                    //*      PERFORM GET-TRANS-CDE
                    //*      IF #SKIP
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*      END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_H_Ppcn.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                                              //Natural: ASSIGN #H-PPCN := #PPCN-NBR
                pnd_H_Ppg.setValue(pnd_Input_Pnd_Ppg_Cde_5);                                                                                                              //Natural: ASSIGN #H-PPG := #PPG-CDE-5
                pnd_H_Opt.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                               //Natural: ASSIGN #H-OPT := #OPTN-CDE
                pnd_H_Plan.setValue(pnd_Input_Pnd_Plan_Nmbr);                                                                                                             //Natural: ASSIGN #H-PLAN := #PLAN-NMBR
                pnd_H_Issue_Date_Pnd_H_Iss_Dte.setValue(pnd_Input_Pnd_Issue_Dte_A);                                                                                       //Natural: ASSIGN #H-ISS-DTE := #ISSUE-DTE-A
                pnd_H_Issue_Date_Pnd_H_Iss_Dd.setValue(pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd);                                                                             //Natural: ASSIGN #H-ISS-DD := #W1-CNTRCT-ISSUE-DTE-DD
                if (condition(pnd_H_Issue_Date_Pnd_H_Iss_Dd.equals(getZero())))                                                                                           //Natural: IF #H-ISS-DD = 0
                {
                    pnd_H_Issue_Date_Pnd_H_Iss_Dd.setValue(1);                                                                                                            //Natural: ASSIGN #H-ISS-DD := 01
                }                                                                                                                                                         //Natural: END-IF
                pnd_H_Orig_Da_Cntrct_Nbr.setValue(pnd_Input_Pnd_Orig_Da_Cntrct_Nbr);                                                                                      //Natural: ASSIGN #H-ORIG-DA-CNTRCT-NBR := #ORIG-DA-CNTRCT-NBR
                pnd_1st_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                        //Natural: ASSIGN #1ST-DOD := #FIRST-ANN-DOD
                pnd_2nd_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                         //Natural: ASSIGN #2ND-DOD := #SCND-ANN-DOD
                pnd_Dt2.setValue(pnd_Input_Pnd_Dob_Yyyymm);                                                                                                               //Natural: ASSIGN #DT2 := #DOB-YYYYMM
                pnd_H_Dob.setValue(pnd_Input_Pnd_First_Ann_Dob_Dte);                                                                                                      //Natural: ASSIGN #H-DOB := #FIRST-ANN-DOB-DTE
                pnd_H_Py.setValue(pnd_Input_Pnd_Payee_Cde_A);                                                                                                             //Natural: ASSIGN #H-PY := #PAYEE-CDE-A
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Bypass.getBoolean()))                                                                                                                       //Natural: IF #BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                pnd_Cpr_Bypass.reset();                                                                                                                                   //Natural: RESET #CPR-BYPASS
                if (condition(pnd_Write_It.getBoolean()))                                                                                                                 //Natural: IF #WRITE-IT
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-IT
                    sub_Write_It();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Counter.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #COUNTER
                if (condition(pnd_Input_Pnd_Status_Cde.notEquals(1) || pnd_Input_Pnd_Pend_Cde.notEquals("0") || pnd_Input_Pnd_Payee_Cde.greater(2)))                      //Natural: IF #STATUS-CDE NE 1 OR #PEND-CDE NE '0' OR #PAYEE-CDE GT 02
                {
                    pnd_Cpr_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #CPR-BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_Ppcn_Nbr,"'Y'")))                                                                                         //Natural: IF #PPCN-NBR = MASK ( 'Y' )
                {
                    if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_Ppcn_Nbr,"'Y9'") || pnd_Input_Pnd_Cpr_Xfr_Iss_Dte.greater(getZero())))                                //Natural: IF #PPCN-NBR = MASK ( 'Y9' ) OR #CPR-XFR-ISS-DTE GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cpr_Bypass.setValue(true);                                                                                                                    //Natural: ASSIGN #CPR-BYPASS := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_Ppcn_Nbr,"'Z'")))                                                                                         //Natural: IF #PPCN-NBR = MASK ( 'Z' )
                {
                    if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_Ppcn_Nbr,"'Z9'") || pnd_Input_Pnd_Cpr_Xfr_Iss_Dte.greater(getZero())))                                //Natural: IF #PPCN-NBR = MASK ( 'Z9' ) OR #CPR-XFR-ISS-DTE GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cpr_Bypass.setValue(true);                                                                                                                    //Natural: ASSIGN #CPR-BYPASS := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.maskMatches(pnd_H_Dob_Pnd_H_Dob_A,"YYYYMMDD") && pnd_H_Dob.greater(18500000)))                                                      //Natural: IF #H-DOB-A = MASK ( YYYYMMDD ) AND #H-DOB GT 18500000
                {
                    pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_H_Dob_Pnd_H_Dob_A);                                                                       //Natural: MOVE EDITED #H-DOB-A TO #T-DTE ( EM = YYYYMMDD )
                    pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                                   //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                    pnd_Dt1.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                                //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #DT1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGE
                    sub_Calculate_Age();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Pnd_Ppcn_Nbr,"=",pnd_H_Dob_Pnd_H_Dob_A);                                                                          //Natural: WRITE '=' #PPCN-NBR '=' #H-DOB-A
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Iaan011.class , getCurrentProcessState(), pnd_H_Opt, pnd_Opt_Desc);                                                                       //Natural: CALLNAT 'IAAN011' #H-OPT #OPT-DESC
                if (condition(Global.isEscape())) return;
                pnd_Pin.setValue(pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                                               //Natural: ASSIGN #PIN := #CPR-ID-NBR
                //*  082017
                pnd_Pin_A.setValueEdited(pnd_Input_Pnd_Cpr_Id_Nbr,new ReportEditMask("ZZZZZ9999999"));                                                                    //Natural: MOVE EDITED #CPR-ID-NBR ( EM = ZZZZZ9999999 ) TO #PIN-A
                                                                                                                                                                          //Natural: PERFORM GET-NAME
                sub_Get_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_H_Ssn.equals(getZero())))                                                                                                               //Natural: IF #H-SSN = 0
                {
                    pnd_H_Ssn.setValue(pnd_Input_Pnd_Tax_Id_Nbr);                                                                                                         //Natural: ASSIGN #H-SSN := #TAX-ID-NBR
                }                                                                                                                                                         //Natural: END-IF
                pnd_H_Stts.setValue(pnd_Input_Pnd_Status_Cde);                                                                                                            //Natural: ASSIGN #H-STTS := #STATUS-CDE
                pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_H_Issue_Date);                                                                                //Natural: MOVE EDITED #H-ISSUE-DATE TO #T-DTE ( EM = YYYYMMDD )
                pnd_W_Dte_10.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                                  //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #W-DTE-10
                //*    IF #STATUS-CDE = 1
                //*      IF #PEND-CDE = 'R'
                //*        #STATUS := 'EXPIRED'
                //*      ELSE
                //*        #STATUS := 'ACTIVE'
                //*      END-IF
                //*    ELSE
                //*      #STATUS := 'TERMINATED'
                //*      RESET #YY1
                //*    END-IF
                short decideConditionsMet605 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #MODE-IND;//Natural: VALUE 100
                if (condition((pnd_Input_Pnd_Mode_Ind.equals(100))))
                {
                    decideConditionsMet605++;
                    pnd_Frqncy.setValue("MONTHLY");                                                                                                                       //Natural: ASSIGN #FRQNCY := 'MONTHLY'
                }                                                                                                                                                         //Natural: VALUE 601:603
                else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(601) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(603)))))
                {
                    decideConditionsMet605++;
                    pnd_Frqncy.setValue("QUARTERLY");                                                                                                                     //Natural: ASSIGN #FRQNCY := 'QUARTERLY'
                }                                                                                                                                                         //Natural: VALUE 701:706
                else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(701) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(706)))))
                {
                    decideConditionsMet605++;
                    pnd_Frqncy.setValue("SEMI-ANNUALLY");                                                                                                                 //Natural: ASSIGN #FRQNCY := 'SEMI-ANNUALLY'
                }                                                                                                                                                         //Natural: VALUE 801:812
                else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(801) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(812)))))
                {
                    decideConditionsMet605++;
                    pnd_Frqncy.setValue("ANNUALLY");                                                                                                                      //Natural: ASSIGN #FRQNCY := 'ANNUALLY'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Residency.reset();                                                                                                                                    //Natural: RESET #RESIDENCY
                DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Input_Pnd_Res, pnd_Residency);                //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL #RES #RESIDENCY
                if (condition(Global.isEscape())) return;
                getReports().display(1, pnd_Input_Pnd_Cntrct_Payee,pnd_Core_Cntrct_Payee);                                                                                //Natural: DISPLAY ( 1 ) #CNTRCT-PAYEE CNTRCT-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cpr_Bypass.getBoolean()))                                                                                                                   //Natural: IF #CPR-BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(30)))                                                                                                          //Natural: IF #RECORD-CODE = 30
            {
                if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("2") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("4") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("U")            //Natural: IF #SUMM-CMPNY-CDE = '2' OR = '4' OR = 'U' OR = 'W'
                    || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W")))
                {
                    pnd_Input_Pnd_Summ_Per_Dvdnd.reset();                                                                                                                 //Natural: RESET #SUMM-PER-DVDND
                    if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("4")))                                                  //Natural: IF #SUMM-CMPNY-CDE = 'W' OR = '4'
                    {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                        sub_Get_Auv();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Input_Pnd_Summ_Per_Pymnt.compute(new ComputeParameters(true, pnd_Input_Pnd_Summ_Per_Pymnt), pnd_Input_Pnd_Summ_Units.multiply(aian026_Pnd_Iuv)); //Natural: COMPUTE ROUNDED #SUMM-PER-PYMNT = #SUMM-UNITS * #IUV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pmts.compute(new ComputeParameters(false, pnd_Pmts), pnd_Pmts.add(pnd_Input_Pnd_Summ_Per_Pymnt).add(pnd_Input_Pnd_Summ_Per_Dvdnd));                   //Natural: COMPUTE #PMTS = #PMTS + #SUMM-PER-PYMNT + #SUMM-PER-DVDND
                pnd_Write_It.setValue(true);                                                                                                                              //Natural: ASSIGN #WRITE-IT := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-AGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IT
        //*  #PIN ';'
        //*  #H-SSN ';'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TRANS-CDE
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Calculate_Age() throws Exception                                                                                                                     //Natural: CALCULATE-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Dt1_Pnd_Yy1.nsubtract(pnd_Dt2_Pnd_Yy2);                                                                                                                       //Natural: SUBTRACT #YY2 FROM #YY1
        if (condition(pnd_Dt2_Pnd_Mm2.greater(pnd_Dt1_Pnd_Mm1)))                                                                                                          //Natural: IF #MM2 GT #MM1
        {
            pnd_Dt1_Pnd_Yy1.nsubtract(1);                                                                                                                                 //Natural: SUBTRACT 1 FROM #YY1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dt1_Pnd_Mm1.nsubtract(pnd_Dt2_Pnd_Mm2);                                                                                                                       //Natural: SUBTRACT #MM2 FROM #MM1
        if (condition(pnd_Dt1_Pnd_Mm1.lessOrEqual(getZero())))                                                                                                            //Natural: IF #MM1 LE 0
        {
            pnd_Dt1_Pnd_Mm1.nadd(12);                                                                                                                                     //Natural: ADD 12 TO #MM1
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Dt1_Pnd_Mm1.equals(12) || pnd_Dt1_Pnd_Mm1.equals(getZero())))                                                                                   //Natural: IF #MM1 = 12 OR = 0
        {
            pnd_Mo.reset();                                                                                                                                               //Natural: RESET #MO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mo.compute(new ComputeParameters(true, pnd_Mo), pnd_Dt1_Pnd_Mm1.divide(12));                                                                                  //Natural: COMPUTE ROUNDED #MO = #MM1 / 12
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, pnd_Pin,pnd_Input_Pnd_Cntrct_Payee,pnd_Core_Cntrct_Payee);                                                                                //Natural: DISPLAY ( 1 ) #PIN #CNTRCT-PAYEE CNTRCT-PAYEE
        if (Global.isEscape()) return;
        //*  082015 - START
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME #1ST-DOB-DTE #SEX
        pnd_1st_Dob_Dte.reset();
        pnd_Sex.reset();
        pnd_Found.setValue(false);                                                                                                                                        //Natural: ASSIGN #FOUND := FALSE
        if (condition(pnd_Core_Eof.getBoolean()))                                                                                                                         //Natural: IF #CORE-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * #PIN NE 9999999 AND #PIN NE 0                 /* 082017
        //*  082017
        if (condition(pnd_Pin.notEquals(new DbsDecimal("999999999999")) && pnd_Pin.notEquals(getZero())))                                                                 //Natural: IF #PIN NE 999999999999 AND #PIN NE 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Core_Pnd_Cor_Status_Cde.notEquals(" ")))                                                                                                    //Natural: IF #COR-STATUS-CDE NE ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Core_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                 //Natural: IF #CORE.CNTRCT-PAYEE GT #CNTRCT-PAYEE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Core_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                  //Natural: IF #CORE.CNTRCT-PAYEE = #CNTRCT-PAYEE
                {
                    pnd_Name.setValue(DbsUtil.compress(pnd_Core_Ph_First_Nme, pnd_Core_Ph_Mddle_Nme, pnd_Core_Ph_Last_Nme));                                              //Natural: COMPRESS PH-FIRST-NME PH-MDDLE-NME PH-LAST-NME INTO #NAME
                    if (condition((pnd_H_Dob.notEquals(pnd_Core_Ph_Dob_Dte) && pnd_Core_Ph_Dob_Dte.greater(18000000)) || pnd_Input_Pnd_Payee_Cde.greater(2)))             //Natural: IF ( #H-DOB NE PH-DOB-DTE AND PH-DOB-DTE GT 18000000 ) OR #PAYEE-CDE GT 02
                    {
                        pnd_F_Dte.setValue(pnd_Core_Ph_Dob_Dte);                                                                                                          //Natural: ASSIGN #F-DTE := PH-DOB-DTE
                        if (condition(DbsUtil.maskMatches(pnd_F_Dte,"YYYYMMDD")))                                                                                         //Natural: IF #F-DTE = MASK ( YYYYMMDD )
                        {
                            pnd_T_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_F_Dte);                                                                           //Natural: MOVE EDITED #F-DTE TO #T-DTE ( EM = YYYYMMDD )
                            pnd_Dob.setValueEdited(pnd_T_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                           //Natural: MOVE EDITED #T-DTE ( EM = MM/DD/YYYY ) TO #DOB
                            pnd_Dt2.setValue(pnd_F_Dte_Pnd_F_Yyyymm);                                                                                                     //Natural: ASSIGN #DT2 := #F-YYYYMM
                            pnd_Dt1.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #DT1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGE
                            sub_Calculate_Age();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Dt1_Pnd_Yy1.reset();                                                                                                                      //Natural: RESET #YY1
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(3, pnd_Core);                                                                                                                             //Natural: READ WORK FILE 3 ONCE #CORE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Core_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #CORE-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *IF #PIN NE 9999999 AND #PIN NE 0
        //*   READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE STARTING FROM #PIN-KEY
        //*     IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE #PIN OR
        //*         COR-XREF-PH.PH-RCD-TYPE-CDE NE 01
        //*       ESCAPE BOTTOM
        //*     END-IF
        //* *
        //*     COMPRESS PH-FIRST-NME PH-MDDLE-INIT PH-LAST-NME INTO #NAME
        //*     #SEX := PH-SEX-CDE
        //* *   IF #H-SSN = 0
        //*       #H-SSN := PH-SOCIAL-SECURITY-NO
        //* *   END-IF
        //*     IF #H-DOB NE PH-DOB-DTE AND PH-DOB-DTE GT 18500000
        //*       #F-DTE := PH-DOB-DTE
        //*       MOVE EDITED #F-DTE TO #T-DTE(EM=YYYYMMDD)
        //*       MOVE EDITED #T-DTE(EM=MM/DD/YYYY) TO #DOB
        //*       #DT2 := #F-YYYYMM
        //*       MOVE EDITED *DATX(EM=YYYYMM) TO #DT1
        //*       PERFORM CALCULATE-AGE
        //*     END-IF
        //*     ESCAPE BOTTOM
        //*   END-READ
        //* *END-IF
        //*  082015 - END
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        aian026_Pnd_Ia_Fund_Code.reset();                                                                                                                                 //Natural: RESET #IA-FUND-CODE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), aian026_Pnd_Ia_Fund_Code, pnd_Input_Pnd_Summ_Fund_Cde, pnd_Rtrn_Cde);                                  //Natural: CALLNAT 'IAAN0511' #IA-FUND-CODE #SUMM-FUND-CDE #RTRN-CDE
        if (condition(Global.isEscape())) return;
        aian026_Pnd_Call_Type.setValue("L");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'L'
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        aian026_Pnd_Uv_Req_Dte.setValue(99999999);                                                                                                                        //Natural: ASSIGN #UV-REQ-DTE := 99999999
        aian026_Pnd_Prtcptn_Dte.setValue(0);                                                                                                                              //Natural: ASSIGN #PRTCPTN-DTE := 0
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.greater(getZero())))                                                                                                            //Natural: IF #RTRN-CD GT 0
        {
            getReports().write(0, "NO AUV FOUND FOR:",aian026_Pnd_Ia_Fund_Code,pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde);                                           //Natural: WRITE 'NO AUV FOUND FOR:' #IA-FUND-CODE #PPCN-NBR #PAYEE-CDE
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_It() throws Exception                                                                                                                          //Natural: WRITE-IT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Status.equals("TERMINATED") || pnd_Status.equals("EXPIRED")))                                                                                   //Natural: IF #STATUS = 'TERMINATED' OR = 'EXPIRED'
        {
            pnd_Pmts.reset();                                                                                                                                             //Natural: RESET #PMTS #FRQNCY #PMTS-A
            pnd_Frqncy.reset();
            pnd_Pmts_A.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pmts_A.setValueEdited(pnd_Pmts,new ReportEditMask("Z,ZZZ,ZZ9.99"));                                                                                       //Natural: MOVE EDITED #PMTS ( EM = Z,ZZZ,ZZ9.99 ) TO #PMTS-A
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Plan_Subplan_Pnd_P_Subp.getValue("*").reset();                                                                                                                //Natural: RESET #P-SUBP ( * ) #J
        pnd_J.reset();
        if (condition(pnd_H_Plan.equals("MLTPLN")))                                                                                                                       //Natural: IF #H-PLAN = 'MLTPLN'
        {
            vw_cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) CNTRCT BY CNTRCT-PPCN-NBR = #H-PPCN
            (
            "READ02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_H_Ppcn, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cntrct.readNextRow("READ02")))
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 20
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                {
                    if (condition(cntrct_Lgcy_Plan_Nbr.getValue(pnd_I).equals(" ")))                                                                                      //Natural: IF LGCY-PLAN-NBR ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cntrct_Lgcy_Plan_Nbr.getValue(pnd_I).equals(pnd_Plan_Subplan_Pnd_P_Subp.getValue("*"))))                                            //Natural: IF LGCY-PLAN-NBR ( #I ) = #P-SUBP ( * )
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_J.nadd(1);                                                                                                                                //Natural: ADD 1 TO #J
                            pnd_Plan_Subplan_Pnd_P_Subp.getValue(pnd_J).setValue(cntrct_Lgcy_Plan_Nbr.getValue(pnd_I));                                                   //Natural: ASSIGN #P-SUBP ( #J ) := LGCY-PLAN-NBR ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_H_Plan.notEquals(" ")))                                                                                                                     //Natural: IF #H-PLAN NE ' '
            {
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                pnd_Plan_Subplan_Pnd_P_Subp.getValue(pnd_J).setValue(pnd_H_Plan);                                                                                         //Natural: ASSIGN #P-SUBP ( #J ) := #H-PLAN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_H_Ppg.notEquals(" ")))                                                                                                                      //Natural: IF #H-PPG NE ' '
            {
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                pnd_Plan_Subplan_Pnd_P_Subp.getValue(pnd_J).setValue(pnd_H_Ppg);                                                                                          //Natural: ASSIGN #P-SUBP ( #J ) := #H-PPG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  082017
        getWorkFiles().write(2, false, pnd_Pin_A, ";", pnd_H_Ppcn, ";", pnd_Name, ";", pnd_Dt1_Pnd_Yy1, ";", pnd_Dob, ";", pnd_W_Dte_10, ";", pnd_Pmts,                   //Natural: WRITE WORK FILE 2 #PIN-A ';' #H-PPCN ';' #NAME ';' #YY1 ';' #DOB ';' #W-DTE-10 ';' #PMTS ';' #FRQNCY ';' #OPT-DESC ';' #PLAN-SUBPLAN ( * )
            ";", pnd_Frqncy, ";", pnd_Opt_Desc, ";", pnd_Plan_Subplan.getValue("*"));
        pnd_Write_It.reset();                                                                                                                                             //Natural: RESET #WRITE-IT #PMTS
        pnd_Pmts.reset();
    }
    private void sub_Get_Trans_Cde() throws Exception                                                                                                                     //Natural: GET-TRANS-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                         //Natural: ASSIGN #TRANS-PPCN-NBR := #PPCN-NBR
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(1);                                                                                                             //Natural: ASSIGN #TRANS-PAYEE-CDE := 01
        pnd_Skip.setValue(true);                                                                                                                                          //Natural: ASSIGN #SKIP := TRUE
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_trans.readNextRow("READ03")))
        {
            if (condition(trans_Trans_Ppcn_Nbr.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr) || trans_Trans_Payee_Cde.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR NE #TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(trans_Trans_Cde.equals(30) || (trans_Trans_Cde.equals(31) && pnd_Input_Pnd_Issue_Dte.greaterOrEqual(200401))))                                  //Natural: IF TRANS-CDE = 030 OR ( TRANS-CDE = 31 AND #ISSUE-DTE GE 200401 )
            {
                pnd_Skip.reset();                                                                                                                                         //Natural: RESET #SKIP
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, "DOB ERROR",pnd_H_Ppcn,"=",pnd_Core_Ph_Dob_Dte);                                                                                            //Natural: WRITE ( 1 ) 'DOB ERROR' #H-PPCN '=' #CORE.PH-DOB-DTE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=180 PS=0");
        Global.format(1, "LS=180 PS=0");
        Global.format(2, "LS=180 PS=0");

        getReports().setDisplayColumns(1, pnd_Input_Pnd_Cntrct_Payee,pnd_Core_Cntrct_Payee);
    }
}
