/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:03 PM
**        * FROM NATURAL PROGRAM : Iatp369
************************************************************
**        * FILE NAME            : Iatp369.java
**        * CLASS NAME           : Iatp369
**        * INSTANCE NAME        : Iatp369
************************************************************
************************************************************************
* PROGRAM  : IATP369
* SYSTEM   : IAS
* TITLE    : IA POST RETIREMENT FLEXIBILITIES
* GENERATED: 03/1999
* FUNCTION : THIS PROGRAM PRODUCES THE IA DAILY TRANSFER REPORT USING
*            A WORK FILE, CREATED IN IATP360, AS INPUT.
*    MAPS USED ARE IATM361H - HEADINGS, PAGE COUNT, DATE AND TIME
*                  IATM369I - DETAIL LINE HEADINGS
*                  IATM369D - DETAIL LINES
*                  IATM369T - TOTAL PAGE
*
*    WORK FILE 1 :- ALL TRANSFER RECORDS ADDED TODAY
*    WORK FILE 2 :- ALL TRANSFER RECORDS DELETED TODAY BUT ADDED ON A
*                   PREVIOUS DAY. (WORK RECORD 6 IN IATP360)
*    WORK FILE 5 :- RUN DATE
*
*
* HISTORY
* AUG. 27, 1997  TMCK. SUB TOTAL MAP AND DATA NOT USED AT THIS TIME
*
* 11/97 LEN B : ADDED THE STATUS OF DELETED RECORDS TO THE REPORT
*               ADDED READ TO WORK FILE 5 TO RETRIEVE REPORT DATE
*
* 3/99 DEBBIE A. - COPIED MODULE IATP361 AND NAMED IT IATP369 FOR
*                  THE TIAA TO CREF TRANSFERS PROCESSING.
*                  THERE ARE TWO NEW REPORTS ( DAILY AND CUMULATIVE)
*                  FOR THE TIAA TO CREF TRANSFERS.
*
* 5/99 RM        - ADDED LOGIC TO REPORT UNIT-IDS
*
*                  DO SCAN ON 5/99
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* AUG 2017 R.CARREON      PIN EXPANSION - EXPAND SOME PIN FIELDS
*                                         AND REALIGNED INFO IN MAPS
*                                         IATM369DAND IATM3691
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp369 extends BLNatBase
{
    // Data Areas
    private PdaIata201 pdaIata201;
    private LdaIatl361 ldaIatl361;
    private LdaIatl300 ldaIatl300;
    private PdaIata36x pdaIata36x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_Acct;
    private DbsField pnd_Report_Date_A8;

    private DbsGroup pnd_Report_Date_A8__R_Field_1;
    private DbsField pnd_Report_Date_A8_Pnd_Report_Date_N8;
    private DbsField pnd_Bsns_Date;

    private DbsGroup pnd_Bsns_Date__R_Field_2;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_A;

    private DbsGroup pnd_Bsns_Date__R_Field_3;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Mm;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Dd;
    private DbsField pnd_Currnt_Dte;

    private DbsGroup pnd_Currnt_Dte__R_Field_4;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd;

    private DbsGroup pnd_Currnt_Dte__R_Field_5;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_N;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_6;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yyyy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_7;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Rqst_Date;

    private DbsGroup pnd_Rqst_Date__R_Field_8;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Mm;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Dd;

    private DbsGroup pnd_Rqst_Date__R_Field_9;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_N;
    private DbsField pnd_Cntct_Mthd;
    private DbsField pnd_Eval_Mthd;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_I4;
    private DbsField pnd_First_Time;
    private DbsField pnd_In_Detail;
    private DbsField pnd_Prior_Deletion;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M361h_Busns_Dte;

    private DbsGroup pnd_M361h_Busns_Dte__R_Field_10;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy;
    private DbsField pnd_M361h_Page;
    private DbsField pnd_Program;
    private DbsField pnd_Rev_Mthd_Na;
    private DbsField pnd_M361i_Cntct_Mthd;
    private DbsField pnd_M361i_Eval_Mthd;
    private DbsField pnd_M361i_Stat_1;
    private DbsField pnd_M361i_Stat_2;

    private DbsGroup iatm361d_Dtl;
    private DbsField iatm361d_Dtl_Pnd_M361d_Eff_Dte_Ot;
    private DbsField iatm361d_Dtl_Pnd_M369d_Frm_Cntrct;
    private DbsField iatm361d_Dtl_Pnd_M361d_Xfr_Opt;
    private DbsField iatm361d_Dtl_Pnd_M361d_Frm_Fnd;
    private DbsField iatm361d_Dtl_Pnd_M361d_Frm_Qunty;
    private DbsField iatm361d_Dtl_Pnd_M361d_Frm_Typ;
    private DbsField iatm361d_Dtl_Pnd_M361d_Prtcpnt_Nme;
    private DbsField iatm361d_Dtl_Pnd_M361d_Rcvd_Dte_Ot;
    private DbsField iatm361d_Dtl_Pnd_M369d_To_Cntrct;
    private DbsField iatm361d_Dtl_Pnd_M361d_To_Fnd;
    private DbsField iatm361d_Dtl_Pnd_M361d_To_Qunty;
    private DbsField iatm361d_Dtl_Pnd_M361d_To_Typ;
    private DbsField iatm361d_Dtl_Pnd_M361d_Unique_Id;
    private DbsField iatm361d_Dtl_Pnd_M361d_Status;
    private DbsField pnd_M361t_Anul_Mthd_Amt;
    private DbsField pnd_M361t_Cntct_At_Amt;
    private DbsField pnd_M361t_Cntct_Cs_Amt;
    private DbsField pnd_M361t_Cntct_Fx_Amt;
    private DbsField pnd_M361t_Cntct_Ig_Amt;
    private DbsField pnd_M361t_Cntct_In_Amt;
    private DbsField pnd_M361t_Cntct_Ml_Amt;
    private DbsField pnd_M361t_Cntct_Vr_Amt;
    private DbsField pnd_M361t_Cntct_Vs_Amt;
    private DbsField pnd_M361t_M_T_M_Acct_Amt;
    private DbsField pnd_M361t_M_T_O_Acct_Amt;
    private DbsField pnd_M361t_Mnth_Mthd_Amt;
    private DbsField pnd_M361t_Graded;
    private DbsField pnd_M361t_O_T_M_Acct_Amt;
    private DbsField pnd_M361t_O_T_O_Acct_Amt;
    private DbsField pnd_M361t_Stat_Af_Amt;
    private DbsField pnd_M361t_Stat_Cm_Amt;
    private DbsField pnd_M361t_Stat_Dy_Amt;
    private DbsField pnd_M361t_Stat_De_Amt;
    private DbsField pnd_M361t_Stat_Pa_Amt;
    private DbsField pnd_M361t_Stat_Ra_Amt;
    private DbsField pnd_M361t_Stat_So_Amt;
    private DbsField pnd_M361t_Stat_Rj_Amt;
    private DbsField pnd_M361t_Prior_Del;
    private DbsField pnd_M361t_Totl_Rqst_Amt;
    private DbsField pnd_M361t_Repet_Rqst_Amt;
    private DbsField pnd_M361t_Single_Rqst_Amt;
    private DbsField pnd_M361t_Ttl_Prtcpnts_Amt;
    private DbsField pnd_M361t_Ttl_Rqst_Pcnt_Amt;
    private DbsField pnd_M361t_Ttl_Rqsts_Dlrs_Amt;
    private DbsField pnd_M361t_Ttl_Rqsts_Unts_Amt;
    private DbsField tbl_Cntct_Mthd_Cde;
    private DbsField tbl_Cntct_Mthd_Litrl;
    private DbsField pnd_Cmpr_Pin;
    private DbsField pnd_Nbr_Frm_P;
    private DbsField pnd_Nbr_To_P;
    private DbsField pnd_Page_Cntrl;
    private DbsField pnd_Stat_1_2;

    private DbsGroup pnd_Stat_1_2__R_Field_11;
    private DbsField pnd_Stat_1_2_Pnd_Stat_1;
    private DbsField pnd_Stat_1_2_Pnd_Stat_2;

    private DbsGroup msg_Info_Sub;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg;

    private DbsGroup msg_Info_Sub__R_Field_12;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Display;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data;

    private DbsGroup msg_Info_Sub__R_Field_13;

    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data_Char;
    private DbsField msg_Info_Sub_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata201 = new PdaIata201(localVariables);
        ldaIatl361 = new LdaIatl361();
        registerRecord(ldaIatl361);
        ldaIatl300 = new LdaIatl300();
        registerRecord(ldaIatl300);
        pdaIata36x = new PdaIata36x(localVariables);

        // Local Variables
        pnd_Max_Acct = localVariables.newFieldInRecord("pnd_Max_Acct", "#MAX-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Report_Date_A8 = localVariables.newFieldInRecord("pnd_Report_Date_A8", "#REPORT-DATE-A8", FieldType.STRING, 8);

        pnd_Report_Date_A8__R_Field_1 = localVariables.newGroupInRecord("pnd_Report_Date_A8__R_Field_1", "REDEFINE", pnd_Report_Date_A8);
        pnd_Report_Date_A8_Pnd_Report_Date_N8 = pnd_Report_Date_A8__R_Field_1.newFieldInGroup("pnd_Report_Date_A8_Pnd_Report_Date_N8", "#REPORT-DATE-N8", 
            FieldType.NUMERIC, 8);
        pnd_Bsns_Date = localVariables.newFieldInRecord("pnd_Bsns_Date", "#BSNS-DATE", FieldType.NUMERIC, 8);

        pnd_Bsns_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Bsns_Date__R_Field_2", "REDEFINE", pnd_Bsns_Date);
        pnd_Bsns_Date_Pnd_Bsns_Date_A = pnd_Bsns_Date__R_Field_2.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_A", "#BSNS-DATE-A", FieldType.STRING, 8);

        pnd_Bsns_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Bsns_Date__R_Field_3", "REDEFINE", pnd_Bsns_Date);
        pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy", "#BSNS-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Bsns_Date_Pnd_Bsns_Date_Mm = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Mm", "#BSNS-DATE-MM", FieldType.STRING, 
            2);
        pnd_Bsns_Date_Pnd_Bsns_Date_Dd = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Dd", "#BSNS-DATE-DD", FieldType.STRING, 
            2);
        pnd_Currnt_Dte = localVariables.newFieldInRecord("pnd_Currnt_Dte", "#CURRNT-DTE", FieldType.STRING, 8);

        pnd_Currnt_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_4", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy", "#CURRNT-DTE-CY", FieldType.STRING, 
            4);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm", "#CURRNT-DTE-MM", FieldType.STRING, 
            2);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd", "#CURRNT-DTE-DD", FieldType.STRING, 
            2);

        pnd_Currnt_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_5", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_N = pnd_Currnt_Dte__R_Field_5.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_N", "#CURRNT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_6", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Yyyy = pnd_Eff_Date__R_Field_6.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yyyy", "#EFF-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_6.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_6.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_7", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_7.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rqst_Date = localVariables.newFieldInRecord("pnd_Rqst_Date", "#RQST-DATE", FieldType.STRING, 8);

        pnd_Rqst_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_8", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy = pnd_Rqst_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy", "#RQST-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Rqst_Date_Pnd_Rqst_Date_Mm = pnd_Rqst_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Mm", "#RQST-DATE-MM", FieldType.STRING, 
            2);
        pnd_Rqst_Date_Pnd_Rqst_Date_Dd = pnd_Rqst_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Dd", "#RQST-DATE-DD", FieldType.STRING, 
            2);

        pnd_Rqst_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_9", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_N = pnd_Rqst_Date__R_Field_9.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_N", "#RQST-DATE-N", FieldType.NUMERIC, 8);
        pnd_Cntct_Mthd = localVariables.newFieldInRecord("pnd_Cntct_Mthd", "#CNTCT-MTHD", FieldType.STRING, 1);
        pnd_Eval_Mthd = localVariables.newFieldInRecord("pnd_Eval_Mthd", "#EVAL-MTHD", FieldType.STRING, 1);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.PACKED_DECIMAL, 3);
        pnd_I4 = localVariables.newFieldInRecord("pnd_I4", "#I4", FieldType.PACKED_DECIMAL, 3);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_In_Detail = localVariables.newFieldInRecord("pnd_In_Detail", "#IN-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Prior_Deletion = localVariables.newFieldInRecord("pnd_Prior_Deletion", "#PRIOR-DELETION", FieldType.BOOLEAN, 1);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M361h_Busns_Dte = localVariables.newFieldInRecord("pnd_M361h_Busns_Dte", "#M361H-BUSNS-DTE", FieldType.NUMERIC, 8);

        pnd_M361h_Busns_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_M361h_Busns_Dte__R_Field_10", "REDEFINE", pnd_M361h_Busns_Dte);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm", "#M361H-BUSNS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd", "#M361H-BUSNS-DTE-DD", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy", "#M361H-BUSNS-DTE-CY", 
            FieldType.STRING, 4);
        pnd_M361h_Page = localVariables.newFieldInRecord("pnd_M361h_Page", "#M361H-PAGE", FieldType.NUMERIC, 5);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Rev_Mthd_Na = localVariables.newFieldInRecord("pnd_Rev_Mthd_Na", "#REV-MTHD-NA", FieldType.STRING, 40);
        pnd_M361i_Cntct_Mthd = localVariables.newFieldInRecord("pnd_M361i_Cntct_Mthd", "#M361I-CNTCT-MTHD", FieldType.STRING, 20);
        pnd_M361i_Eval_Mthd = localVariables.newFieldInRecord("pnd_M361i_Eval_Mthd", "#M361I-EVAL-MTHD", FieldType.STRING, 40);
        pnd_M361i_Stat_1 = localVariables.newFieldInRecord("pnd_M361i_Stat_1", "#M361I-STAT-1", FieldType.STRING, 4);
        pnd_M361i_Stat_2 = localVariables.newFieldInRecord("pnd_M361i_Stat_2", "#M361I-STAT-2", FieldType.STRING, 4);

        iatm361d_Dtl = localVariables.newGroupInRecord("iatm361d_Dtl", "IATM361D-DTL");
        iatm361d_Dtl_Pnd_M361d_Eff_Dte_Ot = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Eff_Dte_Ot", "#M361D-EFF-DTE-OT", FieldType.STRING, 10);
        iatm361d_Dtl_Pnd_M369d_Frm_Cntrct = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M369d_Frm_Cntrct", "#M369D-FRM-CNTRCT", FieldType.STRING, 9);
        iatm361d_Dtl_Pnd_M361d_Xfr_Opt = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Xfr_Opt", "#M361D-XFR-OPT", FieldType.STRING, 1);
        iatm361d_Dtl_Pnd_M361d_Frm_Fnd = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Frm_Fnd", "#M361D-FRM-FND", FieldType.STRING, 1);
        iatm361d_Dtl_Pnd_M361d_Frm_Qunty = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Frm_Qunty", "#M361D-FRM-QUNTY", FieldType.NUMERIC, 11, 
            2);
        iatm361d_Dtl_Pnd_M361d_Frm_Typ = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Frm_Typ", "#M361D-FRM-TYP", FieldType.STRING, 1);
        iatm361d_Dtl_Pnd_M361d_Prtcpnt_Nme = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Prtcpnt_Nme", "#M361D-PRTCPNT-NME", FieldType.STRING, 
            30);
        iatm361d_Dtl_Pnd_M361d_Rcvd_Dte_Ot = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Rcvd_Dte_Ot", "#M361D-RCVD-DTE-OT", FieldType.STRING, 
            10);
        iatm361d_Dtl_Pnd_M369d_To_Cntrct = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M369d_To_Cntrct", "#M369D-TO-CNTRCT", FieldType.STRING, 9);
        iatm361d_Dtl_Pnd_M361d_To_Fnd = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_To_Fnd", "#M361D-TO-FND", FieldType.STRING, 1);
        iatm361d_Dtl_Pnd_M361d_To_Qunty = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_To_Qunty", "#M361D-TO-QUNTY", FieldType.NUMERIC, 11, 2);
        iatm361d_Dtl_Pnd_M361d_To_Typ = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_To_Typ", "#M361D-TO-TYP", FieldType.STRING, 1);
        iatm361d_Dtl_Pnd_M361d_Unique_Id = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Unique_Id", "#M361D-UNIQUE-ID", FieldType.STRING, 12);
        iatm361d_Dtl_Pnd_M361d_Status = iatm361d_Dtl.newFieldInGroup("iatm361d_Dtl_Pnd_M361d_Status", "#M361D-STATUS", FieldType.STRING, 4);
        pnd_M361t_Anul_Mthd_Amt = localVariables.newFieldInRecord("pnd_M361t_Anul_Mthd_Amt", "#M361T-ANUL-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_At_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_At_Amt", "#M361T-CNTCT-AT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Cs_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Cs_Amt", "#M361T-CNTCT-CS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Fx_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Fx_Amt", "#M361T-CNTCT-FX-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Ig_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Ig_Amt", "#M361T-CNTCT-IG-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_In_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_In_Amt", "#M361T-CNTCT-IN-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Ml_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Ml_Amt", "#M361T-CNTCT-ML-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Vr_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Vr_Amt", "#M361T-CNTCT-VR-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Cntct_Vs_Amt = localVariables.newFieldInRecord("pnd_M361t_Cntct_Vs_Amt", "#M361T-CNTCT-VS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_M_T_M_Acct_Amt = localVariables.newFieldInRecord("pnd_M361t_M_T_M_Acct_Amt", "#M361T-M-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_M_T_O_Acct_Amt = localVariables.newFieldInRecord("pnd_M361t_M_T_O_Acct_Amt", "#M361T-M-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Mnth_Mthd_Amt = localVariables.newFieldInRecord("pnd_M361t_Mnth_Mthd_Amt", "#M361T-MNTH-MTHD-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Graded = localVariables.newFieldInRecord("pnd_M361t_Graded", "#M361T-GRADED", FieldType.NUMERIC, 9);
        pnd_M361t_O_T_M_Acct_Amt = localVariables.newFieldInRecord("pnd_M361t_O_T_M_Acct_Amt", "#M361T-O-T-M-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_O_T_O_Acct_Amt = localVariables.newFieldInRecord("pnd_M361t_O_T_O_Acct_Amt", "#M361T-O-T-O-ACCT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Af_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Af_Amt", "#M361T-STAT-AF-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Cm_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Cm_Amt", "#M361T-STAT-CM-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Dy_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Dy_Amt", "#M361T-STAT-DY-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_De_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_De_Amt", "#M361T-STAT-DE-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Pa_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Pa_Amt", "#M361T-STAT-PA-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Ra_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Ra_Amt", "#M361T-STAT-RA-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_So_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_So_Amt", "#M361T-STAT-SO-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Stat_Rj_Amt = localVariables.newFieldInRecord("pnd_M361t_Stat_Rj_Amt", "#M361T-STAT-RJ-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Prior_Del = localVariables.newFieldInRecord("pnd_M361t_Prior_Del", "#M361T-PRIOR-DEL", FieldType.NUMERIC, 9);
        pnd_M361t_Totl_Rqst_Amt = localVariables.newFieldInRecord("pnd_M361t_Totl_Rqst_Amt", "#M361T-TOTL-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Repet_Rqst_Amt = localVariables.newFieldInRecord("pnd_M361t_Repet_Rqst_Amt", "#M361T-REPET-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Single_Rqst_Amt = localVariables.newFieldInRecord("pnd_M361t_Single_Rqst_Amt", "#M361T-SINGLE-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Prtcpnts_Amt = localVariables.newFieldInRecord("pnd_M361t_Ttl_Prtcpnts_Amt", "#M361T-TTL-PRTCPNTS-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Rqst_Pcnt_Amt = localVariables.newFieldInRecord("pnd_M361t_Ttl_Rqst_Pcnt_Amt", "#M361T-TTL-RQST-PCNT-AMT", FieldType.NUMERIC, 9);
        pnd_M361t_Ttl_Rqsts_Dlrs_Amt = localVariables.newFieldInRecord("pnd_M361t_Ttl_Rqsts_Dlrs_Amt", "#M361T-TTL-RQSTS-DLRS-AMT", FieldType.NUMERIC, 
            9);
        pnd_M361t_Ttl_Rqsts_Unts_Amt = localVariables.newFieldInRecord("pnd_M361t_Ttl_Rqsts_Unts_Amt", "#M361T-TTL-RQSTS-UNTS-AMT", FieldType.NUMERIC, 
            9);
        tbl_Cntct_Mthd_Cde = localVariables.newFieldArrayInRecord("tbl_Cntct_Mthd_Cde", "TBL-CNTCT-MTHD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        tbl_Cntct_Mthd_Litrl = localVariables.newFieldArrayInRecord("tbl_Cntct_Mthd_Litrl", "TBL-CNTCT-MTHD-LITRL", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Cmpr_Pin = localVariables.newFieldInRecord("pnd_Cmpr_Pin", "#CMPR-PIN", FieldType.NUMERIC, 12);
        pnd_Nbr_Frm_P = localVariables.newFieldInRecord("pnd_Nbr_Frm_P", "#NBR-FRM-P", FieldType.PACKED_DECIMAL, 3);
        pnd_Nbr_To_P = localVariables.newFieldInRecord("pnd_Nbr_To_P", "#NBR-TO-P", FieldType.PACKED_DECIMAL, 3);
        pnd_Page_Cntrl = localVariables.newFieldInRecord("pnd_Page_Cntrl", "#PAGE-CNTRL", FieldType.PACKED_DECIMAL, 3);
        pnd_Stat_1_2 = localVariables.newFieldInRecord("pnd_Stat_1_2", "#STAT-1-2", FieldType.STRING, 2);

        pnd_Stat_1_2__R_Field_11 = localVariables.newGroupInRecord("pnd_Stat_1_2__R_Field_11", "REDEFINE", pnd_Stat_1_2);
        pnd_Stat_1_2_Pnd_Stat_1 = pnd_Stat_1_2__R_Field_11.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_1", "#STAT-1", FieldType.STRING, 1);
        pnd_Stat_1_2_Pnd_Stat_2 = pnd_Stat_1_2__R_Field_11.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_2", "#STAT-2", FieldType.STRING, 1);

        msg_Info_Sub = localVariables.newGroupInRecord("msg_Info_Sub", "MSG-INFO-SUB");
        msg_Info_Sub_Pnd_Pnd_Msg = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);

        msg_Info_Sub__R_Field_12 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_12", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg);
        msg_Info_Sub_Pnd_Pnd_Msg_Display = msg_Info_Sub__R_Field_12.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Display", "##MSG-DISPLAY", FieldType.STRING, 
            78);
        msg_Info_Sub_Pnd_Pnd_Msg_Nr = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Sub_Pnd_Pnd_Msg_Data = msg_Info_Sub.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));

        msg_Info_Sub__R_Field_13 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_13", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg_Data);

        msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct = msg_Info_Sub__R_Field_13.newGroupArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1, 3));
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Char = msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1, 32));
        msg_Info_Sub_Pnd_Pnd_Return_Code = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Sub_Pnd_Pnd_Error_Field = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index1 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index2 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index3 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIatl361.initializeValues();
        ldaIatl300.initializeValues();

        localVariables.reset();
        pnd_Max_Acct.setInitialValue(20);
        pnd_First_Time.setInitialValue(true);
        pnd_In_Detail.setInitialValue(true);
        pnd_Header1.setInitialValue("            POST SETTLEMENT FLEXIBILITIES ");
        pnd_Header2.setInitialValue(" DAILY - TIAA TO CREF TRANSFER REQUEST REPORT");
        pnd_Rev_Mthd_Na.setInitialValue("N/A For Graded to Standard Requests");
        pnd_M361i_Cntct_Mthd.setInitialValue(" ");
        pnd_M361i_Eval_Mthd.setInitialValue(" ");
        pnd_M361i_Stat_1.setInitialValue("Stat");
        pnd_M361i_Stat_2.setInitialValue("----");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp369() throws Exception
    {
        super("Iatp369");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ======================================================================
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: ASSIGN #PROGRAM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM SET-REPORT-DATE
        sub_Set_Report_Date();
        if (condition(Global.isEscape())) {return;}
        //*  <---ADDED 5/99
        DbsUtil.callnat(Iatn36x.class , getCurrentProcessState(), pdaIata36x.getIata36x());                                                                               //Natural: CALLNAT 'IATN36X' IATA36X
        if (condition(Global.isEscape())) return;
        //*   TOP OF PAGE
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 1 )
        //* **==> ADDED FOLLOWING LINES  5/99
        //* *
        R1:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_I1.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I1
            if (condition(pnd_I1.greater(pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids())))                                                                               //Natural: IF #I1 GT IATA36X-NBR-OF-UNIT-IDS
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //* * END OF ADD  5/99
            //*  PROCESS THE RECORDS IN THE WORK FILE
            //*  ALL TRANSFER REQUESTS ADDED TODAY
            boolean endOfDataReadwork01 = true;                                                                                                                           //Natural: READ WORK FILE 1 IAT-TRNSFR-RPT
            boolean firstReadwork01 = true;
            READWORK01:
            while (condition(getWorkFiles().read(1, ldaIatl361.getIat_Trnsfr_Rpt())))
            {
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom()))
                            break;
                        else if (condition(Global.isEscapeBottomImmediate()))
                        {
                            endOfDataReadwork01 = false;
                            break;
                        }
                        else if (condition(Global.isEscapeTop()))
                        continue;
                        else if (condition())
                        return;
                    }
                }
                //*  TIAA TO CREF TRANSFERS WILL BE PROCESSED BY THIS MODULE,
                //*  THIS TEST WILL BYPASS ALL OTHER TRANSFERS.
                //*  ADDED 5/99 SELECT ONLY
                if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().equals(" ")))                                                                                  //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE = ' '
                {
                    //*  NON BLANK TIAA TO CREF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  XFRS
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *** ADDED FOLLOWING IF TO BYPASS REPORTING COMPLETED REQUESTS 10/99
                pnd_Eff_Date.setValueEdited(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
                //*  M = COMPLETE DO REPORT THESE
                if (condition(pnd_Eff_Date_Pnd_Eff_Date_N.less(pnd_Currnt_Dte_Pnd_Currnt_Dte_N) && DbsUtil.maskMatches(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde(),       //Natural: IF #EFF-DATE-N LT #CURRNT-DTE-N AND XFR-STTS-CDE = MASK ( 'M' )
                    "'M'")))
                {
                    //*  AFTER CURRENT DATE PAST EFFECT-DTE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* * END OF ADD  10/99
                //* *
                //* ** ADDED FOLLOWING IF STMNTS. TO PROCESS ONLY EXISTING REQUESTS  5/99
                if (condition(DbsUtil.maskMatches(pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1),"'ALL'")))                                                    //Natural: IF IATA36X-UNIT-CDE ( #I1 ) = MASK ( 'ALL' )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(!pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt()).equals(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Unit_Cde().getSubstring(1, //Natural: IF SUBSTR ( IATA36X-UNIT-CDE ( #I1 ) ,1,IATA36X-LNGTH ) NE SUBSTR ( RQST-UNIT-CDE,1,IATA36X-LNGTH )
                        pdaIata36x.getIata36x_Iata36x_Lngth().getInt()))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ** END OF ADD  5/99
                pnd_M361t_Totl_Rqst_Amt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #M361T-TOTL-RQST-AMT
                if (condition(pnd_First_Time.getBoolean()))                                                                                                               //Natural: IF #FIRST-TIME
                {
                    pnd_First_Time.reset();                                                                                                                               //Natural: RESET #FIRST-TIME
                    pnd_Cmpr_Pin.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id());                                                                                   //Natural: ASSIGN #CMPR-PIN := IA-UNIQUE-ID
                    pnd_M361t_Ttl_Prtcpnts_Amt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #M361T-TTL-PRTCPNTS-AMT
                    //*    IF  IAT-TRNSFR-RPT.REVAL-MTHD = 'A'   /* COMMENTED THIS CODE 5/99
                    //*      #M361I-EVAL-MTHD := 'Annual'
                    //*    ELSE
                    //*      IF  IAT-TRNSFR-RPT.REVAL-MTHD = 'M'
                    //*        #M361I-EVAL-MTHD := 'Monthly'
                    //*      ELSE
                    //*        #M361I-EVAL-MTHD := #REV-MTHD-NA
                    //*      END-IF
                    //*    END-IF                                /* END OF COMMENTED  5/99
                    //*  GET CONTACT MODE DESCRIPTION.
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MODE
                    sub_Get_Contact_Mode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(msg_Info_Sub_Pnd_Pnd_Return_Code.equals(" ")))                                                                                          //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
                    {
                        //*      #M361I-CNTCT-MTHD := #SHORT-DESC
                        pnd_M361i_Cntct_Mthd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaIata201.getIata201_Pnd_Long_Desc()));                            //Natural: COMPRESS #LONG-DESC INTO #M361I-CNTCT-MTHD LEAVING NO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_M361i_Cntct_Mthd.setValue("CNTCT MTHD NOT ON FILE");                                                                                          //Natural: ASSIGN #M361I-CNTCT-MTHD := 'CNTCT MTHD NOT ON FILE'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FIRST TIME READ OF WORK FILE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().equals("0")))                                                                                  //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE = '0'
                {
                    pnd_M361t_Single_Rqst_Amt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #M361T-SINGLE-RQST-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().greater("0")))                                                                             //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE GT '0'
                    {
                        pnd_M361t_Repet_Rqst_Amt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #M361T-REPET-RQST-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_M361t_Single_Rqst_Amt.nadd(1);                                                                                                                //Natural: ADD 1 TO #M361T-SINGLE-RQST-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: AT BREAK OF IAT-TRNSFR-RPT.RQST-CNTCT-MDE
                //*  REVALUATION METHOD
                //*  AT BREAK OF IAT-TRNSFR-RPT.REVAL-MTHD  /* COMMENTED BREAK LOGIC 5/99
                //*    /*
                //*    IF  IAT-TRNSFR-RPT.REVAL-MTHD = 'A'  /* THIS FIELD ALWAYS BLANK FOR
                //*      #M361I-EVAL-MTHD := 'Annual'       /* TIAA TO CREF XFRS
                //*    ELSE
                //*      IF  IAT-TRNSFR-RPT.REVAL-MTHD = 'M'
                //*        #M361I-EVAL-MTHD := 'Monthly'
                //*      ELSE
                //*        #M361I-EVAL-MTHD := #REV-MTHD-NA
                //*      END-IF
                //*    END-IF
                //*    RESET #PAGE-CNTRL
                //*    NEWPAGE (1)
                //*  END-BREAK
                //*  PIN
                if (condition(pnd_Cmpr_Pin.notEquals(ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id())))                                                                       //Natural: IF #CMPR-PIN NE IA-UNIQUE-ID
                {
                    pnd_M361t_Ttl_Prtcpnts_Amt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #M361T-TTL-PRTCPNTS-AMT
                    pnd_Cmpr_Pin.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id());                                                                                   //Natural: ASSIGN #CMPR-PIN := IA-UNIQUE-ID
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-DETAIL-LINE
                sub_Pnd_Write_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-WORK
            READWORK01_Exit:
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01(endOfDataReadwork01);
            }
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WORK FILE 2 = WORK FILE 6 WRITTEN IN IATP360
            //*  WORK FILE 2 CONTAINS ALL THE TRANSFER REQUESTS THAT WERE DELETED
            //*  TODAY BUT ADDED ON A PREVIOUS DAY.
            pnd_First_Time.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-TIME := TRUE
            pnd_Prior_Deletion.setValue(true);                                                                                                                            //Natural: ASSIGN #PRIOR-DELETION := TRUE
            READWORK02:                                                                                                                                                   //Natural: READ WORK FILE 2 IAT-TRNSFR-RPT
            while (condition(getWorkFiles().read(2, ldaIatl361.getIat_Trnsfr_Rpt())))
            {
                if (condition(pnd_First_Time.getBoolean()))                                                                                                               //Natural: IF #FIRST-TIME
                {
                    pnd_First_Time.reset();                                                                                                                               //Natural: RESET #FIRST-TIME
                    pnd_Page_Cntrl.reset();                                                                                                                               //Natural: RESET #PAGE-CNTRL
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED 5/99
                if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().equals(" ")))                                                                                  //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* ** ADDED FOLLOWING IF STMNTS. TO PROCESS ONLY EXISTING REQUESTS  5/99
                if (condition(DbsUtil.maskMatches(pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1),"'ALL'")))                                                    //Natural: IF IATA36X-UNIT-CDE ( #I1 ) = MASK ( 'ALL' )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(!pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt()).equals(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Unit_Cde().getSubstring(1, //Natural: IF SUBSTR ( IATA36X-UNIT-CDE ( #I1 ) ,1,IATA36X-LNGTH ) NE SUBSTR ( RQST-UNIT-CDE,1,IATA36X-LNGTH )
                        pdaIata36x.getIata36x_Iata36x_Lngth().getInt()))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ** END OF ADD  5/99
                                                                                                                                                                          //Natural: PERFORM #WRITE-DETAIL-LINE
                sub_Pnd_Write_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_In_Detail.reset();                                                                                                                                        //Natural: RESET #IN-DETAIL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm369t.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM369T'
            if (condition(pnd_M361t_Totl_Rqst_Amt.equals(getZero()) && pnd_M361t_Prior_Del.equals(getZero())))                                                            //Natural: IF #M361T-TOTL-RQST-AMT = 0 AND #M361T-PRIOR-DEL = 0
            {
                getReports().write(1, ReportOption.NOTITLE," ",NEWLINE,"                    ******************************************      ",NEWLINE,                    //Natural: WRITE ( 1 ) ' ' / '                    ******************************************      ' / '                    No data was selected for this report today      ' / '                    ******************************************      '
                    "                    No data was selected for this report today      ",NEWLINE,"                    ******************************************      ");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 5/99
            //*  ADDED 5/99
            //*  ADDED 5/99
            pnd_M361h_Page.reset();                                                                                                                                       //Natural: RESET #M361H-PAGE #PAGE-CNTRL #PRIOR-DELETION
            pnd_Page_Cntrl.reset();
            pnd_Prior_Deletion.reset();
            //*  ADDED 5/99
            //*  ADDED 5/99 CONTINUE REPORTING ALL WRK-AREAS
            //*  ADDED 5/99 CONTINUE REPORTING ALL WRK-AREAS
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_In_Detail.setValue(true);                                                                                                                                 //Natural: ASSIGN #IN-DETAIL := TRUE
            pnd_First_Time.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-TIME := TRUE
            //*  ADDED FOLLOWING 5/99
            pnd_M361t_Anul_Mthd_Amt.reset();                                                                                                                              //Natural: RESET #M361T-ANUL-MTHD-AMT #M361T-CNTCT-AT-AMT #M361T-CNTCT-CS-AMT #M361T-CNTCT-FX-AMT #M361T-CNTCT-IG-AMT #M361T-CNTCT-IN-AMT #M361T-CNTCT-ML-AMT #M361T-CNTCT-VR-AMT #M361T-CNTCT-VS-AMT #M361T-M-T-M-ACCT-AMT #M361T-M-T-O-ACCT-AMT #M361T-MNTH-MTHD-AMT #M361T-GRADED #M361T-O-T-M-ACCT-AMT #M361T-O-T-O-ACCT-AMT #M361T-STAT-AF-AMT #M361T-STAT-CM-AMT #M361T-STAT-DY-AMT #M361T-STAT-DE-AMT #M361T-STAT-PA-AMT #M361T-STAT-RA-AMT #M361T-STAT-SO-AMT #M361T-STAT-RJ-AMT #M361T-PRIOR-DEL #M361T-TOTL-RQST-AMT #M361T-REPET-RQST-AMT #M361T-SINGLE-RQST-AMT #M361T-TTL-PRTCPNTS-AMT #M361T-TTL-RQST-PCNT-AMT #M361T-TTL-RQSTS-DLRS-AMT #M361T-TTL-RQSTS-UNTS-AMT
            pnd_M361t_Cntct_At_Amt.reset();
            pnd_M361t_Cntct_Cs_Amt.reset();
            pnd_M361t_Cntct_Fx_Amt.reset();
            pnd_M361t_Cntct_Ig_Amt.reset();
            pnd_M361t_Cntct_In_Amt.reset();
            pnd_M361t_Cntct_Ml_Amt.reset();
            pnd_M361t_Cntct_Vr_Amt.reset();
            pnd_M361t_Cntct_Vs_Amt.reset();
            pnd_M361t_M_T_M_Acct_Amt.reset();
            pnd_M361t_M_T_O_Acct_Amt.reset();
            pnd_M361t_Mnth_Mthd_Amt.reset();
            pnd_M361t_Graded.reset();
            pnd_M361t_O_T_M_Acct_Amt.reset();
            pnd_M361t_O_T_O_Acct_Amt.reset();
            pnd_M361t_Stat_Af_Amt.reset();
            pnd_M361t_Stat_Cm_Amt.reset();
            pnd_M361t_Stat_Dy_Amt.reset();
            pnd_M361t_Stat_De_Amt.reset();
            pnd_M361t_Stat_Pa_Amt.reset();
            pnd_M361t_Stat_Ra_Amt.reset();
            pnd_M361t_Stat_So_Amt.reset();
            pnd_M361t_Stat_Rj_Amt.reset();
            pnd_M361t_Prior_Del.reset();
            pnd_M361t_Totl_Rqst_Amt.reset();
            pnd_M361t_Repet_Rqst_Amt.reset();
            pnd_M361t_Single_Rqst_Amt.reset();
            pnd_M361t_Ttl_Prtcpnts_Amt.reset();
            pnd_M361t_Ttl_Rqst_Pcnt_Amt.reset();
            pnd_M361t_Ttl_Rqsts_Dlrs_Amt.reset();
            pnd_M361t_Ttl_Rqsts_Unts_Amt.reset();
            //*  ADDED 5/99 CONTINUE REPORTING ALL UNIT-IDS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*   PROGRAM SUBROUTINES
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-REPORT-DATE
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTACT-MODE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-TOTALS
        //*  REQUEST STATUS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-SUB-HEADING-1
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-SUB-HEADING-2
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-DETAIL-LINE
        //*  PIN AND CONTRACTS
        //*  REQUEST STATUS
        //*   VALUE 'R' ADD 1 TO #M361T-STAT-RA-AMT
    }
    private void sub_Set_Report_Date() throws Exception                                                                                                                   //Natural: SET-REPORT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  INITIAL DETAIL SETUP                                       /* LB 11/97
        getWorkFiles().read(5, pnd_Report_Date_A8);                                                                                                                       //Natural: READ WORK FILE 5 ONCE #REPORT-DATE-A8
        if (condition(pnd_Report_Date_A8.equals("99999999")))                                                                                                             //Natural: IF #REPORT-DATE-A8 = '99999999'
        {
            pnd_Currnt_Dte.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #CURRNT-DTE := *DATN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* CHECK DATE FORMAT
            if (condition(DbsUtil.maskMatches(pnd_Report_Date_A8,"YYYYMMDD")))                                                                                            //Natural: IF #REPORT-DATE-A8 EQ MASK ( YYYYMMDD )
            {
                pnd_Currnt_Dte.setValue(pnd_Report_Date_A8_Pnd_Report_Date_N8);                                                                                           //Natural: ASSIGN #CURRNT-DTE := #REPORT-DATE-N8
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "* ================ ERROR ======================= *");                                                                              //Natural: WRITE '* ================ ERROR ======================= *'
                if (Global.isEscape()) return;
                getReports().write(0, "* The Requested Date for the Report is           *");                                                                              //Natural: WRITE '* The Requested Date for the Report is           *'
                if (Global.isEscape()) return;
                getReports().write(0, "* not in the format YYYYMMDD.                    *");                                                                              //Natural: WRITE '* not in the format YYYYMMDD.                    *'
                if (Global.isEscape()) return;
                getReports().write(0, "* Request Date = ",pnd_Report_Date_A8);                                                                                            //Natural: WRITE '* Request Date = ' #REPORT-DATE-A8
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-CY := #CURRNT-DTE-CY
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-MM := #CURRNT-DTE-MM
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-DD := #CURRNT-DTE-DD
        //*  SET-REPORT-DATE
    }
    private void sub_Get_Contact_Mode() throws Exception                                                                                                                  //Natural: GET-CONTACT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata201.getIata201_Pnd_Naz_Table_Key().setValue(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde());                                                                //Natural: ASSIGN #NAZ-TABLE-KEY := IAT-TRNSFR-RPT.RQST-CNTCT-MDE
        pdaIata201.getIata201_Pnd_Table_Code().setValue("CMDE");                                                                                                          //Natural: ASSIGN #TABLE-CODE := 'CMDE'
        DbsUtil.callnat(Iatn201.class , getCurrentProcessState(), msg_Info_Sub, pdaIata201.getIata201());                                                                 //Natural: CALLNAT 'IATN201' MSG-INFO-SUB IATA201
        if (condition(Global.isEscape())) return;
    }
    private void sub_Add_To_Totals() throws Exception                                                                                                                     //Natural: ADD-TO-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  SETUP TOTAL PAGE
        //*  ADDED 5/99 SELECT ONLY
        if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().equals("A")))                                                                                          //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE = 'A'
        {
            pnd_M361t_Anul_Mthd_Amt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #M361T-ANUL-MTHD-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ADDED 5/99 SELECT ONLY
            if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type().equals("M")))                                                                                      //Natural: IF IAT-TRNSFR-RPT.RQST-XFR-TYPE = 'M'
            {
                pnd_M361t_Mnth_Mthd_Amt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #M361T-MNTH-MTHD-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_M361t_Anul_Mthd_Amt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #M361T-ANUL-MTHD-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTACT METHODS
        //*  ATS
        //*  CONTACT SHEET/PHONE
        //*  FAX
        //*  INTERNALLY GENERATED
        //*  INTERNET
        //*  MAIL
        //*  VOICE RECORDING
        //*  VISIT
        short decideConditionsMet646 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF RQST-CNTCT-MDE;//Natural: VALUE 'A'
        if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("A"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_At_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-AT-AMT
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("C"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Cs_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-CS-AMT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("X"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Fx_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-FX-AMT
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("I"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Ig_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-IG-AMT
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("N"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_In_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-IN-AMT
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("M"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Ml_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-ML-AMT
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("R"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Vr_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-VR-AMT
        }                                                                                                                                                                 //Natural: VALUE 'V'
        else if (condition((ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().equals("V"))))
        {
            decideConditionsMet646++;
            pnd_M361t_Cntct_Vs_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361T-CNTCT-VS-AMT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  TYPES OF ACCOUNT TRANSFERS
        //*  DETERMINE HOW MANY NON-BLANK CODES THERE ARE ON THE FROM AND TO SIDES
        //*  THEN BASED ON THE NUMBER DETERMINE ONE TO ONE, ONE TO MANY ETC..
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3 #I4 #NBR-FRM-P #NBR-TO-P
        pnd_I4.reset();
        pnd_Nbr_Frm_P.reset();
        pnd_Nbr_To_P.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I3 1 TO #MAX-ACCT
        for (pnd_I3.setValue(1); condition(pnd_I3.lessOrEqual(pnd_Max_Acct)); pnd_I3.nadd(1))
        {
            if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I3).notEquals(" ")))                                                               //Natural: IF XFR-FRM-ACCT-CDE ( #I3 ) NE ' '
            {
                pnd_Nbr_Frm_P.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NBR-FRM-P
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I4 1 TO #MAX-ACCT
        for (pnd_I4.setValue(1); condition(pnd_I4.lessOrEqual(pnd_Max_Acct)); pnd_I4.nadd(1))
        {
            if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(pnd_I4).notEquals(" ")))                                                                //Natural: IF XFR-TO-ACCT-CDE ( #I4 ) NE ' '
            {
                pnd_Nbr_To_P.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NBR-TO-P
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_I3.setValue(pnd_Nbr_Frm_P);                                                                                                                                   //Natural: ASSIGN #I3 := #NBR-FRM-P
        pnd_I4.setValue(pnd_Nbr_To_P);                                                                                                                                    //Natural: ASSIGN #I4 := #NBR-TO-P
        short decideConditionsMet684 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I3 = 1 AND #I4 = 1
        if (condition(pnd_I3.equals(1) && pnd_I4.equals(1)))
        {
            decideConditionsMet684++;
            pnd_M361t_O_T_O_Acct_Amt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #M361T-O-T-O-ACCT-AMT
        }                                                                                                                                                                 //Natural: WHEN #I3 = 1 AND #I4 > 1
        else if (condition(pnd_I3.equals(1) && pnd_I4.greater(1)))
        {
            decideConditionsMet684++;
            pnd_M361t_O_T_M_Acct_Amt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #M361T-O-T-M-ACCT-AMT
        }                                                                                                                                                                 //Natural: WHEN #I3 > 1 AND #I4 = 1
        else if (condition(pnd_I3.greater(1) && pnd_I4.equals(1)))
        {
            decideConditionsMet684++;
            pnd_M361t_M_T_O_Acct_Amt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #M361T-M-T-O-ACCT-AMT
        }                                                                                                                                                                 //Natural: WHEN #I3 > 1 AND #I4 > 1
        else if (condition(pnd_I3.greater(1) && pnd_I4.greater(1)))
        {
            decideConditionsMet684++;
            pnd_M361t_M_T_M_Acct_Amt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #M361T-M-T-M-ACCT-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Stat_1_2.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde());                                                                                               //Natural: ASSIGN #STAT-1-2 := XFR-STTS-CDE
        //*  AWAITING FACTORS
        short decideConditionsMet698 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #STAT-1;//Natural: VALUE 'F'
        if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("F"))))
        {
            decideConditionsMet698++;
            //*  COMPLETE
            pnd_M361t_Stat_Af_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-AF-AMT
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("M"))))
        {
            decideConditionsMet698++;
            //*  DELAYED
            pnd_M361t_Stat_Cm_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-CM-AMT
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("E"))))
        {
            decideConditionsMet698++;
            //*  DELETED
            pnd_M361t_Stat_Dy_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-DY-AMT
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("N"))))
        {
            decideConditionsMet698++;
            //*  PENDING APPLICATION
            pnd_M361t_Stat_De_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-DE-AMT
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("C"))))
        {
            decideConditionsMet698++;
            //*  REJECTED
            pnd_M361t_Stat_Pa_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-PA-AMT
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("P"))))
        {
            decideConditionsMet698++;
            //*  RETRO ACTIVE
            //*  SUPVR OVRD
            pnd_M361t_Stat_Rj_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-RJ-AMT
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("R"))))
        {
            decideConditionsMet698++;
            pnd_M361t_Stat_Ra_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-RA-AMT
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals("B"))))
        {
            decideConditionsMet698++;
            pnd_M361t_Stat_So_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M361T-STAT-SO-AMT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  REQUESTS BY PERCENT, DOLLARS OR UNITS
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("P"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'P' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M361t_Ttl_Rqst_Pcnt_Amt.nadd(pnd_I3);                                                                                                                     //Natural: ADD #I3 TO #M361T-TTL-RQST-PCNT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("D"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'D' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M361t_Ttl_Rqsts_Dlrs_Amt.nadd(pnd_I3);                                                                                                                    //Natural: ADD #I3 TO #M361T-TTL-RQSTS-DLRS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("U"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'U' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M361t_Ttl_Rqsts_Unts_Amt.nadd(pnd_I3);                                                                                                                    //Natural: ADD #I3 TO #M361T-TTL-RQSTS-UNTS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-TO-TOTALS
    }
    private void sub_Pnd_Write_Sub_Heading_1() throws Exception                                                                                                           //Natural: #WRITE-SUB-HEADING-1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Transfer Requests by:", Color.white, new FieldAttributes ("AD=I"),new TabSetting(23),pnd_M361i_Cntct_Mthd,  //Natural: WRITE ( 1 ) 001T 'Transfer Requests by:' ( NEI ) 023T #M361I-CNTCT-MTHD ( AD = OD CD = BL )
            new FieldAttributes ("AD=OD"), Color.blue);
        if (Global.isEscape()) return;
        //*  047T 'Revaluation Method:'(NEI)       /* COMMENTED 5/99
        //*  067T #M361I-EVAL-MTHD  (AD=OD CD=BL ) /*           5/99
        //*  #WRITE-SUB-HEADING-1
    }
    private void sub_Pnd_Write_Sub_Heading_2() throws Exception                                                                                                           //Natural: #WRITE-SUB-HEADING-2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Requests deleted today but entered prior to today", Color.white, new FieldAttributes                //Natural: WRITE ( 1 ) 001T 'Requests deleted today but entered prior to today' ( NEI )
            ("AD=I"));
        if (Global.isEscape()) return;
        //*  #WRITE-SUB-HEADING-1
    }
    private void sub_Pnd_Write_Detail_Line() throws Exception                                                                                                             //Natural: #WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSURE THE FULL REQUEST WILL FIT ON THE CURRENT PAGE
        if (condition(pnd_Page_Cntrl.greater(40)))                                                                                                                        //Natural: IF #PAGE-CNTRL > 40
        {
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET UP AND PRINT THE FIRST LINE OF THE REQUEST
        iatm361d_Dtl.reset();                                                                                                                                             //Natural: RESET IATM361D-DTL
        pnd_I2.setValue(1);                                                                                                                                               //Natural: ASSIGN #I2 = 1
        iatm361d_Dtl_Pnd_M361d_Unique_Id.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id());                                                                           //Natural: ASSIGN #M361D-UNIQUE-ID := IA-UNIQUE-ID
        iatm361d_Dtl_Pnd_M369d_Frm_Cntrct.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Ia_Frm_Cntrct());                                                                         //Natural: ASSIGN #M369D-FRM-CNTRCT := IA-FRM-CNTRCT
        iatm361d_Dtl_Pnd_M369d_To_Cntrct.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Ia_To_Cntrct());                                                                           //Natural: ASSIGN #M369D-TO-CNTRCT := IA-TO-CNTRCT
        //*  RECEIVED DATE AND EFFECTIVE DATE
        pnd_Rqst_Date.setValueEdited(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED RQST-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-DATE
        iatm361d_Dtl_Pnd_M361d_Rcvd_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rqst_Date_Pnd_Rqst_Date_Mm, "/", pnd_Rqst_Date_Pnd_Rqst_Date_Dd,  //Natural: COMPRESS #RQST-DATE-MM '/' #RQST-DATE-DD '/' #RQST-DATE-YYYY INTO #M361D-RCVD-DTE-OT LEAVING NO SPACE
            "/", pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy));
        pnd_Eff_Date.setValueEdited(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
        //*  ADDED 5/99
        iatm361d_Dtl_Pnd_M361d_Eff_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Eff_Date_Pnd_Eff_Date_Mm, "/", pnd_Eff_Date_Pnd_Eff_Date_Dd,       //Natural: COMPRESS #EFF-DATE-MM '/' #EFF-DATE-DD '/' #EFF-DATE-YYYY INTO #M361D-EFF-DTE-OT LEAVING NO SPACE
            "/", pnd_Eff_Date_Pnd_Eff_Date_Yyyy));
        iatm361d_Dtl_Pnd_M361d_Frm_Fnd.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2));                                                        //Natural: ASSIGN #M361D-FRM-FND := XFR-FRM-ACCT-CDE ( #I2 )
        iatm361d_Dtl_Pnd_M361d_To_Fnd.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(pnd_I2));                                                          //Natural: ASSIGN #M361D-TO-FND := XFR-TO-ACCT-CDE ( #I2 )
        iatm361d_Dtl_Pnd_M361d_Frm_Qunty.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Qty().getValue(pnd_I2));                                                           //Natural: ASSIGN #M361D-FRM-QUNTY := XFR-FRM-QTY ( #I2 )
        iatm361d_Dtl_Pnd_M361d_Frm_Typ.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue(pnd_I2));                                                             //Natural: ASSIGN #M361D-FRM-TYP := XFR-FRM-TYP ( #I2 )
        iatm361d_Dtl_Pnd_M361d_To_Qunty.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Qty().getValue(pnd_I2));                                                             //Natural: ASSIGN #M361D-TO-QUNTY := XFR-TO-QTY ( #I2 )
        iatm361d_Dtl_Pnd_M361d_To_Typ.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Typ().getValue(pnd_I2));                                                               //Natural: ASSIGN #M361D-TO-TYP := XFR-TO-TYP ( #I2 )
        iatm361d_Dtl_Pnd_M361d_Prtcpnt_Nme.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Prtcpnt_Nme());                                                                          //Natural: ASSIGN #M361D-PRTCPNT-NME := IAT-TRNSFR-RPT.PRTCPNT-NME
        iatm361d_Dtl_Pnd_M361d_Xfr_Opt.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Xfr_Type());                                                                            //Natural: ASSIGN #M361D-XFR-OPT := IAT-TRNSFR-RPT.RQST-XFR-TYPE
        pnd_Stat_1_2.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde());                                                                                               //Natural: ASSIGN #STAT-1-2 := XFR-STTS-CDE
        //*  AWAITING FACTORS
        //*  COMPLETE
        //*  DELAYED
        //*  DELETED
        //*  PENDING APPLICATION
        //*  REJECTED
        //*  SUPVR OVERIDE
        short decideConditionsMet802 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #STAT-1;//Natural: VALUE #AWAITING-FACTOR
        if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Awaiting_Factor()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Awai");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Awai'
        }                                                                                                                                                                 //Natural: VALUE #COMPLETE
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Complete()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Comp");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Comp'
        }                                                                                                                                                                 //Natural: VALUE #DELAYED
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Delayed()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Dela");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Dela'
        }                                                                                                                                                                 //Natural: VALUE #DELETED
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Deleted()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Dele");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Dele'
        }                                                                                                                                                                 //Natural: VALUE #PENDING-APPL
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Pending_Appl()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Pend");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Pend'
        }                                                                                                                                                                 //Natural: VALUE #REJECTED
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Rejected()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Reje");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Reje'
        }                                                                                                                                                                 //Natural: VALUE #SUPVR-OVRD
        else if (condition((pnd_Stat_1_2_Pnd_Stat_1.equals(ldaIatl300.getPnd_Status_1_Pnd_Supvr_Ovrd()))))
        {
            decideConditionsMet802++;
            iatm361d_Dtl_Pnd_M361d_Status.setValue("Supe");                                                                                                               //Natural: ASSIGN #M361D-STATUS := 'Supe'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm369d.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM369D'
        pnd_Page_Cntrl.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CNTRL
        if (condition(! (pnd_Prior_Deletion.getBoolean())))                                                                                                               //Natural: IF NOT #PRIOR-DELETION
        {
                                                                                                                                                                          //Natural: PERFORM ADD-TO-TOTALS
            sub_Add_To_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_M361t_Prior_Del.nadd(1);                                                                                                                                  //Natural: ASSIGN #M361T-PRIOR-DEL := #M361T-PRIOR-DEL + 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET UP AND PRINT THE REMAINING LINE(S) OF THE REQUEST
        iatm361d_Dtl.reset();                                                                                                                                             //Natural: RESET IATM361D-DTL
        FOR03:                                                                                                                                                            //Natural: FOR #I2 2 TO #MAX-ACCT
        for (pnd_I2.setValue(2); condition(pnd_I2.lessOrEqual(pnd_Max_Acct)); pnd_I2.nadd(1))
        {
            if (condition(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2).equals(" ") && ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(pnd_I2).equals(" "))) //Natural: IF XFR-FRM-ACCT-CDE ( #I2 ) = ' ' AND XFR-TO-ACCT-CDE ( #I2 ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            iatm361d_Dtl_Pnd_M361d_Frm_Fnd.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2));                                                    //Natural: ASSIGN #M361D-FRM-FND := XFR-FRM-ACCT-CDE ( #I2 )
            iatm361d_Dtl_Pnd_M361d_To_Fnd.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(pnd_I2));                                                      //Natural: ASSIGN #M361D-TO-FND := XFR-TO-ACCT-CDE ( #I2 )
            iatm361d_Dtl_Pnd_M361d_Frm_Qunty.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Qty().getValue(pnd_I2));                                                       //Natural: ASSIGN #M361D-FRM-QUNTY := XFR-FRM-QTY ( #I2 )
            iatm361d_Dtl_Pnd_M361d_Frm_Typ.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue(pnd_I2));                                                         //Natural: ASSIGN #M361D-FRM-TYP := XFR-FRM-TYP ( #I2 )
            iatm361d_Dtl_Pnd_M361d_To_Qunty.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Qty().getValue(pnd_I2));                                                         //Natural: ASSIGN #M361D-TO-QUNTY := XFR-TO-QTY ( #I2 )
            iatm361d_Dtl_Pnd_M361d_To_Typ.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Typ().getValue(pnd_I2));                                                           //Natural: ASSIGN #M361D-TO-TYP := XFR-TO-TYP ( #I2 )
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm369d.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM369D'
            pnd_Page_Cntrl.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PAGE-CNTRL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-DETAIL-LINE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_M361h_Page.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361H-PAGE
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm361h.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM361H'
                    //* * ADDED FOLLOWING    5/99
                    getReports().write(1, ReportOption.NOTITLE,"Report For Unit:",pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1));                             //Natural: WRITE ( 1 ) 'Report For Unit:' IATA36X-UNIT-CDE ( #I1 )
                    //* * END OF ADD         5/99
                    if (condition(pnd_In_Detail.getBoolean()))                                                                                                            //Natural: IF #IN-DETAIL
                    {
                        if (condition(! (pnd_Prior_Deletion.getBoolean())))                                                                                               //Natural: IF NOT #PRIOR-DELETION
                        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-SUB-HEADING-1
                            sub_Pnd_Write_Sub_Heading_1();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-SUB-HEADING-2
                            sub_Pnd_Write_Sub_Heading_2();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm369i.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM369I'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DO UNTIL END OF ENTRIES IN PARM TABLE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaIatl361_getIat_Trnsfr_Rpt_Rqst_Cntct_MdeIsBreak = ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde().isBreak(endOfData);
        if (condition(ldaIatl361_getIat_Trnsfr_Rpt_Rqst_Cntct_MdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MODE
            sub_Get_Contact_Mode();
            if (condition(Global.isEscape())) {return;}
            if (condition(msg_Info_Sub_Pnd_Pnd_Return_Code.equals(" ")))                                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
            {
                pnd_M361i_Cntct_Mthd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaIata201.getIata201_Pnd_Long_Desc()));                                    //Natural: COMPRESS #LONG-DESC INTO #M361I-CNTCT-MTHD LEAVING NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_M361i_Cntct_Mthd.setValue("CNTCT MTHD NOT ON FILE");                                                                                                  //Natural: ASSIGN #M361I-CNTCT-MTHD := 'CNTCT MTHD NOT ON FILE'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
