/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:08 PM
**        * FROM NATURAL PROGRAM : Iaap307a
************************************************************
**        * FILE NAME            : Iaap307a.java
**        * CLASS NAME           : Iaap307a
**        * INSTANCE NAME        : Iaap307a
************************************************************
************************************************************************
* PROGRAM:  IAAP307A  JOBNAME: PIA5110D
*
* FUNCTION: CREATES A DAILY FEED TO CWF FOR DEATH PROCESSING. THIS
*           WILL MIMIC THE BERWYN INBOUND FILE PROCESSED BY CWFB9121.
*           THE DATA WILL COME FROM SURVIVOR CENTRAL.
*
*
* CREATED:  07/19/13 : JUN TINIO
*
* HISTORY:
*
* 12/10/13  BYPASS NON-BERWYN RETURNED CONTRACTS. SCAN ON 12/13
* 01/03/14  BYPASS NON-EXACT MATCHES FOR GROUP.   SCAN ON 01/14
* 03/20/14  ADDED 500 BYTES TO THE END OF INPUT.  SCAN ON 03/14
* 04/13/15  REMOVED COR VIEW AS THIS IS NOT USED. SCAN ON 04/15
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap307a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_In_Rec;
    private DbsField pnd_In_Rec_Pnd_In_Cp_Ind;
    private DbsField pnd_In_Rec_Pnd_In_Ssn;

    private DbsGroup pnd_In_Rec__R_Field_1;
    private DbsField pnd_In_Rec_Pnd_In_Ssn_N;
    private DbsField pnd_In_Rec_Pnd_In_First_Name;
    private DbsField pnd_In_Rec_Pnd_In_Middle_Name;
    private DbsField pnd_In_Rec_Pnd_In_Last_Name;
    private DbsField pnd_In_Rec_Pnd_In_Addr1;
    private DbsField pnd_In_Rec_Pnd_In_Addr2;
    private DbsField pnd_In_Rec_Pnd_In_City;
    private DbsField pnd_In_Rec_Pnd_In_State;
    private DbsField pnd_In_Rec_Pnd_In_Zip;
    private DbsField pnd_In_Rec_Pnd_In_Gender;
    private DbsField pnd_In_Rec_Pnd_In_Dob;
    private DbsField pnd_In_Rec_Pnd_Br_Ssn;
    private DbsField pnd_In_Rec_Pnd_Br_First_Name;
    private DbsField pnd_In_Rec_Pnd_Br_Middle_Name;
    private DbsField pnd_In_Rec_Pnd_Br_Last_Name;
    private DbsField pnd_In_Rec_Pnd_Br_Dob;
    private DbsField pnd_In_Rec_Pnd_Br_Dod;
    private DbsField pnd_In_Rec_Pnd_Br_Addr1;
    private DbsField pnd_In_Rec_Pnd_Br_Addr2;
    private DbsField pnd_In_Rec_Pnd_Br_City;
    private DbsField pnd_In_Rec_Pnd_Br_State;
    private DbsField pnd_In_Rec_Pnd_Br_Zip;
    private DbsField pnd_In_Rec_Pnd_Br_Source;
    private DbsField pnd_In_Rec_Pnd_Br_Months_Deceased;
    private DbsField pnd_In_Rec_Pnd_Br_Category;

    private DbsGroup pnd_In_Rec__R_Field_2;
    private DbsField pnd_In_Rec_Pnd_In_Code;
    private DbsField pnd_In_Rec_Pnd_Br_Last_Dte;
    private DbsField pnd_In_Rec_Pnd_Br_Type_Cde;
    private DbsField pnd_In_Rec_Pnd_Br_Pin;
    private DbsField pnd_In_Rec_Pnd_Br_Fill;
    private DbsField pnd_In_Rec_Pnd_New_Fill;

    private DbsGroup pnd_Cwf_Rec;
    private DbsField pnd_Cwf_Rec_Pnd_Obs;
    private DbsField pnd_Cwf_Rec_Pnd_Ssn;
    private DbsField pnd_Cwf_Rec_Pnd_Last_Name;
    private DbsField pnd_Cwf_Rec_Pnd_First_Name;
    private DbsField pnd_Cwf_Rec_Pnd_User_Fiedl1;
    private DbsField pnd_Cwf_Rec_Pnd_User_Fiedl2;

    private DbsGroup pnd_Cwf_Rec__R_Field_3;
    private DbsField pnd_Cwf_Rec_Pnd_Contract_Nbr;
    private DbsField pnd_Cwf_Rec_Pnd_Option_Code;
    private DbsField pnd_Cwf_Rec_Pnd_X2;
    private DbsField pnd_Cwf_Rec_Pnd_D_Ssn;
    private DbsField pnd_Cwf_Rec_Pnd_D_Last_Name;
    private DbsField pnd_Cwf_Rec_Pnd_D_First_Name;
    private DbsField pnd_Cwf_Rec_Pnd_D_Dob;
    private DbsField pnd_Cwf_Rec_Pnd_D_Dod;
    private DbsField pnd_Cwf_Rec_Pnd_Match_Indicator;
    private DbsField pnd_Cwf_Rec_Pnd_Months_Since_Death;
    private DbsField pnd_Cwf_Rec_Pnd_Death_Source;
    private DbsField pnd_Cwf_Rec_Pnd_C_P_Ind;
    private DbsField pnd_Cwf_Rec_Pnd_Tiaa_Pin;
    private DbsField pnd_Cwf_Rec_Pnd_Origin_Code;

    private DbsGroup pnd_Ia_Berwyn;
    private DbsField pnd_Ia_Berwyn_Pnd_Ia_Contract;
    private DbsField pnd_Ia_Berwyn_Pnd_Ia_Payee;
    private DbsField pnd_Ia_Berwyn_Pnd_Ia_Option;
    private DbsField pnd_Ia_Berwyn_Pnd_Ia_Origin;
    private DbsField pnd_Cnt;
    private DbsField pnd_Eof;

    private DataAccessProgramView vw_cntrct;
    private DbsField cntrct_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Cntrct_Optn_Cde;
    private DbsField cntrct_Cntrct_Orgn_Cde;
    private DbsField cntrct_Cntrct_Scnd_Annt_Ssn;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key;

    private DbsGroup pnd_Pin_Cntrct_Payee_Key__R_Field_4;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Pin_Found;
    private DbsField pnd_Ssn_Found;
    private DbsField pnd_Cpr_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_In_Rec = localVariables.newGroupInRecord("pnd_In_Rec", "#IN-REC");
        pnd_In_Rec_Pnd_In_Cp_Ind = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Cp_Ind", "#IN-CP-IND", FieldType.STRING, 1);
        pnd_In_Rec_Pnd_In_Ssn = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Ssn", "#IN-SSN", FieldType.STRING, 9);

        pnd_In_Rec__R_Field_1 = pnd_In_Rec.newGroupInGroup("pnd_In_Rec__R_Field_1", "REDEFINE", pnd_In_Rec_Pnd_In_Ssn);
        pnd_In_Rec_Pnd_In_Ssn_N = pnd_In_Rec__R_Field_1.newFieldInGroup("pnd_In_Rec_Pnd_In_Ssn_N", "#IN-SSN-N", FieldType.NUMERIC, 9);
        pnd_In_Rec_Pnd_In_First_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_First_Name", "#IN-FIRST-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_In_Middle_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Middle_Name", "#IN-MIDDLE-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_In_Last_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Last_Name", "#IN-LAST-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_In_Addr1 = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Addr1", "#IN-ADDR1", FieldType.STRING, 65);
        pnd_In_Rec_Pnd_In_Addr2 = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Addr2", "#IN-ADDR2", FieldType.STRING, 65);
        pnd_In_Rec_Pnd_In_City = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_City", "#IN-CITY", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_In_State = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_State", "#IN-STATE", FieldType.STRING, 2);
        pnd_In_Rec_Pnd_In_Zip = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Zip", "#IN-ZIP", FieldType.STRING, 5);
        pnd_In_Rec_Pnd_In_Gender = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Gender", "#IN-GENDER", FieldType.STRING, 1);
        pnd_In_Rec_Pnd_In_Dob = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_In_Dob", "#IN-DOB", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_Br_Ssn = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Ssn", "#BR-SSN", FieldType.STRING, 9);
        pnd_In_Rec_Pnd_Br_First_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_First_Name", "#BR-FIRST-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_Br_Middle_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Middle_Name", "#BR-MIDDLE-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_Br_Last_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Last_Name", "#BR-LAST-NAME", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_Br_Dob = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Dob", "#BR-DOB", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_Br_Dod = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Dod", "#BR-DOD", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_Br_Addr1 = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Addr1", "#BR-ADDR1", FieldType.STRING, 65);
        pnd_In_Rec_Pnd_Br_Addr2 = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Addr2", "#BR-ADDR2", FieldType.STRING, 65);
        pnd_In_Rec_Pnd_Br_City = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_City", "#BR-CITY", FieldType.STRING, 35);
        pnd_In_Rec_Pnd_Br_State = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_State", "#BR-STATE", FieldType.STRING, 2);
        pnd_In_Rec_Pnd_Br_Zip = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Zip", "#BR-ZIP", FieldType.STRING, 5);
        pnd_In_Rec_Pnd_Br_Source = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Source", "#BR-SOURCE", FieldType.STRING, 20);
        pnd_In_Rec_Pnd_Br_Months_Deceased = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Months_Deceased", "#BR-MONTHS-DECEASED", FieldType.STRING, 4);
        pnd_In_Rec_Pnd_Br_Category = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Category", "#BR-CATEGORY", FieldType.STRING, 100);

        pnd_In_Rec__R_Field_2 = pnd_In_Rec.newGroupInGroup("pnd_In_Rec__R_Field_2", "REDEFINE", pnd_In_Rec_Pnd_Br_Category);
        pnd_In_Rec_Pnd_In_Code = pnd_In_Rec__R_Field_2.newFieldInGroup("pnd_In_Rec_Pnd_In_Code", "#IN-CODE", FieldType.NUMERIC, 2);
        pnd_In_Rec_Pnd_Br_Last_Dte = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Last_Dte", "#BR-LAST-DTE", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_Br_Type_Cde = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Type_Cde", "#BR-TYPE-CDE", FieldType.STRING, 15);
        pnd_In_Rec_Pnd_Br_Pin = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Pin", "#BR-PIN", FieldType.STRING, 12);
        pnd_In_Rec_Pnd_Br_Fill = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_Br_Fill", "#BR-FILL", FieldType.STRING, 40);
        pnd_In_Rec_Pnd_New_Fill = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_New_Fill", "#NEW-FILL", FieldType.STRING, 500);

        pnd_Cwf_Rec = localVariables.newGroupInRecord("pnd_Cwf_Rec", "#CWF-REC");
        pnd_Cwf_Rec_Pnd_Obs = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Obs", "#OBS", FieldType.STRING, 5);
        pnd_Cwf_Rec_Pnd_Ssn = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Cwf_Rec_Pnd_Last_Name = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 12);
        pnd_Cwf_Rec_Pnd_First_Name = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_User_Fiedl1 = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_User_Fiedl1", "#USER-FIEDL1", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_User_Fiedl2 = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_User_Fiedl2", "#USER-FIEDL2", FieldType.STRING, 12);

        pnd_Cwf_Rec__R_Field_3 = pnd_Cwf_Rec.newGroupInGroup("pnd_Cwf_Rec__R_Field_3", "REDEFINE", pnd_Cwf_Rec_Pnd_User_Fiedl2);
        pnd_Cwf_Rec_Pnd_Contract_Nbr = pnd_Cwf_Rec__R_Field_3.newFieldInGroup("pnd_Cwf_Rec_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Cwf_Rec_Pnd_Option_Code = pnd_Cwf_Rec__R_Field_3.newFieldInGroup("pnd_Cwf_Rec_Pnd_Option_Code", "#OPTION-CODE", FieldType.NUMERIC, 2);
        pnd_Cwf_Rec_Pnd_X2 = pnd_Cwf_Rec__R_Field_3.newFieldInGroup("pnd_Cwf_Rec_Pnd_X2", "#X2", FieldType.STRING, 2);
        pnd_Cwf_Rec_Pnd_D_Ssn = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_D_Ssn", "#D-SSN", FieldType.STRING, 9);
        pnd_Cwf_Rec_Pnd_D_Last_Name = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_D_Last_Name", "#D-LAST-NAME", FieldType.STRING, 12);
        pnd_Cwf_Rec_Pnd_D_First_Name = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_D_First_Name", "#D-FIRST-NAME", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_D_Dob = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_D_Dob", "#D-DOB", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_D_Dod = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_D_Dod", "#D-DOD", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_Match_Indicator = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Match_Indicator", "#MATCH-INDICATOR", FieldType.STRING, 3);
        pnd_Cwf_Rec_Pnd_Months_Since_Death = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Months_Since_Death", "#MONTHS-SINCE-DEATH", FieldType.STRING, 
            4);
        pnd_Cwf_Rec_Pnd_Death_Source = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Death_Source", "#DEATH-SOURCE", FieldType.STRING, 10);
        pnd_Cwf_Rec_Pnd_C_P_Ind = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_C_P_Ind", "#C-P-IND", FieldType.STRING, 1);
        pnd_Cwf_Rec_Pnd_Tiaa_Pin = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Tiaa_Pin", "#TIAA-PIN", FieldType.STRING, 12);
        pnd_Cwf_Rec_Pnd_Origin_Code = pnd_Cwf_Rec.newFieldInGroup("pnd_Cwf_Rec_Pnd_Origin_Code", "#ORIGIN-CODE", FieldType.NUMERIC, 2);

        pnd_Ia_Berwyn = localVariables.newGroupInRecord("pnd_Ia_Berwyn", "#IA-BERWYN");
        pnd_Ia_Berwyn_Pnd_Ia_Contract = pnd_Ia_Berwyn.newFieldInGroup("pnd_Ia_Berwyn_Pnd_Ia_Contract", "#IA-CONTRACT", FieldType.STRING, 10);
        pnd_Ia_Berwyn_Pnd_Ia_Payee = pnd_Ia_Berwyn.newFieldInGroup("pnd_Ia_Berwyn_Pnd_Ia_Payee", "#IA-PAYEE", FieldType.NUMERIC, 2);
        pnd_Ia_Berwyn_Pnd_Ia_Option = pnd_Ia_Berwyn.newFieldInGroup("pnd_Ia_Berwyn_Pnd_Ia_Option", "#IA-OPTION", FieldType.NUMERIC, 2);
        pnd_Ia_Berwyn_Pnd_Ia_Origin = pnd_Ia_Berwyn.newFieldInGroup("pnd_Ia_Berwyn_Pnd_Ia_Origin", "#IA-ORIGIN", FieldType.NUMERIC, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.INTEGER, 2);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);

        vw_cntrct = new DataAccessProgramView(new NameInfo("vw_cntrct", "CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        cntrct_Cntrct_Ppcn_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Cntrct_Optn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        cntrct_Cntrct_Orgn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        cntrct_Cntrct_Scnd_Annt_Ssn = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        registerRecord(vw_cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        registerRecord(vw_cpr);

        pnd_Pin_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Payee_Key", "#PIN-CNTRCT-PAYEE-KEY", FieldType.STRING, 24);

        pnd_Pin_Cntrct_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Payee_Key__R_Field_4", "REDEFINE", pnd_Pin_Cntrct_Payee_Key);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Pin_Cntrct_Payee_Key__R_Field_4.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Pin_Found = localVariables.newFieldInRecord("pnd_Pin_Found", "#PIN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ssn_Found = localVariables.newFieldInRecord("pnd_Ssn_Found", "#SSN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Cpr_Found = localVariables.newFieldInRecord("pnd_Cpr_Found", "#CPR-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrct.reset();
        vw_cpr.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap307a() throws Exception
    {
        super("Iaap307a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  01/14
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 250 PS = 0;//Natural: FORMAT ( 1 ) LS = 250 PS = 0
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #IN-REC
        while (condition(getWorkFiles().read(1, pnd_In_Rec)))
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD
            sub_Process_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.equals(getZero())))                                                                                                                         //Natural: IF #CNT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"****************************************************************",NEWLINE,             //Natural: WRITE ( 1 ) / / / / / / '****************************************************************' / '*                                                              *' / '*                                                              *' / '*            No death match for processing found               *' / '*                                                              *' / '*                                                              *' / '****************************************************************'
                "*                                                              *",NEWLINE,"*                                                              *",
                NEWLINE,"*            No death match for processing found               *",NEWLINE,"*                                                              *",
                NEWLINE,"*                                                              *",NEWLINE,"****************************************************************");
            if (Global.isEscape()) return;
            //*  TO DELETE THE OUTPUT FILE SO THE CWF TRIGGER WILL NOT
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
            //*                  BE TURNED ON
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CWF-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD
        //* *  OR #BR-PIN NE MASK(NNNNNNN)
    }
    private void sub_Write_Cwf_Record() throws Exception                                                                                                                  //Natural: WRITE-CWF-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet205 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #IN-CODE;//Natural: VALUE 01
        if (condition((pnd_In_Rec_Pnd_In_Code.equals(1))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("BFL");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'BFL'
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(2))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("DEE");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'DEE'
        }                                                                                                                                                                 //Natural: VALUE 03
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(3))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue(" FL");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := ' FL'
        }                                                                                                                                                                 //Natural: VALUE 04
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(4))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("B L");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'B L'
        }                                                                                                                                                                 //Natural: VALUE 05
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(5))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("BF ");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'BF '
        }                                                                                                                                                                 //Natural: VALUE 06
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(6))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("  L");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := '  L'
        }                                                                                                                                                                 //Natural: VALUE 07
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(7))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue(" F ");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := ' F '
        }                                                                                                                                                                 //Natural: VALUE 08
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(8))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("B  ");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'B  '
        }                                                                                                                                                                 //Natural: VALUE 09
        else if (condition((pnd_In_Rec_Pnd_In_Code.equals(9))))
        {
            decideConditionsMet205++;
            pnd_Cwf_Rec_Pnd_Match_Indicator.setValue("NON");                                                                                                              //Natural: ASSIGN #MATCH-INDICATOR := 'NON'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            //*    RESET #MATCH-INDICATOR
            //*  BYPASS IF NONE OF THE ABOVE.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Cwf_Rec_Pnd_Ssn.setValue(pnd_In_Rec_Pnd_In_Ssn);                                                                                                              //Natural: ASSIGN #SSN := #IN-SSN
        pnd_Cwf_Rec_Pnd_Last_Name.setValue(pnd_In_Rec_Pnd_In_Last_Name);                                                                                                  //Natural: ASSIGN #LAST-NAME := #IN-LAST-NAME
        pnd_Cwf_Rec_Pnd_First_Name.setValue(pnd_In_Rec_Pnd_In_First_Name);                                                                                                //Natural: ASSIGN #FIRST-NAME := #IN-FIRST-NAME
        pnd_Cwf_Rec_Pnd_User_Fiedl1.setValue(pnd_In_Rec_Pnd_In_Middle_Name);                                                                                              //Natural: ASSIGN #USER-FIEDL1 := #IN-MIDDLE-NAME
        pnd_Cwf_Rec_Pnd_Contract_Nbr.setValue(pnd_Ia_Berwyn_Pnd_Ia_Contract);                                                                                             //Natural: ASSIGN #CONTRACT-NBR := #IA-CONTRACT
        pnd_Cwf_Rec_Pnd_Option_Code.setValue(pnd_Ia_Berwyn_Pnd_Ia_Option);                                                                                                //Natural: ASSIGN #OPTION-CODE := #IA-OPTION
        pnd_Cwf_Rec_Pnd_D_Ssn.setValue(pnd_In_Rec_Pnd_Br_Ssn);                                                                                                            //Natural: ASSIGN #D-SSN := #BR-SSN
        pnd_Cwf_Rec_Pnd_D_Last_Name.setValue(pnd_In_Rec_Pnd_Br_Last_Name);                                                                                                //Natural: ASSIGN #D-LAST-NAME := #BR-LAST-NAME
        pnd_Cwf_Rec_Pnd_D_First_Name.setValue(pnd_In_Rec_Pnd_Br_First_Name);                                                                                              //Natural: ASSIGN #D-FIRST-NAME := #BR-FIRST-NAME
        pnd_Cwf_Rec_Pnd_D_Dob.setValue(pnd_In_Rec_Pnd_Br_Dob);                                                                                                            //Natural: ASSIGN #D-DOB := #BR-DOB
        pnd_Cwf_Rec_Pnd_D_Dod.setValue(pnd_In_Rec_Pnd_Br_Dod);                                                                                                            //Natural: ASSIGN #D-DOD := #BR-DOD
        pnd_Cwf_Rec_Pnd_Months_Since_Death.setValue(pnd_In_Rec_Pnd_Br_Months_Deceased);                                                                                   //Natural: ASSIGN #MONTHS-SINCE-DEATH := #BR-MONTHS-DECEASED
        pnd_Cwf_Rec_Pnd_Death_Source.setValue(pnd_In_Rec_Pnd_Br_Source);                                                                                                  //Natural: ASSIGN #DEATH-SOURCE := #BR-SOURCE
        DbsUtil.examine(new ExamineSource(pnd_In_Rec_Pnd_In_Cp_Ind), new ExamineTranslate(TranslateOption.Upper));                                                        //Natural: EXAMINE #IN-CP-IND TRANSLATE INTO UPPER CASE
        if (condition(pnd_In_Rec_Pnd_In_Cp_Ind.notEquals("C") && pnd_In_Rec_Pnd_In_Cp_Ind.notEquals("P")))                                                                //Natural: IF #IN-CP-IND NE 'C' AND #IN-CP-IND NE 'P'
        {
            pnd_In_Rec_Pnd_In_Cp_Ind.setValue("C");                                                                                                                       //Natural: ASSIGN #IN-CP-IND := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cwf_Rec_Pnd_C_P_Ind.setValue(pnd_In_Rec_Pnd_In_Cp_Ind);                                                                                                       //Natural: ASSIGN #C-P-IND := #IN-CP-IND
        pnd_Cwf_Rec_Pnd_Tiaa_Pin.setValue(pnd_In_Rec_Pnd_Br_Pin);                                                                                                         //Natural: ASSIGN #TIAA-PIN := #BR-PIN
        pnd_Cwf_Rec_Pnd_Origin_Code.setValue(pnd_Ia_Berwyn_Pnd_Ia_Origin);                                                                                                //Natural: ASSIGN #ORIGIN-CODE := #IA-ORIGIN
        //*  01/14 - START
        //*  GROUP ANNUITY
        if (condition(pnd_Ia_Berwyn_Pnd_Ia_Origin.equals(4) || pnd_Ia_Berwyn_Pnd_Ia_Origin.equals(6)))                                                                    //Natural: IF #IA-ORIGIN = 04 OR = 06
        {
            //*  EXACT MATCH ONLY
            if (condition(pnd_In_Rec_Pnd_In_Code.equals(1)))                                                                                                              //Natural: IF #IN-CODE = 01
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(0, "Mtch/Ind",                                                                                                                       //Natural: DISPLAY 'Mtch/Ind' #IN-CODE '/IA SSN' #SSN 'Deceased/SSN' #D-SSN '/PIN' #TIAA-PIN '/IA Last Name' #LAST-NAME '/IA First Name' #FIRST-NAME '/BR Last Name' #D-LAST-NAME '/BR First Name' #D-FIRST-NAME '/IA DOB' #IN-DOB '/BR DOB' #D-DOB '/Contract' #CONTRACT-NBR '/Optn' #OPTION-CODE '/Orgn' #ORIGIN-CODE '/DOD' #D-DOD
                		pnd_In_Rec_Pnd_In_Code,"/IA SSN",
                		pnd_Cwf_Rec_Pnd_Ssn,"Deceased/SSN",
                		pnd_Cwf_Rec_Pnd_D_Ssn,"/PIN",
                		pnd_Cwf_Rec_Pnd_Tiaa_Pin,"/IA Last Name",
                		pnd_Cwf_Rec_Pnd_Last_Name,"/IA First Name",
                		pnd_Cwf_Rec_Pnd_First_Name,"/BR Last Name",
                		pnd_Cwf_Rec_Pnd_D_Last_Name,"/BR First Name",
                		pnd_Cwf_Rec_Pnd_D_First_Name,"/IA DOB",
                		pnd_In_Rec_Pnd_In_Dob,"/BR DOB",
                		pnd_Cwf_Rec_Pnd_D_Dob,"/Contract",
                		pnd_Cwf_Rec_Pnd_Contract_Nbr,"/Optn",
                		pnd_Cwf_Rec_Pnd_Option_Code,"/Orgn",
                		pnd_Cwf_Rec_Pnd_Origin_Code,"/DOD",
                		pnd_Cwf_Rec_Pnd_D_Dod);
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  01/14 - END
        getWorkFiles().write(2, false, pnd_Cwf_Rec);                                                                                                                      //Natural: WRITE WORK FILE 2 #CWF-REC
        getReports().display(1, "IA SSN",                                                                                                                                 //Natural: DISPLAY ( 1 ) 'IA SSN' #SSN 'Deceased SSN' #D-SSN 'PIN' #TIAA-PIN 'IA Last Name' #LAST-NAME 'IA First Name' #FIRST-NAME 'BR Last Name' #D-LAST-NAME 'BR First Name' #D-FIRST-NAME 'IA DOB' #IN-DOB 'BR DOB' #D-DOB 'Contract' #CONTRACT-NBR 'Optn' #OPTION-CODE 'Orgn' #ORIGIN-CODE 'DOD' #D-DOD 'Match Ind' #MATCH-INDICATOR 'CP Ind' #C-P-IND
        		pnd_Cwf_Rec_Pnd_Ssn,"Deceased SSN",
        		pnd_Cwf_Rec_Pnd_D_Ssn,"PIN",
        		pnd_Cwf_Rec_Pnd_Tiaa_Pin,"IA Last Name",
        		pnd_Cwf_Rec_Pnd_Last_Name,"IA First Name",
        		pnd_Cwf_Rec_Pnd_First_Name,"BR Last Name",
        		pnd_Cwf_Rec_Pnd_D_Last_Name,"BR First Name",
        		pnd_Cwf_Rec_Pnd_D_First_Name,"IA DOB",
        		pnd_In_Rec_Pnd_In_Dob,"BR DOB",
        		pnd_Cwf_Rec_Pnd_D_Dob,"Contract",
        		pnd_Cwf_Rec_Pnd_Contract_Nbr,"Optn",
        		pnd_Cwf_Rec_Pnd_Option_Code,"Orgn",
        		pnd_Cwf_Rec_Pnd_Origin_Code,"DOD",
        		pnd_Cwf_Rec_Pnd_D_Dod,"Match Ind",
        		pnd_Cwf_Rec_Pnd_Match_Indicator,"CP Ind",
        		pnd_Cwf_Rec_Pnd_C_P_Ind);
        if (Global.isEscape()) return;
        pnd_Cnt.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #CNT
    }
    private void sub_Process_Record() throws Exception                                                                                                                    //Natural: PROCESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  12/23 - START
        if (condition(pnd_In_Rec_Pnd_In_Code.greaterOrEqual(1) && pnd_In_Rec_Pnd_In_Code.lessOrEqual(9)))                                                                 //Natural: IF #IN-CODE = 01 THRU 09
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  12/23 - END
        //*  082017
        if (condition(DbsUtil.maskMatches(pnd_In_Rec_Pnd_Br_Type_Cde,"'FUZZY'") || DbsUtil.maskMatches(pnd_In_Rec_Pnd_Br_Type_Cde,"'UPDATE'") || DbsUtil.maskMatches(pnd_In_Rec_Pnd_Br_Type_Cde,"'DAILY'")  //Natural: IF #BR-TYPE-CDE = MASK ( 'FUZZY' ) OR = MASK ( 'UPDATE' ) OR = MASK ( 'DAILY' ) OR NOT #BR-PIN IS ( N12 )
            || ! (DbsUtil.is(pnd_In_Rec_Pnd_Br_Pin.getText(),"N12"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.reset();                                                                                                        //Natural: RESET #CNTRCT-PART-PPCN-NBR #CNTRCT-PART-PAYEE-CDE #CPR-FOUND
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.reset();
        pnd_Cpr_Found.reset();
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr.compute(new ComputeParameters(false, pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr), pnd_In_Rec_Pnd_Br_Pin.val());              //Natural: ASSIGN #CPR-ID-NBR := VAL ( #BR-PIN )
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ CPR BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #PIN-CNTRCT-PAYEE-KEY
        (
        "R01",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Pin_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        R01:
        while (condition(vw_cpr.readNextRow("R01")))
        {
            if (condition(cpr_Cpr_Id_Nbr.notEquals(pnd_Pin_Cntrct_Payee_Key_Pnd_Cpr_Id_Nbr)))                                                                             //Natural: IF CPR-ID-NBR NE #CPR-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cpr_Cntrct_Actvty_Cde.notEquals(9) && cpr_Prtcpnt_Tax_Id_Nbr.equals(pnd_In_Rec_Pnd_In_Ssn_N))))                                               //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE NE 9 AND PRTCPNT-TAX-ID-NBR = #IN-SSN-N
            {
                continue;
            }
            vw_cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) CNTRCT BY CNTRCT-PPCN-NBR = CNTRCT-PART-PPCN-NBR
            (
            "READ02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", cpr_Cntrct_Part_Ppcn_Nbr, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cntrct.readNextRow("READ02")))
            {
                if (condition(cntrct_Cntrct_Ppcn_Nbr.notEquals(cpr_Cntrct_Part_Ppcn_Nbr)))                                                                                //Natural: IF CNTRCT-PPCN-NBR NE CNTRCT-PART-PPCN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cpr_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #CPR-FOUND := TRUE
                pnd_Ia_Berwyn_Pnd_Ia_Contract.setValue(cpr_Cntrct_Part_Ppcn_Nbr);                                                                                         //Natural: ASSIGN #IA-CONTRACT := CNTRCT-PART-PPCN-NBR
                pnd_Ia_Berwyn_Pnd_Ia_Payee.setValue(cpr_Cntrct_Part_Payee_Cde);                                                                                           //Natural: ASSIGN #IA-PAYEE := CNTRCT-PART-PAYEE-CDE
                pnd_Ia_Berwyn_Pnd_Ia_Option.setValue(cntrct_Cntrct_Optn_Cde);                                                                                             //Natural: ASSIGN #IA-OPTION := CNTRCT-OPTN-CDE
                pnd_Ia_Berwyn_Pnd_Ia_Origin.setValue(cntrct_Cntrct_Orgn_Cde);                                                                                             //Natural: ASSIGN #IA-ORIGIN := CNTRCT-ORGN-CDE
                //*    IF #BR-TYPE-CDE = MASK('DAILY')  /* 12/13
                //*      #IN-CODE := 01
                //*    END-IF                           /* 12/13
                                                                                                                                                                          //Natural: PERFORM WRITE-CWF-RECORD
                sub_Write_Cwf_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break R01;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R01. )
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R01"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R01"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cpr_Found.equals(false)))                                                                                                                       //Natural: IF #CPR-FOUND = FALSE
        {
            vw_cntrct.startDatabaseRead                                                                                                                                   //Natural: READ CNTRCT BY CNTRCT-SCND-ANNT-SSN STARTING FROM #IN-SSN-N
            (
            "R02",
            new Wc[] { new Wc("CNTRCT_SCND_ANNT_SSN", ">=", pnd_In_Rec_Pnd_In_Ssn_N, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_SCND_ANNT_SSN", "ASC") }
            );
            R02:
            while (condition(vw_cntrct.readNextRow("R02")))
            {
                if (condition(cntrct_Cntrct_Scnd_Annt_Ssn.notEquals(pnd_In_Rec_Pnd_In_Ssn_N)))                                                                            //Natural: IF CNTRCT-SCND-ANNT-SSN NE #IN-SSN-N
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                vw_cpr.startDatabaseRead                                                                                                                                  //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM CNTRCT-PPCN-NBR
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", cntrct_Cntrct_Ppcn_Nbr, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
                );
                READ03:
                while (condition(vw_cpr.readNextRow("READ03")))
                {
                    if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(cntrct_Cntrct_Ppcn_Nbr)))                                                                            //Natural: IF CNTRCT-PART-PPCN-NBR NE CNTRCT-PPCN-NBR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(!(cpr_Cntrct_Actvty_Cde.notEquals(9) && cpr_Cpr_Id_Nbr.greater(getZero()) && cpr_Cpr_Id_Nbr.notEquals(9999999))))                       //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE NE 9 AND CPR-ID-NBR GT 0 AND CPR-ID-NBR NE 9999999
                    {
                        continue;
                    }
                    pnd_Ia_Berwyn_Pnd_Ia_Contract.setValue(cpr_Cntrct_Part_Ppcn_Nbr);                                                                                     //Natural: ASSIGN #IA-CONTRACT := CNTRCT-PART-PPCN-NBR
                    pnd_Ia_Berwyn_Pnd_Ia_Payee.setValue(cpr_Cntrct_Part_Payee_Cde);                                                                                       //Natural: ASSIGN #IA-PAYEE := CNTRCT-PART-PAYEE-CDE
                    pnd_Ia_Berwyn_Pnd_Ia_Option.setValue(cntrct_Cntrct_Optn_Cde);                                                                                         //Natural: ASSIGN #IA-OPTION := CNTRCT-OPTN-CDE
                    pnd_Ia_Berwyn_Pnd_Ia_Origin.setValue(cntrct_Cntrct_Orgn_Cde);                                                                                         //Natural: ASSIGN #IA-ORIGIN := CNTRCT-ORGN-CDE
                    //*      IF #BR-TYPE-CDE = MASK('DAILY')  /* 12/13
                    //*        #IN-CODE := 01
                    //*      END-IF                           /* 12/13
                                                                                                                                                                          //Natural: PERFORM WRITE-CWF-RECORD
                    sub_Write_Cwf_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break R02;                                                                                                                                  //Natural: ESCAPE BOTTOM ( R02. )
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R02"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R02"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(20),"BERWYN Death Match Processing",new ColumnSpacing(20),Global.getDATX(), new ReportEditMask                //Natural: WRITE ( 1 ) 20X 'BERWYN Death Match Processing' 20X *DATX ( EM = MM/DD/YYYY )
                        ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=250 PS=0");
        Global.format(1, "LS=250 PS=0");

        getReports().setDisplayColumns(0, "Mtch/Ind",
        		pnd_In_Rec_Pnd_In_Code,"/IA SSN",
        		pnd_Cwf_Rec_Pnd_Ssn,"Deceased/SSN",
        		pnd_Cwf_Rec_Pnd_D_Ssn,"/PIN",
        		pnd_Cwf_Rec_Pnd_Tiaa_Pin,"/IA Last Name",
        		pnd_Cwf_Rec_Pnd_Last_Name,"/IA First Name",
        		pnd_Cwf_Rec_Pnd_First_Name,"/BR Last Name",
        		pnd_Cwf_Rec_Pnd_D_Last_Name,"/BR First Name",
        		pnd_Cwf_Rec_Pnd_D_First_Name,"/IA DOB",
        		pnd_In_Rec_Pnd_In_Dob,"/BR DOB",
        		pnd_Cwf_Rec_Pnd_D_Dob,"/Contract",
        		pnd_Cwf_Rec_Pnd_Contract_Nbr,"/Optn",
        		pnd_Cwf_Rec_Pnd_Option_Code,"/Orgn",
        		pnd_Cwf_Rec_Pnd_Origin_Code,"/DOD",
        		pnd_Cwf_Rec_Pnd_D_Dod);
        getReports().setDisplayColumns(1, "IA SSN",
        		pnd_Cwf_Rec_Pnd_Ssn,"Deceased SSN",
        		pnd_Cwf_Rec_Pnd_D_Ssn,"PIN",
        		pnd_Cwf_Rec_Pnd_Tiaa_Pin,"IA Last Name",
        		pnd_Cwf_Rec_Pnd_Last_Name,"IA First Name",
        		pnd_Cwf_Rec_Pnd_First_Name,"BR Last Name",
        		pnd_Cwf_Rec_Pnd_D_Last_Name,"BR First Name",
        		pnd_Cwf_Rec_Pnd_D_First_Name,"IA DOB",
        		pnd_In_Rec_Pnd_In_Dob,"BR DOB",
        		pnd_Cwf_Rec_Pnd_D_Dob,"Contract",
        		pnd_Cwf_Rec_Pnd_Contract_Nbr,"Optn",
        		pnd_Cwf_Rec_Pnd_Option_Code,"Orgn",
        		pnd_Cwf_Rec_Pnd_Origin_Code,"DOD",
        		pnd_Cwf_Rec_Pnd_D_Dod,"Match Ind",
        		pnd_Cwf_Rec_Pnd_Match_Indicator,"CP Ind",
        		pnd_Cwf_Rec_Pnd_C_P_Ind);
    }
}
