/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:57 PM
**        * FROM NATURAL PROGRAM : Iaap330
************************************************************
**        * FILE NAME            : Iaap330.java
**        * CLASS NAME           : Iaap330
**        * INSTANCE NAME        : Iaap330
************************************************************
***********************************************************************
* PROGRAM:  IAAP330
* FUNCTION: READ GROSS TO NET  FILE FROM CARTRIDGE AND UPDATE CPR WITH
*           TAX WITHOLDING INFORMATION
* CREATED:  03/25/95 BY ARI GROSSMAN
*
* HISTORY:
* 08/2010 : ADDED CANADIAN TAX WITHHOLDING FOR CANADIAN CONVERTED
*           SCAN 08/10
* 12/2011 : RECATALOGED WITH CHANGES TO FCPA800
* 05/2017 : STOWED FOR PIN EXP. CHANGES TO FCPA800.  OS
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap330 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cpr;
    private DbsField iaa_Cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Cntrct_Local_Tax_Amt;

    private DbsGroup iaa_Cpr_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Cntrct_Rtb_Amt;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde_A;
    private DbsField pnd_Work_Reads;
    private DbsField pnd_Cpr_Writes;
    private DbsField pnd_From_Cnt_Tot;
    private DbsField pnd_No_Cpr_Rec_Found;
    private DbsField pnd_New_Ia_Recs;
    private DbsField pnd_To_Cnt_Tot;
    private DbsField pnd_Cnt_Write_Tot;
    private DbsField pnd_Fed_Amt;
    private DbsField pnd_State_Amt;
    private DbsField pnd_Local_Amt;
    private DbsField pnd_Annt_Rsdncy_Cde_A;
    private DbsField pnd_W1_Record;

    private DbsGroup pnd_W1_Record__R_Field_3;
    private DbsField pnd_W1_Record_Pnd_W1_Cntrct;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Osis_Start;

    private DbsGroup pnd_Osis_Start__R_Field_4;
    private DbsField pnd_Osis_Start_Pnd_Osis_Orgn_Cde_S;
    private DbsField pnd_Osis_Start_Pnd_Osis_Stats_Cde_S;
    private DbsField pnd_Osis_Start_Pnd_Osis_Invrse_Dte_S;
    private DbsField pnd_Osis_Start_Pnd_Osis_Prcss_Seq_Nbr_S;
    private DbsField pnd_Osis_End;

    private DbsGroup pnd_Osis_End__R_Field_5;
    private DbsField pnd_Osis_End_Pnd_Osis_Orgn_Cde_E;
    private DbsField pnd_Osis_End_Pnd_Osis_Stats_Cde_E;
    private DbsField pnd_Osis_End_Pnd_Osis_Invrse_Dte_E;
    private DbsField pnd_Osis_End_Pnd_Osis_Prcss_Seq_Nbr_E;
    private DbsField pnd_Cntrct_Payee_Cde_A;

    private DbsGroup pnd_Cntrct_Payee_Cde_A__R_Field_6;
    private DbsField pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2;

    private DbsGroup pnd_Cntrct_Payee_Cde_A__R_Field_7;
    private DbsField pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);

        // Local Variables

        vw_iaa_Cpr = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr", "IAA-CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cpr_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Cntrct_Part_Payee_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Cntrct_State_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_STATE_CDE");
        iaa_Cpr_Cntrct_State_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Cntrct_Local_Cde = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Cntrct_Local_Tax_Amt = vw_iaa_Cpr.getRecord().newFieldInGroup("iaa_Cpr_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");

        iaa_Cpr_Cntrct_Company_Data = vw_iaa_Cpr.getRecord().newGroupInGroup("iaa_Cpr_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Cntrct_Rtb_Amt = iaa_Cpr_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        registerRecord(vw_iaa_Cpr);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Cntrct_Payee_Key__R_Field_2 = pnd_Cntrct_Payee_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Payee_Key_Pnd_Payee_Cde);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde_A = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Work_Reads = localVariables.newFieldInRecord("pnd_Work_Reads", "#WORK-READS", FieldType.NUMERIC, 8);
        pnd_Cpr_Writes = localVariables.newFieldInRecord("pnd_Cpr_Writes", "#CPR-WRITES", FieldType.NUMERIC, 8);
        pnd_From_Cnt_Tot = localVariables.newFieldInRecord("pnd_From_Cnt_Tot", "#FROM-CNT-TOT", FieldType.NUMERIC, 6);
        pnd_No_Cpr_Rec_Found = localVariables.newFieldInRecord("pnd_No_Cpr_Rec_Found", "#NO-CPR-REC-FOUND", FieldType.NUMERIC, 6);
        pnd_New_Ia_Recs = localVariables.newFieldInRecord("pnd_New_Ia_Recs", "#NEW-IA-RECS", FieldType.NUMERIC, 6);
        pnd_To_Cnt_Tot = localVariables.newFieldInRecord("pnd_To_Cnt_Tot", "#TO-CNT-TOT", FieldType.NUMERIC, 6);
        pnd_Cnt_Write_Tot = localVariables.newFieldInRecord("pnd_Cnt_Write_Tot", "#CNT-WRITE-TOT", FieldType.NUMERIC, 6);
        pnd_Fed_Amt = localVariables.newFieldInRecord("pnd_Fed_Amt", "#FED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_State_Amt = localVariables.newFieldInRecord("pnd_State_Amt", "#STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Local_Amt = localVariables.newFieldInRecord("pnd_Local_Amt", "#LOCAL-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Annt_Rsdncy_Cde_A = localVariables.newFieldInRecord("pnd_Annt_Rsdncy_Cde_A", "#ANNT-RSDNCY-CDE-A", FieldType.STRING, 3);
        pnd_W1_Record = localVariables.newFieldInRecord("pnd_W1_Record", "#W1-RECORD", FieldType.STRING, 10);

        pnd_W1_Record__R_Field_3 = localVariables.newGroupInRecord("pnd_W1_Record__R_Field_3", "REDEFINE", pnd_W1_Record);
        pnd_W1_Record_Pnd_W1_Cntrct = pnd_W1_Record__R_Field_3.newFieldInGroup("pnd_W1_Record_Pnd_W1_Cntrct", "#W1-CNTRCT", FieldType.STRING, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 6);
        pnd_Osis_Start = localVariables.newFieldInRecord("pnd_Osis_Start", "#OSIS-START", FieldType.STRING, 20);

        pnd_Osis_Start__R_Field_4 = localVariables.newGroupInRecord("pnd_Osis_Start__R_Field_4", "REDEFINE", pnd_Osis_Start);
        pnd_Osis_Start_Pnd_Osis_Orgn_Cde_S = pnd_Osis_Start__R_Field_4.newFieldInGroup("pnd_Osis_Start_Pnd_Osis_Orgn_Cde_S", "#OSIS-ORGN-CDE-S", FieldType.STRING, 
            2);
        pnd_Osis_Start_Pnd_Osis_Stats_Cde_S = pnd_Osis_Start__R_Field_4.newFieldInGroup("pnd_Osis_Start_Pnd_Osis_Stats_Cde_S", "#OSIS-STATS-CDE-S", FieldType.STRING, 
            1);
        pnd_Osis_Start_Pnd_Osis_Invrse_Dte_S = pnd_Osis_Start__R_Field_4.newFieldInGroup("pnd_Osis_Start_Pnd_Osis_Invrse_Dte_S", "#OSIS-INVRSE-DTE-S", 
            FieldType.NUMERIC, 8);
        pnd_Osis_Start_Pnd_Osis_Prcss_Seq_Nbr_S = pnd_Osis_Start__R_Field_4.newFieldInGroup("pnd_Osis_Start_Pnd_Osis_Prcss_Seq_Nbr_S", "#OSIS-PRCSS-SEQ-NBR-S", 
            FieldType.NUMERIC, 9);
        pnd_Osis_End = localVariables.newFieldInRecord("pnd_Osis_End", "#OSIS-END", FieldType.STRING, 20);

        pnd_Osis_End__R_Field_5 = localVariables.newGroupInRecord("pnd_Osis_End__R_Field_5", "REDEFINE", pnd_Osis_End);
        pnd_Osis_End_Pnd_Osis_Orgn_Cde_E = pnd_Osis_End__R_Field_5.newFieldInGroup("pnd_Osis_End_Pnd_Osis_Orgn_Cde_E", "#OSIS-ORGN-CDE-E", FieldType.STRING, 
            2);
        pnd_Osis_End_Pnd_Osis_Stats_Cde_E = pnd_Osis_End__R_Field_5.newFieldInGroup("pnd_Osis_End_Pnd_Osis_Stats_Cde_E", "#OSIS-STATS-CDE-E", FieldType.STRING, 
            1);
        pnd_Osis_End_Pnd_Osis_Invrse_Dte_E = pnd_Osis_End__R_Field_5.newFieldInGroup("pnd_Osis_End_Pnd_Osis_Invrse_Dte_E", "#OSIS-INVRSE-DTE-E", FieldType.NUMERIC, 
            8);
        pnd_Osis_End_Pnd_Osis_Prcss_Seq_Nbr_E = pnd_Osis_End__R_Field_5.newFieldInGroup("pnd_Osis_End_Pnd_Osis_Prcss_Seq_Nbr_E", "#OSIS-PRCSS-SEQ-NBR-E", 
            FieldType.NUMERIC, 9);
        pnd_Cntrct_Payee_Cde_A = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Cde_A", "#CNTRCT-PAYEE-CDE-A", FieldType.STRING, 4);

        pnd_Cntrct_Payee_Cde_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Cde_A__R_Field_6", "REDEFINE", pnd_Cntrct_Payee_Cde_A);
        pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2 = pnd_Cntrct_Payee_Cde_A__R_Field_6.newFieldInGroup("pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2", 
            "#CNTRCT-PAYEE-CDE-2", FieldType.STRING, 2);

        pnd_Cntrct_Payee_Cde_A__R_Field_7 = pnd_Cntrct_Payee_Cde_A__R_Field_6.newGroupInGroup("pnd_Cntrct_Payee_Cde_A__R_Field_7", "REDEFINE", pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2);
        pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2_N = pnd_Cntrct_Payee_Cde_A__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2_N", 
            "#CNTRCT-PAYEE-CDE-2-N", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cpr.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap330() throws Exception
    {
        super("Iaap330");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP330", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        getReports().write(0, "***************************",NEWLINE,"  START OF ARI1 PROGRAM",NEWLINE,"***************************");                                     //Natural: WRITE '***************************' / '  START OF ARI1 PROGRAM' / '***************************'
        if (Global.isEscape()) return;
        //*   MOVE 'AP'      TO #OSIS-ORGN-CDE-S #OSIS-ORGN-CDE-E                                                                                                         //Natural: ON ERROR
        //*   MOVE ' '       TO #OSIS-STATS-CDE-S
        //*   MOVE 0         TO #OSIS-INVRSE-DTE-S
        //*   MOVE 0         TO #OSIS-PRCSS-SEQ-NBR-S
        //*   MOVE H'FF'     TO #OSIS-STATS-CDE-E
        //*   MOVE 99999999  TO #OSIS-INVRSE-DTE-E
        //*   MOVE 999999999 TO #OSIS-PRCSS-SEQ-NBR-E
        //*  RW. READ  FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ
        //*      STARTING FROM #OSIS-START
        //*      ENDING   AT   #OSIS-END
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            pnd_Work_Reads.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WORK-READS
            if (condition(!(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().equals("AP"))))                                                                             //Natural: ACCEPT IF CNTRCT-ORGN-CDE = 'AP'
            {
                continue;
            }
            pnd_New_Ia_Recs.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NEW-IA-RECS
            //*      MOVE C*INV-ACCT TO #J
            FR:                                                                                                                                                           //Natural: FOR #I = 1 TO INV-ACCT-COUNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_I.nadd(1))
            {
                pnd_Fed_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                                                //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #I ) TO #FED-AMT
                pnd_State_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                                             //Natural: ADD INV-ACCT-STATE-TAX-AMT ( #I ) TO #STATE-AMT
                pnd_Local_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                                             //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #I ) TO #LOCAL-AMT
                //*        WRITE '=' #J '=' #FED-AMT '=' #STATE-AMT '=' #LOCAL-AMT
                //*        WRITE '=' INV-ACCT-FED-CDE(*) '=' INV-ACCT-STATE-CDE(*)
                //*                                      '=' INV-ACCT-LOCAL-CDE(*)
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*      IF #STATE-AMT > 0
            //*        WRITE '=' #STATE-AMT '=' INV-ACCT-STATE-CDE(*)
            //*      END-IF
                                                                                                                                                                          //Natural: PERFORM #READ-CPR
            sub_Pnd_Read_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fed_Amt.reset();                                                                                                                                          //Natural: RESET #FED-AMT #STATE-AMT #LOCAL-AMT
            pnd_State_Amt.reset();
            pnd_Local_Amt.reset();
            //*     END-READ
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,"WORK READS =============> ",pnd_Work_Reads);                                                                               //Natural: WRITE // 'WORK READS =============> ' #WORK-READS
        if (Global.isEscape()) return;
        getReports().write(0, "NO CPR RECORD FOUND ====> ",pnd_No_Cpr_Rec_Found);                                                                                         //Natural: WRITE 'NO CPR RECORD FOUND ====> ' #NO-CPR-REC-FOUND
        if (Global.isEscape()) return;
        getReports().write(0, "NEW IA RECS         ====> ",pnd_New_Ia_Recs);                                                                                              //Natural: WRITE 'NEW IA RECS         ====> ' #NEW-IA-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "CPR UPDATES ============> ",pnd_Cpr_Writes);                                                                                               //Natural: WRITE 'CPR UPDATES ============> ' #CPR-WRITES
        if (Global.isEscape()) return;
        getReports().write(0, "***************************",NEWLINE,"  END OF ARI1 PROGRAM      ",NEWLINE,"***************************");                                 //Natural: WRITE '***************************' / '  END OF ARI1 PROGRAM      ' / '***************************'
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-CPR
        //* **********************************************************************
        //*  R1. READ IAA-CPR BY CNTRCT-PAYEE-KEY STARTING FROM
        //*               #CNTRCT-PAYEE-KEY
        //* **********************************************************************
    }
    private void sub_Pnd_Read_Cpr() throws Exception                                                                                                                      //Natural: #READ-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                                    //Natural: ASSIGN #PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
        pnd_Cntrct_Payee_Cde_A.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                                                              //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE TO #CNTRCT-PAYEE-CDE-A
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.setValue(pnd_Cntrct_Payee_Cde_A_Pnd_Cntrct_Payee_Cde_2_N);                                                                     //Natural: ASSIGN #PAYEE-CDE := #CNTRCT-PAYEE-CDE-2-N
        vw_iaa_Cpr.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) IAA-CPR WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_iaa_Cpr.readNextRow("F1", true)))
        {
            vw_iaa_Cpr.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cpr.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, NEWLINE,"CONTRACT PAYEE NOT FOUND ON CPR REC",pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr,pnd_Cntrct_Payee_Key_Pnd_Payee_Cde_A);              //Natural: WRITE / 'CONTRACT PAYEE NOT FOUND ON CPR REC' #PPCN-NBR #PAYEE-CDE-A
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Cpr_Rec_Found.nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-CPR-REC-FOUND
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-NOREC
            //*       WRITE '=' IAA-CPR.CNTRCT-STATE-CDE
            pnd_Annt_Rsdncy_Cde_A.reset();                                                                                                                                //Natural: RESET #ANNT-RSDNCY-CDE-A
            pnd_Annt_Rsdncy_Cde_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde()));                      //Natural: COMPRESS '0' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE INTO #ANNT-RSDNCY-CDE-A LEAVING NO SPACE
            iaa_Cpr_Cntrct_Fed_Tax_Amt.setValue(pnd_Fed_Amt);                                                                                                             //Natural: ASSIGN IAA-CPR.CNTRCT-FED-TAX-AMT := #FED-AMT
            iaa_Cpr_Cntrct_State_Cde.setValue(pnd_Annt_Rsdncy_Cde_A);                                                                                                     //Natural: ASSIGN IAA-CPR.CNTRCT-STATE-CDE := #ANNT-RSDNCY-CDE-A
            iaa_Cpr_Cntrct_State_Tax_Amt.setValue(pnd_State_Amt);                                                                                                         //Natural: ASSIGN IAA-CPR.CNTRCT-STATE-TAX-AMT := #STATE-AMT
            if (condition(pnd_Local_Amt.greater(getZero())))                                                                                                              //Natural: IF #LOCAL-AMT > 0
            {
                iaa_Cpr_Cntrct_Local_Cde.setValue("03E");                                                                                                                 //Natural: ASSIGN IAA-CPR.CNTRCT-LOCAL-CDE := '03E'
                iaa_Cpr_Cntrct_Local_Tax_Amt.setValue(pnd_Local_Amt);                                                                                                     //Natural: ASSIGN IAA-CPR.CNTRCT-LOCAL-TAX-AMT := #LOCAL-AMT
                //*  08/10
            }                                                                                                                                                             //Natural: END-IF
            iaa_Cpr_Cntrct_Rtb_Amt.getValue(5).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());                                                              //Natural: ASSIGN IAA-CPR.CNTRCT-RTB-AMT ( 5 ) := CANADIAN-TAX-AMT
            vw_iaa_Cpr.updateDBRow("F1");                                                                                                                                 //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*       BACKOUT TRANSACTION
            pnd_Cpr_Writes.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CPR-WRITES
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr,pnd_Cntrct_Payee_Key_Pnd_Payee_Cde_A);        //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #PPCN-NBR #PAYEE-CDE-A
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
