/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:32 PM
**        * FROM NATURAL PROGRAM : Iaap320
************************************************************
**        * FILE NAME            : Iaap320.java
**        * CLASS NAME           : Iaap320
**        * INSTANCE NAME        : Iaap320
************************************************************
* *******************************************************************
* PROGRAM: IAAP320
*
* DATE   : APRIL 21, 2008 DOCUMENTED
*          CREATES EXTRACT FOR P2295IAD/IAM AND P2300IAD/IAM
*
*
*
* *******************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap320 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField pnd_Parm_Card;

    private DbsGroup pnd_Parm_Card__R_Field_1;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date;

    private DbsGroup pnd_Parm_Card__R_Field_2;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm;

    private DbsGroup pnd_Parm_Card__R_Field_3;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Mm;
    private DbsField pnd_Parm_Card_Pnd_Parm_Check_Date_Dd;
    private DbsField pnd_Parm_Card_Pnd_Parm_Filler;
    private DbsField pnd_Parm_Check_Date_Next;

    private DbsGroup pnd_Parm_Check_Date_Next__R_Field_4;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm;

    private DbsGroup pnd_Parm_Check_Date_Next__R_Field_5;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm;
    private DbsField pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd;
    private DbsField pnd_Check_Date_Ccyymmdd_A;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_6;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Check_Date_Ccyymmdd_A__R_Field_7;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm;
    private DbsField pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd;
    private DbsField pnd_W_Trans_Dte;
    private DbsField pnd_W_Trans_Dte_Next;
    private DbsField pnd_W_Trans_Input_Dte;
    private DbsField pnd_Ccyymmdd;
    private DbsField pnd_W_Count;
    private DbsField pnd_W_Cntrl_Trans_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Parm_Card = localVariables.newFieldInRecord("pnd_Parm_Card", "#PARM-CARD", FieldType.STRING, 80);

        pnd_Parm_Card__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Card__R_Field_1", "REDEFINE", pnd_Parm_Card);
        pnd_Parm_Card_Pnd_Parm_Check_Date = pnd_Parm_Card__R_Field_1.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date", "#PARM-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Parm_Card__R_Field_2 = pnd_Parm_Card__R_Field_1.newGroupInGroup("pnd_Parm_Card__R_Field_2", "REDEFINE", pnd_Parm_Card_Pnd_Parm_Check_Date);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm = pnd_Parm_Card__R_Field_2.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm", "#PARM-CHECK-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);

        pnd_Parm_Card__R_Field_3 = pnd_Parm_Card__R_Field_2.newGroupInGroup("pnd_Parm_Card__R_Field_3", "REDEFINE", pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy = pnd_Parm_Card__R_Field_3.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyy", "#PARM-CHECK-DATE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Mm = pnd_Parm_Card__R_Field_3.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Mm", "#PARM-CHECK-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Card_Pnd_Parm_Check_Date_Dd = pnd_Parm_Card__R_Field_2.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Check_Date_Dd", "#PARM-CHECK-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Parm_Card_Pnd_Parm_Filler = pnd_Parm_Card__R_Field_1.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Filler", "#PARM-FILLER", FieldType.STRING, 72);
        pnd_Parm_Check_Date_Next = localVariables.newFieldInRecord("pnd_Parm_Check_Date_Next", "#PARM-CHECK-DATE-NEXT", FieldType.NUMERIC, 8);

        pnd_Parm_Check_Date_Next__R_Field_4 = localVariables.newGroupInRecord("pnd_Parm_Check_Date_Next__R_Field_4", "REDEFINE", pnd_Parm_Check_Date_Next);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm = pnd_Parm_Check_Date_Next__R_Field_4.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm", 
            "#PARM-CHECK-DATE-NEXT-CCYYMM", FieldType.NUMERIC, 6);

        pnd_Parm_Check_Date_Next__R_Field_5 = pnd_Parm_Check_Date_Next__R_Field_4.newGroupInGroup("pnd_Parm_Check_Date_Next__R_Field_5", "REDEFINE", pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy = pnd_Parm_Check_Date_Next__R_Field_5.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy", 
            "#PARM-CHECK-DATE-NEXT-CCYY", FieldType.NUMERIC, 4);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm = pnd_Parm_Check_Date_Next__R_Field_5.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm", 
            "#PARM-CHECK-DATE-NEXT-MM", FieldType.NUMERIC, 2);
        pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd = pnd_Parm_Check_Date_Next__R_Field_4.newFieldInGroup("pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Dd", 
            "#PARM-CHECK-DATE-NEXT-DD", FieldType.NUMERIC, 2);
        pnd_Check_Date_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd_A", "#CHECK-DATE-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_6", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd = pnd_Check_Date_Ccyymmdd_A__R_Field_6.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd", 
            "#CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Check_Date_Ccyymmdd_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd_A__R_Field_7", "REDEFINE", pnd_Check_Date_Ccyymmdd_A);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm = pnd_Check_Date_Ccyymmdd_A__R_Field_7.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm", 
            "#CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd = pnd_Check_Date_Ccyymmdd_A__R_Field_7.newFieldInGroup("pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Dd", 
            "#CHECK-DATE-DD", FieldType.NUMERIC, 2);
        pnd_W_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Dte", "#W-TRANS-DTE", FieldType.STRING, 8);
        pnd_W_Trans_Dte_Next = localVariables.newFieldInRecord("pnd_W_Trans_Dte_Next", "#W-TRANS-DTE-NEXT", FieldType.STRING, 8);
        pnd_W_Trans_Input_Dte = localVariables.newFieldInRecord("pnd_W_Trans_Input_Dte", "#W-TRANS-INPUT-DTE", FieldType.STRING, 8);
        pnd_Ccyymmdd = localVariables.newFieldInRecord("pnd_Ccyymmdd", "#CCYYMMDD", FieldType.STRING, 8);
        pnd_W_Count = localVariables.newFieldInRecord("pnd_W_Count", "#W-COUNT", FieldType.NUMERIC, 9);
        pnd_W_Cntrl_Trans_Dte = localVariables.newFieldInRecord("pnd_W_Cntrl_Trans_Dte", "#W-CNTRL-TRANS-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap320() throws Exception
    {
        super("Iaap320");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 60
        pnd_Ccyymmdd.setValue(Global.getDATN());                                                                                                                          //Natural: MOVE *DATN TO #CCYYMMDD
        pnd_Parm_Card_Pnd_Parm_Check_Date.setValue(Global.getDATN());                                                                                                     //Natural: MOVE *DATN TO #PARM-CHECK-DATE
        pnd_Parm_Check_Date_Next.setValue(pnd_Parm_Card_Pnd_Parm_Check_Date);                                                                                             //Natural: MOVE #PARM-CHECK-DATE TO #PARM-CHECK-DATE-NEXT
        if (condition(pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.equals(12)))                                                                                   //Natural: IF #PARM-CHECK-DATE-NEXT-MM = 12
        {
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyy.nadd(1);                                                                                               //Natural: ADD 1 TO #PARM-CHECK-DATE-NEXT-CCYY
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.setValue(1);                                                                                             //Natural: MOVE 1 TO #PARM-CHECK-DATE-NEXT-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Mm.nadd(1);                                                                                                 //Natural: ADD 1 TO #PARM-CHECK-DATE-NEXT-MM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Trans_Dte.setValue(99999999);                                                                                                                               //Natural: MOVE 99999999 TO #W-TRANS-DTE #W-TRANS-DTE-NEXT
        pnd_W_Trans_Dte_Next.setValue(99999999);
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_W_Cntrl_Trans_Dte.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED CNTRL-FRST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-CNTRL-TRANS-DTE
            pnd_Check_Date_Ccyymmdd_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD-A
            if (condition(pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymm.equals(pnd_Parm_Card_Pnd_Parm_Check_Date_Ccyymm)))                                              //Natural: IF #CHECK-DATE-CCYYMM = #PARM-CHECK-DATE-CCYYMM
            {
                pnd_W_Trans_Dte.setValue(pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd);                                                                              //Natural: MOVE #CHECK-DATE-CCYYMMDD TO #W-TRANS-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd.equals(pnd_Parm_Check_Date_Next_Pnd_Parm_Check_Date_Next_Ccyymm)))                        //Natural: IF #CHECK-DATE-CCYYMMDD = #PARM-CHECK-DATE-NEXT-CCYYMM
                {
                    pnd_W_Trans_Dte_Next.setValue(pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd);                                                                     //Natural: MOVE #CHECK-DATE-CCYYMMDD TO #W-TRANS-DTE-NEXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  TEST 1/98
        getReports().write(0, "CONTROL-CHECK-DATE: ",pnd_Check_Date_Ccyymmdd_A);                                                                                          //Natural: WRITE 'CONTROL-CHECK-DATE: ' #CHECK-DATE-CCYYMMDD-A
        if (Global.isEscape()) return;
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #CHECK-DATE-CCYYMMDD
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            pnd_W_Trans_Input_Dte.setValueEdited(iaa_Trans_Rcrd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #W-TRANS-INPUT-DTE
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.greater(pnd_Check_Date_Ccyymmdd_A_Pnd_Check_Date_Ccyymmdd)))                                                     //Natural: IF TRANS-CHECK-DTE > #CHECK-DATE-CCYYMMDD
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Count.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #W-COUNT
            getWorkFiles().write(1, false, vw_iaa_Trans_Rcrd);                                                                                                            //Natural: WRITE WORK FILE 01 IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ACCEPTED: ",pnd_W_Count);                                                                                                            //Natural: WRITE 'TOTAL ACCEPTED: ' #W-COUNT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
