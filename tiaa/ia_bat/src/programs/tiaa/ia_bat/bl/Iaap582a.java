/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:54 PM
**        * FROM NATURAL PROGRAM : Iaap582a
************************************************************
**        * FILE NAME            : Iaap582a.java
**        * CLASS NAME           : Iaap582a
**        * INSTANCE NAME        : Iaap582a
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP582A   READ WORK FILE (CREATED IN IAAP581S)   *
*                             AND WRITE TO OUTPUT FILE FOR CPS.      *
*                             THIS WILL BE EXECUTED IN TWO STEPS,    *
*                             ONE FOR NFP AND ONE FOR FFP.           *
*   HISTORY :                                                        *
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017                 *
* 04/2017  O SOTTO  PIN EXPANSION - SC 082017 FOR CHANGES.           *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap582a extends BLNatBase
{
    // Data Areas
    private LdaIaal582a ldaIaal582a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Output_File;
    private DbsField pnd_Output_File_Pnd_O_Chk_Dte;
    private DbsField pnd_Output_File_Pnd_O_For_Dte;
    private DbsField pnd_Output_File_Pnd_O_Name;
    private DbsField pnd_Output_File_Pnd_O_Ssn;
    private DbsField pnd_Output_File_Pnd_O_Pin;
    private DbsField pnd_Output_File_Pnd_O_Contract;
    private DbsField pnd_Output_File_Pnd_O_Stat;
    private DbsField pnd_Output_File_Pnd_O_Pyee_Cde;
    private DbsField pnd_Output_File_Pnd_O_Cntrct_Mde;
    private DbsField pnd_Output_File_Pnd_O_Dod;
    private DbsField pnd_Output_File_Pnd_O_Contact;
    private DbsField pnd_Output_File_Pnd_O_User_Area;
    private DbsField pnd_Output_File_Pnd_O_User_Id;
    private DbsField pnd_Output_File_Pnd_O_Nfp;
    private DbsField pnd_In_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal582a = new LdaIaal582a();
        registerRecord(ldaIaal582a);
        registerRecord(ldaIaal582a.getVw_iaa_Cntrl_Rcrd());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Output_File = localVariables.newGroupInRecord("pnd_Output_File", "#OUTPUT-FILE");
        pnd_Output_File_Pnd_O_Chk_Dte = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Chk_Dte", "#O-CHK-DTE", FieldType.STRING, 8);
        pnd_Output_File_Pnd_O_For_Dte = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_For_Dte", "#O-FOR-DTE", FieldType.STRING, 8);
        pnd_Output_File_Pnd_O_Name = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Name", "#O-NAME", FieldType.STRING, 38);
        pnd_Output_File_Pnd_O_Ssn = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Ssn", "#O-SSN", FieldType.STRING, 9);
        pnd_Output_File_Pnd_O_Pin = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 12);
        pnd_Output_File_Pnd_O_Contract = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Contract", "#O-CONTRACT", FieldType.STRING, 8);
        pnd_Output_File_Pnd_O_Stat = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Stat", "#O-STAT", FieldType.STRING, 2);
        pnd_Output_File_Pnd_O_Pyee_Cde = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Pyee_Cde", "#O-PYEE-CDE", FieldType.STRING, 2);
        pnd_Output_File_Pnd_O_Cntrct_Mde = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Cntrct_Mde", "#O-CNTRCT-MDE", FieldType.NUMERIC, 3);
        pnd_Output_File_Pnd_O_Dod = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Dod", "#O-DOD", FieldType.STRING, 8);
        pnd_Output_File_Pnd_O_Contact = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Contact", "#O-CONTACT", FieldType.STRING, 35);
        pnd_Output_File_Pnd_O_User_Area = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_User_Area", "#O-USER-AREA", FieldType.STRING, 6);
        pnd_Output_File_Pnd_O_User_Id = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_User_Id", "#O-USER-ID", FieldType.STRING, 8);
        pnd_Output_File_Pnd_O_Nfp = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_O_Nfp", "#O-NFP", FieldType.STRING, 1);
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal582a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap582a() throws Exception
    {
        super("Iaap582a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().write(0, "EXECUTING",Global.getPROGRAM());                                                                                                           //Natural: WRITE 'EXECUTING' *PROGRAM
        if (Global.isEscape()) return;
        ldaIaal582a.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal582a.getVw_iaa_Cntrl_Rcrd().readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal582a.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal582a.getPnd_Work_Record())))
        {
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-FILE
            sub_Write_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "WORK FILE READS =====> ",pnd_In_Cnt);                                                                                                      //Natural: WRITE 'WORK FILE READS =====> ' #IN-CNT
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FILE
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Write_File() throws Exception                                                                                                                        //Natural: WRITE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Output_File.reset();                                                                                                                                          //Natural: RESET #OUTPUT-FILE
        pnd_Output_File_Pnd_O_Chk_Dte.setValueEdited(ldaIaal582a.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #O-CHK-DTE
        pnd_Output_File_Pnd_O_For_Dte.setValue(ldaIaal582a.getIaa_Parm_Card_Pnd_Parm_Date());                                                                             //Natural: MOVE #PARM-DATE TO #O-FOR-DTE
        pnd_Output_File_Pnd_O_Name.setValue(DbsUtil.compress(ldaIaal582a.getPnd_Work_Record_Pnd_W_Last_Name(), ldaIaal582a.getPnd_Work_Record_Pnd_W_First_Name(),         //Natural: COMPRESS #W-LAST-NAME #W-FIRST-NAME #W-MIDDLE-NAME INTO #O-NAME
            ldaIaal582a.getPnd_Work_Record_Pnd_W_Middle_Name()));
        pnd_Output_File_Pnd_O_Contract.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Contract());                                                                         //Natural: MOVE #W-CONTRACT TO #O-CONTRACT
        pnd_Output_File_Pnd_O_Stat.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Cont_Pyee());                                                                            //Natural: MOVE #W-CONT-PYEE TO #O-STAT
        pnd_Output_File_Pnd_O_Pyee_Cde.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Annu_Type());                                                                        //Natural: MOVE #W-ANNU-TYPE TO #O-PYEE-CDE
        pnd_Output_File_Pnd_O_Cntrct_Mde.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Contract_Mode());                                                                  //Natural: MOVE #W-CONTRACT-MODE TO #O-CNTRCT-MDE
        pnd_Output_File_Pnd_O_Dod.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Dod_Dte());                                                                               //Natural: MOVE #W-DOD-DTE TO #O-DOD
        pnd_Output_File_Pnd_O_Contact.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Notify_Name());                                                                       //Natural: MOVE #W-NOTIFY-NAME TO #O-CONTACT
        pnd_Output_File_Pnd_O_User_Id.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_User_Id());                                                                           //Natural: MOVE #W-USER-ID TO #O-USER-ID
        pnd_Output_File_Pnd_O_User_Area.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_User_Area());                                                                       //Natural: MOVE #W-USER-AREA TO #O-USER-AREA
        pnd_Output_File_Pnd_O_Pin.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Pin());                                                                                   //Natural: MOVE #W-PIN TO #O-PIN
        pnd_Output_File_Pnd_O_Ssn.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Ssn());                                                                                   //Natural: MOVE #W-SSN TO #O-SSN
        pnd_Output_File_Pnd_O_Nfp.setValue(ldaIaal582a.getPnd_Work_Record_Pnd_W_Nfp());                                                                                   //Natural: MOVE #W-NFP TO #O-NFP
        getWorkFiles().write(3, false, pnd_Output_File);                                                                                                                  //Natural: WRITE WORK FILE 3 #OUTPUT-FILE
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal582a.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal582a.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
