/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:38 PM
**        * FROM NATURAL PROGRAM : Iaapxfrs
************************************************************
**        * FILE NAME            : Iaapxfrs.java
**        * CLASS NAME           : Iaapxfrs
**        * INSTANCE NAME        : Iaapxfrs
************************************************************
************************************************************************
* PROGRAM: IAAPXFRS
*
* SYSTEM:  IA POST SETTLEMENT TRANSFERS
*
* PURPOSE: CREATE A DAILY CUMULATIVE FILE FEED TO CLEARSKY OF ALL
*          IA POST-SETTLEMENT TRANSFERS TO CREF.
*
* AUTHOR:  JUN F. TINIO
*
* DATE:    4/25/2007 - ORIGINAL CODE
*
* HISTORY:
* JUN 2017 J BREMER       PIN EXPANSION - STOW ONLY
* SEPT 2017 J TINIO       EXPAND TOTAL AMOUNT FIELD TO RESOLVE
*                         TRUNCATION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapxfrs extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_Rqst_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Wpid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_In_Progress_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Retry_Cnt;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Rate_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Acctng_Dte;
    private DbsField iaa_Xfr_Audit_Lst_Chnge_Dte;
    private DbsField iaa_Xfr_Audit_Rqst_Xfr_Type;
    private DbsField iaa_Xfr_Audit_Rqst_Unit_Cde;
    private DbsField iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField pnd_Fnd_Cde;
    private DbsField pnd_Fnd_Tckr;
    private DbsField pnd_Fnd_Dsc;
    private DbsField pnd_Iaxfr_Audit_De_1;

    private DbsGroup pnd_Iaxfr_Audit_De_1__R_Field_1;
    private DbsField pnd_Iaxfr_Audit_De_1_Pnd_Iaxfr_Cycle_Dte;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_2;
    private DbsField pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField pnd_St_Abbrv;
    private DbsField pnd_To_Fnd_Cnt;
    private DbsField pnd_Fnd_Dtl;
    private DbsField pnd_Fnd_Dtl_Amt;
    private DbsField pnd_Fnd_Trlr;
    private DbsField pnd_Fnd_Trlr_Amt;
    private DbsField pnd_St_Indx;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_To_Fnd_Cde;

    private DbsGroup pnd_Output_Pnd_To_Data;
    private DbsField pnd_Output_Pnd_Iss_St;
    private DbsField pnd_Output_Pnd_To_Asset_Amt;

    private DbsGroup pnd_Bsky_Output;
    private DbsField pnd_Bsky_Output_Pnd_Rec_Type;
    private DbsField pnd_Bsky_Output_Pnd_Rest_Of_Rcrd;

    private DbsGroup pnd_Bsky_Output__R_Field_3;

    private DbsGroup pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr;
    private DbsField pnd_Bsky_Output_Pnd_Bscss;
    private DbsField pnd_Bsky_Output_Pnd_Bus_Dte;
    private DbsField pnd_Bsky_Output_Pnd_F1;
    private DbsField pnd_Bsky_Output_Pnd_Tiaa_Cref;
    private DbsField pnd_Bsky_Output_Pnd_F2;
    private DbsField pnd_Bsky_Output_Pnd_Lia;
    private DbsField pnd_Bsky_Output_Pnd_F3;

    private DbsGroup pnd_Bsky_Output__R_Field_4;

    private DbsGroup pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr;
    private DbsField pnd_Bsky_Output_Pnd_H_Tckr;
    private DbsField pnd_Bsky_Output_Pnd_F4;
    private DbsField pnd_Bsky_Output_Pnd_Fnd_Nme;
    private DbsField pnd_Bsky_Output_Pnd_F5;

    private DbsGroup pnd_Bsky_Output__R_Field_5;

    private DbsGroup pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl;
    private DbsField pnd_Bsky_Output_Pnd_D_Tckr;
    private DbsField pnd_Bsky_Output_Pnd_St_Cde;
    private DbsField pnd_Bsky_Output_Pnd_F6;
    private DbsField pnd_Bsky_Output_Pnd_P1;
    private DbsField pnd_Bsky_Output_Pnd_Tot_St_Fnd_Amt;
    private DbsField pnd_Bsky_Output_Pnd_Tot_St_Fnd_Shrs;
    private DbsField pnd_Bsky_Output_Pnd_Opt1;
    private DbsField pnd_Bsky_Output_Pnd_Opt2;

    private DbsGroup pnd_Bsky_Output__R_Field_6;

    private DbsGroup pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr;
    private DbsField pnd_Bsky_Output_Pnd_T_Tckr;
    private DbsField pnd_Bsky_Output_Pnd_Fnd_Cnt;
    private DbsField pnd_Bsky_Output_Pnd_P2;
    private DbsField pnd_Bsky_Output_Pnd_Tot_Fnd_Amt;
    private DbsField pnd_Bsky_Output_Pnd_Tot_Fnd_Shrs;
    private DbsField pnd_Bsky_Output_Pnd_Opt3;
    private DbsField pnd_Bsky_Output_Pnd_Opt4;

    private DbsGroup pnd_Bsky_Output__R_Field_7;

    private DbsGroup pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr;
    private DbsField pnd_Bsky_Output_Pnd_F7;
    private DbsField pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Cnt;
    private DbsField pnd_Bsky_Output_Pnd_P3;
    private DbsField pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Amt;
    private DbsField pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Shrs;
    private DbsField pnd_Bsky_Output_Pnd_Opt5;
    private DbsField pnd_Bsky_Output_Pnd_Opt6;
    private DbsField pnd_I;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_J;
    private DbsField pnd_M;
    private DbsField pnd_Begin_Effctve_Dte;
    private DbsField pnd_End_Effctve_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_Rcrd_Type_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte", "IAXFR-INVRSE-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_INVRSE_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14, RepeatingFieldStrategy.None, "IAXFR_INVRSE_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_Iaxfr_Entry_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_ENTRY_TME");
        iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr", "IAXFR-CALC-STTMNT-INDCTR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_STTMNT_INDCTR");
        iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        iaa_Xfr_Audit_Iaxfr_Cwf_Wpid = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IAXFR_CWF_WPID");
        iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "IAXFR_CWF_LOG_DTE_TIME");
        iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_REJECT_CDE");
        iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_OPN_CLSD_IND");
        iaa_Xfr_Audit_Iaxfr_In_Progress_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "IAXFR_IN_PROGRESS_IND");
        iaa_Xfr_Audit_Iaxfr_Retry_Cnt = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "IAXFR_RETRY_CNT");

        iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupArrayInGroup("iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", "IAXFR-FROM-FUND-CDE", 
            FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", "IAXFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Qty = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_CURRENT_PMT_GUAR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_CURRENT_PMT_DIVID", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_CURRENT_PMT_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", "IAXFR-FROM-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", "IAXFR-FROM-RQSTD-XFR-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_RQSTD_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_RQSTD_XFR_UNITS", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", "IAXFR-FROM-RQSTD-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_RQSTD_XFR_DIVID", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", "IAXFR-FROM-ASSET-XFR-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_ASSET_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", "IAXFR-FROM-PMT-AFTR-XFR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", "IAXFR-FROM-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", "IAXFR-FROM-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", "IAXFR-FROM-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupArrayInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", "IAXFR-TO-NEW-FUND-REC", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Qty = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", "IAXFR-TO-BFR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", "IAXFR-TO-BFR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", "IAXFR-TO-BFR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", "IAXFR-TO-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", "IAXFR-TO-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Rate_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Rate_Cde", "IAXFR-TO-RATE-CDE", 
            FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_RATE_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", "IAXFR-TO-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", "IAXFR-TO-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", "IAXFR-TO-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", "IAXFR-TO-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_Iaxfr_Acctng_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ACCTNG_DTE");
        iaa_Xfr_Audit_Lst_Chnge_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        iaa_Xfr_Audit_Rqst_Xfr_Type = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Xfr_Audit_Rqst_Unit_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Xfr_Audit);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Fnd_Cde = localVariables.newFieldArrayInRecord("pnd_Fnd_Cde", "#FND-CDE", FieldType.STRING, 1, new DbsArrayController(1, 9));
        pnd_Fnd_Tckr = localVariables.newFieldArrayInRecord("pnd_Fnd_Tckr", "#FND-TCKR", FieldType.STRING, 8, new DbsArrayController(1, 9));
        pnd_Fnd_Dsc = localVariables.newFieldArrayInRecord("pnd_Fnd_Dsc", "#FND-DSC", FieldType.STRING, 30, new DbsArrayController(1, 9));
        pnd_Iaxfr_Audit_De_1 = localVariables.newFieldInRecord("pnd_Iaxfr_Audit_De_1", "#IAXFR-AUDIT-DE-1", FieldType.BINARY, 8);

        pnd_Iaxfr_Audit_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaxfr_Audit_De_1__R_Field_1", "REDEFINE", pnd_Iaxfr_Audit_De_1);
        pnd_Iaxfr_Audit_De_1_Pnd_Iaxfr_Cycle_Dte = pnd_Iaxfr_Audit_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Audit_De_1_Pnd_Iaxfr_Cycle_Dte", "#IAXFR-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_2", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        registerRecord(vw_cpr);

        pnd_St_Abbrv = localVariables.newFieldInRecord("pnd_St_Abbrv", "#ST-ABBRV", FieldType.STRING, 2);
        pnd_To_Fnd_Cnt = localVariables.newFieldInRecord("pnd_To_Fnd_Cnt", "#TO-FND-CNT", FieldType.INTEGER, 2);
        pnd_Fnd_Dtl = localVariables.newFieldInRecord("pnd_Fnd_Dtl", "#FND-DTL", FieldType.NUMERIC, 5);
        pnd_Fnd_Dtl_Amt = localVariables.newFieldInRecord("pnd_Fnd_Dtl_Amt", "#FND-DTL-AMT", FieldType.NUMERIC, 16, 2);
        pnd_Fnd_Trlr = localVariables.newFieldInRecord("pnd_Fnd_Trlr", "#FND-TRLR", FieldType.NUMERIC, 5);
        pnd_Fnd_Trlr_Amt = localVariables.newFieldInRecord("pnd_Fnd_Trlr_Amt", "#FND-TRLR-AMT", FieldType.NUMERIC, 16, 2);
        pnd_St_Indx = localVariables.newFieldInRecord("pnd_St_Indx", "#ST-INDX", FieldType.INTEGER, 2);

        pnd_Output = localVariables.newGroupArrayInRecord("pnd_Output", "#OUTPUT", new DbsArrayController(1, 20));
        pnd_Output_Pnd_To_Fnd_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_To_Fnd_Cde", "#TO-FND-CDE", FieldType.STRING, 1);

        pnd_Output_Pnd_To_Data = pnd_Output.newGroupArrayInGroup("pnd_Output_Pnd_To_Data", "#TO-DATA", new DbsArrayController(1, 50));
        pnd_Output_Pnd_Iss_St = pnd_Output_Pnd_To_Data.newFieldInGroup("pnd_Output_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 2);
        pnd_Output_Pnd_To_Asset_Amt = pnd_Output_Pnd_To_Data.newFieldInGroup("pnd_Output_Pnd_To_Asset_Amt", "#TO-ASSET-AMT", FieldType.NUMERIC, 11, 2);

        pnd_Bsky_Output = localVariables.newGroupInRecord("pnd_Bsky_Output", "#BSKY-OUTPUT");
        pnd_Bsky_Output_Pnd_Rec_Type = pnd_Bsky_Output.newFieldInGroup("pnd_Bsky_Output_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 1);
        pnd_Bsky_Output_Pnd_Rest_Of_Rcrd = pnd_Bsky_Output.newFieldInGroup("pnd_Bsky_Output_Pnd_Rest_Of_Rcrd", "#REST-OF-RCRD", FieldType.STRING, 79);

        pnd_Bsky_Output__R_Field_3 = pnd_Bsky_Output.newGroupInGroup("pnd_Bsky_Output__R_Field_3", "REDEFINE", pnd_Bsky_Output_Pnd_Rest_Of_Rcrd);

        pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr = pnd_Bsky_Output__R_Field_3.newGroupInGroup("pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr", "#BSKY-MN-HDR");
        pnd_Bsky_Output_Pnd_Bscss = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_Bscss", "#BSCSS", FieldType.STRING, 6);
        pnd_Bsky_Output_Pnd_Bus_Dte = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_Bus_Dte", "#BUS-DTE", FieldType.STRING, 6);
        pnd_Bsky_Output_Pnd_F1 = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Bsky_Output_Pnd_Tiaa_Cref = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 
            20);
        pnd_Bsky_Output_Pnd_F2 = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Bsky_Output_Pnd_Lia = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_Lia", "#LIA", FieldType.STRING, 3);
        pnd_Bsky_Output_Pnd_F3 = pnd_Bsky_Output_Pnd_Bsky_Mn_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_F3", "#F3", FieldType.STRING, 42);

        pnd_Bsky_Output__R_Field_4 = pnd_Bsky_Output.newGroupInGroup("pnd_Bsky_Output__R_Field_4", "REDEFINE", pnd_Bsky_Output_Pnd_Rest_Of_Rcrd);

        pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr = pnd_Bsky_Output__R_Field_4.newGroupInGroup("pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr", "#BSKY-FND-HDR");
        pnd_Bsky_Output_Pnd_H_Tckr = pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_H_Tckr", "#H-TCKR", FieldType.STRING, 8);
        pnd_Bsky_Output_Pnd_F4 = pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_F4", "#F4", FieldType.STRING, 5);
        pnd_Bsky_Output_Pnd_Fnd_Nme = pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_Fnd_Nme", "#FND-NME", FieldType.STRING, 30);
        pnd_Bsky_Output_Pnd_F5 = pnd_Bsky_Output_Pnd_Bsky_Fnd_Hdr.newFieldInGroup("pnd_Bsky_Output_Pnd_F5", "#F5", FieldType.STRING, 36);

        pnd_Bsky_Output__R_Field_5 = pnd_Bsky_Output.newGroupInGroup("pnd_Bsky_Output__R_Field_5", "REDEFINE", pnd_Bsky_Output_Pnd_Rest_Of_Rcrd);

        pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl = pnd_Bsky_Output__R_Field_5.newGroupInGroup("pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl", "#BSKY-ST-FND-DTL");
        pnd_Bsky_Output_Pnd_D_Tckr = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_D_Tckr", "#D-TCKR", FieldType.STRING, 8);
        pnd_Bsky_Output_Pnd_St_Cde = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_St_Cde", "#ST-CDE", FieldType.STRING, 3);
        pnd_Bsky_Output_Pnd_F6 = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_F6", "#F6", FieldType.STRING, 2);
        pnd_Bsky_Output_Pnd_P1 = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_P1", "#P1", FieldType.STRING, 6);
        pnd_Bsky_Output_Pnd_Tot_St_Fnd_Amt = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_St_Fnd_Amt", "#TOT-ST-FND-AMT", 
            FieldType.STRING, 12);
        pnd_Bsky_Output_Pnd_Tot_St_Fnd_Shrs = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_St_Fnd_Shrs", "#TOT-ST-FND-SHRS", 
            FieldType.STRING, 18);
        pnd_Bsky_Output_Pnd_Opt1 = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt1", "#OPT1", FieldType.STRING, 15);
        pnd_Bsky_Output_Pnd_Opt2 = pnd_Bsky_Output_Pnd_Bsky_St_Fnd_Dtl.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt2", "#OPT2", FieldType.STRING, 15);

        pnd_Bsky_Output__R_Field_6 = pnd_Bsky_Output.newGroupInGroup("pnd_Bsky_Output__R_Field_6", "REDEFINE", pnd_Bsky_Output_Pnd_Rest_Of_Rcrd);

        pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr = pnd_Bsky_Output__R_Field_6.newGroupInGroup("pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr", "#BSKY-FND-TRLR");
        pnd_Bsky_Output_Pnd_T_Tckr = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_T_Tckr", "#T-TCKR", FieldType.STRING, 8);
        pnd_Bsky_Output_Pnd_Fnd_Cnt = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Fnd_Cnt", "#FND-CNT", FieldType.NUMERIC, 
            5);
        pnd_Bsky_Output_Pnd_P2 = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_P2", "#P2", FieldType.STRING, 1);
        pnd_Bsky_Output_Pnd_Tot_Fnd_Amt = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_Fnd_Amt", "#TOT-FND-AMT", FieldType.STRING, 
            17);
        pnd_Bsky_Output_Pnd_Tot_Fnd_Shrs = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_Fnd_Shrs", "#TOT-FND-SHRS", FieldType.STRING, 
            18);
        pnd_Bsky_Output_Pnd_Opt3 = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt3", "#OPT3", FieldType.STRING, 15);
        pnd_Bsky_Output_Pnd_Opt4 = pnd_Bsky_Output_Pnd_Bsky_Fnd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt4", "#OPT4", FieldType.STRING, 15);

        pnd_Bsky_Output__R_Field_7 = pnd_Bsky_Output.newGroupInGroup("pnd_Bsky_Output__R_Field_7", "REDEFINE", pnd_Bsky_Output_Pnd_Rest_Of_Rcrd);

        pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr = pnd_Bsky_Output__R_Field_7.newGroupInGroup("pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr", "#BSKY-RCRD-TRLR");
        pnd_Bsky_Output_Pnd_F7 = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_F7", "#F7", FieldType.STRING, 8);
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Cnt = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Cnt", "#TOT-TRLR-FND-CNT", 
            FieldType.NUMERIC, 5);
        pnd_Bsky_Output_Pnd_P3 = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_P3", "#P3", FieldType.STRING, 1);
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Amt = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Amt", "#TOT-TRLR-FND-AMT", 
            FieldType.STRING, 17);
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Shrs = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Shrs", "#TOT-TRLR-FND-SHRS", 
            FieldType.STRING, 18);
        pnd_Bsky_Output_Pnd_Opt5 = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt5", "#OPT5", FieldType.STRING, 15);
        pnd_Bsky_Output_Pnd_Opt6 = pnd_Bsky_Output_Pnd_Bsky_Rcrd_Trlr.newFieldInGroup("pnd_Bsky_Output_Pnd_Opt6", "#OPT6", FieldType.STRING, 15);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.INTEGER, 2);
        pnd_Begin_Effctve_Dte = localVariables.newFieldInRecord("pnd_Begin_Effctve_Dte", "#BEGIN-EFFCTVE-DTE", FieldType.DATE);
        pnd_End_Effctve_Dte = localVariables.newFieldInRecord("pnd_End_Effctve_Dte", "#END-EFFCTVE-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Audit.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_cpr.reset();

        localVariables.reset();
        pnd_Fnd_Cde.getValue(1).setInitialValue("B");
        pnd_Fnd_Cde.getValue(2).setInitialValue("E");
        pnd_Fnd_Cde.getValue(3).setInitialValue("W");
        pnd_Fnd_Cde.getValue(4).setInitialValue("L");
        pnd_Fnd_Cde.getValue(5).setInitialValue("I");
        pnd_Fnd_Cde.getValue(6).setInitialValue("M");
        pnd_Fnd_Cde.getValue(7).setInitialValue("S");
        pnd_Fnd_Cde.getValue(8).setInitialValue("R");
        pnd_Fnd_Cde.getValue(9).setInitialValue("C");
        pnd_Fnd_Tckr.getValue(1).setInitialValue("CBND");
        pnd_Fnd_Tckr.getValue(2).setInitialValue("CEQX");
        pnd_Fnd_Tckr.getValue(3).setInitialValue("CGLB");
        pnd_Fnd_Tckr.getValue(4).setInitialValue("CGRW");
        pnd_Fnd_Tckr.getValue(5).setInitialValue("CILB");
        pnd_Fnd_Tckr.getValue(6).setInitialValue("CMMA");
        pnd_Fnd_Tckr.getValue(7).setInitialValue("CSCL");
        pnd_Fnd_Tckr.getValue(8).setInitialValue("TREA");
        pnd_Fnd_Tckr.getValue(9).setInitialValue("CSTK");
        pnd_Fnd_Dsc.getValue(1).setInitialValue("CREF BOND MARKET");
        pnd_Fnd_Dsc.getValue(2).setInitialValue("CREF EQUITY INDEX");
        pnd_Fnd_Dsc.getValue(3).setInitialValue("CREF GLOBAL EQUITIES");
        pnd_Fnd_Dsc.getValue(4).setInitialValue("CREF GROWTH");
        pnd_Fnd_Dsc.getValue(5).setInitialValue("CREF INFLATION-LINKED BOND");
        pnd_Fnd_Dsc.getValue(6).setInitialValue("CREF MONEY MARKET");
        pnd_Fnd_Dsc.getValue(7).setInitialValue("CREF SOCIAL CHOICE");
        pnd_Fnd_Dsc.getValue(8).setInitialValue("TIAA REAL ESTATE");
        pnd_Fnd_Dsc.getValue(9).setInitialValue("CREF STOCK");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapxfrs() throws Exception
    {
        super("Iaapxfrs");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Cde.equals("DC")))                                                                                                       //Natural: IF CNTRL-CDE = 'DC'
            {
                pnd_Bsky_Output_Pnd_Bus_Dte.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYMMDD"));                                               //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYMMDD ) TO #BUS-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Iaxfr_Audit_De_1_Pnd_Iaxfr_Cycle_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),"20070101");                                                               //Natural: MOVE EDITED '20070101' TO #IAXFR-CYCLE-DTE ( EM = YYYYMMDD )
        pnd_Begin_Effctve_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),"20070101");                                                                                  //Natural: MOVE EDITED '20070101' TO #BEGIN-EFFCTVE-DTE ( EM = YYYYMMDD )
        //* *MOVE EDITED '20070228' TO #END-EFFCTVE-DTE(EM=YYYYMMDD)
        vw_iaa_Xfr_Audit.startDatabaseRead                                                                                                                                //Natural: READ IAA-XFR-AUDIT BY IAXFR-AUDIT-DE-1 STARTING FROM #IAXFR-AUDIT-DE-1
        (
        "READ02",
        new Wc[] { new Wc("IAXFR_AUDIT_DE_1", ">=", pnd_Iaxfr_Audit_De_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IAXFR_AUDIT_DE_1", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Xfr_Audit.readNextRow("READ02")))
        {
            if (condition(!(((iaa_Xfr_Audit_Iaxfr_Effctve_Dte.greaterOrEqual(pnd_Begin_Effctve_Dte) && DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde,"'M'"))    //Natural: ACCEPT IF IAXFR-EFFCTVE-DTE >= #BEGIN-EFFCTVE-DTE AND IAXFR-CALC-STATUS-CDE = MASK ( 'M' ) AND IAXFR-TO-ACCT-CD ( * ) = 'B' OR = 'E' OR = 'W' OR = 'L' OR = 'I' OR = 'M' OR = 'S' OR = 'C' OR = 'R'
                && ((((((((iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("B") || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("E")) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("W")) 
                || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("L")) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("I")) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("M")) 
                || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("S")) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("C")) || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*").equals("R"))))))
            {
                continue;
            }
            //*    AND IAXFR-EFFCTVE-DTE <= #END-EFFCTVE-DTE
                                                                                                                                                                          //Natural: PERFORM GET-STATE-CDE
            sub_Get_State_Cde();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_St_Abbrv.equals(" ")))                                                                                                                      //Natural: IF #ST-ABBRV = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                             //Natural: IF IAXFR-TO-ACCT-CD ( #I ) NE ' '
                {
                    if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals(pnd_Fnd_Cde.getValue("*"))))                                                      //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = #FND-CDE ( * )
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Output_Pnd_To_Fnd_Cde.getValue("*")), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I)),    //Natural: EXAMINE #TO-FND-CDE ( * ) FOR IAXFR-TO-ACCT-CD ( #I ) GIVING INDEX #K
                            new ExamineGivingIndex(pnd_K));
                        if (condition(pnd_K.equals(getZero())))                                                                                                           //Natural: IF #K = 0
                        {
                            pnd_To_Fnd_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TO-FND-CNT
                            pnd_K.setValue(pnd_To_Fnd_Cnt);                                                                                                               //Natural: ASSIGN #K := #TO-FND-CNT
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Output_Pnd_To_Fnd_Cde.getValue(pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I));                                               //Natural: ASSIGN #TO-FND-CDE ( #K ) := IAXFR-TO-ACCT-CD ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INDEX
                        sub_Get_State_Index();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Output_Pnd_To_Asset_Amt.getValue(pnd_K,pnd_St_Indx).nadd(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I));                                   //Natural: ADD IAXFR-TO-ASSET-AMT ( #I ) TO #TO-ASSET-AMT ( #K,#ST-INDX )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  HEADER RECORD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  CREATE THE CLEAR SKY FILE
        pnd_Bsky_Output_Pnd_Rec_Type.setValue("0");                                                                                                                       //Natural: ASSIGN #REC-TYPE := '0'
        pnd_Bsky_Output_Pnd_Bscss.setValue("BSCSS");                                                                                                                      //Natural: ASSIGN #BSCSS := 'BSCSS'
        //* *#BUS-DTE := '070430'
        pnd_Bsky_Output_Pnd_Tiaa_Cref.setValue("TIAA CREF");                                                                                                              //Natural: ASSIGN #TIAA-CREF := 'TIAA CREF'
        pnd_Bsky_Output_Pnd_Lia.setValue("LIA");                                                                                                                          //Natural: ASSIGN #LIA := 'LIA'
        //*  HEADER RECORD
        getWorkFiles().write(1, false, pnd_Bsky_Output);                                                                                                                  //Natural: WRITE WORK FILE 1 #BSKY-OUTPUT
        pnd_Bsky_Output_Pnd_Rest_Of_Rcrd.reset();                                                                                                                         //Natural: RESET #REST-OF-RCRD
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pnd_Output_Pnd_To_Fnd_Cde.getValue(pnd_I).notEquals(" ")))                                                                                      //Natural: IF #TO-FND-CDE ( #I ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-FUND-HEADER
                sub_Create_Fund_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #J 1 TO 50
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(50)); pnd_J.nadd(1))
                {
                    if (condition(pnd_Output_Pnd_To_Asset_Amt.getValue(pnd_I,pnd_J).greater(getZero())))                                                                  //Natural: IF #TO-ASSET-AMT ( #I,#J ) GT 0
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-FUND-DETAIL
                        sub_Create_Fund_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-FUND-TRAILER
                sub_Create_Fund_Trailer();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  TRANSMISSION TRAILER
            //*  CHANGED FROM +000000 TO +
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE TRANSMISSION TRAILER RECORD
        pnd_Bsky_Output_Pnd_Rec_Type.setValue("4");                                                                                                                       //Natural: ASSIGN #REC-TYPE := '4'
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Cnt.setValue(pnd_Fnd_Trlr);                                                                                                      //Natural: ASSIGN #TOT-TRLR-FND-CNT := #FND-TRLR
        pnd_Bsky_Output_Pnd_P3.setValue("+");                                                                                                                             //Natural: ASSIGN #P3 := '+'
        //* *MOVE EDITED #FND-TRLR-AMT (EM=999999999.99) TO #TOT-TRLR-FND-AMT
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Amt.setValueEdited(pnd_Fnd_Trlr_Amt,new ReportEditMask("99999999999999.99"));                                                    //Natural: MOVE EDITED #FND-TRLR-AMT ( EM = 99999999999999.99 ) TO #TOT-TRLR-FND-AMT
        pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Shrs.setValue("+0000000000000.000");                                                                                             //Natural: ASSIGN #TOT-TRLR-FND-SHRS := '+0000000000000.000'
        pnd_Bsky_Output_Pnd_Opt5.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT5 := '+00000000000.00'
        pnd_Bsky_Output_Pnd_Opt6.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT6 := '+00000000000.00'
        //*  TRANSMISSION TRAILER RECORD
        getWorkFiles().write(1, false, pnd_Bsky_Output);                                                                                                                  //Natural: WRITE WORK FILE 1 #BSKY-OUTPUT
        getReports().write(1, "Total Funds Transmitted:",pnd_Bsky_Output_Pnd_Tot_Trlr_Fnd_Cnt, new ReportEditMask ("'          'ZZ,ZZ9"),NEWLINE,"Total Fund Amount      :",pnd_Fnd_Trlr_Amt,  //Natural: WRITE ( 1 ) 'Total Funds Transmitted:' #TOT-TRLR-FND-CNT ( EM = '          'ZZ,ZZ9 ) / 'Total Fund Amount      :' #FND-TRLR-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  'Total Funds Transmitted:' #TOT-TRLR-FND-CNT(EM='      'ZZ,ZZ9) /
        //*  'Total Fund Amount      :' #FND-TRLR-AMT    (EM=ZZZ,ZZZ,ZZ9.99)
        //*  9/2017 END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUND-HEADER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUND-DETAIL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUND-TRAILER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-CDE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INDEX
    }
    //*  FUND HEADER RECORD
    private void sub_Create_Fund_Header() throws Exception                                                                                                                //Natural: CREATE-FUND-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bsky_Output_Pnd_Rec_Type.setValue("1");                                                                                                                       //Natural: ASSIGN #REC-TYPE := '1'
        DbsUtil.examine(new ExamineSource(pnd_Fnd_Cde.getValue("*")), new ExamineSearch(pnd_Output_Pnd_To_Fnd_Cde.getValue(pnd_I)), new ExamineGivingIndex(pnd_K));       //Natural: EXAMINE #FND-CDE ( * ) FOR #TO-FND-CDE ( #I ) GIVING INDEX #K
        pnd_Bsky_Output_Pnd_H_Tckr.setValue(pnd_Fnd_Tckr.getValue(pnd_K));                                                                                                //Natural: ASSIGN #H-TCKR := #FND-TCKR ( #K )
        pnd_Bsky_Output_Pnd_Fnd_Nme.setValue(pnd_Fnd_Dsc.getValue(pnd_K));                                                                                                //Natural: ASSIGN #FND-NME := #FND-DSC ( #K )
        //*  FUND HEADER
        getWorkFiles().write(1, false, pnd_Bsky_Output);                                                                                                                  //Natural: WRITE WORK FILE 1 #BSKY-OUTPUT
        pnd_Bsky_Output_Pnd_Rest_Of_Rcrd.reset();                                                                                                                         //Natural: RESET #REST-OF-RCRD
    }
    private void sub_Create_Fund_Detail() throws Exception                                                                                                                //Natural: CREATE-FUND-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fnd_Dtl.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #FND-DTL
        //*  FUND DETAIL
        pnd_Fnd_Dtl_Amt.nadd(pnd_Output_Pnd_To_Asset_Amt.getValue(pnd_I,pnd_J));                                                                                          //Natural: ADD #TO-ASSET-AMT ( #I,#J ) TO #FND-DTL-AMT
        pnd_Bsky_Output_Pnd_Rec_Type.setValue("2");                                                                                                                       //Natural: ASSIGN #REC-TYPE := '2'
        DbsUtil.examine(new ExamineSource(pnd_Fnd_Cde.getValue("*")), new ExamineSearch(pnd_Output_Pnd_To_Fnd_Cde.getValue(pnd_I)), new ExamineGivingIndex(pnd_K));       //Natural: EXAMINE #FND-CDE ( * ) FOR #TO-FND-CDE ( #I ) GIVING INDEX #K
        pnd_Bsky_Output_Pnd_D_Tckr.setValue(pnd_Fnd_Tckr.getValue(pnd_K));                                                                                                //Natural: ASSIGN #D-TCKR := #FND-TCKR ( #K )
        pnd_Bsky_Output_Pnd_St_Cde.setValue(pnd_Output_Pnd_Iss_St.getValue(pnd_I,pnd_J));                                                                                 //Natural: ASSIGN #ST-CDE := #ISS-ST ( #I,#J )
        pnd_Bsky_Output_Pnd_P1.setValue("+00000");                                                                                                                        //Natural: ASSIGN #P1 := '+00000'
        pnd_Bsky_Output_Pnd_Tot_St_Fnd_Amt.setValueEdited(pnd_Output_Pnd_To_Asset_Amt.getValue(pnd_I,pnd_J),new ReportEditMask("999999999.99"));                          //Natural: MOVE EDITED #TO-ASSET-AMT ( #I,#J ) ( EM = 999999999.99 ) TO #TOT-ST-FND-AMT
        pnd_Bsky_Output_Pnd_Tot_St_Fnd_Shrs.setValue("+0000000000000.000");                                                                                               //Natural: ASSIGN #TOT-ST-FND-SHRS := '+0000000000000.000'
        pnd_Bsky_Output_Pnd_Opt1.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT1 := '+00000000000.00'
        pnd_Bsky_Output_Pnd_Opt2.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT2 := '+00000000000.00'
        //*  FUND DETAIL
        getWorkFiles().write(1, false, pnd_Bsky_Output);                                                                                                                  //Natural: WRITE WORK FILE 1 #BSKY-OUTPUT
        getReports().display(1, "/Ticker",                                                                                                                                //Natural: DISPLAY ( 1 ) '/Ticker' #D-TCKR 'St/Cde' #ST-CDE 'Fund/Amount' #TO-ASSET-AMT ( #I,#J ) ( EM = ZZZ,ZZZ,ZZ9.99 )
        		pnd_Bsky_Output_Pnd_D_Tckr,"St/Cde",
        		pnd_Bsky_Output_Pnd_St_Cde,"Fund/Amount",
        		pnd_Output_Pnd_To_Asset_Amt.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Bsky_Output_Pnd_Rest_Of_Rcrd.reset();                                                                                                                         //Natural: RESET #REST-OF-RCRD
    }
    private void sub_Create_Fund_Trailer() throws Exception                                                                                                               //Natural: CREATE-FUND-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fnd_Trlr.nadd(pnd_Fnd_Dtl);                                                                                                                                   //Natural: ADD #FND-DTL TO #FND-TRLR
        //*  FUND TRAILER
        pnd_Fnd_Trlr_Amt.nadd(pnd_Fnd_Dtl_Amt);                                                                                                                           //Natural: ADD #FND-DTL-AMT TO #FND-TRLR-AMT
        pnd_Bsky_Output_Pnd_Rec_Type.setValue("3");                                                                                                                       //Natural: ASSIGN #REC-TYPE := '3'
        //*  9/2017
        DbsUtil.examine(new ExamineSource(pnd_Fnd_Cde.getValue("*")), new ExamineSearch(pnd_Output_Pnd_To_Fnd_Cde.getValue(pnd_I)), new ExamineGivingIndex(pnd_K));       //Natural: EXAMINE #FND-CDE ( * ) FOR #TO-FND-CDE ( #I ) GIVING INDEX #K
        pnd_Bsky_Output_Pnd_T_Tckr.setValue(pnd_Fnd_Tckr.getValue(pnd_K));                                                                                                //Natural: ASSIGN #T-TCKR := #FND-TCKR ( #K )
        pnd_Bsky_Output_Pnd_Fnd_Cnt.setValue(pnd_Fnd_Dtl);                                                                                                                //Natural: ASSIGN #FND-CNT := #FND-DTL
        pnd_Bsky_Output_Pnd_P2.setValue("+");                                                                                                                             //Natural: ASSIGN #P2 := '+'
        //* *MOVE EDITED #FND-DTL-AMT (EM=999999999.99) TO #TOT-FND-AMT 9/2017
        pnd_Bsky_Output_Pnd_Tot_Fnd_Amt.setValueEdited(pnd_Fnd_Dtl_Amt,new ReportEditMask("99999999999999.99"));                                                          //Natural: MOVE EDITED #FND-DTL-AMT ( EM = 99999999999999.99 ) TO #TOT-FND-AMT
        pnd_Bsky_Output_Pnd_Tot_Fnd_Shrs.setValue("+0000000000000.000");                                                                                                  //Natural: ASSIGN #TOT-FND-SHRS := '+0000000000000.000'
        pnd_Bsky_Output_Pnd_Opt3.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT3 := '+00000000000.00'
        pnd_Bsky_Output_Pnd_Opt4.setValue("+00000000000.00");                                                                                                             //Natural: ASSIGN #OPT4 := '+00000000000.00'
        //*  FUND TRAILER
        getWorkFiles().write(1, false, pnd_Bsky_Output);                                                                                                                  //Natural: WRITE WORK FILE 1 #BSKY-OUTPUT
        pnd_Bsky_Output_Pnd_Rest_Of_Rcrd.reset();                                                                                                                         //Natural: RESET #REST-OF-RCRD #FND-DTL #FND-DTL-AMT
        pnd_Fnd_Dtl.reset();
        pnd_Fnd_Dtl_Amt.reset();
    }
    private void sub_Get_State_Cde() throws Exception                                                                                                                     //Natural: GET-STATE-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                                 //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-FROM-PPCN-NBR
        pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                               //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cpr.readNextRow("FIND01")))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pdaNeca4000.getNeca4000_Function_Cde().setValue("STT");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'STT'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Stt_Key_State_Cde().setValue(cpr_Prtcpnt_Rsdncy_Cde);                                                                                     //Natural: ASSIGN NECA4000.STT-KEY-STATE-CDE := PRTCPNT-RSDNCY-CDE
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        pnd_St_Abbrv.setValue(pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1));                                                                                  //Natural: ASSIGN #ST-ABBRV := NECA4000.STT-ABBR-STATE-CDE ( 1 )
        //* *WRITE '=' #ST-ABBRV '=' PRTCPNT-RSDNCY-CDE '=' #CPR-KEY
    }
    private void sub_Get_State_Index() throws Exception                                                                                                                   //Natural: GET-STATE-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_St_Abbrv.equals(pnd_Output_Pnd_Iss_St.getValue(pnd_K,"*"))))                                                                                    //Natural: IF #ST-ABBRV = #ISS-ST ( #K,* )
        {
            FOR04:                                                                                                                                                        //Natural: FOR #J 1 TO 50
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(50)); pnd_J.nadd(1))
            {
                if (condition(pnd_Output_Pnd_Iss_St.getValue(pnd_K,pnd_J).equals(pnd_St_Abbrv)))                                                                          //Natural: IF #ISS-ST ( #K,#J ) = #ST-ABBRV
                {
                    pnd_St_Indx.setValue(pnd_J);                                                                                                                          //Natural: ASSIGN #ST-INDX := #J
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR05:                                                                                                                                                        //Natural: FOR #J 1 TO 50
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(50)); pnd_J.nadd(1))
            {
                if (condition(pnd_Output_Pnd_Iss_St.getValue(pnd_K,pnd_J).equals(" ")))                                                                                   //Natural: IF #ISS-ST ( #K,#J ) = ' '
                {
                    pnd_Output_Pnd_Iss_St.getValue(pnd_K,pnd_J).setValue(pnd_St_Abbrv);                                                                                   //Natural: ASSIGN #ISS-ST ( #K,#J ) := #ST-ABBRV
                    pnd_St_Indx.setValue(pnd_J);                                                                                                                          //Natural: ASSIGN #ST-INDX := #J
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, "Program:",Global.getPROGRAM(),new ColumnSpacing(5),"BlueSky Transmission Report for",iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,        //Natural: WRITE ( 1 ) 'Program:' *PROGRAM 5X 'BlueSky Transmission Report for' CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY ) //
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133");

        getReports().setDisplayColumns(1, "/Ticker",
        		pnd_Bsky_Output_Pnd_D_Tckr,"St/Cde",
        		pnd_Bsky_Output_Pnd_St_Cde,"Fund/Amount",
        		pnd_Output_Pnd_To_Asset_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
    }
}
