/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:26:28 PM
**        * FROM NATURAL PROGRAM : Iaap390
************************************************************
**        * FILE NAME            : Iaap390.java
**        * CLASS NAME           : Iaap390
**        * INSTANCE NAME        : Iaap390
************************************************************
**********************************************************************
*
*
*                                                                    *
*   PROGRAM     -  IAAP390    READS IAA TRANS RECORD                 *
*      DATE     -  10/96      PRINTS REPORT OF 730 TRANSACTIONS      *
*                                                                    *
*  04/2017 OS RE-STOWED ONLY FOR PIN EXPANSION.                      *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap390 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Iaa_Parm_Card;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_1;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_2;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_3;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Filler_1;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField pnd_Recs_Ctr;
    private DbsField pnd_Recs_Ctr_Byp;
    private DbsField pnd_Recs_Ctr_730;
    private DbsField pnd_Recs_Ctr_730_Dup;
    private DbsField pnd_Recs_Ctr_Prt1;
    private DbsField pnd_Recs_Ctr_Prt2;
    private DbsField pnd_Recs_Ctr_Prt3;
    private DbsField pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Payee_Cde;
    private DbsField pnd_Cpr_Aftr_Key;

    private DbsGroup pnd_Cpr_Aftr_Key__R_Field_4;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Iaa_Parm_Card = localVariables.newGroupInRecord("pnd_Iaa_Parm_Card", "#IAA-PARM-CARD");
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte = pnd_Iaa_Parm_Card.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.STRING, 
            8);

        pnd_Iaa_Parm_Card__R_Field_1 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_1", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N = pnd_Iaa_Parm_Card__R_Field_1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N", "#PARM-CHECK-DTE-N", 
            FieldType.NUMERIC, 8);

        pnd_Iaa_Parm_Card__R_Field_2 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_2", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc", "#PARM-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy", "#PARM-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm", "#PARM-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd", "#PARM-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Iaa_Parm_Card__R_Field_3 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_3", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm = pnd_Iaa_Parm_Card__R_Field_3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm", "#PARM-CHECK-DTE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Iaa_Parm_Card_Pnd_Parm_Filler_1 = pnd_Iaa_Parm_Card.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Filler_1", "#PARM-FILLER-1", FieldType.STRING, 
            1);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        registerRecord(vw_iaa_Cpr_Trans);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Recs_Ctr = localVariables.newFieldInRecord("pnd_Recs_Ctr", "#RECS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_Byp = localVariables.newFieldInRecord("pnd_Recs_Ctr_Byp", "#RECS-CTR-BYP", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_730 = localVariables.newFieldInRecord("pnd_Recs_Ctr_730", "#RECS-CTR-730", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_730_Dup = localVariables.newFieldInRecord("pnd_Recs_Ctr_730_Dup", "#RECS-CTR-730-DUP", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_Prt1 = localVariables.newFieldInRecord("pnd_Recs_Ctr_Prt1", "#RECS-CTR-PRT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_Prt2 = localVariables.newFieldInRecord("pnd_Recs_Ctr_Prt2", "#RECS-CTR-PRT2", FieldType.PACKED_DECIMAL, 7);
        pnd_Recs_Ctr_Prt3 = localVariables.newFieldInRecord("pnd_Recs_Ctr_Prt3", "#RECS-CTR-PRT3", FieldType.PACKED_DECIMAL, 7);
        pnd_Save_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key__R_Field_4", "REDEFINE", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cpr_Aftr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap390() throws Exception
    {
        super("Iaap390");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 2 ) LS = 133 PS = 56 ZP = OFF SG = OFF
        //*  INPUT #IAA-PARM-CARD
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 #IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, pnd_Iaa_Parm_Card)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte,pnd_Iaa_Parm_Card_Pnd_Parm_Filler_1);                                                              //Natural: WRITE '=' #IAA-PARM-CARD
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte.equals(" ")))                                                                                                  //Natural: IF #PARM-CHECK-DTE = ' '
        {
            getReports().write(0, " NO PARAMETER CARD ");                                                                                                                 //Natural: WRITE ' NO PARAMETER CARD '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #PARM-CHECK-DTE
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ01")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.greater(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N)))                                                                //Natural: IF IAA-TRANS-RCRD.TRANS-CHECK-DTE GT #PARM-CHECK-DTE-N
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recs_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RECS-CTR
            if (condition(iaa_Trans_Rcrd_Trans_Cde.notEquals(730)))                                                                                                       //Natural: IF IAA-TRANS-RCRD.TRANS-CDE NE 730
            {
                pnd_Recs_Ctr_Byp.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RECS-CTR-BYP
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recs_Ctr_730.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECS-CTR-730
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.equals(pnd_Save_Ppcn_Nbr) && iaa_Trans_Rcrd_Trans_Payee_Cde.equals(pnd_Save_Payee_Cde)))                          //Natural: IF IAA-TRANS-RCRD.TRANS-PPCN-NBR = #SAVE-PPCN-NBR AND IAA-TRANS-RCRD.TRANS-PAYEE-CDE = #SAVE-PAYEE-CDE
            {
                pnd_Recs_Ctr_730_Dup.nadd(1);                                                                                                                             //Natural: ADD 1 TO #RECS-CTR-730-DUP
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Save_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                                    //Natural: ASSIGN #SAVE-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            pnd_Save_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                                  //Natural: ASSIGN #SAVE-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                              //Natural: ASSIGN #AFTR-IMGE-ID := '2'
            pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                            //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                          //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                              //Natural: ASSIGN #INVRSE-TRANS-DTE := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
            vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
            (
            "READ02",
            new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Cpr_Aftr_Key, WcType.BY) },
            new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_iaa_Cpr_Trans.readNextRow("READ02")))
            {
                pnd_Recs_Ctr_Prt1.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECS-CTR-PRT1
                if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr) && iaa_Trans_Rcrd_Trans_Payee_Cde.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde)  //Natural: IF IAA-TRANS-RCRD.TRANS-PPCN-NBR = IAA-CPR-TRANS.CNTRCT-PART-PPCN-NBR AND IAA-TRANS-RCRD.TRANS-PAYEE-CDE = IAA-CPR-TRANS.CNTRCT-PART-PAYEE-CDE AND ( IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( 1 ) GT 0 OR IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( 2 ) GT 0 )
                    && (iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue(1).greater(getZero()) || iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue(2).greater(getZero()))))
                {
                    pnd_Recs_Ctr_Prt3.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RECS-CTR-PRT3
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),iaa_Trans_Rcrd_Trans_Ppcn_Nbr,new TabSetting(14),iaa_Trans_Rcrd_Trans_Payee_Cde,new      //Natural: WRITE ( 2 ) 2T TRANS-PPCN-NBR 14T TRANS-PAYEE-CDE 18T TRANS-USER-ID 28T TRANS-DTE ( EM = MM/DD/YY ) 38T IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( 1 ) ( EM = ZZZZZ.99 ) 48T IAA-CPR-TRANS.CNTRCT-IVC-AMT ( 1 ) ( EM = ZZZZZ.99 ) 58T IAA-CPR-TRANS.CNTRCT-IVC-USED-AMT ( 1 ) ( EM = ZZZZZ.99 ) 68T IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( 2 ) ( EM = ZZZZZ.99 ) 78T IAA-CPR-TRANS.CNTRCT-IVC-AMT ( 2 ) ( EM = ZZZZZ.99 ) 88T IAA-CPR-TRANS.CNTRCT-IVC-USED-AMT ( 2 ) ( EM = ZZZZZ.99 )
                        TabSetting(18),iaa_Trans_Rcrd_Trans_User_Id,new TabSetting(28),iaa_Cpr_Trans_Trans_Dte, new ReportEditMask ("MM/DD/YY"),new TabSetting(38),iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue(1), 
                        new ReportEditMask ("ZZZZZ.99"),new TabSetting(48),iaa_Cpr_Trans_Cntrct_Ivc_Amt.getValue(1), new ReportEditMask ("ZZZZZ.99"),new 
                        TabSetting(58),iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt.getValue(1), new ReportEditMask ("ZZZZZ.99"),new TabSetting(68),iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue(2), 
                        new ReportEditMask ("ZZZZZ.99"),new TabSetting(78),iaa_Cpr_Trans_Cntrct_Ivc_Amt.getValue(2), new ReportEditMask ("ZZZZZ.99"),new 
                        TabSetting(88),iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt.getValue(2), new ReportEditMask ("ZZZZZ.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Recs_Ctr_Prt2.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RECS-CTR-PRT2
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),iaa_Trans_Rcrd_Trans_Ppcn_Nbr,new TabSetting(14),iaa_Trans_Rcrd_Trans_Payee_Cde,new      //Natural: WRITE ( 1 ) 2T TRANS-PPCN-NBR 14T TRANS-PAYEE-CDE 18T TRANS-USER-ID 28T TRANS-DTE ( EM = MM/DD/YY )
                        TabSetting(18),iaa_Trans_Rcrd_Trans_User_Id,new TabSetting(28),iaa_Cpr_Trans_Trans_Dte, new ReportEditMask ("MM/DD/YY"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF TRANSACTION RECORDS READ    : ",pnd_Recs_Ctr);                                                              //Natural: WRITE ( 1 ) 'NUMBER OF TRANSACTION RECORDS READ    : ' #RECS-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF TRANSACTION RECS BYPASSED   : ",pnd_Recs_Ctr_Byp);                                                          //Natural: WRITE ( 1 ) 'NUMBER OF TRANSACTION RECS BYPASSED   : ' #RECS-CTR-BYP
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF 730 TRANSACTIONS            : ",pnd_Recs_Ctr_730);                                                          //Natural: WRITE ( 1 ) 'NUMBER OF 730 TRANSACTIONS            : ' #RECS-CTR-730
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF 730 TRANSACTIONS (DUPS)     : ",pnd_Recs_Ctr_730_Dup);                                                      //Natural: WRITE ( 1 ) 'NUMBER OF 730 TRANSACTIONS (DUPS)     : ' #RECS-CTR-730-DUP
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF 730 TRANSACTIONS PRINTED    : ",pnd_Recs_Ctr_Prt1);                                                         //Natural: WRITE ( 1 ) 'NUMBER OF 730 TRANSACTIONS PRINTED    : ' #RECS-CTR-PRT1
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF 730 TRANS WITH IVC = 0      : ",pnd_Recs_Ctr_Prt2);                                                         //Natural: WRITE ( 1 ) 'NUMBER OF 730 TRANS WITH IVC = 0      : ' #RECS-CTR-PRT2
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NUMBER OF 730 TRANS WITH IVC GT 0     : ",pnd_Recs_Ctr_Prt3);                                                         //Natural: WRITE ( 1 ) 'NUMBER OF 730 TRANS WITH IVC GT 0     : ' #RECS-CTR-PRT3
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"NUMBER OF 730 TRANS WITH IVC GT 0     : ",pnd_Recs_Ctr_Prt3);                                                         //Natural: WRITE ( 2 ) 'NUMBER OF 730 TRANS WITH IVC GT 0     : ' #RECS-CTR-PRT3
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(5),"PROGRAM: ",Global.getPROGRAM(),new TabSetting(31),"IA ADMINISTRATION IAR IVC MAINTENANCE (730) REPORT",new  //Natural: WRITE ( 1 ) NOTITLE 5T 'PROGRAM: ' *PROGRAM 31T 'IA ADMINISTRATION IAR IVC MAINTENANCE (730) REPORT' 111T 'PAGE' *PAGE-NUMBER ( 1 ) / 7T 'DATE: ' *DATU / 2T 'CHECK DATE: ' #PARM-CHECK-DTE //
                        TabSetting(111),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(7),"DATE: ",Global.getDATU(),NEWLINE,new TabSetting(2),
                        "CHECK DATE: ",pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte,NEWLINE,NEWLINE);
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"CONTRACT",new TabSetting(14),"PY",new TabSetting(18),"  USER",new TabSetting(28),       //Natural: WRITE ( 1 ) 2T 'CONTRACT' 14T 'PY' 18T '  USER' 28T '  DATE' /
                        "  DATE",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(5),"PROGRAM: ",Global.getPROGRAM(),new TabSetting(31),"IA ADMINISTRATION IAR IVC MAINTENANCE (730) REPORT",new  //Natural: WRITE ( 2 ) NOTITLE 5T 'PROGRAM: ' *PROGRAM 31T 'IA ADMINISTRATION IAR IVC MAINTENANCE (730) REPORT' 111T 'PAGE' *PAGE-NUMBER ( 2 ) / 7T 'DATE: ' *DATU / 2T 'CHECK DATE: ' #PARM-CHECK-DTE 38T '---------- T I A A ----------' 68T '---------- C R E F ----------' //
                        TabSetting(111),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,new TabSetting(7),"DATE: ",Global.getDATU(),NEWLINE,new TabSetting(2),"CHECK DATE: ",pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte,new 
                        TabSetting(38),"---------- T I A A ----------",new TabSetting(68),"---------- C R E F ----------",NEWLINE,NEWLINE);
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"CONTRACT",new TabSetting(14),"PY",new TabSetting(18),"  USER",new TabSetting(28),"  DATE",new  //Natural: WRITE ( 2 ) 2T 'CONTRACT' 14T 'PY' 18T '  USER' 28T '  DATE' 38T ' PER AMT' 48T 'TOTAL AMT' 58T ' USED AMT' 68T ' PER AMT' 78T 'TOTAL AMT' 88T ' USED AMT' /
                        TabSetting(38)," PER AMT",new TabSetting(48),"TOTAL AMT",new TabSetting(58)," USED AMT",new TabSetting(68)," PER AMT",new TabSetting(78),"TOTAL AMT",new 
                        TabSetting(88)," USED AMT",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(1, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(2, "LS=133 PS=56 ZP=OFF SG=OFF");
    }
}
