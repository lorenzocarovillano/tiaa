/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:19 PM
**        * FROM NATURAL PROGRAM : Iaap920
************************************************************
**        * FILE NAME            : Iaap920.java
**        * CLASS NAME           : Iaap920
**        * INSTANCE NAME        : Iaap920
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP920    READS SELECTION RECORD LAYOUT          *
*      DATE     -  10/94      CREATES BACKWARD CONVERSION REPORTS    *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap920 extends BLNatBase
{
    // Data Areas
    private LdaIaal920 ldaIaal920;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal920 = new LdaIaal920();
        registerRecord(ldaIaal920);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal920.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap920() throws Exception
    {
        super("Iaap920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 3 ) LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 4 ) LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 5 ) LS = 133 PS = 56 ZP = OFF SG = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        ldaIaal920.getPnd_First().setValue(true);                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        ldaIaal920.getPnd_New_Trans_L().setValue(true);                                                                                                                   //Natural: ASSIGN #NEW-TRANS-L := TRUE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 4 IAA-PARM-CARD
        while (condition(getWorkFiles().read(4, ldaIaal920.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte(),"=",ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte(),"=",ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte()); //Natural: WRITE '=' #PARM-CHECK-DTE '=' #PARM-FROM-DTE '=' #PARM-TO-DTE
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-PARM-CARD
        sub_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-REC
        while (condition(getWorkFiles().read(1, ldaIaal920.getPnd_Input_Rec())))
        {
            if (condition(ldaIaal920.getPnd_First().getBoolean()))                                                                                                        //Natural: IF #FIRST
            {
                ldaIaal920.getPnd_Output_Rec().setValue(ldaIaal920.getPnd_Input_Rec());                                                                                   //Natural: ASSIGN #OUTPUT-REC := #INPUT-REC
                ldaIaal920.getPnd_First().setValue(false);                                                                                                                //Natural: ASSIGN #FIRST := FALSE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-BREAK
                sub_Check_For_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal920.getPnd_Output_Rec().setValue(ldaIaal920.getPnd_Input_Rec());                                                                                   //Natural: ASSIGN #OUTPUT-REC := #INPUT-REC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        ldaIaal920.getPnd_Output_Rec().setValue(ldaIaal920.getPnd_Input_Rec());                                                                                           //Natural: ASSIGN #OUTPUT-REC := #INPUT-REC
        ldaIaal920.getPnd_New_Trans_L().setValue(true);                                                                                                                   //Natural: ASSIGN #NEW-TRANS-L := TRUE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TRANS
        sub_Calculate_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-USERID-TOTALS
        sub_Print_Userid_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-USER-AREA-TOTALS
        sub_Print_User_Area_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
        sub_Print_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PARM-CARD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-BREAK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MISC-NON-TAX-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMB-CHK-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TAX
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DDCTN-FROM-NET
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-HIS-YTD-DDCTN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-SELECTION-RCRD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-USERID-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-USER-AREA-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
    }
    private void sub_Check_Parm_Card() throws Exception                                                                                                                   //Natural: CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet847 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PARM-CHECK-DTE NE ' '
        if (condition(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte().notEquals(" ")))
        {
            decideConditionsMet847++;
            if (condition(DbsUtil.maskMatches(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte(),"YYYYMMDD")))                                                              //Natural: IF #PARM-CHECK-DTE = MASK ( YYYYMMDD )
            {
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Mm().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte_Mm());                                               //Natural: ASSIGN #DISP-DTE-MM := #PARM-CHECK-DTE-MM
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S1().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S1 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Dd().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte_Dd());                                               //Natural: ASSIGN #DISP-DTE-DD := #PARM-CHECK-DTE-DD
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S2().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S2 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Yy().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Check_Dte_Yy());                                               //Natural: ASSIGN #DISP-DTE-YY := #PARM-CHECK-DTE-YY
                ldaIaal920.getPnd_Parm_Check_Dte_Disp().setValue(ldaIaal920.getPnd_Disp_Dte());                                                                           //Natural: ASSIGN #PARM-CHECK-DTE-DISP := #DISP-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-FROM-DTE NE ' '
        if (condition(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte().notEquals(" ")))
        {
            decideConditionsMet847++;
            if (condition(DbsUtil.maskMatches(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte(),"YYYYMMDD")))                                                               //Natural: IF #PARM-FROM-DTE = MASK ( YYYYMMDD )
            {
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Mm().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte_Mm());                                                //Natural: ASSIGN #DISP-DTE-MM := #PARM-FROM-DTE-MM
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S1().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S1 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Dd().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte_Dd());                                                //Natural: ASSIGN #DISP-DTE-DD := #PARM-FROM-DTE-DD
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S2().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S2 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Yy().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_From_Dte_Yy());                                                //Natural: ASSIGN #DISP-DTE-YY := #PARM-FROM-DTE-YY
                ldaIaal920.getPnd_Parm_From_Dte_Disp().setValue(ldaIaal920.getPnd_Disp_Dte());                                                                            //Natural: ASSIGN #PARM-FROM-DTE-DISP := #DISP-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PARM-TO-DTE NE ' '
        if (condition(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte().notEquals(" ")))
        {
            decideConditionsMet847++;
            if (condition(DbsUtil.maskMatches(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte(),"YYYYMMDD")))                                                                 //Natural: IF #PARM-TO-DTE = MASK ( YYYYMMDD )
            {
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Mm().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte_Mm());                                                  //Natural: ASSIGN #DISP-DTE-MM := #PARM-TO-DTE-MM
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S1().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S1 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Dd().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte_Dd());                                                  //Natural: ASSIGN #DISP-DTE-DD := #PARM-TO-DTE-DD
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_S2().setValue("/");                                                                                               //Natural: ASSIGN #DISP-DTE-S2 := '/'
                ldaIaal920.getPnd_Disp_Dte_Pnd_Disp_Dte_Yy().setValue(ldaIaal920.getIaa_Parm_Card_Pnd_Parm_To_Dte_Yy());                                                  //Natural: ASSIGN #DISP-DTE-YY := #PARM-TO-DTE-YY
                ldaIaal920.getPnd_Parm_To_Dte_Disp().setValue(ldaIaal920.getPnd_Disp_Dte());                                                                              //Natural: ASSIGN #PARM-TO-DTE-DISP := #DISP-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet847 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Check_For_Break() throws Exception                                                                                                                   //Natural: CHECK-FOR-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal920.getPnd_Output_Rec_Pnd_Contract_Nbr().equals(ldaIaal920.getPnd_Input_Rec_Pnd_Contract_Nbr()) && ldaIaal920.getPnd_Output_Rec_Pnd_Payee().equals(ldaIaal920.getPnd_Input_Rec_Pnd_Payee())  //Natural: IF #OUTPUT-REC.#CONTRACT-NBR = #INPUT-REC.#CONTRACT-NBR AND #OUTPUT-REC.#PAYEE = #INPUT-REC.#PAYEE AND #OUTPUT-REC.#TRANS-CDE = #INPUT-REC.#TRANS-CDE
            && ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde().equals(ldaIaal920.getPnd_Input_Rec_Pnd_Trans_Cde())))
        {
            if (condition(ldaIaal920.getPnd_Output_Rec_Pnd_Seq_Nbr().notEquals(ldaIaal920.getPnd_Input_Rec_Pnd_Seq_Nbr())))                                               //Natural: IF #OUTPUT-REC.#SEQ-NBR NE #INPUT-REC.#SEQ-NBR
            {
                ldaIaal920.getPnd_New_Trans_L().setValue(true);                                                                                                           //Natural: ASSIGN #NEW-TRANS-L := TRUE
                ldaIaal920.getPnd_Output_Rec_Pnd_New_Trans().setValue("N");                                                                                               //Natural: ASSIGN #OUTPUT-REC.#NEW-TRANS := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal920.getPnd_New_Trans_L().setValue(false);                                                                                                          //Natural: ASSIGN #NEW-TRANS-L := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal920.getPnd_New_Trans_L().setValue(true);                                                                                                               //Natural: ASSIGN #NEW-TRANS-L := TRUE
            ldaIaal920.getPnd_Output_Rec_Pnd_New_Trans().setValue("N");                                                                                                   //Natural: ASSIGN #OUTPUT-REC.#NEW-TRANS := 'N'
            ldaIaal920.getPnd_Output_Rec_Pnd_Multiple().setValue("L");                                                                                                    //Natural: ASSIGN #OUTPUT-REC.#MULTIPLE := 'L'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TRANS
        sub_Calculate_Trans();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaIaal920.getPnd_Output_Rec_Pnd_User_Id().notEquals(ldaIaal920.getPnd_Input_Rec_Pnd_User_Id())))                                                   //Natural: IF #OUTPUT-REC.#USER-ID NE #INPUT-REC.#USER-ID
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-USERID-TOTALS
            sub_Print_Userid_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Output_Rec_Pnd_User_Area().notEquals(ldaIaal920.getPnd_Input_Rec_Pnd_User_Area())))                                               //Natural: IF #OUTPUT-REC.#USER-AREA NE #INPUT-REC.#USER-AREA
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-USER-AREA-TOTALS
            sub_Print_User_Area_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Trans() throws Exception                                                                                                                     //Natural: PROCESS-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Report().setValue(" ");                                                                                                                         //Natural: ASSIGN #REPORT := ' '
        ldaIaal920.getPnd_Display_Heading().setValue("   DETAIL TRANSACTIONS  ");                                                                                         //Natural: ASSIGN #DISPLAY-HEADING := '   DETAIL TRANSACTIONS  '
        short decideConditionsMet904 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #OUTPUT-REC.#TRANS-CDE-N;//Natural: VALUE 020
        if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(20))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 037
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(37))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 040
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(40))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 064
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(64))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(66))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 070
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(70))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(102))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(104))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(106))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MISC-NON-TAX-TRANS
            sub_Process_Misc_Non_Tax_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 304
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(304))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMB-CHK-TRANS
            sub_Process_Comb_Chk_Trans();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(724))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-TAX
            sub_Process_Tax();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 902
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(902))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-DDCTN-FROM-NET
            sub_Process_Ddctn_From_Net();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(906))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-HIS-YTD-DDCTN
            sub_Process_His_Ytd_Ddctn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 033
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(33))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-SELECTION-RCRD
            sub_Process_Selection_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(35))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-SELECTION-RCRD
            sub_Process_Selection_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 050
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(50))))
        {
            decideConditionsMet904++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-SELECTION-RCRD
            sub_Process_Selection_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Misc_Non_Tax_Trans() throws Exception                                                                                                        //Natural: PROCESS-MISC-NON-TAX-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Misc_Non_Tax_Trans().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                  //Natural: ASSIGN #MISC-NON-TAX-TRANS := #OUTPUT-REC
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Mm(),           //Natural: COMPRESS #MISC-NON-TAX-TRANS.#TRANS-DTE-MM '/' #MISC-NON-TAX-TRANS.#TRANS-DTE-DD '/' #MISC-NON-TAX-TRANS.#TRANS-DTE-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Dd(), "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Dte_Yy()));
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex().equals(getZero())))                                                                         //Natural: IF #1ST-ANNT-SEX = 0
        {
            ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A().setValue(" ");                                                                                      //Natural: ASSIGN #1ST-ANNT-SEX-A := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm().notEquals(" ")))                                                                         //Natural: IF #1ST-ANNT-DOB-MM NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Dob1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Mm(),     //Natural: COMPRESS #1ST-ANNT-DOB-MM '/' #1ST-ANNT-DOB-DD '/' #1ST-ANNT-DOB-YY INTO #DISPLAY-DTE-DOB1 LEAVING NO
                "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Dd(), "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dob_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm().notEquals(" ")))                                                                         //Natural: IF #1ST-ANNT-DOD-MM NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Dod1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Mm(),     //Natural: COMPRESS #1ST-ANNT-DOD-MM '/' #1ST-ANNT-DOD-YY INTO #DISPLAY-DTE-DOD1 LEAVING NO
                "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Dod_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex().equals(getZero())))                                                                         //Natural: IF #2ND-ANNT-SEX = 0
        {
            ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A().setValue(" ");                                                                                      //Natural: ASSIGN #2ND-ANNT-SEX-A := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm().notEquals(" ")))                                                                         //Natural: IF #2ND-ANNT-DOB-MM NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Dob2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Mm(),     //Natural: COMPRESS #2ND-ANNT-DOB-MM '/' #2ND-ANNT-DOB-DD '/' #2ND-ANNT-DOB-YY INTO #DISPLAY-DTE-DOB2 LEAVING NO
                "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Dd(), "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dob_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm().notEquals(" ")))                                                                         //Natural: IF #2ND-ANNT-DOD-MM NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Dod2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Mm(),     //Natural: COMPRESS #2ND-ANNT-DOD-MM '/' #2ND-ANNT-DOD-YY INTO #DISPLAY-DTE-DOD2 LEAVING NO
                "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Dod_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm().notEquals(" ")))                                                                     //Natural: IF #EFF-DTE-CHNG-RES-MM NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Res().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Mm(),  //Natural: COMPRESS #EFF-DTE-CHNG-RES-MM '/' #EFF-DTE-CHNG-RES-YY INTO #DISPLAY-DTE-RES LEAVING NO
                "/", ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Eff_Dte_Chng_Res_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new  //Natural: WRITE ( 2 ) /// 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 45T 'INTENT' 55T 'HOLD' 60T 'SUSPEND' 80T '----1ST ANNUITANT--  ----2ND-ANNUITANT-- EFF CHANGE'
            TabSetting(36),"CROSS",new TabSetting(45),"INTENT",new TabSetting(55),"HOLD",new TabSetting(60),"SUSPEND",new TabSetting(80),"----1ST ANNUITANT--  ----2ND-ANNUITANT-- EFF CHANGE");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 46T 'CODE' 54T 'CHECK' 60T 'PAYMENT' 69T 'RESIDENCE' 80T 'SEX' 88T 'DOB' 95T 'DOD' 101T 'SEX' 109T 'DOB' 116T 'DOD' 121T 'RESIDENCE'
            TabSetting(46),"CODE",new TabSetting(54),"CHECK",new TabSetting(60),"PAYMENT",new TabSetting(69),"RESIDENCE",new TabSetting(80),"SEX",new TabSetting(88),"DOB",new 
            TabSetting(95),"DOD",new TabSetting(101),"SEX",new TabSetting(109),"DOB",new TabSetting(116),"DOD",new TabSetting(121),"RESIDENCE");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 45T '------' 53T '-----' 60T '-------' 69T '---------' 80T '---' 85T '-------- -----  ---' 106T '-------- ----- ----------'
            TabSetting(34),"---------",new TabSetting(45),"------",new TabSetting(53),"-----",new TabSetting(60),"-------",new TabSetting(69),"---------",new 
            TabSetting(80),"---",new TabSetting(85),"-------- -----  ---",new TabSetting(106),"-------- ----- ----------");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Trans_Nbr(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #MISC-NON-TAX-TRANS.#TRANS-NBR 16T #MISC-NON-TAX-TRANS.#CNTRCT-NBR 27T #MISC-NON-TAX-TRANS.#RECORD-STATUS ( EM = 99 ) 34T #MISC-NON-TAX-TRANS.#CROSS-REF-NBR 48T #MISC-NON-TAX-TRANS.#INTENT-CODE 55T #MISC-NON-TAX-TRANS.#HOLD-CHECK-RSN-CDE 63T #MISC-NON-TAX-TRANS.#SUS-PYMNT-RSN-CDE 72T #MISC-NON-TAX-TRANS.#STATE-CNTRY-RES 81T #MISC-NON-TAX-TRANS.#1ST-ANNT-SEX-A 85T #DISPLAY-DTE-DOB1 94T #DISPLAY-DTE-DOD1 102T #MISC-NON-TAX-TRANS.#2ND-ANNT-SEX-A 106T #DISPLAY-DTE-DOB2 115T #DISPLAY-DTE-DOD2 124T #DISPLAY-DTE-RES
            TabSetting(16),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Cntrct_Nbr(),new TabSetting(27),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Record_Status(), 
            new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Cross_Ref_Nbr(),new TabSetting(48),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Intent_Code(),new 
            TabSetting(55),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Hold_Check_Rsn_Cde(),new TabSetting(63),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_Sus_Pymnt_Rsn_Cde(),new 
            TabSetting(72),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_State_Cntry_Res(),new TabSetting(81),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_1st_Annt_Sex_A(),new 
            TabSetting(85),ldaIaal920.getPnd_Display_Dte_Dob1(),new TabSetting(94),ldaIaal920.getPnd_Display_Dte_Dod1(),new TabSetting(102),ldaIaal920.getPnd_Misc_Non_Tax_Trans_Pnd_2nd_Annt_Sex_A(),new 
            TabSetting(106),ldaIaal920.getPnd_Display_Dte_Dob2(),new TabSetting(115),ldaIaal920.getPnd_Display_Dte_Dod2(),new TabSetting(124),ldaIaal920.getPnd_Display_Dte_Res());
        if (Global.isEscape()) return;
    }
    private void sub_Process_Comb_Chk_Trans() throws Exception                                                                                                            //Natural: PROCESS-COMB-CHK-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Comb_Chk_Trans().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                      //Natural: ASSIGN #COMB-CHK-TRANS := #OUTPUT-REC
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Mm(),               //Natural: COMPRESS #COMB-CHK-TRANS.#TRANS-DTE-MM '/' #COMB-CHK-TRANS.#TRANS-DTE-DD '/' #COMB-CHK-TRANS.#TRANS-DTE-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            "/", ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Dd(), "/", ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Trans_Dte_Yy()));
        if (condition(ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_A().notEquals(" ")))                                                                                  //Natural: IF #ANNT-DOB-A NE ' '
        {
            ldaIaal920.getPnd_Display_Dte_Dob1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Mm(),             //Natural: COMPRESS #ANNT-DOB-MM '/' #ANNT-DOB-DD '/' #ANNT-DOB-YY INTO #DISPLAY-DTE-DOB1 LEAVING NO
                "/", ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Dd(), "/", ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Dob_Yy()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex().equals(getZero())))                                                                                 //Natural: IF #ANNT-SEX = 0
        {
            ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A().setValue(" ");                                                                                              //Natural: ASSIGN #ANNT-SEX-A := ' '
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"REC",new     //Natural: WRITE ( 2 ) /// 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'REC' 33T 'CROSS' 41T 'INT' 76T '2ND' 88T '3RD' 100T '4TH' 112T '5TH' 124T '6TH'
            TabSetting(33),"CROSS",new TabSetting(41),"INT",new TabSetting(76),"2ND",new TabSetting(88),"3RD",new TabSetting(100),"4TH",new TabSetting(112),"5TH",new 
            TabSetting(124),"6TH");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STA",new TabSetting(31),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STA' 31T 'REFERENCE' 41T 'CDE' 45T 'CURR' 50T 'SEX' 57T 'DOB' 63T 'SS NUMBER' 73T 'CONTRACT/ST' 85T 'CONTRACT/ST' 97T 'CONTRACT/ST' 109T 'CONTRACT/ST' 121T 'CONTRACT/ST'
            TabSetting(41),"CDE",new TabSetting(45),"CURR",new TabSetting(50),"SEX",new TabSetting(57),"DOB",new TabSetting(63),"SS NUMBER",new TabSetting(73),"CONTRACT/ST",new 
            TabSetting(85),"CONTRACT/ST",new TabSetting(97),"CONTRACT/ST",new TabSetting(109),"CONTRACT/ST",new TabSetting(121),"CONTRACT/ST");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"---",new   //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '---' 31T '---------' 41T '---' 45T '----' 50T '---' 54T '--------' 63T '---------' 73T '-------- --' 85T '-------- --' 97T '-------- --' 109T '-------- --' 121T '-------- --'
            TabSetting(31),"---------",new TabSetting(41),"---",new TabSetting(45),"----",new TabSetting(50),"---",new TabSetting(54),"--------",new TabSetting(63),"---------",new 
            TabSetting(73),"-------- --",new TabSetting(85),"-------- --",new TabSetting(97),"-------- --",new TabSetting(109),"-------- --",new TabSetting(121),
            "-------- --");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Trans_Nbr(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #COMB-CHK-TRANS.#TRANS-NBR 16T #COMB-CHK-TRANS.#CNTRCT-NBR 27T #COMB-CHK-TRANS.#RCRD-STATUS ( EM = 99 ) 31T #COMB-CHK-TRANS.#CROSS-REF-NBR 42T #COMB-CHK-TRANS.#INTENT-CODE 47T #COMB-CHK-TRANS.#CURRENCY 51T #COMB-CHK-TRANS.#ANNT-SEX-A 54T #DISPLAY-DTE-DOB1 63T #COMB-CHK-TRANS.#SSN 73T #COMB-CHK-TRANS.#2ND-COMB-CNTRCT 82T #COMB-CHK-TRANS.#2ND-STA-NBR 85T #COMB-CHK-TRANS.#3RD-COMB-CNTRCT 94T #COMB-CHK-TRANS.#3RD-STA-NBR 97T #COMB-CHK-TRANS.#4TH-COMB-CNTRCT 106T #COMB-CHK-TRANS.#4TH-STA-NBR 109T #COMB-CHK-TRANS.#5TH-COMB-CNTRCT 118T #COMB-CHK-TRANS.#5TH-STA-NBR 121T #COMB-CHK-TRANS.#6TH-COMB-CNTRCT 130T #COMB-CHK-TRANS.#6TH-STA-NBR
            TabSetting(16),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Cntrct_Nbr(),new TabSetting(27),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Rcrd_Status(), new 
            ReportEditMask ("99"),new TabSetting(31),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Cross_Ref_Nbr(),new TabSetting(42),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Intent_Code(),new 
            TabSetting(47),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Currency(),new TabSetting(51),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Annt_Sex_A(),new TabSetting(54),ldaIaal920.getPnd_Display_Dte_Dob1(),new 
            TabSetting(63),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_Ssn(),new TabSetting(73),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_2nd_Comb_Cntrct(),new TabSetting(82),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_2nd_Sta_Nbr(),new 
            TabSetting(85),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_3rd_Comb_Cntrct(),new TabSetting(94),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_3rd_Sta_Nbr(),new 
            TabSetting(97),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_4th_Comb_Cntrct(),new TabSetting(106),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_4th_Sta_Nbr(),new 
            TabSetting(109),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_5th_Comb_Cntrct(),new TabSetting(118),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_5th_Sta_Nbr(),new 
            TabSetting(121),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_6th_Comb_Cntrct(),new TabSetting(130),ldaIaal920.getPnd_Comb_Chk_Trans_Pnd_6th_Sta_Nbr());
        if (Global.isEscape()) return;
    }
    private void sub_Process_Tax() throws Exception                                                                                                                       //Natural: PROCESS-TAX
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Tax().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                                 //Natural: ASSIGN #TAX := #OUTPUT-REC
        //*  ADD #TAX.#INVEST-IN-CNTRCT-N TO #TOTAL-IVC-AMT(1)
        //*  ADD #TAX.#INVEST-IN-CNTRCT-N TO #TOTAL-IVC-AMT(2)
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Tax_Pnd_Trans_Dte_Mm(), "/",                     //Natural: COMPRESS #TAX.#TRANS-DTE-MM '/' #TAX.#TRANS-DTE-DD '/' #TAX.#TRANS-DTE-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            ldaIaal920.getPnd_Tax_Pnd_Trans_Dte_Dd(), "/", ldaIaal920.getPnd_Tax_Pnd_Trans_Dte_Yy()));
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new  //Natural: WRITE ( 2 ) /// 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS'
            TabSetting(36),"CROSS");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 45T 'IVC AMOUNT'
            TabSetting(45),"IVC AMOUNT");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 45T '------------'
            TabSetting(34),"---------",new TabSetting(45),"------------");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Tax_Pnd_Trans_Nbr(),new   //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #TAX.#TRANS-NBR 16T #TAX.#CNTRCT-NBR 29T #TAX.#RECORD-STATUS ( EM = 99 ) 34T #TAX.#CROSS-REF-NBR 45T #TAX.#INVEST-IN-CNTRCT-N ( EM = Z,ZZZ,ZZZ.99 )
            TabSetting(16),ldaIaal920.getPnd_Tax_Pnd_Cntrct_Nbr(),new TabSetting(29),ldaIaal920.getPnd_Tax_Pnd_Record_Status(), new ReportEditMask ("99"),new 
            TabSetting(34),ldaIaal920.getPnd_Tax_Pnd_Cross_Ref_Nbr(),new TabSetting(45),ldaIaal920.getPnd_Tax_Pnd_Invest_In_Cntrct_N(), new ReportEditMask 
            ("Z,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Process_Ddctn_From_Net() throws Exception                                                                                                            //Natural: PROCESS-DDCTN-FROM-NET
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Ddctn_From_Net().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                      //Natural: ASSIGN #DDCTN-FROM-NET := #OUTPUT-REC
        ldaIaal920.getPnd_Total_Per_Ded().getValue(1).nadd(ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N());                                                       //Natural: ADD #DDCTN-FROM-NET.#PER-DDCTN-AMT-N TO #TOTAL-PER-DED ( 1 )
        ldaIaal920.getPnd_Total_Total_Ded().getValue(1).nadd(ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N());                                                         //Natural: ADD #DDCTN-FROM-NET.#TOT-DDCTN-N TO #TOTAL-TOTAL-DED ( 1 )
        ldaIaal920.getPnd_Total_Per_Ded().getValue(2).nadd(ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N());                                                       //Natural: ADD #DDCTN-FROM-NET.#PER-DDCTN-AMT-N TO #TOTAL-PER-DED ( 2 )
        ldaIaal920.getPnd_Total_Total_Ded().getValue(2).nadd(ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N());                                                         //Natural: ADD #DDCTN-FROM-NET.#TOT-DDCTN-N TO #TOTAL-TOTAL-DED ( 2 )
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Mm(),               //Natural: COMPRESS #DDCTN-FROM-NET.#TRANS-DTE-MM '/' #DDCTN-FROM-NET.#TRANS-DTE-DD '/' #DDCTN-FROM-NET.#TRANS-DTE-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            "/", ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Dd(), "/", ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Trans_Dte_Yy()));
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new  //Natural: WRITE ( 2 ) /// 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 45T 'INTENT' 53T 'SEQUENCE' 63T 'DEDUCTION' 73T 'DEDUCTION' 85T 'PERIODIC' 99T 'TOTAL' 114T 'FINAL'
            TabSetting(36),"CROSS",new TabSetting(45),"INTENT",new TabSetting(53),"SEQUENCE",new TabSetting(63),"DEDUCTION",new TabSetting(73),"DEDUCTION",new 
            TabSetting(85),"PERIODIC",new TabSetting(99),"TOTAL",new TabSetting(114),"FINAL");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 46T 'CODE' 54T 'NUMBER' 65T 'CODE' 75T 'PAYEE' 84T 'DEDUCTION' 97T 'DEDUCTION' 109T 'DEDUCTION DATE'
            TabSetting(46),"CODE",new TabSetting(54),"NUMBER",new TabSetting(65),"CODE",new TabSetting(75),"PAYEE",new TabSetting(84),"DEDUCTION",new TabSetting(97),"DEDUCTION",new 
            TabSetting(109),"DEDUCTION DATE");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 45T '------' 53T '--------' 63T '---------' 75T '-----' 84T '---------' 95T '-------------' 109T '--------------'
            TabSetting(34),"---------",new TabSetting(45),"------",new TabSetting(53),"--------",new TabSetting(63),"---------",new TabSetting(75),"-----",new 
            TabSetting(84),"---------",new TabSetting(95),"-------------",new TabSetting(109),"--------------");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Trans_Nbr(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #DDCTN-FROM-NET.#TRANS-NBR 16T #DDCTN-FROM-NET.#CNTRCT-NBR 27T #DDCTN-FROM-NET.#RECORD-STATUS ( EM = 99 ) 34T #DDCTN-FROM-NET.#CROSS-REF-NBR 48T #DDCTN-FROM-NET.#INTENT-CODE 56T #DDCTN-FROM-NET.#SEQUENCE-NBR 66T #DDCTN-FROM-NET.#DDCTN-CDE 75T #DDCTN-FROM-NET.#DDCTN-PAYEE 84T #DDCTN-FROM-NET.#PER-DDCTN-AMT-N ( EM = ZZ,ZZZ.99 ) 95T #DDCTN-FROM-NET.#TOT-DDCTN-N ( EM = Z,ZZZ,ZZZ.99 ) 109T #DDCTN-FROM-NET.#FINAL-DDCTN-DTE
            TabSetting(16),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Cntrct_Nbr(),new TabSetting(27),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Record_Status(), new 
            ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Cross_Ref_Nbr(),new TabSetting(48),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Intent_Code(),new 
            TabSetting(56),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Sequence_Nbr(),new TabSetting(66),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Ddctn_Cde(),new TabSetting(75),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Ddctn_Payee(),new 
            TabSetting(84),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Per_Ddctn_Amt_N(), new ReportEditMask ("ZZ,ZZZ.99"),new TabSetting(95),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Tot_Ddctn_N(), 
            new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(109),ldaIaal920.getPnd_Ddctn_From_Net_Pnd_Final_Ddctn_Dte());
        if (Global.isEscape()) return;
    }
    private void sub_Process_His_Ytd_Ddctn() throws Exception                                                                                                             //Natural: PROCESS-HIS-YTD-DDCTN
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_His_Ytd_Ddctn().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                       //Natural: ASSIGN #HIS-YTD-DDCTN := #OUTPUT-REC
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Mm(),                //Natural: COMPRESS #HIS-YTD-DDCTN.#TRANS-DTE-MM '/' #HIS-YTD-DDCTN.#TRANS-DTE-DD '/' #HIS-YTD-DDCTN.#TRANS-DTE-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            "/", ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Dd(), "/", ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Trans_Dte_Yy()));
        ldaIaal920.getPnd_Total_Hist_Ded().getValue(1).nadd(ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N());                                                       //Natural: ADD #HIS-YTD-DDCTN.#HIS-DDCTN-AMT-N TO #TOTAL-HIST-DED ( 1 )
        ldaIaal920.getPnd_Total_Hist_Ded().getValue(2).nadd(ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N());                                                       //Natural: ADD #HIS-YTD-DDCTN.#HIS-DDCTN-AMT-N TO #TOTAL-HIST-DED ( 2 )
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new  //Natural: WRITE ( 2 ) /// 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 45T 'INTENT' 53T 'SEQUENCE' 63T 'DEDUCTION' 73T 'DEDUCTION' 83T 'HISTORICAL'
            TabSetting(36),"CROSS",new TabSetting(45),"INTENT",new TabSetting(53),"SEQUENCE",new TabSetting(63),"DEDUCTION",new TabSetting(73),"DEDUCTION",new 
            TabSetting(83),"HISTORICAL");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 46T 'CODE' 54T 'NUMBER' 65T 'CODE' 75T 'PAYEE' 84T 'DEDUCTION'
            TabSetting(46),"CODE",new TabSetting(54),"NUMBER",new TabSetting(65),"CODE",new TabSetting(75),"PAYEE",new TabSetting(84),"DEDUCTION");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 45T '------' 53T '--------' 63T '---------' 75T '-----' 82T '------------'
            TabSetting(34),"---------",new TabSetting(45),"------",new TabSetting(53),"--------",new TabSetting(63),"---------",new TabSetting(75),"-----",new 
            TabSetting(82),"------------");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Trans_Nbr(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #HIS-YTD-DDCTN.#TRANS-NBR 16T #HIS-YTD-DDCTN.#CNTRCT-NBR 27T #HIS-YTD-DDCTN.#RECORD-STATUS ( EM = 99 ) 34T #HIS-YTD-DDCTN.#CROSS-REF-NBR 48T #HIS-YTD-DDCTN.#INTENT-CODE 56T #HIS-YTD-DDCTN.#SEQUENCE-NBR 66T #HIS-YTD-DDCTN.#DDCTN-CDE 75T #HIS-YTD-DDCTN.#DDCTN-PAYEE 82T #HIS-YTD-DDCTN.#HIS-DDCTN-AMT-N ( EM = Z,ZZZ,ZZZ.99 )
            TabSetting(16),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Cntrct_Nbr(),new TabSetting(27),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Record_Status(), new 
            ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Cross_Ref_Nbr(),new TabSetting(48),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Intent_Code(),new 
            TabSetting(56),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Sequence_Nbr(),new TabSetting(66),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Cde(),new TabSetting(75),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_Ddctn_Payee(),new 
            TabSetting(82),ldaIaal920.getPnd_His_Ytd_Ddctn_Pnd_His_Ddctn_Amt_N(), new ReportEditMask ("Z,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Process_Selection_Rcrd() throws Exception                                                                                                            //Natural: PROCESS-SELECTION-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Selection_Rcrd().setValue(ldaIaal920.getPnd_Output_Rec());                                                                                      //Natural: ASSIGN #SELECTION-RCRD := #OUTPUT-REC
        ldaIaal920.getPnd_Display_Dte_Trans().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Mm(),              //Natural: COMPRESS #SELECTION-RCRD.#TRANS-DTE1-MM '/' #SELECTION-RCRD.#TRANS-DTE1-DD '/' #SELECTION-RCRD.#TRANS-DTE1-YY INTO #DISPLAY-DTE-TRANS LEAVING NO
            "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Dd(), "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Dte1_Yy()));
        short decideConditionsMet1046 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RECORD-TYPE-NBR1;//Natural: VALUE 101
        if (condition((ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().equals(101))))
        {
            decideConditionsMet1046++;
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Pend_Dte_Mm().notEquals(" ")))                                                                             //Natural: IF #PEND-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Pend().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Pend_Dte_Mm(),         //Natural: COMPRESS #PEND-DTE-MM '/' #PEND-DTE-YY INTO #DISPLAY-DTE-PEND LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Pend_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm().notEquals(" ")))                                                                              //Natural: IF #ISS-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Iss().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Mm(),           //Natural: COMPRESS #ISS-DTE-MM '/' #ISS-DTE-YY INTO #DISPLAY-DTE-ISS LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Iss_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm().notEquals(" ")))                                                                      //Natural: IF #1ST-PAY-DUE-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_1st_Pay_Due().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Mm(),  //Natural: COMPRESS #1ST-PAY-DUE-DTE-MM '/' #1ST-PAY-DUE-DTE-YY INTO #DISPLAY-DTE-1ST-PAY-DUE LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Pay_Due_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm().notEquals(" ")))                                                                      //Natural: IF #LST-MAN-CHK-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Lst_Man_Chk().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Mm(),  //Natural: COMPRESS #LST-MAN-CHK-DTE-MM '/' #LST-MAN-CHK-DTE-YY INTO #DISPLAY-DTE-LST-MAN-CHK LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Lst_Man_Chk_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm().notEquals(" ")))                                                                      //Natural: IF #FIN-PER-PAY-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Fin_Per_Pay().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Mm(),  //Natural: COMPRESS #FIN-PER-PAY-DTE-MM '/' #FIN-PER-PAY-DTE-YY INTO #DISPLAY-DTE-FIN-PER-PAY LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Per_Pay_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm().notEquals(" ")))                                                                         //Natural: IF #FIN-PYMT-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Fin_Pymt().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Mm(), //Natural: COMPRESS #FIN-PYMT-DTE-MM '/' #FIN-PYMT-DTE-DD '/' #FIN-PYMT-DTE-YY INTO #DISPLAY-DTE-FIN-PYMT LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Dd(), "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pymt_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm().notEquals(" ")))                                                                          //Natural: IF #WDRAWAL-DTE-MM NE ' '
            {
                ldaIaal920.getPnd_Display_Dte_Wdrawal().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Mm(),   //Natural: COMPRESS #WDRAWAL-DTE-MM '/' #WDRAWAL-DTE-YY INTO #DISPLAY-DTE-WDRAWAL LEAVING NO
                    "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Wdrawal_Dte_Yy()));
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                          //Natural: WRITE ( 2 ) ///
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(91),"1ST",new TabSetting(99),"LAST",new TabSetting(105),"FINAL");                                   //Natural: WRITE ( 2 ) 91T '1ST' 99T 'LAST' 105T 'FINAL'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new TabSetting(36),"CROSS",new  //Natural: WRITE ( 2 ) 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 59T 'PEND HOLD' 70T 'PEND' 83T 'ISSUE PAY DUE' 98T 'CHECK PER PAY' 114T 'FINAL' 121T 'WITHDRWL'
                TabSetting(59),"PEND HOLD",new TabSetting(70),"PEND",new TabSetting(83),"ISSUE PAY DUE",new TabSetting(98),"CHECK PER PAY",new TabSetting(114),"FINAL",new 
                TabSetting(121),"WITHDRWL");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 44T 'PROD' 49T 'CURR' 54T 'MODE' 59T 'CODE' 64T 'CODE' 70T 'DATE OPT ORG' 84T 'DATE' 90T 'DATE' 99T 'DATE' 105T 'DATE' 112T 'PAY DATE' 123T 'DATE'
                TabSetting(44),"PROD",new TabSetting(49),"CURR",new TabSetting(54),"MODE",new TabSetting(59),"CODE",new TabSetting(64),"CODE",new TabSetting(70),"DATE OPT ORG",new 
                TabSetting(84),"DATE",new TabSetting(90),"DATE",new TabSetting(99),"DATE",new TabSetting(105),"DATE",new TabSetting(112),"PAY DATE",new 
                TabSetting(123),"DATE");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"-------- ----- ---------- ------ --------- ---- ----",new TabSetting(54),"---- ---- ---- ----- --- --- ----- -------  -----",new  //Natural: WRITE ( 2 ) 1T '-------- ----- ---------- ------ --------- ---- ----' 54T '---- ---- ---- ----- --- --- ----- -------  -----' 104T '------- -------- --------'
                TabSetting(104),"------- -------- --------");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 44T #SELECTION-RCRD.#PRODUCT 51T #SELECTION-RCRD.#CURRENCY 55T #SELECTION-RCRD.#MODE 61T #SELECTION-RCRD.#PEND-CODE 66T #SELECTION-RCRD.#HOLD-CHECK-CDE 69T #DISPLAY-DTE-PEND 76T #SELECTION-RCRD.#OPTION 80T #SELECTION-RCRD.#ORIGIN 83T #DISPLAY-DTE-ISS 90T #DISPLAY-DTE-1ST-PAY-DUE 98T #DISPLAY-DTE-LST-MAN-CHK 105T #DISPLAY-DTE-FIN-PER-PAY 112T #DISPLAY-DTE-FIN-PYMT 123T #DISPLAY-DTE-WDRAWAL
                TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(44),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Product(),new 
                TabSetting(51),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Currency(),new TabSetting(55),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Mode(),new TabSetting(61),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Pend_Code(),new 
                TabSetting(66),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Hold_Check_Cde(),new TabSetting(69),ldaIaal920.getPnd_Display_Dte_Pend(),new TabSetting(76),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Option(),new 
                TabSetting(80),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Origin(),new TabSetting(83),ldaIaal920.getPnd_Display_Dte_Iss(),new TabSetting(90),ldaIaal920.getPnd_Display_Dte_1st_Pay_Due(),new 
                TabSetting(98),ldaIaal920.getPnd_Display_Dte_Lst_Man_Chk(),new TabSetting(105),ldaIaal920.getPnd_Display_Dte_Fin_Per_Pay(),new TabSetting(112),ldaIaal920.getPnd_Display_Dte_Fin_Pymt(),new 
                TabSetting(123),ldaIaal920.getPnd_Display_Dte_Wdrawal());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().equals(102))))
        {
            decideConditionsMet1046++;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                          //Natural: WRITE ( 2 ) ///
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(102),"PENS");                                                                                       //Natural: WRITE ( 2 ) 102T 'PENS'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new TabSetting(36),"CROSS",new  //Natural: WRITE ( 2 ) 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 49T 'ISSUE' 56T 'RES' 61T 'COLLGE' 70T 'RTB/TTB' 91T 'JNT' 102T 'PLAN' 108T 'CNTRCT'
                TabSetting(49),"ISSUE",new TabSetting(56),"RES",new TabSetting(61),"COLLGE",new TabSetting(70),"RTB/TTB",new TabSetting(91),"JNT",new TabSetting(102),"PLAN",new 
                TabSetting(108),"CNTRCT");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 44T 'CITZ' 49T 'ST/CT' 55T 'ST/CT' 62T 'ISSUE' 71T 'AMOUNT' 81T 'SS NUMBER' 91T 'CVT' 95T 'SPIRT' 102T 'CODE' 109T 'TYPE'
                TabSetting(44),"CITZ",new TabSetting(49),"ST/CT",new TabSetting(55),"ST/CT",new TabSetting(62),"ISSUE",new TabSetting(71),"AMOUNT",new TabSetting(81),"SS NUMBER",new 
                TabSetting(91),"CVT",new TabSetting(95),"SPIRT",new TabSetting(102),"CODE",new TabSetting(109),"TYPE");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 44T '----' 49T '-----' 55T '-----' 62T '-----' 68T '------------' 81T '---------' 91T '---' 95T '-----' 102T '----' 108T '------'
                TabSetting(34),"---------",new TabSetting(44),"----",new TabSetting(49),"-----",new TabSetting(55),"-----",new TabSetting(62),"-----",new 
                TabSetting(68),"------------",new TabSetting(81),"---------",new TabSetting(91),"---",new TabSetting(95),"-----",new TabSetting(102),"----",new 
                TabSetting(108),"------");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 45T #SELECTION-RCRD.#CITIZEN 50T #SELECTION-RCRD.#ST/CNTRY-ISS 56T #SELECTION-RCRD.#ST/CNTRY-RES 62T #SELECTION-RCRD.#COLL-ISS 68T #SELECTION-RCRD.#RTB/TTB-AMT ( EM = Z,ZZZ,ZZZ.99 ) 81T #SELECTION-RCRD.#SSN 92T #SELECTION-RCRD.#JOINT-CNVRT 97T #SELECTION-RCRD.#SPIRT 104T #SELECTION-RCRD.#PEN-PLN-CDE 110T #SELECTION-RCRD.#CNTRCT-TYPE
                TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(45),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Citizen(),new 
                TabSetting(50),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Iss(),new TabSetting(56),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Stfslash_Cntry_Res(),new 
                TabSetting(62),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Coll_Iss(),new TabSetting(68),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Rtbfslash_Ttb_Amt(), 
                new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(81),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ssn(),new TabSetting(92),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Joint_Cnvrt(),new 
                TabSetting(97),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Spirt(),new TabSetting(104),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Pen_Pln_Cde(),new TabSetting(110),
                ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Type());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 201
        else if (condition((ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().equals(201))))
        {
            decideConditionsMet1046++;
            ldaIaal920.getPnd_Display_Dte_Dob1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Dob_Mm(),              //Natural: COMPRESS #1ST-DOB-MM '/' #1ST-DOB-DD '/' #1ST-DOB-YY INTO #DISPLAY-DTE-DOB1 LEAVING NO
                "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Dob_Dd(), "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Dob_Yy()));
            ldaIaal920.getPnd_Display_Dte_Dod1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Dod_Mm(),              //Natural: COMPRESS #1ST-DOD-MM '/' #1ST-DOD-YY INTO #DISPLAY-DTE-DOD1 LEAVING NO
                "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Dod_Yy()));
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                          //Natural: WRITE ( 2 ) ///
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(44),"----------------- 1ST ANNUITANT ----------------");                                            //Natural: WRITE ( 2 ) 44T '----------------- 1ST ANNUITANT ----------------'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new TabSetting(36),"CROSS",new  //Natural: WRITE ( 2 ) 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 46T 'CROSS' 62T 'DATE' 71T 'DATE' 77T 'MORT' 82T 'LIFE' 88T 'DIV' 92T 'COLL CODE ORIGINAL CASH EMPLY'
                TabSetting(46),"CROSS",new TabSetting(62),"DATE",new TabSetting(71),"DATE",new TabSetting(77),"MORT",new TabSetting(82),"LIFE",new TabSetting(88),"DIV",new 
                TabSetting(92),"COLL CODE ORIGINAL CASH EMPLY");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 44T 'REFERENCE' 55T 'SEX' 60T 'OF BIRTH' 70T 'DEATH' 77T 'YEAR' 83T 'CNT' 87T 'PAYE' 93T 'DIVIDEND' 102T 'CONTRACT CODE' 117T 'TERM'
                TabSetting(44),"REFERENCE",new TabSetting(55),"SEX",new TabSetting(60),"OF BIRTH",new TabSetting(70),"DEATH",new TabSetting(77),"YEAR",new 
                TabSetting(83),"CNT",new TabSetting(87),"PAYE",new TabSetting(93),"DIVIDEND",new TabSetting(102),"CONTRACT CODE",new TabSetting(117),"TERM");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 44T '---------' 55T '---' 60T '--------' 70T '-----' 77T '----' 82T '----' 87T '----' 92T '---------' 102T '--------' 111T '----' 116T '-----'
                TabSetting(34),"---------",new TabSetting(44),"---------",new TabSetting(55),"---",new TabSetting(60),"--------",new TabSetting(70),"-----",new 
                TabSetting(77),"----",new TabSetting(82),"----",new TabSetting(87),"----",new TabSetting(92),"---------",new TabSetting(102),"--------",new 
                TabSetting(111),"----",new TabSetting(116),"-----");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 44T #SELECTION-RCRD.#1ST-ANNT-X-REF 56T #SELECTION-RCRD.#1ST-SEX 60T #DISPLAY-DTE-DOB1 70T #DISPLAY-DTE-DOD1 78T #SELECTION-RCRD.#MORT-YOB3 84T #SELECTION-RCRD.#LIFE-CNT3 89T #SELECTION-RCRD.#DIV-PAYEE 94T #SELECTION-RCRD.#COLL-CDE 102T #SELECTION-RCRD.#ORIG-CNTRCT 113T #SELECTION-RCRD.#CASH-CDE 118T #SELECTION-RCRD.#EMP-TERM
                TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(44),ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Annt_X_Ref(),new 
                TabSetting(56),ldaIaal920.getPnd_Selection_Rcrd_Pnd_1st_Sex(),new TabSetting(60),ldaIaal920.getPnd_Display_Dte_Dob1(),new TabSetting(70),ldaIaal920.getPnd_Display_Dte_Dod1(),new 
                TabSetting(78),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Mort_Yob3(),new TabSetting(84),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Life_Cnt3(),new 
                TabSetting(89),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Div_Payee(),new TabSetting(94),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Coll_Cde(),new TabSetting(102),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Orig_Cntrct(),new 
                TabSetting(113),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cash_Cde(),new TabSetting(118),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Emp_Term());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 202
        else if (condition((ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().equals(202))))
        {
            decideConditionsMet1046++;
            ldaIaal920.getPnd_Display_Dte_Dod1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Mm(),              //Natural: COMPRESS #BEN-DOD-MM '/' #BEN-DOD-YY INTO #DISPLAY-DTE-DOD1 LEAVING NO
                "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Yy()));
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                          //Natural: WRITE ( 2 ) ///
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(44),"----------------- 2ND ANNUITANT ----------------");                                            //Natural: WRITE ( 2 ) 44T '----------------- 2ND ANNUITANT ----------------'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new TabSetting(36),"CROSS",new  //Natural: WRITE ( 2 ) 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 46T 'CROSS' 62T 'DATE' 71T 'DATE' 77T 'MORT' 82T 'LIFE' 88T 'DIV' 92T 'COLL CODE ORIGINAL CASH EMPLY'
                TabSetting(46),"CROSS",new TabSetting(62),"DATE",new TabSetting(71),"DATE",new TabSetting(77),"MORT",new TabSetting(82),"LIFE",new TabSetting(88),"DIV",new 
                TabSetting(92),"COLL CODE ORIGINAL CASH EMPLY");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 44T 'REFERENCE' 55T 'SEX' 60T 'OF BIRTH' 70T 'DEATH' 77T 'YEAR' 83T 'CNT' 87T 'PAYE' 93T 'DIVIDEND' 102T 'CONTRACT CODE' 117T 'TERM'
                TabSetting(44),"REFERENCE",new TabSetting(55),"SEX",new TabSetting(60),"OF BIRTH",new TabSetting(70),"DEATH",new TabSetting(77),"YEAR",new 
                TabSetting(83),"CNT",new TabSetting(87),"PAYE",new TabSetting(93),"DIVIDEND",new TabSetting(102),"CONTRACT CODE",new TabSetting(117),"TERM");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 44T '---------' 55T '---' 60T '--------' 70T '-----' 77T '----' 82T '----' 87T '----' 92T '---------' 102T '--------' 111T '----' 116T '-----'
                TabSetting(34),"---------",new TabSetting(44),"---------",new TabSetting(55),"---",new TabSetting(60),"--------",new TabSetting(70),"-----",new 
                TabSetting(77),"----",new TabSetting(82),"----",new TabSetting(87),"----",new TabSetting(92),"---------",new TabSetting(102),"--------",new 
                TabSetting(111),"----",new TabSetting(116),"-----");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 44T #SELECTION-RCRD.#2ND-ANNT-X-REF 56T #SELECTION-RCRD.#2ND-SEX 60T #DISPLAY-DTE-DOB1 70T #DISPLAY-DTE-DOD1 78T #SELECTION-RCRD.#MORT-YOB4 84T #SELECTION-RCRD.#LIFE-CNT4 89T #SELECTION-RCRD.#DIV-PAYEE 92T #SELECTION-RCRD.#BEN-XREF 104T #DISPLAY-DTE-DOD1 113T #SELECTION-RCRD.#DEST-PREV 119T #SELECTION-RCRD.#DEST-CURR
                TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(44),ldaIaal920.getPnd_Selection_Rcrd_Pnd_2nd_Annt_X_Ref(),new 
                TabSetting(56),ldaIaal920.getPnd_Selection_Rcrd_Pnd_2nd_Sex(),new TabSetting(60),ldaIaal920.getPnd_Display_Dte_Dob1(),new TabSetting(70),ldaIaal920.getPnd_Display_Dte_Dod1(),new 
                TabSetting(78),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Mort_Yob4(),new TabSetting(84),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Life_Cnt4(),new 
                TabSetting(89),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Div_Payee(),new TabSetting(92),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ben_Xref(),new TabSetting(104),ldaIaal920.getPnd_Display_Dte_Dod1(),new 
                TabSetting(113),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Dest_Prev(),new TabSetting(119),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Dest_Curr());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 501:592
        else if (condition(((ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().greaterOrEqual(501) && ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Type_Nbr1().lessOrEqual(592)))))
        {
            decideConditionsMet1046++;
            ldaIaal920.getPnd_Display_Dte_Dod1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Mm(),              //Natural: COMPRESS #BEN-DOD-MM '/' #BEN-DOD-YY INTO #DISPLAY-DTE-DOD1 LEAVING NO
                "/", ldaIaal920.getPnd_Selection_Rcrd_Pnd_Ben_Dod_Yy()));
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                          //Natural: WRITE ( 2 ) ///
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"TRANS",new TabSetting(10),"TRANS CONTRACT",new TabSetting(27),"RECORD",new TabSetting(36),"CROSS",new  //Natural: WRITE ( 2 ) 2T 'TRANS' 10T 'TRANS CONTRACT' 27T 'RECORD' 36T 'CROSS' 50T 'PERIODIC PAY' 66T 'PERIOD' 79T 'FINAL' 91T 'OLD PERIOD' 108T 'OLD PERIOD'
                TabSetting(50),"PERIODIC PAY",new TabSetting(66),"PERIOD",new TabSetting(79),"FINAL",new TabSetting(91),"OLD PERIOD",new TabSetting(108),
                "OLD PERIOD");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(2)," DATE",new TabSetting(10)," CODE   NUMBER",new TabSetting(27),"STATUS",new TabSetting(34),"REFERENCE",new  //Natural: WRITE ( 2 ) 2T ' DATE' 10T ' CODE   NUMBER' 27T 'STATUS' 34T 'REFERENCE' 44T 'RATE' 51T 'CREF UNITS' 65T 'DIVIDEND' 78T 'PAYMENT' 92T 'DIVIDEND' 104T 'PER PAY/CREF UNITS'
                TabSetting(44),"RATE",new TabSetting(51),"CREF UNITS",new TabSetting(65),"DIVIDEND",new TabSetting(78),"PAYMENT",new TabSetting(92),"DIVIDEND",new 
                TabSetting(104),"PER PAY/CREF UNITS");
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(10),"-----",new TabSetting(16),"----------",new TabSetting(27),"------",new  //Natural: WRITE ( 2 ) 1T '--------' 10T '-----' 16T '----------' 27T '------' 34T '---------' 44T '---' 50T '------------' 64T '---------' 75T '------------' 89T '-------------' 104T '------------------'
                TabSetting(34),"---------",new TabSetting(44),"---",new TabSetting(50),"------------",new TabSetting(64),"---------",new TabSetting(75),"------------",new 
                TabSetting(89),"-------------",new TabSetting(104),"------------------");
            if (Global.isEscape()) return;
            if (condition(DbsUtil.maskMatches(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr_N1(),"N")))                                                                 //Natural: IF #SELECTION-RCRD.#CNTRCT-NBR-N1 = MASK ( N )
            {
                if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1().equals(33)))                                                                              //Natural: IF #SELECTION-RCRD.#TRANS-NBR1 = 033
                {
                    ldaIaal920.getPnd_Total_Per_Div().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 1 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 1 )
                    ldaIaal920.getPnd_Total_Cref_Units().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cref_Units());                                             //Natural: ADD #SELECTION-RCRD.#CREF-UNITS TO #TOTAL-CREF-UNITS ( 1 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 3 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 3 )
                    ldaIaal920.getPnd_Total_Cref_Units().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cref_Units());                                             //Natural: ADD #SELECTION-RCRD.#CREF-UNITS TO #TOTAL-CREF-UNITS ( 3 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1().equals(50)))                                                                              //Natural: IF #SELECTION-RCRD.#TRANS-NBR1 = 050
                {
                    ldaIaal920.getPnd_Total_Per_Div().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 2 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 2 )
                    ldaIaal920.getPnd_Total_Cref_Units().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cref_Units());                                             //Natural: ADD #SELECTION-RCRD.#CREF-UNITS TO #TOTAL-CREF-UNITS ( 2 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 3 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 3 )
                    ldaIaal920.getPnd_Total_Cref_Units().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cref_Units());                                             //Natural: ADD #SELECTION-RCRD.#CREF-UNITS TO #TOTAL-CREF-UNITS ( 3 )
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 44T #SELECTION-RCRD.#RATE 50T #SELECTION-RCRD.#CREF-UNITS ( EM = ZZ,ZZZ.999 ) 64T #SELECTION-RCRD.#PER-DIV ( EM = ZZ,ZZZ.99 ) 75T #SELECTION-RCRD.#FIN-PAY ( EM = Z,ZZZ,ZZZ.99 ) 89T #SELECTION-RCRD.#TOT-OLD-PER-DIV ( EM = Z,ZZZ,ZZZ.99 ) 106T #SELECTION-RCRD.#TOT-OLD-CREF-UNITS ( EM = Z,ZZZ,ZZZ.999 )
                    TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                    new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(44),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Rate(),new 
                    TabSetting(50),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cref_Units(), new ReportEditMask ("ZZ,ZZZ.999"),new TabSetting(64),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div(), 
                    new ReportEditMask ("ZZ,ZZZ.99"),new TabSetting(75),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay(), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
                    TabSetting(89),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div(), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(106),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Tot_Old_Cref_Units(), 
                    new ReportEditMask ("ZZZ,ZZZ.999"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1().equals(33)))                                                                              //Natural: IF #SELECTION-RCRD.#TRANS-NBR1 = 033
                {
                    ldaIaal920.getPnd_Total_Per_Pay().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-PAY ( 1 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 1 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(1).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 1 )
                    ldaIaal920.getPnd_Total_Per_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-PAY ( 3 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div());                                                   //Natural: ADD #SELECTION-RCRD.#PER-DIV TO #TOTAL-PER-DIV ( 3 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 3 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1().equals(50)))                                                                              //Natural: IF #SELECTION-RCRD.#TRANS-NBR1 = 050
                {
                    ldaIaal920.getPnd_Total_Per_Pay().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-PAY ( 2 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-DIV ( 2 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(2).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 2 )
                    ldaIaal920.getPnd_Total_Per_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-PAY ( 3 )
                    ldaIaal920.getPnd_Total_Per_Div().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#PER-PAY TO #TOTAL-PER-DIV ( 3 )
                    ldaIaal920.getPnd_Total_Fin_Pay().getValue(3).nadd(ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay());                                                   //Natural: ADD #SELECTION-RCRD.#FIN-PAY TO #TOTAL-FIN-PAY ( 3 )
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),ldaIaal920.getPnd_Display_Dte_Trans(),new TabSetting(11),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Trans_Nbr1(),new  //Natural: WRITE ( 2 ) 1T #DISPLAY-DTE-TRANS 11T #SELECTION-RCRD.#TRANS-NBR1 16T #SELECTION-RCRD.#CNTRCT-NBR1 27T #SELECTION-RCRD.#RECORD-STATUS1 ( EM = 99 ) 34T #SELECTION-RCRD.#CROSS-REF-NBR1 44T #SELECTION-RCRD.#RATE 50T #SELECTION-RCRD.#PER-PAY ( EM = ZZ,ZZZ.99 ) 64T #SELECTION-RCRD.#PER-DIV ( EM = ZZ,ZZZ.99 ) 75T #SELECTION-RCRD.#FIN-PAY ( EM = Z,ZZZ,ZZZ.99 ) 89T #SELECTION-RCRD.#TOT-OLD-PER-DIV ( EM = Z,ZZZ,ZZZ.99 ) 106T #SELECTION-RCRD.#TOT-OLD-PER-PAY ( EM = Z,ZZZ,ZZZ.99 )
                    TabSetting(16),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cntrct_Nbr1(),new TabSetting(27),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Record_Status1(), 
                    new ReportEditMask ("99"),new TabSetting(34),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Cross_Ref_Nbr1(),new TabSetting(44),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Rate(),new 
                    TabSetting(50),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Pay(), new ReportEditMask ("ZZ,ZZZ.99"),new TabSetting(64),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Per_Div(), 
                    new ReportEditMask ("ZZ,ZZZ.99"),new TabSetting(75),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Fin_Pay(), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
                    TabSetting(89),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Div(), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(106),ldaIaal920.getPnd_Selection_Rcrd_Pnd_Tot_Old_Per_Pay(), 
                    new ReportEditMask ("Z,ZZZ,ZZZ.99"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Calculate_Trans() throws Exception                                                                                                                   //Natural: CALCULATE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1149 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #OUTPUT-REC.#TRANS-CDE-N;//Natural: VALUE 020
        if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(20))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 1 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 1 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 1 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 033
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(33))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 2 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(2).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 2 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 2 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(35))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(3).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 3 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(3).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 3 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 3 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 037
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(37))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(4).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 4 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(4).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 4 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 4 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 040
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(40))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(5).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 5 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(5).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 5 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 5 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 50 : 59
        else if (condition(((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().greaterOrEqual(50) && ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().lessOrEqual(59)))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(6).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 6 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(6).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 6 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 6 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 64
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(64))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(7).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 7 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(7).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 7 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(7).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 7 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 66
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(66))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(8).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 8 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(8).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 8 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(8).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 8 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 70
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(70))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(9).nadd(1);                                                                                                     //Natural: ADD 1 TO #USERID-TRANS ( 9 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(9).nadd(1);                                                                                                  //Natural: ADD 1 TO #USER-AREA-TRANS ( 9 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(9).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-TRANS ( 9 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(102))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(10).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 10 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(10).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 10 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(10).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 10 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(104))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(11).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 11 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(11).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 11 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(11).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 11 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(106))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(12).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 12 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(12).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 12 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(12).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 12 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 304
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(304))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(13).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 13 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(13).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 13 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(13).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 13 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(724))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(14).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 14 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(14).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 14 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(14).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 14 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 902
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(902))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(15).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 15 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(15).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 15 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(15).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 15 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((ldaIaal920.getPnd_Output_Rec_Pnd_Trans_Cde_N().equals(906))))
        {
            decideConditionsMet1149++;
            if (condition(ldaIaal920.getPnd_New_Trans_L().getBoolean()))                                                                                                  //Natural: IF #NEW-TRANS-L
            {
                ldaIaal920.getPnd_Userid_Trans().getValue(16).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 16 )
                ldaIaal920.getPnd_Userid_Trans().getValue(17).nadd(1);                                                                                                    //Natural: ADD 1 TO #USERID-TRANS ( 17 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(16).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 16 )
                ldaIaal920.getPnd_User_Area_Trans().getValue(17).nadd(1);                                                                                                 //Natural: ADD 1 TO #USER-AREA-TRANS ( 17 )
                ldaIaal920.getPnd_Total_Trans().getValue(16).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 16 )
                ldaIaal920.getPnd_Total_Trans().getValue(17).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS
        sub_Process_Trans();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Print_Userid_Totals() throws Exception                                                                                                               //Natural: PRINT-USERID-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Display_Heading().setValue("OPERATOR REPORT WITHIN AREA");                                                                                      //Natural: ASSIGN #DISPLAY-HEADING := 'OPERATOR REPORT WITHIN AREA'
        ldaIaal920.getPnd_Report().setValue("U");                                                                                                                         //Natural: ASSIGN #REPORT := 'U'
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,"USER ID    ",new TabSetting(14),ldaIaal920.getPnd_Output_Rec_Pnd_User_Id());                                          //Natural: WRITE ( 2 ) 'USER ID    ' 14T #OUTPUT-REC.#USER-ID
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"USER AREA  ",new TabSetting(14),ldaIaal920.getPnd_Output_Rec_Pnd_User_Area());                                        //Natural: WRITE ( 2 ) 'USER AREA  ' 14T #OUTPUT-REC.#USER-AREA
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(9),"TOTAL");                                                                    //Natural: WRITE ( 2 ) /// 9T 'TOTAL'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"TRANS",new TabSetting(9),"TRANS");                                                                  //Natural: WRITE ( 2 ) 1T 'TRANS' 9T 'TRANS'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"-----",new TabSetting(7),"-------");                                                                //Natural: WRITE ( 2 ) 1T '-----' 7T '-------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 020",ldaIaal920.getPnd_Userid_Trans().getValue(1), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 020' #USERID-TRANS ( 1 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 033",ldaIaal920.getPnd_Userid_Trans().getValue(2), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 033' #USERID-TRANS ( 2 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 035",ldaIaal920.getPnd_Userid_Trans().getValue(3), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 035' #USERID-TRANS ( 3 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 037",ldaIaal920.getPnd_Userid_Trans().getValue(4), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 037' #USERID-TRANS ( 4 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 040",ldaIaal920.getPnd_Userid_Trans().getValue(5), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 040' #USERID-TRANS ( 5 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 050",ldaIaal920.getPnd_Userid_Trans().getValue(6), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 050' #USERID-TRANS ( 6 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 064",ldaIaal920.getPnd_Userid_Trans().getValue(7), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 064' #USERID-TRANS ( 7 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 066",ldaIaal920.getPnd_Userid_Trans().getValue(8), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 066' #USERID-TRANS ( 8 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 070",ldaIaal920.getPnd_Userid_Trans().getValue(9), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 070' #USERID-TRANS ( 9 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 102",ldaIaal920.getPnd_Userid_Trans().getValue(10), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 102' #USERID-TRANS ( 10 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 104",ldaIaal920.getPnd_Userid_Trans().getValue(11), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 104' #USERID-TRANS ( 11 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 106",ldaIaal920.getPnd_Userid_Trans().getValue(12), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 106' #USERID-TRANS ( 12 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 304",ldaIaal920.getPnd_Userid_Trans().getValue(13), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 304' #USERID-TRANS ( 13 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 724",ldaIaal920.getPnd_Userid_Trans().getValue(14), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 724' #USERID-TRANS ( 14 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 902",ldaIaal920.getPnd_Userid_Trans().getValue(15), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 902' #USERID-TRANS ( 15 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 906",ldaIaal920.getPnd_Userid_Trans().getValue(16), new ReportEditMask ("ZZZ,ZZ9"));                                 //Natural: WRITE ( 2 ) ' 906' #USERID-TRANS ( 16 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL",new TabSetting(7),ldaIaal920.getPnd_Userid_Trans().getValue(17), new ReportEditMask ("ZZZ,ZZ9"));              //Natural: WRITE ( 2 ) 'TOTAL' 7T #USERID-TRANS ( 17 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        ldaIaal920.getPnd_Userid_Trans().getValue("*").reset();                                                                                                           //Natural: RESET #USERID-TRANS ( * ) #DISPLAY-HEADING #REPORT
        ldaIaal920.getPnd_Display_Heading().reset();
        ldaIaal920.getPnd_Report().reset();
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_User_Area_Totals() throws Exception                                                                                                            //Natural: PRINT-USER-AREA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Display_Heading().setValue("       AREA TOTALS         ");                                                                                      //Natural: ASSIGN #DISPLAY-HEADING := '       AREA TOTALS         '
        ldaIaal920.getPnd_Report().setValue("A");                                                                                                                         //Natural: ASSIGN #REPORT := 'A'
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,"USER AREA  ",new TabSetting(14),ldaIaal920.getPnd_Output_Rec_Pnd_User_Area());                                        //Natural: WRITE ( 2 ) 'USER AREA  ' 14T #OUTPUT-REC.#USER-AREA
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(9),"TOTAL");                                                                    //Natural: WRITE ( 2 ) /// 9T 'TOTAL'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"TRANS",new TabSetting(9),"TRANS");                                                                  //Natural: WRITE ( 2 ) 1T 'TRANS' 9T 'TRANS'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"-----",new TabSetting(7),"-------");                                                                //Natural: WRITE ( 2 ) 1T '-----' 7T '-------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 020",ldaIaal920.getPnd_User_Area_Trans().getValue(1), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 020' #USER-AREA-TRANS ( 1 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 033",ldaIaal920.getPnd_User_Area_Trans().getValue(2), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 033' #USER-AREA-TRANS ( 2 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 035",ldaIaal920.getPnd_User_Area_Trans().getValue(3), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 035' #USER-AREA-TRANS ( 3 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 037",ldaIaal920.getPnd_User_Area_Trans().getValue(4), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 037' #USER-AREA-TRANS ( 4 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 040",ldaIaal920.getPnd_User_Area_Trans().getValue(5), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 040' #USER-AREA-TRANS ( 5 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 050",ldaIaal920.getPnd_User_Area_Trans().getValue(6), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 050' #USER-AREA-TRANS ( 6 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 064",ldaIaal920.getPnd_User_Area_Trans().getValue(7), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 064' #USER-AREA-TRANS ( 7 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 066",ldaIaal920.getPnd_User_Area_Trans().getValue(8), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 066' #USER-AREA-TRANS ( 8 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 070",ldaIaal920.getPnd_User_Area_Trans().getValue(9), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 2 ) ' 070' #USER-AREA-TRANS ( 9 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 102",ldaIaal920.getPnd_User_Area_Trans().getValue(10), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 102' #USER-AREA-TRANS ( 10 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 104",ldaIaal920.getPnd_User_Area_Trans().getValue(11), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 104' #USER-AREA-TRANS ( 11 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 106",ldaIaal920.getPnd_User_Area_Trans().getValue(12), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 106' #USER-AREA-TRANS ( 12 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 304",ldaIaal920.getPnd_User_Area_Trans().getValue(13), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 304' #USER-AREA-TRANS ( 13 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 724",ldaIaal920.getPnd_User_Area_Trans().getValue(14), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 724' #USER-AREA-TRANS ( 14 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 902",ldaIaal920.getPnd_User_Area_Trans().getValue(15), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 902' #USER-AREA-TRANS ( 15 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 906",ldaIaal920.getPnd_User_Area_Trans().getValue(16), new ReportEditMask ("ZZZ,ZZ9"));                              //Natural: WRITE ( 2 ) ' 906' #USER-AREA-TRANS ( 16 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL",new TabSetting(7),ldaIaal920.getPnd_User_Area_Trans().getValue(17), new ReportEditMask ("ZZZ,ZZ9"));           //Natural: WRITE ( 2 ) 'TOTAL' 7T #USER-AREA-TRANS ( 17 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        ldaIaal920.getPnd_Userid_Trans().getValue("*").reset();                                                                                                           //Natural: RESET #USERID-TRANS ( * ) #USER-AREA-TRANS ( * ) #DISPLAY-HEADING #REPORT
        ldaIaal920.getPnd_User_Area_Trans().getValue("*").reset();
        ldaIaal920.getPnd_Display_Heading().reset();
        ldaIaal920.getPnd_Report().reset();
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal920.getPnd_Display_Heading().setValue("   GRAND TOTAL ALL AREAS   ");                                                                                      //Natural: ASSIGN #DISPLAY-HEADING := '   GRAND TOTAL ALL AREAS   '
        ldaIaal920.getPnd_Report().setValue("G");                                                                                                                         //Natural: ASSIGN #REPORT := 'G'
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(9),"TOTAL");                                                                    //Natural: WRITE ( 2 ) /// 9T 'TOTAL'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"TRANS",new TabSetting(9),"TRANS");                                                                  //Natural: WRITE ( 2 ) 1T 'TRANS' 9T 'TRANS'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"-----",new TabSetting(7),"-------");                                                                //Natural: WRITE ( 2 ) 1T '-----' 7T '-------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 020",ldaIaal920.getPnd_Total_Trans().getValue(1), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 020' #TOTAL-TRANS ( 1 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 033",ldaIaal920.getPnd_Total_Trans().getValue(2), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 033' #TOTAL-TRANS ( 2 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 035",ldaIaal920.getPnd_Total_Trans().getValue(3), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 035' #TOTAL-TRANS ( 3 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 037",ldaIaal920.getPnd_Total_Trans().getValue(4), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 037' #TOTAL-TRANS ( 4 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 040",ldaIaal920.getPnd_Total_Trans().getValue(5), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 040' #TOTAL-TRANS ( 5 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 050",ldaIaal920.getPnd_Total_Trans().getValue(6), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 050' #TOTAL-TRANS ( 6 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 064",ldaIaal920.getPnd_Total_Trans().getValue(7), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 064' #TOTAL-TRANS ( 7 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 066",ldaIaal920.getPnd_Total_Trans().getValue(8), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 066' #TOTAL-TRANS ( 8 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 070",ldaIaal920.getPnd_Total_Trans().getValue(9), new ReportEditMask ("ZZZ,ZZ9"));                                   //Natural: WRITE ( 2 ) ' 070' #TOTAL-TRANS ( 9 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 102",ldaIaal920.getPnd_Total_Trans().getValue(10), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 102' #TOTAL-TRANS ( 10 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 104",ldaIaal920.getPnd_Total_Trans().getValue(11), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 104' #TOTAL-TRANS ( 11 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 106",ldaIaal920.getPnd_Total_Trans().getValue(12), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 106' #TOTAL-TRANS ( 12 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 304",ldaIaal920.getPnd_Total_Trans().getValue(13), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 304' #TOTAL-TRANS ( 13 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 724",ldaIaal920.getPnd_Total_Trans().getValue(14), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 724' #TOTAL-TRANS ( 14 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 902",ldaIaal920.getPnd_Total_Trans().getValue(15), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 902' #TOTAL-TRANS ( 15 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE," 906",ldaIaal920.getPnd_Total_Trans().getValue(16), new ReportEditMask ("ZZZ,ZZ9"));                                  //Natural: WRITE ( 2 ) ' 906' #TOTAL-TRANS ( 16 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 2 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL",new TabSetting(7),ldaIaal920.getPnd_Total_Trans().getValue(17), new ReportEditMask ("ZZZ,ZZ9"));               //Natural: WRITE ( 2 ) 'TOTAL' 7T #TOTAL-TRANS ( 17 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*   ADD 1 TO #PAGE-CTR /* COMMENTED 11/03
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(26),"IA ADMINISTRATION TRANSACTION CONTROL LISTING",new   //Natural: WRITE ( 2 ) NOTITLE 'PROGRAM ' *PROGRAM 26X 'IA ADMINISTRATION TRANSACTION CONTROL LISTING' 32X 'PAGE ' #PAGE-CTR
                        ColumnSpacing(32),"PAGE ",ldaIaal920.getPnd_Page_Ctr());
                    getReports().write(2, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(52),ldaIaal920.getPnd_Display_Heading());                       //Natural: WRITE ( 2 ) '   DATE ' *DATU 52T #DISPLAY-HEADING
                    if (condition(ldaIaal920.getPnd_Report().equals("U") || ldaIaal920.getPnd_Report().equals("A") || ldaIaal920.getPnd_Report().equals("G")))            //Natural: IF #REPORT = 'U' OR = 'A' OR = 'G'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(78),"----------------- SELECTION CRITERIA ------------------");                 //Natural: WRITE ( 2 ) / 78T '----------------- SELECTION CRITERIA ------------------'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaal920.getPnd_Report().equals("U") || ldaIaal920.getPnd_Report().equals("A") || ldaIaal920.getPnd_Report().equals("G")))            //Natural: IF #REPORT = 'U' OR = 'A' OR = 'G'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,"USER AREA  ",new TabSetting(14),ldaIaal920.getPnd_Output_Rec_Pnd_User_Area(),new TabSetting(23),"CHECK DATE ",new  //Natural: WRITE ( 2 ) 'USER AREA  ' 14T #OUTPUT-REC.#USER-AREA 23T 'CHECK DATE ' 35T #OUTPUT-REC.#CHECK-DTE 78T 'USER AREA  ' 90T #PARM-USER-AREA 99T 'CHECK DATE ' 111T #PARM-CHECK-DTE-DISP
                            TabSetting(35),ldaIaal920.getPnd_Output_Rec_Pnd_Check_Dte(),new TabSetting(78),"USER AREA  ",new TabSetting(90),ldaIaal920.getIaa_Parm_Card_Pnd_Parm_User_Area(),new 
                            TabSetting(99),"CHECK DATE ",new TabSetting(111),ldaIaal920.getPnd_Parm_Check_Dte_Disp());
                        //*             50T '(ACCEPTED)'
                        //*             45T '(UN-VERIFIED OR ACCEPTED)'
                        //*            120T '(UN-VERIFIED'
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"USER ID    ",new TabSetting(14),ldaIaal920.getPnd_Output_Rec_Pnd_User_Id(),new      //Natural: WRITE ( 2 ) 1T 'USER ID    ' 14T #OUTPUT-REC.#USER-ID 78T 'USER ID    ' 90T #PARM-USER-ID 99T ' FROM DATE ' 111T #PARM-FROM-DTE-DISP
                            TabSetting(78),"USER ID    ",new TabSetting(90),ldaIaal920.getIaa_Parm_Card_Pnd_Parm_User_Id(),new TabSetting(99)," FROM DATE ",new 
                            TabSetting(111),ldaIaal920.getPnd_Parm_From_Dte_Disp());
                        //*             23T ' FROM DATE '  35T #OUTPUT-REC.#CHECK-DTE
                        //*            120T 'OR ACCEPTED'
                        //*   #OUTPUT-REC.#VERIFY-ID
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"VERIFIER ID",new TabSetting(14),new TabSetting(78),"VERIFIER ID",new                //Natural: WRITE ( 2 ) 1T 'VERIFIER ID' 14T 78T 'VERIFIER ID' 90T #PARM-VERIFY-ID 99T '   TO DATE ' 111T #PARM-TO-DTE-DISP
                            TabSetting(90),ldaIaal920.getIaa_Parm_Card_Pnd_Parm_Verify_Id(),new TabSetting(99),"   TO DATE ",new TabSetting(111),ldaIaal920.getPnd_Parm_To_Dte_Disp());
                        //*             23T '   TO DATE '  35T #OUTPUT-REC.#CHECK-DTE
                        //*            120T 'TOTALS ' #PARM-TOTALS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(3, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(4, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(5, "LS=133 PS=56 ZP=OFF SG=OFF");
    }
}
