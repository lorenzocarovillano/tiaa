/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:49 PM
**        * FROM NATURAL PROGRAM : Iaap302
************************************************************
**        * FILE NAME            : Iaap302.java
**        * CLASS NAME           : Iaap302
**        * INSTANCE NAME        : Iaap302
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM   -  IAAP302        THIS PROGRAM WILL READ ADABASE FILE    *
*      DATE   -  08/95          201 AND CREATE TWO FLAT EXTRACT FILES; *
*    AUTHOR   -  ARI G.         1) TIAA/CREF FUND EXTRACT DETAIL AND   *
*                               2) TIAA/CREF FUND EXTRACT SUMMARY. BOTH*
*                               FILES CONTAIN HEADER AND TRAILER       *
*                               RECORDS.                               *
*   3/2003                      INCREASE TIAA RATE FROM 1:40 TO 1:60   *
*                               DO SCAN ON 3/03 FOR CHANGES
*   4/2008                      INCREASE TIAA RATE FROM 1:60 TO 1:99 *
*                               DO SCAN ON 4/08 FOR CHANGES
*   3/2012                      INCREASE TIAA RATE FROM 99 TO 250    *
*                               DO SCAN ON 3/12 FOR CHANGES
*   4/2017                      INCREASE LRECL FOR PIN EXPANSION 328
*                               DO SCAN ON 4/17 FOR CHANGES
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap302 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Count_Fund_Record_Reads;
    private DbsField pnd_Count_Master_File_Reads;
    private DbsField pnd_Count_Master_Record_Error;
    private DbsField pnd_Count_Fund_Extract;
    private DbsField pnd_Count_Fund_Extract_2;
    private DbsField pnd_Count;
    private DbsField pnd_Count2;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cmpny_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Fund_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rtb_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Old_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Old_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Per_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Units_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tiaa_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_In_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_Out_Dte;

    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Record_Code;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cmpny_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Fund_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Old_Per_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Old_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Units_Cnt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tiaa_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Filler;

    private DbsGroup pnd_Work_Record_Header;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Record_Code;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Check_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Process_Date;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_1;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_2;
    private DbsField pnd_Work_Record_Header_Pnd_Wh_Filler_3;

    private DbsGroup pnd_Work_Record_Trailer;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Record_Code;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Check_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Process_Date;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_1;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_2;
    private DbsField pnd_Work_Record_Trailer_Pnd_Wt_Filler_3;
    private DbsField pnd_Per_Tot;
    private DbsField pnd_I;
    private DbsField pnd_All_Ok;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_View;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key;
    private DbsField pnd_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Check_Date_Ccyymmdd__R_Field_2;
    private DbsField pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N;
    private DbsField pnd_Blank_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Count_Fund_Record_Reads = localVariables.newFieldInRecord("pnd_Count_Fund_Record_Reads", "#COUNT-FUND-RECORD-READS", FieldType.NUMERIC, 8);
        pnd_Count_Master_File_Reads = localVariables.newFieldInRecord("pnd_Count_Master_File_Reads", "#COUNT-MASTER-FILE-READS", FieldType.NUMERIC, 8);
        pnd_Count_Master_Record_Error = localVariables.newFieldInRecord("pnd_Count_Master_Record_Error", "#COUNT-MASTER-RECORD-ERROR", FieldType.NUMERIC, 
            8);
        pnd_Count_Fund_Extract = localVariables.newFieldInRecord("pnd_Count_Fund_Extract", "#COUNT-FUND-EXTRACT", FieldType.NUMERIC, 8);
        pnd_Count_Fund_Extract_2 = localVariables.newFieldInRecord("pnd_Count_Fund_Extract_2", "#COUNT-FUND-EXTRACT-2", FieldType.NUMERIC, 8);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 8);
        pnd_Count2 = localVariables.newFieldInRecord("pnd_Count2", "#COUNT2", FieldType.NUMERIC, 8);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr", "#W1-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde", "#W1-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Record_Code", "#W1-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Cmpny_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cmpny_Cde", "#W1-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Fund_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Fund_Cde", "#W1-FUND-CDE", FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt", "#W1-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Rtb_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Rtb_Amt", "#W1-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt", "#W1-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt", "#W1-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Old_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Old_Per_Amt", "#W1-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Old_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Old_Div_Amt", "#W1-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W1_Rate_Cde = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Cde", "#W1-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Rate_Dte = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Dte", "#W1-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt", "#W1-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Per_Div_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Per_Div_Amt", "#W1-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Units_Cnt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Units_Cnt", "#W1-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt", "#W1-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt = pnd_Work_Record_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt", "#W1-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_1_Pnd_W1_Tiaa_Xfr_Iss_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tiaa_Xfr_Iss_Dte", "#W1-TIAA-XFR-ISS-DTE", 
            FieldType.DATE);
        pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_In_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_In_Dte", "#W1-TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE);
        pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_Out_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_Out_Dte", "#W1-TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE);

        pnd_Work_Record_2 = localVariables.newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr", "#W2-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde", "#W2-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Record_Code = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Record_Code", "#W2-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Cmpny_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cmpny_Cde", "#W2-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Fund_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Fund_Cde", "#W2-FUND-CDE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt", "#W2-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt", "#W2-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Old_Per_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Old_Per_Amt", "#W2-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Old_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Old_Div_Amt", "#W2-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Rate_Cde = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Cde", "#W2-RATE-CDE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Rate_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Dte", "#W2-RATE-DTE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt", "#W2-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Per_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Per_Div_Amt", "#W2-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_2_Pnd_W2_Units_Cnt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Units_Cnt", "#W2-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt", "#W2-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt", "#W2-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", FieldType.TIME);
        pnd_Work_Record_2_Pnd_W2_Tiaa_Xfr_Iss_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tiaa_Xfr_Iss_Dte", "#W2-TIAA-XFR-ISS-DTE", 
            FieldType.DATE);
        pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte", "#W2-TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE);
        pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_Out_Dte = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_Out_Dte", "#W2-TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE);
        pnd_Work_Record_2_Pnd_W2_Filler = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Filler", "#W2-FILLER", FieldType.STRING, 231);

        pnd_Work_Record_Header = localVariables.newGroupInRecord("pnd_Work_Record_Header", "#WORK-RECORD-HEADER");
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr", "#WH-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde", "#WH-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Record_Code = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Record_Code", "#WH-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Header_Pnd_Wh_Check_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Check_Date", "#WH-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Process_Date = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Process_Date", "#WH-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Header_Pnd_Wh_Filler_1 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_1", "#WH-FILLER-1", FieldType.STRING, 
            225);
        pnd_Work_Record_Header_Pnd_Wh_Filler_2 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_2", "#WH-FILLER-2", FieldType.STRING, 
            60);
        pnd_Work_Record_Header_Pnd_Wh_Filler_3 = pnd_Work_Record_Header.newFieldInGroup("pnd_Work_Record_Header_Pnd_Wh_Filler_3", "#WH-FILLER-3", FieldType.STRING, 
            13);

        pnd_Work_Record_Trailer = localVariables.newGroupInRecord("pnd_Work_Record_Trailer", "#WORK-RECORD-TRAILER");
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr", "#WT-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde", "#WT-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Record_Code", "#WT-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Check_Date", "#WT-CHECK-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Process_Date", "#WT-PROCESS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records", "#WT-TOT-FUND-RECORDS", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_1", "#WT-FILLER-1", FieldType.STRING, 
            193);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_2", "#WT-FILLER-2", FieldType.STRING, 
            84);
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_3 = pnd_Work_Record_Trailer.newFieldInGroup("pnd_Work_Record_Trailer_Pnd_Wt_Filler_3", "#WT-FILLER-3", FieldType.STRING, 
            13);
        pnd_Per_Tot = localVariables.newFieldInRecord("pnd_Per_Tot", "#PER-TOT", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_All_Ok = localVariables.newFieldInRecord("pnd_All_Ok", "#ALL-OK", FieldType.STRING, 1);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cntrl_Rcrd_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_View", "IAA-CNTRL-RCRD-VIEW"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key = vw_iaa_Cntrl_Rcrd_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRL_RCRD_KEY");
        iaa_Cntrl_Rcrd_View_Cntrl_Rcrd_Key.setSuperDescriptor(true);
        registerRecord(vw_iaa_Cntrl_Rcrd_View);

        pnd_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Check_Date_Ccyymmdd__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Date_Ccyymmdd__R_Field_2", "REDEFINE", pnd_Check_Date_Ccyymmdd);
        pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N = pnd_Check_Date_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N", 
            "#CHECK-DATE-CCYYMMDD-N", FieldType.NUMERIC, 8);
        pnd_Blank_Ctr = localVariables.newFieldInRecord("pnd_Blank_Ctr", "#BLANK-CTR", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap302() throws Exception
    {
        super("Iaap302");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP300 ***");                                                                                                        //Natural: WRITE '*** START OF PROGRAM IAAP300 ***'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_View.readNextRow("READ01")))
        {
            pnd_Check_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr.setValue("   FHEADER");                                                                                             //Natural: MOVE '   FHEADER' TO #WH-CNTRCT-PPCN-NBR
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde.setValue(0);                                                                                                       //Natural: MOVE 0 TO #WH-CNTRCT-PAYEE-CDE
        pnd_Work_Record_Header_Pnd_Wh_Record_Code.setValue(0);                                                                                                            //Natural: MOVE 0 TO #WH-RECORD-CODE
        pnd_Work_Record_Header_Pnd_Wh_Check_Date.setValue(pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N);                                                             //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WH-CHECK-DATE
        pnd_Work_Record_Header_Pnd_Wh_Process_Date.setValue(Global.getDATN());                                                                                            //Natural: MOVE *DATN TO #WH-PROCESS-DATE
        getWorkFiles().write(1, true, pnd_Work_Record_Header);                                                                                                            //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-HEADER
        pnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr.setValue("   SHEADER");                                                                                             //Natural: MOVE '   SHEADER' TO #WH-CNTRCT-PPCN-NBR
        pnd_Work_Record_Header_Pnd_Wh_Filler_1.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-1
        pnd_Work_Record_Header_Pnd_Wh_Filler_2.setValue(" ");                                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-2
        getWorkFiles().write(2, false, pnd_Work_Record_Header);                                                                                                           //Natural: WRITE WORK FILE 2 #WORK-RECORD-HEADER
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD PHYSICAL
        (
        "R1",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R1:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("R1")))
        {
            pnd_Count_Fund_Record_Reads.nadd(1);                                                                                                                          //Natural: ADD 1 TO #COUNT-FUND-RECORD-READS
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            pnd_Count2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #COUNT2
            if (condition(pnd_Count.greater(4999)))                                                                                                                       //Natural: IF #COUNT > 4999
            {
                getReports().write(0, "NUMBER OF RECORDS READ",pnd_Count2,Global.getTIMX());                                                                              //Natural: WRITE 'NUMBER OF RECORDS READ' #COUNT2 *TIMX
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Count.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.equals(" ")))                                                                                           //Natural: IF TIAA-CNTRCT-PPCN-NBR = ' '
            {
                pnd_Blank_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #BLANK-CTR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr);                                                                   //Natural: MOVE TIAA-CNTRCT-PPCN-NBR TO #W1-CNTRCT-PPCN-NBR
            pnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde);                                                                 //Natural: MOVE TIAA-CNTRCT-PAYEE-CDE TO #W1-CNTRCT-PAYEE-CDE
            pnd_Work_Record_1_Pnd_W1_Record_Code.setValue(30);                                                                                                            //Natural: MOVE 30 TO #W1-RECORD-CODE
            pnd_Work_Record_1_Pnd_W1_Cmpny_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde);                                                                               //Natural: MOVE TIAA-CMPNY-CDE TO #W1-CMPNY-CDE
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1G") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1S"))) //Natural: IF TIAA-FUND-CDE = '1' OR = '1G' OR = '1S'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde, MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT TIAA-FUND-CDE TO TIAA-FUND-CDE
                DbsUtil.examine(new ExamineSource(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE TIAA-FUND-CDE FOR ' ' REPLACE '0'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_1_Pnd_W1_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde);                                                                                 //Natural: MOVE TIAA-FUND-CDE TO #W1-FUND-CDE
            pnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt);                                                                           //Natural: MOVE TIAA-PER-IVC-AMT TO #W1-PER-IVC-AMT
            pnd_Work_Record_1_Pnd_W1_Rtb_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt);                                                                                   //Natural: MOVE TIAA-RTB-AMT TO #W1-RTB-AMT
            pnd_Work_Record_1_Pnd_W1_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                           //Natural: MOVE TIAA-TOT-PER-AMT TO #W1-TOT-PER-AMT
            pnd_Work_Record_1_Pnd_W1_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                           //Natural: MOVE TIAA-TOT-DIV-AMT TO #W1-TOT-DIV-AMT
            pnd_Work_Record_1_Pnd_W1_Old_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt);                                                                           //Natural: MOVE TIAA-OLD-PER-AMT TO #W1-OLD-PER-AMT
            //*  CHANGED FOLLOWING FROM 1:60 TO 1:250 4/08
            //*  CHANGED FOLLOWING FROM 1:250 TO 1:250 3/12
            pnd_Work_Record_1_Pnd_W1_Old_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt);                                                                           //Natural: MOVE TIAA-OLD-DIV-AMT TO #W1-OLD-DIV-AMT
            pnd_Work_Record_1_Pnd_W1_Rate_Cde.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue("*"));                                                     //Natural: MOVE TIAA-RATE-CDE ( * ) TO #W1-RATE-CDE ( * )
            pnd_Work_Record_1_Pnd_W1_Rate_Dte.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte.getValue("*"));                                                     //Natural: MOVE TIAA-RATE-DTE ( * ) TO #W1-RATE-DTE ( * )
            pnd_Work_Record_1_Pnd_W1_Per_Pay_Amt.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                               //Natural: MOVE TIAA-PER-PAY-AMT ( * ) TO #W1-PER-PAY-AMT ( * )
            pnd_Work_Record_1_Pnd_W1_Per_Div_Amt.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                               //Natural: MOVE TIAA-PER-DIV-AMT ( * ) TO #W1-PER-DIV-AMT ( * )
            pnd_Work_Record_1_Pnd_W1_Units_Cnt.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue("*"));                                                   //Natural: MOVE TIAA-UNITS-CNT ( * ) TO #W1-UNITS-CNT ( * )
            pnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                 //Natural: MOVE TIAA-RATE-FINAL-PAY-AMT ( * ) TO #W1-RATE-FINAL-PAY-AMT ( * )
            pnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt.getValue("*").setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                 //Natural: MOVE TIAA-RATE-FINAL-DIV-AMT ( * ) TO #W1-RATE-FINAL-DIV-AMT ( * )
            pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte);                                                                            //Natural: MOVE LST-TRANS-DTE TO #W1-LST-TRANS-DTE
            pnd_Work_Record_1_Pnd_W1_Tiaa_Xfr_Iss_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte);                                                                      //Natural: MOVE TIAA-XFR-ISS-DTE TO #W1-TIAA-XFR-ISS-DTE
            pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_In_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte);                                                                //Natural: MOVE TIAA-LST-XFR-IN-DTE TO #W1-TIAA-LST-XFR-IN-DTE
            pnd_Work_Record_1_Pnd_W1_Tiaa_Lst_Xfr_Out_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte);                                                              //Natural: MOVE TIAA-LST-XFR-OUT-DTE TO #W1-TIAA-LST-XFR-OUT-DTE
            getWorkFiles().write(1, true, pnd_Work_Record_1);                                                                                                             //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-1
            pnd_Count_Fund_Extract.nadd(1);                                                                                                                               //Natural: ADD 1 TO #COUNT-FUND-EXTRACT
            pnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr);                                                                   //Natural: MOVE TIAA-CNTRCT-PPCN-NBR TO #W2-CNTRCT-PPCN-NBR
            pnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde);                                                                 //Natural: MOVE TIAA-CNTRCT-PAYEE-CDE TO #W2-CNTRCT-PAYEE-CDE
            pnd_Work_Record_2_Pnd_W2_Record_Code.setValue(30);                                                                                                            //Natural: MOVE 30 TO #W2-RECORD-CODE
            pnd_Work_Record_2_Pnd_W2_Cmpny_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde);                                                                               //Natural: MOVE TIAA-CMPNY-CDE TO #W2-CMPNY-CDE
            pnd_Work_Record_2_Pnd_W2_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde);                                                                                 //Natural: MOVE TIAA-FUND-CDE TO #W2-FUND-CDE
            pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt);                                                                           //Natural: MOVE TIAA-PER-IVC-AMT TO #W2-PER-IVC-AMT
            pnd_Work_Record_2_Pnd_W2_Rtb_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt);                                                                                   //Natural: MOVE TIAA-RTB-AMT TO #W2-RTB-AMT
            pnd_Work_Record_2_Pnd_W2_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                           //Natural: MOVE TIAA-TOT-PER-AMT TO #W2-TOT-PER-AMT
            pnd_Work_Record_2_Pnd_W2_Tot_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                           //Natural: MOVE TIAA-TOT-DIV-AMT TO #W2-TOT-DIV-AMT
            pnd_Work_Record_2_Pnd_W2_Old_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt);                                                                           //Natural: MOVE TIAA-OLD-PER-AMT TO #W2-OLD-PER-AMT
            pnd_Work_Record_2_Pnd_W2_Old_Div_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt);                                                                           //Natural: MOVE TIAA-OLD-DIV-AMT TO #W2-OLD-DIV-AMT
            pnd_Work_Record_2_Pnd_W2_Rate_Cde.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO #W2-RATE-CDE
            pnd_Work_Record_2_Pnd_W2_Rate_Dte.setValue(0);                                                                                                                //Natural: MOVE 0 TO #W2-RATE-DTE
            pnd_Work_Record_2_Pnd_W2_Units_Cnt.setValue(0);                                                                                                               //Natural: MOVE 0 TO #W2-UNITS-CNT #W2-RATE-FINAL-PAY-AMT #W2-RATE-FINAL-DIV-AMT #W2-PER-PAY-AMT #W2-PER-DIV-AMT
            pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt.setValue(0);
            pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt.setValue(0);
            pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt.setValue(0);
            pnd_Work_Record_2_Pnd_W2_Per_Div_Amt.setValue(0);
            //*  MOVE C*TIAA-RATE-DATA-GRP TO #PER-TOT /* 4/08
            //*  F1.   FOR #I = 1 TO #PER-TOT          /* 4/08
            pnd_Work_Record_2_Pnd_W2_Per_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                                                 //Natural: ADD TIAA-PER-PAY-AMT ( * ) TO #W2-PER-PAY-AMT
            pnd_Work_Record_2_Pnd_W2_Per_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                                                 //Natural: ADD TIAA-PER-DIV-AMT ( * ) TO #W2-PER-DIV-AMT
            pnd_Work_Record_2_Pnd_W2_Units_Cnt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue("*"));                                                                     //Natural: ADD TIAA-UNITS-CNT ( * ) TO #W2-UNITS-CNT
            pnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                   //Natural: ADD TIAA-RATE-FINAL-PAY-AMT ( * ) TO #W2-RATE-FINAL-PAY-AMT
            pnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                   //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( * ) TO #W2-RATE-FINAL-DIV-AMT
            //*  END-FOR                               /* 4/08
            pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte);                                                                            //Natural: MOVE LST-TRANS-DTE TO #W2-LST-TRANS-DTE
            pnd_Work_Record_2_Pnd_W2_Tiaa_Xfr_Iss_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte);                                                                      //Natural: MOVE TIAA-XFR-ISS-DTE TO #W2-TIAA-XFR-ISS-DTE
            pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte);                                                                //Natural: MOVE TIAA-LST-XFR-IN-DTE TO #W2-TIAA-LST-XFR-IN-DTE
            pnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_Out_Dte.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte);                                                              //Natural: MOVE TIAA-LST-XFR-OUT-DTE TO #W2-TIAA-LST-XFR-OUT-DTE
            pnd_Work_Record_2_Pnd_W2_Filler.setValue(" ");                                                                                                                //Natural: MOVE ' ' TO #W2-FILLER
            getWorkFiles().write(2, false, pnd_Work_Record_2);                                                                                                            //Natural: WRITE WORK FILE 2 #WORK-RECORD-2
            pnd_Count_Fund_Extract_2.nadd(1);                                                                                                                             //Natural: ADD 1 TO #COUNT-FUND-EXTRACT-2
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr.setValue("99FTRAILER");                                                                                            //Natural: MOVE '99FTRAILER' TO #WT-CNTRCT-PPCN-NBR
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde.setValue(0);                                                                                                      //Natural: MOVE 0 TO #WT-CNTRCT-PAYEE-CDE
        pnd_Work_Record_Trailer_Pnd_Wt_Record_Code.setValue(0);                                                                                                           //Natural: MOVE 0 TO #WT-RECORD-CODE
        pnd_Work_Record_Trailer_Pnd_Wt_Check_Date.setValue(pnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N);                                                            //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WT-CHECK-DATE
        pnd_Work_Record_Trailer_Pnd_Wt_Process_Date.setValue(Global.getDATN());                                                                                           //Natural: MOVE *DATN TO #WT-PROCESS-DATE
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records.setValue(pnd_Count_Fund_Extract);                                                                                 //Natural: MOVE #COUNT-FUND-EXTRACT TO #WT-TOT-FUND-RECORDS
        getWorkFiles().write(1, true, pnd_Work_Record_Trailer);                                                                                                           //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-TRAILER
        pnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr.setValue("99STRAILER");                                                                                            //Natural: MOVE '99STRAILER' TO #WT-CNTRCT-PPCN-NBR
        pnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records.setValue(pnd_Count_Fund_Extract_2);                                                                               //Natural: MOVE #COUNT-FUND-EXTRACT-2 TO #WT-TOT-FUND-RECORDS
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_1.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-1
        pnd_Work_Record_Trailer_Pnd_Wt_Filler_2.setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-2
        getWorkFiles().write(2, false, pnd_Work_Record_Trailer);                                                                                                          //Natural: WRITE WORK FILE 2 #WORK-RECORD-TRAILER
        getReports().write(0, NEWLINE,"  FUND RECORD READS =========> ",pnd_Count_Fund_Record_Reads);                                                                     //Natural: WRITE / '  FUND RECORD READS =========> ' #COUNT-FUND-RECORD-READS
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"  FUND RECS W/BLANK CNTRCT #=> ",pnd_Blank_Ctr);                                                                                   //Natural: WRITE / '  FUND RECS W/BLANK CNTRCT #=> ' #BLANK-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"DETAIL FUND EXTRACT ====>",pnd_Count_Fund_Extract, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                        //Natural: WRITE ( 1 ) / 'DETAIL FUND EXTRACT ====>' #COUNT-FUND-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"SUMMARY FUND EXTRACT ===>",pnd_Count_Fund_Extract_2, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                      //Natural: WRITE ( 1 ) / 'SUMMARY FUND EXTRACT ===>' #COUNT-FUND-EXTRACT-2 ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"*** END OF PROGRAM IAAP300 ***");                                                                                                  //Natural: WRITE / '*** END OF PROGRAM IAAP300 ***'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"TOTAL RECORDS");                                                                    //Natural: WRITE ( 1 ) 29X 'TOTAL RECORDS'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"  EXTRACTED");                                                                      //Natural: WRITE ( 1 ) 29X '  EXTRACTED'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
