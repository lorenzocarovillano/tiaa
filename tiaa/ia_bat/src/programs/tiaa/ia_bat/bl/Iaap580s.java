/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:46 PM
**        * FROM NATURAL PROGRAM : Iaap580s
************************************************************
**        * FILE NAME            : Iaap580s.java
**        * CLASS NAME           : Iaap580s
**        * INSTANCE NAME        : Iaap580s
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP580S   SELECT NEW PAYEES ISSUED ONLINE INTERIM  *
*      DATE   -  01/95      PAYMENTS THAT DAY & SEPARATE THEM INTO   *
*    AUTHOR   -  ARI G.     2 WORK FILES. 1) ALL PAYEES (EXCLUDING   *
*                           NRA'S 2) ONLY NRA PAYEES                 *
* 03/25/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE AUDIT FILE.   *
*                   SC 032510.
* 03/30/13  J TINIO ADD REPORT OF ALL PAYMENTS FOR AML.              *
*                   SC 053013 FOR CHANGES
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap580s extends BLNatBase
{
    // Data Areas
    private LdaIaal580s ldaIaal580s;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Dated;
    private DbsField pnd_Name;
    private DbsField pnd_Pmt;
    private DbsField pnd_Corr_Addr;
    private DbsField pnd_Corr_Csz;
    private DbsField pnd_Pmt_Addr;
    private DbsField pnd_Pmt_Csz;
    private DbsField pnd_I;
    private DbsField pnd_Write_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal580s = new LdaIaal580s();
        registerRecord(ldaIaal580s);
        registerRecord(ldaIaal580s.getVw_audit_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Dated = localVariables.newFieldInRecord("pnd_Dated", "#DATED", FieldType.DATE);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 40);
        pnd_Pmt = localVariables.newFieldInRecord("pnd_Pmt", "#PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Corr_Addr = localVariables.newFieldArrayInRecord("pnd_Corr_Addr", "#CORR-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 7));
        pnd_Corr_Csz = localVariables.newFieldInRecord("pnd_Corr_Csz", "#CORR-CSZ", FieldType.STRING, 35);
        pnd_Pmt_Addr = localVariables.newFieldArrayInRecord("pnd_Pmt_Addr", "#PMT-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 7));
        pnd_Pmt_Csz = localVariables.newFieldInRecord("pnd_Pmt_Csz", "#PMT-CSZ", FieldType.STRING, 35);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal580s.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap580s() throws Exception
    {
        super("Iaap580s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  053013
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 150
        //*  053013 - START
        //*  053013 - END                                                                                                                                                 //Natural: AT TOP OF PAGE ( 1 )
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP580S ***");                                                                                                       //Natural: WRITE '*** START OF PROGRAM IAAP580S ***'
        if (Global.isEscape()) return;
        //* *   PERFORM #SETUP-SYSTEM-DATE-TIME
        //* *
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 3 IAA-PARM-CARD
        while (condition(getWorkFiles().read(3, ldaIaal580s.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        //*  032510
        //*  032510
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        //* *. READ  AUDIT-VIEW PHYSICAL
        ldaIaal580s.getVw_audit_View().startDatabaseRead                                                                                                                  //Natural: READ AUDIT-VIEW BY PAUDIT-STATUS-TIMESTAMP STARTING FROM #DATED
        (
        "RD",
        new Wc[] { new Wc("PAUDIT_STATUS_TIMESTAMP", ">=", pnd_Dated, WcType.BY) },
        new Oc[] { new Oc("PAUDIT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(ldaIaal580s.getVw_audit_View().readNextRow("RD")))
        {
            ldaIaal580s.getPnd_Audit_Reads().nadd(1);                                                                                                                     //Natural: ADD 1 TO #AUDIT-READS
            //*      DISPLAY PAUDIT-PPCN-NBR PAUDIT-PAYEE-CDE
            //*       PAUDIT-STATUS-TIMESTAMP (EM=YYYYMMDD)
            ldaIaal580s.getPnd_Fl_Date_Yyyymmdd_Alph().setValueEdited(ldaIaal580s.getAudit_View_Paudit_Status_Timestamp(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED PAUDIT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            //*  032510
            if (condition(ldaIaal580s.getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num().notEquals(ldaIaal580s.getIaa_Parm_Card_Pnd_Parm_Date_N())))                 //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                //*  032510
                if (true) break RD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RD. )
                //*  032510
            }                                                                                                                                                             //Natural: END-IF
            //*      ACCEPT IF #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N /* 032510
            //*   DISPLAY AUDIT-VIEW.PAUDIT-NEW-ACCT-IND
            ldaIaal580s.getPnd_Audit_Selects().nadd(1);                                                                                                                   //Natural: ADD 1 TO #AUDIT-SELECTS
            if (condition(ldaIaal580s.getAudit_View_Paudit_Pyee_Tax_Id_Nbr().equals(getZero())))                                                                          //Natural: IF PAUDIT-PYEE-TAX-ID-NBR = 0
            {
                                                                                                                                                                          //Natural: PERFORM #MOVE-TO-WORK-FILE-2
                sub_Pnd_Move_To_Work_File_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getWorkFiles().write(2, false, ldaIaal580s.getPnd_Work_Record_2());                                                                                       //Natural: WRITE WORK FILE 2 #WORK-RECORD-2
                ldaIaal580s.getPnd_Work_Record_Writes_2().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #MOVE-TO-WORK-FILE-1
                sub_Pnd_Move_To_Work_File_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getWorkFiles().write(1, false, ldaIaal580s.getPnd_Work_Record_1());                                                                                       //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
                ldaIaal580s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
            }                                                                                                                                                             //Natural: END-IF
            //*  053013
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "*** END OF PROGRAM IAAP580S ***");                                                                                                         //Natural: WRITE '*** END OF PROGRAM IAAP580S ***'
        if (Global.isEscape()) return;
        //*  053013 - START
        if (condition(pnd_Write_Cnt.equals(getZero())))                                                                                                                   //Natural: IF #WRITE-CNT = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"********** No payment records in this run **********");                                   //Natural: WRITE ( 1 ) /// '********** No payment records in this run **********'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"Total records written:",pnd_Write_Cnt, new ReportEditMask ("ZZ9"));                       //Natural: WRITE ( 1 ) /// 'Total records written:' #WRITE-CNT ( EM = ZZ9 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  053013 - END
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MOVE-TO-WORK-FILE-1
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MOVE-TO-WORK-FILE-2
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-SYSTEM-DATE-TIME
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* ********************************************************************
        //*  053013 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  053013 - END
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE READS ==========> ",ldaIaal580s.getPnd_Audit_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                    //Natural: WRITE '  AUDIT FILE READS ==========> ' #AUDIT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE SELECTS ========> ",ldaIaal580s.getPnd_Audit_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE '  AUDIT FILE SELECTS ========> ' #AUDIT-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK1 RECORDS WRITTEN =====> ",ldaIaal580s.getPnd_Work_Record_Writes_1(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                           //Natural: WRITE '  WORK1 RECORDS WRITTEN =====> ' #WORK-RECORD-WRITES-1 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK2 RECORDS WRITTEN =====> ",ldaIaal580s.getPnd_Work_Record_Writes_2(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                           //Natural: WRITE '  WORK2 RECORDS WRITTEN =====> ' #WORK-RECORD-WRITES-2 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Move_To_Work_File_1() throws Exception                                                                                                           //Natural: #MOVE-TO-WORK-FILE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Pyee_Tax_Id_Nbr().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A());                              //Natural: MOVE AUDIT-VIEW.#PAUDIT-PYEE-TAX-ID-NBR-A TO #W1-PAUDIT-PYEE-TAX-ID-NBR
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Payee_Code().setValue(ldaIaal580s.getAudit_View_Paudit_Payee_Cde());                                               //Natural: MOVE AUDIT-VIEW.PAUDIT-PAYEE-CDE TO #W1-PAUDIT-PAYEE-CODE
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Ppcn_Nbr().setValue(ldaIaal580s.getAudit_View_Paudit_Ppcn_Nbr());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-PPCN-NBR TO #W1-PAUDIT-PPCN-NBR
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Oper_Id().setValue(ldaIaal580s.getAudit_View_Paudit_Oper_Id());                                                    //Natural: MOVE AUDIT-VIEW.PAUDIT-OPER-ID TO #W1-PAUDIT-OPER-ID
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Oper_User_Grp().setValue(ldaIaal580s.getAudit_View_Paudit_Oper_User_Grp());                                        //Natural: MOVE AUDIT-VIEW.PAUDIT-OPER-USER-GRP TO #W1-PAUDIT-OPER-USER-GRP
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Sex_Code().setValue(ldaIaal580s.getAudit_View_Paudit_Surv_Bene_Sex_Cde());                                         //Natural: MOVE AUDIT-VIEW.PAUDIT-SURV-BENE-SEX-CDE TO #W1-PAUDIT-SEX-CODE
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Dob_Dte().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Dob_Dte_A());                                              //Natural: MOVE AUDIT-VIEW.#PAUDIT-DOB-DTE-A TO #W1-PAUDIT-DOB-DTE
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Eft_Acct_Nbr().setValue(ldaIaal580s.getAudit_View_Paudit_Eft_Acct_Nbr());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-EFT-ACCT-NBR TO #W1-PAUDIT-EFT-ACCT-NBR
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Type_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Type_Ind());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-TYPE-IND TO #W1-PAUDIT-TYPE-IND
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Type_Req_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Type_Req_Ind());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-TYPE-REQ-IND TO #W1-PAUDIT-TYPE-REQ-IND
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_New_Acct_Id().setValue(ldaIaal580s.getAudit_View_Paudit_New_Acct_Ind());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-NEW-ACCT-IND TO #W1-NEW-ACCT-ID
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Chk_Sav_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Chk_Sav_Ind());                                            //Natural: MOVE AUDIT-VIEW.PAUDIT-CHK-SAV-IND TO #W1-PAUDIT-CHK-SAV-IND
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Eft_Transit_Id().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Eft_Transit_Id_A());                                //Natural: MOVE AUDIT-VIEW.#PAUDIT-EFT-TRANSIT-ID-A TO #W1-PAUDIT-EFT-TRANSIT-ID
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_First().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_First());                                        //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-FIRST TO #W1-PAUDIT-PH-NAME-FIRST
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Middle().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_Middle());                                      //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-MIDDLE TO #W1-PAUDIT-PH-NAME-MIDDLE
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Ph_Name_Last().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_Last());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-LAST TO #W1-PAUDIT-PH-NAME-LAST
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Name_Address_Grp_Num().setValue(ldaIaal580s.getAudit_View_Count_Castpaudit_Name_Addrss_Grp());                     //Natural: MOVE AUDIT-VIEW.C*PAUDIT-NAME-ADDRSS-GRP TO #W1-PAUDIT-NAME-ADDRESS-GRP-NUM
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Name().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Name().getValue(1,":",2));                      //Natural: MOVE AUDIT-VIEW.PAUDIT-NAME ( 1:2 ) TO #W1-PAUDIT-NAME ( 1:2 )
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_1().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-1 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_2().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-2 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_3().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-3 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_4().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-4 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_5().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-5 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Lne_6().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 1:2 ) TO #W1-PAUDIT-ADDRSS-LNE-6 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_City().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(1,                //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 1:2 ) TO #W1-PAUDIT-ADDRSS-CITY ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_State().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 1:2 ) TO #W1-PAUDIT-ADDRSS-STATE ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_1_Pnd_W1_Paudit_Addrss_Zip().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(1,                  //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 1:2 ) TO #W1-PAUDIT-ADDRSS-ZIP ( 1:2 )
            ":",2));
    }
    private void sub_Pnd_Move_To_Work_File_2() throws Exception                                                                                                           //Natural: #MOVE-TO-WORK-FILE-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Pyee_Tax_Id_Nbr().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Pyee_Tax_Id_Nbr_A());                              //Natural: MOVE AUDIT-VIEW.#PAUDIT-PYEE-TAX-ID-NBR-A TO #W2-PAUDIT-PYEE-TAX-ID-NBR
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Payee_Code().setValue(ldaIaal580s.getAudit_View_Paudit_Payee_Cde());                                               //Natural: MOVE AUDIT-VIEW.PAUDIT-PAYEE-CDE TO #W2-PAUDIT-PAYEE-CODE
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Ppcn_Nbr().setValue(ldaIaal580s.getAudit_View_Paudit_Ppcn_Nbr());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-PPCN-NBR TO #W2-PAUDIT-PPCN-NBR
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Oper_Id().setValue(ldaIaal580s.getAudit_View_Paudit_Oper_Id());                                                    //Natural: MOVE AUDIT-VIEW.PAUDIT-OPER-ID TO #W2-PAUDIT-OPER-ID
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Oper_User_Grp().setValue(ldaIaal580s.getAudit_View_Paudit_Oper_User_Grp());                                        //Natural: MOVE AUDIT-VIEW.PAUDIT-OPER-USER-GRP TO #W2-PAUDIT-OPER-USER-GRP
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Sex_Code().setValue(ldaIaal580s.getAudit_View_Paudit_Surv_Bene_Sex_Cde());                                         //Natural: MOVE AUDIT-VIEW.PAUDIT-SURV-BENE-SEX-CDE TO #W2-PAUDIT-SEX-CODE
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Dob_Dte().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Dob_Dte_A());                                              //Natural: MOVE AUDIT-VIEW.#PAUDIT-DOB-DTE-A TO #W2-PAUDIT-DOB-DTE
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Dob_Ph_Name_Last().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580s.getAudit_View_Pnd_Paudit_Dob_Dte_A(),  //Natural: COMPRESS AUDIT-VIEW.#PAUDIT-DOB-DTE-A AUDIT-VIEW.PAUDIT-PH-NAME-LAST INTO #W2-DOB-PH-NAME-LAST LEAVING NO
            ldaIaal580s.getAudit_View_Paudit_Ph_Name_Last()));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Eft_Acct_Nbr().setValue(ldaIaal580s.getAudit_View_Paudit_Eft_Acct_Nbr());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-EFT-ACCT-NBR TO #W2-PAUDIT-EFT-ACCT-NBR
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Type_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Type_Ind());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-TYPE-IND TO #W2-PAUDIT-TYPE-IND
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Type_Req_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Type_Req_Ind());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-TYPE-REQ-IND TO #W2-PAUDIT-TYPE-REQ-IND
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_New_Acct_Id().setValue(ldaIaal580s.getAudit_View_Paudit_New_Acct_Ind());                                                  //Natural: MOVE AUDIT-VIEW.PAUDIT-NEW-ACCT-IND TO #W2-NEW-ACCT-ID
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Chk_Sav_Ind().setValue(ldaIaal580s.getAudit_View_Paudit_Chk_Sav_Ind());                                            //Natural: MOVE AUDIT-VIEW.PAUDIT-CHK-SAV-IND TO #W2-PAUDIT-CHK-SAV-IND
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Eft_Transit_Id().setValue(ldaIaal580s.getAudit_View_Pnd_Paudit_Eft_Transit_Id_A());                                //Natural: MOVE AUDIT-VIEW.#PAUDIT-EFT-TRANSIT-ID-A TO #W2-PAUDIT-EFT-TRANSIT-ID
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_First().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_First());                                        //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-FIRST TO #W2-PAUDIT-PH-NAME-FIRST
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Middle().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_Middle());                                      //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-MIDDLE TO #W2-PAUDIT-PH-NAME-MIDDLE
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Ph_Name_Last().setValue(ldaIaal580s.getAudit_View_Paudit_Ph_Name_Last());                                          //Natural: MOVE AUDIT-VIEW.PAUDIT-PH-NAME-LAST TO #W2-PAUDIT-PH-NAME-LAST
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Name_Address_Grp_Num().setValue(ldaIaal580s.getAudit_View_Count_Castpaudit_Name_Addrss_Grp());                     //Natural: MOVE AUDIT-VIEW.C*PAUDIT-NAME-ADDRSS-GRP TO #W2-PAUDIT-NAME-ADDRESS-GRP-NUM
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Name().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Name().getValue(1,":",2));                      //Natural: MOVE AUDIT-VIEW.PAUDIT-NAME ( 1:2 ) TO #W2-PAUDIT-NAME ( 1:2 )
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_1().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-1 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_2().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-2 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_3().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-3 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_4().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-4 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_5().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-5 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Lne_6().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 1:2 ) TO #W2-PAUDIT-ADDRSS-LNE-6 ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_City().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(1,                //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 1:2 ) TO #W2-PAUDIT-ADDRSS-CITY ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_State().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(1,              //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 1:2 ) TO #W2-PAUDIT-ADDRSS-STATE ( 1:2 )
            ":",2));
        ldaIaal580s.getPnd_Work_Record_2_Pnd_W2_Paudit_Addrss_Zip().getValue(1,":",2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(1,                  //Natural: MOVE AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 1:2 ) TO #W2-PAUDIT-ADDRSS-ZIP ( 1:2 )
            ":",2));
    }
    private void sub_Pnd_Setup_System_Date_Time() throws Exception                                                                                                        //Natural: #SETUP-SYSTEM-DATE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal580s.getPnd_Sys_Date().setValue(Global.getDATX());                                                                                                         //Natural: MOVE *DATX TO #SYS-DATE
        ldaIaal580s.getPnd_Sy_Date_Yymmdd_Alph().setValueEdited(ldaIaal580s.getPnd_Sys_Date(),new ReportEditMask("YYMMDD"));                                              //Natural: MOVE EDITED #SYS-DATE ( EM = YYMMDD ) TO #SY-DATE-YYMMDD-ALPH
        ldaIaal580s.getPnd_Sys_Time().setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO #SYS-TIME
        ldaIaal580s.getPnd_Sy_Time_Hhiiss_Alph().setValueEdited(ldaIaal580s.getPnd_Sys_Time(),new ReportEditMask("HHIISS"));                                              //Natural: MOVE EDITED #SYS-TIME ( EM = HHIISS ) TO #SY-TIME-HHIISS-ALPH
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal580s.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal580s.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032510
            ldaIaal580s.getIaa_Parm_Card_Pnd_Parm_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032510
        pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIaal580s.getIaa_Parm_Card_Pnd_Parm_Date());                                                            //Natural: MOVE EDITED #PARM-DATE TO #DATED ( EM = YYYYMMDD )
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Pmt.reset();                                                                                                                                                  //Natural: RESET #PMT #CORR-ADDR ( * ) #PMT-ADDR ( * ) #CORR-CSZ #PMT-CSZ #NAME
        pnd_Corr_Addr.getValue("*").reset();
        pnd_Pmt_Addr.getValue("*").reset();
        pnd_Corr_Csz.reset();
        pnd_Pmt_Csz.reset();
        pnd_Name.reset();
        pnd_Pmt.nadd(ldaIaal580s.getAudit_View_Paudit_Per_Pymnt().getValue("*"));                                                                                         //Natural: ADD AUDIT-VIEW.PAUDIT-PER-PYMNT ( * ) TO #PMT
        pnd_Pmt.nadd(ldaIaal580s.getAudit_View_Paudit_Per_Dvdnd().getValue("*"));                                                                                         //Natural: ADD AUDIT-VIEW.PAUDIT-PER-DVDND ( * ) TO #PMT
        pnd_Pmt.nadd(ldaIaal580s.getAudit_View_Paudit_Instllmnt_Gross().getValue("*"));                                                                                   //Natural: ADD AUDIT-VIEW.PAUDIT-INSTLLMNT-GROSS ( * ) TO #PMT
        if (condition(pnd_Pmt.equals(getZero())))                                                                                                                         //Natural: IF #PMT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal580s.getAudit_View_Paudit_Name().getValue(2).equals(" ")))                                                                                   //Natural: IF AUDIT-VIEW.PAUDIT-NAME ( 2 ) = ' '
        {
            ldaIaal580s.getAudit_View_Paudit_Name().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Name().getValue(1));                                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-NAME ( 2 ) := AUDIT-VIEW.PAUDIT-NAME ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(1));                              //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(1));                            //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 1 )
            ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(1));                                //Natural: ASSIGN AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(2).getSubstring(1,5),"N5") && DbsUtil.is(ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(1).getSubstring(1, //Natural: IF SUBSTR ( AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 2 ) ,1,5 ) IS ( N5 ) AND SUBSTR ( AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 1 ) ,1,5 ) IS ( N5 )
            5),"N5")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name.setValue(DbsUtil.compress(ldaIaal580s.getAudit_View_Paudit_Ph_Name_First(), ldaIaal580s.getAudit_View_Paudit_Ph_Name_Middle(), ldaIaal580s.getAudit_View_Paudit_Ph_Name_Last())); //Natural: COMPRESS AUDIT-VIEW.PAUDIT-PH-NAME-FIRST AUDIT-VIEW.PAUDIT-PH-NAME-MIDDLE AUDIT-VIEW.PAUDIT-PH-NAME-LAST INTO #NAME
        pnd_Corr_Csz.setValue(DbsUtil.compress(ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(1), ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(1),   //Natural: COMPRESS AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 1 ) AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 1 ) AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 1 ) TO #CORR-CSZ
            ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(1)));
        pnd_Pmt_Csz.setValue(DbsUtil.compress(ldaIaal580s.getAudit_View_Paudit_Addrss_City().getValue(2), ldaIaal580s.getAudit_View_Paudit_Addrss_State().getValue(2),    //Natural: COMPRESS AUDIT-VIEW.PAUDIT-ADDRSS-CITY ( 2 ) AUDIT-VIEW.PAUDIT-ADDRSS-STATE ( 2 ) AUDIT-VIEW.PAUDIT-ADDRSS-ZIP ( 2 ) TO #PMT-CSZ
            ldaIaal580s.getAudit_View_Paudit_Addrss_Zip().getValue(2)));
        pnd_Corr_Addr.getValue(1).setValue(ldaIaal580s.getAudit_View_Paudit_Name().getValue(1));                                                                          //Natural: ASSIGN #CORR-ADDR ( 1 ) := AUDIT-VIEW.PAUDIT-NAME ( 1 )
        pnd_Corr_Addr.getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 1 )
        pnd_Corr_Addr.getValue(3).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 3 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 1 )
        pnd_Corr_Addr.getValue(4).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 4 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 1 )
        pnd_Corr_Addr.getValue(5).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 5 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 1 )
        pnd_Corr_Addr.getValue(6).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 6 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 1 )
        pnd_Corr_Addr.getValue(7).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(1));                                                                  //Natural: ASSIGN #CORR-ADDR ( 7 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 1 )
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 7
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
        {
            if (condition(pnd_Corr_Addr.getValue(pnd_I).equals(" ")))                                                                                                     //Natural: IF #CORR-ADDR ( #I ) = ' '
            {
                pnd_Corr_Addr.getValue(pnd_I).setValue(pnd_Corr_Csz);                                                                                                     //Natural: ASSIGN #CORR-ADDR ( #I ) := #CORR-CSZ
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Pmt_Addr.getValue(1).setValue(ldaIaal580s.getAudit_View_Paudit_Name().getValue(2));                                                                           //Natural: ASSIGN #PMT-ADDR ( 1 ) := AUDIT-VIEW.PAUDIT-NAME ( 2 )
        pnd_Pmt_Addr.getValue(2).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_1().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 2 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-1 ( 2 )
        pnd_Pmt_Addr.getValue(3).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_2().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 3 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-2 ( 2 )
        pnd_Pmt_Addr.getValue(4).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_3().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 4 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-3 ( 2 )
        pnd_Pmt_Addr.getValue(5).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_4().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 5 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-4 ( 2 )
        pnd_Pmt_Addr.getValue(6).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_5().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 6 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-5 ( 2 )
        pnd_Pmt_Addr.getValue(7).setValue(ldaIaal580s.getAudit_View_Paudit_Addrss_Lne_6().getValue(2));                                                                   //Natural: ASSIGN #PMT-ADDR ( 7 ) := AUDIT-VIEW.PAUDIT-ADDRSS-LNE-6 ( 2 )
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 7
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
        {
            if (condition(pnd_Pmt_Addr.getValue(pnd_I).equals(" ")))                                                                                                      //Natural: IF #PMT-ADDR ( #I ) = ' '
            {
                pnd_Pmt_Addr.getValue(pnd_I).setValue(pnd_Pmt_Csz);                                                                                                       //Natural: ASSIGN #PMT-ADDR ( #I ) := #PMT-CSZ
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().display(1, "Contract",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Contract' AUDIT-VIEW.PAUDIT-PPCN-NBR 'Py' AUDIT-VIEW.PAUDIT-PAYEE-CDE ( EM = 99 ) 'Name' #NAME 'Correspondence Address' #CORR-ADDR ( * ) 'Payment Address' #PMT-ADDR ( * ) 'Total Pymnt' #PMT ( EM = ZZZZ,ZZ9.99 )
        		ldaIaal580s.getAudit_View_Paudit_Ppcn_Nbr(),"Py",
        		ldaIaal580s.getAudit_View_Paudit_Payee_Cde(), new ReportEditMask ("99"),"Name",
        		pnd_Name,"Correspondence Address",
        		pnd_Corr_Addr.getValue("*"),"Payment Address",
        		pnd_Pmt_Addr.getValue("*"),"Total Pymnt",
        		pnd_Pmt, new ReportEditMask ("ZZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Write_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #WRITE-CNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(37),"Daily IA Death Claims Payments Report for ",pnd_Dated,  //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 37X 'Daily IA Death Claims Payments Report for ' #DATED ( EM = MM/DD/YYYY ) 37X *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *DATX ( EM = MM/DD/YYYY ) 120X *TIMX ( EM = HH:II:SS )
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(37),getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS"));
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=150");

        getReports().setDisplayColumns(1, "Contract",
        		ldaIaal580s.getAudit_View_Paudit_Ppcn_Nbr(),"Py",
        		ldaIaal580s.getAudit_View_Paudit_Payee_Cde(), new ReportEditMask ("99"),"Name",
        		pnd_Name,"Correspondence Address",
        		pnd_Corr_Addr,"Payment Address",
        		pnd_Pmt_Addr,"Total Pymnt",
        		pnd_Pmt, new ReportEditMask ("ZZZZ,ZZ9.99"));
    }
}
