/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:05 PM
**        * FROM NATURAL PROGRAM : Iaap304
************************************************************
**        * FILE NAME            : Iaap304.java
**        * CLASS NAME           : Iaap304
**        * INSTANCE NAME        : Iaap304
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP304    READS IA EXTRACT FLAT FILE             *
*      DATE     -  5/98       & CREATES EXTRACT FOR PBI TAPE         *
*                             (DTH VERIFICATION)                     *
*                             RAY. M                                 *
* 03/19/2010 O. SOTTO  SSN OF 2ND ANNT MUST BE LOADED. SC 031910.    *
* 05/20/2010 O. SOTTO  PROD FIX. IF DOD NOT ZERO, BYPASS RECORD.     *
*                      ALSO ADDED JOINT OPTIONS 18, 51-57. SC 052010.*
* 03/30/2011 O. SOTTO  INCLUDE ORIGIN CODE IN THE EXTRACT. SC 033011.*
* 05/07/2012 J. TINIO  RATE BASE EXPANSION                 SC 050712.*
* 11/24/2015 J. TINIO  NEW INPUT FILE AS A RESULT OF SCHEDULER       *
*                      CHANGE. THIS WILL RUN TWICE MONTHLY, 8TH AND  *
*                      22ND ON BUSINESS DAYS ONLY.         SC 112415 *
* 01/31/2016 J. TINIO  COR/NAAD SUNSET. REMOVE SWITCH OF PAYEE CODE
*                      WHEN DEATH OF SECOND OCCURS AND SURVIVOR IS
*                      FIRST ANNUITANT. SC 013116                    *
* 04/2017    O. SOTTO  PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap304 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Cde;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orgn;
    private DbsField pnd_Input_Record_Pnd_F1;
    private DbsField pnd_Input_Record_Pnd_Issue_Dte;
    private DbsField pnd_Input_Record_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Record_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Record_Pnd_Crrncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Type_Cde;
    private DbsField pnd_Input_Record_Pnd_F2;
    private DbsField pnd_Input_Record_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Record_Pnd_F3;
    private DbsField pnd_Input_Record_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Iss_St;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dob;
    private DbsField pnd_Input_Record_Pnd_F4;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dob;
    private DbsField pnd_Input_Record_Pnd_F6;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dod;
    private DbsField pnd_Input_Record_Pnd_F71;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn;
    private DbsField pnd_Input_Record_Pnd_F72;
    private DbsField pnd_Input_Record_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Record_Pnd_Ppg_Cde_5;
    private DbsField pnd_Input_Record_Pnd_F8;
    private DbsField pnd_Input_Record_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Record_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Record_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Record_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Record_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Record_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_F13;
    private DbsField pnd_Input_Record_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Record_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input_Record_Pnd_F14;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_F15;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Record_Pnd_F16;
    private DbsField pnd_Input_Record_Pnd_Cash_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Record_Pnd_F17;
    private DbsField pnd_Input_Record_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Record_Pnd_F18;
    private DbsField pnd_Input_Record_Pnd_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_F19;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Xref;
    private DbsField pnd_Input_Record_Pnd_F20;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_Input_Record_Pnd_Hold_Cde;
    private DbsField pnd_Input_Record_Pnd_Pend_Dte;
    private DbsField pnd_Input_Record_Pnd_F21;
    private DbsField pnd_Input_Record_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Record_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Record_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Record_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Record_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Record_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Record_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Record_Pnd_State_Cde;
    private DbsField pnd_Input_Record_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Record_Pnd_Local_Cde;
    private DbsField pnd_Input_Record_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Record_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Record_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Record_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Record_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Record_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Record_Pnd_F24;
    private DbsField pnd_Input_Record_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_5;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_6;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2;
    private DbsField pnd_W_Page_Ctr;

    private DbsGroup pnd_Extract_Rec;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Cntrct_Nbr;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Payee;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Ssn;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Last_Nme;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Fst_Nme;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Optn;
    private DbsField pnd_Extract_Rec_Pnd_Ext_X_Ref_1st;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Dob_1st;
    private DbsField pnd_Extract_Rec_Pnd_Ext_X_Ref_2nd;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Dob_2nd;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Bnfcry_Xref;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Pend_Cde;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Pin;
    private DbsField pnd_Extract_Rec_Pnd_Ext_Orgn_Cde;
    private DbsField pnd_Sve_2nd_Ssn;
    private DbsField pnd_Sve_Dod_1st;
    private DbsField pnd_Sve_Dod_2nd;
    private DbsField pnd_Tot_Trans_Out;
    private DbsField pnd_Tot_2_3rds_Opt;
    private DbsField pnd_Tot_Other_Opt;
    private DbsField pnd_Tot_Paye_02_Gen;
    private DbsField pnd_Tot_Benies_Gen;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Payee = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record_Pnd_Cntrct_Payee);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Record_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Cde", "#RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record_353 = pnd_Input_Record.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", 
            FieldType.STRING, 1, new DbsArrayController(1, 353));

        pnd_Input_Record__R_Field_2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_353);
        pnd_Input_Record_Pnd_Header_Chk_Dte = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_353);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Orgn = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orgn", "#CNTRCT-ORGN", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_F1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Issue_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_1st_Due_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_1st_Pd_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_Crrncy_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Record_Pnd_Type_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_F2 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Pnsn_Pln_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_F3 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Iss_St = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_1st_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Xref", "#CNTRCT-1ST-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_1st_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dob", "#CNTRCT-1ST-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_F4 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_1st_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dod", "#CNTRCT-1ST-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Xref", "#CNTRCT-2ND-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dob", "#CNTRCT-2ND-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_F6 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dod", "#CNTRCT-2ND-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_F71 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F71", "#F71", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn", "#CNTRCT-2ND-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_F72 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F72", "#F72", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Div_Coll_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Ppg_Cde_5 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Ppg_Cde_5", "#PPG-CDE-5", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_F8 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Record_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Roth_Ssnng_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Plan_Nmbr = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Input_Record_Pnd_Tax_Exmpt_Ind = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Orig_Ownr_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 
            8);
        pnd_Input_Record_Pnd_Orig_Ownr_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 
            8);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_353);
        pnd_Input_Record_Pnd_Cpr_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Record_Pnd_F13 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Record_Pnd_Ctznshp_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 
            3);
        pnd_Input_Record_Pnd_Rsdncy_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_F14 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr", "#PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_F15 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Actvty_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Trmnte_Rsn = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_F16 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cash_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_F17 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Record_Pnd_Rcvry_Type_Ind = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Amt = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_F18 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Record_Pnd_Mode_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_F19 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Final_Pay_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Final_Pay_Dte", "#FINAL-PAY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Bnfcry_Xref = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Xref", "#BNFCRY-XREF", FieldType.STRING, 
            9);
        pnd_Input_Record_Pnd_F20 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F20", "#F20", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_Pend_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Hold_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Pend_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_F21 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Curr_Dist_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cmbne_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 
            12);
        pnd_Input_Record_Pnd_Spirt_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Spirt_Amt = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Input_Record_Pnd_Spirt_Srce = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Spirt_Arr_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 
            4);
        pnd_Input_Record_Pnd_Spirt_Prcss_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Fed_Tax_Amt = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_State_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_State_Tax_Amt = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Local_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Local_Tax_Amt = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Lst_Chnge_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_Cpr_Xfr_Term_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cpr_Lgl_Res_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", 
            FieldType.DATE);
        pnd_Input_Record_Pnd_Rllvr_Cntrct_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Rllvr_Ivc_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Rllvr_Elgble_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input_Record__R_Field_4.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rllvr_Dstrbtng_Irc_Cde", 
            "#RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Record_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_F24 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Roth_Dsblty_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Curr_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Curr_Check_Date_Ccyymmdd", "#CURR-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_5", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy = pnd_Curr_Check_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy", 
            "#CURR-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm", 
            "#CURR-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd = pnd_Curr_Check_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd", 
            "#CURR-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_6", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm", 
            "#CURR-CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2 = pnd_Curr_Check_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2", 
            "#CURR-CHECK-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 2);

        pnd_Extract_Rec = localVariables.newGroupInRecord("pnd_Extract_Rec", "#EXTRACT-REC");
        pnd_Extract_Rec_Pnd_Ext_Cntrct_Nbr = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Cntrct_Nbr", "#EXT-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Extract_Rec_Pnd_Ext_Payee = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Payee", "#EXT-PAYEE", FieldType.NUMERIC, 2);
        pnd_Extract_Rec_Pnd_Ext_Ssn = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Ssn", "#EXT-SSN", FieldType.NUMERIC, 9);
        pnd_Extract_Rec_Pnd_Ext_Last_Nme = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Last_Nme", "#EXT-LAST-NME", FieldType.STRING, 12);
        pnd_Extract_Rec_Pnd_Ext_Fst_Nme = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Fst_Nme", "#EXT-FST-NME", FieldType.STRING, 10);
        pnd_Extract_Rec_Pnd_Ext_Optn = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Optn", "#EXT-OPTN", FieldType.NUMERIC, 2);
        pnd_Extract_Rec_Pnd_Ext_X_Ref_1st = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_X_Ref_1st", "#EXT-X-REF-1ST", FieldType.STRING, 9);
        pnd_Extract_Rec_Pnd_Ext_Dob_1st = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Dob_1st", "#EXT-DOB-1ST", FieldType.NUMERIC, 8);
        pnd_Extract_Rec_Pnd_Ext_X_Ref_2nd = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_X_Ref_2nd", "#EXT-X-REF-2ND", FieldType.STRING, 9);
        pnd_Extract_Rec_Pnd_Ext_Dob_2nd = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Dob_2nd", "#EXT-DOB-2ND", FieldType.NUMERIC, 8);
        pnd_Extract_Rec_Pnd_Ext_Bnfcry_Xref = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Bnfcry_Xref", "#EXT-BNFCRY-XREF", FieldType.STRING, 
            9);
        pnd_Extract_Rec_Pnd_Ext_Pend_Cde = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Pend_Cde", "#EXT-PEND-CDE", FieldType.STRING, 1);
        pnd_Extract_Rec_Pnd_Ext_Pin = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Pin", "#EXT-PIN", FieldType.NUMERIC, 12);
        pnd_Extract_Rec_Pnd_Ext_Orgn_Cde = pnd_Extract_Rec.newFieldInGroup("pnd_Extract_Rec_Pnd_Ext_Orgn_Cde", "#EXT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Sve_2nd_Ssn = localVariables.newFieldInRecord("pnd_Sve_2nd_Ssn", "#SVE-2ND-SSN", FieldType.NUMERIC, 9);
        pnd_Sve_Dod_1st = localVariables.newFieldInRecord("pnd_Sve_Dod_1st", "#SVE-DOD-1ST", FieldType.NUMERIC, 8);
        pnd_Sve_Dod_2nd = localVariables.newFieldInRecord("pnd_Sve_Dod_2nd", "#SVE-DOD-2ND", FieldType.NUMERIC, 8);
        pnd_Tot_Trans_Out = localVariables.newFieldInRecord("pnd_Tot_Trans_Out", "#TOT-TRANS-OUT", FieldType.NUMERIC, 9);
        pnd_Tot_2_3rds_Opt = localVariables.newFieldInRecord("pnd_Tot_2_3rds_Opt", "#TOT-2-3RDS-OPT", FieldType.NUMERIC, 9);
        pnd_Tot_Other_Opt = localVariables.newFieldInRecord("pnd_Tot_Other_Opt", "#TOT-OTHER-OPT", FieldType.NUMERIC, 9);
        pnd_Tot_Paye_02_Gen = localVariables.newFieldInRecord("pnd_Tot_Paye_02_Gen", "#TOT-PAYE-02-GEN", FieldType.NUMERIC, 9);
        pnd_Tot_Benies_Gen = localVariables.newFieldInRecord("pnd_Tot_Benies_Gen", "#TOT-BENIES-GEN", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap304() throws Exception
    {
        super("Iaap304");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP304", onError);
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: AT TOP OF PAGE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            short decideConditionsMet312 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RECORD-CDE;//Natural: VALUE 00
            if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(0))))
            {
                decideConditionsMet312++;
                //*  112415
                if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER")))                                                                                 //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER'
                {
                    //*        #CURR-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Curr_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_Header_Chk_Dte);                                                                           //Natural: ASSIGN #CURR-CHECK-DATE-CCYYMMDD := #HEADER-CHK-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(10))))
            {
                decideConditionsMet312++;
                //*  031910
                //*  033011
                pnd_Extract_Rec.reset();                                                                                                                                  //Natural: RESET #EXTRACT-REC
                pnd_Extract_Rec_Pnd_Ext_Optn.setValue(pnd_Input_Record_Pnd_Cntrct_Optn_Cde);                                                                              //Natural: ASSIGN #EXT-OPTN := #CNTRCT-OPTN-CDE
                pnd_Extract_Rec_Pnd_Ext_X_Ref_1st.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Xref);                                                                         //Natural: ASSIGN #EXT-X-REF-1ST := #CNTRCT-1ST-XREF
                pnd_Extract_Rec_Pnd_Ext_Dob_1st.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dob);                                                                            //Natural: ASSIGN #EXT-DOB-1ST := #CNTRCT-1ST-DOB
                pnd_Extract_Rec_Pnd_Ext_X_Ref_2nd.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Xref);                                                                         //Natural: ASSIGN #EXT-X-REF-2ND := #CNTRCT-2ND-XREF
                pnd_Extract_Rec_Pnd_Ext_Dob_2nd.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dob);                                                                            //Natural: ASSIGN #EXT-DOB-2ND := #CNTRCT-2ND-DOB
                pnd_Extract_Rec_Pnd_Ext_Cntrct_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                        //Natural: ASSIGN #EXT-CNTRCT-NBR := #CNTRCT-PPCN-NBR
                pnd_Sve_Dod_1st.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dod);                                                                                            //Natural: ASSIGN #SVE-DOD-1ST := #CNTRCT-1ST-DOD
                pnd_Sve_Dod_2nd.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dod);                                                                                            //Natural: ASSIGN #SVE-DOD-2ND := #CNTRCT-2ND-DOD
                pnd_Sve_2nd_Ssn.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn);                                                                                       //Natural: ASSIGN #SVE-2ND-SSN := #CNTRCT-2ND-ANNT-SSN
                pnd_Extract_Rec_Pnd_Ext_Orgn_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Orgn);                                                                              //Natural: ASSIGN #EXT-ORGN-CDE := #CNTRCT-ORGN
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(20))))
            {
                decideConditionsMet312++;
                //*  052010
                if (condition(pnd_Input_Record_Pnd_Cntrct_Actvty_Cde.equals(9) || (pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(1) && pnd_Sve_Dod_1st.greater(getZero())))) //Natural: IF #CNTRCT-ACTVTY-CDE = 9 OR ( #CNTRCT-PAYEE-CDE = 01 AND #SVE-DOD-1ST GT 0 )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  052010 START
                if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(2)))                                                                                           //Natural: IF #CNTRCT-PAYEE-CDE = 02
                {
                    if (condition((pnd_Sve_Dod_1st.equals(getZero()) && pnd_Sve_Dod_2nd.greater(getZero())) || pnd_Sve_Dod_2nd.equals(getZero())))                        //Natural: IF ( #SVE-DOD-1ST = 0 AND #SVE-DOD-2ND GT 0 ) OR #SVE-DOD-2ND EQ 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  052010 END
                    //*  033011
                }                                                                                                                                                         //Natural: END-IF
                pnd_Extract_Rec_Pnd_Ext_Payee.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                            //Natural: ASSIGN #EXT-PAYEE := #CNTRCT-PAYEE-CDE
                pnd_Extract_Rec_Pnd_Ext_Ssn.setValue(pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr);                                                                            //Natural: ASSIGN #EXT-SSN := #PRTCPNT-TAX-ID-NBR
                pnd_Extract_Rec_Pnd_Ext_Bnfcry_Xref.setValue(pnd_Input_Record_Pnd_Bnfcry_Xref);                                                                           //Natural: ASSIGN #EXT-BNFCRY-XREF := #BNFCRY-XREF
                pnd_Extract_Rec_Pnd_Ext_Pend_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Pend_Cde);                                                                          //Natural: ASSIGN #EXT-PEND-CDE := #CNTRCT-PEND-CDE
                pnd_Extract_Rec_Pnd_Ext_Pin.setValue(pnd_Input_Record_Pnd_Cpr_Id_Nbr);                                                                                    //Natural: ASSIGN #EXT-PIN := #CPR-ID-NBR
                //*  052010
                short decideConditionsMet357 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #EXT-OPTN;//Natural: VALUES 03, 04, 07, 08, 51, 52, 53, 54, 55, 56, 57,10:18
                if (condition((pnd_Extract_Rec_Pnd_Ext_Optn.equals(3) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(4) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(7) 
                    || pnd_Extract_Rec_Pnd_Ext_Optn.equals(8) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(51) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(52) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(53) 
                    || pnd_Extract_Rec_Pnd_Ext_Optn.equals(54) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(55) || pnd_Extract_Rec_Pnd_Ext_Optn.equals(56) || 
                    pnd_Extract_Rec_Pnd_Ext_Optn.equals(57) || (pnd_Extract_Rec_Pnd_Ext_Optn.greaterOrEqual(10) && pnd_Extract_Rec_Pnd_Ext_Optn.lessOrEqual(18)))))
                {
                    decideConditionsMet357++;
                    if (condition(pnd_Extract_Rec_Pnd_Ext_Payee.equals(1)))                                                                                               //Natural: IF #EXT-PAYEE = 01
                    {
                        pnd_Tot_Other_Opt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOT-OTHER-OPT
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                        sub_Write_Tran_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Extract_Rec_Pnd_Ext_Payee.setValue(2);                                                                                                        //Natural: ASSIGN #EXT-PAYEE := 02
                        pnd_Extract_Rec_Pnd_Ext_Ssn.reset();                                                                                                              //Natural: RESET #EXT-SSN
                        //*  031910
                        if (condition(pnd_Sve_Dod_2nd.equals(getZero())))                                                                                                 //Natural: IF #SVE-DOD-2ND = 0
                        {
                            pnd_Extract_Rec_Pnd_Ext_Ssn.setValue(pnd_Sve_2nd_Ssn);                                                                                        //Natural: ASSIGN #EXT-SSN := #SVE-2ND-SSN
                            pnd_Tot_Paye_02_Gen.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOT-PAYE-02-GEN
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                            sub_Write_Tran_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.equals(2)))                                                                                       //Natural: IF #CNTRCT-PAYEE-CDE = 02
                    {
                        pnd_Tot_Other_Opt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOT-OTHER-OPT
                        if (condition(pnd_Sve_Dod_1st.equals(getZero())))                                                                                                 //Natural: IF #SVE-DOD-1ST = 0
                        {
                            //*              #EXT-PAYEE     :=  01              /* 013116
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                            sub_Write_Tran_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  052010
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Extract_Rec_Pnd_Ext_Ssn.setValue(pnd_Sve_2nd_Ssn);                                                                                        //Natural: ASSIGN #EXT-SSN := #SVE-2ND-SSN
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                            sub_Write_Tran_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(2)))                                                                                      //Natural: IF #CNTRCT-PAYEE-CDE GT 02
                    {
                        pnd_Tot_Benies_Gen.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOT-BENIES-GEN
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                        sub_Write_Tran_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    if (condition(pnd_Input_Record_Pnd_Cntrct_Payee_Cde.greater(2)))                                                                                      //Natural: IF #CNTRCT-PAYEE-CDE GT 02
                    {
                        pnd_Tot_Benies_Gen.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOT-BENIES-GEN
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Tot_Other_Opt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOT-OTHER-OPT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ALL OTHER OPTIONS
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE," TOTAL JOINT-LIFE PAYEE 02 RECORDS GENERATED   ");                                                            //Natural: WRITE / ' TOTAL JOINT-LIFE PAYEE 02 RECORDS GENERATED   '
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE," BOTH ANNUITANTS ARE ALIVE ------------------->",pnd_Tot_Paye_02_Gen, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));            //Natural: WRITE ' BOTH ANNUITANTS ARE ALIVE ------------------->' #TOT-PAYE-02-GEN ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE," TOTAL CONTRACTS ALL OTHER OPTIONS------------>",pnd_Tot_Other_Opt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));              //Natural: WRITE ' TOTAL CONTRACTS ALL OTHER OPTIONS------------>' #TOT-OTHER-OPT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE," TOTAL BENEFICIARY RECORDS WRITTEN------------>",pnd_Tot_Benies_Gen, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));             //Natural: WRITE ' TOTAL BENEFICIARY RECORDS WRITTEN------------>' #TOT-BENIES-GEN ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE," TOTAL TRANSACTIONS WRITTEN------------------->",pnd_Tot_Trans_Out, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));              //Natural: WRITE ' TOTAL TRANSACTIONS WRITTEN------------------->' #TOT-TRANS-OUT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        //*  ----------------------------------------------                                                                                                               //Natural: ON ERROR
        //*  WRITE TRAN RECORD
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAN-RECORD
    }
    private void sub_Write_Tran_Record() throws Exception                                                                                                                 //Natural: WRITE-TRAN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(2, false, pnd_Extract_Rec);                                                                                                                  //Natural: WRITE WORK FILE 2 #EXTRACT-REC
        pnd_Tot_Trans_Out.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-TRANS-OUT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(0, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN RECORDS SELECTED FOR PBI PROCESSING          ",new  //Natural: WRITE NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN RECORDS SELECTED FOR PBI PROCESSING          ' 4X 'PAGE: ' #W-PAGE-CTR ( EM = Z9 )
                        ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("Z9"));
                    getReports().write(0, ReportOption.NOTITLE,"RUN DATE",Global.getDATU(),"                                                      ");                     //Natural: WRITE 'RUN DATE' *DATU '                                                      '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE," ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                  //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
