/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:14 PM
**        * FROM NATURAL PROGRAM : Iaap605
************************************************************
**        * FILE NAME            : Iaap605.java
**        * CLASS NAME           : Iaap605
**        * INSTANCE NAME        : Iaap605
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP605    THIS PROGRAM WILL PROCESS THE RULES FOR  *
*      DATE   -  07/11      THE IA PROSPECTUS EXTRACT                *
*    AUTHOR   -  TED P.                                              *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap605 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler;
    private DbsField iaa_Tiaa_Fund_Rcrd_Company;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;

    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;

    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;

    private DbsGroup pnd_Contract_Record_In;
    private DbsField pnd_Contract_Record_In_Pnd_Event_Ind_In;
    private DbsField pnd_Contract_Record_In_Pnd_Plan_Num_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ssn_In;
    private DbsField pnd_Contract_Record_In_Pnd_Fund_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Eff_Date_In;
    private DbsField pnd_Contract_Record_In_Pnd_Inverse_Date_In;
    private DbsField pnd_Contract_Record_In_Pnd_Contract_In;
    private DbsField pnd_Contract_Record_In_Pnd_Payee_Cde_In;
    private DbsField pnd_Contract_Record_In_Pnd_Cusip_In;
    private DbsField pnd_Contract_Record_In_Pnd_Plan_Type_In;
    private DbsField pnd_Contract_Record_In_Pnd_Source_App_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trans_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Act_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trns_Act_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Pend_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_First_Ann_Dod_In;
    private DbsField pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trans_Dte_In;
    private DbsField pnd_Contract_Record_In_Pnd_Fund_Id_In_Work;

    private DbsGroup pnd_Contract_Record_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Event_Ind_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Plan_Num_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ssn_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Fund_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Eff_Date_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Inverse_Date_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Contract_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Payee_Cde_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Cusip_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Plan_Type_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Source_App_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trans_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Act_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trns_Act_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Pend_Code_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out;
    private DbsField pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out;

    private DbsGroup pnd_Tot_Report_Rec;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written;
    private DbsField pnd_Tot_Report_Rec_Pnd_Tot_Diff;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Work_Before_Key;

    private DbsGroup pnd_Work_Before_Key__R_Field_2;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Fund_Bef;
    private DbsField pnd_Work_After_Key;

    private DbsGroup pnd_Work_After_Key__R_Field_3;
    private DbsField pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Fund_Aft;
    private DbsField pnd_Work_Cpr_Before_Key;

    private DbsGroup pnd_Work_Cpr_Before_Key__R_Field_4;
    private DbsField pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef;
    private DbsField pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef;
    private DbsField pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef;
    private DbsField pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef;
    private DbsField pnd_Work_Cpr_After_Key;

    private DbsGroup pnd_Work_Cpr_After_Key__R_Field_5;
    private DbsField pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft;
    private DbsField pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft;
    private DbsField pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft;
    private DbsField pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft;
    private DbsField pnd_Work_Contrct_Bef_Key;

    private DbsGroup pnd_Work_Contrct_Bef_Key__R_Field_6;
    private DbsField pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id;
    private DbsField pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef;
    private DbsField pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte;
    private DbsField pnd_Work_Contrct_Aft_Key;

    private DbsGroup pnd_Work_Contrct_Aft_Key__R_Field_7;
    private DbsField pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id;
    private DbsField pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft;
    private DbsField pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte;
    private DbsField pnd_Work_Fund_Id;

    private DbsGroup pnd_Work_Fund_Id__R_Field_8;
    private DbsField pnd_Work_Fund_Id_Pnd_Fund_Id_Fix;
    private DbsField pnd_Work_Fund_Id_Pnd_Fund_Id_Rest;

    private DbsGroup pnd_Fnd_Rec;
    private DbsField pnd_Fnd_Rec_Pnd_Fnd_Rec_Cusip;
    private DbsField pnd_Fnd_Rec_Pnd_Fnd_Rec_Num_Fund_Cde;
    private DbsField pnd_Max_Nbr_Of_Ext_Fnds;

    private DbsGroup pnd_Fnd_Tab_Rec;
    private DbsField pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip;
    private DbsField pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde;
    private DbsField pnd_Notfnd;
    private DbsField pnd_Hold_Key;

    private DbsGroup pnd_Hold_Key__R_Field_9;
    private DbsField pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr;
    private DbsField pnd_Hold_Key_Pnd_Hold_Payee_Cde;
    private DbsField pnd_T;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_Tot_T;
    private DbsField pnd_After_Found;
    private DbsField pnd_Before_Found;
    private DbsField pnd_Dont_Write;
    private DbsField pnd_Hold_Before_Pend_Code;
    private DbsField pnd_Hold_After_Pend_Code;
    private DbsField pnd_Offset_Count;
    private DbsField pnd_Work_Tiaa_Comp_Fund_Cde;

    private DbsGroup pnd_Work_Tiaa_Comp_Fund_Cde__R_Field_10;
    private DbsField pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_First;
    private DbsField pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2;

    private DbsGroup pnd_Bef_Aft_Table;
    private DbsField pnd_Bef_Aft_Table_Pnd_Contract_Tab;
    private DbsField pnd_Bef_Aft_Table_Pnd_Payee_Cde_Tab;
    private DbsField pnd_Bef_Aft_Table_Pnd_Fund_Cde_Tab;
    private DbsField pnd_B_Fnds;
    private DbsField pnd_A_Fnds;
    private DbsField pnd_W_Fnd;
    private DbsField pnd_Frst_Dllr;
    private DbsField pnd_Lst_Dllr;
    private DbsField pnd_I;
    private DbsField pnd_F;
    private DbsField pnd_C_Tr_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Filler = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler", "FILLER", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Company = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Company", "COMPANY", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Filler1 = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler1", "FILLER1", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cntrct_Trans);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cpr_Trans);

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");

        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Trans);

        pnd_Contract_Record_In = localVariables.newGroupInRecord("pnd_Contract_Record_In", "#CONTRACT-RECORD-IN");
        pnd_Contract_Record_In_Pnd_Event_Ind_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Event_Ind_In", "#EVENT-IND-IN", FieldType.STRING, 
            1);
        pnd_Contract_Record_In_Pnd_Plan_Num_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Plan_Num_In", "#PLAN-NUM-IN", FieldType.STRING, 
            6);
        pnd_Contract_Record_In_Pnd_Ssn_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ssn_In", "#SSN-IN", FieldType.NUMERIC, 
            9);
        pnd_Contract_Record_In_Pnd_Fund_Id_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Fund_Id_In", "#FUND-ID-IN", FieldType.STRING, 
            2);
        pnd_Contract_Record_In_Pnd_Eff_Date_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Eff_Date_In", "#EFF-DATE-IN", FieldType.NUMERIC, 
            8);
        pnd_Contract_Record_In_Pnd_Inverse_Date_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Inverse_Date_In", "#INVERSE-DATE-IN", 
            FieldType.NUMERIC, 12);
        pnd_Contract_Record_In_Pnd_Contract_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Contract_In", "#CONTRACT-IN", FieldType.STRING, 
            10);
        pnd_Contract_Record_In_Pnd_Payee_Cde_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Payee_Cde_In", "#PAYEE-CDE-IN", FieldType.NUMERIC, 
            2);
        pnd_Contract_Record_In_Pnd_Cusip_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Cusip_In", "#CUSIP-IN", FieldType.STRING, 
            9);
        pnd_Contract_Record_In_Pnd_Plan_Type_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Plan_Type_In", "#PLAN-TYPE-IN", FieldType.STRING, 
            1);
        pnd_Contract_Record_In_Pnd_Source_App_Id_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Source_App_Id_In", "#SOURCE-APP-ID-IN", 
            FieldType.STRING, 4);
        pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In", "#IA-ORIG-CODE-IN", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In", "#IA-OPT-CODE-IN", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In", "#IA-SETTLEMENT-ID-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Trans_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trans_Code_In", "#TRANS-CODE-IN", 
            FieldType.NUMERIC, 3);
        pnd_Contract_Record_In_Pnd_Act_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Act_Code_In", "#ACT-CODE-IN", FieldType.STRING, 
            1);
        pnd_Contract_Record_In_Pnd_Trns_Act_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trns_Act_Code_In", "#TRNS-ACT-CODE-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Pend_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Pend_Code_In", "#PEND-CODE-IN", FieldType.STRING, 
            1);
        pnd_Contract_Record_In_Pnd_First_Ann_Dod_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_First_Ann_Dod_In", "#FIRST-ANN-DOD-IN", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In", "#SECOND-ANN-DOD-IN", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In", "#TRANS-SUB-CODE-IN", 
            FieldType.STRING, 3);
        pnd_Contract_Record_In_Pnd_Trans_Dte_In = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trans_Dte_In", "#TRANS-DTE-IN", FieldType.TIME);
        pnd_Contract_Record_In_Pnd_Fund_Id_In_Work = pnd_Contract_Record_In.newFieldInGroup("pnd_Contract_Record_In_Pnd_Fund_Id_In_Work", "#FUND-ID-IN-WORK", 
            FieldType.STRING, 3);

        pnd_Contract_Record_Out = localVariables.newGroupInRecord("pnd_Contract_Record_Out", "#CONTRACT-RECORD-OUT");
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Event_Ind_Out", "#EVENT-IND-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Plan_Num_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Plan_Num_Out", "#PLAN-NUM-OUT", 
            FieldType.STRING, 6);
        pnd_Contract_Record_Out_Pnd_Ssn_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ssn_Out", "#SSN-OUT", FieldType.NUMERIC, 
            9);
        pnd_Contract_Record_Out_Pnd_Fund_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Fund_Id_Out", "#FUND-ID-OUT", FieldType.STRING, 
            2);
        pnd_Contract_Record_Out_Pnd_Eff_Date_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Eff_Date_Out", "#EFF-DATE-OUT", 
            FieldType.NUMERIC, 8);
        pnd_Contract_Record_Out_Pnd_Inverse_Date_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Inverse_Date_Out", "#INVERSE-DATE-OUT", 
            FieldType.NUMERIC, 12);
        pnd_Contract_Record_Out_Pnd_Contract_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Contract_Out", "#CONTRACT-OUT", 
            FieldType.STRING, 10);
        pnd_Contract_Record_Out_Pnd_Payee_Cde_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Payee_Cde_Out", "#PAYEE-CDE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Cusip_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Cusip_Out", "#CUSIP-OUT", FieldType.STRING, 
            9);
        pnd_Contract_Record_Out_Pnd_Plan_Type_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Plan_Type_Out", "#PLAN-TYPE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Source_App_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Source_App_Id_Out", "#SOURCE-APP-ID-OUT", 
            FieldType.STRING, 4);
        pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out", "#IA-ORIG-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out", "#IA-OPT-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out", 
            "#IA-SETTLEMENT-ID-OUT", FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Trans_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trans_Code_Out", "#TRANS-CODE-OUT", 
            FieldType.NUMERIC, 3);
        pnd_Contract_Record_Out_Pnd_Act_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Act_Code_Out", "#ACT-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Trns_Act_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trns_Act_Code_Out", "#TRNS-ACT-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_Pend_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Pend_Code_Out", "#PEND-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out", "#FIRST-ANN-DOD-OUT", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out", "#SECOND-ANN-DOD-OUT", 
            FieldType.NUMERIC, 6);
        pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out = pnd_Contract_Record_Out.newFieldInGroup("pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out", "#TRANS-SUB-CODE-OUT", 
            FieldType.STRING, 3);

        pnd_Tot_Report_Rec = localVariables.newGroupInRecord("pnd_Tot_Report_Rec", "#TOT-REPORT-REC");
        pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fst_Dollar", "#TOT-FST-DOLLAR", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Lst_Dollar", "#TOT-LST-DOLLAR", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Events", "#TOT-FSTLST-EVENTS", 
            FieldType.NUMERIC, 5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fst_Offset", "#TOT-FST-OFFSET", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Lst_Offset", "#TOT-LST-OFFSET", FieldType.NUMERIC, 
            5);
        pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Fstlst_Offset_Written", "#TOT-FSTLST-OFFSET-WRITTEN", 
            FieldType.NUMERIC, 5);
        pnd_Tot_Report_Rec_Pnd_Tot_Diff = pnd_Tot_Report_Rec.newFieldInGroup("pnd_Tot_Report_Rec_Pnd_Tot_Diff", "#TOT-DIFF", FieldType.NUMERIC, 5);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.STRING, 9);
        pnd_Work_Before_Key = localVariables.newFieldInRecord("pnd_Work_Before_Key", "#WORK-BEFORE-KEY", FieldType.STRING, 23);

        pnd_Work_Before_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Before_Key__R_Field_2", "REDEFINE", pnd_Work_Before_Key);
        pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef = pnd_Work_Before_Key__R_Field_2.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef", "#WORK-IMGE-ID-BEF", 
            FieldType.STRING, 1);
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef = pnd_Work_Before_Key__R_Field_2.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef", 
            "#WORK-CNTRCT-PPCN-NBR-BEF", FieldType.STRING, 10);
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef = pnd_Work_Before_Key__R_Field_2.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef", 
            "#WORK-CNTRCT-PAYEE-CDE-BEF", FieldType.NUMERIC, 2);
        pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef = pnd_Work_Before_Key__R_Field_2.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef", "#WORK-TRANS-DTE-BEF", 
            FieldType.TIME);
        pnd_Work_Before_Key_Pnd_Work_Fund_Bef = pnd_Work_Before_Key__R_Field_2.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Fund_Bef", "#WORK-FUND-BEF", 
            FieldType.STRING, 3);
        pnd_Work_After_Key = localVariables.newFieldInRecord("pnd_Work_After_Key", "#WORK-AFTER-KEY", FieldType.STRING, 28);

        pnd_Work_After_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_After_Key__R_Field_3", "REDEFINE", pnd_Work_After_Key);
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft = pnd_Work_After_Key__R_Field_3.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft", "#WORK-IMGE-ID-AFT", 
            FieldType.STRING, 1);
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft = pnd_Work_After_Key__R_Field_3.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft", 
            "#WORK-CNTRCT-PPCN-NBR-AFT", FieldType.STRING, 10);
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft = pnd_Work_After_Key__R_Field_3.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft", 
            "#WORK-CNTRCT-PAYEE-CDE-AFT", FieldType.NUMERIC, 2);
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft = pnd_Work_After_Key__R_Field_3.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft", "#WORK-INVRS-DTE-AFT", 
            FieldType.NUMERIC, 12);
        pnd_Work_After_Key_Pnd_Work_Fund_Aft = pnd_Work_After_Key__R_Field_3.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Fund_Aft", "#WORK-FUND-AFT", 
            FieldType.STRING, 3);
        pnd_Work_Cpr_Before_Key = localVariables.newFieldInRecord("pnd_Work_Cpr_Before_Key", "#WORK-CPR-BEFORE-KEY", FieldType.STRING, 20);

        pnd_Work_Cpr_Before_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Cpr_Before_Key__R_Field_4", "REDEFINE", pnd_Work_Cpr_Before_Key);
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef = pnd_Work_Cpr_Before_Key__R_Field_4.newFieldInGroup("pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef", 
            "#WORK-CPR-IMGE-ID-BEF", FieldType.STRING, 1);
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef = pnd_Work_Cpr_Before_Key__R_Field_4.newFieldInGroup("pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef", 
            "#WORK-CPR-CNTRCT-PPCN-NBR-BEF", FieldType.STRING, 10);
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef = pnd_Work_Cpr_Before_Key__R_Field_4.newFieldInGroup("pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef", 
            "#WORK-CPR-CNTRCT-PAYEE-CDE-BEF", FieldType.NUMERIC, 2);
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef = pnd_Work_Cpr_Before_Key__R_Field_4.newFieldInGroup("pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef", 
            "#WORK-CPR-TRANS-DTE-BEF", FieldType.TIME);
        pnd_Work_Cpr_After_Key = localVariables.newFieldInRecord("pnd_Work_Cpr_After_Key", "#WORK-CPR-AFTER-KEY", FieldType.STRING, 25);

        pnd_Work_Cpr_After_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Cpr_After_Key__R_Field_5", "REDEFINE", pnd_Work_Cpr_After_Key);
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft = pnd_Work_Cpr_After_Key__R_Field_5.newFieldInGroup("pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft", 
            "#WORK-CPR-IMGE-ID-AFT", FieldType.STRING, 1);
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft = pnd_Work_Cpr_After_Key__R_Field_5.newFieldInGroup("pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft", 
            "#WORK-CPR-CNTRCT-PPCN-NBR-AFT", FieldType.STRING, 10);
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft = pnd_Work_Cpr_After_Key__R_Field_5.newFieldInGroup("pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft", 
            "#WORK-CPR-CNTRCT-PAYEE-CDE-AFT", FieldType.NUMERIC, 2);
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft = pnd_Work_Cpr_After_Key__R_Field_5.newFieldInGroup("pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft", 
            "#WORK-CPR-INVRS-DTE-AFT", FieldType.NUMERIC, 12);
        pnd_Work_Contrct_Bef_Key = localVariables.newFieldInRecord("pnd_Work_Contrct_Bef_Key", "#WORK-CONTRCT-BEF-KEY", FieldType.STRING, 18);

        pnd_Work_Contrct_Bef_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Contrct_Bef_Key__R_Field_6", "REDEFINE", pnd_Work_Contrct_Bef_Key);
        pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id = pnd_Work_Contrct_Bef_Key__R_Field_6.newFieldInGroup("pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id", 
            "#WORK-CONTRCT-BFRE-IMGE-ID", FieldType.STRING, 1);
        pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef = pnd_Work_Contrct_Bef_Key__R_Field_6.newFieldInGroup("pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef", 
            "#WORK-CONTRCT-PPCN-NBR-BEF", FieldType.STRING, 10);
        pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte = pnd_Work_Contrct_Bef_Key__R_Field_6.newFieldInGroup("pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte", 
            "#WORK-CONTRCT-TRANS-DTE", FieldType.TIME);
        pnd_Work_Contrct_Aft_Key = localVariables.newFieldInRecord("pnd_Work_Contrct_Aft_Key", "#WORK-CONTRCT-AFT-KEY", FieldType.STRING, 23);

        pnd_Work_Contrct_Aft_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Contrct_Aft_Key__R_Field_7", "REDEFINE", pnd_Work_Contrct_Aft_Key);
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id = pnd_Work_Contrct_Aft_Key__R_Field_7.newFieldInGroup("pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id", 
            "#WORK-CONTRCT-AFT-IMGE-ID", FieldType.STRING, 1);
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft = pnd_Work_Contrct_Aft_Key__R_Field_7.newFieldInGroup("pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft", 
            "#WORK-CONTRCT-PPCN-NBR-AFT", FieldType.STRING, 10);
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte = pnd_Work_Contrct_Aft_Key__R_Field_7.newFieldInGroup("pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte", 
            "#WORK-CONTRCT-INVRSE-DTE", FieldType.NUMERIC, 12);
        pnd_Work_Fund_Id = localVariables.newFieldInRecord("pnd_Work_Fund_Id", "#WORK-FUND-ID", FieldType.STRING, 3);

        pnd_Work_Fund_Id__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Fund_Id__R_Field_8", "REDEFINE", pnd_Work_Fund_Id);
        pnd_Work_Fund_Id_Pnd_Fund_Id_Fix = pnd_Work_Fund_Id__R_Field_8.newFieldInGroup("pnd_Work_Fund_Id_Pnd_Fund_Id_Fix", "#FUND-ID-FIX", FieldType.STRING, 
            1);
        pnd_Work_Fund_Id_Pnd_Fund_Id_Rest = pnd_Work_Fund_Id__R_Field_8.newFieldInGroup("pnd_Work_Fund_Id_Pnd_Fund_Id_Rest", "#FUND-ID-REST", FieldType.STRING, 
            2);

        pnd_Fnd_Rec = localVariables.newGroupInRecord("pnd_Fnd_Rec", "#FND-REC");
        pnd_Fnd_Rec_Pnd_Fnd_Rec_Cusip = pnd_Fnd_Rec.newFieldInGroup("pnd_Fnd_Rec_Pnd_Fnd_Rec_Cusip", "#FND-REC-CUSIP", FieldType.STRING, 10);
        pnd_Fnd_Rec_Pnd_Fnd_Rec_Num_Fund_Cde = pnd_Fnd_Rec.newFieldInGroup("pnd_Fnd_Rec_Pnd_Fnd_Rec_Num_Fund_Cde", "#FND-REC-NUM-FUND-CDE", FieldType.NUMERIC, 
            2);
        pnd_Max_Nbr_Of_Ext_Fnds = localVariables.newFieldInRecord("pnd_Max_Nbr_Of_Ext_Fnds", "#MAX-NBR-OF-EXT-FNDS", FieldType.PACKED_DECIMAL, 3);

        pnd_Fnd_Tab_Rec = localVariables.newGroupArrayInRecord("pnd_Fnd_Tab_Rec", "#FND-TAB-REC", new DbsArrayController(1, 150));
        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip = pnd_Fnd_Tab_Rec.newFieldInGroup("pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip", "#FND-TAB-CUSIP", FieldType.STRING, 10);
        pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde = pnd_Fnd_Tab_Rec.newFieldInGroup("pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde", "#FND-TAB-NUM-FUND-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Notfnd = localVariables.newFieldInRecord("pnd_Notfnd", "#NOTFND", FieldType.BOOLEAN, 1);
        pnd_Hold_Key = localVariables.newFieldInRecord("pnd_Hold_Key", "#HOLD-KEY", FieldType.STRING, 12);

        pnd_Hold_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Hold_Key__R_Field_9", "REDEFINE", pnd_Hold_Key);
        pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr = pnd_Hold_Key__R_Field_9.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr", "#HOLD-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Hold_Key_Pnd_Hold_Payee_Cde = pnd_Hold_Key__R_Field_9.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Payee_Cde", "#HOLD-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 4);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 4);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 4);
        pnd_Tot_T = localVariables.newFieldInRecord("pnd_Tot_T", "#TOT-T", FieldType.PACKED_DECIMAL, 4);
        pnd_After_Found = localVariables.newFieldInRecord("pnd_After_Found", "#AFTER-FOUND", FieldType.BOOLEAN, 1);
        pnd_Before_Found = localVariables.newFieldInRecord("pnd_Before_Found", "#BEFORE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Dont_Write = localVariables.newFieldInRecord("pnd_Dont_Write", "#DONT-WRITE", FieldType.BOOLEAN, 1);
        pnd_Hold_Before_Pend_Code = localVariables.newFieldInRecord("pnd_Hold_Before_Pend_Code", "#HOLD-BEFORE-PEND-CODE", FieldType.STRING, 1);
        pnd_Hold_After_Pend_Code = localVariables.newFieldInRecord("pnd_Hold_After_Pend_Code", "#HOLD-AFTER-PEND-CODE", FieldType.STRING, 1);
        pnd_Offset_Count = localVariables.newFieldInRecord("pnd_Offset_Count", "#OFFSET-COUNT", FieldType.NUMERIC, 10);
        pnd_Work_Tiaa_Comp_Fund_Cde = localVariables.newFieldInRecord("pnd_Work_Tiaa_Comp_Fund_Cde", "#WORK-TIAA-COMP-FUND-CDE", FieldType.STRING, 3);

        pnd_Work_Tiaa_Comp_Fund_Cde__R_Field_10 = localVariables.newGroupInRecord("pnd_Work_Tiaa_Comp_Fund_Cde__R_Field_10", "REDEFINE", pnd_Work_Tiaa_Comp_Fund_Cde);
        pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_First = pnd_Work_Tiaa_Comp_Fund_Cde__R_Field_10.newFieldInGroup("pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_First", 
            "#FND-FIRST", FieldType.STRING, 1);
        pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2 = pnd_Work_Tiaa_Comp_Fund_Cde__R_Field_10.newFieldInGroup("pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2", "#FND-2", 
            FieldType.STRING, 2);

        pnd_Bef_Aft_Table = localVariables.newGroupArrayInRecord("pnd_Bef_Aft_Table", "#BEF-AFT-TABLE", new DbsArrayController(1, 1000));
        pnd_Bef_Aft_Table_Pnd_Contract_Tab = pnd_Bef_Aft_Table.newFieldInGroup("pnd_Bef_Aft_Table_Pnd_Contract_Tab", "#CONTRACT-TAB", FieldType.STRING, 
            10);
        pnd_Bef_Aft_Table_Pnd_Payee_Cde_Tab = pnd_Bef_Aft_Table.newFieldInGroup("pnd_Bef_Aft_Table_Pnd_Payee_Cde_Tab", "#PAYEE-CDE-TAB", FieldType.NUMERIC, 
            2);
        pnd_Bef_Aft_Table_Pnd_Fund_Cde_Tab = pnd_Bef_Aft_Table.newFieldInGroup("pnd_Bef_Aft_Table_Pnd_Fund_Cde_Tab", "#FUND-CDE-TAB", FieldType.STRING, 
            3);
        pnd_B_Fnds = localVariables.newFieldArrayInRecord("pnd_B_Fnds", "#B-FNDS", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_A_Fnds = localVariables.newFieldArrayInRecord("pnd_A_Fnds", "#A-FNDS", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_W_Fnd = localVariables.newFieldInRecord("pnd_W_Fnd", "#W-FND", FieldType.STRING, 2);
        pnd_Frst_Dllr = localVariables.newFieldArrayInRecord("pnd_Frst_Dllr", "#FRST-DLLR", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Lst_Dllr = localVariables.newFieldArrayInRecord("pnd_Lst_Dllr", "#LST-DLLR", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_C_Tr_Found = localVariables.newFieldInRecord("pnd_C_Tr_Found", "#C-TR-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();

        localVariables.reset();
        pnd_Max_Nbr_Of_Ext_Fnds.setInitialValue(150);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap605() throws Exception
    {
        super("Iaap605");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM LOAD-CUSIP-TABLE
        sub_Load_Cusip_Table();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CONTRACT-RECORD-IN
        while (condition(getWorkFiles().read(1, pnd_Contract_Record_In)))
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.reset();                                                                                                            //Natural: RESET #EVENT-IND-OUT
            short decideConditionsMet201 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #TRANS-CODE-IN;//Natural: VALUE 006
            if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(6))))
            {
                decideConditionsMet201++;
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                  //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T06-RTN
                sub_Event_T06_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 020
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(20))))
            {
                decideConditionsMet201++;
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                  //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T20-RTN
                sub_Event_T20_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 030
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(30))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T30-RTN
                sub_Event_T30_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 031
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(31))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T31-RTN
                sub_Event_T31_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 033
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(33))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T33-RTN
                sub_Event_T33_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 035
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(35))))
            {
                decideConditionsMet201++;
                //* **** MOVE 'F'  TO  #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T35-RTN
                sub_Event_T35_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 036
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(36))))
            {
                decideConditionsMet201++;
                //* **   MOVE 'F'  TO  #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T36-RTN
                sub_Event_T36_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 037
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(37))))
            {
                decideConditionsMet201++;
                //*      MOVE 'F'  TO  #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T37-RTN
                sub_Event_T37_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 052
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(52))))
            {
                decideConditionsMet201++;
                //* **   MOVE 'L'  TO  #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T52-RTN
                sub_Event_T52_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 053
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(53))))
            {
                decideConditionsMet201++;
                //* **   MOVE 'F'  TO  #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM EVENT-T53-RTN
                sub_Event_T53_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 040
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(40))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T40-RTN
                sub_Event_T40_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 066
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(66))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T66-RTN
                sub_Event_T66_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 102
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(102))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM EVENT-T102-RTN
                sub_Event_T102_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 500, 502, 504, 506, 508, 512, 514, 516, 518, 051, 050
            else if (condition((pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(500) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(502) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(504) 
                || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(506) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(508) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(512) 
                || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(514) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(516) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(518) 
                || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(51) || pnd_Contract_Record_In_Pnd_Trans_Code_In.equals(50))))
            {
                decideConditionsMet201++;
                                                                                                                                                                          //Natural: PERFORM LOAD-BEF-AFT-TABLE
                sub_Load_Bef_Aft_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM EVENT-T500-RTN
                sub_Event_T500_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #EVENT-IND-OUT
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF #TRANS-CODE-IN NOT = 500 AND
            //*      #TRANS-CODE-IN NOT = 502 AND
            //*      #TRANS-CODE-IN NOT = 504 AND
            //*      #TRANS-CODE-IN NOT = 506 AND
            //*      #TRANS-CODE-IN NOT = 508 AND
            //*      #TRANS-CODE-IN NOT = 512 AND
            //*      #TRANS-CODE-IN NOT = 514 AND
            //*      #TRANS-CODE-IN NOT = 516 AND
            //*      #TRANS-CODE-IN NOT = 518 AND
            //*      #TRANS-CODE-IN NOT = 102 AND
            //*      #TRANS-CODE-IN NOT = 051 AND
            //*      #TRANS-CODE-IN NOT = 020 AND
            //*      #TRANS-CODE-IN NOT = 030 AND
            //*      #TRANS-CODE-IN NOT = 031 AND
            //*      #TRANS-CODE-IN NOT = 033 AND
            //*      #TRANS-CODE-IN NOT = 035 AND
            //*      #TRANS-CODE-IN NOT = 036 AND
            //*      #TRANS-CODE-IN NOT = 052 AND
            //*      #TRANS-CODE-IN NOT = 053 AND
            //*      #TRANS-CODE-IN NOT = 050 AND
            //*      #TRANS-CODE-IN NOT = 066
            //*    IF #EVENT-IND-OUT = 'L' OR #EVENT-IND-OUT = 'F'
            //*      PERFORM WRITE-RTN
            //*    END-IF
            //*  END-IF
            //*  MOVE #CONTRACT-IN       TO   #HOLD-CONTRACT
            //*  MOVE #PAYEE-CDE-IN      TO   #HOLD-PAYEE-CDE
            //*  MOVE #FUND-ID-IN        TO   #HOLD-FUND
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-CUSIP-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CUSIP
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T06-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T20-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T30-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T31-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T33-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T35-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T36-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T37-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T40-RTN
        //* **********************************************************************
        //*  DEFINE EVENT-T51-RTN
        //* **********************************************************************
        //*  RESET #BEFORE-FOUND
        //*       #AFTER-FOUND
        //*       #WORK-AFTER-KEY
        //*       #WORK-BEFORE-KEY
        //*  MOVE #T TO #TOT-T
        //*  MOVE 1  TO #T
        //*  REPEAT UNTIL #T > #TOT-T
        //*  MOVE #TRANS-DTE-IN    TO #WORK-TRANS-DTE-BEF
        //*  MOVE #CONTRACT-IN     TO #WORK-CNTRCT-PPCN-NBR-BEF
        //*  MOVE #PAYEE-CDE-TAB(#T) TO #WORK-CNTRCT-PAYEE-CDE-BEF
        //*  MOVE  '1'             TO #WORK-IMGE-ID-BEF
        //*  MOVE #FUND-CDE-TAB(#T) TO #WORK-FUND-BEF
        //*  MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        //*  MOVE #CONTRACT-IN     TO #WORK-CNTRCT-PPCN-NBR-AFT
        //*  MOVE #PAYEE-CDE-TAB(#T)  TO #WORK-CNTRCT-PAYEE-CDE-AFT
        //*  MOVE #FUND-CDE-TAB(#T)    TO #WORK-FUND-AFT
        //*  MOVE  '2'             TO #WORK-IMGE-ID-AFT
        //*  PERFORM READ-TRANS-BEFORE-REC
        //*  PERFORM READ-TRANS-AFTER-REC
        //*  IF #BEFORE-FOUND = FALSE AND #AFTER-FOUND = TRUE
        //*     MOVE 'F'         TO #EVENT-IND-OUT
        //*     PERFORM WRITE-RTN
        //*  END-IF
        //*  IF #BEFORE-FOUND = TRUE  AND #AFTER-FOUND = FALSE
        //*     MOVE 'L'         TO #EVENT-IND-OUT
        //*     PERFORM WRITE-RTN
        //*  END-IF
        //*  ADD 1 TO #T
        //*  RESET #BEFORE-FOUND
        //*        #AFTER-FOUND
        //*        #WORK-AFTER-KEY
        //*        #WORK-BEFORE-KEY
        //*  END-REPEAT
        //*  END-SUBROUTINE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T52-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T53-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T66-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T102-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVENT-T500-RTN
        //*  CYCLE THRU #FRST-DLLR(*) AND #LST-DLLR(*) TO PROCESS EACH FUND
        //*  RESET #BEFORE-FOUND
        //*        #AFTER-FOUND
        //*        #WORK-AFTER-KEY
        //*        #WORK-BEFORE-KEY
        //*  MOVE #T TO #TOT-T
        //*  MOVE 1  TO #T
        //*  REPEAT UNTIL #T > #TOT-T
        //*   MOVE #TRANS-DTE-IN      TO #WORK-TRANS-DTE-BEF
        //*   MOVE #CONTRACT-IN       TO #WORK-CNTRCT-PPCN-NBR-BEF
        //*   MOVE #PAYEE-CDE-TAB(#T) TO #WORK-CNTRCT-PAYEE-CDE-BEF
        //*   MOVE #FUND-CDE-TAB(#T)  TO #WORK-FUND-BEF
        //*   MOVE  '1'               TO #WORK-IMGE-ID-BEF
        //* * OVE  '2'                TO #WORK-IMGE-ID-BEF
        //*   MOVE #INVERSE-DATE-IN   TO #WORK-INVRS-DTE-AFT
        //*   MOVE #CONTRACT-IN       TO #WORK-CNTRCT-PPCN-NBR-AFT
        //*   MOVE #PAYEE-CDE-TAB(#T) TO #WORK-CNTRCT-PAYEE-CDE-AFT
        //*   MOVE #FUND-CDE-TAB(#T)  TO #WORK-FUND-AFT
        //*   MOVE  '2'               TO #WORK-IMGE-ID-AFT
        //*   PERFORM READ-TRANS-BEFORE-REC
        //*   PERFORM READ-TRANS-AFTER-REC
        //* **  IF #BEFORE-FOUND = FALSE AND #AFTER-FOUND = TRUE
        //* **    MOVE 'L'         TO #EVENT-IND-OUT
        //* ** END-IF
        //* ** IF #BEFORE-FOUND = TRUE  AND #AFTER-FOUND = FALSE
        //* **    MOVE 'F'         TO #EVENT-IND-OUT
        //* ** END-IF
        //*   MOVE #FUND-ID-IN-WORK TO #WORK-FUND-ID
        //*   #BEFORE-FOUND = TRUE  AND #ACT-CODE-IN = '1' AND #AFTER-FOUND = FALSE
        //*   IF #FUND-ID-FIX   = 'T' AND TIAA-TOT-PER-AMT NOT = 0
        //*      MOVE 'L'        TO #EVENT-IND-OUT
        //*      PERFORM WRITE-RTN
        //*   END-IF
        //*   IF #FUND-ID-FIX   NOT = 'T' AND TIAA-UNITS-CNT(1) NOT = 0
        //*      MOVE 'L'        TO #EVENT-IND-OUT
        //*      PERFORM WRITE-RTN
        //*   END-IF
        //*  D-IF
        //*   #BEFORE-FOUND = FALSE AND #ACT-CODE-IN = '1' AND #AFTER-FOUND = TRUE
        //*   MOVE 'F'        TO #EVENT-IND-OUT
        //*   PERFORM WRITE-RTN
        //*  END-IF
        //*  F #BEFORE-FOUND = TRUE  AND #ACT-CODE-IN = '1' AND #AFTER-FOUND = TRUE
        //*   IF (#FUND-ID-FIX   = 'T' AND TIAA-TOT-PER-AMT = 0) OR
        //*      (#FUND-ID-FIX   NOT = 'T' AND TIAA-UNITS-CNT(1) = 0)
        //*      MOVE 'F'        TO #EVENT-IND-OUT
        //*      PERFORM WRITE-RTN
        //*   END-IF
        //*  D-IF
        //*  IF(#TRANS-CODE-IN  = 502 OR #TRANS-CODE-IN = 504 OR
        //*     #TRANS-CODE-IN  = 512 OR #TRANS-CODE-IN = 514) AND
        //*     #ACT-CODE-IN     = '9'                         AND
        //*     #BEFORE-FOUND = TRUE
        //*       MOVE 'L'       TO #EVENT-IND-OUT
        //*       PERFORM WRITE-RTN
        //*  D-IF
        //*  ADD 1 TO #T
        //*  RESET #BEFORE-FOUND
        //*        #AFTER-FOUND
        //*        #WORK-AFTER-KEY
        //*        #WORK-BEFORE-KEY
        //*  END-REPEAT
        //*  DEFINE EVENT-T506-T508-RTN
        //* **********************************************************************
        //*  IF #ACT-CODE-IN = '9'
        //*     MOVE 'L' TO #EVENT-IND-OUT
        //*  ELSE
        //*     RESET #BEFORE-FOUND
        //*           #WORK-BEFORE-KEY
        //* *  MOVE #EFF-DATE-IN   TO #WORK-TRANS-DTE-BEF
        //*     MOVE #TRANS-DTE-IN  TO #WORK-TRANS-DTE-BEF
        //*     MOVE #CONTRACT-IN     TO #WORK-CNTRCT-PPCN-NBR-BEF
        //*     MOVE #PAYEE-CDE-IN    TO #WORK-CNTRCT-PAYEE-CDE-BEF
        //*     MOVE #FUND-ID-IN-WORK TO #WORK-FUND-BEF
        //*     MOVE  '1'             TO #WORK-IMGE-ID-BEF
        //*     PERFORM READ-TRANS-BEFORE-REC
        //* ** WRITE 'CONTRACT 502 504 '  #CONTRACT-IN
        //*     IF NOT #BEFORE-FOUND
        //*        MOVE  'F'  TO #EVENT-IND-OUT
        //*     END-IF
        //*  END-IF
        //*  END-SUBROUTINE
        //* **********************************************************************
        //*  DEFINE EVENT-T502-T504-RTN
        //* **********************************************************************
        //*  IF #ACT-CODE-IN = '9'
        //*     MOVE 'L' TO #EVENT-IND-OUT
        //*  ELSE
        //*     RESET #AFTER-FOUND
        //*           #WORK-AFTER-KEY
        //*     MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        //*     MOVE #CONTRACT-IN     TO #WORK-CNTRCT-PPCN-NBR-AFT
        //*     MOVE #PAYEE-CDE-IN    TO #WORK-CNTRCT-PAYEE-CDE-AFT
        //*     MOVE #FUND-ID-IN-WORK TO #WORK-FUND-AFT
        //*     MOVE  '2'             TO #WORK-IMGE-ID-AFT
        //*     PERFORM READ-TRANS-AFTER-REC
        //*     IF NOT #AFTER-FOUND
        //*        MOVE  'L'  TO #EVENT-IND-OUT
        //*     END-IF
        //*  END-IF
        //*  END-SUBROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BEF-AFT-TABLE
        //* ***** AND #WORK-TIAA-COMP-FUND-CDE NOT = #B-FNDS(*)
        //* ****  AND #WORK-TIAA-COMP-FUND-CDE NOT = #B-FNDS(*)
        //* ***** AND #WORK-TIAA-COMP-FUND-CDE NOT = #A-FNDS(*)
        //* ****  AND #WORK-TIAA-COMP-FUND-CDE NOT = #A-FNDS(*)
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TRANS-BEFORE-REC
        //* **********************************************************************
        //*  ITE 'IN TRANS BEFORE '
        //*  ITE ' WORK BEFORE KEY ' #WORK-BEFORE-KEY
        //*  AD(1) IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TRANS-AFTER-REC
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR-REC
        //* **********************************************************************
        //* *
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FUND-RCRD
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RTN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PAYEE-02
    }
    private void sub_Load_Cusip_Table() throws Exception                                                                                                                  //Natural: LOAD-CUSIP-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_C.setValue(0);                                                                                                                                                //Natural: MOVE 0 TO #C
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #FND-REC
        while (condition(getWorkFiles().read(3, pnd_Fnd_Rec)))
        {
            pnd_C.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #C
            pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde.getValue(pnd_C).setValue(pnd_Fnd_Rec_Pnd_Fnd_Rec_Num_Fund_Cde);                                                      //Natural: MOVE #FND-REC-NUM-FUND-CDE TO #FND-TAB-NUM-FUND-CDE ( #C )
            pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip.getValue(pnd_C).setValue(pnd_Fnd_Rec_Pnd_Fnd_Rec_Cusip);                                                                    //Natural: MOVE #FND-REC-CUSIP TO #FND-TAB-CUSIP ( #C )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Find_Cusip() throws Exception                                                                                                                        //Natural: FIND-CUSIP
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_D.setValue(1);                                                                                                                                                //Natural: MOVE 1 TO #D
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_D.greater(pnd_C))) {break;}                                                                                                                 //Natural: UNTIL #D > #C
            //*  WRITE 'FIND CU FUND IN ' #FUND-ID-IN
            if (condition(pnd_Contract_Record_In_Pnd_Fund_Id_In.equals("1G") || pnd_Contract_Record_In_Pnd_Fund_Id_In.equals("1S")))                                      //Natural: IF #FUND-ID-IN = '1G' OR #FUND-ID-IN = '1S'
            {
                //* ***    OR #FUND-ID-IN  = '1 '
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue("1 ");                                                                                                     //Natural: MOVE '1 ' TO #FUND-ID-IN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.is(pnd_Contract_Record_In_Pnd_Fund_Id_In.getText(),"N2")))                                                                              //Natural: IF #FUND-ID-IN IS ( N2 )
            {
                if (condition(pnd_Contract_Record_In_Pnd_Fund_Id_In.val().equals(pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Num_Fund_Cde.getValue(pnd_D))))                              //Natural: IF VAL ( #FUND-ID-IN ) = #FND-TAB-NUM-FUND-CDE ( #D )
                {
                    pnd_Contract_Record_In_Pnd_Cusip_In.setValue(pnd_Fnd_Tab_Rec_Pnd_Fnd_Tab_Cusip.getValue(pnd_D));                                                      //Natural: MOVE #FND-TAB-CUSIP ( #D ) TO #CUSIP-IN
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_D.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #D
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Event_T06_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T06-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.greaterOrEqual(3)))                                                                                         //Natural: IF #PAYEE-CDE-IN GE 03
        {
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                         //Natural: MOVE #TRANS-DTE-IN TO #WORK-CPR-TRANS-DTE-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                    //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                  //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.setValue("1");                                                                                               //Natural: MOVE '1' TO #WORK-CPR-IMGE-ID-BEF
            vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #WORK-CPR-BEFORE-KEY
            (
            "READ03",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Work_Cpr_Before_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ03:
            while (condition(vw_iaa_Cpr_Trans.readNextRow("READ03")))
            {
                if (condition(pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.equals(iaa_Cpr_Trans_Trans_Dte) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-BEF = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-BEF = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-BEF = BFRE-IMGE-ID
                    && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.equals(iaa_Cpr_Trans_Bfre_Imge_Id)))
                {
                    //*  ONLY WRITE OUT LAST DOLLAR IF THE
                    if (condition(iaa_Cpr_Trans_Cntrct_Pend_Cde.notEquals("0")))                                                                                          //Natural: IF CNTRCT-PEND-CDE NE '0'
                    {
                        //*  PEND CODE = 0
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
        sub_Process_Fund_Rcrd();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Event_T20_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T20-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ04",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ04")))
        {
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                    //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Event_T30_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T30-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Trns_Act_Code_In.equals("R")))                                                                                           //Natural: IF #TRNS-ACT-CODE-IN = 'R'
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
            pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                                 //Natural: MOVE #TRANS-DTE-IN TO #WORK-TRANS-DTE-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                            //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                          //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.setValue("1");                                                                                                       //Natural: MOVE '1' TO #WORK-IMGE-ID-BEF
            vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                      //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #WORK-BEFORE-KEY
            (
            "READ05",
            new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Work_Before_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
            );
            READ05:
            while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ05")))
            {
                if (condition(pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.equals(iaa_Tiaa_Fund_Trans_Trans_Dte) && pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-BEF = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-BEF = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-BEF = BFRE-IMGE-ID
                    && pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.equals(iaa_Tiaa_Fund_Trans_Bfre_Imge_Id)))
                {
                    pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                    pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                    sub_Find_Cusip();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                    sub_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
            sub_Process_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T31_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T31-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Trns_Act_Code_In.equals("R")))                                                                                           //Natural: IF #TRNS-ACT-CODE-IN = 'R'
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
            pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                                 //Natural: MOVE #TRANS-DTE-IN TO #WORK-TRANS-DTE-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                            //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                          //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.setValue("1");                                                                                                       //Natural: MOVE '1' TO #WORK-IMGE-ID-BEF
            vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                      //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #WORK-BEFORE-KEY
            (
            "READ06",
            new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Work_Before_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
            );
            READ06:
            while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ06")))
            {
                if (condition(pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.equals(iaa_Tiaa_Fund_Trans_Trans_Dte) && pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-BEF = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-BEF = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-BEF = BFRE-IMGE-ID
                    && pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.equals(iaa_Tiaa_Fund_Trans_Bfre_Imge_Id)))
                {
                    pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                    pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                    sub_Find_Cusip();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                    sub_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MOVE 'F'  TO #EVENT-IND-OUT
            //*  PERFORM PROCESS-FUND-RCRD
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
            pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                               //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
            pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                             //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
            pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                           //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
            pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                        //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
            vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                      //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
            (
            "READ07",
            new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
            );
            READ07:
            while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ07")))
            {
                if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                    && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
                {
                    pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                    pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                    sub_Find_Cusip();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                    sub_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T33_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T33-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Trns_Act_Code_In.equals("R")))                                                                                           //Natural: IF #TRNS-ACT-CODE-IN = 'R'
        {
            //*  WRITE OUT A TRANSACTION ONLY IF NOT PENDED
            if (condition(pnd_Contract_Record_In_Pnd_Pend_Code_In.equals("0")))                                                                                           //Natural: IF #PEND-CODE-IN = '0'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
            pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                                 //Natural: MOVE #TRANS-DTE-IN TO #WORK-TRANS-DTE-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                            //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-BEF
            pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                          //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.setValue("1");                                                                                                       //Natural: MOVE '1' TO #WORK-IMGE-ID-BEF
            vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                      //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #WORK-BEFORE-KEY
            (
            "READ08",
            new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Work_Before_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
            );
            READ08:
            while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ08")))
            {
                if (condition(pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.equals(iaa_Tiaa_Fund_Trans_Trans_Dte) && pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-BEF = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-BEF = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-BEF = BFRE-IMGE-ID
                    && pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.equals(iaa_Tiaa_Fund_Trans_Bfre_Imge_Id)))
                {
                    pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                    pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                    sub_Find_Cusip();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                    sub_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Contract_Record_In_Pnd_Pend_Code_In.notEquals("0")))                                                                                        //Natural: IF #PEND-CODE-IN NE '0'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #PEND-CODE-IN = '0' AND #TRANS-SUB-CODE-IN NOT = '066'
            if (condition(pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In.notEquals("066")))                                                                                 //Natural: IF #TRANS-SUB-CODE-IN NOT = '066'
            {
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                  //Natural: MOVE 'F' TO #EVENT-IND-OUT
                pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                           //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
                pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                         //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
                pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                       //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
                pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                    //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
                vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                  //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
                (
                "READ09",
                new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
                new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
                );
                READ09:
                while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ09")))
                {
                    if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                        && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
                    {
                        pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                    //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                        pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                            //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                        sub_Find_Cusip();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                        sub_Write_Rtn();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* *   MOVE 'F'  TO #EVENT-IND-OUT
                //* *   PERFORM PROCESS-FUND-RCRD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T35_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T35-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                          //Natural: MOVE 'F' TO #EVENT-IND-OUT
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ10",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ10:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ10")))
        {
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                    //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Event_T36_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T36-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                          //Natural: MOVE 'F' TO #EVENT-IND-OUT
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ11",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ11:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ11")))
        {
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                    //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Event_T37_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T37-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   #PAYEE-CDE-IN      =  02 AND
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.greater(1) && pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In.equals("37B")))                                  //Natural: IF #PAYEE-CDE-IN > 01 AND #TRANS-SUB-CODE-IN = '37B'
        {
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                        //Natural: MOVE #TRANS-DTE-IN TO #WORK-CONTRCT-TRANS-DTE
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                      //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-BEF
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.setValue("1");                                                                                         //Natural: MOVE '1' TO #WORK-CONTRCT-BFRE-IMGE-ID
            vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #WORK-CONTRCT-BEF-KEY
            (
            "READ12",
            new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Work_Contrct_Bef_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
            1
            );
            READ12:
            while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ12")))
            {
                if (condition(pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.equals(iaa_Cntrct_Trans_Trans_Dte) && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-TRANS-DTE = TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-BEF = CNTRCT-PPCN-NBR AND #WORK-CONTRCT-BFRE-IMGE-ID = BFRE-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE > 0 AND CNTRCT-SCND-ANNT-DOD-DTE > 0
                    && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.equals(iaa_Cntrct_Trans_Bfre_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero()) 
                    && iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))
                {
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                               //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CPR-INVRS-DTE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                             //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                           //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.setValue("2");                                                                                        //Natural: MOVE '2' TO #WORK-CPR-IMGE-ID-AFT
                    vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #WORK-CPR-AFTER-KEY
                    (
                    "READ13",
                    new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Work_Cpr_After_Key, WcType.BY) },
                    new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
                    1
                    );
                    READ13:
                    while (condition(vw_iaa_Cpr_Trans.readNextRow("READ13")))
                    {
                        if (condition(pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.equals(iaa_Cpr_Trans_Invrse_Trans_Dte) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-AFT = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-AFT = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-AFT = AFTR-IMGE-ID AND CNTRCT-PEND-CDE = '0'
                            && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.equals(iaa_Cpr_Trans_Aftr_Imge_Id) 
                            && iaa_Cpr_Trans_Cntrct_Pend_Cde.equals("0")))
                        {
                            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                            sub_Process_Fund_Rcrd();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(1)))                                                                                                 //Natural: IF #PAYEE-CDE-IN = 01
        {
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                        //Natural: MOVE #TRANS-DTE-IN TO #WORK-CONTRCT-TRANS-DTE
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                      //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-BEF
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.setValue("1");                                                                                         //Natural: MOVE '1' TO #WORK-CONTRCT-BFRE-IMGE-ID
            vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #WORK-CONTRCT-BEF-KEY
            (
            "READ14",
            new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Work_Contrct_Bef_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
            1
            );
            READ14:
            while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ14")))
            {
                if (condition(pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.equals(iaa_Cntrct_Trans_Trans_Dte) && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-TRANS-DTE = TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-BEF = CNTRCT-PPCN-NBR AND #WORK-CONTRCT-BFRE-IMGE-ID = BFRE-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE > 0
                    && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.equals(iaa_Cntrct_Trans_Bfre_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero())))
                {
                    pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                              //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                    sub_Process_Fund_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2) && pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In.notEquals("37B")))                                //Natural: IF #PAYEE-CDE-IN = 02 AND #TRANS-SUB-CODE-IN NOT = '37B'
        {
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                        //Natural: MOVE #TRANS-DTE-IN TO #WORK-CONTRCT-TRANS-DTE
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                      //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-BEF
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.setValue("1");                                                                                         //Natural: MOVE '1' TO #WORK-CONTRCT-BFRE-IMGE-ID
            vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #WORK-CONTRCT-BEF-KEY
            (
            "READ15",
            new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Work_Contrct_Bef_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
            1
            );
            READ15:
            while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ15")))
            {
                if (condition(pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.equals(iaa_Cntrct_Trans_Trans_Dte) && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-TRANS-DTE = TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-BEF = CNTRCT-PPCN-NBR AND #WORK-CONTRCT-BFRE-IMGE-ID = BFRE-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE > 0 AND CNTRCT-SCND-ANNT-DOD-DTE > 0
                    && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.equals(iaa_Cntrct_Trans_Bfre_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero()) 
                    && iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))
                {
                    pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                 //Natural: MOVE #TRANS-DTE-IN TO #WORK-CPR-TRANS-DTE-BEF
                    pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                            //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-BEF
                    pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                          //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-BEF
                    pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.setValue("1");                                                                                       //Natural: MOVE '1' TO #WORK-CPR-IMGE-ID-BEF
                    vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #WORK-CPR-BEFORE-KEY
                    (
                    "READ16",
                    new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Work_Cpr_Before_Key.getBinary(), WcType.BY) },
                    new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
                    1
                    );
                    READ16:
                    while (condition(vw_iaa_Cpr_Trans.readNextRow("READ16")))
                    {
                        if (condition(pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.equals(iaa_Cpr_Trans_Trans_Dte) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-BEF = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-BEF = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-BEF = BFRE-IMGE-ID AND CNTRCT-PEND-CDE = '0'
                            && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.equals(iaa_Cpr_Trans_Bfre_Imge_Id) 
                            && iaa_Cpr_Trans_Cntrct_Pend_Cde.equals("0")))
                        {
                            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                            sub_Process_Fund_Rcrd();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IGNORE
        //*  SE
        //*  MOVE 'F'  TO #EVENT-IND-OUT
        //*  PERFORM PROCESS-FUND-RCRD
        //*  D-IF
    }
    private void sub_Event_T40_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T40-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(1) || pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2)))                                            //Natural: IF #PAYEE-CDE-IN = 01 OR #PAYEE-CDE-IN = 02
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
            sub_Process_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.greater(2)))                                                                                                //Natural: IF #PAYEE-CDE-IN > 2
        {
            pnd_Before_Found.reset();                                                                                                                                     //Natural: RESET #BEFORE-FOUND #WORK-CPR-BEFORE-KEY
            pnd_Work_Cpr_Before_Key.reset();
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                         //Natural: MOVE #TRANS-DTE-IN TO #WORK-CPR-TRANS-DTE-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                    //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                  //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.setValue("1");                                                                                               //Natural: MOVE '1' TO #WORK-CPR-IMGE-ID-BEF
            vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #WORK-CPR-BEFORE-KEY
            (
            "READ17",
            new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Work_Cpr_Before_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
            1
            );
            READ17:
            while (condition(vw_iaa_Cpr_Trans.readNextRow("READ17")))
            {
                if (condition(pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.equals(iaa_Cpr_Trans_Trans_Dte) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-BEF = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-BEF = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-BEF = BFRE-IMGE-ID AND CNTRCT-PEND-CDE = '0'
                    && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.equals(iaa_Cpr_Trans_Bfre_Imge_Id) 
                    && iaa_Cpr_Trans_Cntrct_Pend_Cde.equals("0")))
                {
                    pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                              //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                    sub_Process_Fund_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T52_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T52-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                          //Natural: MOVE 'L' TO #EVENT-IND-OUT
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ18",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ18:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ18")))
        {
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                    //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Event_T53_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T53-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                          //Natural: MOVE 'F' TO #EVENT-IND-OUT
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ19",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ19:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ19")))
        {
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                            //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                    //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Event_T66_Rtn() throws Exception                                                                                                                     //Natural: EVENT-T66-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Work_Cpr_After_Key.reset();                                                                                                                                   //Natural: RESET #WORK-CPR-AFTER-KEY #WORK-CPR-BEFORE-KEY #WORK-CONTRCT-BEF-KEY
        pnd_Work_Cpr_Before_Key.reset();
        pnd_Work_Contrct_Bef_Key.reset();
        if (condition(pnd_Contract_Record_In_Pnd_Trns_Act_Code_In.equals("R")))                                                                                           //Natural: IF #TRNS-ACT-CODE-IN = 'R'
        {
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                        //Natural: MOVE #TRANS-DTE-IN TO #WORK-CONTRCT-TRANS-DTE
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                      //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-BEF
            pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.setValue("1");                                                                                         //Natural: MOVE '1' TO #WORK-CONTRCT-BFRE-IMGE-ID
            vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #WORK-CONTRCT-BEF-KEY
            (
            "READ20",
            new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Work_Contrct_Bef_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") },
            1
            );
            READ20:
            while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ20")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(1)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 01
            {
                if (condition(pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.equals(iaa_Cntrct_Trans_Trans_Dte) && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-TRANS-DTE = IAA-CNTRCT-TRANS.TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-BEF = IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR AND #WORK-CONTRCT-BFRE-IMGE-ID = IAA-CNTRCT-TRANS.BFRE-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE = 0
                    && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.equals(iaa_Cntrct_Trans_Bfre_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.equals(getZero())))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                              //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                    sub_Process_Fund_Rcrd();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 02
            {
                if (condition(pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Trans_Dte.equals(iaa_Cntrct_Trans_Trans_Dte) && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Ppcn_Nbr_Bef.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-TRANS-DTE = IAA-CNTRCT-TRANS.TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-BEF = IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR AND #WORK-CONTRCT-BFRE-IMGE-ID = IAA-CNTRCT-TRANS.BFRE-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE > 0 AND CNTRCT-SCND-ANNT-DOD-DTE > 0
                    && pnd_Work_Contrct_Bef_Key_Pnd_Work_Contrct_Bfre_Imge_Id.equals(iaa_Cntrct_Trans_Bfre_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero()) 
                    && iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))
                {
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                               //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CPR-INVRS-DTE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                             //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                           //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.setValue("2");                                                                                        //Natural: MOVE '2' TO #WORK-CPR-IMGE-ID-AFT
                    vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #WORK-CPR-AFTER-KEY
                    (
                    "READ21",
                    new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Work_Cpr_After_Key, WcType.BY) },
                    new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
                    1
                    );
                    READ21:
                    while (condition(vw_iaa_Cpr_Trans.readNextRow("READ21")))
                    {
                        if (condition(pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.equals(iaa_Cpr_Trans_Invrse_Trans_Dte) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-AFT = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-AFT = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-AFT = AFTR-IMGE-ID AND CNTRCT-PEND-CDE = '0'
                            && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.equals(iaa_Cpr_Trans_Aftr_Imge_Id) 
                            && iaa_Cpr_Trans_Cntrct_Pend_Cde.equals("0")))
                        {
                            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                            sub_Process_Fund_Rcrd();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(1)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 01
            {
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CONTRCT-INVRSE-DTE
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                  //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-AFT
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id.setValue("2");                                                                                      //Natural: MOVE '2' TO #WORK-CONTRCT-AFT-IMGE-ID
                vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #WORK-CONTRCT-AFT-KEY
                (
                "READ22",
                new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_Work_Contrct_Aft_Key, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
                1
                );
                READ22:
                while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ22")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte.equals(iaa_Cntrct_Trans_Invrse_Trans_Dte) && pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-INVRSE-DTE = IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-AFT = IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR AND #WORK-CONTRCT-AFT-IMGE-ID = IAA-CNTRCT-TRANS.AFTR-IMGE-ID
                    && pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id.equals(iaa_Cntrct_Trans_Aftr_Imge_Id)))
                {
                    if (condition(iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                         //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE > 0
                    {
                        pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                          //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                        sub_Process_Fund_Rcrd();
                        if (condition(Global.isEscape())) {return;}
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        DbsUtil.callnat(Iaan605.class , getCurrentProcessState(), pnd_Contract_Record_In_Pnd_Contract_In, pnd_Contract_Record_In_Pnd_Eff_Date_In,         //Natural: CALLNAT 'IAAN605' USING #CONTRACT-IN #EFF-DATE-IN CNTRCT-FIRST-ANNT-DOD-DTE
                            iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte);
                        if (condition(Global.isEscape())) return;
                        if (condition(iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                     //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE > 0
                        {
                            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                            sub_Process_Fund_Rcrd();
                            if (condition(Global.isEscape())) {return;}
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 02
            {
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CONTRCT-INVRSE-DTE
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                  //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-AFT
                pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id.setValue("2");                                                                                      //Natural: MOVE '2' TO #WORK-CONTRCT-AFT-IMGE-ID
                vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #WORK-CONTRCT-AFT-KEY
                (
                "READ23",
                new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_Work_Contrct_Aft_Key, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") },
                1
                );
                READ23:
                while (condition(vw_iaa_Cntrct_Trans.readNextRow("READ23")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte.equals(iaa_Cntrct_Trans_Invrse_Trans_Dte) && pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft.equals(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-CONTRCT-INVRSE-DTE = IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE AND #WORK-CONTRCT-PPCN-NBR-AFT = IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR AND #WORK-CONTRCT-AFT-IMGE-ID = IAA-CNTRCT-TRANS.AFTR-IMGE-ID AND CNTRCT-FIRST-ANNT-DOD-DTE > 0 AND CNTRCT-SCND-ANNT-DOD-DTE > 0
                    && pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id.equals(iaa_Cntrct_Trans_Aftr_Imge_Id) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero()) 
                    && iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))
                {
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                               //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CPR-INVRS-DTE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                             //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                           //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-AFT
                    pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.setValue("2");                                                                                        //Natural: MOVE '2' TO #WORK-CPR-IMGE-ID-AFT
                    vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #WORK-CPR-AFTER-KEY
                    (
                    "READ24",
                    new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Work_Cpr_After_Key, WcType.BY) },
                    new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
                    1
                    );
                    READ24:
                    while (condition(vw_iaa_Cpr_Trans.readNextRow("READ24")))
                    {
                        if (condition(pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.equals(iaa_Cpr_Trans_Invrse_Trans_Dte) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-AFT = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-AFT = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-AFT = AFTR-IMGE-ID AND CNTRCT-PEND-CDE = '0'
                            && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.equals(iaa_Cpr_Trans_Aftr_Imge_Id) 
                            && iaa_Cpr_Trans_Cntrct_Pend_Cde.equals("0")))
                        {
                            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
                            sub_Process_Fund_Rcrd();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T102_Rtn() throws Exception                                                                                                                    //Natural: EVENT-T102-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_After_Found.reset();                                                                                                                                          //Natural: RESET #AFTER-FOUND #BEFORE-FOUND #WORK-CPR-AFTER-KEY #WORK-CPR-BEFORE-KEY
        pnd_Before_Found.reset();
        pnd_Work_Cpr_After_Key.reset();
        pnd_Work_Cpr_Before_Key.reset();
        //*  OVE #EFF-DATE-IN   TO #WORK-TRANS-DTE-BEF
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                             //Natural: MOVE #TRANS-DTE-IN TO #WORK-CPR-TRANS-DTE-BEF
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                        //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-BEF
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                      //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-BEF
        //*  VE #FUND-ID-IN-WORK TO #WORK-FUND-BEF
        pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.setValue("1");                                                                                                   //Natural: MOVE '1' TO #WORK-CPR-IMGE-ID-BEF
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                           //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CPR-INVRS-DTE-AFT
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                         //Natural: MOVE #CONTRACT-IN TO #WORK-CPR-CNTRCT-PPCN-NBR-AFT
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                       //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CPR-CNTRCT-PAYEE-CDE-AFT
        //* *VE #FUND-ID-IN-WORK TO #WORK-FUND-AFT
        pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.setValue("2");                                                                                                    //Natural: MOVE '2' TO #WORK-CPR-IMGE-ID-AFT
        pnd_C_Tr_Found.reset();                                                                                                                                           //Natural: RESET #C-TR-FOUND
                                                                                                                                                                          //Natural: PERFORM READ-CPR-REC
        sub_Read_Cpr_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In.equals("66U")))                                                                                        //Natural: IF #TRANS-SUB-CODE-IN = '66U'
        {
            //*  IF DECEASED IS SECOND ANNUITANT
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 02
            {
                //*  DO NOT CREATE FIRST DOLLAR
                                                                                                                                                                          //Natural: PERFORM CHECK-PAYEE-02
                sub_Check_Payee_02();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_C_Tr_Found.equals(true) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                                               //Natural: IF #C-TR-FOUND = TRUE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
            sub_Process_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_After_Found.equals(true) && pnd_Hold_After_Pend_Code.equals("R") && pnd_Hold_Before_Pend_Code.notEquals("A") && pnd_Hold_Before_Pend_Code.notEquals("C")  //Natural: IF #AFTER-FOUND = TRUE AND #HOLD-AFTER-PEND-CODE = 'R' AND #HOLD-BEFORE-PEND-CODE NE 'A' AND #HOLD-BEFORE-PEND-CODE NE 'C' AND #HOLD-BEFORE-PEND-CODE NE 'R'
            && pnd_Hold_Before_Pend_Code.notEquals("R")))
        {
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                      //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND-RCRD
            sub_Process_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Before_Found.equals(true) && pnd_After_Found.equals(true) && pnd_Hold_After_Pend_Code.equals("0") && (pnd_Hold_Before_Pend_Code.equals("A")     //Natural: IF #BEFORE-FOUND = TRUE AND #AFTER-FOUND = TRUE AND #HOLD-AFTER-PEND-CODE = '0' AND ( #HOLD-BEFORE-PEND-CODE = 'A' OR #HOLD-BEFORE-PEND-CODE = 'C' )
            || pnd_Hold_Before_Pend_Code.equals("C"))))
        {
            //*  IF DECEASED IS SECOND ANNUITANT
            if (condition(pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(2)))                                                                                             //Natural: IF #PAYEE-CDE-IN = 02
            {
                //*  DO NOT CREATE FIRST DOLLAR
                                                                                                                                                                          //Natural: PERFORM CHECK-PAYEE-02
                sub_Check_Payee_02();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_C_Tr_Found.equals(true) && iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                                               //Natural: IF #C-TR-FOUND = TRUE AND IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                      //Natural: MOVE 'F' TO #EVENT-IND-OUT
            pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                               //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
            pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                             //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
            pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                           //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
            pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                        //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
            vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                      //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
            (
            "READ25",
            new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
            );
            READ25:
            while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ25")))
            {
                if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                    && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
                {
                    pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                        //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
                    pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                    sub_Find_Cusip();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                    sub_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Event_T500_Rtn() throws Exception                                                                                                                    //Natural: EVENT-T500-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  ALTERNATE PROCESS  - NEW CODE
        pnd_F.reset();                                                                                                                                                    //Natural: RESET #F
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  BEFORE AND AFTER IMAGE EXIST FOR
            //*  FUND
            if (condition(pnd_B_Fnds.getValue(pnd_I).equals(pnd_A_Fnds.getValue("*"))))                                                                                   //Natural: IF #B-FNDS ( #I ) = #A-FNDS ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ALREADY LOADED - DUPLICATE
                if (condition(pnd_B_Fnds.getValue(pnd_I).equals(pnd_Lst_Dllr.getValue("*"))))                                                                             //Natural: IF #B-FNDS ( #I ) = #LST-DLLR ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Lst_Dllr.getValue(pnd_F).setValue(pnd_B_Fnds.getValue(pnd_I));                                                                                    //Natural: ASSIGN #LST-DLLR ( #F ) := #B-FNDS ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_F.reset();                                                                                                                                                    //Natural: RESET #F
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  BEFORE AND AFTER IMAGE EXIST FOR
            //*  FUND
            if (condition(pnd_A_Fnds.getValue(pnd_I).equals(pnd_B_Fnds.getValue("*"))))                                                                                   //Natural: IF #A-FNDS ( #I ) = #B-FNDS ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ALREADY LOADED - DUPLICATE
                if (condition(pnd_A_Fnds.getValue(pnd_I).equals(pnd_Frst_Dllr.getValue("*"))))                                                                            //Natural: IF #A-FNDS ( #I ) = #FRST-DLLR ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Frst_Dllr.getValue(pnd_F).setValue(pnd_A_Fnds.getValue(pnd_I));                                                                                   //Natural: ASSIGN #FRST-DLLR ( #F ) := #A-FNDS ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pnd_Frst_Dllr.getValue(pnd_I).notEquals(" ")))                                                                                                  //Natural: IF #FRST-DLLR ( #I ) NE ' '
            {
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Frst_Dllr.getValue(pnd_I));                                                                            //Natural: ASSIGN #FUND-ID-IN := #FRST-DLLR ( #I )
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("F");                                                                                                  //Natural: MOVE 'F' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      WRITE '****** WRITING FIRST DOLLAR   *********'
                //*      WRITE 'CONTRACT ' #CONTRACT-IN
                //*      WRITE 'PAYEE CD '#PAYEE-CDE-IN
                //*      WRITE 'FUND     ' #FRST-DLLR(#I)
                //*      WRITE '***************************************'
            }                                                                                                                                                             //Natural: END-IF
            //*   MARINA
            if (condition(pnd_Lst_Dllr.getValue(pnd_I).notEquals(" ")))                                                                                                   //Natural: IF #LST-DLLR ( #I ) NE ' '
            {
                pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Lst_Dllr.getValue(pnd_I));                                                                             //Natural: ASSIGN #FUND-ID-IN := #LST-DLLR ( #I )
                pnd_Contract_Record_Out_Pnd_Event_Ind_Out.setValue("L");                                                                                                  //Natural: MOVE 'L' TO #EVENT-IND-OUT
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
                sub_Find_Cusip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
                sub_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      WRITE '****** WRITING LAST DOLLAR    *********'
                //*      WRITE 'CONTRACT ' #CONTRACT-IN
                //*      WRITE 'PAYEE CD '#PAYEE-CDE-IN
                //*      WRITE 'FUND     ' #LST-DLLR(#I)
                //*      WRITE '***************************************'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Load_Bef_Aft_Table() throws Exception                                                                                                                //Natural: LOAD-BEF-AFT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  NEW CODE
        pnd_Before_Found.reset();                                                                                                                                         //Natural: RESET #BEFORE-FOUND #AFTER-FOUND #WORK-AFTER-KEY #WORK-BEFORE-KEY #I #B-FNDS ( * ) #A-FNDS ( * ) #FRST-DLLR ( * ) #LST-DLLR ( * )
        pnd_After_Found.reset();
        pnd_Work_After_Key.reset();
        pnd_Work_Before_Key.reset();
        pnd_I.reset();
        pnd_B_Fnds.getValue("*").reset();
        pnd_A_Fnds.getValue("*").reset();
        pnd_Frst_Dllr.getValue("*").reset();
        pnd_Lst_Dllr.getValue("*").reset();
        //*       #T
        //*       #BEF-AFT-TABLE
        pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.setValue(pnd_Contract_Record_In_Pnd_Trans_Dte_In);                                                                     //Natural: MOVE #TRANS-DTE-IN TO #WORK-TRANS-DTE-BEF
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-BEF
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                              //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-BEF
        //*  MOVE #FUND-ID-IN-WORK TO #WORK-FUND-BEF
        pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.setValue("1");                                                                                                           //Natural: MOVE '1' TO #WORK-IMGE-ID-BEF
        //*  OVE  '2'             TO #WORK-IMGE-ID-BEF
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                   //Natural: MOVE #INVERSE-DATE-IN TO #WORK-INVRS-DTE-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                 //Natural: MOVE #CONTRACT-IN TO #WORK-CNTRCT-PPCN-NBR-AFT
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                               //Natural: MOVE #PAYEE-CDE-IN TO #WORK-CNTRCT-PAYEE-CDE-AFT
        //*  MOVE #FUND-ID-IN-WORK TO #WORK-FUND-AFT
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.setValue("2");                                                                                                            //Natural: MOVE '2' TO #WORK-IMGE-ID-AFT
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #WORK-BEFORE-KEY
        (
        "READ26",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Work_Before_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") }
        );
        READ26:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ26")))
        {
            //*  WRITE 'TRANS READ CONTRACT ' TIAA-CNTRCT-PPCN-NBR
            //*  WRITE 'TRANS DATE ' TRANS-DTE
            //*  WRITE 'TRANS PAY CDE ' TIAA-CNTRCT-PAYEE-CDE
            //*  WRITE 'WORK CNTRACT  ' #WORK-CNTRCT-PPCN-NBR-BEF
            //*  WRITE 'WORK TRANS DT ' #WORK-TRANS-DTE-BEF
            //*  WRITE 'WORK PAY CODE ' #WORK-CNTRCT-PAYEE-CDE-BEF
            pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
            if (condition(pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.equals(iaa_Tiaa_Fund_Trans_Trans_Dte) && pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-BEF = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-BEF = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-BEF = BFRE-IMGE-ID
                && pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.equals(iaa_Tiaa_Fund_Trans_Bfre_Imge_Id)))
            {
                //*     #WORK-TIAA-COMP-FUND-CDE   NOT =    #FUND-CDE-TAB(*)
                //* **  #WORK-FUND-BEF             = #WORK-TIAA-COMP-FUND-CDE
                //*       ADD     1                     TO  #T
                //*       MOVE TIAA-CNTRCT-PPCN-NBR     TO  #CONTRACT-TAB  (#T)
                //*       MOVE TIAA-CNTRCT-PAYEE-CDE    TO  #PAYEE-CDE-TAB (#T)
                //*       MOVE #WORK-TIAA-COMP-FUND-CDE TO  #FUND-CDE-TAB  (#T)
                //*       WRITE 'TABLE CONTRACT ' #CONTRACT-TAB(#T)
                //*       WRITE 'FUND CODE      ' #FUND-CDE-TAB (#T)
                //* **********************
                //*  LOAD BEFORE IMAGES
                //*  RESET #I
                pnd_W_Fnd.reset();                                                                                                                                        //Natural: RESET #W-FND
                if (condition(DbsUtil.maskMatches(pnd_Work_Tiaa_Comp_Fund_Cde,"'T'") && iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt.notEquals(getZero())))                       //Natural: IF #WORK-TIAA-COMP-FUND-CDE = MASK ( 'T' ) AND TIAA-TOT-PER-AMT NOT = 0
                {
                    pnd_W_Fnd.setValue("1 ");                                                                                                                             //Natural: ASSIGN #W-FND := '1 '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (DbsUtil.maskMatches(pnd_Work_Tiaa_Comp_Fund_Cde,"'T'")) && iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1).notEquals(getZero())))         //Natural: IF #WORK-TIAA-COMP-FUND-CDE NOT = MASK ( 'T' ) AND TIAA-UNITS-CNT ( 1 ) NOT = 0
                {
                    pnd_W_Fnd.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                                            //Natural: ASSIGN #W-FND := #FND-2
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fnd.notEquals("   ")))                                                                                                                //Natural: IF #W-FND NOT = '   '
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pnd_B_Fnds.getValue(pnd_I).setValue(pnd_W_Fnd);                                                                                                       //Natural: ASSIGN #B-FNDS ( #I ) := #W-FND
                    //*      WRITE '****** BEFORE RECORD IN TABLE *********'
                    //*      WRITE 'CONTRACT ' #CONTRACT-IN
                    //*      WRITE 'PAYEE CD '#PAYEE-CDE-IN
                    //*      WRITE 'FUND     ' #W-FND
                    //*      WRITE '***************************************'
                }                                                                                                                                                         //Natural: END-IF
                //* **********************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ27",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") }
        );
        READ27:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ27")))
        {
            pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id)))
            {
                //*     #WORK-TIAA-COMP-FUND-CDE   NOT =    #FUND-CDE-TAB(*)
                //*       ADD     1                     TO  #T
                //*       MOVE TIAA-CNTRCT-PPCN-NBR     TO  #CONTRACT-TAB  (#T)
                //*       MOVE TIAA-CNTRCT-PAYEE-CDE    TO  #PAYEE-CDE-TAB (#T)
                //*       MOVE #WORK-TIAA-COMP-FUND-CDE TO  #FUND-CDE-TAB  (#T)
                //*       WRITE 'TABLE CONTRACT ' #CONTRACT-TAB(#T)
                //*       WRITE 'FUND CODE      ' #FUND-CDE-TAB (#T)
                //*  ELSE
                //* ***********************
                //*  LOAD AFTER IMAGES
                //*  RESET #I
                //*  IF #WORK-TIAA-COMP-FUND-CDE = MASK('T') /* FIXED
                //*    #W-FND := '1 '
                //*  ELSE
                //*   #W-FND := #FND-2
                //*  END-IF
                //*  ADD 1 TO #I
                //*  #A-FNDS(#I) := #W-FND
                pnd_W_Fnd.reset();                                                                                                                                        //Natural: RESET #W-FND
                if (condition(DbsUtil.maskMatches(pnd_Work_Tiaa_Comp_Fund_Cde,"'T'") && iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt.notEquals(getZero())))                       //Natural: IF #WORK-TIAA-COMP-FUND-CDE = MASK ( 'T' ) AND TIAA-TOT-PER-AMT NOT = 0
                {
                    pnd_W_Fnd.setValue("1 ");                                                                                                                             //Natural: ASSIGN #W-FND := '1 '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (DbsUtil.maskMatches(pnd_Work_Tiaa_Comp_Fund_Cde,"'T'")) && iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt.getValue(1).notEquals(getZero())))         //Natural: IF #WORK-TIAA-COMP-FUND-CDE NOT = MASK ( 'T' ) AND TIAA-UNITS-CNT ( 1 ) NOT = 0
                {
                    pnd_W_Fnd.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                                            //Natural: ASSIGN #W-FND := #FND-2
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fnd.notEquals("   ")))                                                                                                                //Natural: IF #W-FND NOT = '   '
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pnd_A_Fnds.getValue(pnd_I).setValue(pnd_W_Fnd);                                                                                                       //Natural: ASSIGN #A-FNDS ( #I ) := #W-FND
                    //*      WRITE '****** AFTER RECORD IN TABLE **********'
                    //*      WRITE 'CONTRACT ' #CONTRACT-IN
                    //*      WRITE 'PAYEE CD '#PAYEE-CDE-IN
                    //*      WRITE 'FUND     ' #W-FND
                    //*      WRITE '***************************************'
                }                                                                                                                                                         //Natural: END-IF
                //* **********************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Trans_Before_Rec() throws Exception                                                                                                             //Natural: READ-TRANS-BEFORE-REC
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-BFRE-KEY-2 STARTING FROM #WORK-BEFORE-KEY
        (
        "READ28",
        new Wc[] { new Wc("CREF_FUND_BFRE_KEY_2", ">=", pnd_Work_Before_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_BFRE_KEY_2", "ASC") },
        1
        );
        READ28:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ28")))
        {
            pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
            if (condition(pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef.equals(iaa_Tiaa_Fund_Trans_Trans_Dte) && pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-BEF = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-BEF = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-BEF = BFRE-IMGE-ID AND #WORK-FUND-BEF = #WORK-TIAA-COMP-FUND-CDE
                && pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef.equals(iaa_Tiaa_Fund_Trans_Bfre_Imge_Id) 
                && pnd_Work_Before_Key_Pnd_Work_Fund_Bef.equals(pnd_Work_Tiaa_Comp_Fund_Cde)))
            {
                //* **  #FUND-CDE-TAB(#T)          = #WORK-TIAA-COMP-FUND-CDE
                getReports().write(0, "FND BEF  CNTRCT PCCN NBR ",iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr);                                                              //Natural: WRITE 'FND BEF  CNTRCT PCCN NBR ' TIAA-CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Before_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #BEFORE-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ITE 'TRAMS PAYEE CODE                ' TIAA-CNTRCT-PAYEE-CDE
    }
    private void sub_Read_Trans_After_Rec() throws Exception                                                                                                              //Natural: READ-TRANS-AFTER-REC
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY-2 STARTING FROM #WORK-AFTER-KEY
        (
        "READ29",
        new Wc[] { new Wc("CREF_FUND_AFTR_KEY_2", ">=", pnd_Work_After_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_FUND_AFTR_KEY_2", "ASC") },
        1
        );
        READ29:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ29")))
        {
            pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);                                                                                //Natural: MOVE IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
            if (condition(pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft.equals(iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte) && pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr)  //Natural: IF #WORK-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CNTRCT-PPCN-NBR-AFT = TIAA-CNTRCT-PPCN-NBR AND #WORK-CNTRCT-PAYEE-CDE-AFT = TIAA-CNTRCT-PAYEE-CDE AND #WORK-IMGE-ID-AFT = AFTR-IMGE-ID AND #WORK-FUND-AFT = #WORK-TIAA-COMP-FUND-CDE
                && pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft.equals(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde) && pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft.equals(iaa_Tiaa_Fund_Trans_Aftr_Imge_Id) 
                && pnd_Work_After_Key_Pnd_Work_Fund_Aft.equals(pnd_Work_Tiaa_Comp_Fund_Cde)))
            {
                getReports().write(0, "FOUND IN AFTER IMAGE  ",iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr);                                                                 //Natural: WRITE 'FOUND IN AFTER IMAGE  ' TIAA-CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_After_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #AFTER-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ITE 'TRAMS PAYEE CODE                ' TIAA-CNTRCT-PAYEE-CDE
    }
    private void sub_Read_Cpr_Rec() throws Exception                                                                                                                      //Natural: READ-CPR-REC
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #WORK-CPR-BEFORE-KEY
        (
        "READ30",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Work_Cpr_Before_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") },
        1
        );
        READ30:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("READ30")))
        {
            if (condition(pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Trans_Dte_Bef.equals(iaa_Cpr_Trans_Trans_Dte) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-TRANS-DTE-BEF = TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-BEF = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-BEF = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-BEF = BFRE-IMGE-ID
                && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Bef.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_Before_Key_Pnd_Work_Cpr_Imge_Id_Bef.equals(iaa_Cpr_Trans_Bfre_Imge_Id)))
            {
                pnd_Before_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #BEFORE-FOUND := TRUE
                pnd_Hold_Before_Pend_Code.setValue(iaa_Cpr_Trans_Cntrct_Pend_Cde);                                                                                        //Natural: MOVE CNTRCT-PEND-CDE TO #HOLD-BEFORE-PEND-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #WORK-CPR-AFTER-KEY
        (
        "READ31",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Work_Cpr_After_Key, WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") },
        1
        );
        READ31:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("READ31")))
        {
            if (condition(pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Invrs_Dte_Aft.equals(iaa_Cpr_Trans_Invrse_Trans_Dte) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Ppcn_Nbr_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr)  //Natural: IF #WORK-CPR-INVRS-DTE-AFT = INVRSE-TRANS-DTE AND #WORK-CPR-CNTRCT-PPCN-NBR-AFT = CNTRCT-PART-PPCN-NBR AND #WORK-CPR-CNTRCT-PAYEE-CDE-AFT = CNTRCT-PART-PAYEE-CDE AND #WORK-CPR-IMGE-ID-AFT = AFTR-IMGE-ID
                && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Cntrct_Payee_Cde_Aft.equals(iaa_Cpr_Trans_Cntrct_Part_Payee_Cde) && pnd_Work_Cpr_After_Key_Pnd_Work_Cpr_Imge_Id_Aft.equals(iaa_Cpr_Trans_Aftr_Imge_Id)))
            {
                pnd_After_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #AFTER-FOUND := TRUE
                pnd_Hold_After_Pend_Code.setValue(iaa_Cpr_Trans_Cntrct_Pend_Cde);                                                                                         //Natural: MOVE CNTRCT-PEND-CDE TO #HOLD-AFTER-PEND-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Fund_Rcrd() throws Exception                                                                                                                 //Natural: PROCESS-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                                  //Natural: MOVE #CONTRACT-IN TO #HOLD-PPCN-NBR
        pnd_Hold_Key_Pnd_Hold_Payee_Cde.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                                                //Natural: MOVE #PAYEE-CDE-IN TO #HOLD-PAYEE-CDE
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #HOLD-KEY
        (
        "READ32",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Hold_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ32:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ32")))
        {
            if (condition((iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Hold_Key_Pnd_Hold_Ppcn_Nbr)) || (iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Hold_Key_Pnd_Hold_Payee_Cde)))) //Natural: IF ( TIAA-CNTRCT-PPCN-NBR NE #HOLD-PPCN-NBR ) OR ( TIAA-CNTRCT-PAYEE-CDE NE #HOLD-PAYEE-CDE )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("U41") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("W41")))                                  //Natural: IF TIAA-CMPNY-FUND-CDE = 'U41' OR TIAA-CMPNY-FUND-CDE = 'W41'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1G") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1S") || iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.equals("T1 "))) //Natural: IF TIAA-CMPNY-FUND-CDE = 'T1G' OR TIAA-CMPNY-FUND-CDE = 'T1S' OR TIAA-CMPNY-FUND-CDE = 'T1 '
            {
                iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.setValue("T1");                                                                                                    //Natural: MOVE 'T1' TO TIAA-CMPNY-FUND-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Tiaa_Comp_Fund_Cde.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);                                                                                 //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #WORK-TIAA-COMP-FUND-CDE
            pnd_Contract_Record_In_Pnd_Fund_Id_In.setValue(pnd_Work_Tiaa_Comp_Fund_Cde_Pnd_Fnd_2);                                                                        //Natural: MOVE #FND-2 TO #FUND-ID-IN
                                                                                                                                                                          //Natural: PERFORM FIND-CUSIP
            sub_Find_Cusip();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-RTN
            sub_Write_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Rtn() throws Exception                                                                                                                         //Natural: WRITE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Contract_Record_In_Pnd_Fund_Id_In.equals("41")))                                                                                                //Natural: IF #FUND-ID-IN = '41'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract_Record_Out_Pnd_Plan_Num_Out.setValue(pnd_Contract_Record_In_Pnd_Plan_Num_In);                                                                        //Natural: MOVE #PLAN-NUM-IN TO #PLAN-NUM-OUT
        pnd_Contract_Record_Out_Pnd_Ssn_Out.setValue(pnd_Contract_Record_In_Pnd_Ssn_In);                                                                                  //Natural: MOVE #SSN-IN TO #SSN-OUT
        if (condition(pnd_Contract_Record_In_Pnd_Fund_Id_In.equals("1 ")))                                                                                                //Natural: IF #FUND-ID-IN = '1 '
        {
            pnd_Contract_Record_Out_Pnd_Fund_Id_Out.setValue("T1");                                                                                                       //Natural: MOVE 'T1' TO #FUND-ID-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Record_Out_Pnd_Fund_Id_Out.setValue(pnd_Contract_Record_In_Pnd_Fund_Id_In);                                                                      //Natural: MOVE #FUND-ID-IN TO #FUND-ID-OUT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract_Record_Out_Pnd_Eff_Date_Out.setValue(pnd_Contract_Record_In_Pnd_Eff_Date_In);                                                                        //Natural: MOVE #EFF-DATE-IN TO #EFF-DATE-OUT
        pnd_Contract_Record_Out_Pnd_Inverse_Date_Out.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                                //Natural: MOVE #INVERSE-DATE-IN TO #INVERSE-DATE-OUT
        pnd_Contract_Record_Out_Pnd_Contract_Out.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                                        //Natural: MOVE #CONTRACT-IN TO #CONTRACT-OUT
        pnd_Contract_Record_Out_Pnd_Payee_Cde_Out.setValue(pnd_Contract_Record_In_Pnd_Payee_Cde_In);                                                                      //Natural: MOVE #PAYEE-CDE-IN TO #PAYEE-CDE-OUT
        pnd_Contract_Record_Out_Pnd_Cusip_Out.setValue(pnd_Contract_Record_In_Pnd_Cusip_In);                                                                              //Natural: MOVE #CUSIP-IN TO #CUSIP-OUT
        pnd_Contract_Record_Out_Pnd_Plan_Type_Out.setValue(pnd_Contract_Record_In_Pnd_Plan_Type_In);                                                                      //Natural: MOVE #PLAN-TYPE-IN TO #PLAN-TYPE-OUT
        pnd_Contract_Record_Out_Pnd_Source_App_Id_Out.setValue(pnd_Contract_Record_In_Pnd_Source_App_Id_In);                                                              //Natural: MOVE #SOURCE-APP-ID-IN TO #SOURCE-APP-ID-OUT
        pnd_Contract_Record_Out_Pnd_Ia_Orig_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In);                                                                //Natural: MOVE #IA-ORIG-CODE-IN TO #IA-ORIG-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Ia_Opt_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In);                                                                  //Natural: MOVE #IA-OPT-CODE-IN TO #IA-OPT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Ia_Settlement_Id_Out.setValue(pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In);                                                        //Natural: MOVE #IA-SETTLEMENT-ID-IN TO #IA-SETTLEMENT-ID-OUT
        pnd_Contract_Record_Out_Pnd_Trans_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Trans_Code_In);                                                                    //Natural: MOVE #TRANS-CODE-IN TO #TRANS-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Act_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Act_Code_In);                                                                        //Natural: MOVE #ACT-CODE-IN TO #ACT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Trns_Act_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Trns_Act_Code_In);                                                              //Natural: MOVE #TRNS-ACT-CODE-IN TO #TRNS-ACT-CODE-OUT
        pnd_Contract_Record_Out_Pnd_Pend_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Pend_Code_In);                                                                      //Natural: MOVE #PEND-CODE-IN TO #PEND-CODE-OUT
        pnd_Contract_Record_Out_Pnd_First_Ann_Dod_Out.setValue(pnd_Contract_Record_In_Pnd_First_Ann_Dod_In);                                                              //Natural: MOVE #FIRST-ANN-DOD-IN TO #FIRST-ANN-DOD-OUT
        pnd_Contract_Record_Out_Pnd_Second_Ann_Dod_Out.setValue(pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In);                                                            //Natural: MOVE #SECOND-ANN-DOD-IN TO #SECOND-ANN-DOD-OUT
        pnd_Contract_Record_Out_Pnd_Trans_Sub_Code_Out.setValue(pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In);                                                            //Natural: MOVE #TRANS-SUB-CODE-IN TO #TRANS-SUB-CODE-OUT
        getWorkFiles().write(2, false, pnd_Contract_Record_Out);                                                                                                          //Natural: WRITE WORK FILE 2 #CONTRACT-RECORD-OUT
    }
    private void sub_Check_Payee_02() throws Exception                                                                                                                    //Natural: CHECK-PAYEE-02
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Invrse_Dte.setValue(pnd_Contract_Record_In_Pnd_Inverse_Date_In);                                                        //Natural: MOVE #INVERSE-DATE-IN TO #WORK-CONTRCT-INVRSE-DTE
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Ppcn_Nbr_Aft.setValue(pnd_Contract_Record_In_Pnd_Contract_In);                                                          //Natural: MOVE #CONTRACT-IN TO #WORK-CONTRCT-PPCN-NBR-AFT
        pnd_Work_Contrct_Aft_Key_Pnd_Work_Contrct_Aft_Imge_Id.setValue("2");                                                                                              //Natural: MOVE '2' TO #WORK-CONTRCT-AFT-IMGE-ID
        vw_iaa_Cntrct_Trans.startDatabaseFind                                                                                                                             //Natural: FIND IAA-CNTRCT-TRANS WITH CNTRCT-AFTR-KEY = #WORK-CONTRCT-AFT-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", "=", pnd_Work_Contrct_Aft_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("FIND01")))
        {
            vw_iaa_Cntrct_Trans.setIfNotFoundControlFlag(false);
            pnd_C_Tr_Found.setValue(true);                                                                                                                                //Natural: ASSIGN #C-TR-FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
