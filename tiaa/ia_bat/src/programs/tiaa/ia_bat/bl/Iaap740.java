/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:29 PM
**        * FROM NATURAL PROGRAM : Iaap740
************************************************************
**        * FILE NAME            : Iaap740.java
**        * CLASS NAME           : Iaap740
**        * INSTANCE NAME        : Iaap740
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP740 - PIN COMPARE UPDATE                      *
*                                                                    *
*   05/27/2010 OS REPLACED COR EXTRACT WITH ACCESS TO COR. SC 052710.*
*   08/04/2015 JT COR/NAAD SUNSET. USE ETL INSTEAD OF DIRECT COR     *
*                 ACCESS. SC 08/15
*   08/2017    RC PIN EXPANSION - SC 082017
*   08/2017    OS PIN EXPANSION CHANGE MARKED OS 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap740 extends BLNatBase
{
    // Data Areas
    private LdaIaal200b ldaIaal200b;
    private LdaIaal202g ldaIaal202g;
    private LdaIaal202h ldaIaal202h;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_251;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr_A;
    private DbsField pnd_Input__Filler14;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input__Filler15;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler16;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input__Filler17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input__Filler18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input__Filler19;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input__Filler20;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_51;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Core;

    private DbsGroup pnd_Core__R_Field_14;
    private DbsField pnd_Core_Cntrct_Payee;

    private DbsGroup pnd_Core__R_Field_15;
    private DbsField pnd_Core_Cntrct_Nbr;
    private DbsField pnd_Core_Cntrct_Payee_Cde;
    private DbsField pnd_Core_Ph_Unique_Id_Nbr;
    private DbsField pnd_Core_Ph_Rcd_Type_Cde;
    private DbsField pnd_Core_Ph_Social_Security_No;
    private DbsField pnd_Core_Ph_Dob_Dte;
    private DbsField pnd_Core_Ph_Dod_Dte;

    private DbsGroup pnd_Core_Ph_Nme;
    private DbsField pnd_Core_Ph_Last_Nme;

    private DbsGroup pnd_Core__R_Field_16;
    private DbsField pnd_Core_Ph_Last_Nme_16;
    private DbsField pnd_Core_Ph_First_Nme;

    private DbsGroup pnd_Core__R_Field_17;
    private DbsField pnd_Core_Ph_First_Nme_10;
    private DbsField pnd_Core_Ph_Mddle_Nme;

    private DbsGroup pnd_Core__R_Field_18;
    private DbsField pnd_Core_Ph_Mddle_Nme_12;
    private DbsField pnd_Core_Pnd_Cor_Status_Cde;
    private DbsField pnd_Core_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Cor_Found;
    private DbsField pnd_Cor_Cnt;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_19;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Check_Date_A;

    private DbsGroup pnd_Check_Date_A__R_Field_20;
    private DbsField pnd_Check_Date_A_Pnd_Check_Date;
    private DbsField pnd_Todays_Date_A;

    private DbsGroup pnd_Todays_Date_A__R_Field_21;
    private DbsField pnd_Todays_Date_A_Pnd_Todays_Date;
    private DbsField pnd_Time;
    private DbsField pnd_Input_Eof;
    private DbsField pnd_Core_Eof;
    private DbsField pnd_Save_First_Dod;
    private DbsField pnd_Save_Scnd_Dod;
    private DbsField pnd_Count1;
    private DbsField pnd_Count2;
    private DbsField pnd_Ctr;
    private DbsField pnd_Ph_Unique_Id_Nbr_01;
    private DbsField pnd_Cor_Cntrct_01;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal202g = new LdaIaal202g();
        registerRecord(ldaIaal202g);
        registerRecord(ldaIaal202g.getVw_iaa_Trans_Rcrd());
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_251 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_251", "#REST-OF-RECORD-251", FieldType.STRING, 251);

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_251);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_251);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Input__Filler1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input__Filler2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 12);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input__Filler3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 21);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler8 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 10);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);

        pnd_Input__R_Field_7 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_251);
        pnd_Input__Filler9 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_7.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input__Filler10 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input__Filler11 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_9 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_251);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler12 = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler13 = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 7, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_10 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_251);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Cpr_Id_Nbr);
        pnd_Input_Pnd_Cpr_Id_Nbr_A = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr_A", "#CPR-ID-NBR-A", FieldType.STRING, 12);
        pnd_Input__Filler14 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler14", "_FILLER14", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input__Filler15 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler15", "_FILLER15", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler16 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler16", "_FILLER16", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input__Filler17 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler17", "_FILLER17", FieldType.STRING, 10);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_10.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler18 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler18", "_FILLER18", FieldType.STRING, 140);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input__Filler19 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler19", "_FILLER19", FieldType.STRING, 35);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input__Filler20 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler20", "_FILLER20", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Rest_Of_Record_51 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_51", "#REST-OF-RECORD-51", FieldType.STRING, 98);

        pnd_Input__R_Field_13 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_51);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Core = localVariables.newFieldInRecord("pnd_Core", "#CORE", FieldType.STRING, 150);

        pnd_Core__R_Field_14 = localVariables.newGroupInRecord("pnd_Core__R_Field_14", "REDEFINE", pnd_Core);
        pnd_Core_Cntrct_Payee = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Core__R_Field_15 = pnd_Core__R_Field_14.newGroupInGroup("pnd_Core__R_Field_15", "REDEFINE", pnd_Core_Cntrct_Payee);
        pnd_Core_Cntrct_Nbr = pnd_Core__R_Field_15.newFieldInGroup("pnd_Core_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Core_Cntrct_Payee_Cde = pnd_Core__R_Field_15.newFieldInGroup("pnd_Core_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Core_Ph_Unique_Id_Nbr = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Core_Ph_Rcd_Type_Cde = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Ph_Rcd_Type_Cde", "PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Ph_Social_Security_No = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Ph_Social_Security_No", "PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 
            9);
        pnd_Core_Ph_Dob_Dte = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Ph_Dob_Dte", "PH-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Core_Ph_Dod_Dte = pnd_Core__R_Field_14.newFieldInGroup("pnd_Core_Ph_Dod_Dte", "PH-DOD-DTE", FieldType.NUMERIC, 8);

        pnd_Core_Ph_Nme = pnd_Core__R_Field_14.newGroupInGroup("pnd_Core_Ph_Nme", "PH-NME");
        pnd_Core_Ph_Last_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Last_Nme", "PH-LAST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_16 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_16", "REDEFINE", pnd_Core_Ph_Last_Nme);
        pnd_Core_Ph_Last_Nme_16 = pnd_Core__R_Field_16.newFieldInGroup("pnd_Core_Ph_Last_Nme_16", "PH-LAST-NME-16", FieldType.STRING, 16);
        pnd_Core_Ph_First_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_First_Nme", "PH-FIRST-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_17 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_17", "REDEFINE", pnd_Core_Ph_First_Nme);
        pnd_Core_Ph_First_Nme_10 = pnd_Core__R_Field_17.newFieldInGroup("pnd_Core_Ph_First_Nme_10", "PH-FIRST-NME-10", FieldType.STRING, 10);
        pnd_Core_Ph_Mddle_Nme = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Ph_Mddle_Nme", "PH-MDDLE-NME", FieldType.STRING, 30);

        pnd_Core__R_Field_18 = pnd_Core_Ph_Nme.newGroupInGroup("pnd_Core__R_Field_18", "REDEFINE", pnd_Core_Ph_Mddle_Nme);
        pnd_Core_Ph_Mddle_Nme_12 = pnd_Core__R_Field_18.newFieldInGroup("pnd_Core_Ph_Mddle_Nme_12", "PH-MDDLE-NME-12", FieldType.STRING, 12);
        pnd_Core_Pnd_Cor_Status_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 1);
        pnd_Core_Pnd_Cor_Sex_Cde = pnd_Core_Ph_Nme.newFieldInGroup("pnd_Core_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Cor_Found = localVariables.newFieldInRecord("pnd_Cor_Found", "#COR-FOUND", FieldType.BOOLEAN, 1);
        pnd_Cor_Cnt = localVariables.newFieldInRecord("pnd_Cor_Cnt", "#COR-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_19", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_19.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_19.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Check_Date_A = localVariables.newFieldInRecord("pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 8);

        pnd_Check_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Check_Date_A__R_Field_20", "REDEFINE", pnd_Check_Date_A);
        pnd_Check_Date_A_Pnd_Check_Date = pnd_Check_Date_A__R_Field_20.newFieldInGroup("pnd_Check_Date_A_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Todays_Date_A = localVariables.newFieldInRecord("pnd_Todays_Date_A", "#TODAYS-DATE-A", FieldType.STRING, 8);

        pnd_Todays_Date_A__R_Field_21 = localVariables.newGroupInRecord("pnd_Todays_Date_A__R_Field_21", "REDEFINE", pnd_Todays_Date_A);
        pnd_Todays_Date_A_Pnd_Todays_Date = pnd_Todays_Date_A__R_Field_21.newFieldInGroup("pnd_Todays_Date_A_Pnd_Todays_Date", "#TODAYS-DATE", FieldType.NUMERIC, 
            8);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Input_Eof = localVariables.newFieldInRecord("pnd_Input_Eof", "#INPUT-EOF", FieldType.BOOLEAN, 1);
        pnd_Core_Eof = localVariables.newFieldInRecord("pnd_Core_Eof", "#CORE-EOF", FieldType.BOOLEAN, 1);
        pnd_Save_First_Dod = localVariables.newFieldInRecord("pnd_Save_First_Dod", "#SAVE-FIRST-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Scnd_Dod = localVariables.newFieldInRecord("pnd_Save_Scnd_Dod", "#SAVE-SCND-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Count1 = localVariables.newFieldInRecord("pnd_Count1", "#COUNT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Count2 = localVariables.newFieldInRecord("pnd_Count2", "#COUNT2", FieldType.PACKED_DECIMAL, 7);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Ph_Unique_Id_Nbr_01 = localVariables.newFieldInRecord("pnd_Ph_Unique_Id_Nbr_01", "#PH-UNIQUE-ID-NBR-01", FieldType.NUMERIC, 12);
        pnd_Cor_Cntrct_01 = localVariables.newFieldInRecord("pnd_Cor_Cntrct_01", "#COR-CNTRCT-01", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200b.initializeValues();
        ldaIaal202g.initializeValues();
        ldaIaal202h.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap740() throws Exception
    {
        super("Iaap740");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 132 PS = 54
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 132 PS = 54
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* *======================================================================
        //* *                      START OF PROGRAM
        //* *======================================================================
        getReports().write(0, "START OF IAAP740");                                                                                                                        //Natural: WRITE 'START OF IAAP740'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #GET-CONTROL-INFO
        sub_Pnd_Get_Control_Info();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER")       //Natural: IF #PPCN-NBR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet410 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
            if (condition((pnd_Input_Pnd_Record_Code.equals(10))))
            {
                decideConditionsMet410++;
                pnd_Save_First_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                 //Natural: ASSIGN #SAVE-FIRST-DOD := #INPUT.#FIRST-ANN-DOD
                pnd_Save_Scnd_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                   //Natural: ASSIGN #SAVE-SCND-DOD := #INPUT.#SCND-ANN-DOD
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Pnd_Record_Code.equals(20))))
            {
                decideConditionsMet410++;
                if (condition(pnd_Input_Pnd_Pend_Cde.notEquals("0") || pnd_Input_Pnd_Status_Cde.equals(9)))                                                               //Natural: IF #PEND-CDE NE '0' OR #STATUS-CDE = 9
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  052710 /* 08/15
                                                                                                                                                                          //Natural: PERFORM GET-CORE
                    sub_Get_Core();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *      PERFORM ACCESS-COR                       /* 052710 /* 08/15
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *BACKOUT TRANSACTION
        getReports().write(1, NEWLINE,"TOTAL:",pnd_Count1);                                                                                                               //Natural: WRITE ( 1 ) / 'TOTAL:' #COUNT1
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,"TOTAL:",pnd_Count2);                                                                                                               //Natural: WRITE ( 2 ) / 'TOTAL:' #COUNT2
        if (Global.isEscape()) return;
        //*  08/15 START
        //* ***************************************************052710**************
        //*  DEFINE SUBROUTINE ACCESS-COR
        //* ***********************************************************************
        //*  RESET #COR-FOUND
        //*  #CNTRCT-NBR := #INPUT.#PPCN-NBR
        //*  #CNTRCT-PAYEE-CDE := #INPUT.#PAYEE-CDE-A
        //*  READ COR-XREF-CNTRCT BY COR-SUPER-CNTRCT-PAYEE-PIN
        //*     STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
        //*   IF COR-XREF-CNTRCT.CNTRCT-NBR NE #CNTRCT-NBR
        //*       OR COR-XREF-CNTRCT.CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   ACCEPT IF COR-XREF-CNTRCT.CNTRCT-STATUS-CDE = ' '
        //*   #COR-FOUND := TRUE
        //*   IF #INPUT.#CPR-ID-NBR = COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*     IGNORE
        //*   ELSE
        //*  MAKE SURE COR CONTRACT/PAYEE IS UNIQUE. WE SHOULD NOT UPDATE THESE
        //*  SINCE WE ARE NOT SURE WHICH ONE IS CORRECT.
        //*     PERFORM CHECK-IF-ONLY-ONE
        //*     IF #COR-CNT GT 1
        //*       WRITE 'DUPLICATE CONTRACT/PAYEE IN COR'
        //*         #COR-SUPER-CNTRCT-PAYEE-PIN
        //*       ESCAPE BOTTOM
        //*     END-IF
        //*     IF #INPUT.#PAYEE-CDE = 02
        //*         AND #SAVE-SCND-DOD > 0
        //*         AND #INPUT.#CPR-ID-NBR GT 0
        //*       #COR-FOUND := FALSE
        //*     ELSE
        //*       ADD 1 TO #COUNT1
        //*       WRITE(1) 7X
        //*         #INPUT.#PPCN-NBR
        //*         #INPUT.#PAYEE-CDE 11X
        //*         #INPUT.#CPR-ID-NBR
        //*         COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*       PERFORM UPDATE-REC
        //*     END-IF
        //*   END-IF
        //*   ESCAPE BOTTOM
        //*  END-READ
        //*  IF NOT #COR-FOUND
        //*   IF #INPUT.#PAYEE-CDE = 02
        //* **    AND #SAVE-SCND-DOD > 0
        //*     #CNTRCT-PAYEE-CDE := '01'
        //*     READ COR-XREF-CNTRCT BY COR-SUPER-CNTRCT-PAYEE-PIN
        //*         STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
        //*       IF COR-XREF-CNTRCT.CNTRCT-NBR NE #CNTRCT-NBR
        //*           OR COR-XREF-CNTRCT.CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
        //*         ESCAPE BOTTOM
        //*       END-IF
        //*       ACCEPT IF COR-XREF-CNTRCT.CNTRCT-STATUS-CDE = ' '
        //*       #COR-FOUND := TRUE
        //*       IF #INPUT.#CPR-ID-NBR = COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*         IGNORE
        //*       ELSE
        //*         PERFORM CHECK-IF-ONLY-ONE
        //*         IF #COR-CNT GT 1
        //*           WRITE 'DUPLICATE CONTRACT/PAYEE IN COR'
        //*             #COR-SUPER-CNTRCT-PAYEE-PIN
        //*           ESCAPE BOTTOM
        //*         END-IF
        //*         ADD 1 TO #COUNT1
        //*         WRITE(1) 7X
        //*           #INPUT.#PPCN-NBR
        //*           #INPUT.#PAYEE-CDE 11X
        //*           #INPUT.#CPR-ID-NBR
        //*           COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*         PERFORM UPDATE-REC
        //*       END-IF
        //*       ESCAPE BOTTOM
        //*     END-READ
        //*   END-IF
        //*  END-IF
        //*  END-SUBROUTINE                                   /* 052710
        //* ***************************************************052710**************
        //*  DEFINE SUBROUTINE CHECK-IF-ONLY-ONE
        //* ***********************************************************************
        //*  RESET #COR-CNT
        //*  READ COR-XREF-2 BY COR-SUPER-CNTRCT-PAYEE-PIN
        //*     STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
        //*   IF COR-XREF-2.CNTRCT-NBR NE #CNTRCT-NBR
        //*       OR COR-XREF-2.CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   ACCEPT IF COR-XREF-2.CNTRCT-STATUS-CDE = ' '
        //*   ADD 1 TO #COR-CNT
        //*  END-READ
        //*  END-SUBROUTINE                                   /* 052710
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CORE
        //*  08/15 END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-REC
        //*  IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR :=
        //*    COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CONTROL-INFO
        //* **********************************************************************
    }
    private void sub_Get_Core() throws Exception                                                                                                                          //Natural: GET-CORE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Core_Eof.getBoolean()))                                                                                                                         //Natural: IF #CORE-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Core_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                     //Natural: IF #CORE.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Core);                                                                                                                             //Natural: READ WORK 2 ONCE #CORE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Core_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #CORE-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  08/15
            if (condition(pnd_Core_Pnd_Cor_Status_Cde.notEquals(" ")))                                                                                                    //Natural: IF #COR-STATUS-CDE NE ' '
            {
                //*  08/15
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  08/15
            }                                                                                                                                                             //Natural: END-IF
            //*  SAVE TO COMPARE WITH THE
            //*  02 CPR REC
            if (condition(pnd_Core_Cntrct_Payee_Cde.equals("01")))                                                                                                        //Natural: IF #CORE.CNTRCT-PAYEE-CDE = '01'
            {
                pnd_Ph_Unique_Id_Nbr_01.setValue(pnd_Core_Ph_Unique_Id_Nbr);                                                                                              //Natural: ASSIGN #PH-UNIQUE-ID-NBR-01 := #CORE.PH-UNIQUE-ID-NBR
                pnd_Cor_Cntrct_01.setValue(pnd_Core_Cntrct_Nbr);                                                                                                          //Natural: ASSIGN #COR-CNTRCT-01 := #CORE.CNTRCT-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Core_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                      //Natural: IF #CORE.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
            {
                //*  IS IT FIRST OR SCND ANNT ?
                //*  COMPARE 01 CORE REC
                if (condition(pnd_Input_Pnd_Payee_Cde.equals(2) && pnd_Save_Scnd_Dod.greater(getZero()) && pnd_Core_Cntrct_Nbr.equals(pnd_Cor_Cntrct_01)))                //Natural: IF #INPUT.#PAYEE-CDE = 02 AND #SAVE-SCND-DOD > 0 AND #CORE.CNTRCT-NBR = #COR-CNTRCT-01
                {
                    //*      IF #PH-UNIQUE-ID-NBR-01 = #CORE.PH-UNIQUE-ID-NBR /* 02 CORE
                    //*  CPR
                    if (condition(pnd_Ph_Unique_Id_Nbr_01.equals(pnd_Input_Pnd_Cpr_Id_Nbr)))                                                                              //Natural: IF #PH-UNIQUE-ID-NBR-01 = #INPUT.#CPR-ID-NBR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ********************* NO MATCH'
                        pnd_Count1.nadd(1);                                                                                                                               //Natural: ADD 1 TO #COUNT1
                        getReports().write(1, new ColumnSpacing(7),pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde,new ColumnSpacing(11),pnd_Ph_Unique_Id_Nbr_01,          //Natural: WRITE ( 1 ) 7X #INPUT.#PPCN-NBR #INPUT.#PAYEE-CDE 11X #PH-UNIQUE-ID-NBR-01 #CORE.PH-UNIQUE-ID-NBR
                            pnd_Core_Ph_Unique_Id_Nbr);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM UPDATE-REC
                        sub_Update_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM COMPARE-RECORDS
                    sub_Compare_Records();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  GET-CORE
    }
    private void sub_Compare_Records() throws Exception                                                                                                                   //Natural: COMPARE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Core_Ph_Unique_Id_Nbr.equals(pnd_Input_Pnd_Cpr_Id_Nbr) || pnd_Core_Ph_Unique_Id_Nbr.equals(getZero())))                                         //Natural: IF #CORE.PH-UNIQUE-ID-NBR = #INPUT.#CPR-ID-NBR OR #CORE.PH-UNIQUE-ID-NBR = 0
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Pnd_Payee_Cde.equals(1) && pnd_Input_Pnd_Cpr_Id_Nbr.greater(getZero())))                                                                  //Natural: IF #INPUT.#PAYEE-CDE = 01 AND #INPUT.#CPR-ID-NBR GT 0
        {
            //*  NO MATCH / NO UPDATE
            pnd_Count2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #COUNT2
            getReports().write(2, new ColumnSpacing(7),pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde,new ColumnSpacing(11),pnd_Input_Pnd_Cpr_Id_Nbr,pnd_Core_Ph_Unique_Id_Nbr); //Natural: WRITE ( 2 ) 7X #INPUT.#PPCN-NBR #INPUT.#PAYEE-CDE 11X #INPUT.#CPR-ID-NBR #CORE.PH-UNIQUE-ID-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  NO MATCH / UPDATE
            pnd_Count1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #COUNT1
            getReports().write(1, new ColumnSpacing(7),pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde,new ColumnSpacing(11),pnd_Input_Pnd_Cpr_Id_Nbr,pnd_Core_Ph_Unique_Id_Nbr); //Natural: WRITE ( 1 ) 7X #INPUT.#PPCN-NBR #INPUT.#PAYEE-CDE 11X #INPUT.#CPR-ID-NBR #CORE.PH-UNIQUE-ID-NBR
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM UPDATE-REC
            sub_Update_Rec();
            if (condition(Global.isEscape())) {return;}
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMPARE-RECORDS
    }
    private void sub_Update_Rec() throws Exception                                                                                                                        //Natural: UPDATE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  052710
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Input_Pnd_Cntrct_Payee, WcType.WITH) },
        1
        );
        F1:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("F1")))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr().setValue(pnd_Core_Ph_Unique_Id_Nbr);                                                                      //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR := #CORE.PH-UNIQUE-ID-NBR
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().updateDBRow("F1");                                                                                                //Natural: UPDATE ( F1. )
            pnd_Time.setValue(Global.getTIMX());                                                                                                                          //Natural: MOVE *TIMX TO #TIME
            ldaIaal202g.getVw_iaa_Trans_Rcrd().reset();                                                                                                                   //Natural: RESET IAA-TRANS-RCRD
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Time);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TIME
            ldaIaal202g.getIaa_Trans_Rcrd_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202g.getIaa_Trans_Rcrd_Invrse_Trans_Dte()), new                  //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := 1000000000000 - IAA-TRANS-RCRD.TRANS-DTE
                DbsDecimal("1000000000000").subtract(ldaIaal202g.getIaa_Trans_Rcrd_Trans_Dte()));
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                              //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #INPUT.#PPCN-NBR
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(pnd_Input_Pnd_Payee_Cde);                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #INPUT.#PAYEE-CDE
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Check_Date_A_Pnd_Check_Date);                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #CHECK-DATE
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Todays_Date_A_Pnd_Todays_Date);                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DATE
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Id().setValue("IAAP740");                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP740'
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue(" ");                                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := ' '
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Area().setValue(" ");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := ' '
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Cde().setValue(" ");                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-CDE := ' '
            if (condition(pnd_Input_Pnd_Cpr_Id_Nbr.greater(9999999)))                                                                                                     //Natural: IF #CPR-ID-NBR GT 9999999
            {
                ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Id().setValue(pnd_Input_Pnd_Cpr_Id_Nbr_A);                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-ID := #INPUT.#CPR-ID-NBR-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Id().setValue(pnd_Input_Pnd_Cpr_Id_Nbr_A.getSubstring(6,7));                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-ID := SUBSTR ( #INPUT.#CPR-ID-NBR-A,6,7 )
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Wpid().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-WPID := ' '
            ldaIaal202g.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TIME
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr().setValue(0);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-ID-NBR := 0
            ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cde().setValue(800);                                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 800
            ldaIaal202g.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                             //Natural: STORE IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *BACKOUT TRANSACTION
        //*  UPDATE-REC
    }
    private void sub_Pnd_Get_Control_Info() throws Exception                                                                                                              //Natural: #GET-CONTROL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "RD1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RD1:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("RD1")))
        {
            pnd_Check_Date_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.reset();                                                                                                                  //Natural: RESET #CNTRL-INVRSE-DTE
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("DC");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'DC'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "RD2",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RD2:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("RD2")))
        {
            pnd_Todays_Date_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DATE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "CHECK DATE =====>",pnd_Check_Date_A_Pnd_Check_Date);                                                                                       //Natural: WRITE 'CHECK DATE =====>' #CHECK-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "BUSINESS DATE ==>",pnd_Todays_Date_A_Pnd_Todays_Date);                                                                                     //Natural: WRITE 'BUSINESS DATE ==>' #TODAYS-DATE
        if (Global.isEscape()) return;
        //*  #GET-CONTROL-INFO
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(25),"IA ADMINISTRATION UNIQUE ID UPDATE",NEWLINE,"DATE   ",Global.getDATX(),  //Natural: WRITE ( 1 ) // 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION UNIQUE ID UPDATE' / 'DATE   ' *DATX ( EM = MM/DD/YYYY ) // '       ----  CONTRACT ----       -------- UNIQUE ID --------' / '         NUMBER PAYEE               PREVIOUS        NEW' / '       -------------------       ----------------------------'
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"       ----  CONTRACT ----       -------- UNIQUE ID --------",NEWLINE,"         NUMBER PAYEE               PREVIOUS        NEW",
                        NEWLINE,"       -------------------       ----------------------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(25),"IA ADMINISTRATION UNIQUE ID UPDATE",NEWLINE,"DATE   ",Global.getDATX(),  //Natural: WRITE ( 2 ) // 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION UNIQUE ID UPDATE' / 'DATE   ' *DATX ( EM = MM/DD/YYYY ) / 'PAYEE 01 WARNING REPORT - CPR NOT UPDATED' // '       ----  CONTRACT ----       --------- UNIQUE ID --------' / '         NUMBER PAYEE               PREVIOUS        NEW' / '       -------------------       ----------------------------'
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"PAYEE 01 WARNING REPORT - CPR NOT UPDATED",NEWLINE,NEWLINE,"       ----  CONTRACT ----       --------- UNIQUE ID --------",
                        NEWLINE,"         NUMBER PAYEE               PREVIOUS        NEW",NEWLINE,"       -------------------       ----------------------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=54");
        Global.format(2, "LS=132 PS=54");
    }
}
