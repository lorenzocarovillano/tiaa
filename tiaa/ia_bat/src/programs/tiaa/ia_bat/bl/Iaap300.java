/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:45 PM
**        * FROM NATURAL PROGRAM : Iaap300
************************************************************
**        * FILE NAME            : Iaap300.java
**        * CLASS NAME           : Iaap300
**        * INSTANCE NAME        : Iaap300
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM   -  IAAP300        THIS PROGRAM WILL READ ADABASE FILE    *
*      DATE   -  08/95          201 AND CREATE TWO FLAT EXTRACT FILES; *
*    AUTHOR   -  ARI G.         1) TIAA/CREF FUND EXTRACT DETAIL AND   *
*                               2) TIAA/CREF FUND EXTRACT SUMMARY. BOTH*
*                               FILES CONTAIN HEADER AND TRAILER       *
*                               RECORDS.                               *
*   CHANGES                                                            *
*   6/2003                      INCREASE TIAA RATE FROM 1:60 TO 1:90   *
*                               DO SCAN ON 6/03 FOR CHANGES
*   3/2003                      INCREASE TIAA RATE FROM 1:40 TO 1:60   *
*                               DO SCAN ON 3/03 FOR CHANGES
*   7/2006                      ADDED AN 8-BYTE FILLER FIELD
*                               DO SCAN ON 7/06 FOR CHANGES
*   4/2008                      INCREASE TIAA RATE FROM 90 TO 99
*                               DO SCAN ON 4/08 FOR CHANGES
*   3/2012                      INCREASE TIAA RATE FROM 99 TO 250 AND
*                               INCREASE UNITS-CNT FOR 4.3 TO 6.3
*                               DO SCAN ON 3/12 FOR CHANGES
*   7/2016                      ADDED TIAA-LST-XFR-IN-DTE TO END OF FILE
*                               DO SCAN ON 7/16 FOR CHANGES
*   4/2017                      PIN EXPANSION - RECATALOG
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap300 extends BLNatBase
{
    // Data Areas
    private LdaIaal300 ldaIaal300;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Blank_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal300 = new LdaIaal300();
        registerRecord(ldaIaal300);
        registerRecord(ldaIaal300.getVw_iaa_Tiaa_Fund_Rcrd_View());
        registerRecord(ldaIaal300.getVw_iaa_Cntrl_Rcrd_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Blank_Ctr = localVariables.newFieldInRecord("pnd_Blank_Ctr", "#BLANK-CTR", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal300.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap300() throws Exception
    {
        super("Iaap300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP300 ***");                                                                                                        //Natural: WRITE '*** START OF PROGRAM IAAP300 ***'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        ldaIaal300.getVw_iaa_Cntrl_Rcrd_View().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal300.getVw_iaa_Cntrl_Rcrd_View().readNextRow("READ01")))
        {
            ldaIaal300.getPnd_Check_Date_Ccyymmdd().setValueEdited(ldaIaal300.getIaa_Cntrl_Rcrd_View_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr().setValue("   FHEADER");                                                                             //Natural: MOVE '   FHEADER' TO #WH-CNTRCT-PPCN-NBR
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde().setValue(0);                                                                                       //Natural: MOVE 0 TO #WH-CNTRCT-PAYEE-CDE
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Record_Code().setValue(0);                                                                                            //Natural: MOVE 0 TO #WH-RECORD-CODE
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Check_Date().setValue(ldaIaal300.getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N());                             //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WH-CHECK-DATE
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Process_Date().setValue(Global.getDATN());                                                                            //Natural: MOVE *DATN TO #WH-PROCESS-DATE
        getWorkFiles().write(1, true, ldaIaal300.getPnd_Work_Record_Header());                                                                                            //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-HEADER
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr().setValue("   SHEADER");                                                                             //Natural: MOVE '   SHEADER' TO #WH-CNTRCT-PPCN-NBR
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Filler_1().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-1
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Filler_2().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-2
        //*  ADDED 7/06
        ldaIaal300.getPnd_Work_Record_Header_Pnd_Wh_Filler_3().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-3
        getWorkFiles().write(2, false, ldaIaal300.getPnd_Work_Record_Header());                                                                                           //Natural: WRITE WORK FILE 2 #WORK-RECORD-HEADER
        ldaIaal300.getVw_iaa_Tiaa_Fund_Rcrd_View().startDatabaseRead                                                                                                      //Natural: READ IAA-TIAA-FUND-RCRD-VIEW PHYSICAL
        (
        "R1",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R1:
        while (condition(ldaIaal300.getVw_iaa_Tiaa_Fund_Rcrd_View().readNextRow("R1")))
        {
            ldaIaal300.getPnd_Count_Fund_Record_Reads().nadd(1);                                                                                                          //Natural: ADD 1 TO #COUNT-FUND-RECORD-READS
            ldaIaal300.getPnd_Count().nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT
            ldaIaal300.getPnd_Count2().nadd(1);                                                                                                                           //Natural: ADD 1 TO #COUNT2
            if (condition(ldaIaal300.getPnd_Count().greater(4999)))                                                                                                       //Natural: IF #COUNT > 4999
            {
                getReports().write(0, "NUMBER OF RECORDS READ",ldaIaal300.getPnd_Count2(),Global.getTIMX());                                                              //Natural: WRITE 'NUMBER OF RECORDS READ' #COUNT2 *TIMX
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal300.getPnd_Count().setValue(0);                                                                                                                    //Natural: MOVE 0 TO #COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr().equals(" ")))                                                                      //Natural: IF TIAA-CNTRCT-PPCN-NBR = ' '
            {
                pnd_Blank_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #BLANK-CTR
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Cntrct_Ppcn_Nbr().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr());                              //Natural: MOVE TIAA-CNTRCT-PPCN-NBR TO #W1-CNTRCT-PPCN-NBR
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Cntrct_Payee_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde());                            //Natural: MOVE TIAA-CNTRCT-PAYEE-CDE TO #W1-CNTRCT-PAYEE-CDE
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Record_Code().setValue(30);                                                                                            //Natural: MOVE 30 TO #W1-RECORD-CODE
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Cmpny_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde());                                          //Natural: MOVE TIAA-CMPNY-CDE TO #W1-CMPNY-CDE
            if (condition(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde().equals("1") || ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde().equals("1G")         //Natural: IF TIAA-FUND-CDE = '1' OR = '1G' OR = '1S'
                || ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde().equals("1S")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde(), MoveOption.RightJustified);         //Natural: MOVE RIGHT TIAA-FUND-CDE TO TIAA-FUND-CDE
                DbsUtil.examine(new ExamineSource(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde()), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE TIAA-FUND-CDE FOR ' ' REPLACE '0'
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Fund_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde());                                            //Natural: MOVE TIAA-FUND-CDE TO #W1-FUND-CDE
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Per_Ivc_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt());                                      //Natural: MOVE TIAA-PER-IVC-AMT TO #W1-PER-IVC-AMT
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Rtb_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt());                                              //Natural: MOVE TIAA-RTB-AMT TO #W1-RTB-AMT
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Tot_Per_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                      //Natural: MOVE TIAA-TOT-PER-AMT TO #W1-TOT-PER-AMT
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Tot_Div_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                      //Natural: MOVE TIAA-TOT-DIV-AMT TO #W1-TOT-DIV-AMT
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Old_Per_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt());                                      //Natural: MOVE TIAA-OLD-PER-AMT TO #W1-OLD-PER-AMT
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Old_Div_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt());                                      //Natural: MOVE TIAA-OLD-DIV-AMT TO #W1-OLD-DIV-AMT
            //*  CHANGED FOLLOWING FROM 1:40 TO 1:60 3/03
            //*  CHANGED FOLLOWING FROM 1:60 TO 1:250 4/08
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Rate_Cde().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Cde().getValue(1,              //Natural: MOVE TIAA-RATE-CDE ( 1:250 ) TO #W1-RATE-CDE ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Rate_Dte().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Dte().getValue(1,              //Natural: MOVE TIAA-RATE-DTE ( 1:250 ) TO #W1-RATE-DTE ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Per_Pay_Amt().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue(1,        //Natural: MOVE TIAA-PER-PAY-AMT ( 1:250 ) TO #W1-PER-PAY-AMT ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Per_Div_Amt().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue(1,        //Natural: MOVE TIAA-PER-DIV-AMT ( 1:250 ) TO #W1-PER-DIV-AMT ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Units_Cnt().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(1,            //Natural: MOVE TIAA-UNITS-CNT ( 1:250 ) TO #W1-UNITS-CNT ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Rate_Final_Pay_Amt().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt().getValue(1, //Natural: MOVE TIAA-RATE-FINAL-PAY-AMT ( 1:250 ) TO #W1-RATE-FINAL-PAY-AMT ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Rate_Final_Div_Amt().getValue(1,":",250).setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt().getValue(1, //Natural: MOVE TIAA-RATE-FINAL-DIV-AMT ( 1:250 ) TO #W1-RATE-FINAL-DIV-AMT ( 1:250 )
                ":",250));
            ldaIaal300.getPnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte());                                       //Natural: MOVE LST-TRANS-DTE TO #W1-LST-TRANS-DTE
            //*      MOVE ' '                           TO #W1-FILLER /* 07/06
            getWorkFiles().write(1, true, ldaIaal300.getPnd_Work_Record_1());                                                                                             //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-1
            ldaIaal300.getPnd_Count_Fund_Extract().nadd(1);                                                                                                               //Natural: ADD 1 TO #COUNT-FUND-EXTRACT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Cntrct_Ppcn_Nbr().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Ppcn_Nbr());                              //Natural: MOVE TIAA-CNTRCT-PPCN-NBR TO #W2-CNTRCT-PPCN-NBR
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Cntrct_Payee_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cntrct_Payee_Cde());                            //Natural: MOVE TIAA-CNTRCT-PAYEE-CDE TO #W2-CNTRCT-PAYEE-CDE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Record_Code().setValue(30);                                                                                            //Natural: MOVE 30 TO #W2-RECORD-CODE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Cmpny_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Cmpny_Cde());                                          //Natural: MOVE TIAA-CMPNY-CDE TO #W2-CMPNY-CDE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Fund_Cde().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Fund_Cde());                                            //Natural: MOVE TIAA-FUND-CDE TO #W2-FUND-CDE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Ivc_Amt());                                      //Natural: MOVE TIAA-PER-IVC-AMT TO #W2-PER-IVC-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rtb_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rtb_Amt());                                              //Natural: MOVE TIAA-RTB-AMT TO #W2-RTB-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Tot_Per_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Per_Amt());                                      //Natural: MOVE TIAA-TOT-PER-AMT TO #W2-TOT-PER-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Tot_Div_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Tot_Div_Amt());                                      //Natural: MOVE TIAA-TOT-DIV-AMT TO #W2-TOT-DIV-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Old_Per_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Per_Amt());                                      //Natural: MOVE TIAA-OLD-PER-AMT TO #W2-OLD-PER-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Old_Div_Amt().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Old_Div_Amt());                                      //Natural: MOVE TIAA-OLD-DIV-AMT TO #W2-OLD-DIV-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Cde().setValue(" ");                                                                                              //Natural: MOVE ' ' TO #W2-RATE-CDE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Dte().setValue(0);                                                                                                //Natural: MOVE 0 TO #W2-RATE-DTE
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Units_Cnt().setValue(0);                                                                                               //Natural: MOVE 0 TO #W2-UNITS-CNT #W2-RATE-FINAL-PAY-AMT #W2-RATE-FINAL-DIV-AMT #W2-PER-PAY-AMT #W2-PER-DIV-AMT
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt().setValue(0);
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt().setValue(0);
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Per_Pay_Amt().setValue(0);
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Per_Div_Amt().setValue(0);
            ldaIaal300.getPnd_Per_Tot().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Count_Casttiaa_Rate_Data_Grp());                                                   //Natural: MOVE C*TIAA-RATE-DATA-GRP TO #PER-TOT
            F1:                                                                                                                                                           //Natural: FOR #I = 1 TO #PER-TOT
            for (ldaIaal300.getPnd_I().setValue(1); condition(ldaIaal300.getPnd_I().lessOrEqual(ldaIaal300.getPnd_Per_Tot())); ldaIaal300.getPnd_I().nadd(1))
            {
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Per_Pay_Amt().nadd(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Pay_Amt().getValue(ldaIaal300.getPnd_I()));      //Natural: ADD TIAA-PER-PAY-AMT ( #I ) TO #W2-PER-PAY-AMT
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Per_Div_Amt().nadd(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Per_Div_Amt().getValue(ldaIaal300.getPnd_I()));      //Natural: ADD TIAA-PER-DIV-AMT ( #I ) TO #W2-PER-DIV-AMT
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Units_Cnt().nadd(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Units_Cnt().getValue(ldaIaal300.getPnd_I()));          //Natural: ADD TIAA-UNITS-CNT ( #I ) TO #W2-UNITS-CNT
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Final_Pay_Amt().nadd(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Pay_Amt().getValue(ldaIaal300.getPnd_I())); //Natural: ADD TIAA-RATE-FINAL-PAY-AMT ( #I ) TO #W2-RATE-FINAL-PAY-AMT
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Rate_Final_Div_Amt().nadd(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Rate_Final_Div_Amt().getValue(ldaIaal300.getPnd_I())); //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( #I ) TO #W2-RATE-FINAL-DIV-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte().setValue(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Lst_Trans_Dte());                                       //Natural: MOVE LST-TRANS-DTE TO #W2-LST-TRANS-DTE
            //*  7/16 START
            if (condition(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte().greater(getZero())))                                                                //Natural: IF TIAA-LST-XFR-IN-DTE GT 0
            {
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte().setValueEdited(ldaIaal300.getIaa_Tiaa_Fund_Rcrd_View_Tiaa_Lst_Xfr_In_Dte(),new               //Natural: MOVE EDITED TIAA-LST-XFR-IN-DTE ( EM = YYYYMMDD ) TO #W2-TIAA-LST-XFR-IN-DTE
                    ReportEditMask("YYYYMMDD"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Tiaa_Lst_Xfr_In_Dte().reset();                                                                                     //Natural: RESET #W2-TIAA-LST-XFR-IN-DTE
            }                                                                                                                                                             //Natural: END-IF
            //*  7/16 END
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Filler().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #W2-FILLER
            //*  ADDED 6/03
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Filler2().setValue(" ");                                                                                               //Natural: MOVE ' ' TO #W2-FILLER2
            //*  ADDED 7/06
            ldaIaal300.getPnd_Work_Record_2_Pnd_W2_Filler3().setValue(" ");                                                                                               //Natural: MOVE ' ' TO #W2-FILLER3
            getWorkFiles().write(2, false, ldaIaal300.getPnd_Work_Record_2());                                                                                            //Natural: WRITE WORK FILE 2 #WORK-RECORD-2
            ldaIaal300.getPnd_Count_Fund_Extract_2().nadd(1);                                                                                                             //Natural: ADD 1 TO #COUNT-FUND-EXTRACT-2
            //*      IF #W1-CMPNY-CDE = 'T'
            //*         MOVE C*TIAA-RATE-DATA-GRP TO #PER-TOT
            //* * F1.   FOR #I = 1 TO #PER-TOT
            //*          ADD TIAA-RATE-FINAL-PAY-AMT(#I)  TO #W2-RATE-FINAL-PAY-AMT
            //*           ADD TIAA-RATE-FINAL-DIV-AMT(#I) TO #W2-RATE-FINAL-DIV-AMT
            //*         END-FOR
            //*      ELSE
            //*        ADD TIAA-UNITS-CNT(1) TIAA-UNITS-CNT(2) TO #W1-UNITS-CNT
            //*      END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr().setValue("99FTRAILER");                                                                            //Natural: MOVE '99FTRAILER' TO #WT-CNTRCT-PPCN-NBR
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde().setValue(0);                                                                                      //Natural: MOVE 0 TO #WT-CNTRCT-PAYEE-CDE
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Record_Code().setValue(0);                                                                                           //Natural: MOVE 0 TO #WT-RECORD-CODE
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Check_Date().setValue(ldaIaal300.getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N());                            //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WT-CHECK-DATE
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Process_Date().setValue(Global.getDATN());                                                                           //Natural: MOVE *DATN TO #WT-PROCESS-DATE
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records().setValue(ldaIaal300.getPnd_Count_Fund_Extract());                                                 //Natural: MOVE #COUNT-FUND-EXTRACT TO #WT-TOT-FUND-RECORDS
        getWorkFiles().write(1, true, ldaIaal300.getPnd_Work_Record_Trailer());                                                                                           //Natural: WRITE WORK FILE 1 VARIABLE #WORK-RECORD-TRAILER
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr().setValue("99STRAILER");                                                                            //Natural: MOVE '99STRAILER' TO #WT-CNTRCT-PPCN-NBR
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Fund_Records().setValue(ldaIaal300.getPnd_Count_Fund_Extract_2());                                               //Natural: MOVE #COUNT-FUND-EXTRACT-2 TO #WT-TOT-FUND-RECORDS
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_1().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-1
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_2().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-2
        //*  7/06
        ldaIaal300.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_3().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-3
        getWorkFiles().write(2, false, ldaIaal300.getPnd_Work_Record_Trailer());                                                                                          //Natural: WRITE WORK FILE 2 #WORK-RECORD-TRAILER
        getReports().write(0, NEWLINE,"  FUND RECORD READS =========> ",ldaIaal300.getPnd_Count_Fund_Record_Reads());                                                     //Natural: WRITE / '  FUND RECORD READS =========> ' #COUNT-FUND-RECORD-READS
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"  FUND RECS W/BLANK CNTRCT #=> ",pnd_Blank_Ctr);                                                                                   //Natural: WRITE / '  FUND RECS W/BLANK CNTRCT #=> ' #BLANK-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"DETAIL FUND EXTRACT ====>",ldaIaal300.getPnd_Count_Fund_Extract(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));        //Natural: WRITE ( 1 ) / 'DETAIL FUND EXTRACT ====>' #COUNT-FUND-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"SUMMARY FUND EXTRACT ===>",ldaIaal300.getPnd_Count_Fund_Extract_2(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));      //Natural: WRITE ( 1 ) / 'SUMMARY FUND EXTRACT ===>' #COUNT-FUND-EXTRACT-2 ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"*** END OF PROGRAM IAAP300 ***");                                                                                                  //Natural: WRITE / '*** END OF PROGRAM IAAP300 ***'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"TOTAL RECORDS");                                                                    //Natural: WRITE ( 1 ) 29X 'TOTAL RECORDS'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"  EXTRACTED");                                                                      //Natural: WRITE ( 1 ) 29X '  EXTRACTED'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
