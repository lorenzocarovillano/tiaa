/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:32:31 PM
**        * FROM NATURAL PROGRAM : Iaap796a
************************************************************
**        * FILE NAME            : Iaap796a.java
**        * CLASS NAME           : Iaap796a
**        * INSTANCE NAME        : Iaap796a
************************************************************
************************************************************************
*
* PROGRAM: IAAP796A
* DATE   : 02/21/96
* AUTHOR : ARI GROSSMAN - PROGRAM IS SIMILAR TO IAAP795
* DESC   : IA ADMINISTRATION - NEW ISSUES (TYPE 30)
*          THIS REPORT SHOWS THE MASTER INPUT, IA TRANSACTION AND
*          THE MASTER OUTPUT.
*
* HISTORY: LEN B 01/12/98 REMOVED CHECKS FOR SPECIFIC FUND CODES
*          04/02/98 LB FIXED TOTALS
*          04/29/02 RESTOWED FOR OIA CHANGES MADE IN IAAN051A
*
*          6/02  CHANGE MOVE RIGHT  6/02
*          TO TEST THIS PROGRAM USING ALL RECORDS AS INPUT RUN A
*          BATCH JOB ENDING WITH A 'T'
*
*          9/08 ADDED TIAA ACCESS
*
*          3/12 CHANGES FOR RATE BASE EXPANSION -- SCAN ON 3/12
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap796a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Amt;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte;

    private DbsGroup pnd_Work_Record_1__R_Field_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Date;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W_Sub_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Activity_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Sub_Code;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Work_Record_1__R_Field_2;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde;

    private DbsGroup pnd_Work_Record_1__R_Field_3;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde;

    private DbsGroup pnd_Work_Record_1__R_Field_4;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1;

    private DbsGroup pnd_Work_Record_1__R_Field_5;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde_Num;
    private DbsField pnd_Work_Record_1_Pnd_W_Product_Cde_Filler;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte;

    private DbsGroup pnd_Work_Record_1__R_Field_6;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Ccyy;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Mm;
    private DbsField pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W_Critical_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W_Company_Cd;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Pay;
    private DbsField pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Div;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Payment_B;
    private DbsField pnd_Work_Record_1_Pnd_W_Final_Dividend_B;
    private DbsField iaan051a_Fund;
    private DbsField iaan051a_Desc;
    private DbsField iaan051a_Cmpy_Desc;
    private DbsField iaan051a_Length;
    private DbsField pnd_Transactions;
    private DbsField pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Total_Cref_Products_Cnt;
    private DbsField pnd_Total_Cref_Products;
    private DbsField pnd_Total_Cref_Sw;
    private DbsField pnd_Cntrl_Fund_Cde;

    private DbsGroup pnd_Cntrl_Fund_Cde__R_Field_7;
    private DbsField pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1;
    private DbsField pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2;
    private DbsField pnd_Text;
    private DbsField pnd_I;
    private DbsField pnd_K;
    private DbsField pnd_Mode_Hold;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_8;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Pnd_Date_Ccyy;
    private DbsField pnd_C_Trans_Check_Dte;

    private DbsGroup pnd_C_Trans_Check_Dte__R_Field_9;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm;
    private DbsField pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd;
    private DbsField pnd_Test_Prod;
    private DbsField pnd_Total_Records_Read;
    private DbsField pnd_Mode_Sub;
    private DbsField pnd_Prod_Sub;
    private DbsField pnd_Mode_Code;
    private DbsField pnd_Search_Key_Rcrd;

    private DbsGroup pnd_Search_Key_Rcrd__R_Field_10;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte;
    private DbsField pnd_Product_Name_Rpt3;

    private DbsGroup pnd_Product_Name_Rpt3__R_Field_11;
    private DbsField pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod;
    private DbsField pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode;
    private DbsField pnd_Rept1_In_Out;
    private DbsField pnd_Rept1_Net_Per_Pmt;
    private DbsField pnd_Rept1_Net_Per_Dvd;
    private DbsField pnd_Rept1_Net_Fin_Dvd;
    private DbsField pnd_Rept1_Net_Fin_Pay;
    private DbsField pnd_Rept1_Net_Units;
    private DbsField pnd_Rept2_Prod_Titles_T;
    private DbsField pnd_Rept2_Prod_Titles_C;
    private DbsField pnd_R_Prod_Titles_C;

    private DbsGroup pnd_R_Prod_Titles_C__R_Field_12;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units;
    private DbsField pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler;
    private DbsField pnd_Rept2_Prod_Titles_2_C;
    private DbsField pnd_R_Prod_Titles_2_C;

    private DbsGroup pnd_R_Prod_Titles_2_C__R_Field_13;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments;
    private DbsField pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler;
    private DbsField pnd_Rept2_Mode;
    private DbsField pnd_Rept2_Tot_Per_Pmt_In;
    private DbsField pnd_Rept2_Tot_Per_Dvd_In;
    private DbsField pnd_Rept2_Tot_Fin_Pay_In;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_In;
    private DbsField pnd_Rept2_Tot_Per_Pmt_Out;
    private DbsField pnd_Rept2_Tot_Per_Dvd_Out;
    private DbsField pnd_Rept2_Tot_Fin_Pay_Out;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_Out;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_In;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_In;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_In;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_In;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_Out;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_Out;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_Out;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_Out;
    private DbsField pnd_Rept2_Tot_Units_Prod_In;
    private DbsField pnd_Rept2_Tot_Units_Pymt_In;
    private DbsField pnd_Rept2_Tot_Units_Prod_Out;
    private DbsField pnd_Rept2_Tot_Units_Pymt_Out;
    private DbsField pnd_Rept2_Tot_Unit_Prod_N_Crit;
    private DbsField pnd_Rept2_Tot_Unit_Pymt_N_Crit;
    private DbsField pnd_Rept2_Total_Units_Prod_In_A;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_A;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_A;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_A;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Crit_A;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Crit_A;
    private DbsField pnd_Rept2_Total_Units_Prod_In_M;
    private DbsField pnd_Rept2_Total_Units_Pymt_In_M;
    private DbsField pnd_Rept2_Total_Units_Prod_Out_M;
    private DbsField pnd_Rept2_Total_Units_Pymt_Out_M;
    private DbsField pnd_Rept2_Total_Unit_Prod_N_Crit_M;
    private DbsField pnd_Rept2_Total_Unit_Pymt_N_Crit_M;
    private DbsField pnd_Rept2_G_Tot_Units_Prod_In;
    private DbsField pnd_Rept2_G_Tot_Units_Pymt_In;
    private DbsField pnd_Rept2_G_Tot_Units_Prod_Out;
    private DbsField pnd_Rept2_G_Tot_Units_Pymt_Out;
    private DbsField pnd_Rept2_G_Tot_Unit_Prod_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Unit_Pymt_N_Crit;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_A;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_A;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_A;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_A;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Crita;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Crita;
    private DbsField pnd_Rept2_G_Total_Units_Prod_In_M;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_In_M;
    private DbsField pnd_Rept2_G_Total_Units_Prod_Out_M;
    private DbsField pnd_Rept2_G_Total_Units_Pymt_Out_M;
    private DbsField pnd_Rept2_G_Total_Unit_Pymt_N_Critm;
    private DbsField pnd_Rept2_G_Total_Unit_Prod_N_Critm;
    private DbsField pnd_Rept2_Tot_Per_Pmt_N_Crit;
    private DbsField pnd_Rept2_Tot_Per_Dvd_N_Crit;
    private DbsField pnd_Rept2_Tot_Fin_Pay_N_Crit;
    private DbsField pnd_Rept2_Tot_Fin_Dvd_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Per_Pmt_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Per_Dvd_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Fin_Pay_N_Crit;
    private DbsField pnd_Rept2_G_Tot_Fin_Dvd_N_Crit;
    private DbsField pnd_Rept2_Title_Sub;
    private DbsField pnd_Rept2_Mode_Sub;
    private DbsField pnd_Rept2_Cntl_Sub;
    private DbsField pnd_Rept2_Prod_Sub;
    private DbsField pnd_W_Print_Sw;
    private DbsField pnd_Init_Program;

    private DbsGroup pnd_Init_Program__R_Field_14;
    private DbsField pnd_Init_Program_Pnd_Ip_Prefix;
    private DbsField pnd_Init_Program_Pnd_Ip_Rest;
    private DbsField pnd_Init_Program_Pnd_Ip_Sufix;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newGroupArrayInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Units = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Units", "CNTRL-UNITS", 
            FieldType.PACKED_DECIMAL, 13, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Amt = iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte", "#W-TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_Record_1__R_Field_1 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_1", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Check_Dte);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Ccyy", "#W-TRANS-CHECK-DTE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Mm", "#W-TRANS-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Check_Dte_Dd", "#W-TRANS-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Date = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Date", "#W-TRANS-DATE", FieldType.TIME);
        pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind", "#W-CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_1_Pnd_W_Trans_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Cde", "#W-TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Work_Record_1_Pnd_W_Sub_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Sub_Code", "#W-SUB-CODE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W_Activity_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Activity_Code", "#W-ACTIVITY-CODE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde", "#W-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte", "#W-CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W_Trans_Sub_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Sub_Code", "#W-TRANS-SUB-CODE", FieldType.STRING, 
            3);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr", "#W-TRANS-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Work_Record_1__R_Field_2 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_2", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1 = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1", "#W-TRANS-PPCN-NBR-1", 
            FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2 = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_2", "#W-TRANS-PPCN-NBR-2", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde", "#W-TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Work_Record_1__R_Field_3 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_3", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde);
        pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A", "#W-TRANS-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_W_Product_Cde = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde", "#W-PRODUCT-CDE", FieldType.STRING, 
            3);

        pnd_Work_Record_1__R_Field_4 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_4", "REDEFINE", pnd_Work_Record_1_Pnd_W_Product_Cde);
        pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1", "#W-PRODUCT-CDE-NUM-1", 
            FieldType.STRING, 2);

        pnd_Work_Record_1__R_Field_5 = pnd_Work_Record_1__R_Field_4.newGroupInGroup("pnd_Work_Record_1__R_Field_5", "REDEFINE", pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1);
        pnd_Work_Record_1_Pnd_W_Product_Cde_Num = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde_Num", "#W-PRODUCT-CDE-NUM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Product_Cde_Filler = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Product_Cde_Filler", "#W-PRODUCT-CDE-FILLER", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt", "#W-TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt", "#W-TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt", "#W-CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B", "#W-TIAA-TOT-PER-AMT-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B", "#W-TIAA-TOT-DIV-AMT-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B", "#W-CREF-UNITS-CNT-B", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte", "#W-TRANS-EFFCTV-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_Record_1__R_Field_6 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_6", "REDEFINE", pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte);
        pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Ccyy = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Ccyy", 
            "#W-TRANS-EFFCTV-DTE-CCYY", FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Mm = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Mm", "#W-TRANS-EFFCTV-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Dd = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Dd", "#W-TRANS-EFFCTV-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W_Critical_Ind = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Critical_Ind", "#W-CRITICAL-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Company_Cd = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Company_Cd", "#W-COMPANY-CD", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Pay = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Pay", "#W-TIAA-TOT-FINAL-PAY", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Div = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Div", "#W-TIAA-TOT-FINAL-DIV", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_W_Final_Payment_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Payment_B", "#W-FINAL-PAYMENT-B", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Record_1_Pnd_W_Final_Dividend_B = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W_Final_Dividend_B", "#W-FINAL-DIVIDEND-B", 
            FieldType.PACKED_DECIMAL, 9, 2);
        iaan051a_Fund = localVariables.newFieldInRecord("iaan051a_Fund", "IAAN051A-FUND", FieldType.STRING, 2);
        iaan051a_Desc = localVariables.newFieldInRecord("iaan051a_Desc", "IAAN051A-DESC", FieldType.STRING, 35);
        iaan051a_Cmpy_Desc = localVariables.newFieldInRecord("iaan051a_Cmpy_Desc", "IAAN051A-CMPY-DESC", FieldType.STRING, 4);
        iaan051a_Length = localVariables.newFieldInRecord("iaan051a_Length", "IAAN051A-LENGTH", FieldType.PACKED_DECIMAL, 3);
        pnd_Transactions = localVariables.newFieldInRecord("pnd_Transactions", "#TRANSACTIONS", FieldType.STRING, 1);
        pnd_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 11);
        pnd_Total_Cref_Products_Cnt = localVariables.newFieldInRecord("pnd_Total_Cref_Products_Cnt", "#TOTAL-CREF-PRODUCTS-CNT", FieldType.NUMERIC, 3);
        pnd_Total_Cref_Products = localVariables.newFieldInRecord("pnd_Total_Cref_Products", "#TOTAL-CREF-PRODUCTS", FieldType.NUMERIC, 3);
        pnd_Total_Cref_Sw = localVariables.newFieldInRecord("pnd_Total_Cref_Sw", "#TOTAL-CREF-SW", FieldType.STRING, 1);
        pnd_Cntrl_Fund_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Fund_Cde", "#CNTRL-FUND-CDE", FieldType.STRING, 3);

        pnd_Cntrl_Fund_Cde__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrl_Fund_Cde__R_Field_7", "REDEFINE", pnd_Cntrl_Fund_Cde);
        pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1 = pnd_Cntrl_Fund_Cde__R_Field_7.newFieldInGroup("pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_1", "#CNTRL-FUND-CDE-1", 
            FieldType.STRING, 1);
        pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2 = pnd_Cntrl_Fund_Cde__R_Field_7.newFieldInGroup("pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2", "#CNTRL-FUND-CDE-2", 
            FieldType.STRING, 2);
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Mode_Hold = localVariables.newFieldInRecord("pnd_Mode_Hold", "#MODE-HOLD", FieldType.NUMERIC, 3);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Date__R_Field_8", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_C_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_C_Trans_Check_Dte", "#C-TRANS-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_C_Trans_Check_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_C_Trans_Check_Dte__R_Field_9", "REDEFINE", pnd_C_Trans_Check_Dte);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy = pnd_C_Trans_Check_Dte__R_Field_9.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy", 
            "#TRANS-CHECK-DTE-CCYY", FieldType.NUMERIC, 4);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm = pnd_C_Trans_Check_Dte__R_Field_9.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm", 
            "#TRANS-CHECK-DTE-MM", FieldType.NUMERIC, 2);
        pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd = pnd_C_Trans_Check_Dte__R_Field_9.newFieldInGroup("pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Dd", 
            "#TRANS-CHECK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Test_Prod = localVariables.newFieldInRecord("pnd_Test_Prod", "#TEST-PROD", FieldType.NUMERIC, 1);
        pnd_Total_Records_Read = localVariables.newFieldInRecord("pnd_Total_Records_Read", "#TOTAL-RECORDS-READ", FieldType.NUMERIC, 5);
        pnd_Mode_Sub = localVariables.newFieldInRecord("pnd_Mode_Sub", "#MODE-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub = localVariables.newFieldInRecord("pnd_Prod_Sub", "#PROD-SUB", FieldType.NUMERIC, 2);
        pnd_Mode_Code = localVariables.newFieldInRecord("pnd_Mode_Code", "#MODE-CODE", FieldType.STRING, 2);
        pnd_Search_Key_Rcrd = localVariables.newFieldInRecord("pnd_Search_Key_Rcrd", "#SEARCH-KEY-RCRD", FieldType.STRING, 10);

        pnd_Search_Key_Rcrd__R_Field_10 = localVariables.newGroupInRecord("pnd_Search_Key_Rcrd__R_Field_10", "REDEFINE", pnd_Search_Key_Rcrd);
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde = pnd_Search_Key_Rcrd__R_Field_10.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde", "#SEARCH-CNTRL-CDE", 
            FieldType.STRING, 2);
        pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte = pnd_Search_Key_Rcrd__R_Field_10.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte", "#SEARCH-TRANS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Product_Name_Rpt3 = localVariables.newFieldInRecord("pnd_Product_Name_Rpt3", "#PRODUCT-NAME-RPT3", FieldType.STRING, 5);

        pnd_Product_Name_Rpt3__R_Field_11 = localVariables.newGroupInRecord("pnd_Product_Name_Rpt3__R_Field_11", "REDEFINE", pnd_Product_Name_Rpt3);
        pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod = pnd_Product_Name_Rpt3__R_Field_11.newFieldInGroup("pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod", 
            "#PRODUCT-NAME-RPT3-PROD", FieldType.STRING, 3);
        pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode = pnd_Product_Name_Rpt3__R_Field_11.newFieldInGroup("pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode", 
            "#PRODUCT-NAME-RPT3-MODE", FieldType.STRING, 2);
        pnd_Rept1_In_Out = localVariables.newFieldInRecord("pnd_Rept1_In_Out", "#REPT1-IN-OUT", FieldType.STRING, 3);
        pnd_Rept1_Net_Per_Pmt = localVariables.newFieldInRecord("pnd_Rept1_Net_Per_Pmt", "#REPT1-NET-PER-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Rept1_Net_Per_Dvd = localVariables.newFieldInRecord("pnd_Rept1_Net_Per_Dvd", "#REPT1-NET-PER-DVD", FieldType.NUMERIC, 13, 2);
        pnd_Rept1_Net_Fin_Dvd = localVariables.newFieldInRecord("pnd_Rept1_Net_Fin_Dvd", "#REPT1-NET-FIN-DVD", FieldType.NUMERIC, 13, 2);
        pnd_Rept1_Net_Fin_Pay = localVariables.newFieldInRecord("pnd_Rept1_Net_Fin_Pay", "#REPT1-NET-FIN-PAY", FieldType.NUMERIC, 13, 2);
        pnd_Rept1_Net_Units = localVariables.newFieldInRecord("pnd_Rept1_Net_Units", "#REPT1-NET-UNITS", FieldType.NUMERIC, 10, 3);
        pnd_Rept2_Prod_Titles_T = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_T", "#REPT2-PROD-TITLES-T", FieldType.STRING, 23, new DbsArrayController(1, 
            4));
        pnd_Rept2_Prod_Titles_C = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_C", "#REPT2-PROD-TITLES-C", FieldType.STRING, 23, new DbsArrayController(1, 
            22));
        pnd_R_Prod_Titles_C = localVariables.newFieldInRecord("pnd_R_Prod_Titles_C", "#R-PROD-TITLES-C", FieldType.STRING, 23);

        pnd_R_Prod_Titles_C__R_Field_12 = localVariables.newGroupInRecord("pnd_R_Prod_Titles_C__R_Field_12", "REDEFINE", pnd_R_Prod_Titles_C);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co = pnd_R_Prod_Titles_C__R_Field_12.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co", "#RPT-TITLE-C-CO", 
            FieldType.STRING, 5);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod = pnd_R_Prod_Titles_C__R_Field_12.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod", "#RPT-TITLE-C-PROD", 
            FieldType.STRING, 4);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units = pnd_R_Prod_Titles_C__R_Field_12.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units", "#RPT-TITLE-C-UNITS", 
            FieldType.STRING, 8);
        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler = pnd_R_Prod_Titles_C__R_Field_12.newFieldInGroup("pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Filler", "#RPT-TITLE-C-FILLER", 
            FieldType.STRING, 6);
        pnd_Rept2_Prod_Titles_2_C = localVariables.newFieldArrayInRecord("pnd_Rept2_Prod_Titles_2_C", "#REPT2-PROD-TITLES-2-C", FieldType.STRING, 23, 
            new DbsArrayController(1, 22));
        pnd_R_Prod_Titles_2_C = localVariables.newFieldInRecord("pnd_R_Prod_Titles_2_C", "#R-PROD-TITLES-2-C", FieldType.STRING, 23);

        pnd_R_Prod_Titles_2_C__R_Field_13 = localVariables.newGroupInRecord("pnd_R_Prod_Titles_2_C__R_Field_13", "REDEFINE", pnd_R_Prod_Titles_2_C);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co = pnd_R_Prod_Titles_2_C__R_Field_13.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co", "#RPT-TITLE-2-C-CO", 
            FieldType.STRING, 5);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod = pnd_R_Prod_Titles_2_C__R_Field_13.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod", 
            "#RPT-TITLE-2-C-PROD", FieldType.STRING, 4);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments = pnd_R_Prod_Titles_2_C__R_Field_13.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments", 
            "#RPT-TITLE-2-C-PAYMENTS", FieldType.STRING, 8);
        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler = pnd_R_Prod_Titles_2_C__R_Field_13.newFieldInGroup("pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Filler", 
            "#RPT-TITLE-2-C-FILLER", FieldType.STRING, 6);
        pnd_Rept2_Mode = localVariables.newFieldInRecord("pnd_Rept2_Mode", "#REPT2-MODE", FieldType.STRING, 5);
        pnd_Rept2_Tot_Per_Pmt_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_In", "#REPT2-TOT-PER-PMT-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Per_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_In", "#REPT2-TOT-PER-DVD-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Pay_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_In", "#REPT2-TOT-FIN-PAY-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_In", "#REPT2-TOT-FIN-DVD-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Per_Pmt_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_Out", "#REPT2-TOT-PER-PMT-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Per_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_Out", "#REPT2-TOT-PER-DVD-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Rept2_Tot_Fin_Pay_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_Out", "#REPT2-TOT-FIN-PAY-OUT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_Tot_Fin_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_Out", "#REPT2-TOT-FIN-DVD-OUT", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_In", "#REPT2-G-TOT-PER-DVD-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Pmt_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_In", "#REPT2-G-TOT-PER-PMT-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Pay_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_In", "#REPT2-G-TOT-FIN-PAY-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Fin_Dvd_In = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_In", "#REPT2-G-TOT-FIN-DVD-IN", FieldType.NUMERIC, 13, 2);
        pnd_Rept2_G_Tot_Per_Pmt_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_Out", "#REPT2-G-TOT-PER-PMT-OUT", FieldType.NUMERIC, 12, 
            2);
        pnd_Rept2_G_Tot_Per_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_Out", "#REPT2-G-TOT-PER-DVD-OUT", FieldType.NUMERIC, 12, 
            2);
        pnd_Rept2_G_Tot_Fin_Pay_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_Out", "#REPT2-G-TOT-FIN-PAY-OUT", FieldType.NUMERIC, 14, 
            2);
        pnd_Rept2_G_Tot_Fin_Dvd_Out = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_Out", "#REPT2-G-TOT-FIN-DVD-OUT", FieldType.NUMERIC, 14, 
            2);
        pnd_Rept2_Tot_Units_Prod_In = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Prod_In", "#REPT2-TOT-UNITS-PROD-IN", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 40));
        pnd_Rept2_Tot_Units_Pymt_In = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Pymt_In", "#REPT2-TOT-UNITS-PYMT-IN", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Rept2_Tot_Units_Prod_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Prod_Out", "#REPT2-TOT-UNITS-PROD-OUT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 40));
        pnd_Rept2_Tot_Units_Pymt_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Units_Pymt_Out", "#REPT2-TOT-UNITS-PYMT-OUT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Rept2_Tot_Unit_Prod_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Unit_Prod_N_Crit", "#REPT2-TOT-UNIT-PROD-N-CRIT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 40));
        pnd_Rept2_Tot_Unit_Pymt_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_Tot_Unit_Pymt_N_Crit", "#REPT2-TOT-UNIT-PYMT-N-CRIT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Rept2_Total_Units_Prod_In_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_A", "#REPT2-TOTAL-UNITS-PROD-IN-A", FieldType.NUMERIC, 
            13, 3);
        pnd_Rept2_Total_Units_Pymt_In_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_A", "#REPT2-TOTAL-UNITS-PYMT-IN-A", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_Total_Units_Prod_Out_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_A", "#REPT2-TOTAL-UNITS-PROD-OUT-A", FieldType.NUMERIC, 
            13, 3);
        pnd_Rept2_Total_Units_Pymt_Out_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_A", "#REPT2-TOTAL-UNITS-PYMT-OUT-A", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_Total_Unit_Prod_N_Crit_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Crit_A", "#REPT2-TOTAL-UNIT-PROD-N-CRIT-A", 
            FieldType.NUMERIC, 13, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Crit_A = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Crit_A", "#REPT2-TOTAL-UNIT-PYMT-N-CRIT-A", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_Total_Units_Prod_In_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_In_M", "#REPT2-TOTAL-UNITS-PROD-IN-M", FieldType.NUMERIC, 
            13, 3);
        pnd_Rept2_Total_Units_Pymt_In_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_In_M", "#REPT2-TOTAL-UNITS-PYMT-IN-M", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_Total_Units_Prod_Out_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Prod_Out_M", "#REPT2-TOTAL-UNITS-PROD-OUT-M", FieldType.NUMERIC, 
            13, 3);
        pnd_Rept2_Total_Units_Pymt_Out_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Units_Pymt_Out_M", "#REPT2-TOTAL-UNITS-PYMT-OUT-M", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_Total_Unit_Prod_N_Crit_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Prod_N_Crit_M", "#REPT2-TOTAL-UNIT-PROD-N-CRIT-M", 
            FieldType.NUMERIC, 13, 3);
        pnd_Rept2_Total_Unit_Pymt_N_Crit_M = localVariables.newFieldInRecord("pnd_Rept2_Total_Unit_Pymt_N_Crit_M", "#REPT2-TOTAL-UNIT-PYMT-N-CRIT-M", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_G_Tot_Units_Prod_In = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Prod_In", "#REPT2-G-TOT-UNITS-PROD-IN", FieldType.NUMERIC, 
            13, 3, new DbsArrayController(1, 40));
        pnd_Rept2_G_Tot_Units_Pymt_In = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Pymt_In", "#REPT2-G-TOT-UNITS-PYMT-IN", FieldType.NUMERIC, 
            12, 2, new DbsArrayController(1, 40));
        pnd_Rept2_G_Tot_Units_Prod_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Prod_Out", "#REPT2-G-TOT-UNITS-PROD-OUT", FieldType.NUMERIC, 
            13, 3, new DbsArrayController(1, 40));
        pnd_Rept2_G_Tot_Units_Pymt_Out = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Units_Pymt_Out", "#REPT2-G-TOT-UNITS-PYMT-OUT", FieldType.NUMERIC, 
            12, 2, new DbsArrayController(1, 40));
        pnd_Rept2_G_Tot_Unit_Prod_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Unit_Prod_N_Crit", "#REPT2-G-TOT-UNIT-PROD-N-CRIT", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 40));
        pnd_Rept2_G_Tot_Unit_Pymt_N_Crit = localVariables.newFieldArrayInRecord("pnd_Rept2_G_Tot_Unit_Pymt_N_Crit", "#REPT2-G-TOT-UNIT-PYMT-N-CRIT", FieldType.NUMERIC, 
            12, 2, new DbsArrayController(1, 40));
        pnd_Rept2_G_Total_Units_Prod_In_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_A", "#REPT2-G-TOTAL-UNITS-PROD-IN-A", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_A", "#REPT2-G-TOTAL-UNITS-PYMT-IN-A", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_A", "#REPT2-G-TOTAL-UNITS-PROD-OUT-A", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_A = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_A", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-A", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_G_Total_Unit_Pymt_N_Crita = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Crita", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRITA", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Crita = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Crita", "#REPT2-G-TOTAL-UNIT-PROD-N-CRITA", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Prod_In_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_In_M", "#REPT2-G-TOTAL-UNITS-PROD-IN-M", FieldType.NUMERIC, 
            11, 3);
        pnd_Rept2_G_Total_Units_Pymt_In_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_In_M", "#REPT2-G-TOTAL-UNITS-PYMT-IN-M", FieldType.NUMERIC, 
            12, 2);
        pnd_Rept2_G_Total_Units_Prod_Out_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Prod_Out_M", "#REPT2-G-TOTAL-UNITS-PROD-OUT-M", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_G_Total_Units_Pymt_Out_M = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Units_Pymt_Out_M", "#REPT2-G-TOTAL-UNITS-PYMT-OUT-M", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_G_Total_Unit_Pymt_N_Critm = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Pymt_N_Critm", "#REPT2-G-TOTAL-UNIT-PYMT-N-CRITM", 
            FieldType.NUMERIC, 12, 2);
        pnd_Rept2_G_Total_Unit_Prod_N_Critm = localVariables.newFieldInRecord("pnd_Rept2_G_Total_Unit_Prod_N_Critm", "#REPT2-G-TOTAL-UNIT-PROD-N-CRITM", 
            FieldType.NUMERIC, 11, 3);
        pnd_Rept2_Tot_Per_Pmt_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Pmt_N_Crit", "#REPT2-TOT-PER-PMT-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Tot_Per_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Per_Dvd_N_Crit", "#REPT2-TOT-PER-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Tot_Fin_Pay_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Pay_N_Crit", "#REPT2-TOT-FIN-PAY-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Tot_Fin_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_Tot_Fin_Dvd_N_Crit", "#REPT2-TOT-FIN-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Per_Pmt_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Pmt_N_Crit", "#REPT2-G-TOT-PER-PMT-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Per_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Per_Dvd_N_Crit", "#REPT2-G-TOT-PER-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Fin_Pay_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Pay_N_Crit", "#REPT2-G-TOT-FIN-PAY-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_G_Tot_Fin_Dvd_N_Crit = localVariables.newFieldInRecord("pnd_Rept2_G_Tot_Fin_Dvd_N_Crit", "#REPT2-G-TOT-FIN-DVD-N-CRIT", FieldType.NUMERIC, 
            13, 2);
        pnd_Rept2_Title_Sub = localVariables.newFieldInRecord("pnd_Rept2_Title_Sub", "#REPT2-TITLE-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Mode_Sub = localVariables.newFieldInRecord("pnd_Rept2_Mode_Sub", "#REPT2-MODE-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Cntl_Sub = localVariables.newFieldInRecord("pnd_Rept2_Cntl_Sub", "#REPT2-CNTL-SUB", FieldType.NUMERIC, 2);
        pnd_Rept2_Prod_Sub = localVariables.newFieldInRecord("pnd_Rept2_Prod_Sub", "#REPT2-PROD-SUB", FieldType.NUMERIC, 2);
        pnd_W_Print_Sw = localVariables.newFieldInRecord("pnd_W_Print_Sw", "#W-PRINT-SW", FieldType.STRING, 1);
        pnd_Init_Program = localVariables.newFieldInRecord("pnd_Init_Program", "#INIT-PROGRAM", FieldType.STRING, 8);

        pnd_Init_Program__R_Field_14 = localVariables.newGroupInRecord("pnd_Init_Program__R_Field_14", "REDEFINE", pnd_Init_Program);
        pnd_Init_Program_Pnd_Ip_Prefix = pnd_Init_Program__R_Field_14.newFieldInGroup("pnd_Init_Program_Pnd_Ip_Prefix", "#IP-PREFIX", FieldType.STRING, 
            2);
        pnd_Init_Program_Pnd_Ip_Rest = pnd_Init_Program__R_Field_14.newFieldInGroup("pnd_Init_Program_Pnd_Ip_Rest", "#IP-REST", FieldType.STRING, 5);
        pnd_Init_Program_Pnd_Ip_Sufix = pnd_Init_Program__R_Field_14.newFieldInGroup("pnd_Init_Program_Pnd_Ip_Sufix", "#IP-SUFIX", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        localVariables.reset();
        pnd_Transactions.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap796a() throws Exception
    {
        super("Iaap796a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP796A", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ======================================================================
        //*                          SET PAGE HEADINGS
        //*  ======================================================================
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  ======================================================================
        //*                          START OF REPORT
        //*  ======================================================================
        pnd_Init_Program.setValue(Global.getINIT_PROGRAM());                                                                                                              //Natural: ASSIGN #INIT-PROGRAM := *INIT-PROGRAM
                                                                                                                                                                          //Natural: PERFORM #READ-CONTROL-RECORD
        sub_Pnd_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD-1
        while (condition(getWorkFiles().read(1, pnd_Work_Record_1)))
        {
            if (condition(!(pnd_Init_Program_Pnd_Ip_Sufix.equals("T") || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(30) || pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(0))))   //Natural: ACCEPT IF ( #IP-SUFIX = 'T' ) OR ( #W-TRANS-CDE = 030 OR = 000 )
            {
                continue;
            }
            pnd_Total_Records_Read.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-RECORDS-READ
            if (condition(pnd_Total_Records_Read.equals(1)))                                                                                                              //Natural: IF #TOTAL-RECORDS-READ = 1
            {
                pnd_Mode_Hold.setValue(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind);                                                                                          //Natural: MOVE #W-CNTRCT-MODE-IND TO #MODE-HOLD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Pnd_Date_Ccyy.setValue(pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Ccyy);                                                                               //Natural: MOVE #W-TRANS-EFFCTV-DTE-CCYY TO #DATE-CCYY
            pnd_Date_Pnd_Date_Mm.setValue(pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Mm);                                                                                   //Natural: MOVE #W-TRANS-EFFCTV-DTE-MM TO #DATE-MM
            pnd_Date_Pnd_Date_Dd.setValue(pnd_Work_Record_1_Pnd_W_Trans_Effctv_Dte_Dd);                                                                                   //Natural: MOVE #W-TRANS-EFFCTV-DTE-DD TO #DATE-DD
            pnd_C_Trans_Check_Dte.setValue(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte);                                                                                      //Natural: MOVE #W-TRANS-CHECK-DTE TO #C-TRANS-CHECK-DTE
            if (condition(pnd_Work_Record_1_Pnd_W_Trans_Check_Dte.equals(getZero())))                                                                                     //Natural: IF #W-TRANS-CHECK-DTE = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm.equals(1)))                                                                                    //Natural: IF #TRANS-CHECK-DTE-MM = 1
                {
                    pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Mm.setValue(12);                                                                                            //Natural: MOVE 12 TO #TRANS-CHECK-DTE-MM
                    pnd_C_Trans_Check_Dte_Pnd_Trans_Check_Dte_Ccyy.nsubtract(1);                                                                                          //Natural: SUBTRACT 1 FROM #TRANS-CHECK-DTE-CCYY
                    pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000000, //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000000 - #C-TRANS-CHECK-DTE
                        pnd_C_Trans_Check_Dte));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000100, //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000100 - #C-TRANS-CHECK-DTE
                        pnd_C_Trans_Check_Dte));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.notEquals(pnd_Mode_Hold)))                                                                              //Natural: IF #W-CNTRCT-MODE-IND NOT = #MODE-HOLD
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-2
                sub_Pnd_Write_Report_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
                sub_Pnd_Reset_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Mode_Hold.setValue(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind);                                                                                          //Natural: MOVE #W-CNTRCT-MODE-IND TO #MODE-HOLD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr.equals("9999999999")))                                                                                   //Natural: IF #W-TRANS-PPCN-NBR = '9999999999'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #EDIT-CHECK
            sub_Pnd_Edit_Check();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Record_1_Pnd_W_Trans_Cde.equals(102)))                                                                                                 //Natural: IF #W-TRANS-CDE = 102
            {
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.reset();                                                                                                         //Natural: RESET #W-TIAA-TOT-PER-AMT #W-TIAA-TOT-DIV-AMT #W-CREF-UNITS-CNT #W-TIAA-TOT-PER-AMT-B #W-TIAA-TOT-DIV-AMT-B #W-CREF-UNITS-CNT-B
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.reset();
                pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.reset();
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B.reset();
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B.reset();
                pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B.reset();
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #PRODUCT-CODE-TRANS
            sub_Pnd_Product_Code_Trans();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Record_1_Pnd_W_Activity_Code.equals("R")))                                                                                             //Natural: IF #W-ACTIVITY-CODE = 'R'
            {
                pnd_W_Print_Sw.setValue("R");                                                                                                                             //Natural: MOVE 'R' TO #W-PRINT-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Print_Sw.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #W-PRINT-SW
            }                                                                                                                                                             //Natural: END-IF
            pnd_Transactions.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #TRANSACTIONS
            pnd_Trans_Ppcn_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr_1, "-", pnd_Work_Record_1_Pnd_W_Trans_Payee_Cde_A)); //Natural: COMPRESS #W-TRANS-PPCN-NBR-1 '-' #W-TRANS-PAYEE-CDE-A INTO #TRANS-PPCN-NBR LEAVING NO SPACE
            pnd_Rept1_In_Out.setValue("IN ");                                                                                                                             //Natural: MOVE 'IN ' TO #REPT1-IN-OUT
            getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new    //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) #W-PRINT-SW 01X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 72X #REPT1-IN-OUT
                ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),pnd_W_Print_Sw,new ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(72),pnd_Rept1_In_Out);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept1_Net_Per_Pmt.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Pmt), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-PMT = #W-TIAA-TOT-PER-AMT - #W-TIAA-TOT-PER-AMT-B
            pnd_Rept1_Net_Per_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Per_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.subtract(pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt_B)); //Natural: COMPUTE #REPT1-NET-PER-DVD = #W-TIAA-TOT-DIV-AMT - #W-TIAA-TOT-DIV-AMT-B
            pnd_Rept1_Net_Fin_Pay.compute(new ComputeParameters(false, pnd_Rept1_Net_Fin_Pay), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Pay.subtract(pnd_Work_Record_1_Pnd_W_Final_Payment_B)); //Natural: COMPUTE #REPT1-NET-FIN-PAY = #W-TIAA-TOT-FINAL-PAY - #W-FINAL-PAYMENT-B
            pnd_Rept1_Net_Fin_Dvd.compute(new ComputeParameters(false, pnd_Rept1_Net_Fin_Dvd), pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Div.subtract(pnd_Work_Record_1_Pnd_W_Final_Dividend_B)); //Natural: COMPUTE #REPT1-NET-FIN-DVD = #W-TIAA-TOT-FINAL-DIV - #W-FINAL-DIVIDEND-B
            pnd_Rept1_Net_Units.compute(new ComputeParameters(false, pnd_Rept1_Net_Units), pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt.subtract(pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt_B)); //Natural: COMPUTE #REPT1-NET-UNITS = #W-CREF-UNITS-CNT - #W-CREF-UNITS-CNT-B
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-REPORT2
            sub_Pnd_Fill_Up_Report2();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  3/12
            pnd_Rept1_In_Out.setValue("OUT");                                                                                                                             //Natural: MOVE 'OUT' TO #REPT1-IN-OUT
            getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new    //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 02X #W-TIAA-TOT-PER-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 03X #W-TIAA-TOT-DIV-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 03X #W-CREF-UNITS-CNT ( EM = ZZZ,ZZZ.999 ) 03X #W-TIAA-TOT-FINAL-PAY ( EM = ZZ,ZZZ,ZZZ.99 ) 03X #W-TIAA-TOT-FINAL-DIV ( EM = ZZ,ZZZ,ZZZ.99 ) #REPT1-IN-OUT
                ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,new ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
                ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cref_Units_Cnt, new ReportEditMask ("ZZZ,ZZZ.999"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Pay, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Final_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),
                pnd_Rept1_In_Out);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Work_Record_1_Pnd_W_Trans_Date, new ReportEditMask ("MM/DD/YY"),pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,new    //Natural: WRITE ( 1 ) #W-TRANS-DATE ( EM = MM/DD/YY ) #W-CNTRCT-MODE-IND 01X #W-TRANS-CDE ( EM = ZZ9 ) 03X #W-CNTRCT-OPTN-CDE 02X #W-CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999/99 ) 03X #W-TRANS-SUB-CODE 01X #TRANS-PPCN-NBR 01X #PRODUCT-NAME-RPT3 #REPT1-NET-PER-PMT ( EM = ZZ,ZZZ,ZZZ.99- ) #REPT1-NET-PER-DVD ( EM = ZZ,ZZZ,ZZZ.99- ) 01X #REPT1-NET-UNITS ( EM = ZZZZ,ZZZ.999- ) #REPT1-NET-FIN-PAY ( EM = ZZ,ZZZ,ZZZ.99- ) #REPT1-NET-FIN-DVD ( EM = ZZ,ZZZ,ZZZ.99- ) 'NET' /
                ColumnSpacing(1),pnd_Work_Record_1_Pnd_W_Trans_Cde, new ReportEditMask ("ZZ9"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Cntrct_Optn_Cde,new 
                ColumnSpacing(2),pnd_Work_Record_1_Pnd_W_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999/99"),new ColumnSpacing(3),pnd_Work_Record_1_Pnd_W_Trans_Sub_Code,new 
                ColumnSpacing(1),pnd_Trans_Ppcn_Nbr,new ColumnSpacing(1),pnd_Product_Name_Rpt3,pnd_Rept1_Net_Per_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),pnd_Rept1_Net_Per_Dvd, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Rept1_Net_Units, new ReportEditMask ("ZZZZ,ZZZ.999-"),pnd_Rept1_Net_Fin_Pay, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),pnd_Rept1_Net_Fin_Dvd, new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),"NET",NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Transactions.equals("N")))                                                                                                                      //Natural: IF #TRANSACTIONS = 'N'
        {
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"****************************************************** ");                                                        //Natural: WRITE ( 1 ) '****************************************************** '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 1 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 1 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*        NO TRANSACTIONS IN THIS RUN                 * ");                                                        //Natural: WRITE ( 1 ) '*        NO TRANSACTIONS IN THIS RUN                 * '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 1 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 1 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"****************************************************** ");                                                        //Natural: WRITE ( 1 ) '****************************************************** '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-2
        sub_Pnd_Write_Report_2();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-G-A
        sub_Pnd_Write_Report_G_A();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-G-M
        sub_Pnd_Write_Report_G_M();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "========== IAAP796A ==============");                                                                                                      //Natural: WRITE '========== IAAP796A =============='
        if (Global.isEscape()) return;
        if (condition(pnd_Init_Program_Pnd_Ip_Sufix.equals("T")))                                                                                                         //Natural: IF #IP-SUFIX = 'T'
        {
            getReports().write(0, "**** IN TEST MODE NOT ALL RECORDS WERE PROCESSED ****");                                                                               //Natural: WRITE '**** IN TEST MODE NOT ALL RECORDS WERE PROCESSED ****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "TOTAL RECORDS PROCESSED ",pnd_Total_Records_Read);                                                                                         //Natural: WRITE 'TOTAL RECORDS PROCESSED ' #TOTAL-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "==================================");                                                                                                      //Natural: WRITE '=================================='
        if (Global.isEscape()) return;
        //*  ======================================================================
        //*                          START OF SUBROUTINES
        //*  ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MODE-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-CONTROL-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-A
        //*  ==================
        //*   REAL ESTATE LINE  AND TIAA ACCESS   9/08
        //*  ==================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-M
        //*  ==================
        //*   REAL ESTATE LINE    AND TIAA ACCESS  9/08  START
        //*  ==================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-G-A
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-G-M
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-UP-REPORT2
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT-2
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRODUCT-CODE-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #EDIT-CHECK
        //* ************************  O N   E R R O R  ****************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Pnd_Mode_Trans() throws Exception                                                                                                                    //Natural: #MODE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet484 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MODE-HOLD;//Natural: VALUE 100
        if (condition((pnd_Mode_Hold.equals(100))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MA");                                                                                                                                 //Natural: MOVE 'MA' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Mode_Hold.equals(601))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MB");                                                                                                                                 //Natural: MOVE 'MB' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Mode_Hold.equals(602))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MC");                                                                                                                                 //Natural: MOVE 'MC' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Mode_Hold.equals(603))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MD");                                                                                                                                 //Natural: MOVE 'MD' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Mode_Hold.equals(701))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("ME");                                                                                                                                 //Natural: MOVE 'ME' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Mode_Hold.equals(702))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MF");                                                                                                                                 //Natural: MOVE 'MF' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Mode_Hold.equals(703))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MG");                                                                                                                                 //Natural: MOVE 'MG' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Mode_Hold.equals(704))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MH");                                                                                                                                 //Natural: MOVE 'MH' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Mode_Hold.equals(705))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MI");                                                                                                                                 //Natural: MOVE 'MI' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Mode_Hold.equals(706))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MJ");                                                                                                                                 //Natural: MOVE 'MJ' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Mode_Hold.equals(801))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MK");                                                                                                                                 //Natural: MOVE 'MK' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Mode_Hold.equals(802))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("ML");                                                                                                                                 //Natural: MOVE 'ML' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Mode_Hold.equals(803))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MM");                                                                                                                                 //Natural: MOVE 'MM' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Mode_Hold.equals(804))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MN");                                                                                                                                 //Natural: MOVE 'MN' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Mode_Hold.equals(805))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MO");                                                                                                                                 //Natural: MOVE 'MO' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Mode_Hold.equals(806))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MP");                                                                                                                                 //Natural: MOVE 'MP' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Mode_Hold.equals(807))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MQ");                                                                                                                                 //Natural: MOVE 'MQ' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Mode_Hold.equals(808))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MR");                                                                                                                                 //Natural: MOVE 'MR' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Mode_Hold.equals(809))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MS");                                                                                                                                 //Natural: MOVE 'MS' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Mode_Hold.equals(810))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MT");                                                                                                                                 //Natural: MOVE 'MT' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Mode_Hold.equals(811))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MU");                                                                                                                                 //Natural: MOVE 'MU' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Mode_Hold.equals(812))))
        {
            decideConditionsMet484++;
            pnd_Mode_Code.setValue("MV");                                                                                                                                 //Natural: MOVE 'MV' TO #MODE-CODE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "ERROR IN MODE","=",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,"=",pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,"=",pnd_Mode_Hold);              //Natural: WRITE 'ERROR IN MODE' '=' #W-TRANS-PPCN-NBR '=' #W-CNTRCT-MODE-IND '=' #MODE-HOLD
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Read_Control_Record() throws Exception                                                                                                           //Natural: #READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'MA'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "MA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ02")))
        {
            pnd_Rept2_Prod_Titles_T.getValue(1).setValue("TIAA PERIODIC PAYMENT ");                                                                                       //Natural: MOVE 'TIAA PERIODIC PAYMENT ' TO #REPT2-PROD-TITLES-T ( 1 )
            pnd_Rept2_Prod_Titles_T.getValue(2).setValue("TIAA PERIODIC DIVIDEND");                                                                                       //Natural: MOVE 'TIAA PERIODIC DIVIDEND' TO #REPT2-PROD-TITLES-T ( 2 )
            //*  TAM 03/01
            pnd_Rept2_Prod_Titles_T.getValue(3).setValue("TIAA FINAL PAYMENT");                                                                                           //Natural: MOVE 'TIAA FINAL PAYMENT' TO #REPT2-PROD-TITLES-T ( 3 )
            //*  TAM 03/01
            pnd_Rept2_Prod_Titles_T.getValue(4).setValue("TIAA FINAL DIVIDEND");                                                                                          //Natural: MOVE 'TIAA FINAL DIVIDEND' TO #REPT2-PROD-TITLES-T ( 4 )
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(pnd_I.greaterOrEqual(2) && pnd_I.lessOrEqual(20) && iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).greater(" ") &&                    //Natural: IF #I = 2 THRU 20 AND CNTRL-FUND-CDE ( #I ) > ' ' AND #TOTAL-CREF-SW = ' '
                    pnd_Total_Cref_Sw.equals(" ")))
                {
                    pnd_Total_Cref_Products.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-CREF-PRODUCTS
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF CNTRL-FUND-CDE ( #I ) NE ' '
                {
                    if (condition(pnd_I.greaterOrEqual(2) && pnd_I.lessOrEqual(20)))                                                                                      //Natural: IF #I = 2 THRU 20
                    {
                        pnd_Cntrl_Fund_Cde.setValue(iaa_Cntrl_Rcrd_1_View_Cntrl_Fund_Cde.getValue(pnd_I));                                                                //Natural: MOVE CNTRL-FUND-CDE ( #I ) TO #CNTRL-FUND-CDE
                        iaan051a_Fund.setValue(pnd_Cntrl_Fund_Cde_Pnd_Cntrl_Fund_Cde_2);                                                                                  //Natural: MOVE #CNTRL-FUND-CDE-2 TO IAAN051A-FUND
                        iaan051a_Length.setValue(3);                                                                                                                      //Natural: MOVE 3 TO IAAN051A-LENGTH
                        iaan051a_Desc.reset();                                                                                                                            //Natural: RESET IAAN051A-DESC IAAN051A-CMPY-DESC #R-PROD-TITLES-C #R-PROD-TITLES-2-C
                        iaan051a_Cmpy_Desc.reset();
                        pnd_R_Prod_Titles_C.reset();
                        pnd_R_Prod_Titles_2_C.reset();
                        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), iaan051a_Fund, iaan051a_Desc, iaan051a_Cmpy_Desc, iaan051a_Length);                    //Natural: CALLNAT 'IAAN051A' USING IAAN051A-FUND IAAN051A-DESC IAAN051A-CMPY-DESC IAAN051A-LENGTH
                        if (condition(Global.isEscape())) return;
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Co.setValue(iaan051a_Cmpy_Desc);                                                                              //Natural: MOVE IAAN051A-CMPY-DESC TO #RPT-TITLE-C-CO #RPT-TITLE-2-C-CO
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Co.setValue(iaan051a_Cmpy_Desc);
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #RPT-TITLE-C-PROD #RPT-TITLE-2-C-PROD
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Prod.setValue(iaan051a_Desc);
                        pnd_R_Prod_Titles_C_Pnd_Rpt_Title_C_Units.setValue("UNITS");                                                                                      //Natural: MOVE 'UNITS' TO #RPT-TITLE-C-UNITS
                        pnd_R_Prod_Titles_2_C_Pnd_Rpt_Title_2_C_Payments.setValue("PAYMENTS");                                                                            //Natural: MOVE 'PAYMENTS' TO #RPT-TITLE-2-C-PAYMENTS
                        pnd_Rept2_Prod_Titles_C.getValue(pnd_I).setValue(pnd_R_Prod_Titles_C);                                                                            //Natural: MOVE #R-PROD-TITLES-C TO #REPT2-PROD-TITLES-C ( #I )
                        pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I).setValue(pnd_R_Prod_Titles_2_C);                                                                        //Natural: MOVE #R-PROD-TITLES-2-C TO #REPT2-PROD-TITLES-2-C ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Cref_Sw.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #TOTAL-CREF-SW
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  #READ-CONTROL-RECORD
    }
    private void sub_Pnd_Write_Report_A() throws Exception                                                                                                                //Natural: #WRITE-REPORT-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Rept2_Mode_Sub.setValue(1);                                                                                                                                   //Natural: MOVE 1 TO #REPT2-MODE-SUB
        pnd_Rept2_Mode.setValue(pnd_Mode_Hold);                                                                                                                           //Natural: MOVE #MODE-HOLD TO #REPT2-MODE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Text.setValue("(ANNUAL)");                                                                                                                                    //Natural: MOVE '(ANNUAL)' TO #TEXT
        //*  ====================
        //*   TIAA PAYMENT LINE
        //*  ====================
        pnd_Rept2_Tot_Per_Pmt_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Pmt_Out), pnd_Rept2_Tot_Per_Pmt_In.add(pnd_Rept2_Tot_Per_Pmt_N_Crit));           //Natural: COMPUTE #REPT2-TOT-PER-PMT-OUT = #REPT2-TOT-PER-PMT-IN + #REPT2-TOT-PER-PMT-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(1),pnd_Rept2_Tot_Per_Pmt_In, new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 1 ) #REPT2-TOT-PER-PMT-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-PER-PMT-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-PMT-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Per_Pmt_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Pmt_Out, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ====================
        //*   TIAA DIVIDEND LINE
        //*  ====================
        pnd_Rept2_Tot_Per_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Dvd_Out), pnd_Rept2_Tot_Per_Dvd_In.add(pnd_Rept2_Tot_Per_Dvd_N_Crit));           //Natural: COMPUTE #REPT2-TOT-PER-DVD-OUT = #REPT2-TOT-PER-DVD-IN + #REPT2-TOT-PER-DVD-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(2),pnd_Rept2_Tot_Per_Dvd_In, new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 2 ) #REPT2-TOT-PER-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-PER-DVD-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-PER-DVD-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Per_Dvd_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Per_Dvd_Out, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ========================
        //*   TIAA FINAL PAYMENT LINE
        //*  ========================
        pnd_Rept2_Tot_Fin_Pay_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Pay_Out), pnd_Rept2_Tot_Fin_Pay_In.add(pnd_Rept2_Tot_Fin_Pay_N_Crit));           //Natural: COMPUTE #REPT2-TOT-FIN-PAY-OUT = #REPT2-TOT-FIN-PAY-IN + #REPT2-TOT-FIN-PAY-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(3),pnd_Rept2_Tot_Fin_Pay_In, new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 3 ) #REPT2-TOT-FIN-PAY-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-FIN-PAY-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-FIN-PAY-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Fin_Pay_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Fin_Pay_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  =========================
        //*   TIAA FINAL DIVIDEND LINE
        //*  =========================
        //*  9/08 START
        pnd_Rept2_Tot_Fin_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Dvd_Out), pnd_Rept2_Tot_Fin_Dvd_In.add(pnd_Rept2_Tot_Fin_Dvd_N_Crit));           //Natural: COMPUTE #REPT2-TOT-FIN-DVD-OUT = #REPT2-TOT-FIN-DVD-IN + #REPT2-TOT-FIN-DVD-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(4),pnd_Rept2_Tot_Fin_Dvd_In, new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 4 ) #REPT2-TOT-FIN-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-FIN-DVD-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOT-FIN-DVD-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Fin_Dvd_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Tot_Fin_Dvd_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
            pnd_Rept2_Total_Units_Prod_Out_A.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-A
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #I ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
            pnd_Rept2_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-A
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #I ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        pnd_Total_Cref_Products_Cnt.compute(new ComputeParameters(false, pnd_Total_Cref_Products_Cnt), (pnd_Total_Cref_Products.add(1)));                                 //Natural: COMPUTE #TOTAL-CREF-PRODUCTS-CNT = ( #TOTAL-CREF-PRODUCTS + 1 )
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #TOTAL-CREF-PRODUCTS-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Total_Cref_Products_Cnt)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_I.equals(1) || pnd_I.equals(9) || pnd_I.equals(11)))                                                                                        //Natural: IF #I = 1 OR = 9 OR = 11
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_A.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-A
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-A
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Prod_Titles_C.getValue(21).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 21 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(21),new ColumnSpacing(2),pnd_Rept2_Total_Units_Prod_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 21 ) 02X #REPT2-TOTAL-UNITS-PROD-IN-A ( EM = ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-TOTAL-UNIT-PROD-N-CRIT-A ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-TOTAL-UNITS-PROD-OUT-A ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(2),pnd_Rept2_Total_Unit_Prod_N_Crit_A, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(2),pnd_Rept2_Total_Units_Prod_Out_A, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_2_C.getValue(22).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 22 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(22),new ColumnSpacing(1),pnd_Rept2_Total_Units_Pymt_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 22 ) 01X #REPT2-TOTAL-UNITS-PYMT-IN-A ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOTAL-UNITS-PYMT-OUT-A ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Total_Unit_Pymt_N_Crit_A, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_Out_A, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  #WRITE-REPORT-A
    }
    private void sub_Pnd_Write_Report_M() throws Exception                                                                                                                //Natural: #WRITE-REPORT-M
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Rept2_Mode_Sub.setValue(1);                                                                                                                                   //Natural: MOVE 1 TO #REPT2-MODE-SUB
        pnd_Rept2_Mode.setValue(pnd_Mode_Hold);                                                                                                                           //Natural: MOVE #MODE-HOLD TO #REPT2-MODE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Text.setValue("(MONTHLY)");                                                                                                                                   //Natural: MOVE '(MONTHLY)' TO #TEXT
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_K.setValue(9);                                                                                                                                                //Natural: ASSIGN #K := 9
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
            pnd_Rept2_Total_Units_Prod_Out_M.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-M
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_K),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #K ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
            pnd_Rept2_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                          //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-M
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_K),new ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #K ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_K.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #K
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        pnd_Total_Cref_Products_Cnt.compute(new ComputeParameters(false, pnd_Total_Cref_Products_Cnt), (pnd_Total_Cref_Products.add(1).add(20)));                         //Natural: COMPUTE #TOTAL-CREF-PRODUCTS-CNT = ( #TOTAL-CREF-PRODUCTS + 1 +20 )
        FOR03:                                                                                                                                                            //Natural: FOR #I = 21 TO #TOTAL-CREF-PRODUCTS-CNT
        for (pnd_I.setValue(21); condition(pnd_I.lessOrEqual(pnd_Total_Cref_Products_Cnt)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  9/08
            if (condition(pnd_I.equals(21) || pnd_I.equals(29) || pnd_I.equals(31)))                                                                                      //Natural: IF #I = 21 OR = 29 OR = 31
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-TOT-UNITS-PROD-IN ( #I ) + #REPT2-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Prod_Out_M.nadd(pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PROD-OUT-M
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PROD-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-TOTAL-UNITS-PYMT-OUT-M
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 03X #REPT2-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 05X #REPT2-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(3),pnd_Rept2_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(5),pnd_Rept2_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Prod_Titles_C.getValue(21).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 21 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(21),new ColumnSpacing(2),pnd_Rept2_Total_Units_Prod_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 21 ) 02X #REPT2-TOTAL-UNITS-PROD-IN-M ( EM = ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-TOTAL-UNIT-PROD-N-CRIT-M ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-TOTAL-UNITS-PROD-OUT-M ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(2),pnd_Rept2_Total_Unit_Prod_N_Crit_M, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(2),pnd_Rept2_Total_Units_Prod_Out_M, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_2_C.getValue(22).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 22 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(22),pnd_Rept2_Total_Units_Pymt_In_M,            //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 22 ) #REPT2-TOTAL-UNITS-PYMT-IN-M ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-TOTAL-UNITS-PYMT-OUT-M ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_Total_Unit_Pymt_N_Crit_M, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(3),pnd_Rept2_Total_Units_Pymt_Out_M, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  #WRITE-REPORT-M
    }
    private void sub_Pnd_Write_Report_G_A() throws Exception                                                                                                              //Natural: #WRITE-REPORT-G-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Rept2_Mode.setValue("TOTAL");                                                                                                                                 //Natural: MOVE 'TOTAL' TO #REPT2-MODE
        pnd_Text.setValue("(ANNUAL)");                                                                                                                                    //Natural: MOVE '(ANNUAL)' TO #TEXT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Rept2_G_Tot_Per_Pmt_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Pmt_Out), pnd_Rept2_G_Tot_Per_Pmt_In.add(pnd_Rept2_G_Tot_Per_Pmt_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-PER-PMT-OUT = #REPT2-G-TOT-PER-PMT-IN + #REPT2-G-TOT-PER-PMT-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(1),new ColumnSpacing(1),pnd_Rept2_G_Tot_Per_Pmt_In,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 1 ) 01X #REPT2-G-TOT-PER-PMT-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-PER-PMT-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-PER-PMT-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Per_Pmt_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Per_Pmt_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Rept2_G_Tot_Per_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Dvd_Out), pnd_Rept2_G_Tot_Per_Dvd_In.add(pnd_Rept2_G_Tot_Per_Dvd_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-PER-DVD-OUT = #REPT2-G-TOT-PER-DVD-IN + #REPT2-G-TOT-PER-DVD-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(2),pnd_Rept2_G_Tot_Per_Dvd_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 2 ) #REPT2-G-TOT-PER-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-PER-DVD-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-PER-DVD-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Per_Dvd_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Per_Dvd_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Rept2_G_Tot_Fin_Pay_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Pay_Out), pnd_Rept2_G_Tot_Fin_Pay_In.add(pnd_Rept2_G_Tot_Fin_Pay_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-FIN-PAY-OUT = #REPT2-G-TOT-FIN-PAY-IN + #REPT2-G-TOT-FIN-PAY-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(3),pnd_Rept2_G_Tot_Fin_Pay_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 3 ) #REPT2-G-TOT-FIN-PAY-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-FIN-PAY-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-FIN-PAY-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Fin_Pay_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Fin_Pay_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  9/08 START
        pnd_Rept2_G_Tot_Fin_Dvd_Out.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Dvd_Out), pnd_Rept2_G_Tot_Fin_Dvd_In.add(pnd_Rept2_G_Tot_Fin_Dvd_N_Crit));   //Natural: COMPUTE #REPT2-G-TOT-FIN-DVD-OUT = #REPT2-G-TOT-FIN-DVD-IN + #REPT2-G-TOT-FIN-DVD-N-CRIT
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_T.getValue(4),pnd_Rept2_G_Tot_Fin_Dvd_In,                    //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-T ( 4 ) #REPT2-G-TOT-FIN-DVD-IN ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-FIN-DVD-N-CRIT ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-FIN-DVD-OUT ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Fin_Dvd_N_Crit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Fin_Dvd_Out, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
            pnd_Rept2_G_Total_Units_Prod_Out_A.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-A
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_I),new ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #I ) 02X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
            pnd_Rept2_G_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-A
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_I),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #I ) #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        pnd_Total_Cref_Products_Cnt.compute(new ComputeParameters(false, pnd_Total_Cref_Products_Cnt), (pnd_Total_Cref_Products.add(1)));                                 //Natural: COMPUTE #TOTAL-CREF-PRODUCTS-CNT = ( #TOTAL-CREF-PRODUCTS + 1 )
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO #TOTAL-CREF-PRODUCTS-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Total_Cref_Products_Cnt)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            if (condition(pnd_I.equals(1) || pnd_I.equals(9) || pnd_I.equals(11)))                                                                                        //Natural: IF #I = 1 OR = 9 OR = 11
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_A.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-A
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 02X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_A.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-A
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
                    ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_C.getValue(21).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 21 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(21),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 21 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-A ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRITA ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-A ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Crita, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_A, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_2_C.getValue(22).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 22 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(22),new ColumnSpacing(1),pnd_Rept2_G_Total_Units_Pymt_In_A,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 22 ) 01X #REPT2-G-TOTAL-UNITS-PYMT-IN-A ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOTAL-UNITS-PYMT-OUT-A ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Total_Unit_Pymt_N_Crita, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_Out_A, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  #WRITE-REPORT-G-A
    }
    private void sub_Pnd_Write_Report_G_M() throws Exception                                                                                                              //Natural: #WRITE-REPORT-G-M
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Rept2_Mode.setValue("TOTAL");                                                                                                                                 //Natural: MOVE 'TOTAL' TO #REPT2-MODE
        //*  9/08 START
        pnd_Text.setValue("(MONTHLY)");                                                                                                                                   //Natural: MOVE '(MONTHLY)' TO #TEXT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_K.setValue(9);                                                                                                                                                //Natural: ASSIGN #K := 9
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
            pnd_Rept2_G_Total_Units_Prod_Out_M.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-M
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_K),new ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #K ) 02X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
                ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
            pnd_Rept2_G_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                      //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-M
            getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_K),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I),  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #K ) #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_K.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #K
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        pnd_Rept2_Title_Sub.reset();                                                                                                                                      //Natural: RESET #REPT2-TITLE-SUB
        pnd_Total_Cref_Products_Cnt.compute(new ComputeParameters(false, pnd_Total_Cref_Products_Cnt), (pnd_Total_Cref_Products.add(1).add(20)));                         //Natural: COMPUTE #TOTAL-CREF-PRODUCTS-CNT = ( #TOTAL-CREF-PRODUCTS + 1 +20 )
        FOR05:                                                                                                                                                            //Natural: FOR #I = 21 TO #TOTAL-CREF-PRODUCTS-CNT
        for (pnd_I.setValue(21); condition(pnd_I.lessOrEqual(pnd_Total_Cref_Products_Cnt)); pnd_I.nadd(1))
        {
            pnd_Rept2_Title_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REPT2-TITLE-SUB
            //*  9/08
            if (condition(pnd_I.equals(21) || pnd_I.equals(29) || pnd_I.equals(31)))                                                                                      //Natural: IF #I = 21 OR = 29 OR = 31
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) = #REPT2-G-TOT-UNITS-PROD-IN ( #I ) + #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Prod_Out_M.nadd(pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PROD-OUT-M
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(pnd_Rept2_Title_Sub),new                  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( #REPT2-TITLE-SUB ) 02X #REPT2-G-TOT-UNITS-PROD-IN ( #I ) ( EM = ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 02X #REPT2-G-TOT-UNITS-PROD-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
                    ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_In.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(2),pnd_Rept2_G_Tot_Units_Prod_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I)), pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I).add(pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I))); //Natural: COMPUTE #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) = #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) + #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I )
                pnd_Rept2_G_Total_Units_Pymt_Out_M.nadd(pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I));                                                                  //Natural: ADD #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) TO #REPT2-G-TOTAL-UNITS-PYMT-OUT-M
                getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(pnd_Rept2_Title_Sub),new                //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( #REPT2-TITLE-SUB ) 01X #REPT2-G-TOT-UNITS-PYMT-IN ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOT-UNITS-PYMT-OUT ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
                    ColumnSpacing(1),pnd_Rept2_G_Tot_Units_Pymt_In.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Tot_Units_Pymt_Out.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_C.getValue(21).setValue("CREF TOTAL       UNITS");                                                                                          //Natural: MOVE 'CREF TOTAL       UNITS' TO #REPT2-PROD-TITLES-C ( 21 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_C.getValue(21),new ColumnSpacing(3),pnd_Rept2_G_Total_Units_Prod_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-C ( 21 ) 03X #REPT2-G-TOTAL-UNITS-PROD-IN-M ( EM = ZZ,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNIT-PROD-N-CRITM ( EM = Z,ZZZ,ZZZ,ZZZ.999- ) 05X #REPT2-G-TOTAL-UNITS-PROD-OUT-M ( EM = Z,ZZZ,ZZZ,ZZZ.999- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new ColumnSpacing(5),pnd_Rept2_G_Total_Unit_Prod_N_Critm, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"),new 
            ColumnSpacing(5),pnd_Rept2_G_Total_Units_Prod_Out_M, new ReportEditMask ("ZZ,ZZZ,ZZZ.999-"));
        if (Global.isEscape()) return;
        pnd_Rept2_Prod_Titles_2_C.getValue(22).setValue("CREF TOTAL    PAYMENTS");                                                                                        //Natural: MOVE 'CREF TOTAL    PAYMENTS' TO #REPT2-PROD-TITLES-2-C ( 22 )
        getReports().write(2, ReportOption.NOTITLE,pnd_Rept2_Mode,new ColumnSpacing(2),pnd_Rept2_Prod_Titles_2_C.getValue(22),new ColumnSpacing(1),pnd_Rept2_G_Total_Units_Pymt_In_M,  //Natural: WRITE ( 2 ) #REPT2-MODE 02X #REPT2-PROD-TITLES-2-C ( 22 ) 01X #REPT2-G-TOTAL-UNITS-PYMT-IN-M ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM ( EM = Z,ZZZ,ZZZ,ZZZ.99- ) 03X #REPT2-G-TOTAL-UNITS-PYMT-OUT-M ( EM = Z,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(3),pnd_Rept2_G_Total_Unit_Pymt_N_Critm, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(3),pnd_Rept2_G_Total_Units_Pymt_Out_M, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  #WRITE-REPORT-G-M
    }
    private void sub_Pnd_Fill_Up_Report2() throws Exception                                                                                                               //Natural: #FILL-UP-REPORT2
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Prod_Sub.equals(getZero())))                                                                                                                    //Natural: IF #PROD-SUB = 0
        {
            //*  FINAL PAYMENT & DIVIDEND
            pnd_Rept2_Tot_Fin_Pay_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Pay_N_Crit), pnd_Rept1_Net_Fin_Pay.add(pnd_Rept2_Tot_Fin_Pay_N_Crit));    //Natural: COMPUTE #REPT2-TOT-FIN-PAY-N-CRIT = #REPT1-NET-FIN-PAY + #REPT2-TOT-FIN-PAY-N-CRIT
            pnd_Rept2_G_Tot_Fin_Pay_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Pay_N_Crit), pnd_Rept1_Net_Fin_Pay.add(pnd_Rept2_G_Tot_Fin_Pay_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-FIN-PAY-N-CRIT = #REPT1-NET-FIN-PAY + #REPT2-G-TOT-FIN-PAY-N-CRIT
            pnd_Rept2_Tot_Fin_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Fin_Dvd_N_Crit), pnd_Rept1_Net_Fin_Dvd.add(pnd_Rept2_Tot_Fin_Dvd_N_Crit));    //Natural: COMPUTE #REPT2-TOT-FIN-DVD-N-CRIT = #REPT1-NET-FIN-DVD + #REPT2-TOT-FIN-DVD-N-CRIT
            pnd_Rept2_G_Tot_Fin_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Fin_Dvd_N_Crit), pnd_Rept1_Net_Fin_Dvd.add(pnd_Rept2_G_Tot_Fin_Dvd_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-FIN-DVD-N-CRIT = #REPT1-NET-FIN-DVD + #REPT2-G-TOT-FIN-DVD-N-CRIT
            pnd_Rept2_Tot_Per_Pmt_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Pmt_N_Crit), pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Per_Pmt_N_Crit));    //Natural: COMPUTE #REPT2-TOT-PER-PMT-N-CRIT = #REPT1-NET-PER-PMT + #REPT2-TOT-PER-PMT-N-CRIT
            pnd_Rept2_G_Tot_Per_Pmt_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Pmt_N_Crit), pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_G_Tot_Per_Pmt_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-PER-PMT-N-CRIT = #REPT1-NET-PER-PMT + #REPT2-G-TOT-PER-PMT-N-CRIT
            pnd_Rept2_Tot_Per_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_Tot_Per_Dvd_N_Crit), pnd_Rept1_Net_Per_Dvd.add(pnd_Rept2_Tot_Per_Dvd_N_Crit));    //Natural: COMPUTE #REPT2-TOT-PER-DVD-N-CRIT = #REPT1-NET-PER-DVD + #REPT2-TOT-PER-DVD-N-CRIT
            pnd_Rept2_G_Tot_Per_Dvd_N_Crit.compute(new ComputeParameters(false, pnd_Rept2_G_Tot_Per_Dvd_N_Crit), pnd_Rept1_Net_Per_Dvd.add(pnd_Rept2_G_Tot_Per_Dvd_N_Crit)); //Natural: COMPUTE #REPT2-G-TOT-PER-DVD-N-CRIT = #REPT1-NET-PER-DVD + #REPT2-G-TOT-PER-DVD-N-CRIT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUAL
        if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("U") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("2")))                                                  //Natural: IF #W-COMPANY-CD = 'U' OR = '2'
        {
            pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),            //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
            pnd_Rept2_Total_Unit_Pymt_N_Crit_A.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                               //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A = #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A + #REPT1-NET-PER-PMT
            pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                          //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
            pnd_Rept2_G_Total_Unit_Pymt_N_Crita.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                              //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA = #REPT2-G-TOTAL-UNIT-PYMT-N-CRITA + #REPT1-NET-PER-PMT
            pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),            //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
            pnd_Rept2_Total_Unit_Prod_N_Crit_A.nadd(pnd_Rept1_Net_Units);                                                                                                 //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRIT-A = #REPT2-TOTAL-UNIT-PROD-N-CRIT-A + #REPT1-NET-UNITS
            pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                            //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
            pnd_Rept2_G_Total_Unit_Prod_N_Crita.nadd(pnd_Rept1_Net_Units);                                                                                                //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRITA = #REPT2-G-TOTAL-UNIT-PROD-N-CRITA + #REPT1-NET-UNITS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MONTHLY
            if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("W") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("4")))                                              //Natural: IF #W-COMPANY-CD = 'W' OR = '4'
            {
                pnd_Prod_Sub.compute(new ComputeParameters(false, pnd_Prod_Sub), (pnd_Prod_Sub.add(20)));                                                                 //Natural: COMPUTE #PROD-SUB = ( #PROD-SUB + 20 )
                pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT1-NET-PER-PMT + #REPT2-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Per_Pmt.add(pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Pymt_N_Crit_M.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                           //Natural: COMPUTE #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M = #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Tot_Unit_Pymt_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Per_Pmt);                                                                      //Natural: COMPUTE #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PYMT-N-CRIT ( #PROD-SUB ) + #REPT1-NET-PER-PMT
                pnd_Rept2_G_Total_Unit_Pymt_N_Critm.nadd(pnd_Rept1_Net_Per_Pmt);                                                                                          //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM = #REPT2-G-TOTAL-UNIT-PYMT-N-CRITM + #REPT1-NET-PER-PMT
                pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).compute(new ComputeParameters(false, pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)),        //Natural: COMPUTE #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT1-NET-UNITS + #REPT2-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB )
                    pnd_Rept1_Net_Units.add(pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub)));
                pnd_Rept2_Total_Unit_Prod_N_Crit_M.nadd(pnd_Rept1_Net_Units);                                                                                             //Natural: COMPUTE #REPT2-TOTAL-UNIT-PROD-N-CRIT-M = #REPT2-TOTAL-UNIT-PROD-N-CRIT-M + #REPT1-NET-UNITS
                pnd_Rept2_G_Tot_Unit_Prod_N_Crit.getValue(pnd_Prod_Sub).nadd(pnd_Rept1_Net_Units);                                                                        //Natural: COMPUTE #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) = #REPT2-G-TOT-UNIT-PROD-N-CRIT ( #PROD-SUB ) + #REPT1-NET-UNITS
                pnd_Rept2_G_Total_Unit_Prod_N_Critm.nadd(pnd_Rept1_Net_Units);                                                                                            //Natural: COMPUTE #REPT2-G-TOTAL-UNIT-PROD-N-CRITM = #REPT2-G-TOTAL-UNIT-PROD-N-CRITM + #REPT1-NET-UNITS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  #FILL-UP-REPORT2
    }
    private void sub_Pnd_Write_Report_2() throws Exception                                                                                                                //Natural: #WRITE-REPORT-2
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM #MODE-TRANS
        sub_Pnd_Mode_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-A
        sub_Pnd_Write_Report_A();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT-M
        sub_Pnd_Write_Report_M();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Pnd_Product_Code_Trans() throws Exception                                                                                                            //Natural: #PRODUCT-CODE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1 ") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1G") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1S"))) //Natural: IF #W-PRODUCT-CDE = '1 ' OR = '1G' OR = '1S'
        {
            pnd_Prod_Sub.setValue(0);                                                                                                                                     //Natural: MOVE 0 TO #PROD-SUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*    MOVE RIGHT #W-PRODUCT-CDE TO #W-PRODUCT-CDE
            //*   CHANGED ABOVE TO FOLLOWING TO CORRECT FUND-CODE MOVE 6/02
            pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde_Num_1, MoveOption.RightJustified);                                     //Natural: MOVE RIGHT JUSTIFIED #W-PRODUCT-CDE-NUM-1 TO #W-PRODUCT-CDE-NUM-1
            DbsUtil.examine(new ExamineSource(pnd_Work_Record_1_Pnd_W_Product_Cde), new ExamineSearch(" "), new ExamineReplace("0"));                                     //Natural: EXAMINE #W-PRODUCT-CDE FOR ' ' REPLACE '0'
            pnd_Prod_Sub.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde_Num);                                                                                               //Natural: MOVE #W-PRODUCT-CDE-NUM TO #PROD-SUB
        }                                                                                                                                                                 //Natural: END-IF
        iaan051a_Fund.setValue(pnd_Work_Record_1_Pnd_W_Product_Cde);                                                                                                      //Natural: MOVE #W-PRODUCT-CDE TO IAAN051A-FUND
        iaan051a_Length.setValue(3);                                                                                                                                      //Natural: MOVE 3 TO IAAN051A-LENGTH
        iaan051a_Desc.reset();                                                                                                                                            //Natural: RESET IAAN051A-DESC IAAN051A-CMPY-DESC #PRODUCT-NAME-RPT3
        iaan051a_Cmpy_Desc.reset();
        pnd_Product_Name_Rpt3.reset();
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), iaan051a_Fund, iaan051a_Desc, iaan051a_Cmpy_Desc, iaan051a_Length);                                    //Natural: CALLNAT 'IAAN051A' USING IAAN051A-FUND IAAN051A-DESC IAAN051A-CMPY-DESC IAAN051A-LENGTH
        if (condition(Global.isEscape())) return;
        if (condition(iaan051a_Desc.equals(" ")))                                                                                                                         //Natural: IF IAAN051A-DESC = ' '
        {
            pnd_Product_Name_Rpt3.setValue("CHECK PROD. CODE");                                                                                                           //Natural: MOVE 'CHECK PROD. CODE' TO #PRODUCT-NAME-RPT3
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUAL
        if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("U") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("2")))                                                  //Natural: IF #W-COMPANY-CD = 'U' OR = '2'
        {
            pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                     //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
            pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode.setValue("-A");                                                                                              //Natural: MOVE '-A' TO #PRODUCT-NAME-RPT3-MODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MONTHLY
            if (condition(pnd_Work_Record_1_Pnd_W_Company_Cd.equals("W") || pnd_Work_Record_1_Pnd_W_Company_Cd.equals("4")))                                              //Natural: IF #W-COMPANY-CD = 'W' OR = '4'
            {
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Mode.setValue("-M");                                                                                          //Natural: MOVE '-M' TO #PRODUCT-NAME-RPT3-MODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Product_Name_Rpt3_Pnd_Product_Name_Rpt3_Prod.setValue(iaan051a_Desc);                                                                                 //Natural: MOVE IAAN051A-DESC TO #PRODUCT-NAME-RPT3-PROD
                if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1")))                                                                                           //Natural: IF #W-PRODUCT-CDE = '1'
                {
                    pnd_Product_Name_Rpt3.setValue("TIAA GROUP      ");                                                                                                   //Natural: MOVE 'TIAA GROUP      ' TO #PRODUCT-NAME-RPT3
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  #FILL-UP-REPORT2
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  LB 04/02
        pnd_Rept2_Tot_Per_Pmt_In.reset();                                                                                                                                 //Natural: RESET #REPT2-TOT-PER-PMT-IN #REPT2-TOT-PER-DVD-IN #REPT2-TOT-FIN-PAY-IN #REPT2-TOT-FIN-DVD-IN #REPT2-TOT-PER-PMT-OUT #REPT2-TOT-PER-DVD-OUT #REPT2-TOT-FIN-PAY-OUT #REPT2-TOT-FIN-DVD-OUT #REPT2-TOT-PER-PMT-N-CRIT #REPT2-TOT-PER-DVD-N-CRIT #REPT2-TOT-FIN-PAY-N-CRIT #REPT2-TOT-FIN-DVD-N-CRIT #REPT2-TOTAL-UNITS-PROD-IN-A #REPT2-TOTAL-UNITS-PYMT-IN-A #REPT2-TOTAL-UNITS-PROD-OUT-A #REPT2-TOTAL-UNITS-PYMT-OUT-A #REPT2-TOTAL-UNIT-PROD-N-CRIT-A #REPT2-TOTAL-UNIT-PYMT-N-CRIT-A #REPT2-TOTAL-UNITS-PROD-OUT-M #REPT2-TOTAL-UNITS-PYMT-OUT-M #REPT2-TOTAL-UNIT-PROD-N-CRIT-M #REPT2-TOTAL-UNIT-PYMT-N-CRIT-M #REPT2-TOTAL-UNITS-PROD-IN-M #REPT2-TOTAL-UNITS-PYMT-IN-M #REPT2-TOT-UNITS-PROD-IN ( 1:40 ) #REPT2-TOT-UNITS-PYMT-IN ( 1:40 ) #REPT2-TOT-UNITS-PROD-OUT ( 1:40 ) #REPT2-TOT-UNITS-PYMT-OUT ( 1:40 ) #REPT2-TOT-UNIT-PROD-N-CRIT ( 1:40 ) #REPT2-TOT-UNIT-PYMT-N-CRIT ( 1:40 )
        pnd_Rept2_Tot_Per_Dvd_In.reset();
        pnd_Rept2_Tot_Fin_Pay_In.reset();
        pnd_Rept2_Tot_Fin_Dvd_In.reset();
        pnd_Rept2_Tot_Per_Pmt_Out.reset();
        pnd_Rept2_Tot_Per_Dvd_Out.reset();
        pnd_Rept2_Tot_Fin_Pay_Out.reset();
        pnd_Rept2_Tot_Fin_Dvd_Out.reset();
        pnd_Rept2_Tot_Per_Pmt_N_Crit.reset();
        pnd_Rept2_Tot_Per_Dvd_N_Crit.reset();
        pnd_Rept2_Tot_Fin_Pay_N_Crit.reset();
        pnd_Rept2_Tot_Fin_Dvd_N_Crit.reset();
        pnd_Rept2_Total_Units_Prod_In_A.reset();
        pnd_Rept2_Total_Units_Pymt_In_A.reset();
        pnd_Rept2_Total_Units_Prod_Out_A.reset();
        pnd_Rept2_Total_Units_Pymt_Out_A.reset();
        pnd_Rept2_Total_Unit_Prod_N_Crit_A.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Crit_A.reset();
        pnd_Rept2_Total_Units_Prod_Out_M.reset();
        pnd_Rept2_Total_Units_Pymt_Out_M.reset();
        pnd_Rept2_Total_Unit_Prod_N_Crit_M.reset();
        pnd_Rept2_Total_Unit_Pymt_N_Crit_M.reset();
        pnd_Rept2_Total_Units_Prod_In_M.reset();
        pnd_Rept2_Total_Units_Pymt_In_M.reset();
        pnd_Rept2_Tot_Units_Prod_In.getValue(1,":",40).reset();
        pnd_Rept2_Tot_Units_Pymt_In.getValue(1,":",40).reset();
        pnd_Rept2_Tot_Units_Prod_Out.getValue(1,":",40).reset();
        pnd_Rept2_Tot_Units_Pymt_Out.getValue(1,":",40).reset();
        pnd_Rept2_Tot_Unit_Prod_N_Crit.getValue(1,":",40).reset();
        pnd_Rept2_Tot_Unit_Pymt_N_Crit.getValue(1,":",40).reset();
        //*  #RESET-PARA
    }
    private void sub_Pnd_Edit_Check() throws Exception                                                                                                                    //Natural: #EDIT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(100) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(601) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(602)  //Natural: IF #W-CNTRCT-MODE-IND = 100 OR = 601 OR = 602 OR = 603 OR = 701 OR = 702 OR = 703 OR = 704 OR = 705 OR = 706 OR = 801 OR = 802 OR = 803 OR = 804 OR = 805 OR = 806 OR = 807 OR = 808 OR = 809 OR = 810 OR = 811 OR = 812
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(603) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(701) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(702) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(703) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(704) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(705) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(706) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(801) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(802) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(803) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(804) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(805) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(806) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(807) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(808) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(809) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(810) || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(811) 
            || pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind.equals(812)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOHDR,"| INVALID MODE FOUND ",pnd_Work_Record_1_Pnd_W_Cntrct_Mode_Ind,"    |");                                            //Natural: WRITE NOHDR '| INVALID MODE FOUND ' #W-CNTRCT-MODE-IND '    |'
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);                                                                             //Natural: WRITE 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"===================================");                                                                              //Natural: WRITE NOHDR '==================================='
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((((pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1 ") || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1G")) || pnd_Work_Record_1_Pnd_W_Product_Cde.equals("1S"))  //Natural: IF ( #W-PRODUCT-CDE = '1 ' OR = '1G' OR = '1S' ) AND ( #W-TIAA-TOT-PER-AMT = 0 ) AND ( #W-TIAA-TOT-DIV-AMT = 0 )
            && pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt.equals(getZero())) && pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt.equals(getZero()))))
        {
            getReports().write(0, ReportOption.NOHDR,"| NO MONEY TEACHERS PRODUCT","TIAA-TOT-PER-AMT = ",pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Per_Amt,"TIAA-TOT-DIV-AMT = ",  //Natural: WRITE NOHDR '| NO MONEY TEACHERS PRODUCT' 'TIAA-TOT-PER-AMT = ' #W-TIAA-TOT-PER-AMT 'TIAA-TOT-DIV-AMT = ' #W-TIAA-TOT-DIV-AMT
                pnd_Work_Record_1_Pnd_W_Tiaa_Tot_Div_Amt);
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER ",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr);                                                                             //Natural: WRITE 'CONTRACT NUMBER '#W-TRANS-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Record_1_Pnd_W_Product_Cde.equals("  ")))                                                                                                  //Natural: IF #W-PRODUCT-CDE = '  '
        {
            getReports().write(0, ReportOption.NOHDR,"| NO PRODUCT FOUND ",pnd_Work_Record_1_Pnd_W_Product_Cde,pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,                    //Natural: WRITE NOHDR '| NO PRODUCT FOUND ' #W-PRODUCT-CDE #W-TRANS-PPCN-NBR '    |'
                "    |");
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr.equals(" ")))                                                                                                //Natural: IF #W-TRANS-PPCN-NBR = ' '
        {
            getReports().write(0, ReportOption.NOHDR,"| PPCN-NBR",pnd_Work_Record_1_Pnd_W_Trans_Ppcn_Nbr,"    |");                                                        //Natural: WRITE NOHDR '| PPCN-NBR' #W-TRANS-PPCN-NBR '    |'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
            DbsUtil.terminateApplication("External Subroutine WRITE_TEST_DATA is missing from the collection!");                                                          //Natural: PERFORM WRITE-TEST-DATA
        }                                                                                                                                                                 //Natural: END-IF
        //*  #EDIT-CHECK
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1)," PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(21),"IA ADMINISTRATION - NEW ISSUES (TYPE 30)        ",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X ' PROGRAM:' *PROGRAM 21X 'IA ADMINISTRATION - NEW ISSUES (TYPE 30)        ' 30X 'PAGE :' *PAGE-NUMBER ( 1 ) / 01X 'RUN DATE: ' *DATU 14X '     IA TRANSACTIONS FOR ACCOUNTING DATE ' #DATE ( EM = ZZ/ZZ/9999 ) // 26X 'FINAL'/ '    TRAN   MO TR    CON  PAYMENT ISSUE   CONTRACT' 13X 'PERIODIC       PERIODIC   TIAA/CREF          FINAL          FINAL' 1X 'INP' / '    DATE   DE ANN   OPT    DATE  TYPE      NUMBER' 01X ' PROD.       PAYMENT       DIVIDEND ' 06X 'UNITS        PAYMENT       DIVIDEND OUT' / '--------  --- ---   ---  ------- ----- -----------' '----- -------------  -------------' 01X '-----------  -------------  ------------- ---' /
                        ColumnSpacing(30),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(1),"RUN DATE: ",Global.getDATU(),new ColumnSpacing(14),"     IA TRANSACTIONS FOR ACCOUNTING DATE ",pnd_Date, 
                        new ReportEditMask ("ZZ/ZZ/9999"),NEWLINE,NEWLINE,new ColumnSpacing(26),"FINAL",NEWLINE,"    TRAN   MO TR    CON  PAYMENT ISSUE   CONTRACT",new 
                        ColumnSpacing(13),"PERIODIC       PERIODIC   TIAA/CREF          FINAL          FINAL",new ColumnSpacing(1),"INP",NEWLINE,"    DATE   DE ANN   OPT    DATE  TYPE      NUMBER",new 
                        ColumnSpacing(1)," PROD.       PAYMENT       DIVIDEND ",new ColumnSpacing(6),"UNITS        PAYMENT       DIVIDEND OUT",NEWLINE,"--------  --- ---   ---  ------- ----- -----------","----- -------------  -------------",new 
                        ColumnSpacing(1),"-----------  -------------  ------------- ---",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1)," PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(21),"IA ADMINISTRATION - NEW ISSUES (TYPE 30)        ",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR 01X ' PROGRAM:' *PROGRAM 21X 'IA ADMINISTRATION - NEW ISSUES (TYPE 30)        ' 16X 'PAGE :' *PAGE-NUMBER ( 2 ) / 01X 'RUN DATE: ' *DATU 09X '    IA TRANSACTIONS FOR ACCOUNTING DATE ' #DATE ( EM = ZZ/ZZ/9999 ) #TEXT // 57X '  IA   '/ ' MODE  FUND                        MASTER INPUT' '      TRANSACTIONS        MASTER OUTPUT' 01X '----  ------------------------ ----------------' '   ----------------    ----------------'
                        ColumnSpacing(16),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,new ColumnSpacing(1),"RUN DATE: ",Global.getDATU(),new ColumnSpacing(9),"    IA TRANSACTIONS FOR ACCOUNTING DATE ",pnd_Date, 
                        new ReportEditMask ("ZZ/ZZ/9999"),pnd_Text,NEWLINE,NEWLINE,new ColumnSpacing(57),"  IA   ",NEWLINE," MODE  FUND                        MASTER INPUT","      TRANSACTIONS        MASTER OUTPUT",new 
                        ColumnSpacing(1),"----  ------------------------ ----------------","   ----------------    ----------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
