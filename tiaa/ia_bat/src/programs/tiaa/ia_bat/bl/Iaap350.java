/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:25:44 PM
**        * FROM NATURAL PROGRAM : Iaap350
************************************************************
**        * FILE NAME            : Iaap350.java
**        * CLASS NAME           : Iaap350
**        * INSTANCE NAME        : Iaap350
************************************************************
************************************************************************
* PROGRAM  : IAAP350
* SYSTEM   : IAD
* TITLE    : IA ADMIN COR TRANSACTIONS
* CREATED  : FEB 7, 1996
* FUNCTION : CREATE COR TRANSACTIONS FOR TERMINATED RECORDS
*           FED INTO COR SYSTEM
*
*
* HISTORY
* 04/27/2011 - POPULATE DAY OF DEATH USING IAA-DC-STTLMNT-REQ
* 07/11/2011 - CREATE AN ADDITIONAL 010 TRANSACTION FOR DEATH
*              AND POPULATE 2ND ANNT SSN.
* 11/15/2011 - LOGIC TO HANDLE DOUBLE DEATH.
* 05/06/2013 - DO NOT SEND LUMPSUM TERMINATION FOR BENEFICIARIES
*              AND JOINT LIFE DEATH CASES PER MIKE WEBER.
*              SCAN ON 5/13
* 10/01/2013 - CHANGE FREQUENCY FROM MONTHLY TO DAILY. SCAN ON 10/13
* 08/14/2014 - SEND DEATH OF FIRST ANNUITANT TO MDM FOR JOINT.
*              SCAN ON 8/14
* 02/10/2017 - SEND DEATH OF 2ND ANNUITANT, DEATH REVERSAL, TRANSFER
*              TERMINATION, TRANSFER REINSTATE TO MDM AND ANOTHER
*              TRANSACTION 012 TO SWITCH BACK PAYEE CODE TO 01 FROM
*              02 FOR FIRST ANNUITANT. SCAN 2/17
* 06/07/2017 - PIN EXPANSION. SCAN 082017 FOR CHANGES.
* 05/09/2018 - ALWAYS RESET STATUS FOR ACTIVATION. SCAN 5/19
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap350 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;

    private DataAccessProgramView vw_cntrct_Trans;
    private DbsField cntrct_Trans_Bfre_Imge_Id;
    private DbsField cntrct_Trans_Trans_Dte;
    private DbsField cntrct_Trans_Invrse_Trans_Dte;
    private DbsField cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField cntrct_Trans_Cntrct_Type_Cde;
    private DbsField cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField pnd_Cntrct_Bfre_Key;

    private DbsGroup pnd_Cntrct_Bfre_Key__R_Field_1;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Cntrct_Cash_Cde;
    private DbsField cpr_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Wthdrwl_Dte;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;
    private DbsField cpr_Bnfcry_Xref_Ind;
    private DbsField cpr_Bnfcry_Dod_Dte;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Hold_Cde;
    private DbsField cpr_Cntrct_Pend_Dte;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Trmnte_Rsn;

    private DataAccessProgramView vw_cpr2;
    private DbsField cpr2_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr2_Cntrct_Part_Payee_Cde;
    private DbsField cpr2_Cpr_Id_Nbr;
    private DbsField cpr2_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr2_Cntrct_Actvty_Cde;
    private DbsField cpr2_Cntrct_Trmnte_Rsn;

    private DataAccessProgramView vw_cpr_Trans;
    private DbsField cpr_Trans_Trans_Dte;
    private DbsField cpr_Trans_Invrse_Trans_Dte;
    private DbsField cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Trans_Cpr_Id_Nbr;
    private DbsField cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_2;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bf1;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cpr_Trans_Dte;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_3;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Date_Parms;

    private DbsGroup pnd_Date_Parms__R_Field_4;
    private DbsField pnd_Date_Parms_Pnd_Ovrrde_Dte;
    private DbsField pnd_Date_Parms__Filler1;
    private DbsField pnd_Date_Parms_Pnd_Ov_Chck_Dte;
    private DbsField pnd_Activate;
    private DbsField pnd_First;

    private DataAccessProgramView vw_settl;
    private DbsField settl_Sttlmnt_Id_Nbr;
    private DbsField settl_Sttlmnt_Tax_Id_Nbr;
    private DbsField settl_Sttlmnt_Req_Seq_Nbr;
    private DbsField settl_Sttlmnt_Process_Type;
    private DbsField settl_Sttlmnt_Timestamp;
    private DbsField settl_Sttlmnt_Status_Cde;
    private DbsField settl_Sttlmnt_Status_Timestamp;
    private DbsField settl_Sttlmnt_Cwf_Wpid;
    private DbsField settl_Sttlmnt_Decedent_Name;
    private DbsField settl_Sttlmnt_Decedent_Type;
    private DbsField settl_Sttlmnt_Dod_Dte;
    private DbsField settl_Sttlmnt_2nd_Dod_Dte;
    private DbsField pnd_Pin_Taxid_Seq_Key;

    private DbsGroup pnd_Pin_Taxid_Seq_Key__R_Field_5;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr;

    private DbsGroup pnd_Work_Rec;
    private DbsField pnd_Work_Rec_Pnd_Rqstr_Id;
    private DbsField pnd_Work_Rec_Pnd_Fnctn_Cde;
    private DbsField pnd_Work_Rec_Pnd_Pin;
    private DbsField pnd_Work_Rec_Pnd_Ssn1;
    private DbsField pnd_Work_Rec_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Rec_Pnd_Old_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Rec_Pnd_Nme_Prfx;
    private DbsField pnd_Work_Rec_Pnd_Lst_Nme;
    private DbsField pnd_Work_Rec_Pnd_Frst_Nme;
    private DbsField pnd_Work_Rec_Pnd_Mddle_Nme;
    private DbsField pnd_Work_Rec_Pnd_Nme_Sffx;
    private DbsField pnd_Work_Rec_Pnd_Dob;
    private DbsField pnd_Work_Rec_Pnd_Ssn2;
    private DbsField pnd_Work_Rec_Pnd_Sex_Cde;
    private DbsField pnd_Work_Rec_Pnd_Dod;

    private DbsGroup pnd_Work_Rec__R_Field_6;
    private DbsField pnd_Work_Rec_Pnd_Dod_Ccyymm;
    private DbsField pnd_Work_Rec_Pnd_Dod_Dd;
    private DbsField pnd_Work_Rec_Pnd_F1;
    private DbsField pnd_Work_Rec_Pnd_Cntrct_Nbr;
    private DbsField pnd_Work_Rec_Pnd_Issue_Dte;

    private DbsGroup pnd_Work_Rec__R_Field_7;
    private DbsField pnd_Work_Rec_Pnd_Issue_Yyyymm;
    private DbsField pnd_Work_Rec_Pnd_Issue_Dd;
    private DbsField pnd_Work_Rec_Pnd_Term_Cde;
    private DbsField pnd_Work_Rec_Pnd_Term_Dte;
    private DbsField pnd_Work_Rec_Pnd_Cntrct_Payee;
    private DbsField pnd_Work_Rec_Pnd_F2a;
    private DbsField pnd_Work_Rec_Pnd_Trmntn_Dte;
    private DbsField pnd_Work_Rec_Pnd_F2b;
    private DbsField pnd_Work_Rec_Pnd_Tax_Id;
    private DbsField pnd_Work_Rec_Pnd_F3;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_8;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_9;
    private DbsField pnd_Cpr_Key_Pnd_C_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_C_Payee;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_10;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_11;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_Alpha;
    private DbsField pnd_Sve_Sub_Cde;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Last_Updte_Dte;
    private DbsField pnd_Next_Updte_Dte;
    private DbsField pnd_Wrk_Dod;

    private DbsGroup pnd_Wrk_Dod__R_Field_12;
    private DbsField pnd_Wrk_Dod_Pnd_Wrk_Ccyy;
    private DbsField pnd_Wrk_Dod_Pnd_Wrk_Mm;
    private DbsField pnd_Datd;
    private DbsField pnd_Date8;

    private DbsGroup pnd_Date8__R_Field_13;
    private DbsField pnd_Date8_Pnd_Last_Updt_Ccyymm;
    private DbsField pnd_Date8_Pnd_Last_Updt_Dd;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_14;
    private DbsField pnd_Year_Pnd_Year_N;
    private DbsField pnd_Desc_Txt;
    private DbsField pnd_Tot_Cor_Trn06;
    private DbsField pnd_Tot_Cor_Trn20;
    private DbsField pnd_Tot_Cor_Trn40;
    private DbsField pnd_Tot_Cor_Trn66;
    private DbsField pnd_Tot_Cor_Xfr_Term;
    private DbsField pnd_Tot_Cor_Dth_Act;
    private DbsField pnd_Tot_Cor_Xfr_Act;
    private DbsField pnd_T_Dte;
    private DbsField pnd_Joint;
    private DbsField pnd_Reversal;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_15;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Todays_Dte;

    private DbsGroup pnd_Todays_Dte__R_Field_16;
    private DbsField pnd_Todays_Dte_Pnd_Todays_Dte_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        registerRecord(vw_iaa_Cntrct);

        vw_cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_cntrct_Trans", "CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        cntrct_Trans_Bfre_Imge_Id = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        cntrct_Trans_Trans_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cntrct_Trans_Invrse_Trans_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cntrct_Trans_Cntrct_Ppcn_Nbr = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        cntrct_Trans_Cntrct_Optn_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        cntrct_Trans_Cntrct_Orgn_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        cntrct_Trans_Cntrct_Issue_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        cntrct_Trans_Cntrct_Crrncy_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        cntrct_Trans_Cntrct_Type_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_cntrct_Trans.getRecord().newFieldInGroup("cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        registerRecord(vw_cntrct_Trans);

        pnd_Cntrct_Bfre_Key = localVariables.newFieldInRecord("pnd_Cntrct_Bfre_Key", "#CNTRCT-BFRE-KEY", FieldType.STRING, 18);

        pnd_Cntrct_Bfre_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Bfre_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Bfre_Key);
        pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Cntrct_Bfre_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte = pnd_Cntrct_Bfre_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Cntrct_Cash_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CASH_CDE");
        cpr_Cntrct_Emplymnt_Trmnt_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Wthdrwl_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_WTHDRWL_DTE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        cpr_Bnfcry_Xref_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr_Bnfcry_Dod_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Hold_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        cpr_Cntrct_Pend_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Trmnte_Rsn = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TRMNTE_RSN");
        registerRecord(vw_cpr);

        vw_cpr2 = new DataAccessProgramView(new NameInfo("vw_cpr2", "CPR2"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr2_Cntrct_Part_Ppcn_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr2_Cntrct_Part_Payee_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr2_Cpr_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr2_Prtcpnt_Tax_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr2_Cntrct_Actvty_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr2_Cntrct_Trmnte_Rsn = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TRMNTE_RSN");
        registerRecord(vw_cpr2);

        vw_cpr_Trans = new DataAccessProgramView(new NameInfo("vw_cpr_Trans", "CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE");
        cpr_Trans_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cpr_Trans_Invrse_Trans_Dte = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cpr_Trans_Cntrct_Part_Payee_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr_Trans_Cpr_Id_Nbr = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        cpr_Trans_Cntrct_Actvty_Cde = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        cpr_Trans_Cntrct_Trmnte_Rsn = vw_cpr_Trans.getRecord().newFieldInGroup("cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        registerRecord(vw_cpr_Trans);

        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_2", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bf1 = pnd_Cpr_Bfre_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bf1", "#BF1", FieldType.STRING, 1);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Bfre_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Bfre_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Cpr_Trans_Dte = pnd_Cpr_Bfre_Key__R_Field_2.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cpr_Trans_Dte", "#CPR-TRANS-DTE", FieldType.TIME);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_3", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Date_Parms = localVariables.newFieldInRecord("pnd_Date_Parms", "#DATE-PARMS", FieldType.STRING, 17);

        pnd_Date_Parms__R_Field_4 = localVariables.newGroupInRecord("pnd_Date_Parms__R_Field_4", "REDEFINE", pnd_Date_Parms);
        pnd_Date_Parms_Pnd_Ovrrde_Dte = pnd_Date_Parms__R_Field_4.newFieldInGroup("pnd_Date_Parms_Pnd_Ovrrde_Dte", "#OVRRDE-DTE", FieldType.STRING, 8);
        pnd_Date_Parms__Filler1 = pnd_Date_Parms__R_Field_4.newFieldInGroup("pnd_Date_Parms__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Date_Parms_Pnd_Ov_Chck_Dte = pnd_Date_Parms__R_Field_4.newFieldInGroup("pnd_Date_Parms_Pnd_Ov_Chck_Dte", "#OV-CHCK-DTE", FieldType.STRING, 
            8);
        pnd_Activate = localVariables.newFieldInRecord("pnd_Activate", "#ACTIVATE", FieldType.BOOLEAN, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);

        vw_settl = new DataAccessProgramView(new NameInfo("vw_settl", "SETTL"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        settl_Sttlmnt_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "STTLMNT_ID_NBR");
        settl_Sttlmnt_Tax_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "STTLMNT_TAX_ID_NBR");
        settl_Sttlmnt_Req_Seq_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "STTLMNT_REQ_SEQ_NBR");
        settl_Sttlmnt_Process_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STTLMNT_PROCESS_TYPE");
        settl_Sttlmnt_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STTLMNT_TIMESTAMP");
        settl_Sttlmnt_Status_Cde = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "STTLMNT_STATUS_CDE");
        settl_Sttlmnt_Status_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Timestamp", "STTLMNT-STATUS-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        settl_Sttlmnt_Cwf_Wpid = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Cwf_Wpid", "STTLMNT-CWF-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STTLMNT_CWF_WPID");
        settl_Sttlmnt_Decedent_Name = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Decedent_Name", "STTLMNT-DECEDENT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_NAME");
        settl_Sttlmnt_Decedent_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        settl_Sttlmnt_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_DOD_DTE");
        settl_Sttlmnt_2nd_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_2ND_DOD_DTE");
        registerRecord(vw_settl);

        pnd_Pin_Taxid_Seq_Key = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Key", "#PIN-TAXID-SEQ-KEY", FieldType.BINARY, 24);

        pnd_Pin_Taxid_Seq_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Key__R_Field_5", "REDEFINE", pnd_Pin_Taxid_Seq_Key);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_5.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr", "#STTLMNT-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_5.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr", 
            "#STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_5.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr", 
            "#STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Work_Rec = localVariables.newGroupInRecord("pnd_Work_Rec", "#WORK-REC");
        pnd_Work_Rec_Pnd_Rqstr_Id = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Rqstr_Id", "#RQSTR-ID", FieldType.STRING, 8);
        pnd_Work_Rec_Pnd_Fnctn_Cde = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Fnctn_Cde", "#FNCTN-CDE", FieldType.STRING, 3);
        pnd_Work_Rec_Pnd_Pin = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Pin", "#PIN", FieldType.STRING, 12);
        pnd_Work_Rec_Pnd_Ssn1 = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Ssn1", "#SSN1", FieldType.NUMERIC, 9);
        pnd_Work_Rec_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Old_Cntrct_Ppcn_Nbr", "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Rec_Pnd_Old_Cntrct_Payee_Cde = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Old_Cntrct_Payee_Cde", "#OLD-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Work_Rec_Pnd_Nme_Prfx = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Nme_Prfx", "#NME-PRFX", FieldType.STRING, 8);
        pnd_Work_Rec_Pnd_Lst_Nme = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Lst_Nme", "#LST-NME", FieldType.STRING, 30);
        pnd_Work_Rec_Pnd_Frst_Nme = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Frst_Nme", "#FRST-NME", FieldType.STRING, 30);
        pnd_Work_Rec_Pnd_Mddle_Nme = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Mddle_Nme", "#MDDLE-NME", FieldType.STRING, 30);
        pnd_Work_Rec_Pnd_Nme_Sffx = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Nme_Sffx", "#NME-SFFX", FieldType.STRING, 8);
        pnd_Work_Rec_Pnd_Dob = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Dob", "#DOB", FieldType.NUMERIC, 8);
        pnd_Work_Rec_Pnd_Ssn2 = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Ssn2", "#SSN2", FieldType.NUMERIC, 9);
        pnd_Work_Rec_Pnd_Sex_Cde = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Sex_Cde", "#SEX-CDE", FieldType.STRING, 1);
        pnd_Work_Rec_Pnd_Dod = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Dod", "#DOD", FieldType.NUMERIC, 8);

        pnd_Work_Rec__R_Field_6 = pnd_Work_Rec.newGroupInGroup("pnd_Work_Rec__R_Field_6", "REDEFINE", pnd_Work_Rec_Pnd_Dod);
        pnd_Work_Rec_Pnd_Dod_Ccyymm = pnd_Work_Rec__R_Field_6.newFieldInGroup("pnd_Work_Rec_Pnd_Dod_Ccyymm", "#DOD-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Work_Rec_Pnd_Dod_Dd = pnd_Work_Rec__R_Field_6.newFieldInGroup("pnd_Work_Rec_Pnd_Dod_Dd", "#DOD-DD", FieldType.NUMERIC, 2);
        pnd_Work_Rec_Pnd_F1 = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_F1", "#F1", FieldType.STRING, 5);
        pnd_Work_Rec_Pnd_Cntrct_Nbr = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Work_Rec_Pnd_Issue_Dte = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 8);

        pnd_Work_Rec__R_Field_7 = pnd_Work_Rec.newGroupInGroup("pnd_Work_Rec__R_Field_7", "REDEFINE", pnd_Work_Rec_Pnd_Issue_Dte);
        pnd_Work_Rec_Pnd_Issue_Yyyymm = pnd_Work_Rec__R_Field_7.newFieldInGroup("pnd_Work_Rec_Pnd_Issue_Yyyymm", "#ISSUE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Work_Rec_Pnd_Issue_Dd = pnd_Work_Rec__R_Field_7.newFieldInGroup("pnd_Work_Rec_Pnd_Issue_Dd", "#ISSUE-DD", FieldType.STRING, 2);
        pnd_Work_Rec_Pnd_Term_Cde = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Term_Cde", "#TERM-CDE", FieldType.STRING, 1);
        pnd_Work_Rec_Pnd_Term_Dte = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Term_Dte", "#TERM-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Rec_Pnd_Cntrct_Payee = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 2);
        pnd_Work_Rec_Pnd_F2a = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_F2a", "#F2A", FieldType.STRING, 3);
        pnd_Work_Rec_Pnd_Trmntn_Dte = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Trmntn_Dte", "#TRMNTN-DTE", FieldType.STRING, 8);
        pnd_Work_Rec_Pnd_F2b = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_F2b", "#F2B", FieldType.STRING, 3);
        pnd_Work_Rec_Pnd_Tax_Id = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Tax_Id", "#TAX-ID", FieldType.NUMERIC, 9);
        pnd_Work_Rec_Pnd_F3 = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_F3", "#F3", FieldType.STRING, 224);
        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_8", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte", "#TRANS-CHCK-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Ppcn_Nbr", 
            "#TRANS-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Payee_Cde = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cntrct_Payee_Cde", 
            "#TRANS-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde", "#TRANS-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_9", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_C_Cntrct = pnd_Cpr_Key__R_Field_9.newFieldInGroup("pnd_Cpr_Key_Pnd_C_Cntrct", "#C-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_C_Payee = pnd_Cpr_Key__R_Field_9.newFieldInGroup("pnd_Cpr_Key_Pnd_C_Payee", "#C-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_10", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        pnd_Cntrct_Payee_Key__R_Field_11 = pnd_Cntrct_Payee_Key__R_Field_10.newGroupInGroup("pnd_Cntrct_Payee_Key__R_Field_11", "REDEFINE", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_Alpha = pnd_Cntrct_Payee_Key__R_Field_11.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_Alpha", 
            "#CNTRCT-PAYEE-CDE-ALPHA", FieldType.STRING, 2);
        pnd_Sve_Sub_Cde = localVariables.newFieldInRecord("pnd_Sve_Sub_Cde", "#SVE-SUB-CDE", FieldType.STRING, 3);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 3);
        pnd_Last_Updte_Dte = localVariables.newFieldInRecord("pnd_Last_Updte_Dte", "#LAST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Next_Updte_Dte = localVariables.newFieldInRecord("pnd_Next_Updte_Dte", "#NEXT-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Wrk_Dod = localVariables.newFieldInRecord("pnd_Wrk_Dod", "#WRK-DOD", FieldType.NUMERIC, 6);

        pnd_Wrk_Dod__R_Field_12 = localVariables.newGroupInRecord("pnd_Wrk_Dod__R_Field_12", "REDEFINE", pnd_Wrk_Dod);
        pnd_Wrk_Dod_Pnd_Wrk_Ccyy = pnd_Wrk_Dod__R_Field_12.newFieldInGroup("pnd_Wrk_Dod_Pnd_Wrk_Ccyy", "#WRK-CCYY", FieldType.NUMERIC, 4);
        pnd_Wrk_Dod_Pnd_Wrk_Mm = pnd_Wrk_Dod__R_Field_12.newFieldInGroup("pnd_Wrk_Dod_Pnd_Wrk_Mm", "#WRK-MM", FieldType.NUMERIC, 2);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);

        pnd_Date8__R_Field_13 = localVariables.newGroupInRecord("pnd_Date8__R_Field_13", "REDEFINE", pnd_Date8);
        pnd_Date8_Pnd_Last_Updt_Ccyymm = pnd_Date8__R_Field_13.newFieldInGroup("pnd_Date8_Pnd_Last_Updt_Ccyymm", "#LAST-UPDT-CCYYMM", FieldType.NUMERIC, 
            6);
        pnd_Date8_Pnd_Last_Updt_Dd = pnd_Date8__R_Field_13.newFieldInGroup("pnd_Date8_Pnd_Last_Updt_Dd", "#LAST-UPDT-DD", FieldType.NUMERIC, 2);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_14 = localVariables.newGroupInRecord("pnd_Year__R_Field_14", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_14.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);
        pnd_Desc_Txt = localVariables.newFieldInRecord("pnd_Desc_Txt", "#DESC-TXT", FieldType.STRING, 30);
        pnd_Tot_Cor_Trn06 = localVariables.newFieldInRecord("pnd_Tot_Cor_Trn06", "#TOT-COR-TRN06", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Trn20 = localVariables.newFieldInRecord("pnd_Tot_Cor_Trn20", "#TOT-COR-TRN20", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Trn40 = localVariables.newFieldInRecord("pnd_Tot_Cor_Trn40", "#TOT-COR-TRN40", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Trn66 = localVariables.newFieldInRecord("pnd_Tot_Cor_Trn66", "#TOT-COR-TRN66", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Xfr_Term = localVariables.newFieldInRecord("pnd_Tot_Cor_Xfr_Term", "#TOT-COR-XFR-TERM", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Dth_Act = localVariables.newFieldInRecord("pnd_Tot_Cor_Dth_Act", "#TOT-COR-DTH-ACT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cor_Xfr_Act = localVariables.newFieldInRecord("pnd_Tot_Cor_Xfr_Act", "#TOT-COR-XFR-ACT", FieldType.PACKED_DECIMAL, 7);
        pnd_T_Dte = localVariables.newFieldInRecord("pnd_T_Dte", "#T-DTE", FieldType.STRING, 8);
        pnd_Joint = localVariables.newFieldArrayInRecord("pnd_Joint", "#JOINT", FieldType.NUMERIC, 2, new DbsArrayController(1, 20));
        pnd_Reversal = localVariables.newFieldInRecord("pnd_Reversal", "#REVERSAL", FieldType.BOOLEAN, 1);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        registerRecord(vw_cntrl);

        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_15", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_15.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_15.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);

        pnd_Todays_Dte__R_Field_16 = localVariables.newGroupInRecord("pnd_Todays_Dte__R_Field_16", "REDEFINE", pnd_Todays_Dte);
        pnd_Todays_Dte_Pnd_Todays_Dte_N = pnd_Todays_Dte__R_Field_16.newFieldInGroup("pnd_Todays_Dte_Pnd_Todays_Dte_N", "#TODAYS-DTE-N", FieldType.NUMERIC, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_cntrct_Trans.reset();
        vw_cpr.reset();
        vw_cpr2.reset();
        vw_cpr_Trans.reset();
        vw_naz_Table_Ddm.reset();
        vw_settl.reset();
        vw_cntrl.reset();

        localVariables.reset();
        pnd_Cntrct_Bfre_Key.setInitialValue("1");
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Joint.getValue(1).setInitialValue(3);
        pnd_Joint.getValue(2).setInitialValue(4);
        pnd_Joint.getValue(3).setInitialValue(7);
        pnd_Joint.getValue(4).setInitialValue(8);
        pnd_Joint.getValue(5).setInitialValue(10);
        pnd_Joint.getValue(6).setInitialValue(11);
        pnd_Joint.getValue(7).setInitialValue(12);
        pnd_Joint.getValue(8).setInitialValue(13);
        pnd_Joint.getValue(9).setInitialValue(14);
        pnd_Joint.getValue(10).setInitialValue(15);
        pnd_Joint.getValue(11).setInitialValue(16);
        pnd_Joint.getValue(12).setInitialValue(17);
        pnd_Joint.getValue(13).setInitialValue(18);
        pnd_Joint.getValue(14).setInitialValue(51);
        pnd_Joint.getValue(15).setInitialValue(52);
        pnd_Joint.getValue(16).setInitialValue(53);
        pnd_Joint.getValue(17).setInitialValue(54);
        pnd_Joint.getValue(18).setInitialValue(55);
        pnd_Joint.getValue(19).setInitialValue(56);
        pnd_Joint.getValue(20).setInitialValue(57);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap350() throws Exception
    {
        super("Iaap350");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: FORMAT ( 2 ) LS = 132 PS = 0;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Updte_Dte, pnd_Next_Updte_Dte);                                                               //Natural: CALLNAT 'IAAN0020' #LAST-UPDTE-DTE #NEXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"MM'/'DD'/'YYYY")))                                                                                          //Natural: IF #LAST-UPDTE-DTE = MASK ( MM'/'DD'/'YYYY )
        {
            pnd_Datd.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Last_Updte_Dte);                                                                                 //Natural: MOVE EDITED #LAST-UPDTE-DTE TO #DATD ( EM = MM/DD/YYYY )
            pnd_Date8.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE8
            pnd_Year.setValueEdited(pnd_Datd,new ReportEditMask("YYYY"));                                                                                                 //Natural: MOVE EDITED #DATD ( EM = YYYY ) TO #YEAR
            if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"'01'")))                                                                                                //Natural: IF #LAST-UPDTE-DTE = MASK ( '01' )
            {
                pnd_Year_Pnd_Year_N.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #YEAR-N
            }                                                                                                                                                             //Natural: END-IF
            pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte.compute(new ComputeParameters(false, pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte), pnd_Date8.val());                  //Natural: ASSIGN #TRANS-CHCK-DTE := VAL ( #DATE8 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR   ERROR   ERROR  ERROR",NEWLINE,"NO CONTROL RECORD ON FILE",NEWLINE,"PROCESSING WILL NOT CONTINUE");                             //Natural: WRITE 'ERROR   ERROR   ERROR  ERROR' / 'NO CONTROL RECORD ON FILE' / 'PROCESSING WILL NOT CONTINUE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            pnd_Todays_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DTE
            pnd_Trans_Chck_Dte_Key.setValueEdited(cntrl_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #TRANS-CHCK-DTE-KEY
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM GET-OVERRIDE
        sub_Get_Override();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Date_Parms_Pnd_Ovrrde_Dte.notEquals(" ")))                                                                                                      //Natural: IF #OVRRDE-DTE NE ' '
        {
            pnd_Todays_Dte.setValue(pnd_Date_Parms_Pnd_Ovrrde_Dte);                                                                                                       //Natural: ASSIGN #TODAYS-DTE := #OVRRDE-DTE
            pnd_Trans_Chck_Dte_Key.setValue(pnd_Date_Parms_Pnd_Ov_Chck_Dte);                                                                                              //Natural: ASSIGN #TRANS-CHCK-DTE-KEY := #OV-CHCK-DTE
            cntrl_Cntrl_Todays_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Todays_Dte);                                                                         //Natural: MOVE EDITED #TODAYS-DTE TO CNTRL-TODAYS-DTE ( EM = YYYYMMDD )
            getReports().write(1, ReportOption.NOTITLE,"RUNNING WITH OVERRIDE DATE:",pnd_Date_Parms_Pnd_Ovrrde_Dte,pnd_Date_Parms_Pnd_Ov_Chck_Dte);                       //Natural: WRITE ( 1 ) 'RUNNING WITH OVERRIDE DATE:' #OVRRDE-DTE #OV-CHCK-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.notEquals(pnd_Trans_Chck_Dte_Key_Pnd_Trans_Chck_Dte)))                                                           //Natural: IF IAA-TRANS-RCRD.TRANS-CHECK-DTE NE #TRANS-CHCK-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Todays_Dte.notEquals(pnd_Todays_Dte_Pnd_Todays_Dte_N)))                                                                    //Natural: IF TRANS-TODAYS-DTE NE #TODAYS-DTE-N
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  DISPLAY(1)
            //*    IAA-TRANS-RCRD.TRANS-PPCN-NBR
            //*    IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            //*    IAA-TRANS-RCRD.TRANS-SUB-CDE
            //*    IAA-TRANS-RCRD.TRANS-VERIFY-CDE
            //*    IAA-TRANS-RCRD.TRANS-CDE #TODAYS-DTE
            //*  2/17 - START
            //*  REJECT IF NOT (IAA-TRANS-RCRD.TRANS-CDE = 6 OR = 20 OR = 40 OR = 66)
            pnd_Activate.reset();                                                                                                                                         //Natural: RESET #ACTIVATE
            if (condition(! (iaa_Trans_Rcrd_Trans_Cde.equals(6) || iaa_Trans_Rcrd_Trans_Cde.equals(20) || iaa_Trans_Rcrd_Trans_Cde.equals(40) || iaa_Trans_Rcrd_Trans_Cde.equals(66)  //Natural: REJECT IF NOT ( IAA-TRANS-RCRD.TRANS-CDE = 6 OR = 20 OR = 40 OR = 66 OR = 52 OR = 53 OR = 35 OR = 37 OR = 502 OR = 504 OR = 512 OR = 514 OR = 506 OR = 508 OR = 516 OR = 518 )
                || iaa_Trans_Rcrd_Trans_Cde.equals(52) || iaa_Trans_Rcrd_Trans_Cde.equals(53) || iaa_Trans_Rcrd_Trans_Cde.equals(35) || iaa_Trans_Rcrd_Trans_Cde.equals(37) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(502) || iaa_Trans_Rcrd_Trans_Cde.equals(504) || iaa_Trans_Rcrd_Trans_Cde.equals(512) || iaa_Trans_Rcrd_Trans_Cde.equals(514) 
                || iaa_Trans_Rcrd_Trans_Cde.equals(506) || iaa_Trans_Rcrd_Trans_Cde.equals(508) || iaa_Trans_Rcrd_Trans_Cde.equals(516) || iaa_Trans_Rcrd_Trans_Cde.equals(518))))
            {
                continue;
            }
            //*  35 = ACTIVATION      /* 506,508,516,518
            //*  37 = DEATH REVERSAL
            //*  52 = TERMINATION XFR   502,504,512,514
            //*  53 = ACTIVATION XFR
            //*  2/17 - END
            if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) && pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.equals(iaa_Trans_Rcrd_Trans_Payee_Cde)  //Natural: IF #CNTRCT-PPCN-NBR = IAA-TRANS-RCRD.TRANS-PPCN-NBR AND #CNTRCT-PAYEE-CDE = IAA-TRANS-RCRD.TRANS-PAYEE-CDE AND #SVE-SUB-CDE = IAA-TRANS-RCRD.TRANS-SUB-CDE
                && pnd_Sve_Sub_Cde.equals(iaa_Trans_Rcrd_Trans_Sub_Cde)))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  5/13 - EVERY NEW RECORD, CHECK IF REVERSAL IS LATEST TRANSCTION
            //*  NEW
            //*  RECORD
            if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.notEquals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr) || pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.notEquals(iaa_Trans_Rcrd_Trans_Payee_Cde))) //Natural: IF #CNTRCT-PPCN-NBR NE IAA-TRANS-RCRD.TRANS-PPCN-NBR OR #CNTRCT-PAYEE-CDE NE IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            {
                //*  EVERY NEW RECORD, SET TO FALSE
                pnd_Reversal.reset();                                                                                                                                     //Natural: RESET #REVERSAL
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                         //Natural: ASSIGN #CNTRCT-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                       //Natural: ASSIGN #CNTRCT-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
                pnd_Sve_Sub_Cde.setValue(iaa_Trans_Rcrd_Trans_Sub_Cde);                                                                                                   //Natural: ASSIGN #SVE-SUB-CDE := IAA-TRANS-RCRD.TRANS-SUB-CDE
                //*  2/17 - START
                //*    IF (TRANS-CDE = 66 AND TRANS-ACTVTY-CDE = 'R') /* LAST TRANS IS
                //*      #REVERSAL := TRUE                            /* REVERSAL
                //*    END-IF
                if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(66) && iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")) || iaa_Trans_Rcrd_Trans_Cde.equals(37)))               //Natural: IF ( TRANS-CDE = 66 AND TRANS-ACTVTY-CDE = 'R' ) OR TRANS-CDE = 37
                {
                    pnd_Reversal.setValue(true);                                                                                                                          //Natural: ASSIGN #REVERSAL := TRUE
                    pnd_First.setValue(true);                                                                                                                             //Natural: ASSIGN #FIRST := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  2/17 - END
            }                                                                                                                                                             //Natural: END-IF
            //*  2/17 - START
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  5/13 - ALWAYS REJECT IF
                if (condition(iaa_Trans_Rcrd_Trans_Actvty_Cde.notEquals("A") || pnd_Reversal.getBoolean()))                                                               //Natural: IF TRANS-ACTVTY-CDE NE 'A' OR #REVERSAL
                {
                    //*  5/13   LAST TRANS IS
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  5/13   REVERSAL
                }                                                                                                                                                         //Natural: END-IF
                //*  5/13
            }                                                                                                                                                             //Natural: END-IF
            //*  2/17 - END
            //*  #CNTRCT-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            //*  #CNTRCT-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            pnd_Sve_Sub_Cde.setValue(iaa_Trans_Rcrd_Trans_Sub_Cde);                                                                                                       //Natural: ASSIGN #SVE-SUB-CDE := IAA-TRANS-RCRD.TRANS-SUB-CDE
            pnd_Work_Rec_Pnd_Trmntn_Dte.setValueEdited(iaa_Trans_Rcrd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-DTE ( EM = YYYYMMDD ) TO #TRMNTN-DTE
            pnd_Work_Rec_Pnd_Rqstr_Id.setValue("IATERM");                                                                                                                 //Natural: ASSIGN #WORK-REC.#RQSTR-ID := 'IATERM'
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("010");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '010'
            pnd_Work_Rec_Pnd_Cntrct_Payee.setValueEdited(iaa_Trans_Rcrd_Trans_Payee_Cde,new ReportEditMask("99"));                                                        //Natural: MOVE EDITED IAA-TRANS-RCRD.TRANS-PAYEE-CDE ( EM = 99 ) TO #WORK-REC.#CNTRCT-PAYEE
            vw_iaa_Cntrct.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM IAA-TRANS-RCRD.TRANS-PPCN-NBR
            (
            "R1",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", iaa_Trans_Rcrd_Trans_Ppcn_Nbr, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
            1
            );
            R1:
            while (condition(vw_iaa_Cntrct.readNextRow("R1")))
            {
                if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.equals(iaa_Trans_Rcrd_Trans_Ppcn_Nbr)))                                                                          //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = IAA-TRANS-RCRD.TRANS-PPCN-NBR
                {
                    pnd_Work_Rec_Pnd_Cntrct_Nbr.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);                                                                       //Natural: ASSIGN #WORK-REC.#CNTRCT-NBR := #CNTRCT-PPCN-NBR
                    pnd_Work_Rec_Pnd_Issue_Yyyymm.setValue(0);                                                                                                            //Natural: ASSIGN #WORK-REC.#ISSUE-YYYYMM := 0
                    pnd_Work_Rec_Pnd_Issue_Dd.setValue("00");                                                                                                             //Natural: ASSIGN #WORK-REC.#ISSUE-DD := '00'
                    pnd_Work_Rec_Pnd_Cntrct_Payee.setValueEdited(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde,new ReportEditMask("99"));                                     //Natural: MOVE EDITED #CNTRCT-PAYEE-CDE ( EM = 99 ) TO #WORK-REC.#CNTRCT-PAYEE
                    vw_cpr.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
                    (
                    "R2",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
                    new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
                    1
                    );
                    R2:
                    while (condition(vw_cpr.readNextRow("R2")))
                    {
                        if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.equals(cpr_Cntrct_Part_Ppcn_Nbr) && pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.equals(cpr_Cntrct_Part_Payee_Cde))) //Natural: IF #CNTRCT-PPCN-NBR = CPR.CNTRCT-PART-PPCN-NBR AND #CNTRCT-PAYEE-CDE = CPR.CNTRCT-PART-PAYEE-CDE
                        {
                            pnd_Work_Rec_Pnd_Tax_Id.setValue(0);                                                                                                          //Natural: ASSIGN #WORK-REC.#TAX-ID := 0
                            //*          #WORK-REC.#PIN := CPR.CPR-ID-NBR           /* 2/17
                            //*  2/17
                            pnd_Work_Rec_Pnd_Pin.setValue(cpr_Cpr_Id_Nbr, MoveOption.LeftJustified);                                                                      //Natural: MOVE LEFT CPR.CPR-ID-NBR TO #WORK-REC.#PIN
                            pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                       //Natural: ASSIGN #WORK-REC.#SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                            if (condition(cpr_Prtcpnt_Tax_Id_Nbr.equals(getZero())))                                                                                      //Natural: IF CPR.PRTCPNT-TAX-ID-NBR = 0
                            {
                                pnd_Work_Rec_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);                                                  //Natural: ASSIGN #WORK-REC.#OLD-CNTRCT-PPCN-NBR := #CNTRCT-PPCN-NBR
                                pnd_Work_Rec_Pnd_Old_Cntrct_Payee_Cde.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_Alpha);                                          //Natural: ASSIGN #WORK-REC.#OLD-CNTRCT-PAYEE-CDE := #CNTRCT-PAYEE-CDE-ALPHA
                            }                                                                                                                                             //Natural: END-IF
                            short decideConditionsMet465 = 0;                                                                                                             //Natural: DECIDE ON FIRST VALUE IAA-TRANS-RCRD.TRANS-CDE;//Natural: VALUE 6
                            if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(6))))
                            {
                                decideConditionsMet465++;
                                pnd_Work_Rec_Pnd_Term_Cde.setValue("T");                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'T'
                                pnd_Tot_Cor_Trn06.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-COR-TRN06
                                pnd_Desc_Txt.setValue("TERMINATED FINAL PAYMENT");                                                                                        //Natural: ASSIGN #DESC-TXT := 'TERMINATED FINAL PAYMENT'
                            }                                                                                                                                             //Natural: VALUE 20
                            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(20))))
                            {
                                decideConditionsMet465++;
                                pnd_Work_Rec_Pnd_Term_Cde.setValue("C");                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'C'
                                pnd_Tot_Cor_Trn20.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-COR-TRN20
                                pnd_Desc_Txt.setValue("REVERSAL OF CONTRACT    ");                                                                                        //Natural: ASSIGN #DESC-TXT := 'REVERSAL OF CONTRACT    '
                            }                                                                                                                                             //Natural: VALUE 40
                            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(40))))
                            {
                                decideConditionsMet465++;
                                pnd_Work_Rec_Pnd_Term_Cde.setValue("W");                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'W'
                                pnd_Tot_Cor_Trn40.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-COR-TRN40
                                pnd_Desc_Txt.setValue("FULL WITHDRAWAL         ");                                                                                        //Natural: ASSIGN #DESC-TXT := 'FULL WITHDRAWAL         '
                                //*  2/17 - START
                            }                                                                                                                                             //Natural: VALUE 52
                            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(52))))
                            {
                                decideConditionsMet465++;
                                pnd_Work_Rec_Pnd_Term_Cde.setValue("X");                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'X'
                                pnd_Tot_Cor_Xfr_Term.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-COR-XFR-TERM
                                pnd_Desc_Txt.setValue("TRANSFER TERMINATION    ");                                                                                        //Natural: ASSIGN #DESC-TXT := 'TRANSFER TERMINATION    '
                            }                                                                                                                                             //Natural: VALUE 502,504,512,514
                            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(502) || iaa_Trans_Rcrd_Trans_Cde.equals(504) || iaa_Trans_Rcrd_Trans_Cde.equals(512) 
                                || iaa_Trans_Rcrd_Trans_Cde.equals(514))))
                            {
                                decideConditionsMet465++;
                                if (condition(cpr_Cntrct_Actvty_Cde.equals(9)))                                                                                           //Natural: IF CPR.CNTRCT-ACTVTY-CDE = 9
                                {
                                    pnd_Work_Rec_Pnd_Term_Cde.setValue("X");                                                                                              //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'X'
                                    pnd_Tot_Cor_Xfr_Term.nadd(1);                                                                                                         //Natural: ADD 1 TO #TOT-COR-XFR-TERM
                                    pnd_Desc_Txt.setValue("TRANSFER                ");                                                                                    //Natural: ASSIGN #DESC-TXT := 'TRANSFER                '
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                    //*  5/19
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: VALUE 35,53,506,508,516,518
                            else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(35) || iaa_Trans_Rcrd_Trans_Cde.equals(53) || iaa_Trans_Rcrd_Trans_Cde.equals(506) 
                                || iaa_Trans_Rcrd_Trans_Cde.equals(508) || iaa_Trans_Rcrd_Trans_Cde.equals(516) || iaa_Trans_Rcrd_Trans_Cde.equals(518))))
                            {
                                decideConditionsMet465++;
                                pnd_Activate.setValue(true);                                                                                                              //Natural: ASSIGN #ACTIVATE := TRUE
                                pnd_Work_Rec_Pnd_Term_Cde.setValue(" ");                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-CDE := ' '
                                pnd_Tot_Cor_Xfr_Act.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOT-COR-XFR-ACT
                                //*  2/17 - END
                            }                                                                                                                                             //Natural: ANY VALUE
                            if (condition(decideConditionsMet465 > 0))
                            {
                                pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Year_Pnd_Year_N);                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-DTE := #YEAR-N
                                                                                                                                                                          //Natural: PERFORM NON-DEATH-TRANSACTIONS
                                sub_Non_Death_Transactions();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("R2"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: NONE VALUE
                            else if (condition())
                            {
                                //*  2/17 - START - SEND ALL DEATH
                                //*              IF CNTRCT-OPTN-CDE = #JOINT(*) AND
                                //*                  CPR.CNTRCT-PART-PAYEE-CDE LT 3
                                //*  8/14 START
                                //*                  AND CNTRCT-FIRST-ANNT-DOD-DTE = 0
                                //*  8/14 END
                                //*                ESCAPE BOTTOM
                                //*              END-IF
                                pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("023");                                                                                               //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '023'
                                if (condition(pnd_Reversal.getBoolean()))                                                                                                 //Natural: IF #REVERSAL
                                {
                                                                                                                                                                          //Natural: PERFORM DEATH-REVERSAL
                                    sub_Death_Reversal();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("R2"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Work_Rec_Pnd_Term_Cde.setValue("D");                                                                                              //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'D'
                                    pnd_Desc_Txt.setValue("DEATH CLAIM             ");                                                                                    //Natural: ASSIGN #DESC-TXT := 'DEATH CLAIM             '
                                                                                                                                                                          //Natural: PERFORM DEATH-TRANSACTION
                                    sub_Death_Transaction();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("R2"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                                //*  2/17 - END
                            }                                                                                                                                             //Natural: END-DECIDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR TERMINATION(06) TRANSACTIONS.......",pnd_Tot_Cor_Trn06, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) 'TOTAL COR TERMINATION(06) TRANSACTIONS.......' #TOT-COR-TRN06 ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR REVERSAL(20) TRANSACTIONS..........",pnd_Tot_Cor_Trn20, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) 'TOTAL COR REVERSAL(20) TRANSACTIONS..........' #TOT-COR-TRN20 ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR WITHDRAWAL(40) TRANSACTIONS........",pnd_Tot_Cor_Trn40, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) 'TOTAL COR WITHDRAWAL(40) TRANSACTIONS........' #TOT-COR-TRN40 ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR DEATH CLAIM(66) TRANSACTIONS.......",pnd_Tot_Cor_Trn66, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) 'TOTAL COR DEATH CLAIM(66) TRANSACTIONS.......' #TOT-COR-TRN66 ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  2/17 - START
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR TRANSFERS TERM  TRANSACTIONS.......",pnd_Tot_Cor_Xfr_Term, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 1 ) 'TOTAL COR TRANSFERS TERM  TRANSACTIONS.......' #TOT-COR-XFR-TERM ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR TRANSFERS ACTIVATION TRANS.........",pnd_Tot_Cor_Xfr_Act, new ReportEditMask ("Z,ZZZ,ZZ9"));                //Natural: WRITE ( 1 ) 'TOTAL COR TRANSFERS ACTIVATION TRANS.........' #TOT-COR-XFR-ACT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL COR DEATH REVERSAL TRANSACTIONS........",pnd_Tot_Cor_Dth_Act, new ReportEditMask ("Z,ZZZ,ZZ9"));                //Natural: WRITE ( 1 ) 'TOTAL COR DEATH REVERSAL TRANSACTIONS........' #TOT-COR-DTH-ACT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  2/17 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NON-DEATH-TRANSACTIONS
        //*    MOVE EDITED IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE(EM=99999999)
        //*      TO #DATE8
        //*    #WORK-REC.#DOB := VAL(#DATE8)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEATH-TRANSACTION
        //*  -------------------------------------------
        //*  PRINT CONTROL CONTRACT NUMBER FOR COMBINES
        //*  -------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINE1
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOD
        //* ***********************************************************************
        //* *#STTLMNT-ID-NBR := #PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BEFORE-IMAGE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-01-CPR
        //* ***********************************************************************
        //*  2/17 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BEFORE-CPR
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEATH-REVERSAL
        //*  2/17 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OVERRIDE
        //* ***********************************************************************
    }
    private void sub_Non_Death_Transactions() throws Exception                                                                                                            //Natural: NON-DEATH-TRANSACTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  2/17 - START
        if (condition(pnd_Work_Rec_Pnd_Term_Cde.equals("X")))                                                                                                             //Natural: IF #WORK-REC.#TERM-CDE = 'X'
        {
            pnd_Work_Rec_Pnd_Term_Cde.setValue("T");                                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-CDE := 'T'
            pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Year_Pnd_Year_N);                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-DTE := #YEAR-N
            if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.greater(2)))                                                                                          //Natural: IF #CNTRCT-PAYEE-CDE GT 2
            {
                pnd_Work_Rec_Pnd_Dob.setValue(0);                                                                                                                         //Natural: ASSIGN #WORK-REC.#DOB := 0
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
            sub_Print_Detail_Line1();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Rec.reset();                                                                                                                                         //Natural: RESET #WORK-REC
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activate.getBoolean()))                                                                                                                         //Natural: IF #ACTIVATE
        {
            pnd_Work_Rec_Pnd_Term_Dte.setValue(0);                                                                                                                        //Natural: ASSIGN #WORK-REC.#TERM-DTE := 0
            pnd_Desc_Txt.setValue("ACTIVATION OF CONTRACT    ");                                                                                                          //Natural: ASSIGN #DESC-TXT := 'ACTIVATION OF CONTRACT    '
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
            sub_Print_Detail_Line1();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Rec.reset();                                                                                                                                         //Natural: RESET #WORK-REC
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  2/17 - END
        if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.greater(2)))                                                                                              //Natural: IF #CNTRCT-PAYEE-CDE GT 2
        {
            if (condition(pnd_Work_Rec_Pnd_Term_Cde.equals("W")))                                                                                                         //Natural: IF #WORK-REC.#TERM-CDE = 'W'
            {
                //*  TERMINATION WITH PEND CODE
                if (condition(cpr_Cntrct_Pend_Cde.equals("A") || cpr_Cntrct_Pend_Cde.equals("C")))                                                                        //Natural: IF CPR.CNTRCT-PEND-CDE = 'A' OR = 'C'
                {
                    //*  MEANS LUMPSUM WAS TAKEN
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                    //*  AND WILL NOT BE SENT 5/13
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Rec_Pnd_Dob.setValue(0);                                                                                                                             //Natural: ASSIGN #WORK-REC.#DOB := 0
            pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Year_Pnd_Year_N);                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-DTE := #YEAR-N
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(iaa_Cntrct_Cntrct_First_Annt_Dob_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                             //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE GT 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                //*    MOVE EDITED IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE(EM=99999999)
                //*      TO #DATE8
                //*    #WORK-REC.#DOB := VAL(#DATE8)
                getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                             //Natural: WRITE WORK FILE 1 #WORK-REC
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                              //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE GT 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
            {
                pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Year_Pnd_Year_N);                                                                                                  //Natural: ASSIGN #WORK-REC.#TERM-DTE := #YEAR-N
                getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                             //Natural: WRITE WORK FILE 1 #WORK-REC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
        sub_Print_Detail_Line1();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Rec.reset();                                                                                                                                             //Natural: RESET #WORK-REC
    }
    private void sub_Death_Transaction() throws Exception                                                                                                                 //Natural: DEATH-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.less(3)))                                                                                                 //Natural: IF #CNTRCT-PAYEE-CDE LT 3
        {
            if (condition(iaa_Trans_Rcrd_Trans_Sub_Cde.equals("001")))                                                                                                    //Natural: IF IAA-TRANS-RCRD.TRANS-SUB-CDE = '001'
            {
                if (condition(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                                   //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GT 0
                {
                    //*    MOVE EDITED IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE(EM=99999999)
                    //*      TO #DATE8
                    //*    #WORK-REC.#DOB := VAL(#DATE8)
                    pnd_Wrk_Dod.setValue(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);                                                                                           //Natural: MOVE IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE TO #WRK-DOD
                    pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Wrk_Dod_Pnd_Wrk_Ccyy);                                                                                         //Natural: ASSIGN #WORK-REC.#TERM-DTE := #WRK-CCYY
                    pnd_Work_Rec_Pnd_Dod_Ccyymm.setValue(pnd_Wrk_Dod);                                                                                                    //Natural: ASSIGN #WORK-REC.#DOD-CCYYMM := #WRK-DOD
                    pnd_Work_Rec_Pnd_Dod_Dd.setValue(0);                                                                                                                  //Natural: ASSIGN #WORK-REC.#DOD-DD := 0
                    pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("01");                                                                                                         //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '01'
                    //*      #STTLMNT-PROCESS-TYPE := 'ID1A'  /* 11/11
                                                                                                                                                                          //Natural: PERFORM GET-DOD
                    sub_Get_Dod();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Tot_Cor_Trn66.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-COR-TRN66
                    getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                         //Natural: WRITE WORK FILE 1 #WORK-REC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *END-IF
            //* *IF #CNTRCT-PAYEE-CDE LT 3
            if (condition(iaa_Trans_Rcrd_Trans_Sub_Cde.equals("002")))                                                                                                    //Natural: IF IAA-TRANS-RCRD.TRANS-SUB-CDE = '002'
            {
                if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    //*    MOVE EDITED IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE(EM=99999999)
                    //*      TO #DATE8
                    //*    #WORK-REC.#DOB := VAL(#DATE8)
                    pnd_Wrk_Dod.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                                                            //Natural: MOVE IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE TO #WRK-DOD
                    pnd_Work_Rec_Pnd_Ssn1.setValue(0);                                                                                                                    //Natural: ASSIGN #WORK-REC.#SSN1 := 0
                    //*  07/11
                    //*  07/11
                    if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.greater(getZero())))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN GT 0
                    {
                        pnd_Work_Rec_Pnd_Ssn1.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn);                                                                                  //Natural: ASSIGN #WORK-REC.#SSN1 := IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN
                        //*  07/11
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  11/11
                                                                                                                                                                          //Natural: PERFORM GET-01-CPR
                        sub_Get_01_Cpr();
                        if (condition(Global.isEscape())) {return;}
                        //*        #C-CNTRCT := TRANS-PPCN-NBR                         /* 07/11
                        //*        #C-PAYEE := 01                                      /* 07/11
                        //*        RESET #CPR-FOUND                                    /* 07/11
                        //*        FIND(1) CPR2 WITH CNTRCT-PAYEE-KEY = #CPR-KEY       /* 07/11
                        //*          IF CPR2.PRTCPNT-TAX-ID-NBR NE CPR.PRTCPNT-TAX-ID-NBR  /* 07/11
                        //*            #SSN1 := CPR.PRTCPNT-TAX-ID-NBR                 /* 07/11
                        //*          END-IF                                            /* 07/11
                        //*          #CPR-FOUND := TRUE                                /* 07/11
                        //*        END-FIND                                            /* 07/11
                        //*        IF #CPR-FOUND = FALSE  /* NO PAYEE 01 EXISTS        /* 07/11
                        //*          #SSN1 := CPR.PRTCPNT-TAX-ID-NBR /* USE PART SSN   /* 07/11
                        //*        END-IF                                              /* 07/11
                        //*  07/11
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Wrk_Dod_Pnd_Wrk_Ccyy);                                                                                         //Natural: ASSIGN #WORK-REC.#TERM-DTE := #WRK-CCYY
                    pnd_Work_Rec_Pnd_Dod_Ccyymm.setValue(pnd_Wrk_Dod);                                                                                                    //Natural: ASSIGN #WORK-REC.#DOD-CCYYMM := #WRK-DOD
                    pnd_Work_Rec_Pnd_Dod_Dd.setValue(0);                                                                                                                  //Natural: ASSIGN #WORK-REC.#DOD-DD := 0
                    pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("02");                                                                                                         //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '02'
                    //*      #STTLMNT-PROCESS-TYPE := 'ID2A'                       /* 11/11
                                                                                                                                                                          //Natural: PERFORM GET-DOD
                    sub_Get_Dod();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Tot_Cor_Trn66.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-COR-TRN66
                    getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                         //Natural: WRITE WORK FILE 1 #WORK-REC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.greater(2)))                                                                                              //Natural: IF #CNTRCT-PAYEE-CDE GT 2
        {
            if (condition(cpr_Bnfcry_Dod_Dte.greater(getZero())))                                                                                                         //Natural: IF CPR.BNFCRY-DOD-DTE GT 0
            {
                pnd_Wrk_Dod.setValue(cpr_Bnfcry_Dod_Dte);                                                                                                                 //Natural: MOVE CPR.BNFCRY-DOD-DTE TO #WRK-DOD
                pnd_Work_Rec_Pnd_Term_Dte.setValue(pnd_Wrk_Dod_Pnd_Wrk_Ccyy);                                                                                             //Natural: ASSIGN #WORK-REC.#TERM-DTE := #WRK-CCYY
                pnd_Work_Rec_Pnd_Dod_Ccyymm.setValue(pnd_Wrk_Dod);                                                                                                        //Natural: ASSIGN #WORK-REC.#DOD-CCYYMM := #WRK-DOD
                pnd_Work_Rec_Pnd_Dod_Dd.setValue(0);                                                                                                                      //Natural: ASSIGN #WORK-REC.#DOD-DD := 0
                pnd_Work_Rec_Pnd_Dob.setValue(0);                                                                                                                         //Natural: ASSIGN #WORK-REC.#DOB := 0
                pnd_Tot_Cor_Trn66.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-COR-TRN66
                getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                             //Natural: WRITE WORK FILE 1 #WORK-REC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  07/11 AS REQUIRED FOR MDM UPDATE
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE1
        sub_Print_Detail_Line1();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("010");                                                                                                                       //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '010'
        //*  07/11
        getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                     //Natural: WRITE WORK FILE 1 #WORK-REC
        pnd_Work_Rec.reset();                                                                                                                                             //Natural: RESET #WORK-REC
    }
    private void sub_Print_Detail_Line1() throws Exception                                                                                                                //Natural: PRINT-DETAIL-LINE1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Work_Rec_Pnd_Cntrct_Nbr, new ReportEditMask ("XXXXXXX-XXX"),new ColumnSpacing(1),pnd_Work_Rec_Pnd_Cntrct_Payee,  //Natural: WRITE ( 1 ) / #WORK-REC.#CNTRCT-NBR ( EM = XXXXXXX-XXX ) 1X#WORK-REC.#CNTRCT-PAYEE ( EM = XX ) 6XIAA-TRANS-RCRD.TRANS-CDE ( EM = 999 ) 3X#WORK-REC.#TERM-CDE 4X#DESC-TXT
            new ReportEditMask ("XX"),new ColumnSpacing(6),iaa_Trans_Rcrd_Trans_Cde, new ReportEditMask ("999"),new ColumnSpacing(3),pnd_Work_Rec_Pnd_Term_Cde,new 
            ColumnSpacing(4),pnd_Desc_Txt);
        if (Global.isEscape()) return;
    }
    //*  2/17
    private void sub_Get_Dod() throws Exception                                                                                                                           //Natural: GET-DOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr.setValue(cpr_Cpr_Id_Nbr);                                                                                                //Natural: ASSIGN #STTLMNT-ID-NBR := CPR.CPR-ID-NBR
        vw_settl.startDatabaseRead                                                                                                                                        //Natural: READ SETTL BY PIN-TAXID-SEQ-KEY STARTING FROM #PIN-TAXID-SEQ-KEY
        (
        "READ03",
        new Wc[] { new Wc("PIN_TAXID_SEQ_KEY", ">=", pnd_Pin_Taxid_Seq_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_settl.readNextRow("READ03")))
        {
            if (condition(settl_Sttlmnt_Id_Nbr.notEquals(pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr)))                                                                      //Natural: IF STTLMNT-ID-NBR NE #STTLMNT-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  11/11
            pnd_T_Dte.setValueEdited(settl_Sttlmnt_Status_Timestamp,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED STTLMNT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #T-DTE
            //*  11/11
            //*  11/11
            if (condition(!(DbsUtil.maskMatches(settl_Sttlmnt_Process_Type,"'ID'") && pnd_T_Dte.greaterOrEqual(pnd_Work_Rec_Pnd_Trmntn_Dte))))                            //Natural: ACCEPT IF STTLMNT-PROCESS-TYPE = MASK ( 'ID' ) AND #T-DTE GE #TRMNTN-DTE
            {
                continue;
            }
            //*  11/11
            //*  11/11
            //*  11/11
            //*  11/11
            //*  11/11
            //*  11/11
            short decideConditionsMet768 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE STTLMNT-PROCESS-TYPE;//Natural: VALUE 'ID1A'
            if (condition((settl_Sttlmnt_Process_Type.equals("ID1A"))))
            {
                decideConditionsMet768++;
                pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                     //Natural: ASSIGN #DOD := STTLMNT-DOD-DTE
            }                                                                                                                                                             //Natural: VALUE 'ID2A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID2A"))))
            {
                decideConditionsMet768++;
                pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                 //Natural: ASSIGN #DOD := STTLMNT-2ND-DOD-DTE
            }                                                                                                                                                             //Natural: VALUE 'ID3A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID3A"))))
            {
                decideConditionsMet768++;
                //*  11/11
                //*  11/11
                if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.greater(getZero())))                              //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GT 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN GT 0
                {
                    //*  11/11
                    //*  11/11
                    //*  11/11
                    if (condition(iaa_Trans_Rcrd_Trans_Payee_Cde.equals(1)))                                                                                              //Natural: IF TRANS-PAYEE-CDE = 01
                    {
                        pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                             //Natural: ASSIGN #DOD := STTLMNT-DOD-DTE
                        pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                           //Natural: ASSIGN #WORK-REC.#SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                        pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("01");                                                                                                     //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '01'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                         //Natural: ASSIGN #DOD := STTLMNT-2ND-DOD-DTE
                        pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("02");                                                                                                     //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '02'
                        if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.greater(getZero())))                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN GT 0
                        {
                            pnd_Work_Rec_Pnd_Ssn1.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn);                                                                              //Natural: ASSIGN #WORK-REC.#SSN1 := IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM GET-01-CPR
                            sub_Get_01_Cpr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-BEFORE-IMAGE
                sub_Get_Before_Image();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero()) && iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                         //Natural: IF CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                             //Natural: ASSIGN #DOD := STTLMNT-2ND-DOD-DTE
                    pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("02");                                                                                                         //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '02'
                    if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.greater(getZero())))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN GT 0
                    {
                        pnd_Work_Rec_Pnd_Ssn1.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn);                                                                                  //Natural: ASSIGN #WORK-REC.#SSN1 := IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM GET-01-CPR
                        sub_Get_01_Cpr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cntrct_Trans_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                   //Natural: IF CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE GT 0
                    {
                        pnd_Work_Rec_Pnd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                             //Natural: ASSIGN #DOD := STTLMNT-DOD-DTE
                        pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("01");                                                                                                     //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '01'
                        pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                           //Natural: ASSIGN #WORK-REC.#SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  11/11
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
                //*  11/11 END
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DISPLAY(2)
        //*    #PIN
        //*    #SSN1
        //*    #DOD
        //*    #CNTRCT-NBR
        //*    #CNTRCT-PAYEE
    }
    //*  11/11
    private void sub_Get_Before_Image() throws Exception                                                                                                                  //Natural: GET-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Bfre_Key_Pnd_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                         //Natural: ASSIGN #PPCN-NBR := TRANS-PPCN-NBR
        pnd_Cntrct_Bfre_Key_Pnd_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                             //Natural: ASSIGN #TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        vw_cntrct_Trans.startDatabaseFind                                                                                                                                 //Natural: FIND CNTRCT-TRANS WITH CNTRCT-BFRE-KEY = #CNTRCT-BFRE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", "=", pnd_Cntrct_Bfre_Key.getBinary(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cntrct_Trans.readNextRow("FIND01")))
        {
            vw_cntrct_Trans.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    //*  11/11
    private void sub_Get_01_Cpr() throws Exception                                                                                                                        //Natural: GET-01-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Key_Pnd_C_Cntrct.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #C-CNTRCT := TRANS-PPCN-NBR
        pnd_Cpr_Key_Pnd_C_Payee.setValue(1);                                                                                                                              //Natural: ASSIGN #C-PAYEE := 01
        vw_cpr2.startDatabaseFind                                                                                                                                         //Natural: FIND ( 1 ) CPR2 WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_cpr2.readNextRow("FIND02", true)))
        {
            vw_cpr2.setIfNotFoundControlFlag(false);
            //*  USE PART SSN
            if (condition(vw_cpr2.getAstCOUNTER().equals(0)))                                                                                                             //Natural: IF NO RECORD FOUND
            {
                pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                                   //Natural: ASSIGN #SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(cpr2_Prtcpnt_Tax_Id_Nbr.notEquals(cpr_Prtcpnt_Tax_Id_Nbr)))                                                                                     //Natural: IF CPR2.PRTCPNT-TAX-ID-NBR NE CPR.PRTCPNT-TAX-ID-NBR
            {
                pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                                   //Natural: ASSIGN #SSN1 := CPR.PRTCPNT-TAX-ID-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Before_Cpr() throws Exception                                                                                                                    //Natural: GET-BEFORE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                              //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
        pnd_Cpr_Bfre_Key_Pnd_Cpr_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                            //Natural: ASSIGN #CPR-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        vw_cpr_Trans.startDatabaseFind                                                                                                                                    //Natural: FIND CPR-TRANS WITH CPR-BFRE-KEY = #CPR-BFRE-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CPR_BFRE_KEY", "=", pnd_Cpr_Bfre_Key.getBinary(), WcType.WITH) }
        );
        FIND03:
        while (condition(vw_cpr_Trans.readNextRow("FIND03")))
        {
            vw_cpr_Trans.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Death_Reversal() throws Exception                                                                                                                    //Natural: DEATH-REVERSAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM GET-BEFORE-IMAGE
        sub_Get_Before_Image();
        if (condition(Global.isEscape())) {return;}
        if (condition(cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero())))                                 //Natural: IF CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE GT 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0
        {
            pnd_Desc_Txt.setValue("DEATH CLAIM REVERSAL    ");                                                                                                            //Natural: ASSIGN #DESC-TXT := 'DEATH CLAIM REVERSAL    '
            pnd_Work_Rec_Pnd_Dod.setValue(0);                                                                                                                             //Natural: ASSIGN #DOD := 0
            pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("02");                                                                                                                 //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '02'
            if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.greater(getZero())))                                                                                            //Natural: IF IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN GT 0
            {
                pnd_Work_Rec_Pnd_Ssn1.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Ssn);                                                                                          //Natural: ASSIGN #WORK-REC.#SSN1 := IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaa_Trans_Rcrd_Trans_Payee_Cde.equals(2)))                                                                                                  //Natural: IF TRANS-PAYEE-CDE = 02
                {
                    pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                               //Natural: ASSIGN #WORK-REC.#SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cpr_Key_Pnd_C_Cntrct.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                     //Natural: ASSIGN #C-CNTRCT := TRANS-PPCN-NBR
                    pnd_Cpr_Key_Pnd_C_Payee.setValue(2);                                                                                                                  //Natural: ASSIGN #C-PAYEE := 02
                    vw_cpr2.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CPR2 WITH CNTRCT-PAYEE-KEY = #CPR-KEY
                    (
                    "FIND04",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) },
                    1
                    );
                    FIND04:
                    while (condition(vw_cpr2.readNextRow("FIND04", true)))
                    {
                        vw_cpr2.setIfNotFoundControlFlag(false);
                        //*  USE PART SSN
                        if (condition(vw_cpr2.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
                        {
                            pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                       //Natural: ASSIGN #SSN1 := CPR.PRTCPNT-TAX-ID-NBR
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-NOREC
                        pnd_Work_Rec_Pnd_Ssn1.setValue(cpr2_Prtcpnt_Tax_Id_Nbr);                                                                                          //Natural: ASSIGN #SSN1 := CPR2.PRTCPNT-TAX-ID-NBR
                    }                                                                                                                                                     //Natural: END-FIND
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("023");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '023'
            pnd_Work_Rec_Pnd_Term_Cde.setValue(" ");                                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-CDE := ' '
            pnd_Work_Rec_Pnd_Term_Dte.reset();                                                                                                                            //Natural: RESET #TERM-DTE #TRMNTN-DTE
            pnd_Work_Rec_Pnd_Trmntn_Dte.reset();
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("010");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '010'
            pnd_Work_Rec_Pnd_Term_Cde.setValue(" ");                                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-CDE := ' '
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
            pnd_Tot_Cor_Dth_Act.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOT-COR-DTH-ACT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cntrct_Trans_Cntrct_First_Annt_Dod_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero())))                               //Natural: IF CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0
        {
            pnd_Work_Rec_Pnd_Dod.setValue(0);                                                                                                                             //Natural: ASSIGN #DOD := 0
            pnd_Work_Rec_Pnd_Cntrct_Payee.setValueEdited(iaa_Trans_Rcrd_Trans_Payee_Cde,new ReportEditMask("99"));                                                        //Natural: MOVE EDITED TRANS-PAYEE-CDE ( EM = 99 ) TO #WORK-REC.#CNTRCT-PAYEE
            if (condition(iaa_Trans_Rcrd_Trans_Payee_Cde.notEquals(1)))                                                                                                   //Natural: IF TRANS-PAYEE-CDE NE 01
            {
                                                                                                                                                                          //Natural: PERFORM GET-01-CPR
                sub_Get_01_Cpr();
                if (condition(Global.isEscape())) {return;}
                if (condition(cpr2_Prtcpnt_Tax_Id_Nbr.greater(getZero())))                                                                                                //Natural: IF CPR2.PRTCPNT-TAX-ID-NBR GT 0
                {
                    pnd_Work_Rec_Pnd_Ssn1.setValue(cpr2_Prtcpnt_Tax_Id_Nbr);                                                                                              //Natural: ASSIGN #WORK-REC.#SSN1 := CPR2.PRTCPNT-TAX-ID-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Rec_Pnd_Ssn1.setValue(cpr_Prtcpnt_Tax_Id_Nbr);                                                                                                   //Natural: ASSIGN #WORK-REC.#SSN1 := CPR.PRTCPNT-TAX-ID-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Desc_Txt.setValue("DEATH CLAIM REVERSAL    ");                                                                                                            //Natural: ASSIGN #DESC-TXT := 'DEATH CLAIM REVERSAL    '
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("023");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '023'
            pnd_Work_Rec_Pnd_Term_Cde.setValue(" ");                                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-CDE := ' '
            pnd_Work_Rec_Pnd_Term_Dte.reset();                                                                                                                            //Natural: RESET #TERM-DTE #TRMNTN-DTE
            pnd_Work_Rec_Pnd_Trmntn_Dte.reset();
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
            pnd_Tot_Cor_Dth_Act.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOT-COR-DTH-ACT
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("010");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '010'
            pnd_Work_Rec_Pnd_Term_Cde.setValue(" ");                                                                                                                      //Natural: ASSIGN #WORK-REC.#TERM-CDE := ' '
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.equals(getZero()) && iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(getZero()) && iaa_Cntrct_Cntrct_Optn_Cde.equals(pnd_Joint.getValue("*"))  //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE = 0 AND IAA-CNTRCT.CNTRCT-OPTN-CDE = #JOINT ( * ) AND #DESC-TXT = 'DEATH CLAIM REVERSAL'
            && pnd_Desc_Txt.equals("DEATH CLAIM REVERSAL")))
        {
            pnd_Work_Rec_Pnd_Cntrct_Payee.setValue("01");                                                                                                                 //Natural: ASSIGN #WORK-REC.#CNTRCT-PAYEE := '01'
                                                                                                                                                                          //Natural: PERFORM GET-01-CPR
            sub_Get_01_Cpr();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Rec_Pnd_Ssn1.setValue(cpr2_Prtcpnt_Tax_Id_Nbr);                                                                                                      //Natural: ASSIGN #WORK-REC.#SSN1 := CPR2.PRTCPNT-TAX-ID-NBR
            pnd_Work_Rec_Pnd_Fnctn_Cde.setValue("012");                                                                                                                   //Natural: ASSIGN #WORK-REC.#FNCTN-CDE := '012'
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Rec.reset();                                                                                                                                             //Natural: RESET #WORK-REC
    }
    private void sub_Get_Override() throws Exception                                                                                                                      //Natural: GET-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ070");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ070'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("IAD");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'IAD'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("OVRRDE");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'OVRRDE'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "RT",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        RT:
        while (condition(vw_naz_Table_Ddm.readNextRow("RT")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID OR NAZ-TBL-RCRD-LVL3-ID NE #NAZ-TABLE-LVL3-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Parms.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                              //Natural: ASSIGN #DATE-PARMS := NAZ-TBL-RCRD-DSCRPTN-TXT
            G2:                                                                                                                                                           //Natural: GET NAZ-TABLE-DDM *ISN ( RT. )
            vw_naz_Table_Ddm.readByID(vw_naz_Table_Ddm.getAstISN("RT"), "G2");
            vw_naz_Table_Ddm.deleteDBRow("G2");                                                                                                                           //Natural: DELETE ( G2. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            if (condition(! (DbsUtil.maskMatches(pnd_Date_Parms_Pnd_Ovrrde_Dte,"YYYYMMDD")) || ! (DbsUtil.maskMatches(pnd_Date_Parms_Pnd_Ov_Chck_Dte,"YYYYMMDD"))))       //Natural: IF #OVRRDE-DTE NE MASK ( YYYYMMDD ) OR #OV-CHCK-DTE NE MASK ( YYYYMMDD )
            {
                getReports().write(0, "ERROR IN OVERRIDE DATE FORMAT:",pnd_Date_Parms_Pnd_Ovrrde_Dte,NEWLINE,"PLEASE RE-ENTER USING YYYYMMDD FORMAT");                    //Natural: WRITE 'ERROR IN OVERRIDE DATE FORMAT:' #OVRRDE-DTE / 'PLEASE RE-ENTER USING YYYYMMDD FORMAT'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "RUNNING WITH OVERRIDE DATE:",pnd_Date_Parms_Pnd_Ovrrde_Dte,pnd_Date_Parms_Pnd_Ov_Chck_Dte);                                            //Natural: WRITE 'RUNNING WITH OVERRIDE DATE:' #OVRRDE-DTE #OV-CHCK-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN COR TRANSACTIONS FOR BUSINESS DATE ",cntrl_Cntrl_Todays_Dte,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN COR TRANSACTIONS FOR BUSINESS DATE ' CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY ) 'ON CHECK DATE: ' #LAST-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZZ,ZZ9 )
                        new ReportEditMask ("MM/DD/YYYY"),"ON CHECK DATE: ",pnd_Date8_Pnd_Last_Updt_Ccyymm, new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), 
                        new ReportEditMask ("Z,ZZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE,NEWLINE);                                                                //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT //
                    getReports().write(1, ReportOption.NOTITLE," CONTRACT PAYEE    TRAN  TERM                              ");                                            //Natural: WRITE ( 1 ) ' CONTRACT PAYEE    TRAN  TERM                              '
                    getReports().write(1, ReportOption.NOTITLE,"  NUMBER   CODE    CODE  CODE  DESCRIPTION                 ");                                            //Natural: WRITE ( 1 ) '  NUMBER   CODE    CODE  CODE  DESCRIPTION                 '
                    getReports().write(1, ReportOption.NOTITLE,"--------- ------   ----  ----- ----------------------------");                                            //Natural: WRITE ( 1 ) '--------- ------   ----  ----- ----------------------------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
        Global.format(2, "LS=132 PS=0");
    }
}
