/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:04 PM
**        * FROM NATURAL PROGRAM : Iaap900t
************************************************************
**        * FILE NAME            : Iaap900t.java
**        * CLASS NAME           : Iaap900t
**        * INSTANCE NAME        : Iaap900t
************************************************************
************************************************************************
* PROGRAM : IAAP900T
* PURPOSE : REPORT OF ALL AUTOMATED POST-SETTLEMENT TRANSFERS ON A
*           MONTHLY BASIS.
* DATE    : 10/11/2017
* AUTHOR  : JUN TINIO
* HISTORY :
* 10/11/17  ORIGINAL CODE
*
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap900t extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_Rqst_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Rate_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_Rqst_Xfr_Type;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Rcrd_Key;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Iaxfr_Audit_De_1;

    private DbsGroup pnd_Iaxfr_Audit_De_1__R_Field_1;
    private DbsField pnd_Iaxfr_Audit_De_1_Pnd_Cycle_Dte;
    private DbsField pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Fr_Cntrct;
    private DbsField pnd_Fr_Payee;
    private DbsField pnd_To_Cntrct;
    private DbsField pnd_To_Payee;
    private DbsField pnd_Eff_Dte;
    private DbsField pnd_Ent_Dte;
    private DbsField pnd_Type;
    private DbsField pnd_Pins;
    private DbsField pnd_Cmpny;
    private DbsField pnd_Iaxfr_From_Typ;
    private DbsField pnd_Iaxfr_To_Typ;
    private DbsField pnd_Iaxfr_Frm_Acct_Cd;
    private DbsField pnd_Iaxfr_Frm_Unit_Typ;
    private DbsField pnd_Iaxfr_From_Qty;
    private DbsField pnd_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField pnd_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField pnd_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField pnd_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField pnd_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField pnd_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField pnd_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField pnd_Iaxfr_To_Acct_Cd;
    private DbsField pnd_Iaxfr_To_Unit_Typ;
    private DbsField pnd_Iaxfr_To_Qty;
    private DbsField pnd_Iaxfr_To_Asset_Amt;
    private DbsField pnd_Iaxfr_To_Xfr_Units;
    private DbsField pnd_Iaxfr_To_Xfr_Guar;
    private DbsField pnd_Iaxfr_To_Xfr_Divid;
    private DbsField pnd_Tiaxfr_From_Typ;
    private DbsField pnd_Tiaxfr_To_Typ;
    private DbsField pnd_Tiaxfr_Frm_Acct_Cd;
    private DbsField pnd_Tiaxfr_Frm_Unit_Typ;
    private DbsField pnd_Tiaxfr_From_Qty;
    private DbsField pnd_Tiaxfr_From_Rqstd_Xfr_Units;
    private DbsField pnd_Tiaxfr_From_Rqstd_Xfr_Guar;
    private DbsField pnd_Tiaxfr_From_Rqstd_Xfr_Divid;
    private DbsField pnd_Tiaxfr_From_Asset_Xfr_Amt;
    private DbsField pnd_Tiaxfr_To_Aftr_Xfr_Units;
    private DbsField pnd_Tiaxfr_To_Aftr_Xfr_Guar;
    private DbsField pnd_Tiaxfr_To_Aftr_Xfr_Divid;
    private DbsField pnd_Tiaxfr_To_Acct_Cd;
    private DbsField pnd_Tiaxfr_To_Unit_Typ;
    private DbsField pnd_Tiaxfr_To_Qty;
    private DbsField pnd_Tiaxfr_To_Asset_Amt;
    private DbsField pnd_Tiaxfr_To_Xfr_Units;
    private DbsField pnd_Tiaxfr_To_Xfr_Guar;
    private DbsField pnd_Tiaxfr_To_Xfr_Divid;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_End_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Begin_Date;

    private DbsGroup pnd_Begin_Date__R_Field_2;
    private DbsField pnd_Begin_Date_Pnd_Beg_Yyyy;
    private DbsField pnd_Begin_Date_Pnd_Beg_Mm;
    private DbsField pnd_Begin_Date_Pnd_Beg_Dd;
    private DbsField pnd_O_Begin_Date;
    private DbsField pnd_O_End_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_Rcrd_Type_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data", 
            "C*IAXFR-CALC-FROM-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", "IAXFR-FROM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", "IAXFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Qty = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_ASSET_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data", 
            "C*IAXFR-CALC-TO-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", "IAXFR-TO-NEW-FUND-REC", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Qty = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", "IAXFR-TO-BFR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", "IAXFR-TO-BFR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", "IAXFR-TO-BFR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", "IAXFR-TO-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", "IAXFR-TO-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Rate_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Rate_Cde", "IAXFR-TO-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_RATE_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", "IAXFR-TO-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", "IAXFR-TO-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", "IAXFR-TO-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", "IAXFR-TO-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_Rqst_Xfr_Type = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Xfr_Audit_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        registerRecord(vw_iaa_Xfr_Audit);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Rcrd_Key = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRL_RCRD_KEY");
        cntrl_Cntrl_Rcrd_Key.setSuperDescriptor(true);
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_cntrl);

        pnd_Iaxfr_Audit_De_1 = localVariables.newFieldInRecord("pnd_Iaxfr_Audit_De_1", "#IAXFR-AUDIT-DE-1", FieldType.BINARY, 8);

        pnd_Iaxfr_Audit_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Iaxfr_Audit_De_1__R_Field_1", "REDEFINE", pnd_Iaxfr_Audit_De_1);
        pnd_Iaxfr_Audit_De_1_Pnd_Cycle_Dte = pnd_Iaxfr_Audit_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Audit_De_1_Pnd_Cycle_Dte", "#CYCLE-DTE", FieldType.DATE);
        pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte = pnd_Iaxfr_Audit_De_1__R_Field_1.newFieldInGroup("pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", 
            FieldType.DATE);
        pnd_Fr_Cntrct = localVariables.newFieldArrayInRecord("pnd_Fr_Cntrct", "#FR-CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Fr_Payee = localVariables.newFieldArrayInRecord("pnd_Fr_Payee", "#FR-PAYEE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_To_Cntrct = localVariables.newFieldArrayInRecord("pnd_To_Cntrct", "#TO-CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_To_Payee = localVariables.newFieldArrayInRecord("pnd_To_Payee", "#TO-PAYEE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Eff_Dte = localVariables.newFieldArrayInRecord("pnd_Eff_Dte", "#EFF-DTE", FieldType.DATE, new DbsArrayController(1, 20));
        pnd_Ent_Dte = localVariables.newFieldArrayInRecord("pnd_Ent_Dte", "#ENT-DTE", FieldType.DATE, new DbsArrayController(1, 20));
        pnd_Type = localVariables.newFieldArrayInRecord("pnd_Type", "#TYPE", FieldType.STRING, 5, new DbsArrayController(1, 20));
        pnd_Pins = localVariables.newFieldArrayInRecord("pnd_Pins", "#PINS", FieldType.NUMERIC, 12, new DbsArrayController(1, 20));
        pnd_Cmpny = localVariables.newFieldArrayInRecord("pnd_Cmpny", "#CMPNY", FieldType.STRING, 4, new DbsArrayController(1, 20));
        pnd_Iaxfr_From_Typ = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Typ", "#IAXFR-FROM-TYP", FieldType.STRING, 5, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_To_Typ = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Typ", "#IAXFR-TO-TYP", FieldType.STRING, 5, new DbsArrayController(1, 20));
        pnd_Iaxfr_Frm_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Frm_Acct_Cd", "#IAXFR-FRM-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_Frm_Unit_Typ = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Frm_Unit_Typ", "#IAXFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_From_Qty = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Qty", "#IAXFR-FROM-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_From_Rqstd_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Rqstd_Xfr_Units", "#IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 20));
        pnd_Iaxfr_From_Rqstd_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Rqstd_Xfr_Guar", "#IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Iaxfr_From_Rqstd_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Rqstd_Xfr_Divid", "#IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Iaxfr_From_Asset_Xfr_Amt = localVariables.newFieldArrayInRecord("pnd_Iaxfr_From_Asset_Xfr_Amt", "#IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Aftr_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Aftr_Xfr_Units", "#IAXFR-TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Aftr_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Aftr_Xfr_Guar", "#IAXFR-TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Aftr_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Aftr_Xfr_Divid", "#IAXFR-TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Acct_Cd", "#IAXFR-TO-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_To_Unit_Typ = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Unit_Typ", "#IAXFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_To_Qty = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Qty", "#IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_To_Asset_Amt = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Asset_Amt", "#IAXFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Xfr_Units", "#IAXFR-TO-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, 
            new DbsArrayController(1, 20));
        pnd_Iaxfr_To_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Xfr_Guar", "#IAXFR-TO-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 20));
        pnd_Iaxfr_To_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Iaxfr_To_Xfr_Divid", "#IAXFR-TO-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 20));
        pnd_Tiaxfr_From_Typ = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Typ", "#TIAXFR-FROM-TYP", FieldType.STRING, 5, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_To_Typ = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Typ", "#TIAXFR-TO-TYP", FieldType.STRING, 5, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_Frm_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_Frm_Acct_Cd", "#TIAXFR-FRM-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_Frm_Unit_Typ = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_Frm_Unit_Typ", "#TIAXFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_From_Qty = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Qty", "#TIAXFR-FROM-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_From_Rqstd_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Rqstd_Xfr_Units", "#TIAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 20));
        pnd_Tiaxfr_From_Rqstd_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Rqstd_Xfr_Guar", "#TIAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_From_Rqstd_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Rqstd_Xfr_Divid", "#TIAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_From_Asset_Xfr_Amt = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_From_Asset_Xfr_Amt", "#TIAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Aftr_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Aftr_Xfr_Units", "#TIAXFR-TO-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Aftr_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Aftr_Xfr_Guar", "#TIAXFR-TO-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Aftr_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Aftr_Xfr_Divid", "#TIAXFR-TO-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Acct_Cd = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Acct_Cd", "#TIAXFR-TO-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_To_Unit_Typ = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Unit_Typ", "#TIAXFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_To_Qty = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Qty", "#TIAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20));
        pnd_Tiaxfr_To_Asset_Amt = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Asset_Amt", "#TIAXFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Xfr_Units = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Xfr_Units", "#TIAXFR-TO-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 
            3, new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Xfr_Guar = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Xfr_Guar", "#TIAXFR-TO-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 20));
        pnd_Tiaxfr_To_Xfr_Divid = localVariables.newFieldArrayInRecord("pnd_Tiaxfr_To_Xfr_Divid", "#TIAXFR-TO-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 20));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.DATE);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Begin_Date = localVariables.newFieldInRecord("pnd_Begin_Date", "#BEGIN-DATE", FieldType.STRING, 8);

        pnd_Begin_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Begin_Date__R_Field_2", "REDEFINE", pnd_Begin_Date);
        pnd_Begin_Date_Pnd_Beg_Yyyy = pnd_Begin_Date__R_Field_2.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Yyyy", "#BEG-YYYY", FieldType.NUMERIC, 4);
        pnd_Begin_Date_Pnd_Beg_Mm = pnd_Begin_Date__R_Field_2.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Mm", "#BEG-MM", FieldType.NUMERIC, 2);
        pnd_Begin_Date_Pnd_Beg_Dd = pnd_Begin_Date__R_Field_2.newFieldInGroup("pnd_Begin_Date_Pnd_Beg_Dd", "#BEG-DD", FieldType.NUMERIC, 2);
        pnd_O_Begin_Date = localVariables.newFieldInRecord("pnd_O_Begin_Date", "#O-BEGIN-DATE", FieldType.STRING, 8);
        pnd_O_End_Date = localVariables.newFieldInRecord("pnd_O_End_Date", "#O-END-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Xfr_Audit.reset();
        vw_cntrl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap900t() throws Exception
    {
        super("Iaap900t");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 250
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        DbsUtil.callnat(Iaan900t.class , getCurrentProcessState(), pnd_O_Begin_Date, pnd_O_End_Date);                                                                     //Natural: CALLNAT 'IAAN900T' #O-BEGIN-DATE #O-END-DATE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_O_Begin_Date.notEquals(" ")))                                                                                                                   //Natural: IF #O-BEGIN-DATE NE ' '
        {
            pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_O_Begin_Date);                                                     //Natural: MOVE EDITED #O-BEGIN-DATE TO #RQST-EFFCTV-DTE ( EM = YYYYMMDD )
            pnd_End_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_O_End_Date);                                                                                    //Natural: MOVE EDITED #O-END-DATE TO #END-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_cntrl.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
            (
            "READ01",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_cntrl.readNextRow("READ01")))
            {
                pnd_End_Dte.setValue(cntrl_Cntrl_Todays_Dte);                                                                                                             //Natural: ASSIGN #END-DTE := CNTRL-TODAYS-DTE
                pnd_Begin_Date.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #BEGIN-DATE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            pnd_Begin_Date_Pnd_Beg_Mm.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #BEG-MM
            if (condition(pnd_Begin_Date_Pnd_Beg_Mm.lessOrEqual(getZero())))                                                                                              //Natural: IF #BEG-MM LE 0
            {
                pnd_Begin_Date_Pnd_Beg_Mm.setValue(12);                                                                                                                   //Natural: ASSIGN #BEG-MM := 12
                pnd_Begin_Date_Pnd_Beg_Yyyy.nsubtract(1);                                                                                                                 //Natural: SUBTRACT 1 FROM #BEG-YYYY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Begin_Date_Pnd_Beg_Dd.setValue(31);                                                                                                                       //Natural: ASSIGN #BEG-DD := 31
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(DbsUtil.maskMatches(pnd_Begin_Date,"YYYYMMDD")))                                                                                            //Natural: IF #BEGIN-DATE = MASK ( YYYYMMDD )
                {
                    pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Begin_Date);                                               //Natural: MOVE EDITED #BEGIN-DATE TO #RQST-EFFCTV-DTE ( EM = YYYYMMDD )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Begin_Date_Pnd_Beg_Dd.nsubtract(1);                                                                                                                   //Natural: SUBTRACT 1 FROM #BEG-DD
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            DbsUtil.callnat(Adsn607.class , getCurrentProcessState(), pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte, pnd_Rc);                                                  //Natural: CALLNAT 'ADSN607' #RQST-EFFCTV-DTE #RC
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Iaxfr_Audit_De_1_Pnd_Cycle_Dte.setValue(pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte);                                                                            //Natural: ASSIGN #CYCLE-DTE := #RQST-EFFCTV-DTE
        getReports().write(0, "=",pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte, new ReportEditMask ("YYYYMMDD"),"=",pnd_End_Dte, new ReportEditMask ("YYYYMMDD"));            //Natural: WRITE '=' #RQST-EFFCTV-DTE ( EM = YYYYMMDD ) '=' #END-DTE ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        vw_iaa_Xfr_Audit.startDatabaseRead                                                                                                                                //Natural: READ IAA-XFR-AUDIT BY IAXFR-AUDIT-DE-1
        (
        "READ02",
        new Oc[] { new Oc("IAXFR_AUDIT_DE_1", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Xfr_Audit.readNextRow("READ02")))
        {
            if (condition(!(iaa_Xfr_Audit_Iaxfr_Effctve_Dte.greaterOrEqual(pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte) && DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde, //Natural: ACCEPT IF IAXFR-EFFCTVE-DTE GE #RQST-EFFCTV-DTE AND IAXFR-CALC-STATUS-CDE = MASK ( 'M' )
                "'M'"))))
            {
                continue;
            }
            if (condition(iaa_Xfr_Audit_Iaxfr_Effctve_Dte.greater(pnd_End_Dte)))                                                                                          //Natural: IF IAXFR-EFFCTVE-DTE GT #END-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"N") || DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"'Z'")))                    //Natural: IF IAXFR-FROM-PPCN-NBR = MASK ( N ) OR = MASK ( 'Z' )
            {
                pnd_Cmpny.getValue("*").setValue("CREF");                                                                                                                 //Natural: ASSIGN #CMPNY ( * ) := 'CREF'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cmpny.getValue("*").setValue("TIAA");                                                                                                                 //Natural: ASSIGN #CMPNY ( * ) := 'TIAA'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"'6L7'") || DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"'6M7'")                //Natural: IF IAXFR-FROM-PPCN-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
                || DbsUtil.maskMatches(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"'6N7'")))
            {
                pnd_Cmpny.getValue("*").setValue("TIAA");                                                                                                                 //Natural: ASSIGN #CMPNY ( * ) := 'TIAA'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pins.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id);                                                                                          //Natural: ASSIGN #PINS ( * ) := IAXFR-CALC-UNIQUE-ID
            pnd_Fr_Cntrct.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #FR-CNTRCT ( * ) := IAXFR-FROM-PPCN-NBR
            pnd_Fr_Payee.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                                      //Natural: ASSIGN #FR-PAYEE ( * ) := IAXFR-FROM-PAYEE-CDE
            if (condition(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr.equals(" ")))                                                                                              //Natural: IF IAXFR-CALC-TO-PPCN-NBR = ' '
            {
                pnd_To_Cntrct.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                                  //Natural: ASSIGN #TO-CNTRCT ( * ) := IAXFR-FROM-PPCN-NBR
                pnd_To_Payee.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                                  //Natural: ASSIGN #TO-PAYEE ( * ) := IAXFR-FROM-PAYEE-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_To_Cntrct.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                                               //Natural: ASSIGN #TO-CNTRCT ( * ) := IAXFR-CALC-TO-PPCN-NBR
                pnd_To_Payee.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde);                                                                               //Natural: ASSIGN #TO-PAYEE ( * ) := IAXFR-CALC-TO-PAYEE-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Eff_Dte.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Effctve_Dte);                                                                                          //Natural: ASSIGN #EFF-DTE ( * ) := IAXFR-EFFCTVE-DTE
            pnd_Ent_Dte.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Entry_Dte);                                                                                            //Natural: ASSIGN #ENT-DTE ( * ) := IAXFR-ENTRY-DTE
            if (condition(iaa_Xfr_Audit_Rcrd_Type_Cde.equals("1")))                                                                                                       //Natural: IF RCRD-TYPE-CDE = '1'
            {
                pnd_Type.getValue("*").setValue("Xfer");                                                                                                                  //Natural: ASSIGN #TYPE ( * ) := 'Xfer'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Type.getValue("*").setValue("Swtch");                                                                                                                 //Natural: ASSIGN #TYPE ( * ) := 'Swtch'
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet229 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE IAXFR-FROM-TYP ( 1 );//Natural: VALUE 'P'
            if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(1).equals("P"))))
            {
                decideConditionsMet229++;
                pnd_Iaxfr_From_Typ.getValue("*").setValue("Pct");                                                                                                         //Natural: ASSIGN #IAXFR-FROM-TYP ( * ) := 'Pct'
            }                                                                                                                                                             //Natural: VALUE 'U'
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(1).equals("U"))))
            {
                decideConditionsMet229++;
                pnd_Iaxfr_From_Typ.getValue("*").setValue("Units");                                                                                                       //Natural: ASSIGN #IAXFR-FROM-TYP ( * ) := 'Units'
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((iaa_Xfr_Audit_Iaxfr_From_Typ.getValue(1).equals("D"))))
            {
                decideConditionsMet229++;
                pnd_Iaxfr_From_Typ.getValue("*").setValue("USD");                                                                                                         //Natural: ASSIGN #IAXFR-FROM-TYP ( * ) := 'USD'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet239 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE IAXFR-TO-TYP ( 1 );//Natural: VALUE 'P'
            if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(1).equals("P"))))
            {
                decideConditionsMet239++;
                pnd_Iaxfr_To_Typ.getValue("*").setValue("Pct");                                                                                                           //Natural: ASSIGN #IAXFR-TO-TYP ( * ) := 'Pct'
            }                                                                                                                                                             //Natural: VALUE 'U'
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(1).equals("U"))))
            {
                decideConditionsMet239++;
                pnd_Iaxfr_To_Typ.getValue("*").setValue("Units");                                                                                                         //Natural: ASSIGN #IAXFR-TO-TYP ( * ) := 'Units'
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((iaa_Xfr_Audit_Iaxfr_To_Typ.getValue(1).equals("D"))))
            {
                decideConditionsMet239++;
                pnd_Iaxfr_To_Typ.getValue("*").setValue("USD");                                                                                                           //Natural: ASSIGN #IAXFR-TO-TYP ( * ) := 'USD'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_I.setValue(iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data);                                                                                            //Natural: ASSIGN #I := C*IAXFR-CALC-FROM-ACCT-DATA
            pnd_J.setValue(iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data);                                                                                              //Natural: ASSIGN #J := C*IAXFR-CALC-TO-ACCT-DATA
            pnd_Iaxfr_Frm_Acct_Cd.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue("*"));                                                                  //Natural: ASSIGN #IAXFR-FRM-ACCT-CD ( * ) := IAXFR-FRM-ACCT-CD ( * )
            pnd_Iaxfr_Frm_Unit_Typ.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue("*"));                                                                //Natural: ASSIGN #IAXFR-FRM-UNIT-TYP ( * ) := IAXFR-FRM-UNIT-TYP ( * )
            pnd_Iaxfr_From_Qty.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Qty.getValue("*"));                                                                        //Natural: ASSIGN #IAXFR-FROM-QTY ( * ) := IAXFR-FROM-QTY ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue("*"));                                                //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-UNITS ( * ) := IAXFR-FROM-RQSTD-XFR-UNITS ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar.getValue("*"));                                                  //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-GUAR ( * ) := IAXFR-FROM-RQSTD-XFR-GUAR ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue("*"));                                                //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-DIVID ( * ) := IAXFR-FROM-RQSTD-XFR-DIVID ( * )
            pnd_Iaxfr_From_Asset_Xfr_Amt.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt.getValue("*"));                                                    //Natural: ASSIGN #IAXFR-FROM-ASSET-XFR-AMT ( * ) := IAXFR-FROM-ASSET-XFR-AMT ( * )
            pnd_Iaxfr_To_Acct_Cd.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue("*"));                                                                    //Natural: ASSIGN #IAXFR-TO-ACCT-CD ( * ) := IAXFR-TO-ACCT-CD ( * )
            pnd_Iaxfr_To_Unit_Typ.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue("*"));                                                                  //Natural: ASSIGN #IAXFR-TO-UNIT-TYP ( * ) := IAXFR-TO-UNIT-TYP ( * )
            pnd_Iaxfr_To_Qty.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue("*"));                                                                            //Natural: ASSIGN #IAXFR-TO-QTY ( * ) := IAXFR-TO-QTY ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Units.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue("*"));                                                      //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-UNITS ( * ) := IAXFR-TO-AFTR-XFR-UNITS ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Guar.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue("*"));                                                        //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-GUAR ( * ) := IAXFR-TO-AFTR-XFR-GUAR ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Divid.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid.getValue("*"));                                                      //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-DIVID ( * ) := IAXFR-TO-AFTR-XFR-DIVID ( * )
            pnd_Iaxfr_To_Asset_Amt.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue("*"));                                                                //Natural: ASSIGN #IAXFR-TO-ASSET-AMT ( * ) := IAXFR-TO-ASSET-AMT ( * )
            pnd_Iaxfr_To_Xfr_Units.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue("*"));                                                                //Natural: ASSIGN #IAXFR-TO-XFR-UNITS ( * ) := IAXFR-TO-XFR-UNITS ( * )
            pnd_Iaxfr_To_Xfr_Guar.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar.getValue("*"));                                                                  //Natural: ASSIGN #IAXFR-TO-XFR-GUAR ( * ) := IAXFR-TO-XFR-GUAR ( * )
            pnd_Iaxfr_To_Xfr_Divid.getValue("*").setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue("*"));                                                                //Natural: ASSIGN #IAXFR-TO-XFR-DIVID ( * ) := IAXFR-TO-XFR-DIVID ( * )
            if (condition(pnd_I.equals(pnd_J)))                                                                                                                           //Natural: IF #I = #J
            {
                pnd_K.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #K := #I
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_I.greater(pnd_J)))                                                                                                                      //Natural: IF #I GT #J
                {
                    pnd_K.setValue(pnd_I);                                                                                                                                //Natural: ASSIGN #K := #I
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_K.setValue(pnd_J);                                                                                                                                //Natural: ASSIGN #K := #J
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM POPULATE-ARRAY
                sub_Populate_Array();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-EMPTY-ROWS
            sub_Check_For_Empty_Rows();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().display(1, ReportOption.NOTITLE,"/PIN",                                                                                                          //Natural: DISPLAY ( 1 ) NOTITLE '/PIN' #PINS ( 1:#K ) '/Cmpny' #CMPNY ( 1:#K ) '/Fr Cntrct' #FR-CNTRCT ( 1:#K ) 'Fr/Py' #FR-PAYEE ( 1:#K ) '/To Cntrct' #TO-CNTRCT ( 1:#K ) 'To/Py' #TO-PAYEE ( 1:#K ) 'Effctve/Dte' #EFF-DTE ( 1:#K ) ( EM = MM/DD/YYYY ) '/Entry Dte' #ENT-DTE ( 1:#K ) ( EM = MM/DD/YYYY ) '/Typ' #TYPE ( 1:#K ) 'Fr/Fnd' #IAXFR-FRM-ACCT-CD ( 1:#K ) 'Fr/Reval' #IAXFR-FRM-UNIT-TYP ( 1:#K ) 'Xfr/Typ' #IAXFR-FROM-TYP ( 1:#K ) '/Qty' #IAXFR-FROM-QTY ( 1:#K ) ( EM = ZZZZ9.99 ZP = OFF ) '/Fr Units' #IAXFR-FROM-RQSTD-XFR-UNITS ( 1:#K ) ( EM = ZZZZZ.999 ZP = OFF ) '/Fr Guar' #IAXFR-FROM-RQSTD-XFR-GUAR ( 1:#K ) ( EM = ZZZZ,ZZ9.99 ZP = OFF ) '/Fr Div' #IAXFR-FROM-RQSTD-XFR-DIVID ( 1:#K ) ( EM = ZZZZ,ZZ9.99 ZP = OFF ) 'Fr Asset/Amount' #IAXFR-FROM-ASSET-XFR-AMT ( 1:#K ) ( EM = ZZZZZ,ZZ9.99 ) 'To/Fnd' #IAXFR-TO-ACCT-CD ( 1:#K ) 'To/Reval' #IAXFR-TO-UNIT-TYP ( 1:#K ) 'Xfr/Typ' #IAXFR-TO-TYP ( 1:#K ) '/Qty' #IAXFR-TO-QTY ( 1:#K ) ( EM = ZZZZ9.99 ZP = OFF ) '/To Units' #IAXFR-TO-XFR-UNITS ( 1:#K ) ( EM = ZZZZZ.999 ZP = OFF ) '/To Guar' #IAXFR-TO-XFR-GUAR ( 1:#K ) ( EM = ZZZZ,ZZ9.99 ZP = OFF ) '/To Div' #IAXFR-TO-XFR-DIVID ( 1:#K ) ( EM = ZZZZ,ZZ9.99 ZP = OFF ) 'To Asset/Amount' #IAXFR-TO-ASSET-AMT ( 1:#K ) ( EM = ZZZZZ,ZZ9.99 )
            		pnd_Pins.getValue(1,":",pnd_K),"/Cmpny",
            		pnd_Cmpny.getValue(1,":",pnd_K),"/Fr Cntrct",
            		pnd_Fr_Cntrct.getValue(1,":",pnd_K),"Fr/Py",
            		pnd_Fr_Payee.getValue(1,":",pnd_K),"/To Cntrct",
            		pnd_To_Cntrct.getValue(1,":",pnd_K),"To/Py",
            		pnd_To_Payee.getValue(1,":",pnd_K),"Effctve/Dte",
            		pnd_Eff_Dte.getValue(1,":",pnd_K), new ReportEditMask ("MM/DD/YYYY"),"/Entry Dte",
            		pnd_Ent_Dte.getValue(1,":",pnd_K), new ReportEditMask ("MM/DD/YYYY"),"/Typ",
            		pnd_Type.getValue(1,":",pnd_K),"Fr/Fnd",
            		pnd_Iaxfr_Frm_Acct_Cd.getValue(1,":",pnd_K),"Fr/Reval",
            		pnd_Iaxfr_Frm_Unit_Typ.getValue(1,":",pnd_K),"Xfr/Typ",
            		pnd_Iaxfr_From_Typ.getValue(1,":",pnd_K),"/Qty",
            		pnd_Iaxfr_From_Qty.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"/Fr Units",
            		pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZZ.999"), new ReportZeroPrint (false),"/Fr Guar",
            		pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/Fr Div",
            		pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"Fr Asset/Amount",
            		pnd_Iaxfr_From_Asset_Xfr_Amt.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZZ,ZZ9.99"),"To/Fnd",
            		pnd_Iaxfr_To_Acct_Cd.getValue(1,":",pnd_K),"To/Reval",
            		pnd_Iaxfr_To_Unit_Typ.getValue(1,":",pnd_K),"Xfr/Typ",
            		pnd_Iaxfr_To_Typ.getValue(1,":",pnd_K),"/Qty",
            		pnd_Iaxfr_To_Qty.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"/To Units",
            		pnd_Iaxfr_To_Xfr_Units.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZZ.999"), new ReportZeroPrint (false),"/To Guar",
            		pnd_Iaxfr_To_Xfr_Guar.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/To Div",
            		pnd_Iaxfr_To_Xfr_Divid.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"To Asset/Amount",
            		pnd_Iaxfr_To_Asset_Amt.getValue(1,":",pnd_K), new ReportEditMask ("ZZZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Iaxfr_Frm_Acct_Cd.getValue("*").reset();                                                                                                                  //Natural: RESET #IAXFR-FRM-ACCT-CD ( * ) #IAXFR-FRM-UNIT-TYP ( * ) #IAXFR-FROM-QTY ( * ) #IAXFR-FROM-RQSTD-XFR-UNITS ( * ) #IAXFR-FROM-RQSTD-XFR-GUAR ( * ) #IAXFR-FROM-RQSTD-XFR-DIVID ( * ) #IAXFR-FROM-ASSET-XFR-AMT ( * ) #IAXFR-TO-ACCT-CD ( * ) #IAXFR-FRM-ACCT-CD ( * ) #IAXFR-TO-UNIT-TYP ( * ) #IAXFR-TO-QTY ( * ) #IAXFR-TO-AFTR-XFR-UNITS ( * ) #IAXFR-TO-AFTR-XFR-GUAR ( * ) #IAXFR-TO-AFTR-XFR-DIVID ( * ) #IAXFR-TO-ASSET-AMT ( * ) #FR-CNTRCT ( * ) #FR-PAYEE ( * ) #TO-CNTRCT ( * ) #TO-PAYEE ( * ) #EFF-DTE ( * ) #ENT-DTE ( * ) #TYPE ( * ) #PINS ( * ) #CMPNY ( * ) #IAXFR-FROM-TYP ( * ) #IAXFR-TO-TYP ( * ) #TIAXFR-FROM-TYP ( * ) #TIAXFR-TO-TYP ( * ) #TIAXFR-FRM-ACCT-CD ( * ) #TIAXFR-FRM-UNIT-TYP ( * ) #TIAXFR-FROM-QTY ( * ) #TIAXFR-FROM-RQSTD-XFR-UNITS ( * ) #TIAXFR-FROM-RQSTD-XFR-GUAR ( * ) #TIAXFR-FROM-RQSTD-XFR-DIVID ( * ) #TIAXFR-FROM-ASSET-XFR-AMT ( * ) #TIAXFR-TO-AFTR-XFR-UNITS ( * ) #TIAXFR-TO-AFTR-XFR-GUAR ( * ) #TIAXFR-TO-AFTR-XFR-DIVID ( * ) #TIAXFR-TO-ACCT-CD ( * ) #TIAXFR-TO-UNIT-TYP ( * ) #TIAXFR-TO-QTY ( * ) #TIAXFR-TO-ASSET-AMT ( * ) #IAXFR-TO-XFR-UNITS ( * ) #IAXFR-TO-XFR-GUAR ( * ) #IAXFR-TO-XFR-DIVID ( * )
            pnd_Iaxfr_Frm_Unit_Typ.getValue("*").reset();
            pnd_Iaxfr_From_Qty.getValue("*").reset();
            pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue("*").reset();
            pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue("*").reset();
            pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue("*").reset();
            pnd_Iaxfr_From_Asset_Xfr_Amt.getValue("*").reset();
            pnd_Iaxfr_To_Acct_Cd.getValue("*").reset();
            pnd_Iaxfr_Frm_Acct_Cd.getValue("*").reset();
            pnd_Iaxfr_To_Unit_Typ.getValue("*").reset();
            pnd_Iaxfr_To_Qty.getValue("*").reset();
            pnd_Iaxfr_To_Aftr_Xfr_Units.getValue("*").reset();
            pnd_Iaxfr_To_Aftr_Xfr_Guar.getValue("*").reset();
            pnd_Iaxfr_To_Aftr_Xfr_Divid.getValue("*").reset();
            pnd_Iaxfr_To_Asset_Amt.getValue("*").reset();
            pnd_Fr_Cntrct.getValue("*").reset();
            pnd_Fr_Payee.getValue("*").reset();
            pnd_To_Cntrct.getValue("*").reset();
            pnd_To_Payee.getValue("*").reset();
            pnd_Eff_Dte.getValue("*").reset();
            pnd_Ent_Dte.getValue("*").reset();
            pnd_Type.getValue("*").reset();
            pnd_Pins.getValue("*").reset();
            pnd_Cmpny.getValue("*").reset();
            pnd_Iaxfr_From_Typ.getValue("*").reset();
            pnd_Iaxfr_To_Typ.getValue("*").reset();
            pnd_Tiaxfr_From_Typ.getValue("*").reset();
            pnd_Tiaxfr_To_Typ.getValue("*").reset();
            pnd_Tiaxfr_Frm_Acct_Cd.getValue("*").reset();
            pnd_Tiaxfr_Frm_Unit_Typ.getValue("*").reset();
            pnd_Tiaxfr_From_Qty.getValue("*").reset();
            pnd_Tiaxfr_From_Rqstd_Xfr_Units.getValue("*").reset();
            pnd_Tiaxfr_From_Rqstd_Xfr_Guar.getValue("*").reset();
            pnd_Tiaxfr_From_Rqstd_Xfr_Divid.getValue("*").reset();
            pnd_Tiaxfr_From_Asset_Xfr_Amt.getValue("*").reset();
            pnd_Tiaxfr_To_Aftr_Xfr_Units.getValue("*").reset();
            pnd_Tiaxfr_To_Aftr_Xfr_Guar.getValue("*").reset();
            pnd_Tiaxfr_To_Aftr_Xfr_Divid.getValue("*").reset();
            pnd_Tiaxfr_To_Acct_Cd.getValue("*").reset();
            pnd_Tiaxfr_To_Unit_Typ.getValue("*").reset();
            pnd_Tiaxfr_To_Qty.getValue("*").reset();
            pnd_Tiaxfr_To_Asset_Amt.getValue("*").reset();
            pnd_Iaxfr_To_Xfr_Units.getValue("*").reset();
            pnd_Iaxfr_To_Xfr_Guar.getValue("*").reset();
            pnd_Iaxfr_To_Xfr_Divid.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-ARRAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-EMPTY-ROWS
    }
    private void sub_Populate_Array() throws Exception                                                                                                                    //Natural: POPULATE-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_J.greater(pnd_I)))                                                                                                                              //Natural: IF #J GT #I
        {
            pnd_L.compute(new ComputeParameters(false, pnd_L), pnd_I.add(1));                                                                                             //Natural: ASSIGN #L := #I + 1
            pnd_Iaxfr_Frm_Acct_Cd.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I));                                                    //Natural: ASSIGN #IAXFR-FRM-ACCT-CD ( #L:#K ) := IAXFR-FRM-ACCT-CD ( #I )
            pnd_Iaxfr_Frm_Unit_Typ.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I));                                                  //Natural: ASSIGN #IAXFR-FRM-UNIT-TYP ( #L:#K ) := IAXFR-FRM-UNIT-TYP ( #I )
            pnd_Iaxfr_From_Qty.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Qty.getValue(pnd_I));                                                          //Natural: ASSIGN #IAXFR-FROM-QTY ( #L:#K ) := IAXFR-FROM-QTY ( #I )
            pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I));                                  //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-UNITS ( #L:#K ) := IAXFR-FROM-RQSTD-XFR-UNITS ( #I )
            pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_I));                                    //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-GUAR ( #L:#K ) := IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
            pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I));                                  //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-DIVID ( #L:#K ) := IAXFR-FROM-RQSTD-XFR-DIVID ( #I )
            pnd_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I));                                      //Natural: ASSIGN #IAXFR-FROM-ASSET-XFR-AMT ( #L:#K ) := IAXFR-FROM-ASSET-XFR-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.greater(pnd_J)))                                                                                                                              //Natural: IF #I GT #J
        {
            pnd_L.compute(new ComputeParameters(false, pnd_L), pnd_J.add(1));                                                                                             //Natural: ASSIGN #L := #J + 1
            pnd_Iaxfr_To_Acct_Cd.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_J));                                                      //Natural: ASSIGN #IAXFR-TO-ACCT-CD ( #L:#K ) := IAXFR-TO-ACCT-CD ( #J )
            pnd_Iaxfr_To_Unit_Typ.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_J));                                                    //Natural: ASSIGN #IAXFR-TO-UNIT-TYP ( #L:#K ) := IAXFR-TO-UNIT-TYP ( #J )
            pnd_Iaxfr_To_Qty.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Qty.getValue(pnd_J));                                                              //Natural: ASSIGN #IAXFR-TO-QTY ( #L:#K ) := IAXFR-TO-QTY ( #J )
            pnd_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_J));                                        //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-UNITS ( #L:#K ) := IAXFR-TO-AFTR-XFR-UNITS ( #J )
            pnd_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_J));                                          //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-GUAR ( #L:#K ) := IAXFR-TO-AFTR-XFR-GUAR ( #J )
            pnd_Iaxfr_To_Aftr_Xfr_Divid.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid.getValue(pnd_J));                                        //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-DIVID ( #L:#K ) := IAXFR-TO-AFTR-XFR-DIVID ( #J )
            pnd_Iaxfr_To_Asset_Amt.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_J));                                                  //Natural: ASSIGN #IAXFR-TO-ASSET-AMT ( #L:#K ) := IAXFR-TO-ASSET-AMT ( #J )
            pnd_Iaxfr_To_Xfr_Units.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue(pnd_J));                                                  //Natural: ASSIGN #IAXFR-TO-XFR-UNITS ( #L:#K ) := IAXFR-TO-XFR-UNITS ( #J )
            pnd_Iaxfr_To_Xfr_Guar.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar.getValue(pnd_J));                                                    //Natural: ASSIGN #IAXFR-TO-XFR-GUAR ( #L:#K ) := IAXFR-TO-XFR-GUAR ( #J )
            pnd_Iaxfr_To_Xfr_Divid.getValue(pnd_L,":",pnd_K).setValue(iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue(pnd_J));                                                  //Natural: ASSIGN #IAXFR-TO-XFR-DIVID ( #L:#K ) := IAXFR-TO-XFR-DIVID ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_For_Empty_Rows() throws Exception                                                                                                              //Natural: CHECK-FOR-EMPTY-ROWS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        if (condition(pnd_Iaxfr_Frm_Acct_Cd.getValue(1,":",pnd_K).equals(" ") && pnd_Iaxfr_To_Acct_Cd.getValue(1,":",pnd_K).equals(" ")))                                 //Natural: IF #IAXFR-FRM-ACCT-CD ( 1:#K ) = ' ' AND #IAXFR-TO-ACCT-CD ( 1:#K ) = ' '
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO #K
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_K)); pnd_I.nadd(1))
            {
                if (condition(pnd_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals(" ")))                                                                                         //Natural: IF #IAXFR-FRM-ACCT-CD ( #I ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_L.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L
                pnd_Tiaxfr_Frm_Unit_Typ.getValue(pnd_L).setValue(pnd_Iaxfr_Frm_Unit_Typ.getValue(pnd_I));                                                                 //Natural: ASSIGN #TIAXFR-FRM-UNIT-TYP ( #L ) := #IAXFR-FRM-UNIT-TYP ( #I )
                pnd_Tiaxfr_From_Qty.getValue(pnd_L).setValue(pnd_Iaxfr_From_Qty.getValue(pnd_I));                                                                         //Natural: ASSIGN #TIAXFR-FROM-QTY ( #L ) := #IAXFR-FROM-QTY ( #I )
                pnd_Tiaxfr_Frm_Acct_Cd.getValue(pnd_L).setValue(pnd_Iaxfr_Frm_Acct_Cd.getValue(pnd_I));                                                                   //Natural: ASSIGN #TIAXFR-FRM-ACCT-CD ( #L ) := #IAXFR-FRM-ACCT-CD ( #I )
                pnd_Tiaxfr_From_Rqstd_Xfr_Units.getValue(pnd_L).setValue(pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I));                                                 //Natural: ASSIGN #TIAXFR-FROM-RQSTD-XFR-UNITS ( #L ) := #IAXFR-FROM-RQSTD-XFR-UNITS ( #I )
                pnd_Tiaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_L).setValue(pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_I));                                                   //Natural: ASSIGN #TIAXFR-FROM-RQSTD-XFR-GUAR ( #L ) := #IAXFR-FROM-RQSTD-XFR-GUAR ( #I )
                pnd_Tiaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_L).setValue(pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I));                                                 //Natural: ASSIGN #TIAXFR-FROM-RQSTD-XFR-DIVID ( #L ) := #IAXFR-FROM-RQSTD-XFR-DIVID ( #I )
                pnd_Tiaxfr_From_Asset_Xfr_Amt.getValue(pnd_L).setValue(pnd_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I));                                                     //Natural: ASSIGN #TIAXFR-FROM-ASSET-XFR-AMT ( #L ) := #IAXFR-FROM-ASSET-XFR-AMT ( #I )
                pnd_Tiaxfr_To_Acct_Cd.getValue(pnd_L).setValue(pnd_Iaxfr_To_Acct_Cd.getValue(pnd_I));                                                                     //Natural: ASSIGN #TIAXFR-TO-ACCT-CD ( #L ) := #IAXFR-TO-ACCT-CD ( #I )
                pnd_Tiaxfr_To_Unit_Typ.getValue(pnd_L).setValue(pnd_Iaxfr_To_Unit_Typ.getValue(pnd_I));                                                                   //Natural: ASSIGN #TIAXFR-TO-UNIT-TYP ( #L ) := #IAXFR-TO-UNIT-TYP ( #I )
                pnd_Tiaxfr_To_Qty.getValue(pnd_L).setValue(pnd_Iaxfr_To_Qty.getValue(pnd_I));                                                                             //Natural: ASSIGN #TIAXFR-TO-QTY ( #L ) := #IAXFR-TO-QTY ( #I )
                pnd_Tiaxfr_To_Aftr_Xfr_Units.getValue(pnd_L).setValue(pnd_Iaxfr_To_Aftr_Xfr_Units.getValue(pnd_I));                                                       //Natural: ASSIGN #TIAXFR-TO-AFTR-XFR-UNITS ( #L ) := #IAXFR-TO-AFTR-XFR-UNITS ( #I )
                pnd_Tiaxfr_To_Aftr_Xfr_Guar.getValue(pnd_L).setValue(pnd_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I));                                                         //Natural: ASSIGN #TIAXFR-TO-AFTR-XFR-GUAR ( #L ) := #IAXFR-TO-AFTR-XFR-GUAR ( #I )
                pnd_Tiaxfr_To_Aftr_Xfr_Divid.getValue(pnd_L).setValue(pnd_Iaxfr_To_Aftr_Xfr_Divid.getValue(pnd_I));                                                       //Natural: ASSIGN #TIAXFR-TO-AFTR-XFR-DIVID ( #L ) := #IAXFR-TO-AFTR-XFR-DIVID ( #I )
                pnd_Tiaxfr_To_Asset_Amt.getValue(pnd_L).setValue(pnd_Iaxfr_To_Asset_Amt.getValue(pnd_I));                                                                 //Natural: ASSIGN #TIAXFR-TO-ASSET-AMT ( #L ) := #IAXFR-TO-ASSET-AMT ( #I )
                pnd_Tiaxfr_To_Xfr_Units.getValue(pnd_L).setValue(pnd_Iaxfr_To_Xfr_Units.getValue(pnd_I));                                                                 //Natural: ASSIGN #TIAXFR-TO-XFR-UNITS ( #L ) := #IAXFR-TO-XFR-UNITS ( #I )
                pnd_Tiaxfr_To_Xfr_Guar.getValue(pnd_L).setValue(pnd_Iaxfr_To_Xfr_Guar.getValue(pnd_I));                                                                   //Natural: ASSIGN #TIAXFR-TO-XFR-GUAR ( #L ) := #IAXFR-TO-XFR-GUAR ( #I )
                pnd_Tiaxfr_To_Xfr_Divid.getValue(pnd_L).setValue(pnd_Iaxfr_To_Xfr_Divid.getValue(pnd_I));                                                                 //Natural: ASSIGN #TIAXFR-TO-XFR-DIVID ( #L ) := #IAXFR-TO-XFR-DIVID ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_K.setValue(pnd_L);                                                                                                                                        //Natural: ASSIGN #K := #L
            pnd_Iaxfr_Frm_Unit_Typ.getValue("*").setValue(pnd_Tiaxfr_Frm_Unit_Typ.getValue("*"));                                                                         //Natural: ASSIGN #IAXFR-FRM-UNIT-TYP ( * ) := #TIAXFR-FRM-UNIT-TYP ( * )
            pnd_Iaxfr_From_Qty.getValue("*").setValue(pnd_Tiaxfr_From_Qty.getValue("*"));                                                                                 //Natural: ASSIGN #IAXFR-FROM-QTY ( * ) := #TIAXFR-FROM-QTY ( * )
            pnd_Iaxfr_Frm_Acct_Cd.getValue("*").setValue(pnd_Tiaxfr_Frm_Acct_Cd.getValue("*"));                                                                           //Natural: ASSIGN #IAXFR-FRM-ACCT-CD ( * ) := #TIAXFR-FRM-ACCT-CD ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Units.getValue("*").setValue(pnd_Tiaxfr_From_Rqstd_Xfr_Units.getValue("*"));                                                         //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-UNITS ( * ) := #TIAXFR-FROM-RQSTD-XFR-UNITS ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Guar.getValue("*").setValue(pnd_Tiaxfr_From_Rqstd_Xfr_Guar.getValue("*"));                                                           //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-GUAR ( * ) := #TIAXFR-FROM-RQSTD-XFR-GUAR ( * )
            pnd_Iaxfr_From_Rqstd_Xfr_Divid.getValue("*").setValue(pnd_Tiaxfr_From_Rqstd_Xfr_Divid.getValue("*"));                                                         //Natural: ASSIGN #IAXFR-FROM-RQSTD-XFR-DIVID ( * ) := #TIAXFR-FROM-RQSTD-XFR-DIVID ( * )
            pnd_Iaxfr_From_Asset_Xfr_Amt.getValue("*").setValue(pnd_Tiaxfr_From_Asset_Xfr_Amt.getValue("*"));                                                             //Natural: ASSIGN #IAXFR-FROM-ASSET-XFR-AMT ( * ) := #TIAXFR-FROM-ASSET-XFR-AMT ( * )
            pnd_Iaxfr_To_Acct_Cd.getValue("*").setValue(pnd_Tiaxfr_To_Acct_Cd.getValue("*"));                                                                             //Natural: ASSIGN #IAXFR-TO-ACCT-CD ( * ) := #TIAXFR-TO-ACCT-CD ( * )
            pnd_Iaxfr_To_Unit_Typ.getValue("*").setValue(pnd_Tiaxfr_To_Unit_Typ.getValue("*"));                                                                           //Natural: ASSIGN #IAXFR-TO-UNIT-TYP ( * ) := #TIAXFR-TO-UNIT-TYP ( * )
            pnd_Iaxfr_To_Qty.getValue("*").setValue(pnd_Tiaxfr_To_Qty.getValue("*"));                                                                                     //Natural: ASSIGN #IAXFR-TO-QTY ( * ) := #TIAXFR-TO-QTY ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Units.getValue("*").setValue(pnd_Tiaxfr_To_Aftr_Xfr_Units.getValue("*"));                                                               //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-UNITS ( * ) := #TIAXFR-TO-AFTR-XFR-UNITS ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Guar.getValue("*").setValue(pnd_Tiaxfr_To_Aftr_Xfr_Guar.getValue("*"));                                                                 //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-GUAR ( * ) := #TIAXFR-TO-AFTR-XFR-GUAR ( * )
            pnd_Iaxfr_To_Aftr_Xfr_Divid.getValue("*").setValue(pnd_Tiaxfr_To_Aftr_Xfr_Divid.getValue("*"));                                                               //Natural: ASSIGN #IAXFR-TO-AFTR-XFR-DIVID ( * ) := #TIAXFR-TO-AFTR-XFR-DIVID ( * )
            pnd_Iaxfr_To_Asset_Amt.getValue("*").setValue(pnd_Tiaxfr_To_Asset_Amt.getValue("*"));                                                                         //Natural: ASSIGN #IAXFR-TO-ASSET-AMT ( * ) := #TIAXFR-TO-ASSET-AMT ( * )
            pnd_Iaxfr_To_Xfr_Units.getValue("*").setValue(pnd_Tiaxfr_To_Xfr_Units.getValue("*"));                                                                         //Natural: ASSIGN #IAXFR-TO-XFR-UNITS ( * ) := #TIAXFR-TO-XFR-UNITS ( * )
            pnd_Iaxfr_To_Xfr_Guar.getValue("*").setValue(pnd_Tiaxfr_To_Xfr_Guar.getValue("*"));                                                                           //Natural: ASSIGN #IAXFR-TO-XFR-GUAR ( * ) := #TIAXFR-TO-XFR-GUAR ( * )
            pnd_Iaxfr_To_Xfr_Divid.getValue("*").setValue(pnd_Tiaxfr_To_Xfr_Divid.getValue("*"));                                                                         //Natural: ASSIGN #IAXFR-TO-XFR-DIVID ( * ) := #TIAXFR-TO-XFR-DIVID ( * )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(75),"IA POST-SETTLEMENT TRANSFERS REPORT FOR:",pnd_Iaxfr_Audit_De_1_Pnd_Rqst_Effctv_Dte, new                  //Natural: WRITE ( 1 ) 75X 'IA POST-SETTLEMENT TRANSFERS REPORT FOR:' #RQST-EFFCTV-DTE ( EM = MM/DD/YYYY ) 'THRU' #END-DTE ( EM = MM/DD/YYYY ) / 90X 'PROCESSING DATE:' *DATX ( EM = MM/DD/YYYY )
                        ReportEditMask ("MM/DD/YYYY"),"THRU",pnd_End_Dte, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new ColumnSpacing(90),"PROCESSING DATE:",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=250");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"/PIN",
        		pnd_Pins,"/Cmpny",
        		pnd_Cmpny,"/Fr Cntrct",
        		pnd_Fr_Cntrct,"Fr/Py",
        		pnd_Fr_Payee,"/To Cntrct",
        		pnd_To_Cntrct,"To/Py",
        		pnd_To_Payee,"Effctve/Dte",
        		pnd_Eff_Dte, new ReportEditMask ("MM/DD/YYYY"),"/Entry Dte",
        		pnd_Ent_Dte, new ReportEditMask ("MM/DD/YYYY"),"/Typ",
        		pnd_Type,"Fr/Fnd",
        		pnd_Iaxfr_Frm_Acct_Cd,"Fr/Reval",
        		pnd_Iaxfr_Frm_Unit_Typ,"Xfr/Typ",
        		pnd_Iaxfr_From_Typ,"/Qty",
        		pnd_Iaxfr_From_Qty, new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"/Fr Units",
        		pnd_Iaxfr_From_Rqstd_Xfr_Units, new ReportEditMask ("ZZZZZ.999"), new ReportZeroPrint (false),"/Fr Guar",
        		pnd_Iaxfr_From_Rqstd_Xfr_Guar, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/Fr Div",
        		pnd_Iaxfr_From_Rqstd_Xfr_Divid, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"Fr Asset/Amount",
        		pnd_Iaxfr_From_Asset_Xfr_Amt, new ReportEditMask ("ZZZZZ,ZZ9.99"),"To/Fnd",
        		pnd_Iaxfr_To_Acct_Cd,"To/Reval",
        		pnd_Iaxfr_To_Unit_Typ,"Xfr/Typ",
        		pnd_Iaxfr_To_Typ,"/Qty",
        		pnd_Iaxfr_To_Qty, new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"/To Units",
        		pnd_Iaxfr_To_Xfr_Units, new ReportEditMask ("ZZZZZ.999"), new ReportZeroPrint (false),"/To Guar",
        		pnd_Iaxfr_To_Xfr_Guar, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"/To Div",
        		pnd_Iaxfr_To_Xfr_Divid, new ReportEditMask ("ZZZZ,ZZ9.99"), new ReportZeroPrint (false),"To Asset/Amount",
        		pnd_Iaxfr_To_Asset_Amt, new ReportEditMask ("ZZZZZ,ZZ9.99"));
    }
}
