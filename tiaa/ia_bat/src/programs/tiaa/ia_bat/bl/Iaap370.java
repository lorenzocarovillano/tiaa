/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:25:59 PM
**        * FROM NATURAL PROGRAM : Iaap370
************************************************************
**        * FILE NAME            : Iaap370.java
**        * CLASS NAME           : Iaap370
**        * INSTANCE NAME        : Iaap370
************************************************************
************************************************************************
*
* PROGRAM  : IAAP370
*
* FUNCTION : GENERATE A REPORT OF ALL ACTIVE TIAA P.A. CONTRACTS
*            (ORIGIN 17 & 18) FROM A WORK FILE USED AS INPUT BY
*            IAAP100. THIS WILL BE RUN MONTHLY.
*
* DATE     : 12/20/96
*
* HISTORY  :
*
* 10/21/14 : JT - EXPANDED TOTALS FIELDS. SCAN ON 10/14
* 05/2017  : OS - STOWED DUE TO IAAL100 PIN EXP. CHANGES.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap370 extends BLNatBase
{
    // Data Areas
    private LdaIaal100 ldaIaal100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Not_17_Or_18;
    private DbsField pnd_Terminated;
    private DbsField pnd_Orgn_Cde;
    private DbsField pnd_S_Cntrct_Issue_Dte;
    private DbsField pnd_S_Cntrct_Mode_Ind;
    private DbsField pnd_Cpr_Chunk;

    private DbsGroup pnd_Cpr_Chunk__R_Field_1;
    private DbsField pnd_Cpr_Chunk__Filler1;
    private DbsField pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_Div_17;
    private DbsField pnd_Div_18;
    private DbsField pnd_Guar_17;
    private DbsField pnd_Guar_18;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal100 = new LdaIaal100();
        registerRecord(ldaIaal100);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Not_17_Or_18 = localVariables.newFieldInRecord("pnd_Not_17_Or_18", "#NOT-17-OR-18", FieldType.BOOLEAN, 1);
        pnd_Terminated = localVariables.newFieldInRecord("pnd_Terminated", "#TERMINATED", FieldType.BOOLEAN, 1);
        pnd_Orgn_Cde = localVariables.newFieldInRecord("pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_S_Cntrct_Issue_Dte = localVariables.newFieldInRecord("pnd_S_Cntrct_Issue_Dte", "#S-CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_S_Cntrct_Mode_Ind = localVariables.newFieldInRecord("pnd_S_Cntrct_Mode_Ind", "#S-CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Cpr_Chunk = localVariables.newFieldInRecord("pnd_Cpr_Chunk", "#CPR-CHUNK", FieldType.STRING, 231);

        pnd_Cpr_Chunk__R_Field_1 = localVariables.newGroupInRecord("pnd_Cpr_Chunk__R_Field_1", "REDEFINE", pnd_Cpr_Chunk);
        pnd_Cpr_Chunk__Filler1 = pnd_Cpr_Chunk__R_Field_1.newFieldInGroup("pnd_Cpr_Chunk__Filler1", "_FILLER1", FieldType.STRING, 230);
        pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde = pnd_Cpr_Chunk__R_Field_1.newFieldInGroup("pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", FieldType.STRING, 
            1);
        pnd_Div_17 = localVariables.newFieldInRecord("pnd_Div_17", "#DIV-17", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Div_18 = localVariables.newFieldInRecord("pnd_Div_18", "#DIV-18", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Guar_17 = localVariables.newFieldInRecord("pnd_Guar_17", "#GUAR-17", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Guar_18 = localVariables.newFieldInRecord("pnd_Guar_18", "#GUAR-18", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal100.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap370() throws Exception
    {
        super("Iaap370");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().definePrinter(2, "PRT1");                                                                                                                            //Natural: DEFINE PRINTER ( PRT1 = 01 ) OUTPUT 'CMPRT01'
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133
        getReports().definePrinter(3, "PRT2");                                                                                                                            //Natural: DEFINE PRINTER ( PRT2 = 02 ) OUTPUT 'CMPRT02'
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        R1:                                                                                                                                                               //Natural: READ WORK 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal100.getPnd_Input_Record())))
        {
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   CHEADER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   FHEADER")  //Natural: REJECT IF #CNTRCT-PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER' OR NOT ( #RECORD-CDE = 10 OR = 20 OR = 30 )
                || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   SHEADER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99CTRAILER") 
                || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99FTRAILER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99STRAILER") 
                || ! (ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(10) || ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(20) || ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(30))))
            {
                continue;
            }
            //*  10/14
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().greater("W9999999")))                                                                      //Natural: IF #CNTRCT-PPCN-NBR GT 'W9999999'
            {
                //*  10/14
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  10/14
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(10)))                                                                                    //Natural: IF #RECORD-CDE = 10
            {
                if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Filler_1().getSubstring(1,2).equals("17") || ldaIaal100.getPnd_Input_Record_Pnd_Filler_1().getSubstring(1, //Natural: IF SUBSTR ( #FILLER-1,1,2 ) = '17' OR = '18'
                    2).equals("18")))
                {
                    pnd_Not_17_Or_18.setValue(false);                                                                                                                     //Natural: ASSIGN #NOT-17-OR-18 := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Not_17_Or_18.setValue(true);                                                                                                                      //Natural: ASSIGN #NOT-17-OR-18 := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Orgn_Cde.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Filler_1().getSubstring(1,2));                                                                   //Natural: ASSIGN #ORGN-CDE := SUBSTR ( #FILLER-1,1,2 )
                pnd_S_Cntrct_Issue_Dte.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Issue_Dte());                                                                   //Natural: ASSIGN #S-CNTRCT-ISSUE-DTE := #CNTRCT-ISSUE-DTE
                pnd_Terminated.reset();                                                                                                                                   //Natural: RESET #TERMINATED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Not_17_Or_18.getBoolean()))                                                                                                                 //Natural: REJECT IF #NOT-17-OR-18
            {
                continue;
            }
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(20)))                                                                                    //Natural: IF #RECORD-CDE = 20
            {
                if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde().equals(9)))                                                                          //Natural: IF #CNTRCT-ACTVTY-CDE = 9
                {
                    pnd_Terminated.setValue(true);                                                                                                                        //Natural: ASSIGN #TERMINATED := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Terminated.setValue(false);                                                                                                                       //Natural: ASSIGN #TERMINATED := FALSE
                    pnd_Cpr_Chunk.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Rest_Of_Record());                                                                          //Natural: ASSIGN #CPR-CHUNK := #REST-OF-RECORD
                    pnd_S_Cntrct_Mode_Ind.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Mode_Ind());                                                                 //Natural: ASSIGN #S-CNTRCT-MODE-IND := #CNTRCT-MODE-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Terminated.getBoolean()))                                                                                                                   //Natural: REJECT IF #TERMINATED
            {
                continue;
            }
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(30)))                                                                                    //Natural: IF #RECORD-CDE = 30
            {
                if (condition(pnd_Orgn_Cde.equals("17")))                                                                                                                 //Natural: IF #ORGN-CDE = '17'
                {
                    pnd_Div_17.nadd(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt());                                                                               //Natural: ASSIGN #DIV-17 := #DIV-17 + #TIAA-PER-DIV-AMT
                    pnd_Guar_17.nadd(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt());                                                                              //Natural: ASSIGN #GUAR-17 := #GUAR-17 + #TIAA-PER-PAY-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Div_18.nadd(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt());                                                                               //Natural: ASSIGN #DIV-18 := #DIV-18 + #TIAA-PER-DIV-AMT
                    pnd_Guar_18.nadd(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt());                                                                              //Natural: ASSIGN #GUAR-18 := #GUAR-18 + #TIAA-PER-PAY-AMT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  10/14
            //*  10/14
            //*  10/14
            //*  10/14
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(37),"-------------- ------------",NEWLINE,"TOTAL:",new TabSetting(37),pnd_Guar_17,              //Natural: WRITE ( 1 ) / 37T '-------------- ------------' / 'TOTAL:' 37T #GUAR-17 ( EM = ZZZ,ZZZ,ZZ9.99 ) #DIV-17 ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Div_17, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(37),"-------------- ------------",NEWLINE,"TOTAL:",new TabSetting(37),pnd_Guar_18,              //Natural: WRITE ( 2 ) / 37T '-------------- ------------' / 'TOTAL:' 37T #GUAR-18 ( EM = ZZZ,ZZZ,ZZ9.99 ) #DIV-18 ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Div_18, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Orgn_Cde.equals("17")))                                                                                                                         //Natural: IF #ORGN-CDE = '17'
        {
            getReports().display(1, "CONTRCT",                                                                                                                            //Natural: DISPLAY ( 1 ) 'CONTRCT' #CNTRCT-PPCN-NBR ( AL = 8 ) 'PYEE' #CNTRCT-PAYEE-CDE ( EM = 99 ) 'ORGN' #ORGN-CDE 'ISSUE DT' #S-CNTRCT-ISSUE-DTE 'MODE' #S-CNTRCT-MODE-IND ( EM = 999 ) 'PEND/CODE' #CNTRCT-PEND-CDE 'GUAR PMT' #TIAA-PER-PAY-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'DIV PMT' #TIAA-PER-DIV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"PYEE",
            		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"ORGN",
            		pnd_Orgn_Cde,"ISSUE DT",
            		pnd_S_Cntrct_Issue_Dte,"MODE",
            		pnd_S_Cntrct_Mode_Ind, new ReportEditMask ("999"),"PEND/CODE",
            		pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde,"GUAR PMT",
            		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"DIV PMT",
            		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(2, "CONTRCT",                                                                                                                            //Natural: DISPLAY ( 2 ) 'CONTRCT' #CNTRCT-PPCN-NBR ( AL = 8 ) 'PYEE' #CNTRCT-PAYEE-CDE ( EM = 99 ) 'ORGN' #ORGN-CDE 'ISSUE DT' #S-CNTRCT-ISSUE-DTE 'MODE' #S-CNTRCT-MODE-IND ( EM = 999 ) 'PEND/CODE' #CNTRCT-PEND-CDE 'GUAR PMT' #TIAA-PER-PAY-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'DIV PMT' #TIAA-PER-DIV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"PYEE",
            		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"ORGN",
            		pnd_Orgn_Cde,"ISSUE DT",
            		pnd_S_Cntrct_Issue_Dte,"MODE",
            		pnd_S_Cntrct_Mode_Ind, new ReportEditMask ("999"),"PEND/CODE",
            		pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde,"GUAR PMT",
            		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"DIV PMT",
            		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(40),"CONTRACTS WITH ORIGIN CODE OF 17 REPORT",new    //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 40T 'CONTRACTS WITH ORIGIN CODE OF 17 REPORT' 90T *DATX / 'REPORT1' 90T 'PAGE:' *PAGE-NUMBER ( 1 ) ( NL = 3 ) /
                        TabSetting(90),Global.getDATX(),NEWLINE,"REPORT1",new TabSetting(90),"PAGE:",getReports().getPageNumberDbs(1), new NumericLength 
                        (3),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(40),"CONTRACTS WITH ORIGIN CODE OF 18 REPORT",new    //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM 40T 'CONTRACTS WITH ORIGIN CODE OF 18 REPORT' 90T *DATX / 'REPORT2' 90T 'PAGE:' *PAGE-NUMBER ( 2 ) ( NL = 3 ) /
                        TabSetting(90),Global.getDATX(),NEWLINE,"REPORT2",new TabSetting(90),"PAGE:",getReports().getPageNumberDbs(2), new NumericLength 
                        (3),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "CONTRCT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"PYEE",
        		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"ORGN",
        		pnd_Orgn_Cde,"ISSUE DT",
        		pnd_S_Cntrct_Issue_Dte,"MODE",
        		pnd_S_Cntrct_Mode_Ind, new ReportEditMask ("999"),"PEND/CODE",
        		pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde,"GUAR PMT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"DIV PMT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(2, "CONTRCT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"PYEE",
        		ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"ORGN",
        		pnd_Orgn_Cde,"ISSUE DT",
        		pnd_S_Cntrct_Issue_Dte,"MODE",
        		pnd_S_Cntrct_Mode_Ind, new ReportEditMask ("999"),"PEND/CODE",
        		pnd_Cpr_Chunk_Pnd_Cntrct_Pend_Cde,"GUAR PMT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"DIV PMT",
        		ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
    }
}
