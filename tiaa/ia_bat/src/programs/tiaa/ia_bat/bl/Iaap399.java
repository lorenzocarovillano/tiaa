/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:27:06 PM
**        * FROM NATURAL PROGRAM : Iaap399
************************************************************
**        * FILE NAME            : Iaap399.java
**        * CLASS NAME           : Iaap399
**        * INSTANCE NAME        : Iaap399
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM   -  IAAP399                                               *
*      DATE   -  09/05                                                 *
*    AUTHOR   -  STRAL                                                 *
*                                                                      *
* HISTORY                                                              *
* 06/27/2012 - REWRITTEN FOR SIEBEL UPGRADE
* JUN TINIO
* 09/16/2016 - RECATALOGED FOR PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap399 extends BLNatBase
{
    // Data Areas
    private LdaIaal399 ldaIaal399;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Record_Cnt_10;
    private DbsField pnd_Record_Cnt_20;
    private DbsField pnd_Record_Cnt_30;
    private DbsField pnd_Record_Cnt_40;
    private DbsField pnd_Record_Cnt_All;
    private DbsField pnd_Record_Cnt_Rest;
    private DbsField pnd_W_Dte;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal399 = new LdaIaal399();
        registerRecord(ldaIaal399);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Record_Cnt_10 = localVariables.newFieldInRecord("pnd_Record_Cnt_10", "#RECORD-CNT-10", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_20 = localVariables.newFieldInRecord("pnd_Record_Cnt_20", "#RECORD-CNT-20", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_30 = localVariables.newFieldInRecord("pnd_Record_Cnt_30", "#RECORD-CNT-30", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_40 = localVariables.newFieldInRecord("pnd_Record_Cnt_40", "#RECORD-CNT-40", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_All = localVariables.newFieldInRecord("pnd_Record_Cnt_All", "#RECORD-CNT-ALL", FieldType.NUMERIC, 10);
        pnd_Record_Cnt_Rest = localVariables.newFieldInRecord("pnd_Record_Cnt_Rest", "#RECORD-CNT-REST", FieldType.NUMERIC, 10);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal399.initializeValues();

        localVariables.reset();
        pnd_Record_Cnt_10.setInitialValue(0);
        pnd_Record_Cnt_20.setInitialValue(0);
        pnd_Record_Cnt_30.setInitialValue(0);
        pnd_Record_Cnt_40.setInitialValue(0);
        pnd_Record_Cnt_All.setInitialValue(0);
        pnd_Record_Cnt_Rest.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap399() throws Exception
    {
        super("Iaap399");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP399", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD-IN
        while (condition(getWorkFiles().read(1, ldaIaal399.getPnd_Work_Record_In())))
        {
            ldaIaal399.getPnd_Work_Record_Out().reset();                                                                                                                  //Natural: RESET #WORK-RECORD-OUT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Contract().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Contract());                                 //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-CONTRACT := #WORK-RECORD-IN.#W-RECORD-CONTRACT
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Payee().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Payee());                                       //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-PAYEE := #WORK-RECORD-IN.#W-RECORD-PAYEE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_W_Record_Code().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code());                                         //Natural: ASSIGN #WORK-RECORD-OUT.#W-RECORD-CODE := #WORK-RECORD-IN.#W-RECORD-CODE
            ldaIaal399.getPnd_Work_Record_Out_Pnd_Rest_Of_Record_Out().setValue(ldaIaal399.getPnd_Work_Record_In_Pnd_Rest_Of_Record());                                   //Natural: ASSIGN #REST-OF-RECORD-OUT := #REST-OF-RECORD
            short decideConditionsMet339 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WORK-RECORD-IN.#W-RECORD-CODE;//Natural: VALUE 10
            if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(10))))
            {
                decideConditionsMet339++;
                pnd_Record_Cnt_10.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-10
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_10_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_10());                        //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #RECORD-TYPE-10-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W1-LST-TRANS-DTE-N #W1-CNTRCT-ANNTY-STRT-DTE-N #W1-CNTRCT-FNL-PRM-DTE-N ( * )
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N().reset();
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue("*").reset();
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W1-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W1-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W1-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte().greater(getZero())))                                                        //Natural: IF #W1-CNTRCT-ANNTY-STRT-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED #W1-CNTRCT-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Annty_Strt_Dte_N()),  //Natural: ASSIGN #W1-CNTRCT-ANNTY-STRT-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 4
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte().getValue(pnd_I).greater(getZero())))                                       //Natural: IF #W1-CNTRCT-FNL-PRM-DTE ( #I ) GT 0
                    {
                        pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W1_Cntrct_Fnl_Prm_Dte().getValue(pnd_I),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED #W1-CNTRCT-FNL-PRM-DTE ( #I ) ( EM = YYYYMMDD ) TO #W-DTE
                        ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue(pnd_I).compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Fnl_Prm_Dte_N().getValue(pnd_I)),  //Natural: ASSIGN #W1-CNTRCT-FNL-PRM-DTE-N ( #I ) := VAL ( #W-DTE )
                            pnd_W_Dte.val());
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(20))))
            {
                decideConditionsMet339++;
                pnd_Record_Cnt_20.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-20
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_20_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_20());                        //Natural: MOVE BY NAME #RECORD-TYPE-20 TO #RECORD-TYPE-20-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W2-LST-TRANS-DTE-N #W2-CPR-XFR-ISS-DTE-N
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N().reset();
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W2-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W2-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W2-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte().greater(getZero())))                                                              //Natural: IF #W2-CPR-XFR-ISS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W2_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED #W2-CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W2_Cpr_Xfr_Iss_Dte_N()),  //Natural: ASSIGN #W2-CPR-XFR-ISS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(30))))
            {
                decideConditionsMet339++;
                pnd_Record_Cnt_30.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-30
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_30_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_30());                        //Natural: MOVE BY NAME #RECORD-TYPE-30 TO #RECORD-TYPE-30-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W3-LST-TRANS-DTE-N
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W3-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W3_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W3-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W3_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W3-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((ldaIaal399.getPnd_Work_Record_In_Pnd_W_Record_Code().equals(40))))
            {
                decideConditionsMet339++;
                pnd_Record_Cnt_40.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORD-CNT-40
                ldaIaal399.getPnd_Work_Record_Out_Pnd_Record_Type_40_Out().setValuesByName(ldaIaal399.getPnd_Work_Record_In_Pnd_Record_Type_40());                        //Natural: MOVE BY NAME #RECORD-TYPE-40 TO #RECORD-TYPE-40-OUT
                ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N().reset();                                                                                       //Natural: RESET #W4-LST-TRANS-DTE-N
                if (condition(ldaIaal399.getPnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte().greater(getZero())))                                                                //Natural: IF #W4-LST-TRANS-DTE GT 0
                {
                    pnd_W_Dte.setValueEdited(ldaIaal399.getPnd_Work_Record_In_Pnd_W4_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED #W4-LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W-DTE
                    ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N().compute(new ComputeParameters(false, ldaIaal399.getPnd_Work_Record_Out_Pnd_W4_Lst_Trans_Dte_N()),  //Natural: ASSIGN #W4-LST-TRANS-DTE-N := VAL ( #W-DTE )
                        pnd_W_Dte.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Record_Cnt_Rest.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORD-CNT-REST
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(2, false, ldaIaal399.getPnd_Work_Record_Out());                                                                                          //Natural: WRITE WORK FILE 2 #WORK-RECORD-OUT
            pnd_Record_Cnt_All.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #RECORD-CNT-ALL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* **
        getReports().write(0, "Total rcds read          :  ",pnd_Record_Cnt_All);                                                                                         //Natural: WRITE 'Total rcds read          :  ' #RECORD-CNT-ALL
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 10 processed  :  ",pnd_Record_Cnt_10);                                                                                          //Natural: WRITE 'Total type 10 processed  :  ' #RECORD-CNT-10
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 20 processed  :  ",pnd_Record_Cnt_20);                                                                                          //Natural: WRITE 'Total type 20 processed  :  ' #RECORD-CNT-20
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 30 processed  :  ",pnd_Record_Cnt_30);                                                                                          //Natural: WRITE 'Total type 30 processed  :  ' #RECORD-CNT-30
        if (Global.isEscape()) return;
        getReports().write(0, "Total type 40 processed  :  ",pnd_Record_Cnt_40);                                                                                          //Natural: WRITE 'Total type 40 processed  :  ' #RECORD-CNT-40
        if (Global.isEscape()) return;
        getReports().write(0, "Total other processed    :  ",pnd_Record_Cnt_Rest);                                                                                        //Natural: WRITE 'Total other processed    :  ' #RECORD-CNT-REST
        if (Global.isEscape()) return;
        //* **
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* **
        getReports().write(0, "ERROR                   ",Global.getERROR());                                                                                              //Natural: WRITE 'ERROR                   ' *ERROR
        getReports().write(0, "PROGRAM                 ",Global.getPROGRAM());                                                                                            //Natural: WRITE 'PROGRAM                 ' *PROGRAM
        getReports().write(0, "ERROR LINE              ",Global.getERROR_LINE());                                                                                         //Natural: WRITE 'ERROR LINE              ' *ERROR-LINE
        getReports().write(0, "INPUT RECORD NUMBER     ");                                                                                                                //Natural: WRITE 'INPUT RECORD NUMBER     '
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133");
    }
}
