/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:21 PM
**        * FROM NATURAL PROGRAM : Iaap607t
************************************************************
**        * FILE NAME            : Iaap607t.java
**        * CLASS NAME           : Iaap607t
**        * INSTANCE NAME        : Iaap607t
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP607T   THIS PROGRAM WILL WRITE OUT THE REPORT   *
*      DATE   -  03/13      FILE AND ETL FILE FOR TC LIFE PROSPECTUS *
*    AUTHOR   -  JUN T.                                              *
*
* HISTORY -
*
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap607t extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Hold_Table;
    private DbsField pnd_Hold_Table_Pnd_Hold_Table_Rec;
    private DbsField pnd_Hold_Table_Pnd_Hold_Skip_Ind;
    private DbsField pnd_Contract_Record_In;

    private DbsGroup pnd_Contract_Record_In__R_Field_1;
    private DbsField pnd_Contract_Record_In_Pnd_Event_Ind_In;
    private DbsField pnd_Contract_Record_In_Pnd_Plan_Num_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ssn_In;
    private DbsField pnd_Contract_Record_In_Pnd_Fund_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Eff_Date_In;
    private DbsField pnd_Contract_Record_In_Pnd_Inverse_Date_In;
    private DbsField pnd_Contract_Record_In_Pnd_Contract_In;
    private DbsField pnd_Contract_Record_In_Pnd_Payee_Cde_In;
    private DbsField pnd_Contract_Record_In_Pnd_Cusip_In;
    private DbsField pnd_Contract_Record_In_Pnd_Plan_Type_In;
    private DbsField pnd_Contract_Record_In_Pnd_Source_App_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trans_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Act_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trns_Act_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_Pend_Code_In;
    private DbsField pnd_Contract_Record_In_Pnd_First_Ann_Dod_In;
    private DbsField pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In;
    private DbsField pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In;

    private DbsGroup pnd_Contract_Record_Rpt;
    private DbsField pnd_Contract_Record_Rpt_Ia_Event_Ind;
    private DbsField pnd_Contract_Record_Rpt_Ia_Ssn;

    private DbsGroup pnd_Contract_Record_Rpt__R_Field_2;
    private DbsField pnd_Contract_Record_Rpt_Ia_Ssn_Alpha;
    private DbsField pnd_Contract_Record_Rpt_Ia_Fund_Id;
    private DbsField pnd_Contract_Record_Rpt_Ia_Eff_Date;
    private DbsField pnd_Contract_Record_Rpt_Ia_Contract;
    private DbsField pnd_Contract_Record_Rpt_Ia_Payee_Cde;
    private DbsField pnd_Contract_Record_Rpt_Ia_Cusip;
    private DbsField pnd_Contract_Record_Rpt_Ia_Actvty_Cd;
    private DbsField pnd_Contract_Record_Rpt_Ia_Trns_Actvty_Cd;
    private DbsField pnd_Contract_Record_Rpt_Ia_Orig_Code;
    private DbsField pnd_Contract_Record_Rpt_Ia_Opt_Code;
    private DbsField pnd_Contract_Record_Rpt_Ia_Settlement_Id;
    private DbsField pnd_Contract_Record_Rpt_Ia_Tran_Cd;
    private DbsField pnd_Contract_Record_Rpt_Ia_Sub_Tran_Cd;
    private DbsField pnd_Contract_Record_Rpt_Ia_Pend_Cd;
    private DbsField pnd_Contract_Record_Rpt_Ia_Fst_Annt_Dod;
    private DbsField pnd_Contract_Record_Rpt_Ia_2nd_Annt_Dod;
    private DbsField pnd_Contract_Record_Rpt_Ia_Offset_Ind;
    private DbsField pnd_Hold_Record_Out;

    private DbsGroup pnd_Hold_Record_Out__R_Field_3;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Plan_Num_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Eff_Date_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Inverse_Date_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Contract_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Cusip_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Plan_Type_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Source_App_Id_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Ia_Orig_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Ia_Opt_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Ia_Settlement_Id_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Act_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Trns_Act_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Pend_Code_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_First_Ann_Dod_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Second_Ann_Dod_Out;
    private DbsField pnd_Hold_Record_Out_Pnd_Hold_Trans_Sub_Code_Out;
    private DbsField pnd_S_Hold_Record_Out;

    private DbsGroup pnd_S_Hold_Record_Out__R_Field_4;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Event_Ind_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Num_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Ssn_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Fund_Id_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Eff_Date_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Inverse_Date_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Contract_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Payee_Cde_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Cusip_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Type_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Source_App_Id_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Orig_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Opt_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Settlement_Id_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Act_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Trns_Act_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Pend_Code_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_First_Ann_Dod_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Second_Ann_Dod_Out;
    private DbsField pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Sub_Code_Out;

    private DbsGroup pnd_Daily_Record_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Srce_Id;
    private DbsField pnd_Daily_Record_Out_Pnd_Event_Ind_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Ownr_Type;
    private DbsField pnd_Daily_Record_Out_Pnd_Ssn_Out;

    private DbsGroup pnd_Daily_Record_Out__R_Field_5;
    private DbsField pnd_Daily_Record_Out_Pnd_Ssn_Out_Alpha;
    private DbsField pnd_Daily_Record_Out_Pnd_Client_Id;
    private DbsField pnd_Daily_Record_Out_Pnd_Pin;
    private DbsField pnd_Daily_Record_Out_Pnd_Eff_Date_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Proc_Date_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Contract_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Prod_Code;
    private DbsField pnd_Daily_Record_Out_Pnd_Fund_Id_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Payee_Cde_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Ia_Opt_Code_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Ia_Settlement_Id_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Cusip_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Prod_Cusip;
    private DbsField pnd_Daily_Record_Out_Pnd_Plan_Type_Out;
    private DbsField pnd_Daily_Record_Out_Pnd_Cmpny_Nme;
    private DbsField pnd_Daily_Record_Out_Pnd_Addr1;
    private DbsField pnd_Daily_Record_Out_Pnd_Addr2;
    private DbsField pnd_Daily_Record_Out_Pnd_Addr3;
    private DbsField pnd_Daily_Record_Out_Pnd_Addr4;
    private DbsField pnd_Daily_Record_Out_Pnd_Addr5;
    private DbsField pnd_Daily_Record_Out_Pnd_City;
    private DbsField pnd_Daily_Record_Out_Pnd_State;
    private DbsField pnd_Daily_Record_Out_Pnd_Country;
    private DbsField pnd_Daily_Record_Out_Pnd_Zip;
    private DbsField pnd_Daily_Record_Out_Pnd_Edelivery;
    private DbsField pnd_Daily_Record_Out_Pnd_Email;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Work_Before_Key;

    private DbsGroup pnd_Work_Before_Key__R_Field_6;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef;
    private DbsField pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef;
    private DbsField pnd_Work_After_Key;

    private DbsGroup pnd_Work_After_Key__R_Field_7;
    private DbsField pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft;
    private DbsField pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft;

    private DbsGroup hold_Fields;
    private DbsField hold_Fields_Pnd_Hold_Contract;
    private DbsField hold_Fields_Pnd_Hold_Payee_Cde;
    private DbsField hold_Fields_Pnd_Hold_Fund;
    private DbsField hold_Fields_Pnd_Hold_Event_Ind;
    private DbsField pnd_Offset_Found;
    private DbsField pnd_Dup_Found;
    private DbsField pnd_Notfnd;
    private DbsField pnd_After_Found;
    private DbsField pnd_Before_Found;
    private DbsField pnd_Dont_Write;
    private DbsField pnd_First_Done;
    private DbsField pnd_First_Time;
    private DbsField pnd_Hold_Before_Pend_Code;
    private DbsField pnd_Hold_After_Pend_Code;
    private DbsField pnd_Offset_Count;
    private DbsField pnd_S;
    private DbsField pnd_H;
    private DbsField pnd_H_Tot;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Start;
    private DbsField pnd_End;
    private DbsField pnd_P_Ind;
    private DbsField pnd_P_Date;
    private DbsField pnd_Temp_Table_Rec;

    private DbsGroup pnd_Temp_Table_Rec__R_Field_8;
    private DbsField pnd_Temp_Table_Rec_Pnd_Hold_Table_Ind;
    private DbsField pnd_Temp_Table_Rec__Filler1;
    private DbsField pnd_Temp_Table_Rec_Pnd_Hold_Table_Fund;
    private DbsField pnd_Temp_Table_Rec__Filler2;
    private DbsField pnd_Temp_Table_Rec_Pnd_Temp_Table_Trans;
    private DbsField pnd_Temp_Record_Out;

    private DbsGroup pnd_Temp_Record_Out__R_Field_9;
    private DbsField pnd_Temp_Record_Out_Pnd_Temp_Record_Ind;
    private DbsField pnd_Temp_Record_Out__Filler3;
    private DbsField pnd_Temp_Record_Out_Pnd_Temp_Record_Fund;
    private DbsField pnd_Temp_Record_Out__Filler4;
    private DbsField pnd_Temp_Record_Out_Pnd_Temp_Contract;
    private DbsField pnd_Temp_Record_Out_Pnd_Temp_Payee;
    private DbsField pnd_Temp_Record_Out__Filler5;
    private DbsField pnd_Temp_Record_Out_Pnd_Temp_Record_Trans;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Hold_Table = localVariables.newGroupArrayInRecord("pnd_Hold_Table", "#HOLD-TABLE", new DbsArrayController(1, 1000));
        pnd_Hold_Table_Pnd_Hold_Table_Rec = pnd_Hold_Table.newFieldInGroup("pnd_Hold_Table_Pnd_Hold_Table_Rec", "#HOLD-TABLE-REC", FieldType.STRING, 90);
        pnd_Hold_Table_Pnd_Hold_Skip_Ind = pnd_Hold_Table.newFieldInGroup("pnd_Hold_Table_Pnd_Hold_Skip_Ind", "#HOLD-SKIP-IND", FieldType.STRING, 1);
        pnd_Contract_Record_In = localVariables.newFieldInRecord("pnd_Contract_Record_In", "#CONTRACT-RECORD-IN", FieldType.STRING, 90);

        pnd_Contract_Record_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Contract_Record_In__R_Field_1", "REDEFINE", pnd_Contract_Record_In);
        pnd_Contract_Record_In_Pnd_Event_Ind_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Event_Ind_In", "#EVENT-IND-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Plan_Num_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Plan_Num_In", "#PLAN-NUM-IN", 
            FieldType.STRING, 6);
        pnd_Contract_Record_In_Pnd_Ssn_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ssn_In", "#SSN-IN", FieldType.NUMERIC, 
            9);
        pnd_Contract_Record_In_Pnd_Fund_Id_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Fund_Id_In", "#FUND-ID-IN", 
            FieldType.STRING, 2);
        pnd_Contract_Record_In_Pnd_Eff_Date_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Eff_Date_In", "#EFF-DATE-IN", 
            FieldType.NUMERIC, 8);
        pnd_Contract_Record_In_Pnd_Inverse_Date_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Inverse_Date_In", "#INVERSE-DATE-IN", 
            FieldType.NUMERIC, 12);
        pnd_Contract_Record_In_Pnd_Contract_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Contract_In", "#CONTRACT-IN", 
            FieldType.STRING, 10);
        pnd_Contract_Record_In_Pnd_Payee_Cde_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Payee_Cde_In", "#PAYEE-CDE-IN", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_In_Pnd_Cusip_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Cusip_In", "#CUSIP-IN", FieldType.STRING, 
            9);
        pnd_Contract_Record_In_Pnd_Plan_Type_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Plan_Type_In", "#PLAN-TYPE-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Source_App_Id_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Source_App_Id_In", 
            "#SOURCE-APP-ID-IN", FieldType.STRING, 4);
        pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Orig_Code_In", "#IA-ORIG-CODE-IN", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Opt_Code_In", "#IA-OPT-CODE-IN", 
            FieldType.NUMERIC, 2);
        pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Ia_Settlement_Id_In", 
            "#IA-SETTLEMENT-ID-IN", FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Trans_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trans_Code_In", "#TRANS-CODE-IN", 
            FieldType.NUMERIC, 3);
        pnd_Contract_Record_In_Pnd_Act_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Act_Code_In", "#ACT-CODE-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Trns_Act_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trns_Act_Code_In", 
            "#TRNS-ACT-CODE-IN", FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_Pend_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Pend_Code_In", "#PEND-CODE-IN", 
            FieldType.STRING, 1);
        pnd_Contract_Record_In_Pnd_First_Ann_Dod_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_First_Ann_Dod_In", 
            "#FIRST-ANN-DOD-IN", FieldType.NUMERIC, 6);
        pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Second_Ann_Dod_In", 
            "#SECOND-ANN-DOD-IN", FieldType.NUMERIC, 6);
        pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In = pnd_Contract_Record_In__R_Field_1.newFieldInGroup("pnd_Contract_Record_In_Pnd_Trans_Sub_Code_In", 
            "#TRANS-SUB-CODE-IN", FieldType.STRING, 3);

        pnd_Contract_Record_Rpt = localVariables.newGroupInRecord("pnd_Contract_Record_Rpt", "#CONTRACT-RECORD-RPT");
        pnd_Contract_Record_Rpt_Ia_Event_Ind = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Event_Ind", "IA-EVENT-IND", FieldType.STRING, 
            1);
        pnd_Contract_Record_Rpt_Ia_Ssn = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Ssn", "IA-SSN", FieldType.NUMERIC, 9);

        pnd_Contract_Record_Rpt__R_Field_2 = pnd_Contract_Record_Rpt.newGroupInGroup("pnd_Contract_Record_Rpt__R_Field_2", "REDEFINE", pnd_Contract_Record_Rpt_Ia_Ssn);
        pnd_Contract_Record_Rpt_Ia_Ssn_Alpha = pnd_Contract_Record_Rpt__R_Field_2.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Ssn_Alpha", "IA-SSN-ALPHA", 
            FieldType.STRING, 9);
        pnd_Contract_Record_Rpt_Ia_Fund_Id = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Fund_Id", "IA-FUND-ID", FieldType.STRING, 
            2);
        pnd_Contract_Record_Rpt_Ia_Eff_Date = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Eff_Date", "IA-EFF-DATE", FieldType.STRING, 
            8);
        pnd_Contract_Record_Rpt_Ia_Contract = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Contract", "IA-CONTRACT", FieldType.STRING, 
            10);
        pnd_Contract_Record_Rpt_Ia_Payee_Cde = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Payee_Cde", "IA-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Contract_Record_Rpt_Ia_Cusip = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Cusip", "IA-CUSIP", FieldType.STRING, 9);
        pnd_Contract_Record_Rpt_Ia_Actvty_Cd = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Actvty_Cd", "IA-ACTVTY-CD", FieldType.STRING, 
            1);
        pnd_Contract_Record_Rpt_Ia_Trns_Actvty_Cd = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Trns_Actvty_Cd", "IA-TRNS-ACTVTY-CD", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Rpt_Ia_Orig_Code = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Orig_Code", "IA-ORIG-CODE", FieldType.NUMERIC, 
            2);
        pnd_Contract_Record_Rpt_Ia_Opt_Code = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Opt_Code", "IA-OPT-CODE", FieldType.NUMERIC, 
            2);
        pnd_Contract_Record_Rpt_Ia_Settlement_Id = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Settlement_Id", "IA-SETTLEMENT-ID", 
            FieldType.STRING, 1);
        pnd_Contract_Record_Rpt_Ia_Tran_Cd = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Tran_Cd", "IA-TRAN-CD", FieldType.STRING, 
            3);
        pnd_Contract_Record_Rpt_Ia_Sub_Tran_Cd = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Sub_Tran_Cd", "IA-SUB-TRAN-CD", FieldType.STRING, 
            3);
        pnd_Contract_Record_Rpt_Ia_Pend_Cd = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Pend_Cd", "IA-PEND-CD", FieldType.STRING, 
            1);
        pnd_Contract_Record_Rpt_Ia_Fst_Annt_Dod = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Fst_Annt_Dod", "IA-FST-ANNT-DOD", 
            FieldType.NUMERIC, 8);
        pnd_Contract_Record_Rpt_Ia_2nd_Annt_Dod = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_2nd_Annt_Dod", "IA-2ND-ANNT-DOD", 
            FieldType.NUMERIC, 8);
        pnd_Contract_Record_Rpt_Ia_Offset_Ind = pnd_Contract_Record_Rpt.newFieldInGroup("pnd_Contract_Record_Rpt_Ia_Offset_Ind", "IA-OFFSET-IND", FieldType.STRING, 
            1);
        pnd_Hold_Record_Out = localVariables.newFieldInRecord("pnd_Hold_Record_Out", "#HOLD-RECORD-OUT", FieldType.STRING, 90);

        pnd_Hold_Record_Out__R_Field_3 = localVariables.newGroupInRecord("pnd_Hold_Record_Out__R_Field_3", "REDEFINE", pnd_Hold_Record_Out);
        pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out", "#HOLD-EVENT-IND-OUT", 
            FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_Plan_Num_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Plan_Num_Out", "#HOLD-PLAN-NUM-OUT", 
            FieldType.STRING, 6);
        pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out", "#HOLD-SSN-OUT", 
            FieldType.NUMERIC, 9);
        pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out", "#HOLD-FUND-ID-OUT", 
            FieldType.STRING, 2);
        pnd_Hold_Record_Out_Pnd_Hold_Eff_Date_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Eff_Date_Out", "#HOLD-EFF-DATE-OUT", 
            FieldType.NUMERIC, 8);
        pnd_Hold_Record_Out_Pnd_Hold_Inverse_Date_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Inverse_Date_Out", 
            "#HOLD-INVERSE-DATE-OUT", FieldType.NUMERIC, 12);
        pnd_Hold_Record_Out_Pnd_Hold_Contract_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Contract_Out", "#HOLD-CONTRACT-OUT", 
            FieldType.STRING, 10);
        pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out", "#HOLD-PAYEE-CDE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Hold_Record_Out_Pnd_Hold_Cusip_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Cusip_Out", "#HOLD-CUSIP-OUT", 
            FieldType.STRING, 9);
        pnd_Hold_Record_Out_Pnd_Hold_Plan_Type_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Plan_Type_Out", "#HOLD-PLAN-TYPE-OUT", 
            FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_Source_App_Id_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Source_App_Id_Out", 
            "#HOLD-SOURCE-APP-ID-OUT", FieldType.STRING, 4);
        pnd_Hold_Record_Out_Pnd_Hold_Ia_Orig_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Ia_Orig_Code_Out", 
            "#HOLD-IA-ORIG-CODE-OUT", FieldType.NUMERIC, 2);
        pnd_Hold_Record_Out_Pnd_Hold_Ia_Opt_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Ia_Opt_Code_Out", 
            "#HOLD-IA-OPT-CODE-OUT", FieldType.NUMERIC, 2);
        pnd_Hold_Record_Out_Pnd_Hold_Ia_Settlement_Id_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Ia_Settlement_Id_Out", 
            "#HOLD-IA-SETTLEMENT-ID-OUT", FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out", "#HOLD-TRANS-CODE-OUT", 
            FieldType.NUMERIC, 3);
        pnd_Hold_Record_Out_Pnd_Hold_Act_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Act_Code_Out", "#HOLD-ACT-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_Trns_Act_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Trns_Act_Code_Out", 
            "#HOLD-TRNS-ACT-CODE-OUT", FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_Pend_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Pend_Code_Out", "#HOLD-PEND-CODE-OUT", 
            FieldType.STRING, 1);
        pnd_Hold_Record_Out_Pnd_Hold_First_Ann_Dod_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_First_Ann_Dod_Out", 
            "#HOLD-FIRST-ANN-DOD-OUT", FieldType.NUMERIC, 6);
        pnd_Hold_Record_Out_Pnd_Hold_Second_Ann_Dod_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Second_Ann_Dod_Out", 
            "#HOLD-SECOND-ANN-DOD-OUT", FieldType.NUMERIC, 6);
        pnd_Hold_Record_Out_Pnd_Hold_Trans_Sub_Code_Out = pnd_Hold_Record_Out__R_Field_3.newFieldInGroup("pnd_Hold_Record_Out_Pnd_Hold_Trans_Sub_Code_Out", 
            "#HOLD-TRANS-SUB-CODE-OUT", FieldType.STRING, 3);
        pnd_S_Hold_Record_Out = localVariables.newFieldInRecord("pnd_S_Hold_Record_Out", "#S-HOLD-RECORD-OUT", FieldType.STRING, 90);

        pnd_S_Hold_Record_Out__R_Field_4 = localVariables.newGroupInRecord("pnd_S_Hold_Record_Out__R_Field_4", "REDEFINE", pnd_S_Hold_Record_Out);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Event_Ind_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Event_Ind_Out", 
            "#S-HOLD-EVENT-IND-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Num_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Num_Out", 
            "#S-HOLD-PLAN-NUM-OUT", FieldType.STRING, 6);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Ssn_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Ssn_Out", "#S-HOLD-SSN-OUT", 
            FieldType.NUMERIC, 9);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Fund_Id_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Fund_Id_Out", 
            "#S-HOLD-FUND-ID-OUT", FieldType.STRING, 2);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Eff_Date_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Eff_Date_Out", 
            "#S-HOLD-EFF-DATE-OUT", FieldType.NUMERIC, 8);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Inverse_Date_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Inverse_Date_Out", 
            "#S-HOLD-INVERSE-DATE-OUT", FieldType.NUMERIC, 12);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Contract_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Contract_Out", 
            "#S-HOLD-CONTRACT-OUT", FieldType.STRING, 10);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Payee_Cde_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Payee_Cde_Out", 
            "#S-HOLD-PAYEE-CDE-OUT", FieldType.NUMERIC, 2);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Cusip_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Cusip_Out", "#S-HOLD-CUSIP-OUT", 
            FieldType.STRING, 9);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Type_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Plan_Type_Out", 
            "#S-HOLD-PLAN-TYPE-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Source_App_Id_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Source_App_Id_Out", 
            "#S-HOLD-SOURCE-APP-ID-OUT", FieldType.STRING, 4);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Orig_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Orig_Code_Out", 
            "#S-HOLD-IA-ORIG-CODE-OUT", FieldType.NUMERIC, 2);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Opt_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Opt_Code_Out", 
            "#S-HOLD-IA-OPT-CODE-OUT", FieldType.NUMERIC, 2);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Settlement_Id_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Ia_Settlement_Id_Out", 
            "#S-HOLD-IA-SETTLEMENT-ID-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Code_Out", 
            "#S-HOLD-TRANS-CODE-OUT", FieldType.NUMERIC, 3);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Act_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Act_Code_Out", 
            "#S-HOLD-ACT-CODE-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Trns_Act_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Trns_Act_Code_Out", 
            "#S-HOLD-TRNS-ACT-CODE-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Pend_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Pend_Code_Out", 
            "#S-HOLD-PEND-CODE-OUT", FieldType.STRING, 1);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_First_Ann_Dod_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_First_Ann_Dod_Out", 
            "#S-HOLD-FIRST-ANN-DOD-OUT", FieldType.NUMERIC, 6);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Second_Ann_Dod_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Second_Ann_Dod_Out", 
            "#S-HOLD-SECOND-ANN-DOD-OUT", FieldType.NUMERIC, 6);
        pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Sub_Code_Out = pnd_S_Hold_Record_Out__R_Field_4.newFieldInGroup("pnd_S_Hold_Record_Out_Pnd_S_Hold_Trans_Sub_Code_Out", 
            "#S-HOLD-TRANS-SUB-CODE-OUT", FieldType.STRING, 3);

        pnd_Daily_Record_Out = localVariables.newGroupInRecord("pnd_Daily_Record_Out", "#DAILY-RECORD-OUT");
        pnd_Daily_Record_Out_Pnd_Srce_Id = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Srce_Id", "#SRCE-ID", FieldType.STRING, 4);
        pnd_Daily_Record_Out_Pnd_Event_Ind_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Event_Ind_Out", "#EVENT-IND-OUT", FieldType.STRING, 
            1);
        pnd_Daily_Record_Out_Pnd_Ownr_Type = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ownr_Type", "#OWNR-TYPE", FieldType.STRING, 
            1);
        pnd_Daily_Record_Out_Pnd_Ssn_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ssn_Out", "#SSN-OUT", FieldType.NUMERIC, 9);

        pnd_Daily_Record_Out__R_Field_5 = pnd_Daily_Record_Out.newGroupInGroup("pnd_Daily_Record_Out__R_Field_5", "REDEFINE", pnd_Daily_Record_Out_Pnd_Ssn_Out);
        pnd_Daily_Record_Out_Pnd_Ssn_Out_Alpha = pnd_Daily_Record_Out__R_Field_5.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ssn_Out_Alpha", "#SSN-OUT-ALPHA", 
            FieldType.STRING, 9);
        pnd_Daily_Record_Out_Pnd_Client_Id = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Client_Id", "#CLIENT-ID", FieldType.STRING, 
            22);
        pnd_Daily_Record_Out_Pnd_Pin = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Pin", "#PIN", FieldType.STRING, 7);
        pnd_Daily_Record_Out_Pnd_Eff_Date_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Eff_Date_Out", "#EFF-DATE-OUT", FieldType.NUMERIC, 
            8);
        pnd_Daily_Record_Out_Pnd_Proc_Date_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Proc_Date_Out", "#PROC-DATE-OUT", FieldType.STRING, 
            8);
        pnd_Daily_Record_Out_Pnd_Contract_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Contract_Out", "#CONTRACT-OUT", FieldType.STRING, 
            12);
        pnd_Daily_Record_Out_Pnd_Prod_Code = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Prod_Code", "#PROD-CODE", FieldType.STRING, 
            3);
        pnd_Daily_Record_Out_Pnd_Fund_Id_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Fund_Id_Out", "#FUND-ID-OUT", FieldType.STRING, 
            2);
        pnd_Daily_Record_Out_Pnd_Payee_Cde_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Payee_Cde_Out", "#PAYEE-CDE-OUT", FieldType.NUMERIC, 
            2);
        pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out", "#IA-ORIG-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Daily_Record_Out_Pnd_Ia_Opt_Code_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ia_Opt_Code_Out", "#IA-OPT-CODE-OUT", 
            FieldType.NUMERIC, 2);
        pnd_Daily_Record_Out_Pnd_Ia_Settlement_Id_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Ia_Settlement_Id_Out", "#IA-SETTLEMENT-ID-OUT", 
            FieldType.STRING, 1);
        pnd_Daily_Record_Out_Pnd_Cusip_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Cusip_Out", "#CUSIP-OUT", FieldType.STRING, 
            9);
        pnd_Daily_Record_Out_Pnd_Prod_Cusip = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Prod_Cusip", "#PROD-CUSIP", FieldType.STRING, 
            9);
        pnd_Daily_Record_Out_Pnd_Plan_Type_Out = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Plan_Type_Out", "#PLAN-TYPE-OUT", FieldType.STRING, 
            1);
        pnd_Daily_Record_Out_Pnd_Cmpny_Nme = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Cmpny_Nme", "#CMPNY-NME", FieldType.STRING, 
            32);
        pnd_Daily_Record_Out_Pnd_Addr1 = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Addr1", "#ADDR1", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_Addr2 = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Addr2", "#ADDR2", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_Addr3 = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Addr3", "#ADDR3", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_Addr4 = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Addr4", "#ADDR4", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_Addr5 = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Addr5", "#ADDR5", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_City = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_City", "#CITY", FieldType.STRING, 40);
        pnd_Daily_Record_Out_Pnd_State = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Daily_Record_Out_Pnd_Country = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Country", "#COUNTRY", FieldType.STRING, 2);
        pnd_Daily_Record_Out_Pnd_Zip = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Zip", "#ZIP", FieldType.STRING, 10);
        pnd_Daily_Record_Out_Pnd_Edelivery = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Edelivery", "#EDELIVERY", FieldType.STRING, 
            1);
        pnd_Daily_Record_Out_Pnd_Email = pnd_Daily_Record_Out.newFieldInGroup("pnd_Daily_Record_Out_Pnd_Email", "#EMAIL", FieldType.STRING, 255);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.STRING, 9);
        pnd_Work_Before_Key = localVariables.newFieldInRecord("pnd_Work_Before_Key", "#WORK-BEFORE-KEY", FieldType.STRING, 21);

        pnd_Work_Before_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Before_Key__R_Field_6", "REDEFINE", pnd_Work_Before_Key);
        pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef = pnd_Work_Before_Key__R_Field_6.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Trans_Dte_Bef", "#WORK-TRANS-DTE-BEF", 
            FieldType.NUMERIC, 8);
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef = pnd_Work_Before_Key__R_Field_6.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Bef", 
            "#WORK-CNTRCT-PPCN-NBR-BEF", FieldType.STRING, 10);
        pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef = pnd_Work_Before_Key__R_Field_6.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Cntrct_Payee_Cde_Bef", 
            "#WORK-CNTRCT-PAYEE-CDE-BEF", FieldType.NUMERIC, 2);
        pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef = pnd_Work_Before_Key__R_Field_6.newFieldInGroup("pnd_Work_Before_Key_Pnd_Work_Imge_Id_Bef", "#WORK-IMGE-ID-BEF", 
            FieldType.STRING, 1);
        pnd_Work_After_Key = localVariables.newFieldInRecord("pnd_Work_After_Key", "#WORK-AFTER-KEY", FieldType.STRING, 25);

        pnd_Work_After_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_After_Key__R_Field_7", "REDEFINE", pnd_Work_After_Key);
        pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft = pnd_Work_After_Key__R_Field_7.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Invrs_Dte_Aft", "#WORK-INVRS-DTE-AFT", 
            FieldType.NUMERIC, 12);
        pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft = pnd_Work_After_Key__R_Field_7.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Cntrct_Ppcn_Nbr_Aft", 
            "#WORK-CNTRCT-PPCN-NBR-AFT", FieldType.STRING, 10);
        pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft = pnd_Work_After_Key__R_Field_7.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Cntrct_Payee_Cde_Aft", 
            "#WORK-CNTRCT-PAYEE-CDE-AFT", FieldType.NUMERIC, 2);
        pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft = pnd_Work_After_Key__R_Field_7.newFieldInGroup("pnd_Work_After_Key_Pnd_Work_Imge_Id_Aft", "#WORK-IMGE-ID-AFT", 
            FieldType.STRING, 1);

        hold_Fields = localVariables.newGroupInRecord("hold_Fields", "HOLD-FIELDS");
        hold_Fields_Pnd_Hold_Contract = hold_Fields.newFieldInGroup("hold_Fields_Pnd_Hold_Contract", "#HOLD-CONTRACT", FieldType.STRING, 10);
        hold_Fields_Pnd_Hold_Payee_Cde = hold_Fields.newFieldInGroup("hold_Fields_Pnd_Hold_Payee_Cde", "#HOLD-PAYEE-CDE", FieldType.NUMERIC, 2);
        hold_Fields_Pnd_Hold_Fund = hold_Fields.newFieldInGroup("hold_Fields_Pnd_Hold_Fund", "#HOLD-FUND", FieldType.STRING, 2);
        hold_Fields_Pnd_Hold_Event_Ind = hold_Fields.newFieldInGroup("hold_Fields_Pnd_Hold_Event_Ind", "#HOLD-EVENT-IND", FieldType.STRING, 1);
        pnd_Offset_Found = localVariables.newFieldInRecord("pnd_Offset_Found", "#OFFSET-FOUND", FieldType.BOOLEAN, 1);
        pnd_Dup_Found = localVariables.newFieldInRecord("pnd_Dup_Found", "#DUP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Notfnd = localVariables.newFieldInRecord("pnd_Notfnd", "#NOTFND", FieldType.BOOLEAN, 1);
        pnd_After_Found = localVariables.newFieldInRecord("pnd_After_Found", "#AFTER-FOUND", FieldType.BOOLEAN, 1);
        pnd_Before_Found = localVariables.newFieldInRecord("pnd_Before_Found", "#BEFORE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Dont_Write = localVariables.newFieldInRecord("pnd_Dont_Write", "#DONT-WRITE", FieldType.BOOLEAN, 1);
        pnd_First_Done = localVariables.newFieldInRecord("pnd_First_Done", "#FIRST-DONE", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Hold_Before_Pend_Code = localVariables.newFieldInRecord("pnd_Hold_Before_Pend_Code", "#HOLD-BEFORE-PEND-CODE", FieldType.STRING, 1);
        pnd_Hold_After_Pend_Code = localVariables.newFieldInRecord("pnd_Hold_After_Pend_Code", "#HOLD-AFTER-PEND-CODE", FieldType.STRING, 1);
        pnd_Offset_Count = localVariables.newFieldInRecord("pnd_Offset_Count", "#OFFSET-COUNT", FieldType.NUMERIC, 10);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.PACKED_DECIMAL, 4);
        pnd_H = localVariables.newFieldInRecord("pnd_H", "#H", FieldType.PACKED_DECIMAL, 4);
        pnd_H_Tot = localVariables.newFieldInRecord("pnd_H_Tot", "#H-TOT", FieldType.PACKED_DECIMAL, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 4);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 4);
        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.PACKED_DECIMAL, 4);
        pnd_End = localVariables.newFieldInRecord("pnd_End", "#END", FieldType.PACKED_DECIMAL, 4);
        pnd_P_Ind = localVariables.newFieldInRecord("pnd_P_Ind", "#P-IND", FieldType.BOOLEAN, 1);
        pnd_P_Date = localVariables.newFieldInRecord("pnd_P_Date", "#P-DATE", FieldType.STRING, 8);
        pnd_Temp_Table_Rec = localVariables.newFieldInRecord("pnd_Temp_Table_Rec", "#TEMP-TABLE-REC", FieldType.STRING, 90);

        pnd_Temp_Table_Rec__R_Field_8 = localVariables.newGroupInRecord("pnd_Temp_Table_Rec__R_Field_8", "REDEFINE", pnd_Temp_Table_Rec);
        pnd_Temp_Table_Rec_Pnd_Hold_Table_Ind = pnd_Temp_Table_Rec__R_Field_8.newFieldInGroup("pnd_Temp_Table_Rec_Pnd_Hold_Table_Ind", "#HOLD-TABLE-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Table_Rec__Filler1 = pnd_Temp_Table_Rec__R_Field_8.newFieldInGroup("pnd_Temp_Table_Rec__Filler1", "_FILLER1", FieldType.STRING, 15);
        pnd_Temp_Table_Rec_Pnd_Hold_Table_Fund = pnd_Temp_Table_Rec__R_Field_8.newFieldInGroup("pnd_Temp_Table_Rec_Pnd_Hold_Table_Fund", "#HOLD-TABLE-FUND", 
            FieldType.STRING, 2);
        pnd_Temp_Table_Rec__Filler2 = pnd_Temp_Table_Rec__R_Field_8.newFieldInGroup("pnd_Temp_Table_Rec__Filler2", "_FILLER2", FieldType.STRING, 51);
        pnd_Temp_Table_Rec_Pnd_Temp_Table_Trans = pnd_Temp_Table_Rec__R_Field_8.newFieldInGroup("pnd_Temp_Table_Rec_Pnd_Temp_Table_Trans", "#TEMP-TABLE-TRANS", 
            FieldType.NUMERIC, 3);
        pnd_Temp_Record_Out = localVariables.newFieldInRecord("pnd_Temp_Record_Out", "#TEMP-RECORD-OUT", FieldType.STRING, 90);

        pnd_Temp_Record_Out__R_Field_9 = localVariables.newGroupInRecord("pnd_Temp_Record_Out__R_Field_9", "REDEFINE", pnd_Temp_Record_Out);
        pnd_Temp_Record_Out_Pnd_Temp_Record_Ind = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out_Pnd_Temp_Record_Ind", "#TEMP-RECORD-IND", 
            FieldType.STRING, 1);
        pnd_Temp_Record_Out__Filler3 = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out__Filler3", "_FILLER3", FieldType.STRING, 15);
        pnd_Temp_Record_Out_Pnd_Temp_Record_Fund = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out_Pnd_Temp_Record_Fund", "#TEMP-RECORD-FUND", 
            FieldType.STRING, 2);
        pnd_Temp_Record_Out__Filler4 = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out__Filler4", "_FILLER4", FieldType.STRING, 20);
        pnd_Temp_Record_Out_Pnd_Temp_Contract = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out_Pnd_Temp_Contract", "#TEMP-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Temp_Record_Out_Pnd_Temp_Payee = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out_Pnd_Temp_Payee", "#TEMP-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Temp_Record_Out__Filler5 = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out__Filler5", "_FILLER5", FieldType.STRING, 19);
        pnd_Temp_Record_Out_Pnd_Temp_Record_Trans = pnd_Temp_Record_Out__R_Field_9.newFieldInGroup("pnd_Temp_Record_Out_Pnd_Temp_Record_Trans", "#TEMP-RECORD-TRANS", 
            FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Daily_Record_Out_Pnd_Srce_Id.setInitialValue("IA");
        pnd_Daily_Record_Out_Pnd_Ownr_Type.setInitialValue("I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap607t() throws Exception
    {
        super("Iaap607t");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133;//Natural: FORMAT ( 2 ) PS = 0 LS = 133;//Natural: FORMAT ( 3 ) PS = 0 LS = 133
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        pnd_P_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #P-DATE
        pnd_Offset_Found.reset();                                                                                                                                         //Natural: RESET #OFFSET-FOUND #H #S #H-TOT
        pnd_H.reset();
        pnd_S.reset();
        pnd_H_Tot.reset();
        pnd_First_Time.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-TIME := TRUE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CONTRACT-RECORD-IN
        while (condition(getWorkFiles().read(1, pnd_Contract_Record_In)))
        {
            //*  FIXED FUND
            //*  OVERRIDE
            if (condition(pnd_Contract_Record_In_Pnd_Cusip_In.equals("878094101")))                                                                                       //Natural: IF #CUSIP-IN = '878094101'
            {
                pnd_Contract_Record_In_Pnd_Cusip_In.setValue("88630X118");                                                                                                //Natural: ASSIGN #CUSIP-IN := '88630X118'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Hold_Record_Out.setValue(pnd_Contract_Record_In);                                                                                                     //Natural: MOVE #CONTRACT-RECORD-IN TO #HOLD-RECORD-OUT
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Contract_Record_In_Pnd_Contract_In.equals(pnd_Hold_Record_Out_Pnd_Hold_Contract_Out) && pnd_Contract_Record_In_Pnd_Payee_Cde_In.equals(pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out)  //Natural: IF #CONTRACT-IN = #HOLD-CONTRACT-OUT AND #PAYEE-CDE-IN = #HOLD-PAYEE-CDE-OUT AND #FUND-ID-IN = #HOLD-FUND-ID-OUT
                && pnd_Contract_Record_In_Pnd_Fund_Id_In.equals(pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out)))
            {
                pnd_H.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #H
                pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_H).setValue(pnd_Contract_Record_In);                                                                       //Natural: MOVE #CONTRACT-RECORD-IN TO #HOLD-TABLE-REC ( #H )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TABLE
                sub_Process_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_H_Tot.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #H-TOT #S
                pnd_S.setValue(0);
                pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue("*").reset();                                                                                                  //Natural: RESET #HOLD-TABLE-REC ( * ) #HOLD-SKIP-IND ( * )
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue("*").reset();
                pnd_H.setValue(1);                                                                                                                                        //Natural: MOVE 1 TO #H
                pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_H).setValue(pnd_Contract_Record_In);                                                                       //Natural: MOVE #CONTRACT-RECORD-IN TO #HOLD-TABLE-REC ( #H )
                pnd_Hold_Record_Out.setValue(pnd_Contract_Record_In);                                                                                                     //Natural: MOVE #CONTRACT-RECORD-IN TO #HOLD-RECORD-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_H.greater(getZero())))                                                                                                                          //Natural: IF #H > 0
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TABLE
            sub_Process_Table();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-OFFSETS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DAILY-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-DUPS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MARK-DUPLICATES
    }
    private void sub_Process_Table() throws Exception                                                                                                                     //Natural: PROCESS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ADDED 6/24/11 TO CHECK FOR DUPS
        if (condition(pnd_H.greater(1)))                                                                                                                                  //Natural: IF #H GT 1
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-DUPS
            sub_Check_For_Dups();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_H_Tot.setValue(pnd_H);                                                                                                                                        //Natural: MOVE #H TO #H-TOT
        pnd_H.setValue(1);                                                                                                                                                //Natural: MOVE 1 TO #H
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_H.greater(pnd_H_Tot))) {break;}                                                                                                             //Natural: UNTIL #H > #H-TOT
            pnd_Hold_Record_Out.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_H));                                                                              //Natural: MOVE #HOLD-TABLE-REC ( #H ) TO #HOLD-RECORD-OUT
            if (condition(pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_H).notEquals("Y")))                                                                               //Natural: IF #HOLD-SKIP-IND ( #H ) NOT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-OFFSETS
                sub_Process_Offsets();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_H).equals("Y")))                                                                                  //Natural: IF #HOLD-SKIP-IND ( #H ) = 'Y'
            {
                pnd_Contract_Record_Rpt_Ia_Offset_Ind.setValue("M");                                                                                                      //Natural: MOVE 'M' TO IA-OFFSET-IND
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
            sub_Write_Report_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_H.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #H
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_H_Tot).notEquals("Y")))                                                                           //Natural: IF #HOLD-SKIP-IND ( #H-TOT ) NOT = 'Y'
            {
                pnd_Hold_Record_Out.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_H_Tot));                                                                      //Natural: MOVE #HOLD-TABLE-REC ( #H-TOT ) TO #HOLD-RECORD-OUT
                                                                                                                                                                          //Natural: PERFORM WRITE-DAILY-REC
                sub_Write_Daily_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_H_Tot.nsubtract(1);                                                                                                                                       //Natural: SUBTRACT 1 FROM #H-TOT
            if (condition(pnd_H_Tot.equals(getZero())))                                                                                                                   //Natural: IF #H-TOT = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Process_Offsets() throws Exception                                                                                                                   //Natural: PROCESS-OFFSETS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_S.setValue(0);                                                                                                                                                //Natural: MOVE 0 TO #S
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_S.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #S
            if (condition(pnd_S.greater(pnd_H_Tot)))                                                                                                                      //Natural: IF #S > #H-TOT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_S_Hold_Record_Out.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_S));                                                                            //Natural: MOVE #HOLD-TABLE-REC ( #S ) TO #S-HOLD-RECORD-OUT
            if (condition(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out.equals("L") && pnd_S.notEquals(pnd_H) && pnd_S_Hold_Record_Out_Pnd_S_Hold_Event_Ind_Out.equals("F")  //Natural: IF #HOLD-EVENT-IND-OUT = 'L' AND #S NOT = #H AND #S-HOLD-EVENT-IND-OUT = 'F' AND #HOLD-SKIP-IND ( #S ) NOT = 'Y'
                && pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_S).notEquals("Y")))
            {
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_H).setValue("Y");                                                                                           //Natural: MOVE 'Y' TO #HOLD-SKIP-IND ( #H )
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_S).setValue("Y");                                                                                           //Natural: MOVE 'Y' TO #HOLD-SKIP-IND ( #S )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out.equals("F") && pnd_S.notEquals(pnd_H) && pnd_S_Hold_Record_Out_Pnd_S_Hold_Event_Ind_Out.equals("L")  //Natural: IF #HOLD-EVENT-IND-OUT = 'F' AND #S NOT = #H AND #S-HOLD-EVENT-IND-OUT = 'L' AND #HOLD-SKIP-IND ( #S ) NOT = 'Y'
                && pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_S).notEquals("Y")))
            {
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_H).setValue("Y");                                                                                           //Natural: MOVE 'Y' TO #HOLD-SKIP-IND ( #H )
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_S).setValue("Y");                                                                                           //Natural: MOVE 'Y' TO #HOLD-SKIP-IND ( #S )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Write_Report_Rec() throws Exception                                                                                                                  //Natural: WRITE-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Contract_Record_Rpt_Ia_Event_Ind.setValue(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out);                                                                        //Natural: MOVE #HOLD-EVENT-IND-OUT TO IA-EVENT-IND
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out.equals(getZero()) || pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out.equals(999999999)))                                  //Natural: IF #HOLD-SSN-OUT = 0 OR #HOLD-SSN-OUT = 999999999
        {
            pnd_Contract_Record_Rpt_Ia_Ssn_Alpha.setValue("         ");                                                                                                   //Natural: MOVE '         ' TO IA-SSN-ALPHA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Record_Rpt_Ia_Ssn.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out);                                                                                //Natural: MOVE #HOLD-SSN-OUT TO IA-SSN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out.equals("1 ")))                                                                                             //Natural: IF #HOLD-FUND-ID-OUT = '1 '
        {
            pnd_Contract_Record_Rpt_Ia_Fund_Id.setValue("T1");                                                                                                            //Natural: MOVE 'T1' TO IA-FUND-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Record_Rpt_Ia_Fund_Id.setValue(pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out);                                                                        //Natural: MOVE #HOLD-FUND-ID-OUT TO IA-FUND-ID
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract_Record_Rpt_Ia_Eff_Date.setValue(pnd_Hold_Record_Out_Pnd_Hold_Eff_Date_Out);                                                                          //Natural: MOVE #HOLD-EFF-DATE-OUT TO IA-EFF-DATE
        pnd_Contract_Record_Rpt_Ia_Contract.setValue(pnd_Hold_Record_Out_Pnd_Hold_Contract_Out);                                                                          //Natural: MOVE #HOLD-CONTRACT-OUT TO IA-CONTRACT
        pnd_Contract_Record_Rpt_Ia_Payee_Cde.setValue(pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out);                                                                        //Natural: MOVE #HOLD-PAYEE-CDE-OUT TO IA-PAYEE-CDE
        pnd_Contract_Record_Rpt_Ia_Cusip.setValue(pnd_Hold_Record_Out_Pnd_Hold_Cusip_Out);                                                                                //Natural: MOVE #HOLD-CUSIP-OUT TO IA-CUSIP
        pnd_Contract_Record_Rpt_Ia_Actvty_Cd.setValue(pnd_Hold_Record_Out_Pnd_Hold_Act_Code_Out);                                                                         //Natural: MOVE #HOLD-ACT-CODE-OUT TO IA-ACTVTY-CD
        pnd_Contract_Record_Rpt_Ia_Trns_Actvty_Cd.setValue(pnd_Hold_Record_Out_Pnd_Hold_Trns_Act_Code_Out);                                                               //Natural: MOVE #HOLD-TRNS-ACT-CODE-OUT TO IA-TRNS-ACTVTY-CD
        pnd_Contract_Record_Rpt_Ia_Orig_Code.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ia_Orig_Code_Out);                                                                     //Natural: MOVE #HOLD-IA-ORIG-CODE-OUT TO IA-ORIG-CODE
        pnd_Contract_Record_Rpt_Ia_Opt_Code.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ia_Opt_Code_Out);                                                                       //Natural: MOVE #HOLD-IA-OPT-CODE-OUT TO IA-OPT-CODE
        //*    MOVE #HOLD-IA-SETTLEMENT-ID-OUT    TO  IA-SETTLEMENT-ID
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out.equals("F")))                                                                                            //Natural: IF #HOLD-EVENT-IND-OUT = 'F'
        {
            if (condition(pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out.equals(31) || pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out.equals(33)))                              //Natural: IF #HOLD-TRANS-CODE-OUT = 31 OR = 33
            {
                pnd_Contract_Record_Rpt_Ia_Settlement_Id.setValue("N");                                                                                                   //Natural: MOVE 'N' TO IA-SETTLEMENT-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Contract_Record_Rpt_Ia_Settlement_Id.setValue("P");                                                                                                   //Natural: MOVE 'P' TO IA-SETTLEMENT-ID
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract_Record_Rpt_Ia_Tran_Cd.setValue(pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out);                                                                         //Natural: MOVE #HOLD-TRANS-CODE-OUT TO IA-TRAN-CD
        pnd_Contract_Record_Rpt_Ia_Sub_Tran_Cd.setValue(pnd_Hold_Record_Out_Pnd_Hold_Trans_Sub_Code_Out);                                                                 //Natural: MOVE #HOLD-TRANS-SUB-CODE-OUT TO IA-SUB-TRAN-CD
        pnd_Contract_Record_Rpt_Ia_Pend_Cd.setValue(pnd_Hold_Record_Out_Pnd_Hold_Pend_Code_Out);                                                                          //Natural: MOVE #HOLD-PEND-CODE-OUT TO IA-PEND-CD
        pnd_Contract_Record_Rpt_Ia_Fst_Annt_Dod.setValue(pnd_Hold_Record_Out_Pnd_Hold_First_Ann_Dod_Out);                                                                 //Natural: MOVE #HOLD-FIRST-ANN-DOD-OUT TO IA-FST-ANNT-DOD
        pnd_Contract_Record_Rpt_Ia_2nd_Annt_Dod.setValue(pnd_Hold_Record_Out_Pnd_Hold_Second_Ann_Dod_Out);                                                                //Natural: MOVE #HOLD-SECOND-ANN-DOD-OUT TO IA-2ND-ANNT-DOD
        getWorkFiles().write(2, false, pnd_Contract_Record_Rpt);                                                                                                          //Natural: WRITE WORK FILE 2 #CONTRACT-RECORD-RPT
        pnd_Contract_Record_Rpt.reset();                                                                                                                                  //Natural: RESET #CONTRACT-RECORD-RPT
    }
    private void sub_Write_Daily_Rec() throws Exception                                                                                                                   //Natural: WRITE-DAILY-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Daily_Record_Out_Pnd_Event_Ind_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out);                                                                      //Natural: MOVE #HOLD-EVENT-IND-OUT TO #EVENT-IND-OUT
        //* *MOVE #HOLD-PLAN-NUM-OUT            TO  #PLAN-NUM-OUT
        //* *MOVE #HOLD-SSN-OUT                 TO  #SSN-OUT
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out.equals(getZero()) || pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out.equals(999999999)))                                  //Natural: IF #HOLD-SSN-OUT = 0 OR #HOLD-SSN-OUT = 999999999
        {
            pnd_Daily_Record_Out_Pnd_Ssn_Out_Alpha.setValue("         ");                                                                                                 //Natural: MOVE '         ' TO #SSN-OUT-ALPHA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Daily_Record_Out_Pnd_Ssn_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ssn_Out);                                                                              //Natural: MOVE #HOLD-SSN-OUT TO #SSN-OUT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out.equals("1 ")))                                                                                             //Natural: IF #HOLD-FUND-ID-OUT = '1 '
        {
            pnd_Daily_Record_Out_Pnd_Fund_Id_Out.setValue("T1");                                                                                                          //Natural: MOVE 'T1' TO #FUND-ID-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Daily_Record_Out_Pnd_Fund_Id_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Fund_Id_Out);                                                                      //Natural: MOVE #HOLD-FUND-ID-OUT TO #FUND-ID-OUT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Daily_Record_Out_Pnd_Proc_Date_Out.setValue(pnd_P_Date);                                                                                                      //Natural: MOVE #P-DATE TO #PROC-DATE-OUT
        pnd_Daily_Record_Out_Pnd_Eff_Date_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Eff_Date_Out);                                                                        //Natural: MOVE #HOLD-EFF-DATE-OUT TO #EFF-DATE-OUT
        pnd_Daily_Record_Out_Pnd_Contract_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Contract_Out);                                                                        //Natural: MOVE #HOLD-CONTRACT-OUT TO #CONTRACT-OUT
        pnd_Daily_Record_Out_Pnd_Payee_Cde_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Payee_Cde_Out);                                                                      //Natural: MOVE #HOLD-PAYEE-CDE-OUT TO #PAYEE-CDE-OUT
        pnd_Daily_Record_Out_Pnd_Cusip_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Cusip_Out);                                                                              //Natural: MOVE #HOLD-CUSIP-OUT TO #CUSIP-OUT
        pnd_Daily_Record_Out_Pnd_Plan_Type_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Plan_Type_Out);                                                                      //Natural: MOVE #HOLD-PLAN-TYPE-OUT TO #PLAN-TYPE-OUT
        //* *MOVE #HOLD-SOURCE-APP-ID-OUT       TO  #SOURCE-APP-ID-OUT
        pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ia_Orig_Code_Out);                                                                //Natural: MOVE #HOLD-IA-ORIG-CODE-OUT TO #IA-ORIG-CODE-OUT
        pnd_Daily_Record_Out_Pnd_Ia_Opt_Code_Out.setValue(pnd_Hold_Record_Out_Pnd_Hold_Ia_Opt_Code_Out);                                                                  //Natural: MOVE #HOLD-IA-OPT-CODE-OUT TO #IA-OPT-CODE-OUT
        if (condition(pnd_Hold_Record_Out_Pnd_Hold_Event_Ind_Out.equals("F")))                                                                                            //Natural: IF #HOLD-EVENT-IND-OUT = 'F'
        {
            if (condition(pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out.equals(31) || pnd_Hold_Record_Out_Pnd_Hold_Trans_Code_Out.equals(33)))                              //Natural: IF #HOLD-TRANS-CODE-OUT = 31 OR = 33
            {
                pnd_Daily_Record_Out_Pnd_Ia_Settlement_Id_Out.setValue("N");                                                                                              //Natural: MOVE 'N' TO #IA-SETTLEMENT-ID-OUT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Daily_Record_Out_Pnd_Ia_Settlement_Id_Out.setValue("P");                                                                                              //Natural: MOVE 'P' TO #IA-SETTLEMENT-ID-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet413 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #IA-ORIG-CODE-OUT;//Natural: VALUE 37,38
        if (condition((pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out.equals(37) || pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out.equals(38))))
        {
            decideConditionsMet413++;
            pnd_Daily_Record_Out_Pnd_Prod_Cusip.setValue("88630X407");                                                                                                    //Natural: ASSIGN #PROD-CUSIP := '88630X407'
        }                                                                                                                                                                 //Natural: VALUE 40
        else if (condition((pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out.equals(40))))
        {
            decideConditionsMet413++;
            pnd_Daily_Record_Out_Pnd_Prod_Cusip.setValue("NRVA01516");                                                                                                    //Natural: ASSIGN #PROD-CUSIP := 'NRVA01516'
        }                                                                                                                                                                 //Natural: VALUE 43
        else if (condition((pnd_Daily_Record_Out_Pnd_Ia_Orig_Code_Out.equals(43))))
        {
            decideConditionsMet413++;
            pnd_Daily_Record_Out_Pnd_Prod_Cusip.setValue("NRVA03232");                                                                                                    //Natural: ASSIGN #PROD-CUSIP := 'NRVA03232'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Daily_Record_Out_Pnd_Prod_Cusip.setValue(" ");                                                                                                            //Natural: ASSIGN #PROD-CUSIP := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(3, false, pnd_Daily_Record_Out);                                                                                                             //Natural: WRITE WORK FILE 3 #DAILY-RECORD-OUT
        pnd_Daily_Record_Out.resetInitial();                                                                                                                              //Natural: RESET INITIAL #DAILY-RECORD-OUT
    }
    private void sub_Check_For_Dups() throws Exception                                                                                                                    //Natural: CHECK-FOR-DUPS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ADDED 6/24/11 TO CHECK FOR DUPS
        pnd_Start.reset();                                                                                                                                                //Natural: RESET #START #END #P-IND #TEMP-RECORD-OUT #TEMP-TABLE-REC
        pnd_End.reset();
        pnd_P_Ind.reset();
        pnd_Temp_Record_Out.reset();
        pnd_Temp_Table_Rec.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #H
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_H)); pnd_I.nadd(1))
        {
            pnd_Temp_Table_Rec.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_I));                                                                               //Natural: ASSIGN #TEMP-TABLE-REC := #HOLD-TABLE-REC ( #I )
            if (condition(pnd_Temp_Table_Rec_Pnd_Hold_Table_Fund.equals(pnd_Temp_Record_Out_Pnd_Temp_Record_Fund) && pnd_Temp_Table_Rec_Pnd_Hold_Table_Ind.equals(pnd_Temp_Record_Out_Pnd_Temp_Record_Ind))) //Natural: IF #HOLD-TABLE-FUND = #TEMP-RECORD-FUND AND #HOLD-TABLE-IND = #TEMP-RECORD-IND
            {
                //*  FIRST DOLLAR
                if (condition(DbsUtil.maskMatches(pnd_Temp_Table_Rec,"'F'")))                                                                                             //Natural: IF #TEMP-TABLE-REC = MASK ( 'F' )
                {
                    if (condition(pnd_Start.equals(getZero())))                                                                                                           //Natural: IF #START = 0
                    {
                        pnd_Start.compute(new ComputeParameters(false, pnd_Start), pnd_I.subtract(1));                                                                    //Natural: ASSIGN #START := #I - 1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_End.setValue(pnd_I);                                                                                                                              //Natural: ASSIGN #END := #I
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_I).setValue("Y");                                                                                       //Natural: ASSIGN #HOLD-SKIP-IND ( #I ) := 'Y'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Start.greater(getZero())))                                                                                                              //Natural: IF #START GT 0
                {
                                                                                                                                                                          //Natural: PERFORM MARK-DUPLICATES
                    sub_Mark_Duplicates();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Temp_Record_Out.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_I));                                                                              //Natural: ASSIGN #TEMP-RECORD-OUT := #HOLD-TABLE-REC ( #I )
            //*  FIRST DOLLAR
            if (condition(DbsUtil.maskMatches(pnd_Temp_Record_Out,"'F'")))                                                                                                //Natural: IF #TEMP-RECORD-OUT = MASK ( 'F' )
            {
                //*  TREAT AS TRANSFER
                if (condition(pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(35) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(36) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(50)  //Natural: IF #TEMP-RECORD-TRANS = 35 OR = 36 OR = 50 OR = 51 OR = 53 OR = 37 OR = 66 OR = 102 OR = 500 OR = 502 OR = 504 OR = 506 OR = 508 OR = 516 OR = 518
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(51) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(53) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(37) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(66) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(102) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(500) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(502) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(504) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(506) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(508) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(516) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(518)))
                {
                    pnd_P_Ind.setValue(true);                                                                                                                             //Natural: ASSIGN #P-IND := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Start.greater(getZero())))                                                                                                                      //Natural: IF #START GT 0
        {
                                                                                                                                                                          //Natural: PERFORM MARK-DUPLICATES
            sub_Mark_Duplicates();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mark_Duplicates() throws Exception                                                                                                                   //Natural: MARK-DUPLICATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MARK THE DUPLICATES - IF COMBO OF NEW ISSUE AND TRANSFER, KEEP LATTER
        F0:                                                                                                                                                               //Natural: FOR #K #START TO #END
        for (pnd_K.setValue(pnd_Start); condition(pnd_K.lessOrEqual(pnd_End)); pnd_K.nadd(1))
        {
            pnd_Temp_Record_Out.setValue(pnd_Hold_Table_Pnd_Hold_Table_Rec.getValue(pnd_K));                                                                              //Natural: ASSIGN #TEMP-RECORD-OUT := #HOLD-TABLE-REC ( #K )
            if (condition(pnd_P_Ind.getBoolean()))                                                                                                                        //Natural: IF #P-IND
            {
                if (condition(pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(35) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(36) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(50)  //Natural: IF #TEMP-RECORD-TRANS = 35 OR = 36 OR = 50 OR = 51 OR = 53 OR = 37 OR = 66 OR = 102 OR = 500 OR = 502 OR = 504 OR = 506 OR = 508 OR = 516 OR = 518
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(51) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(53) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(37) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(66) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(102) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(500) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(502) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(504) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(506) 
                    || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(508) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(516) || pnd_Temp_Record_Out_Pnd_Temp_Record_Trans.equals(518)))
                {
                    //*  IF NOT THE LAST RECORD
                    //*  SKIP AND MARK THE REST AS DUPS
                    if (condition(pnd_K.less(pnd_End)))                                                                                                                   //Natural: IF #K LT #END
                    {
                        pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_K.add(1));                                                                                 //Natural: ASSIGN #J := #K + 1
                        pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_J,":",pnd_End).setValue("Y");                                                                       //Natural: ASSIGN #HOLD-SKIP-IND ( #J:#END ) := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    if (true) break F0;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F0. )
                    //*  MARK AS DUP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_K).setValue("Y");                                                                                       //Natural: ASSIGN #HOLD-SKIP-IND ( #K ) := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                //*  SKIP AND MARK THE REST AS DUPS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_K.add(1));                                                                                         //Natural: ASSIGN #J := #K + 1
                pnd_Hold_Table_Pnd_Hold_Skip_Ind.getValue(pnd_J,":",pnd_End).setValue("Y");                                                                               //Natural: ASSIGN #HOLD-SKIP-IND ( #J:#END ) := 'Y'
                if (true) break F0;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F0. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Start.reset();                                                                                                                                                //Natural: RESET #START #END #P-IND
        pnd_End.reset();
        pnd_P_Ind.reset();
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133");
        Global.format(2, "PS=0 LS=133");
        Global.format(3, "PS=0 LS=133");
    }
}
