/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:51 PM
**        * FROM NATURAL PROGRAM : Iaap3027
************************************************************
**        * FILE NAME            : Iaap3027.java
**        * CLASS NAME           : Iaap3027
**        * INSTANCE NAME        : Iaap3027
************************************************************
************************************************************************
*  PROGRAM NAME  :  IAAP3027                                           *
*  DESCRIPTION   :  NET CHANGE DATA EXTRACTION                         *
*  DATE          :  FEB 1998                                           *
*  DESCRIPTION   :  PROCESS GRADED FILE. INPUT WORKFILES ARE SORTED    *
*                   BY CONTRACT-NUM AND PAYEE-CODE. FOR FUND CODE 'TG' *
*                   A CERTAIN PERCENTAGE IS CALCULATED AND PASS TO THE *
*                   NET CHANGE RECORD.                                 *
*                   ACCEPTS INPUT PARAMETER AND CHECKED FOR JANUARY RUN*
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* ---------+---------+----------------------------------------------
* 09/17/98 |         | INCLUDE PAYEE CODE IN SEARCHING FOR A CORRES-
*          |         | PONDING RECORD IN THE GRADED FILE
* 07/13/00 | JHH     | PA SELECT/SPIA - NEW FIELDS ON IAAA323
* 04/21/17 | SAI K   | RESTOW FOR PIN EXPANSION
* ______________________________________________
*********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3027 extends BLNatBase
{
    // Data Areas
    private PdaIaaa323 pdaIaaa323;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Graded_File;
    private DbsField pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee;

    private DbsGroup pnd_Graded_File__R_Field_1;
    private DbsField pnd_Graded_File_Pnd_Grd_Cntrct_Num;
    private DbsField pnd_Graded_File_Pnd_Grd_Payee_Code;
    private DbsField pnd_Graded_File_Pnd_Grd_Compny_Code;
    private DbsField pnd_Graded_File_Pnd_Grd_Fund_Code;
    private DbsField pnd_Graded_File_Pnd_Grd_Gross_Prev_Amt;
    private DbsField pnd_Graded_File_Pnd_Grd_Gross_Curr_Amt;
    private DbsField pnd_Graded_File_Pnd_Grd_Gross_Nxt_Yr_Amt;
    private DbsField pnd_Ctrl_Record_Month;

    private DbsGroup pnd_Ws_Other_Variables;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Date;

    private DbsGroup pnd_Ws_Other_Variables__R_Field_2;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Yyyy;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Dd;
    private DbsField pnd_Ws_Other_Variables_Pnd_Match;
    private DbsField pnd_Ws_Other_Variables_Pnd_Idx;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Graded;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee;

    private DbsGroup pnd_Ws_Other_Variables__R_Field_3;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Num;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Payee_Cde;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent;

    private DbsGroup pnd_Ws_Other_Variables__R_Field_4;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Int;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Dec;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent;
    private DbsField pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent;
    private DbsField pnd_Ws_Other_Variables_Pnd_Hi_Values;
    private DbsField pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa323 = new PdaIaaa323(localVariables);

        // Local Variables

        pnd_Graded_File = localVariables.newGroupInRecord("pnd_Graded_File", "#GRADED-FILE");
        pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee", "#GRD-CNTRCT-NUM-PAYEE", 
            FieldType.STRING, 12);

        pnd_Graded_File__R_Field_1 = pnd_Graded_File.newGroupInGroup("pnd_Graded_File__R_Field_1", "REDEFINE", pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee);
        pnd_Graded_File_Pnd_Grd_Cntrct_Num = pnd_Graded_File__R_Field_1.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Cntrct_Num", "#GRD-CNTRCT-NUM", FieldType.STRING, 
            10);
        pnd_Graded_File_Pnd_Grd_Payee_Code = pnd_Graded_File__R_Field_1.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Payee_Code", "#GRD-PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_Graded_File_Pnd_Grd_Compny_Code = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Compny_Code", "#GRD-COMPNY-CODE", FieldType.STRING, 
            1);
        pnd_Graded_File_Pnd_Grd_Fund_Code = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Fund_Code", "#GRD-FUND-CODE", FieldType.STRING, 2);
        pnd_Graded_File_Pnd_Grd_Gross_Prev_Amt = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Gross_Prev_Amt", "#GRD-GROSS-PREV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Graded_File_Pnd_Grd_Gross_Curr_Amt = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Gross_Curr_Amt", "#GRD-GROSS-CURR-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Graded_File_Pnd_Grd_Gross_Nxt_Yr_Amt = pnd_Graded_File.newFieldInGroup("pnd_Graded_File_Pnd_Grd_Gross_Nxt_Yr_Amt", "#GRD-GROSS-NXT-YR-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ctrl_Record_Month = localVariables.newFieldInRecord("pnd_Ctrl_Record_Month", "#CTRL-RECORD-MONTH", FieldType.STRING, 1);

        pnd_Ws_Other_Variables = localVariables.newGroupInRecord("pnd_Ws_Other_Variables", "#WS-OTHER-VARIABLES");
        pnd_Ws_Other_Variables_Pnd_Ws_Date = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 
            8);

        pnd_Ws_Other_Variables__R_Field_2 = pnd_Ws_Other_Variables.newGroupInGroup("pnd_Ws_Other_Variables__R_Field_2", "REDEFINE", pnd_Ws_Other_Variables_Pnd_Ws_Date);
        pnd_Ws_Other_Variables_Pnd_Ws_Yyyy = pnd_Ws_Other_Variables__R_Field_2.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Yyyy", "#WS-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Ws_Other_Variables_Pnd_Ws_Mm = pnd_Ws_Other_Variables__R_Field_2.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Mm", "#WS-MM", FieldType.NUMERIC, 
            2);
        pnd_Ws_Other_Variables_Pnd_Ws_Dd = pnd_Ws_Other_Variables__R_Field_2.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Dd", "#WS-DD", FieldType.NUMERIC, 
            2);
        pnd_Ws_Other_Variables_Pnd_Match = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Match", "#MATCH", FieldType.NUMERIC, 1);
        pnd_Ws_Other_Variables_Pnd_Idx = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Idx", "#IDX", FieldType.NUMERIC, 2);
        pnd_Ws_Other_Variables_Pnd_Ws_Graded = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Graded", "#WS-GRADED", FieldType.NUMERIC, 
            1);
        pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind", "#WS-GRADED-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee", "#WS-CNTRCT-PAYEE", 
            FieldType.STRING, 12);

        pnd_Ws_Other_Variables__R_Field_3 = pnd_Ws_Other_Variables.newGroupInGroup("pnd_Ws_Other_Variables__R_Field_3", "REDEFINE", pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee);
        pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Num = pnd_Ws_Other_Variables__R_Field_3.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Num", "#WS-CNTRCT-NUM", 
            FieldType.STRING, 10);
        pnd_Ws_Other_Variables_Pnd_Ws_Payee_Cde = pnd_Ws_Other_Variables__R_Field_3.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Payee_Cde", "#WS-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent", "#WS-COMPUTE-PERCENT", 
            FieldType.NUMERIC, 8, 5);

        pnd_Ws_Other_Variables__R_Field_4 = pnd_Ws_Other_Variables.newGroupInGroup("pnd_Ws_Other_Variables__R_Field_4", "REDEFINE", pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent);
        pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Int = pnd_Ws_Other_Variables__R_Field_4.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Int", 
            "#WS-COMPUTE-PERCENT-INT", FieldType.NUMERIC, 3);
        pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Dec = pnd_Ws_Other_Variables__R_Field_4.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Dec", 
            "#WS-COMPUTE-PERCENT-DEC", FieldType.NUMERIC, 5, 5);
        pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent", "#WS-PRIOR-PERCENT", 
            FieldType.NUMERIC, 5, 3);
        pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent", "#WS-CURR-PERCENT", 
            FieldType.NUMERIC, 5, 3);
        pnd_Ws_Other_Variables_Pnd_Hi_Values = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Hi_Values", "#HI-VALUES", FieldType.STRING, 
            1);
        pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd = pnd_Ws_Other_Variables.newFieldInGroup("pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd", "#HI-VALUES-GRD", 
            FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent.setInitialValue(0);
        pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent.setInitialValue(0);
        pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent.setInitialValue(0);
        pnd_Ws_Other_Variables_Pnd_Hi_Values.setInitialValue("H'99'");
        pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3027() throws Exception
    {
        super("Iaap3027");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap3027|Main");
        while(true)
        {
            try
            {
                //* ***********************************************************************
                //*           >>>>>     PROCEDURE DIVISION STARTS HERE     <<<<<          *
                //* ***********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ctrl_Record_Month);                                                                                //Natural: INPUT #CTRL-RECORD-MONTH
                //*  ROXAN 12-18-02
                pnd_Ws_Other_Variables_Pnd_Ws_Date.setValue(Global.getDATN());                                                                                            //Natural: MOVE *DATN TO #WS-DATE
                if (condition(pnd_Ctrl_Record_Month.equals("0") || pnd_Ctrl_Record_Month.equals("1")))                                                                    //Natural: IF #CTRL-RECORD-MONTH = '0' OR = '1'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "Valid values are '0' and '1' only ");                                                                                          //Natural: WRITE 'Valid values are "0" and "1" only '
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------------------------------
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 IAAA320
                while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320())))
                {
                    pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind.setValue(" ");                                                                                           //Natural: MOVE ' ' TO #WS-GRADED-CHG-IND
                    pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Num.setValue(pdaIaaa323.getIaaa320_Contract_Nbr());                                                              //Natural: MOVE IAAA320.CONTRACT-NBR TO #WS-CNTRCT-NUM
                    pnd_Ws_Other_Variables_Pnd_Ws_Payee_Cde.setValue(pdaIaaa323.getIaaa320_Payee_Cde());                                                                  //Natural: MOVE IAAA320.PAYEE-CDE TO #WS-PAYEE-CDE
                    pnd_Ws_Other_Variables_Pnd_Match.setValue(1);                                                                                                         //Natural: MOVE 1 TO #MATCH
                    //*  ALL TYPES OF RUN
                    pnd_Ws_Other_Variables_Pnd_Ws_Graded.setValue(0);                                                                                                     //Natural: MOVE 0 TO #WS-GRADED
                    //*    >>>  ALL RECORDS WILL BE REWRITTEN TO THE NETCHANGE FILE <<<    *
                    FOR01:                                                                                                                                                //Natural: FOR #IDX 1 FUND-CNT
                    for (pnd_Ws_Other_Variables_Pnd_Idx.setValue(1); condition(pnd_Ws_Other_Variables_Pnd_Idx.lessOrEqual(pdaIaaa323.getIaaa320_Fund_Cnt())); 
                        pnd_Ws_Other_Variables_Pnd_Idx.nadd(1))
                    {
                        if (condition(pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Ws_Other_Variables_Pnd_Idx).equals("TG")))                                  //Natural: IF FUND-ACCOUNT-CDE-A ( #IDX ) = 'TG'
                        {
                            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Ws_Other_Variables_Pnd_Idx).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Ws_Other_Variables_Pnd_Idx)))) //Natural: IF FUND-PRIOR-CONTRACT-AMT ( #IDX ) NE FUND-CURR-CONTRACT-AMT ( #IDX )
                            {
                                pnd_Ws_Other_Variables_Pnd_Ws_Graded.setValue(1);                                                                                         //Natural: MOVE 1 TO #WS-GRADED
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent.setValue(0);                                                                                              //Natural: MOVE 0 TO #WS-PRIOR-PERCENT #WS-CURR-PERCENT #WS-GRADED-CHG-IND
                    pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent.setValue(0);
                    pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind.setValue(0);
                    //*  WITH TG & PAYMENTS CHANGED
                    if (condition(pnd_Ws_Other_Variables_Pnd_Ws_Graded.equals(1) && pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd.equals(getZero())))                          //Natural: IF #WS-GRADED = 1 AND #HI-VALUES-GRD = 0
                    {
                        //*  PAYMENTS CHANGED IND
                        pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind.setValue("1");                                                                                       //Natural: MOVE '1' TO #WS-GRADED-CHG-IND
                        //* *  IF #CTRL-RECORD-MONTH EQ '1'
                        if (condition(pnd_Ws_Other_Variables_Pnd_Ws_Mm.equals(12)))                                                                                       //Natural: IF #WS-MM = 12
                        {
                                                                                                                                                                          //Natural: PERFORM CHECK-GRADED-FILE
                            sub_Check_Graded_File();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  JAN RUN AND WITH MATCH
                            if (condition(pnd_Ws_Other_Variables_Pnd_Match.equals(getZero())))                                                                            //Natural: IF #MATCH = 0
                            {
                                pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent.compute(new ComputeParameters(true, pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent),         //Natural: COMPUTE ROUNDED #WS-COMPUTE-PERCENT = ( #GRD-GROSS-CURR-AMT / #GRD-GROSS-PREV-AMT )
                                    (pnd_Graded_File_Pnd_Grd_Gross_Curr_Amt.divide(pnd_Graded_File_Pnd_Grd_Gross_Prev_Amt)));
                                pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent.compute(new ComputeParameters(false, pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent),            //Natural: ASSIGN #WS-PRIOR-PERCENT := #WS-COMPUTE-PERCENT-DEC * 100
                                    pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Dec.multiply(100));
                                pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent.compute(new ComputeParameters(true, pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent),         //Natural: COMPUTE ROUNDED #WS-COMPUTE-PERCENT = ( #GRD-GROSS-NXT-YR-AMT / #GRD-GROSS-CURR-AMT )
                                    (pnd_Graded_File_Pnd_Grd_Gross_Nxt_Yr_Amt.divide(pnd_Graded_File_Pnd_Grd_Gross_Curr_Amt)));
                                pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent.compute(new ComputeParameters(false, pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent),              //Natural: ASSIGN #WS-CURR-PERCENT := #WS-COMPUTE-PERCENT-DEC * 100
                                    pnd_Ws_Other_Variables_Pnd_Ws_Compute_Percent_Dec.multiply(100));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(3, false, pdaIaaa323.getIaaa320(), pnd_Ws_Other_Variables_Pnd_Ws_Prior_Percent, pnd_Ws_Other_Variables_Pnd_Ws_Curr_Percent,      //Natural: WRITE WORK FILE 3 IAAA320 #WS-PRIOR-PERCENT #WS-CURR-PERCENT #WS-GRADED-CHG-IND
                        pnd_Ws_Other_Variables_Pnd_Ws_Graded_Chg_Ind);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-GRADED-FILE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-GRADED
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_Graded_File() throws Exception                                                                                                                 //Natural: CHECK-GRADED-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ws_Other_Variables_Pnd_Match.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #MATCH
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd.equals(1)))                                                                                            //Natural: IF #HI-VALUES-GRD = 1
            {
                //*  GRADED FILE EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee.equals(pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee)))                                                   //Natural: IF #WS-CNTRCT-PAYEE = #GRD-CNTRCT-NUM-PAYEE
            {
                pnd_Ws_Other_Variables_Pnd_Match.setValue(0);                                                                                                             //Natural: MOVE 0 TO #MATCH
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee.less(pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee)))                                                     //Natural: IF #WS-CNTRCT-PAYEE < #GRD-CNTRCT-NUM-PAYEE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Other_Variables_Pnd_Ws_Cntrct_Payee.greater(pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee)))                                                  //Natural: IF #WS-CNTRCT-PAYEE > #GRD-CNTRCT-NUM-PAYEE
            {
                                                                                                                                                                          //Natural: PERFORM READ-GRADED
                sub_Read_Graded();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Read_Graded() throws Exception                                                                                                                       //Natural: READ-GRADED
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  JH TEMPORARY!!!
        getWorkFiles().read(2, pnd_Graded_File);                                                                                                                          //Natural: READ WORK FILE 2 ONCE #GRADED-FILE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Graded_File_Pnd_Grd_Cntrct_Num_Payee.setValue(pnd_Ws_Other_Variables_Pnd_Hi_Values);                                                                      //Natural: MOVE #HI-VALUES TO #GRD-CNTRCT-NUM-PAYEE
            pnd_Ws_Other_Variables_Pnd_Hi_Values_Grd.setValue(1);                                                                                                         //Natural: MOVE 1 TO #HI-VALUES-GRD
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }

    //
}
