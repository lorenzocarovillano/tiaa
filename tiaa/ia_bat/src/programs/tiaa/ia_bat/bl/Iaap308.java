/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:14 PM
**        * FROM NATURAL PROGRAM : Iaap308
************************************************************
**        * FILE NAME            : Iaap308.java
**        * CLASS NAME           : Iaap308
**        * INSTANCE NAME        : Iaap308
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP308      PROGRAM CREATES P/I FINAL-PAYMENTS   *
*                               COMING DUE REPORT P/I WARNING REPORT *
*      DATE    -   03/01                                             *
*                                                                    *
*   HISTORY                                                          *
*   9/12       - EXPANDED UNITS-CNT FROM 4.3 TO 6.3                  *
*  05/17  OS PIN EXPANSION CHANGES MARKED 082017.                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap308 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_250;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Check_Dte;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Check_Dte1;
    private DbsField pnd_Input_Pnd_Check_Dte2;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_1st_Pymnt_Due_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_1st_Pymnt_Pd_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_Cntrct_Inst_Iss_Cde;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_Cpr_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Cpr_Status_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Filler;
    private DbsField pnd_Input_Pnd_Cpr_Mode;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Cpr_Fin_Per_Date;
    private DbsField pnd_Input_Pnd_Cpr_Fin_Pay_Date;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Input__Filler14;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_51;

    private DbsGroup pnd_Name;
    private DbsField pnd_Name_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee;

    private DbsGroup pnd_Name__R_Field_8;
    private DbsField pnd_Name_Cntrct_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name__R_Field_9;
    private DbsField pnd_Name_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Addrss_Lne_1_K;
    private DbsField pnd_Name_Addrss_Lne_2_K;
    private DbsField pnd_Name_Addrss_Lne_3_K;
    private DbsField pnd_Name_Addrss_Lne_4_K;
    private DbsField pnd_Name_Addrss_Lne_5_K;
    private DbsField pnd_Name_Addrss_Lne_6_K;
    private DbsField pnd_Name_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name__R_Field_10;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_K;

    private DbsGroup pnd_Name__R_Field_11;
    private DbsField pnd_Name_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_Eft_Status_Ind_K;
    private DbsField pnd_Name_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name__R_Field_12;
    private DbsField pnd_Name_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Addrss_Lne_1_R;
    private DbsField pnd_Name_Addrss_Lne_2_R;
    private DbsField pnd_Name_Addrss_Lne_3_R;
    private DbsField pnd_Name_Addrss_Lne_4_R;
    private DbsField pnd_Name_Addrss_Lne_5_R;
    private DbsField pnd_Name_Addrss_Lne_6_R;
    private DbsField pnd_Name_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name__R_Field_13;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_R;

    private DbsGroup pnd_Name__R_Field_14;
    private DbsField pnd_Name_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_Eft_Status_Ind_R;
    private DbsField pnd_Name_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name__R_Field_15;
    private DbsField pnd_Name_V_Cntrct_Name_Free_K;
    private DbsField pnd_Name_V_Addrss_Lne_1_K;
    private DbsField pnd_Name_V_Addrss_Lne_2_K;
    private DbsField pnd_Name_V_Addrss_Lne_3_K;
    private DbsField pnd_Name_V_Addrss_Lne_4_K;
    private DbsField pnd_Name_V_Addrss_Lne_5_K;
    private DbsField pnd_Name_V_Addrss_Lne_6_K;
    private DbsField pnd_Name_V_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name__R_Field_16;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_K;

    private DbsGroup pnd_Name__R_Field_17;
    private DbsField pnd_Name_V_Addrss_Type_Cde_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_V_Eft_Status_Ind_K;
    private DbsField pnd_Name_V_Checking_Saving_Cde_K;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_V_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Pnd_V_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name__R_Field_18;
    private DbsField pnd_Name_V_Cntrct_Name_Free_R;
    private DbsField pnd_Name_V_Addrss_Lne_1_R;
    private DbsField pnd_Name_V_Addrss_Lne_2_R;
    private DbsField pnd_Name_V_Addrss_Lne_3_R;
    private DbsField pnd_Name_V_Addrss_Lne_4_R;
    private DbsField pnd_Name_V_Addrss_Lne_5_R;
    private DbsField pnd_Name_V_Addrss_Lne_6_R;
    private DbsField pnd_Name_V_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name__R_Field_19;
    private DbsField pnd_Name_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_V_Rest_Of_Record_R;

    private DbsGroup pnd_Name__R_Field_20;
    private DbsField pnd_Name_V_Addrss_Type_Cde_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_V_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_V_Eft_Status_Ind_R;
    private DbsField pnd_Name_V_Checking_Saving_Cde_R;
    private DbsField pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Name_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Name_Global_Indicator;
    private DbsField pnd_Name_Global_Country;
    private DbsField pnd_Name_Legal_State;

    private DbsGroup pnd_Extr_Rec_Out;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cont_No;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_21;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_22;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_23;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Cc;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Yy;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Mm;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_24;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Cc;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Yy;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Mm;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_25;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Cc;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Yy;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_26;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Ccyy;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_27;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Dd;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_28;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyy;

    private DbsGroup pnd_Extr_Rec_Out__R_Field_29;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Cc;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Yy;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Mm;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Name;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr1;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr2;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr3;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr4;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr5;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Adr6;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Zip;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind;
    private DbsField pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_30;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Bypass;
    private DbsField pnd_Name_Eof;
    private DbsField pnd_New_Payee;
    private DbsField pnd_Log_Dte;
    private DbsField pnd_Extr_Clg_Cde;
    private DbsField pnd_Tiaa_Cref_Fld;
    private DbsField pnd_Prt_Sex_Cde;
    private DbsField pnd_Extr_Status_Cde;
    private DbsField pnd_Parm_Check_Dte_A;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_31;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_32;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Yyyy;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_33;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Cc;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Yy;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Mm;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_P_Dd;

    private DbsGroup pnd_Parm_Check_Dte_A__R_Field_34;
    private DbsField pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm;
    private DbsField pnd_Sve_Check_Dte_A;

    private DbsGroup pnd_Sve_Check_Dte_A__R_Field_35;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8;

    private DbsGroup pnd_Sve_Check_Dte_A__R_Field_36;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Sve_Check_Dte_A__R_Field_37;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Mm;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Sve_Check_Dte_A__R_Field_38;
    private DbsField pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte;
    private DbsField pnd_Print_Dte;

    private DbsGroup pnd_Print_Dte__R_Field_39;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Mm;
    private DbsField pnd_Print_Dte_Pnd_Flr1;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Dd;
    private DbsField pnd_Print_Dte_Pnd_Flr2;
    private DbsField pnd_Print_Dte_Pnd_Prt_Dte_Yy;
    private DbsField pnd_Ctr_Clg;
    private DbsField pnd_Ctr_Sort;
    private DbsField pnd_Ctr_Read;
    private DbsField pnd_Ctr_Extr;
    private DbsField pnd_Name_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Bypass_Status;
    private DbsField pnd_Bypass_Payee;
    private DbsField pnd_Tot_Fin_Pymnt;
    private DbsField pnd_Tot_Recs;
    private DbsField pnd_Tot_Curr_Mnth;
    private DbsField pnd_Tot_1_Mnth_Left;
    private DbsField pnd_Tot_2_Mnth_Left;
    private DbsField pnd_Tot_3_Mnth_Left;
    private DbsField pnd_Tot_4_Mnth_Left;
    private DbsField pnd_Tot_5_Mnth_Left;
    private DbsField pnd_Tot_6_Mnth_Left;
    private DbsField pnd_Wrk_Ck_Date;

    private DbsGroup pnd_Wrk_Ck_Date__R_Field_40;
    private DbsField pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Ccyy;
    private DbsField pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm;
    private DbsField pnd_Wrk_Ck_Date_6_Mnths;
    private DbsField pnd_Wrk_Ck_Date_5_Mnths;
    private DbsField pnd_Wrk_Ck_Date_4_Mnths;
    private DbsField pnd_Wrk_Ck_Date_3_Mnths;
    private DbsField pnd_Wrk_Ck_Date_2_Mnths;
    private DbsField pnd_Wrk_Ck_Date_1_Mnths;
    private DbsField pnd_Sel_Opt_22;
    private DbsField pnd_Hdr_Mnth_Nme;
    private DbsField pnd_Hdr_Exp_Year;
    private DbsField pnd_Hdr_Exp_Line;
    private DbsField pnd_Hdr_Desc;
    private DbsField pnd_Prt_Issue_Dte;
    private DbsField pnd_Prt_1st_Due_Dte;
    private DbsField pnd_Prt_Fin_Per_Dte;
    private DbsField pnd_Prt_Fin_Pmt_Dte;
    private DbsField pnd_First_Time;
    private DbsField pnd_Sve_Rpt_Brk;
    private DbsField mnth_Nme_Table;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_250 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_250", "#REST-OF-RECORD-250", FieldType.STRING, 250);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Check_Dte = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_2.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Check_Dte);
        pnd_Input_Pnd_Check_Dte1 = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Check_Dte1", "#CHECK-DTE1", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Check_Dte2 = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Check_Dte2", "#CHECK-DTE2", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Cntrct_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input__Filler1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 4);
        pnd_Input_Pnd_Cntrct_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Cntrct_1st_Pymnt_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_1st_Pymnt_Due_Dte", "#CNTRCT-1ST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cntrct_1st_Pymnt_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_1st_Pymnt_Pd_Dte", "#CNTRCT-1ST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cntrct_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input__Filler2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 24);
        pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Dob_Dte", "#CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input__Filler3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 4);
        pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Sex_Cde", "#CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_First_Annt_Dod_Dte", "#CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 9);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Dob_Dte", "#CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input__Filler6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 4);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Sex_Cde", "#CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Scnd_Annt_Dod_Dte", "#CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 16);
        pnd_Input_Pnd_Cntrct_Inst_Iss_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Inst_Iss_Cde", "#CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input__Filler8 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 26);
        pnd_Input_Pnd_Cpr_Tax_Id_Nbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Tax_Id_Nbr", "#CPR-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler9 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 1);
        pnd_Input_Pnd_Cpr_Status_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Status_Cde", "#CPR-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Cpr_Filler = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Filler", "#CPR-FILLER", FieldType.STRING, 160);
        pnd_Input_Pnd_Cpr_Mode = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Mode", "#CPR-MODE", FieldType.NUMERIC, 3);
        pnd_Input__Filler10 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 6);
        pnd_Input_Pnd_Cpr_Fin_Per_Date = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Fin_Per_Date", "#CPR-FIN-PER-DATE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Cpr_Fin_Pay_Date = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cpr_Fin_Pay_Date", "#CPR-FIN-PAY-DATE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_6 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input__Filler11 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 23);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 5);
        pnd_Input__Filler12 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 16);
        pnd_Input_Pnd_Ddctn_Strt_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_7 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_250);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input__Filler13 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 10);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler14 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input__Filler14", "_FILLER14", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Rest_Of_Record_51 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Rest_Of_Record_51", "#REST-OF-RECORD-51", FieldType.STRING, 51);

        pnd_Name = localVariables.newGroupInRecord("pnd_Name", "#NAME");
        pnd_Name_Ph_Unque_Id_Nmbr = pnd_Name.newFieldInGroup("pnd_Name_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Cntrct_Payee = pnd_Name.newFieldInGroup("pnd_Name_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name__R_Field_8 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_8", "REDEFINE", pnd_Name_Cntrct_Payee);
        pnd_Name_Cntrct_Nmbr = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Cntrct_Payee_Cde = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Name_Pnd_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);

        pnd_Name__R_Field_9 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_9", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Cntrct_Name_Free_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_K = pnd_Name__R_Field_9.newFieldInGroup("pnd_Name_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);

        pnd_Name__R_Field_10 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_10", "REDEFINE", pnd_Name_Addrss_Postal_Data_K);
        pnd_Name_Addrss_Zip_Plus_4_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_K = pnd_Name__R_Field_10.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 76);

        pnd_Name__R_Field_11 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_11", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_K);
        pnd_Name_Addrss_Type_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Mailing_Addrss_Ind_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_K = pnd_Name__R_Field_11.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 
            2);
        pnd_Name_Pnd_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);

        pnd_Name__R_Field_12 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_12", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Cntrct_Name_Free_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_R = pnd_Name__R_Field_12.newFieldInGroup("pnd_Name_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);

        pnd_Name__R_Field_13 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_13", "REDEFINE", pnd_Name_Addrss_Postal_Data_R);
        pnd_Name_Addrss_Zip_Plus_4_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_R = pnd_Name__R_Field_13.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name__R_Field_14 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_14", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_R);
        pnd_Name_Addrss_Type_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Correspondence_Addrss_Ind_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_R = pnd_Name__R_Field_14.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 
            2);
        pnd_Name_Pnd_V_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 245);

        pnd_Name__R_Field_15 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_15", "REDEFINE", pnd_Name_Pnd_V_Cntrct_Name_Add_K);
        pnd_Name_V_Cntrct_Name_Free_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_1_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_2_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_3_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_4_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_5_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_6_K = pnd_Name__R_Field_15.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);

        pnd_Name__R_Field_16 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_16", "REDEFINE", pnd_Name_V_Addrss_Postal_Data_K);
        pnd_Name_V_Addrss_Zip_Plus_4_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Name_V_Addrss_Carrier_Rte_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Walk_Rte_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", FieldType.STRING, 4);
        pnd_Name_V_Addrss_Usps_Future_Use_K = pnd_Name__R_Field_16.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 76);

        pnd_Name__R_Field_17 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_17", "REDEFINE", pnd_Name_Pnd_V_Rest_Of_Record_K);
        pnd_Name_V_Addrss_Type_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", FieldType.STRING, 1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 
            8);
        pnd_Name_V_Addrss_Last_Chnge_Time_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_V_Eft_Status_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", FieldType.STRING, 1);
        pnd_Name_V_Checking_Saving_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_V_Pending_Addrss_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Addrss_Chnge_Dte_K", "V-PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Addrss_Restore_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Addrss_Restore_Dte_K", "V-PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Pending_Perm_Addrss_Chnge_Dte_K", "V-PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Check_Mailing_Addrss_Ind_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Check_Mailing_Addrss_Ind_K", "V-CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_K = pnd_Name__R_Field_17.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 
            2);
        pnd_Name_Pnd_V_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 245);

        pnd_Name__R_Field_18 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_18", "REDEFINE", pnd_Name_Pnd_V_Cntrct_Name_Add_R);
        pnd_Name_V_Cntrct_Name_Free_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Name_V_Addrss_Lne_1_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_2_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_3_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_4_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_5_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Lne_6_R = pnd_Name__R_Field_18.newFieldInGroup("pnd_Name_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_V_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);

        pnd_Name__R_Field_19 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_19", "REDEFINE", pnd_Name_V_Addrss_Postal_Data_R);
        pnd_Name_V_Addrss_Zip_Plus_4_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Name_V_Addrss_Carrier_Rte_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_V_Addrss_Walk_Rte_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", FieldType.STRING, 4);
        pnd_Name_V_Addrss_Usps_Future_Use_R = pnd_Name__R_Field_19.newFieldInGroup("pnd_Name_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Pnd_V_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name__R_Field_20 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_20", "REDEFINE", pnd_Name_Pnd_V_Rest_Of_Record_R);
        pnd_Name_V_Addrss_Type_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", FieldType.STRING, 1);
        pnd_Name_V_Addrss_Last_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 
            8);
        pnd_Name_V_Addrss_Last_Chnge_Time_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_V_Permanent_Addrss_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_V_Bank_Aba_Acct_Nmbr_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_V_Eft_Status_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", FieldType.STRING, 1);
        pnd_Name_V_Checking_Saving_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_V_Pending_Addrss_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Addrss_Chnge_Dte_R", "V-PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Addrss_Restore_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Addrss_Restore_Dte_R", "V-PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Pending_Perm_Add_Chnge_Dte_R", "V-PENDING-PERM-ADD-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_V_Correspondence_Addrss_Ind_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Correspondence_Addrss_Ind_R", "V-CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_V_Addrss_Geographic_Cde_R = pnd_Name__R_Field_20.newFieldInGroup("pnd_Name_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 
            2);
        pnd_Name_Global_Indicator = pnd_Name.newFieldInGroup("pnd_Name_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 1);
        pnd_Name_Global_Country = pnd_Name.newFieldInGroup("pnd_Name_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Name_Legal_State = pnd_Name.newFieldInGroup("pnd_Name_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        pnd_Extr_Rec_Out = localVariables.newGroupInRecord("pnd_Extr_Rec_Out", "#EXTR-REC-OUT");
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths", "#EXTR-NBR-MNTHS", FieldType.NUMERIC, 
            1);
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cont_No", "#EXTR-CONT-NO", FieldType.STRING, 12);

        pnd_Extr_Rec_Out__R_Field_21 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_21", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Cont_No);
        pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr = pnd_Extr_Rec_Out__R_Field_21.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr", "#EXTR-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde = pnd_Extr_Rec_Out__R_Field_21.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde", "#EXTR-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Extr_Rec_Out__R_Field_22 = pnd_Extr_Rec_Out__R_Field_21.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_22", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde);
        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A = pnd_Extr_Rec_Out__R_Field_22.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde_A", "#EXTR-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde", "#EXTR-CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte", "#EXTR-CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);

        pnd_Extr_Rec_Out__R_Field_23 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_23", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Cc = pnd_Extr_Rec_Out__R_Field_23.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Cc", "#EXTR-CNTRCT-ISSUE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Yy = pnd_Extr_Rec_Out__R_Field_23.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Yy", "#EXTR-CNTRCT-ISSUE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Mm = pnd_Extr_Rec_Out__R_Field_23.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Mm", "#EXTR-CNTRCT-ISSUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte", "#EXTR-CNTRCT-1ST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);

        pnd_Extr_Rec_Out__R_Field_24 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_24", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Cc = pnd_Extr_Rec_Out__R_Field_24.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Cc", 
            "#EXTR-CNTRCT-1ST-PYMNT-DUE-CC", FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Yy = pnd_Extr_Rec_Out__R_Field_24.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Yy", 
            "#EXTR-CNTRCT-1ST-PYMNT-DUE-YY", FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Mm = pnd_Extr_Rec_Out__R_Field_24.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Mm", 
            "#EXTR-CNTRCT-1ST-PYMNT-DUE-MM", FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date", "#EXTR-FIN-PER-DATE", FieldType.NUMERIC, 
            6);

        pnd_Extr_Rec_Out__R_Field_25 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_25", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Cc = pnd_Extr_Rec_Out__R_Field_25.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Cc", "#EXTR-FIN-PER-DATE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Yy = pnd_Extr_Rec_Out__R_Field_25.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Yy", "#EXTR-FIN-PER-DATE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm = pnd_Extr_Rec_Out__R_Field_25.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm", "#EXTR-FIN-PER-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Extr_Rec_Out__R_Field_26 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_26", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Ccyy = pnd_Extr_Rec_Out__R_Field_26.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Ccyy", "#EXTR-FIN-PER-DATE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date", "#EXTR-FIN-PAY-DATE", FieldType.NUMERIC, 
            8);

        pnd_Extr_Rec_Out__R_Field_27 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_27", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm = pnd_Extr_Rec_Out__R_Field_27.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm", 
            "#EXTR-FIN-PAY-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Dd = pnd_Extr_Rec_Out__R_Field_27.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Dd", "#EXTR-FIN-PAY-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Extr_Rec_Out__R_Field_28 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_28", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyy = pnd_Extr_Rec_Out__R_Field_28.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyy", "#EXTR-FIN-PAY-DATE-CCYY", 
            FieldType.NUMERIC, 4);

        pnd_Extr_Rec_Out__R_Field_29 = pnd_Extr_Rec_Out.newGroupInGroup("pnd_Extr_Rec_Out__R_Field_29", "REDEFINE", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Cc = pnd_Extr_Rec_Out__R_Field_29.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Cc", "#EXTR-FIN-PAY-DATE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Yy = pnd_Extr_Rec_Out__R_Field_29.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Yy", "#EXTR-FIN-PAY-DATE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Mm = pnd_Extr_Rec_Out__R_Field_29.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Mm", "#EXTR-FIN-PAY-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt", "#EXTR-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Extr_Rec_Out_Pnd_Extr_Name = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Name", "#EXTR-NAME", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr1 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr1", "#EXTR-ADR1", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr2 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr2", "#EXTR-ADR2", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr3 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr3", "#EXTR-ADR3", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr4 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr4", "#EXTR-ADR4", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr5 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr5", "#EXTR-ADR5", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Adr6 = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Adr6", "#EXTR-ADR6", FieldType.STRING, 35);
        pnd_Extr_Rec_Out_Pnd_Extr_Zip = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Zip", "#EXTR-ZIP", FieldType.STRING, 9);
        pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr", "#EXTR-SS-NBR", FieldType.NUMERIC, 9);
        pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind", "#EXTR-INST-IND", FieldType.STRING, 
            1);
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk = pnd_Extr_Rec_Out.newFieldInGroup("pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk", "#EXTR-RPT-BRK", FieldType.NUMERIC, 
            2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_30", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_30.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_30.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Name_Eof = localVariables.newFieldInRecord("pnd_Name_Eof", "#NAME-EOF", FieldType.BOOLEAN, 1);
        pnd_New_Payee = localVariables.newFieldInRecord("pnd_New_Payee", "#NEW-PAYEE", FieldType.BOOLEAN, 1);
        pnd_Log_Dte = localVariables.newFieldInRecord("pnd_Log_Dte", "#LOG-DTE", FieldType.STRING, 15);
        pnd_Extr_Clg_Cde = localVariables.newFieldInRecord("pnd_Extr_Clg_Cde", "#EXTR-CLG-CDE", FieldType.STRING, 5);
        pnd_Tiaa_Cref_Fld = localVariables.newFieldInRecord("pnd_Tiaa_Cref_Fld", "#TIAA-CREF-FLD", FieldType.STRING, 13);
        pnd_Prt_Sex_Cde = localVariables.newFieldInRecord("pnd_Prt_Sex_Cde", "#PRT-SEX-CDE", FieldType.STRING, 1);
        pnd_Extr_Status_Cde = localVariables.newFieldInRecord("pnd_Extr_Status_Cde", "#EXTR-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Parm_Check_Dte_A = localVariables.newFieldInRecord("pnd_Parm_Check_Dte_A", "#PARM-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Parm_Check_Dte_A__R_Field_31 = localVariables.newGroupInRecord("pnd_Parm_Check_Dte_A__R_Field_31", "REDEFINE", pnd_Parm_Check_Dte_A);
        pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte = pnd_Parm_Check_Dte_A__R_Field_31.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Parm_Check_Dte_A__R_Field_32 = pnd_Parm_Check_Dte_A__R_Field_31.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_32", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte);
        pnd_Parm_Check_Dte_A_Pnd_P_Yyyy = pnd_Parm_Check_Dte_A__R_Field_32.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Yyyy", "#P-YYYY", FieldType.NUMERIC, 
            4);

        pnd_Parm_Check_Dte_A__R_Field_33 = pnd_Parm_Check_Dte_A__R_Field_32.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_33", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_P_Yyyy);
        pnd_Parm_Check_Dte_A_Pnd_P_Cc = pnd_Parm_Check_Dte_A__R_Field_33.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Cc", "#P-CC", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Yy = pnd_Parm_Check_Dte_A__R_Field_33.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Yy", "#P-YY", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Mm = pnd_Parm_Check_Dte_A__R_Field_32.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Mm", "#P-MM", FieldType.NUMERIC, 
            2);
        pnd_Parm_Check_Dte_A_Pnd_P_Dd = pnd_Parm_Check_Dte_A__R_Field_32.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_P_Dd", "#P-DD", FieldType.NUMERIC, 
            2);

        pnd_Parm_Check_Dte_A__R_Field_34 = pnd_Parm_Check_Dte_A__R_Field_31.newGroupInGroup("pnd_Parm_Check_Dte_A__R_Field_34", "REDEFINE", pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte);
        pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm = pnd_Parm_Check_Dte_A__R_Field_34.newFieldInGroup("pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm", 
            "#PARM-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Sve_Check_Dte_A = localVariables.newFieldInRecord("pnd_Sve_Check_Dte_A", "#SVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Sve_Check_Dte_A__R_Field_35 = localVariables.newGroupInRecord("pnd_Sve_Check_Dte_A__R_Field_35", "REDEFINE", pnd_Sve_Check_Dte_A);
        pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8 = pnd_Sve_Check_Dte_A__R_Field_35.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8", "#SVE-CHECK-DTE-8", 
            FieldType.NUMERIC, 8);

        pnd_Sve_Check_Dte_A__R_Field_36 = pnd_Sve_Check_Dte_A__R_Field_35.newGroupInGroup("pnd_Sve_Check_Dte_A__R_Field_36", "REDEFINE", pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8);
        pnd_Sve_Check_Dte_A_Pnd_Yyyy = pnd_Sve_Check_Dte_A__R_Field_36.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Sve_Check_Dte_A__R_Field_37 = pnd_Sve_Check_Dte_A__R_Field_36.newGroupInGroup("pnd_Sve_Check_Dte_A__R_Field_37", "REDEFINE", pnd_Sve_Check_Dte_A_Pnd_Yyyy);
        pnd_Sve_Check_Dte_A_Pnd_Cc = pnd_Sve_Check_Dte_A__R_Field_37.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Sve_Check_Dte_A_Pnd_Yy = pnd_Sve_Check_Dte_A__R_Field_37.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Sve_Check_Dte_A_Pnd_Mm = pnd_Sve_Check_Dte_A__R_Field_36.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Sve_Check_Dte_A_Pnd_Dd = pnd_Sve_Check_Dte_A__R_Field_36.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Sve_Check_Dte_A__R_Field_38 = pnd_Sve_Check_Dte_A__R_Field_35.newGroupInGroup("pnd_Sve_Check_Dte_A__R_Field_38", "REDEFINE", pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8);
        pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte = pnd_Sve_Check_Dte_A__R_Field_38.newFieldInGroup("pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte", "#SVE-CHECK-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Print_Dte = localVariables.newFieldInRecord("pnd_Print_Dte", "#PRINT-DTE", FieldType.STRING, 8);

        pnd_Print_Dte__R_Field_39 = localVariables.newGroupInRecord("pnd_Print_Dte__R_Field_39", "REDEFINE", pnd_Print_Dte);
        pnd_Print_Dte_Pnd_Prt_Dte_Mm = pnd_Print_Dte__R_Field_39.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Mm", "#PRT-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Print_Dte_Pnd_Flr1 = pnd_Print_Dte__R_Field_39.newFieldInGroup("pnd_Print_Dte_Pnd_Flr1", "#FLR1", FieldType.STRING, 1);
        pnd_Print_Dte_Pnd_Prt_Dte_Dd = pnd_Print_Dte__R_Field_39.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Dd", "#PRT-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Print_Dte_Pnd_Flr2 = pnd_Print_Dte__R_Field_39.newFieldInGroup("pnd_Print_Dte_Pnd_Flr2", "#FLR2", FieldType.STRING, 1);
        pnd_Print_Dte_Pnd_Prt_Dte_Yy = pnd_Print_Dte__R_Field_39.newFieldInGroup("pnd_Print_Dte_Pnd_Prt_Dte_Yy", "#PRT-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Ctr_Clg = localVariables.newFieldInRecord("pnd_Ctr_Clg", "#CTR-CLG", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Sort = localVariables.newFieldInRecord("pnd_Ctr_Sort", "#CTR-SORT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Read = localVariables.newFieldInRecord("pnd_Ctr_Read", "#CTR-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Extr = localVariables.newFieldInRecord("pnd_Ctr_Extr", "#CTR-EXTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Name_Ctr = localVariables.newFieldInRecord("pnd_Name_Ctr", "#NAME-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Status = localVariables.newFieldInRecord("pnd_Bypass_Status", "#BYPASS-STATUS", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Payee = localVariables.newFieldInRecord("pnd_Bypass_Payee", "#BYPASS-PAYEE", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Fin_Pymnt = localVariables.newFieldInRecord("pnd_Tot_Fin_Pymnt", "#TOT-FIN-PYMNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Recs = localVariables.newFieldInRecord("pnd_Tot_Recs", "#TOT-RECS", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_Curr_Mnth = localVariables.newFieldInRecord("pnd_Tot_Curr_Mnth", "#TOT-CURR-MNTH", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_1_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_1_Mnth_Left", "#TOT-1-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_2_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_2_Mnth_Left", "#TOT-2-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_3_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_3_Mnth_Left", "#TOT-3-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_4_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_4_Mnth_Left", "#TOT-4-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_5_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_5_Mnth_Left", "#TOT-5-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tot_6_Mnth_Left = localVariables.newFieldInRecord("pnd_Tot_6_Mnth_Left", "#TOT-6-MNTH-LEFT", FieldType.PACKED_DECIMAL, 5);
        pnd_Wrk_Ck_Date = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date", "#WRK-CK-DATE", FieldType.NUMERIC, 6);

        pnd_Wrk_Ck_Date__R_Field_40 = localVariables.newGroupInRecord("pnd_Wrk_Ck_Date__R_Field_40", "REDEFINE", pnd_Wrk_Ck_Date);
        pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Ccyy = pnd_Wrk_Ck_Date__R_Field_40.newFieldInGroup("pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Ccyy", "#WRK-CK-DTE-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm = pnd_Wrk_Ck_Date__R_Field_40.newFieldInGroup("pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm", "#WRK-CK-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Wrk_Ck_Date_6_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_6_Mnths", "#WRK-CK-DATE-6-MNTHS", FieldType.NUMERIC, 6);
        pnd_Wrk_Ck_Date_5_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_5_Mnths", "#WRK-CK-DATE-5-MNTHS", FieldType.NUMERIC, 6);
        pnd_Wrk_Ck_Date_4_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_4_Mnths", "#WRK-CK-DATE-4-MNTHS", FieldType.NUMERIC, 6);
        pnd_Wrk_Ck_Date_3_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_3_Mnths", "#WRK-CK-DATE-3-MNTHS", FieldType.NUMERIC, 6);
        pnd_Wrk_Ck_Date_2_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_2_Mnths", "#WRK-CK-DATE-2-MNTHS", FieldType.NUMERIC, 6);
        pnd_Wrk_Ck_Date_1_Mnths = localVariables.newFieldInRecord("pnd_Wrk_Ck_Date_1_Mnths", "#WRK-CK-DATE-1-MNTHS", FieldType.NUMERIC, 6);
        pnd_Sel_Opt_22 = localVariables.newFieldInRecord("pnd_Sel_Opt_22", "#SEL-OPT-22", FieldType.STRING, 1);
        pnd_Hdr_Mnth_Nme = localVariables.newFieldInRecord("pnd_Hdr_Mnth_Nme", "#HDR-MNTH-NME", FieldType.STRING, 9);
        pnd_Hdr_Exp_Year = localVariables.newFieldInRecord("pnd_Hdr_Exp_Year", "#HDR-EXP-YEAR", FieldType.NUMERIC, 4);
        pnd_Hdr_Exp_Line = localVariables.newFieldInRecord("pnd_Hdr_Exp_Line", "#HDR-EXP-LINE", FieldType.STRING, 38);
        pnd_Hdr_Desc = localVariables.newFieldInRecord("pnd_Hdr_Desc", "#HDR-DESC", FieldType.STRING, 6);
        pnd_Prt_Issue_Dte = localVariables.newFieldInRecord("pnd_Prt_Issue_Dte", "#PRT-ISSUE-DTE", FieldType.STRING, 6);
        pnd_Prt_1st_Due_Dte = localVariables.newFieldInRecord("pnd_Prt_1st_Due_Dte", "#PRT-1ST-DUE-DTE", FieldType.STRING, 6);
        pnd_Prt_Fin_Per_Dte = localVariables.newFieldInRecord("pnd_Prt_Fin_Per_Dte", "#PRT-FIN-PER-DTE", FieldType.STRING, 8);
        pnd_Prt_Fin_Pmt_Dte = localVariables.newFieldInRecord("pnd_Prt_Fin_Pmt_Dte", "#PRT-FIN-PMT-DTE", FieldType.STRING, 10);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.STRING, 1);
        pnd_Sve_Rpt_Brk = localVariables.newFieldInRecord("pnd_Sve_Rpt_Brk", "#SVE-RPT-BRK", FieldType.NUMERIC, 2);
        mnth_Nme_Table = localVariables.newFieldArrayInRecord("mnth_Nme_Table", "MNTH-NME-TABLE", FieldType.STRING, 9, new DbsArrayController(1, 12));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Tot_Recs.setInitialValue(0);
        pnd_Tot_Curr_Mnth.setInitialValue(0);
        pnd_Tot_1_Mnth_Left.setInitialValue(0);
        pnd_Tot_2_Mnth_Left.setInitialValue(0);
        pnd_Tot_3_Mnth_Left.setInitialValue(0);
        pnd_Tot_4_Mnth_Left.setInitialValue(0);
        pnd_Tot_5_Mnth_Left.setInitialValue(0);
        pnd_Tot_6_Mnth_Left.setInitialValue(0);
        pnd_First_Time.setInitialValue("Y");
        mnth_Nme_Table.getValue(1).setInitialValue(" JANUARY ");
        mnth_Nme_Table.getValue(2).setInitialValue(" FEBRUARY");
        mnth_Nme_Table.getValue(3).setInitialValue("  MARCH  ");
        mnth_Nme_Table.getValue(4).setInitialValue("  APRIL  ");
        mnth_Nme_Table.getValue(5).setInitialValue("   MAY   ");
        mnth_Nme_Table.getValue(6).setInitialValue("   JUNE  ");
        mnth_Nme_Table.getValue(7).setInitialValue("   JULY  ");
        mnth_Nme_Table.getValue(8).setInitialValue("  AUGUST ");
        mnth_Nme_Table.getValue(9).setInitialValue("SEPTEMBER");
        mnth_Nme_Table.getValue(10).setInitialValue(" OCTOBER ");
        mnth_Nme_Table.getValue(11).setInitialValue(" NOVEMBER");
        mnth_Nme_Table.getValue(12).setInitialValue(" DECEMBER");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap308() throws Exception
    {
        super("Iaap308");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap308|Main");
        OnErrorManager.pushEvent("IAAP308", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 56 ZP = OFF SG = OFF
                //*  END OF ADD FOR P/I REPORT 3/01                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Parm_Check_Dte_A);                                                                                 //Natural: INPUT #PARM-CHECK-DTE-A
                READWORK01:                                                                                                                                               //Natural: READ WORK 1 #INPUT
                while (condition(getWorkFiles().read(1, pnd_Input)))
                {
                    pnd_Ctr_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTR-READ
                    if (condition(pnd_Input_Pnd_Record_Code.equals(0)))                                                                                                   //Natural: IF #RECORD-CODE = 00
                    {
                        if (condition(pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm.equals(getZero())))                                                                  //Natural: IF #PARM-CHECK-DTE-YYYYMM = 0
                        {
                            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                   //Natural: IF #PPCN-NBR = '   CHEADER'
                            {
                                pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.setValue(pnd_Input_Pnd_Check_Dte);                                                                //Natural: ASSIGN #SVE-CHECK-DTE-8 = #CHECK-DTE
                                                                                                                                                                          //Natural: PERFORM SET-UP-FIN-PMT-WRK-DTES
                                sub_Set_Up_Fin_Pmt_Wrk_Dtes();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER")  //Natural: IF #PPCN-NBR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                                    || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
                                {
                                    pnd_Sel_Opt_22.reset();                                                                                                               //Natural: RESET #SEL-OPT-22
                                    if (condition(true)) continue;                                                                                                        //Natural: ESCAPE TOP
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Sve_Check_Dte_A.setValue(pnd_Parm_Check_Dte_A);                                                                                           //Natural: ASSIGN #SVE-CHECK-DTE-A = #PARM-CHECK-DTE-A
                            pnd_Wrk_Ck_Date.setValue(pnd_Parm_Check_Dte_A_Pnd_Parm_Check_Dte_Yyyymm);                                                                     //Natural: ASSIGN #WRK-CK-DATE := #PARM-CHECK-DTE-YYYYMM
                                                                                                                                                                          //Natural: PERFORM SET-UP-FIN-PMT-WRK-DTES
                            sub_Set_Up_Fin_Pmt_Wrk_Dtes();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet411 = 0;                                                                                                                     //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
                    if (condition((pnd_Input_Pnd_Record_Code.equals(10))))
                    {
                        decideConditionsMet411++;
                        pnd_Bypass.reset();                                                                                                                               //Natural: RESET #BYPASS #SEL-OPT-22 #EXTR-CONT-NO #EXTR-PAYEE-CDE #EXTR-CNTRCT-OPTN-CDE #EXTR-CNTRCT-ISSUE-DTE #EXTR-CNTRCT-1ST-PYMNT-DUE-DTE #EXTR-FIN-PER-DATE #EXTR-FIN-PAY-DATE #EXTR-FIN-PYMNT
                        pnd_Sel_Opt_22.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date.reset();
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt.reset();
                        if (condition(pnd_Input_Pnd_Cntrct_Crrncy_Cde.equals(2) || pnd_Input_Pnd_Cntrct_Optn_Cde.notEquals(22)))                                          //Natural: IF #CNTRCT-CRRNCY-CDE = 2 OR #CNTRCT-OPTN-CDE NE 22
                        {
                            pnd_Bypass.setValue(true);                                                                                                                    //Natural: ASSIGN #BYPASS := TRUE
                            pnd_Bypass_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #BYPASS-CTR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Pnd_Cntrct_Optn_Cde.equals(22)))                                                                                      //Natural: IF #CNTRCT-OPTN-CDE = 22
                            {
                                pnd_Sel_Opt_22.setValue("Y");                                                                                                             //Natural: ASSIGN #SEL-OPT-22 := 'Y'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte.setValue(pnd_Input_Pnd_Cntrct_Issue_Dte);                                                              //Natural: ASSIGN #EXTR-CNTRCT-ISSUE-DTE := #INPUT.#CNTRCT-ISSUE-DTE
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte.setValue(pnd_Input_Pnd_Cntrct_1st_Pymnt_Due_Dte);                                              //Natural: ASSIGN #EXTR-CNTRCT-1ST-PYMNT-DUE-DTE := #INPUT.#CNTRCT-1ST-PYMNT-DUE-DTE
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde.setValue(pnd_Input_Pnd_Cntrct_Optn_Cde);                                                                //Natural: ASSIGN #EXTR-CNTRCT-OPTN-CDE := #INPUT.#CNTRCT-OPTN-CDE
                        pnd_Extr_Clg_Cde.setValue(pnd_Input_Pnd_Cntrct_Inst_Iss_Cde);                                                                                     //Natural: ASSIGN #EXTR-CLG-CDE := #INPUT.#CNTRCT-INST-ISS-CDE
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Input_Pnd_Cpr_Fin_Per_Date);                                                                  //Natural: ASSIGN #EXTR-FIN-PER-DATE := #INPUT.#CPR-FIN-PER-DATE
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date.setValue(pnd_Input_Pnd_Cpr_Fin_Pay_Date);                                                                  //Natural: ASSIGN #EXTR-FIN-PAY-DATE := #INPUT.#CPR-FIN-PAY-DATE
                    }                                                                                                                                                     //Natural: VALUE 20
                    else if (condition((pnd_Input_Pnd_Record_Code.equals(20))))
                    {
                        decideConditionsMet411++;
                        if (condition(pnd_Bypass.getBoolean()))                                                                                                           //Natural: IF #BYPASS
                        {
                            pnd_Bypass_Ctr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #BYPASS-CTR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Pnd_Cpr_Status_Cde.equals(9)))                                                                                        //Natural: IF #CPR-STATUS-CDE = 9
                            {
                                pnd_Bypass_Status.nadd(1);                                                                                                                //Natural: ADD 1 TO #BYPASS-STATUS
                                pnd_Sel_Opt_22.reset();                                                                                                                   //Natural: RESET #SEL-OPT-22
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                      //Natural: ASSIGN #EXTR-PPCN-NBR := #PPCN-NBR
                                pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde.setValue(pnd_Input_Pnd_Payee_Cde);                                                                    //Natural: ASSIGN #EXTR-PAYEE-CDE := #INPUT.#PAYEE-CDE
                                pnd_Extr_Status_Cde.setValue(pnd_Input_Pnd_Cpr_Status_Cde);                                                                               //Natural: ASSIGN #EXTR-STATUS-CDE := #INPUT.#CPR-STATUS-CDE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Input_Pnd_Cpr_Fin_Per_Date.equals(getZero()) || pnd_Input_Pnd_Cpr_Fin_Per_Date.equals(9) || pnd_Input_Pnd_Cpr_Fin_Pay_Date.equals(getZero())  //Natural: IF #CPR-FIN-PER-DATE = 0 OR = 9 OR #CPR-FIN-PAY-DATE = 0 OR = 9
                            || pnd_Input_Pnd_Cpr_Fin_Pay_Date.equals(9)))
                        {
                            pnd_Sel_Opt_22.reset();                                                                                                                       //Natural: RESET #SEL-OPT-22
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Sel_Opt_22.equals("Y")))                                                                                                        //Natural: IF #SEL-OPT-22 = 'Y'
                        {
                            pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Input_Pnd_Cpr_Fin_Per_Date);                                                              //Natural: ASSIGN #EXTR-FIN-PER-DATE := #CPR-FIN-PER-DATE
                            pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date.setValue(pnd_Input_Pnd_Cpr_Fin_Pay_Date);                                                              //Natural: ASSIGN #EXTR-FIN-PAY-DATE := #CPR-FIN-PAY-DATE
                            //*        ASSIGN #SVE-CHECK-DTE-8 = #CHECK-DTE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.less(pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte)))                                         //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM LT #SVE-CHECK-DTE
                        {
                            pnd_Sel_Opt_22.reset();                                                                                                                       //Natural: RESET #SEL-OPT-22
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte)))                                       //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #SVE-CHECK-DTE
                        {
                            pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(0);                                                                                              //Natural: ASSIGN #EXTR-NBR-MNTHS := 0
                            pnd_Tot_Curr_Mnth.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOT-CURR-MNTH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_1_Mnths)))                                                 //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-1-MNTHS
                            {
                                pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(1);                                                                                          //Natural: ASSIGN #EXTR-NBR-MNTHS := 1
                                pnd_Tot_1_Mnth_Left.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOT-1-MNTH-LEFT
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_2_Mnths)))                                             //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-2-MNTHS
                                {
                                    pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(2);                                                                                      //Natural: ASSIGN #EXTR-NBR-MNTHS := 2
                                    pnd_Tot_2_Mnth_Left.nadd(1);                                                                                                          //Natural: ADD 1 TO #TOT-2-MNTH-LEFT
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_3_Mnths)))                                         //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-3-MNTHS
                                    {
                                        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(3);                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 3
                                        pnd_Tot_3_Mnth_Left.nadd(1);                                                                                                      //Natural: ADD 1 TO #TOT-3-MNTH-LEFT
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_4_Mnths)))                                     //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-4-MNTHS
                                        {
                                            pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(4);                                                                              //Natural: ASSIGN #EXTR-NBR-MNTHS := 4
                                            pnd_Tot_4_Mnth_Left.nadd(1);                                                                                                  //Natural: ADD 1 TO #TOT-4-MNTH-LEFT
                                        }                                                                                                                                 //Natural: ELSE
                                        else if (condition())
                                        {
                                            if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_5_Mnths)))                                 //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-5-MNTHS
                                            {
                                                pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(5);                                                                          //Natural: ASSIGN #EXTR-NBR-MNTHS := 5
                                                pnd_Tot_5_Mnth_Left.nadd(1);                                                                                              //Natural: ADD 1 TO #TOT-5-MNTH-LEFT
                                            }                                                                                                                             //Natural: ELSE
                                            else if (condition())
                                            {
                                                if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyymm.equals(pnd_Wrk_Ck_Date_6_Mnths)))                             //Natural: IF #EXTR-FIN-PAY-DATE-CCYYMM = #WRK-CK-DATE-6-MNTHS
                                                {
                                                    pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(6);                                                                      //Natural: ASSIGN #EXTR-NBR-MNTHS := 6
                                                    pnd_Tot_6_Mnth_Left.nadd(1);                                                                                          //Natural: ADD 1 TO #TOT-6-MNTH-LEFT
                                                }                                                                                                                         //Natural: ELSE
                                                else if (condition())
                                                {
                                                    pnd_Sel_Opt_22.reset();                                                                                               //Natural: RESET #SEL-OPT-22
                                                }                                                                                                                         //Natural: END-IF
                                            }                                                                                                                             //Natural: END-IF
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: VALUE 30
                    else if (condition((pnd_Input_Pnd_Record_Code.equals(30))))
                    {
                        decideConditionsMet411++;
                        if (condition(pnd_Sel_Opt_22.equals("Y")))                                                                                                        //Natural: IF #SEL-OPT-22 = 'Y'
                        {
                            pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                              //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
                                                                                                                                                                          //Natural: PERFORM #GET-NAME-ADDRESS
                            sub_Pnd_Get_Name_Address();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt.setValue(pnd_Input_Pnd_Summ_Fin_Pymnt);                                                                   //Natural: ASSIGN #EXTR-FIN-PYMNT := #SUMM-FIN-PYMNT
                            pnd_Sel_Opt_22.reset();                                                                                                                       //Natural: RESET #SEL-OPT-22
                            getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                             //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-RECORDS
                sub_Write_Header_Records();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                READWORK02:                                                                                                                                               //Natural: READ WORK 3 #EXTR-REC-OUT
                while (condition(getWorkFiles().read(3, pnd_Extr_Rec_Out)))
                {
                    getSort().writeSortInData(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths, pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde,               //Natural: END-ALL
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt, pnd_Extr_Rec_Out_Pnd_Extr_Name, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1, pnd_Extr_Rec_Out_Pnd_Extr_Adr2, pnd_Extr_Rec_Out_Pnd_Extr_Adr3, pnd_Extr_Rec_Out_Pnd_Extr_Adr4, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5, pnd_Extr_Rec_Out_Pnd_Extr_Adr6, pnd_Extr_Rec_Out_Pnd_Extr_Zip, pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr, 
                        pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind, pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths, "DESCENDING", pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde);           //Natural: SORT BY #EXTR-NBR-MNTHS DESCENDING #EXTR-PPCN-NBR ASCENDING #EXTR-PAYEE-CDE ASCENDING USING #EXTR-CNTRCT-OPTN-CDE #EXTR-CNTRCT-ISSUE-DTE #EXTR-CNTRCT-1ST-PYMNT-DUE-DTE #EXTR-FIN-PER-DATE #EXTR-FIN-PAY-DATE #EXTR-FIN-PYMNT #EXTR-NAME #EXTR-ADR1 #EXTR-ADR2 #EXTR-ADR3 #EXTR-ADR4 #EXTR-ADR5 #EXTR-ADR6 #EXTR-ZIP #EXTR-SS-NBR #EXTR-INST-IND #EXTR-RPT-BRK
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths, pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt, pnd_Extr_Rec_Out_Pnd_Extr_Name, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Adr1, pnd_Extr_Rec_Out_Pnd_Extr_Adr2, pnd_Extr_Rec_Out_Pnd_Extr_Adr3, pnd_Extr_Rec_Out_Pnd_Extr_Adr4, pnd_Extr_Rec_Out_Pnd_Extr_Adr5, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Adr6, pnd_Extr_Rec_Out_Pnd_Extr_Zip, pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr, pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind, 
                    pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk)))
                {
                    if (condition(!(! (pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.equals(getZero()) || pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.equals(9)))))                   //Natural: ACCEPT IF NOT ( #EXTR-FIN-PER-DATE = 0 OR = 9 )
                    {
                        continue;
                    }
                    if (condition(pnd_First_Time.equals("Y")))                                                                                                            //Natural: IF #FIRST-TIME = 'Y'
                    {
                        pnd_First_Time.setValue("N");                                                                                                                     //Natural: ASSIGN #FIRST-TIME := 'N'
                        pnd_Ctr_Sort.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CTR-SORT
                        pnd_Sve_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);                                                                                      //Natural: ASSIGN #SVE-RPT-BRK := #EXTR-RPT-BRK
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADING-LINE
                        sub_Set_Up_Heading_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ( 1 ) ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.notEquals(pnd_Sve_Rpt_Brk)))                                                                          //Natural: IF #EXTR-RPT-BRK NE #SVE-RPT-BRK
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-LINE
                        sub_Write_Total_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Sve_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);                                                                                      //Natural: ASSIGN #SVE-RPT-BRK := #EXTR-RPT-BRK
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADING-LINE
                        sub_Set_Up_Heading_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().newPage(new ReportSpecification(1));                                                                                                 //Natural: NEWPAGE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ( 1 ) ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tot_Fin_Pymnt.nadd(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt);                                                                                          //Natural: ADD #EXTR-FIN-PYMNT TO #TOT-FIN-PYMNT
                    //*   PRINT P/I REPORT
                    pnd_Prt_Issue_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Mm, "/", pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Yy)); //Natural: COMPRESS #EXTR-CNTRCT-ISSUE-MM '/' #EXTR-CNTRCT-ISSUE-YY INTO #PRT-ISSUE-DTE LEAVING NO SPACE
                    pnd_Prt_1st_Due_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Mm, "/",                  //Natural: COMPRESS #EXTR-CNTRCT-1ST-PYMNT-DUE-MM '/' #EXTR-CNTRCT-1ST-PYMNT-DUE-YY INTO #PRT-1ST-DUE-DTE LEAVING NO SPACE
                        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Yy));
                    pnd_Prt_Fin_Per_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm, "/", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Ccyy)); //Natural: COMPRESS #EXTR-FIN-PER-DATE-MM '/' #EXTR-FIN-PER-DATE-CCYY INTO #PRT-FIN-PER-DTE LEAVING NO SPACE
                    pnd_Prt_Fin_Pmt_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Mm, "/", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Dd,  //Natural: COMPRESS #EXTR-FIN-PAY-DATE-MM '/' #EXTR-FIN-PAY-DATE-DD '/' #EXTR-FIN-PAY-DATE-CCYY INTO #PRT-FIN-PMT-DTE LEAVING NO SPACE
                        "/", pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date_Ccyy));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),pnd_Extr_Rec_Out_Pnd_Extr_Ppcn_Nbr, new ReportEditMask ("XXXXXXXX"),new                 //Natural: WRITE ( 1 ) 10T #EXTR-PPCN-NBR ( EM = XXXXXXXX ) 20T #EXTR-PAYEE-CDE ( EM = 99 ) 28T #EXTR-CNTRCT-OPTN-CDE 35T #PRT-ISSUE-DTE 44T #PRT-1ST-DUE-DTE 53T #PRT-FIN-PER-DTE 65T #PRT-FIN-PMT-DTE 77T #EXTR-FIN-PYMNT ( EM = Z,ZZZ,ZZZ.99 ) 91T #EXTR-NAME
                        TabSetting(20),pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde, new ReportEditMask ("99"),new TabSetting(28),pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde,new 
                        TabSetting(35),pnd_Prt_Issue_Dte,new TabSetting(44),pnd_Prt_1st_Due_Dte,new TabSetting(53),pnd_Prt_Fin_Per_Dte,new TabSetting(65),pnd_Prt_Fin_Pmt_Dte,new 
                        TabSetting(77),pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Name);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WRITE (1) (ES=ON) 1T #PRINT-PPCN-NBR
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr1.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR1 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr1);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR1
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr2.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR2 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr2);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR2
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr3.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR3 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr3);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR3
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr4.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR4 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr4);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR4
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr5.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR5 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr5);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR5
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Extr_Rec_Out_Pnd_Extr_Adr6.greater(" ")))                                                                                           //Natural: IF #EXTR-ADR6 GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(91),pnd_Extr_Rec_Out_Pnd_Extr_Adr6);                                                    //Natural: WRITE ( 1 ) 91T #EXTR-ADR6
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RECORDS:",pnd_Tot_Recs, new ReportEditMask ("ZZ,ZZZ"),"TOTAL PAYMENT:",pnd_Tot_Fin_Pymnt,       //Natural: WRITE ( 1 ) / 'TOTAL RECORDS:' #TOT-RECS ( EM = ZZ,ZZZ ) 'TOTAL PAYMENT:' #TOT-FIN-PYMNT ( EM = ZZZ,ZZZ,ZZZ.99- )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (Global.isEscape()) return;
                if (condition(pnd_Ctr_Sort.equals(getZero())))                                                                                                            //Natural: IF #CTR-SORT = 0
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE," *****NO P/I RECORDS SELECTED FOR THIS REPORTING CYCLE*****");                            //Natural: WRITE ( 1 ) // ' *****NO P/I RECORDS SELECTED FOR THIS REPORTING CYCLE*****'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                              //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - IA FILE      : ",pnd_Ctr_Read);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - IA FILE      : ' #CTR-READ
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - NAME FILE    : ",pnd_Name_Ctr);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - NAME FILE    : ' #NAME-CTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS READ - SORTED       : ",pnd_Ctr_Sort);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ - SORTED       : ' #CTR-SORT
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS WRITTEN             : ",pnd_Ctr_Extr);                                                                           //Natural: WRITE 'NUMBER OF RECORDS WRITTEN             : ' #CTR-EXTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Bypass_Ctr);                                                                         //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #BYPASS-CTR
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED-STATUS     : ",pnd_Bypass_Status);                                                                      //Natural: WRITE 'NUMBER OF RECORDS BYPASSED-STATUS     : ' #BYPASS-STATUS
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF RECORDS BYPASSED-PAYEE      : ",pnd_Bypass_Payee);                                                                       //Natural: WRITE 'NUMBER OF RECORDS BYPASSED-PAYEE      : ' #BYPASS-PAYEE
                if (Global.isEscape()) return;
                //* ******************************************
                //*  SET REPORT HEADER DETAIL LINE
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-HEADING-LINE
                //* ******************************************
                //*  WRITE TOTAL LINE
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-LINE
                //* *****************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-NAME-ADDRESS
                //* *********************************************
                //*  SET UP DATES FOR P&I FINAL PMT REPORT
                //* *********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-FIN-PMT-WRK-DTES
                //* *********************************************
                //*  ADJUST DATES FOR 6 MONTHS IN FUTURE
                //* *********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADJUST-FIN-PMT-WRK-DTES
                //* *********************************************
                //*  WRITE HEADER REPORT RECORDS
                //* *********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-RECORDS
                //*                                                                                                                                                       //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Set_Up_Heading_Line() throws Exception                                                                                                               //Natural: SET-UP-HEADING-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Hdr_Mnth_Nme.setValue(mnth_Nme_Table.getValue(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm));                                                                    //Natural: ASSIGN #HDR-MNTH-NME := MNTH-NME-TABLE ( #EXTR-FIN-PER-DATE-MM )
        pnd_Sve_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);                                                                                                      //Natural: ASSIGN #SVE-RPT-BRK := #EXTR-RPT-BRK
        pnd_Hdr_Mnth_Nme.setValue(mnth_Nme_Table.getValue(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Mm));                                                                    //Natural: ASSIGN #HDR-MNTH-NME := MNTH-NME-TABLE ( #EXTR-FIN-PER-DATE-MM )
        pnd_Hdr_Exp_Year.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date_Ccyy);                                                                                           //Natural: ASSIGN #HDR-EXP-YEAR := #EXTR-FIN-PER-DATE-CCYY
        pnd_Hdr_Desc.reset();                                                                                                                                             //Natural: RESET #HDR-DESC
        short decideConditionsMet648 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SVE-RPT-BRK;//Natural: VALUE 0
        if (condition((pnd_Sve_Rpt_Brk.equals(0))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_Curr_Mnth);                                                                                                                     //Natural: MOVE #TOT-CURR-MNTH TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 1
        else if (condition((pnd_Sve_Rpt_Brk.equals(1))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_1_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-1-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Sve_Rpt_Brk.equals(2))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_2_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-2-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Sve_Rpt_Brk.equals(3))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_3_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-3-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Sve_Rpt_Brk.equals(4))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_4_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-4-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Sve_Rpt_Brk.equals(5))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_5_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-5-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Sve_Rpt_Brk.equals(6))))
        {
            decideConditionsMet648++;
            pnd_Tot_Recs.setValue(pnd_Tot_6_Mnth_Left);                                                                                                                   //Natural: MOVE #TOT-6-MNTH-LEFT TO #TOT-RECS
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Hdr_Desc.reset();                                                                                                                                             //Natural: RESET #HDR-DESC
        if (condition(pnd_Tot_Recs.equals(getZero())))                                                                                                                    //Natural: IF #TOT-RECS = 0
        {
            pnd_Hdr_Desc.setValue("(NONE)");                                                                                                                              //Natural: ASSIGN #HDR-DESC := '(NONE)'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sve_Rpt_Brk.notEquals(getZero())))                                                                                                              //Natural: IF #SVE-RPT-BRK NE 0
        {
            pnd_Hdr_Exp_Line.setValue("XXXX   CONTRACT(S) WHICH EXPIRE(S) IN ");                                                                                          //Natural: ASSIGN #HDR-EXP-LINE := 'XXXX   CONTRACT(S) WHICH EXPIRE(S) IN '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdr_Exp_Line.setValue("XXXX CONTRACT(S) WHICH EXPIRE(S) THIS ");                                                                                          //Natural: ASSIGN #HDR-EXP-LINE := 'XXXX CONTRACT(S) WHICH EXPIRE(S) THIS '
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Total_Line() throws Exception                                                                                                                  //Natural: WRITE-TOTAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL RECORDS:",pnd_Tot_Recs, new ReportEditMask ("ZZ,ZZ9"),"TOTAL PAYMENT:",pnd_Tot_Fin_Pymnt,               //Natural: WRITE ( 1 ) / 'TOTAL RECORDS:' #TOT-RECS ( EM = ZZ,ZZ9 ) 'TOTAL PAYMENT:' #TOT-FIN-PYMNT ( EM = ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Tot_Fin_Pymnt.reset();                                                                                                                                        //Natural: RESET #TOT-FIN-PYMNT
    }
    private void sub_Pnd_Get_Name_Address() throws Exception                                                                                                              //Natural: #GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'SUBROUTINE #GET-NAME-ADDRESS'
        if (condition(pnd_Name_Eof.getBoolean()))                                                                                                                         //Natural: IF #NAME-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue("NO NAME & ADDRESS FOUND");                                                                                           //Natural: MOVE 'NO NAME & ADDRESS FOUND' TO #EXTR-NAME
            pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO #EXTR-ADR1 #EXTR-ADR2 #EXTR-ADR3 #EXTR-ADR4 #EXTR-ADR5 #EXTR-ADR6 #EXTR-ZIP
            pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(" ");
            pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(" ");
            pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(" ");
            pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(" ");
            pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(" ");
            pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(" ");
            if (condition(pnd_Name_Cntrct_Payee.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                                     //Natural: IF #NAME.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Cntrct_Payee.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                                      //Natural: IF #NAME.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
            {
                short decideConditionsMet701 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R = 0 AND #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-R = 0
                if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.equals(getZero()) && pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R.equals(getZero())))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_Cntrct_Name_Free_R.equals(" ")))                                                                                               //Natural: IF CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_K);                                                                             //Natural: ASSIGN #EXTR-NAME := CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_K);                                                                                 //Natural: ASSIGN #EXTR-ADR1 := ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_K);                                                                                 //Natural: ASSIGN #EXTR-ADR2 := ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_K);                                                                                 //Natural: ASSIGN #EXTR-ADR3 := ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_K);                                                                                 //Natural: ASSIGN #EXTR-ADR4 := ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_K);                                                                                 //Natural: ASSIGN #EXTR-ADR5 := ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_K);                                                                                 //Natural: ASSIGN #EXTR-ADR6 := ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_K.getSubstring(1,9));                                                          //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_R);                                                                             //Natural: ASSIGN #EXTR-NAME := CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_R);                                                                                 //Natural: ASSIGN #EXTR-ADR1 := ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_R);                                                                                 //Natural: ASSIGN #EXTR-ADR2 := ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_R);                                                                                 //Natural: ASSIGN #EXTR-ADR3 := ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_R);                                                                                 //Natural: ASSIGN #EXTR-ADR4 := ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_R);                                                                                 //Natural: ASSIGN #EXTR-ADR5 := ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_R);                                                                                 //Natural: ASSIGN #EXTR-ADR6 := ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_R.getSubstring(1,9));                                                          //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-K = 0
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.equals(getZero()) && 
                    pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K.equals(getZero())))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_Cntrct_Name_Free_K.equals(" ")))                                                                                               //Natural: IF CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_R);                                                                             //Natural: ASSIGN #EXTR-NAME := CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_R);                                                                                 //Natural: ASSIGN #EXTR-ADR1 := ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_R);                                                                                 //Natural: ASSIGN #EXTR-ADR2 := ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_R);                                                                                 //Natural: ASSIGN #EXTR-ADR3 := ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_R);                                                                                 //Natural: ASSIGN #EXTR-ADR4 := ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_R);                                                                                 //Natural: ASSIGN #EXTR-ADR5 := ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_R);                                                                                 //Natural: ASSIGN #EXTR-ADR6 := ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_R.getSubstring(1,9));                                                          //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_Cntrct_Name_Free_K);                                                                             //Natural: ASSIGN #EXTR-NAME := CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_Addrss_Lne_1_K);                                                                                 //Natural: ASSIGN #EXTR-ADR1 := ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_Addrss_Lne_2_K);                                                                                 //Natural: ASSIGN #EXTR-ADR2 := ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_Addrss_Lne_3_K);                                                                                 //Natural: ASSIGN #EXTR-ADR3 := ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_Addrss_Lne_4_K);                                                                                 //Natural: ASSIGN #EXTR-ADR4 := ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_Addrss_Lne_5_K);                                                                                 //Natural: ASSIGN #EXTR-ADR5 := ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_Addrss_Lne_6_K);                                                                                 //Natural: ASSIGN #EXTR-ADR6 := ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_Addrss_Postal_Data_K.getSubstring(1,9));                                                          //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R = 0 AND #SVE-CHECK-DTE-8 GE #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-R
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.equals(getZero()) && 
                    pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.greaterOrEqual(pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R)))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_R.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #SVE-CHECK-DTE-8 GE #NAME.PENDING-PERM-ADDRSS-CHNGE-DTE-K
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.equals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.equals(getZero()) && 
                    pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.greaterOrEqual(pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K)))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_K.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-R NE 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-R NE 0 AND ( #SVE-CHECK-DTE-8 GE #NAME.PENDING-ADDRSS-CHNGE-DTE-R AND #SVE-CHECK-DTE-8 LE #NAME.PENDING-ADDRSS-RESTORE-DTE-R )
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_R.notEquals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_R.notEquals(getZero()) 
                    && pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.greaterOrEqual(pnd_Name_Pending_Addrss_Chnge_Dte_R) && pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.lessOrEqual(pnd_Name_Pending_Addrss_Restore_Dte_R)))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_R.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-R = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #NAME.PENDING-ADDRSS-CHNGE-DTE-K NE 0 AND #NAME.PENDING-ADDRSS-RESTORE-DTE-K NE 0 AND ( #SVE-CHECK-DTE-8 GE #NAME.PENDING-ADDRSS-CHNGE-DTE-K AND #SVE-CHECK-DTE-8 LE #NAME.PENDING-ADDRSS-RESTORE-DTE-K )
                else if (condition(pnd_Name_Pending_Addrss_Chnge_Dte_K.notEquals(getZero()) && pnd_Name_Pending_Addrss_Restore_Dte_K.notEquals(getZero()) 
                    && pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.greaterOrEqual(pnd_Name_Pending_Addrss_Chnge_Dte_K) && pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte_8.lessOrEqual(pnd_Name_Pending_Addrss_Restore_Dte_K)))
                {
                    decideConditionsMet701++;
                    if (condition(pnd_Name_V_Cntrct_Name_Free_K.equals(" ")))                                                                                             //Natural: IF V-CNTRCT-NAME-FREE-K = ' '
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_R);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_R);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_R);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_R);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_R);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_R);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_R);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-R
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_R.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-R,1,9 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Extr_Rec_Out_Pnd_Extr_Name.setValue(pnd_Name_V_Cntrct_Name_Free_K);                                                                           //Natural: ASSIGN #EXTR-NAME := V-CNTRCT-NAME-FREE-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr1.setValue(pnd_Name_V_Addrss_Lne_1_K);                                                                               //Natural: ASSIGN #EXTR-ADR1 := V-ADDRSS-LNE-1-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr2.setValue(pnd_Name_V_Addrss_Lne_2_K);                                                                               //Natural: ASSIGN #EXTR-ADR2 := V-ADDRSS-LNE-2-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr3.setValue(pnd_Name_V_Addrss_Lne_3_K);                                                                               //Natural: ASSIGN #EXTR-ADR3 := V-ADDRSS-LNE-3-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr4.setValue(pnd_Name_V_Addrss_Lne_4_K);                                                                               //Natural: ASSIGN #EXTR-ADR4 := V-ADDRSS-LNE-4-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr5.setValue(pnd_Name_V_Addrss_Lne_5_K);                                                                               //Natural: ASSIGN #EXTR-ADR5 := V-ADDRSS-LNE-5-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Adr6.setValue(pnd_Name_V_Addrss_Lne_6_K);                                                                               //Natural: ASSIGN #EXTR-ADR6 := V-ADDRSS-LNE-6-K
                        pnd_Extr_Rec_Out_Pnd_Extr_Zip.setValue(pnd_Name_V_Addrss_Postal_Data_K.getSubstring(1,9));                                                        //Natural: ASSIGN #EXTR-ZIP := SUBSTR ( V-ADDRSS-POSTAL-DATA-K,1,9 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Name);                                                                                                                             //Natural: READ WORK 2 ONCE #NAME
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Name_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NAME-EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-ENDFILE
            pnd_Name_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NAME-CTR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Set_Up_Fin_Pmt_Wrk_Dtes() throws Exception                                                                                                           //Natural: SET-UP-FIN-PMT-WRK-DTES
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_1_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-1-MNTHS := #WRK-CK-DATE
                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_2_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-2-MNTHS := #WRK-CK-DATE
                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_3_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-3-MNTHS := #WRK-CK-DATE
                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_4_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-4-MNTHS := #WRK-CK-DATE
                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_5_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-5-MNTHS := #WRK-CK-DATE
                                                                                                                                                                          //Natural: PERFORM ADJUST-FIN-PMT-WRK-DTES
        sub_Adjust_Fin_Pmt_Wrk_Dtes();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Wrk_Ck_Date_6_Mnths.setValue(pnd_Wrk_Ck_Date);                                                                                                                //Natural: ASSIGN #WRK-CK-DATE-6-MNTHS := #WRK-CK-DATE
    }
    private void sub_Adjust_Fin_Pmt_Wrk_Dtes() throws Exception                                                                                                           //Natural: ADJUST-FIN-PMT-WRK-DTES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WRK-CK-DTE-MM
        if (condition(pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm.greater(12)))                                                                                                     //Natural: IF #WRK-CK-DTE-MM GT 12
        {
            pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Mm.nsubtract(12);                                                                                                              //Natural: SUBTRACT 12 FROM #WRK-CK-DTE-MM
            pnd_Wrk_Ck_Date_Pnd_Wrk_Ck_Dte_Ccyy.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WRK-CK-DTE-CCYY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Header_Records() throws Exception                                                                                                              //Natural: WRITE-HEADER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.reset();                                                                                                                        //Natural: RESET #EXTR-CONT-NO #EXTR-PAYEE-CDE #EXTR-CNTRCT-OPTN-CDE #EXTR-CNTRCT-ISSUE-DTE #EXTR-CNTRCT-1ST-PYMNT-DUE-DTE #EXTR-FIN-PER-DATE #EXTR-FIN-PAY-DATE #EXTR-FIN-PYMNT
        pnd_Extr_Rec_Out_Pnd_Extr_Payee_Cde.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt.reset();
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(0);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 0
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte);                                                                           //Natural: ASSIGN #EXTR-FIN-PER-DATE := #SVE-CHECK-DTE
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(1);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 1
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_1_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-1-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        getReports().write(0, pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths,pnd_Extr_Rec_Out_Pnd_Extr_Cont_No,pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde,pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte, //Natural: WRITE #EXTR-REC-OUT
            pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt,
            pnd_Extr_Rec_Out_Pnd_Extr_Name,pnd_Extr_Rec_Out_Pnd_Extr_Adr1,pnd_Extr_Rec_Out_Pnd_Extr_Adr2,pnd_Extr_Rec_Out_Pnd_Extr_Adr3,pnd_Extr_Rec_Out_Pnd_Extr_Adr4,
            pnd_Extr_Rec_Out_Pnd_Extr_Adr5,pnd_Extr_Rec_Out_Pnd_Extr_Adr6,pnd_Extr_Rec_Out_Pnd_Extr_Zip,pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr,pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind,
            pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);
        if (Global.isEscape()) return;
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(2);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 2
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_2_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-2-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        getReports().write(0, pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths,pnd_Extr_Rec_Out_Pnd_Extr_Cont_No,pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Optn_Cde,pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_Issue_Dte, //Natural: WRITE #EXTR-REC-OUT
            pnd_Extr_Rec_Out_Pnd_Extr_Cntrct_1st_Pymnt_Due_Dte,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pay_Date,pnd_Extr_Rec_Out_Pnd_Extr_Fin_Pymnt,
            pnd_Extr_Rec_Out_Pnd_Extr_Name,pnd_Extr_Rec_Out_Pnd_Extr_Adr1,pnd_Extr_Rec_Out_Pnd_Extr_Adr2,pnd_Extr_Rec_Out_Pnd_Extr_Adr3,pnd_Extr_Rec_Out_Pnd_Extr_Adr4,
            pnd_Extr_Rec_Out_Pnd_Extr_Adr5,pnd_Extr_Rec_Out_Pnd_Extr_Adr6,pnd_Extr_Rec_Out_Pnd_Extr_Zip,pnd_Extr_Rec_Out_Pnd_Extr_Ss_Nbr,pnd_Extr_Rec_Out_Pnd_Extr_Inst_Ind,
            pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk);
        if (Global.isEscape()) return;
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(3);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 3
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_3_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-3-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(4);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 4
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_4_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-4-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(5);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 5
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_5_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-5-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.setValue("HEADER");                                                                                                             //Natural: ASSIGN #EXTR-CONT-NO := 'HEADER'
        pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths.setValue(6);                                                                                                                  //Natural: ASSIGN #EXTR-NBR-MNTHS := 6
        pnd_Extr_Rec_Out_Pnd_Extr_Rpt_Brk.setValue(pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths);                                                                                  //Natural: ASSIGN #EXTR-RPT-BRK := #EXTR-NBR-MNTHS
        pnd_Extr_Rec_Out_Pnd_Extr_Fin_Per_Date.setValue(pnd_Wrk_Ck_Date_6_Mnths);                                                                                         //Natural: ASSIGN #EXTR-FIN-PER-DATE := #WRK-CK-DATE-6-MNTHS
        getWorkFiles().write(3, false, pnd_Extr_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 3 #EXTR-REC-OUT
        pnd_Extr_Rec_Out_Pnd_Extr_Cont_No.reset();                                                                                                                        //Natural: RESET #EXTR-CONT-NO
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"PROGRAM: ",Global.getPROGRAM(),new TabSetting(45),"IA MONTHLY P/I REPORT FOR PAYMENTS DUE",pnd_Sve_Check_Dte_A_Pnd_Sve_Check_Dte,  //Natural: WRITE ( 1 ) NOTITLE 2T 'PROGRAM: ' *PROGRAM 45T 'IA MONTHLY P/I REPORT FOR PAYMENTS DUE' #SVE-CHECK-DTE ( EM = 9999/99 ) 111T 'PAGE' *PAGE-NUMBER ( 1 ) / 5T 'DATE: ' *DATU
                        new ReportEditMask ("9999/99"),new TabSetting(111),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(5),"DATE: ",Global.getDATU());
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),pnd_Hdr_Exp_Line,new ColumnSpacing(1),pnd_Extr_Rec_Out_Pnd_Extr_Nbr_Mnths,new            //Natural: WRITE ( 1 ) NOTITLE 2T #HDR-EXP-LINE 1X #EXTR-NBR-MNTHS 1X 'MONTH(S)  -  ' 1X #HDR-MNTH-NME 1X #HDR-EXP-YEAR 1X #HDR-DESC /
                        ColumnSpacing(1),"MONTH(S)  -  ",new ColumnSpacing(1),pnd_Hdr_Mnth_Nme,new ColumnSpacing(1),pnd_Hdr_Exp_Year,new ColumnSpacing(1),
                        pnd_Hdr_Desc,NEWLINE);
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),"CONTRACT",new TabSetting(20),"PAYEE",new TabSetting(28),"OPT",new TabSetting(35),"ISSUE",new  //Natural: WRITE ( 1 ) 10T 'CONTRACT' 20T 'PAYEE' 28T 'OPT' 35T 'ISSUE' 44T 'FIRST' 53T 'FINAL' 65T 'FINAL' 77T 'FINAL ' 90T ' NAME And ADDRESS'
                        TabSetting(44),"FIRST",new TabSetting(53),"FINAL",new TabSetting(65),"FINAL",new TabSetting(77),"FINAL ",new TabSetting(90)," NAME And ADDRESS");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(10)," NUMBER ",new TabSetting(20)," CODE",new TabSetting(28),"CDE",new TabSetting(35),"DATE ",new  //Natural: WRITE ( 1 ) 10T ' NUMBER ' 20T ' CODE' 28T 'CDE' 35T 'DATE ' 43T 'PMT DUE' 53T 'PER DTE' 65T 'PMT DTE' 77T 'PAYMENT' 90T '                 '
                        TabSetting(43),"PMT DUE",new TabSetting(53),"PER DTE",new TabSetting(65),"PMT DTE",new TabSetting(77),"PAYMENT",new TabSetting(90),
                        "                 ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM(),"TFOR CONTRACT #: ",pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde);                                //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM 'TFOR CONTRACT #: ' #PPCN-NBR #PAYEE-CDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56 ZP=OFF SG=OFF");
    }
}
