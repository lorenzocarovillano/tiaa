/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:21 PM
**        * FROM NATURAL PROGRAM : Iaap730
************************************************************
**        * FILE NAME            : Iaap730.java
**        * CLASS NAME           : Iaap730
**        * INSTANCE NAME        : Iaap730
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP730  - ROLLOVER REPORTS (IAARPT40)            *
*                                                                    *
*   MODIFIED   -   1/2003                                            *
*                  MADE CHANGES TO REPORTS FOR EGTRRA IVC ROLLOVER   *
*                  CONTRACTS                                         *
*                                                                    *
*   MODIFIED   -   1/98                                              *
*                  MADE CHANGES TO HANDLE CREF MONTHLY PMT CONTRACTS *
*                  USING MONTHLY UNIT VALUE TO CALCULATE PAYMENT AMT *
*                  CALL AIAN026 TO GET LATEST UNIT VALUE             *
*                  ADDED FUND DESC TO DETAIL REPORT-1                *
*                                                                    *
*                  FOR ANNUAL USE PMT AMT ON THE FILE                *
*                                                                    *
*                  DO SCAN ON 1/98 FOR CHANGES                       *
*                                                                    *
*   MODIFIED   -   3/97                                              *
*                  MADE CHANGES ELIMINATE CALL TO AIAN003 FOR CREF   *
*                  UNIT VALUE TO CALCULATE CREF PMT AMT              *
*                  CREF PMT AMT IN CREF FUND                         *
*                  DO SCAN ON 3/97 FOR CHANGES                       *
*                                                                    *
*   MODIFIED   -   8/96                                              *
*                  USE EXTERNALIZATION FILE FOR VALID CREF PRODS     *
*                  & DESCRIPTIONS                                    *
*                  DO SCAN ON 8/96 FOR CHANGES                       *
*                                                                    *
*                  INCREASED RECORD LENGTH FOR ROTH  4/08
*    3/12          RATE BASE EXPANSION - SCAN ON 3/12 FOR CHANGES
*   05/2017  OS PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap730 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Account_Key;
    private DbsField pnd_Account_Num;
    private DbsField pnd_Account_Val;
    private DbsField pnd_Account_Val2;
    private DbsField pnd_Account_Val3;

    private DbsGroup iaaa730;
    private DbsField iaaa730_Tiaa_Fund;
    private DbsField iaaa730_Per_Pay_Amt;
    private DbsField iaaa730_Per_Div_Amt;
    private DbsField iaaa730_Tot_Fin_Payment;
    private DbsField iaaa730_Tot_Fin_Div;
    private DbsField iaaa730_Tot_Amt;
    private DbsField iaaa730_Tpa_Fund;
    private DbsField iaaa730_Ipro_Fund;
    private DbsField iaaa730_Pamp_I_Fund;
    private DbsField iaaa730_Afslash_C_Fund;
    private DbsField iaaa730_Tpa_Pay_Amt;
    private DbsField iaaa730_Ipro_Pay_Amt;
    private DbsField iaaa730_Pamp_I_Pay_Amt;
    private DbsField iaaa730_Afslash_C_Pay_Amt;
    private DbsField iaaa730_Tpa_Div_Amt;
    private DbsField iaaa730_Ipro_Div_Amt;
    private DbsField iaaa730_Pamp_I_Div_Amt;
    private DbsField iaaa730_Afslash_C_Div_Amt;
    private DbsField iaaa730_Tpa_Amt;
    private DbsField iaaa730_Ipro_Amt;
    private DbsField iaaa730_Pamp_I_Amt;
    private DbsField iaaa730_Afslash_C_Amt;
    private DbsField iaaa730_Pnd_Fin_Payment;
    private DbsField iaaa730_Pnd_Fin_Div;

    private DbsGroup iaaa730_Cref_Fund_Cnts;
    private DbsField iaaa730_Cref_Fund_Payees;
    private DbsField iaaa730_Cref_Units;
    private DbsField iaaa730_Cref_Amt;
    private DbsField iaaa730_Cref_Fund_Payees_Mnthly;
    private DbsField iaaa730_Cref_Units_Mnthly;
    private DbsField iaaa730_Cref_Amt_Mnthly;

    private DbsGroup iaaa730a_Rec_Out;
    private DbsField iaaa730a_Rec_Out_Distination_Curr;
    private DbsField iaaa730a_Rec_Out_Distination_Prev;
    private DbsField iaaa730a_Rec_Out_Soc_Sec;
    private DbsField iaaa730a_Rec_Out_Da_Contract;
    private DbsField iaaa730a_Rec_Out_Ia_Contract;
    private DbsField iaaa730a_Rec_Out_Contractual_Amt;
    private DbsField iaaa730a_Rec_Out_Per_Dividend;
    private DbsField iaaa730a_Rec_Out_Cash_Cd;
    private DbsField iaaa730a_Rec_Out_Term_Cd;
    private DbsField iaaa730a_Rec_Out_Hold_Cd;
    private DbsField iaaa730a_Rec_Out_Pend_Cd;
    private DbsField iaaa730a_Rec_Out_Check_Date;
    private DbsField iaaa730a_Rec_Out_Mode;
    private DbsField iaaa730a_Rec_Out_Coll_Cd;
    private DbsField pnd_Parm_Fund_3;
    private DbsField pnd_Parm_Desc;

    private DbsGroup pnd_Parm_Desc__R_Field_1;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_6;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_Rem;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;
    private DbsField pnd_I;
    private DbsField pnd_Fund_Desc_Lne;

    private DbsGroup pnd_Fund_Desc_Lne__R_Field_2;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Desc;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Txt;
    private DbsField pnd_T_Tot_P_P;
    private DbsField pnd_T_Tot_D_P;
    private DbsField pnd_T_Per_Amt;
    private DbsField pnd_T_Div_Amt;
    private DbsField pnd_T_Units;
    private DbsField pnd_T_Amt;
    private DbsField pnd_T_Ivc;
    private DbsField pnd_T_Payees;
    private DbsField pnd_Fund_Desc_Det;
    private DbsField count;
    private DbsField pnd_Prod_Cnt;
    private DbsField pnd_Max_Prd_Cde;
    private DbsField pnd_Acct_Info;

    private DbsGroup pnd_Acct_Info__R_Field_3;
    private DbsField pnd_Acct_Info_Pnd_Acct_Key1;

    private DbsGroup pnd_Acct_Info__R_Field_4;
    private DbsField pnd_Acct_Info_Pnd_Acct_Cmpny;
    private DbsField pnd_Acct_Info_Pnd_Acct_Fund;
    private DbsField pnd_Acct_Info_Pnd_Acct_Rpt_Num;
    private DbsField pnd_Acct_Info_Pnd_Acct_Pmt_Type;
    private DbsField pnd_Acct_Info_Pnd_Acct_Pfx;
    private DbsField pnd_Acct_Info_Pnd_Acct_Desc;
    private DbsField pnd_Acct_Info_Pnd_Acct_Key_Comment;
    private DbsField pnd_Account_Num_Info;

    private DbsGroup pnd_Account_Num_Info__R_Field_5;
    private DbsField pnd_Account_Num_Info_Pnd_Account_Num1;
    private DbsField pnd_Account_Num_Info_Pnd_Account_Num2;
    private DbsField pnd_Account_Num_Info_Pnd_Account_Num3;
    private DbsField pnd_Account_Num_Info_Pnd_Account_Num4;
    private DbsField pnd_Account_Num_Info_Pnd_Account_Comm;
    private DbsField pnd_Sve_Account_Num;
    private DbsField pnd_Sve_Acct_Val;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Code;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record_344;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Input_Record__Filler1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record__Filler2;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input_Record__Filler3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Record__Filler4;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Name;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Dob;
    private DbsField pnd_Input_Record__Filler5;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Dod;
    private DbsField pnd_Input_Record_Filler;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde;
    private DbsField pnd_Input_Record__Filler6;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd;

    private DbsGroup pnd_Input_Record__R_Field_7;
    private DbsField pnd_Input_Record__Filler7;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Input_Record__Filler8;
    private DbsField pnd_Input_Record_Pnd_Cpr_Status_Cde;
    private DbsField pnd_Input_Record__Filler9;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Cash_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Xfr_Term_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Company_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record__Filler10;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Input_Record__Filler11;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Input_Record__Filler12;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Hold_Cde;
    private DbsField pnd_Input_Record__Filler13;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde;

    private DbsGroup pnd_Input_Record__R_Field_8;
    private DbsField pnd_Input_Record__Filler14;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record__Filler15;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input_Record__R_Field_9;
    private DbsField pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde;

    private DbsGroup pnd_Input_Record__R_Field_10;
    private DbsField pnd_Input_Record_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input_Record__R_Field_11;
    private DbsField pnd_Input_Record_Pnd_Summ_Fund_Cde_N;
    private DbsField pnd_Input_Record__Filler16;
    private DbsField pnd_Input_Record_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Record_Pnd_Summ_Per_Dvdnd;
    private DbsField pnd_Input_Record__Filler17;
    private DbsField pnd_Input_Record_Pnd_Summ_Units;
    private DbsField pnd_Input_Record_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Record_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Ipro_Output;
    private DbsField pnd_Ipro_Output_Ipro_Contract;
    private DbsField pnd_Ipro_Output_Ipro_Payee;
    private DbsField pnd_Ipro_Output_Ipro_Pend_Cde;
    private DbsField pnd_Ipro_Output_Ipro_Hold_Cde;
    private DbsField pnd_Ipro_Output_Ipro_Name;
    private DbsField pnd_Ipro_Output_Ipro_Dob_Redf;
    private DbsField pnd_Ipro_Output_Ipro_Issue_Dte_Yy_Mm;
    private DbsField pnd_Ipro_Output_Ipro_Fin_Pmt_Yy_Mm;
    private DbsField pnd_Ipro_Output_Ipro_Msg;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_12;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField fst_Ann_Dob;

    private DbsGroup fst_Ann_Dob__R_Field_13;
    private DbsField fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm;

    private DbsGroup fst_Ann_Dob__R_Field_14;
    private DbsField fst_Ann_Dob_Fst_Ann_Dob_Yr;
    private DbsField fst_Ann_Dob_Fst_Ann_Dob_Mm;
    private DbsField fst_Ann_Dob_Fst_Ann_Dob_Dd;

    private DbsGroup ipro_Warning_Rpt_Accumulators;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Recs_In;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Hi;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Eq;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Age_Eq_70_1_2;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Age_Under_70;
    private DbsField ipro_Warning_Rpt_Accumulators_Ipro_Age_Over_70;
    private DbsField pnd_Bypass;
    private DbsField pnd_Unit_Val_Type;
    private DbsField pnd_New_Payee;
    private DbsField pnd_Save_Pnd_Cntrct_Inst_Iss_Cde;
    private DbsField pnd_Save_Summ_Cmpny_Fnd_Cde;

    private DbsGroup pnd_Save_Summ_Cmpny_Fnd_Cde__R_Field_15;
    private DbsField pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde;
    private DbsField pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Fund_Cde;
    private DbsField pnd_Save_Part_Ppcn_Nbr;
    private DbsField pnd_Save_Payee_Cde;
    private DbsField pnd_Save_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Save_Cntrct_Record_Code;
    private DbsField pnd_Save_Cntrct_Optn_Cde;
    private DbsField pnd_Save_Cntrct_Orgn_Cde;
    private DbsField pnd_Save_Cntrct_Crrncy_Cde;
    private DbsField pnd_Save_Cpr_Cmpny_Cde;
    private DbsField pnd_Save_Cntrct_Mode_Ind;
    private DbsField pnd_Save_Cpr_Status_Cde;
    private DbsField pnd_Save_Strt_Dte;
    private DbsField pnd_Save_Stp_Dte;
    private DbsField pnd_Save_Issue_Dte;
    private DbsField pnd_Save_Issue_Dte_Dd;
    private DbsField pnd_Save_Cntrct_Prev_Dist_Cde;
    private DbsField pnd_Save_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Save_Cntrct_Cash_Cde;
    private DbsField pnd_Save_Cntrct_Xfr_Term_Cde;
    private DbsField pnd_Save_Cntrct_Hold_Cde;
    private DbsField pnd_Save_Cntrct_Pend_Cde;
    private DbsField pnd_Save_Cntrct_Dob;
    private DbsField pnd_Save_Cntrct_Name;
    private DbsField pnd_Save_Per_Ivc_Amt;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_16;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_17;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_18;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_19;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Ctr;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_20;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy;
    private DbsField pnd_Amount;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_Occ_A;

    private DbsGroup pnd_Occ_A__R_Field_21;
    private DbsField pnd_Occ_A_Pnd_Occ_1a;
    private DbsField pnd_Occ_A_Pnd_Occ;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_22;
    private DbsField pnd_Today_Pnd_Today_N;

    private DbsGroup pnd_Today__R_Field_23;
    private DbsField pnd_Today_Pnd_Today_Yyyymm;
    private DbsField pnd_Today_Pnd_Today_Dd;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_24;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;
    private DbsField pnd_Per_Amt_A;

    private DbsGroup pnd_Per_Amt_A__R_Field_25;
    private DbsField pnd_Per_Amt_A_Pnd_Per_Amt;
    private DbsField pnd_Per_Amt_1;
    private DbsField pnd_Div_Amt_A;

    private DbsGroup pnd_Div_Amt_A__R_Field_26;
    private DbsField pnd_Div_Amt_A_Pnd_Div_Amt;
    private DbsField pnd_Div_Amt_1;
    private DbsField pnd_Units_A;

    private DbsGroup pnd_Units_A__R_Field_27;
    private DbsField pnd_Units_A_Pnd_Units;
    private DbsField pnd_Units_1;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Amt;
    private DbsField pnd_Units_Total;
    private DbsField pnd_Munits_A;

    private DbsGroup pnd_Munits_A__R_Field_28;
    private DbsField pnd_Munits_A_Pnd_Munits;
    private DbsField pnd_Munits_1;
    private DbsField pnd_Mtotal;
    private DbsField pnd_Mtotal_Amt;
    private DbsField pnd_Munits_Total;
    private DbsField pnd_Mtot_Units;
    private DbsField pnd_Mtot_Units_Amt;
    private DbsField pnd_Mtot_Payees;
    private DbsField pnd_Aunits_A;

    private DbsGroup pnd_Aunits_A__R_Field_29;
    private DbsField pnd_Aunits_A_Pnd_Aunits;
    private DbsField pnd_Aunits_1;
    private DbsField pnd_Atotal;
    private DbsField pnd_Atotal_Amt;
    private DbsField pnd_Aunits_Total;
    private DbsField pnd_Atot_Units;
    private DbsField pnd_Atot_Units_Amt;
    private DbsField pnd_Atot_Payees;

    private DbsGroup pnd_Cpr_Print;
    private DbsField pnd_Cpr_Print_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Cpr_Print_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Print_Cntrct_Prev_Dist_Cde;
    private DbsField pnd_Cpr_Print_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Cpr_Print_Cntrct_Cash_Cde;
    private DbsField pnd_Cpr_Print_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Cpr_Print_Cntrct_Hold_Cde;
    private DbsField pnd_Cpr_Print_Cntrct_Pend_Cde;
    private DbsField pnd_Cpr_Print_Cntrct_Mode_Ind;
    private DbsField pnd_Auv_Returned;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Cntrl_Dte_A;

    private DbsGroup pnd_Cntrl_Dte_A__R_Field_30;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm;

    private DbsGroup pnd_Cntrl_Dte_A__R_Field_31;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm;
    private DbsField pnd_Dte_A;

    private DbsGroup pnd_Dte_A__R_Field_32;
    private DbsField pnd_Dte_A_Pnd_Yyyymm;

    private DbsGroup pnd_Dte_A__R_Field_33;
    private DbsField pnd_Dte_A_Pnd_Yyy;
    private DbsField pnd_Dte_A_Pnd_Mm;
    private DbsField pnd_Fund_Cde_2;

    private DbsGroup pnd_Fund_Cde_2__R_Field_34;
    private DbsField pnd_Fund_Cde_2_Pnd_Fund_Cde_1a;
    private DbsField pnd_Fund_Cde_2_Pnd_Fund_Cde_1c;

    private DbsGroup pnd_Fund_Cde_2__R_Field_35;
    private DbsField pnd_Fund_Cde_2_Pnd_Fund_Cde_1b;
    private DbsField pnd_Print;
    private DbsField pnd_Tot_Ivc_Amt;
    private DbsField pnd_Tot_Per_Amt;
    private DbsField pnd_Tot_Div_Amt;
    private DbsField pnd_Tot_Units;
    private DbsField pnd_Tot_Units_Amt;
    private DbsField pnd_Tot_Payees;
    private DbsField pnd_Wrk_Payees;
    private DbsField pnd_Tot_Payees_A;
    private DbsField pnd_Tot_Payees_M;
    private DbsField pnd_Cref_Dol;
    private DbsField pnd_Cref_Fund_Cdes;

    private DbsGroup pnd_Cref_Fund_Cdes__R_Field_36;
    private DbsField pnd_Cref_Fund_Cdes_Pnd_Cref_Array;
    private DbsField pnd_Dest_Cde;
    private DbsField pnd_Dest_Payees;
    private DbsField pnd_Dest_Per_Pay;
    private DbsField pnd_Dest_Div_Pay;
    private DbsField pnd_Dest_Units;
    private DbsField pnd_Dest_Units_Total;
    private DbsField pnd_Dest_Total_Amt;
    private DbsField pnd_Dest_Total_Ivc;
    private DbsField pnd_Dest_Occ;
    private DbsField pnd_Dest_Payees_Grand;
    private DbsField pnd_Dest_Per_Pay_Grand;
    private DbsField pnd_Dest_Div_Pay_Grand;
    private DbsField pnd_Dest_Units_Grand;
    private DbsField pnd_Dest_Units_Total_Grand;
    private DbsField pnd_Dest_Total_Amt_Grand;
    private DbsField pnd_Dest_Total_Ivc_Grand;
    private DbsField pnd_Payment_Due;

    private DbsGroup ia_Aian026_Linkage;
    private DbsField ia_Aian026_Linkage_Ia_Call_Type;
    private DbsField ia_Aian026_Linkage_Ia_Fund_Code;
    private DbsField ia_Aian026_Linkage_Ia_Reval_Methd;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_37;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Check_Dd;

    private DbsGroup ia_Aian026_Linkage__R_Field_38;
    private DbsField ia_Aian026_Linkage_Ia_Check_Year;
    private DbsField ia_Aian026_Linkage_Ia_Check_Month;
    private DbsField ia_Aian026_Linkage_Ia_Check_Day;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_39;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Day;

    private DbsGroup ia_Aian026_Linkage__R_Field_40;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date_A;
    private DbsField ia_Aian026_Linkage_Return_Code_11;

    private DbsGroup ia_Aian026_Linkage__R_Field_41;
    private DbsField ia_Aian026_Linkage_Return_Code_Pgm;
    private DbsField ia_Aian026_Linkage_Return_Code;
    private DbsField ia_Aian026_Linkage_Auv_Returned;
    private DbsField ia_Aian026_Linkage_Auv_Date_Return;
    private DbsField ia_Aian026_Linkage_Days_In_Request_Month;
    private DbsField ia_Aian026_Linkage_Days_In_Particip_Month;

    private DbsGroup pnd_Input_Record_Work_2;
    private DbsField pnd_Input_Record_Work_2_Pnd_Call_Type;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Mode__R_Field_42;
    private DbsField pnd_Mode_Pnd_Filler;
    private DbsField pnd_Mode_Pnd_Mode_Mm;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Latest_Factor;
    private DbsField pnd_Const_Pnd_Payment_Orign_Factor;
    private DbsField pnd_Record_Length;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_Account_Key = localVariables.newFieldArrayInRecord("pnd_Account_Key", "#ACCOUNT-KEY", FieldType.STRING, 50, new DbsArrayController(1, 60));
        pnd_Account_Num = localVariables.newFieldArrayInRecord("pnd_Account_Num", "#ACCOUNT-NUM", FieldType.STRING, 50, new DbsArrayController(1, 60));
        pnd_Account_Val = localVariables.newFieldArrayInRecord("pnd_Account_Val", "#ACCOUNT-VAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            60));
        pnd_Account_Val2 = localVariables.newFieldArrayInRecord("pnd_Account_Val2", "#ACCOUNT-VAL2", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            60));
        pnd_Account_Val3 = localVariables.newFieldArrayInRecord("pnd_Account_Val3", "#ACCOUNT-VAL3", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            60));

        iaaa730 = localVariables.newGroupInRecord("iaaa730", "IAAA730");
        iaaa730_Tiaa_Fund = iaaa730.newFieldInGroup("iaaa730_Tiaa_Fund", "TIAA-FUND", FieldType.PACKED_DECIMAL, 9);
        iaaa730_Per_Pay_Amt = iaaa730.newFieldInGroup("iaaa730_Per_Pay_Amt", "PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Per_Div_Amt = iaaa730.newFieldInGroup("iaaa730_Per_Div_Amt", "PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Tot_Fin_Payment = iaaa730.newFieldInGroup("iaaa730_Tot_Fin_Payment", "TOT-FIN-PAYMENT", FieldType.PACKED_DECIMAL, 14, 2);
        iaaa730_Tot_Fin_Div = iaaa730.newFieldInGroup("iaaa730_Tot_Fin_Div", "TOT-FIN-DIV", FieldType.PACKED_DECIMAL, 14, 2);
        iaaa730_Tot_Amt = iaaa730.newFieldInGroup("iaaa730_Tot_Amt", "TOT-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        iaaa730_Tpa_Fund = iaaa730.newFieldInGroup("iaaa730_Tpa_Fund", "TPA-FUND", FieldType.PACKED_DECIMAL, 9);
        iaaa730_Ipro_Fund = iaaa730.newFieldInGroup("iaaa730_Ipro_Fund", "IPRO-FUND", FieldType.PACKED_DECIMAL, 9);
        iaaa730_Pamp_I_Fund = iaaa730.newFieldInGroup("iaaa730_Pamp_I_Fund", "P&I-FUND", FieldType.PACKED_DECIMAL, 9);
        iaaa730_Afslash_C_Fund = iaaa730.newFieldInGroup("iaaa730_Afslash_C_Fund", "A/C-FUND", FieldType.PACKED_DECIMAL, 9);
        iaaa730_Tpa_Pay_Amt = iaaa730.newFieldInGroup("iaaa730_Tpa_Pay_Amt", "TPA-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Ipro_Pay_Amt = iaaa730.newFieldInGroup("iaaa730_Ipro_Pay_Amt", "IPRO-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Pamp_I_Pay_Amt = iaaa730.newFieldInGroup("iaaa730_Pamp_I_Pay_Amt", "P&I-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Afslash_C_Pay_Amt = iaaa730.newFieldInGroup("iaaa730_Afslash_C_Pay_Amt", "A/C-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Tpa_Div_Amt = iaaa730.newFieldInGroup("iaaa730_Tpa_Div_Amt", "TPA-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Ipro_Div_Amt = iaaa730.newFieldInGroup("iaaa730_Ipro_Div_Amt", "IPRO-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Pamp_I_Div_Amt = iaaa730.newFieldInGroup("iaaa730_Pamp_I_Div_Amt", "P&I-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Afslash_C_Div_Amt = iaaa730.newFieldInGroup("iaaa730_Afslash_C_Div_Amt", "A/C-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Tpa_Amt = iaaa730.newFieldInGroup("iaaa730_Tpa_Amt", "TPA-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Ipro_Amt = iaaa730.newFieldInGroup("iaaa730_Ipro_Amt", "IPRO-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Pamp_I_Amt = iaaa730.newFieldInGroup("iaaa730_Pamp_I_Amt", "P&I-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Afslash_C_Amt = iaaa730.newFieldInGroup("iaaa730_Afslash_C_Amt", "A/C-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        iaaa730_Pnd_Fin_Payment = iaaa730.newFieldArrayInGroup("iaaa730_Pnd_Fin_Payment", "#FIN-PAYMENT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));
        iaaa730_Pnd_Fin_Div = iaaa730.newFieldArrayInGroup("iaaa730_Pnd_Fin_Div", "#FIN-DIV", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));

        iaaa730_Cref_Fund_Cnts = iaaa730.newGroupArrayInGroup("iaaa730_Cref_Fund_Cnts", "CREF-FUND-CNTS", new DbsArrayController(1, 20));
        iaaa730_Cref_Fund_Payees = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Fund_Payees", "CREF-FUND-PAYEES", FieldType.PACKED_DECIMAL, 8);
        iaaa730_Cref_Units = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Units", "CREF-UNITS", FieldType.PACKED_DECIMAL, 12, 3);
        iaaa730_Cref_Amt = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Amt", "CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        iaaa730_Cref_Fund_Payees_Mnthly = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Fund_Payees_Mnthly", "CREF-FUND-PAYEES-MNTHLY", FieldType.PACKED_DECIMAL, 
            8);
        iaaa730_Cref_Units_Mnthly = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Units_Mnthly", "CREF-UNITS-MNTHLY", FieldType.PACKED_DECIMAL, 
            12, 3);
        iaaa730_Cref_Amt_Mnthly = iaaa730_Cref_Fund_Cnts.newFieldInGroup("iaaa730_Cref_Amt_Mnthly", "CREF-AMT-MNTHLY", FieldType.PACKED_DECIMAL, 11, 2);

        iaaa730a_Rec_Out = localVariables.newGroupInRecord("iaaa730a_Rec_Out", "IAAA730A-REC-OUT");
        iaaa730a_Rec_Out_Distination_Curr = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Distination_Curr", "DISTINATION-CURR", FieldType.STRING, 
            4);
        iaaa730a_Rec_Out_Distination_Prev = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Distination_Prev", "DISTINATION-PREV", FieldType.STRING, 
            4);
        iaaa730a_Rec_Out_Soc_Sec = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Soc_Sec", "SOC-SEC", FieldType.NUMERIC, 9);
        iaaa730a_Rec_Out_Da_Contract = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Da_Contract", "DA-CONTRACT", FieldType.STRING, 8);
        iaaa730a_Rec_Out_Ia_Contract = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Ia_Contract", "IA-CONTRACT", FieldType.STRING, 10);
        iaaa730a_Rec_Out_Contractual_Amt = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Contractual_Amt", "CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        iaaa730a_Rec_Out_Per_Dividend = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Per_Dividend", "PER-DIVIDEND", FieldType.PACKED_DECIMAL, 9, 
            2);
        iaaa730a_Rec_Out_Cash_Cd = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Cash_Cd", "CASH-CD", FieldType.STRING, 1);
        iaaa730a_Rec_Out_Term_Cd = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Term_Cd", "TERM-CD", FieldType.STRING, 1);
        iaaa730a_Rec_Out_Hold_Cd = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Hold_Cd", "HOLD-CD", FieldType.STRING, 1);
        iaaa730a_Rec_Out_Pend_Cd = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Pend_Cd", "PEND-CD", FieldType.STRING, 1);
        iaaa730a_Rec_Out_Check_Date = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Check_Date", "CHECK-DATE", FieldType.NUMERIC, 6);
        iaaa730a_Rec_Out_Mode = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Mode", "MODE", FieldType.NUMERIC, 3);
        iaaa730a_Rec_Out_Coll_Cd = iaaa730a_Rec_Out.newFieldInGroup("iaaa730a_Rec_Out_Coll_Cd", "COLL-CD", FieldType.STRING, 5);
        pnd_Parm_Fund_3 = localVariables.newFieldInRecord("pnd_Parm_Fund_3", "#PARM-FUND-3", FieldType.STRING, 2);
        pnd_Parm_Desc = localVariables.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);

        pnd_Parm_Desc__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Desc__R_Field_1", "REDEFINE", pnd_Parm_Desc);
        pnd_Parm_Desc_Pnd_Parm_Desc_6 = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_6", "#PARM-DESC-6", FieldType.STRING, 6);
        pnd_Parm_Desc_Pnd_Parm_Desc_Rem = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_Rem", "#PARM-DESC-REM", FieldType.STRING, 
            29);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Parm_Len = localVariables.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Fund_Desc_Lne = localVariables.newFieldInRecord("pnd_Fund_Desc_Lne", "#FUND-DESC-LNE", FieldType.STRING, 36);

        pnd_Fund_Desc_Lne__R_Field_2 = localVariables.newGroupInRecord("pnd_Fund_Desc_Lne__R_Field_2", "REDEFINE", pnd_Fund_Desc_Lne);
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con = pnd_Fund_Desc_Lne__R_Field_2.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con", "#FUND-CREF-CON", FieldType.STRING, 
            5);
        pnd_Fund_Desc_Lne_Pnd_Fund_Desc = pnd_Fund_Desc_Lne__R_Field_2.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 
            6);
        pnd_Fund_Desc_Lne_Pnd_Fund_Txt = pnd_Fund_Desc_Lne__R_Field_2.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Txt", "#FUND-TXT", FieldType.STRING, 
            25);
        pnd_T_Tot_P_P = localVariables.newFieldArrayInRecord("pnd_T_Tot_P_P", "#T-TOT-P-P", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 3));
        pnd_T_Tot_D_P = localVariables.newFieldArrayInRecord("pnd_T_Tot_D_P", "#T-TOT-D-P", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 3));
        pnd_T_Per_Amt = localVariables.newFieldArrayInRecord("pnd_T_Per_Amt", "#T-PER-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 4));
        pnd_T_Div_Amt = localVariables.newFieldArrayInRecord("pnd_T_Div_Amt", "#T-DIV-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 4));
        pnd_T_Units = localVariables.newFieldArrayInRecord("pnd_T_Units", "#T-UNITS", FieldType.NUMERIC, 10, 3, new DbsArrayController(1, 4));
        pnd_T_Amt = localVariables.newFieldArrayInRecord("pnd_T_Amt", "#T-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 4));
        pnd_T_Ivc = localVariables.newFieldArrayInRecord("pnd_T_Ivc", "#T-IVC", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 4));
        pnd_T_Payees = localVariables.newFieldArrayInRecord("pnd_T_Payees", "#T-PAYEES", FieldType.NUMERIC, 7, new DbsArrayController(1, 4));
        pnd_Fund_Desc_Det = localVariables.newFieldInRecord("pnd_Fund_Desc_Det", "#FUND-DESC-DET", FieldType.STRING, 3);
        count = localVariables.newFieldInRecord("count", "COUNT", FieldType.NUMERIC, 3);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cde = localVariables.newFieldInRecord("pnd_Max_Prd_Cde", "#MAX-PRD-CDE", FieldType.NUMERIC, 2);
        pnd_Acct_Info = localVariables.newFieldInRecord("pnd_Acct_Info", "#ACCT-INFO", FieldType.STRING, 50);

        pnd_Acct_Info__R_Field_3 = localVariables.newGroupInRecord("pnd_Acct_Info__R_Field_3", "REDEFINE", pnd_Acct_Info);
        pnd_Acct_Info_Pnd_Acct_Key1 = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Key1", "#ACCT-KEY1", FieldType.STRING, 3);

        pnd_Acct_Info__R_Field_4 = pnd_Acct_Info__R_Field_3.newGroupInGroup("pnd_Acct_Info__R_Field_4", "REDEFINE", pnd_Acct_Info_Pnd_Acct_Key1);
        pnd_Acct_Info_Pnd_Acct_Cmpny = pnd_Acct_Info__R_Field_4.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Cmpny", "#ACCT-CMPNY", FieldType.STRING, 1);
        pnd_Acct_Info_Pnd_Acct_Fund = pnd_Acct_Info__R_Field_4.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Fund", "#ACCT-FUND", FieldType.NUMERIC, 2);
        pnd_Acct_Info_Pnd_Acct_Rpt_Num = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Rpt_Num", "#ACCT-RPT-NUM", FieldType.NUMERIC, 
            1);
        pnd_Acct_Info_Pnd_Acct_Pmt_Type = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Pmt_Type", "#ACCT-PMT-TYPE", FieldType.STRING, 
            3);
        pnd_Acct_Info_Pnd_Acct_Pfx = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Pfx", "#ACCT-PFX", FieldType.STRING, 8);
        pnd_Acct_Info_Pnd_Acct_Desc = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Desc", "#ACCT-DESC", FieldType.STRING, 11);
        pnd_Acct_Info_Pnd_Acct_Key_Comment = pnd_Acct_Info__R_Field_3.newFieldInGroup("pnd_Acct_Info_Pnd_Acct_Key_Comment", "#ACCT-KEY-COMMENT", FieldType.STRING, 
            21);
        pnd_Account_Num_Info = localVariables.newFieldInRecord("pnd_Account_Num_Info", "#ACCOUNT-NUM-INFO", FieldType.STRING, 50);

        pnd_Account_Num_Info__R_Field_5 = localVariables.newGroupInRecord("pnd_Account_Num_Info__R_Field_5", "REDEFINE", pnd_Account_Num_Info);
        pnd_Account_Num_Info_Pnd_Account_Num1 = pnd_Account_Num_Info__R_Field_5.newFieldInGroup("pnd_Account_Num_Info_Pnd_Account_Num1", "#ACCOUNT-NUM1", 
            FieldType.STRING, 8);
        pnd_Account_Num_Info_Pnd_Account_Num2 = pnd_Account_Num_Info__R_Field_5.newFieldInGroup("pnd_Account_Num_Info_Pnd_Account_Num2", "#ACCOUNT-NUM2", 
            FieldType.STRING, 8);
        pnd_Account_Num_Info_Pnd_Account_Num3 = pnd_Account_Num_Info__R_Field_5.newFieldInGroup("pnd_Account_Num_Info_Pnd_Account_Num3", "#ACCOUNT-NUM3", 
            FieldType.STRING, 8);
        pnd_Account_Num_Info_Pnd_Account_Num4 = pnd_Account_Num_Info__R_Field_5.newFieldInGroup("pnd_Account_Num_Info_Pnd_Account_Num4", "#ACCOUNT-NUM4", 
            FieldType.STRING, 8);
        pnd_Account_Num_Info_Pnd_Account_Comm = pnd_Account_Num_Info__R_Field_5.newFieldInGroup("pnd_Account_Num_Info_Pnd_Account_Comm", "#ACCOUNT-COMM", 
            FieldType.STRING, 18);
        pnd_Sve_Account_Num = localVariables.newFieldInRecord("pnd_Sve_Account_Num", "#SVE-ACCOUNT-NUM", FieldType.STRING, 8);
        pnd_Sve_Acct_Val = localVariables.newFieldInRecord("pnd_Sve_Acct_Val", "#SVE-ACCT-VAL", FieldType.PACKED_DECIMAL, 11, 2);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Code = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record_344 = pnd_Input_Record.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rest_Of_Record_344", "#REST-OF-RECORD-344", 
            FieldType.STRING, 1, new DbsArrayController(1, 344));

        pnd_Input_Record__R_Field_6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Orgn_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record__Filler1 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record__Filler2 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler2", "_FILLER2", FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record__Filler3 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler3", "_FILLER3", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_Orig_Da_Cntrct_Nbr = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orig_Da_Cntrct_Nbr", 
            "#CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Input_Record__Filler4 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler4", "_FILLER4", FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_Name = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Name", "#CNTRCT-NAME", FieldType.NUMERIC, 
            9);
        pnd_Input_Record_Pnd_Cntrct_Dob = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Dob", "#CNTRCT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record__Filler5 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler5", "_FILLER5", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_Dod = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Dod", "#CNTRCT-DOD", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Filler = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Filler", "FILLER", FieldType.STRING, 38);
        pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde", "#CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Input_Record__Filler6 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record__Filler6", "_FILLER6", FieldType.STRING, 45);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Input_Record__R_Field_7 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_7", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record__Filler7 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler7", "_FILLER7", FieldType.STRING, 26);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr", "#PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record__Filler8 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler8", "_FILLER8", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cpr_Status_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Status_Cde", "#CPR-STATUS-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Record__Filler9 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler9", "_FILLER9", FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_Cash_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Cash_Cde", "#CNTRCT-CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Xfr_Term_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Xfr_Term_Cde", "#CNTRCT-XFR-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Company_Cde = pnd_Input_Record__R_Field_7.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Company_Cde", "#CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind = pnd_Input_Record__R_Field_7.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind", "#CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record__R_Field_7.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record__Filler10 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler10", "_FILLER10", FieldType.STRING, 120);
        pnd_Input_Record_Pnd_Cntrct_Mode_Ind = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record__Filler11 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler11", "_FILLER11", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte", "#CNTRCT-FINAL-PER-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte", "#CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record__Filler12 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler12", "_FILLER12", FieldType.STRING, 15);
        pnd_Input_Record_Pnd_Cntrct_Pend_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Hold_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record__Filler13 = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record__Filler13", "_FILLER13", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde", "#CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde", "#CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4);

        pnd_Input_Record__R_Field_8 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_8", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record__Filler14 = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record__Filler14", "_FILLER14", FieldType.STRING, 23);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Input_Record__Filler15 = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record__Filler15", "_FILLER15", FieldType.STRING, 16);
        pnd_Input_Record_Pnd_Ddctn_Strt_Dte = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Stp_Dte = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input_Record__R_Field_9 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_9", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record_344);
        pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde", "#SUMM-CMPNY-FND-CDE", 
            FieldType.STRING, 3);

        pnd_Input_Record__R_Field_10 = pnd_Input_Record__R_Field_9.newGroupInGroup("pnd_Input_Record__R_Field_10", "REDEFINE", pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde);
        pnd_Input_Record_Pnd_Summ_Cmpny_Cde = pnd_Input_Record__R_Field_10.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Summ_Fund_Cde = pnd_Input_Record__R_Field_10.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 
            2);

        pnd_Input_Record__R_Field_11 = pnd_Input_Record__R_Field_10.newGroupInGroup("pnd_Input_Record__R_Field_11", "REDEFINE", pnd_Input_Record_Pnd_Summ_Fund_Cde);
        pnd_Input_Record_Pnd_Summ_Fund_Cde_N = pnd_Input_Record__R_Field_11.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fund_Cde_N", "#SUMM-FUND-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record__Filler16 = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record__Filler16", "_FILLER16", FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Summ_Per_Pymnt = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Summ_Per_Dvdnd = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record__Filler17 = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record__Filler17", "_FILLER17", FieldType.STRING, 26);
        pnd_Input_Record_Pnd_Summ_Units = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Input_Record_Pnd_Summ_Fin_Pymnt = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Summ_Fin_Dvdnd = pnd_Input_Record__R_Field_9.newFieldInGroup("pnd_Input_Record_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Ipro_Output = localVariables.newGroupInRecord("pnd_Ipro_Output", "#IPRO-OUTPUT");
        pnd_Ipro_Output_Ipro_Contract = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Contract", "IPRO-CONTRACT", FieldType.STRING, 8);
        pnd_Ipro_Output_Ipro_Payee = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Payee", "IPRO-PAYEE", FieldType.STRING, 1);
        pnd_Ipro_Output_Ipro_Pend_Cde = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Pend_Cde", "IPRO-PEND-CDE", FieldType.STRING, 1);
        pnd_Ipro_Output_Ipro_Hold_Cde = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Hold_Cde", "IPRO-HOLD-CDE", FieldType.STRING, 1);
        pnd_Ipro_Output_Ipro_Name = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Name", "IPRO-NAME", FieldType.STRING, 9);
        pnd_Ipro_Output_Ipro_Dob_Redf = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Dob_Redf", "IPRO-DOB-REDF", FieldType.NUMERIC, 8);
        pnd_Ipro_Output_Ipro_Issue_Dte_Yy_Mm = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Issue_Dte_Yy_Mm", "IPRO-ISSUE-DTE-YY-MM", FieldType.NUMERIC, 
            8);
        pnd_Ipro_Output_Ipro_Fin_Pmt_Yy_Mm = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Fin_Pmt_Yy_Mm", "IPRO-FIN-PMT-YY-MM", FieldType.NUMERIC, 
            6);
        pnd_Ipro_Output_Ipro_Msg = pnd_Ipro_Output.newFieldInGroup("pnd_Ipro_Output_Ipro_Msg", "IPRO-MSG", FieldType.STRING, 50);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_12", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_12.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        fst_Ann_Dob = localVariables.newFieldInRecord("fst_Ann_Dob", "FST-ANN-DOB", FieldType.STRING, 8);

        fst_Ann_Dob__R_Field_13 = localVariables.newGroupInRecord("fst_Ann_Dob__R_Field_13", "REDEFINE", fst_Ann_Dob);
        fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm = fst_Ann_Dob__R_Field_13.newFieldInGroup("fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm", "FST-ANN-DOB-YR-MM", FieldType.NUMERIC, 
            6);

        fst_Ann_Dob__R_Field_14 = fst_Ann_Dob__R_Field_13.newGroupInGroup("fst_Ann_Dob__R_Field_14", "REDEFINE", fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm);
        fst_Ann_Dob_Fst_Ann_Dob_Yr = fst_Ann_Dob__R_Field_14.newFieldInGroup("fst_Ann_Dob_Fst_Ann_Dob_Yr", "FST-ANN-DOB-YR", FieldType.NUMERIC, 4);
        fst_Ann_Dob_Fst_Ann_Dob_Mm = fst_Ann_Dob__R_Field_14.newFieldInGroup("fst_Ann_Dob_Fst_Ann_Dob_Mm", "FST-ANN-DOB-MM", FieldType.NUMERIC, 2);
        fst_Ann_Dob_Fst_Ann_Dob_Dd = fst_Ann_Dob__R_Field_13.newFieldInGroup("fst_Ann_Dob_Fst_Ann_Dob_Dd", "FST-ANN-DOB-DD", FieldType.NUMERIC, 2);

        ipro_Warning_Rpt_Accumulators = localVariables.newGroupInRecord("ipro_Warning_Rpt_Accumulators", "IPRO-WARNING-RPT-ACCUMULATORS");
        ipro_Warning_Rpt_Accumulators_Ipro_Recs_In = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Recs_In", "IPRO-RECS-IN", 
            FieldType.PACKED_DECIMAL, 7);
        ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Hi = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Hi", 
            "IPRO-FIN-DATE-HI", FieldType.PACKED_DECIMAL, 7);
        ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Eq = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Eq", 
            "IPRO-FIN-DATE-EQ", FieldType.PACKED_DECIMAL, 7);
        ipro_Warning_Rpt_Accumulators_Ipro_Age_Eq_70_1_2 = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Age_Eq_70_1_2", 
            "IPRO-AGE-EQ-70-1-2", FieldType.PACKED_DECIMAL, 7);
        ipro_Warning_Rpt_Accumulators_Ipro_Age_Under_70 = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Age_Under_70", 
            "IPRO-AGE-UNDER-70", FieldType.PACKED_DECIMAL, 7);
        ipro_Warning_Rpt_Accumulators_Ipro_Age_Over_70 = ipro_Warning_Rpt_Accumulators.newFieldInGroup("ipro_Warning_Rpt_Accumulators_Ipro_Age_Over_70", 
            "IPRO-AGE-OVER-70", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Unit_Val_Type = localVariables.newFieldInRecord("pnd_Unit_Val_Type", "#UNIT-VAL-TYPE", FieldType.STRING, 4);
        pnd_New_Payee = localVariables.newFieldInRecord("pnd_New_Payee", "#NEW-PAYEE", FieldType.BOOLEAN, 1);
        pnd_Save_Pnd_Cntrct_Inst_Iss_Cde = localVariables.newFieldInRecord("pnd_Save_Pnd_Cntrct_Inst_Iss_Cde", "#SAVE-#CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5);
        pnd_Save_Summ_Cmpny_Fnd_Cde = localVariables.newFieldInRecord("pnd_Save_Summ_Cmpny_Fnd_Cde", "#SAVE-SUMM-CMPNY-FND-CDE", FieldType.STRING, 3);

        pnd_Save_Summ_Cmpny_Fnd_Cde__R_Field_15 = localVariables.newGroupInRecord("pnd_Save_Summ_Cmpny_Fnd_Cde__R_Field_15", "REDEFINE", pnd_Save_Summ_Cmpny_Fnd_Cde);
        pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde = pnd_Save_Summ_Cmpny_Fnd_Cde__R_Field_15.newFieldInGroup("pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde", 
            "#SAVE-SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Fund_Cde = pnd_Save_Summ_Cmpny_Fnd_Cde__R_Field_15.newFieldInGroup("pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Fund_Cde", 
            "#SAVE-SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Save_Part_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Part_Ppcn_Nbr", "#SAVE-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Prtcpnt_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_Save_Prtcpnt_Tax_Id_Nbr", "#SAVE-PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Save_Cntrct_Record_Code = localVariables.newFieldInRecord("pnd_Save_Cntrct_Record_Code", "#SAVE-CNTRCT-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Optn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Optn_Cde", "#SAVE-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Orgn_Cde", "#SAVE-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Save_Cntrct_Crrncy_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Crrncy_Cde", "#SAVE-CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Cpr_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Save_Cpr_Cmpny_Cde", "#SAVE-CPR-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Mode_Ind = localVariables.newFieldInRecord("pnd_Save_Cntrct_Mode_Ind", "#SAVE-CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Save_Cpr_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Cpr_Status_Cde", "#SAVE-CPR-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Strt_Dte = localVariables.newFieldInRecord("pnd_Save_Strt_Dte", "#SAVE-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Stp_Dte = localVariables.newFieldInRecord("pnd_Save_Stp_Dte", "#SAVE-STP-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Issue_Dte = localVariables.newFieldInRecord("pnd_Save_Issue_Dte", "#SAVE-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Save_Issue_Dte_Dd = localVariables.newFieldInRecord("pnd_Save_Issue_Dte_Dd", "#SAVE-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Prev_Dist_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Prev_Dist_Cde", "#SAVE-CNTRCT-PREV-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Save_Cntrct_Curr_Dist_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Curr_Dist_Cde", "#SAVE-CNTRCT-CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Save_Cntrct_Cash_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Cash_Cde", "#SAVE-CNTRCT-CASH-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Xfr_Term_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Xfr_Term_Cde", "#SAVE-CNTRCT-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Save_Cntrct_Hold_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Hold_Cde", "#SAVE-CNTRCT-HOLD-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Pend_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Pend_Cde", "#SAVE-CNTRCT-PEND-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Dob", "#SAVE-CNTRCT-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Name = localVariables.newFieldInRecord("pnd_Save_Cntrct_Name", "#SAVE-CNTRCT-NAME", FieldType.STRING, 9);
        pnd_Save_Per_Ivc_Amt = localVariables.newFieldInRecord("pnd_Save_Per_Ivc_Amt", "#SAVE-PER-IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_16 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_16", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_17 = pnd_Save_Check_Dte_A__R_Field_16.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_17", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_18 = pnd_Save_Check_Dte_A__R_Field_17.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_18", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_19 = pnd_Save_Check_Dte_A__R_Field_16.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_19", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_19.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 8);

        pnd_Payment_Due_Dte__R_Field_20 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_20", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy", "#PAYMENT-DUE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Amount = localVariables.newFieldInRecord("pnd_Amount", "#AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.PACKED_DECIMAL, 3);
        pnd_Occ_A = localVariables.newFieldInRecord("pnd_Occ_A", "#OCC-A", FieldType.STRING, 3);

        pnd_Occ_A__R_Field_21 = localVariables.newGroupInRecord("pnd_Occ_A__R_Field_21", "REDEFINE", pnd_Occ_A);
        pnd_Occ_A_Pnd_Occ_1a = pnd_Occ_A__R_Field_21.newFieldInGroup("pnd_Occ_A_Pnd_Occ_1a", "#OCC-1A", FieldType.STRING, 1);
        pnd_Occ_A_Pnd_Occ = pnd_Occ_A__R_Field_21.newFieldInGroup("pnd_Occ_A_Pnd_Occ", "#OCC", FieldType.NUMERIC, 2);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_22 = localVariables.newGroupInRecord("pnd_Today__R_Field_22", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_N = pnd_Today__R_Field_22.newFieldInGroup("pnd_Today_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_23 = pnd_Today__R_Field_22.newGroupInGroup("pnd_Today__R_Field_23", "REDEFINE", pnd_Today_Pnd_Today_N);
        pnd_Today_Pnd_Today_Yyyymm = pnd_Today__R_Field_23.newFieldInGroup("pnd_Today_Pnd_Today_Yyyymm", "#TODAY-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Today_Pnd_Today_Dd = pnd_Today__R_Field_23.newFieldInGroup("pnd_Today_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_24", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_24.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_24.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_24.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);
        pnd_Per_Amt_A = localVariables.newFieldInRecord("pnd_Per_Amt_A", "#PER-AMT-A", FieldType.STRING, 10);

        pnd_Per_Amt_A__R_Field_25 = localVariables.newGroupInRecord("pnd_Per_Amt_A__R_Field_25", "REDEFINE", pnd_Per_Amt_A);
        pnd_Per_Amt_A_Pnd_Per_Amt = pnd_Per_Amt_A__R_Field_25.newFieldInGroup("pnd_Per_Amt_A_Pnd_Per_Amt", "#PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Amt_1 = localVariables.newFieldInRecord("pnd_Per_Amt_1", "#PER-AMT-1", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Div_Amt_A = localVariables.newFieldInRecord("pnd_Div_Amt_A", "#DIV-AMT-A", FieldType.STRING, 10);

        pnd_Div_Amt_A__R_Field_26 = localVariables.newGroupInRecord("pnd_Div_Amt_A__R_Field_26", "REDEFINE", pnd_Div_Amt_A);
        pnd_Div_Amt_A_Pnd_Div_Amt = pnd_Div_Amt_A__R_Field_26.newFieldInGroup("pnd_Div_Amt_A_Pnd_Div_Amt", "#DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Div_Amt_1 = localVariables.newFieldInRecord("pnd_Div_Amt_1", "#DIV-AMT-1", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Units_A = localVariables.newFieldInRecord("pnd_Units_A", "#UNITS-A", FieldType.STRING, 8);

        pnd_Units_A__R_Field_27 = localVariables.newGroupInRecord("pnd_Units_A__R_Field_27", "REDEFINE", pnd_Units_A);
        pnd_Units_A_Pnd_Units = pnd_Units_A__R_Field_27.newFieldInGroup("pnd_Units_A_Pnd_Units", "#UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Units_1 = localVariables.newFieldInRecord("pnd_Units_1", "#UNITS-1", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Amt = localVariables.newFieldInRecord("pnd_Total_Amt", "#TOTAL-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Units_Total = localVariables.newFieldInRecord("pnd_Units_Total", "#UNITS-TOTAL", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Munits_A = localVariables.newFieldInRecord("pnd_Munits_A", "#MUNITS-A", FieldType.STRING, 8);

        pnd_Munits_A__R_Field_28 = localVariables.newGroupInRecord("pnd_Munits_A__R_Field_28", "REDEFINE", pnd_Munits_A);
        pnd_Munits_A_Pnd_Munits = pnd_Munits_A__R_Field_28.newFieldInGroup("pnd_Munits_A_Pnd_Munits", "#MUNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Munits_1 = localVariables.newFieldInRecord("pnd_Munits_1", "#MUNITS-1", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Mtotal = localVariables.newFieldInRecord("pnd_Mtotal", "#MTOTAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Mtotal_Amt = localVariables.newFieldInRecord("pnd_Mtotal_Amt", "#MTOTAL-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Munits_Total = localVariables.newFieldInRecord("pnd_Munits_Total", "#MUNITS-TOTAL", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Mtot_Units = localVariables.newFieldInRecord("pnd_Mtot_Units", "#MTOT-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Mtot_Units_Amt = localVariables.newFieldInRecord("pnd_Mtot_Units_Amt", "#MTOT-UNITS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Mtot_Payees = localVariables.newFieldInRecord("pnd_Mtot_Payees", "#MTOT-PAYEES", FieldType.PACKED_DECIMAL, 6);
        pnd_Aunits_A = localVariables.newFieldInRecord("pnd_Aunits_A", "#AUNITS-A", FieldType.STRING, 8);

        pnd_Aunits_A__R_Field_29 = localVariables.newGroupInRecord("pnd_Aunits_A__R_Field_29", "REDEFINE", pnd_Aunits_A);
        pnd_Aunits_A_Pnd_Aunits = pnd_Aunits_A__R_Field_29.newFieldInGroup("pnd_Aunits_A_Pnd_Aunits", "#AUNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Aunits_1 = localVariables.newFieldInRecord("pnd_Aunits_1", "#AUNITS-1", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Atotal = localVariables.newFieldInRecord("pnd_Atotal", "#ATOTAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Atotal_Amt = localVariables.newFieldInRecord("pnd_Atotal_Amt", "#ATOTAL-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Aunits_Total = localVariables.newFieldInRecord("pnd_Aunits_Total", "#AUNITS-TOTAL", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Atot_Units = localVariables.newFieldInRecord("pnd_Atot_Units", "#ATOT-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Atot_Units_Amt = localVariables.newFieldInRecord("pnd_Atot_Units_Amt", "#ATOT-UNITS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Atot_Payees = localVariables.newFieldInRecord("pnd_Atot_Payees", "#ATOT-PAYEES", FieldType.PACKED_DECIMAL, 6);

        pnd_Cpr_Print = localVariables.newGroupInRecord("pnd_Cpr_Print", "#CPR-PRINT");
        pnd_Cpr_Print_Prtcpnt_Tax_Id_Nbr = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9);
        pnd_Cpr_Print_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cpr_Print_Cntrct_Prev_Dist_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Cpr_Print_Cntrct_Curr_Dist_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Cpr_Print_Cntrct_Cash_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 1);
        pnd_Cpr_Print_Cpr_Xfr_Term_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 1);
        pnd_Cpr_Print_Cntrct_Hold_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 1);
        pnd_Cpr_Print_Cntrct_Pend_Cde = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1);
        pnd_Cpr_Print_Cntrct_Mode_Ind = pnd_Cpr_Print.newFieldInGroup("pnd_Cpr_Print_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Auv_Returned = localVariables.newFieldInRecord("pnd_Auv_Returned", "#AUV-RETURNED", FieldType.NUMERIC, 5, 2);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 2);
        pnd_Cntrl_Dte_A = localVariables.newFieldInRecord("pnd_Cntrl_Dte_A", "#CNTRL-DTE-A", FieldType.STRING, 8);

        pnd_Cntrl_Dte_A__R_Field_30 = localVariables.newGroupInRecord("pnd_Cntrl_Dte_A__R_Field_30", "REDEFINE", pnd_Cntrl_Dte_A);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm = pnd_Cntrl_Dte_A__R_Field_30.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm", "#CNTRL-YYYYMM", FieldType.NUMERIC, 
            6);

        pnd_Cntrl_Dte_A__R_Field_31 = pnd_Cntrl_Dte_A__R_Field_30.newGroupInGroup("pnd_Cntrl_Dte_A__R_Field_31", "REDEFINE", pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy = pnd_Cntrl_Dte_A__R_Field_31.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy", "#CNTRL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm = pnd_Cntrl_Dte_A__R_Field_31.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm", "#CNTRL-MM", FieldType.NUMERIC, 2);
        pnd_Dte_A = localVariables.newFieldInRecord("pnd_Dte_A", "#DTE-A", FieldType.STRING, 6);

        pnd_Dte_A__R_Field_32 = localVariables.newGroupInRecord("pnd_Dte_A__R_Field_32", "REDEFINE", pnd_Dte_A);
        pnd_Dte_A_Pnd_Yyyymm = pnd_Dte_A__R_Field_32.newFieldInGroup("pnd_Dte_A_Pnd_Yyyymm", "#YYYYMM", FieldType.NUMERIC, 6);

        pnd_Dte_A__R_Field_33 = pnd_Dte_A__R_Field_32.newGroupInGroup("pnd_Dte_A__R_Field_33", "REDEFINE", pnd_Dte_A_Pnd_Yyyymm);
        pnd_Dte_A_Pnd_Yyy = pnd_Dte_A__R_Field_33.newFieldInGroup("pnd_Dte_A_Pnd_Yyy", "#YYY", FieldType.NUMERIC, 4);
        pnd_Dte_A_Pnd_Mm = pnd_Dte_A__R_Field_33.newFieldInGroup("pnd_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Fund_Cde_2 = localVariables.newFieldInRecord("pnd_Fund_Cde_2", "#FUND-CDE-2", FieldType.STRING, 2);

        pnd_Fund_Cde_2__R_Field_34 = localVariables.newGroupInRecord("pnd_Fund_Cde_2__R_Field_34", "REDEFINE", pnd_Fund_Cde_2);
        pnd_Fund_Cde_2_Pnd_Fund_Cde_1a = pnd_Fund_Cde_2__R_Field_34.newFieldInGroup("pnd_Fund_Cde_2_Pnd_Fund_Cde_1a", "#FUND-CDE-1A", FieldType.STRING, 
            1);
        pnd_Fund_Cde_2_Pnd_Fund_Cde_1c = pnd_Fund_Cde_2__R_Field_34.newFieldInGroup("pnd_Fund_Cde_2_Pnd_Fund_Cde_1c", "#FUND-CDE-1C", FieldType.NUMERIC, 
            1);

        pnd_Fund_Cde_2__R_Field_35 = localVariables.newGroupInRecord("pnd_Fund_Cde_2__R_Field_35", "REDEFINE", pnd_Fund_Cde_2);
        pnd_Fund_Cde_2_Pnd_Fund_Cde_1b = pnd_Fund_Cde_2__R_Field_35.newFieldInGroup("pnd_Fund_Cde_2_Pnd_Fund_Cde_1b", "#FUND-CDE-1B", FieldType.NUMERIC, 
            2);
        pnd_Print = localVariables.newFieldInRecord("pnd_Print", "#PRINT", FieldType.BOOLEAN, 1);
        pnd_Tot_Ivc_Amt = localVariables.newFieldInRecord("pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Div_Amt = localVariables.newFieldInRecord("pnd_Tot_Div_Amt", "#TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Units = localVariables.newFieldInRecord("pnd_Tot_Units", "#TOT-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Tot_Units_Amt = localVariables.newFieldInRecord("pnd_Tot_Units_Amt", "#TOT-UNITS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Payees = localVariables.newFieldInRecord("pnd_Tot_Payees", "#TOT-PAYEES", FieldType.PACKED_DECIMAL, 6);
        pnd_Wrk_Payees = localVariables.newFieldInRecord("pnd_Wrk_Payees", "#WRK-PAYEES", FieldType.NUMERIC, 6);
        pnd_Tot_Payees_A = localVariables.newFieldInRecord("pnd_Tot_Payees_A", "#TOT-PAYEES-A", FieldType.NUMERIC, 6);
        pnd_Tot_Payees_M = localVariables.newFieldInRecord("pnd_Tot_Payees_M", "#TOT-PAYEES-M", FieldType.NUMERIC, 6);
        pnd_Cref_Dol = localVariables.newFieldInRecord("pnd_Cref_Dol", "#CREF-DOL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Fund_Cdes = localVariables.newFieldInRecord("pnd_Cref_Fund_Cdes", "#CREF-FUND-CDES", FieldType.STRING, 10);

        pnd_Cref_Fund_Cdes__R_Field_36 = localVariables.newGroupInRecord("pnd_Cref_Fund_Cdes__R_Field_36", "REDEFINE", pnd_Cref_Fund_Cdes);
        pnd_Cref_Fund_Cdes_Pnd_Cref_Array = pnd_Cref_Fund_Cdes__R_Field_36.newFieldArrayInGroup("pnd_Cref_Fund_Cdes_Pnd_Cref_Array", "#CREF-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1, 10));
        pnd_Dest_Cde = localVariables.newFieldArrayInRecord("pnd_Dest_Cde", "#DEST-CDE", FieldType.STRING, 4, new DbsArrayController(1, 8));
        pnd_Dest_Payees = localVariables.newFieldArrayInRecord("pnd_Dest_Payees", "#DEST-PAYEES", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 
            8));
        pnd_Dest_Per_Pay = localVariables.newFieldArrayInRecord("pnd_Dest_Per_Pay", "#DEST-PER-PAY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            8));
        pnd_Dest_Div_Pay = localVariables.newFieldArrayInRecord("pnd_Dest_Div_Pay", "#DEST-DIV-PAY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            8));
        pnd_Dest_Units = localVariables.newFieldArrayInRecord("pnd_Dest_Units", "#DEST-UNITS", FieldType.PACKED_DECIMAL, 11, 3, new DbsArrayController(1, 
            8));
        pnd_Dest_Units_Total = localVariables.newFieldArrayInRecord("pnd_Dest_Units_Total", "#DEST-UNITS-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            8));
        pnd_Dest_Total_Amt = localVariables.newFieldArrayInRecord("pnd_Dest_Total_Amt", "#DEST-TOTAL-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            8));
        pnd_Dest_Total_Ivc = localVariables.newFieldArrayInRecord("pnd_Dest_Total_Ivc", "#DEST-TOTAL-IVC", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            8));
        pnd_Dest_Occ = localVariables.newFieldInRecord("pnd_Dest_Occ", "#DEST-OCC", FieldType.PACKED_DECIMAL, 2);
        pnd_Dest_Payees_Grand = localVariables.newFieldInRecord("pnd_Dest_Payees_Grand", "#DEST-PAYEES-GRAND", FieldType.PACKED_DECIMAL, 7);
        pnd_Dest_Per_Pay_Grand = localVariables.newFieldInRecord("pnd_Dest_Per_Pay_Grand", "#DEST-PER-PAY-GRAND", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Dest_Div_Pay_Grand = localVariables.newFieldInRecord("pnd_Dest_Div_Pay_Grand", "#DEST-DIV-PAY-GRAND", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Dest_Units_Grand = localVariables.newFieldInRecord("pnd_Dest_Units_Grand", "#DEST-UNITS-GRAND", FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Dest_Units_Total_Grand = localVariables.newFieldInRecord("pnd_Dest_Units_Total_Grand", "#DEST-UNITS-TOTAL-GRAND", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Dest_Total_Amt_Grand = localVariables.newFieldInRecord("pnd_Dest_Total_Amt_Grand", "#DEST-TOTAL-AMT-GRAND", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dest_Total_Ivc_Grand = localVariables.newFieldInRecord("pnd_Dest_Total_Ivc_Grand", "#DEST-TOTAL-IVC-GRAND", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);

        ia_Aian026_Linkage = localVariables.newGroupInRecord("ia_Aian026_Linkage", "IA-AIAN026-LINKAGE");
        ia_Aian026_Linkage_Ia_Call_Type = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Call_Type", "IA-CALL-TYPE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Fund_Code = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Fund_Code", "IA-FUND-CODE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Reval_Methd = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Reval_Methd", "IA-REVAL-METHD", FieldType.STRING, 
            1);
        ia_Aian026_Linkage_Ia_Check_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date", "IA-CHECK-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_37 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_37", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Date_Ccyymm = ia_Aian026_Linkage__R_Field_37.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date_Ccyymm", "IA-CHECK-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        ia_Aian026_Linkage_Ia_Check_Dd = ia_Aian026_Linkage__R_Field_37.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Dd", "IA-CHECK-DD", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_38 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_38", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Year = ia_Aian026_Linkage__R_Field_38.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Year", "IA-CHECK-YEAR", FieldType.NUMERIC, 
            4);
        ia_Aian026_Linkage_Ia_Check_Month = ia_Aian026_Linkage__R_Field_38.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Month", "IA-CHECK-MONTH", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Check_Day = ia_Aian026_Linkage__R_Field_38.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Day", "IA-CHECK-DAY", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Issue_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date", "IA-ISSUE-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_39 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_39", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Ccyymm = ia_Aian026_Linkage__R_Field_39.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Ccyymm", "IA-ISSUE-CCYYMM", FieldType.NUMERIC, 
            6);
        ia_Aian026_Linkage_Ia_Issue_Day = ia_Aian026_Linkage__R_Field_39.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Day", "IA-ISSUE-DAY", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_40 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_40", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Date_A = ia_Aian026_Linkage__R_Field_40.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date_A", "IA-ISSUE-DATE-A", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code_11 = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Return_Code_11", "RETURN-CODE-11", FieldType.STRING, 
            11);

        ia_Aian026_Linkage__R_Field_41 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_41", "REDEFINE", ia_Aian026_Linkage_Return_Code_11);
        ia_Aian026_Linkage_Return_Code_Pgm = ia_Aian026_Linkage__R_Field_41.newFieldInGroup("ia_Aian026_Linkage_Return_Code_Pgm", "RETURN-CODE-PGM", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code = ia_Aian026_Linkage__R_Field_41.newFieldInGroup("ia_Aian026_Linkage_Return_Code", "RETURN-CODE", FieldType.NUMERIC, 
            3);
        ia_Aian026_Linkage_Auv_Returned = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Returned", "AUV-RETURNED", FieldType.NUMERIC, 8, 
            4);
        ia_Aian026_Linkage_Auv_Date_Return = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Date_Return", "AUV-DATE-RETURN", FieldType.NUMERIC, 
            8);
        ia_Aian026_Linkage_Days_In_Request_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Days_In_Request_Month", "DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        ia_Aian026_Linkage_Days_In_Particip_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Days_In_Particip_Month", "DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);

        pnd_Input_Record_Work_2 = localVariables.newGroupInRecord("pnd_Input_Record_Work_2", "#INPUT-RECORD-WORK-2");
        pnd_Input_Record_Work_2_Pnd_Call_Type = pnd_Input_Record_Work_2.newFieldInGroup("pnd_Input_Record_Work_2_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 
            1);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.STRING, 3);

        pnd_Mode__R_Field_42 = localVariables.newGroupInRecord("pnd_Mode__R_Field_42", "REDEFINE", pnd_Mode);
        pnd_Mode_Pnd_Filler = pnd_Mode__R_Field_42.newFieldInGroup("pnd_Mode_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Mode_Pnd_Mode_Mm = pnd_Mode__R_Field_42.newFieldInGroup("pnd_Mode_Pnd_Mode_Mm", "#MODE-MM", FieldType.NUMERIC, 2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Latest_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Latest_Factor", "#LATEST-FACTOR", FieldType.STRING, 1);
        pnd_Const_Pnd_Payment_Orign_Factor = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Payment_Orign_Factor", "#PAYMENT-ORIGN-FACTOR", FieldType.STRING, 
            1);
        pnd_Record_Length = localVariables.newFieldInRecord("pnd_Record_Length", "#RECORD-LENGTH", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
        pnd_Account_Key.getValue(1).setInitialValue("T011                   TIAA CREDIT ACCT 13080");
        pnd_Account_Key.getValue(2).setInitialValue("T011PMTIA              IA-IF PMT ACCT");
        pnd_Account_Key.getValue(3).setInitialValue("T011DIVIA              IA-IF DIV ACCT");
        pnd_Account_Key.getValue(4).setInitialValue("T011PMTIP              IP PMT");
        pnd_Account_Key.getValue(5).setInitialValue("T011DIVIP              IP DIV");
        pnd_Account_Key.getValue(6).setInitialValue("T011PMTS0              S0 PMT");
        pnd_Account_Key.getValue(7).setInitialValue("T011DIVS0              S0 DIV");
        pnd_Account_Key.getValue(8).setInitialValue("T011PMTS0              S0 PMT");
        pnd_Account_Key.getValue(9).setInitialValue("T011DIVS0              S0 DIV");
        pnd_Account_Key.getValue(10).setInitialValue("T011PMTS0              S0 PMT");
        pnd_Account_Key.getValue(11).setInitialValue("T011DIVS0              S0 DIV");
        pnd_Account_Key.getValue(12).setInitialValue("T011PMTW0              W0 PMT");
        pnd_Account_Key.getValue(13).setInitialValue("T011DIVW0              W0 DIV");
        pnd_Account_Key.getValue(14).setInitialValue("2021                   STOCK TIAA DEBIT ACCT");
        pnd_Account_Key.getValue(15).setInitialValue("2031                   MMA");
        pnd_Account_Key.getValue(16).setInitialValue("2041                   SOCIAL");
        pnd_Account_Key.getValue(17).setInitialValue("2061                   GLOBAL TIAA DEBIT");
        pnd_Account_Key.getValue(18).setInitialValue("2081                   GROWTH TIAA DEBIT");
        pnd_Account_Key.getValue(19).setInitialValue("2071                   EQUITY TIAA DEBIT");
        pnd_Account_Key.getValue(20).setInitialValue("08083112               UNKNOWN");
        pnd_Account_Key.getValue(21).setInitialValue("2051                   BOND");
        pnd_Account_Key.getValue(22).setInitialValue("2091                   INFLATION BOND");
        pnd_Account_Key.getValue(23).setInitialValue("2101                   TIAA ACCESS DEBIT");
        pnd_Account_Key.getValue(24).setInitialValue("0000     FOR FUTURE AS NEW PRODS");
        pnd_Account_Key.getValue(25).setInitialValue("0000     ARE ACTIVATED");
        pnd_Account_Key.getValue(26).setInitialValue("0000");
        pnd_Account_Key.getValue(27).setInitialValue("0000");
        pnd_Account_Key.getValue(28).setInitialValue("0000");
        pnd_Account_Key.getValue(29).setInitialValue("0000");
        pnd_Account_Key.getValue(30).setInitialValue("0000");
        pnd_Account_Key.getValue(31).setInitialValue("0000");
        pnd_Account_Key.getValue(32).setInitialValue("0000");
        pnd_Account_Key.getValue(33).setInitialValue("0000");
        pnd_Account_Key.getValue(34).setInitialValue("U000");
        pnd_Account_Key.getValue(35).setInitialValue("U092           REAL ESTATE 6M/N REA DEBIT&CREDIT");
        pnd_Account_Key.getValue(36).setInitialValue("U112           TIAA ACCESS 6M/N LRI DEBIT&CREDIT");
        pnd_Account_Key.getValue(37).setInitialValue("2022           STOCK       STK DR/CR & TIAA DEBIT");
        pnd_Account_Key.getValue(38).setInitialValue("2032           MMA         MMA DR/CR & TIAA DEBIT");
        pnd_Account_Key.getValue(39).setInitialValue("2042           SOCIAL      SOC DR/CR & TIAA DEBIT");
        pnd_Account_Key.getValue(40).setInitialValue("2052           BOND        BND DR/CR & ' '  DEBIT");
        pnd_Account_Key.getValue(41).setInitialValue("2062           GLOBAL      GLB DR/CR & ' '  DEBIT");
        pnd_Account_Key.getValue(42).setInitialValue("2082           GROWTH      GRW '  '' &  '  'DEBIT");
        pnd_Account_Key.getValue(43).setInitialValue("2072           EQUITY      EQT '  ' '&   ' 'DEBIT");
        pnd_Account_Key.getValue(44).setInitialValue("2092           ILB BOND    ILB '  ' '& TIAA DEBIT");
        pnd_Account_Num.getValue(1).setInitialValue("13080      TIAA CREDIT ACCT");
        pnd_Account_Num.getValue(2).setInitialValue("61055      IA-IF PMT ACCT");
        pnd_Account_Num.getValue(3).setInitialValue("61056      IA-IF DIV ACCT");
        pnd_Account_Num.getValue(4).setInitialValue("62555      IP PMT");
        pnd_Account_Num.getValue(5).setInitialValue("62556      IP DIV");
        pnd_Account_Num.getValue(6).setInitialValue("62655040    S0 PMT");
        pnd_Account_Num.getValue(7).setInitialValue("62656040    S0 DIV");
        pnd_Account_Num.getValue(8).setInitialValue("62655050    S0 PMT");
        pnd_Account_Num.getValue(9).setInitialValue("62656050    S0 DIV");
        pnd_Account_Num.getValue(10).setInitialValue("62655060    S0 PMT");
        pnd_Account_Num.getValue(11).setInitialValue("62656060    S0 DIV");
        pnd_Account_Num.getValue(12).setInitialValue("64163       W0 PMT");
        pnd_Account_Num.getValue(13).setInitialValue("64264       W0 DIV");
        pnd_Account_Num.getValue(14).setInitialValue("08090010    STOCK TIAA DEBIT ACCT");
        pnd_Account_Num.getValue(15).setInitialValue("08090022    MMA");
        pnd_Account_Num.getValue(16).setInitialValue("083222      SOCIAL");
        pnd_Account_Num.getValue(17).setInitialValue("083242    GLOBAL TIAA DEBIT");
        pnd_Account_Num.getValue(18).setInitialValue("083252    GROWTH TIAA DEBIT");
        pnd_Account_Num.getValue(19).setInitialValue("083262    EQUITY");
        pnd_Account_Num.getValue(20).setInitialValue("08083112");
        pnd_Account_Num.getValue(21).setInitialValue("083232    BOND TIAA DEBIT");
        pnd_Account_Num.getValue(22).setInitialValue("083272    ILB  TIAA DEBIT");
        pnd_Account_Num.getValue(23).setInitialValue("083281    LRI  TIAA DEBIT");
        pnd_Account_Num.getValue(24).setInitialValue("00000000  AS NEW PRODUCTS ACTIVATED");
        pnd_Account_Num.getValue(25).setInitialValue("00000000");
        pnd_Account_Num.getValue(26).setInitialValue("00000000");
        pnd_Account_Num.getValue(27).setInitialValue("00000000");
        pnd_Account_Num.getValue(28).setInitialValue("00000000");
        pnd_Account_Num.getValue(29).setInitialValue("00000000");
        pnd_Account_Num.getValue(30).setInitialValue("00000000");
        pnd_Account_Num.getValue(31).setInitialValue("00000000");
        pnd_Account_Num.getValue(32).setInitialValue("00000000");
        pnd_Account_Num.getValue(33).setInitialValue("00000000");
        pnd_Account_Num.getValue(34).setInitialValue("00000000");
        pnd_Account_Num.getValue(35).setInitialValue("R61055  R62555  R163102          REA DEBIT&CREDIT");
        pnd_Account_Num.getValue(36).setInitialValue("!61055  !62555  !163101          LRI DEBIT&CREDIT");
        pnd_Account_Num.getValue(37).setInitialValue("861055  862555  816041  08090010 ST DR/CR TIAA DR");
        pnd_Account_Num.getValue(38).setInitialValue("961055  962555  961041  08090022 MMA DR/CR TIAA DR");
        pnd_Account_Num.getValue(39).setInitialValue("A61055  A61255  A163102 083222   SOC DR/CR TIAA DR");
        pnd_Account_Num.getValue(40).setInitialValue("B61055  B61255  B163102 083232   BND DR/CR ' '  '");
        pnd_Account_Num.getValue(41).setInitialValue("C61055  C61255  C163102 083242   GLB DR/CR ' ' '");
        pnd_Account_Num.getValue(42).setInitialValue("D61055  D61255  D163102 083252   GRW '  '' '  '");
        pnd_Account_Num.getValue(43).setInitialValue("E61055  E61255  E163102 083262   EQT '  ' '''");
        pnd_Account_Num.getValue(44).setInitialValue("F61055  F61255  F163102 083272   ILB '  ' &TIAA DR");
        pnd_Account_Num.getValue(45).setInitialValue("00000000");
        pnd_Account_Num.getValue(46).setInitialValue("00000000");
        pnd_Account_Num.getValue(47).setInitialValue("00000000");
        pnd_Account_Num.getValue(48).setInitialValue("00000000");
        pnd_Account_Num.getValue(49).setInitialValue("00000000");
        pnd_Account_Num.getValue(50).setInitialValue("00000000");
        pnd_Account_Num.getValue(51).setInitialValue("00000000");
        pnd_Account_Num.getValue(52).setInitialValue("00000000");
        pnd_Account_Num.getValue(53).setInitialValue("00000000");
        pnd_Account_Num.getValue(54).setInitialValue("00000000");
        pnd_Account_Num.getValue(55).setInitialValue("00000000");
        pnd_Account_Num.getValue(56).setInitialValue("00000000");
        pnd_Account_Num.getValue(57).setInitialValue("00000000");
        pnd_Account_Num.getValue(58).setInitialValue("00000000");
        pnd_Account_Num.getValue(59).setInitialValue("00000000");
        pnd_Account_Num.getValue(60).setInitialValue("00000000");
        pnd_Prod_Cnt.setInitialValue(0);
        pnd_Max_Prd_Cde.setInitialValue(0);
        pnd_Cref_Fund_Cdes.setInitialValue("ACMSBWELRI");
        ia_Aian026_Linkage_Ia_Call_Type.setInitialValue("P");
        ia_Aian026_Linkage_Ia_Reval_Methd.setInitialValue("M");
        pnd_Const_Pnd_Latest_Factor.setInitialValue("L");
        pnd_Const_Pnd_Payment_Orign_Factor.setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap730() throws Exception
    {
        super("Iaap730");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP730", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        getReports().atTopOfPage(atTopEventRpt8, 8);
        getReports().atTopOfPage(atTopEventRpt9, 9);
        getReports().atTopOfPage(atTopEventRpt10, 10);
        getReports().atTopOfPage(atTopEventRpt11, 11);
        getReports().atTopOfPage(atTopEventRpt12, 12);
        setupReports();
        //*  ADDED 8/96                                                                                                                                                   //Natural: FORMAT PS = 55 LS = 133;//Natural: FORMAT ( 1 ) PS = 55 LS = 133;//Natural: FORMAT ( 2 ) PS = 55 LS = 133;//Natural: FORMAT ( 3 ) PS = 55 LS = 133;//Natural: FORMAT ( 4 ) PS = 55 LS = 133;//Natural: FORMAT ( 5 ) PS = 55 LS = 133;//Natural: FORMAT ( 6 ) PS = 55 LS = 133;//Natural: FORMAT ( 7 ) PS = 55 LS = 133;//Natural: FORMAT ( 8 ) PS = 55 LS = 133;//Natural: FORMAT ( 8 ) PS = 55 LS = 133;//Natural: FORMAT ( 9 ) PS = 55 LS = 133;//Natural: FORMAT ( 10 ) PS = 55 LS = 133;//Natural: FORMAT ( 11 ) PS = 55 LS = 133;//Natural: FORMAT ( 12 ) PS = 55 LS = 133
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN0500-READ-EXTERN-FILE
        sub_Call_Iaan0500_Read_Extern_File();
        if (condition(Global.isEscape())) {return;}
        //*  CALL TYPE TO BE SENT TO (AIAN026) TO GET THE ANNUITY UNIT VALUE
        getWorkFiles().read(2, pnd_Input_Record_Work_2);                                                                                                                  //Natural: READ WORK 2 ONCE #INPUT-RECORD-WORK-2
        //*  LATEST OR PAYMENT FACTOR
        if (condition(! (pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor) || pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Payment_Orign_Factor)))) //Natural: IF NOT ( #CALL-TYPE = #LATEST-FACTOR OR = #PAYMENT-ORIGN-FACTOR )
        {
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*           ERROR READING WORK FILE 2");                                                                                               //Natural: WRITE '*           ERROR READING WORK FILE 2'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                                         //Natural: WRITE '*' '=' #CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*  CALL TYPE MUST BE 'L' OR 'P' ");                                                                                                    //Natural: WRITE '*  CALL TYPE MUST BE "L" OR "P" '
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        //*    MAIN LOGIC
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY
        (
        "READ01",
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Cntrl_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CNTRL-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   MADE CHANGES TO THIS EPORT EGTRRA 1/03
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*   MADE CHANGES TO THIS REPORT EGTRRA 1/03
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 7 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 8 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 9 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 10 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 11 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 12 )
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   SHEADER")  //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("99STRAILER"))) //Natural: IF #CNTRCT-PPCN-NBR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet668 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
            if (condition((pnd_Input_Record_Pnd_Record_Code.equals(10))))
            {
                decideConditionsMet668++;
                pnd_Dte_A.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte);                                                                                                //Natural: ASSIGN #DTE-A := #CNTRCT-ISSUE-DTE
                iaaa730a_Rec_Out_Da_Contract.setValue(pnd_Input_Record_Pnd_Cntrct_Orig_Da_Cntrct_Nbr);                                                                    //Natural: ASSIGN DA-CONTRACT := #CNTRCT-ORIG-DA-CNTRCT-NBR
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                pnd_Save_Cntrct_Optn_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Optn_Cde);                                                                                  //Natural: ASSIGN #SAVE-CNTRCT-OPTN-CDE := #INPUT-RECORD.#CNTRCT-OPTN-CDE
                pnd_Save_Cntrct_Orgn_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Orgn_Cde);                                                                                  //Natural: ASSIGN #SAVE-CNTRCT-ORGN-CDE := #INPUT-RECORD.#CNTRCT-ORGN-CDE
                pnd_Save_Cntrct_Crrncy_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde);                                                                              //Natural: ASSIGN #SAVE-CNTRCT-CRRNCY-CDE := #INPUT-RECORD.#CNTRCT-CRRNCY-CDE
                pnd_Save_Issue_Dte.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte);                                                                                       //Natural: ASSIGN #SAVE-ISSUE-DTE := #CNTRCT-ISSUE-DTE
                pnd_Save_Issue_Dte_Dd.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd);                                                                                 //Natural: ASSIGN #SAVE-ISSUE-DTE-DD := #CNTRCT-ISSUE-DTE-DD
                pnd_Save_Pnd_Cntrct_Inst_Iss_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde);                                                                      //Natural: ASSIGN #SAVE-#CNTRCT-INST-ISS-CDE := #CNTRCT-INST-ISS-CDE
                pnd_Save_Cntrct_Dob.setValue(pnd_Input_Record_Pnd_Cntrct_Dob);                                                                                            //Natural: ASSIGN #SAVE-CNTRCT-DOB := #CNTRCT-DOB
                pnd_Save_Cntrct_Name.setValue(pnd_Input_Record_Pnd_Cntrct_Name);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-NAME := #CNTRCT-NAME
                pnd_Save_Part_Ppcn_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                                    //Natural: ASSIGN #SAVE-PART-PPCN-NBR := #CNTRCT-PPCN-NBR
                //* **********************************************************
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Record_Pnd_Record_Code.equals(20))))
            {
                decideConditionsMet668++;
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                if (condition(((((pnd_Save_Cntrct_Optn_Cde.equals(21) || pnd_Save_Cntrct_Optn_Cde.equals(22)) || pnd_Save_Cntrct_Optn_Cde.equals(25))                     //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21 OR #SAVE-CNTRCT-OPTN-CDE = 22 OR #SAVE-CNTRCT-OPTN-CDE = 25 OR #SAVE-CNTRCT-OPTN-CDE = 27 OR #SAVE-CNTRCT-OPTN-CDE = 28 THRU 30
                    || pnd_Save_Cntrct_Optn_Cde.equals(27)) || (pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cpr_Status_Cde.equals(9)))                                                                                             //Natural: IF #CPR-STATUS-CDE = 9
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED OR = '57BT' OR  = '57BC' TO FOLLOWING 1/03
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    if (condition(pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("IRAC")                    //Natural: IF #CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC' OR = 'RINV' OR = 'CREF' OR = 'FIDE' OR = 'CASH' OR = 'DTRA'
                        || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("03BT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("QPLT") 
                        || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("QPLC") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("57BC") 
                        || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("CREF") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("FIDE") 
                        || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("DTRA")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #BYPASS := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    if (condition(pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27)))                                                            //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 25 OR #SAVE-CNTRCT-OPTN-CDE = 27
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-WARNING-REPORT
                        sub_Write_Warning_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED OR = '57BT' OR  = '57BC' TO FOLLOWING 1/03
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21) || pnd_Save_Cntrct_Optn_Cde.equals(22) || pnd_Save_Cntrct_Optn_Cde.equals(25) ||                    //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21 OR #SAVE-CNTRCT-OPTN-CDE = 22 OR #SAVE-CNTRCT-OPTN-CDE = 25 OR #SAVE-CNTRCT-OPTN-CDE = 27
                        pnd_Save_Cntrct_Optn_Cde.equals(27)))
                    {
                        //*  ADDED 1/03
                        if (condition(pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("IRAC")                //Natural: IF #CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                            || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("03BT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("QPLT") 
                            || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("QPLC") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde.equals("57BC")))
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Bypass.setValue(true);                                                                                                                    //Natural: ASSIGN #BYPASS := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-PAYMENT-DUE
                    sub_Check_Payment_Due();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                                                                                                                                                                          //Natural: PERFORM SAVE-CPR
                        sub_Save_Cpr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mode.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                                  //Natural: ASSIGN #MODE := #CNTRCT-MODE-IND
                //*  CREATE FILE FOR DET TPA RPT
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    if (condition(pnd_Dte_A_Pnd_Yyy.lessOrEqual(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy)))                                                                         //Natural: IF #YYY NOT GT #CNTRL-YYYY
                    {
                        if (condition(pnd_Mode_Pnd_Mode_Mm.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm)))                                                                         //Natural: IF #MODE-MM = #CNTRL-MM
                        {
                            if (condition(pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))                                       //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 28 THRU 30
                            {
                                if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("DTRA"))) //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'CASH' OR = 'RINV' OR = 'DTRA'
                                {
                                    //*  INPUT TO IAAP730A
                                                                                                                                                                          //Natural: PERFORM LOAD-WORK-FILE
                                    sub_Load_Work_File();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ******************************************************************
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Input_Record_Pnd_Record_Code.equals(30))))
            {
                decideConditionsMet668++;
                //*  ADDED OR = '57BT' OR  = '57BC' TO FOLLOWING 1/03
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    //*  ADDED 1/03
                    if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BT")  //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                        || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLC") 
                        || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC")))
                    {
                        if (condition((((pnd_Save_Cntrct_Optn_Cde.equals(22) || pnd_Save_Cntrct_Optn_Cde.equals(25)) || pnd_Save_Cntrct_Optn_Cde.equals(27))              //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 22 OR #SAVE-CNTRCT-OPTN-CDE = 25 OR #SAVE-CNTRCT-OPTN-CDE = 27 OR #SAVE-CNTRCT-OPTN-CDE = 28 THRU 30
                            || (pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))))
                        {
                            if (condition(pnd_Save_Cntrct_Pend_Cde.equals("0") || pnd_Save_Cntrct_Pend_Cde.equals(" ")))                                                  //Natural: IF #SAVE-CNTRCT-PEND-CDE = '0' OR #SAVE-CNTRCT-PEND-CDE = ' '
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM WRITE-PEND-REPORT
                                sub_Write_Pend_Report();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Bypass.setValue(true);                                                                                                                //Natural: ASSIGN #BYPASS := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Bypass.getBoolean()) && pnd_Payment_Due.getBoolean()))                                                                               //Natural: IF NOT #BYPASS AND #PAYMENT-DUE
                {
                                                                                                                                                                          //Natural: PERFORM SAVE-FUND-SUMM
                    sub_Save_Fund_Summ();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND
                    sub_Process_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-1
                    sub_Print_Report_1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //* **11/2000 IK
                if (condition(! (pnd_Bypass.getBoolean())))                                                                                                               //Natural: IF NOT #BYPASS
                {
                    if (condition(pnd_Dte_A_Pnd_Yyy.lessOrEqual(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy)))                                                                         //Natural: IF #YYY NOT GT #CNTRL-YYYY
                    {
                        if (condition(pnd_Mode_Pnd_Mode_Mm.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm)))                                                                         //Natural: IF #MODE-MM = #CNTRL-MM
                        {
                            if (condition(pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(29) || pnd_Save_Cntrct_Optn_Cde.equals(30)))             //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 28 OR = 29 OR = 30
                            {
                                //*  ADD 11122001
                                //*  ADDED 1/03
                                if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("DTRA")  //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'CASH' OR = 'RINV' OR = 'DTRA' OR = 'IRAC' OR = 'IRAT' OR = 'IRAX' OR = '57BT' OR = '57BC' OR = '57BX'
                                    || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAX") 
                                    || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BX")))
                                {
                                    iaaa730a_Rec_Out_Contractual_Amt.setValue(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                       //Natural: ASSIGN CONTRACTUAL-AMT := #SUMM-PER-PYMNT
                                    iaaa730a_Rec_Out_Per_Dividend.setValue(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                          //Natural: ASSIGN PER-DIVIDEND := #SUMM-PER-DVDND
                                    if (condition(iaaa730a_Rec_Out_Ia_Contract.notEquals(" ")))                                                                           //Natural: IF IA-CONTRACT NE ' '
                                    {
                                        getWorkFiles().write(3, false, iaaa730a_Rec_Out);                                                                                 //Natural: WRITE WORK FILE 3 IAAA730A-REC-OUT
                                        iaaa730a_Rec_Out.reset();                                                                                                         //Natural: RESET IAAA730A-REC-OUT
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PEND-REPORT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WARNING-REPORT
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-WORK-FILE
        //* **************************************************************
        //*  ANNUITY CERTAIN REPORT TOTAL LINE
        //*  ADDED TOTAL-IVC 1/03
        //* K 6/11
        //*  ADDED  1/03
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(28),"CONTRACTUAL",NEWLINE,new ColumnSpacing(29),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new //Natural: WRITE ( 4 ) / 28X 'CONTRACTUAL' / 29X 'AMOUNT' 7X 'DIVIDEND' 7X 'UNITS' 8X 'TOTAL' 9X 'TOTAL-IVC' /14X 'TOTAL' 10X #T-PER-AMT ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #T-DIV-AMT ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) #T-UNITS ( 1 ) ( EM = Z,ZZZ,ZZ9.999 ) #T-AMT ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #T-IVC ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 4X 'PAYEES' 2X #T-PAYEES ( 1 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(7),"UNITS",new ColumnSpacing(8),"TOTAL",new ColumnSpacing(9),"TOTAL-IVC",NEWLINE,new ColumnSpacing(14),"TOTAL",new ColumnSpacing(10),pnd_T_Per_Amt.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_T_Div_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"),pnd_T_Units.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZ9.999"),pnd_T_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_T_Ivc.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(4),"PAYEES",new ColumnSpacing(2),pnd_T_Payees.getValue(1), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  P&I OPT 22 REPORT TOTAL LINE
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(28),"CONTRACTUAL",NEWLINE,new ColumnSpacing(29),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new //Natural: WRITE ( 2 ) / 28X 'CONTRACTUAL' / 29X 'AMOUNT' 7X 'DIVIDEND' 20X 'TOTAL' /11X 'TOTAL' 9X #T-PER-AMT ( 2 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #T-DIV-AMT ( 2 ) ( EM = Z,ZZZ,ZZ9.99 ) 16X #T-AMT ( 2 ) ( EM = Z,ZZZ,ZZ9.99 ) 4X 'PAYEES' 2X #T-PAYEES ( 2 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(20),"TOTAL",NEWLINE,new ColumnSpacing(11),"TOTAL",new ColumnSpacing(9),pnd_T_Per_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_T_Div_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(16),pnd_T_Amt.getValue(2), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(4),"PAYEES",new ColumnSpacing(2),pnd_T_Payees.getValue(2), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  IPRO REPORT TOTAL LINE
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(28),"CONTRACTUAL",NEWLINE,new ColumnSpacing(29),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new //Natural: WRITE ( 3 ) / 28X 'CONTRACTUAL' / 29X 'AMOUNT' 7X 'DIVIDEND' 20X 'TOTAL' /11X 'TOTAL' 7X #T-PER-AMT ( 3 ) ( EM = Z,ZZZ,ZZ9.99 ) 3X #T-DIV-AMT ( 3 ) ( EM = Z,ZZZ,ZZ9.99 ) 14X #T-AMT ( 3 ) ( EM = Z,ZZZ,ZZ9.99 ) 4X 'PAYEES' 2X #T-PAYEES ( 3 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(20),"TOTAL",NEWLINE,new ColumnSpacing(11),"TOTAL",new ColumnSpacing(7),pnd_T_Per_Amt.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),pnd_T_Div_Amt.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_T_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(4),"PAYEES",new ColumnSpacing(2),pnd_T_Payees.getValue(3), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  TPA REPORT TOTAL LINE
        //*  ADDED TOTAL-IVC 1/03
        //*  ADDED  1/03
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(28),"CONTRACTUAL",NEWLINE,new ColumnSpacing(29),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new //Natural: WRITE ( 1 ) / 28X 'CONTRACTUAL' / 29X 'AMOUNT' 7X 'DIVIDEND' 9X 'TOTAL' 7X 'TOTAL-IVC' /11X 'TOTAL' 9X #T-PER-AMT ( 4 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #T-DIV-AMT ( 4 ) ( EM = Z,ZZZ,ZZ9.99 ) 5X #T-AMT ( 4 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #T-IVC ( 4 ) ( EM = Z,ZZZ,ZZ9.99 ) 4X 'PAYEES' 2X #T-PAYEES ( 4 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(9),"TOTAL",new ColumnSpacing(7),"TOTAL-IVC",NEWLINE,new ColumnSpacing(11),"TOTAL",new ColumnSpacing(9),pnd_T_Per_Amt.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_T_Div_Amt.getValue(4), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_T_Amt.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_T_Ivc.getValue(4), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(4),"PAYEES",new 
            ColumnSpacing(2),pnd_T_Payees.getValue(4), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  GRAND TOTAL INTERNAL ROLL REPORT
        //*  ADDED 1/03
        getReports().write(5, ReportOption.NOTITLE,new ColumnSpacing(7),"GRAND TOTAL",new ColumnSpacing(7),pnd_Tot_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),pnd_Tot_Div_Amt,  //Natural: WRITE ( 5 ) 7X 'GRAND TOTAL' 7X #TOT-PER-AMT ( EM = Z,ZZZ,ZZ9.99 ) #TOT-DIV-AMT ( EM = Z,ZZZ,ZZ9.99 ) #TOT-UNITS ( EM = Z,ZZZ,ZZ9.999 ) #TOTAL-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) #TOT-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 ) 4X 'PAYEES' 2X #TOT-PAYEES ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),pnd_Tot_Units, new ReportEditMask ("Z,ZZZ,ZZ9.999"),pnd_Total_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Tot_Ivc_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(4),"PAYEES",new ColumnSpacing(2),pnd_Tot_Payees, new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(10, ReportOption.NOTITLE,new ColumnSpacing(7),"GRAND TOTAL",new ColumnSpacing(5),pnd_T_Tot_P_P.getValue(1), new ReportEditMask                 //Natural: WRITE ( 10 ) 7X 'GRAND TOTAL' 5X #T-TOT-P-P ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) #T-TOT-D-P ( 1 ) ( EM = Z,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9.99"),pnd_T_Tot_D_P.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(11, ReportOption.NOTITLE,new ColumnSpacing(7),"GRAND TOTAL",new ColumnSpacing(5),pnd_T_Tot_P_P.getValue(2), new ReportEditMask                 //Natural: WRITE ( 11 ) 7X 'GRAND TOTAL' 5X #T-TOT-P-P ( 2 ) ( EM = Z,ZZZ,ZZ9.99 ) #T-TOT-D-P ( 2 ) ( EM = Z,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9.99"),pnd_T_Tot_D_P.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(12, ReportOption.NOTITLE,new ColumnSpacing(7),"GRAND TOTAL",new ColumnSpacing(5),pnd_T_Tot_P_P.getValue(3), new ReportEditMask                 //Natural: WRITE ( 12 ) 7X 'GRAND TOTAL' 5X #T-TOT-P-P ( 3 ) ( EM = Z,ZZZ,ZZ9.99 ) #T-TOT-D-P ( 3 ) ( EM = Z,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9.99"),pnd_T_Tot_D_P.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-2
        sub_Print_Report_2();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-3
        sub_Print_Report_3();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-8
        sub_Print_Report_8();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-9
        sub_Print_Report_9();
        if (condition(Global.isEscape())) {return;}
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-CPR
        //* **************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-FUND-SUMM
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-1
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-2
        //* **********************************************************************
        //*  11/2000 IK
        //* *******************************************************
        //* *********************************************************
        //* ***********************************************************
        //* ***********************************************************
        //* *******************
        //*  ADDED FOLLOWING  1/98
        //*  END OF ADD 1/98
        //*  11/08 ADD TIAA ACCESS FUND
        //*  END OF ADD 11/08
        //*   ADDED FOLLOWING PRINT CREF PRODS    8/96
        //*    60T IAAA730.CREF-FUND-PAYEES(#I)(EM=ZZZZ,ZZZ,ZZZ,ZZ9)
        //*  END OF ADD 1/98
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-3
        //* **********************************************************************
        //*  ==> TOTAL BY DEST REPORT FOR INTERNAL ROLLOVERS
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-DIST
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FUND
        //*  END OF ADD  1/98
        //*   END OF ADD  1/98
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-LEDGER-TIAA
        //*  ADDED FOLLOWING NEW ANNUITIZATION 8/96
        //*  END OF ADD  8/96
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-LEDGER-CREF
        //*  ADDED FOLLOWING FOR CREF MONTHLY UNIT VALUES      1/98
        //*  ACCUM MONTHLY & ANNUAL IN SAME LEDGER ACCOUNT#
        //*  END OF ADD  1/98
        //*  ACCUM CREF  CREDITS INTO TIAA DEBIT ACCT#
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-LEDGER-FINAL
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-8
        //*  PRINT TIAA LEDGER REPORT
        //* **********************************************************************
        //*  ---------------------------------------------
        //*   PRINT CREF LEDGER REPORT
        //*  ---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-9
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PAYMENT-DUE
        //*  *****************************************************
        //*   READS EXTERNALIZATION FILE FOR VALID PRODUCT CODES
        //*  *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN0500-READ-EXTERN-FILE
        //*   ADDED FOLLOWING ROUTINE CREF MONTHLY UNIT VALUE   1/98
        //*  ***********************************************************
        //*   GET UNIT VALUE FOR MONTHLY UNIT VALUE CREF CONTRACTS
        //*  ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN026-CREF-UNIT-VALUE
        //*  ****************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Write_Pend_Report() throws Exception                                                                                                                 //Natural: WRITE-PEND-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))                                                           //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 28 THRU 30
        {
            getReports().write(10, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(2),pnd_Save_Part_Ppcn_Nbr,new   //Natural: WRITE ( 10 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 2X #SAVE-PART-PPCN-NBR 2X #SUMM-PER-PYMNT ( EM = ZZZ,ZZ9.99 ) 3X #SUMM-PER-DVDND ( EM = ZZZ,ZZ9.99 )
                ColumnSpacing(2),pnd_Input_Record_Pnd_Summ_Per_Pymnt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Input_Record_Pnd_Summ_Per_Dvdnd, 
                new ReportEditMask ("ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_T_Tot_P_P.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                          //Natural: ASSIGN #T-TOT-P-P ( 1 ) := #T-TOT-P-P ( 1 ) + #SUMM-PER-PYMNT
            pnd_T_Tot_D_P.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                          //Natural: ASSIGN #T-TOT-D-P ( 1 ) := #T-TOT-D-P ( 1 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27)))                                                                        //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 25 OR #SAVE-CNTRCT-OPTN-CDE = 27
        {
            getReports().write(11, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(2),pnd_Save_Part_Ppcn_Nbr,new   //Natural: WRITE ( 11 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 2X #SAVE-PART-PPCN-NBR 2X #SUMM-PER-PYMNT ( EM = ZZZ,ZZ9.99 ) 3X #SUMM-PER-DVDND ( EM = ZZZ,ZZ9.99 )
                ColumnSpacing(2),pnd_Input_Record_Pnd_Summ_Per_Pymnt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Input_Record_Pnd_Summ_Per_Dvdnd, 
                new ReportEditMask ("ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_T_Tot_P_P.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                          //Natural: ASSIGN #T-TOT-P-P ( 2 ) := #T-TOT-P-P ( 2 ) + #SUMM-PER-PYMNT
            pnd_T_Tot_D_P.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                          //Natural: ASSIGN #T-TOT-D-P ( 2 ) := #T-TOT-D-P ( 2 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(22)))                                                                                                               //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 22
        {
            getReports().write(12, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(2),pnd_Save_Part_Ppcn_Nbr,new   //Natural: WRITE ( 12 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 2X #SAVE-PART-PPCN-NBR 2X #SUMM-PER-PYMNT ( EM = ZZZ,ZZ9.99 ) 3X #SUMM-PER-DVDND ( EM = ZZZ,ZZ9.99 )
                ColumnSpacing(2),pnd_Input_Record_Pnd_Summ_Per_Pymnt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Input_Record_Pnd_Summ_Per_Dvdnd, 
                new ReportEditMask ("ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_T_Tot_P_P.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                          //Natural: ASSIGN #T-TOT-P-P ( 3 ) := #T-TOT-P-P ( 3 ) + #SUMM-PER-PYMNT
            pnd_T_Tot_D_P.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                          //Natural: ASSIGN #T-TOT-D-P ( 3 ) := #T-TOT-D-P ( 3 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Warning_Report() throws Exception                                                                                                              //Natural: WRITE-WARNING-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ipro_Warning_Rpt_Accumulators_Ipro_Recs_In.nadd(1);                                                                                                               //Natural: ADD 1 TO IPRO-RECS-IN
        fst_Ann_Dob.setValue(pnd_Save_Cntrct_Dob);                                                                                                                        //Natural: MOVE #SAVE-CNTRCT-DOB TO FST-ANN-DOB
        if (condition(fst_Ann_Dob_Fst_Ann_Dob_Dd.greater(1)))                                                                                                             //Natural: IF FST-ANN-DOB-DD GT 1
        {
            fst_Ann_Dob_Fst_Ann_Dob_Mm.nadd(1);                                                                                                                           //Natural: ADD 1 TO FST-ANN-DOB-MM
        }                                                                                                                                                                 //Natural: END-IF
        fst_Ann_Dob_Fst_Ann_Dob_Yr.nadd(70);                                                                                                                              //Natural: ADD 70 TO FST-ANN-DOB-YR
        fst_Ann_Dob_Fst_Ann_Dob_Mm.nadd(6);                                                                                                                               //Natural: ADD 6 TO FST-ANN-DOB-MM
        if (condition(fst_Ann_Dob_Fst_Ann_Dob_Mm.greater(12)))                                                                                                            //Natural: IF FST-ANN-DOB-MM > 12
        {
            fst_Ann_Dob_Fst_Ann_Dob_Yr.nadd(1);                                                                                                                           //Natural: ADD 1 TO FST-ANN-DOB-YR
        }                                                                                                                                                                 //Natural: END-IF
        fst_Ann_Dob_Fst_Ann_Dob_Mm.setValue(4);                                                                                                                           //Natural: MOVE 4 TO FST-ANN-DOB-MM
        if (condition(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte.equals(999999)))                                                                                          //Natural: IF #CNTRCT-FINAL-PER-DTE = 999999
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte.greater(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm)))                                                           //Natural: IF #CNTRCT-FINAL-PER-DTE > #CNTRL-YYYYMM
            {
                ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Hi.nadd(1);                                                                                                   //Natural: ADD 1 TO IPRO-FIN-DATE-HI
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm)))                                                                //Natural: IF #CNTRCT-FINAL-PER-DTE = #CNTRL-YYYYMM
        {
            ipro_Warning_Rpt_Accumulators_Ipro_Fin_Date_Eq.nadd(1);                                                                                                       //Natural: ADD 1 TO IPRO-FIN-DATE-EQ
            pnd_Ipro_Output_Ipro_Msg.setValue("FIN-PMT-DATE EQ  TO CK-PMT-DATE");                                                                                         //Natural: MOVE 'FIN-PMT-DATE EQ  TO CK-PMT-DATE' TO IPRO-MSG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm.equals(fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm)))                                                                        //Natural: IF #CNTRL-YYYYMM = FST-ANN-DOB-YR-MM
            {
                ipro_Warning_Rpt_Accumulators_Ipro_Age_Eq_70_1_2.nadd(1);                                                                                                 //Natural: ADD 1 TO IPRO-AGE-EQ-70-1-2
                pnd_Ipro_Output_Ipro_Msg.setValue("FIN-PMT-DATE LE THAN CK-PMT-DATE & AGE EQ 70 1/2  ");                                                                  //Natural: MOVE 'FIN-PMT-DATE LE THAN CK-PMT-DATE & AGE EQ 70 1/2  ' TO IPRO-MSG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm.less(fst_Ann_Dob_Fst_Ann_Dob_Yr_Mm)))                                                                      //Natural: IF #CNTRL-YYYYMM < FST-ANN-DOB-YR-MM
                {
                    ipro_Warning_Rpt_Accumulators_Ipro_Age_Under_70.nadd(1);                                                                                              //Natural: ADD 1 TO IPRO-AGE-UNDER-70
                    pnd_Ipro_Output_Ipro_Msg.setValue("FIN-PMT-DATE LE THAN CK-PMT-DATE & UNDER 70 1/2  ");                                                               //Natural: MOVE 'FIN-PMT-DATE LE THAN CK-PMT-DATE & UNDER 70 1/2  ' TO IPRO-MSG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ipro_Warning_Rpt_Accumulators_Ipro_Age_Over_70.nadd(1);                                                                                               //Natural: ADD 1 TO IPRO-AGE-OVER-70
                    pnd_Ipro_Output_Ipro_Msg.setValue("FIN-PMT-DATE LE THAN CK-PMT-DATE & OVER 70 1/2  ");                                                                //Natural: MOVE 'FIN-PMT-DATE LE THAN CK-PMT-DATE & OVER 70 1/2  ' TO IPRO-MSG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte.equals(999999)))                                                                                          //Natural: IF #CNTRCT-FINAL-PER-DTE = 999999
        {
            pnd_Ipro_Output_Ipro_Msg.setValue("FIN-PMT-DATE IS 999999");                                                                                                  //Natural: MOVE 'FIN-PMT-DATE IS 999999' TO IPRO-MSG
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ipro_Output_Ipro_Contract.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                                     //Natural: MOVE #CNTRCT-PPCN-NBR TO IPRO-CONTRACT
        pnd_Ipro_Output_Ipro_Payee.setValue(pnd_Save_Payee_Cde);                                                                                                          //Natural: MOVE #SAVE-PAYEE-CDE TO IPRO-PAYEE
        pnd_Ipro_Output_Ipro_Pend_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Pend_Cde);                                                                                     //Natural: MOVE #CNTRCT-PEND-CDE TO IPRO-PEND-CDE
        //*  MOVE  #SAVE-CNTRCT-PEND-CDE      TO  IPRO-PEND-CDE
        pnd_Ipro_Output_Ipro_Hold_Cde.setValue(pnd_Save_Cntrct_Hold_Cde);                                                                                                 //Natural: MOVE #SAVE-CNTRCT-HOLD-CDE TO IPRO-HOLD-CDE
        pnd_Ipro_Output_Ipro_Name.setValue(pnd_Save_Cntrct_Name);                                                                                                         //Natural: MOVE #SAVE-CNTRCT-NAME TO IPRO-NAME
        pnd_Ipro_Output_Ipro_Dob_Redf.setValue(pnd_Save_Cntrct_Dob);                                                                                                      //Natural: MOVE #SAVE-CNTRCT-DOB TO IPRO-DOB-REDF
        pnd_Ipro_Output_Ipro_Issue_Dte_Yy_Mm.setValue(pnd_Save_Issue_Dte);                                                                                                //Natural: MOVE #SAVE-ISSUE-DTE TO IPRO-ISSUE-DTE-YY-MM
        pnd_Ipro_Output_Ipro_Fin_Pmt_Yy_Mm.setValue(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte);                                                                           //Natural: MOVE #CNTRCT-FINAL-PER-DTE TO IPRO-FIN-PMT-YY-MM
        getReports().write(9, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),pnd_Ipro_Output_Ipro_Contract,new TabSetting(16),pnd_Ipro_Output_Ipro_Payee,new      //Natural: WRITE ( 9 ) / /3T IPRO-CONTRACT 16T IPRO-PAYEE 23T IPRO-PEND-CDE 5X IPRO-HOLD-CDE 5X IPRO-NAME 2X IPRO-DOB-REDF ( EM = 9999/99/99 ) 2X IPRO-ISSUE-DTE-YY-MM ( EM = 9999/99/01 ) 3X IPRO-FIN-PMT-YY-MM ( EM = 9999/99/01 ) 2X IPRO-MSG
            TabSetting(23),pnd_Ipro_Output_Ipro_Pend_Cde,new ColumnSpacing(5),pnd_Ipro_Output_Ipro_Hold_Cde,new ColumnSpacing(5),pnd_Ipro_Output_Ipro_Name,new 
            ColumnSpacing(2),pnd_Ipro_Output_Ipro_Dob_Redf, new ReportEditMask ("9999/99/99"),new ColumnSpacing(2),pnd_Ipro_Output_Ipro_Issue_Dte_Yy_Mm, 
            new ReportEditMask ("9999/99/01"),new ColumnSpacing(3),pnd_Ipro_Output_Ipro_Fin_Pmt_Yy_Mm, new ReportEditMask ("9999/99/01"),new ColumnSpacing(2),
            pnd_Ipro_Output_Ipro_Msg);
        if (Global.isEscape()) return;
    }
    private void sub_Load_Work_File() throws Exception                                                                                                                    //Natural: LOAD-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        iaaa730a_Rec_Out_Soc_Sec.setValue(pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr);                                                                                       //Natural: ASSIGN SOC-SEC := #PRTCPNT-TAX-ID-NBR
        iaaa730a_Rec_Out_Ia_Contract.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                                      //Natural: ASSIGN IA-CONTRACT := #CNTRCT-PPCN-NBR
        iaaa730a_Rec_Out_Distination_Prev.setValue(pnd_Save_Cntrct_Prev_Dist_Cde);                                                                                        //Natural: ASSIGN DISTINATION-PREV := #SAVE-CNTRCT-PREV-DIST-CDE
        iaaa730a_Rec_Out_Distination_Curr.setValue(pnd_Save_Cntrct_Curr_Dist_Cde);                                                                                        //Natural: ASSIGN DISTINATION-CURR := #SAVE-CNTRCT-CURR-DIST-CDE
        iaaa730a_Rec_Out_Cash_Cd.setValue(pnd_Save_Cntrct_Cash_Cde);                                                                                                      //Natural: ASSIGN CASH-CD := #SAVE-CNTRCT-CASH-CDE
        iaaa730a_Rec_Out_Term_Cd.setValue(pnd_Save_Cntrct_Xfr_Term_Cde);                                                                                                  //Natural: ASSIGN TERM-CD := #SAVE-CNTRCT-XFR-TERM-CDE
        iaaa730a_Rec_Out_Hold_Cd.setValue(pnd_Save_Cntrct_Hold_Cde);                                                                                                      //Natural: ASSIGN HOLD-CD := #SAVE-CNTRCT-HOLD-CDE
        iaaa730a_Rec_Out_Pend_Cd.setValue(pnd_Save_Cntrct_Pend_Cde);                                                                                                      //Natural: ASSIGN PEND-CD := #SAVE-CNTRCT-PEND-CDE
        iaaa730a_Rec_Out_Check_Date.setValue(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm);                                                                                           //Natural: ASSIGN CHECK-DATE := #CNTRL-YYYYMM
        iaaa730a_Rec_Out_Mode.setValue(pnd_Save_Cntrct_Mode_Ind);                                                                                                         //Natural: ASSIGN MODE := #SAVE-CNTRCT-MODE-IND
        iaaa730a_Rec_Out_Coll_Cd.setValue(pnd_Save_Pnd_Cntrct_Inst_Iss_Cde);                                                                                              //Natural: ASSIGN COLL-CD := #SAVE-#CNTRCT-INST-ISS-CDE
    }
    private void sub_Save_Cpr() throws Exception                                                                                                                          //Natural: SAVE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Payee_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                                               //Natural: ASSIGN #SAVE-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Save_Prtcpnt_Tax_Id_Nbr.setValue(pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr);                                                                                    //Natural: ASSIGN #SAVE-PRTCPNT-TAX-ID-NBR := #PRTCPNT-TAX-ID-NBR
        pnd_Save_Cntrct_Mode_Ind.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-MODE-IND := #CNTRCT-MODE-IND
        pnd_Save_Cpr_Status_Cde.setValue(pnd_Input_Record_Pnd_Cpr_Status_Cde);                                                                                            //Natural: ASSIGN #SAVE-CPR-STATUS-CDE := #CPR-STATUS-CDE
        pnd_Save_Cntrct_Prev_Dist_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde);                                                                                //Natural: ASSIGN #SAVE-CNTRCT-PREV-DIST-CDE := #CNTRCT-PREV-DIST-CDE
        pnd_Save_Cntrct_Curr_Dist_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde);                                                                                //Natural: ASSIGN #SAVE-CNTRCT-CURR-DIST-CDE := #CNTRCT-CURR-DIST-CDE
        pnd_Save_Cntrct_Cash_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Cash_Cde);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-CASH-CDE := #CNTRCT-CASH-CDE
        pnd_Save_Cntrct_Xfr_Term_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Xfr_Term_Cde);                                                                                  //Natural: ASSIGN #SAVE-CNTRCT-XFR-TERM-CDE := #CNTRCT-XFR-TERM-CDE
        pnd_Save_Cntrct_Hold_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Hold_Cde);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-HOLD-CDE := #CNTRCT-HOLD-CDE
        pnd_Save_Cntrct_Pend_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Pend_Cde);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-PEND-CDE := #CNTRCT-PEND-CDE
        pnd_Save_Cntrct_Mode_Ind.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                          //Natural: ASSIGN #SAVE-CNTRCT-MODE-IND := #CNTRCT-MODE-IND
        //*  ADDED 1/03
        //*  ADDED 1/03
        if (condition(pnd_Input_Record_Pnd_Cntrct_Company_Cde.getValue(1).equals("T")))                                                                                   //Natural: IF #CNTRCT-COMPANY-CDE ( 1 ) = 'T'
        {
            pnd_Save_Per_Ivc_Amt.setValue(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue(1));                                                                           //Natural: ASSIGN #SAVE-PER-IVC-AMT := #CNTRCT-PER-IVC-AMT ( 1 )
            //*  ADDED 1/03
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Save_Per_Ivc_Amt.setValue(pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt.getValue(2));                                                                           //Natural: ASSIGN #SAVE-PER-IVC-AMT := #CNTRCT-PER-IVC-AMT ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED 1/98
        pnd_Wrk_Payees.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRK-PAYEES
    }
    private void sub_Save_Fund_Summ() throws Exception                                                                                                                    //Natural: SAVE-FUND-SUMM
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
                                                                                                                                                                          //Natural: PERFORM ACCUM-DIST
        sub_Accum_Dist();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet1079 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #SAVE-CNTRCT-OPTN-CDE;//Natural: VALUE 28 : 30
        if (condition(((pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))))
        {
            decideConditionsMet1079++;
            if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("DTRA")))  //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'CASH' OR = 'RINV' OR = 'DTRA'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_T_Payees.getValue(4).nadd(1);                                                                                                                         //Natural: ADD 1 TO #T-PAYEES ( 4 )
                pnd_Tot_Payees.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOT-PAYEES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 22
        else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(22))))
        {
            decideConditionsMet1079++;
            pnd_T_Payees.getValue(2).nadd(1);                                                                                                                             //Natural: ADD 1 TO #T-PAYEES ( 2 )
            pnd_Tot_Payees.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOT-PAYEES
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27))))
        {
            decideConditionsMet1079++;
            pnd_T_Payees.getValue(3).nadd(1);                                                                                                                             //Natural: ADD 1 TO #T-PAYEES ( 3 )
            pnd_Tot_Payees.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOT-PAYEES
        }                                                                                                                                                                 //Natural: VALUE 21
        else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(21))))
        {
            decideConditionsMet1079++;
            pnd_T_Payees.getValue(1).nadd(1);                                                                                                                             //Natural: ADD 1 TO #T-PAYEES ( 1 )
            pnd_Tot_Payees.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOT-PAYEES
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde.setValue(pnd_Input_Record_Pnd_Summ_Cmpny_Cde);                                                                //Natural: ASSIGN #SAVE-SUMM-CMPNY-CDE := #SUMM-CMPNY-CDE
        pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Fund_Cde.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde);                                                                  //Natural: ASSIGN #SAVE-SUMM-FUND-CDE := #SUMM-FUND-CDE
    }
    private void sub_Print_Report_1() throws Exception                                                                                                                    //Natural: PRINT-REPORT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  ADDED FOLLOWING  1/98
        pnd_Unit_Val_Type.reset();                                                                                                                                        //Natural: RESET #UNIT-VAL-TYPE
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4")))                                                //Natural: IF #SUMM-CMPNY-CDE = 'W' OR = '4'
        {
            pnd_Unit_Val_Type.setValue("MNTH");                                                                                                                           //Natural: ASSIGN #UNIT-VAL-TYPE := 'MNTH'
            pnd_Mtot_Units.compute(new ComputeParameters(false, pnd_Mtot_Units), pnd_Units_1.add(pnd_Mtot_Units));                                                        //Natural: ASSIGN #MTOT-UNITS := #UNITS-1 + #MTOT-UNITS
            pnd_Mtotal_Amt.nadd(pnd_Total);                                                                                                                               //Natural: ASSIGN #MTOTAL-AMT := #MTOTAL-AMT + #TOTAL
            pnd_Tot_Payees_M.nadd(pnd_Wrk_Payees);                                                                                                                        //Natural: ADD #WRK-PAYEES TO #TOT-PAYEES-M
            pnd_Wrk_Payees.setValue(0);                                                                                                                                   //Natural: ASSIGN #WRK-PAYEES := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2")))                                            //Natural: IF #SUMM-CMPNY-CDE = 'U' OR = '2'
            {
                pnd_Unit_Val_Type.setValue("ANNL");                                                                                                                       //Natural: ASSIGN #UNIT-VAL-TYPE := 'ANNL'
                pnd_Atot_Units.compute(new ComputeParameters(false, pnd_Atot_Units), pnd_Units_1.add(pnd_Atot_Units));                                                    //Natural: ASSIGN #ATOT-UNITS := #UNITS-1 + #ATOT-UNITS
                pnd_Atotal_Amt.nadd(pnd_Total);                                                                                                                           //Natural: ASSIGN #ATOTAL-AMT := #ATOTAL-AMT + #TOTAL
                pnd_Tot_Payees_A.nadd(pnd_Wrk_Payees);                                                                                                                    //Natural: ADD #WRK-PAYEES TO #TOT-PAYEES-A
                pnd_Wrk_Payees.setValue(0);                                                                                                                               //Natural: ASSIGN #WRK-PAYEES := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wrk_Payees.reset();                                                                                                                                           //Natural: RESET #WRK-PAYEES
        pnd_Parm_Desc.reset();                                                                                                                                            //Natural: RESET #PARM-DESC
        pnd_Parm_Fund_3.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde_N);                                                                                                   //Natural: ASSIGN #PARM-FUND-3 := #SUMM-FUND-CDE-N
        pnd_Parm_Len.setValue(3);                                                                                                                                         //Natural: ASSIGN #PARM-LEN := 3
        DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                         //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
        if (condition(Global.isEscape())) return;
        pnd_Fund_Desc_Det.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                                        //Natural: ASSIGN #FUND-DESC-DET := #PARM-DESC-6
        //*  END OF ADD 1/98
        short decideConditionsMet1133 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #SAVE-CNTRCT-OPTN-CDE;//Natural: VALUE 22
        if (condition((pnd_Save_Cntrct_Optn_Cde.equals(22))))
        {
            decideConditionsMet1133++;
            pnd_T_Per_Amt.getValue(2).compute(new ComputeParameters(false, pnd_T_Per_Amt.getValue(2)), pnd_Per_Amt_1.add(pnd_T_Per_Amt.getValue(2)));                     //Natural: ASSIGN #T-PER-AMT ( 2 ) := #PER-AMT-1 + #T-PER-AMT ( 2 )
            pnd_T_Div_Amt.getValue(2).compute(new ComputeParameters(false, pnd_T_Div_Amt.getValue(2)), pnd_Div_Amt_1.add(pnd_T_Div_Amt.getValue(2)));                     //Natural: ASSIGN #T-DIV-AMT ( 2 ) := #DIV-AMT-1 + #T-DIV-AMT ( 2 )
            pnd_T_Amt.getValue(2).compute(new ComputeParameters(false, pnd_T_Amt.getValue(2)), pnd_T_Per_Amt.getValue(2).add(pnd_T_Div_Amt.getValue(2)));                 //Natural: ASSIGN #T-AMT ( 2 ) := #T-PER-AMT ( 2 ) + #T-DIV-AMT ( 2 )
            if (condition(pnd_Units_1.equals(getZero())))                                                                                                                 //Natural: IF #UNITS-1 = 0
            {
                pnd_Tot_Per_Amt.compute(new ComputeParameters(false, pnd_Tot_Per_Amt), pnd_Per_Amt_1.add(pnd_Tot_Per_Amt));                                               //Natural: ASSIGN #TOT-PER-AMT := #PER-AMT-1 + #TOT-PER-AMT
                pnd_Tot_Div_Amt.compute(new ComputeParameters(false, pnd_Tot_Div_Amt), pnd_Div_Amt_1.add(pnd_Tot_Div_Amt));                                               //Natural: ASSIGN #TOT-DIV-AMT := #DIV-AMT-1 + #TOT-DIV-AMT
                pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Per_Amt_1.add(pnd_Div_Amt_1));                                                             //Natural: ASSIGN #TOTAL := #PER-AMT-1 + #DIV-AMT-1
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                pnd_Per_Amt_A_Pnd_Per_Amt.setValue(pnd_Per_Amt_1);                                                                                                        //Natural: ASSIGN #PER-AMT := #PER-AMT-1
                pnd_Div_Amt_A_Pnd_Div_Amt.setValue(pnd_Div_Amt_1);                                                                                                        //Natural: ASSIGN #DIV-AMT := #DIV-AMT-1
                pnd_Units_A.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #UNITS-A
                getReports().write(2, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 2X #PER-AMT ( EM = ZZZ,ZZ9.99 ) 3X #DIV-AMT ( EM = ZZZ,ZZ9.99 ) 18X #TOTAL ( EM = ZZZ,ZZ9.99 ) 8X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 4X #SAVE-CNTRCT-CASH-CDE 5X #SAVE-CNTRCT-XFR-TERM-CDE 5X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND
                    ColumnSpacing(2),pnd_Per_Amt_A_Pnd_Per_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Div_Amt_A_Pnd_Div_Amt, new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new ColumnSpacing(18),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(8),pnd_Save_Cntrct_Prev_Dist_Cde,new 
                    ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new ColumnSpacing(4),pnd_Save_Cntrct_Cash_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Xfr_Term_Cde,new 
                    ColumnSpacing(5),pnd_Save_Cntrct_Hold_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Units_A_Pnd_Units.setValue(pnd_Units_1);                                                                                                              //Natural: ASSIGN #UNITS := #UNITS-1
                pnd_Tot_Units.compute(new ComputeParameters(false, pnd_Tot_Units), pnd_Units_1.add(pnd_Tot_Units));                                                       //Natural: ASSIGN #TOT-UNITS := #UNITS-1 + #TOT-UNITS
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                pnd_Per_Amt_A.setValue(" ");                                                                                                                              //Natural: MOVE ' ' TO #PER-AMT-A #DIV-AMT-A
                pnd_Div_Amt_A.setValue(" ");
                getReports().write(2, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 2 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 30X #UNITS ( EM = ZZZ,ZZ9.999 ) 4X #TOTAL ( EM = ZZZ,ZZ9.99 ) 3X #FUND-DESC-DET 2X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 3X #SAVE-CNTRCT-CASH-CDE 5X #SAVE-CNTRCT-XFR-TERM-CDE 6X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND 2X #UNIT-VAL-TYPE
                    ColumnSpacing(30),pnd_Units_A_Pnd_Units, new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(4),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),new 
                    ColumnSpacing(3),pnd_Fund_Desc_Det,new ColumnSpacing(2),pnd_Save_Cntrct_Prev_Dist_Cde,new ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new 
                    ColumnSpacing(3),pnd_Save_Cntrct_Cash_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Xfr_Term_Cde,new ColumnSpacing(6),pnd_Save_Cntrct_Hold_Cde,new 
                    ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind,new ColumnSpacing(2),pnd_Unit_Val_Type);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27))))
        {
            decideConditionsMet1133++;
            pnd_T_Per_Amt.getValue(3).compute(new ComputeParameters(false, pnd_T_Per_Amt.getValue(3)), pnd_Per_Amt_1.add(pnd_T_Per_Amt.getValue(3)));                     //Natural: ASSIGN #T-PER-AMT ( 3 ) := #PER-AMT-1 + #T-PER-AMT ( 3 )
            pnd_T_Div_Amt.getValue(3).compute(new ComputeParameters(false, pnd_T_Div_Amt.getValue(3)), pnd_Div_Amt_1.add(pnd_T_Div_Amt.getValue(3)));                     //Natural: ASSIGN #T-DIV-AMT ( 3 ) := #DIV-AMT-1 + #T-DIV-AMT ( 3 )
            pnd_T_Amt.getValue(3).compute(new ComputeParameters(false, pnd_T_Amt.getValue(3)), pnd_T_Per_Amt.getValue(3).add(pnd_T_Div_Amt.getValue(3)));                 //Natural: ASSIGN #T-AMT ( 3 ) := #T-PER-AMT ( 3 ) + #T-DIV-AMT ( 3 )
            if (condition(pnd_Units_1.equals(getZero())))                                                                                                                 //Natural: IF #UNITS-1 = 0
            {
                pnd_Tot_Per_Amt.compute(new ComputeParameters(false, pnd_Tot_Per_Amt), pnd_Per_Amt_1.add(pnd_Tot_Per_Amt));                                               //Natural: ASSIGN #TOT-PER-AMT := #PER-AMT-1 + #TOT-PER-AMT
                pnd_Tot_Div_Amt.compute(new ComputeParameters(false, pnd_Tot_Div_Amt), pnd_Div_Amt_1.add(pnd_Tot_Div_Amt));                                               //Natural: ASSIGN #TOT-DIV-AMT := #DIV-AMT-1 + #TOT-DIV-AMT
                pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Per_Amt_1.add(pnd_Div_Amt_1));                                                             //Natural: ASSIGN #TOTAL := #PER-AMT-1 + #DIV-AMT-1
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                pnd_Per_Amt_A_Pnd_Per_Amt.setValue(pnd_Per_Amt_1);                                                                                                        //Natural: ASSIGN #PER-AMT := #PER-AMT-1
                pnd_Div_Amt_A_Pnd_Div_Amt.setValue(pnd_Div_Amt_1);                                                                                                        //Natural: ASSIGN #DIV-AMT := #DIV-AMT-1
                pnd_Units_A.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #UNITS-A
                getReports().write(3, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 3 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 2X #PER-AMT ( EM = ZZZ,ZZ9.99 ) 3X #DIV-AMT ( EM = ZZZ,ZZ9.99 ) 18X #TOTAL ( EM = ZZZ,ZZ9.99 ) 8X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 4X #SAVE-CNTRCT-CASH-CDE 5X #SAVE-CNTRCT-XFR-TERM-CDE 5X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND
                    ColumnSpacing(2),pnd_Per_Amt_A_Pnd_Per_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Div_Amt_A_Pnd_Div_Amt, new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new ColumnSpacing(18),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(8),pnd_Save_Cntrct_Prev_Dist_Cde,new 
                    ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new ColumnSpacing(4),pnd_Save_Cntrct_Cash_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Xfr_Term_Cde,new 
                    ColumnSpacing(5),pnd_Save_Cntrct_Hold_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Units_A_Pnd_Units.setValue(pnd_Units_1);                                                                                                              //Natural: ASSIGN #UNITS := #UNITS-1
                pnd_Tot_Units.compute(new ComputeParameters(false, pnd_Tot_Units), pnd_Units_1.add(pnd_Tot_Units));                                                       //Natural: ASSIGN #TOT-UNITS := #UNITS-1 + #TOT-UNITS
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                pnd_Per_Amt_A.setValue(" ");                                                                                                                              //Natural: MOVE ' ' TO #PER-AMT-A #DIV-AMT-A
                pnd_Div_Amt_A.setValue(" ");
                getReports().write(3, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 3 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 30X #UNITS ( EM = ZZZ,ZZ9.999 ) 4X #TOTAL ( EM = ZZZ,ZZ9.99 ) 3X #FUND-DESC-DET 2X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 3X #SAVE-CNTRCT-CASH-CDE 5X #SAVE-CNTRCT-XFR-TERM-CDE 6X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND 2X #UNIT-VAL-TYPE
                    ColumnSpacing(30),pnd_Units_A_Pnd_Units, new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(4),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),new 
                    ColumnSpacing(3),pnd_Fund_Desc_Det,new ColumnSpacing(2),pnd_Save_Cntrct_Prev_Dist_Cde,new ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new 
                    ColumnSpacing(3),pnd_Save_Cntrct_Cash_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Xfr_Term_Cde,new ColumnSpacing(6),pnd_Save_Cntrct_Hold_Cde,new 
                    ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind,new ColumnSpacing(2),pnd_Unit_Val_Type);
                if (Global.isEscape()) return;
                //*  TPA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 28:31
        else if (condition(((pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(31)))))
        {
            decideConditionsMet1133++;
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BT")    //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLC") 
                || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC")))
            {
                pnd_T_Per_Amt.getValue(4).compute(new ComputeParameters(false, pnd_T_Per_Amt.getValue(4)), pnd_Per_Amt_1.add(pnd_T_Per_Amt.getValue(4)));                 //Natural: ASSIGN #T-PER-AMT ( 4 ) := #PER-AMT-1 + #T-PER-AMT ( 4 )
                pnd_T_Div_Amt.getValue(4).compute(new ComputeParameters(false, pnd_T_Div_Amt.getValue(4)), pnd_Div_Amt_1.add(pnd_T_Div_Amt.getValue(4)));                 //Natural: ASSIGN #T-DIV-AMT ( 4 ) := #DIV-AMT-1 + #T-DIV-AMT ( 4 )
                pnd_T_Amt.getValue(4).compute(new ComputeParameters(false, pnd_T_Amt.getValue(4)), pnd_T_Per_Amt.getValue(4).add(pnd_T_Div_Amt.getValue(4)));             //Natural: ASSIGN #T-AMT ( 4 ) := #T-PER-AMT ( 4 ) + #T-DIV-AMT ( 4 )
                pnd_T_Ivc.getValue(4).nadd(pnd_Save_Per_Ivc_Amt);                                                                                                         //Natural: ASSIGN #T-IVC ( 4 ) := #T-IVC ( 4 ) + #SAVE-PER-IVC-AMT
                pnd_Tot_Ivc_Amt.nadd(pnd_Save_Per_Ivc_Amt);                                                                                                               //Natural: ASSIGN #TOT-IVC-AMT := #TOT-IVC-AMT + #SAVE-PER-IVC-AMT
                pnd_Dest_Total_Ivc.getValue(pnd_Dest_Occ).nadd(pnd_Save_Per_Ivc_Amt);                                                                                     //Natural: ASSIGN #DEST-TOTAL-IVC ( #DEST-OCC ) := #DEST-TOTAL-IVC ( #DEST-OCC ) + #SAVE-PER-IVC-AMT
                if (condition(pnd_Units_1.equals(getZero())))                                                                                                             //Natural: IF #UNITS-1 = 0
                {
                    pnd_Tot_Per_Amt.compute(new ComputeParameters(false, pnd_Tot_Per_Amt), pnd_Per_Amt_1.add(pnd_Tot_Per_Amt));                                           //Natural: ASSIGN #TOT-PER-AMT := #PER-AMT-1 + #TOT-PER-AMT
                    pnd_Tot_Div_Amt.compute(new ComputeParameters(false, pnd_Tot_Div_Amt), pnd_Div_Amt_1.add(pnd_Tot_Div_Amt));                                           //Natural: ASSIGN #TOT-DIV-AMT := #DIV-AMT-1 + #TOT-DIV-AMT
                    pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Per_Amt_1.add(pnd_Div_Amt_1));                                                         //Natural: ASSIGN #TOTAL := #PER-AMT-1 + #DIV-AMT-1
                    pnd_Total_Amt.nadd(pnd_Total);                                                                                                                        //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                    pnd_Per_Amt_A_Pnd_Per_Amt.setValue(pnd_Per_Amt_1);                                                                                                    //Natural: ASSIGN #PER-AMT := #PER-AMT-1
                    pnd_Div_Amt_A_Pnd_Div_Amt.setValue(pnd_Div_Amt_1);                                                                                                    //Natural: ASSIGN #DIV-AMT := #DIV-AMT-1
                    pnd_Units_A.setValue(" ");                                                                                                                            //Natural: MOVE ' ' TO #UNITS-A
                    //*  ADDED THIS 1/03
                    //*  ADDED 1/03
                    getReports().display(1, "SOCIAL/SECURITY",                                                                                                            //Natural: DISPLAY ( 1 ) 'SOCIAL/SECURITY' #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) '/IA-CONTRACT' #SAVE-PART-PPCN-NBR 'COMTRACTUAL/AMOUNT' #PER-AMT ( EM = ZZZ,ZZ9.99 ) 'PERIODIC/DIVIDEND' #DIV-AMT ( EM = ZZZ,ZZ9.99 ) '/TOTAL-AMT' #TOTAL ( EM = ZZ,ZZZ,ZZ9.99 ) '/PER-IVC$' #SAVE-PER-IVC-AMT 'DEST/PREV' #SAVE-CNTRCT-PREV-DIST-CDE 'DEST/CURR' #SAVE-CNTRCT-CURR-DIST-CDE 'CASH/CODE' #SAVE-CNTRCT-CASH-CDE 'TERM/CODE' #SAVE-CNTRCT-XFR-TERM-CDE '/HOLD' #SAVE-CNTRCT-HOLD-CDE '/PEND' #SAVE-CNTRCT-PEND-CDE '/MODE' #SAVE-CNTRCT-MODE-IND
                    		pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"/IA-CONTRACT",
                    		pnd_Save_Part_Ppcn_Nbr,"COMTRACTUAL/AMOUNT",
                    		pnd_Per_Amt_A_Pnd_Per_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"PERIODIC/DIVIDEND",
                    		pnd_Div_Amt_A_Pnd_Div_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"/TOTAL-AMT",
                    		pnd_Total, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/PER-IVC$",
                    		pnd_Save_Per_Ivc_Amt,"DEST/PREV",
                    		pnd_Save_Cntrct_Prev_Dist_Cde,"DEST/CURR",
                    		pnd_Save_Cntrct_Curr_Dist_Cde,"CASH/CODE",
                    		pnd_Save_Cntrct_Cash_Cde,"TERM/CODE",
                    		pnd_Save_Cntrct_Xfr_Term_Cde,"/HOLD",
                    		pnd_Save_Cntrct_Hold_Cde,"/PEND",
                    		pnd_Save_Cntrct_Pend_Cde,"/MODE",
                    		pnd_Save_Cntrct_Mode_Ind);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Units_A_Pnd_Units.setValue(pnd_Units_1);                                                                                                          //Natural: ASSIGN #UNITS := #UNITS-1
                    pnd_Tot_Units.compute(new ComputeParameters(false, pnd_Tot_Units), pnd_Units_1.add(pnd_Tot_Units));                                                   //Natural: ASSIGN #TOT-UNITS := #UNITS-1 + #TOT-UNITS
                    pnd_Total_Amt.nadd(pnd_Total);                                                                                                                        //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                    pnd_Per_Amt_A.setValue(" ");                                                                                                                          //Natural: MOVE ' ' TO #PER-AMT-A #DIV-AMT-A
                    pnd_Div_Amt_A.setValue(" ");
                    getReports().write(1, ReportOption.NOTITLE,"error tpa contract with units ");                                                                         //Natural: WRITE ( 1 ) 'error tpa contract with units '
                    if (Global.isEscape()) return;
                    getReports().write(1, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 1 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 30X #UNITS ( EM = ZZZ,ZZ9.999 ) 4X #TOTAL ( EM = ZZZ,ZZ9.99 ) 3X #FUND-DESC-DET 2X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 3X #SAVE-CNTRCT-CASH-CDE 5X #SAVE-CNTRCT-XFR-TERM-CDE 6X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND 2X #UNIT-VAL-TYPE
                        ColumnSpacing(30),pnd_Units_A_Pnd_Units, new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(4),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),new 
                        ColumnSpacing(3),pnd_Fund_Desc_Det,new ColumnSpacing(2),pnd_Save_Cntrct_Prev_Dist_Cde,new ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new 
                        ColumnSpacing(3),pnd_Save_Cntrct_Cash_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Xfr_Term_Cde,new ColumnSpacing(6),pnd_Save_Cntrct_Hold_Cde,new 
                        ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind,new ColumnSpacing(2),pnd_Unit_Val_Type);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 1/03
            //*  ANNUITY CERTAIN
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            //*  ADDED 1/03
            pnd_Save_Per_Ivc_Amt.reset();                                                                                                                                 //Natural: RESET #SAVE-PER-IVC-AMT
        }                                                                                                                                                                 //Natural: VALUE 21
        else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(21))))
        {
            decideConditionsMet1133++;
            pnd_T_Per_Amt.getValue(1).compute(new ComputeParameters(false, pnd_T_Per_Amt.getValue(1)), pnd_Per_Amt_1.add(pnd_T_Per_Amt.getValue(1)));                     //Natural: ASSIGN #T-PER-AMT ( 1 ) := #PER-AMT-1 + #T-PER-AMT ( 1 )
            pnd_T_Div_Amt.getValue(1).compute(new ComputeParameters(false, pnd_T_Div_Amt.getValue(1)), pnd_Div_Amt_1.add(pnd_T_Div_Amt.getValue(1)));                     //Natural: ASSIGN #T-DIV-AMT ( 1 ) := #DIV-AMT-1 + #T-DIV-AMT ( 1 )
            pnd_T_Amt.getValue(1).compute(new ComputeParameters(false, pnd_T_Amt.getValue(1)), pnd_T_Per_Amt.getValue(1).add(pnd_T_Div_Amt.getValue(1)));                 //Natural: ASSIGN #T-AMT ( 1 ) := #T-PER-AMT ( 1 ) + #T-DIV-AMT ( 1 )
            pnd_T_Units.getValue(1).compute(new ComputeParameters(false, pnd_T_Units.getValue(1)), pnd_Units_1.add(pnd_T_Units.getValue(1)));                             //Natural: ASSIGN #T-UNITS ( 1 ) := #UNITS-1 + #T-UNITS ( 1 )
            pnd_T_Ivc.getValue(1).nadd(pnd_Save_Per_Ivc_Amt);                                                                                                             //Natural: ASSIGN #T-IVC ( 1 ) := #T-IVC ( 1 ) + #SAVE-PER-IVC-AMT
            pnd_Tot_Ivc_Amt.nadd(pnd_Save_Per_Ivc_Amt);                                                                                                                   //Natural: ASSIGN #TOT-IVC-AMT := #TOT-IVC-AMT + #SAVE-PER-IVC-AMT
            pnd_Dest_Total_Ivc.getValue(pnd_Dest_Occ).nadd(pnd_Save_Per_Ivc_Amt);                                                                                         //Natural: ASSIGN #DEST-TOTAL-IVC ( #DEST-OCC ) := #DEST-TOTAL-IVC ( #DEST-OCC ) + #SAVE-PER-IVC-AMT
            if (condition(pnd_Units_1.equals(getZero())))                                                                                                                 //Natural: IF #UNITS-1 = 0
            {
                pnd_Tot_Per_Amt.compute(new ComputeParameters(false, pnd_Tot_Per_Amt), pnd_Per_Amt_1.add(pnd_Tot_Per_Amt));                                               //Natural: ASSIGN #TOT-PER-AMT := #PER-AMT-1 + #TOT-PER-AMT
                pnd_Tot_Div_Amt.compute(new ComputeParameters(false, pnd_Tot_Div_Amt), pnd_Div_Amt_1.add(pnd_Tot_Div_Amt));                                               //Natural: ASSIGN #TOT-DIV-AMT := #DIV-AMT-1 + #TOT-DIV-AMT
                pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Per_Amt_1.add(pnd_Div_Amt_1));                                                             //Natural: ASSIGN #TOTAL := #PER-AMT-1 + #DIV-AMT-1
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                pnd_Per_Amt_A_Pnd_Per_Amt.setValue(pnd_Per_Amt_1);                                                                                                        //Natural: ASSIGN #PER-AMT := #PER-AMT-1
                pnd_Div_Amt_A_Pnd_Div_Amt.setValue(pnd_Div_Amt_1);                                                                                                        //Natural: ASSIGN #DIV-AMT := #DIV-AMT-1
                //*  WAS 18X
                //*  ADDED 1/03
                pnd_Units_A.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #UNITS-A
                getReports().write(4, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 4 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 2X #PER-AMT ( EM = ZZZ,ZZ9.99 ) 3X #DIV-AMT ( EM = ZZZ,ZZ9.99 ) 17X #TOTAL ( EM = ZZZ,ZZ9.99 ) #SAVE-PER-IVC-AMT 6X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 7X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND
                    ColumnSpacing(2),pnd_Per_Amt_A_Pnd_Per_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Div_Amt_A_Pnd_Div_Amt, new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),pnd_Save_Per_Ivc_Amt,new ColumnSpacing(6),pnd_Save_Cntrct_Prev_Dist_Cde,new 
                    ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new ColumnSpacing(7),pnd_Save_Cntrct_Hold_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new 
                    ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Units_A_Pnd_Units.setValue(pnd_Units_1);                                                                                                              //Natural: ASSIGN #UNITS := #UNITS-1
                pnd_Tot_Units.compute(new ComputeParameters(false, pnd_Tot_Units), pnd_Units_1.add(pnd_Tot_Units));                                                       //Natural: ASSIGN #TOT-UNITS := #UNITS-1 + #TOT-UNITS
                pnd_Total_Amt.nadd(pnd_Total);                                                                                                                            //Natural: ASSIGN #TOTAL-AMT := #TOTAL-AMT + #TOTAL
                //*  WAS 4X
                //*  HEADING FUND CODE
                pnd_Per_Amt_A.setValue(" ");                                                                                                                              //Natural: MOVE ' ' TO #PER-AMT-A #DIV-AMT-A
                pnd_Div_Amt_A.setValue(" ");
                getReports().write(4, ReportOption.NOTITLE,pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(4),pnd_Save_Part_Ppcn_Nbr,new  //Natural: WRITE ( 4 ) #SAVE-PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 4X #SAVE-PART-PPCN-NBR 30X #UNITS ( EM = ZZZ,ZZ9.999 ) 3X #TOTAL ( EM = ZZZ,ZZ9.99 ) #SAVE-PER-IVC-AMT 1X #FUND-DESC-DET 2X #SAVE-CNTRCT-PREV-DIST-CDE 2X #SAVE-CNTRCT-CURR-DIST-CDE 7X #SAVE-CNTRCT-HOLD-CDE 5X #SAVE-CNTRCT-PEND-CDE 3X #SAVE-CNTRCT-MODE-IND 2X #UNIT-VAL-TYPE
                    ColumnSpacing(30),pnd_Units_A_Pnd_Units, new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(3),pnd_Total, new ReportEditMask ("ZZZ,ZZ9.99"),pnd_Save_Per_Ivc_Amt,new 
                    ColumnSpacing(1),pnd_Fund_Desc_Det,new ColumnSpacing(2),pnd_Save_Cntrct_Prev_Dist_Cde,new ColumnSpacing(2),pnd_Save_Cntrct_Curr_Dist_Cde,new 
                    ColumnSpacing(7),pnd_Save_Cntrct_Hold_Cde,new ColumnSpacing(5),pnd_Save_Cntrct_Pend_Cde,new ColumnSpacing(3),pnd_Save_Cntrct_Mode_Ind,new 
                    ColumnSpacing(2),pnd_Unit_Val_Type);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 1/03
            pnd_Save_Per_Ivc_Amt.reset();                                                                                                                                 //Natural: RESET #SAVE-PER-IVC-AMT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Per_Amt_A_Pnd_Per_Amt.reset();                                                                                                                                //Natural: RESET #PER-AMT #DIV-AMT #UNITS #TOTAL #PER-AMT-1 #DIV-AMT-1 #UNITS-1
        pnd_Div_Amt_A_Pnd_Div_Amt.reset();
        pnd_Units_A_Pnd_Units.reset();
        pnd_Total.reset();
        pnd_Per_Amt_1.reset();
        pnd_Div_Amt_1.reset();
        pnd_Units_1.reset();
    }
    //*  ADDED 1/98
    //*  ADDED 1/98
    private void sub_Print_Report_2() throws Exception                                                                                                                    //Natural: PRINT-REPORT-2
    {
        if (BLNatReinput.isReinput()) return;

        iaaa730_Tot_Amt.compute(new ComputeParameters(false, iaaa730_Tot_Amt), iaaa730_Per_Div_Amt.add(iaaa730_Per_Pay_Amt));                                             //Natural: ASSIGN IAAA730.TOT-AMT := IAAA730.PER-DIV-AMT + IAAA730.PER-PAY-AMT
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,new TabSetting(43),"      ANNUAL",new TabSetting(62),"      MONTHLY");                                         //Natural: WRITE ( 6 ) / 43T '      ANNUAL' 62T '      MONTHLY'
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(43),"----------------",new TabSetting(62),"----------------");                                          //Natural: WRITE ( 6 ) 43T '----------------' 62T '----------------'
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 4
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            iaaa730_Tot_Fin_Payment.nadd(iaaa730_Pnd_Fin_Payment.getValue(pnd_I));                                                                                        //Natural: ADD #FIN-PAYMENT ( #I ) TO TOT-FIN-PAYMENT
            iaaa730_Tot_Fin_Div.nadd(iaaa730_Pnd_Fin_Div.getValue(pnd_I));                                                                                                //Natural: ADD #FIN-DIV ( #I ) TO TOT-FIN-DIV
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Fund_Desc_Lne.setValue("TIAA TRADIONAL FUND RECORDS             ");                                                                                           //Natural: ASSIGN #FUND-DESC-LNE := 'TIAA TRADIONAL FUND RECORDS             '
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,pnd_Fund_Desc_Lne,new TabSetting(47),iaaa730_Tiaa_Fund, new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"TIAA TRADITIONAL PERIODIC PAYMENTS      ",new  //Natural: WRITE ( 6 ) / #FUND-DESC-LNE 47TIAAA730.TIAA-FUND ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'TIAA TRADITIONAL PERIODIC PAYMENTS      ' 42TIAAA730.PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL PERIODIC DIVIDEND      ' 42TIAAA730.PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL FINAL PAYMENTS         ' 42TIAAA730.TOT-FIN-PAYMENT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL FINAL DIVIDEND         ' 42TIAAA730.TOT-FIN-DIV ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL TOTAL AMOUNT           ' 42TIAAA730.TOT-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  FUND RECORDS             ' 47TIAAA730.TPA-FUND ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'TPA  PERIODIC PAYMENTS      ' 42TIAAA730.TPA-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL PERIODIC DIVIDEND      ' 42TIAAA730.TPA-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL FINAL PAYMENT          ' 42TIAAA730.#FIN-PAYMENT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL FINAL  DIVIDEND        ' 42TIAAA730.#FIN-DIV ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL TOTAL AMOUNT           ' 42TIAAA730.TPA-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO FUND RECORDS             ' 47TIAAA730.IPRO-FUND ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'IPRO PERIODIC PAYMENTS      ' 42TIAAA730.IPRO-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL PERIODIC DIVIDEND ' 42TIAAA730.IPRO-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL FINAL PAYMENT          ' 42TIAAA730.#FIN-PAYMENT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL FINAL  DIVIDEND        ' 42TIAAA730.#FIN-DIV ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL TOTAL AMOUNT  ' 42TIAAA730.IPRO-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  FUND RECORDS      ' 47TIAAA730.P&I-FUND ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'P&I  PERIODIC PAYMENTS      ' 42TIAAA730.P&I-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL PERIODIC DIVIDEND      ' 42TIAAA730.P&I-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL FINAL PAYMENT          ' 42TIAAA730.#FIN-PAYMENT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL FINAL  DIVIDEND        ' 42TIAAA730.#FIN-DIV ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL TOTAL AMOUNT           ' 42TIAAA730.P&I-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'A/C   FUND RECORDS     ' 47TIAAA730.A/C-FUND ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'A/C   PERIODIC PAYMENTS     ' 42TIAAA730.A/C-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'A/C   TRADITIONAL PERIODIC DIVIDEND     ' 42TIAAA730.A/C-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'A/C   TRADITIONAL FINAL PAYMENT         ' 42TIAAA730.#FIN-PAYMENT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'A/C   TRADITIONAL FINAL  DIVIDEND       ' 42TIAAA730.#FIN-DIV ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'A/C   TRADITIONAL TOTAL AMOUNT          ' 42TIAAA730.A/C-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            TabSetting(42),iaaa730_Per_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL PERIODIC DIVIDEND      ",new TabSetting(42),iaaa730_Per_Div_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL FINAL PAYMENTS         ",new TabSetting(42),iaaa730_Tot_Fin_Payment, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL FINAL DIVIDEND         ",new TabSetting(42),iaaa730_Tot_Fin_Div, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL TOTAL AMOUNT           ",new 
            TabSetting(42),iaaa730_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  FUND RECORDS             ",new TabSetting(47),iaaa730_Tpa_Fund, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"TPA  PERIODIC PAYMENTS      ",new TabSetting(42),iaaa730_Tpa_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL PERIODIC DIVIDEND      ",new 
            TabSetting(42),iaaa730_Tpa_Div_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL FINAL PAYMENT          ",new TabSetting(42),iaaa730_Pnd_Fin_Payment.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL FINAL  DIVIDEND        ",new TabSetting(42),iaaa730_Pnd_Fin_Div.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL TOTAL AMOUNT           ",new TabSetting(42),iaaa730_Tpa_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO FUND RECORDS             ",new TabSetting(47),iaaa730_Ipro_Fund, new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"IPRO PERIODIC PAYMENTS      ",new 
            TabSetting(42),iaaa730_Ipro_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL PERIODIC DIVIDEND ",new TabSetting(42),iaaa730_Ipro_Div_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL FINAL PAYMENT          ",new TabSetting(42),iaaa730_Pnd_Fin_Payment.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL FINAL  DIVIDEND        ",new TabSetting(42),iaaa730_Pnd_Fin_Div.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL TOTAL AMOUNT  ",new TabSetting(42),iaaa730_Ipro_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  FUND RECORDS      ",new 
            TabSetting(47),iaaa730_Pamp_I_Fund, new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"P&I  PERIODIC PAYMENTS      ",new TabSetting(42),iaaa730_Pamp_I_Pay_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL PERIODIC DIVIDEND      ",new TabSetting(42),iaaa730_Pamp_I_Div_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL FINAL PAYMENT          ",new TabSetting(42),iaaa730_Pnd_Fin_Payment.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL FINAL  DIVIDEND        ",new TabSetting(42),iaaa730_Pnd_Fin_Div.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL TOTAL AMOUNT           ",new TabSetting(42),iaaa730_Pamp_I_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"A/C   FUND RECORDS     ",new 
            TabSetting(47),iaaa730_Afslash_C_Fund, new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"A/C   PERIODIC PAYMENTS     ",new TabSetting(42),iaaa730_Afslash_C_Pay_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"A/C   TRADITIONAL PERIODIC DIVIDEND     ",new TabSetting(42),iaaa730_Afslash_C_Div_Amt, new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"A/C   TRADITIONAL FINAL PAYMENT         ",new TabSetting(42),iaaa730_Pnd_Fin_Payment.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"A/C   TRADITIONAL FINAL  DIVIDEND       ",new TabSetting(42),iaaa730_Pnd_Fin_Div.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"A/C   TRADITIONAL TOTAL AMOUNT          ",new TabSetting(42),iaaa730_Afslash_C_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fund_Desc_Lne.setValue("TIAA REAL-ESTATE FUND RECORDS    ");                                                                                                  //Natural: ASSIGN #FUND-DESC-LNE := 'TIAA REAL-ESTATE FUND RECORDS    '
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,pnd_Fund_Desc_Lne,new TabSetting(48),iaaa730_Cref_Fund_Payees.getValue(9), new ReportEditMask                  //Natural: WRITE ( 6 ) / #FUND-DESC-LNE 48T IAAA730.CREF-FUND-PAYEES ( 9 ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) 68T IAAA730.CREF-FUND-PAYEES-MNTHLY ( 9 ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'TIAA REAL-ESTATE UNITS                  ' 43T IAAA730.CREF-UNITS ( 9 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 63T IAAA730.CREF-UNITS-MNTHLY ( 9 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) / 'TIAA REAL-ESTATE DOLLARS                ' 44T IAAA730.CREF-AMT ( 9 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 64T IAAA730.CREF-AMT-MNTHLY ( 9 ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
            ("ZZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(68),iaaa730_Cref_Fund_Payees_Mnthly.getValue(9), new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"TIAA REAL-ESTATE UNITS                  ",new 
            TabSetting(43),iaaa730_Cref_Units.getValue(9), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(63),iaaa730_Cref_Units_Mnthly.getValue(9), 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA REAL-ESTATE DOLLARS                ",new TabSetting(44),iaaa730_Cref_Amt.getValue(9), 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(64),iaaa730_Cref_Amt_Mnthly.getValue(9), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        pnd_Fund_Desc_Lne.setValue("TIAA ACCESS FUND RECORDS    ");                                                                                                       //Natural: ASSIGN #FUND-DESC-LNE := 'TIAA ACCESS FUND RECORDS    '
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,pnd_Fund_Desc_Lne,new TabSetting(48),iaaa730_Cref_Fund_Payees.getValue(11), new ReportEditMask                 //Natural: WRITE ( 6 ) / #FUND-DESC-LNE 48T IAAA730.CREF-FUND-PAYEES ( 11 ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) 68T IAAA730.CREF-FUND-PAYEES-MNTHLY ( 11 ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) / 'TIAA ACCESS UNITS                  ' 43T IAAA730.CREF-UNITS ( 11 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 63T IAAA730.CREF-UNITS-MNTHLY ( 11 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) / 'TIAA ACCESS DOLLARS                ' 44T IAAA730.CREF-AMT ( 11 ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 64T IAAA730.CREF-AMT-MNTHLY ( 11 ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
            ("ZZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(68),iaaa730_Cref_Fund_Payees_Mnthly.getValue(11), new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"TIAA ACCESS UNITS                  ",new 
            TabSetting(43),iaaa730_Cref_Units.getValue(11), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(63),iaaa730_Cref_Units_Mnthly.getValue(11), 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS DOLLARS                ",new TabSetting(44),iaaa730_Cref_Amt.getValue(11), new 
            ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(64),iaaa730_Cref_Amt_Mnthly.getValue(11), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            //*  BYPASS REAL ESTATE AND TIAA ACCESS
            if (condition(pnd_I.equals(9) || pnd_I.equals(11)))                                                                                                           //Natural: IF #I = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_3.setValue(pnd_I);                                                                                                                              //Natural: ASSIGN #PARM-FUND-3 := #I
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_3, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-3 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF ");                                                                                                        //Natural: ASSIGN #FUND-CREF-CON := 'CREF '
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue(" FUND RECORDS  ");                                                                                                   //Natural: ASSIGN #FUND-TXT := ' FUND RECORDS  '
            getReports().write(6, ReportOption.NOTITLE,NEWLINE,pnd_Fund_Desc_Lne,new TabSetting(48),iaaa730_Cref_Fund_Payees.getValue(pnd_I), new ReportEditMask          //Natural: WRITE ( 6 ) / #FUND-DESC-LNE 48T IAAA730.CREF-FUND-PAYEES ( #I ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 ) 68T IAAA730.CREF-FUND-PAYEES-MNTHLY ( #I ) ( EM = ZZZZ,ZZZ,ZZZ,ZZ9 )
                ("ZZZZ,ZZZ,ZZZ,ZZ9"),new TabSetting(68),iaaa730_Cref_Fund_Payees_Mnthly.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue(" UNITS         ");                                                                                                   //Natural: ASSIGN #FUND-TXT := ' UNITS         '
            getReports().write(6, ReportOption.NOTITLE,pnd_Fund_Desc_Lne,new TabSetting(43),iaaa730_Cref_Units.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new  //Natural: WRITE ( 6 ) #FUND-DESC-LNE 43T IAAA730.CREF-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 63T IAAA730.CREF-UNITS-MNTHLY ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                TabSetting(63),iaaa730_Cref_Units_Mnthly.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue(" DOLLARS       ");                                                                                                   //Natural: ASSIGN #FUND-TXT := ' DOLLARS       '
            getReports().write(6, ReportOption.NOTITLE,pnd_Fund_Desc_Lne,new TabSetting(44),iaaa730_Cref_Amt.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new //Natural: WRITE ( 6 ) #FUND-DESC-LNE 44T IAAA730.CREF-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 64T IAAA730.CREF-AMT-MNTHLY ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                TabSetting(64),iaaa730_Cref_Amt_Mnthly.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   END OF ADD   8/96
    }
    //*  CHANGED FROM 6 TO 8  1/03
    private void sub_Print_Report_3() throws Exception                                                                                                                    //Natural: PRINT-REPORT-3
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #I1 1 8
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(8)); pnd_I1.nadd(1))
        {
            pnd_Dest_Total_Amt.getValue(pnd_I1).compute(new ComputeParameters(false, pnd_Dest_Total_Amt.getValue(pnd_I1)), pnd_Dest_Per_Pay.getValue(pnd_I1).add(pnd_Dest_Div_Pay.getValue(pnd_I1)).add(pnd_Dest_Units_Total.getValue(pnd_I1))); //Natural: ASSIGN #DEST-TOTAL-AMT ( #I1 ) := #DEST-PER-PAY ( #I1 ) + #DEST-DIV-PAY ( #I1 ) + #DEST-UNITS-TOTAL ( #I1 )
            pnd_Dest_Payees_Grand.nadd(pnd_Dest_Payees.getValue(pnd_I1));                                                                                                 //Natural: ADD #DEST-PAYEES ( #I1 ) TO #DEST-PAYEES-GRAND
            pnd_Dest_Per_Pay_Grand.nadd(pnd_Dest_Per_Pay.getValue(pnd_I1));                                                                                               //Natural: ADD #DEST-PER-PAY ( #I1 ) TO #DEST-PER-PAY-GRAND
            pnd_Dest_Div_Pay_Grand.nadd(pnd_Dest_Div_Pay.getValue(pnd_I1));                                                                                               //Natural: ADD #DEST-DIV-PAY ( #I1 ) TO #DEST-DIV-PAY-GRAND
            pnd_Dest_Units_Total_Grand.nadd(pnd_Dest_Units_Total.getValue(pnd_I1));                                                                                       //Natural: ADD #DEST-UNITS-TOTAL ( #I1 ) TO #DEST-UNITS-TOTAL-GRAND
            pnd_Dest_Units_Grand.nadd(pnd_Dest_Units.getValue(pnd_I1));                                                                                                   //Natural: ADD #DEST-UNITS ( #I1 ) TO #DEST-UNITS-GRAND
            pnd_Dest_Total_Amt_Grand.nadd(pnd_Dest_Total_Amt.getValue(pnd_I1));                                                                                           //Natural: ADD #DEST-TOTAL-AMT ( #I1 ) TO #DEST-TOTAL-AMT-GRAND
            //*  ADDED 1/03
            //*  ADDED 1/03
            pnd_Dest_Total_Ivc_Grand.nadd(pnd_Dest_Total_Ivc.getValue(pnd_I1));                                                                                           //Natural: ADD #DEST-TOTAL-IVC ( #I1 ) TO #DEST-TOTAL-IVC-GRAND
            getReports().write(7, ReportOption.NOTITLE,new ColumnSpacing(3),pnd_Dest_Cde.getValue(pnd_I1),new ColumnSpacing(5),pnd_Dest_Payees.getValue(pnd_I1),          //Natural: WRITE ( 7 ) 3X #DEST-CDE ( #I1 ) 5X #DEST-PAYEES ( #I1 ) ( EM = ZZZ,ZZ9 ) 5X #DEST-PER-PAY ( #I1 ) ( EM = Z,ZZZ,ZZ9.99 ) 2X #DEST-DIV-PAY ( #I1 ) ( EM = Z,ZZZ,ZZ9.99 ) 3X #DEST-UNITS ( #I1 ) ( EM = ZZZ,ZZ9.999 ) #DEST-UNITS-TOTAL ( #I1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 2X #DEST-TOTAL-AMT ( #I1 ) ( EM = ZZ,ZZZ,ZZ9.99 ) 3X #DEST-TOTAL-IVC ( #I1 ) ( EM = Z,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(5),pnd_Dest_Per_Pay.getValue(pnd_I1), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Dest_Div_Pay.getValue(pnd_I1), 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Dest_Units.getValue(pnd_I1), new ReportEditMask ("ZZZ,ZZ9.999"),pnd_Dest_Units_Total.getValue(pnd_I1), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Dest_Total_Amt.getValue(pnd_I1), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(3),pnd_Dest_Total_Ivc.getValue(pnd_I1), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED 1/03
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(7, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(10),pnd_Dest_Payees_Grand, new ReportEditMask                //Natural: WRITE ( 7 ) ///// 10X #DEST-PAYEES-GRAND ( EM = Z,ZZZ,ZZ9 ) 4X #DEST-PER-PAY-GRAND ( EM = ZZ,ZZZ,ZZ9.99 ) #DEST-DIV-PAY-GRAND ( EM = ZZ,ZZZ,ZZ9.99 ) #DEST-UNITS-GRAND ( EM = ZZ,ZZZ,ZZ9.999 ) #DEST-UNITS-TOTAL-GRAND ( EM = ZZZ,ZZZ,ZZ9.99 ) #DEST-TOTAL-AMT-GRAND ( EM = ZZZ,ZZZ,ZZ9.99 ) 3X#DEST-TOTAL-IVC-GRAND ( EM = Z,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(4),pnd_Dest_Per_Pay_Grand, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Dest_Div_Pay_Grand, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Dest_Units_Grand, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),pnd_Dest_Units_Total_Grand, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Dest_Total_Amt_Grand, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Dest_Total_Ivc_Grand, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Dist() throws Exception                                                                                                                        //Natural: ACCUM-DIST
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("DTRA")))      //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'CASH' OR = 'RINV' OR = 'DTRA'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1324 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #SAVE-CNTRCT-CURR-DIST-CDE;//Natural: VALUE 'IRAT'
        if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT"))))
        {
            decideConditionsMet1324++;
            pnd_Dest_Payees.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 1 )
            pnd_Dest_Occ.setValue(1);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 1
        }                                                                                                                                                                 //Natural: VALUE 'IRAC'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC"))))
        {
            decideConditionsMet1324++;
            pnd_Dest_Payees.getValue(2).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 2 )
            pnd_Dest_Occ.setValue(2);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 2
        }                                                                                                                                                                 //Natural: VALUE '03BT'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BT"))))
        {
            decideConditionsMet1324++;
            pnd_Dest_Payees.getValue(3).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 3 )
            pnd_Dest_Occ.setValue(3);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 3
        }                                                                                                                                                                 //Natural: VALUE '03BC'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BC"))))
        {
            decideConditionsMet1324++;
            pnd_Dest_Payees.getValue(4).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 4 )
            pnd_Dest_Occ.setValue(4);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 4
        }                                                                                                                                                                 //Natural: VALUE 'QPLT'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLT"))))
        {
            decideConditionsMet1324++;
            pnd_Dest_Payees.getValue(5).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 5 )
            pnd_Dest_Occ.setValue(5);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 5
        }                                                                                                                                                                 //Natural: VALUE 'QPLC'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLC"))))
        {
            decideConditionsMet1324++;
            //*  ADDED 1/03 NEW ROLL DEST
            pnd_Dest_Payees.getValue(6).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 6 )
            pnd_Dest_Occ.setValue(6);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 6
        }                                                                                                                                                                 //Natural: VALUE '57BT'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT"))))
        {
            decideConditionsMet1324++;
            //*  ADDED 1/03 NEW ROLL DEST
            //*  ADDED 1/03 NEW ROLL DEST
            //*  ADDED 1/03 NEW ROLL DEST
            pnd_Dest_Payees.getValue(7).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 7 )
            pnd_Dest_Occ.setValue(7);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 7
        }                                                                                                                                                                 //Natural: VALUE '57BC'
        else if (condition((pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC"))))
        {
            decideConditionsMet1324++;
            //*  ADDED 1/03 NEW ROLL DEST
            //*  ADDED 1/03 NEW ROLL DEST
            pnd_Dest_Payees.getValue(8).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 8 )
            pnd_Dest_Occ.setValue(8);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 8
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Dest_Payees.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #DEST-PAYEES ( 1 )
            pnd_Dest_Occ.setValue(1);                                                                                                                                     //Natural: ASSIGN #DEST-OCC := 1
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Fund() throws Exception                                                                                                                      //Natural: PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("T")))                                                                                                   //Natural: IF #SUMM-CMPNY-CDE = 'T'
        {
            //*  TPA FUNDS
            short decideConditionsMet1365 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #SAVE-CNTRCT-OPTN-CDE;//Natural: VALUE 28:30
            if (condition(((pnd_Save_Cntrct_Optn_Cde.greaterOrEqual(28) && pnd_Save_Cntrct_Optn_Cde.lessOrEqual(30)))))
            {
                decideConditionsMet1365++;
                //*  ADDED 1/03
                if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAX")  //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                    || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLT") 
                    || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC")))
                {
                    iaaa730_Tpa_Fund.nadd(1);                                                                                                                             //Natural: ADD 1 TO TPA-FUND
                    iaaa730_Tpa_Pay_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ADD #SUMM-PER-PYMNT TO TPA-PAY-AMT
                    iaaa730_Tpa_Div_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ADD #SUMM-PER-DVDND TO TPA-DIV-AMT
                    iaaa730_Tpa_Amt.compute(new ComputeParameters(false, iaaa730_Tpa_Amt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.add(pnd_Input_Record_Pnd_Summ_Per_Dvdnd).add(iaaa730_Tpa_Amt)); //Natural: ASSIGN TPA-AMT := #SUMM-PER-PYMNT + #SUMM-PER-DVDND + TPA-AMT
                    iaaa730_Pnd_Fin_Payment.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                        //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAYMENT ( 1 )
                    iaaa730_Pnd_Fin_Div.getValue(1).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                            //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 25,27
            else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27))))
            {
                decideConditionsMet1365++;
                iaaa730_Ipro_Fund.nadd(1);                                                                                                                                //Natural: ADD 1 TO IPRO-FUND
                iaaa730_Ipro_Pay_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                           //Natural: ADD #SUMM-PER-PYMNT TO IPRO-PAY-AMT
                iaaa730_Ipro_Div_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                           //Natural: ADD #SUMM-PER-DVDND TO IPRO-DIV-AMT
                iaaa730_Ipro_Amt.compute(new ComputeParameters(false, iaaa730_Ipro_Amt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.add(pnd_Input_Record_Pnd_Summ_Per_Dvdnd).add(iaaa730_Ipro_Amt)); //Natural: ASSIGN IPRO-AMT := #SUMM-PER-PYMNT + #SUMM-PER-DVDND + IPRO-AMT
                iaaa730_Pnd_Fin_Payment.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                            //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAYMENT ( 2 )
                iaaa730_Pnd_Fin_Div.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                                //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( 2 )
            }                                                                                                                                                             //Natural: VALUE 22
            else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(22))))
            {
                decideConditionsMet1365++;
                iaaa730_Pamp_I_Fund.nadd(1);                                                                                                                              //Natural: ADD 1 TO P&I-FUND
                iaaa730_Pamp_I_Pay_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                         //Natural: ADD #SUMM-PER-PYMNT TO P&I-PAY-AMT
                iaaa730_Pamp_I_Div_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                         //Natural: ADD #SUMM-PER-DVDND TO P&I-DIV-AMT
                iaaa730_Pamp_I_Amt.compute(new ComputeParameters(false, iaaa730_Pamp_I_Amt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.add(pnd_Input_Record_Pnd_Summ_Per_Dvdnd).add(iaaa730_Pamp_I_Amt)); //Natural: ASSIGN P&I-AMT := #SUMM-PER-PYMNT + #SUMM-PER-DVDND + P&I-AMT
                iaaa730_Pnd_Fin_Payment.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                            //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAYMENT ( 3 )
                iaaa730_Pnd_Fin_Div.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                                //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( 3 )
            }                                                                                                                                                             //Natural: VALUE 21
            else if (condition((pnd_Save_Cntrct_Optn_Cde.equals(21))))
            {
                decideConditionsMet1365++;
                iaaa730_Afslash_C_Fund.nadd(1);                                                                                                                           //Natural: ADD 1 TO A/C-FUND
                iaaa730_Afslash_C_Pay_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                      //Natural: ADD #SUMM-PER-PYMNT TO A/C-PAY-AMT
                iaaa730_Afslash_C_Div_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                      //Natural: ADD #SUMM-PER-DVDND TO A/C-DIV-AMT
                iaaa730_Afslash_C_Amt.compute(new ComputeParameters(false, iaaa730_Afslash_C_Amt), pnd_Input_Record_Pnd_Summ_Per_Pymnt.add(pnd_Input_Record_Pnd_Summ_Per_Dvdnd).add(iaaa730_Afslash_C_Amt)); //Natural: ASSIGN A/C-AMT := #SUMM-PER-PYMNT + #SUMM-PER-DVDND + A/C-AMT
                iaaa730_Pnd_Fin_Payment.getValue(4).nadd(pnd_Input_Record_Pnd_Summ_Fin_Pymnt);                                                                            //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAYMENT ( 4 )
                iaaa730_Pnd_Fin_Div.getValue(4).nadd(pnd_Input_Record_Pnd_Summ_Fin_Dvdnd);                                                                                //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( 4 )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1402 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SUMM-CMPNY-CDE = 'T'
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("T")))
        {
            decideConditionsMet1402++;
            if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("CASH") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("RINV") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("DTRA")))  //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'CASH' OR = 'RINV' OR = 'DTRA'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dest_Per_Pay.getValue(pnd_Dest_Occ).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                        //Natural: ADD #SUMM-PER-PYMNT TO #DEST-PER-PAY ( #DEST-OCC )
                pnd_Dest_Div_Pay.getValue(pnd_Dest_Occ).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                        //Natural: ADD #SUMM-PER-DVDND TO #DEST-DIV-PAY ( #DEST-OCC )
            }                                                                                                                                                             //Natural: END-IF
            //*  ADD 12132001
            //*  ADDED 1/03
            if (condition(pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("IRAX")    //Natural: IF #SAVE-CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("03BC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLT") 
                || pnd_Save_Cntrct_Curr_Dist_Cde.equals("QPLC") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BT") || pnd_Save_Cntrct_Curr_Dist_Cde.equals("57BC")))
            {
                iaaa730_Tiaa_Fund.nadd(1);                                                                                                                                //Natural: ADD 1 TO TIAA-FUND
                iaaa730_Per_Pay_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                            //Natural: ADD #SUMM-PER-PYMNT TO IAAA730.PER-PAY-AMT
                pnd_Per_Amt_1.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                                  //Natural: ADD #SUMM-PER-PYMNT TO #PER-AMT-1
                iaaa730_Per_Div_Amt.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                            //Natural: ADD #SUMM-PER-DVDND TO IAAA730.PER-DIV-AMT
                pnd_Div_Amt_1.nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                                  //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21)))                                                                                                           //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21
            {
                                                                                                                                                                          //Natural: PERFORM LOAD-LEDGER-TIAA
                sub_Load_Ledger_Tiaa();
                if (condition(Global.isEscape())) {return;}
                //*  REAL ESTATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #SUMM-FUND-CDE = '09'
        if (condition(pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("09")))
        {
            decideConditionsMet1402++;
            pnd_Units_1.nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                                            //Natural: ADD #SUMM-UNITS TO #UNITS-1
            pnd_Dest_Units.getValue(pnd_Dest_Occ).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                  //Natural: ADD #SUMM-UNITS TO #DEST-UNITS ( #DEST-OCC )
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = 'W'
            {
                iaaa730_Cref_Units_Mnthly.getValue(9).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                              //Natural: ADD #SUMM-UNITS TO CREF-UNITS-MNTHLY ( 9 )
                ia_Aian026_Linkage_Ia_Fund_Code.setValue("R");                                                                                                            //Natural: ASSIGN IA-FUND-CODE := 'R'
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-CREF-UNIT-VALUE
                sub_Call_Aian026_Cref_Unit_Value();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Return_Code.equals(getZero())))                                                                                                         //Natural: IF #RETURN-CODE = 0
                {
                    pnd_Units_Total.compute(new ComputeParameters(true, pnd_Units_Total), ia_Aian026_Linkage_Auv_Returned.multiply(pnd_Units_1));                         //Natural: COMPUTE ROUNDED #UNITS-TOTAL = AUV-RETURNED * #UNITS-1
                    pnd_Total.nadd(pnd_Units_Total);                                                                                                                      //Natural: ADD #UNITS-TOTAL TO #TOTAL
                    pnd_Cref_Dol.compute(new ComputeParameters(true, pnd_Cref_Dol), ia_Aian026_Linkage_Auv_Returned.multiply(iaaa730_Cref_Units_Mnthly.getValue(9)));     //Natural: COMPUTE ROUNDED #CREF-DOL = AUV-RETURNED * CREF-UNITS-MNTHLY ( 9 )
                }                                                                                                                                                         //Natural: END-IF
                iaaa730_Cref_Fund_Payees_Mnthly.getValue(9).nadd(1);                                                                                                      //Natural: ADD 1 TO CREF-FUND-PAYEES-MNTHLY ( 9 )
                iaaa730_Cref_Amt_Mnthly.getValue(9).nadd(pnd_Cref_Dol);                                                                                                   //Natural: ADD #CREF-DOL TO CREF-AMT-MNTHLY ( 9 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = 'U'
            {
                iaaa730_Cref_Fund_Payees.getValue(9).nadd(1);                                                                                                             //Natural: ADD 1 TO IAAA730.CREF-FUND-PAYEES ( 9 )
                iaaa730_Cref_Units.getValue(9).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                     //Natural: ADD #SUMM-UNITS TO CREF-UNITS ( 9 )
                pnd_Cref_Dol.setValue(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                               //Natural: ASSIGN #CREF-DOL := #SUMM-PER-PYMNT
                iaaa730_Cref_Amt.getValue(9).compute(new ComputeParameters(false, iaaa730_Cref_Amt.getValue(9)), pnd_Cref_Dol.add(iaaa730_Cref_Amt.getValue(9)));         //Natural: ASSIGN CREF-AMT ( 9 ) := #CREF-DOL + CREF-AMT ( 9 )
                pnd_Total.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                                      //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dest_Units_Total.getValue(pnd_Dest_Occ).nadd(pnd_Cref_Dol);                                                                                               //Natural: ASSIGN #DEST-UNITS-TOTAL ( #DEST-OCC ) := #DEST-UNITS-TOTAL ( #DEST-OCC ) + #CREF-DOL
            //*  TIAA ACCESS
                                                                                                                                                                          //Natural: PERFORM LOAD-LEDGER-CREF
            sub_Load_Ledger_Cref();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #SUMM-FUND-CDE = '11'
        if (condition(pnd_Input_Record_Pnd_Summ_Fund_Cde.equals("11")))
        {
            decideConditionsMet1402++;
            pnd_Units_1.nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                                            //Natural: ADD #SUMM-UNITS TO #UNITS-1
            pnd_Dest_Units.getValue(pnd_Dest_Occ).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                  //Natural: ADD #SUMM-UNITS TO #DEST-UNITS ( #DEST-OCC )
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("W")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = 'W'
            {
                iaaa730_Cref_Units_Mnthly.getValue(11).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                             //Natural: ADD #SUMM-UNITS TO CREF-UNITS-MNTHLY ( 11 )
                ia_Aian026_Linkage_Ia_Fund_Code.setValue("D");                                                                                                            //Natural: ASSIGN IA-FUND-CODE := 'D'
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-CREF-UNIT-VALUE
                sub_Call_Aian026_Cref_Unit_Value();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Return_Code.equals(getZero())))                                                                                                         //Natural: IF #RETURN-CODE = 0
                {
                    pnd_Units_Total.compute(new ComputeParameters(true, pnd_Units_Total), ia_Aian026_Linkage_Auv_Returned.multiply(pnd_Units_1));                         //Natural: COMPUTE ROUNDED #UNITS-TOTAL = AUV-RETURNED * #UNITS-1
                    pnd_Total.nadd(pnd_Units_Total);                                                                                                                      //Natural: ADD #UNITS-TOTAL TO #TOTAL
                    pnd_Cref_Dol.compute(new ComputeParameters(true, pnd_Cref_Dol), ia_Aian026_Linkage_Auv_Returned.multiply(iaaa730_Cref_Units_Mnthly.getValue(11)));    //Natural: COMPUTE ROUNDED #CREF-DOL = AUV-RETURNED * CREF-UNITS-MNTHLY ( 11 )
                }                                                                                                                                                         //Natural: END-IF
                iaaa730_Cref_Fund_Payees_Mnthly.getValue(11).nadd(1);                                                                                                     //Natural: ADD 1 TO CREF-FUND-PAYEES-MNTHLY ( 11 )
                iaaa730_Cref_Amt_Mnthly.getValue(11).nadd(pnd_Cref_Dol);                                                                                                  //Natural: ADD #CREF-DOL TO CREF-AMT-MNTHLY ( 11 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("U")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = 'U'
            {
                iaaa730_Cref_Fund_Payees.getValue(11).nadd(1);                                                                                                            //Natural: ADD 1 TO IAAA730.CREF-FUND-PAYEES ( 11 )
                iaaa730_Cref_Units.getValue(11).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                    //Natural: ADD #SUMM-UNITS TO CREF-UNITS ( 11 )
                pnd_Cref_Dol.setValue(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                               //Natural: ASSIGN #CREF-DOL := #SUMM-PER-PYMNT
                iaaa730_Cref_Amt.getValue(11).compute(new ComputeParameters(false, iaaa730_Cref_Amt.getValue(11)), pnd_Cref_Dol.add(iaaa730_Cref_Amt.getValue(11)));      //Natural: ASSIGN CREF-AMT ( 11 ) := #CREF-DOL + CREF-AMT ( 11 )
                pnd_Total.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                                      //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dest_Units_Total.getValue(pnd_Dest_Occ).nadd(pnd_Cref_Dol);                                                                                               //Natural: ASSIGN #DEST-UNITS-TOTAL ( #DEST-OCC ) := #DEST-UNITS-TOTAL ( #DEST-OCC ) + #CREF-DOL
                                                                                                                                                                          //Natural: PERFORM LOAD-LEDGER-CREF
            sub_Load_Ledger_Cref();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #SUMM-CMPNY-CDE = '2' OR = '4'
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2") || pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4")))
        {
            decideConditionsMet1402++;
            pnd_Fund_Cde_2.setValue(pnd_Input_Record_Pnd_Summ_Fund_Cde);                                                                                                  //Natural: ASSIGN #FUND-CDE-2 := #SUMM-FUND-CDE
            pnd_Units_1.nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                                                            //Natural: ADD #SUMM-UNITS TO #UNITS-1
            //*  MONTHLY UNIT VALUE
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("4")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = '4'
            {
                iaaa730_Cref_Units_Mnthly.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                 //Natural: ADD #SUMM-UNITS TO CREF-UNITS-MNTHLY ( #FUND-CDE-1B )
                ia_Aian026_Linkage_Ia_Fund_Code.setValue(pnd_Cref_Fund_Cdes_Pnd_Cref_Array.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b));                                     //Natural: ASSIGN IA-FUND-CODE := #CREF-ARRAY ( #FUND-CDE-1B )
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-CREF-UNIT-VALUE
                sub_Call_Aian026_Cref_Unit_Value();
                if (condition(Global.isEscape())) {return;}
                //*  TEST
                getReports().write(0, "=",ia_Aian026_Linkage_Auv_Returned,"MNTHLY");                                                                                      //Natural: WRITE '=' AUV-RETURNED 'MNTHLY'
                if (Global.isEscape()) return;
                if (condition(pnd_Return_Code.equals(getZero())))                                                                                                         //Natural: IF #RETURN-CODE = 0
                {
                    pnd_Units_Total.compute(new ComputeParameters(true, pnd_Units_Total), ia_Aian026_Linkage_Auv_Returned.multiply(pnd_Units_1));                         //Natural: COMPUTE ROUNDED #UNITS-TOTAL = AUV-RETURNED * #UNITS-1
                    pnd_Total.nadd(pnd_Units_Total);                                                                                                                      //Natural: ADD #UNITS-TOTAL TO #TOTAL
                    pnd_Cref_Dol.setValue(pnd_Units_Total);                                                                                                               //Natural: ASSIGN #CREF-DOL := #UNITS-TOTAL
                }                                                                                                                                                         //Natural: END-IF
                iaaa730_Cref_Fund_Payees_Mnthly.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(1);                                                                         //Natural: ADD 1 TO CREF-FUND-PAYEES-MNTHLY ( #FUND-CDE-1B )
                iaaa730_Cref_Amt_Mnthly.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(pnd_Cref_Dol);                                                                      //Natural: ADD #CREF-DOL TO CREF-AMT-MNTHLY ( #FUND-CDE-1B )
            }                                                                                                                                                             //Natural: END-IF
            //*  ANNUAL UNIT VALUE
            if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Cde.equals("2")))                                                                                               //Natural: IF #SUMM-CMPNY-CDE = '2'
            {
                iaaa730_Cref_Fund_Payees.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(1);                                                                                //Natural: ADD 1 TO IAAA730.CREF-FUND-PAYEES ( #FUND-CDE-1B )
                pnd_Dest_Units.getValue(pnd_Dest_Occ).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                                              //Natural: ADD #SUMM-UNITS TO #DEST-UNITS ( #DEST-OCC )
                iaaa730_Cref_Units.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(pnd_Input_Record_Pnd_Summ_Units);                                                        //Natural: ADD #SUMM-UNITS TO IAAA730.CREF-UNITS ( #FUND-CDE-1B )
                pnd_Cref_Dol.setValue(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                               //Natural: ASSIGN #CREF-DOL := #SUMM-PER-PYMNT
                pnd_Total.nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                                      //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL
                iaaa730_Cref_Amt.getValue(pnd_Fund_Cde_2_Pnd_Fund_Cde_1b).nadd(pnd_Cref_Dol);                                                                             //Natural: ASSIGN CREF-AMT ( #FUND-CDE-1B ) := CREF-AMT ( #FUND-CDE-1B ) + #CREF-DOL
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dest_Units_Total.getValue(pnd_Dest_Occ).nadd(pnd_Cref_Dol);                                                                                               //Natural: ASSIGN #DEST-UNITS-TOTAL ( #DEST-OCC ) := #DEST-UNITS-TOTAL ( #DEST-OCC ) + #CREF-DOL
                                                                                                                                                                          //Natural: PERFORM LOAD-LEDGER-CREF
            sub_Load_Ledger_Cref();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1402 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Load_Ledger_Tiaa() throws Exception                                                                                                                  //Natural: LOAD-LEDGER-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        short decideConditionsMet1514 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CNTRCT-PPCN-NBR = 'IA000000' THRU 'IH999999' OR #CNTRCT-PPCN-NBR = 'IJ999999'
        if (condition(((pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("IA000000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("IH999999")) 
            || pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("IJ999999"))))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 2 ) := #ACCOUNT-VAL ( 2 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 3 ) := #ACCOUNT-VAL ( 3 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'Y0000000' THRU 'Y9999999'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("Y0000000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("Y9999999")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(2).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 2 ) := #ACCOUNT-VAL ( 2 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(3).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 3 ) := #ACCOUNT-VAL ( 3 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'S0000010' THRU 'S0869999'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("S0000010") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("S0869999")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(4).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 4 ) := #ACCOUNT-VAL ( 4 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(5).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 5 ) := #ACCOUNT-VAL ( 5 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'S0880000' THRU 'S0889990'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("S0880000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("S0889990")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(6).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 6 ) := #ACCOUNT-VAL ( 6 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(7).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 7 ) := #ACCOUNT-VAL ( 7 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'S0870000' THRU 'S0879999'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("S0870000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("S0879999")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(8).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 8 ) := #ACCOUNT-VAL ( 8 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(9).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 9 ) := #ACCOUNT-VAL ( 9 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'S0890000' THRU 'S0899999'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("S0890000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("S0899999")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(10).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                       //Natural: ASSIGN #ACCOUNT-VAL ( 10 ) := #ACCOUNT-VAL ( 10 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(11).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                       //Natural: ASSIGN #ACCOUNT-VAL ( 11 ) := #ACCOUNT-VAL ( 11 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-PPCN-NBR = 'W0250000' THRU 'W0899999'
        else if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.greaterOrEqual("W0250000") && pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.lessOrEqual("W0899999")))
        {
            decideConditionsMet1514++;
            pnd_Account_Val.getValue(12).nadd(pnd_Input_Record_Pnd_Summ_Per_Pymnt);                                                                                       //Natural: ASSIGN #ACCOUNT-VAL ( 12 ) := #ACCOUNT-VAL ( 12 ) + #SUMM-PER-PYMNT
            pnd_Account_Val.getValue(13).nadd(pnd_Input_Record_Pnd_Summ_Per_Dvdnd);                                                                                       //Natural: ASSIGN #ACCOUNT-VAL ( 13 ) := #ACCOUNT-VAL ( 13 ) + #SUMM-PER-DVDND
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Load_Ledger_Cref() throws Exception                                                                                                                  //Natural: LOAD-LEDGER-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  ADDED 1/98
        pnd_I2.reset();                                                                                                                                                   //Natural: RESET #I2
        pnd_Save_Summ_Cmpny_Fnd_Cde.setValue(pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde);                                                                                    //Natural: ASSIGN #SAVE-SUMM-CMPNY-FND-CDE := #SUMM-CMPNY-FND-CDE
        if (condition(pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde.equals("W")))                                                                                   //Natural: IF #SAVE-SUMM-CMPNY-CDE = 'W'
        {
            pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde.setValue("U");                                                                                            //Natural: ASSIGN #SAVE-SUMM-CMPNY-CDE := 'U'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde.equals("4")))                                                                                   //Natural: IF #SAVE-SUMM-CMPNY-CDE = '4'
        {
            pnd_Save_Summ_Cmpny_Fnd_Cde_Pnd_Save_Summ_Cmpny_Cde.setValue("2");                                                                                            //Natural: ASSIGN #SAVE-SUMM-CMPNY-CDE := '2'
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I = 35 TO 60
        for (pnd_I.setValue(35); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Acct_Info.setValue(pnd_Account_Key.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #ACCT-INFO := #ACCOUNT-KEY ( #I )
            if (condition(pnd_Save_Summ_Cmpny_Fnd_Cde.equals(pnd_Acct_Info_Pnd_Acct_Key1) && pnd_Acct_Info_Pnd_Acct_Rpt_Num.equals(2)))                                   //Natural: IF #SAVE-SUMM-CMPNY-FND-CDE = #ACCT-KEY1 AND #ACCT-RPT-NUM = 2
            {
                pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(pnd_I));                                                                                           //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( #I )
                pnd_Sve_Account_Num.setValue(pnd_Account_Num_Info_Pnd_Account_Num4);                                                                                      //Natural: ASSIGN #SVE-ACCOUNT-NUM := #ACCOUNT-NUM4
                pnd_I2.setValue(pnd_I);                                                                                                                                   //Natural: ASSIGN #I2 := #I
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_I2.equals(getZero())))                                                                                                                          //Natural: IF #I2 = 0
        {
            getReports().write(4, ReportOption.NOTITLE,"ERROR ERROR ERROR ERROR FOLLOWING");                                                                              //Natural: WRITE ( 4 ) 'ERROR ERROR ERROR ERROR FOLLOWING'
            if (Global.isEscape()) return;
            getReports().write(4, ReportOption.NOTITLE,"COMPANY & FUND CODE NOT FOUND IN LEDGER ACCT TABLE-->IAAL730");                                                   //Natural: WRITE ( 4 ) 'COMPANY & FUND CODE NOT FOUND IN LEDGER ACCT TABLE-->IAAL730'
            if (Global.isEscape()) return;
            getReports().write(4, ReportOption.NOTITLE,"#SUMM-CMPNY-FND-CDE-->",pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde);                                                 //Natural: WRITE ( 4 ) '#SUMM-CMPNY-FND-CDE-->' #SUMM-CMPNY-FND-CDE
            if (Global.isEscape()) return;
            getReports().write(4, ReportOption.NOTITLE,"#CNTRCT-PPCN-NBR-->",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                 //Natural: WRITE ( 4 ) '#CNTRCT-PPCN-NBR-->' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED Z 8/96
        if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,"'0M'") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,"'0T'")                //Natural: IF #CNTRCT-PPCN-NBR = MASK ( '0M' ) OR = MASK ( '0T' ) OR = MASK ( '0U' ) OR = MASK ( '6M' ) OR = MASK ( 'Z' )
            || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,"'0U'") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,"'6M'") || DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,
            "'Z'")))
        {
            pnd_Account_Val.getValue(pnd_I2).nadd(pnd_Cref_Dol);                                                                                                          //Natural: ASSIGN #ACCOUNT-VAL ( #I2 ) := #ACCOUNT-VAL ( #I2 ) + #CREF-DOL
            pnd_Account_Val3.getValue(pnd_I2).nadd(pnd_Cref_Dol);                                                                                                         //Natural: ASSIGN #ACCOUNT-VAL3 ( #I2 ) := #ACCOUNT-VAL3 ( #I2 ) + #CREF-DOL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Account_Val2.getValue(pnd_I2).nadd(pnd_Cref_Dol);                                                                                                         //Natural: ASSIGN #ACCOUNT-VAL2 ( #I2 ) := #ACCOUNT-VAL2 ( #I2 ) + #CREF-DOL
            pnd_Account_Val3.getValue(pnd_I2).nadd(pnd_Cref_Dol);                                                                                                         //Natural: ASSIGN #ACCOUNT-VAL3 ( #I2 ) := #ACCOUNT-VAL3 ( #I2 ) + #CREF-DOL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED W09 TO FOLLOWING FOR CREF MONTHLY    1/98
        //*  REAL ESTATE
        //*  TIAA ACCESS
        if (condition(pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde.equals("U09") || pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde.equals("W09") || pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde.equals("U11")  //Natural: IF #SUMM-CMPNY-FND-CDE = 'U09' OR = 'W09' OR = 'U11' OR = 'W11'
            || pnd_Input_Record_Pnd_Summ_Cmpny_Fnd_Cde.equals("W11")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sve_Account_Num.setValue(pnd_Account_Num_Info_Pnd_Account_Num4);                                                                                              //Natural: ASSIGN #SVE-ACCOUNT-NUM := #ACCOUNT-NUM4
        FOR05:                                                                                                                                                            //Natural: FOR #I1 = 1 TO 35
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(35)); pnd_I1.nadd(1))
        {
            pnd_Acct_Info.setValue(pnd_Account_Key.getValue(pnd_I1));                                                                                                     //Natural: ASSIGN #ACCT-INFO := #ACCOUNT-KEY ( #I1 )
            pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(pnd_I1));                                                                                              //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( #I1 )
            if (condition(pnd_Sve_Account_Num.equals(pnd_Account_Num_Info_Pnd_Account_Num1)))                                                                             //Natural: IF #SVE-ACCOUNT-NUM = #ACCOUNT-NUM1
            {
                pnd_Account_Val.getValue(pnd_I1).nadd(pnd_Cref_Dol);                                                                                                      //Natural: ASSIGN #ACCOUNT-VAL ( #I1 ) := #ACCOUNT-VAL ( #I1 ) + #CREF-DOL
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Load_Ledger_Final() throws Exception                                                                                                                 //Natural: LOAD-LEDGER-FINAL
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #I = 2 TO 60
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Acct_Info.setValue(pnd_Account_Key.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #ACCT-INFO := #ACCOUNT-KEY ( #I )
            pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(pnd_I));                                                                                               //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( #I )
            if (condition(((pnd_Acct_Info_Pnd_Acct_Key1.equals("U09") || pnd_Acct_Info_Pnd_Acct_Key1.equals("U11")) && pnd_Acct_Info_Pnd_Acct_Rpt_Num.equals(2))))        //Natural: IF #ACCT-KEY1 = 'U09' OR = 'U11' AND #ACCT-RPT-NUM = 2
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Acct_Info_Pnd_Acct_Key1.equals("U09") || pnd_Acct_Info_Pnd_Acct_Key1.equals("U11")))                                                        //Natural: IF #ACCT-KEY1 = 'U09' OR = 'U11'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Account_Num_Info_Pnd_Account_Num1.notEquals("00000000")))                                                                                   //Natural: IF #ACCOUNT-NUM1 NE '00000000'
            {
                pnd_Account_Val.getValue(1).nadd(pnd_Account_Val.getValue(pnd_I));                                                                                        //Natural: ASSIGN #ACCOUNT-VAL ( 1 ) := #ACCOUNT-VAL ( 1 ) + #ACCOUNT-VAL ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Report_8() throws Exception                                                                                                                    //Natural: PRINT-REPORT-8
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  LOAD TIAA ACCESS
                                                                                                                                                                          //Natural: PERFORM LOAD-LEDGER-FINAL
        sub_Load_Ledger_Final();
        if (condition(Global.isEscape())) {return;}
        pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(2));                                                                                                       //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( 2 )
        pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(2));                                                                                                       //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( 2 )
        pnd_Sve_Account_Num.setValue(pnd_Account_Num_Info_Pnd_Account_Num1);                                                                                              //Natural: ASSIGN #SVE-ACCOUNT-NUM := #ACCOUNT-NUM1
        pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(1));                                                                                                       //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( 1 )
        getReports().write(8, ReportOption.NOTITLE,"TIAA",new ColumnSpacing(18),pnd_Sve_Account_Num,new ColumnSpacing(6),pnd_Account_Val.getValue(2),new                  //Natural: WRITE ( 8 ) 'TIAA' 18X #SVE-ACCOUNT-NUM 6X #ACCOUNT-VAL ( 2 ) 8X #ACCOUNT-NUM1 6X #ACCOUNT-VAL ( 1 )
            ColumnSpacing(8),pnd_Account_Num_Info_Pnd_Account_Num1,new ColumnSpacing(6),pnd_Account_Val.getValue(1));
        if (Global.isEscape()) return;
        pnd_Account_Val.getValue(23).setValue(pnd_Account_Val2.getValue(36));                                                                                             //Natural: ASSIGN #ACCOUNT-VAL ( 23 ) := #ACCOUNT-VAL2 ( 36 )
        FOR07:                                                                                                                                                            //Natural: FOR #I1 = 3 TO 60
        for (pnd_I1.setValue(3); condition(pnd_I1.lessOrEqual(60)); pnd_I1.nadd(1))
        {
            pnd_Acct_Info.setValue(pnd_Account_Key.getValue(pnd_I1));                                                                                                     //Natural: ASSIGN #ACCT-INFO := #ACCOUNT-KEY ( #I1 )
            pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(pnd_I1));                                                                                              //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( #I1 )
            if (condition(((pnd_Acct_Info_Pnd_Acct_Key1.equals("U09") || pnd_Acct_Info_Pnd_Acct_Key1.equals("U11")) && pnd_Acct_Info_Pnd_Acct_Rpt_Num.equals(2))))        //Natural: IF #ACCT-KEY1 = 'U09' OR = 'U11' AND #ACCT-RPT-NUM = 2
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pnd_Acct_Info_Pnd_Acct_Key1.equals("U09") || pnd_Acct_Info_Pnd_Acct_Key1.equals("U11")) && pnd_Acct_Info_Pnd_Acct_Rpt_Num.equals(1))))    //Natural: IF #ACCT-KEY1 = 'U09' OR = 'U11' AND #ACCT-RPT-NUM = 1
                {
                    if (condition(pnd_Acct_Info_Pnd_Acct_Key1.equals("U09")))                                                                                             //Natural: IF #ACCT-KEY1 = 'U09'
                    {
                        pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(35));                                                                                      //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( 35 )
                        getReports().write(8, ReportOption.NOTITLE,"REAL ESTATE",new ColumnSpacing(11),pnd_Account_Num_Info_Pnd_Account_Num1,new ColumnSpacing(6),pnd_Account_Val.getValue(35),new  //Natural: WRITE ( 8 ) 'REAL ESTATE' 11X #ACCOUNT-NUM1 6X #ACCOUNT-VAL ( 35 ) 8X #ACCOUNT-NUM3 6X #ACCOUNT-VAL3 ( 35 )
                            ColumnSpacing(8),pnd_Account_Num_Info_Pnd_Account_Num3,new ColumnSpacing(6),pnd_Account_Val3.getValue(35));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(8, ReportOption.NOTITLE,new ColumnSpacing(22),pnd_Account_Num_Info_Pnd_Account_Num2,new ColumnSpacing(6),pnd_Account_Val2.getValue(35)); //Natural: WRITE ( 8 ) 22X #ACCOUNT-NUM2 6X #ACCOUNT-VAL2 ( 35 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(36));                                                                                      //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( 36 )
                        getReports().write(8, ReportOption.NOTITLE,"TIAA ACCESS",new ColumnSpacing(11),pnd_Account_Num_Info_Pnd_Account_Num1,new ColumnSpacing(6),pnd_Account_Val.getValue(36),new  //Natural: WRITE ( 8 ) 'TIAA ACCESS' 11X #ACCOUNT-NUM1 6X #ACCOUNT-VAL ( 36 ) 8X #ACCOUNT-NUM3 6X #ACCOUNT-VAL3 ( 36 )
                            ColumnSpacing(8),pnd_Account_Num_Info_Pnd_Account_Num3,new ColumnSpacing(6),pnd_Account_Val3.getValue(36));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(8, ReportOption.NOTITLE,new ColumnSpacing(22),pnd_Account_Num_Info_Pnd_Account_Num2,new ColumnSpacing(6),pnd_Account_Val2.getValue(36)); //Natural: WRITE ( 8 ) 22X #ACCOUNT-NUM2 6X #ACCOUNT-VAL2 ( 36 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Account_Num_Info_Pnd_Account_Num1.notEquals("00000000")))                                                                           //Natural: IF #ACCOUNT-NUM1 NE '00000000'
                    {
                        getReports().write(8, ReportOption.NOTITLE,new ColumnSpacing(22),pnd_Account_Num_Info_Pnd_Account_Num1,new ColumnSpacing(6),pnd_Account_Val.getValue(pnd_I1)); //Natural: WRITE ( 8 ) 22X #ACCOUNT-NUM1 6X #ACCOUNT-VAL ( #I1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Report_9() throws Exception                                                                                                                    //Natural: PRINT-REPORT-9
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #I2 = #I1 TO 60
        for (pnd_I2.setValue(pnd_I1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Acct_Info.setValue(pnd_Account_Key.getValue(pnd_I2));                                                                                                     //Natural: ASSIGN #ACCT-INFO := #ACCOUNT-KEY ( #I2 )
            pnd_Account_Num_Info.setValue(pnd_Account_Num.getValue(pnd_I2));                                                                                              //Natural: ASSIGN #ACCOUNT-NUM-INFO := #ACCOUNT-NUM ( #I2 )
            if (condition(pnd_Account_Num_Info_Pnd_Account_Num1.notEquals("00000000")))                                                                                   //Natural: IF #ACCOUNT-NUM1 NE '00000000'
            {
                getReports().write(8, ReportOption.NOTITLE,NEWLINE,pnd_Acct_Info_Pnd_Acct_Desc,new ColumnSpacing(11),pnd_Account_Num_Info_Pnd_Account_Num1,new            //Natural: WRITE ( 8 ) / #ACCT-DESC 11X #ACCOUNT-NUM1 6X #ACCOUNT-VAL ( #I2 )
                    ColumnSpacing(6),pnd_Account_Val.getValue(pnd_I2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(8, ReportOption.NOTITLE,new ColumnSpacing(22),pnd_Account_Num_Info_Pnd_Account_Num2,new ColumnSpacing(6),pnd_Account_Val2.getValue(pnd_I2),new  //Natural: WRITE ( 8 ) 22X #ACCOUNT-NUM2 6X #ACCOUNT-VAL2 ( #I2 ) 8X #ACCOUNT-NUM3 6X #ACCOUNT-VAL3 ( #I2 )
                    ColumnSpacing(8),pnd_Account_Num_Info_Pnd_Account_Num3,new ColumnSpacing(6),pnd_Account_Val3.getValue(pnd_I2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Payment_Due() throws Exception                                                                                                                 //Natural: CHECK-PAYMENT-DUE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Payment_Due.reset();                                                                                                                                          //Natural: RESET #PAYMENT-DUE
        short decideConditionsMet1671 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(100))))
        {
            decideConditionsMet1671++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(601))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7) ||                   //Natural: IF #CNTRL-MM = 1 OR = 4 OR = 7 OR = 10
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(602))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8) ||                   //Natural: IF #CNTRL-MM = 2 OR = 5 OR = 8 OR = 11
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(603))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9) ||                   //Natural: IF #CNTRL-MM = 3 OR = 6 OR = 9 OR = 12
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(701))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7)))                                                              //Natural: IF #CNTRL-MM = 1 OR = 7
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(702))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8)))                                                              //Natural: IF #CNTRL-MM = 2 OR = 8
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(703))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9)))                                                              //Natural: IF #CNTRL-MM = 3 OR = 9
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(704))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))                                                             //Natural: IF #CNTRL-MM = 4 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(705))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))                                                             //Natural: IF #CNTRL-MM = 5 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(706))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))                                                             //Natural: IF #CNTRL-MM = 6 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(801))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1)))                                                                                                        //Natural: IF #CNTRL-MM = 1
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(802))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2)))                                                                                                        //Natural: IF #CNTRL-MM = 2
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(803))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3)))                                                                                                        //Natural: IF #CNTRL-MM = 3
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(804))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4)))                                                                                                        //Natural: IF #CNTRL-MM = 4
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(805))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5)))                                                                                                        //Natural: IF #CNTRL-MM = 5
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(806))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6)))                                                                                                        //Natural: IF #CNTRL-MM = 6
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(807))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7)))                                                                                                        //Natural: IF #CNTRL-MM = 7
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(808))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8)))                                                                                                        //Natural: IF #CNTRL-MM = 8
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(809))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9)))                                                                                                        //Natural: IF #CNTRL-MM = 9
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(810))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))                                                                                                       //Natural: IF #CNTRL-MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(811))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))                                                                                                       //Natural: IF #CNTRL-MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Input_Record_Pnd_Cntrct_Mode_Ind.equals(812))))
        {
            decideConditionsMet1671++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))                                                                                                       //Natural: IF #CNTRL-MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Call_Iaan0500_Read_Extern_File() throws Exception                                                                                                    //Natural: CALL-IAAN0500-READ-EXTERN-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  GET TIAA & CREF ACTIVE PRODUCTS
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Max_Prd_Cde.setValue(pdaIaaa051z.getIaaa051z_Pnd_Cref_Cnt());                                                                                                 //Natural: ASSIGN #MAX-PRD-CDE := IAAA051Z.#CREF-CNT
    }
    private void sub_Call_Aian026_Cref_Unit_Value() throws Exception                                                                                                      //Natural: CALL-AIAN026-CREF-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  MONTHLY
        ia_Aian026_Linkage_Return_Code.reset();                                                                                                                           //Natural: RESET RETURN-CODE AUV-RETURNED AUV-DATE-RETURN
        ia_Aian026_Linkage_Auv_Returned.reset();
        ia_Aian026_Linkage_Auv_Date_Return.reset();
        ia_Aian026_Linkage_Ia_Call_Type.setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                                  //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
        ia_Aian026_Linkage_Ia_Reval_Methd.setValue("M");                                                                                                                  //Natural: ASSIGN IA-REVAL-METHD := 'M'
        //*  L FOR LATEST AVAILABLE FACTOR
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals(pnd_Const_Pnd_Latest_Factor)))                                                                         //Natural: IF #CALL-TYPE = #LATEST-FACTOR
        {
            ia_Aian026_Linkage_Ia_Call_Type.setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ia_Aian026_Linkage_Ia_Check_Date.setValue(99999999);                                                                                                          //Natural: ASSIGN IA-CHECK-DATE := 99999999
            ia_Aian026_Linkage_Ia_Issue_Date.reset();                                                                                                                     //Natural: RESET IA-ISSUE-DATE
            //*  "P"= PAYMENT FOR ORIGINAL SETTLEMENT UNITS (FIRST OF MONTH ONLY
            //*  P FOR FACTOR USED FOR PMT
            //*  USED FOR 1ST OF MNTH PMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ia_Aian026_Linkage_Ia_Check_Date_Ccyymm.setValue(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm);                                                                           //Natural: ASSIGN IA-CHECK-DATE-CCYYMM := #CNTRL-YYYYMM
            ia_Aian026_Linkage_Ia_Check_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN IA-CHECK-DD := 01
            ia_Aian026_Linkage_Ia_Issue_Ccyymm.setValue(pnd_Save_Issue_Dte);                                                                                              //Natural: ASSIGN IA-ISSUE-CCYYMM := #SAVE-ISSUE-DTE
            if (condition(DbsUtil.maskMatches(pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd,"DD")))                                                                            //Natural: IF #CNTRCT-ISSUE-DTE-DD EQ MASK ( DD )
            {
                ia_Aian026_Linkage_Ia_Issue_Day.setValue(pnd_Save_Issue_Dte_Dd);                                                                                          //Natural: ASSIGN IA-ISSUE-DAY := #SAVE-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ia_Aian026_Linkage_Ia_Issue_Day.setValue(1);                                                                                                              //Natural: ASSIGN IA-ISSUE-DAY := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NEW ACTUARY MODULE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ia_Aian026_Linkage);                                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ia_Aian026_Linkage_Return_Code.notEquals(getZero())))                                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  ERROR                                 *");                                                                          //Natural: WRITE '*                  ERROR                                 *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an RETURN CODE of =",ia_Aian026_Linkage_Return_Code);                                                              //Natural: WRITE '* and returned with an RETURN CODE of =' RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Call_Type);                                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Fund_Code);                                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Reval_Methd);                                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Check_Date);                                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Issue_Date);                                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ia_Aian026_Linkage_Auv_Date_Return);                                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ia_Aian026_Linkage_Auv_Returned);                                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ia_Aian026_Linkage_Auv_Returned.equals(getZero())))                                                                                                 //Natural: IF AUV-RETURNED = 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  ERROR                                 *");                                                                          //Natural: WRITE '*                  ERROR                                 *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                        *");                                                                          //Natural: WRITE '*                                                        *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an ANNUITY-UNIT-VALUE of '0'         *");                                                                          //Natural: WRITE '* and returned with an ANNUITY-UNIT-VALUE of "0"         *'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Call_Type);                                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Fund_Code);                                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Reval_Methd);                                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Check_Date);                                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Issue_Date);                                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ia_Aian026_Linkage_Auv_Date_Return);                                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ia_Aian026_Linkage_Auv_Returned);                                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 4 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT' 25X 'PAGE :' *PAGE-NUMBER ( 4 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 25X 'A/C    INTERNAL ROLLOVER' / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 38X ' FUND  DEST  IVC-DEST HOLD  PEND        UNIT' / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND' 7X 'UNITS' 8X 'TOTAL    IVC$' 5X ' CODE  CURR' 2X 'CURR' '    CODE  CODE   MODE VALU'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT",new ColumnSpacing(25),"PAGE :",getReports().getPageNumberDbs(4),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(25),"A/C    INTERNAL ROLLOVER",NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(38)," FUND  DEST  IVC-DEST HOLD  PEND        UNIT",NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new ColumnSpacing(7),"UNITS",new ColumnSpacing(8),"TOTAL    IVC$",new 
                        ColumnSpacing(5)," CODE  CURR",new ColumnSpacing(2),"CURR","    CODE  CODE   MODE VALU");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 2 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT' / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 25X 'P&I  INTERNAL ROLLOVER' 30X 'PAGE :' *PAGE-NUMBER ( 2 ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 30X '      -- DEST --  CASH  TERM  HOLD  PEND           ' / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND' 20X 'TOTAL' 5X '      PREV' 2X 'CURR' 2X 'CODE  CODE  CODE  CODE  MODE     '
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT",NEWLINE,"RUN DATE    ",Global.getDATX(), new ReportEditMask 
                        ("MM/DD/YYYY"),new ColumnSpacing(25),"P&I  INTERNAL ROLLOVER",new ColumnSpacing(30),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(30),"      -- DEST --  CASH  TERM  HOLD  PEND           ",NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new ColumnSpacing(20),"TOTAL",new ColumnSpacing(5),"      PREV",new 
                        ColumnSpacing(2),"CURR",new ColumnSpacing(2),"CODE  CODE  CODE  CODE  MODE     ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 3 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT' / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 35X 'IPRO INTERNAL ROLLOVER' 35X 'PAGE :' *PAGE-NUMBER ( 3 ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 30X '      -- DEST --  CASH  TERM  HOLD  PEND           ' / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND' 20X 'TOTAL' 5X '      PREV' 2X 'CURR' 2X 'CODE  CODE  CODE  CODE  MODE     '
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT",NEWLINE,"RUN DATE    ",Global.getDATX(), new ReportEditMask 
                        ("MM/DD/YYYY"),new ColumnSpacing(35),"IPRO INTERNAL ROLLOVER",new ColumnSpacing(35),"PAGE :",getReports().getPageNumberDbs(3),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(30),"      -- DEST --  CASH  TERM  HOLD  PEND           ",NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new ColumnSpacing(20),"TOTAL",new ColumnSpacing(5),"      PREV",new 
                        ColumnSpacing(2),"CURR",new ColumnSpacing(2),"CODE  CODE  CODE  CODE  MODE     ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 1 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT' / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 35X 'TPA   INTERNAL ROLLOVER' 30X 'PAGE :' *PAGE-NUMBER ( 1 ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) //
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT",NEWLINE,"RUN DATE    ",Global.getDATX(), new ReportEditMask 
                        ("MM/DD/YYYY"),new ColumnSpacing(35),"TPA   INTERNAL ROLLOVER",new ColumnSpacing(30),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 5 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT' / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) 80X 'PAGE :' *PAGE-NUMBER ( 5 ) / 28X 'CONTRACTUAL' / 29X 'AMOUNT' 7X 'DIVIDEND' 7X 'UNITS' 8X 'TOTAL' 7X 'TOTAL IVC'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER DETAIL REPORT",NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new 
                        ReportEditMask ("9999/99/01"),new ColumnSpacing(80),"PAGE :",getReports().getPageNumberDbs(5),NEWLINE,new ColumnSpacing(28),"CONTRACTUAL",NEWLINE,new 
                        ColumnSpacing(29),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new ColumnSpacing(7),"UNITS",new ColumnSpacing(8),"TOTAL",new ColumnSpacing(7),
                        "TOTAL IVC");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(25),"IA ADMINISTRATION TOTAL INTERNAL ROLLOVER SETTLEMENTS BY PRODUCT",new  //Natural: WRITE ( 6 ) NOTITLE NOHDR // 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION TOTAL INTERNAL ROLLOVER SETTLEMENTS BY PRODUCT' 10X 'PAGE :' *PAGE-NUMBER ( 6 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) /
                        ColumnSpacing(10),"PAGE :",getReports().getPageNumberDbs(6),NEWLINE,"RUN DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE);
                    //*  ADDED 1/03
                    //*  ADDED 1/03
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Dest_Cde.getValue(1).setValue("IRAT");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 1 ) := 'IRAT'
                    pnd_Dest_Cde.getValue(2).setValue("IRAC");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 2 ) := 'IRAC'
                    pnd_Dest_Cde.getValue(3).setValue("03BT");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 3 ) := '03BT'
                    pnd_Dest_Cde.getValue(4).setValue("03BC");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 4 ) := '03BC'
                    pnd_Dest_Cde.getValue(5).setValue("QPLT");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 5 ) := 'QPLT'
                    pnd_Dest_Cde.getValue(6).setValue("QPLC");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 6 ) := 'QPLC'
                    pnd_Dest_Cde.getValue(7).setValue("57BT");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 7 ) := '57BT'
                    pnd_Dest_Cde.getValue(8).setValue("57BC");                                                                                                            //Natural: ASSIGN #DEST-CDE ( 8 ) := '57BC'
                    getReports().write(7, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 7 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION TOTAL INTERNAL ROLLOVER SETTLEMENTS BY DESTINATION' 10X 'PAGE :' *PAGE-NUMBER ( 7 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) // / 'DESTINATION' 15X 'CONTRACTUAL' 4X 'PERIODIC' 5X 'TIAA/CREF' 7X 'TIAA/CREFF' / '  CODE    ' 3X '  PAYEES  ' 5X 'AMOUNT' 7X 'DIVIDEND' 7X 'UNITS' 12X 'TOTAL' 5X 'TOTAL AMOUNT' 3X 'TOTAL-IVC-AMT'
                        ColumnSpacing(25),"IA ADMINISTRATION TOTAL INTERNAL ROLLOVER SETTLEMENTS BY DESTINATION",new ColumnSpacing(10),"PAGE :",getReports().getPageNumberDbs(7),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new ReportEditMask ("9999/99/01"),NEWLINE,NEWLINE,NEWLINE,"DESTINATION",new 
                        ColumnSpacing(15),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(5),"TIAA/CREF",new ColumnSpacing(7),"TIAA/CREFF",NEWLINE,"  CODE    ",new 
                        ColumnSpacing(3),"  PAYEES  ",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND",new ColumnSpacing(7),"UNITS",new ColumnSpacing(12),"TOTAL",new 
                        ColumnSpacing(5),"TOTAL AMOUNT",new ColumnSpacing(3),"TOTAL-IVC-AMT");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt8 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(8, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 8 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL ROLLOVER LEDGER ENTRY SUMMARY' 15X 'PAGE :' *PAGE-NUMBER ( 8 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) //// 20X '--------- G/L DEBIT ---------      --------- G/L CREDIT --------' /20X 'ACCOUNT NUMBER       AMOUNT        ACCOUNT NUMBER       AMOUNT' /20X '--------------   ------------      --------------   ------------'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL ROLLOVER LEDGER ENTRY SUMMARY",new ColumnSpacing(15),"PAGE :",getReports().getPageNumberDbs(8),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new ReportEditMask ("9999/99/01"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
                        ColumnSpacing(20),"--------- G/L DEBIT ---------      --------- G/L CREDIT --------",NEWLINE,new ColumnSpacing(20),"ACCOUNT NUMBER       AMOUNT        ACCOUNT NUMBER       AMOUNT",NEWLINE,new 
                        ColumnSpacing(20),"--------------   ------------      --------------   ------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt9 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(9, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new       //Natural: WRITE ( 9 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA IPRO WARNING REPORT FOR PAYMENTS DUE ' 30X 'PAGE :' *PAGE-NUMBER ( 9 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) 30X '    CK-PMT-DATE' #CNTRL-YYYYMM ( EM = 9999/99/01 ) /2X 'CONTRACT' 3X 'PAYEE' 2X 'PEND' 2X 'HOLD' 6X 'NAME' 5X 'BIRTH-DATE' 2X 'ISSUE-DATE' 2X 'FIN-PMT-DATE' 8X 'WARNING  MESSAGES'
                        ColumnSpacing(25),"IA IPRO WARNING REPORT FOR PAYMENTS DUE ",new ColumnSpacing(30),"PAGE :",getReports().getPageNumberDbs(9),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new ReportEditMask ("9999/99/01"),new 
                        ColumnSpacing(30),"    CK-PMT-DATE",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new ReportEditMask ("9999/99/01"),NEWLINE,new ColumnSpacing(2),"CONTRACT",new 
                        ColumnSpacing(3),"PAYEE",new ColumnSpacing(2),"PEND",new ColumnSpacing(2),"HOLD",new ColumnSpacing(6),"NAME",new ColumnSpacing(5),"BIRTH-DATE",new 
                        ColumnSpacing(2),"ISSUE-DATE",new ColumnSpacing(2),"FIN-PMT-DATE",new ColumnSpacing(8),"WARNING  MESSAGES");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt10 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(10, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new      //Natural: WRITE ( 10 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL  REPORT' 15X 'PAGE :' *PAGE-NUMBER ( 10 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 20X 'TPA BYPASSED PENDED CONTRACTS ' / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 30X / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL  REPORT",new ColumnSpacing(15),"PAGE :",getReports().getPageNumberDbs(10),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20),"TPA BYPASSED PENDED CONTRACTS ",NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(30),NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt11 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(11, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new      //Natural: WRITE ( 11 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL  REPORT' 15X 'PAGE :' *PAGE-NUMBER ( 11 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 20X 'IPRO BYPASSED PENDED CONTRACTS ' / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 30X / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL  REPORT",new ColumnSpacing(15),"PAGE :",getReports().getPageNumberDbs(11),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20),"IPRO BYPASSED PENDED CONTRACTS ",NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(30),NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt12 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(12, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM",Global.getPROGRAM(),new      //Natural: WRITE ( 12 ) NOTITLE NOHDR ////// 'PROGRAM' *PROGRAM 25X 'IA ADMINISTRATION INTERNAL  REPORT' 15X 'PAGE :' *PAGE-NUMBER ( 12 ) / 'RUN DATE    ' *DATX ( EM = MM/DD/YYYY ) 20X 'P&I  BYPASSED PENDED CONTRACTS ' / 'CHECK DATE: ' #CNTRL-YYYYMM ( EM = 9999/99/01 ) / '   SOCIAL' 18X 'CONTRACTUAL' 4X 'PERIODIC' 30X / '  SECURITY' 3X 'IA-CONTRACT' 5X 'AMOUNT' 7X 'DIVIDEND'
                        ColumnSpacing(25),"IA ADMINISTRATION INTERNAL  REPORT",new ColumnSpacing(15),"PAGE :",getReports().getPageNumberDbs(12),NEWLINE,"RUN DATE    ",Global.getDATX(), 
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20),"P&I  BYPASSED PENDED CONTRACTS ",NEWLINE,"CHECK DATE: ",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, 
                        new ReportEditMask ("9999/99/01"),NEWLINE,"   SOCIAL",new ColumnSpacing(18),"CONTRACTUAL",new ColumnSpacing(4),"PERIODIC",new ColumnSpacing(30),NEWLINE,"  SECURITY",new 
                        ColumnSpacing(3),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new ColumnSpacing(7),"DIVIDEND");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,pnd_Input_Record_Pnd_Cntrct_Payee_Cde,     //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE / / '=' #SAVE-ISSUE-DTE / '=' #AUV-RETURNED / '=' #RETURN-CODE / '=' #FUND-CDE-1B / '=' #CNTRCT-CURR-DIST-CDE
            NEWLINE,NEWLINE,"=",pnd_Save_Issue_Dte,NEWLINE,"=",pnd_Auv_Returned,NEWLINE,"=",pnd_Return_Code,NEWLINE,"=",pnd_Fund_Cde_2_Pnd_Fund_Cde_1b,NEWLINE,
            "=",pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=133");
        Global.format(1, "PS=55 LS=133");
        Global.format(2, "PS=55 LS=133");
        Global.format(3, "PS=55 LS=133");
        Global.format(4, "PS=55 LS=133");
        Global.format(5, "PS=55 LS=133");
        Global.format(6, "PS=55 LS=133");
        Global.format(7, "PS=55 LS=133");
        Global.format(8, "PS=55 LS=133");
        Global.format(8, "PS=55 LS=133");
        Global.format(9, "PS=55 LS=133");
        Global.format(10, "PS=55 LS=133");
        Global.format(11, "PS=55 LS=133");
        Global.format(12, "PS=55 LS=133");

        getReports().setDisplayColumns(1, "SOCIAL/SECURITY",
        		pnd_Save_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"/IA-CONTRACT",
        		pnd_Save_Part_Ppcn_Nbr,"COMTRACTUAL/AMOUNT",
        		pnd_Per_Amt_A_Pnd_Per_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"PERIODIC/DIVIDEND",
        		pnd_Div_Amt_A_Pnd_Div_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"/TOTAL-AMT",
        		pnd_Total, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/PER-IVC$",
        		pnd_Save_Per_Ivc_Amt,"DEST/PREV",
        		pnd_Save_Cntrct_Prev_Dist_Cde,"DEST/CURR",
        		pnd_Save_Cntrct_Curr_Dist_Cde,"CASH/CODE",
        		pnd_Save_Cntrct_Cash_Cde,"TERM/CODE",
        		pnd_Save_Cntrct_Xfr_Term_Cde,"/HOLD",
        		pnd_Save_Cntrct_Hold_Cde,"/PEND",
        		pnd_Save_Cntrct_Pend_Cde,"/MODE",
        		pnd_Save_Cntrct_Mode_Ind);
    }
}
