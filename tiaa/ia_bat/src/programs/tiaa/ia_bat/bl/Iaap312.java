/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:22 PM
**        * FROM NATURAL PROGRAM : Iaap312
************************************************************
**        * FILE NAME            : Iaap312.java
**        * CLASS NAME           : Iaap312
**        * INSTANCE NAME        : Iaap312
************************************************************
******************************************************************
* YR2000 COMPLIANT FIX APPLIED-->N.HAWTHORN 04/24/98             *
******************************************************************
**Y2CHNH CP IMNMEN
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP312    READS IAA TRANS RECORD                 *
*      DATE     -  02/96      CREATES SELECTION RECORD LAYOUT        *
*   AUTHOR      -  ALEX RATNER / ARI GROSSMAN
*                                                                    *
*   HISTORY     -  ADDED NAZ TRANSACTIONS
*
*  04/2017 OS RE-STOWED FOR PIN EXPANSION.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap312 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct_View;
    private DbsField iaa_Cntrct_View_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField pnd_Year_Table;
    private DbsField pnd_A;
    private DbsField pnd_After_Image;
    private DbsField pnd_B;
    private DbsField pnd_Ytd_Tot;
    private DbsField pnd_Per_Ivc_Hold;
    private DbsField pnd_Num_Of_Pay;
    private DbsField pnd_Number_Of_Payments;
    private DbsField pnd_Mde_Sub;

    private DbsGroup pnd_Trailer_Record;
    private DbsField pnd_Trailer_Record_Pnd_T_Timestamp;
    private DbsField pnd_Trailer_Record_Pnd_T_Interface_Dte;
    private DbsField pnd_Trailer_Record_Pnd_T_Title;
    private DbsField pnd_Trailer_Record_Pnd_T_Rec_Count;
    private DbsField pnd_Trailer_Record_Pnd_T_Filler1;
    private DbsField pnd_Trailer_Record_Pnd_T_Filler2;
    private DbsField pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_1;
    private DbsField pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_2_3;
    private DbsField pnd_Trailer_Record_Pnd_T_Filler3;

    private DbsGroup pnd_Iaa_Parm_Card;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_1;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_2;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_3;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc_A;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy_A;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm_A;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd_A;

    private DbsGroup pnd_Iaa_Parm_Card__R_Field_4;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm;
    private DbsField pnd_Iaa_Parm_Card_Pnd_Parm_Year_End;
    private DbsField pnd_Period_End_Dte_A;

    private DbsGroup pnd_Period_End_Dte_A__R_Field_5;
    private DbsField pnd_Period_End_Dte_A_Pnd_Period_End_Dte;

    private DbsGroup pnd_Period_End_Dte_A__R_Field_6;
    private DbsField pnd_Period_End_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Period_End_Dte_A__R_Field_7;
    private DbsField pnd_Period_End_Dte_A_Pnd_Cc;
    private DbsField pnd_Period_End_Dte_A_Pnd_Yy;
    private DbsField pnd_Period_End_Dte_A_Pnd_Mm;
    private DbsField pnd_Period_End_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Period_End_Dte_A__R_Field_8;
    private DbsField pnd_Period_End_Dte_A_Pnd_Period_End_Dte_Yyyymm;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Yy_Minus_One;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;

    private DbsGroup pnd_Cps_Npd_Record;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde;

    private DbsGroup pnd_Cps_Npd_Record__R_Field_9;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde_A;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Source;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Data_Type;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Case_Type;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Currence_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Geographic_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rollover_Ind;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Rsdncy;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Dob;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Gross_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Fed_Whhld_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Whhld_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Interest_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Co_Nmbr;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Recs;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Sys_Rsdncy_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line_Cnt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line;

    private DbsGroup pnd_Cps_Npd_Record__R_Field_10;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne1;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne2;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne3;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne4;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne5;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne6;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne7;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne8;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Elct_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Tax_Id;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_F;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_S;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_F;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_S;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Ctzn_Cde;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler2;
    private DbsField pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde;
    private DbsField save_Filler2;

    private DbsGroup save_Filler2__R_Field_11;
    private DbsField save_Filler2_Pnd_F1;
    private DbsField save_Filler2_Pnd_T_F2;
    private DbsField save_Filler2_Pnd_F3;
    private DbsField pnd_Skip_Sw_1;
    private DbsField pnd_Skip_Sw_2;
    private DbsField pnd_Skip_Sw_3;
    private DbsField pnd_Skip_Sw_4;
    private DbsField pnd_Next_Invrse_Date;
    private DbsField pnd_Invrse_Date;
    private DbsField pnd_Dt_Trn_Tot;
    private DbsField pnd_Trn_Dte;

    private DbsGroup pnd_Trn_Dte__R_Field_12;
    private DbsField pnd_Trn_Dte_Pnd_Trn_Dte_Cc;
    private DbsField pnd_Trn_Dte_Pnd_Trn_Dte_Yy;
    private DbsField pnd_Trn_Dte_Pnd_Trn_Dte_Mm;

    private DbsGroup pnd_Trn_Dte__R_Field_13;
    private DbsField pnd_Trn_Dte_Pnd_Trn_Dte_Mm_N;
    private DbsField pnd_Trn_Dte_Pnd_Trn_Dte_Dd;
    private DbsField pnd_Time;
    private DbsField pnd_Prt_Dob_F;
    private DbsField pnd_Prt_Dob_S;
    private DbsField pnd_Dob_F;

    private DbsGroup pnd_Dob_F__R_Field_14;
    private DbsField pnd_Dob_F_Pnd_Dob_F_A;

    private DbsGroup pnd_Dob_F__R_Field_15;
    private DbsField pnd_Dob_F_Pnd_Dob_F_Cc;
    private DbsField pnd_Dob_F_Pnd_Dob_F_Yy;
    private DbsField pnd_Dob_F_Pnd_Dob_F_Mm;
    private DbsField pnd_Dob_F_Pnd_Dob_F_Dd;
    private DbsField pnd_Dob_S;

    private DbsGroup pnd_Dob_S__R_Field_16;
    private DbsField pnd_Dob_S_Pnd_Dob_S_A;

    private DbsGroup pnd_Dob_S__R_Field_17;
    private DbsField pnd_Dob_S_Pnd_Dob_S_Cc;
    private DbsField pnd_Dob_S_Pnd_Dob_S_Yy;
    private DbsField pnd_Dob_S_Pnd_Dob_S_Mm;
    private DbsField pnd_Dob_S_Pnd_Dob_S_Dd;
    private DbsField pnd_Prt_Dod_F;
    private DbsField pnd_Prt_Dod_S;
    private DbsField pnd_Dod_F;

    private DbsGroup pnd_Dod_F__R_Field_18;
    private DbsField pnd_Dod_F_Pnd_Dod_F_A;

    private DbsGroup pnd_Dod_F__R_Field_19;
    private DbsField pnd_Dod_F_Pnd_Dod_F_Cc;
    private DbsField pnd_Dod_F_Pnd_Dod_F_Yy;
    private DbsField pnd_Dod_F_Pnd_Dod_F_Mm;
    private DbsField pnd_Dod_S;

    private DbsGroup pnd_Dod_S__R_Field_20;
    private DbsField pnd_Dod_S_Pnd_Dod_S_A;

    private DbsGroup pnd_Dod_S__R_Field_21;
    private DbsField pnd_Dod_S_Pnd_Dod_S_Cc;
    private DbsField pnd_Dod_S_Pnd_Dod_S_Yy;
    private DbsField pnd_Dod_S_Pnd_Dod_S_Mm;
    private DbsField pnd_Prt_First_Pay_Dte;
    private DbsField pnd_First_Pay_Dte;

    private DbsGroup pnd_First_Pay_Dte__R_Field_22;
    private DbsField pnd_First_Pay_Dte_Pnd_First_Pay_Dte_A;

    private DbsGroup pnd_First_Pay_Dte__R_Field_23;
    private DbsField pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Cc;
    private DbsField pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Yy;
    private DbsField pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Mm;
    private DbsField pnd_Current_Check_Date;

    private DbsGroup pnd_Current_Check_Date__R_Field_24;
    private DbsField pnd_Current_Check_Date_Pnd_Current_Check_Date_A;
    private DbsField pnd_Current_First_Trans_Date;
    private DbsField pnd_Next_Check_Date;

    private DbsGroup pnd_Next_Check_Date__R_Field_25;
    private DbsField pnd_Next_Check_Date_Pnd_Next_Check_Date_A;
    private DbsField pnd_Next_First_Trans_Date;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_26;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_27;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Cntrct_Bfre_Key;

    private DbsGroup pnd_Cntrct_Bfre_Key__R_Field_28;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Cpr_Aftr_Key;

    private DbsGroup pnd_Cpr_Aftr_Key__R_Field_29;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Cntrct_Aftr_Key;

    private DbsGroup pnd_Cntrct_Aftr_Key__R_Field_30;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Save_Cpr_Bfre_Ssn;
    private DbsField pnd_Save_Cpr_Aftr_Ssn;
    private DbsField pnd_Save_Cpr_Bfre_Ctznshp;
    private DbsField pnd_Save_Cpr_Aftr_Ctznshp;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Due_Dte;

    private DbsGroup pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Cc;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Mm;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Pd_Dte;

    private DbsGroup pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Cc;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Mm;

    private DbsGroup pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_33;
    private DbsField pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Ccyy;
    private DbsField pnd_Save_Cntrct_Bfre_F_Dob;
    private DbsField pnd_Save_Cntrct_Aftr_F_Dob;
    private DbsField pnd_Save_Cntrct_Bfre_S_Dob;
    private DbsField pnd_Save_Cntrct_Aftr_S_Dob;
    private DbsField pnd_Save_Cntrct_Disp_Dob;
    private DbsField pnd_Save_Cntrct_Bfre_F_Dod;
    private DbsField pnd_Save_Cntrct_Aftr_F_Dod;
    private DbsField pnd_Save_Cntrct_Bfre_S_Dod;
    private DbsField pnd_Save_Cntrct_Aftr_S_Dod;
    private DbsField pnd_Save_Cntrct_Disp_Dod;
    private DbsField pnd_Save_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField pnd_Save_Cntrct_First_Pymnt_Due_Dte;
    private DbsField pnd_Save_Cntrct_Comp_Cd;
    private DbsField pnd_Save_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Save_Prtcpnt_Bfre_Rsdncy_Cde;
    private DbsField pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde;

    private DbsGroup pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde__R_Field_34;
    private DbsField pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_1;
    private DbsField pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3;
    private DbsField pnd_Save_Cntrct_Bfre_Per_Ivc_Amt;
    private DbsField pnd_Save_Cntrct_Aftr_Per_Ivc_Amt;
    private DbsField pnd_Save_Cntrct_Disp_Per_Ivc_Amt;
    private DbsField pnd_Save_Cntrct_Aftr_Ivc_Amt;
    private DbsField pnd_Save_Cntrct_Aftr_Rtb_Amt;
    private DbsField pnd_Save_Cntrct_Disp_Ivc_Amt;
    private DbsField pnd_Ivc_Rtb_Amt;
    private DbsField pnd_Ivc_Amt;
    private DbsField pnd_Dsp_Amt;
    private DbsField pnd_Save_Trans_Dte;
    private DbsField pnd_Save_Trans_Check_Dte;

    private DbsGroup pnd_Save_Trans_Check_Dte__R_Field_35;
    private DbsField pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr1;
    private DbsField pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm;

    private DbsGroup pnd_Save_Trans_Check_Dte__R_Field_36;
    private DbsField pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm_N;
    private DbsField pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr2;
    private DbsField pnd_Trans_Effctve_Dte;

    private DbsGroup pnd_Trans_Effctve_Dte__R_Field_37;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd;

    private DbsGroup pnd_Trans_Effctve_Dte__R_Field_38;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Effctve_Dte_A;

    private DbsGroup pnd_Trans_Effctve_Dte__R_Field_39;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc_A;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy_A;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm_A;
    private DbsField pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd_A;
    private DbsField pnd_Prt_Trans_Eff;
    private DbsField pnd_Save_Trans_Check_Mm;

    private DbsGroup pnd_Save_Trans_Check_Mm__R_Field_40;
    private DbsField pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N;
    private DbsField pnd_Ws_Check_Dte;

    private DbsGroup pnd_Ws_Check_Dte__R_Field_41;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Check_Dte_N;

    private DbsGroup pnd_Ws_Check_Dte__R_Field_42;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Cc;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Yy;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Dd;

    private DbsGroup pnd_Ws_Check_Dte__R_Field_43;
    private DbsField pnd_Ws_Check_Dte_Pnd_Ws_Ccyy;
    private DbsField pnd_Save_Trans_Ppcn_Nbr_C;
    private DbsField pnd_Save_Trans_Payee_Cde_C;
    private DbsField pnd_Save_Trans_Cde_C;
    private DbsField pnd_Save_Trans_Ppcn_Nbr;

    private DbsGroup pnd_Save_Trans_Ppcn_Nbr__R_Field_44;
    private DbsField pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1;
    private DbsField pnd_Save_Trans_Ppcn_Nbr_Pnd_Trans_Flr2;
    private DbsField pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Save_Trans_Cde;

    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_Trans_Ind;
    private DbsField pnd_Logical_Variables_Pnd_Bypass;

    private DbsGroup pnd_Packed_Variables;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ctr_Eq_Dt;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr3;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr5;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Processed_Ctr6;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Bypassed_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Cps_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ivc;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ric;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ivm;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Res;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Ssn;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Dob_F;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Dob_S;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Dod_F;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Dod_S;
    private DbsField pnd_Packed_Variables_Pnd_Recs_Cit;
    private DbsField pnd_Packed_Variables_Pnd_Final_Tot;
    private DbsField pnd_D;
    private DbsField pnd_R;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Chk_Dte;
    private DbsField pnd_Cntrct_Mode_Tot;

    private DbsGroup pnd_Cntrct_Mode_Tot__R_Field_45;
    private DbsField pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1;
    private DbsField pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_2_3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_View", "IAA-CNTRCT-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_View_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_View.getRecord().newFieldInGroup("iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        registerRecord(vw_iaa_Cntrct_View);

        pnd_Year_Table = localVariables.newFieldArrayInRecord("pnd_Year_Table", "#YEAR-TABLE", FieldType.NUMERIC, 2, new DbsArrayController(1, 22, 1, 
            12));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_After_Image = localVariables.newFieldInRecord("pnd_After_Image", "#AFTER-IMAGE", FieldType.STRING, 1);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_Ytd_Tot = localVariables.newFieldInRecord("pnd_Ytd_Tot", "#YTD-TOT", FieldType.NUMERIC, 9, 2);
        pnd_Per_Ivc_Hold = localVariables.newFieldInRecord("pnd_Per_Ivc_Hold", "#PER-IVC-HOLD", FieldType.NUMERIC, 9, 2);
        pnd_Num_Of_Pay = localVariables.newFieldInRecord("pnd_Num_Of_Pay", "#NUM-OF-PAY", FieldType.NUMERIC, 2);
        pnd_Number_Of_Payments = localVariables.newFieldInRecord("pnd_Number_Of_Payments", "#NUMBER-OF-PAYMENTS", FieldType.NUMERIC, 2);
        pnd_Mde_Sub = localVariables.newFieldInRecord("pnd_Mde_Sub", "#MDE-SUB", FieldType.NUMERIC, 2);

        pnd_Trailer_Record = localVariables.newGroupInRecord("pnd_Trailer_Record", "#TRAILER-RECORD");
        pnd_Trailer_Record_Pnd_T_Timestamp = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Timestamp", "#T-TIMESTAMP", FieldType.BINARY, 
            8);
        pnd_Trailer_Record_Pnd_T_Interface_Dte = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Interface_Dte", "#T-INTERFACE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Trailer_Record_Pnd_T_Title = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Title", "#T-TITLE", FieldType.STRING, 25);
        pnd_Trailer_Record_Pnd_T_Rec_Count = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Rec_Count", "#T-REC-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Trailer_Record_Pnd_T_Filler1 = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Filler1", "#T-FILLER1", FieldType.STRING, 230);
        pnd_Trailer_Record_Pnd_T_Filler2 = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Filler2", "#T-FILLER2", FieldType.STRING, 100);
        pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_1 = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_1", "#T-CPS-NPD-MAINT-CDE-1", 
            FieldType.STRING, 1);
        pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_2_3 = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_2_3", "#T-CPS-NPD-MAINT-CDE-2-3", 
            FieldType.STRING, 2);
        pnd_Trailer_Record_Pnd_T_Filler3 = pnd_Trailer_Record.newFieldInGroup("pnd_Trailer_Record_Pnd_T_Filler3", "#T-FILLER3", FieldType.STRING, 81);

        pnd_Iaa_Parm_Card = localVariables.newGroupInRecord("pnd_Iaa_Parm_Card", "#IAA-PARM-CARD");
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte = pnd_Iaa_Parm_Card.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.STRING, 
            8);

        pnd_Iaa_Parm_Card__R_Field_1 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_1", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N = pnd_Iaa_Parm_Card__R_Field_1.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N", "#PARM-CHECK-DTE-N", 
            FieldType.NUMERIC, 8);

        pnd_Iaa_Parm_Card__R_Field_2 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_2", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc", "#PARM-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy", "#PARM-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm", "#PARM-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd = pnd_Iaa_Parm_Card__R_Field_2.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd", "#PARM-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Iaa_Parm_Card__R_Field_3 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_3", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc_A = pnd_Iaa_Parm_Card__R_Field_3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc_A", "#PARM-CHECK-DTE-CC-A", 
            FieldType.STRING, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy_A = pnd_Iaa_Parm_Card__R_Field_3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy_A", "#PARM-CHECK-DTE-YY-A", 
            FieldType.STRING, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm_A = pnd_Iaa_Parm_Card__R_Field_3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm_A", "#PARM-CHECK-DTE-MM-A", 
            FieldType.STRING, 2);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd_A = pnd_Iaa_Parm_Card__R_Field_3.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd_A", "#PARM-CHECK-DTE-DD-A", 
            FieldType.STRING, 2);

        pnd_Iaa_Parm_Card__R_Field_4 = pnd_Iaa_Parm_Card.newGroupInGroup("pnd_Iaa_Parm_Card__R_Field_4", "REDEFINE", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);
        pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm = pnd_Iaa_Parm_Card__R_Field_4.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm", "#PARM-CHECK-DTE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Iaa_Parm_Card_Pnd_Parm_Year_End = pnd_Iaa_Parm_Card.newFieldInGroup("pnd_Iaa_Parm_Card_Pnd_Parm_Year_End", "#PARM-YEAR-END", FieldType.STRING, 
            1);
        pnd_Period_End_Dte_A = localVariables.newFieldInRecord("pnd_Period_End_Dte_A", "#PERIOD-END-DTE-A", FieldType.STRING, 8);

        pnd_Period_End_Dte_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Period_End_Dte_A__R_Field_5", "REDEFINE", pnd_Period_End_Dte_A);
        pnd_Period_End_Dte_A_Pnd_Period_End_Dte = pnd_Period_End_Dte_A__R_Field_5.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Period_End_Dte", "#PERIOD-END-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Period_End_Dte_A__R_Field_6 = pnd_Period_End_Dte_A__R_Field_5.newGroupInGroup("pnd_Period_End_Dte_A__R_Field_6", "REDEFINE", pnd_Period_End_Dte_A_Pnd_Period_End_Dte);
        pnd_Period_End_Dte_A_Pnd_Yyyy = pnd_Period_End_Dte_A__R_Field_6.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);

        pnd_Period_End_Dte_A__R_Field_7 = pnd_Period_End_Dte_A__R_Field_6.newGroupInGroup("pnd_Period_End_Dte_A__R_Field_7", "REDEFINE", pnd_Period_End_Dte_A_Pnd_Yyyy);
        pnd_Period_End_Dte_A_Pnd_Cc = pnd_Period_End_Dte_A__R_Field_7.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Period_End_Dte_A_Pnd_Yy = pnd_Period_End_Dte_A__R_Field_7.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Period_End_Dte_A_Pnd_Mm = pnd_Period_End_Dte_A__R_Field_6.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Period_End_Dte_A_Pnd_Dd = pnd_Period_End_Dte_A__R_Field_6.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Period_End_Dte_A__R_Field_8 = pnd_Period_End_Dte_A__R_Field_5.newGroupInGroup("pnd_Period_End_Dte_A__R_Field_8", "REDEFINE", pnd_Period_End_Dte_A_Pnd_Period_End_Dte);
        pnd_Period_End_Dte_A_Pnd_Period_End_Dte_Yyyymm = pnd_Period_End_Dte_A__R_Field_8.newFieldInGroup("pnd_Period_End_Dte_A_Pnd_Period_End_Dte_Yyyymm", 
            "#PERIOD-END-DTE-YYYYMM", FieldType.NUMERIC, 6);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Yy_Minus_One = localVariables.newFieldInRecord("pnd_Yy_Minus_One", "#YY-MINUS-ONE", FieldType.NUMERIC, 2);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cntrct_Trans);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cpr_Trans);

        pnd_Cps_Npd_Record = localVariables.newGroupInRecord("pnd_Cps_Npd_Record", "#CPS-NPD-RECORD");
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde", "#CPS-NPD-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr", "#CPS-NPD-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde", "#CPS-NPD-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        pnd_Cps_Npd_Record__R_Field_9 = pnd_Cps_Npd_Record.newGroupInGroup("pnd_Cps_Npd_Record__R_Field_9", "REDEFINE", pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde_A = pnd_Cps_Npd_Record__R_Field_9.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde_A", "#CPS-NPD-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id_Cde", "#CPS-NPD-TAX-ID-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id", "#CPS-NPD-TAX-ID", FieldType.NUMERIC, 
            9);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Source = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Source", "#CPS-NPD-SOURCE", FieldType.STRING, 
            3);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month", "#CPS-NPD-PYMNT-MONTH", 
            FieldType.STRING, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte", "#CPS-NPD-TRANS-DTE", 
            FieldType.TIME);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Data_Type = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Data_Type", "#CPS-NPD-DATA-TYPE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Case_Type = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Case_Type", "#CPS-NPD-CASE-TYPE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Currence_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Currence_Cde", "#CPS-NPD-CURRENCE-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Geographic_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Geographic_Cde", "#CPS-NPD-GEOGRAPHIC-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rollover_Ind = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rollover_Ind", "#CPS-NPD-ROLLOVER-IND", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Rsdncy = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Rsdncy", "#CPS-NPD-STATE-RSDNCY", 
            FieldType.STRING, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship", "#CPS-NPD-CITIZENSHIP", 
            FieldType.NUMERIC, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Dob = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Dob", "#CPS-NPD-DOB", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Gross_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Gross_Amt", "#CPS-NPD-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Fed_Whhld_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Fed_Whhld_Amt", "#CPS-NPD-FED-WHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Whhld_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_State_Whhld_Amt", "#CPS-NPD-STATE-WHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Interest_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Interest_Amt", "#CPS-NPD-INTEREST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Amt", "#CPS-NPD-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ivc_Cde", "#CPS-NPD-IVC-CDE", FieldType.STRING, 
            1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Co_Nmbr = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Co_Nmbr", "#CPS-NPD-CO-NMBR", FieldType.NUMERIC, 
            1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Recs = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Recs", "#CPS-NPD-RECS", FieldType.STRING, 
            3);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Sys_Rsdncy_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Sys_Rsdncy_Cde", "#CPS-NPD-SYS-RSDNCY-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler", "#CPS-NPD-FILLER", FieldType.STRING, 
            11);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line_Cnt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line_Cnt", "#CPS-NPD-NA-LINE-CNT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line", "#CPS-NPD-NA-LINE", FieldType.STRING, 
            245);

        pnd_Cps_Npd_Record__R_Field_10 = pnd_Cps_Npd_Record.newGroupInGroup("pnd_Cps_Npd_Record__R_Field_10", "REDEFINE", pnd_Cps_Npd_Record_Pnd_Cps_Npd_Na_Line);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne1 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne1", "#CPS-NPD-LNE1", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne2 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne2", "#CPS-NPD-LNE2", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne3 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne3", "#CPS-NPD-LNE3", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne4 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne4", "#CPS-NPD-LNE4", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne5 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne5", "#CPS-NPD-LNE5", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne6 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne6", "#CPS-NPD-LNE6", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne7 = pnd_Cps_Npd_Record__R_Field_10.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne7", "#CPS-NPD-LNE7", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne8 = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Lne8", "#CPS-NPD-LNE8", FieldType.STRING, 
            35);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Elct_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Elct_Cde", "#CPS-NPD-TAX-ELCT-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde", "#CPS-NPD-MAINT-CDE", 
            FieldType.STRING, 3);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde", "#CPS-NPD-PER-IVC-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt", "#CPS-NPD-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Cde", "#CPS-NPD-RTB-IVC-CDE", 
            FieldType.STRING, 1);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt", "#CPS-NPD-RTB-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde", "#CPS-NPD-NEW-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Tax_Id = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Tax_Id", "#CPS-NPD-NEW-TAX-ID", 
            FieldType.NUMERIC, 9);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_F = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_F", "#CPS-NPD-NEW-DOB-F", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_S = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_S", "#CPS-NPD-NEW-DOB-S", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_F = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_F", "#CPS-NPD-NEW-DOD-F", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_S = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_S", "#CPS-NPD-NEW-DOD-S", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Ctzn_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Ctzn_Cde", "#CPS-NPD-NEW-CTZN-CDE", 
            FieldType.STRING, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt", "#CPS-NPD-TOT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt", "#CPS-NPD-YTD-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte", "#CPS-NPD-FIRST-PAY-PD-DTE", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler2 = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Filler2", "#CPS-NPD-FILLER2", FieldType.STRING, 
            21);
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde = pnd_Cps_Npd_Record.newFieldInGroup("pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde", "#CPS-NPD-TRANS-CDE", 
            FieldType.NUMERIC, 3);
        save_Filler2 = localVariables.newFieldInRecord("save_Filler2", "SAVE-FILLER2", FieldType.STRING, 100);

        save_Filler2__R_Field_11 = localVariables.newGroupInRecord("save_Filler2__R_Field_11", "REDEFINE", save_Filler2);
        save_Filler2_Pnd_F1 = save_Filler2__R_Field_11.newFieldInGroup("save_Filler2_Pnd_F1", "#F1", FieldType.STRING, 92);
        save_Filler2_Pnd_T_F2 = save_Filler2__R_Field_11.newFieldInGroup("save_Filler2_Pnd_T_F2", "#T-F2", FieldType.STRING, 7);
        save_Filler2_Pnd_F3 = save_Filler2__R_Field_11.newFieldInGroup("save_Filler2_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Skip_Sw_1 = localVariables.newFieldInRecord("pnd_Skip_Sw_1", "#SKIP-SW-1", FieldType.STRING, 1);
        pnd_Skip_Sw_2 = localVariables.newFieldInRecord("pnd_Skip_Sw_2", "#SKIP-SW-2", FieldType.STRING, 1);
        pnd_Skip_Sw_3 = localVariables.newFieldInRecord("pnd_Skip_Sw_3", "#SKIP-SW-3", FieldType.STRING, 1);
        pnd_Skip_Sw_4 = localVariables.newFieldInRecord("pnd_Skip_Sw_4", "#SKIP-SW-4", FieldType.STRING, 1);
        pnd_Next_Invrse_Date = localVariables.newFieldInRecord("pnd_Next_Invrse_Date", "#NEXT-INVRSE-DATE", FieldType.NUMERIC, 12);
        pnd_Invrse_Date = localVariables.newFieldInRecord("pnd_Invrse_Date", "#INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_Dt_Trn_Tot = localVariables.newFieldInRecord("pnd_Dt_Trn_Tot", "#DT-TRN-TOT", FieldType.STRING, 8);
        pnd_Trn_Dte = localVariables.newFieldInRecord("pnd_Trn_Dte", "#TRN-DTE", FieldType.STRING, 8);

        pnd_Trn_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_Trn_Dte__R_Field_12", "REDEFINE", pnd_Trn_Dte);
        pnd_Trn_Dte_Pnd_Trn_Dte_Cc = pnd_Trn_Dte__R_Field_12.newFieldInGroup("pnd_Trn_Dte_Pnd_Trn_Dte_Cc", "#TRN-DTE-CC", FieldType.STRING, 2);
        pnd_Trn_Dte_Pnd_Trn_Dte_Yy = pnd_Trn_Dte__R_Field_12.newFieldInGroup("pnd_Trn_Dte_Pnd_Trn_Dte_Yy", "#TRN-DTE-YY", FieldType.STRING, 2);
        pnd_Trn_Dte_Pnd_Trn_Dte_Mm = pnd_Trn_Dte__R_Field_12.newFieldInGroup("pnd_Trn_Dte_Pnd_Trn_Dte_Mm", "#TRN-DTE-MM", FieldType.STRING, 2);

        pnd_Trn_Dte__R_Field_13 = pnd_Trn_Dte__R_Field_12.newGroupInGroup("pnd_Trn_Dte__R_Field_13", "REDEFINE", pnd_Trn_Dte_Pnd_Trn_Dte_Mm);
        pnd_Trn_Dte_Pnd_Trn_Dte_Mm_N = pnd_Trn_Dte__R_Field_13.newFieldInGroup("pnd_Trn_Dte_Pnd_Trn_Dte_Mm_N", "#TRN-DTE-MM-N", FieldType.NUMERIC, 2);
        pnd_Trn_Dte_Pnd_Trn_Dte_Dd = pnd_Trn_Dte__R_Field_12.newFieldInGroup("pnd_Trn_Dte_Pnd_Trn_Dte_Dd", "#TRN-DTE-DD", FieldType.STRING, 2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Prt_Dob_F = localVariables.newFieldInRecord("pnd_Prt_Dob_F", "#PRT-DOB-F", FieldType.STRING, 8);
        pnd_Prt_Dob_S = localVariables.newFieldInRecord("pnd_Prt_Dob_S", "#PRT-DOB-S", FieldType.STRING, 8);
        pnd_Dob_F = localVariables.newFieldInRecord("pnd_Dob_F", "#DOB-F", FieldType.NUMERIC, 8);

        pnd_Dob_F__R_Field_14 = localVariables.newGroupInRecord("pnd_Dob_F__R_Field_14", "REDEFINE", pnd_Dob_F);
        pnd_Dob_F_Pnd_Dob_F_A = pnd_Dob_F__R_Field_14.newFieldInGroup("pnd_Dob_F_Pnd_Dob_F_A", "#DOB-F-A", FieldType.STRING, 8);

        pnd_Dob_F__R_Field_15 = pnd_Dob_F__R_Field_14.newGroupInGroup("pnd_Dob_F__R_Field_15", "REDEFINE", pnd_Dob_F_Pnd_Dob_F_A);
        pnd_Dob_F_Pnd_Dob_F_Cc = pnd_Dob_F__R_Field_15.newFieldInGroup("pnd_Dob_F_Pnd_Dob_F_Cc", "#DOB-F-CC", FieldType.STRING, 2);
        pnd_Dob_F_Pnd_Dob_F_Yy = pnd_Dob_F__R_Field_15.newFieldInGroup("pnd_Dob_F_Pnd_Dob_F_Yy", "#DOB-F-YY", FieldType.STRING, 2);
        pnd_Dob_F_Pnd_Dob_F_Mm = pnd_Dob_F__R_Field_15.newFieldInGroup("pnd_Dob_F_Pnd_Dob_F_Mm", "#DOB-F-MM", FieldType.STRING, 2);
        pnd_Dob_F_Pnd_Dob_F_Dd = pnd_Dob_F__R_Field_15.newFieldInGroup("pnd_Dob_F_Pnd_Dob_F_Dd", "#DOB-F-DD", FieldType.STRING, 2);
        pnd_Dob_S = localVariables.newFieldInRecord("pnd_Dob_S", "#DOB-S", FieldType.NUMERIC, 8);

        pnd_Dob_S__R_Field_16 = localVariables.newGroupInRecord("pnd_Dob_S__R_Field_16", "REDEFINE", pnd_Dob_S);
        pnd_Dob_S_Pnd_Dob_S_A = pnd_Dob_S__R_Field_16.newFieldInGroup("pnd_Dob_S_Pnd_Dob_S_A", "#DOB-S-A", FieldType.STRING, 8);

        pnd_Dob_S__R_Field_17 = pnd_Dob_S__R_Field_16.newGroupInGroup("pnd_Dob_S__R_Field_17", "REDEFINE", pnd_Dob_S_Pnd_Dob_S_A);
        pnd_Dob_S_Pnd_Dob_S_Cc = pnd_Dob_S__R_Field_17.newFieldInGroup("pnd_Dob_S_Pnd_Dob_S_Cc", "#DOB-S-CC", FieldType.STRING, 2);
        pnd_Dob_S_Pnd_Dob_S_Yy = pnd_Dob_S__R_Field_17.newFieldInGroup("pnd_Dob_S_Pnd_Dob_S_Yy", "#DOB-S-YY", FieldType.STRING, 2);
        pnd_Dob_S_Pnd_Dob_S_Mm = pnd_Dob_S__R_Field_17.newFieldInGroup("pnd_Dob_S_Pnd_Dob_S_Mm", "#DOB-S-MM", FieldType.STRING, 2);
        pnd_Dob_S_Pnd_Dob_S_Dd = pnd_Dob_S__R_Field_17.newFieldInGroup("pnd_Dob_S_Pnd_Dob_S_Dd", "#DOB-S-DD", FieldType.STRING, 2);
        pnd_Prt_Dod_F = localVariables.newFieldInRecord("pnd_Prt_Dod_F", "#PRT-DOD-F", FieldType.STRING, 5);
        pnd_Prt_Dod_S = localVariables.newFieldInRecord("pnd_Prt_Dod_S", "#PRT-DOD-S", FieldType.STRING, 5);
        pnd_Dod_F = localVariables.newFieldInRecord("pnd_Dod_F", "#DOD-F", FieldType.NUMERIC, 6);

        pnd_Dod_F__R_Field_18 = localVariables.newGroupInRecord("pnd_Dod_F__R_Field_18", "REDEFINE", pnd_Dod_F);
        pnd_Dod_F_Pnd_Dod_F_A = pnd_Dod_F__R_Field_18.newFieldInGroup("pnd_Dod_F_Pnd_Dod_F_A", "#DOD-F-A", FieldType.STRING, 6);

        pnd_Dod_F__R_Field_19 = pnd_Dod_F__R_Field_18.newGroupInGroup("pnd_Dod_F__R_Field_19", "REDEFINE", pnd_Dod_F_Pnd_Dod_F_A);
        pnd_Dod_F_Pnd_Dod_F_Cc = pnd_Dod_F__R_Field_19.newFieldInGroup("pnd_Dod_F_Pnd_Dod_F_Cc", "#DOD-F-CC", FieldType.STRING, 2);
        pnd_Dod_F_Pnd_Dod_F_Yy = pnd_Dod_F__R_Field_19.newFieldInGroup("pnd_Dod_F_Pnd_Dod_F_Yy", "#DOD-F-YY", FieldType.STRING, 2);
        pnd_Dod_F_Pnd_Dod_F_Mm = pnd_Dod_F__R_Field_19.newFieldInGroup("pnd_Dod_F_Pnd_Dod_F_Mm", "#DOD-F-MM", FieldType.STRING, 2);
        pnd_Dod_S = localVariables.newFieldInRecord("pnd_Dod_S", "#DOD-S", FieldType.NUMERIC, 6);

        pnd_Dod_S__R_Field_20 = localVariables.newGroupInRecord("pnd_Dod_S__R_Field_20", "REDEFINE", pnd_Dod_S);
        pnd_Dod_S_Pnd_Dod_S_A = pnd_Dod_S__R_Field_20.newFieldInGroup("pnd_Dod_S_Pnd_Dod_S_A", "#DOD-S-A", FieldType.STRING, 6);

        pnd_Dod_S__R_Field_21 = pnd_Dod_S__R_Field_20.newGroupInGroup("pnd_Dod_S__R_Field_21", "REDEFINE", pnd_Dod_S_Pnd_Dod_S_A);
        pnd_Dod_S_Pnd_Dod_S_Cc = pnd_Dod_S__R_Field_21.newFieldInGroup("pnd_Dod_S_Pnd_Dod_S_Cc", "#DOD-S-CC", FieldType.STRING, 2);
        pnd_Dod_S_Pnd_Dod_S_Yy = pnd_Dod_S__R_Field_21.newFieldInGroup("pnd_Dod_S_Pnd_Dod_S_Yy", "#DOD-S-YY", FieldType.STRING, 2);
        pnd_Dod_S_Pnd_Dod_S_Mm = pnd_Dod_S__R_Field_21.newFieldInGroup("pnd_Dod_S_Pnd_Dod_S_Mm", "#DOD-S-MM", FieldType.STRING, 2);
        pnd_Prt_First_Pay_Dte = localVariables.newFieldInRecord("pnd_Prt_First_Pay_Dte", "#PRT-FIRST-PAY-DTE", FieldType.STRING, 5);
        pnd_First_Pay_Dte = localVariables.newFieldInRecord("pnd_First_Pay_Dte", "#FIRST-PAY-DTE", FieldType.NUMERIC, 6);

        pnd_First_Pay_Dte__R_Field_22 = localVariables.newGroupInRecord("pnd_First_Pay_Dte__R_Field_22", "REDEFINE", pnd_First_Pay_Dte);
        pnd_First_Pay_Dte_Pnd_First_Pay_Dte_A = pnd_First_Pay_Dte__R_Field_22.newFieldInGroup("pnd_First_Pay_Dte_Pnd_First_Pay_Dte_A", "#FIRST-PAY-DTE-A", 
            FieldType.STRING, 6);

        pnd_First_Pay_Dte__R_Field_23 = pnd_First_Pay_Dte__R_Field_22.newGroupInGroup("pnd_First_Pay_Dte__R_Field_23", "REDEFINE", pnd_First_Pay_Dte_Pnd_First_Pay_Dte_A);
        pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Cc = pnd_First_Pay_Dte__R_Field_23.newFieldInGroup("pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Cc", "#FIRST-PAY-DTE-CC", 
            FieldType.STRING, 2);
        pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Yy = pnd_First_Pay_Dte__R_Field_23.newFieldInGroup("pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Yy", "#FIRST-PAY-DTE-YY", 
            FieldType.STRING, 2);
        pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Mm = pnd_First_Pay_Dte__R_Field_23.newFieldInGroup("pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Mm", "#FIRST-PAY-DTE-MM", 
            FieldType.STRING, 2);
        pnd_Current_Check_Date = localVariables.newFieldInRecord("pnd_Current_Check_Date", "#CURRENT-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Check_Date__R_Field_24 = localVariables.newGroupInRecord("pnd_Current_Check_Date__R_Field_24", "REDEFINE", pnd_Current_Check_Date);
        pnd_Current_Check_Date_Pnd_Current_Check_Date_A = pnd_Current_Check_Date__R_Field_24.newFieldInGroup("pnd_Current_Check_Date_Pnd_Current_Check_Date_A", 
            "#CURRENT-CHECK-DATE-A", FieldType.STRING, 8);
        pnd_Current_First_Trans_Date = localVariables.newFieldInRecord("pnd_Current_First_Trans_Date", "#CURRENT-FIRST-TRANS-DATE", FieldType.TIME);
        pnd_Next_Check_Date = localVariables.newFieldInRecord("pnd_Next_Check_Date", "#NEXT-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Next_Check_Date__R_Field_25 = localVariables.newGroupInRecord("pnd_Next_Check_Date__R_Field_25", "REDEFINE", pnd_Next_Check_Date);
        pnd_Next_Check_Date_Pnd_Next_Check_Date_A = pnd_Next_Check_Date__R_Field_25.newFieldInGroup("pnd_Next_Check_Date_Pnd_Next_Check_Date_A", "#NEXT-CHECK-DATE-A", 
            FieldType.STRING, 8);
        pnd_Next_First_Trans_Date = localVariables.newFieldInRecord("pnd_Next_First_Trans_Date", "#NEXT-FIRST-TRANS-DATE", FieldType.PACKED_DECIMAL, 12);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_26", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_26.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_26.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_27 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_27", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cpr_Bfre_Key__R_Field_27.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Bfre_Key__R_Field_27.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Bfre_Key__R_Field_27.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte = pnd_Cpr_Bfre_Key__R_Field_27.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Cntrct_Bfre_Key = localVariables.newFieldInRecord("pnd_Cntrct_Bfre_Key", "#CNTRCT-BFRE-KEY", FieldType.STRING, 18);

        pnd_Cntrct_Bfre_Key__R_Field_28 = localVariables.newGroupInRecord("pnd_Cntrct_Bfre_Key__R_Field_28", "REDEFINE", pnd_Cntrct_Bfre_Key);
        pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Cntrct_Bfre_Key__R_Field_28.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Bfre_Key__R_Field_28.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte = pnd_Cntrct_Bfre_Key__R_Field_28.newFieldInGroup("pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", 
            FieldType.PACKED_DECIMAL, 12);
        pnd_Cpr_Aftr_Key = localVariables.newFieldInRecord("pnd_Cpr_Aftr_Key", "#CPR-AFTR-KEY", FieldType.STRING, 25);

        pnd_Cpr_Aftr_Key__R_Field_29 = localVariables.newGroupInRecord("pnd_Cpr_Aftr_Key__R_Field_29", "REDEFINE", pnd_Cpr_Aftr_Key);
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cpr_Aftr_Key__R_Field_29.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Aftr_Key__R_Field_29.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Aftr_Key__R_Field_29.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cpr_Aftr_Key__R_Field_29.newFieldInGroup("pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Cntrct_Aftr_Key = localVariables.newFieldInRecord("pnd_Cntrct_Aftr_Key", "#CNTRCT-AFTR-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Aftr_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Cntrct_Aftr_Key__R_Field_30", "REDEFINE", pnd_Cntrct_Aftr_Key);
        pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id = pnd_Cntrct_Aftr_Key__R_Field_30.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Aftr_Key__R_Field_30.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte = pnd_Cntrct_Aftr_Key__R_Field_30.newFieldInGroup("pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Save_Cpr_Bfre_Ssn = localVariables.newFieldInRecord("pnd_Save_Cpr_Bfre_Ssn", "#SAVE-CPR-BFRE-SSN", FieldType.NUMERIC, 9);
        pnd_Save_Cpr_Aftr_Ssn = localVariables.newFieldInRecord("pnd_Save_Cpr_Aftr_Ssn", "#SAVE-CPR-AFTR-SSN", FieldType.NUMERIC, 9);
        pnd_Save_Cpr_Bfre_Ctznshp = localVariables.newFieldInRecord("pnd_Save_Cpr_Bfre_Ctznshp", "#SAVE-CPR-BFRE-CTZNSHP", FieldType.NUMERIC, 3);
        pnd_Save_Cpr_Aftr_Ctznshp = localVariables.newFieldInRecord("pnd_Save_Cpr_Aftr_Ctznshp", "#SAVE-CPR-AFTR-CTZNSHP", FieldType.NUMERIC, 3);
        pnd_Save_Cps_Npd_First_Pay_Due_Dte = localVariables.newFieldInRecord("pnd_Save_Cps_Npd_First_Pay_Due_Dte", "#SAVE-CPS-NPD-FIRST-PAY-DUE-DTE", 
            FieldType.NUMERIC, 6);

        pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31 = localVariables.newGroupInRecord("pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31", "REDEFINE", 
            pnd_Save_Cps_Npd_First_Pay_Due_Dte);
        pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Cc = pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Cc", 
            "#FIRST-PAY-DUE-CC", FieldType.NUMERIC, 2);
        pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy = pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy", 
            "#FIRST-PAY-DUE-YY", FieldType.NUMERIC, 2);
        pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Mm = pnd_Save_Cps_Npd_First_Pay_Due_Dte__R_Field_31.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Mm", 
            "#FIRST-PAY-DUE-MM", FieldType.NUMERIC, 2);
        pnd_Save_Cps_Npd_First_Pay_Pd_Dte = localVariables.newFieldInRecord("pnd_Save_Cps_Npd_First_Pay_Pd_Dte", "#SAVE-CPS-NPD-FIRST-PAY-PD-DTE", FieldType.NUMERIC, 
            6);

        pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32 = localVariables.newGroupInRecord("pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32", "REDEFINE", pnd_Save_Cps_Npd_First_Pay_Pd_Dte);
        pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Cc = pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Cc", 
            "#FIRST-PAY-CC", FieldType.NUMERIC, 2);
        pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy = pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy", 
            "#FIRST-PAY-YY", FieldType.NUMERIC, 2);
        pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Mm = pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_32.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Mm", 
            "#FIRST-PAY-MM", FieldType.NUMERIC, 2);

        pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_33 = localVariables.newGroupInRecord("pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_33", "REDEFINE", pnd_Save_Cps_Npd_First_Pay_Pd_Dte);
        pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Ccyy = pnd_Save_Cps_Npd_First_Pay_Pd_Dte__R_Field_33.newFieldInGroup("pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Ccyy", 
            "#FIRST-PAY-CCYY", FieldType.NUMERIC, 4);
        pnd_Save_Cntrct_Bfre_F_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Bfre_F_Dob", "#SAVE-CNTRCT-BFRE-F-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Aftr_F_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Aftr_F_Dob", "#SAVE-CNTRCT-AFTR-F-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Bfre_S_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Bfre_S_Dob", "#SAVE-CNTRCT-BFRE-S-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Aftr_S_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Aftr_S_Dob", "#SAVE-CNTRCT-AFTR-S-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Disp_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Disp_Dob", "#SAVE-CNTRCT-DISP-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Bfre_F_Dod = localVariables.newFieldInRecord("pnd_Save_Cntrct_Bfre_F_Dod", "#SAVE-CNTRCT-BFRE-F-DOD", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Aftr_F_Dod = localVariables.newFieldInRecord("pnd_Save_Cntrct_Aftr_F_Dod", "#SAVE-CNTRCT-AFTR-F-DOD", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Bfre_S_Dod = localVariables.newFieldInRecord("pnd_Save_Cntrct_Bfre_S_Dod", "#SAVE-CNTRCT-BFRE-S-DOD", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Aftr_S_Dod = localVariables.newFieldInRecord("pnd_Save_Cntrct_Aftr_S_Dod", "#SAVE-CNTRCT-AFTR-S-DOD", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Disp_Dod = localVariables.newFieldInRecord("pnd_Save_Cntrct_Disp_Dod", "#SAVE-CNTRCT-DISP-DOD", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_First_Pymnt_Pd_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_First_Pymnt_Pd_Dte", "#SAVE-CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 7);
        pnd_Save_Cntrct_First_Pymnt_Due_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_First_Pymnt_Due_Dte", "#SAVE-CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Save_Cntrct_Comp_Cd = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Comp_Cd", "#SAVE-CNTRCT-COMP-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            5));
        pnd_Save_Prtcpnt_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_Save_Prtcpnt_Tax_Id_Nbr", "#SAVE-PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Save_Prtcpnt_Bfre_Rsdncy_Cde = localVariables.newFieldInRecord("pnd_Save_Prtcpnt_Bfre_Rsdncy_Cde", "#SAVE-PRTCPNT-BFRE-RSDNCY-CDE", FieldType.STRING, 
            3);
        pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde = localVariables.newFieldInRecord("pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde", "#SAVE-PRTCPNT-AFTR-RSDNCY-CDE", FieldType.STRING, 
            3);

        pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde__R_Field_34 = localVariables.newGroupInRecord("pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde__R_Field_34", "REDEFINE", pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde);
        pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_1 = pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde__R_Field_34.newFieldInGroup("pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_1", 
            "#SAVE-PRTCPNT-AFTR-RSDNCY-CDE-1", FieldType.STRING, 1);
        pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3 = pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde__R_Field_34.newFieldInGroup("pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3", 
            "#SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3", FieldType.STRING, 2);
        pnd_Save_Cntrct_Bfre_Per_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Bfre_Per_Ivc_Amt", "#SAVE-CNTRCT-BFRE-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Save_Cntrct_Aftr_Per_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Aftr_Per_Ivc_Amt", "#SAVE-CNTRCT-AFTR-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Save_Cntrct_Disp_Per_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Disp_Per_Ivc_Amt", "#SAVE-CNTRCT-DISP-PER-IVC-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Save_Cntrct_Aftr_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Aftr_Ivc_Amt", "#SAVE-CNTRCT-AFTR-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Save_Cntrct_Aftr_Rtb_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Aftr_Rtb_Amt", "#SAVE-CNTRCT-AFTR-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Save_Cntrct_Disp_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Save_Cntrct_Disp_Ivc_Amt", "#SAVE-CNTRCT-DISP-IVC-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Ivc_Rtb_Amt = localVariables.newFieldInRecord("pnd_Ivc_Rtb_Amt", "#IVC-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Dsp_Amt = localVariables.newFieldArrayInRecord("pnd_Dsp_Amt", "#DSP-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 5));
        pnd_Save_Trans_Dte = localVariables.newFieldInRecord("pnd_Save_Trans_Dte", "#SAVE-TRANS-DTE", FieldType.TIME);
        pnd_Save_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_Save_Trans_Check_Dte", "#SAVE-TRANS-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Save_Trans_Check_Dte__R_Field_35 = localVariables.newGroupInRecord("pnd_Save_Trans_Check_Dte__R_Field_35", "REDEFINE", pnd_Save_Trans_Check_Dte);
        pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr1 = pnd_Save_Trans_Check_Dte__R_Field_35.newFieldInGroup("pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr1", 
            "#TRANS-DTE-FLR1", FieldType.STRING, 4);
        pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm = pnd_Save_Trans_Check_Dte__R_Field_35.newFieldInGroup("pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm", 
            "#TRANS-CHECK-MM", FieldType.STRING, 2);

        pnd_Save_Trans_Check_Dte__R_Field_36 = pnd_Save_Trans_Check_Dte__R_Field_35.newGroupInGroup("pnd_Save_Trans_Check_Dte__R_Field_36", "REDEFINE", 
            pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm);
        pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm_N = pnd_Save_Trans_Check_Dte__R_Field_36.newFieldInGroup("pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm_N", 
            "#TRANS-CHECK-MM-N", FieldType.NUMERIC, 2);
        pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr2 = pnd_Save_Trans_Check_Dte__R_Field_35.newFieldInGroup("pnd_Save_Trans_Check_Dte_Pnd_Trans_Dte_Flr2", 
            "#TRANS-DTE-FLR2", FieldType.STRING, 2);
        pnd_Trans_Effctve_Dte = localVariables.newFieldInRecord("pnd_Trans_Effctve_Dte", "#TRANS-EFFCTVE-DTE", FieldType.NUMERIC, 8);

        pnd_Trans_Effctve_Dte__R_Field_37 = localVariables.newGroupInRecord("pnd_Trans_Effctve_Dte__R_Field_37", "REDEFINE", pnd_Trans_Effctve_Dte);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc = pnd_Trans_Effctve_Dte__R_Field_37.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc", "#TRANS-EFF-CC", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy = pnd_Trans_Effctve_Dte__R_Field_37.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy", "#TRANS-EFF-YY", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm = pnd_Trans_Effctve_Dte__R_Field_37.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm", "#TRANS-EFF-MM", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd = pnd_Trans_Effctve_Dte__R_Field_37.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd", "#TRANS-EFF-DD", 
            FieldType.NUMERIC, 2);

        pnd_Trans_Effctve_Dte__R_Field_38 = localVariables.newGroupInRecord("pnd_Trans_Effctve_Dte__R_Field_38", "REDEFINE", pnd_Trans_Effctve_Dte);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Effctve_Dte_A = pnd_Trans_Effctve_Dte__R_Field_38.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Effctve_Dte_A", 
            "#TRANS-EFFCTVE-DTE-A", FieldType.STRING, 8);

        pnd_Trans_Effctve_Dte__R_Field_39 = pnd_Trans_Effctve_Dte__R_Field_38.newGroupInGroup("pnd_Trans_Effctve_Dte__R_Field_39", "REDEFINE", pnd_Trans_Effctve_Dte_Pnd_Trans_Effctve_Dte_A);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc_A = pnd_Trans_Effctve_Dte__R_Field_39.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Cc_A", "#TRANS-EFF-CC-A", 
            FieldType.STRING, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy_A = pnd_Trans_Effctve_Dte__R_Field_39.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy_A", "#TRANS-EFF-YY-A", 
            FieldType.STRING, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm_A = pnd_Trans_Effctve_Dte__R_Field_39.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm_A", "#TRANS-EFF-MM-A", 
            FieldType.STRING, 2);
        pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd_A = pnd_Trans_Effctve_Dte__R_Field_39.newFieldInGroup("pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd_A", "#TRANS-EFF-DD-A", 
            FieldType.STRING, 2);
        pnd_Prt_Trans_Eff = localVariables.newFieldInRecord("pnd_Prt_Trans_Eff", "#PRT-TRANS-EFF", FieldType.STRING, 8);
        pnd_Save_Trans_Check_Mm = localVariables.newFieldInRecord("pnd_Save_Trans_Check_Mm", "#SAVE-TRANS-CHECK-MM", FieldType.STRING, 2);

        pnd_Save_Trans_Check_Mm__R_Field_40 = localVariables.newGroupInRecord("pnd_Save_Trans_Check_Mm__R_Field_40", "REDEFINE", pnd_Save_Trans_Check_Mm);
        pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N = pnd_Save_Trans_Check_Mm__R_Field_40.newFieldInGroup("pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N", 
            "#SAVE-TRANS-CHECK-MM-N", FieldType.NUMERIC, 2);
        pnd_Ws_Check_Dte = localVariables.newFieldInRecord("pnd_Ws_Check_Dte", "#WS-CHECK-DTE", FieldType.STRING, 8);

        pnd_Ws_Check_Dte__R_Field_41 = localVariables.newGroupInRecord("pnd_Ws_Check_Dte__R_Field_41", "REDEFINE", pnd_Ws_Check_Dte);
        pnd_Ws_Check_Dte_Pnd_Ws_Check_Dte_N = pnd_Ws_Check_Dte__R_Field_41.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Check_Dte_N", "#WS-CHECK-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Ws_Check_Dte__R_Field_42 = localVariables.newGroupInRecord("pnd_Ws_Check_Dte__R_Field_42", "REDEFINE", pnd_Ws_Check_Dte);
        pnd_Ws_Check_Dte_Pnd_Ws_Cc = pnd_Ws_Check_Dte__R_Field_42.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Cc", "#WS-CC", FieldType.NUMERIC, 2);
        pnd_Ws_Check_Dte_Pnd_Ws_Yy = pnd_Ws_Check_Dte__R_Field_42.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Yy", "#WS-YY", FieldType.NUMERIC, 2);
        pnd_Ws_Check_Dte_Pnd_Ws_Mm = pnd_Ws_Check_Dte__R_Field_42.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Mm", "#WS-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Check_Dte_Pnd_Ws_Dd = pnd_Ws_Check_Dte__R_Field_42.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Dd", "#WS-DD", FieldType.NUMERIC, 2);

        pnd_Ws_Check_Dte__R_Field_43 = localVariables.newGroupInRecord("pnd_Ws_Check_Dte__R_Field_43", "REDEFINE", pnd_Ws_Check_Dte);
        pnd_Ws_Check_Dte_Pnd_Ws_Ccyy = pnd_Ws_Check_Dte__R_Field_43.newFieldInGroup("pnd_Ws_Check_Dte_Pnd_Ws_Ccyy", "#WS-CCYY", FieldType.NUMERIC, 4);
        pnd_Save_Trans_Ppcn_Nbr_C = localVariables.newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr_C", "#SAVE-TRANS-PPCN-NBR-C", FieldType.STRING, 10);
        pnd_Save_Trans_Payee_Cde_C = localVariables.newFieldInRecord("pnd_Save_Trans_Payee_Cde_C", "#SAVE-TRANS-PAYEE-CDE-C", FieldType.NUMERIC, 2);
        pnd_Save_Trans_Cde_C = localVariables.newFieldInRecord("pnd_Save_Trans_Cde_C", "#SAVE-TRANS-CDE-C", FieldType.NUMERIC, 3);
        pnd_Save_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 10);

        pnd_Save_Trans_Ppcn_Nbr__R_Field_44 = localVariables.newGroupInRecord("pnd_Save_Trans_Ppcn_Nbr__R_Field_44", "REDEFINE", pnd_Save_Trans_Ppcn_Nbr);
        pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1 = pnd_Save_Trans_Ppcn_Nbr__R_Field_44.newFieldInGroup("pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1", 
            "#SAVE-TRANS-PPCN-NBR1", FieldType.STRING, 8);
        pnd_Save_Trans_Ppcn_Nbr_Pnd_Trans_Flr2 = pnd_Save_Trans_Ppcn_Nbr__R_Field_44.newFieldInGroup("pnd_Save_Trans_Ppcn_Nbr_Pnd_Trans_Flr2", "#TRANS-FLR2", 
            FieldType.STRING, 2);
        pnd_Save_Trans_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Trans_Cde = localVariables.newFieldInRecord("pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 3);

        pnd_Logical_Variables = localVariables.newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_Trans_Ind = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Trans_Ind", "#TRANS-IND", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Variables_Pnd_Bypass = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);

        pnd_Packed_Variables = localVariables.newGroupInRecord("pnd_Packed_Variables", "#PACKED-VARIABLES");
        pnd_Packed_Variables_Pnd_Recs_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ctr", "#RECS-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Ctr_Eq_Dt = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ctr_Eq_Dt", "#RECS-CTR-EQ-DT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1", "#RECS-PROCESSED-CTR1", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2", "#RECS-PROCESSED-CTR2", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr3 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr3", "#RECS-PROCESSED-CTR3", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4", "#RECS-PROCESSED-CTR4", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr5 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr5", "#RECS-PROCESSED-CTR5", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr6 = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Processed_Ctr6", "#RECS-PROCESSED-CTR6", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Bypassed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Bypassed_Ctr", "#RECS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Recs_Cps_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Cps_Ctr", "#RECS-CPS-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Packed_Variables_Pnd_Recs_Ivc = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ivc", "#RECS-IVC", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Ric = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ric", "#RECS-RIC", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Ivm = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ivm", "#RECS-IVM", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Res = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Res", "#RECS-RES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Ssn = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Ssn", "#RECS-SSN", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Dob_F = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Dob_F", "#RECS-DOB-F", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Dob_S = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Dob_S", "#RECS-DOB-S", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Dod_F = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Dod_F", "#RECS-DOD-F", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Dod_S = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Dod_S", "#RECS-DOD-S", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Recs_Cit = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Recs_Cit", "#RECS-CIT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Final_Tot = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Final_Tot", "#FINAL-TOT", FieldType.PACKED_DECIMAL, 
            12);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 2);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 2);
        pnd_Chk_Dte = localVariables.newFieldInRecord("pnd_Chk_Dte", "#CHK-DTE", FieldType.STRING, 8);
        pnd_Cntrct_Mode_Tot = localVariables.newFieldInRecord("pnd_Cntrct_Mode_Tot", "#CNTRCT-MODE-TOT", FieldType.NUMERIC, 3);

        pnd_Cntrct_Mode_Tot__R_Field_45 = localVariables.newGroupInRecord("pnd_Cntrct_Mode_Tot__R_Field_45", "REDEFINE", pnd_Cntrct_Mode_Tot);
        pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1 = pnd_Cntrct_Mode_Tot__R_Field_45.newFieldInGroup("pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1", "#CNTRCT-MODE-1", 
            FieldType.NUMERIC, 1);
        pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_2_3 = pnd_Cntrct_Mode_Tot__R_Field_45.newFieldInGroup("pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_2_3", "#CNTRCT-MODE-2-3", 
            FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_View.reset();
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();

        localVariables.reset();
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Cntrct_Bfre_Key.setInitialValue("1");
        pnd_Cpr_Aftr_Key.setInitialValue("2");
        pnd_Cntrct_Aftr_Key.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap312() throws Exception
    {
        super("Iaap312");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 56 ZP = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 56 ZP = OFF SG = OFF
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 #IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, pnd_Iaa_Parm_Card)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        //* *Y2NCNH
        if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Year_End.notEquals(" ") && pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm.notEquals(2)))                                        //Natural: IF #PARM-YEAR-END NE ' ' AND #PARM-CHECK-DTE-MM NE 02
        {
            //* *Y2NCNH
            getReports().write(0, "YEAR END PARAMETER INCONSISTENCY","=",pnd_Iaa_Parm_Card_Pnd_Parm_Year_End,"=",pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm);                //Natural: WRITE 'YEAR END PARAMETER INCONSISTENCY' '=' #PARM-YEAR-END '=' #PARM-CHECK-DTE-MM
            if (Global.isEscape()) return;
            //* *Y2NCNH
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *Y2NCNH
        pnd_Chk_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm_A, "/", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd_A,   //Natural: COMPRESS #PARM-CHECK-DTE-MM-A '/' #PARM-CHECK-DTE-DD-A '/' #PARM-CHECK-DTE-YY-A INTO #CHK-DTE LEAVING NO
            "/", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy_A));
        //* *Y2NCNH -  #CHK-DTE USED FOR DISPLAY ONLY.
        //*   ASSIGN #PARM-CHECK-DTE-N = 19960601
        //*  WRITE '=' #IAA-PARM-CARD
        //* *Y2NCNH
        pnd_Period_End_Dte_A.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte);                                                                                              //Natural: MOVE #PARM-CHECK-DTE TO #PERIOD-END-DTE-A
        //*  WRITE '=' #PERIOD-END-DTE
        //* *Y2NCNH
        if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Year_End.notEquals(" ")))                                                                                                //Natural: IF #PARM-YEAR-END NE ' '
        {
            pnd_Period_End_Dte_A_Pnd_Mm.setValue(12);                                                                                                                     //Natural: MOVE 12 TO #MM
            //* ****************************************************************
            //* ****  YEAR2000 FIX START
            //* ****************************************************************
            //* *Y2CHNH
            //*    SUBTRACT 1 FROM #YY   /* - Y2K COMMENTED OUT.
            pnd_Period_End_Dte_A_Pnd_Yyyy.nsubtract(1);                                                                                                                   //Natural: SUBTRACT 1 FROM #YYYY
            //* ****************************************************************
            //* ****  YEAR2000 FIX END
            //* ****************************************************************
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Period_End_Dte_A_Pnd_Mm.equals(1)))                                                                                                         //Natural: IF #MM = 01
            {
                pnd_Period_End_Dte_A_Pnd_Mm.setValue(12);                                                                                                                 //Natural: MOVE 12 TO #MM
                //* ****************************************************************
                //* ****  YEAR2000 FIX START
                //* ****************************************************************
                //* *Y2CHNH
                //*    SUBTRACT 1 FROM #YY   /* - Y2K COMMENTED OUT.
                pnd_Period_End_Dte_A_Pnd_Yyyy.nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #YYYY
                //* ****************************************************************
                //* ****  YEAR2000 FIX END
                //* ****************************************************************
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Period_End_Dte_A_Pnd_Mm.nsubtract(1);                                                                                                                 //Natural: SUBTRACT 1 FROM #MM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '=' #PERIOD-END-DTE
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM #SETUP-TABLE
        sub_Pnd_Setup_Table();
        if (condition(Global.isEscape())) {return;}
        //* *Y2NCNH
        if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte.equals(" ")))                                                                                                  //Natural: IF #PARM-CHECK-DTE = ' '
        {
            getReports().write(0, " NO PARAMETER CARD ");                                                                                                                 //Natural: WRITE ' NO PARAMETER CARD '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *Y2NCNH
                                                                                                                                                                          //Natural: PERFORM READ1-CNTRL-RCRD
            sub_Read1_Cntrl_Rcrd();
            if (condition(Global.isEscape())) {return;}
            //* *Y2NCNH
                                                                                                                                                                          //Natural: PERFORM READ2-CNTRL-RCRD
            sub_Read2_Cntrl_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *Y2NCNH
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #PARM-CHECK-DTE
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ01")))
        {
            //* *Y2NCNH
            pnd_Packed_Variables_Pnd_Recs_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECS-CTR
            //*  IF #RECS-CTR > 750
            //*   ESCAPE BOTTOM
            //*  END-IF
            //* *Y2NCNH - BOTH FIELDS FORMAT IS YYYYMMDD.
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.greater(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N)))                                                                //Natural: IF IAA-TRANS-RCRD.TRANS-CHECK-DTE GT #PARM-CHECK-DTE-N
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE ' TRANS-PPCN-NBR   = ' IAA-TRANS-RCRD.TRANS-PPCN-NBR
            //*        ' TRANS-PAYEE-CDE  = ' IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            //*        ' TRANS-CDE        = ' IAA-TRANS-RCRD.TRANS-CDE
            //*  WRITE 'BEFORE CHECK'
            if (condition(iaa_Trans_Rcrd_Trans_Cde.notEquals(50) && iaa_Trans_Rcrd_Trans_Cde.notEquals(66) && iaa_Trans_Rcrd_Trans_Cde.notEquals(106)                     //Natural: IF IAA-TRANS-RCRD.TRANS-CDE NE 050 AND IAA-TRANS-RCRD.TRANS-CDE NE 066 AND IAA-TRANS-RCRD.TRANS-CDE NE 106 AND IAA-TRANS-RCRD.TRANS-CDE NE 724 AND IAA-TRANS-RCRD.TRANS-CDE NE 725 AND IAA-TRANS-RCRD.TRANS-CDE NE 730
                && iaa_Trans_Rcrd_Trans_Cde.notEquals(724) && iaa_Trans_Rcrd_Trans_Cde.notEquals(725) && iaa_Trans_Rcrd_Trans_Cde.notEquals(730)))
            {
                pnd_Packed_Variables_Pnd_Recs_Bypassed_Ctr.nadd(1);                                                                                                       //Natural: ADD 1 TO #RECS-BYPASSED-CTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE 'AFTER CHECK'
            //*   WRITE
            //*    ' TRANS-CHECK-DATE = ' IAA-TRANS-RCRD.TRANS-CHECK-DTE
            //*  /   ' TRANS-DTE        = ' IAA-TRANS-RCRD.TRANS-DTE
            //*  /     ' TRANS-PPCN-NBR   = ' IAA-TRANS-RCRD.TRANS-PPCN-NBR
            //*  /    ' TRANS-PAYEE-CDE  = ' IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            //*  /   ' TRANS-CDE        = ' IAA-TRANS-RCRD.TRANS-CDE
            pnd_Save_Trans_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                              //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #SAVE-TRANS-PPCN-NBR
            pnd_Save_Trans_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #SAVE-TRANS-PAYEE-CDE
            //* *Y2NCNH
            pnd_Save_Trans_Check_Dte.setValue(iaa_Trans_Rcrd_Trans_Check_Dte);                                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-CHECK-DTE TO #SAVE-TRANS-CHECK-DTE
            //* *Y2NCNH
            pnd_Trans_Effctve_Dte.setValue(iaa_Trans_Rcrd_Trans_Todays_Dte);                                                                                              //Natural: MOVE IAA-TRANS-RCRD.TRANS-TODAYS-DTE TO #TRANS-EFFCTVE-DTE
            //*    WRITE '=' #SAVE-TRANS-PPCN-NBR
            //*     '=' #SAVE-TRANS-PAYEE-CDE
            //*     '=' #SAVE-TRANS-CHECK-DTE
            //*     '=' #TRANS-EFFCTVE-DTE
            //* *Y2NCNH
            if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Year_End.notEquals(" ")))                                                                                            //Natural: IF #PARM-YEAR-END NE ' '
            {
                pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N.setValue(12);                                                                                           //Natural: MOVE 12 TO #SAVE-TRANS-CHECK-MM-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N.compute(new ComputeParameters(false, pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N),                //Natural: SUBTRACT 1 FROM #TRANS-CHECK-MM-N GIVING #SAVE-TRANS-CHECK-MM-N
                    pnd_Save_Trans_Check_Dte_Pnd_Trans_Check_Mm_N.subtract(1));
                if (condition(pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N.equals(getZero())))                                                                       //Natural: IF #SAVE-TRANS-CHECK-MM-N = 0
                {
                    pnd_Save_Trans_Check_Mm_Pnd_Save_Trans_Check_Mm_N.setValue(12);                                                                                       //Natural: MOVE 12 TO #SAVE-TRANS-CHECK-MM-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *Y2NCNH
            pnd_Save_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-DTE TO #SAVE-TRANS-DTE
            //* *Y2NCNH
            pnd_Trn_Dte.setValueEdited(pnd_Save_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED #SAVE-TRANS-DTE ( EM = YYYYMMDD ) TO #TRN-DTE
                                                                                                                                                                          //Natural: PERFORM CHECK-PARM-CARD
            sub_Check_Parm_Card();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Logical_Variables_Pnd_Bypass.getBoolean()))                                                                                                 //Natural: IF #BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MAIN-RTN
            sub_Main_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Save_Trans_Ppcn_Nbr_C.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                            //Natural: MOVE IAA-TRANS-RCRD.TRANS-PPCN-NBR TO #SAVE-TRANS-PPCN-NBR-C
            pnd_Save_Trans_Payee_Cde_C.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                          //Natural: MOVE IAA-TRANS-RCRD.TRANS-PAYEE-CDE TO #SAVE-TRANS-PAYEE-CDE-C
            pnd_Save_Trans_Cde_C.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                      //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE-C
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Packed_Variables_Pnd_Final_Tot.compute(new ComputeParameters(false, pnd_Packed_Variables_Pnd_Final_Tot), pnd_Packed_Variables_Pnd_Recs_Ivc.add(pnd_Packed_Variables_Pnd_Recs_Ric).add(pnd_Packed_Variables_Pnd_Recs_Ivm).add(pnd_Packed_Variables_Pnd_Recs_Res).add(pnd_Packed_Variables_Pnd_Recs_Ssn).add(pnd_Packed_Variables_Pnd_Recs_Dob_F).add(pnd_Packed_Variables_Pnd_Recs_Dob_S).add(pnd_Packed_Variables_Pnd_Recs_Dod_F).add(pnd_Packed_Variables_Pnd_Recs_Dod_S).add(pnd_Packed_Variables_Pnd_Recs_Cit)); //Natural: COMPUTE #FINAL-TOT = #RECS-IVC + #RECS-RIC + #RECS-IVM + #RECS-RES + #RECS-SSN + #RECS-DOB-F + #RECS-DOB-S + #RECS-DOD-F + #RECS-DOD-S + #RECS-CIT
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"TOTAL RECORDS SELECTED");                                                   //Natural: WRITE ( 1 ) /// 2T 'TOTAL RECORDS SELECTED'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"IVC (IVC ADJUSTMENT)          ",pnd_Packed_Variables_Pnd_Recs_Ivc,NEWLINE,new TabSetting(2),"RIC (RTB IVC ADJUSTMENT)      ",pnd_Packed_Variables_Pnd_Recs_Ric,NEWLINE,new  //Natural: WRITE ( 1 ) 2T 'IVC (IVC ADJUSTMENT)          ' #RECS-IVC / 2T 'RIC (RTB IVC ADJUSTMENT)      ' #RECS-RIC / 2T 'IVM (IVM ADJUSTMENT)          ' #RECS-IVM / 2T 'RES (RESIDENCE CODE CHANGE)   ' #RECS-RES / 2T 'SSN (SSN CHANGE)              ' #RECS-SSN / 2T 'DOB (DOB 1ST ANNUITANT CHANGE)' #RECS-DOB-F / 2T 'DOB (DOB 2ND ANNUITANT CHANGE)' #RECS-DOB-S / 2T 'DOD (DOD 1ST ANNUITANT CHANGE)' #RECS-DOD-F / 2T 'DOD (DOD 2ND ANNUITANT CHANGE)' #RECS-DOD-S / 2T 'CIT (CITIZENSHIP CODE CHANGE) ' #RECS-CIT // 24T 'TOTAL' #FINAL-TOT
            TabSetting(2),"IVM (IVM ADJUSTMENT)          ",pnd_Packed_Variables_Pnd_Recs_Ivm,NEWLINE,new TabSetting(2),"RES (RESIDENCE CODE CHANGE)   ",pnd_Packed_Variables_Pnd_Recs_Res,NEWLINE,new 
            TabSetting(2),"SSN (SSN CHANGE)              ",pnd_Packed_Variables_Pnd_Recs_Ssn,NEWLINE,new TabSetting(2),"DOB (DOB 1ST ANNUITANT CHANGE)",pnd_Packed_Variables_Pnd_Recs_Dob_F,NEWLINE,new 
            TabSetting(2),"DOB (DOB 2ND ANNUITANT CHANGE)",pnd_Packed_Variables_Pnd_Recs_Dob_S,NEWLINE,new TabSetting(2),"DOD (DOD 1ST ANNUITANT CHANGE)",pnd_Packed_Variables_Pnd_Recs_Dod_F,NEWLINE,new 
            TabSetting(2),"DOD (DOD 2ND ANNUITANT CHANGE)",pnd_Packed_Variables_Pnd_Recs_Dod_S,NEWLINE,new TabSetting(2),"CIT (CITIZENSHIP CODE CHANGE) ",pnd_Packed_Variables_Pnd_Recs_Cit,NEWLINE,NEWLINE,new 
            TabSetting(24),"TOTAL",pnd_Packed_Variables_Pnd_Final_Tot);
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM MAIN-RTN
        sub_Main_Rtn();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE TAX MAINTENANCE RPT(LATER)
                                                                                                                                                                          //Natural: PERFORM #WRITE-TRAILER-RECORD
        sub_Pnd_Write_Trailer_Record();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",pnd_Packed_Variables_Pnd_Recs_Ctr);                                                              //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #RECS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS TRANS-CH-DT = PRM-DT: ",pnd_Packed_Variables_Pnd_Recs_Ctr_Eq_Dt);                                                        //Natural: WRITE 'NUMBER OF RECORDS TRANS-CH-DT = PRM-DT: ' #RECS-CTR-EQ-DT
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-050       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-050       : ' #RECS-PROCESSED-CTR1
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-066       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-066       : ' #RECS-PROCESSED-CTR2
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-106       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr3);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-106       : ' #RECS-PROCESSED-CTR3
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-724       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-724       : ' #RECS-PROCESSED-CTR4
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-730       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr5);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-730       : ' #RECS-PROCESSED-CTR5
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED-NAZ       : ",pnd_Packed_Variables_Pnd_Recs_Processed_Ctr6);                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED-NAZ       : ' #RECS-PROCESSED-CTR6
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Packed_Variables_Pnd_Recs_Bypassed_Ctr);                                                     //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #RECS-BYPASSED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF CPS-RECORDS WRITTEN         : ",pnd_Packed_Variables_Pnd_Recs_Cps_Ctr);                                                          //Natural: WRITE 'NUMBER OF CPS-RECORDS WRITTEN         : ' #RECS-CPS-CTR
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIN-RTN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-TRANS-RTN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRANS-RCRD
        //* *Y2NCNH
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ1-CNTRL-RCRD
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TRAILER-RECORD
        //* ****************************************************************
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-TABLE
        //* ****************************************************************
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-REPORT
        //* ****************************************************************
        //* *Y2NCNH
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ2-CNTRL-RCRD
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHK-NEXT-DATE-RTN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PARM-CARD
        //* *Y2NCNH
        //* *Y2NCNH - BOTH FIELDS FORMAT IS YYYYMMDD.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IMAGE-RECS-RTN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-CONTRACT
        //* ***********************************************************************
        //*    WRITE 'READ CONTRACT'
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-1
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-2
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-3
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-4
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-050
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-066
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-106
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-724
        //*    WRITE '=' #PER-IVC-HOLD
        //*   WRITE ' CNTRCT-PER-IVC-AMT (IVC) ' #SAVE-CNTRCT-DISP-PER-IVC-AMT (#J)
        //*     WRITE 'AFTER YTD PARA' '=' #CPS-NPD-YTD-IVC-AMT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-NAZ
        //*    WRITE '=' #PER-IVC-HOLD
        //*   WRITE ' CNTRCT-PER-IVC-AMT (IVC) ' #SAVE-CNTRCT-DISP-PER-IVC-AMT (#J)
        //*     WRITE 'AFTER YTD PARA' '=' #CPS-NPD-YTD-IVC-AMT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPS-REC-730
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #YTD-IVC-PARA
        //* *Y2CHNH
        //* ****************************************************************
        //* ****  YEAR2000 FIX START
        //* ****************************************************************
        //*  WHEN #FIRST-PAY-YY < #YY - COMMENTED OUT.
        //* ****************************************************************
        //* ****  YEAR2000 FIX START
        //* ****************************************************************
        //* *Y2NCNH
        //* *Y2NCNH
        //* *Y2NCNH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MODE-PARA
    }
    private void sub_Main_Rtn() throws Exception                                                                                                                          //Natural: MAIN-RTN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.equals(pnd_Save_Trans_Ppcn_Nbr_C) && iaa_Trans_Rcrd_Trans_Payee_Cde.equals(pnd_Save_Trans_Payee_Cde_C)                //Natural: IF IAA-TRANS-RCRD.TRANS-PPCN-NBR = #SAVE-TRANS-PPCN-NBR-C AND IAA-TRANS-RCRD.TRANS-PAYEE-CDE = #SAVE-TRANS-PAYEE-CDE-C AND IAA-TRANS-RCRD.TRANS-CDE = #SAVE-TRANS-CDE-C
            && iaa_Trans_Rcrd_Trans_Cde.equals(pnd_Save_Trans_Cde_C)))
        {
            ignore();
            //*  PERFORM SET-TRANS-RTN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IF #TRANS-IND
            //*   WRITE 'USE THE ELSE'
                                                                                                                                                                          //Natural: PERFORM READ-IMAGE-RECS-RTN
            sub_Read_Image_Recs_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRANS-RCRD
            sub_Process_Trans_Rcrd();
            if (condition(Global.isEscape())) {return;}
            //*    #TRANS-IND := FALSE
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Trans_Rtn() throws Exception                                                                                                                     //Natural: SET-TRANS-RTN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(50) || iaa_Trans_Rcrd_Trans_Cde.equals(66) || iaa_Trans_Rcrd_Trans_Cde.equals(106) || iaa_Trans_Rcrd_Trans_Cde.equals(724))) //Natural: IF TRANS-CDE = 050 OR TRANS-CDE = 066 OR TRANS-CDE = 106 OR TRANS-CDE = 724
        {
            pnd_Logical_Variables_Pnd_Trans_Ind.setValue(true);                                                                                                           //Natural: ASSIGN #TRANS-IND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Trans_Rcrd() throws Exception                                                                                                                //Natural: PROCESS-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //*  WRITE '=' TRANS-CDE
        short decideConditionsMet1039 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TRANS-CDE;//Natural: VALUE 050
        if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(50))))
        {
            decideConditionsMet1039++;
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-050
            sub_Create_Cps_Rec_050();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 066
        else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(66))))
        {
            decideConditionsMet1039++;
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-066
            sub_Create_Cps_Rec_066();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(106))))
        {
            decideConditionsMet1039++;
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-106
            sub_Create_Cps_Rec_106();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(724))))
        {
            decideConditionsMet1039++;
            if (condition(iaa_Trans_Rcrd_Trans_User_Area.equals("NAZ")))                                                                                                  //Natural: IF IAA-TRANS-RCRD.TRANS-USER-AREA = 'NAZ'
            {
                pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-NAZ
                sub_Create_Cps_Rec_Naz();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                    //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-724
                sub_Create_Cps_Rec_724();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 725
        else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(725))))
        {
            decideConditionsMet1039++;
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-NAZ
            sub_Create_Cps_Rec_Naz();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 730
        else if (condition((iaa_Trans_Rcrd_Trans_Cde.equals(730))))
        {
            decideConditionsMet1039++;
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: MOVE IAA-TRANS-RCRD.TRANS-CDE TO #SAVE-TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-CPS-REC-730
            sub_Create_Cps_Rec_730();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Read1_Cntrl_Rcrd() throws Exception                                                                                                                  //Natural: READ1-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* *Y2NCNH
        pnd_Invrse_Date.compute(new ComputeParameters(false, pnd_Invrse_Date), DbsField.subtract(100000000,pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N));                      //Natural: COMPUTE #INVRSE-DATE = 100000000 - #PARM-CHECK-DTE-N
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.setValue(pnd_Invrse_Date);                                                                                                //Natural: ASSIGN #CNTRL-INVRSE-DTE := #INVRSE-DATE
        //* *Y2NCNH
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ02")))
        {
            //* *Y2NCNH
            pnd_Current_Check_Date_Pnd_Current_Check_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CURRENT-CHECK-DATE-A
            pnd_Current_First_Trans_Date.setValue(iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte);                                                                                   //Natural: ASSIGN #CURRENT-FIRST-TRANS-DATE := CNTRL-FRST-TRANS-DTE
            //*  WRITE 'TRNS-DT = ' #CURRENT-FIRST-TRANS-DATE(EM=YYYYMMDDHHIISST)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Trailer_Record() throws Exception                                                                                                          //Natural: #WRITE-TRAILER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Trailer_Record.reset();                                                                                                                                       //Natural: RESET #TRAILER-RECORD
        pnd_Trailer_Record_Pnd_T_Filler2.setValue(save_Filler2);                                                                                                          //Natural: ASSIGN #T-FILLER2 := SAVE-FILLER2
        save_Filler2.reset();                                                                                                                                             //Natural: RESET SAVE-FILLER2
        pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_1.setValue("H'00'");                                                                                                   //Natural: MOVE H'00' TO #T-CPS-NPD-MAINT-CDE-1
        pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_2_3.setValue("HD");                                                                                                    //Natural: MOVE 'HD' TO #T-CPS-NPD-MAINT-CDE-2-3
        pnd_Trailer_Record_Pnd_T_Timestamp.setValue(Global.getTIMESTMP());                                                                                                //Natural: MOVE *TIMESTMP TO #T-TIMESTAMP
        //*  MOVE *TIMX                       TO #TIME
        //*  MOVE EDITED #TIME(EM=HH:II:SS)   TO #T-TIMESTAMP
        //* *Y2NCNH
        pnd_Trailer_Record_Pnd_T_Interface_Dte.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yyyymm);                                                                     //Natural: MOVE #PARM-CHECK-DTE-YYYYMM TO #T-INTERFACE-DTE
        pnd_Trailer_Record_Pnd_T_Title.setValue("TAX MAINTENANCE INTERFACE");                                                                                             //Natural: MOVE 'TAX MAINTENANCE INTERFACE' TO #T-TITLE
        pnd_Trailer_Record_Pnd_T_Rec_Count.setValue(pnd_Packed_Variables_Pnd_Recs_Cps_Ctr);                                                                               //Natural: MOVE #RECS-CPS-CTR TO #T-REC-COUNT
        getReports().write(0, "=",pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_1,NEWLINE,"=",pnd_Trailer_Record_Pnd_T_Cps_Npd_Maint_Cde_2_3,NEWLINE,"=",                    //Natural: WRITE '=' #T-CPS-NPD-MAINT-CDE-1 / '=' #T-CPS-NPD-MAINT-CDE-2-3 / '=' #TIME / '=' #T-TIMESTAMP / '=' #T-INTERFACE-DTE / '=' #T-TITLE / '=' #T-REC-COUNT /
            pnd_Time,NEWLINE,"=",pnd_Trailer_Record_Pnd_T_Timestamp,NEWLINE,"=",pnd_Trailer_Record_Pnd_T_Interface_Dte,NEWLINE,"=",pnd_Trailer_Record_Pnd_T_Title,
            NEWLINE,"=",pnd_Trailer_Record_Pnd_T_Rec_Count,NEWLINE);
        if (Global.isEscape()) return;
        getWorkFiles().write(1, false, pnd_Trailer_Record);                                                                                                               //Natural: WRITE WORK FILE 1 #TRAILER-RECORD
        getReports().write(0, "TRAILER RECORD WRITTEN");                                                                                                                  //Natural: WRITE 'TRAILER RECORD WRITTEN'
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Setup_Table() throws Exception                                                                                                                   //Natural: #SETUP-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Year_Table.getValue("*","*").reset();                                                                                                                         //Natural: RESET #YEAR-TABLE ( *,* )
        FT1:                                                                                                                                                              //Natural: FOR #A = 1 TO 12
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
        {
            pnd_Year_Table.getValue(1,pnd_A).setValue(pnd_A);                                                                                                             //Natural: MOVE #A TO #YEAR-TABLE ( 1,#A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FT2:                                                                                                                                                              //Natural: FOR #A = 1 TO 12
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
        {
            pnd_B.compute(new ComputeParameters(false, pnd_B), pnd_A.add(10));                                                                                            //Natural: COMPUTE #B = #A + 10
            pnd_Year_Table.getValue(pnd_B,1).setValue(pnd_A);                                                                                                             //Natural: MOVE #A TO #YEAR-TABLE ( #B,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.setValue(4);                                                                                                                                                //Natural: ASSIGN #B := 4
        FT3:                                                                                                                                                              //Natural: FOR #A = 1 TO 6
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(6)); pnd_A.nadd(1))
        {
            pnd_B.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #B
            pnd_Year_Table.getValue(pnd_B,1).setValue(pnd_A);                                                                                                             //Natural: MOVE #A TO #YEAR-TABLE ( #B,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.setValue(4);                                                                                                                                                //Natural: ASSIGN #B := 4
        FT4:                                                                                                                                                              //Natural: FOR #A = 7 TO 12
        for (pnd_A.setValue(7); condition(pnd_A.lessOrEqual(12)); pnd_A.nadd(1))
        {
            pnd_B.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #B
            pnd_Year_Table.getValue(pnd_B,2).setValue(pnd_A);                                                                                                             //Natural: MOVE #A TO #YEAR-TABLE ( #B,2 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.setValue(1);                                                                                                                                                //Natural: ASSIGN #B := 1
        FT5:                                                                                                                                                              //Natural: FOR #A = 1 TO 4
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(4)); pnd_A.nadd(1))
        {
            pnd_Year_Table.getValue(2,pnd_A).setValue(pnd_B);                                                                                                             //Natural: MOVE #B TO #YEAR-TABLE ( 2,#A )
            pnd_B.nadd(3);                                                                                                                                                //Natural: ADD 3 TO #B
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.setValue(2);                                                                                                                                                //Natural: ASSIGN #B := 2
        FT6:                                                                                                                                                              //Natural: FOR #A = 1 TO 4
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(4)); pnd_A.nadd(1))
        {
            pnd_Year_Table.getValue(3,pnd_A).setValue(pnd_B);                                                                                                             //Natural: MOVE #B TO #YEAR-TABLE ( 3,#A )
            pnd_B.nadd(3);                                                                                                                                                //Natural: ADD 3 TO #B
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.setValue(3);                                                                                                                                                //Natural: ASSIGN #B := 3
        FT7:                                                                                                                                                              //Natural: FOR #A = 1 TO 4
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(4)); pnd_A.nadd(1))
        {
            pnd_Year_Table.getValue(4,pnd_A).setValue(pnd_B);                                                                                                             //Natural: MOVE #B TO #YEAR-TABLE ( 4,#A )
            pnd_B.nadd(3);                                                                                                                                                //Natural: ADD 3 TO #B
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        F9:                                                                                                                                                               //Natural: FOR #A = 1 TO 22
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(22)); pnd_A.nadd(1))
        {
            getReports().write(0, pnd_Year_Table.getValue(pnd_A,"*"));                                                                                                    //Natural: WRITE #YEAR-TABLE ( #A,* )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F9"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Report() throws Exception                                                                                                                  //Natural: #WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Prt_Trans_Eff.reset();                                                                                                                                        //Natural: RESET #PRT-TRANS-EFF
        //* *Y2NCNH
        if (condition(pnd_Trans_Effctve_Dte_Pnd_Trans_Effctve_Dte_A.notEquals(" ")))                                                                                      //Natural: IF #TRANS-EFFCTVE-DTE-A NE ' '
        {
            //* *Y2NCNH - #PRT-TRANS-EFF USED FOR DISPLAY ONLY.
            pnd_Prt_Trans_Eff.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm_A, "/", pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd_A,  //Natural: COMPRESS #TRANS-EFF-MM-A '/' #TRANS-EFF-DD-A '/' #TRANS-EFF-YY-A INTO #PRT-TRANS-EFF LEAVING NO
                "/", pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Yy_A));
        }                                                                                                                                                                 //Natural: END-IF
        //* *Y2NCNH
        pnd_Dt_Trn_Tot.setValueEdited(pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED #CPS-NPD-TRANS-DTE ( EM = YYYYMMDD ) TO #DT-TRN-TOT
        //*  COMPRESS-DT-TRN-YYYY '/' #DT-TRN-MM '/' #DT-TRN-DD INTO #PRT-TRN-DT
        //*    LEAVING NO
        //* *Y2NCNH
        pnd_Dob_F.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_F);                                                                                                     //Natural: MOVE #CPS-NPD-NEW-DOB-F TO #DOB-F
        //* *Y2NCNH - #PRT-DOB-F USED FOR DISPLAY ONLY.
        pnd_Prt_Dob_F.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_F_Pnd_Dob_F_Mm, "/", pnd_Dob_F_Pnd_Dob_F_Dd, "/", pnd_Dob_F_Pnd_Dob_F_Yy));        //Natural: COMPRESS #DOB-F-MM '/' #DOB-F-DD '/' #DOB-F-YY TO #PRT-DOB-F LEAVING NO
        //* *Y2NCNH
        pnd_Dob_S.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_S);                                                                                                     //Natural: MOVE #CPS-NPD-NEW-DOB-S TO #DOB-S
        //* *Y2NCNH - #PRT-DOB-S USED FOR DISPLAY ONLY.
        pnd_Prt_Dob_S.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_S_Pnd_Dob_S_Mm, "/", pnd_Dob_S_Pnd_Dob_S_Dd, "/", pnd_Dob_S_Pnd_Dob_S_Yy));        //Natural: COMPRESS #DOB-S-MM '/' #DOB-S-DD '/' #DOB-S-YY TO #PRT-DOB-S LEAVING NO
        //* *Y2NCNH
        pnd_Dod_F.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_F);                                                                                                     //Natural: MOVE #CPS-NPD-NEW-DOD-F TO #DOD-F
        //* *Y2NCNH - #PRT-DOD-F USED FOR DISPLAY ONLY.
        pnd_Prt_Dod_F.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dod_F_Pnd_Dod_F_Mm, "/", pnd_Dod_F_Pnd_Dod_F_Yy));                                     //Natural: COMPRESS #DOD-F-MM '/' #DOD-F-YY TO #PRT-DOD-F LEAVING NO
        //* *Y2NCNH
        pnd_Dod_S.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_S);                                                                                                     //Natural: MOVE #CPS-NPD-NEW-DOD-S TO #DOD-S
        //* *Y2NCNH - #PRT-DOD-S USED FOR DISPLAY ONLY.
        pnd_Prt_Dod_S.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dod_S_Pnd_Dod_S_Mm, "/", pnd_Dod_S_Pnd_Dod_S_Yy));                                     //Natural: COMPRESS #DOD-S-MM '/' #DOD-S-YY TO #PRT-DOD-S LEAVING NO
        //* *Y2NCNH
        pnd_First_Pay_Dte.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte);                                                                                      //Natural: MOVE #CPS-NPD-FIRST-PAY-PD-DTE TO #FIRST-PAY-DTE
        //* *Y2NCNH - #PRT-FIRST-PAY-DTE USED FOR DISPLAY ONLY.
        pnd_Prt_First_Pay_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Mm, "/", pnd_First_Pay_Dte_Pnd_First_Pay_Dte_Yy)); //Natural: COMPRESS #FIRST-PAY-DTE-MM '/' #FIRST-PAY-DTE-YY TO #PRT-FIRST-PAY-DTE LEAVING NO
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde,new TabSetting(6),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr,new  //Natural: WRITE ( 1 ) 002T #CPS-NPD-PRODUCT-CDE 006T #CPS-NPD-CNTRCT-NBR 015T #CPS-NPD-PAYEE-CDE-A 019T #CPS-NPD-TAX-ID ( EM = 999-999-999 ) 032T #CPS-NPD-SOURCE 037T #CPS-NPD-PYMNT-MONTH 041T #DT-TRN-TOT ( EM = XXXX/XX/XX ) 053T #CPS-NPD-MAINT-CDE 058T #CPS-NPD-PER-IVC-CDE 061T #CPS-NPD-PER-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 ) 075T #CPS-NPD-NEW-RSDNCY-CDE 080T #CPS-NPD-NEW-TAX-ID ( EM = 999-999-999 ) 093T #PRT-DOB-F 103T #PRT-DOB-S 113T #CPS-NPD-CITIZENSHIP 117T #CPS-NPD-TOT-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(15),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde_A,new TabSetting(19),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id, new ReportEditMask ("999-999-999"),new 
            TabSetting(32),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Source,new TabSetting(37),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month,new TabSetting(41),pnd_Dt_Trn_Tot, 
            new ReportEditMask ("XXXX/XX/XX"),new TabSetting(53),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde,new TabSetting(58),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde,new 
            TabSetting(61),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(75),pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde,new 
            TabSetting(80),pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Tax_Id, new ReportEditMask ("999-999-999"),new TabSetting(93),pnd_Prt_Dob_F,new TabSetting(103),pnd_Prt_Dob_S,new 
            TabSetting(113),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship,new TabSetting(117),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(43),pnd_Prt_First_Pay_Dte,new TabSetting(58),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Cde,new             //Natural: WRITE ( 1 ) 043T #PRT-FIRST-PAY-DTE 058T #CPS-NPD-RTB-IVC-CDE 061T #CPS-NPD-RTB-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 ) 082T #PRT-TRANS-EFF 095T #PRT-DOD-F 105T #PRT-DOD-S 113T #CPS-NPD-NEW-CTZN-CDE 117T #CPS-NPD-YTD-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 ) /
            TabSetting(61),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Prt_Trans_Eff,new TabSetting(95),pnd_Prt_Dod_F,new 
            TabSetting(105),pnd_Prt_Dod_S,new TabSetting(113),pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Ctzn_Cde,new TabSetting(117),pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Read2_Cntrl_Rcrd() throws Exception                                                                                                                  //Natural: READ2-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Check_Dte_Pnd_Ws_Cc.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Cc);                                                                                     //Natural: ASSIGN #WS-CC := #PARM-CHECK-DTE-CC
        pnd_Ws_Check_Dte_Pnd_Ws_Yy.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Yy);                                                                                     //Natural: ASSIGN #WS-YY := #PARM-CHECK-DTE-YY
        pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Mm);                                                                                     //Natural: ASSIGN #WS-MM := #PARM-CHECK-DTE-MM
        pnd_Ws_Check_Dte_Pnd_Ws_Dd.setValue(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_Dd);                                                                                     //Natural: ASSIGN #WS-DD := #PARM-CHECK-DTE-DD
        pnd_Ws_Check_Dte_Pnd_Ws_Mm.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WS-MM
        if (condition(pnd_Ws_Check_Dte_Pnd_Ws_Mm.greater(12)))                                                                                                            //Natural: IF #WS-MM > 12
        {
            pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #WS-MM
            //* ****************************************************************
            //* ****  YEAR2000 FIX START
            //* ****************************************************************
            //* *Y2CHNH
            //*  ADD  1 TO #WS-YY  - COMMENTED OUT.
            pnd_Ws_Check_Dte_Pnd_Ws_Ccyy.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-CCYY
            //* ****************************************************************
            //* ****  YEAR2000 FIX END
            //* ****************************************************************
        }                                                                                                                                                                 //Natural: END-IF
        //* *Y2NCNH
        pnd_Invrse_Date.compute(new ComputeParameters(false, pnd_Invrse_Date), DbsField.subtract(100000000,pnd_Ws_Check_Dte_Pnd_Ws_Check_Dte_N));                         //Natural: COMPUTE #INVRSE-DATE = 100000000 - #WS-CHECK-DTE-N
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.setValue(pnd_Invrse_Date);                                                                                                //Natural: ASSIGN #CNTRL-INVRSE-DTE := #INVRSE-DATE
        //* *Y2NCNH
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ03")))
        {
            //* *Y2NCNH
            pnd_Next_Check_Date_Pnd_Next_Check_Date_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #NEXT-CHECK-DATE-A
            //*  REMOVE
            if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Year_End.equals("Y")))                                                                                               //Natural: IF #PARM-YEAR-END = 'Y'
            {
                pnd_Next_First_Trans_Date.setValue(Global.getTIMX());                                                                                                     //Natural: ASSIGN #NEXT-FIRST-TRANS-DATE := *TIMX
                //* *IK 12/05/2002
                save_Filler2_Pnd_T_F2.setValue("SPECIAL");                                                                                                                //Natural: MOVE 'SPECIAL' TO #T-F2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Next_First_Trans_Date.setValue(iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte);                                                                                  //Natural: ASSIGN #NEXT-FIRST-TRANS-DATE := CNTRL-FRST-TRANS-DTE
            }                                                                                                                                                             //Natural: END-IF
            //*  THIS REDUCES THE #NEXT-FIRST-TRANS-DATE BY 6000 TENTHS OF A SECOND
            //* *Y2NCNH
            pnd_Next_First_Trans_Date.nsubtract(6000);                                                                                                                    //Natural: SUBTRACT 6000 FROM #NEXT-FIRST-TRANS-DATE
            //*    PERFORM CHK-NEXT-DATE-RTN
            //*  MOVE #WS-CHECK-DTE-N
            //*    TO #NEXT-FIRST-TRANS-DATE
            //* *Y2NCNH
            pnd_Next_Invrse_Date.compute(new ComputeParameters(false, pnd_Next_Invrse_Date), new DbsDecimal("1000000000000").subtract(pnd_Next_First_Trans_Date));        //Natural: COMPUTE #NEXT-INVRSE-DATE = 1000000000000 - #NEXT-FIRST-TRANS-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Chk_Next_Date_Rtn() throws Exception                                                                                                                 //Natural: CHK-NEXT-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Check_Dte_Pnd_Ws_Mm.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #WS-MM
        if (condition(pnd_Ws_Check_Dte_Pnd_Ws_Mm.less(1)))                                                                                                                //Natural: IF #WS-MM < 1
        {
            //* *Y2CHNH
            //* ****************************************************************
            //* ****  YEAR2000 FIX START
            //* ****************************************************************
            //*  SUBTRACT 1 FROM #WS-YY /* Y2K - COMMENTED OUT.
            pnd_Ws_Check_Dte_Pnd_Ws_Ccyy.nsubtract(1);                                                                                                                    //Natural: SUBTRACT 1 FROM #WS-CCYY
            //* ****************************************************************
            //* ****  YEAR2000 FIX END
            //* ****************************************************************
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(1) || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(3) || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(5) || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(7)  //Natural: IF #WS-MM = 01 OR #WS-MM = 03 OR #WS-MM = 05 OR #WS-MM = 07 OR #WS-MM = 08 OR #WS-MM = 10 OR #WS-MM = 12
            || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(8) || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(10) || pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(12)))
        {
            pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(31);                                                                                                                      //Natural: MOVE 31 TO #WS-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(30);                                                                                                                      //Natural: MOVE 30 TO #WS-MM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Check_Dte_Pnd_Ws_Mm.equals(2)))                                                                                                              //Natural: IF #WS-MM = 02
        {
            //* *Y2NCNH
            pnd_R.compute(new ComputeParameters(false, pnd_R), pnd_Ws_Check_Dte_Pnd_Ws_Yy.mod(4));                                                                        //Natural: DIVIDE 4 INTO #WS-YY GIVING #D REMAINDER #R
            pnd_D.compute(new ComputeParameters(false, pnd_D), pnd_Ws_Check_Dte_Pnd_Ws_Yy.divide(4));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_R.notEquals(getZero())))                                                                                                                        //Natural: IF #R NOT = 0
        {
            pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(28);                                                                                                                      //Natural: MOVE 28 TO #WS-MM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Check_Dte_Pnd_Ws_Mm.setValue(29);                                                                                                                      //Natural: MOVE 29 TO #WS-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Parm_Card() throws Exception                                                                                                                   //Natural: CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Logical_Variables_Pnd_Bypass.reset();                                                                                                                         //Natural: RESET #BYPASS
        short decideConditionsMet1274 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PARM-CHECK-DTE NE ' '
        if (condition(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte.notEquals(" ")))
        {
            decideConditionsMet1274++;
            //* *Y2NCNH
            if (condition(DbsUtil.maskMatches(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte,"YYYYMMDD") && iaa_Trans_Rcrd_Trans_Check_Dte.equals(pnd_Iaa_Parm_Card_Pnd_Parm_Check_Dte_N))) //Natural: IF #PARM-CHECK-DTE = MASK ( YYYYMMDD ) AND IAA-TRANS-RCRD.TRANS-CHECK-DTE = #PARM-CHECK-DTE-N
            {
                //* *Y2NCNH
                pnd_Packed_Variables_Pnd_Recs_Ctr_Eq_Dt.nadd(1);                                                                                                          //Natural: ADD 1 TO #RECS-CTR-EQ-DT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Logical_Variables_Pnd_Bypass.setValue(true);                                                                                                          //Natural: ASSIGN #BYPASS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1274 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Read_Image_Recs_Rtn() throws Exception                                                                                                               //Natural: READ-IMAGE-RECS-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cpr_Bfre_Key_Pnd_Bfre_Imge_Id.setValue("1");                                                                                                                  //Natural: MOVE '1' TO #CPR-BFRE-KEY.#BFRE-IMGE-ID
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr);                                                                                      //Natural: MOVE #SAVE-TRANS-PPCN-NBR TO #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR
        pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                    //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE
        //* *Y2NCNH
        pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte.setValue(pnd_Current_First_Trans_Date);                                                                                        //Natural: MOVE #CURRENT-FIRST-TRANS-DATE TO #CPR-BFRE-KEY.#LST-TRANS-DTE
        //*  IF #SKIP-SW-1 NE 'G'
                                                                                                                                                                          //Natural: PERFORM GET-CPR-1
        sub_Get_Cpr_1();
        if (condition(Global.isEscape())) {return;}
        //*  END-IF
        pnd_Cntrct_Bfre_Key_Pnd_Bfre_Imge_Id.setValue("1");                                                                                                               //Natural: MOVE '1' TO #CNTRCT-BFRE-KEY.#BFRE-IMGE-ID
        pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr);                                                                                        //Natural: MOVE #SAVE-TRANS-PPCN-NBR TO #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR
        //* *Y2NCNH
        pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte.setValue(pnd_Current_First_Trans_Date);                                                                                     //Natural: MOVE #CURRENT-FIRST-TRANS-DATE TO #CNTRCT-BFRE-KEY.#LST-TRANS-DTE
        //*  IF #SKIP-SW-2 NE 'G'
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-2
        sub_Get_Cntrct_2();
        if (condition(Global.isEscape())) {return;}
        //*  END-IF
        pnd_Cpr_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                                  //Natural: MOVE '2' TO #CPR-AFTR-KEY.#AFTR-IMGE-ID
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr);                                                                                      //Natural: MOVE #SAVE-TRANS-PPCN-NBR TO #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR
        pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                    //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE
        //*  MOVE #NEXT-FIRST-TRANS-DATE    TO #CPR-AFTR-KEY.#INVRSE-TRANS-DTE
        //* *Y2NCNH
        pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Next_Invrse_Date);                                                                                             //Natural: MOVE #NEXT-INVRSE-DATE TO #CPR-AFTR-KEY.#INVRSE-TRANS-DTE
        //*  IF #SKIP-SW-3 NE 'G'
                                                                                                                                                                          //Natural: PERFORM GET-CPR-3
        sub_Get_Cpr_3();
        if (condition(Global.isEscape())) {return;}
        //*  END-IF
        //*   WRITE '=' #SAVE-TRANS-PPCN-NBR
        pnd_Cntrct_Aftr_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                               //Natural: MOVE '2' TO #CNTRCT-AFTR-KEY.#AFTR-IMGE-ID
        pnd_Cntrct_Aftr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr);                                                                                        //Natural: MOVE #SAVE-TRANS-PPCN-NBR TO #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR
        //*  MOVE #NEXT-FIRST-TRANS-DATE    TO #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE
        //* *Y2NCNH
        pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte.setValue(pnd_Next_Invrse_Date);                                                                                          //Natural: MOVE #NEXT-INVRSE-DATE TO #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE
        //*  IF #SKIP-SW-4 NE 'G'
        pnd_After_Image.reset();                                                                                                                                          //Natural: RESET #AFTER-IMAGE
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-4
        sub_Get_Cntrct_4();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_After_Image.equals(" ")))                                                                                                                       //Natural: IF #AFTER-IMAGE = ' '
        {
                                                                                                                                                                          //Natural: PERFORM #READ-CONTRACT
            sub_Pnd_Read_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-IF
    }
    private void sub_Pnd_Read_Contract() throws Exception                                                                                                                 //Natural: #READ-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct_View.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) IAA-CNTRCT-VIEW BY CNTRCT-PPCN-NBR = #SAVE-TRANS-PPCN-NBR
        (
        "RQ",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_Save_Trans_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        RQ:
        while (condition(vw_iaa_Cntrct_View.readNextRow("RQ")))
        {
            //* *Y2NCNH
            pnd_Save_Cps_Npd_First_Pay_Due_Dte.setValue(iaa_Cntrct_View_Cntrct_First_Pymnt_Due_Dte);                                                                      //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE TO #SAVE-CPS-NPD-FIRST-PAY-DUE-DTE
            //*          WRITE '=' IAA-CNTRCT-VIEW.CNTRCT-FIRST-PYMNT-PD-DTE
            //*               '=' IAA-CNTRCT-VIEW.CNTRCT-PPCN-NBR
            //* *Y2NCNH
            pnd_Save_Cps_Npd_First_Pay_Pd_Dte.setValue(iaa_Cntrct_View_Cntrct_First_Pymnt_Pd_Dte);                                                                        //Natural: MOVE IAA-CNTRCT-VIEW.CNTRCT-FIRST-PYMNT-PD-DTE TO #SAVE-CPS-NPD-FIRST-PAY-PD-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr_1() throws Exception                                                                                                                         //Natural: GET-CPR-1
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *Y2NCNH
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ IAA-CPR-TRANS BY CPR-BFRE-KEY STARTING FROM #CPR-BFRE-KEY
        (
        "R1",
        new Wc[] { new Wc("CPR_BFRE_KEY", ">=", pnd_Cpr_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CPR_BFRE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("R1")))
        {
            if (condition(pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Save_Trans_Ppcn_Nbr) || pnd_Cpr_Bfre_Key_Pnd_Cntrct_Part_Payee_Cde.notEquals(pnd_Save_Trans_Payee_Cde))) //Natural: IF #CPR-BFRE-KEY.#CNTRCT-PART-PPCN-NBR NE #SAVE-TRANS-PPCN-NBR OR #CPR-BFRE-KEY.#CNTRCT-PART-PAYEE-CDE NE #SAVE-TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Skip_Sw_1.setValue("N");                                                                                                                                  //Natural: MOVE 'N' TO #SKIP-SW-1
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT (T).
            if (condition(pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte.less(pnd_Current_First_Trans_Date)))                                                                         //Natural: REJECT IF #CPR-BFRE-KEY.#LST-TRANS-DTE LT #CURRENT-FIRST-TRANS-DATE
            {
                continue;
            }
            //* *Y2NCNH
            if (condition(pnd_Cpr_Bfre_Key_Pnd_Lst_Trans_Dte.equals(pnd_Current_First_Trans_Date)))                                                                       //Natural: IF #CPR-BFRE-KEY.#LST-TRANS-DTE EQ #CURRENT-FIRST-TRANS-DATE
            {
                pnd_Skip_Sw_1.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #SKIP-SW-1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Skip_Sw_1.equals("N")))                                                                                                                     //Natural: IF #SKIP-SW-1 = 'N'
            {
                pnd_Skip_Sw_1.setValue("G");                                                                                                                              //Natural: MOVE 'G' TO #SKIP-SW-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Save_Cpr_Bfre_Ssn.setValue(iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr);                                                                                             //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR TO #SAVE-CPR-BFRE-SSN
            pnd_Save_Cpr_Bfre_Ctznshp.setValue(iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde);                                                                                        //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-CTZNSHP-CDE TO #SAVE-CPR-BFRE-CTZNSHP
            pnd_Save_Cntrct_Comp_Cd.getValue("*").setValue(iaa_Cpr_Trans_Cntrct_Company_Cd.getValue("*"));                                                                //Natural: MOVE IAA-CPR-TRANS.CNTRCT-COMPANY-CD ( * ) TO #SAVE-CNTRCT-COMP-CD ( * )
            pnd_Save_Cntrct_Bfre_Per_Ivc_Amt.getValue("*").setValue(iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue("*"));                                                      //Natural: MOVE IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( * ) TO #SAVE-CNTRCT-BFRE-PER-IVC-AMT ( * )
            pnd_Save_Prtcpnt_Bfre_Rsdncy_Cde.setValue(iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde);                                                                                  //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-RSDNCY-CDE TO #SAVE-PRTCPNT-BFRE-RSDNCY-CDE
            if (condition(pnd_Skip_Sw_1.equals("G") || pnd_Skip_Sw_1.equals("Y")))                                                                                        //Natural: IF #SKIP-SW-1 = 'G' OR #SKIP-SW-1 = 'Y'
            {
                //*    WRITE 'FIRST RTN CHCK LAST-TRANS-DT PARM-DATE==>'
                //*      LST-TRANS-DTE(EM=YYYYMMDDHHIISST)
                //*      #CURRENT-FIRST-TRANS-DATE(EM=YYYYMMDDHHIISST)
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT (*)
            //*    TO #SAVE-CNTRCT-DISP-PER-IVC-AMT (*)
            //*  ESCAPE BOTTOM
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cntrct_2() throws Exception                                                                                                                      //Natural: GET-CNTRCT-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *Y2NCNH
        vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                             //Natural: READ IAA-CNTRCT-TRANS BY CNTRCT-BFRE-KEY STARTING FROM #CNTRCT-BFRE-KEY
        (
        "R2",
        new Wc[] { new Wc("CNTRCT_BFRE_KEY", ">=", pnd_Cntrct_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNTRCT_BFRE_KEY", "ASC") }
        );
        R2:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("R2")))
        {
            if (condition(pnd_Cntrct_Bfre_Key_Pnd_Cntrct_Ppcn_Nbr.notEquals(pnd_Save_Trans_Ppcn_Nbr)))                                                                    //Natural: IF #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR NE #SAVE-TRANS-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Skip_Sw_2.setValue("N");                                                                                                                                  //Natural: MOVE 'N' TO #SKIP-SW-2
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT (T).
            if (condition(pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte.less(pnd_Current_First_Trans_Date)))                                                                      //Natural: REJECT IF #CNTRCT-BFRE-KEY.#LST-TRANS-DTE LT #CURRENT-FIRST-TRANS-DATE
            {
                continue;
            }
            //* *Y2NCNH
            if (condition(pnd_Cntrct_Bfre_Key_Pnd_Lst_Trans_Dte.equals(pnd_Current_First_Trans_Date)))                                                                    //Natural: IF #CNTRCT-BFRE-KEY.#LST-TRANS-DTE EQ #CURRENT-FIRST-TRANS-DATE
            {
                pnd_Skip_Sw_2.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #SKIP-SW-2
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Skip_Sw_2.equals("N")))                                                                                                                     //Natural: IF #SKIP-SW-2 = 'N'
            {
                pnd_Skip_Sw_2.setValue("G");                                                                                                                              //Natural: MOVE 'G' TO #SKIP-SW-2
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE
            //*    ' CNTRCT-1ST-DOB-B = ' IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE
            //*    ' CNTRCT-2ND-DOB-B = ' IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE
            //*    ' CNTRCT-1ST-DOD-B = ' IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    ' CNTRCT-2ND-DOD-B = ' IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE
            //* *Y2NCNH
            pnd_Save_Cntrct_Bfre_F_Dob.setValue(iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte);                                                                              //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE TO #SAVE-CNTRCT-BFRE-F-DOB
            //* *Y2NCNH
            pnd_Save_Cntrct_Bfre_S_Dob.setValue(iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte);                                                                               //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE TO #SAVE-CNTRCT-BFRE-S-DOB
            //* *Y2NCNH
            pnd_Save_Cntrct_Bfre_F_Dod.setValue(iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte);                                                                              //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE TO #SAVE-CNTRCT-BFRE-F-DOD
            //* *Y2NCNH
            pnd_Save_Cntrct_Bfre_S_Dod.setValue(iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte);                                                                               //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE TO #SAVE-CNTRCT-BFRE-S-DOD
            if (condition(pnd_Skip_Sw_2.equals("Y") || pnd_Skip_Sw_2.equals("G")))                                                                                        //Natural: IF #SKIP-SW-2 = 'Y' OR #SKIP-SW-2 = 'G'
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-1ST-DOB-B = (R2) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-2ND-DOB-B = (R2) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOD
            //*  WRITE ' CNTRCT-1ST-DOD-B = (R2) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOD
            //*  WRITE ' PPCN-NBR = (R2) '         #CNTRCT-BFRE-KEY.#CNTRCT-PPCN-NBR
            //*  WRITE ' CNTRCT-2ND-DOD-B = (R2) ' #SAVE-CNTRCT-DISP-DOB
            //*    ESCAPE BOTTOM
            //*  R2.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr_3() throws Exception                                                                                                                         //Natural: GET-CPR-3
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *Y2NCNH
        vw_iaa_Cpr_Trans.startDatabaseRead                                                                                                                                //Natural: READ IAA-CPR-TRANS BY CPR-AFTR-KEY STARTING FROM #CPR-AFTR-KEY
        (
        "R3",
        new Wc[] { new Wc("CPR_AFTR_KEY", ">=", pnd_Cpr_Aftr_Key, WcType.BY) },
        new Oc[] { new Oc("CPR_AFTR_KEY", "ASC") }
        );
        R3:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("R3")))
        {
            if (condition(pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Save_Trans_Ppcn_Nbr) || pnd_Cpr_Aftr_Key_Pnd_Cntrct_Part_Payee_Cde.notEquals(pnd_Save_Trans_Payee_Cde))) //Natural: IF #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR NE #SAVE-TRANS-PPCN-NBR OR #CPR-AFTR-KEY.#CNTRCT-PART-PAYEE-CDE NE #SAVE-TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Skip_Sw_3.setValue("N");                                                                                                                                  //Natural: MOVE 'N' TO #SKIP-SW-3
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT.
            if (condition(pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte.less(pnd_Next_Invrse_Date)))                                                                              //Natural: REJECT IF #CPR-AFTR-KEY.#INVRSE-TRANS-DTE LT #NEXT-INVRSE-DATE
            {
                continue;
            }
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT.
            if (condition(pnd_Cpr_Aftr_Key_Pnd_Invrse_Trans_Dte.equals(pnd_Next_Invrse_Date)))                                                                            //Natural: IF #CPR-AFTR-KEY.#INVRSE-TRANS-DTE EQ #NEXT-INVRSE-DATE
            {
                pnd_Skip_Sw_3.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #SKIP-SW-3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Skip_Sw_3.equals("N")))                                                                                                                     //Natural: IF #SKIP-SW-3 = 'N'
            {
                pnd_Skip_Sw_3.setValue("G");                                                                                                                              //Natural: MOVE 'G' TO #SKIP-SW-3
            }                                                                                                                                                             //Natural: END-IF
            pnd_Save_Cpr_Aftr_Ssn.setValue(iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr);                                                                                             //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR TO #SAVE-CPR-AFTR-SSN
            pnd_Save_Cpr_Aftr_Ctznshp.setValue(iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde);                                                                                        //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-CTZNSHP-CDE TO #SAVE-CPR-AFTR-CTZNSHP
            pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue("*").setValue(iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt.getValue("*"));                                                      //Natural: MOVE IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT ( * ) TO #SAVE-CNTRCT-AFTR-PER-IVC-AMT ( * )
            pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue("*").setValue(iaa_Cpr_Trans_Cntrct_Ivc_Amt.getValue("*"));                                                              //Natural: MOVE IAA-CPR-TRANS.CNTRCT-IVC-AMT ( * ) TO #SAVE-CNTRCT-AFTR-IVC-AMT ( * )
            pnd_Save_Cntrct_Aftr_Rtb_Amt.getValue("*").setValue(iaa_Cpr_Trans_Cntrct_Rtb_Amt.getValue("*"));                                                              //Natural: MOVE IAA-CPR-TRANS.CNTRCT-RTB-AMT ( * ) TO #SAVE-CNTRCT-AFTR-RTB-AMT ( * )
            //*    WRITE / 'READ CONTRACT' '=' #SAVE-CNTRCT-AFTR-RTB-AMT (*)
            pnd_Cntrct_Mode_Tot.setValue(iaa_Cpr_Trans_Cntrct_Mode_Ind);                                                                                                  //Natural: MOVE IAA-CPR-TRANS.CNTRCT-MODE-IND TO #CNTRCT-MODE-TOT
            pnd_Save_Prtcpnt_Tax_Id_Nbr.setValue(iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr);                                                                                       //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-TAX-ID-NBR TO #SAVE-PRTCPNT-TAX-ID-NBR
            pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde.setValue(iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde);                                                                                  //Natural: MOVE IAA-CPR-TRANS.PRTCPNT-RSDNCY-CDE TO #SAVE-PRTCPNT-AFTR-RSDNCY-CDE
            if (condition(pnd_Skip_Sw_3.equals("Y") || pnd_Skip_Sw_3.equals("G")))                                                                                        //Natural: IF #SKIP-SW-3 = 'Y' OR #SKIP-SW-3 = 'G'
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE IAA-CPR-TRANS.CNTRCT-PER-IVC-AMT (*)
            //*    TO #SAVE-CNTRCT-DISP-PER-IVC-AMT (*)
            //*  WRITE ' CNTRCT-PER-IVC-AMT = (R3) ' #SAVE-CNTRCT-DISP-PER-IVC-AMT (*)
            //*  MOVE IAA-CPR-TRANS.CNTRCT-IVC-AMT (*)
            //*    TO #SAVE-CNTRCT-DISP-IVC-AMT (*)
            //*  WRITE ' PPCN-NBR = (R3) '       #CPR-AFTR-KEY.#CNTRCT-PART-PPCN-NBR
            //*  WRITE ' CNTRCT-IVC-AMT = (R3) ' #SAVE-CNTRCT-DISP-IVC-AMT (*)
            //*  ESCAPE BOTTOM
            //*  R3.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cntrct_4() throws Exception                                                                                                                      //Natural: GET-CNTRCT-4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*     WRITE '=' #SAVE-TRANS-PPCN-NBR 'CONTRACT 4'
        //*    WRITE '=' #CNTRCT-AFTR-KEY
        //*    WRITE 'ENTERING READ'
        //* *Y2NCNH
        vw_iaa_Cntrct_Trans.startDatabaseRead                                                                                                                             //Natural: READ IAA-CNTRCT-TRANS BY CNTRCT-AFTR-KEY STARTING FROM #CNTRCT-AFTR-KEY
        (
        "R4",
        new Wc[] { new Wc("CNTRCT_AFTR_KEY", ">=", pnd_Cntrct_Aftr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_AFTR_KEY", "ASC") }
        );
        R4:
        while (condition(vw_iaa_Cntrct_Trans.readNextRow("R4")))
        {
            //*  WRITE '=' #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR '=' #SAVE-TRANS-PPCN-NBR
            //*      '=' IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR
            //*  IF #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR NE #SAVE-TRANS-PPCN-NBR
            if (condition(iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr.notEquals(pnd_Save_Trans_Ppcn_Nbr)))                                                                           //Natural: IF IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR NE #SAVE-TRANS-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*   WRITE 'AFTER IF '
            pnd_After_Image.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #AFTER-IMAGE
            pnd_Skip_Sw_4.setValue("N");                                                                                                                                  //Natural: MOVE 'N' TO #SKIP-SW-4
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT.
            if (condition(pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte.less(pnd_Next_Invrse_Date)))                                                                           //Natural: REJECT #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE LT #NEXT-INVRSE-DATE
            {
                continue;
            }
            //*   WRITE 'AFTER REJECT'
            //*  WRITE '=' #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR '=' #SAVE-TRANS-PPCN-NBR
            //* *Y2NCNH - BOTH FIELDS SAME FORMAT.
            if (condition(pnd_Cntrct_Aftr_Key_Pnd_Invrse_Trans_Dte.equals(pnd_Next_Invrse_Date)))                                                                         //Natural: IF #CNTRCT-AFTR-KEY.#INVRSE-TRANS-DTE EQ #NEXT-INVRSE-DATE
            {
                pnd_Skip_Sw_4.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #SKIP-SW-4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Skip_Sw_4.equals("N")))                                                                                                                     //Natural: IF #SKIP-SW-4 = 'N'
            {
                pnd_Skip_Sw_4.setValue("G");                                                                                                                              //Natural: MOVE 'G' TO #SKIP-SW-4
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE
            //*    ' CNTRCT-1ST-DOB-A = ' IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE
            //*    ' CNTRCT-2ND-DOB-A = ' IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE
            //*    ' CNTRCT-1ST-DOD-A = ' IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    ' CNTRCT-2ND-DOD-A = ' IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE
            //* *Y2NCNH
            pnd_Save_Cps_Npd_First_Pay_Due_Dte.setValue(iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte);                                                                     //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-DUE-DTE TO #SAVE-CPS-NPD-FIRST-PAY-DUE-DTE
            //*     WRITE '=' IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-PD-DTE
            //*           '=' IAA-CNTRCT-TRANS.CNTRCT-PPCN-NBR
            //* *Y2NCNH
            pnd_Save_Cps_Npd_First_Pay_Pd_Dte.setValue(iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte);                                                                       //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-PYMNT-PD-DTE TO #SAVE-CPS-NPD-FIRST-PAY-PD-DTE
            //* *Y2NCNH
            pnd_Save_Cntrct_Aftr_F_Dob.setValue(iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte);                                                                              //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE TO #SAVE-CNTRCT-AFTR-F-DOB
            //* *Y2NCNH
            pnd_Save_Cntrct_Aftr_S_Dob.setValue(iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte);                                                                               //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE TO #SAVE-CNTRCT-AFTR-S-DOB
            //* *Y2NCNH
            pnd_Save_Cntrct_Aftr_F_Dod.setValue(iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte);                                                                              //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE TO #SAVE-CNTRCT-AFTR-F-DOD
            //* *Y2NCNH
            pnd_Save_Cntrct_Aftr_S_Dod.setValue(iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte);                                                                               //Natural: MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE TO #SAVE-CNTRCT-AFTR-S-DOD
            //*  WRITE 'BEFORE IF'
            //*  WRITE '=' #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR '=' #SAVE-TRANS-PPCN-NBR
            if (condition(pnd_Skip_Sw_4.equals("Y") || pnd_Skip_Sw_4.equals("G")))                                                                                        //Natural: IF #SKIP-SW-4 = 'Y' OR #SKIP-SW-4 = 'G'
            {
                if (true) break R4;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R4. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE 'AFTER  IF'
            //*  WRITE '=' #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR '=' #SAVE-TRANS-PPCN-NBR
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOB-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-1ST-DOB-A = (R4) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOB-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-2ND-DOB-A = (R4) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOD
            //*  WRITE ' CNTRCT-1ST-DOD-A = (R4) ' #SAVE-CNTRCT-DISP-DOB
            //*  MOVE IAA-CNTRCT-TRANS.CNTRCT-SCND-ANNT-DOD-DTE
            //*    TO #SAVE-CNTRCT-DISP-DOD
            //*  WRITE ' CNTRCT-2ND-DOD-A = (R4) ' #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' PPCN-NBR = (R4) '         #CNTRCT-AFTR-KEY.#CNTRCT-PPCN-NBR
            //*  ESCAPE BOTTOM
            //*  R4.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_Cps_Rec_050() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-050
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        if (condition(pnd_Save_Cpr_Bfre_Ssn.notEquals(pnd_Save_Cpr_Aftr_Ssn)))                                                                                            //Natural: IF #SAVE-CPR-BFRE-SSN NOT = #SAVE-CPR-AFTR-SSN
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (SSN) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            //*  MOVE #SAVE-PRTCPNT-TAX-ID-NBR      TO #CPS-NPD-TAX-ID
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Cpr_Bfre_Ssn);                                                                                        //Natural: MOVE #SAVE-CPR-BFRE-SSN TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                              //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-NEW-TAX-ID
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("SSN");                                                                                                     //Natural: MOVE 'SSN' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR1
            pnd_Packed_Variables_Pnd_Recs_Ssn.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECS-SSN
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        //* *Y2NCNH
        if (condition(pnd_Save_Cntrct_Bfre_F_Dob.notEquals(pnd_Save_Cntrct_Aftr_F_Dob)))                                                                                  //Natural: IF #SAVE-CNTRCT-BFRE-F-DOB NOT = #SAVE-CNTRCT-AFTR-F-DOB
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (DB1) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_F.setValue(pnd_Save_Cntrct_Aftr_F_Dob);                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-F-DOB TO #CPS-NPD-NEW-DOB-F
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("DB1");                                                                                                     //Natural: MOVE 'DB1' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Save_Cntrct_Disp_Dob.setValue(pnd_Save_Cntrct_Aftr_F_Dob);                                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-F-DOB TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-1ST-DOB-A = (DB1) ' #SAVE-CNTRCT-DISP-DOB
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR1
            //* *Y2NCNH
            pnd_Packed_Variables_Pnd_Recs_Dob_F.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECS-DOB-F
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        //* *Y2NCNH
        if (condition(pnd_Save_Cntrct_Bfre_S_Dob.notEquals(pnd_Save_Cntrct_Aftr_S_Dob)))                                                                                  //Natural: IF #SAVE-CNTRCT-BFRE-S-DOB NOT = #SAVE-CNTRCT-AFTR-S-DOB
        {
            //* *Y2NCNH
            getReports().write(0, "=",pnd_Save_Cntrct_Bfre_S_Dob,"=",pnd_Save_Cntrct_Aftr_S_Dob);                                                                         //Natural: WRITE '=' #SAVE-CNTRCT-BFRE-S-DOB '=' #SAVE-CNTRCT-AFTR-S-DOB
            if (Global.isEscape()) return;
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            getReports().write(0, " PPCN-NBR = (DB2) ",pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr);                                                                        //Natural: WRITE ' PPCN-NBR = (DB2) ' #CPS-NPD-CNTRCT-NBR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dob_S.setValue(pnd_Save_Cntrct_Aftr_S_Dob);                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-S-DOB TO #CPS-NPD-NEW-DOB-S
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("DB2");                                                                                                     //Natural: MOVE 'DB2' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Save_Cntrct_Disp_Dob.setValue(pnd_Save_Cntrct_Aftr_S_Dob);                                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-S-DOB TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-2ND-DOB-A = (DB2) ' #SAVE-CNTRCT-DISP-DOB
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR1
            //* *Y2NCNH
            pnd_Packed_Variables_Pnd_Recs_Dob_S.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECS-DOB-S
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        if (condition(pnd_Save_Cpr_Bfre_Ctznshp.notEquals(pnd_Save_Cpr_Aftr_Ctznshp)))                                                                                    //Natural: IF #SAVE-CPR-BFRE-CTZNSHP NOT = #SAVE-CPR-AFTR-CTZNSHP
        {
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (CIT) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Ctzn_Cde.setValue(pnd_Save_Cpr_Aftr_Ctznshp);                                                                              //Natural: MOVE #SAVE-CPR-AFTR-CTZNSHP TO #CPS-NPD-NEW-CTZN-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("CIT");                                                                                                     //Natural: MOVE 'CIT' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr1.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR1
            pnd_Packed_Variables_Pnd_Recs_Cit.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECS-CIT
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Cps_Rec_066() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-066
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        //* *Y2NCNH
        if (condition(pnd_Save_Cntrct_Bfre_F_Dod.notEquals(pnd_Save_Cntrct_Aftr_F_Dod)))                                                                                  //Natural: IF #SAVE-CNTRCT-BFRE-F-DOD NOT = #SAVE-CNTRCT-AFTR-F-DOD
        {
            FOR05:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (DD1) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_F.setValue(pnd_Save_Cntrct_Aftr_F_Dod);                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-F-DOD TO #CPS-NPD-NEW-DOD-F
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("DD1");                                                                                                     //Natural: MOVE 'DD1' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Save_Cntrct_Disp_Dob.setValue(pnd_Save_Cntrct_Aftr_F_Dod);                                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-F-DOD TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-1ST-DOD-A = (DD1) ' #SAVE-CNTRCT-DISP-DOB
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR2
            //* *Y2NCNH
            pnd_Packed_Variables_Pnd_Recs_Dod_F.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECS-DOD-F
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        //* *Y2NCNH
        if (condition(pnd_Save_Cntrct_Bfre_S_Dod.notEquals(pnd_Save_Cntrct_Aftr_S_Dod)))                                                                                  //Natural: IF #SAVE-CNTRCT-BFRE-S-DOD NOT = #SAVE-CNTRCT-AFTR-S-DOD
        {
            FOR06:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (DD2) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            //*  MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Dod_S.setValue(pnd_Save_Cntrct_Aftr_S_Dod);                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-S-DOD TO #CPS-NPD-NEW-DOD-S
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("DD2");                                                                                                     //Natural: MOVE 'DD2' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Save_Cntrct_Disp_Dob.setValue(pnd_Save_Cntrct_Aftr_S_Dod);                                                                                                //Natural: MOVE #SAVE-CNTRCT-AFTR-S-DOD TO #SAVE-CNTRCT-DISP-DOB
            //*  WRITE ' CNTRCT-2ND-DOD-A = (DD2) ' #SAVE-CNTRCT-DISP-DOB
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr2.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR2
            //* *Y2NCNH
            pnd_Packed_Variables_Pnd_Recs_Dod_S.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECS-DOD-S
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Cps_Rec_106() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-106
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        //*  WRITE '111111111111000000000000066666666' '=' #TRN-DTE-MM-N '=' #MM
        if (condition(pnd_Save_Prtcpnt_Bfre_Rsdncy_Cde.notEquals(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde) && (pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm.less(pnd_Period_End_Dte_A_Pnd_Mm)  //Natural: IF #SAVE-PRTCPNT-BFRE-RSDNCY-CDE NOT = #SAVE-PRTCPNT-AFTR-RSDNCY-CDE AND ( #TRANS-EFF-MM < #MM OR ( #TRANS-EFF-MM = #MM AND #TRANS-EFF-DD-A = '01' ) )
            || (pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Mm.equals(pnd_Period_End_Dte_A_Pnd_Mm) && pnd_Trans_Effctve_Dte_Pnd_Trans_Eff_Dd_A.equals("01")))))
        {
            FOR07:                                                                                                                                                        //Natural: FOR #I 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                             //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                         //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                     //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                         //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
            //*  WRITE ' PPCN-NBR = (RES) ' #CPS-NPD-CNTRCT-NBR
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                  //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                  //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                 //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                        //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("RES");                                                                                                     //Natural: MOVE 'RES' TO #CPS-NPD-MAINT-CDE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                        //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
            //* *Y2NCNH
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                                  //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship.setValue(pnd_Save_Cpr_Bfre_Ctznshp);                                                                               //Natural: MOVE #SAVE-CPR-BFRE-CTZNSHP TO #CPS-NPD-CITIZENSHIP
            getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                           //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
            sub_Pnd_Write_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Packed_Variables_Pnd_Recs_Processed_Ctr3.nadd(1);                                                                                                         //Natural: ADD 1 TO #RECS-PROCESSED-CTR3
            pnd_Packed_Variables_Pnd_Recs_Res.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECS-RES
            pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-CPS-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Cps_Rec_724() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-724
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD #PER-IVC-HOLD
        pnd_Per_Ivc_Hold.reset();
        FOR08:                                                                                                                                                            //Natural: FOR #J 1 TO 5
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
        {
            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_J).notEquals(" ")))                                                                                        //Natural: IF #SAVE-CNTRCT-COMP-CD ( #J ) NOT = ' '
            {
                //*   IF #SAVE-CNTRCT-BFRE-PER-IVC-AMT (#J) NOT =
                //*       #SAVE-CNTRCT-AFTR-PER-IVC-AMT (#J)
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt.setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));                                                    //Natural: MOVE #SAVE-CNTRCT-AFTR-PER-IVC-AMT ( #J ) TO #CPS-NPD-PER-IVC-AMT
                //*  WRITE '='  #SAVE-CNTRCT-AFTR-PER-IVC-AMT (#J) '=' #CPS-NPD-PER-IVC-AMT
                pnd_Save_Cntrct_Disp_Per_Ivc_Amt.getValue(pnd_J).setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));                                              //Natural: MOVE #SAVE-CNTRCT-AFTR-PER-IVC-AMT ( #J ) TO #SAVE-CNTRCT-DISP-PER-IVC-AMT ( #J ) #PER-IVC-HOLD
                pnd_Per_Ivc_Hold.setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));
                FOR09:                                                                                                                                                    //Natural: FOR #I 1 TO 5
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                         //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                     //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                           //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                            {
                                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                 //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde.setValue("2");                                                                                                 //Natural: MOVE '2' TO #CPS-NPD-PER-IVC-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                     //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
                //*    WRITE ' PPCN-NBR = (IVC) ' #CPS-NPD-CNTRCT-NBR
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                              //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                              //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
                //*    MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                            //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                             //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                    //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("IVC");                                                                                                 //Natural: MOVE 'IVC' TO #CPS-NPD-MAINT-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                    //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                              //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship.setValue(pnd_Save_Cpr_Bfre_Ctznshp);                                                                           //Natural: MOVE #SAVE-CPR-BFRE-CTZNSHP TO #CPS-NPD-CITIZENSHIP
                                                                                                                                                                          //Natural: PERFORM #YTD-IVC-PARA
                sub_Pnd_Ytd_Ivc_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt.setValue(pnd_Ytd_Tot);                                                                                         //Natural: MOVE #YTD-TOT TO #CPS-NPD-YTD-IVC-AMT
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt.setValue(pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J));                                                        //Natural: ASSIGN #CPS-NPD-TOT-IVC-AMT := #SAVE-CNTRCT-AFTR-IVC-AMT ( #J )
                //*    WRITE '!!!!!!!!!!! 724 !!!!!!!!!!!!!!!!!'
                //*     WRITE '=' #PARM-YEAR-END '=' #FIRST-PAY-DUE-YY '=' #YY
                //*     WRITE '='  #CPS-NPD-TOT-IVC-AMT '=' #SAVE-CNTRCT-AFTR-IVC-AMT(#J)
                //*                                  '='     #CPS-NPD-RTB-IVC-AMT
                pnd_Ivc_Rtb_Amt.reset();                                                                                                                                  //Natural: RESET #IVC-RTB-AMT
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy) && pnd_Save_Cntrct_Aftr_Rtb_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF #FIRST-PAY-YY = #YY AND #SAVE-CNTRCT-AFTR-RTB-AMT ( #J ) > 0
                {
                    pnd_Ivc_Rtb_Amt.compute(new ComputeParameters(true, pnd_Ivc_Rtb_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).multiply(new DbsDecimal("0.1")));  //Natural: MULTIPLY ROUNDED #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) BY 0.1 GIVING #IVC-RTB-AMT
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt.setValue(pnd_Ivc_Rtb_Amt);                                                                                 //Natural: MOVE #IVC-RTB-AMT TO #CPS-NPD-RTB-IVC-AMT
                }                                                                                                                                                         //Natural: END-IF
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy)))                                               //Natural: IF #FIRST-PAY-DUE-YY = #YY
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt.compute(new ComputeParameters(false, pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).subtract(pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt)); //Natural: COMPUTE #CPS-NPD-TOT-IVC-AMT = #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) - #CPS-NPD-RTB-IVC-AMT
                    //*           WRITE 'YES' '=' #CPS-NPD-TOT-IVC-AMT
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                       //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
                sub_Pnd_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECS-PROCESSED-CTR4
                pnd_Packed_Variables_Pnd_Recs_Ivc.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-IVC
                pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECS-CPS-CTR
                pnd_Cps_Npd_Record.reset();                                                                                                                               //Natural: RESET #CPS-NPD-RECORD
                pnd_Ivc_Rtb_Amt.reset();                                                                                                                                  //Natural: RESET #IVC-RTB-AMT
                //*  WRITE '=' #FIRST-PAY-YY  '=' #YY '=' #SAVE-CNTRCT-AFTR-RTB-AMT (*)
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy) && pnd_Save_Cntrct_Aftr_Rtb_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF #FIRST-PAY-YY = #YY AND #SAVE-CNTRCT-AFTR-RTB-AMT ( #J ) > 0
                {
                    //*     FOR #K 1 TO 5
                    //*       IF #SAVE-CNTRCT-AFTR-IVC-AMT (#K) GT 0
                    pnd_Ivc_Rtb_Amt.compute(new ComputeParameters(true, pnd_Ivc_Rtb_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).multiply(new DbsDecimal("0.1")));  //Natural: MULTIPLY ROUNDED #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) BY 0.1 GIVING #IVC-RTB-AMT
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt.setValue(pnd_Ivc_Rtb_Amt);                                                                                 //Natural: MOVE #IVC-RTB-AMT TO #CPS-NPD-RTB-IVC-AMT
                    //*       END-IF
                    //*      END-FOR
                    //*    #CPS-NPD-TOT-IVC-AMT := #SAVE-CNTRCT-AFTR-IVC-AMT(#J)
                    //* *Y2NCNH
                    if (condition(pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy)))                                           //Natural: IF #FIRST-PAY-DUE-YY = #YY
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt.compute(new ComputeParameters(false, pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).subtract(pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt)); //Natural: COMPUTE #CPS-NPD-TOT-IVC-AMT = #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) - #CPS-NPD-RTB-IVC-AMT
                        //*           WRITE 'YES' '=' #CPS-NPD-TOT-IVC-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    FOR10:                                                                                                                                                //Natural: FOR #K 1 TO 5
                    for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(5)); pnd_K.nadd(1))
                    {
                        pnd_Dsp_Amt.getValue(pnd_K).setValue(pnd_Ivc_Amt.getValue(pnd_K));                                                                                //Natural: MOVE #IVC-AMT ( #K ) TO #DSP-AMT ( #K )
                        //*      WRITE ' CNTRCT-IVC-AMT (RIC) ' #DSP-AMT (#K)
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR11:                                                                                                                                                //Natural: FOR #I 1 TO 5
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                     //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                           //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                            {
                                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                 //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                                {
                                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                             //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Cde.setValue("2");                                                                                             //Natural: MOVE '2' TO #CPS-NPD-RTB-IVC-CDE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                 //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
                    //*     WRITE ' PPCN-NBR = (RIC) ' #CPS-NPD-CNTRCT-NBR
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                          //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                          //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
                    //*     MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                        //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                         //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
                    //* *Y2NCNH
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("RIC");                                                                                             //Natural: MOVE 'RIC' TO #CPS-NPD-MAINT-CDE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
                    //*     MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship.setValue(pnd_Save_Cpr_Bfre_Ctznshp);                                                                       //Natural: MOVE #SAVE-CPR-BFRE-CTZNSHP TO #CPS-NPD-CITIZENSHIP
                    //* *Y2NCNH
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt.setValue(pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt);                                                      //Natural: MOVE #CPS-NPD-RTB-IVC-AMT TO #CPS-NPD-YTD-IVC-AMT
                    //*      MOVE #YTD-TOT TO #CPS-NPD-YTD-IVC-AMT
                    getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                   //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
                    sub_Pnd_Write_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Packed_Variables_Pnd_Recs_Processed_Ctr4.nadd(1);                                                                                                 //Natural: ADD 1 TO #RECS-PROCESSED-CTR4
                    pnd_Packed_Variables_Pnd_Recs_Ric.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECS-RIC
                    pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                        //Natural: ADD 1 TO #RECS-CPS-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Create_Cps_Rec_Naz() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-NAZ
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD #PER-IVC-HOLD
        pnd_Per_Ivc_Hold.reset();
        FOR12:                                                                                                                                                            //Natural: FOR #J 1 TO 5
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
        {
            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_J).notEquals(" ")))                                                                                        //Natural: IF #SAVE-CNTRCT-COMP-CD ( #J ) NOT = ' '
            {
                //*   IF #SAVE-CNTRCT-BFRE-PER-IVC-AMT (#J) NOT =
                //*       #SAVE-CNTRCT-AFTR-PER-IVC-AMT (#J)
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt.setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));                                                    //Natural: MOVE #SAVE-CNTRCT-AFTR-PER-IVC-AMT ( #J ) TO #CPS-NPD-PER-IVC-AMT
                //*  WRITE '='  #SAVE-CNTRCT-AFTR-PER-IVC-AMT (#J) '=' #CPS-NPD-PER-IVC-AMT
                pnd_Save_Cntrct_Disp_Per_Ivc_Amt.getValue(pnd_J).setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));                                              //Natural: MOVE #SAVE-CNTRCT-AFTR-PER-IVC-AMT ( #J ) TO #SAVE-CNTRCT-DISP-PER-IVC-AMT ( #J ) #PER-IVC-HOLD
                pnd_Per_Ivc_Hold.setValue(pnd_Save_Cntrct_Aftr_Per_Ivc_Amt.getValue(pnd_J));
                FOR13:                                                                                                                                                    //Natural: FOR #I 1 TO 5
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                         //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                               //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                        {
                            pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                     //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                           //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                            {
                                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                 //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde.setValue("2");                                                                                                 //Natural: MOVE '2' TO #CPS-NPD-PER-IVC-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                     //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
                //*    WRITE ' PPCN-NBR = (IVC) ' #CPS-NPD-CNTRCT-NBR
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                              //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                              //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
                //*    MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                            //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                             //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                    //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
                //*    MOVE 'NAZ'                         TO #CPS-NPD-MAINT-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("TIC");                                                                                                 //Natural: MOVE 'TIC' TO #CPS-NPD-MAINT-CDE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                    //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_First_Pay_Pd_Dte.setValue(pnd_Save_Cps_Npd_First_Pay_Pd_Dte);                                                              //Natural: MOVE #SAVE-CPS-NPD-FIRST-PAY-PD-DTE TO #CPS-NPD-FIRST-PAY-PD-DTE
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Citizenship.setValue(pnd_Save_Cpr_Bfre_Ctznshp);                                                                           //Natural: MOVE #SAVE-CPR-BFRE-CTZNSHP TO #CPS-NPD-CITIZENSHIP
                                                                                                                                                                          //Natural: PERFORM #YTD-IVC-PARA
                sub_Pnd_Ytd_Ivc_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt.setValue(pnd_Ytd_Tot);                                                                                         //Natural: MOVE #YTD-TOT TO #CPS-NPD-YTD-IVC-AMT
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt.setValue(pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J));                                                        //Natural: ASSIGN #CPS-NPD-TOT-IVC-AMT := #SAVE-CNTRCT-AFTR-IVC-AMT ( #J )
                //*    WRITE '!!!!!!!!!!! 724 !!!!!!!!!!!!!!!!!'
                //*     WRITE '=' #PARM-YEAR-END '=' #FIRST-PAY-DUE-YY '=' #YY
                //*     WRITE '='  #CPS-NPD-TOT-IVC-AMT '=' #SAVE-CNTRCT-AFTR-IVC-AMT(#J)
                //*                                  '='     #CPS-NPD-RTB-IVC-AMT
                pnd_Ivc_Rtb_Amt.reset();                                                                                                                                  //Natural: RESET #IVC-RTB-AMT
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy) && pnd_Save_Cntrct_Aftr_Rtb_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF #FIRST-PAY-YY = #YY AND #SAVE-CNTRCT-AFTR-RTB-AMT ( #J ) > 0
                {
                    pnd_Ivc_Rtb_Amt.compute(new ComputeParameters(true, pnd_Ivc_Rtb_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).multiply(new DbsDecimal("0.1")));  //Natural: MULTIPLY ROUNDED #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) BY 0.1 GIVING #IVC-RTB-AMT
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt.setValue(pnd_Ivc_Rtb_Amt);                                                                                 //Natural: MOVE #IVC-RTB-AMT TO #CPS-NPD-RTB-IVC-AMT
                }                                                                                                                                                         //Natural: END-IF
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy)))                                               //Natural: IF #FIRST-PAY-DUE-YY = #YY
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt.compute(new ComputeParameters(false, pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tot_Ivc_Amt), pnd_Save_Cntrct_Aftr_Ivc_Amt.getValue(pnd_J).subtract(pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt)); //Natural: COMPUTE #CPS-NPD-TOT-IVC-AMT = #SAVE-CNTRCT-AFTR-IVC-AMT ( #J ) - #CPS-NPD-RTB-IVC-AMT
                    //*           WRITE 'YES' '=' #CPS-NPD-TOT-IVC-AMT
                }                                                                                                                                                         //Natural: END-IF
                //* *Y2NCNH
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Amt.reset();                                                                                                       //Natural: RESET #CPS-NPD-PER-IVC-AMT #CPS-NPD-YTD-IVC-AMT #CPS-NPD-RTB-IVC-AMT
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Ytd_Ivc_Amt.reset();
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Rtb_Ivc_Amt.reset();
                getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                       //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
                sub_Pnd_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Packed_Variables_Pnd_Recs_Processed_Ctr6.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECS-PROCESSED-CTR6
                pnd_Packed_Variables_Pnd_Recs_Ivc.nadd(1);                                                                                                                //Natural: ADD 1 TO #RECS-IVC
                pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECS-CPS-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Create_Cps_Rec_730() throws Exception                                                                                                                //Natural: CREATE-CPS-REC-730
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cps_Npd_Record.reset();                                                                                                                                       //Natural: RESET #CPS-NPD-RECORD
        FOR14:                                                                                                                                                            //Natural: FOR #I 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("T")))                                                                                           //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'T'
            {
                pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("T");                                                                                                 //Natural: MOVE 'T' TO #CPS-NPD-PRODUCT-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("C")))                                                                                       //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'C'
                {
                    pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("C");                                                                                             //Natural: MOVE 'C' TO #CPS-NPD-PRODUCT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Save_Cntrct_Comp_Cd.getValue(pnd_I).equals("R")))                                                                                   //Natural: IF #SAVE-CNTRCT-COMP-CD ( #I ) = 'R'
                    {
                        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Product_Cde.setValue("R");                                                                                         //Natural: MOVE 'R' TO #CPS-NPD-PRODUCT-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Per_Ivc_Cde.setValue("2");                                                                                                         //Natural: MOVE '2' TO #CPS-NPD-PER-IVC-CDE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr_Pnd_Save_Trans_Ppcn_Nbr1);                                                             //Natural: MOVE #SAVE-TRANS-PPCN-NBR1 TO #CPS-NPD-CNTRCT-NBR
        //*    WRITE ' PPCN-NBR = (IVC) ' #CPS-NPD-CNTRCT-NBR
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                                      //Natural: MOVE #SAVE-TRANS-PAYEE-CDE TO #CPS-NPD-PAYEE-CDE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Tax_Id.setValue(pnd_Save_Prtcpnt_Tax_Id_Nbr);                                                                                      //Natural: MOVE #SAVE-PRTCPNT-TAX-ID-NBR TO #CPS-NPD-TAX-ID
        //*    MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE TO #CPS-NPD-SOURCE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_New_Rsdncy_Cde.setValue(pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_Pnd_Save_Prtcpnt_Aftr_Rsdncy_Cde_2_3);                                    //Natural: MOVE #SAVE-PRTCPNT-AFTR-RSDNCY-CDE-2-3 TO #CPS-NPD-NEW-RSDNCY-CDE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Pymnt_Month.setValue(pnd_Save_Trans_Check_Mm);                                                                                     //Natural: MOVE #SAVE-TRANS-CHECK-MM TO #CPS-NPD-PYMNT-MONTH
        //* *Y2NCNH
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Dte.setValue(pnd_Save_Trans_Dte);                                                                                            //Natural: MOVE #SAVE-TRANS-DTE TO #CPS-NPD-TRANS-DTE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Maint_Cde.setValue("IVC");                                                                                                         //Natural: MOVE 'IVC' TO #CPS-NPD-MAINT-CDE
        pnd_Cps_Npd_Record_Pnd_Cps_Npd_Trans_Cde.setValue(pnd_Save_Trans_Cde);                                                                                            //Natural: MOVE #SAVE-TRANS-CDE TO #CPS-NPD-TRANS-CDE
        getWorkFiles().write(1, false, pnd_Cps_Npd_Record);                                                                                                               //Natural: WRITE WORK FILE 1 #CPS-NPD-RECORD
                                                                                                                                                                          //Natural: PERFORM #WRITE-REPORT
        sub_Pnd_Write_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Packed_Variables_Pnd_Recs_Processed_Ctr5.nadd(1);                                                                                                             //Natural: ADD 1 TO #RECS-PROCESSED-CTR5
        pnd_Packed_Variables_Pnd_Recs_Ivm.nadd(1);                                                                                                                        //Natural: ADD 1 TO #RECS-IVM
        pnd_Packed_Variables_Pnd_Recs_Cps_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECS-CPS-CTR
    }
    private void sub_Pnd_Ytd_Ivc_Para() throws Exception                                                                                                                  //Natural: #YTD-IVC-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Num_Of_Pay.reset();                                                                                                                                           //Natural: RESET #NUM-OF-PAY #YTD-TOT
        pnd_Ytd_Tot.reset();
        short decideConditionsMet2189 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #CNTRCT-MODE-1;//Natural: VALUE 1
        if (condition((pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1.equals(1))))
        {
            decideConditionsMet2189++;
            pnd_Num_Of_Pay.setValue(12);                                                                                                                                  //Natural: MOVE 12 TO #NUM-OF-PAY
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1.equals(6))))
        {
            decideConditionsMet2189++;
            pnd_Num_Of_Pay.setValue(4);                                                                                                                                   //Natural: MOVE 4 TO #NUM-OF-PAY
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1.equals(7))))
        {
            decideConditionsMet2189++;
            pnd_Num_Of_Pay.setValue(2);                                                                                                                                   //Natural: MOVE 2 TO #NUM-OF-PAY
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pnd_Cntrct_Mode_Tot_Pnd_Cntrct_Mode_1.equals(8))))
        {
            decideConditionsMet2189++;
            pnd_Num_Of_Pay.setValue(1);                                                                                                                                   //Natural: MOVE 1 TO #NUM-OF-PAY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM #MODE-PARA
        sub_Pnd_Mode_Para();
        if (condition(Global.isEscape())) {return;}
        pnd_Number_Of_Payments.reset();                                                                                                                                   //Natural: RESET #NUMBER-OF-PAYMENTS
        //*   WRITE '=' #CNTRCT-MODE-TOT '=' #NUM-OF-PAY '=' #MDE-SUB
        //*   WRITE '=' #PERIOD-END-DTE-A '=' #SAVE-CPS-NPD-FIRST-PAY-DUE-DTE
        //*   WRITE  '=' #SAVE-CPS-NPD-FIRST-PAY-PD-DTE
        short decideConditionsMet2209 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-PAY-CCYY < #YYYY
        if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Ccyy.less(pnd_Period_End_Dte_A_Pnd_Yyyy)))
        {
            decideConditionsMet2209++;
            FC:                                                                                                                                                           //Natural: FOR #A = 1 TO #NUM-OF-PAY
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Pay)); pnd_A.nadd(1))
            {
                //* *Y2NCNH
                if (condition(pnd_Period_End_Dte_A_Pnd_Mm.greaterOrEqual(pnd_Year_Table.getValue(pnd_Mde_Sub,pnd_A))))                                                    //Natural: IF #MM >= #YEAR-TABLE ( #MDE-SUB,#A )
                {
                    pnd_Number_Of_Payments.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUMBER-OF-PAYMENTS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #FIRST-PAY-YY = #YY AND #FIRST-PAY-DUE-YY = #YY
        else if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy) && pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy)))
        {
            decideConditionsMet2209++;
            FC1:                                                                                                                                                          //Natural: FOR #A = 1 TO #NUM-OF-PAY
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Pay)); pnd_A.nadd(1))
            {
                //* *Y2NCNH
                if (condition(pnd_Year_Table.getValue(pnd_Mde_Sub,pnd_A).greaterOrEqual(pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Mm) && pnd_Year_Table.getValue(pnd_Mde_Sub, //Natural: IF #YEAR-TABLE ( #MDE-SUB,#A ) >= #FIRST-PAY-DUE-MM AND #YEAR-TABLE ( #MDE-SUB,#A ) <= #MM
                    pnd_A).lessOrEqual(pnd_Period_End_Dte_A_Pnd_Mm)))
                {
                    pnd_Number_Of_Payments.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUMBER-OF-PAYMENTS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #FIRST-PAY-YY = #YY AND #FIRST-PAY-DUE-YY NE #YY
        else if (condition(pnd_Save_Cps_Npd_First_Pay_Pd_Dte_Pnd_First_Pay_Yy.equals(pnd_Period_End_Dte_A_Pnd_Yy) && pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Yy.notEquals(pnd_Period_End_Dte_A_Pnd_Yy)))
        {
            decideConditionsMet2209++;
            FC2:                                                                                                                                                          //Natural: FOR #A = 1 TO #NUM-OF-PAY
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Pay)); pnd_A.nadd(1))
            {
                //* *Y2NCNH
                if (condition(pnd_Save_Cps_Npd_First_Pay_Due_Dte_Pnd_First_Pay_Due_Mm.lessOrEqual(pnd_Year_Table.getValue(pnd_Mde_Sub,pnd_A))))                           //Natural: IF #FIRST-PAY-DUE-MM <= #YEAR-TABLE ( #MDE-SUB,#A )
                {
                    pnd_Number_Of_Payments.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUMBER-OF-PAYMENTS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FC3:                                                                                                                                                          //Natural: FOR #A = 1 TO #NUM-OF-PAY
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Pay)); pnd_A.nadd(1))
            {
                //* *Y2NCNH
                if (condition(pnd_Period_End_Dte_A_Pnd_Mm.greaterOrEqual(pnd_Year_Table.getValue(pnd_Mde_Sub,pnd_A))))                                                    //Natural: IF #MM >= #YEAR-TABLE ( #MDE-SUB,#A )
                {
                    pnd_Number_Of_Payments.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUMBER-OF-PAYMENTS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "NONE OF 3 YTD CONDITIONS OCCURRED");                                                                                                   //Natural: WRITE 'NONE OF 3 YTD CONDITIONS OCCURRED'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ytd_Tot.compute(new ComputeParameters(false, pnd_Ytd_Tot), pnd_Per_Ivc_Hold.multiply(pnd_Number_Of_Payments));                                                //Natural: COMPUTE #YTD-TOT = #PER-IVC-HOLD * #NUMBER-OF-PAYMENTS
        //*  WRITE '='  #YTD-TOT '=' #PER-IVC-HOLD '=' #NUMBER-OF-PAYMENTS
    }
    private void sub_Pnd_Mode_Para() throws Exception                                                                                                                     //Natural: #MODE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Mde_Sub.reset();                                                                                                                                              //Natural: RESET #MDE-SUB
        short decideConditionsMet2251 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #CNTRCT-MODE-TOT;//Natural: VALUE 100
        if (condition((pnd_Cntrct_Mode_Tot.equals(100))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(1);                                                                                                                                      //Natural: MOVE 1 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Cntrct_Mode_Tot.equals(601))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(2);                                                                                                                                      //Natural: MOVE 2 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Cntrct_Mode_Tot.equals(602))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(3);                                                                                                                                      //Natural: MOVE 3 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Cntrct_Mode_Tot.equals(603))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(4);                                                                                                                                      //Natural: MOVE 4 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Cntrct_Mode_Tot.equals(701))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(5);                                                                                                                                      //Natural: MOVE 5 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Cntrct_Mode_Tot.equals(702))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(6);                                                                                                                                      //Natural: MOVE 6 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Cntrct_Mode_Tot.equals(703))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(7);                                                                                                                                      //Natural: MOVE 7 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Cntrct_Mode_Tot.equals(704))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(8);                                                                                                                                      //Natural: MOVE 8 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Cntrct_Mode_Tot.equals(705))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(9);                                                                                                                                      //Natural: MOVE 9 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Cntrct_Mode_Tot.equals(706))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(10);                                                                                                                                     //Natural: MOVE 10 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Cntrct_Mode_Tot.equals(801))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(11);                                                                                                                                     //Natural: MOVE 11 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Cntrct_Mode_Tot.equals(802))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(12);                                                                                                                                     //Natural: MOVE 12 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Cntrct_Mode_Tot.equals(803))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(13);                                                                                                                                     //Natural: MOVE 13 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Cntrct_Mode_Tot.equals(804))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(14);                                                                                                                                     //Natural: MOVE 14 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Cntrct_Mode_Tot.equals(805))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(15);                                                                                                                                     //Natural: MOVE 15 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Cntrct_Mode_Tot.equals(806))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(16);                                                                                                                                     //Natural: MOVE 16 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Cntrct_Mode_Tot.equals(807))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(17);                                                                                                                                     //Natural: MOVE 17 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Cntrct_Mode_Tot.equals(808))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(18);                                                                                                                                     //Natural: MOVE 18 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Cntrct_Mode_Tot.equals(809))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(19);                                                                                                                                     //Natural: MOVE 19 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Cntrct_Mode_Tot.equals(810))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(20);                                                                                                                                     //Natural: MOVE 20 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Cntrct_Mode_Tot.equals(811))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(21);                                                                                                                                     //Natural: MOVE 21 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Cntrct_Mode_Tot.equals(812))))
        {
            decideConditionsMet2251++;
            pnd_Mde_Sub.setValue(22);                                                                                                                                     //Natural: MOVE 22 TO #MDE-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(5),"PROGRAM: ",Global.getPROGRAM(),new TabSetting(31),"IA ADMINISTRATION IAR TAX MAINTENANCE CONTROL REPORT",new  //Natural: WRITE ( 1 ) NOTITLE 5T 'PROGRAM: ' *PROGRAM 31T 'IA ADMINISTRATION IAR TAX MAINTENANCE CONTROL REPORT' 111T 'PAGE' *PAGE-NUMBER ( 1 ) / 8T 'DATE: ' *DATU 45T / 2T 'CHECK DATE: ' #CHK-DTE //
                        TabSetting(111),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(8),"DATE: ",Global.getDATU(),new TabSetting(45),NEWLINE,new 
                        TabSetting(2),"CHECK DATE: ",pnd_Chk_Dte,NEWLINE,NEWLINE);
                    //*     ///  2T 'TOTAL RECORDS SELECTED'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CMP",new TabSetting(6),"CONTRACT",new TabSetting(15),"PY",new TabSetting(19),"TAX-ID-NUMB",new  //Natural: WRITE ( 1 ) 001T 'CMP' 006T 'CONTRACT' 015T 'PY' 019T 'TAX-ID-NUMB' 032T 'SRC' 037T 'MN' 041T 'TRANS DATE' 053T 'CDE' 058T 'C' 061T 'PERIODIC IVC' 075T 'RES' 080T 'TAX-ID-NUMB' 093T ' 1ST DOB' 103T ' 2ND DOB' 113T 'CTO' 117T ' TOTAL IVC  '
                        TabSetting(32),"SRC",new TabSetting(37),"MN",new TabSetting(41),"TRANS DATE",new TabSetting(53),"CDE",new TabSetting(58),"C",new 
                        TabSetting(61),"PERIODIC IVC",new TabSetting(75),"RES",new TabSetting(80),"TAX-ID-NUMB",new TabSetting(93)," 1ST DOB",new TabSetting(103)," 2ND DOB",new 
                        TabSetting(113),"CTO",new TabSetting(117)," TOTAL IVC  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(41),"1ST PAY PD",new TabSetting(58),"C",new TabSetting(61),"     RTB IVC",new               //Natural: WRITE ( 1 ) 041T '1ST PAY PD' 058T 'C' 061T '     RTB IVC' 075T 'CDE' 080T 'TRN EFF DTE' 093T ' 1ST DOD' 103T ' 2ND DOD' 113T 'CTN' 117T '   YTD IVC  '
                        TabSetting(75),"CDE",new TabSetting(80),"TRN EFF DTE",new TabSetting(93)," 1ST DOD",new TabSetting(103)," 2ND DOD",new TabSetting(113),"CTN",new 
                        TabSetting(117),"   YTD IVC  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---",new TabSetting(6),"--------",new TabSetting(15),"--",new TabSetting(19),"-----------",new  //Natural: WRITE ( 1 ) 001T '---' 006T '--------' 015T '--' 019T '-----------' 032T '---' 037T '--' 041T '----------' 053T '---' 058T '-' 061T '------------' 075T '---' 080T '-----------' 093T '--------' 103T '--------' 113T '--' 117T '------------' /
                        TabSetting(32),"---",new TabSetting(37),"--",new TabSetting(41),"----------",new TabSetting(53),"---",new TabSetting(58),"-",new 
                        TabSetting(61),"------------",new TabSetting(75),"---",new TabSetting(80),"-----------",new TabSetting(93),"--------",new TabSetting(103),"--------",new 
                        TabSetting(113),"--",new TabSetting(117),"------------",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56 ZP=OFF SG=OFF");
        Global.format(1, "LS=133 PS=56 ZP=OFF SG=OFF");
    }
}
